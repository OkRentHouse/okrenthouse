<?php

abstract class Controller
{
	public $_errors = [];        //錯誤
	public $_msgs = [];        //錯誤
	protected $display_header = true;
	protected static $instance = null;        //是否實立
	/** @var Context */
	protected $context;

	/** @var array List of CSS files */
	public $css_files = [];

	public $css_header_files = [];

	public $css_footer_files = [];

	/** 設定沒有權限頁面*/
	public $layout = '';

	/** @var array List of JavaScript files */
	public $js_files = [];

	public $js_header_files = [];

	public $js_footer_files = [];

	/** @var array List of PHP errors */
	public static $php_errors = [];

	/** @var bool Set to true to display page header javascript */
	protected $display_header_javascript = true;

	/** @var string Template filename for the page content */
	protected $template;

	/** 自己的設定*/
	protected $my;

	/** @var string Set to true to display page footer */
	protected $display_footer = true;

	/** @var bool Set to true to only render page content (used to get iframe content) */
	protected $content_only = false;

	protected $content = '';

	/** @var bool 如果請求為 AJAX, set true */
	public $ajax = false;

	/** @var bool 如果請求為 XML, set true */
	public $xml = false;

	protected $breadcrumbs = [];
	protected $breadcrumbs_txt = [];

	/** @var bool If set to true, page content and messages will be encoded to JSON before responding to AJAX request */
	protected $json = false;

	/** @var string JSON response status string */
	protected $status = '';

	protected $page_header_toolbar_title;
	protected $display_page_header_toolbar;
	protected $back_url;


	/**
	 * @see Controller::run()
	 * @var string|null Redirect link. If not empty, the user will be redirected after initializing and processing input.
	 */
	protected $redirect_after = null;

	/** @var values: 'front', 'frontmodule', 'admin', 'adminmodule' */
	public $controller_type;

	/** @var string Controller name */
	public $page;

	public $UpFileVal;

	/**
	 * Check if the controller is available for the current user/visitor
	 */
	abstract public function checkAccess();

	/**
	 * Check if the current user/visitor has valid view permissions
	 */
	abstract public function viewAccess();

	abstract public function setMedia();

	abstract public function postProcess();

	/**
	 * Initialize the page
	 */
	public function init()
	{
	}

	public function __construct()
	{
		if (Configuration::get('WEB_SSL_ENABLED') && !Tools::usingSecureMode()) {
			Tools::redirectLink(Tools::getHttpHost(true) . $_SERVER['REQUEST_URI']);
		}
		define('THEME', Themes::getContext()->getTheme());            //樣版名稱
		if (Context::getContext()->getMobileDevice() || Dispatcher::getContext()->is_app) {
			define('THEME_URL', '/themes/' . THEME . '/mobile');                            //樣版資料夾位置
		} else {
			define('THEME_URL', '/themes/' . THEME);                            //樣版資料夾位置
		}
		define('THEME_DIR', WEB_DIR . DS . 'themes' . DS . THEME . DS);        //樣版資料夾位置
		define('THEME_MOBILE_DIR', THEME_DIR . 'mobile' . DS);                    //手機樣版資料夾位置
		$this->ajax = Tools::getValue('ajax') || Tools::isSubmit('ajax');
		if (is_null($this->display_header))
			$this->display_header = true;
		if (is_null($this->display_header_javascript))
			$this->display_header_javascript = true;
		if (is_null($this->display_footer))
			$this->display_footer = true;
		$this->context             = Context::getContext();
		$this->context->controller = $this;
		if (is_file(CACHE_DIR . DS . 'css_index.php') && count($this->context->css_list) == 0)
			$this->context->css_list = include(CACHE_DIR . DS . 'css_index.php');
		if (is_file(CACHE_DIR . DS . 'js_index.php') && count($this->context->js_list) == 0)
			$this->context->js_list = include(CACHE_DIR . DS . 'js_index.php');
		if (!headers_sent()
			&& isset($_SERVER['HTTP_USER_AGENT'])
			&& (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false
				|| strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false)) {
			header('X-UA-Compatible: IE=edge,chrome=1');
		}
	}

	/**
	 * Assigns Smarty variables for the page header
	 */
	abstract public function initHeader();

	/**
	 * Assigns Smarty variables for the page main content
	 */
	abstract public function initContent();

	/**
	 * 設定沒有權限頁面
	 */
	abstract public function initRefusePage();

	/**
	 * Assigns Smarty variables for the page footer
	 */
	abstract public function initFooter();

	/**
	 * Sets page header display
	 *
	 * @param bool $display
	 */
	public function displayHeader($display = true)
	{
		$this->display_header = $display;
	}

	public static function getController($class_name)
	{
		//只有前台才會有重複Class,後台(Admin不用)
		if (Dispatcher::getContext()->controller_type == 'front') {
			$class_name = SYSTEM_FOLDER . '\\' . $class_name;
		}
		if (class_exists($class_name)) {
			return new $class_name();
		} else {
			return false;
		};
	}

	public function run()
	{
		$this->init();
		if ($this->checkAccess()) {
			//媒體
			if (!$this->content_only && ($this->display_header || (isset($this->className) && $this->className)))
				$this->setMedia();

			//POST處理 模組POST等
			$this->postProcess();

			//重定向
			if (!empty($this->redirect_after))
				$this->redirect();

			//初始化Header
			if (!$this->content_only && ($this->display_header || (isset($this->className) && $this->className)))
				$this->initHeader();

			if ($this->viewAccess()) {
				$this->initContent();
			} else {
				$this->_errors[] = $this->l('您沒有權限查看此頁!');
			};

			//初始Footer
			if (!$this->content_only && ($this->display_footer || (isset($this->className) && $this->className)))
				$this->initFooter();

			// action = $_POST['action'] or $_GET['action']
			// 執行 displayAjax[action]

			if ($this->ajax) {
				$action = str_replace('action', '', Tools::getValue('action'));
				if (!empty($action) && method_exists($this, 'displayAjax' . $action)) {
					$this->{'displayAjax' . $action}();
				} elseif (method_exists($this, 'displayAjax')) {
					$this->displayAjax();
				}
			} else {
				$this->display();
			}
		} else {
			$this->initRefusePage();//沒有權限頁面
			$this->smartyOutputContent($this->layout);
		}
	}

	//輸出smarty內容
	protected function smartyOutputContent($content)
	{
		$html = '';
		if (is_array($content)) {
			foreach ($content as $tpl) {
				$html .= $this->context->smarty->fetch($tpl);
			}
		} else {
			$html = $this->context->smarty->fetch($content);
		};
		$html = trim($html);
		if (in_array($this->controller_type, ['front', 'modulefront']) && !empty($html) && $this->getLayout()) {
			echo $html;
		} else {
			echo $html;
		};
	}

	/**
	 * Sets page header javascript display
	 *
	 * @param bool $display
	 */
	public function displayHeaderJavaScript($display = true)
	{
		$this->display_header_javascript = $display;
	}

	/**
	 * Sets page header display
	 *
	 * @param bool $display
	 */
	public function displayFooter($display = true)
	{
		$this->display_footer = $display;
	}

	/**
	 * Sets template file for page content output
	 *
	 * @param string $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

	/**
	 * Redirects to $this->redirect_after after the process if there is no error
	 */
	abstract protected function redirect();

	abstract public function display();

	abstract public function displayAjax();

	public function addJS($js_uri, $first = false, $version = null)
	{
		if (!empty($version))
			$version = '_' . $version;
		if (!is_array($js_uri)) {
			$js_uri = [$js_uri . '?v=' . WEB_VERSION . $version];
		} else {
			foreach ($js_uri as $i => $v) {
				$js_uri[$i] = $v . '?v=' . WEB_VERSION . $version;
			}
		};
		if ($first) {
			$js_uri = array_reverse($js_uri);
			foreach ($js_uri as $i => $js) {
				array_unshift($this->js_files, $js);
			}
		} else {
			$this->js_files = array_merge($this->js_files, $js_uri);
		}
	}

	public function addJquery()
	{

	}

	public function addjQueryPlugin()
	{

	}

	public function addJqueryUI()
	{
		/*
		if(!is_array($component)){
			$component = array($component);
		  }
	
		  foreach ($component as $ui){
			$ui_path = Media::getJqueryUIPath($ui, $theme, $check_dependencies);
			$this->addCSS($ui_path['css'], 'all', false);
			$this->addJS($ui_path['js'], false);
		  }
		  */
	}

	public function removeJS($js_uri, $check_path = true)
	{
		if (is_array($js_uri)) {
			foreach ($js_uri as $js_file) {
				$js_path = $js_file;
				if ($check_path) {
					$js_path = Media::getJSPath($js_file);
				};
				if ($js_path && in_array($js_path, $this->js_files)) {
					unset($this->js_files[array_search($js_path, $this->js_files)]);
				};
			};
		} else {
			$js_path = $js_uri;
			if ($check_path) {
				$js_path = Media::getJSPath($js_uri);
			};
			if ($js_path) {
				unset($this->js_files[array_search($js_path, $this->js_files)]);
			};
		};
	}

	public function addCSS($css_uri, $first = false, $version = null)
	{
		if (!empty($version))
			$version = '_' . $version;
		if (!is_array($css_uri)) {
			$css_uri = [$css_uri . '?v=' . WEB_VERSION . $version];
		} else {
			foreach ($css_uri as $i => $v) {
				$css_uri[$i] = $v . '?v=' . WEB_VERSION . $version;
			}
		}
		if ($first) {
			$css_uri = array_reverse($css_uri);    //陣列反轉
			foreach ($css_uri as $i => $css) {
				array_unshift($this->css_files, $css);
			}
		} else {
			$this->css_files = array_merge($this->css_files, $css_uri);
		};
	}

	public function addFooterCSS($css_uri, $first = false, $version = null)
	{
		if (!empty($version))
			$version = '_' . $version;
		if (!is_array($css_uri)) {
			$css_uri = [$css_uri . '?v=' . WEB_VERSION . $version];
		} else {
			foreach ($css_uri as $i => $v) {
				$css_uri[$i] = $v . '?v=' . WEB_VERSION . $version;
			}
		}
		if ($first) {
			$css_uri = array_reverse($css_uri);    //陣列反轉
			foreach ($css_uri as $i => $css) {
				array_unshift($this->css_footer_files, $css);
			}
		} else {
			$this->css_footer_files = array_merge($this->css_footer_files, $css_uri);
		}
	}

	public function addFooterJS($js_uri, $first = false, $version = null)
	{
		if (!empty($version))
			$version = '_' . $version;
		if (!is_array($js_uri)) {
			$js_uri = [$js_uri . '?v=' . WEB_VERSION . $version];
		} else {
			foreach ($js_uri as $i => $v) {
				$js_uri[$i] = $v . '?v=' . WEB_VERSION . $version;
			}
		}
		if ($first) {
			$js_uri = array_reverse($js_uri);    //陣列反轉
			foreach ($js_uri as $i => $js) {
				array_unshift($this->js_footer_files, $js);
			}
		} else {
			$this->js_footer_files = array_merge($this->js_footer_files, $js_uri);
		};
	}

	public function addHeaderCSS($css_uri, $first = false, $version = null)
	{
		if (!empty($version))
			$version = '_' . $version;
		if (!is_array($css_uri)) {
			$css_uri = [$css_uri . '?v=' . WEB_VERSION . $version];
		} else {
			foreach ($css_uri as $i => $v) {
				$css_uri[$i] = $v . '?v=' . WEB_VERSION . $version;
			}
		}
		if ($first) {
			$css_uri = array_reverse($css_uri);    //陣列反轉
			foreach ($css_uri as $i => $css) {
				array_unshift($this->css_header_files, $css);
			}
		} else {
			$this->css_header_files = array_merge($this->css_header_files, $css_uri);
		};
	}

	public function addHeaderJS($js_uri, $first = false, $version = null)
	{
		if (!empty($version))
			$version = '_' . $version;
		if (!is_array($js_uri)) {
			$js_uri = [$js_uri . '?v=' . WEB_VERSION . $version];
		} else {
			foreach ($js_uri as $i => $v) {
				$js_uri[$i] = $v . '?v=' . WEB_VERSION . $version;
			}
		}
		if ($first) {
			$js_uri = array_reverse($js_uri);    //陣列反轉
			foreach ($js_uri as $i => $js) {
				array_unshift($this->js_header_files, $js);
			}
		} else {
			$this->js_header_files = array_merge($this->js_header_files, $js_uri);
		};
	}

	public function removeCSS()
	{

	}

	public function l($str)
	{
		return $str;
	}
}