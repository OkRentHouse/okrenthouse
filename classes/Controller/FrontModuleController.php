<?php
require_once (__DIR__ .DS. 'Controller.php');
class FrontModuleController extends FrontController{
	public $module; //模組ID
	
	public function __construct(){	//數值
		$this->module = Module::getInstanceByName(Tools::getValue('module'));
		if(empty($this->module)){
			$this->module = Dispatcher::getContext()->module;
		}
		if(is_file(MODULE_DIR.DS.$this->module.DS.'Autoload.php')){
			include_once(MODULE_DIR.DS.$this->module.DS.'Autoload.php');
		}
		$this->controller_type = 'frontmodule';
		Context::getContext()->smarty->setTemplateDir(array(
			OVERRIDE_MODULES_DIR.DS.$this->module.DS.'templates'.DS.'front',
			OVERRIDE_MODULES_DIR.DS.$this->module,
			WEB_DIR.DS.'modules'.DS.$this->module.DS.'templates'.DS.'front',
			WEB_DIR.DS.'modules'.DS.$this->module,
		));
		parent::__construct();
		Context::getContext()->smarty->setTemplateDir(array(
			OVERRIDE_DIR.DS.'modules'.DS.$this->module,
			MODULE_DIR.DS.$this->theme
		));
	}
	
	public function setMedia(){
		parent::setMedia();
		if(is_file(MODULE_DIR.DS.$this->module.DS.'css'.DS.strtolower($this->module).'.min.css')){
			$this->addCSS(MODULE_URL.'/'.$this->module.'/css/'.strtolower($this->module).'.min.css');
		}elseif(is_file(MODULE_DIR.DS.$this->module.DS.'css'.DS.strtolower($this->module).'.css')){
			$this->addCSS(MODULE_URL.'/'.$this->module.'/css/'.strtolower($this->module).'.css');
		}
		
		if(is_file(MODULE_DIR.DS.$this->module.DS.'js'.DS.strtolower($this->module).'.min.js')){
			$this->addJS(MODULE_URL.'/'.$this->module.'/js/'.strtolower($this->module).'.min.js');
		}elseif(is_file(MODULE_DIR.DS.$this->module.DS.'js'.DS.strtolower($this->module).'.js')){
			$this->addJS(MODULE_URL.'/'.$this->module.'/js/'.strtolower($this->module).'.js');
		}
	}
	
}