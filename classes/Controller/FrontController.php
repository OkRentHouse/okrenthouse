<?php
include_once(__DIR__ . DS . 'Controller.php');

class FrontController extends Controller
{
	protected $maintenance = false;            //網站維修中
	protected $_errors_name = [];        //錯誤欄位
	protected $_errors_name_i = [];        //錯誤欄位
	protected static $cookie;                    //cookie
	protected static $smarty;                    //smarty
	protected static $link;                    //連結
	protected static $cart;                    //購物車
	public $page;                            //頁面名稱
	public $tabAccess;
	public static $currentIndex;
	public $_conf;
	public $_conf2;

	public $meta_title;
	public $meta_description;
	public $meta_keywords;
    public $meta_img;
    public $meta_icon;
	public $table;
	public $theme;
	public $fields;
	public $display_media_bak = true;
	public $display_edit_div = true;    //顯示編輯欄位
	public $mamber_url;
	public $no_link = true;        //點列表沒有聯結
	public $FormTable;
	public $nobots = false;
	public $nofollow = false;
	public $display_main_menu = true;    //主選單
	public $no_information = '';        //沒有資料文字
	public $no_FormTable = true;            //不啟用表單模組
	public $have_login = false;                //不需要登入
	public $no_page = false;
    public $WEB_NAME;           //meta_title 如果不使用 別名WEB_NAME 後面贅述
    public $conn;//連接用
    public $fb_app_id;          //fb登入的id
    public $fb_api_version;     //fb登入的版本
	public $controller_name;    //通常是別編給的預設值
	//數值
    public $actions=[];//預設值arr

	public function __construct()
	{
		$this->theme           = Themes::getContext()->getTheme();
		$this->no_information  = $this->l('找不到資料!');
		$this->controller_type = 'front';


		if (empty($this->index))
			$this->index = $this->fields['index'];

		parent::__construct();

		$this->_conf = [
			'1' => $this->l('新增成功!'),
			'2' => $this->l('修改成功!'),
			'3' => $this->l('刪除成功'),
		];

		$this->_conf2 = [
			'1' => $this->l('沒有刪除任何資料!'),
			'2' => $this->l('請先修改密碼才能啟用帳號'),
			'3' => $this->l('您沒有檢視權限'),
			'4' => $this->l('您沒有新增權限'),
			'5' => $this->l('您沒有修改權限'),
			'6' => $this->l('您沒有刪除權限'),
			'7' => $this->l('找不到檔案'),
			'8' => sprintf($this->l('%s一個使用者只能在一支手機上登入<br>無法將在前一台手機上使用!'), WEB_NAME),
		];

		foreach ($this->tabAccess as $i => $v) {
			if (in_array($i, ['view', 'add', 'edit', 'del'])) {
				if ($v)
					$this->actions[] = $i;
			};
		};

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new FrontController();
		};
		return self::$instance;
	}

	public function getLanguages()
	{
		return $this->_languages;
	}

	//初始化設定
	public function init()
	{
		$dir = '';
		if (Dispatcher::getContext()->is_media) {        //手機模式
			$dir = 'mobile' . DS;
		}

		if (Dispatcher::getContext()->is_app) {        //APP模式
			$dir = 'mobile' . DS;
		}

		$this->controller_name = array_pop(explode('\\', get_class($this)));
		if (strpos($this->controller_name, 'Controller')) {
			$this->controller_name = substr($this->controller_name, 0, -10);
		};

		//AdminController 會使用前台參數，所以不使用smarty
		if ($this->controller_name != 'Front') {
			Context::getContext()->smarty->setTemplateDir([
				OVERRIDE_DIR . DS . 'front' . DS . 'templates' . DS . $dir . 'controllers' . DS . $this->controller_name,        //覆蓋 / 樣板 / controllers
				OVERRIDE_DIR . DS . 'front' . DS . 'templates' . DS . $dir,                                    //覆蓋 / 樣板
				THEME_DIR . $dir . 'controllers' . DS . $this->controller_name,                            //樣板 / controllers
				THEME_DIR . $dir                                                        //樣板
			]);
			Context::getContext()->smarty->assign([
				'WEB_VERSION'          => WEB_VERSION,
				'THEME_URL'            => THEME_URL,
				'FormTable'            => '[]',
				'initialPreviewConfig' => '[]',
				'UpFileVal'            => '[]',
			]);
		}
		parent::init();
		$this->controller_name = array_pop(explode('\\', get_class($this)));
		Member::autologoing();
		if (Tools::isSubmit('logout'))
			Member::logout();
		if (strpos($this->controller_name, 'Controller')) {
			$this->controller_name = substr($this->controller_name, 0, -10);
		};
		if (empty($this->page))
			$this->page = $this->controller_name;

		if ($this->have_login) {
			$this->login();
		}
		$this->initProcess();

		foreach ($this->fields['form'] as $j => $jv) {
			foreach ($jv['input'] as $key => $v) {
				if ($this->display == 'add') {
					if ($v['type'] == 'change-password') {
						unset($this->fields['form'][$j]['input'][$key]);
					}
				} elseif ($this->display == 'edit') {
					if ($v['type'] == 'new-password' || $v['type'] == 'confirm-password') {
						unset($this->fields['form'][$j]['input'][$key]);
					}
				}
				if ($v['type'] == 'signature') {    //如果是簽名
					if (!isset($v['col'])) {
						$this->fields['form'][$j]['input']['col'] = 12;
					}
					if (!isset($v['signature']['screen_full'])) {        //全螢幕
						$this->fields['form'][$j]['input'][$key]['signature']['screen_full'] = true;
					}
					if ($this->fields['form'][$j]['input'][$key]['signature']['screen_full']) {
						$this->fields['form'][$j]['input'][$key]['signature']['screen_full'] = 'true';
					} else {
						$this->fields['form'][$j]['input'][$key]['signature']['screen_full'] = 'false';
					}
					if (!isset($v['signature']['width'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['width'] = 850;
					}
					if (!isset($v['signature']['height'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['height'] = 215;
					}
					if (!isset($v['signature']['color'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['color'] = '#000';
					}
					if (!isset($v['signature']['background-color'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['background-color'] = '#fff';
					}
					if (!isset($v['signature']['lineWidth'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['lineWidth'] = 1;
					}
				}
				if ($v['type'] == 'take_photo') {    //如果是拍照
					if (Tools::usingSecureMode()) {
						//相機初始設定
						if (!isset($v['take_photo']['open_txt'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['open_txt'] = $this->l('開啟相機');
						}
						if (!isset($v['take_photo']['close_txt'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['close_txt'] = $this->l('關閉相機');
						}
						if (!isset($v['take_photo']['take_txt'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['take_txt'] = $this->l('拍照');
						}
						if (!isset($v['take_photo']['height'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['height'] = 225;
						}
						if (!isset($v['take_photo']['width'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['width'] = 300;
						}
						if (!isset($v['take_photo']['font_size'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['font_size'] = '40px';
						}
						if (!isset($v['take_photo']['min_height'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['min_height'] = 30;
						}
						if (!isset($v['take_photo']['min_width'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['min_width'] = 40;
						}
						if (!isset($v['take_photo']['confirm'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['confirm'] = $this->l('您確定要刪除此照片?');
						}
					} else {
						Tools::redirectLink('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
					}
				}
			}
		}
		$this->mamber_url   = str_replace('Controller', '', $this->controller_name);
		self::$currentIndex = '/' . $this->mamber_url . '?';
		$this->context->smarty->assign([
			'table'           => $this->table,
			'controller_name' => $this->controller_name,
			'page'            => $this->page,
			'no_link'         => $this->no_link,
			'mamber_url'      => $this->mamber_url,
			'form_action'     => self::$currentIndex,
		]);

		$this->context->smarty->assign([
			'tpl_dir'                  => THEME_DIR,
			'tpl_mobile_dir'           => THEME_MOBILE_DIR,
			'tpl_uri'                  => THEME_URL,
			'modules_dir'              => MODULE_DIR,
			'GOOGLE_SITE_VERIFICATION' => Configuration::get('GOOGLE_SITE_VERIFICATION'),
			'lang_iso'                 => $this->context->language->iso_code,
			'lang_id'                  => (int)$this->context->language->id,
			'come_from'                => Tools::getHttpHost(true, true) . Tools::htmlentitiesUTF8(str_replace(['\'', '\\'], '', urldecode($_SERVER['REQUEST_URI']))),
			'languages'                => $languages,
			'link'                     => $link,
			'page'                     => $this->page,
			'hide_left_column'         => !$this->display_column_left,
			'hide_right_column'        => !$this->display_column_right,
			'CSS_DIR'                  => CSS_DIR,
			'JS_DIR'                   => JS_DIR,
			'AJAX_DIR'                 => AJAX_DIR,
			'NAVIGATION_PIPE'          => NAVIGATION_PIPE,
		]);
		self::$cookie = $this->context->cookie;
		self::$smarty = $this->context->smarty;
		self::$link   = $link;
		$this->initBreadcrumbs();
		$this->displayMaintenancePage();
	}

	//確認當前使用者
	public function checkAccess()
	{
		return true;
	}

	//有瀏覽權限
	public function viewAccess()
	{
		return true;
	}

	public function initToolbarTitle()
	{
		if (empty($this->fields['title'])) {
			$this->fields['title'] = $this->page_header_toolbar_title;
		}
	}

	public function initPageHeaderToolbar()
	{
		if (empty($this->toolbar_title)) {
			$this->initToolbarTitle();
		};

		if (!is_array($this->toolbar_title)) {
			$this->toolbar_title = [$this->toolbar_title];
		};
		if (empty($this->page_header_toolbar_title)) {    //標題
			$this->page_header_toolbar_title = $this->toolbar_title[count($this->toolbar_title) - 1];
		};
	}

	//加入標題
	public function addMetaTitle($entry)
	{
		if (is_array($this->meta_title)) {
			$this->meta_title[] = $entry;
		};
	}

	//初始化麵包屑
	public function initBreadcrumbs($tab_id = null)
	{
		foreach ($this->breadcrumbs as $i => $v) {
			$this->breadcrumbs_txt[] = $this->l($v['title']);
		}
		$this->context->smarty->assign([
			'breadcrumbs'     => $this->breadcrumbs,
			'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
		]);
	}

	//設訂CSS JS
	public function setMedia()
	{
		if (is_file(CACHE_DIR . DS . 'css_index.php') && count($this->context->css_list) == 0)
			$this->context->css_list = include_once(CACHE_DIR . DS . 'css_index.php');
		if (is_file(CACHE_DIR . DS . 'js_index.php') && count($this->context->js_list) == 0)
			$this->context->js_list = include_once(CACHE_DIR . DS . 'js_index.php');


		if (is_file(THEME_DIR . 'css' . DS . 'reset.css')) {
			$this->addCSS(THEME_URL . '/css/reset.css');
		}

		if (Context::getContext()->getMobileDevice() || Dispatcher::getContext()->is_app) {    //手機版

			/* todo 改到樣板裡去 Start */
			$this->addCSS('/' . MEDIA_URL . '/bootstrap-3.3.7/css/bootstrap.min.css');
			$this->addCSS('/' . MEDIA_URL . '/jquery.mobile-1.4.5/css/jquery.mobile.inline-svg-1.4.5.min.css');
//			$this->addCSS('/' . MEDIA_URL . '/jquery.growl-1.3.2/jquery.growl.css');							//提醒

			$this->addJS($this->context->js_list['jquery-1.12.4.min']);
            $this->addJS( '/js/jquery.mobile_step.js');//用非ajax請求
			$this->addJS('/' . MEDIA_URL . '/jquery.mobile-1.4.5/js/jquery.mobile-1.4.5.js');//問題在這邊
			$this->addJS('/' . MEDIA_URL . '/jquery.lazy-master/jquery.lazy.min.js');
//			$this->addJS('/' . MEDIA_URL . '/bootstrap-3.3.7/js/bootstrap.min.js');
//			$this->addJS('/' . MEDIA_URL . '/jquery.growl-1.3.2/jquery.growl.js');								//提醒
			$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/jquery.validate.min.js');
			$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/additional-methods.min.js');
			$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/localization/messages_zh_TW.js');
			$this->addJS($this->context->js_list['model']);

			//彈出訊息
			$this->addCSS('/' . MEDIA_URL . '/sweetalert2/css/sweetalert2.min.css');
			$this->addJS('/' . MEDIA_URL . '/sweetalert2/js/sweetalert2.all.min.js');

			/* todo 改到樣板裡去 End */

			if (1) {        //todo 快取
				if (is_file(THEME_DIR . 'mobile' . DS . 'css' . DS . 'main.min.css')) {
					$this->addCSS(THEME_URL . '/css/main.min.css');
					$arr_file['main.min'] = THEME_URL . '/css/main.min.css';
				} elseif (is_file(THEME_DIR . 'mobile' . DS . 'css' . DS . 'main.css')) {
					$this->addCSS(THEME_URL . '/css/main.css');
					$arr_file['main'] = THEME_URL . '/css/main.css';
				}

				if (is_file(THEME_DIR . 'mobile' . DS . 'js' . DS . 'main.min.js')) {
					$this->addJS(THEME_URL . '/js/main.min.js');
					$arr_file['main.min'] = THEME_URL . '/js/main.min.js';
				} elseif (is_file(THEME_DIR . 'mobile' . DS . 'js' . DS . 'main.js')) {
					$this->addJS(THEME_URL . '/js/main.js');
					$arr_file['main'] = THEME_URL . '/js/main.js';
				}

				//手機字體	有些字體在手機(IOS會閃爍)
				if (check_mobile()) {
					if (is_file(THEME_DIR . 'css' . DS . 'mobile_font.min.css')) {
						$this->addCSS(THEME_URL . '/css/mobile_font.min.css');
						$arr_file['mobile_font.min'] = THEME_URL . '/css/mobile_font.min.css';
					} elseif (is_file(THEME_DIR . 'css' . DS . 'mobile_font.css')) {
						$this->addCSS(THEME_URL . '/css/mobile_font.css');
						$arr_file['mobile_font'] = THEME_URL . '/css/mobile_font.css';
					}
				} else {
					if (is_file(THEME_DIR . 'css' . DS . 'not_mobile_font.min.css')) {
						$this->addCSS(THEME_URL . '/css/not_mobile_font.min.css');
						$arr_file['not_mobile_font.min'] = THEME_URL . '/media/css/not_mobile_font.min.css';
					} elseif (is_file(THEME_DIR . 'css' . DS . 'not_mobile_font.css')) {
						$this->addCSS(THEME_URL . '/css/not_mobile_font.css');
						$arr_file['not_mobile_font'] = THEME_URL . '/css/not_mobile_font.css';
					}
				}

				if (is_file(THEME_DIR . 'mobile' . DS . 'css' . DS . 'main_animate.min.css')) {
					$this->addCSS(THEME_URL . '/css/main_animate.min.css');
					$arr_file['main_animate.min'] = THEME_URL . '/css/main_animate.min.css';
				} elseif (is_file(THEME_DIR . 'mobile' . DS . 'css' . DS . 'main_animate.css')) {
					$this->addCSS(THEME_URL . '/css/main_animate.css');
					$arr_file['main_animate'] = THEME_URL . '/css/main_animate.css';
				}
//				set_cache_index($arr_file, 'front_css_footer_index');
			}


			if (is_file(THEME_DIR . 'mobile' . DS . 'css' . DS . strtolower($this->controller_name) . '.min.css')) {
				$this->addFooterCSS(THEME_URL . '/css/' . strtolower($this->controller_name) . '.min.css');
			} elseif (is_file(THEME_DIR . 'mobile' . DS . 'css' . DS . strtolower($this->controller_name) . '.css')) {
				$this->addFooterCSS(THEME_URL . '/css/' . strtolower($this->controller_name) . '.css');
			}

			if (is_file(THEME_DIR . 'mobile' . DS . 'js' . DS . strtolower($this->controller_name) . '.min.js')) {
				$this->addFooterJS(THEME_URL . '/js/' . strtolower($this->controller_name) . '.min.js');
			} elseif (is_file(THEME_DIR . 'mobile' . DS . 'js' . DS . strtolower($this->controller_name) . '.js')) {
				$this->addFooterJS(THEME_URL . '/js/' . strtolower($this->controller_name) . '.js');
			}


			$fileinput = false;
			foreach ($this->fields['form'] as $key => $form) {
				foreach ($form['input'] as $i => $v) {            //表單驗證
					switch ($v['type']) {
						case 'signature':    //簽名
							$this->addHeaderCSS('/' . MEDIA_URL . '/jSignature/css/jSignature.css');
							$this->addFooterJS('/' . MEDIA_URL . '/jSignature/js/jSignature.min.js');
							$this->addFooterJS('/' . MEDIA_URL . '/jSignature/js/jSignature.min.noconflict.js');
							$this->addFooterJS($this->context->js_list['screenfull.min']);
						break;
						case 'take_photo':    //拍照
							$this->addHeaderCSS('/' . MEDIA_URL . '/webcam/css/webcam.css');
							$this->addHeaderJS('/' . MEDIA_URL . '/webcam/js/mp3.js');
							$this->addHeaderJS('/' . MEDIA_URL . '/webcam/js/webcam.min.js');
						break;
						case 'file':            //檔案上傳
							$this->addHeaderCSS('/' . MEDIA_URL . '/bootstrap-fileinput-master/css/fileinput.css');
							$this->addHeaderJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/plugins/sortable.min.js');
							$this->addHeaderJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/plugins/purify.min.js');
							$this->addHeaderJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/fileinput.min.js');
							$this->addHeaderJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/locales/zh-TW.js');
						break;
					}
				}
			}

		} else {                            //電腦版
			/* todo 改到樣板裡去 Start */
            $this->addCSS('/' . MEDIA_URL . '/bootstrap-3.3.7/css/bootstrap.min.css');
			$this->addCSS('/' . MEDIA_URL . '/bootstrap-3.3.7/css/bootstrap-theme.min.css');
			$this->addCSS('/' . MEDIA_URL . '/jquery.growl-1.3.2/jquery.growl.css');
			$this->addCSS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/css/bootstrap-select.min.css');
			$this->addJquery();
			$this->addjQueryPlugin();
			$this->addJqueryUI();
			$this->addJS($this->context->js_list['jquery-1.12.4.min']);
			$this->addJS($this->context->js_list['utf8tobig5.min']);
			$this->addJS('/' . MEDIA_URL . '/jquery-ui-1.12.0/jquery-ui.min.js');
			$this->addJS('/' . MEDIA_URL . '/jquery.tablednd/jquery.tablednd.1.0.3.min.js');
			$this->addJS('/' . MEDIA_URL . '/bootstrap-3.3.7/js/bootstrap.min.js');
			$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/jquery.validate.min.js');
			$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/additional-methods.min.js');
			$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/localization/messages_zh_TW.js');
			$this->addJS('/' . MEDIA_URL . '/jquery.growl-1.3.2/jquery.growl.js');
			$this->addJS($this->context->js_list['url.min']);
			$this->addJS($this->context->js_list['model']);

            //新家
						$this->addCSS('/css/brands.css');
            $this->addCSS('/css/fontawesome.css');
            $this->addCSS('/css/all.css');
            $this->addCSS('/css/slick.css');
            $this->addCSS('/css/slick-theme.css');
            $this->addJS('/js/slick.min.js');
            $this->addJS('/js/solid.min.js');
            $this->addJS('/js/regular.min.js');
						$this->addJS('/js/all.js');
			//todo
			$this->addCSS('/modules/FloatShare/css/float_share.css?'.date('l jS \of F Y h:i:s A'));
			$this->addJS('/modules/FloatShare/js/float_share.js?'.date('l jS \of F Y h:i:s A'));
			//彈出訊息
			$this->addCSS('/' . MEDIA_URL . '/sweetalert2/css/sweetalert2.min.css');
			$this->addJS('/' . MEDIA_URL . '/sweetalert2/js/sweetalert2.all.min.js');

			$this->addJS('/' . MEDIA_URL . '/jquery.lazy-master/jquery.lazy.min.js');
			$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/js/bootstrap-select.min.js');
			$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/js/i18n/defaults-zh_TW.min.js');
			$this->addJS('/' . MEDIA_URL . '/tinymce-5.1.1/tinymce.min.js');
			//if(is_file(THEME_DIR.'js'.DS.'main.js')) $this->addJS(THEME_URL.'/js/main.js');

			/* todo 改到樣板裡去 Start */
			//todo 快取分割手機跟電腦版
			if (!is_file(CACHE_DIR . DS . 'front_css_index.php')) {
				$arr_file = [];

				//手機字體	有些字體在手機(IOS會閃爍)
				if (check_mobile()) {
					if (is_file(THEME_DIR . 'css' . DS . 'mobile_font.min.css')) {
						$this->addCSS(THEME_URL . '/css/mobile_font.min.css');
						$arr_file['mobile_font.min'] = THEME_URL . '/css/mobile_font.min.css';
					} elseif (is_file(THEME_DIR . 'css' . DS . 'mobile_font.css')) {
						$this->addCSS(THEME_URL . '/css/mobile_font.css');
						$arr_file['mobile_font'] = THEME_URL . '/css/mobile_font.css';
					}
				} else {
					if (is_file(THEME_DIR . 'css' . DS . 'not_mobile_font.min.css')) {
						$this->addCSS(THEME_URL . '/css/not_mobile_font.min.css');
						$arr_file['not_mobile_font.min'] = THEME_URL . '/css/not_mobile_font.min.css';
					} elseif (is_file(THEME_DIR . 'css' . DS . 'not_mobile_font.css')) {
						$this->addCSS(THEME_URL . '/css/not_mobile_font.css');
						$arr_file['not_mobile_font'] = THEME_URL . '/css/not_mobile_font.css';
					}
				}

				if (is_file(THEME_DIR . 'css' . DS . 'main.min.css')) {
					$this->addCSS(THEME_URL . '/css/main.min.css');
					$arr_file['main.min'] = THEME_URL . '/css/main.min.css';
				} elseif (is_file(THEME_DIR . 'css' . DS . 'main.css')) {
					$this->addCSS(THEME_URL . '/css/main.css');
					$arr_file['main'] = THEME_URL . '/css/main.css';
				}

				//手機樣式
				if (check_mobile()) {
					if (is_file(THEME_DIR . 'css' . DS . 'mobile.min.css')) {
						$this->addCSS(THEME_URL . '/css/mobile.min.css');
						$arr_file['mobile.min'] = THEME_URL . '/css/mobile.min.css';
					} elseif (is_file(THEME_DIR . 'css' . DS . 'mobile.css')) {
						$this->addCSS(THEME_URL . '/css/mobile.css');
						$arr_file['mobile'] = THEME_URL . '/css/mobile.css';
					}
				} else {
					if (is_file(THEME_DIR . 'css' . DS . 'not_mobile.min.css')) {
						$this->addCSS(THEME_URL . '/css/not_mobile.min.css');
						$arr_file['not_mobile.min'] = THEME_URL . '/css/not_mobile.min.css';
					} elseif (is_file(THEME_DIR . 'css' . DS . 'not_mobile.css')) {
						$this->addCSS(THEME_URL . '/css/not_mobile.css');
						$arr_file['not_mobile'] = THEME_URL . '/css/not_mobile.css';
					}
				}

				if (is_file(THEME_DIR . 'css' . DS . 'main_rwd.min.css')) {
					$this->addCSS(THEME_URL . '/css/main_rwd.min.css');
					$arr_file['main_rwd.min'] = THEME_URL . '/main_rwd/main.min.css';
				} elseif (is_file(THEME_DIR . 'css' . DS . 'main_rwd.css')) {
					$this->addCSS(THEME_URL . '/css/main_rwd.css');
					$arr_file['main_rwd'] = THEME_URL . '/css/main_rwd.css';
				}
				if (is_file(THEME_DIR . 'css' . DS . 'main_animate.min.css')) {
					$this->addCSS(THEME_URL . '/css/main_animate.min.css');
					$arr_file['main_animate.min'] = THEME_URL . '/css/main_animate.min.css';
				} elseif (is_file(THEME_DIR . 'css' . DS . 'main_animate.css')) {
					$this->addCSS(THEME_URL . '/css/main_animate.css');
					$arr_file['main_animate'] = THEME_URL . '/css/main_animate.css';
				}
				set_cache_index($arr_file, 'front_css_index');
			} else {
				$_insex = get_cache_index('front_css_index');
				foreach ($_insex as $i => $v) {
					$this->addCSS($v);
				}
			}

			if (!is_file(CACHE_DIR . DS . 'front_js_index.php')) {
				if (is_file(THEME_DIR . 'js' . DS . 'main.min.js')) {
					$this->addJS(THEME_URL . '/js/main.min.js');
				} elseif (is_file(THEME_DIR . 'js' . DS . 'main.js')) {
					$this->addJS(THEME_URL . '/js/main.js');
				}
				//set_cache_index('front_js_index');
			} else {
				$_insex = get_cache_index('front_js_index');
				foreach ($_insex as $i => $v) {
					$this->addJS($v);
				}
			}


			if (is_file(THEME_DIR . 'css' . DS . strtolower($this->controller_name) . '.min.css')) {
				$this->addCSS(THEME_URL . '/css/' . strtolower($this->controller_name) . '.min.css');
			} elseif (is_file(THEME_DIR . 'css' . DS . strtolower($this->controller_name) . '.css')) {
				$this->addCSS(THEME_URL . '/css/' . strtolower($this->controller_name) . '.css');
			}

			if (is_file(THEME_DIR . 'js' . DS . strtolower($this->controller_name) . '.min.js')) {
				$this->addJS(THEME_URL . '/js/' . strtolower($this->controller_name) . '.min.js');
			} elseif (is_file(THEME_DIR . 'js' . DS . strtolower($this->controller_name) . '.js')) {
				$this->addJS(THEME_URL . '/js/' . strtolower($this->controller_name) . '.js');
			}
		}

		Hook::exec('hookFront_setMedia');
		//todo
		//AdminController 會使用前台參數，所以不使用smarty
		if ($this->controller_name != 'Front') {
			$this->context->smarty->assign('css_files', array_unique($this->css_files));
			$this->context->smarty->assign('js_files', array_unique($this->js_files));

			$this->context->smarty->assign('css_header_files', array_unique($this->css_header_files));
			$this->context->smarty->assign('js_header_files', array_unique($this->js_header_files));

			$this->context->smarty->assign('css_footer_files', array_unique($this->css_footer_files));
			$this->context->smarty->assign('js_footer_files', array_unique($this->js_footer_files));

			$this->context->smarty->assign('GoogleAnalytics', Configuration::get('GoogleAnalytics'));
		}
	}

	//header初始設訂
	public function initHeader()
	{
		header('Content-Type:text/html; charset=utf-8');
		header('X-Powered-By: ' . POWERED_BY);
		$this->context->smarty->assign([
			'time' => time(),
		]);
	}


	//出使化 功能按鈕
	public function initToolbar()
	{
		$url = '';
		foreach ($_GET as $k => $v) {
			$url .= '&' . $k . '=' . $v;
		}

		if (empty($this->back_url))
			$this->back_url = self::$currentIndex;

		switch ($this->display) {
			case 'tabAccess':
				$this->toolbar_btn['cancel'] = [
					'href' => self::$currentIndex . 'view',
					'desc' => $this->l('取消'),
				];
			case 'add':        //新增
				$this->toolbar_btn['save'] = [
					'href' => '#',
					'desc' => $this->l('儲存'),
				];
			break;
			case 'edit':        //修改
				if (in_array('edit', $this->post_processing)) {
					$this->toolbar_btn['back'] = [
						'href' => self::$currentIndex . 'view' . $this->table . '&' . $this->index . '=' . Tools::getValue($this->index),
						//回上一頁
						//						'href' => 'javascript:history.back();',
						'desc' => $this->l('返回'),
					];
				}
				$this->toolbar_btn['save'] = [
					'href' => '#',
					'desc' => $this->l('儲存'),
				];
			break;
			case 'view':        //檢視
				if ($this->tabAccess['add'] && in_array('add', $this->post_processing)) {
					$this->toolbar_btn['new'] = [
						'href' => self::$currentIndex . '&add' . $this->table,
						'desc' => $this->l('新增'),
					];
				}

				if ($this->tabAccess['edit'] && in_array('edit', $this->post_processing)) {
					$this->toolbar_btn['edit'] = [
						'href' => self::$currentIndex . 'edit' . $this->table . '&' . $this->index . '=' . Tools::getValue($this->index),
						'desc' => $this->l('修改'),
					];
				}

				if ($this->tabAccess['del'] && in_array('del', $this->post_processing)) {
					$this->toolbar_btn['del'] = [
						'href' => self::$currentIndex . '&del' . $this->table . '&' . $this->index . '=' . Tools::getValue($this->index),
						'desc' => $this->l('刪除'),
					];
				}

				$this->toolbar_btn['back'] = [
					'href' => $this->back_url,
					//回上一頁
					//					'href' => 'javascript:history.back();',
					'desc' => $this->l('返回'),
				];
//

			break;
			case 'options':        //選項
				$this->toolbar_btn['save'] = [
					'href' => '#',
					'desc' => $this->l('儲存'),
				];
				if ($this->tabAccess['view']) {
					$typs = '';
					if (isset($this->fields['excel']['list']) && (count($this->fields['excel']['list']) == 0 || !is_array($this->fields['excel']['list']))) {
						if ((count($this->fields['list']) > 0) && !isset($this->fields['excel']['assign'])) {
							$this->fields['excel']['list'] = $this->fields['list'];
							$typs                          = 'list';
						}
					} elseif (isset($this->fields['excel']['assign']) && (count($this->fields['excel']['assign']) == 0 || !is_array($this->fields['excel']['assign']))) {
//							$this->fields['excel']['list'] = $this->fields['form'];       //有待修改成 excel_coordinate
						foreach ($this->fields['form'] as $key => $v) {

						}
					}
					if (count($this->fields['excel']['list'])) {
						$typs = 'list';
					}
					if ((isset($this->fields['excel']['list']) || isset($this->fields['excel']['assign'])))
						$this->toolbar_btn['excel'] = [
							'href' => self::$currentIndex . '&excel' . $typs . $this->table . $url,
							'desc' => $this->l('匯出Excel'),
						];
				}
			break;
			case 'excellist':        //Exec列表
				$this->toolbar_btn['excel']  = [
					'href' => self::$currentIndex . '&excel' . $this->table,
					'desc' => $this->l('匯出'),
				];
				$this->toolbar_btn['filter'] = [
					'href' => '#',
					'desc' => $this->l('篩選'),
				];
				$this->toolbar_btn['back']   = [
					'href' => self::$currentIndex,
					'desc' => $this->l('返回'),
				];
			break;
			default: // list
				if ($this->tabAccess['add']) {
					$this->toolbar_btn['new'] = [
						'href' => self::$currentIndex . '&add' . $this->table,
						'desc' => $this->l('新增'),
					];
					if (count($this->fields['excel']['import']['from'])) {
						$this->toolbar_btn['excelimport'] = [
							'href' => self::$currentIndex . '&excelimport' . $this->table,
							'desc' => $this->l('匯入Excel'),
						];
					}
				}

				if ($this->display != 'calendar') {
					foreach ($this->fields['list'] as $i => $v) {
						if ($v['filter']) {
							$this->toolbar_btn['media_search'] = [
								'href' => '#',
								'desc' => $this->l('搜尋'),
							];
							break;
						}
					}
				}
				if ($this->tabAccess['view']) {
					//月曆顯示
					if (isset($this->fields['calendar'])) {
						$this->toolbar_btn['calendar'] = [
							'href' => self::$currentIndex . '&calendar' . $this->table,
							'desc' => $this->l('圖表顯示'),
						];
					}

					//表單顯示
					if (isset($this->fields['calendar']) || false) {
						$this->toolbar_btn['list'] = [
							'href' => str_replace('&calendar' . $this->table, '', self::$currentIndex),
							'desc' => $this->l('表單顯示'),
						];
					}

					$typs = '';
					if (isset($this->fields['excel']['list']) && (count($this->fields['excel']['list']) == 0 || !is_array($this->fields['excel']['list']))) {
						if ((count($this->fields['list']) > 0) && !isset($this->fields['excel']['assign'])) {
							$this->fields['excel']['list'] = $this->fields['list'];
							$typs                          = 'list';
						}
					} elseif (isset($this->fields['excel']['assign']) && (count($this->fields['excel']['assign']) == 0 || !is_array($this->fields['excel']['assign']))) {
//							$this->fields['excel']['list'] = $this->fields['form'];       //有待修改成 excel_coordinate
						foreach ($this->fields['form'] as $key => $v) {

						}
					}
					if (count($this->fields['excel']['list'])) {
						$typs = 'list';
						$url  = '';
					}
					if ((isset($this->fields['excel']['list']) || isset($this->fields['excel']['assign'])))
						$this->toolbar_btn['excel'] = [
							'href' => self::$currentIndex . '&excel' . $typs . $this->table . $url,
							'desc' => $this->l('匯出Excel'),
						];
				}
			break;
		}

		$this->context->smarty->assign(
			[
				'top_bar_before_htm' => $this->top_bar_before_htm,
				'top_bar_after_htm'  => $this->top_bar_after_htm,
			]
		);
	}


	//內容初始設定
	public function initContent()
	{
		$this->getLanguages();

		$this->initToolbar();

		$this->initPageHeaderToolbar();

		$this->process();

		if ($this->display == 'options') {
			$this->display_media_bak = false;
			if ($this->tabAccess['view'])
				$this->display = 'view';
			if ($this->tabAccess['edit'])
				$this->display = 'edit';
		}

		if (empty($this->no_FormTable)) {
			if ($this->display == 'edit' || $this->display == 'add') {
				$this->content .= $this->renderForm();    //啟用表單
			} elseif ($this->display == 'view') {            //檢視
				$this->content .= $this->renderView();
			} elseif ($this->display == 'details') {        //細節
				$this->content .= $this->renderDetails();
			} elseif ($this->display == 'calendar') {        //排程
				$this->content .= $this->renderCalendar();
			} elseif (!$this->ajax) {
				$this->content .= $this->renderList();    //表單列表
			} elseif ($this->ajax) {
				$this->content .= $this->renderList();    //表單列表
			};
		}

		$theme_color                           = Configuration::get('theme-color');
		$msapplication_navbutton_color         = Configuration::get('msapplication-navbutton-color');
		$apple_mobile_web_app_status_bar_style = Configuration::get('apple-mobile-web-app-status-bar-style');

		$this->context->smarty->assign([
			't_color'                     => $theme_color,
			'mn_color'                    => $msapplication_navbutton_color,
			'amwasb_style'                => $apple_mobile_web_app_status_bar_style,
			'title'                       => $this->fields['title'],
			'page_header_toolbar_title'   => $this->page_header_toolbar_title,
			'page_header_toolbar_img'     => $this->page_header_toolbar_img,
			'toolbar_btn'                 => $this->toolbar_btn,
			'display_page_header_toolbar' => $this->display_page_header_toolbar,
		]);

		$breadcrumbs_htm = '';

		if (Dispatcher::getContext()->is_media && is_file(THEME_DIR . DS . 'mobile' . DS . 'breadcrumbs.tpl')) {
			$breadcrumbs_htm = $this->context->smarty->fetch(THEME_DIR . DS . 'mobile' . DS . 'breadcrumbs.tpl');
		} elseif (Dispatcher::getContext()->is_app && is_file(THEME_DIR . DS . 'mobile' . DS . 'breadcrumbs.tpl')) {
			$breadcrumbs_htm = $this->context->smarty->fetch(THEME_DIR . DS . 'mobile' . DS . 'breadcrumbs.tpl');
		} elseif (is_file(THEME_DIR . DS . 'breadcrumbs.tpl')) {
			$breadcrumbs_htm = $this->context->smarty->fetch(THEME_DIR . DS . 'breadcrumbs.tpl');
		}

		$this->context->smarty->assign([
			'breadcrumbs_htm' => $breadcrumbs_htm,
		]);

		if (!$this->useMobileTheme()) {            //PS樣版
			$this->context->smarty->assign([
				'HOOK_HEADER'          => '',
				'HOOK_TOP'             => '',
				'HOOK_LEFT_COLUMN'     => '',
				'HOOK_CENTERAL_COLUMN' => '',
				'HOOK_RIGHT_COLUMN'    => '',
			]);
		} else {                            //手機樣版
			$this->context->smarty->assign([
				'HOOK_MOBILE_HEADER' => '',
			]);
		}


		if (Dispatcher::getContext()->is_media && is_file(OVERRIDE_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl')) {
			$this->content .= $this->context->smarty->fetch(OVERRIDE_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl');
		} elseif (Dispatcher::getContext()->is_app && is_file(OVERRIDE_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl')) {
			$this->content .= $this->context->smarty->fetch(OVERRIDE_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl');
		} elseif (Dispatcher::getContext()->is_media && is_file(THEME_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl')) {
			$this->content .= $this->context->smarty->fetch(THEME_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl');
		} elseif (Dispatcher::getContext()->is_app && is_file(THEME_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl')) {
			$this->content .= $this->context->smarty->fetch(THEME_DIR . 'mobile' . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl');
		} elseif (is_file(THEME_DIR . 'controllers' . DS . $this->controller_name . DS . 'content.tpl')
		|| is_file(OVERRIDE_THEME_DIR . DS . 'controllers' . DS . $this->controller_name . DS . 'content.tpl')) {
			$this->content .= $this->context->smarty->fetch('content.tpl');
		}

		$this->context->smarty->assign([
			'back_url' => $this->back_url,
			'_msg'     => implode('<br>', $this->_msgs),
			'_error'   => implode('<br>', $this->_errors),
			'content'  => $this->content,
		]);
	}

	//顯示維修中頁面
	protected function displayMaintenancePage()
	{
		if ($this->maintenance == true || !(int)Config::get('WEB_ENABLE')) {    //維修中
			$this->maintenance = true;
			if (!in_array(Tools::getRemoteAddr(), explode(',', Configuration::get('PS_MAINTENANCE_IP')))) {
				header('HTTP/1.1 503 temporarily overloaded');
				$this->context->smarty->assign([
					'HOOK_MAINTENANCE' => '',
				]);
				$front_controller = preg_match('/ModuleFrontController$/', get_class($this)) ? new FrontController() : $this;
				$this->smartyOutputContent($front_controller->getTemplatePath($this->getThemeDir() . 'maintenance.tpl'));
				exit;
			};
		};
	}

	public function getTemplatePath($template)
	{
		if (!$this->useMobileTheme())
			return $template;

		$tpl_file = basename($template);
		$dirname  = dirname($template) . (substr(dirname($template), -1, 1) == DS ? '' : DS);
		if ($dirname == THEME_DIR) {                //movile資料夾有檔案
			if (file_exists(THEME_MOBILE_DIR . $tpl_file))
				$template = THEME_MOBILE_DIR . $tpl_file;
		} elseif ($dirname == THEME_MOBILE_DIR) {        //movile資料夾沒有檔案 但theme有
			if (!file_exists(THEME_MOBILE_DIR . $tpl_file) && file_exists(THEME_DIR . $tpl_file))
				$template = THEME_DIR . $tpl_file;
		};

		return $template;
	}

	//初始化頁面
	public function initCentralPage()
	{
		$this->displayMaintenancePage();
	}

	//POST處理
	public function postProcess()
	{                //POST處理
		$action = Tools::getValue('action');
		if ($this->ajax) {
// no need to use displayConf() here
			$this->action = $action;
			if (!empty($this->action) && method_exists($this, 'ajaxProcess' . $this->action)) {
//Hook::exec('actionAdmin'.ucfirst($this->action).'Before', array('controller' => $this));			//所有Admin 前動作
//Hook::exec('action'.get_class($this).ucfirst($this->action).'Before', array('controller' => $this));	//指定Class 後動作

				$return = $this->{'ajaxProcess' . $this->action}();

//Hook::exec('actionAdmin'.ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));			//所有Admin 前動作
//Hook::exec('action'.get_class($this).ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));	//指定Classn 後動作
				return $return;
			} elseif (!empty($this->action) && $this->controller_name == 'AdminModules' && Tools::getIsset('configure')) {
				$module_obj = Module::getContextByName(Tools::getValue('configure'));
				if (Validate::isLoadedObject($module_obj) && method_exists($module_obj, 'ajaxProcess' . $this->action)) {
					return $module_obj->{'ajaxProcess' . $this->action}();
				};
			} elseif (method_exists($this, 'ajaxProcess')) {
				return $this->ajaxProcess();
			};
		} else {
// If the method named after the action exists, call "before" hooks, then call action method, then call "after" hooks
			if (!empty($this->action) && method_exists($this, 'process' . ucfirst($this->action))) {
// Hook before action
//Hook::exec('actionAdmin'.ucfirst($this->action).'Before', array('controller' => $this));
//Hook::exec('action'.get_class($this).ucfirst($this->action).'Before', array('controller' => $this));
// Call process
				$return = $this->{'process' . ucfirst($this->action)}();
// Hook After Action
//Hook::exec('actionAdmin'.ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));
//Hook::exec('action'.get_class($this).ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));
				return $return;
			};
		};
	}

	//處理
	public function process()
	{

	}

	public function initFooter()
	{
		$this->context->smarty->assign([
			'HOOK_FOOTER' => '',
		]);

		/* //判斷語言 覆蓋相對應css
		if($this->context->language->is_rtl) {
		    $this->addCSS(_THEME_CSS_DIR_.'rtl.css');
		    $this->addCSS(_THEME_CSS_DIR_.$this->context->language->iso_code.'.css');
		}
		*/
	}

	public function redirect()
	{

	}

	public function validateRules()
	{    //驗證
		if (method_exists($this, 'getValidationRules') && empty($this->definition)) {
			$this->definition = $this->getValidationRules();
		} else {
			$this->definition;
		};
		$Validate             = new Validate();
		$Validate->definition = $this->definition;
		$Validate->table      = $this->table;
		$Validate->index      = $this->index;
		$Validate->validateRules();
		$this->_errors        = array_merge($this->_errors, $Validate->_errors);
		$this->_errors_name   = array_merge($this->_errors_name, $Validate->_errors_name);
		$this->_errors_name_i = array_merge($this->_errors_name_i, $Validate->_errors_name_i);
	}

	public function initProcess()
	{
		$post_code = Tools::getValue('post_code');
		if (Tools::isSubmit('submitAdd' . $this->table) || Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {                                          //送出新增(執行)
			if ($this->tabAccess['add']) {
				$this->action  = 'Save';
				$this->display = 'add';
			} else {
				//$check=0;
				//if(Tools::isSubmit('submitAdd' . $this->table) ){$check=1;}
				//if(Tools::isSubmit('submitAdd' . $this->table . 'AndStay') ){$check=2;}
				//if($this->tabAccess['add']){$check=9;}

				//$this->_errors[] = $this->l('您沒有新增權限'.$check);
				$this->_errors[] = $this->l('您沒有新增權限');

				//$var = "$this->tabAccess['add']";
				//$this->_errors[] = $this->l($var);
			};
		} elseif ((Tools::isSubmit('submitEdit' . $this->table) || Tools::isSubmit('submitEdit' . $this->table . 'AndStay')) && (Tools::getValue($this->index) || empty($this->table))) {      //送出修改(執行)
			if ($this->tabAccess['edit']) {
				$this->action  = 'Save';
				$this->display = 'edit';
			} else {
				$this->_errors[] = $this->l('您沒有修改權限');
			};
		} elseif ((Tools::isSubmit('submitDel' . $this->table) || Tools::isSubmit('del' . $this->table)) && (Tools::getValue($this->index) || empty($this->table))) {      //送出刪除(執行)
			if ($this->tabAccess['del']) {
				$this->action  = 'del';
				$this->display = 'del';
			} else {
				$this->_errors[] = $this->l('您沒有刪除權限');
			};
		} elseif (Tools::isSubmit('add' . $this->table)) {                                                //新增頁面
			if ($this->tabAccess['add']) {
				$this->display = 'add';
			} else {
				$this->_errors[] = $this->l('您沒有新增權限');
			};
		} elseif (Tools::isSubmit('edit' . $this->table) && Tools::getValue($this->index)) {            //修改頁面
			if ($this->tabAccess['edit']) {
				$this->display = 'edit';
			} else {
				$this->_errors[] = $this->l('您沒有修改權限');
			};
		} elseif (Tools::isSubmit('view' . $this->table) && Tools::getValue($this->index)) {            //送出檢視(執行)
			if ($this->tabAccess['view']) {
				$this->display = 'view';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			};
		} elseif (Tools::isSubmit('list' . $this->table) && Tools::getValue($this->index)) {             //送出列表(執行)
			if ($this->tabAccess['view']) {
				$this->action  = 'list';
				$this->display = 'list';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			};
		} elseif (Tools::isSubmit('calendar' . $this->table)) {                                        //送出行事曆(執行)
			if ($this->tabAccess['view']) {
				$this->action  = 'calendar';
				$this->display = 'calendar';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			};
		} else {
			switch ($this->display) {
				case 'excelimport':
				case 'add':
					if (!$this->tabAccess['add'])
						$this->_errors[] = $this->l('您沒有新增權限');
				break;
				case 'edit':
					if (!$this->tabAccess['edit'])
						$this->_errors[] = $this->l('您沒有修改權限');
				break;
				case 'del':
					if (!$this->tabAccess['del'])
						$this->_errors[] = $this->l('您沒有刪除權限');
				break;
				case 'view':
				case 'excel':
				case 'excellist':
					if (!$this->tabAccess['view'])
						$this->_errors[] = $this->l('您沒有檢視權限');
				break;
			}
		}

		if (($_SESSION['post_code'] != $post_code) || empty($_SESSION['post_code'])) {
			if (!empty($post_code))
				$_SESSION['post_code'] = $post_code;
		} else {
			$this->action = null;
		}

		$role = 'page'; //JQuery Mobile 用
		if (Tools::isSubmit('dialog')) {
			$role = 'dialog';
		}

		$this->context->smarty->assign([
			'role'    => $role,
			'main_menu'  => $this->main_menu,
			'display' => $this->display,
		]);
	}

	//預設內容
	public function displayContent()
	{
		$layout   = $this->getLayout();
		$template = $this->context->smarty->fetch('default.tpl');
		$this->context->smarty->assign('template', $template);
		$this->context->smarty->assign([
			'title'                     => WEB_NAME,
			'page_header_toolbar_title' => WEB_NAME,
		]);
		return $this->smartyOutputContent($layout);
	}

	public function setFormTable()
	{
		if (empty($this->FormTable))
			$this->FormTable = new FormTable();
		$this->FormTable->back_url         = self::$currentIndex;
		$this->FormTable->table            = $this->table;
		$this->FormTable->_join            = $this->_join;
		$this->FormTable->_group           = $this->_group;
		$this->FormTable->_as              = $this->_as;
		$this->FormTable->list_id          = $this->list_id;
		$this->FormTable->actions          = $this->actions;
		$this->FormTable->tabAccess        = $this->tabAccess;
		$this->FormTable->fields           = $this->fields;
		$this->FormTable->display          = $this->display;
		$this->FormTable->submit_action    = $this->submit_action;
		$this->FormTable->bulk_actions     = $this->bulk_actions;
		$this->FormTable->display_edit_div = $this->display_edit_div;
	}

	public function renderForm($setFormType = true)
	{
		if ($setFormType) {
			$this->setFormTable();
		}
		$this->FormTable->renderForm();

		$Context = Context::getContext();
		if (count($this->fields['tabs']) == 1) {
			unset($this->fields['tabs']);
		}

		$this->_errors                     = array_merge($this->_errors, $this->FormTable->_errors);
		$this->display_page_header_toolbar = $this->FormTable->display_page_header_toolbar;

		if (count($this->fields['tabs']) == 1) {
			unset($this->fields['tabs']);
		}

		$Context->smarty->assign([
			'back_url'                    => $this->back_url,
			'fields'                      => $this->FormTable->fields,
			'submit_display'              => ucfirst($this->FormTable->display),
			'submit_action'               => $this->FormTable->submit_action,
			'show_cancel_button'          => $this->show_cancel_button,
			'display_page_header_toolbar' => $this->FormTable->display_page_header_toolbar,
			'arr_take_photo_data'         => $this->FormTable->arr_take_photo_data,
			'arr_signature_data'          => $this->FormTable->arr_signature_data,
		]);

		if (count($this->FormTable->_errors) == 0) {
//			$mobile_file = THEME_DIR . 'mobile' . DS . 'form' . DS . 'form_form.tpl';
//			if(is_file($mobile_file)){
//				if(Dispatcher::getContext()->is_media){
//					return $Context->smarty->fetch('mobile' . DS . 'form' . DS . 'form_form.tpl');
//				}
//				if(Dispatcher::getContext()->is_app){
//					return $Context->smarty->fetch('mobile' . DS . 'form' . DS . 'form_form.tpl');
//				}
//			}
			return $Context->smarty->fetch('form' . DS . 'form_form.tpl');
		}
		return '';
	}

	public function renderView()
	{
		$this->setFormTable();
		$this->FormTable->ini();
		foreach ($this->fields['form'] as $key2 => $v2) {
			if ($key2 != 'class') {
				unset($this->fields['form'][$key2]['submit']);
				unset($this->fields['form'][$key2]['cancel']);
				unset($this->fields['form'][$key2]['reset']);
				if (isset($this->fields['form'][$key2]['input'])) {
					foreach ($this->fields['form'][$key2]['input'] as $key => $v) {
						if ($v['type'] == 'checkbox')
							$this->fields['form'][$key2]['input'][$key]['multiple'] = true;

						if ($v['type'] != 'hidden' && $v['type'] != 'tpl' && $v['type'] != 'hr' && $v['type'] != 'color' && $v['type'] != 'confirm-password' && $v['type'] != 'change-password' && $v['type'] != 'html' && $v['type'] != 'file') {
							if ($v['type'] == 'take_photo' || $v['type'] == 'signature') {
								$this->fields['form'][$key2]['input'][$key]['key']['type'] = 'view';
							} else {
								$this->fields['form'][$key2]['input'][$key]['type'] = 'view';
							}
							//'date' || $input.type == 'datetime' || $input.type == 'datetime-local
						} else {
							$this->fields['form'][$key2]['input'][$key]['disabled'] = true;
						}
					}
				}
			}
		}
		return $this->renderForm();
	}

	public function renderCalendar()
	{
		if (!($this->fields['calendar'] && is_array($this->fields['calendar'])))
			return false; //有宣告fields['calendar'] 排程顯示
		if (!is_array($this->bulk_actions)) {
			$this->bulk_actions = [];    //批量操作
		};
		unset($this->fields['form']);        //移除表單格式

		$this->setFormTable();
		$html = '';
		if (isset($this->fields['calendar']['before_form']) && !empty($this->fields['calendar']['before_form'])) {
			$this->FormTable->fields['form'] = $this->fields['calendar']['before_form'];
			$html                            .= $this->renderForm(false);
		};

		$this->FormTable->renderCalendar();
		$html .= Context::getContext()->smarty->fetch('form' . DS . 'form_calendar.tpl');

		if (isset($this->fields['calendar']['after_form']) && !empty($this->fields['calendar']['after_form'])) {
			$this->FormTable->fields['form'] = $this->fields['calendar']['after_form'];
			$html                            .= $this->renderForm(false);
		};
		return $html;
	}

	public function renderList()
	{
//		if (!($this->fields['list'] && is_array($this->fields['list']))) return false; //有宣告fields['list'] 代表是列表
		//判別列表內有無 active 啟用
		if (!is_array($this->bulk_actions)) {
			$this->bulk_actions = [];    //批量操作
		};
		//取出標題
		if (!empty($this->fields['parent_key']) && !empty($this->fields['parent_key'])) {
			if (Tools::getValue($this->fields['index'])) {
				$this->fields['where'] .= ' AND `' . $this->fields['parent_key'] . '` = ' . Tools::getValue($this->fields['index']);
				$sql                   = sprintf('SELECT `' . $this->fields['title_key'] . '` FROM `' . $this->table . '` WHERE `' . $this->fields['index'] . '` = %d LIMIT 0, 1',
					GetSQL(Tools::getValue($this->fields['index']), 'int'));
				$row                   = Db::rowSQL($sql, true);
				$this->fields['title'] = $row[$this->fields['title_key']];
			} else {
				$this->fields['where'] .= ' AND (`' . $this->fields['parent_key'] . '` IS NULL OR `' . $this->fields['parent_key'] . '` = "")';
			};
		};
		if (isset($this->fields['list']) && is_array($this->fields['list']) && array_key_exists('active', $this->fields['list']) && !empty($this->fields['list']['active'])) {

			//列表點擊立即啟用/關閉
			/*
			$this->bulk_actions = array_merge(array(
				'divider' => array(
					'text' => '批次啟用關閉'
				),
				'enableSelection' => array(
					'text' => $this->l('啟用'),
					'icon' => 'icon-power-off text-success'
				),
				'disableSelection' => array(
					'text' => $this->l('關閉'),
					'icon' => 'icon-power-off text-danger'
				)
			), $this->bulk_actions);
			*/

		};
		$this->setFormTable();
		$this->FormTable->renderList();
		$Context = Context::getContext();
		$Context->smarty->assign([
				'fields'           => $this->FormTable->fields,
				'has_actions'      => !empty($this->actions),
				'actions'          => $this->actions,
				'post_processing'  => $this->post_processing,
				'display_edit_div' => $this->display_edit_div,
				'bulk_actions'     => $this->bulk_actions,
				'no_information'   => $this->no_information,
			]
		);
		if (count($this->FormTable->_errors) == 0) {
//			$mobile_file = THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list.tpl';
//			if(is_file($mobile_file)){
//				if(Dispatcher::getContext()->is_media){			//手機版
//					$Context->smarty->assign(array(
//						'form_list_field_list' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_field_list.tpl'),
//						'form_list_thead' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_thead.tpl'),
//						'form_list_tbody' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_tbody.tpl'),
//						'form_list_tfoot' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_tfoot.tpl'),
//						'form_list_pagination' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_pagination.tpl')
//					));
//					return $Context->smarty->fetch($mobile_file);
//				}
//				if(Dispatcher::getContext()->is_app){			//APP
//					$Context->smarty->assign(array(
//						'form_list_field_list' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_field_list.tpl'),
//						'form_list_thead' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_thead.tpl'),
//						'form_list_tbody' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_tbody.tpl'),
//						'form_list_tfoot' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_tfoot.tpl'),
//						'form_list_pagination' => $Context->smarty->fetch(THEME_DIR.'mobile' . DS . 'form' . DS . 'form_list_pagination.tpl')
//					));
//					return $Context->smarty->fetch($mobile_file);
//				}
//			}

			$Context->smarty->assign([                //電腦版
													  'form_list_field_list' => $Context->smarty->fetch('form' . DS . 'form_list_field_list.tpl'),
													  'form_list_thead'      => $Context->smarty->fetch('form' . DS . 'form_list_thead.tpl'),
													  'form_list_tbody'      => $Context->smarty->fetch('form' . DS . 'form_list_tbody.tpl'),
													  'form_list_tfoot'      => $Context->smarty->fetch('form' . DS . 'form_list_tfoot.tpl'),
													  'form_list_pagination' => $Context->smarty->fetch('form' . DS . 'form_list_pagination.tpl'),
			]);

			return $Context->smarty->fetch('form' . DS . 'form_list.tpl');
		}
		return '';
	}

	public function display()
	{
		$header_tpl = '';
		$footer_tpl = '';
		if (is_file(THEME_DIR . 'header.tpl'))
			$header_tpl = 'header.tpl';
		if (is_file(THEME_DIR . 'footer.tpl'))
			$footer_tpl = 'footer.tpl';

		if (Tools::getValue('conf')) {
			$this->_msgs[] = $this->_conf[Tools::getValue('conf')];
		}
//		die(Tools::getValue('conf2'));
		if (Tools::getValue('conf2')) {
			$this->_errors[] = $this->_conf2[Tools::getValue('conf2')];
		}
		if (!$this->meta_title) {
			$this->meta_title = $this->toolbar_title;
		};
		if (is_array($this->meta_title)) {    //標題是陣列
			$this->meta_title = strip_tags(implode(NAVIGATION_PIPE, $this->meta_title));
		};
		/*
		 * 資料夾優先權要改
		 */
		$this->context->smarty->assign('meta_title', $this->meta_title);

		$this->context->smarty->assign([
			'display_media_bak' => $this->display_media_bak,
		]);


		$meta = Meta::getPageMetas($this);

		$this->context->smarty->assign([
			'WEB_NAME'                  => WEB_NAME,
			'nobots'                    => $meta['nobots'],
			'nofollow'                  => $meta['nofollow'],
			'meta_title'                => $meta['meta_title'],
			'meta_description'          => $meta['meta_description'],
			'meta_keywords'             => $meta['meta_keywords'],
			'meta_img'                  => $this->meta_img,
            'meta_icon'                  => $this->meta_icon,
			'css_files'                 => $this->css_files,
			'js_files'                  => $this->js_files,
			'errors'                    => $this->_errors,
			'_msg'                      => implode('<br>', array_unique($this->_msgs)),
			'_error'                    => implode('<br>', array_unique($this->_errors)),
			'_errors_name'              => $this->_errors_name,
			'_errors_name_i'            => $this->_errors_name_i,
			'header_tpl'                => $header_tpl,
			'footer_tpl'                => $footer_tpl,
			'display_main_menu'         => $this->display_main_menu,
			'display_header'            => $this->display_header,
			'display_footer'            => $this->display_footer,
			'display_header_javascript' => $this->display_header_javascript,
			'display_footer_javascript' => $this->display_footer_javascript,
		]);
		$layout = $this->getLayout();
		if ($layout) {    //以Hook
			if ($this->template) {    //自己指定 template
				$template = $this->context->smarty->fetch($this->template);
				$this->context->smarty->assign('template', $template);
				$this->smartyOutputContent($layout);
			} elseif ($layout && !empty($this->content)) {        //conternt/資料夾來分類 / content.tpl
				$this->smartyOutputContent($layout);
			} else {                //預設版型
				ob_start();
				$this->displayContent();
				$template = ob_get_contents();
				ob_clean();
				echo $template;
			};
		} else {        //以smarty樣版
			if ($this->display_header && is_file($this->display_header))
				$this->smartyOutputContent(THEME_DIR . 'header.tpl');
			if ($this->template)
				$this->smartyOutputContent($this->template);
			if ($this->content)
				echo $this->content;
			if ($this->display_footer && is_file($this->display_footer))
				$this->smartyOutputContent(THEME_DIR . 'footer.tpl');
		};
		return true;
	}

	//設訂樣版
	public function setTemplate($default_template = null)
	{
		if ($this->useMobileTheme()) {
			$this->setMobileTemplate($default_template);
		} else {
			$template = $this->getOverrideTemplate();
			if ($template) {    //有覆蓋
				parent::setTemplate($template);
			} else {        //沒有覆蓋
				parent::setTemplate($default_template);
			};
		};
	}

	public function processSave()
	{
		if (Tools::getValue($this->index) || empty($this->index)) {
			return $this->processEdit();
		} else {
			return $this->processAdd();
		};
	}

	public function processAdd()
	{
		$this->validateRules();
		if (count($this->_errors))
			return;
		$this->setFormTable();
		$num = $this->FormTable->insertDB();
		if ($num) {
			if (Tools::isSubmit('submitAdd' . $this->table)) {
				$this->back_url = self::$currentIndex . '&conf=1';
			};
			if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
				if (in_array('edit', $this->post_processing)) {
					$this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
				} else {
					$this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
				}
			};
			if (Tools::isSubmit('submitAdd' . $this->table . 'AndContinue')) {
				$this->back_url = self::$currentIndex . '&add' . $this->table . '&conf=1';
			};
			Tools::redirectLink($this->back_url);
		} else {
			$this->_errors[] = $this->l('沒有新增任何資料!');
		};
	}

	public function processDel()
	{
		if (count($this->_errors))
			return;
		$this->setFormTable();
		$num = $this->FormTable->deleteDB();
		if ($num && (Tools::isSubmit('submitDel' . $this->table) || Tools::isSubmit('del' . $this->table))) {
			$this->back_url = self::$currentIndex . '&conf=3';
		} elseif (Tools::isSubmit('submitDel' . $this->table) || Tools::isSubmit('del' . $this->table)) {
			$this->back_url = self::$currentIndex . '&conf2=1';
		} elseif ($num && Tools::isSubmit('submitDel' . $this->table . 'AndStay') || Tools::isSubmit('del' . $this->table)) {
			$this->back_url = self::$currentIndex . '&del' . $this->table . '&conf=3&' . $this->index . '=' . $this->FormTable->index_id;
		} elseif (Tools::isSubmit('submitDel' . $this->table . 'AndStay') || Tools::isSubmit('del' . $this->table)) {
			$this->back_url = self::$currentIndex . '&del' . $this->table . '&conf2=1&' . $this->index . '=' . $this->FormTable->index_id;
		};
		Tools::redirectLink($this->back_url);
	}

	public function processEdit()
	{
		$this->validateRules();
		if (count($this->_errors))
			return;
		$this->setFormTable();
		$num = $this->FormTable->updateDB();

		if (Tools::isSubmit('submitEdit' . $this->table)) {
			$this->back_url = self::$currentIndex . '&conf=2';
		};
		if (Tools::isSubmit('submitEdit' . $this->table . 'AndStay')) {
			if (in_array('edit', $this->post_processing)) {
				$this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=2&' . $this->index . '=' . $this->FormTable->index_id;
			} else {
				$this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=2&' . $this->index . '=' . $this->FormTable->index_id;
			}
		};
		Tools::redirectLink($this->back_url);
	}

	public function getOverrideTemplate()
	{
		$controller     = Dispatcher::getContext()->getController();
		$controller_obj = Context::getContext()->controller;
		$dir            = OVERRIDE_THEME_DIR;
		if (isset($controller_obj->module) && Validate::isLoadedObject($controller_obj->module)) {
			$controller = 'module-' . $controller_obj->module->name . '-' . $controller;
			$dir        = MODULE_DIR . $controller_obj->module->name . DS . 'themes' . DS . 'front' . DS . THEME;
		};
		$controller = strtolower(str_replace('Controller', '', $controller));
		$template   = $dir . $controller . '.tpl';
		if (is_file($template))
			return $template;
		return '';
	}

	//設訂手機樣版
	public function setMobileTemplate($template)
	{
		$template           = $this->getTemplatePath($template);
		$assign             = [];
		$assign['tpl_file'] = basename($template, '.tpl');
		if (isset($this->page))
			$assign['controller_name'] = $this->page;
		$this->context->smarty->assign($assign);
		$this->template = $template;
	}

	//是否有手機樣版
	protected function useMobileTheme()
	{
		static $use_mobile_template = null;
		//判斷是否開放手機版AND有手機樣版
		if ($use_mobile_template === null)
			$use_mobile_template = ($this->context->getMobileDevice() && file_exists(THEME_MOBILE_DIR . 'layout.tpl'));
		return $use_mobile_template;
	}

	//取出驗證項目
	public function getValidationRules()
	{
		if (empty($this->definition)) {
			$this->definition = [];
			$arr_fields_form  = [$this->fields['form'], $this->fields['calendar']['before_form'], $this->fields['calendar']['after_form']];
			foreach ($arr_fields_form as $ffi => $f_form) {
				foreach ($f_form as $key => $form) {
					if (isset($form['input'])) {
						foreach ($form['input'] as $i => $v) {            //表單驗證
							if ($v['type'] != 'view') {
								if ($v['required']) {
									$this->definition[] = [
										'validate' => 'required',
										'name'     => $v['name'],
										'val'      => true,
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}
								if (isset($v['minlength']) && !isset($v['maxlength'])) {
									if ($v['type'] == 'change-password') {
										$this->definition[] = ['validate' => 'minlength', 'name' => $v['name'], 'val' => $v['minlength'], 'label' => $this->l('新密碼'), 'type' => $v['type']];
										$this->definition[] = ['validate' => 'minlength', 'name' => $v['name'] . '2', 'val' => $v['minlength'], 'label' => $this->l('確認密碼'), 'type' => $v['type']];
									} else {
										$this->definition[] = ['validate' => 'minlength', 'name' => $v['name'], 'val' => $v['minlength'], 'label' => $v['label'], 'type' => $v['type']];
									};
								};
								if (isset($v['maxlength']) && !isset($v['minlength'])) {
									if ($v['type'] == 'change-password') {
										$this->definition[] = ['validate' => 'maxlength', 'name' => $v['name'], 'val' => $v['minlength'], 'label' => $this->l('新密碼'), 'type' => $v['type']];
										$this->definition[] = ['validate' => 'maxlength', 'name' => $v['name'] . '2', 'val' => $v['minlength'], 'label' => $this->l('確認密碼'), 'type' => $v['type']];
									} else {
										$this->definition[] = ['validate' => 'maxlength', 'name' => $v['name'], 'val' => $v['maxlength'], 'label' => $v['label'], 'type' => $v['type']];
									};
								};
								if ((isset($v['minlength']) && isset($v['maxlength'])) || $v['rangelength']) {
									if ($v['type'] == 'change-password') {
										$this->definition[] = [
											'validate' => 'rangelength',
											'name'     => $v['name'],
											'val'      => ($v['rangelength']) ? $v['rangelength'] : $v['minlength'] . '-' . $v['maxlength'],
											'label'    => $this->l('新密碼'),
											'type'     => $v['type'],
										];
										$this->definition[] = [
											'validate' => 'rangelength',
											'name'     => $v['name'] . '2',
											'val'      => ($v['rangelength']) ? $v['rangelength'] : $v['minlength'] . '-' . $v['maxlength'],
											'label'    => $this->l('確認密碼'),
											'type'     => $v['type'],
										];
									} else {
										$this->definition[] = [
											'validate' => 'rangelength',
											'name'     => $v['name'],
											'val'      => ($v['rangelength']) ? $v['rangelength'] : $v['minlength'] . '-' . $v['maxlength'],
											'label'    => $v['label'],
											'type'     => $v['type'],
										];
									};
								};
								if (isset($v['min']) && !isset($v['max']))
									$this->definition[] = ['validate' => 'min', 'name' => $v['name'], 'val' => $v['min'], 'label' => $v['label'], 'type' => $v['type']];
								if (isset($v['max']) && !isset($v['min']))
									$this->definition[] = ['validate' => 'max', 'name' => $v['name'], 'val' => $v['max'], 'label' => $v['label'], 'type' => $v['type']];
								if ((isset($v['min']) && isset($v['max'])) || $v['range']) {
									$this->definition[] = [
										'validate' => 'range',
										'name'     => $v['name'],
										'val'      => ($v['range']) ? $v['range'] : $v['min'] . '-' . $v['max'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								};
//								if(isset($v['step'])) $this->definition[]	= array('validate' => 'step',		'name' => $v['name'],	'val' => $v['step'],		'label' => $v['label'],	'type' => $v['type']);
								if ($v['validate'])
									$this->definition[] = ['validate' => 'validate', 'name' => $v['name'], 'val' => $v['validate'], 'label' => $v['label'], 'type' => $v['type']];    //指定
								if ($v['type'] == 'change-password') {    //更換密碼
									$this->definition[] = ['validate' => 'change-password', 'name' => $v['name'], 'val' => $v['name'], 'label' => $this->l('當前密碼'), 'type' => $v['type']];
								};
								if ($v['unique']) {            //唯一數值
									$this->definition[] = ['validate' => 'unique', 'name' => $v['name'], 'val' => $v['name'], 'label' => $v['label'], 'type' => $v['type']];
								};

								if ($v['type'] == 'datetime-local') {        //日期時間
									$this->definition[] = [
										'validate' => 'isdatetime',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if ($v['type'] == 'date') {        //日期
									$this->definition[] = [
										'validate' => 'isdate',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if ($v['type'] == 'time') {        //時間
									$this->definition[] = [
										'validate' => 'istime',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if ($v['type'] == 'email') {        //email
									$this->definition[] = [
										'validate' => 'isemail',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if (isset($v['confirm'])) {    //確認密碼
									foreach ($form['input'] as $ji => $jv) {
										if ($jv['name'] == $v['confirm'])
											$label = $jv['label'];
									};
									$this->definition[] = ['validate' => 'confirm', 'name' => $v['name'], 'val' => $v['confirm'], 'label' => [$v['label'], $label], 'type' => $v['type']];
								};
							};
						};
					};
				};
			}
		};
		return $this->definition;
	}

	//取得樣版
	public function getLayout()
	{
		$entity              = $this->page;
		$layout_dir          = $this->getThemeDir();
		$layout_override_dir = $this->getOverrideThemeDir();
		$layout              = false;
		if ($entity) {    //覆蓋
			if (file_exists($layout_override_dir . DS . 'layout-' . $entity . '.tpl')) {
				$layout = $layout_override_dir . DS . 'layout-' . $entity . '.tpl';
			};
			if (!$layout && file_exists($layout_dir . DS . 'layout-' . $entity . '.tpl')) {
				$layout = $layout_dir . DS . 'layout-' . $entity . '.tpl';
			};
		}
		//預設);
		if (!$layout && file_exists($layout_dir . 'layout.tpl')) {
			$layout = $layout_dir . 'layout.tpl';
		};
		return $layout;
	}

	//取得樣版
	protected function getThemeDir()
	{
		return $this->useMobileTheme() ? THEME_MOBILE_DIR : THEME_DIR;
	}


	//是否有線上編輯權限
	public function checkLiveEditAccess()
	{
		/*
		if (!Tools::isSubmit('live_edit') || !Tools::getValue('ad') || !Tools::getValue('liveToken')) return false;
		if (Tools::getValue('liveToken') != Tools::getAdminToken('AdminModulesPositions'.(int)Tab::getIdFromClassName('AdminModulesPositions').(int)Tools::getValue('id_employee'))) return false;
		return is_dir(_PS_CORE_DIR_.DIRECTORY_SEPARATOR.Tools::getValue('ad'));
		*/
	}

	//線上編輯樣版
	public function getLiveEditFooter()
	{
		//是否有線上編輯權限
		/*
		if($this->checkLiveEditAccess()){
		$data = $this->context->smarty->createData();
		$data->assign(array(
			'ad'        => Tools::getValue('ad'),
			'live_edit' => true,
			'hook_list' => Hook::$executed_hooks,
			'id_shop'   => $this->context->shop->id
			));
			return $this->context->smarty->createTemplate(_PS_ALL_THEMES_DIR_.'live_edit.tpl', $data)->fetch();
		}else{
			return '';
		};
		*/
	}


	//取得覆蓋樣版
	protected function getOverrideThemeDir()
	{
		return $this->useMobileTheme() ? OVERRIDE_THEME_DIR . DS . 'mobile' : OVERRIDE_THEME_DIR;
	}

	public function displayAjax()
	{
		if ($this->json) {
			$this->context->smarty->assign([
				'json'   => true,
				'status' => $this->status,
			]);
		};
		$this->layout                    = 'layout-ajax.tpl';
		$this->display_header            = false;
		$this->display_footer            = false;
		$this->display_header_javascript = false;
		$this->display_footer_javascript = false;
		return $this->display();
	}

	public function initRefusePage()
	{
		$this->ajax           = false;
		$this->display_header = false;
		$this->display_footer = false;
		$this->layout         = THEME_DIR . 'page-not-found.tpl';
	}

	public function login($page = 'Login')
	{    //需要登入
		if ($this->controller_name != 'Login' && !isset($_SESSION['id_admin'])) {
			Member::logout();
			Tools::redirectLink('//' . WEB_DNS . '/' . $page . '?page=' . base64_encode($_SERVER['REQUEST_URI']));        //todo 未登入無SESSION到404 會變成預設404
			exit;
		}
	}

	public function displayAjaxFCM()
	{
		$reg_id = Tools::getValue('reg_id');
		$type   = Tools::getValue('type');
		/*
		 * 新增 reg_id
		 */
		if (!empty($reg_id) && !empty($_SESSION['id_member']) && !empty($type)) {
			$id_member = $_SESSION['id_member'];
			if (!empty($id_member)) {
				$sql = sprintf('INSERT INTO `' . DB_PREFIX_ . 'fcm_reg_id` (`reg_id`, `id_member`, `type`, `model`)
									VALUES (%s, %d, %s, %s) ON
									DUPLICATE KEY
									UPDATE `reg_id` = %s,`id_member` = %d, `type` = %s, `model` = %s',
					GetSQL($reg_id, 'text'),
					$id_member,
					GetSQL($type, 'text'),
					GetSQL(get_mobile(), 'text'),
					GetSQL($reg_id, 'text'),
					$id_member,
					GetSQL($type, 'text'),
					GetSQL(get_mobile(), 'text'));
				Db::rowSQL($sql);
			}
			$json = new JSON();
			$num  = 1;//Message::getMemberNum($_SESSION['id_member']);		//所有未讀數字
			$time = time();
			echo $json->encode([
				'reg_id'    => $reg_id,
				'id_member' => $id_member,
				'msg_num'   => $num,
				'msg_time'  => $time,
			]);
		}
		exit;
	}

	public function displayAjaxPopularity(){//店家星星
        $action = Tools::getValue('action');
        $star   = Tools::getValue('star');
        $id = Tools::getValue('id');
        $id_member = $_SESSION["id_member"];

        switch($action){
            case 'Popularity':
                if(empty($id_member)){
                    echo json_encode(array("error"=>"請登入您的帳號","return"=>""));
                    break;
                }
                if(empty($id)){
                    echo json_encode(array("error"=>"未選擇店家","return"=>""));
                    break;
                }
                $sql = "SELECT count(*) star_num FROM `store_popularity` WHERE id_member=".GetSQL($id_member,"int");
                $num = Db::rowSQL($sql,true);//取得查看是否有該檔案
                if($num["star_num"]<='0'){//沒有點選過
                    $sql = "INSERT INTO `store_popularity`(`id_store`,`popularity`,`id_member`)
                            VALUES(".GetSQL($id,"int").",".GetSQL($star,"int").",".GetSQL($id_member,"int").")";
                    Db::rowSQL($sql);
                    $mystar = $star;//丟進來就丟回去
                    $sql = "SELECT sum(popularity)/count(*) as total FROM `store_popularity` WHERE `id_store`=".GetSQL($id,"int");
                    $popularity = Db::rowSQL($sql,true);

                    echo json_encode(array("error"=>"","return"=>array("popularity"=>$popularity["total"],"mystar"=>$mystar)));
                }else{//以點選過
                    $sql = "UPDATE `store_popularity` set `popularity`=".GetSQL($star,"int")."
                            WHERE id_store=".GetSQL($id,"int")." AND id_member=".GetSQL($id_member,"int");
                    Db::rowSQL($sql);
                    $mystar = $star;//丟進來就丟回去
                    $sql = "SELECT sum(popularity)/count(*) as total FROM `store_popularity` WHERE `id_store`=".GetSQL($id,"int");
                    $popularity = Db::rowSQL($sql,true);

                    echo json_encode(array("error"=>"","return"=>array("popularity"=>$popularity["total"],"mystar"=>$mystar)));
                }

            break;

            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
            break;

        }
    }

    public function displayAjaxFavorite(){
        $action = Tools::getValue('action');
        $table_id = Tools::getValue('table_id');
        $table_name = Tools::getValue('table_name');
        $session_id = session_id();//取得session_id
        $active = "";
        switch($action){
            case 'Favorite':
                if(empty($table_id)){
                    echo json_encode(array("error"=>"資料錯誤","return"=>""));
                    break;
                }
                if(empty($table_name)){
                    echo json_encode(array("error"=>"資料錯誤","return"=>""));
                    break;
                }

                if(!empty($_SESSION["id_member"])){//取得會員id
                    $id_member = $_SESSION["id_member"];
                    $sql = "SELECT count(*) count_num,active FROM `favorite`
                    WHERE id_member=".GetSQL($id_member,"int")." AND table_id=".GetSQL($table_id,"int")." AND table_name=".GetSQL($table_name,"text");
                    $row = Db::rowSQL($sql,true);
                    if($row["count_num"]==0){//不存在
                        $sql = "INSERT INTO `favorite`(`table_name`,`table_id`,`id_member`,`active`,`update_time`)
                        VALUES (".GetSQL($table_name,"text").",".GetSQL($table_id,"int").",
                        ".GetSQL($id_member,"int").",'1',NOW())";
                        $active = "1";
                    }else{//存在
                        $sql = "SELECT * FROM `favorite`
                        WHERE id_member=".GetSQL($id_member,"int")." AND table_id=".GetSQL($table_id,"int")." AND table_name=".GetSQL($table_name,"text");//存在則拿去來
                        $row = Db::rowSQL($sql,true);
                        $active = $row["active"]=='0'?'1':'0';//如果現在是0 要改成1 如果非0改0 因為點選後就是改變現有狀態
                        $sql = "UPDATE `favorite` set `update_time`=NOW() , `active`=".GetSQL($active,"int")."
                        WHERE `id_member`=".GetSQL($id_member,"int")." AND table_id=".GetSQL($table_id,"int")." AND table_name=".GetSQL($table_name,"text");
                    }
            Db::rowSQL($sql);
//            echo $sql;
                }else{//沒有會員id 操作session_id
                    $id_member = "";
                    $sql = "SELECT count(*) count_num,active FROM `favorite`
                    WHERE `session_id`=".GetSQL($session_id,"text")." AND table_id=".GetSQL($table_id,"int")." AND table_name=".GetSQL($table_name,"text");

                    $row = Db::rowSQL($sql,true);
                    if($row["count_num"]==0){//不存在
                        $sql = "INSERT INTO `favorite`(`table_name`,`table_id`,`session_id`,`active`,`update_time`)
                        VALUES (".GetSQL($table_name,"text").",".GetSQL($table_id,"int").",
                        ".GetSQL($session_id,"text").",'1',NOW())";
                        $active = "1";
                    }else{//存在
                        $sql = "SELECT * FROM `favorite`
                        WHERE `session_id`=".GetSQL($session_id,"text")." AND table_id=".GetSQL($table_id,"int")." AND table_name=".GetSQL($table_name,"text");//存在則拿去來
                        $row = Db::rowSQL($sql,true);
                        $active = $row["active"]=='0'?'1':'0';//如果現在是0 要改成1 如果非0改0 因為點選後就是改變現有狀態
                        $sql = "UPDATE `favorite` set `update_time`=NOW() , `active`=".GetSQL($active,"int")."
                        WHERE `session_id`=".GetSQL($session_id,"text")." AND table_id=".GetSQL($table_id,"int")." AND table_name=".GetSQL($table_name,"text");
                    }
            Db::rowSQL($sql);
//            echo $sql;
                }

            echo json_encode(array("error"=>"","active"=>$active));
            break;

            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
            break;
        }

    }


    public function displayAjaxMessageVerificationSMS()//發送簡訊驗證碼 已存在則跳出
    {
        $action = Tools::getValue('action');    //action = 'Tel';
        $mobile = Tools::getValue('phone');    //手機號碼
        switch($action){
            case 'MessageVerificationSMS':

                $sql = "select count(*) coun from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($mobile, 'text');
                //避免10分鐘內重複撥打同支手機
                $row_session     = Db::rowSQL($sql, true);
                $session_count = $row_session["coun"];

                $sql = "SELECT count(*) as m_count  FROM `member` WHERE active=1 AND user=".GetSQL($mobile,'text');
                $row_count     = Db::rowSQL($sql, true);

                if(!preg_match("/09[0-9]{8}/",$mobile)){
                    echo json_encode(array("error"=>"tel error","return"=>"請輸入手機號碼 如:09XXXXXXXX123"));
                    break;
                }else if($session_count > '0'){
                    echo json_encode(array("error"=>"tel wait","return"=>"請等待10分鐘後再輸入手機號碼"));
                    break;
                }else if($row_count>=1){
                    echo json_encode(array("error"=>"user exist","return"=>"此帳號已存在 請登入"));
                    break;
                }

                $user_ip = $_SERVER['REMOTE_ADDR'];//如果有異常的發送則能夠透過log得知
                $text = rand(1000,9999);//生成隨機的4碼
                $text_sms = '您的驗證碼為:'.$text;
                $user_id = '';
                $sms_id= '';
                $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
                //自己輸入否則會連 您的驗證碼為:都有影響輸入
                $sql = "INSERT INTO sms_log (`session_id`, `tel`, `captcha`, `ip`, `msgid`, `statuscode`,
                            `return_status`, `accountPoint`)
                            VALUES (".GetSQL(session_id(), 'text').",".GetSQL($mobile, 'text').",
                            ".GetSQL($text, 'text').",".GetSQL($user_ip, 'text').",".GetSQL($sms_return['msgid'],
                        'text').",".GetSQL($sms_return['statuscode'], 'text').",
                            ".GetSQL(SMS::sms_status_text($sms_return['statuscode'],"mitake"), 'text').",".GetSQL($sms_return['AccountPoint'], 'int').")";
                Db::rowSQL($sql);//存入資料庫

                if($sms_return['statuscode'] =='0' ||$sms_return['statuscode'] =='1' || $sms_return['statuscode'] =='2'|| $sms_return['statuscode'] =='4'){
                    echo json_encode(array("error"=>"","return"=>"請等候簡訊發送"));
                    break;
                }else{
                    echo json_encode(array("error"=>"","return"=>"簡訊發送有誤 請洽管理人員"));
                    break;
                }
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

    public function displayAjaxRentHouseMessage()//發送訊息~ 這邊是對應RentHouse 跟 List的留言 預約賞屋 呼叫樂租顧問
    {
        $action = Tools::getValue('action');
        $id = Tools::getValue('id');
        $type = Tools::getValue('type');
        $name = Tools::getValue('name');
        $sex = Tools::getValue('sex');
        $phone = Tools::getValue('phone');
        $tel = Tools::getValue('tel');
        $email = Tools::getValue('email');
        $contact_time = Tools::getValue('contact_time');
        $appointment_period = Tools::getValue('appointment_period');
        $message = Tools::getValue('message');
        $agree = Tools::getValue('agree');
        $appointment_time = Tools::getValue('appointment_time');
        $verification = Tools::getValue('verification');
        $id_member = $_SESSION['id_member'];
        //另外加的共通判斷

//        if(empty($id_member)){
//            echo json_encode(array("error"=>"請登入帳號或註冊帳號後再登入","return"=>""));
//            exit;
//        }

        $sql = "select count(*) coun from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($phone, 'text');
        //避免10分鐘內重複撥打同支手機
        $row_session     = Db::rowSQL($sql, true);
        $session_count = $row_session["coun"];
        $sql = "SELECT max(*) as member_num FROM `member` WHERE user=".GetSQL($phone,'text');
        $is_member = Db::rowSQL($sql,true);

        if(!empty($id_member)) {
            //直接到下面階段
        }else if(empty($id_member) && $is_member['member_num']>=1 && empty($verification) && $session_count<=1){//代表有帳號未登入
            echo json_encode(array("error"=>"該手機已註冊請登入帳號並留言","return"=>""));
            exit;
        }else if(empty($id_member) && ($type==2 || $type==3)){//預約跟呼叫 必須要登入
            echo json_encode(array("error"=>"請註冊後再留言","return"=>""));
        }else if(empty($verification) && $session_count<=1 && $type==1){
            echo json_encode(array("error"=>"請註冊並登入帳號或是驗證手機","return"=>""));
            exit;
        }else{
            $sql = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($phone, 'text');
            $row    = Db::rowSQL($sql, true);
            $captcha_log = $row['captcha']; //驗證碼
            if(empty($captcha_log) || $captcha_log!=$verification){
                echo json_encode(array("error"=>"請重新輸入驗證碼","return"=>""));
                exit;
            }
        }

        switch($action){
            case 'RentHouseMessage':
                if($id=="" && ($type!="1" && $type!="2" && $type!="3")){//走特別路線才會這樣
                    echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                    break;
                }
                if($agree !='1'){
                    echo json_encode(array("error"=>"請勾選同意書","return"=>""));
                    break;
                }
                if($name ==''){
                    echo json_encode(array("error"=>"請輸入姓名","return"=>""));
                    break;
                }
                if(strlen($name)>'32'){
                    echo json_encode(array("error"=>"輸入姓名過長","return"=>""));
                    break;
                }
                if($sex!='0' && $sex !='1' && $sex !='2'){
                    echo json_encode(array("error"=>"請選擇身分","return"=>""));
                    break;
                }
                if($contact_time!='0' && $contact_time !='1' && $contact_time !='2' && $contact_time !='3'){
                    echo json_encode(array("error"=>"請選擇聯絡時間","return"=>""));
                    break;
                }
                if($type=='2' && ($appointment_period!='0' && $appointment_period !='1' && $appointment_period !='2' && $appointment_period !='3')){
                    echo json_encode(array("error"=>"請選擇預約時間","return"=>""));
                    break;
                }
                if(strlen($email)>'128'){
                    echo json_encode(array("error"=>"信箱輸入過長","return"=>""));
                    break;
                }
                if(strlen($message)>'100'){
                    echo json_encode(array("error"=>"請留言100個字內","return"=>""));
                    break;
                }
                if($type=='2'){
                    if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$appointment_time)){
                        echo json_encode(array("error"=>"請輸入正確的預約賞屋時間","return"=>""));
                        break;
                    }else if(strtotime(date("Y-m-d",strtotime("+1 week")." 23:59:99"))<=strtotime($appointment_time." 23:59:99")){
                        echo json_encode(array("error"=>"請輸入一週內的時間","return"=>""));
                        break;
                    }
                }
                if($tel!='' && strlen($tel)>"16"){
                    echo json_encode(array("error"=>"市話號碼輸入過長","return"=>""));
                    break;
                }
                if(strlen($phone)>'16'){
                    echo json_encode(array("error"=>"手機號碼輸入過長","return"=>""));
                    break;
                }else if(!preg_match("/09[0-9]{8}/",$phone)){
                    echo json_encode(array("error"=>"請輸入正確的手機號碼","return"=>""));
                    break;
                }

                $sex_name = '';
                if(empty($sex_name)){
                    $sex_name = "女士";
                }else{
                    $sex_name = "先生";
                }
                $contact_time_name = '';
                if($contact_time=="0"){
                    $contact_time_name = "早上";
                }else if($contact_time=="1"){
                    $contact_time_name = "下午";
                }else if($contact_time=="2"){
                    $contact_time_name = "晚上";
                }else if($contact_time=="3"){
                    $contact_time_name = "其他";
                }
                $appointment_period_name = "";
                if($appointment_period=="0"){
                    $appointment_period_name = "早上";
                }else if($appointment_period=="1"){
                    $appointment_period_name = "下午";
                }else if($appointment_period=="2"){
                    $appointment_period_name = "晚上";
                }else if($appointment_period=="3"){
                    $appointment_period_name = "皆可";
                }

                if($type=='1'){//留言 要寄email
                    $sql = "SELECT r_h.`id_member` as id_member,
                    (IF(m.`gender`=1,'先生','小姐')) as member_gender,m.`user` as user,m.`name` as name,
                    r_h.`case_name` as case_name,
                    IF(r_h.`rent_price_m` ,r_h.`rent_price_m`,r_h.`reserve_price_m`) as rent_cash,
                    r_h.`ping` as ping,
                    IFNULL((CASE a.`phone` WHEN '' THEN r_h.`ag_phone`
                                    WHEN null THEN r_h.`ag_phone`
                                    ELSE a.`phone`
                                    END ),'0939688089')as ag_phone,
                    IFNULL((CASE a.`first_name` WHEN ''   THEN r_h.`develop_ag`
                                         WHEN null THEN r_h.`develop_ag`
                                         ELSE CONCAT(a.`last_name`,a.`first_name`)
                                        END ),'Joy')  as develop_ag,
                     IFNULL((CASE a.`email` WHEN '' THEN r_h.`ag_email`
                                    WHEN null THEN r_h.`ag_email`
                                    ELSE a.`email`
                                    END ),'ilifeproperty@gmail.com')as email,
                    IF(((r_h.`rental_floor_s` = r_h.`all_floor`) &&
                    (r_h.`rental_floor_e` = r_h.`underground` ||
                    ((r_h.`rental_floor_e`='' || r_h.`rental_floor_e`='0' || r_h.`rental_floor_e`='1')
                    && (r_h.`underground`='' || r_h.`underground`='0'))
                    )),1,0) whole_building,
                    (CASE r_h.`part_address` WHEN '' THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
                    IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,'鄰'),''),
                    r_h.`road`,r_h.`segment`,
                    IF(r_h.`lane`,CONCAT(r_h.`lane`,'巷'),'')  )
                    ELSE r_h.`part_address`
                     END) rent_address,
                    m.`id_member_additional` as id_member_additional
                    FROM rent_house AS r_h
                    LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                    LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                    LEFT JOIN member as m ON m.`id_member` = r_h.`id_member`
                    LEFT JOIN admin as a ON a.ag = r_h.`ag`
                    WHERE id_rent_house=".GetSQL($id,"int");
                    $rent_house_data = Db::rowSQL($sql,true);




                    $subject = "生活房屋租屋留言通知 案名:{$rent_house_data["case_name"]} 留言";
                    $email_message =<<<html
<html>
    <body>
       <div><span>[樂租-留言]生活房屋祝你諸事順利!</span></div>
       <div><span>案名:{$rent_house_data["case_name"]}</span></div>
       <div><span>預約人:{$name} {$sex_name}</span></div>
       <div><span>電話號碼:{$phone} 市話:{$tel} 聯絡時段:{$contact_time_name}</span></div>
       <div><span>電子信箱:{$email}</span></div>
       <div><span>地址:{$rent_house_data["rent_address"]} 坪數:{$rent_house_data["ping"]}坪</span></div>
       <div><span>留言:{$message}</span></div>
    </body>
</html>
html;

//                    GmailAPI::sendmail($rent_house_data["email"],$subject,$email_message);

                }else if($type=='2'){//傳送預約賞屋的簡訊 這邊涉及了三種身分
                    $sql = "SELECT r_h.`id_member` as id_member,
                    (IF(m.`gender`=1,'先生','小姐')) as member_gender,m.`user` as user,m.`name` as name,
                    r_h.`case_name` as case_name,
                    IF(r_h.`rent_price_m` ,r_h.`rent_price_m`,r_h.`reserve_price_m`) as rent_cash,
                    r_h.`ping` as ping,
                    IFNULL((CASE a.`phone` WHEN '' THEN r_h.`ag_phone`
                                    WHEN null THEN r_h.`ag_phone`
                                    ELSE a.`phone`
                                    END ),'0939688089')as ag_phone,
                    IFNULL((CASE a.`first_name` WHEN ''   THEN r_h.`develop_ag`
                                         WHEN null THEN r_h.`develop_ag`
                                         ELSE CONCAT(a.`last_name`,a.`first_name`)
                                        END ),'Joy')  as develop_ag,
                    IF(((r_h.`rental_floor_s` = r_h.`all_floor`) &&
                    (r_h.`rental_floor_e` = r_h.`underground` ||
                    ((r_h.`rental_floor_e`='' || r_h.`rental_floor_e`='0' || r_h.`rental_floor_e`='1')
                    && (r_h.`underground`='' || r_h.`underground`='0'))
                    )),1,0) whole_building,
                    (CASE r_h.`part_address` WHEN '' THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
                    IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,'鄰'),''),
                    r_h.`road`,r_h.`segment`,
                    IF(r_h.`lane`,CONCAT(r_h.`lane`,'巷'),'')  )
                    ELSE r_h.`part_address`
                     END) rent_address,
                    m.`id_member_additional` as id_member_additional
                    FROM rent_house AS r_h
                    LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                    LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                    LEFT JOIN member as m ON m.`id_member` = r_h.`id_member`
                    LEFT JOIN admin as a ON a.ag = r_h.`ag`
                    WHERE id_rent_house=".GetSQL($id,"int");
                    $rent_house_data = Db::rowSQL($sql,true);
                    $head_name= "";//sms傳送前導文字
                    $mobile = "";//發送手機
                    $sms_name = "";//發送

                    if(strstr($rent_house_data["id_member_additional"],"3")){//房仲身分
                        $sql = "SELECT * FROM `broker` WHERE id_member=".GetSQL($rent_house_data["id_member"],"int");
                        $broker_data =  Db::rowSQL($sql,true);
                        $head_name =  $broker_data["job"];
                        $mobile = $rent_house_data["user"];
                        $sms_name = $rent_house_data["name"];

                    }else if(strstr($rent_house_data["id_member_additional"],"4")){//房東身分
                        $head_name = "房東";
                        $mobile = $rent_house_data["user"];
                        $sms_name = $rent_house_data["name"];
                    }else{//都沒有就是我們公司的
                        $head_name = "業務";
                        if(empty($rent_house_data["ag_phone"])){$mobile = "0939688089";}else{$mobile=$rent_house_data["ag_phone"];}
                        if(empty($rent_house_data["develop_ag"])){$sms_name = "Joy";}else{$sms_name=$rent_house_data["develop_ag"];}
                    }

                    $text_sms =<<<html
                    [樂租-預約看屋]生活房屋祝你諸事順利! 預約人:{$name} {$sex_name}、電話號碼:{$phone}、市話:{$tel}、電子信箱:{$email}、生活樂租的租-{$rent_house_data["case_name"]}、地址:{$rent_house_data["rent_address"]}、{$rent_house_data["ping"]}坪、聯絡時段:{$contact_time_name}、留言:{$message}
html;

//                    $text_sms =<<<html
//                    {$head_name}:{$sms_name}
//                    留言者:{$name} {$sex_name}   電話號碼:{$phone}
//                    市話: {$tel}
//                    電子信箱:{$email}
//                    預約賞屋時間:{$appointment_time} {$appointment_period_name}
//                    聯絡時段:{$contact_time_name}
//                    留言:{$message}
//html;
                    $user_id = '';
                    $sms_id= '';
                    $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"on");

                }else if($type=='3'){//呼叫樂租顧問 只有樂租ag與手機問題
                    $date = date("H:i:s");

                    if(strtotime($date)>= strtotime("20:00:00") || strtotime($date)<= strtotime("08:00:00")){
                        echo json_encode(array("error"=>"顧問們呼叫時間為早上8點~晚上10點之間","return"=>""));
                        break;
                    }


                    $sql = "SELECT
                    r_h.`case_name` as case_name,r_h.`ping` as ping,
                    (CASE r_h.`part_address` WHEN '' THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
                    IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,'鄰'),''),
                    r_h.`road`,r_h.`segment`,
                     IF(r_h.`lane`,CONCAT(r_h.`lane`,'巷'),'')  )
                     ELSE r_h.`part_address`
                     END) rent_address,
                    IFNULL((CASE a.`phone` WHEN '' THEN r_h.`ag_phone`
                                    WHEN null THEN r_h.`ag_phone`
                                    ELSE a.`phone`
                                    END ),'0939688089')as ag_phone,
                    IFNULL((CASE a.`first_name` WHEN ''   THEN r_h.`develop_ag`
                                         WHEN null THEN r_h.`develop_ag`
                                         ELSE CONCAT(a.`last_name`,a.`first_name`)
                                        END ),'Joy')  as develop_ag,
                    FROM rent_house as r_h
                    LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                    LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                    LEFT JOIN admin as a ON a.ag = r_h.`ag`
                    WHERE r_h.`id_rent_house`=".GetSQL($id,"int");
                    $member_data = Db::rowSQL($sql,true);
                    if(empty($member_data["develop_ag"])){$member_data["develop_ag"] = "Joy";}
                    if(empty($member_data["ag_phone"])){$member_data["develop_ag"] = "0939688089";}
                    $mobile = $member_data["ag_phone"];


                    $text_sms =<<<html
                    [樂租-呼叫樂租顧問]生活房屋祝你諸事順利! 預約人:{$name} {$sex_name}、電話號碼:{$phone}、市話:{$tel}、電子信箱:{$email}、生活樂租的租-{$member_data["case_name"]}、地址:{$member_data["rent_address"]}、{$member_data["ping"]}坪、聯絡時段:{$contact_time_name}、留言:{$message}
html;

//                    $text_sms =<<<html
//                    業務:{$member_data["develop_ag"]}
//                    留言者:{$name} {$sex_name}   電話號碼:{$phone}
//                    市話: {$tel}
//                    電子信箱:{$email}
//                    聯絡時段:{$contact_time_name}
//                    留言:{$message}
//html;
                    $user_id = '';
                    $sms_id= '';
                    $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"on");
                }

                $sql = "INSERT INTO `rent_house_message`(`id_rent_house`,`message_type`,`name`,`sex`,`phone`,`tel`,`email`,`appointment_time`,`appointment_period`,`contact_time`,`message`,`schedule`,`create_time`)
                        VALUES(".GetSQL($id,"int").",".GetSQL($type,"int").",".GetSQL($name,"text").",".GetSQL($sex,"int").",
                        ".GetSQL($phone,"text").",".GetSQL($tel,"text").",".GetSQL($email,"text").",
                        ".GetSQL($appointment_time,"text").",".GetSQL($appointment_period,"int").",".GetSQL($contact_time,"int").",
                        ".GetSQL($message,"text").",0,NOW())";
                Db::rowSQL($sql);//儲存資料

                echo json_encode(array("error"=>"","return"=>$type));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;
    }


    public function displayAjaxInternationalTel(){//有包含國際碼的註冊
        $action = Tools::getValue('action');    //action = 'Tel';
        $tel_code = (substr(Tools::getValue('tel_code'),0,1)=='0')?substr(Tools::getValue('tel_code'),1):Tools::getValue('tel_code');
        $mobile = Tools::getValue('international_code').$tel_code;//包含國際碼的手機號碼

        switch($action){
            case 'InternationalTel':
                $sql = 'SELECT count(*) coun FROM member WHERE user='.GetSQL($mobile, 'text');
                $row     = Db::rowSQL($sql, true);
                $count =  $row['coun'];//該手機是否有申請

                $sql = "select count(*) coun from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($mobile, 'text');
                //避免10分鐘內重複撥打同支手機

                $row_session     = Db::rowSQL($sql, true);
                $session_count = $row_session["coun"];

                if(empty($mobile) || $mobile==Tools::getValue('international_code')){
                    echo json_encode(array("error"=>"tel error","return"=>"請輸入手機號碼 "));
                    break;
                }else if($count > '0'){
                    echo json_encode(array("error"=>"Repeat registration","return"=>"該帳號已註冊"));
                    break;
                }else if($session_count > '0'){
                    echo json_encode(array("error"=>"tel wait","return"=>"請等待10分鐘後再輸入手機號碼"));
                    break;
                }

                $user_ip = $_SERVER['REMOTE_ADDR'];//如果有異常的發送則能夠透過log得知
                $text = rand(1000,9999);//生成隨機的4碼
                $text_sms = '您的驗證碼為:'.$text;
                $user_id = '';
                $sms_id= '';
                $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
                //自己輸入否則會連 您的驗證碼為:都有影響輸入
                $sql = "INSERT INTO sms_log (`session_id`, `tel`, `captcha`, `ip`, `msgid`, `statuscode`,
                            `return_status`, `accountPoint`)
                            VALUES (".GetSQL(session_id(), 'text').",".GetSQL($mobile, 'text').",
                            ".GetSQL($text, 'text').",".GetSQL($user_ip, 'text').",".GetSQL($sms_return['msgid'],
                        'text').",".GetSQL($sms_return['statuscode'], 'text').",
                            ".GetSQL(SMS::sms_status_text($sms_return['statuscode'],"mitake"), 'text').",".GetSQL($sms_return['AccountPoint'], 'int').")";
                Db::rowSQL($sql);//存入資料庫

                if($sms_return['statuscode'] =='0' ||$sms_return['statuscode'] =='1' || $sms_return['statuscode'] =='2'|| $sms_return['statuscode'] =='4'){
                    echo json_encode(array("error"=>"","return"=>"請等候簡訊發送"));
                    break;
                }else{
                    echo json_encode(array("error"=>"","return"=>"簡訊發送有誤 請洽管理人員"));
                    break;
                }
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }

    }

}
