<?php
require_once (__DIR__ .DS. 'Controller.php');
abstract class AdminModuleController extends AdminController{
	public function __construct(){	//數值
		$this->controller_type = 'adminmodule';
		$this->module = Module::getInstanceByName(Tools::getValue('module'));
		if(empty($this->module)){
			$this->module = Dispatcher::getContext()->module;
		}
		if(is_file(MODULE_DIR.DS.$this->module.DS.'Autoload.php')){
			include_once(MODULE_DIR.DS.$this->module.DS.'Autoload.php');
		}
		Context::getContext()->smarty->setTemplateDir(array(
			OVERRIDE_MODULES_DIR.DS.$this->module.DS.'templates'.DS.'admin',
			OVERRIDE_MODULES_DIR.DS.$this->module,
			WEB_DIR.DS.'modules'.DS.$this->module.DS.'templates'.DS.'admin',
			WEB_DIR.DS.'modules'.DS.$this->module,
		));
		
		parent::__construct();
		$this->id_tab = Tab::getContext()->getIdFromClassName($this->controller_name);
		self::$currentIndex = '/'.ADMIN_URL.'/m/'.$this->admin_url.'?';
	}
}