<?php
require_once(__DIR__ . DS . 'Controller.php');

abstract class AdminController extends Controller
{
	protected $FormTable;

	public $_errors = [];        //錯誤訊息
	public $_errors_name = [];    //錯誤欄位
	public $_errors_name_i = [];    //錯誤欄位
	public $admin_css;
	public $admin_js;
	public $className;
	public $admin_theme;
	public $meta_title;
	public $breadcrumb;
	public $layout = 'layout.tpl';
	public $override_folder;
	public $admin_url;
	public $id_tab;
	public $table;
	public $_join;
	public $_group;
	public $_as;
	public $list_id;
	public $fields = [];        //列表
	public $actions;                //可用編輯
	public $bulk_actions;            //批量操作
	public $display_media_bak = true;
	public $submit_action;            //送出執行動做
	public $index;
	public $tabAccess;            //頁面權限
	public static $currentIndex;
	public $definition;            //表單定義
	public $excel_definition;        //表單定義
	public $_conf;
	public $no_link = true;            //點列表沒有聯結
	public $display_edit_div = true;    //顯示編輯欄位
	public $page;                //頁面
	public $no_information = '';        //沒有資料文字
	public $display_page_header_toolbar = true;
	public $show_list_num = true;
	public $post_processing = [];    //事後處理 例 array('edit') = 點完檢視後進去才能修改, array('edit', 'del') = 點完檢視後進去才能修改、刪除
	public $back_url = '';
	public $my_where_web = null;        //區分網站權限

	public $excel_import_result_dir = '';            //匯入EXCEL資料夾路徑
	public $excel_import_dir = '';                //下載空白匯入EXCEL資料夾路徑
	public $excel_import_data = [];            //excel POST EXCEL匯入的DATA
	public $excel_num = 0;                    //excel 資料筆數
	public $excel_error_num = [];                    //excel 錯誤欄數
	public $_excel_errors = [];            //excel 錯誤訊息
	public $_excel_errors_txt = [];            //excel 錯誤訊息(註解)
	public $_excel_errors_key = [];            //excel 錯誤位置
	public $excel_sheet_data = [];            //excel 匯入的值
	public $excel_sheet_index_data = [];        //excel 匯入關連鍵索引值
	public $meta_viewport = [];

	public $show_cancel_button = false;        //顯示取消按鈕 (預設關閉)		todo 調查作用

	public $arr_calendar_input = [];            //行事曆輸出格式

	public $top_bar_before_htm = '';                //top_bar 前原始碼
	public $top_bar_after_htm = '';                    //top_bar 後原始碼
	public $min = null;						//最小筆數
	public $max = null;						//最大筆數
	public $num = 0;
    public $conn;//連接用


	public function __construct()
	{    //數值
		Admin::autologoing();
		$this->no_information          = $this->l('找不到資料!');
		$this->fields['show_list_num'] = $this->show_list_num;
		$this->layout                  = 'layout.tpl';
		$this->controller_type         = 'admin';
		$this->controller_name         = get_class($this);        //class 名稱
		$this->admin_theme             = Themes::getContext()->getAdminTheme();
		define('ADMIN_THEME_URL', '/' . ADMIN_URL . '/themes/' . $this->admin_theme);                    //樣版資料夾位置
		define('ADMIN_THEME_DIR', ADMIN_DIR . DS . 'themes' . DS . $this->admin_theme . DS);        //樣版資料夾位置

		if (strpos($this->controller_name, 'Controller')) {
			$this->controller_name = substr($this->controller_name, 0, -10);
		}
		$this->controller_name = str_replace('Admin', '', $this->controller_name);
		$this->page            = $this->controller_name;

		if (!empty($_SESSION['id_web'])) {            //區分網站權限
			$this->my_where_web = ' AND `id_web` = ' . $_SESSION['id_web'];
		}

		$this->excel_import_result_dir = WEB_DIR . DS . FILE_DIR . DS . 'controller' . DS . $this->controller_name . DS . 'excel_import' . DS;
		$this->excel_import_dir        = ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS;

		Context::getContext()->smarty->assign([
			'initialPreview'       => '[]',
			'initialPreviewConfig' => '[]',
			'UpFileVal'            => '[]',
		]);

		if (empty($this->index))
			$this->index = $this->fields['index'];
//$this->definition = '';		//提取快取
		parent::__construct();

//成功訊息
		$this->_conf = [
			'1' => $this->l('新增成功!'),
			'2' => $this->l('修改成功!'),
			'3' => $this->l('刪除成功'),
		];

//失敗訊息
		$this->_conf2 = [
			'1' => $this->l('沒有刪除任何資料!'),
			'2' => $this->l('請先修改密碼才能啟用帳號'),
			'3' => $this->l('您沒有檢視權限'),
			'4' => $this->l('您沒有新增權限'),
			'5' => $this->l('您沒有修改權限'),
			'6' => $this->l('您沒有刪除權限'),
			'7' => $this->l('找不到檔案'),
		];

		$this->override_folder = $this->controller_name;    //樣版資料夾
		$default_theme_name    = 'default';
		if (ADMIN_DEFAULT_THEME && @filemtime(ADMIN_THEME_DIR . ADMIN_DEFAULT_THEME)) {
			$default_theme_name = ADMIN_DEFAULT_THEME;        //後台預設樣版名稱
		}

		//使用者預設樣版路徑
		if (!@filemtime(ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates')) {
			$this->admin_theme = $default_theme_name;
		}

		$this->id_tab = Tab::getContext()->getIdFromClassName($this->controller_name);
		/**自己的主CSS樣版設定*/
		$this->admin_css = ($this->admin_css) ? $this->admin_css : 'admin-theme.css';
		$this->admin_js  = ($this->admin_js) ? $this->admin_js : 'admin-theme.js';
		if (!@filemtime(ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'css' . DS . $this->admin_css)) {
			$this->admin_css = 'admin-theme.css';
		}

		if (!@filemtime(ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'js' . DS . $this->admin_js)) {
			$this->admin_js = 'admin-theme.js';
		}

		$this->tabAccess = Competence::getContext()->getAccess($this->id_tab);
		foreach ($this->tabAccess as $i => $v) {
			if (in_array($i, ['view', 'add', 'edit', 'del'])) {
				if ($v)
					$this->actions[] = $i;
			}
		}

		$this->tpl_folder   = $this->controller_name;
		$this->admin_url    = str_replace('Controller', '', $this->controller_name);
		$this->admin_url    = str_replace('Admin', '', $this->admin_url);
		self::$currentIndex = '/' . ADMIN_URL . '/' . $this->admin_url . '?';
		$this->context->smarty->setTemplateDir([
			OVERRIDE_DIR . DS . 'admin' . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name,
			OVERRIDE_DIR . DS . 'admin' . DS . 'templates',
			ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name,
			ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates',
		]);
		$this->admin_css = (Config::get('ADMIN_CSS')) ? Config::get('ADMIN_CSS') : 'admin-theme.css';
	}

	public function init()
	{
		parent::init();

		//判別管理者社區是否有啟用
		if (!empty($_SESSION['id_web'])) {
			if (!Web::is_active($_SESSION['id_web'])) {        //沒啟用就登出
				Admin::logout(true);
			}
		}

		if (Tools::isSubmit('logout')) {
			Admin::logout();
			Tools::redirectLink('//' . WEB_DNS . '/' . ADMIN_URL);
		}

		if($this->controller_name == 'API'){
            Tools::redirectLink('//' . WEB_DNS . '/' . ADMIN_URL . '/API');        //todo 未登入無SESSION到404 會變成預設404
            exit;
        }else if ($this->controller_name != 'Loging' && !isset($_SESSION['id_admin'])) {
			Admin::logout();
			Tools::redirectLink('//' . WEB_DNS . '/' . ADMIN_URL . '/Loging?page=' . base64_encode($_SERVER['REQUEST_URI']));        //todo 未登入無SESSION到404 會變成預設404
			exit;
		}

		if ($this->controller_name != 'Loging' && $this->controller_name != 'Management') {
			$this->passValidate();
		}
		$this->context->smarty->assign([
			'table'           => $this->table,
			'controller_name' => $this->controller_name,
			'page'            => $this->page,
			'ADMIN_URL'       => ADMIN_URL,
			'admin_url'       => $this->admin_url,
			'no_link'         => $this->no_link,
			'form_action'     => self::$currentIndex,
			'ENTER_SUBMIT'    => Configuration::get('ENTER_SUBMIT'),
		]);

		$this->initProcess();
		$arr_initialPreview       = null;
		$arr_initialPreviewConfig = null;

		foreach ($this->fields['form'] as $j => $jv) {
			foreach ($jv['input'] as $key => $v) {
				if ($this->display == 'add') {
					if ($v['type'] == 'change-password') {
						unset($this->fields['form'][$j]['input'][$key]);
					}
					if ($v['type'] == 'file') {
						$this->fields['form'][$j]['input'][$key]['type'] = 'html';
						$this->fields['form'][$j]['input'][$key]['p']    = $this->l('請先新增儲存完畢後才能上傳檔案!');
					}
//					if ($v['auto_datetime'] == 'add') {
//						unset($this->fields['form'][$j]['input'][$key]);
//					}
				} elseif ($this->display == 'edit') {
					if ($v['type'] == 'new-password' || $v['type'] == 'confirm-password') {
						unset($this->fields['form'][$j]['input'][$key]);
					}
				}
				if ($this->display != 'add') {
					$this->getUpFileList();
				}

				if ($v['type'] == 'signature') {    //如果是簽名
					if (!isset($v['col'])) {
						$this->fields['form'][$j]['input']['col'] = 12;
					}
					if (!isset($v['signature']['screen_full'])) {        //全螢幕
						$this->fields['form'][$j]['input'][$key]['signature']['screen_full'] = true;
					}
					if ($this->fields['form'][$j]['input'][$key]['signature']['screen_full']) {
						$this->fields['form'][$j]['input'][$key]['signature']['screen_full'] = 'true';
					} else {
						$this->fields['form'][$j]['input'][$key]['signature']['screen_full'] = 'false';
					}
					if (!isset($v['signature']['width'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['width'] = 850;
					}
					if (!isset($v['signature']['height'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['height'] = 215;
					}
					if (!isset($v['signature']['color'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['color'] = '#000';
					}
					if (!isset($v['signature']['background-color'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['background-color'] = '#fff';
					}
					if (!isset($v['signature']['lineWidth'])) {
						$this->fields['form'][$j]['input'][$key]['signature']['lineWidth'] = 1;
					}
				}
				if ($v['type'] == 'take_photo') {    //如果是拍照
					if (Tools::usingSecureMode()) {
						//相機初始設定
						if (!isset($v['take_photo']['open_txt'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['open_txt'] = $this->l('開啟相機');
						}
						if (!isset($v['take_photo']['close_txt'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['close_txt'] = $this->l('關閉相機');
						}
						if (!isset($v['take_photo']['take_txt'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['take_txt'] = $this->l('拍照');
						}
						if (!isset($v['take_photo']['height'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['height'] = 225;
						}
						if (!isset($v['take_photo']['width'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['width'] = 300;
						}
						if (!isset($v['take_photo']['font_size'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['font_size'] = '40px';
						}
						if (!isset($v['take_photo']['min_height'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['min_height'] = 30;
						}
						if (!isset($v['take_photo']['min_width'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['min_width'] = 40;
						}
						if (!isset($v['take_photo']['confirm'])) {
							$this->fields['form'][$j]['input'][$key]['take_photo']['confirm'] = $this->l('您確定要刪除此照片?');
						}
					} else {
						Tools::redirectLink('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
					}
				}
				if ($v['type'] == 'map') {    //如果是地圖
					$this->fields['form'][$j]['input'][$key]['no_action'] = true;
					if (!isset($this->fields['form'][$j]['input'][$key]['data']['zoom'])) {
						$this->fields['form'][$j]['input'][$key]['data']['zoom'] = 1;
					}
//					$this->fields['form'][$j]['input'][$key]['html'] = '<div id="map" data-draggable="' . (($this->display == 'edit') ? 'true' : 'false') . '" data-address_key="'.$v['address_key'].' data-lat_key="'.$v['lat_key'].' data-lng_key="'.$v['address_key'].' data-lng_key="'.$v['address_key'].'"></div>';
				}
			}
		}

		$this->initBreadcrumbs();
	}

//POST處理
	public function initProcess()
	{
		$index     = Tools::getValue($this->index);
		$post_code = Tools::getValue('post_code');
		if (Tools::isSubmit('submitAdd' . $this->table) || Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {                            //送出新增(執行)
			if ($this->tabAccess['add']) {
				$this->action  = 'Save';
				$this->display = 'add';
			} else {
				$this->_errors[] = $this->l('您沒有新增權限');
			}
		} elseif ((Tools::isSubmit('submitEdit' . $this->table) || Tools::isSubmit('submitEdit' . $this->table . 'AndStay')) && ($index || empty($this->table)) && !in_array($index, $this->fields['no_edit'])) {    //送出修改(執行)
			if ($this->tabAccess['edit']) {
				$this->action  = 'Save';
				$this->display = 'edit';
			} else {
				$this->_errors[] = $this->l('您沒有修改權限');
			}
		} elseif ((Tools::isSubmit('submitDel' . $this->table) || Tools::isSubmit('del' . $this->table)) && ($index || empty($this->table)) && !in_array($index, $this->fields['no_del'])) {    //送出刪除(執行)
			if ($this->tabAccess['del']) {
				$this->action  = 'del';
				$this->display = 'del';
			} else {
				$this->_errors[] = $this->l('您沒有刪除權限');
			}
		} elseif (Tools::isSubmit('add' . $this->table)) {                                //新增頁面
			if ($this->tabAccess['add']) {
				$this->display = 'add';
			} else {
				$this->_errors[] = $this->l('您沒有新增權限');
			}
		} elseif (Tools::isSubmit('edit' . $this->table) && $index && !in_array($index, $this->fields['no_edit'])) {        //修改頁面
			if ($this->tabAccess['edit']) {
				$this->display = 'edit';
			} else {
				$this->_errors[] = $this->l('您沒有修改權限');
			}
		} elseif (Tools::isSubmit('view' . $this->table) && $index && !in_array($index, $this->fields['no_view'])) {    //送出修改(執行)
			if ($this->tabAccess['view']) {
				$this->display = 'view';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			}
		} elseif (Tools::isSubmit('list' . $this->table) && $index) {    //送出修改(執行)
			if ($this->tabAccess['view']) {
				$this->action  = 'list';
				$this->display = 'list';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			}
		} elseif (Tools::isSubmit('calendar' . $this->table)) {    //送出修改(執行)
			if ($this->tabAccess['view']) {
				$this->action  = 'calendar';
				$this->display = 'calendar';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			}
		} elseif (Tools::isSubmit('excellist' . $this->table)) {    //顯示Excel列表
			if ($this->tabAccess['view']) {
				$this->action  = 'excellist';
				$this->display = 'excellist';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			}
		} elseif (Tools::isSubmit('excel' . $this->table)) {    //匯出Excel(執行)
			if ($this->tabAccess['view']) {
				$this->action  = 'excel';
				$this->display = 'excel';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			}
		} elseif (Tools::isSubmit('excelimport' . $this->table)) {    //匯出Excel(執行)
			if ($this->tabAccess['add']) {
//					$this->action = 'excelimport';
				$this->display = 'excelimport';
			} else {
				$this->_errors[] = $this->l('您沒有新增權限');
			}
		} elseif (Tools::isSubmit('SetMyFieldList' . $this->admin_url)) {        //設定LIST欄會顯示
			if ($this->tabAccess['view']) {
				$this->action  = 'SetMyFieldList';
				$this->display = 'list';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			}
		} elseif (Tools::isSubmit('DelMyFieldList' . $this->admin_url)) {        //清除LIST欄會顯示
			if ($this->tabAccess['view']) {
				$this->action  = 'DelMyFieldList';
				$this->display = 'list';
			} else {
				$this->_errors[] = $this->l('您沒有檢視權限');
			}
		} elseif (Tools::isSubmit('downloadexcelimport' . $this->table)) {    //下載EXCEL空白匯入檔案
			if ($this->tabAccess['add']) {
				$this->display = 'download_excel_import';
			} else {
				$this->_errors[] = $this->l('您沒有新增權限');
			}
		} elseif (Tools::isSubmit('downloadexcelimportresult' . $this->table)) {    //下載EXCEL匯入錯誤結果
			if ($this->tabAccess['view']) {
				$this->display = 'download_excel_import_result';
			} else {
				$this->_errors[] = $this->l('您沒有觀看權限');
			}
		} elseif (Tools::isSubmit('delexcelimport' . $this->table)) {        //刪除EXCEL匯入結果
			if ($this->tabAccess['del']) {
				$this->display = 'del_excel_import';
			} else {
				$this->_errors[] = $this->l('您沒有刪除權限');
			}
		} else {
			switch ($this->display) {
				case 'excelimport':
				case 'add':
					if (!$this->tabAccess['add'])
						$this->_errors[] = $this->l('您沒有新增權限');
				break;
				case 'edit':
					if (!$this->tabAccess['edit'])
						$this->_errors[] = $this->l('您沒有修改權限');
				break;
				case 'del':
					if (!$this->tabAccess['del'])
						$this->_errors[] = $this->l('您沒有刪除權限');
				break;
				case 'view':
				case 'excel':
				case 'excellist':
					if (!$this->tabAccess['view'])
						$this->_errors[] = $this->l('您沒有檢視權限');
				break;
			}
		}
		if (($_SESSION['post_code'] != $post_code) || empty($_SESSION['post_code'])) {
			if (!empty($post_code))
				$_SESSION['post_code'] = $post_code;
		} else {
			$this->action = null;
		}

		$sql = 'SELECT count(`'.$this->index.'`) AS num FROM `'.DB_PREFIX_.$this->table.'`';
		$row = Db::rowSQL($sql, true);
		$this->num = $row['num'];

		$this->context->smarty->assign([
			'menu_close' => ($_COOKIE['menu_close'] === 'true'),
			'display'    => $this->display,
		]);
	}

	public function displayAjax()
	{
		if ($this->json) {    //todo 不透過 layout-ajax.tpl, 以php直接輸出需要值
			header('Content-Type: application/json; charset=utf-8');
			$this->context->smarty->assign([
				'json'   => true,
				'status' => $this->status,
			]);
		}
		$this->layout                    = 'layout-ajax.tpl';
		$this->display_header            = false;
		$this->display_footer            = false;
		$this->display_header_javascript = false;
		return $this->display();
	}

	public function displayAjaxMenuClose()
	{
		$start = Tools::getValue('start');
		setcookie('menu_close', $start, time() + 3600, '/');
	}

	public function displayAjaxCalendarInputArray()
	{
		return $this->arr_calendar_input;

//		以下是樣式 請勿取消註解
//		return array(
//							//排程
//			'title'			=> '',		//標題
//			'constraint'		=> '',		//約數範圍(id, 'businessHours')
//							//範圍
//			'id'			=> '',		//範圍ID
//			'rendering'		=> '',
//			'overlap'		=> '',		//紅色區域(true, false)
//							//共用
//			'start'			=> '',		//起始時間
//			'end'			=> '',		//結束時間
//			'color'			=> ''		//顏色
//		);
	}

	public function getUpFileVal()
	{
		return $this->UpFileVal;
	}

	public function getUpFile($id)
	{
		return [];
	}

	/**
	 * 取得上傳檔案列表資料
	 */
	public function getUpFileList()
	{
		${$this->index} = Tools::getValue($this->index);
		$json           = new JSON();
		$file_up        = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
		$file_move      = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
		$file_download  = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
		$file_del       = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');

		if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
			$arr_file    = $this->getUpFile(${$this->index});
			$arr_id_file = [];

			foreach ($this->fields['form'] as $j => $jv) {
				foreach ($jv['input'] as $key => $v) {
					$this->UpFileVal[$key] = '\'' . $json->encode([
							$this->index => ${$this->index},
						]) . '\'';
					foreach ($arr_file as $i => $id_file) {
						$arr_id_file[] = $id_file;
					}
					$arr_row = FileUpload::get($arr_id_file);
					foreach ($arr_row as $i => $v) {
						if ($_SESSION['id_group'] == 1 || $file_download) {
							$url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
							$url = urldecode('http://' . WEB_DNS . $url);
						} else {
							$url = '';
						}

						$arr_initialPreview[$key][]       = $url;
						$file_type                        = est_to_type(ext($v['filename']));
						$arr_initialPreviewConfig[$key][] = [
							'type'        => $file_type,
							'filetype'    => $v['type'],
							'caption'     => $v['filename'],
							'size'        => $v['size'],
							'url'         => '/manage/' . $this->controller_name . '?&view' . $this->table . '&' . $this->index . '=' . ${$this->index} . '&ajax=1&action=DelFile',
							'downloadUrl' => $url,
							'key'         => $v['id_file'],
						];
					};
					if (count($arr_row)) {
						$arr_initialPreview[$key]       = $json->encode($arr_initialPreview[$key]);
						$arr_initialPreviewConfig[$key] = $json->encode($arr_initialPreviewConfig[$key]);
					}
				}
			}
		}

		$UpFileVal = $this->getUpFileVal();
		Context::getContext()->smarty->assign([
			'initialPreview'       => $arr_initialPreview,
			'initialPreviewConfig' => $arr_initialPreviewConfig,
			'UpFileVal'            => $UpFileVal,
		]);
	}

	function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
	{
		return true;
	}

	public function iniUpFileDir()
	{
		$dir = WEB_DIR . DS . FILE_DIR . DS . date('Y') . DS . date('m') . DS . date('d');
		return $dir;
	}

	public function iniUpFileUrl()
	{
		$url = FILE_URL . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
		return $url;
	}

	public function displayAjaxUpFile()
	{

//	    print_r($_FILES);

		$UpFileVal = Tools::getValue('UpFileVal');
		if ($_SESSION['id_group'] != 1 && GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up') == 0) {
			echo '{"error":"' . $this->l('您沒有檔案上傳的權限!') . '"}';
			exit;
		}
		if ($this->tabAccess['add']) {
			$input_name = Tools::getValue('input_name');
			$json       = new Services_JSON();
			if (empty($_FILES[$input_name])) {
				echo $json->encode(['error' => $this->l('沒有上傳任何文件!')]);  // or you can throw an exception
				return; // terminate
			}else if($_FILES[$input_name]["name"]["0"]=='blob'){
                //			    新增的 ($_FILES[$input_name]["name"]=='blob' && $_FILES[$input_name]["size"]=='261929') 防止上傳圖片錯誤傳送預設擋
                echo $json->encode(['error' => $this->l('沒有上傳任何文件!因為圖片不符合規定')]);  // or you can throw an exception
                return; // terminate
            }
			$images = $_FILES[$input_name];

// 狀態是否正常
			$success = null;

// 檔案路徑
			$paths     = [];
			$filenames = $images['name'];

			$arr_name = [];
			$arr_type = [];
			$arr_ext  = [];
			$arr_size = [];
			$dir      = $this->iniUpFileDir();

			add_dir($dir);
// loop and process files
			$url = $this->iniUpFileUrl();
//
//			echo 'dir :'.$dir.'</br>';
//            echo 'url :'.$url.'</br>';
//            echo 'count'.count($filenames).'</br>';
			$arr = [];
			for ($i = 0; $i < count($filenames); $i++) {
				$ext       = explode('.', basename($filenames[$i]));
//				echo 'check_name:'.$images['name'][$i] .' _ '.$dir.'</br>';
				$file_name = FileUpload::check_name($images['name'][$i], $dir);
				$target    = $dir . DS . $file_name;
				if (move_uploaded_file($images['tmp_name'][$i], $target)) {
					$success = true;
					$arr[]   = [
						'ext'   => array_pop($ext),
						'name'  => $file_name,
						'type'  => $images['type'][$i],
						'size'  => $images['size'][$i],
						//						'downloadUrl' => $url.$file_name,
						'paths' => $target,
					];
//存檔
				} else {
					//todo 單黨上傳 2019-12-18
					$ext       = explode('.', basename($filenames));
					$file_name = FileUpload::check_name($images['name'], $dir);
					$target    = $dir . DS . $file_name;

					if (move_uploaded_file($images['tmp_name'], $target)) {
						$success = true;
						$arr[]   = [
							'ext'   => array_pop($ext),
							'name'  => $file_name,
							'type'  => $images['type'],
							'size'  => $images['size'],
							//						'downloadUrl' => $url.$file_name,
							'paths' => $target,
						];
					} else {
						$success = false;
						break;
					}
				}
			}
			if ($success === false) {
				$output = ['error' => $this->l('沒有文件進行處理!')];
				echo $json->encode($output);
				exit;
			}
			if ($success === true) {
				if ($this->displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)) {
					$success = true;
					$output  = [];
				} else {
					$success = false;
				}
			}
			if ($success === false) {
				$output = ['error' => $this->l('上傳檔案時出錯!請聯繫系統管理員!')];
				foreach ($paths as $file) {
					unlink($file);
				}
			}
			echo $json->encode($output);
			exit;
		} else {
			echo '{"error":"' . $this->l('您沒有新增的權限!') . '"}';
			exit;
		}
	}

	public function displayAjaxMoveFileAction()
	{
		return true;
	}


	public function displayAjaxMoveFile()
	{
		if ($_SESSION['id_group'] != 1 && GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move') == 0) {
			echo '{"error":"' . $this->l('您沒有檔案移動的權限!') . '"}';
			exit;
		}
		if ($this->tabAccess['del']) {
			if ($this->displayAjaxMoveFileAction()) {
				$arr_id = Tools::getValue('arr_id');
				if (count($arr_id)) {
					foreach ($arr_id as $i => $id) {
						$sql = sprintf('UPDATE `file` SET `position` = %d
							WHERE `id_file` = %d',
							GetSQL($i, 'int'),
							GetSQL($id, 'int'));
						Db::rowSQL($sql);
					}
					echo '{"msg":"' . $this->l('更新成功!') . '"}';
					exit;
				}
			}
			echo '{"error":"' . $this->l('沒有移動任何資料!') . '"}';
			exit;
		} else {
			echo '{"error":"' . $this->l('沒有編輯的權限!') . '"}';
			exit;
		}
	}

	public function displayAjaxDelFileAction()
	{
		return true;
	}

	public function displayAjaxDelFile()
	{
		if ($_SESSION['id_group'] != 1 && GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del') == 0) {
			echo '{"error":"' . $this->l('您沒有檔案刪除的權限!') . '"}';
			exit;
		}
		if ($this->tabAccess['del']) {
			if ($this->displayAjaxDelFileAction()) {
				$id_file = Tools::getValue('key');
				$sql     = sprintf('SELECT `file_dir`, `filename` FROM `file`
					WHERE `id_file` = %d
					LIMIT 0, 1',
					GetSQL($id_file, 'int'));
				$row     = Db::rowSQL($sql, true);
				$id_file = Tools::getValue('key');
				$del_table = Tools::getValue('table');
				$num     = 0;
				if (count($row) > 0) {
				    $sql = "DELETE FROM `{$del_table}` WHERE `id_file` = ".GetSQL($id_file,"int");
                    Db::rowSQL($sql, false,0,$this->conn);
					unlink(urldecode($row['file_dir']) . DS . $row['filename']);
					$sql = sprintf('DELETE FROM `file` WHERE `id_file` = %d', GetSQL($id_file, 'int'));
                    Db::rowSQL($sql);
					$num += Db::getContext()->num;

					$sql = sprintf('DELETE FROM `file_in_type` WHERE `id_file` = %d', GetSQL($id_file, 'int'));
                    Db::rowSQL($sql);
					$num += Db::getContext()->num;
					if ($num) {
						echo '[]';
						exit;
					}
				}
			}
			echo $this->l('沒有刪除任何資料!');
			exit;
		} else {
			echo $this->l('沒有刪除的權限!');
			exit;
		}
	}

	public function displayAjaxCalendar()
	{
		$start    = Tools::getValue('start');
		$end      = Tools::getValue('end');
		$timezone = null;
		if (Tools::isSubmit('timezone'))
			$timezone = Tools::getValue('timezone');
		if (!isset($start) || !isset($end))
			die($this->l('請選擇日期範圍'));
		//解析開始/結束參數
		//這些假定是ISO8601字符串沒有時間也沒有時區，如“2013年12月29日”
		//由於沒有時區將出席，他們將解析為UTC

		if ($start > $end)
			die('[]');
		$range_start  = parseDateTime($start);
		$range_end    = parseDateTime($end);
		$input_arrays = $this->displayAjaxCalendarInputArray();
		//累積事件數據陣列的輸出數組
		$output_arrays = [];
		foreach ($input_arrays as $array) {

			//將輸入數組變成一個有用的事件對象
			$event = new Event($array, $timezone);

			//如果事件是在界，將它添加到輸出
			if ($event->isWithinDayRange($range_start, $range_end))
				$output_arrays[] = $event->toArray();
		}
		//發送JSON到客戶端
		echo json_encode($output_arrays);
	}

	public function displayValidateExcelImport()
	{

	}

	//---------------------------------------------
	//匯入EXCEL
	public function displayAjaxExcelImport()
	{
		if ($this->tabAccess['add']) {
			$json = new JSON();

			//取得資料
			$input_name              = Tools::getValue('input_name');
			$file                    = $_FILES[$input_name];
			$filenames               = $file['name'];
			$this->excel_import_data = (array)$json->decode(Tools::getValue('data'));

			//驗證
			if (empty($file)) {
				$this->_errors[] = $this->l('沒有上傳任何文件!');
			}

			if ($filenames != $this->fields['excel']['import']['file_name'] . '.xlsx') {
				$this->_errors[] = sprintf($this->l('上傳檔案錯誤!請使用 %s 檔'), $this->fields['excel']['import']['file_name'] . '.xlsx');
			}

			//EXCEL匯入自定驗證
			$this->displayValidateExcelImport();

			//沒有錯誤
			//開始取得EXCEL資料
			if (count($this->_errors) == 0) {
				//取得EXCEL資料

				$objPHPExcel = PHPExcel_IOFactory::load($file['tmp_name'], 'Excel2007');

				$this->excel_sheet_data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
				$this->validateExcelRules();
				if (count($this->excel_error_num) > 0 && $this->excel_num > 0) {    //有錯誤 有筆數
					foreach ($this->_excel_errors_key as $err_i => $excel) {
						$objPHPExcel->getActiveSheet()->getStyle($excel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);    //有問題的欄位背景改紅色
						$objPHPExcel->getActiveSheet()->getStyle($excel)->getFill()->getStartColor()->setARGB('FFFF0000');            //背景顏色
						$objPHPExcel->getActiveSheet()->getComment($excel)->getText()->createTextRun("\r\n");
						$objPHPExcel->getActiveSheet()->getComment($excel)->getText()->createTextRun($this->_excel_errors_txt[$excel]);        //插入註解
						$objPHPExcel->getActiveSheet()->getComment($excel)->setWidth('200pt');      //设置批注显示的宽高，在office中有效在wps中无效
						$objPHPExcel->getActiveSheet()->getComment($excel)->setHeight('200pt');
						$error_coordinate = $this->fields['excel']['import']['error_coordinate'];
						if (!empty($error_coordinate)) {
							$objPHPExcel->getActiveSheet()->setCellValue($error_coordinate . preg_replace('/\D/', '', $excel), $this->l('error'));
						}
					}

					//錯誤的EXCEL檔案存檔
					if (empty($this->fields['excel']['import']['error_file_name'])) {
						$this->fields['excel']['import']['error_file_name'] = now() . $this->fields['excel']['import']['file_name'];
					}
					$new_file_name = $this->fields['excel']['import']['error_file_name'] . '.xlsx';
					$objWriter     = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					add_dir($this->excel_import_result_dir);
					$objWriter->save($this->excel_import_result_dir . $new_file_name);
					$this->_errors[] = sprintf($this->l('有 %d 筆資料 %d 欄錯誤!請查看 錯誤結果： <a href=\"%s\" target=\"_blank\">%s</a>'), count($this->excel_error_num), count($this->_excel_errors_key), '/' . ADMIN_URL . '/' . $this->admin_url . '?downloadexcelimportresult' . $this->table . '&file=' . $new_file_name, $new_file_name);
				}
			}
			if (count($this->_errors)) {
				die('{"error":"' . implode('<br>', $this->_errors) . '"}');
			} else {
				$num = $this->processExcelImport();
				if ($num == 0) {
					die('{"error":"' . $this->l('匯入失敗!請聯絡系統管理者!') . '"}');
				}
				$this->_msgs[] = $this->l('匯入成功!');
				$this->_msgs[] = sprintf($this->l('成功匯入 %d 筆建案資料'), $this->excel_num);
				die('{"msg":"' . implode('\n', $this->_msgs) . '"}');
				return true;
			}
		} else {
			die('{"error":"' . $this->l('您沒有新增的權限') . '"}');
		}
	}

	//執行EXCEL匯入動作
	public function processExcelImport()
	{
		$table      = $this->fields['excel']['import']['table'];
		$arr_INSERT = [];    //要存入的欄位
		$arr_VALUES = [];    //要存入的資料欄位
		$VALUES     = '';

		if (count($this->fields['excel']['import']['insert'])) {        //指定欄位 (指定匯入整筆資料某一欄位數值)
			foreach ($this->fields['excel']['import']['insert'] as $name => $val) {
				$arr_INSERT[] = '`' . $name . '`';
			}
		}

		foreach ($this->fields['excel']['import']['definition'] as $excel => $v) {
			if (!empty($v['name'])) {        //有資料欄位名稱就加入新增位置
				$arr_INSERT[] = '`' . $v['name'] . '`';
				$arr_VALUES[] = $excel;
			}
		}

		foreach ($this->excel_sheet_data as $i => $v) {
			if (!empty($VALUES))
				$VALUES .= ', ';
			$VALUES                         .= '(';
			$arr_val                        = [];
			$this->excel_sheet_index_data[] = $v[$this->fields['excel']['import']['index']];
			foreach ($this->fields['excel']['import']['insert'] as $name => $val) {
				$arr_val[] = '"' . $val . '"';
			}
			foreach ($arr_VALUES as $j => $excel) {
				$arr_val[] = '"' . $v[$excel] . '"';
			}
			$VALUES .= implode(', ', $arr_val);
			$VALUES .= ')';
		}

		if (!empty($VALUES)) {    //新增匯入
			$sql = 'INSERT INTO `' . $table . '` (' . implode(', ', $arr_INSERT) . ') VALUES ' . $VALUES;
			Db::rowSQL($sql);
			return Db::getContext()->num();
			exit;
		}
		return false;
	}

	//下載匯入檔案
	public function displayDownloadExcelImport()
	{
		$fils = $this->excel_import_dir . 'excelimport.xlsx';
		if (filemtime($fils)) {
			print_file($fils, $this->fields['excel']['import']['file_name'] . '.xlsx');
		} else {
			gotourl('/找不到檔案');
		}

	}

	//下載匯入檔案
	public function displayDownloadExcelImportResult()
	{
		$file_name = Tools::getValue('file');
		$file      = $this->excel_import_result_dir . $file_name;
		if (filemtime($file)) {
			print_file($file, $file_name);
		} else {
			gotourl('/找不到檔案');
		}
	}

	public function displayDelExcelImport()
	{
		if ($this->tabAccess['edit']) {
			$file_name = Tools::getValue('file');
			$file      = $this->excel_import_result_dir . $file_name;
			if (!is_file($file) || !unlink($file)) {
				gotourl('/' . ADMIN_URL . '/' . $this->admin_url . '?excelimport' . $this->table . '&conf2=7');
			} else {
				gotourl('/' . ADMIN_URL . '/' . $this->admin_url . '?excelimport' . $this->table . '&conf=3');
			}
		} else {
			gotourl('/' . ADMIN_URL . '/' . $this->admin_url . '?excelimport' . $this->table . '&conf2=6');
		}
	}

	public function displayAjaxPosition()
	{
		if (!$this->tabAccess['edit'])
			die('{"error": "' . $this->l('您沒有修改權限') . '"}');
		if ($this->json) {
			$this->context->smarty->assign([
				'json'   => true,
				'status' => $this->status,
			]);
		}
		$arr    = [];
		$arr_id = Tools::getValue('id');
		if (is_array($arr_id)) {
			$num = 0;
			foreach ($arr_id as $i => $id) {
				$sql = sprintf('UPDATE `' . DB_PREFIX_ . $this->table . '` SET `position` = %d
					WHERE `' . $this->fields['index'] . '` = %d',
					GetSQL(($i + 1), 'int'),
					GetSQL($id, 'int'));
				Db::rowSQL($sql,false,0,$this->conn);
				$num += Db::getContext()->num();
			}
			if ($num)
				$arr['msg'] = $this->l('更新完成');
			$json = new Services_JSON();
			die($json->encode($arr));
		}
		exit;
	}

	public function processSave()
	{
		if (Tools::getValue($this->index) || empty($this->index)) {
			return $this->processEdit();
		} else {
			return $this->processAdd();
		}
	}

	//初始化麵包屑
	public function initBreadcrumbs($tab_id = null)
	{
		$breadcrumbs2 = [];
		if (is_null($tab_id))
			$tab_id = $this->id_tab;
		$tabs = Tab::getContext()->recursiveTab($tab_id);
		foreach ($tabs as $i => $v) {
			$this->breadcrumbs_txt[] = $this->l($v['title']);
			$url                     = str_replace('Controller', '', $this->controller_name);
			$url                     = str_replace('Admin', '', $url);
			$breadcrumbs2[]          = [
				'href'  => '/' . ADMIN_URL . '/' . $url . '?',
				'hint'  => $this->l($v['hint']),
				'icon'  => $v['icon'],
				'title' => $this->l($v['title']),
			];
		}
		switch ($this->display) {
			case 'add':
				$breadcrumbs2[] = [
					'href'  => '#',
					'icon'  => 'icon-plus',
					'title' => $this->l('新增'),
				];
			break;
			case 'edit':
				$breadcrumbs2[] = [
					'href'  => '#',
					'icon'  => 'icon-pencil',
					'title' => $this->l('修改'),
				];
			break;
			case 'view':
				$breadcrumbs2[] = [
					'href'  => '#',
					'icon'  => 'icon-zoom-in',
					'title' => $this->l('檢視'),
				];
			break;
			case 'options':
				$breadcrumbs2[] = [
					'href'  => '#',
					'icon'  => 'icon-th-list',
					'title' => $this->l('清單'),
				];
			break;
			case 'excel':
				$breadcrumbs2[] = [
					'href'  => '#',
					'icon'  => 'icon-th-list',
					'title' => $this->l(''),
				];
			case 'excelimport':
				$breadcrumbs2[] = [
					'href'  => '#',
					'icon'  => 'icon-th-list',
					'title' => $this->l('匯入Excel'),
				];
			default:
			case 'list':
			break;
		}
		//2020/10/12 新增 $this->breadcrumb_is_not ==1 去除 breadcrumbs部分
		$this->context->smarty->assign([
			'breadcrumbs1' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
			'breadcrumbs2' => $breadcrumbs2,
            'breadcrumb_is_not' => $this->breadcrumb_is_not,
		]);

	}

//初始化抬頭
	public function initHeader()
	{
		header('Content-Type:text/html; charset=utf-8');
		header('Cache-Control: no-store, no-cache');
		$this->context->smarty->assign([
			'tab_list'        => Tab::getContext()->getTabList(),
			'web_name'        => WEB_NAME,                //網站名稱
			'NAVIGATION_PIPE' => NAVIGATION_PIPE    //標題間格標示
		]);
	}

	public function postProcess()
	{                //POST處理
		$action = Tools::getValue('action');
		if ($this->ajax) {
// no need to use displayConf() here
			$this->action = $action;
			if (!empty($action) && method_exists($this, 'ajaxProcess' . $this->action)) {
//Hook::exec('actionAdmin'.ucfirst($this->action).'Before', array('controller' => $this));			//所有Admin 前動作
//Hook::exec('action'.get_class($this).ucfirst($this->action).'Before', array('controller' => $this));	//指定Class 後動作

				$return = $this->{'ajaxProcess' . $this->action}();

//Hook::exec('actionAdmin'.ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));			//所有Admin 前動作
//Hook::exec('action'.get_class($this).ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));	//指定Classn 後動作
				return $return;
			} elseif (!empty($action) && $this->controller_name == 'AdminModules' && Tools::getIsset('configure')) {
				$module_obj = Module::getContextByName(Tools::getValue('configure'));
				if (Validate::isLoadedObject($module_obj) && method_exists($module_obj, 'ajaxProcess' . $action)) {
					return $module_obj->{'ajaxProcess' . $action}();
				}
			} elseif (method_exists($this, 'ajaxProcess')) {
				return $this->ajaxProcess();
			}
		} else {
// If the method named after the action exists, call "before" hooks, then call action method, then call "after" hooks
			if (!empty($this->action) && method_exists($this, 'process' . ucfirst($this->action))) {
// Hook before action
//Hook::exec('actionAdmin'.ucfirst($this->action).'Before', array('controller' => $this));
//Hook::exec('action'.get_class($this).ucfirst($this->action).'Before', array('controller' => $this));
// Call process
				$return = $this->{'process' . ucfirst($this->action)}();
// Hook After Action
//Hook::exec('actionAdmin'.ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));
//Hook::exec('action'.get_class($this).ucfirst($this->action).'After', array('controller' => $this, 'return' => $return));
				return $return;
			}
		}
	}

	public function processAdd()
	{
		$this->validateRules();
		if($this->max > 0){
			if (($this->num >= $this->max)
				&& ($this->max !== null)) {
				$this->_errors[] = sprintf($this->l('最多只能新增 %d 筆資料!'), $this->max);
			}
		}
		if (count($this->_errors))
			return;
		$this->setFormTable();
		$num = $this->FormTable->insertDB();
		if ($num) {
			if (Tools::isSubmit('submitAdd' . $this->table)) {
				$this->back_url = self::$currentIndex . '&conf=1';
			}
			if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
				if (in_array('edit', $this->post_processing)) {
					$this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
				} else {
					$this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
				}
			}
			if (Tools::isSubmit('submitAdd' . $this->table . 'AndContinue')) {
				$this->back_url = self::$currentIndex . '&add' . $this->table . '&conf=1';
			}
			Tools::redirectLink($this->back_url);
		} else {
			$this->_errors[] = $this->l('沒有新增任何資料!');
		}
	}

	public function processDel()
	{
		if (count($this->fields['no_del']) && in_array(Tools::getValue($this->fields['index']), $this->fields['no_del'])) {
			$this->_errors[] = $this->l('無法刪除此資料');
		}
		if (count($this->_errors))
			return;
		$this->setFormTable();
		$num = $this->FormTable->deleteDB();
		if ($num && (Tools::isSubmit('submitDel' . $this->table) || Tools::isSubmit('del' . $this->table))) {
			$this->back_url = self::$currentIndex . '&conf=3';
		} elseif (Tools::isSubmit('submitDel' . $this->table) || Tools::isSubmit('del' . $this->table)) {
			$this->back_url = self::$currentIndex . '&conf2=1';
		} elseif ($num && Tools::isSubmit('submitDel' . $this->table . 'AndStay') || Tools::isSubmit('del' . $this->table)) {
			$this->back_url = self::$currentIndex . '&del' . $this->table . '&conf=3&' . $this->index . '=' . $this->FormTable->index_id;
		} elseif (Tools::isSubmit('submitDel' . $this->table . 'AndStay') || Tools::isSubmit('del' . $this->table)) {
			$this->back_url = self::$currentIndex . '&del' . $this->table . '&conf2=1&' . $this->index . '=' . $this->FormTable->index_id;
		}
		Tools::redirectLink($this->back_url);
	}

	public function processEdit()
	{
		$this->validateRules();
		//todo
		if (count($this->fields['no_edit']) && in_array(Tools::getValue($this->fields['index']), $this->fields['no_edit'])) {
			$this->_errors[] = $this->l('無法修改此資料');
		}
		if (count($this->_errors))
			return;
		$this->setFormTable();
		$num = $this->FormTable->updateDB();

		if (Tools::isSubmit('submitEdit' . $this->table)) {

			$this->back_url = self::$currentIndex . '&conf=2';
		}
		if (Tools::isSubmit('submitEdit' . $this->table . 'AndStay')) {
			if (in_array('edit', $this->post_processing)) {
				$this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=2&' . $this->index . '=' . $this->FormTable->index_id;
			} else {
				$this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=2&' . $this->index . '=' . $this->FormTable->index_id;
			}
		}
		Tools::redirectLink($this->back_url);
	}

//初始化內容
	public function initContent()
	{
		$this->getLanguages();
		$this->initToolbar();

		$this->initPageHeaderToolbar();
		if ($this->display == 'options') {
			$this->display_media_bak = false;
			if ($this->tabAccess['view'])
				$this->display = 'view';
			if ($this->tabAccess['edit'])
				$this->display = 'edit';
		}

		if ($this->display == 'edit' || $this->display == 'add') {
			$this->content .= $this->renderForm();    //啟用表單
		} elseif ($this->display == 'view') {        //檢視
			$this->content .= $this->renderView();
		} elseif ($this->display == 'details') {        //細節
			$this->content .= $this->renderDetails();
		} elseif ($this->display == 'calendar') {        //排程
			$this->content .= $this->renderCalendar();
		} elseif ($this->display == 'excellist') {        //Excel列表
			$this->content .= $this->renderExcelList();
		} elseif ($this->display == 'excel') {        //Excel
			$this->content .= $this->renderExcel();
		} elseif ($this->display == 'excelimport') {        //Excel
			$this->content .= $this->renderExcelImport();
		} elseif ($this->display == 'download_excel_import') {
			$this->displayDownloadExcelImport();
		} elseif ($this->display == 'download_excel_import_result') {        //下載EXCEL匯入錯誤結果
			$this->displayDownloadExcelImportResult();
		} elseif ($this->display == 'del_excel_import') {
			$this->displayDelExcelImport();
		} elseif (!$this->ajax) {
			$this->content .= $this->renderList();    //表單列表
		} elseif ($this->ajax) {
			$this->content .= $this->renderList();    //表單列表
		}
		$theme_color                           = Configuration::get('theme-color');
		$msapplication_navbutton_color         = Configuration::get('msapplication-navbutton-color');
		$apple_mobile_web_app_status_bar_style = Configuration::get('apple-mobile-web-app-status-bar-style');
		$this->context->smarty->assign([
			't_color'                     => $theme_color,
			'mn_color'                    => $msapplication_navbutton_color,
			'amwasb_style'                => $apple_mobile_web_app_status_bar_style,
			'title'                       => $this->page_header_toolbar_title,
			'page_header_toolbar_title'   => $this->page_header_toolbar_title,
			'toolbar_btn'                 => $this->toolbar_btn,
			'display_page_header_toolbar' => $this->display_page_header_toolbar,
		]);
		$this->context->smarty->assign([
			'breadcrumbs_htm' => $this->context->smarty->fetch('breadcrumbs.tpl'),
		]);

		$this->content .= $this->context->smarty->fetch('content.tpl');
		$this->context->smarty->assign([
			'page_header_toolbar' => $this->context->smarty->fetch('page_header_toolbar.tpl'),
			'content'             => $this->content,
		]);
	}

//取出驗證項目
	public function getValidationRules()
	{
		if (empty($this->definition)) {
			$this->definition = [];
			$arr_fields_form  = [$this->fields['form'], $this->fields['calendar']['before_form'], $this->fields['calendar']['after_form']];
			foreach ($arr_fields_form as $ffi => $f_form) {
				foreach ($f_form as $key => $form) {
					if (isset($form['input'])) {
						foreach ($form['input'] as $i => $v) {            //表單驗證
							if ($v['type'] != 'view') {
								if ($v['required']) {
									$this->definition[] = [
										'validate' => 'required',
										'name'     => $v['name'],
										'val'      => true,
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}
								if (isset($v['minlength']) && !isset($v['maxlength'])) {
									if ($v['type'] == 'change-password') {
										$this->definition[] = ['validate' => 'minlength', 'name' => $v['name'], 'val' => $v['minlength'], 'label' => $this->l('新密碼'), 'type' => $v['type']];
										$this->definition[] = ['validate' => 'minlength', 'name' => $v['name'] . '2', 'val' => $v['minlength'], 'label' => $this->l('確認密碼'), 'type' => $v['type']];
									} else {
										$this->definition[] = ['validate' => 'minlength', 'name' => $v['name'], 'val' => $v['minlength'], 'label' => $v['label'], 'type' => $v['type']];
									}
								}
								if (isset($v['maxlength']) && !isset($v['minlength'])) {
									if ($v['type'] == 'change-password') {
										$this->definition[] = ['validate' => 'maxlength', 'name' => $v['name'], 'val' => $v['minlength'], 'label' => $this->l('新密碼'), 'type' => $v['type']];
										$this->definition[] = ['validate' => 'maxlength', 'name' => $v['name'] . '2', 'val' => $v['minlength'], 'label' => $this->l('確認密碼'), 'type' => $v['type']];
									} else {
										$this->definition[] = ['validate' => 'maxlength', 'name' => $v['name'], 'val' => $v['maxlength'], 'label' => $v['label'], 'type' => $v['type']];
									}
								}
								if ((isset($v['minlength']) && isset($v['maxlength'])) || $v['rangelength']) {
									if ($v['type'] == 'change-password') {
										$this->definition[] = [
											'validate' => 'rangelength',
											'name'     => $v['name'],
											'val'      => ($v['rangelength']) ? $v['rangelength'] : $v['minlength'] . '-' . $v['maxlength'],
											'label'    => $this->l('新密碼'),
											'type'     => $v['type'],
										];
										$this->definition[] = [
											'validate' => 'rangelength',
											'name'     => $v['name'] . '2',
											'val'      => ($v['rangelength']) ? $v['rangelength'] : $v['minlength'] . '-' . $v['maxlength'],
											'label'    => $this->l('確認密碼'),
											'type'     => $v['type'],
										];
									} else {
										$this->definition[] = [
											'validate' => 'rangelength',
											'name'     => $v['name'],
											'val'      => ($v['rangelength']) ? $v['rangelength'] : $v['minlength'] . '-' . $v['maxlength'],
											'label'    => $v['label'],
											'type'     => $v['type'],
										];
									}
								}
								if (isset($v['min']) && !isset($v['max']))
									$this->definition[] = ['validate' => 'min', 'name' => $v['name'], 'val' => $v['min'], 'label' => $v['label'], 'type' => $v['type']];
								if (isset($v['max']) && !isset($v['min']))
									$this->definition[] = ['validate' => 'max', 'name' => $v['name'], 'val' => $v['max'], 'label' => $v['label'], 'type' => $v['type']];
								if ((isset($v['min']) && isset($v['max'])) || $v['range']) {
									$this->definition[] = [
										'validate' => 'range',
										'name'     => $v['name'],
										'val'      => ($v['range']) ? $v['range'] : $v['min'] . '-' . $v['max'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}
//								if(isset($v['step'])) $this->definition[]	= array('validate' => 'step',		'name' => $v['name'],	'val' => $v['step'],		'label' => $v['label'],	'type' => $v['type']);
								if ($v['validate'])
									$this->definition[] = ['validate' => 'validate', 'name' => $v['name'], 'val' => $v['validate'], 'label' => $v['label'], 'type' => $v['type']];    //指定
								if ($v['type'] == 'change-password') {    //更換密碼
									$this->definition[] = ['validate' => 'change-password', 'name' => $v['name'], 'val' => $v['name'], 'label' => $this->l('當前密碼'), 'type' => $v['type']];
								}
								if ($v['unique']) {            //唯一數值
									$this->definition[] = ['validate' => 'unique', 'name' => $v['name'], 'val' => $v['name'], 'label' => $v['label'], 'type' => $v['type']];
								}

								if ($v['type'] == 'datetime-local') {        //日期時間
									$this->definition[] = [
										'validate' => 'isdatetime',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if ($v['type'] == 'date') {        //日期
									$this->definition[] = [
										'validate' => 'isdate',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if ($v['type'] == 'time') {        //時間
									$this->definition[] = [
										'validate' => 'istime',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if ($v['type'] == 'email') {        //email
									$this->definition[] = [
										'validate' => 'isemail',
										'name'     => $v['name'],
										'val'      => $v['name'],
										'label'    => $v['label'],
										'type'     => $v['type'],
									];
								}

								if (isset($v['confirm'])) {    //確認密碼
									foreach ($form['input'] as $ji => $jv) {
										if ($jv['name'] == $v['confirm'])
											$label = $jv['label'];
									}
									$this->definition[] = ['validate' => 'confirm', 'name' => $v['name'], 'val' => $v['confirm'], 'label' => [$v['label'], $label], 'type' => $v['type']];
								}
							}
						}
					}
				}
			}
		}
		return $this->definition;
	}

	public function getExcelValidationRules()
	{
		if (empty($this->excel_definition)) {        //有無EXCEL驗證
			if (empty($this->fields['excel']['import']['definition'])) {    //有無自訂EXCEL驗證
				$this->copyFormValidationRules();
			}
			foreach ($this->fields['excel']['import']['definition'] as $excel => $v) {
				foreach ($v as $key => $jv) {
					switch ($key) {
						case 'values':    //選單類
							$arr = [];
							foreach ($v['values'] as $vi => $v_v) {
								$arr[$v_v['label']] = $v_v['value'];
							}
							$this->excel_definition[] = ['validate' => $key, 'key' => $excel, 'val' => $arr, 'default' => $v['val']];
						break;
						case 'table':        //選單類
						break;
						case 'options':    //選單類
						break;
						default:
							$this->excel_definition[] = ['validate' => $key, 'key' => $excel, 'val' => $jv];
						break;
					}
				}
			}
		}
		return $this->excel_definition;
	}

	public function copyFormValidationRules()
	{
		$arr_excel_validate_key = Validate::getContext()->arr_excel_validate_key;
		foreach ($this->fields['form'] as $fi => $form) {        //複製表單驗證
			foreach ($form['input'] as $fj => $fv) {
				if (isset($fv['excel']) && !empty($fv['excel'])) {    //判斷From 有沒有設定EXCEL位置
					$excel = $fv['excel'];
					foreach ($fv as $key => $v) {    //複製From表單驗證
						if (in_array($key, $arr_excel_validate_key)) {        //取出驗證表單所需資內容
							$this->fields['excel']['import']['definition'][$excel][$key] = $v;
						}
					}
				}
			}
		}
	}

	public function validateRules()
	{    //驗證
		if (method_exists($this, 'getValidationRules') && empty($this->definition)) {
			$this->definition = $this->getValidationRules();
		} else {
			$this->definition;
		}
		$Validate             = new Validate();
		$Validate->definition = $this->definition;
		$Validate->table      = $this->table;
		$Validate->index      = $this->index;
		$Validate->validateRules();
		$this->_errors        = array_merge($this->_errors, $Validate->_errors);
		$this->_errors_name   = array_merge($this->_errors_name, $Validate->_errors_name);
		$this->_errors_name_i = array_merge($this->_errors_name_i, $Validate->_errors_name_i);
	}

	public function validateExcelRules()
	{
		if (method_exists($this, 'getExcelValidationRules') && empty($this->excel_definition)) {
			$this->excel_definition = $this->getExcelValidationRules();
		} else {
			$this->excel_definition;
		}
		$this->excel_num = 0;
		$new_sheetData   = [];
		if (count($this->excel_sheet_data)) {
			foreach ($this->excel_sheet_data as $i => $v) {    //取出EXCEL每一行
				if ($i > $this->fields['excel']['import']['title_row']) {
					if (empty($v[$this->fields['excel']['import']['index']])) {        //判別資料筆數是否空白了	是:不用往下跑
						break;
					}
					foreach ($v as $excel => $vv) {
						$prefix = $this->fields['excel']['import']['definition'][$excel]['prefix'];
						$suffix = $this->fields['excel']['import']['definition'][$excel]['suffix'];
						if (!is_array($vv)) {
							if (!empty($prefix) || !empty($suffix)) {
								$v[$excel] = del_prefix_and_suffix($vv, $prefix, $suffix);
							}
							$v[$excel] = trim($v[$excel]);    //刪除前後空白
						} else {
							foreach ($vv as $x => $y) {
								if (!empty($prefix) || !empty($suffix)) {
									$vv[$x] = del_prefix_and_suffix($y, $prefix, $suffix);
								}
								$vv[$x] = trim($vv[$x]);    //刪除前後空白
							}
							$v[$excel] = $vv;
						}
					}
					$Validate                   = new Validate;
					$Validate->excel_data       = $v;    //取一整列的值
					$Validate->excel_this_row   = $i;
					$Validate->excel_definition = $this->excel_definition;
					$Validate->validateExcelRules();
					$new_sheetData[]         = $Validate->excel_data;
					$this->_excel_errors     = array_merge($this->_excel_errors, $Validate->_excel_errors);
					$this->_excel_errors_txt = array_merge($this->_excel_errors_txt, $Validate->_excel_errors_txt);    //錯誤訊息(註解)
					$this->_excel_errors_key = array_merge($this->_excel_errors_key, $Validate->_excel_errors_key);

					if (count($Validate->_excel_errors_key)) {
						$this->excel_error_num[$i] = 1;
					}
					$this->excel_num++;
				}
			}
		}
		//驗證EXCEL資料
		if (empty($this->excel_num)) {
			$this->_errors[] = $this->l('EXCEL沒有資料筆數!');
		}
		$this->_excel_errors_key = array_unique($this->_excel_errors_key);        //去除重複
		$this->excel_sheet_data  = $new_sheetData;

	}

	public function initFooter()
	{
		$this->context->smarty->assign('css_files', $this->css_files);
		$this->context->smarty->assign('js_files', array_unique($this->js_files));
	}

	public function redirect()
	{

	}

	public function renderForm($setFormType = true)
	{
		if (count($this->fields['tabs']) == 1) {
			unset($this->fields['tabs']);
		}
		if ($setFormType) {
			$this->setFormTable();
		}
		$this->FormTable->renderForm();
		$Context = Context::getContext();

		$this->_errors                     = array_merge($this->_errors, $this->FormTable->_errors);
		$this->display_page_header_toolbar = $this->FormTable->display_page_header_toolbar;

		$Context->smarty->assign([
			'back_url'                    => $this->back_url,
			'fields'                      => $this->FormTable->fields,
			'submit_display'              => ucfirst($this->FormTable->display),
			'submit_action'               => $this->FormTable->submit_action,
			'show_cancel_button'          => $this->show_cancel_button,
			'from_error'                  => implode('<br>', $this->_errors),
			'display_page_header_toolbar' => $this->FormTable->display_page_header_toolbar,
			'arr_take_photo_data'         => $this->FormTable->arr_take_photo_data,
			'arr_signature_data'          => $this->FormTable->arr_signature_data,
		]);
		if (count($this->FormTable->_errors) == 0) {
			return $Context->smarty->fetch('form' . DS . 'form_form.tpl');
		}
		return false;
	}

	public function renderView()
	{
		$this->setFormTable();
		$this->FormTable->ini();
		foreach ($this->fields['form'] as $key2 => $v2) {
			if ($key2 != 'class') {
				unset($this->fields['form'][$key2]['submit']);
				unset($this->fields['form'][$key2]['cancel']);
				unset($this->fields['form'][$key2]['reset']);
				if (isset($this->fields['form'][$key2]['input'])) {
					foreach ($this->fields['form'][$key2]['input'] as $key => $v) {
						if ($v['type'] == 'checkbox')
							$this->fields['form'][$key2]['input'][$key]['multiple'] = true;

						if ($v['type'] != 'hidden' && $v['type'] != 'tpl' && $v['type'] != 'hr' && $v['type'] != 'color' && $v['type'] != 'confirm-password' && $v['type'] != 'change-password' && $v['type'] != 'html' && $v['type'] != 'file' && $v['type'] != 'map') {
							if ($v['type'] == 'take_photo' || $v['type'] == 'signature') {
								$this->fields['form'][$key2]['input'][$key]['key']['type'] = 'view';
							} else {
								$this->fields['form'][$key2]['input'][$key]['type'] = 'view';
							}
							//'date' || $input.type == 'datetime' || $input.type == 'datetime-local
						} else {
							$this->fields['form'][$key2]['input'][$key]['disabled'] = true;
						}
					}
				}
			}
		}
		return $this->renderForm();
	}

	public function renderCalendar()
	{
		if (!($this->fields['calendar'] && is_array($this->fields['calendar'])))
			return false; //有宣告fields['calendar'] 排程顯示
		if (!is_array($this->bulk_actions)) {
			$this->bulk_actions = [];    //批量操作
		}
		unset($this->fields['form']);        //移除表單格式

		$this->setFormTable();
		$html = '';
		if (isset($this->fields['calendar']['before_form']) && !empty($this->fields['calendar']['before_form'])) {
			$this->FormTable->fields['form'] = $this->fields['calendar']['before_form'];
			$html                            .= $this->renderForm(false);
		}

		$this->FormTable->renderCalendar();
		$html .= Context::getContext()->smarty->fetch('form' . DS . 'form_calendar.tpl');

		if (isset($this->fields['calendar']['after_form']) && !empty($this->fields['calendar']['after_form'])) {
			$this->FormTable->fields['form'] = $this->fields['calendar']['after_form'];
			$html                            .= $this->renderForm(false);
		}
		return $html;
	}

	public function renderList()
	{
		if (!isset($this->fields['field_display']))
			$this->fields['field_display'] = true;    //是否開啟自訂顯示
		if ($this->fields['field_display']) {
			$MyFieldLst = $this->getMyFieldLst();
			foreach ($this->fields['list'] as $key => $v) {
				if (empty($v['hidden'])) {
					if (isset($MyFieldLst[$key])) {    //自訂
						$this->fields['list'][$key]['show'] = $MyFieldLst[$key];
						$show                               = $MyFieldLst[$key];
					} elseif (isset($v['show'])) {            //預設
						$show = $v['show'];
					} else {
						$show = 1;
					}
					$this->fields['all_field_list'][$key] = [
						'title' => $v['title'],
						'show'  => $show,
					];
				}
			}
		}
		foreach ($this->fields['list'] as $i => $v) {
			if (!isset($this->fields['list'][$i]['show']))
				$this->fields['list'][$i]['show'] = true;    //預設顯示
			if (empty($this->fields['list'][$i]['show'])) {        //沒有顯示
				unset($this->fields['list'][$i]);        //移除
			}
		}
		if (!($this->fields['list'] && is_array($this->fields['list'])))
			return false; //有宣告fields['list'] 代表是列表
//判別列表內有無 active 啟用
		if (!is_array($this->bulk_actions)) {
			$this->bulk_actions = [];    //批量操作
		}
		//取出標題
		//判斷使否有檢視後處理
		if (count($this->post_processing) && (in_array('view', $this->actions))) {
			//列出所有權限
			foreach ($this->actions as $i => $action) {
				if (in_array($action, $this->post_processing) && $action != 'view') {
					unset($this->actions[$i]);
				}
			}
		}
		if (!empty($this->fields['parent_key']) && !empty($this->fields['parent_key'])) {
			if (Tools::getValue($this->fields['index'])) {
				$this->fields['where'] .= ' AND `' . $this->fields['parent_key'] . '` = ' . Tools::getValue($this->fields['index']);
				$sql                   = sprintf('SELECT `' . $this->fields['title_key'] . '` FROM `' . $this->table . '` WHERE `' . $this->fields['index'] . '` = %d LIMIT 0, 1',
					GetSQL(Tools::getValue($this->fields['index']), 'int'));
				$row                   = Db::rowSQL($sql, true);
				$this->fields['title'] = $row[$this->fields['title_key']];
			} else {
				$this->fields['where'] .= ' AND (`' . $this->fields['parent_key'] . '` IS NULL OR `' . $this->fields['parent_key'] . '` = "")';
			}
		}
		if (isset($this->fields['list']) && is_array($this->fields['list']) && array_key_exists('active', $this->fields['list']) && !empty($this->fields['list']['active'])) {

			//列表點擊立即啟用/關閉
			/*
			$this->bulk_actions = array_merge(array(
			'divider' => array(
			'text' => '批次啟用關閉'
			),
			'enableSelection' => array(
			'text' => $this->l('啟用'),
			'icon' => 'icon-power-off text-success'
			),
			'disableSelection' => array(
			'text' => $this->l('關閉'),
			'icon' => 'icon-power-off text-danger'
			)
			), $this->bulk_actions);
			*/

		}
		//
		//待修正
		//最高系統管理者
		if ($_SESSION['id_group'] == 1 && false) {
			//顯示ID邊號
			foreach ($this->fields['list'] as $i => $v) {
				if ($v['index']) {
					$arr = [$i => $this->fields['list'][$i], 'ID' => $this->fields['list'][$i]];
					if (!empty($this->_as)) {
						$arr['ID']['filter_key'] = $this->_as . '!' . $i;
					} else {
						$arr['ID']['filter_key'] = $i;
					}
					if (!isset($arr['ID']['title']) || $_SESSION['id_group'] == 1)
						$arr['ID']['title'] = $this->l('ID');

					//系統管理者 ID 不隱藏
					if ($_SESSION['id_group'] == 1) {
						unset($arr['ID']['hidden']);
						unset($arr['ID']['type']);
						unset($arr[$i]['hidden']);
						$arr[$i]['type'] = 'checkbox';
					}

					unset($this->fields['list'][$i]);
					$arr                  = array_merge($arr, $this->fields['list']);
					$this->fields['list'] = $arr;
					break;
				}
			}
		}


		$this->setFormTable();
//		FormTable::getContext()->fields = $this->fields;

		$this->FormTable->renderList();

		if (count($this->FormTable->_errors) == 0) {
		

			$Context = Context::getContext();
			$Context->smarty->assign([
					'fields'           => $this->FormTable->fields,
					'has_actions'      => !empty($this->actions),
					'actions'          => $this->actions,
					'post_processing'  => $this->post_processing,
					'display_edit_div' => $this->display_edit_div,
					'bulk_actions'     => $this->bulk_actions,
					'no_information'   => $this->no_information,
				]
			);

			$Context->smarty->assign([
				'form_list_field_list' => $Context->smarty->fetch('form' . DS . 'form_list_field_list.tpl'),
				'form_list_thead'      => $Context->smarty->fetch('form' . DS . 'form_list_thead.tpl'),
				'form_list_tbody'      => $Context->smarty->fetch('form' . DS . 'form_list_tbody.tpl'),
				'form_list_tfoot'      => $Context->smarty->fetch('form' . DS . 'form_list_tfoot.tpl'),
				'form_list_pagination' => $Context->smarty->fetch('form' . DS . 'form_list_pagination.tpl'),
			]);
			return $Context->smarty->fetch('form' . DS . 'form_list.tpl');
		}
		return false;
	}

	//取得我的顯是隱藏欄位
	public function getMyFieldLst($id_admin = null, $page = null)
	{
		if (empty($id_admin))
			$id_admin = $_SESSION['id_admin'];
		if (empty($page))
			$page = $this->page;

		$json = new JSON();

		$sql        = sprintf('SELECT `field`
			FROM `show_list`
			WHERE `id_admin` = %d
			AND `page` = %s
			LIMIT 0, 1',
			GetSQL($id_admin, 'int'),
			GetSQL($page, 'text'));
		$row        = Db::rowSQL($sql, true);
		$field_list = (array)$json->decode($row['field']);
		return $field_list;
	}

	//修改我的顯是隱藏欄位
	public function processSetMyFieldList()
	{
		$field_list     = Tools::getValue('field_name', []);
		$field_name_val = Tools::getValue('field_name_val', []);
		if (!is_array($field_list)) {
			$field_list = [];
		}
		$arr_field_list = [];
		foreach ($field_list as $i => $v) {
			if (in_array($v, $field_name_val)) {
				$field_list[$v] = 1;
			} else {
				$field_list[$v] = 0;
			}
		}
		$this->setMyFieldList($_SESSION['id_admin'], $this->page, $field_list);
	}

	//修改我的顯是隱藏欄位
	public function setMyFieldList($id_admin = null, $page = null, $field_list = [])
	{
		if (empty($id_admin))
			$id_admin = $_SESSION['id_admin'];
		if (empty($page))
			$page = $this->page;
		$json       = new JSON();
		$field_list = $json->encode($field_list);
		$sql        = sprintf('SELECT `id_show_list`
			FROM `show_list`
			WHERE `id_admin` = %d
			AND `page` = %s
			LIMIT 0, 1',
			GetSQL($id_admin, 'int'),
			GetSQL($page, 'text'),
			GetSQL($field_list, 'text'));
		$row        = Db::rowSQL($sql, true);

		if ($row['id_show_list']) {
			$sql = sprintf('UPDATE `show_list`
				SET `field` = %s
				WHERE `id_admin` = %d
				AND `page` = %s',
				GetSQL($field_list, 'text'),
				GetSQL($id_admin, 'int'),
				GetSQL($page, 'text'));
			Db::rowSQL($sql);
			return Db::getContext()->num;
		} else {
			$sql = sprintf('INSERT INTO `show_list` (`id_admin`, `page`, `field`)
				VALUES (%d, %s, %s)',
				GetSQL($id_admin, 'int'),
				GetSQL($page, 'text'),
				GetSQL($field_list, 'text'));
			Db::rowSQL($sql);
			return Db::getContext()->num;
		}
	}

	//刪除我的顯是隱藏欄位
	public function processdelMyFieldList()
	{
		$this->setMyFieldList($_SESSION['id_admin'], $this->page);
	}

	//刪除我的顯是隱藏欄位
	public function delMyFieldList($id_admin = null, $page = null)
	{
		if (empty($id_admin))
			$id_admin = $_SESSION['id_admin'];
		if (empty($page))
			$page = $this->page;
		$sql = sprintf('DELETE FROM `show_list`
			WHERE `id_admin`= %d
			AND `page` = %s',
			GetSQL($id_admin, 'int'),
			GetSQL($page, 'text'));
		Db::rowSQL($sql);
	}

	public function renderExcel()
	{
		if (empty($this->fields['excel']['file'])) {
			if (filemtime(OVERRIDE_DIR . DS . 'admin' . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excel.xlsx')) {
				$this->fields['excel']['file'] = OVERRIDE_DIR . DS . 'admin' . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excel.xlsx';
			} elseif (filemtime(ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excel.xlsx')) {
				$this->fields['excel']['file'] = ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excel.xlsx';
			}
		}
		if (empty($this->fields['excel']['title']))
			$this->fields['excel']['title'] = $this->fields['title'];
		$this->display                 = 'excel';
		$this->fields['show_list_num'] = false;
		$this->display_edit_div        = false;
		$this->setFormTable();
		$this->FormTable->renderExcel();
		exit;
	}

	public function renderExcelImport()
	{

		$this->fields['tabs'] = [];
		$this->fields['form'] = $this->fields['excel']['import']['from'];
		foreach ($this->fields['form'] as $i => $v) {
			$this->fields['tabs'][$v['tab']] = $v['legend']['title'];
		}
		$this->setFormTable();
		$html                              = $this->FormTable->renderForm();
		$this->_errors                     = array_merge($this->_errors, $this->FormTable->_errors);
		$this->display_page_header_toolbar = $this->FormTable->display_page_header_toolbar;
		return $html;
	}

	public function renderExcelList()
	{
		if (empty($this->fields['excel']['file'])) {
			if (filemtime(OVERRIDE_DIR . DS . 'admin' . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excellist.xlsx')) {
				$this->fields['excel']['file'] = OVERRIDE_DIR . DS . 'admin' . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excellist.xlsx';
			} elseif (filemtime(ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excellist.xlsx')) {
				$this->fields['excel']['file'] = ADMIN_THEME_DIR . $this->admin_theme . DS . 'templates' . DS . 'controllers' . DS . $this->controller_name . DS . 'excellist.xlsx';
			}
		}
		if (empty($this->fields['excel']['title'])) {
			$this->fields['excel']['title'] = $this->fields['title'];
		}
		$this->display                 = 'excel';
		$this->fields['show_list_num'] = false;
		$this->display_edit_div        = false;
		if (!isset($this->fields['excel']['show_list_title'])) {
			$this->fields['excel']['show_list_title'] = true;
		}
		$this->setFormTable();
		$this->FormTable->renderExcelList();
		exit;
	}

	public function setFormTable()
	{
		if (empty($this->FormTable)) {
			$this->FormTable = new FormTable();
		}
		$this->FormTable->table            = $this->table;
		$this->FormTable->_join            = $this->_join;
		$this->FormTable->_group           = $this->_group;
		$this->FormTable->_as              = $this->_as;
		$this->FormTable->list_id          = $this->list_id;
		$this->FormTable->actions          = $this->actions;
		$this->FormTable->tabAccess        = $this->tabAccess;
		$this->FormTable->fields           = $this->fields;
		$this->FormTable->display          = $this->display;
		$this->FormTable->post_processing  = $this->post_processing;
		$this->FormTable->submit_action    = $this->submit_action;
		$this->FormTable->bulk_actions     = $this->bulk_actions;
		$this->FormTable->display_edit_div = $this->display_edit_div;
        $this->FormTable->conn = $this->conn;
	}

//初始化工具列 並加入標題
	public function initToolbarTitle()
	{
		$this->toolbar_title = is_array($this->breadcrumbs_txt) ? array_unique($this->breadcrumbs_txt) : [$this->breadcrumbs_txt];    //麵包屑 移除重複
		switch ($this->display) {
			case 'edit':
				$this->toolbar_title[] = $this->l('修改', null, null, false);
				$this->addMetaTitle($this->l('修改', null, null, false));
			break;
			case 'add':
				$this->toolbar_title[] = $this->l('新增', null, null, false);
				$this->addMetaTitle($this->l('新增', null, null, false));
			break;
			case 'view':
				$this->toolbar_title[] = $this->l('檢視', null, null, false);
				$this->addMetaTitle($this->l('檢視', null, null, false));
			break;
			case 'options':
				$this->toolbar_title[] = $this->l('', null, null, false);
				$this->addMetaTitle($this->l('設定', null, null, false));
				/*
				$this->toolbar_btn['save'] = array(
					'href' => '#',
					'desc' => $this->l('Save')
				);
				*/
			break;
			case 'excel':
				$this->toolbar_title[] = $this->l('匯出Excel', null, null, false);
				$this->addMetaTitle($this->l('匯出Excel', null, null, false));
			break;
			case 'excelimport':
				$this->toolbar_title[] = $this->l('匯入Excel', null, null, false);
				$this->addMetaTitle($this->l('匯入Excel', null, null, false));
			break;
		}
	}

	public function initPageHeaderToolbar()
	{
		if (empty($this->toolbar_title)) {
			$this->initToolbarTitle();
		}

		if (!is_array($this->toolbar_title)) {
			$this->toolbar_title = [$this->toolbar_title];
		}
		if (empty($this->page_header_toolbar_title)) {    //標題
			$this->page_header_toolbar_title = $this->toolbar_title[count($this->toolbar_title) - 1];
		}
	}

//加入標題
	public function addMetaTitle($entry)
	{
		if (is_array($this->meta_title)) {
			$this->meta_title[] = $entry;
		}
	}

	public function display()
	{
		$meta_viewport = '';
		if (count($this->meta_viewport)) {
			$meta_viewport = [];
			foreach ($this->meta_viewport as $key => $v) {
				$meta_viewport[] = $key . '=' . $v;
			}
			$meta_viewport = implode(', ', $meta_viewport);
		}
		$this->context->smarty->assign([
			'meta_viewport' => $meta_viewport,
		]);
		$this->context->smarty->assign([
			'display_media_bak' => $this->display_media_bak,
		]);
//宣告顯示
		$this->context->smarty->assign([
			'display_header'            => $this->display_header,
			'display_header_javascript' => $this->display_header_javascript,
			'display_footer'            => $this->display_footer,
		]);
//標題
		if (!$this->meta_title) {
			$this->meta_title = $this->toolbar_title;
		}
		if (is_array($this->meta_title)) {    //標題是陣列
			$this->meta_title = strip_tags(implode(NAVIGATION_PIPE, $this->meta_title));
		}
		/*
		 * 資料夾優先權要改
		 */
		$this->context->smarty->assign('meta_title', $this->meta_title);
//樣版資料夾(組)
		$template_dirs = $this->context->smarty->getTemplateDir();
		$dir           = $this->context->smarty->getTemplateDir(0);    //樣版資料夾位置
		$module_dir    = $this->context->smarty->getTemplateDir(0) . 'modules' . DS;
//模組樣版資料夾位置
//各樣版位置
		$minbar_tpl          = 'minbar.tpl';
		$header_tpl          = 'header.tpl';
		$page_header_toolbar = 'page_header_toolbar.tpl';
		$footer_tpl          = 'footer.tpl';
		$modal_module_list   = 'modal_lise.tpl';
		$page_tpl            = 'page.tpl';
		$tpl_action          = $this->tpl_folder . DS . $this->display . '_content.tpl';    //動做頁面

		$msg_box = 'msg_box.tpl';
//訊息處理
		if ($conf = Tools::getValue('conf')) {
			$this->_msgs[] = $this->_conf[$conf];
		}
		if ($conf2 = Tools::getValue('conf2')) {
			$this->_errors[] = $this->_conf2[$conf2];
		}

//確認動作後樣版

		foreach ($template_dirs as $template_dir) {
			if (file_exists($template_dir . 'controllers' . DS . $tpl_action) && $this->display != 'view' && $this->display != 'options') {    //不是顯示、選項
				if (method_exists($this, $this->display . $this->className)) {        //是否有Class function
					$this->{$this->display . $this->className}();    //執行Class
				}
				$this->context->smarty->assign('content', $this->context->smarty->fetch('controllers' . DS . $tpl_action));    //中間樣版
				break;
			}
		}

		$this->context->smarty->assign([
			'admin_menu' => $this->context->smarty->fetch('admin_menu.tpl'),
		]);

		$this->context->smarty->assign([
			'front_css'       => ['//' . THEME_URL . '/css/reset.css', '//' . THEME_URL . '/css/main.css'],
			'THEME_URL'       => THEME_URL,
			'ADMIN_THEME_URL' => ADMIN_THEME_URL,
			'css_files'       => $this->css_files,
			'js_files'        => $this->js_files,
			'_msg'            => implode('<br>', array_unique($this->_msgs)),
			'_error'          => implode('<br>', array_unique($this->_errors)),
			'_errors_name'    => $this->_errors_name,
			'_errors_name_i'  => $this->_errors_name_i,
		]);

		$this->context->smarty->assign([
			'msg_box' => $this->context->smarty->fetch($msg_box),
			'minbar'  => $this->context->smarty->fetch($minbar_tpl),
		]);

		global $start_time;
		$ent_time = microtime(true);
		$this->context->smarty->assign([
			'start_time'     => $start_time,
			'ent_time'       => $ent_time,
			'execution_time' => round($ent_time - $start_time, 4),
		]);
		$this->context->smarty->assign([
			'header' => $this->context->smarty->fetch($header_tpl),
			'page'   => $this->context->smarty->fetch($page_tpl),    //未知
			'footer' => $this->context->smarty->fetch($footer_tpl),
		]);

//輸出原始碼
		$this->smartyOutputContent($this->layout);
	}

	/**
	 *    設定沒有權限頁面
	 */
	public function initRefusePage()
	{
		$this->layout = '';
	}

//觀看權限
	public function viewAccess($disable = false)
	{
		if ($disable)
			return true;
		if ($this->tabAccess['view'] == 1)
			return true;
		return false;
	}

	public function checkAccess()
	{
		return true;
	}

	public function setMedia()
	{

		if (is_file(CACHE_DIR . DS . 'css_index.php') && count($this->context->css_list) == 0)
			$this->context->css_list = include_once(CACHE_DIR . DS . 'css_index.php');
		if (is_file(CACHE_DIR . DS . 'js_index.php') && count($this->context->js_list) == 0)
			$this->context->js_list = include_once(CACHE_DIR . DS . 'js_index.php');

		$this->addCSS($this->context->css_list['reset']);
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-3.3.7/css/bootstrap.min.css');
		$this->addCSS('/' . MEDIA_URL . '/jquery.growl-1.3.2/jquery.growl.css');
		$this->addCSS('/' . MEDIA_URL . '/jquery-ui-1.12.0/jquery-ui.min.css');
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-3.3.7/css/bootstrap-theme.min.css');
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-select-1.11.2/css/bootstrap-select.min.css');
		$this->addCSS('/' . MEDIA_URL . '/fontawesome-free-5.1.0-web/css/all.css');



		$arr_file = [];
		// todo:快取分割手機跟電腦版

		if (!is_file(CACHE_DIR . DS . 'admin_css_index.php')) {
			//手機字體	有些字體在手機(IOS會閃爍)
			if (check_mobile()) {
				if (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'mobile_font.min.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile_font.min.css');
					$arr_file['mobile_font.min'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile_font.min.css';
				} elseif (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'mobile_font.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile_font.css');
					$arr_file['mobile_font'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile_font.css';
				}
			} else {
				if (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'not_mobile_font.min.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile_font.min.css');
					$arr_file['not_mobile_font.min'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile_font.min.css';
				} elseif (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'not_mobile_font.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile_font.css');
					$arr_file['not_mobile_font'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile_font.css';
				}
			}
		}

		$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/' . $this->admin_css);
		// TODO:快取分割手機跟電腦版
		if (!is_file(CACHE_DIR . DS . 'admin_css_index.php')) {
			//手機字體	有些字體在手機(IOS會閃爍)
			if (check_mobile()) {
				if (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'mobile.min.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile.min.css');
					$arr_file['mobile.min'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile.min.css';
				} elseif (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'mobile.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile.css');
					$arr_file['mobile'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/mobile.css';
				}
			} else {
				if (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'not_mobile.min.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile.min.css');
					$arr_file['not_mobile.min'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile.min.css';
				} elseif (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . 'not_mobile.css')) {
					$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile.css');
					$arr_file['not_mobile'] = '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/not_mobile.css';
				}
			}
		}

		if (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'css' . DS . strtolower($this->controller_name) . '.css')) {
			$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/' . strtolower($this->controller_name) . '.css');
		}

		$this->addJquery();
		$this->addjQueryPlugin();
		$this->addJqueryUI();
		$this->addJS($this->context->js_list['jquery-1.12.4.min']);
		$this->addJS('/' . MEDIA_URL . '/jquery-ui-1.12.0/jquery-ui.min.js');
		$this->addJS($this->context->js_list['url.min']);

		$this->addJS('/' . MEDIA_URL . '/jquery.tablednd/jquery.tablednd.1.0.3.min.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.11.2/js/bootstrap-select.min.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.11.2/js/i18n/defaults-zh_TW.min.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-3.3.7/js/bootstrap.min.js');


		$fileinput = false;
		foreach ($this->fields['form'] as $key => $form) {
			foreach ($form['input'] as $i => $v) {            //表單驗證
				switch ($v['type']) {
					case 'signature':    //簽名
						$this->addCSS('/' . MEDIA_URL . '/jSignature/css/jSignature.css');
						$this->addJS('/' . MEDIA_URL . '/jSignature/js/jSignature.min.js');
						$this->addJS('/' . MEDIA_URL . '/jSignature/js/jSignature.min.noconflict.js');
						$this->addJS($this->context->js_list['screenfull.min']);
					break;
					case 'take_photo':    //拍照
						$this->addCSS('/' . MEDIA_URL . '/webcam/css/webcam.css');
						$this->addJS('/' . MEDIA_URL . '/webcam/js/mp3.js');
						$this->addJS('/' . MEDIA_URL . '/webcam/js/webcam.min.js');
					break;
					case 'file':            //檔案上傳
						$this->addCSS('/' . MEDIA_URL . '/bootstrap-fileinput-master/css/fileinput.css');
						$this->addJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/plugins/sortable.min.js');
						$this->addJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/plugins/purify.min.js');
						$this->addJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/fileinput.min.js');
						$this->addJS('/' . MEDIA_URL . '/bootstrap-fileinput-master/js/locales/zh-TW.js');
					break;
					case 'map':        //地圖
						$this->addJS('https://maps.googleapis.com/maps/api/js?key=' . Config::get('GoogleAPIKey') . '&libraries=geometry&sensor=false');
						$this->addJS('/' . MEDIA_URL . '/google-map/js/google_map.js');
					break;
				}
			}
		}

		$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/jquery.validate.min.js');
		$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/additional-methods.min.js');
		$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/localization/messages_zh_TW.js');
		$this->addJS('/' . MEDIA_URL . '/jquery.growl-1.3.2/jquery.growl.js');
		$this->addJS('/' . MEDIA_URL . '/tinymce-5.1.1/tinymce.min.js');

		$this->addJS($this->context->js_list['model']);

		$this->addJS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/js/' . $this->admin_js);
		if (is_file(ADMIN_THEME_DIR . $this->admin_theme . DS . 'js' . DS . strtolower($this->controller_name) . '.js')) {
			$this->addJS('//' . WEB_DNS . '/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/js/' . strtolower($this->controller_name) . '.js');
		}

		if ($this->display == 'calendar') {
			$this->addCSS('/' . MEDIA_URL . '/fullcalendar-3.0.1/fullcalendar.min.css');
			$this->addJS('/' . MEDIA_URL . '/fullcalendar-3.0.1/lib/moment.min.js');
			$this->addJS('/' . MEDIA_URL . '/fullcalendar-3.0.1/fullcalendar.min.js');
			$this->addJS('/' . MEDIA_URL . '/fullcalendar-3.0.1/locale/' . Configuration::get('WEB_LANGIAGE') . '.js');
		}

		Hook::exec('Admin_setMedia');

		$FrontController = FrontController::getContext();
		$FrontController->setMedia();
		$json = new JSON();

		$this->context->smarty->assign([
			'front_css_files' => $json->encode($FrontController->css_files),
		]);

		$this->meta_viewport = [
			'width'         => 'device-width',
			'initial-scale' => '1',
			'maximum-scale' => '1',
			'user-scalable' => '1',
		];
	}

	public function getLanguages()
	{
		return $this->_languages;
	}

	//出使化 功能按鈕
	public function initToolbar()
	{
		$url = '';
		foreach ($_GET as $k => $v) {
			$url .= '&' . $k . '=' . $v;
		}

		if (empty($this->back_url))
			$this->back_url = self::$currentIndex;

		switch ($this->display) {
			case 'tabAccess':
				$this->toolbar_btn['cancel'] = [
					'href' => self::$currentIndex . 'view',
					'desc' => $this->l('取消'),
				];
			case 'add':        //新增
				$this->toolbar_btn['save'] = [
					'href' => '#',
					'desc' => $this->l('儲存'),
				];
			break;
			case 'edit':        //修改
				if (in_array('edit', $this->post_processing)) {
					$this->toolbar_btn['back'] = [
						'href' => self::$currentIndex . 'view' . $this->table . '&' . $this->index . '=' . Tools::getValue($this->index),
						//回上一頁
						//						'href' => 'javascript:history.back();',
						'desc' => $this->l('返回'),
					];
				}
				$this->toolbar_btn['save'] = [
					'href' => '#',
					'desc' => $this->l('儲存'),
				];
			break;
			case 'view':        //檢視
				if ($this->tabAccess['add'] && in_array('add', $this->post_processing)) {
					$this->toolbar_btn['new'] = [
						'href' => self::$currentIndex . '&add' . $this->table,
						'desc' => $this->l('新增'),
					];
				}

				if ($this->tabAccess['edit'] && in_array('edit', $this->post_processing)) {
					$this->toolbar_btn['edit'] = [
						'href' => self::$currentIndex . 'edit' . $this->table . '&' . $this->index . '=' . Tools::getValue($this->index),
						'desc' => $this->l('修改'),
					];
				}

				if ($this->tabAccess['del'] && in_array('del', $this->post_processing)) {
					$this->toolbar_btn['del'] = [
						'href' => self::$currentIndex . '&del' . $this->table . '&' . $this->index . '=' . Tools::getValue($this->index),
						'desc' => $this->l('刪除'),
					];
				}

				$this->toolbar_btn['back'] = [
					'href' => $this->back_url,
					//回上一頁
					//					'href' => 'javascript:history.back();',
					'desc' => $this->l('返回'),
				];
//

			break;
			case 'options':        //選項
				$this->toolbar_btn['save'] = [
					'href' => '#',
					'desc' => $this->l('儲存'),
				];
				if ($this->tabAccess['view']) {
					$typs = '';
					if (isset($this->fields['excel']['list']) && (count($this->fields['excel']['list']) == 0 || !is_array($this->fields['excel']['list']))) {
						if ((count($this->fields['list']) > 0) && !isset($this->fields['excel']['assign'])) {
							$this->fields['excel']['list'] = $this->fields['list'];
							$typs                          = 'list';
						}
					} elseif (isset($this->fields['excel']['assign']) && (count($this->fields['excel']['assign']) == 0 || !is_array($this->fields['excel']['assign']))) {
//							$this->fields['excel']['list'] = $this->fields['form'];       //有待修改成 excel_coordinate
						foreach ($this->fields['form'] as $key => $v) {

						}
					}
					if (count($this->fields['excel']['list'])) {
						$typs = 'list';
					}
					if ((isset($this->fields['excel']['list']) || isset($this->fields['excel']['assign'])))
						$this->toolbar_btn['excel'] = [
							'href' => self::$currentIndex . '&excel' . $typs . $this->table . $url,
							'desc' => $this->l('匯出Excel'),
						];
				}
			break;
			case 'excellist':        //Exec列表
				$this->toolbar_btn['excel']  = [
					'href' => self::$currentIndex . '&excel' . $this->table,
					'desc' => $this->l('匯出'),
				];
				$this->toolbar_btn['filter'] = [
					'href' => '#',
					'desc' => $this->l('篩選'),
				];
				$this->toolbar_btn['back']   = [
					'href' => self::$currentIndex,
					'desc' => $this->l('返回'),
				];
			break;
			default: // list
				if ($this->tabAccess['add'] && ($this->max == 0 || $this->num < $this->max)) {
					$this->toolbar_btn['new'] = [
						'href' => self::$currentIndex . '&add' . $this->table,
						'desc' => $this->l('新增'),
					];
					if(!empty($this->fields['excel']['import']['from'])){
                        if (count($this->fields['excel']['import']['from'])) {
                            $this->toolbar_btn['excelimport'] = [
                                'href' => self::$currentIndex . '&excelimport' . $this->table,
                                'desc' => $this->l('匯入Excel'),
                            ];
                        }
                    }
				}

				if ($this->display != 'calendar') {
					foreach ($this->fields['list'] as $i => $v) {
						if ($v['filter']) {
							$this->toolbar_btn['media_search'] = [
								'href' => '#',
								'desc' => $this->l('搜尋'),
							];
							break;
						}
					}
				}
				if ($this->tabAccess['view']) {
					//月曆顯示
					if (isset($this->fields['calendar'])) {
						$this->toolbar_btn['calendar'] = [
							'href' => self::$currentIndex . '&calendar' . $this->table,
							'desc' => $this->l('圖表顯示'),
						];
					}

					//表單顯示
					if (isset($this->fields['calendar']) || false) {
						$this->toolbar_btn['list'] = [
							'href' => str_replace('&calendar' . $this->table, '', self::$currentIndex),
							'desc' => $this->l('表單顯示'),
						];
					}

					$typs = '';
					if (isset($this->fields['excel']['list']) && (count($this->fields['excel']['list']) == 0 || !is_array($this->fields['excel']['list']))) {
						if ((count($this->fields['list']) > 0) && !isset($this->fields['excel']['assign'])) {
							$this->fields['excel']['list'] = $this->fields['list'];
							$typs                          = 'list';
						}
					} elseif (isset($this->fields['excel']['assign']) && (count($this->fields['excel']['assign']) == 0 || !is_array($this->fields['excel']['assign']))) {
//							$this->fields['excel']['list'] = $this->fields['form'];       //有待修改成 excel_coordinate
						foreach ($this->fields['form'] as $key => $v) {

						}
					}
					if(!empty($this->fields['excel']['list'])){
                        if (count($this->fields['excel']['list'])) {
                            $typs = 'list';
                            $url  = '';
                        }
                    }
					if ((isset($this->fields['excel']['list']) || isset($this->fields['excel']['assign'])))
						$this->toolbar_btn['excel'] = [
							'href' => self::$currentIndex . '&excel' . $typs . $this->table . $url,
							'desc' => $this->l('匯出Excel'),
						];
				}
			break;
			/*
			if($this->tabAccess['del']) $this->toolbar_btn['del'] = array(
			'href' => self::$currentIndex.'&del'.$this->table,
			'desc' => $this->l('刪除')
			);
			*/
			/* // 匯出
			if ($this->allow_export) {
			$this->toolbar_btn['export'] = array(
			'href' => self::$currentIndex.'&export'.$this->table,
			'desc' => $this->l('Export')
			);
			}
			*/
		}

		$this->context->smarty->assign(
			[
				'top_bar_before_htm' => $this->top_bar_before_htm,
				'top_bar_after_htm'  => $this->top_bar_after_htm,
			]
		);
	}

	public function passValidate()
	{
		if (!empty($_SESSION['id_admin'])) {        //查看是否為預設密碼
			$sql = sprintf('SELECT `id_admin`
				FROM `admin`
				WHERE `id_admin` = %d
				AND `password` = %s
				LIMIT 0, 1',
				$_SESSION['id_admin'],
				GetSQL(DEFAULT_PASS, 'text'));
			$row = DB::rowSQL($sql, 1);
			if (!empty($row['id_admin'])) {        //跳至重設密碼
				Tools::redirectLink('//' . WEB_DNS . '/' . ADMIN_URL . '/Management?editadmin&conf2=2&id_admin=' . $_SESSION['id_admin']);
				exit;
			}
		}
	}
}