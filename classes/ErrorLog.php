<?php
/**
 * Created by PhpStorm.
 * User: Allen
 * Date: 2019/4/28
 * Time: 下午 06:02
 */

class ErrorLog extends ClassCore
{
	protected static $instance;
	public $_errors = array();
	public $_msgs = array();
	public $msg = '';


	public $errortype = array(
		E_ERROR					=> 'Error',
		E_WARNING					=> 'Warning',
		E_PARSE					=> 'Parse',
		E_NOTICE					=> 'Notice',
		E_CORE_ERROR				=> 'Core Error',
		E_CORE_WARNING			=> 'Core Warning',
		E_COMPILE_ERROR			=> 'Compile Error',
		E_COMPILE_WARNING		=> 'Compile Warning',
		E_USER_ERROR				=> 'Error',
		E_USER_WARNING			=> 'User warning',
		E_USER_NOTICE				=> 'User notice',
		E_STRICT					=> 'Runtime Notice',
		E_RECOVERABLE_ERROR		=> 'Recoverable error'
	);

	public function __construct()
	{
		$this->errortype = array(
			E_ERROR					=> $this->l('Error'),
			E_WARNING					=> $this->l('Warning'),
			E_PARSE					=> $this->l('Parse'),
			E_NOTICE					=> $this->l('Notice'),
			E_CORE_ERROR				=> $this->l('Core Error'),
			E_CORE_WARNING			=> $this->l('Core Warning'),
			E_COMPILE_ERROR			=> $this->l('Compile Error'),
			E_COMPILE_WARNING		=> $this->l('Compile Warning'),
			E_USER_ERROR				=> $this->l('Error'),
			E_USER_WARNING			=> $this->l('User warning'),
			E_USER_NOTICE				=> $this->l('User notice'),
			E_STRICT					=> $this->l('Runtime Notice'),
			E_RECOVERABLE_ERROR		=> $this->l('Recoverable error')
		);
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new ErrorLog();
		};
		return self::$instance;
	}

	public function myErrorHandler($errno, $errstr, $errfile, $errline)
	{
		if (!(error_reporting() & $errno)) {
			//這個錯誤代碼不包含在error_reporting中
			//通過標準的PHP錯誤處理程序
			return false;
		}
		$error_txt = '';
		switch ($errno) {
			case E_USER_ERROR:
				$error_txt .= "<b>My ERROR</b> [$errno] $errstr<br />\n";
				$error_txt .= "  Fatal error on line $errline in file $errfile";
				$error_txt .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
				$error_txt .= "Aborting...<br />\n";
				exit(1);
				break;

			case E_USER_WARNING:
				$error_txt .= "<b>My WARNING</b> [$errno] $errstr<br />\n";
				break;

			case E_USER_NOTICE:
				$error_txt .= "<b>My NOTICE</b> [$errno] $errstr<br />\n";
				break;

			default:
				$error_txt .= "Unknown error type: [$errno] $errstr<br />\n";
				break;
		}

		$this->_errors[] = $error_txt;
		return true;
	}

	public function set_error_handler(){
		set_error_handler(array($this, 'myErrorHandler'));
	}

	public function displayError(){
		$html = implode("<br>\n", $this->_errors);

		echo $html;
	}
}