<?php
class MemberFCM{
	public static function sendMessageById_houses($id_houses, $title = NULL, $body = NULL, $data=array()){
		$sql = sprintf('SELECT fri.`reg_id`, fri.`id_member`, fri.`type`
			FROM `fcm_reg_id` AS fri
			LEFT JOIN `member_in_house` AS mih ON mih.`id_member` = fri.`id_member`
			WHERE mih.`id_houses` = %d',
			GetSQL($id_houses, 'int'));
		$arr_row = Db::rowSQL($sql);
		$FCM = new FCM();
		foreach($arr_row AS $i => $row){
			$row['id_member'];
			$set_up = Member::getMemberSetUp($row['id_member']);
			if($set_up['fcm_on']){		//是否有開啟推播
				$FCM->to = $row['reg_id'];

				/*
				 * Android
				 * 開啟的標題、內容
				 */
				$FCM->data = $data;
				$FCM->data['title'] = $title;
				$FCM->data['body'] = $body;

				$FCM->notification = array(
					'url' => $FCM->data['url'],
					'title'	=> $title,
					'body'	=> $body,
				);

				$msg_num = HouseMessage::getMemberNoReadNum($row['id_member']);
				if($msg_num !== null){
					$FCM->data['msg_time'] = time();
					$FCM->data['msg_num'] = $msg_num;
					$FCM->notification['badge'] = $msg_num;
				}
				$FCM->type = $row['type'];
				$FCM->to = $row['reg_id'];
				$FCM->sendMessage();
			}
		}
		return true;
	}

	public static function sendMessageById_member($id_memeber, $title = NULL, $body = NULL, $data=array()){
		$set_up = Member::getMemberSetUp($id_memeber);
		if($set_up['fcm_on']){		//是否有開啟推播
			$sql = sprintf('SELECT `reg_id`
				FROM `fcm_reg_id`
				WHERE `id_member` = %d
				LIMIT 0, 1',
				GetSQL($id_memeber, 'int'));
			$row = Db::rowSQL($sql, true);
			$FCM = new FCM();
			$FCM->to = $row['reg_id'];

			/*
			 * Android
			 * 開啟的標題、內容
			 */
			$FCM->data = $data;
			$FCM->data['title'] = $title;
			$FCM->data['body'] = $body;

			$FCM->notification = array(
				'url' => $FCM->data['url'],
				'title'	=> $title,
				'body'	=> $body
			);

			$msg_num = 0;
			if($msg_num !== null){
				$FCM->data['msg_time'] = time();
				$FCM->data['msg_num'] = $msg_num;
				$FCM->notification['badge'] = $msg_num;
			}
			$FCM->type = $row['type'];
			$FCM->to = $row['reg_id'];
			$FCM->sendMessage();
			return true;
		}
		return false;
	}

//	public static function sendMessageByid_service($id_service, $title = NULL, $body = NULL, $id_schedule=NULL, $id_message=NULL){
//		$sql = sprintf('SELECT fr.`reg_id`, m.`id_member`
//			FROM `service` AS s
//			LEFT JOIN `houses` AS h ON h.`house_code` = s.`house_code`
//			LEFT JOIN `member` AS m ON m.`id_houses` = h.`id_houses`
//			LEFT JOIN `fcm_reg_id_to_member` AS f ON f.`id_member` = m.`id_member`
//			LEFT JOIN `fcm_reg_id` AS fr ON fr.`fcm_reg_id` = f.`fcm_reg_id`
//			WHERE s.`id_service` = %d',
//			GetSQL($id_service, 'int'));
//		$arr_row = Db::rowSQL($sql);
//		if(empty($id_schedule)){
//			$sql = sprintf('SELECT `id_schedule`
//				FROM `schedule`
//				WHERE `id_service` = %d
//				ORDER BY `id_schedule` DESC
//				LIMIT 0, 1',
//				GetSQL($id_service, 'int'));
//			$row2 = Db::rowSQL($sql, true);
//			$id_schedule = $row2['id_schedule'];
//		}
//		/*
//		 * test
//		 */
//
//		$FCM = new FCM();
////		$FCM->notification = array(
////			'title'	=> $title,
////			'body'	=> $body
////		);
//
//		$FCM->data = array(
//			//'url' => 'http://'.WEB_DNS.'/Schedule?&viewschedule&id_schedule='.$id_schedule.'&id_message='.$id_message
//			'url' => 'https://'.WEB_DNS.'/Service',
//			'title'	=> $title,
//			'body'	=> $body
//		);
//
//		foreach($arr_row AS $i => $row){
//			$msg_num = Message::getMemberNum($row['id_member']);
//			MemberIndex::add($row['id_member'], '/Service');
//			if($msg_num !== null){
//				$FCM->data['msg_time'] = time();
//				$FCM->data['msg_num'] = $msg_num;
//			}
//			$FCM->to = $row['reg_id'];
//			$FCM->sendMessage();
//		}
//		return true;
//	}
}