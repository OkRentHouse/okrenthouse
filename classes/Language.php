<?php
class Language{
	public $code = WEB_LANGIAGE;		//預設語系代碼
	public $id;					//語系ID
	protected static $instance = null;
	public function __construct(){
		
	}
	
	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Language();
		};
		return self::$instance;
	}
	
	public function l($str){
		return $str;
	}
	
	public function id_by_code($code=NULL){
		if(empty($code)) $code = $this->code;
		$sql = 'SELECT `id_language`
			FROM `language`
			WHERE `iso_code` = "'.$code.'"
			LIMIT 0, 1';
		$SELECT_language = new db_mysql($sql);
		if($SELECT_language->num() > 0){
			$language = $SELECT_language->row();
			return $language['id_language'];
		};
		return false;
	}
	
	public function code_by_id($id=0){
		if(empty($id)) $id = $this->id;
		$sql = 'SELECT `iso_code`
			FROM `language`
			WHERE `id` = '.$id.'
			LIMIT 0, 1';
		$SELECT_language = new db_mysql($sql);
		if($SELECT_language->num() > 0){
			$language = $SELECT_language->row();
			return $language['iso_code'];
		};
		return false;
	}
}