<?php

class FormTable
{
	public $module;
	public $table;                    //資料庫表單
	public $_join;
	public $_as;
	public $_group;
	public $list_id;                    //列表ID
	public $cache = true;        //快取
	public $form = [];
	public $has_actions = false;            //是否有編輯權限
	public $fields;
//	public $back_url;                    //返回、取消路徑

	public $submit_action;                //post動作
	public $display;                    //處理動作
	public $tabAccess;                //權限
	public $definition;                //表單定義
	public $index_id;                    //所引ID
	public $display_edit_div;
	public $_errors = [];
	public $bulk_actions;
	public $must;
	public $display_page_header_toolbar = true;
	public $sql;
	public $post_processing = [];    //事後處理 例 array('edit') = 點完檢視後進去才能修改, array('edit', 'del') = 點完檢視後進去才能修改、刪除
	public $arr_take_photo_data = [];    //拍照
	public $arr_signature_data = [];    //簽名
	protected static $instance;
    public $conn;//連接資料庫


	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new FormTable();
		}
		return self::$instance;
	}

	public function __construct()
	{
		$list_filter = Context::getContext()->cookie['list_filter'];
		if (empty($list_filter))
			Context::getContext()->cookie['list_filter'] = unserialize($_COOKIE['list_filter']);
		$this->fields['list']        = [];                //列表
		$this->fields['list_filter'] = [];            //列表篩選
		$this->fields['list_val']    = [];            //列表值
		$this->fields['list_num']    = 20;                //表單顯示數量
		$this->cache                 = CACHE;
	}

	public function set()
	{
		[
			'label'       => '',    //欄位名稱
			'type'        => '',    //欄位形態
			'class'       => '',    //class
			'id'          => '',    //id
			'name'        => '',    //name
			'value'       => '',    //val
			'length'      => '',    //maxlength
			'cols'        => '',    //欄
			'rows'        => '',    //列
			'title'       => '',    //title
			'required'    => '',    //是否必填
			'placeholder' => '',    //預設內容
			'attr'        => '',    //array('data-time' = > '12:00')	//自訂元素
			'pattern'     => '',
		];
	}

	//表單碼
	public function formcode($table = null)
	{
		if (empty($table))
			$table = $this->table;
		return $this->field_to_form_code();
	}

	//資料庫轉列表
	public function field_to_list_code()
	{

	}

	//資料庫轉表單
	public function field_to_form_code()
	{
		$show = [];
		if ($this->cache) {
			add_dir(CACHE_DIR . DS . 'form');
			$file = CACHE_DIR . DS . 'form' . DS . 'form_' . DB_PREFIX_ . $this->table . '_formcode.php';
			if (is_file($file))
				return include($file);
		}
		$Db        = new Db();
		$Db->cache = $this->cache;
		$Db->table = $this->table;
		$show      = $Db->show();
		if ($this->cache) {
			$file = CACHE_DIR . DS . 'form' . DS . 'form_' . DB_PREFIX_ . $this->table . '_formcode.php';
			$fp   = fopen($file, 'w');        //新增、覆蓋檔案
			fwrite($fp, "<?php return array (\r\n");
			foreach ($show as $key => $v) {
				fwrite($fp, "\tarray(\r\n");
				$type = [];
				$num  = [];
				$min  = [];
				$max  = [];
				foreach ($v as $j => $jv) {
					$jv = str_replace("'", "\'", $jv);
					switch ($j) {
						case 'Field':
							fwrite($fp, "\t\t'name'\t\t=> '" . $jv . "',\r\n");
						break;
						case 'Type':
							$type[$key] = $jv;
							if (strpos($jv, '(') >= 0) {
								$type[$key] = substr($jv, 0, strpos($jv, '('));
								$num[$key]  = (int)substr($jv, strpos($jv, '(') + 1, strpos($jv, ')'));
							}
							if (empty($num[$key]))
								$num[$key] = 0;
							fwrite($fp, "\t\t'maxlength'\t\t=> '" . $num[$key] . "',\r\n");
						break;
						case 'Collation':

						break;
						case 'Null':
							if ($jv == 'NO')
								$jv = true;
							fwrite($fp, "\t\t'required'\t\t=> '" . $jv . "',\r\n");
						break;
						case 'Key':

						break;
						case 'Default':

						break;
						case 'Extra':
							if ($jv == 'auto_increment')
								$type[$key] = 'hidden';
						break;
						case 'Privileges':

						break;
						case 'Comment':
							$jv = strpos($jv, ' ') ? substr($jv, 0, strpos($jv, ' ')) : $jv;
							fwrite($fp, "\t\t'label'\t\t=> '" . $jv . "',\r\n");
							fwrite($fp, "\t\t'title'\t\t=> '" . $jv . "',\r\n");
							fwrite($fp, "\t\t'placeholder'\t=> '" . $jv . "',\r\n");
							fwrite($fp, "\t\t'class'\t\t=> '',\r\n");
							fwrite($fp, "\t\t'value'\t\t=> '',\r\n");
							fwrite($fp, "\t\t'attr'\t\t=> '',\r\n");
						break;
					}
				}

				if ($type[$key] == 'text' && (strpos($jv, '(') >= 0)) {
					$ro = substr($jv, strpos($jv, '(') + 1, strpos($jv, '(') - 1);
					$ro = explode(',', $ro);
					fwrite($fp, "\t\t'rows'\t\t=> '" . $ro[0] . "',\r\n");
					fwrite($fp, "\t\t'cols'\t\t=> '" . $ro[0] . "',\r\n");
				}
				fwrite($fp, "\t\t'type'\t\t=> '" . $type[$key] . "',\r\n");
				fwrite($fp, "\t\t'min'\t\t\t=> '" . $min[$key] . "',\r\n");
				fwrite($fp, "\t\t'max'\t\t\t=> '" . $max[$key] . "',\r\n");
				fwrite($fp, "\t),\r\n");
			}
			fwrite($fp, "); ?>");
			fclose($fp);
			if (is_file($file))
				return include($file);
		}
	}

	//表單驗證
	public static function verification_form($form_name, $name, $value, $table = null)
	{
		return '';
	}

	//表單驗證
	public function v_f($table, $name, $value)
	{
		return $this->verification_form($table, $name, $value);
	}

	//表單碼轉表單Ｆ
	public function code_to_form()
	{
		$formcode = $this->formcode();
		$html     = '';
		foreach ($formcode as $key => $v) {
			$html .= '';
		}
		return $html;
	}

	public function ini()
	{
		if (empty($this->submit_action)) {
			if (empty($this->display)) {
				$this->display = 'add';
			}
			$this->submit_action = 'submit' . ucfirst($this->display) . $this->table;
		}
	}

	//輸入表單
	public function renderForm()
	{
		$this->ini();
		$must           = [];
		$left_index     = [];
		$arr_take_photo = [];
		$arr_signature  = [];


		if (!empty($this->fields['tabs']) || (!empty($this->fields['form'] && is_array($this->fields['form'])))) {
			$json = new Services_JSON();
			foreach ($this->fields['form'] as $key2 => $v2) {
				if (!isset($v2['tab'])) {
					$this->fields['form'][$key2]['tab'] = 'default';
				}
				$index             = '';
				$arr_select        = [];        //資料庫
				$arr_select2       = [];        //資料庫
				$arr_configuration = [];    //組態
				$arr_no_action     = [];    //不動
				if (!empty($this->fields['form'][$key2]['table'])) {
					$table = $this->fields['form'][$key2]['table'];
				} else {
					$table                                = $this->table;
					$this->fields['form'][$key2]['table'] = $table;
				}

				if (!isset($this->fields['form'][$key2]['heading_display']))
					$this->fields['form'][$key2]['heading_display'] = true;
				if (!isset($this->fields['form'][$key2]['body_display']))
					$this->fields['form'][$key2]['body_display'] = true;
				if (!isset($this->fields['form'][$key2]['footer_display']))
					$this->fields['form'][$key2]['footer_display'] = true;
				if (empty($this->fields['form'][$key2]['submit'])
					&& empty($this->fields['form'][$key2]['cancel'])
					&& empty($this->fields['form'][$key2]['reset'])
					&& empty($this->fields['form'][$key2]['buttons']))
					$this->fields['form'][$key2]['footer_display'] = false;
				if (isset($this->fields['form'][$key2]['input'])) {
					if (!isset($this->fields['form'][$key2]['display_from']))
						$this->fields['form'][$key2]['display_from'] = true;
					foreach ($this->fields['form'][$key2]['input'] as $key => $v) {
						if ($this->fields['form'][$key2]['input'][$key]['type'] == 'view')
							$this->fields['form'][$key2]['input'][$key]['required'] = false;    //移除必填
						if ($this->fields['form'][$key2]['input'][$key]['name'] == 'position') {
							$this->fields['form'][$key2]['input'][$key]['required'] = true;
							$this->fields['form'][$key2]['input'][$key]['val']      = 0;
							$this->fields['form'][$key2]['input'][$key]['min']      = 0;

							if ($this->display == 'view') {
								$this->fields['form'][$key2]['input'][$key]['type'] = 'view';
								unset($this->fields['form'][$key2]['input'][$key]['required']);
							} else {
								$this->fields['form'][$key2]['input'][$key]['type'] = 'number';
							}
						}

						if ($this->fields['form'][$key2]['input'][$key]['auto_datetime'] == 'add' && ($this->display == 'add')) {
							$this->fields['form'][$key2]['input'][$key]['type'] = 'hidden';
						}

						if ($this->fields['form'][$key2]['input'][$key]['auto_datetime'] == 'edit' && ($this->display == 'edit')) {
							$this->fields['form'][$key2]['input'][$key]['type'] = 'view';
						}

						if (!empty($this->fields['form'][$key2]['input'][$key]['options']['table']) && $this->fields['form'][$key2]['input'][$key]['auto_id'] == 'add' && ($this->display == 'add')) {
							$this->fields['form'][$key2]['input'][$key]['type'] = 'view';
							if ($this->fields['form'][$key2]['input'][$key]['options']['table'] == 'admin') {
								$this->fields['form'][$key2]['input'][$key]['val'] = $_SESSION['id_admin'];
							}
							if ($this->fields['form'][$key2]['input'][$key]['options']['table'] == 'member') {
								$this->fields['form'][$key2]['input'][$key]['val'] = $_SESSION['id_member'];
							}
						}

						if (($this->fields['form'][$key2]['input'][$key]['type'] == 'change-password') || ($this->fields['form'][$key2]['input'][$key]['type'] == 'confirm-password')) {
							if ($this->display == 'view') {
								$this->fields['form'][$key2]['input'][$key]['required']  = false;    //移除必填
								$this->fields['form'][$key2]['input'][$key]['no_action'] = true;    //不動作
								$this->fields['form'][$key2]['input'][$key]['type']      = 'hidden';        //影藏
							}
						}
						if ($this->fields['form'][$key2]['input'][$key]['type'] == 'take_photo') {
							$this->fields['form'][$key2]['input'][$key]['no_action']   = true;
							$this->fields['form'][$key2]['input'][$key]['key']['name'] = $this->fields['form'][$key2]['input'][$key]['name'];
							$arr_take_photo[]                                          = $this->fields['form'][$key2]['input'][$key]['key'];
						}

						if ($this->fields['form'][$key2]['input'][$key]['type'] == 'signature') {
							$this->fields['form'][$key2]['input'][$key]['key']['name'] = $this->fields['form'][$key2]['input'][$key]['name'];
							$arr_signature[]                                           = $this->fields['form'][$key2]['input'][$key]['key'];
						}

						if ($v['name'] == 'avtive') {
							$this->fields['form'][$key2]['input'][$key]['values'] = [
								[
									'id'    => 'active_on',
									'value' => 1,
									'label' => $this->l('啟用'),
								],
								[
									'id'    => 'active_off',
									'value' => 0,
									'label' => $this->l('關閉'),
								],
							];
						}

						if (!empty($v['filter_sql'])) {
							$arr_select2[] = $v['filter_sql'] . ' AS ' . $this->fields['form'][$key2]['input'][$key]['name'];
						}
						$name_key = $v['name'];
						if (!empty($v['filter_key'])) {
							$arr_filter_key = explode('!', $v['filter_key']);
							if (count($arr_filter_key) == 1) {
								$name_key = $arr_filter_key[0];        //SELEST搜尋表
							} else {
								$name_key = $arr_filter_key[1];        //SELEST搜尋表
							}
						}
						if (Tools::isSubmit($v['name'])) {
							$arr = Tools::getValue($v['name']);
							if ($this->fields['form'][$key2]['input'][$key]['multiple']
								&& ($this->fields['form'][$key2]['input'][$key]['type'] != 'select'
									&& $this->fields['form'][$key2]['input'][$key]['type'] != 'checkbox'
									&& $this->fields['form'][$key2]['input'][$key]['type'] != 'tpl')
							) {
								$this->fields['form'][$key2]['input'][$key]['string_format'] = $arr[(int)${'multiple_' . $v['name']}]; //收到的POST
								${'multiple_' . $v['name']}++;
							} else {
								$this->fields['form'][$key2]['input'][$key]['string_format'] = $arr; //收到的POST
							}
						}
						$this->fields['form'][$key2]['input'][$key]['name_key'] = $name_key;
						//$this->addCSS($this->context->css_list['reset']);
						if (isset($v['configuration']) && $v['configuration']) {
							$arr_configuration[] = $v['name'];        //組態
						} elseif ($v['no_action']) {
							$arr_no_action[] = $v['name'];        //組態
						}

						if (isset($v['type']) && $v['type'] != 'change-password' && $v['type'] != 'confirm-password' && isset($v['name']) && (!isset($v['no_action']) || !$v['no_action']) && !isset($v['configuration']) && !isset($v['filter_sql'])) {
							if (!in_array($name_key, $arr_select)) {
								$arr_select[] = $name_key;
							}
						}
						$db_prefix_ = DB_PREFIX_;
						if (isset($v2['db_prefix_'])) {
							$db_prefix_ = $v2['db_prefix_'];
						}
						if (isset($v['index']) && $v['index']) {
							$index                                         = $name_key;
							$must[$db_prefix_ . $table . '.' . $v['name']] = Tools::getValue($index);                            //TABLE索引鍵KEY
							if (isset($v['left_index']) && !empty($v['left_index']['table'])) {
								$left_index[$v['left_index']['db_prefix_'] . $v['left_index']['table'] . '.' . $v['left_index']['key']] = $must[$db_prefix_ . $table . '.' . $v['name']];
							}
						}

						if (isset($v['left_index']) && !empty($v['left_index']['table'])) {
							$must[$db_prefix_ . $table . '.' . $v['name']]                                                          = Tools::getValue($index);
							$left_index[$v['left_index']['db_prefix_'] . $v['left_index']['table'] . '.' . $v['left_index']['key']] = $must[$db_prefix_ . $table . '.' . $v['name']];
						}

						if (isset($v['must']) && !empty($v['must'])) {
							$left_index[$db_prefix_ . $table . '.' . $index] = $must[$v['must']];
						}

						if (isset($v['options']['table'])) {    //下拉選單
							$select     = '';
							$db_prefix_ = DB_PREFIX_;
							if (isset($v['options']['db_prefix_'])) {
								$db_prefix_ = $v['options']['db_prefix_'];
							}
							if (isset($v['options']['parent']) && $v['options']['parent'] != '')
								$select = ' ,`' . $v['options']['parent'] . '`';        //父層
							if (isset($v['options']['data']) && $v['options']['data'] != '') {
								if (!is_array($v['options']['data'])) {
									$v['options']['data'] = [$v['options']['data']];
								}
								foreach ($v['options']['data'] as $uu => $uuv) {
									$select .= ' ,`' . $uuv . '`';
								}
							}
							if (isset($v['options']['class']) && $v['options']['class'] != '') {
								if (!is_array($v['options']['class'])) {
									$v['options']['class'] = [$v['options']['class']];
								}
								foreach ($v['options']['class'] as $uu => $uuv) {
									$select .= ' ,`' . $uuv . '`';
								}
							}
							if (!is_array($v['options']['text'])) {
								$sql = 'SELECT `' . $v['options']['value'] . '`, `' . $v['options']['text'] . '`' . $select . '
								FROM `' . $db_prefix_ . $v['options']['table'] . '`';
							} else {
								$sql = 'SELECT `' . $v['options']['value'] . '`, `' . implode('`, `', $v['options']['text']) . '`' . $select . '
								FROM `' . $db_prefix_ . $v['options']['table'] . '`';
							}
							if (empty($v['options']['where']) || $v['type'] == 'view') {
								$sql .= ' WHERE TRUE';
							} else {
								$sql .= ' WHERE TRUE' . $v['options']['where'];
							}

							if (isset($v['options']['order']))
								$sql .= ' ORDER BY ' . $v['options']['order'];
							if (isset($v['options']['num']))
								$sql .= ' LIMIT 0,  ' . $v['options']['num'];
//							$arr_row         = Db::rowSQL($sql);
                            $arr_row         = Db::rowSQL($sql,false,0,$this->conn);
							$arr_options_val = [];
							if (count($arr_row) > 0) {
								$this->fields['form'][$key2]['input'][$key]['options']['val'] = [];
								if (!isset($v['options']['text_divide']))
									$v['options']['text_divide'] = '%s - %s';
								foreach ($arr_row as $i => $t_v) { //選單內值
									if (is_array($v['options']['text'])) {
										$arr_text = [];
										foreach ($v['options']['text'] as $i => $options_text) {
											$arr_text[] = $t_v[$options_text];
										}
										$text = vsprintf($v['options']['text_divide'], $arr_text);
									} else {
										$text = $t_v[$v['options']['text']];
									}
									if (!is_array($arr_options_val[$t_v[$v['options']['value']]])) {
										$arr_options_val[$t_v[$v['options']['value']]] = ['val' => $t_v[$v['options']['value']], 'text' => $text];    //父
									}
									if (isset($v['options']['parent']) && $v['options']['parent'] != '') {
										$arr_options_val[$t_v[$v['options']['value']]]['parent'][] = $t_v[$v['options']['parent']];
										if (isset($v['options']['parent_name']) && $v['options']['parent_name']) {
											$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent_name'];
										} else {
											$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent'];
										}
									}
									if (isset($v['options']['data']) && $v['options']['data'] != '') {        //jquery data
										$arr_options_val[$t_v[$v['options']['value']]]['data'] = [];
										foreach ($v['options']['data'] as $uu => $uuv) {
											$arr_options_val[$t_v[$v['options']['value']]]['data'][$uuv] = $t_v[$uuv];
										}
									}
									if (isset($v['options']['class']) && $v['options']['class'] != '') {        //class
										$arr_options_val[$t_v[$v['options']['value']]]['class'] = [];
										foreach ($v['options']['class'] as $uu => $uuv) {
											$arr_options_val[$t_v[$v['options']['value']]]['class'][$uuv] = $t_v[$uuv];
										}
									}
								}
								foreach ($arr_options_val as $o_v_i => $o_v) {
								    if(!empty($o_v['parent'])){
                                        $arr_options_val[$o_v_i]['parent'] = '_' . implode('_', $o_v['parent']) . '_';
                                    }else{
                                        $arr_options_val[$o_v_i]['parent'] = $o_v['parent'][0];
                                    }
								    //上面代替下面
//									if (count($o_v['parent']) > 1) {
//										$arr_options_val[$o_v_i]['parent'] = '_' . implode('_', $o_v['parent']) . '_';
//									} else {
//										$arr_options_val[$o_v_i]['parent'] = $o_v['parent'][0];
//									}
								}
								$this->fields['form'][$key2]['input'][$key]['options']['val'] = $arr_options_val;
							}
						} else {
							if (isset($v['key']) && !isset($v['values'])) {    //switch
								$related_where  = ' WHERE TRUE';
								$related_select = '';
								$text_select    = '';
								if (!empty($v['key']['where']))
									$related_where .= $v['key']['where'];
								if (!empty($v['key']['parent'])) {
									$related_select = ', `' . $v['key']['parent'] . '`';
								}
								$related_order = '';
								if (!empty($v['key']['order'])) {
									$related_where .= ' ORDER BY ' . $v['key']['order'];
								}
								if (!empty($v['key']['text'])) {
									if (is_array($v['key']['text'])) {
										$text_select = ', `' . implode('`, `', $v['key']['text']) . '`';
									} else {
										$text_select = ', `' . $v['key']['text'] . '`';
									}
								}
								$db_prefix_ = DB_PREFIX_;
								if (isset($v['key']['db_prefix_'])) {
									$db_prefix_ = $v['key']['db_prefix_'];
								}

								$sql     = 'SELECT `' . $v['key']['key'] . '`, `' . $v['key']['val'] . '`' . $related_select . $text_select . '
													FROM `' . $db_prefix_ . $v['key']['table'] . '`' . $related_where . $related_order;
								$arr_row = Db::rowSQL($sql,false,0,$this->conn);

								foreach ($arr_row as $j => $w) {
									$label = $w[$v['key']['val']];
									if (isset($v['key']['text'])) {
										if (is_array($v['key']['text'])) {
											$arr_text = [];
											foreach ($v['key']['text'] as $it => $vvv) {
												$arr_text[] = $w[$vvv];
											}
										} else {
											$label = $w[$v['key']['text']];
										}
									}

									$this->fields['form'][$key2]['input'][$key]['values'][] = [
										'id'    => $v['name'] . '_' . $w[$v['key']['key']],
										'value' => $w[$v['key']['key']],
										'label' => $label,
									];
								}
							}
						}
					}
				}


				if ($this->display == 'add') {
					foreach ($this->fields['form'][$key2]['input'] as $key => $v) {
						if ($this->fields['form'][$key2]['input'][$key]['type'] == 'view') { //顯示 view
							//不複選
							$arr_values = [];
							if (isset($this->fields['form'][$key2]['input'][$key]['values'])) {
								foreach ($this->fields['form'][$key2]['input'][$key]['values'] as $ii => $vv) {
									$arr_values[(string)$vv['value']] = $vv['label'];
								}
							}
							$arr_options = [];
							if (isset($this->fields['form'][$key2]['input'][$key]['options'])) {    //
								foreach ($this->fields['form'][$key2]['input'][$key]['options']['val'] as $ii => $vv) {
									$arr_options[(string)$vv['val']] = $vv['text'];
								}
							}
							if (!$v['multiple']) {//todo
								//print $this->fields['form'][$key2]['input'][$key]['label'].' = '.(string) $this->fields['form'][$key2]['input'][$key]['val'].' = '.'<br>';
								if ($this->fields['form'][$key2]['input'][$key]['in_table']) {    //存入資料表裡
									$arr_test = $json->decode($this->fields['form'][$key2]['input'][$key]['val']);
									if (is_array($arr_test)) {
										$this->fields['form'][$key2]['input'][$key]['val'] = $arr_test[0];
									}
								}
								if (count($arr_values) > 0) {
									$this->fields['form'][$key2]['input'][$key]['val'] = $arr_values[(string)$this->fields['form'][$key2]['input'][$key]['val']];
								}
								//options
								if (count($arr_options) > 0) {
									if (isset($this->fields['form'][$key2]['input'][$key]['options']['data'])) {
										foreach ($this->fields['form'][$key2]['input'][$key]['options']['val'] as $data_i => $data_v) {
											if ($data_v['val'] == $this->fields['form'][$key2]['input'][$key]['val']) {
												foreach ($this->fields['form'][$key2]['input'][$key]['options']['data'] as $data_key => $data_txt) {
													$this->fields['form'][$key2]['input'][$key]['data'][$data_txt] = $data_v[$data_i]['data'][$data_txt];
												}
												break;
											}
										}
									}
									if (isset($this->fields['form'][$key2]['input'][$key]['options']['class'])) {
										foreach ($this->fields['form'][$key2]['input'][$key]['options']['val'] as $class_i => $class_v) {
											if ($class_v['val'] == $this->fields['form'][$key2]['input'][$key]['val']) {
												foreach ($this->fields['form'][$key2]['input'][$key]['options']['class'] as $class_key => $class_txt) {
													$this->fields['form'][$key2]['input'][$key]['class'] .= ' ' . $class_v[$class_i]['class'][$class_txt];
												}
												break;
											}
										}
									}
									$this->fields['form'][$key2]['input'][$key]['val'] = $arr_options[(string)$this->fields['form'][$key2]['input'][$key]['val']];
								}
							} else {    //複選

								if (count($arr_values) > 0) {
									$arr_label = [];
									foreach ($this->fields['form'][$key2]['input'][$key]['val'] as $ii => $vv) {
										$arr_label[] = $arr_values[$vv];
									}
									$this->fields['form'][$key2]['input'][$key]['val'] = implode('、', $arr_label);
									unset($arr_label);
								}
								//options
								if (count($arr_options) > 0) {
									$arr_text = [];
									foreach ($this->fields['form'][$key2]['input'][$key]['val'] as $ii => $vv) {
										$arr_text[] = $arr_options[$vv];
									}
									$this->fields['form'][$key2]['input'][$key]['val'] = implode("\n", $arr_text);
									unset($arr_text);
								}
							}
						}
					}
				}
				$db_prefix_ = DB_PREFIX_;
				if (isset($v2['db_prefix_'])) {
					$db_prefix_ = $v2['db_prefix_'];
				}
				$Tools_index = Tools::getValue($index);
				if (empty($Tools_index))
					$Tools_index = $left_index[$db_prefix_ . $table . '.' . $index];
				if (!empty($Tools_index) || (count($arr_configuration) > 0) || (count($arr_no_action) > 0)) {

					if (count($arr_select) > 0 || count($arr_select2) > 0) {
						$select2 = implode(', ', $arr_select2);
						if (!empty($select2))
							$select2 = ', ' . $select2;
						if (!empty($this->_as))
							$as = ' AS ' . $this->_as;
						$sql = 'SELECT `' . implode('`, `', $arr_select) . '`' . $select2 . '
									FROM `' . $db_prefix_ . $table . '`' . $as . '
									WHERE `' . $index . '` = "' . $Tools_index . '"' . $this->fields['where'] . '
									LIMIT 0, 1';
						$row = Db::rowSQL($sql,true,0,$this->conn);
						if (count($row) > 0) {
							$arr_multiple   = [];
							$arr_multiple_v = [];
							foreach ($this->fields['form'][$key2]['input'] as $key => $v) {
								if (!in_array($v['name'], $arr_multiple)) {
									foreach ($row as $j => $jv) {
										if ($v['type'] == 'datetime-local')
											$jv = substr_replace($jv, 'T', 10, 1);
										if ($v['name_key'] == $j) {            //todo checkbox 要跟 select 、List合併統一格式
											if (($v['type'] == 'checkbox') || (isset($v['multiple']) && $v['multiple'])) {
												$arr_multiple[]                                    = $v['name'];
												$jv                                                = $json->decode($jv);
												$this->fields['form'][$key2]['input'][$key]['val'] = $jv;    //預設值
												if (($v['type'] == 'text' || $v['type'] == 'textarea' || $v['type'] == 'number' || $v['type'] == 'view') && !isset($v['options'])) {
													$arr_multiple_v[$v['name']]                        = $jv;
													$this->fields['form'][$key2]['input'][$key]['val'] = array_shift($arr_multiple_v[$v['name']]);    //預設值
												}
											} else {
												$this->fields['form'][$key2]['input'][$key]['val'] = $jv;    //預設值
											}
											if (isset($v['left_index']) && !empty($v['left_index']['table'])) {
												$left_index[$v['left_index']['db_prefix_'] . $v['left_index']['table'] . '.' . $v['left_index']['key']] = $jv;
											}
											if (isset($this->fields['form'][$key2]['input'][$key]['eval_val'])) {        //todo
												$this->fields['form'][$key2]['input'][$key]['val'] = eval($this->fields['form'][$key2]['input'][$key]['eval_val']);    //預設值
											}
										}
									}
									if ($this->fields['form'][$key2]['input'][$key]['type'] == 'view') { //顯示 view
										$arr_values       = [];
										$arr_values_class = [];
										if (isset($this->fields['form'][$key2]['input'][$key]['values'])) {
											foreach ($this->fields['form'][$key2]['input'][$key]['values'] as $ii => $vv) {
												$arr_values[(string)$vv['value']]       = $vv['label'];
												$arr_values_class[(string)$vv['value']] = $vv['class'];
											}
										}
										$arr_options       = [];
										$arr_options_data  = [];
										$arr_options_class = [];
										if (isset($this->fields['form'][$key2]['input'][$key]['options'])) {
											if (isset($this->fields['form'][$key2]['input'][$key]['options']['default'])) {
												$default                                    = $this->fields['form'][$key2]['input'][$key]['options']['default'];
												$arr_options[(string)(int)$default['val']]  = $default['text'];
												$arr_options_data[(string)$default['val']]  = $default['data'];
												$arr_options_class[(string)$default['val']] = $default['class'];
											}
											foreach ($this->fields['form'][$key2]['input'][$key]['options']['val'] as $ii => $vv) {
												$arr_options[(string)$vv['val']]       = $vv['text'];
												$arr_options_data[(string)$vv['val']]  = $vv['data'];
												$arr_options_class[(string)$vv['val']] = $vv['class'];
											}
										}

										//不複選
										if (!$v['multiple']) {//todo
											//print $this->fields['form'][$key2]['input'][$key]['label'].' = '.(string) $this->fields['form'][$key2]['input'][$key]['val'].' = '.'<br>';
											if ($this->fields['form'][$key2]['input'][$key]['in_table']) {    //存入資料表裡
												$arr_test = $json->decode($this->fields['form'][$key2]['input'][$key]['val']);
												if (is_array($arr_test)) {
													$this->fields['form'][$key2]['input'][$key]['val'] = $arr_test[0];
												}
											}
											if (count($arr_values) > 0) {
//											$this->fields['form'][$key2]['input'][$key]['val'] = $arr_values[(string)$this->fields['form'][$key2]['input'][$key]['val']];
												$val                                               = $arr_values[(string)$this->fields['form'][$key2]['input'][$key]['val']];
												$class                                             = $arr_values_class[(string)$this->fields['form'][$key2]['input'][$key]['val']];
												$this->fields['form'][$key2]['input'][$key]['val'] = ($val) ? $val : $this->fields['form'][$key2]['input'][$key]['val'];
												if (!empty($class)) {
													$this->fields['form'][$key2]['input'][$key]['class'] .= ' ' . $class;
												}
											}
											//options
											if (count($arr_options) > 0) {
												$class                                              = $arr_options_class[(string)$this->fields['form'][$key2]['input'][$key]['val']];
												$this->fields['form'][$key2]['input'][$key]['data'] = $arr_options_data[(string)$this->fields['form'][$key2]['input'][$key]['val']];
												$this->fields['form'][$key2]['input'][$key]['val']  = $arr_options[(string)$this->fields['form'][$key2]['input'][$key]['val']];
												if (!empty($class)) {
													if (is_array($class)) {
														foreach ($class as $ci => $cv) {
															$this->fields['form'][$key2]['input'][$key]['class'] .= ' ' . $cv;
														}
													} else {
														$this->fields['form'][$key2]['input'][$key]['class'] .= ' ' . $class;
													}
												}
											}

										} else {    //複選
											if (count($arr_values) > 0) {
												$arr_label = [];
												foreach ($this->fields['form'][$key2]['input'][$key]['val'] as $ii => $vv) {
													$arr_label[]       = $arr_values[$vv];
													$arr_label_class[] = $arr_values_class[$vv];
												}
												$this->fields['form'][$key2]['input'][$key]['val'] = implode('、', $arr_label);
												if (count($arr_label_class)) {
													$this->fields['form'][$key2]['input'][$key]['class'] .= ' ' . implode(' ', $arr_label_class);
												}
												unset($arr_label);
												unset($arr_label_class);
											}
											//options
											if (count($arr_options) > 0) {
												$arr_text = [];
												foreach ($this->fields['form'][$key2]['input'][$key]['val'] as $ii => $vv) {
													$arr_text[] = $arr_options[$vv];
												}
												$this->fields['form'][$key2]['input'][$key]['val'] = implode("\n", $arr_text);
												unset($arr_text);
											}
										}

										//下拉卷軸
										$v = $this->fields['form'][$key2]['input'][$key];
										if (isset($v['options']['table'])) {    //下拉選單
											$select = '';
											if (isset($v['options']['parent']) && $v['options']['parent'] != '')
												$select = ' ,`' . $v['options']['parent'] . '`';
											if (isset($v['options']['data']) && $v['options']['data'] != '') {
												if (!is_array($v['options']['data'])) {
													$v['options']['data'] = [$v['options']['data']];
												}
												foreach ($v['options']['data'] as $uu => $uuv) {
													$select .= ' ,`' . $uuv . '`';
												}
											}
											$db_prefix_ = DB_PREFIX_;
											if (isset($v['options']['db_prefix_'])) {
												$db_prefix_ = $v['options']['db_prefix_'];
											}
											if (!is_array($v['options']['text'])) {
												$sql = 'SELECT `' . $v['options']['value'] . '`, `' . $v['options']['text'] . '`' . $select . '
													FROM `' . $db_prefix_ . $v['options']['table'] . '`';
											} else {
												$sql = 'SELECT `' . $v['options']['value'] . '`, `' . implode('`, `', $v['options']['text']) . '`' . $select . '
													FROM `' . $db_prefix_ . $v['options']['table'] . '`';
											}

											$no_arr = [];
											foreach ($v['options']['val'] as $jj => $ww) {
												$no_arr[] = $ww['val'];
											}
											if (is_array($v['val'])) {
												$sql .= ' WHERE `' . $v['options']['value'] . '` IN (' . implode(', ', ($v['val'])) . ')';
											} else {
												$sql .= ' WHERE `' . $v['options']['value'] . '` = ' . $v['val'] . '';
											}
											if (count($no_arr)) {
												$sql .= ' AND `' . $v['options']['value'] . '` NOT IN (' . implode(', ', $no_arr) . ')';
											}
											$arr_options_val = [];
											$arr_row         = Db::rowSQL($sql,false,0,$this->conn);
//											echo $sql;
											if (count($arr_row) > 0) {
												if (!isset($v['options']['text_divide']))
													$v['options']['text_divide'] = '%s - %s';
												foreach ($arr_row as $i => $t_v) {        //選單內值
													if (is_array($v['options']['text'])) {
														$arr_text = [];
														foreach ($v['options']['text'] as $i => $options_text) {
															$arr_text[] = $t_v[$options_text];
														}
														$text = vsprintf($v['options']['text_divide'], $arr_text);
													} else {
														$text = $t_v[$v['options']['text']];
													}
													if (!is_array($arr_options_val[$t_v[$v['options']['value']]])) {
														$arr_options_val[$t_v[$v['options']['value']]] = ['val' => $t_v[$v['options']['value']], 'text' => $text];    //父
													}
													if (isset($v['options']['parent']) && $v['options']['parent'] != '') {
														$arr_options_val[$t_v[$v['options']['value']]]['parent'][] = $t_v[$v['options']['parent']];
														if (isset($v['options']['parent_name']) && $v['options']['parent_name']) {
															$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent_name'];
														} else {
															$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent'];
														}
													}
													if (isset($v['options']['data']) && $v['options']['data'] != '') {        //jquery data
														$arr_options_val[$t_v[$v['options']['value']]]['data'] = [];
														foreach ($v['options']['data'] as $uu => $uuv) {
															$arr_options_val[$t_v[$v['options']['value']]]['data'][$uuv] = $t_v[$uuv];
														}
													}
													foreach ($arr_options_val as $o_v_i => $o_v) {
														if (count($o_v['parent']) > 1) {
															$arr_options_val[$o_v_i]['parent'] = '_' . implode('_', $o_v['parent']) . '_';
														} else {
															$arr_options_val[$o_v_i]['parent'] = $o_v['parent'][0];
														}
													}
													$this->fields['form'][$key2]['input'][$key]['options']['val'] = $arr_options_val;
												}
											}
										}
									} elseif ($this->display == 'edit') {
										$v = $this->fields['form'][$key2]['input'][$key];
										if (isset($v['options']['table'])) {    //下拉選單
											$select = '';
											if (isset($v['options']['parent']) && $v['options']['parent'] != '')
												$select = ' ,`' . $v['options']['parent'] . '`';
											if (isset($v['options']['data']) && $v['options']['data'] != '') {
												if (!is_array($v['options']['data'])) {
													$v['options']['data'] = [$v['options']['data']];
												}
												foreach ($v['options']['data'] as $uu => $uuv) {
													$select .= ' ,`' . $uuv . '`';
												}
											}
											$db_prefix_ = DB_PREFIX_;
											if (isset($v['options']['db_prefix_'])) {
												$db_prefix_ = $v['options']['db_prefix_'];
											}
											if (!is_array($v['options']['text'])) {
												$sql = 'SELECT `' . $v['options']['value'] . '`, `' . $v['options']['text'] . '`' . $select . '
													FROM `' . $db_prefix_ . $v['options']['table'] . '`';
											} else {
												$sql = 'SELECT `' . $v['options']['value'] . '`, `' . implode('`, `', $v['options']['text']) . '`' . $select . '
													FROM `' . $db_prefix_ . $v['options']['table'] . '`';
											}

											$no_arr = [];
											foreach ($v['options']['val'] as $jj => $ww) {
												$no_arr[] = $ww['val'];
											}
											if (is_array($v['val'])) {
												$sql .= ' WHERE `' . $v['options']['value'] . '` IN (' . implode(', ', ($v['val'])) . ')';
											} else {
												$sql .= ' WHERE `' . $v['options']['value'] . '` = ' . $v['val'] . '';
											}
											if (count($no_arr)) {
												$sql .= ' AND `' . $v['options']['value'] . '` NOT IN (' . implode(', ', $no_arr) . ')';
											}
											$arr_options_val = [];

											$arr_row = Db::rowSQL($sql,false,0,$this->conn);
											if ($arr_row > 0) {
												if (!isset($v['options']['text_divide']))
													$v['options']['text_divide'] = '%s - %s';
												foreach ($arr_row as $i => $t_v) {        //選單內值
													if (is_array($v['options']['text'])) {
														$arr_text = [];
														foreach ($v['options']['text'] as $i => $options_text) {
															$arr_text[] = $t_v[$options_text];
														}
														$text = vsprintf($v['options']['text_divide'], $arr_text);
													} else {
														$text = $t_v[$v['options']['text']];
													}
													if (!is_array($arr_options_val[$t_v[$v['options']['value']]])) {
														$arr_options_val[$t_v[$v['options']['value']]] = ['val' => $t_v[$v['options']['value']], 'text' => $text];    //父
													}
													if (isset($v['options']['parent']) && $v['options']['parent'] != '') {
														$arr_options_val[$t_v[$v['options']['value']]]['parent'][] = $t_v[$v['options']['parent']];
														if (isset($v['options']['parent_name']) && $v['options']['parent_name']) {
															$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent_name'];
														} else {
															$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent'];
														}
													}
													if (isset($v['options']['data']) && $v['options']['data'] != '') {        //jquery data
														$arr_options_val[$t_v[$v['options']['value']]]['data'] = [];
														foreach ($v['options']['data'] as $uu => $uuv) {
															$arr_options_val[$t_v[$v['options']['value']]]['data'][$uuv] = $t_v[$uuv];
														}
													}
													foreach ($arr_options_val as $o_v_i => $o_v) {
														if (count($o_v['parent']) > 1) {
															$arr_options_val[$o_v_i]['parent'] = '_' . implode('_', $o_v['parent']) . '_';
														} else {
															$arr_options_val[$o_v_i]['parent'] = $o_v['parent'][0];
														}
													}
													$this->fields['form'][$key2]['input'][$key]['options']['val'] = $arr_options_val;
												}
											}
										}
									}
								} else {
									$this->fields['form'][$key2]['input'][$key]['val'] = array_shift($arr_multiple_v[$v['name']]);    //預設值

								}
							}
						} elseif (($this->display != 'add') && ($key2 == $this->table) && ($index == $this->fields['index'])) {
							$this->_errors[]                   = $this->l('無法載入資料');
							$this->display_page_header_toolbar = false;
						}
					}

					if (count($arr_no_action) > 0) {

					}

					//資料庫組態
					if ((count($arr_configuration) > 0) || (count($arr_no_action) > 0)) {
						$arr_con = [];

						foreach ($arr_configuration as $i => $v) {
							$arr_con[$v] = Configuration::get($v);
						}

						foreach ($this->fields['form'][$key2]['input'] as $key => $v) {

							if ((isset($v['configuration']) && $v['configuration']) || (isset($v['no_action']) && $v['no_action'])) {
								if (isset($v['configuration']) && $v['configuration']) {

									if (($v['type'] == 'checkbox') || (isset($v['multiple']) && $v['multiple'])) {
										$arr_con[$v['name']] = $json->decode($arr_con[$v['name']]);
									}
									$this->fields['form'][$key2]['input'][$key]['val'] = $arr_con[$v['name']];    //預設值
									if (isset($v['left_index']) && !empty($v['left_index']['table'])) {
										$left_index[$v['left_index']['db_prefix_'] . $v['left_index']['table'] . $v['left_index']['key']] = $arr_con[$v['name']];
									}
								} elseif (isset($v['no_action']) && $v['no_action']) {
									/*
									$arr_no_act[$v['name']] = $v['val'];
									if(($v['type'] == 'checkbox') || (isset($v['multiple']) && $v['multiple'])){
										$json = new Services_JSON();
										$arr_no_act[$v['name']] = $json->decode($arr_no_act[$v['name']]);
									}
									$this->fields['form'][$key2]['input'][$key]['val'] = $arr_no_act[$v['name']];	//預設值
									if(isset($v['left_index']) && !empty($v['left_index']['table'])){
										$left_index[$v['left_index']['table'].$v['left_index']['key']] = $arr_no_act[$v['name']];
									}
									*/
								}

								//下拉選單
								if ($this->fields['form'][$key2]['input'][$key]['type'] == 'view') { //顯示 view
									//不複選
									$arr_values       = [];
									$arr_values_class = [];
									if (isset($this->fields['form'][$key2]['input'][$key]['values'])) {
										foreach ($this->fields['form'][$key2]['input'][$key]['values'] as $ii => $vv) {
											$arr_values[(string)$vv['value']]       = $vv['label'];
											$arr_values_class[(string)$vv['value']] = $vv['class'];
										}
									}
									$arr_options = [];
									if (isset($this->fields['form'][$key2]['input'][$key]['options'])) {
										foreach ($this->fields['form'][$key2]['input'][$key]['options']['val'] as $ii => $vv) {
											$arr_options[(string)$vv['val']]      = $vv['text'];
											$arr_options_data[(string)$vv['val']] = $vv['data'];
										}
									}
									if (!$v['multiple']) {//todo
										//print $this->fields['form'][$key2]['input'][$key]['label'].' = '.(string) $this->fields['form'][$key2]['input'][$key]['val'].' = '.'<br>';
										if ($this->fields['form'][$key2]['input'][$key]['in_table']) {    //存入資料表裡
											$arr_test = $json->decode($this->fields['form'][$key2]['input'][$key]['val']);
											if (is_array($arr_test)) {
												$this->fields['form'][$key2]['input'][$key]['val'] = $arr_test[0];
											}
										}
										if (count($arr_values) > 0) {
											$val                                                 = $arr_values[(string)$this->fields['form'][$key2]['input'][$key]['val']];
											$class                                               = $arr_values_class[(string)$this->fields['form'][$key2]['input'][$key]['val']];
											$this->fields['form'][$key2]['input'][$key]['val']   = ($val) ? $val : $this->fields['form'][$key2]['input'][$key]['val'];
											$this->fields['form'][$key2]['input'][$key]['class'] = ($class) ? $class : $this->fields['form'][$key2]['input'][$key]['class'];
										}
										//options
										if (count($arr_options) > 0) {
											$val                                               = $arr_options[(string)$this->fields['form'][$key2]['input'][$key]['val']];
											$this->fields['form'][$key2]['input'][$key]['val'] = ($val) ? $val : $this->fields['form'][$key2]['input'][$key]['val'];
										}
									} else {    //複選
										if (count($arr_values) > 0) {
											$arr_label       = [];
											$arr_label_class = [];
											foreach ($this->fields['form'][$key2]['input'][$key]['val'] as $ii => $vv) {
												$arr_label[]       = $arr_values[$vv];
												$arr_label_class[] = $arr_values_class[$vv];
											}
											$this->fields['form'][$key2]['input'][$key]['val']   = implode('、', $arr_label);
											$this->fields['form'][$key2]['input'][$key]['class'] = implode(' ', $arr_label_class);
											unset($arr_label);
											unset($arr_label_class);
										}
										//options
										if (count($arr_options) > 0) {
											$arr_text = [];
											foreach ($this->fields['form'][$key2]['input'][$key]['val'] as $ii => $vv) {
												$arr_text[] = $arr_options[$vv];
											}
											$this->fields['form'][$key2]['input'][$key]['val'] = implode("\n", $arr_text);
											unset($arr_text);
										}
									}

								} elseif ($this->display == 'edit') {
									$v = $this->fields['form'][$key2]['input'][$key];
									if (isset($v['options']['table'])) {    //下拉選單
										$select     = '';
										$db_prefix_ = DB_PREFIX_;
										if (isset($v['options']['db_prefix_'])) {
											$db_prefix_ = $v['options']['db_prefix_'];
										}
										if (isset($v['options']['parent']) && $v['options']['parent'] != '')
											$select = ' ,`' . $v['options']['parent'] . '`';
										if (isset($v['options']['data']) && $v['options']['data'] != '') {
											if (!is_array($v['options']['data'])) {
												$v['options']['data'] = [$v['options']['data']];
											}
											foreach ($v['options']['data'] as $uu => $uuv) {
												$select .= ' ,`' . $uuv . '`';
											}
										}
										if (!is_array($v['options']['text'])) {
											$sql = 'SELECT `' . $v['options']['value'] . '`, `' . $v['options']['text'] . '`' . $select . '
												FROM `' . $db_prefix_ . $v['options']['table'] . '`';
										} else {
											$sql = 'SELECT `' . $v['options']['value'] . '`, `' . implode('`, `', $v['options']['text']) . '`' . $select . '
												FROM `' . $db_prefix_ . $v['options']['table'] . '`';
										}

										$no_arr = [];
										foreach ($v['options']['val'] as $jj => $ww) {
											$no_arr[] = $v['options']['val'];
										}
										if (is_array($v['val'])) {
											$sql .= ' WHERE `' . $v['options']['value'] . '` IN (' . implode(', ', ($v['val'])) . ')';
										} else {
											$sql .= ' WHERE `' . $v['options']['value'] . '` = ' . $v['val'] . '';
										}
										if (count($no_arr)) {
											$sql .= ' AND `' . $v['options']['value'] . '` NOT IN (' . implode(', ', $no_arr) . ')';
										}
										$arr_options_val = [];
										$arr_row         = Db::rowSQL($sql,false,0,$this->conn);

										if (count($arr_row) > 0) {
											if (!isset($v['options']['text_divide']))
												$v['options']['text_divide'] = '%s - %s';
											foreach ($arr_row as $i => $t_v) {        //選單內值
												if (is_array($v['options']['text'])) {
													$arr_text = [];
													foreach ($v['options']['text'] as $i => $options_text) {
														$arr_text[] = $t_v[$options_text];
													}
													$text = vsprintf($v['options']['text_divide'], $arr_text);
												} else {
													$text = $t_v[$v['options']['text']];
												}
												if (!is_array($arr_options_val[$t_v[$v['options']['value']]])) {
													$arr_options_val[$t_v[$v['options']['value']]] = ['val' => $t_v[$v['options']['value']], 'text' => $text];    //父
												}
												if (isset($v['options']['parent']) && $v['options']['parent'] != '') {
													$arr_options_val[$t_v[$v['options']['value']]]['parent'][] = $t_v[$v['options']['parent']];
													if (isset($v['options']['parent_name']) && $v['options']['parent_name']) {
														$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent_name'];
													} else {
														$arr_options_val[$t_v[$v['options']['value']]]['parent_name'] = $v['options']['parent'];
													}
												}
												if (isset($v['options']['data']) && $v['options']['data'] != '') {        //jquery data
													$arr_options_val[$t_v[$v['options']['value']]]['data'] = [];
													foreach ($v['options']['data'] as $uu => $uuv) {
														$arr_options_val[$t_v[$v['options']['value']]]['data'][$uuv] = $t_v[$uuv];
													}
												}
												foreach ($arr_options_val as $o_v_i => $o_v) {
													if (count($o_v['parent']) > 1) {
														$arr_options_val[$o_v_i]['parent'] = '_' . implode('_', $o_v['parent']) . '_';
													} else {
														$arr_options_val[$o_v_i]['parent'] = $o_v['parent'][0];
													}
												}

												$this->fields['form'][$key2]['input'][$key]['options']['val'] = $arr_options_val;
											}
										}
									}
								}
							}
						}
					}
					//照相
					$this->arr_take_photo_data = [];

					if (count($arr_take_photo)) {
						foreach ($arr_take_photo as $key => $v) {
							if (!empty($v['table'])) {
								$db_prefix_ = DB_PREFIX_;
								if (isset($v['db_prefix_'])) {
									$db_prefix_ = $v['db_prefix_'];
								}
								$sql      = 'SELECT *
									FROM `' . $db_prefix_ . $v['table'] . '`
									WHERE `' . $v['left_index'] . '` = ' . $must[$v['must']];
								$row      = Db::rowSQL($sql,false,0,$this->conn);
								$arr_data = [];
								foreach ($row as $i => $w) {
									$arr_data[] = [
										'index' => $w[$v['key']],
										'data'  => $w[$v['val']],
										'type'  => $this->display,
									];
								}
								$this->arr_take_photo_data[$v['name']] = $arr_data;
							}
						}
					}

					//簽名
					$this->arr_signature_data = [];
					if (count($arr_signature)) {
						foreach ($arr_signature as $key => $v) {
							if (!empty($v['table'])) {
								$db_prefix_ = DB_PREFIX_;
								if (isset($v['db_prefix_'])) {
									$db_prefix_ = $v['db_prefix_'];
								}
								$sql                                  = 'SELECT *
									FROM `' . $db_prefix_ . $v['table'] . '`
									WHERE `' . $v['left_index'] . '` = ' . $must[$v['must']] . '
									LIMIT 0, 1';
								$row                                  = Db::rowSQL($sql,true,0,$this->conn);
								$this->arr_signature_data[$v['name']] = [
									'index' => $row[$v['key']],
									'data'  => $row['img_data'],
									'type'  => $this->display,
								];
							}
						}
					}
				}
			}
		}

		$Context = Context::getContext();
		$Context->smarty->assign([
			'post_code' => mt_rand(0, 1000000),
		]);
	}

	public function insertDB()
	{
		$must     = [];
		$num      = 0;
		$arr_form = [];
		foreach ($this->fields['form'] as $key => $form) {
			if (empty($form['table']))
				$form['table'] = $this->table;
			$db_prefix_ = DB_PREFIX_;
			if (isset($this->fields['form'][$key]['db_prefix_'])) {
				$db_prefix_ = $this->fields['form'][$key]['db_prefix_'];
			}
			$arr_form[$db_prefix_ . $form['table']][] = $key;
		}
		foreach ($arr_form as $table => $table_key) {    //$table 以合併db_prefix_
			$index          = '';
			$index_POST     = [];
			$arr_set        = [];
			$arr_post       = [];
			$arr_name       = [];
			$arr_take_photo = [];    //照相
			$arr_signature  = [];    //簽章
			foreach ($table_key as $k => $tab) {
				foreach ($this->fields['form'][$tab]['input'] as $i => $v) {
					if ($v['auto_datetime'] == 'add') {    //有自動加入時間 (新增)
						$_POST[$v['name']]                               = now();
						$this->fields['form'][$tab]['input'][$i]['type'] = 'datetime';
						$v['type']                                       = 'datetime';
					}

					if (!empty($this->fields['form'][$tab]['input'][$i]['options']['table']) && $v['auto_id'] == 'add') {
						$this->fields['form'][$tab]['input'][$i]['type'] = 'select';
						$v['type']                                       = 'select';
						//有自動加入管理者ID
						if ($this->fields['form'][$tab]['input'][$i]['options']['table'] == 'admin') {
							$_POST[$v['name']] = $_SESSION['id_admin'];
						}
						//有自動加入使用者ID
						if ($this->fields['form'][$tab]['input'][$i]['options']['table'] == 'member') {
							$_POST[$v['name']] = $_SESSION['id_member'];
						}
					}
					if (!empty($v['name']) && !in_array($v['name'], $arr_name)) {
						$arr_name[] = $v['name'];
						$post       = Tools::getValue($v['name']);
						if (!empty($v['filter_key'])) {
							$arr_filter_key = explode('!', $v['filter_key']);
							if (count($arr_filter_key) == 1) {
								$name = $arr_filter_key[0];        //SELEST搜尋表
							} else {
								$name = $arr_filter_key[1];        //SELEST搜尋表
							}
						} else {
							$name = $v['name'];
						}
						if (isset($v['left_index']) && $v['left_index']['table'] && $v['left_index']['key']) {
							$index_POST[$v['left_index']['db_prefix_'] . $v['left_index']['table'] . $v['left_index']['key']] = $post;
						}
						if (is_array($post)) {
							$post = json_encode($post, JSON_UNESCAPED_UNICODE);
						}
//						$post = str_repeat("'", "\'", $post);
						if (isset($v['configuration']) && $v['configuration'])
							$num += Configuration::set($name, $post);        //組態
						if ((!isset($v['index']) && $this->fields['index'] != $name) || !empty($v['must'])) {        //索引鍵不加入
							if ($v['type'] != 'take_photo' && $v['type'] != 'confirm-password' && $v['type'] != 'view' && $v['type'] != 'hr' && (!isset($v['no_action']) || !$v['no_action']) && !isset($v['view']) && !isset($v['configuration'])) {
								$arr_set[] = $name;    //非確認密碼
								if ($post !== null && ($post === 0 || $post === '0' || !empty($post)) && $post !== '') {
									$arr_post[] = '\'' . $post . '\'';
								} elseif (!empty($index_POST[$table . $v['name']])) {
									$arr_post[] = '\'' . $index_POST[$table . $v['name']] . '\'';
								} elseif (!empty($v['must'])) {
									$arr_post[] = $must[$v['must']];
								} elseif (($v['type'] == 'date') || ($v['type'] == 'time') || ($v['type'] == 'datetime') || ($v['type'] == 'number') || ($v['type'] == 'hidden')) {
									$arr_post[] = 'NULL';
								} elseif ($post === null) {
									$arr_post[] = 'NULL';
								} else {
									$arr_post[] = 'NULL';
								}
							}
							if ($v['type'] == 'take_photo') {    //照相
								$this->fields['form'][$tab]['input'][$i]['no_action'] = true;
								$v['key']['name']                                     = $v['name'];
								$arr_take_photo[]                                     = $v['key'];
							}

							if ($v['type'] == 'signature') {    //簽名
								$this->fields['form'][$tab]['input'][$i]['no_action'] = true;
								$v['key']['name']                                     = $v['name'];
								$arr_signature[]                                      = $v['key'];
							}
						} else {
							$index = $name;
						}
					}
				}
			}
			if (count($arr_set) > 0) {

				$sql                         = 'INSERT INTO `' . $db_prefix_ . $table . '`
					(`' . implode('`, `', $arr_set) . '`) VALUES (' . implode(', ', $arr_post) . ')';
				//因切換資料庫而變動的參數
//				$db                          = new db_mysql($sql);
                $db                          = new db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm',$this->conn);
				$this->index_id              = $db->num();
				$must[$table . '.' . $index] = $this->index_id;
				$_POST[$index]               = (string)$this->index_id;
				$this->must                  = $must;
			}
			$num += $this->index_id;
		}
		if (count($arr_take_photo) > 0) {    //照相
			foreach ($arr_take_photo as $key => $v) {
				$name = Tools::getValue($v['name']);
				if (is_array($name)) {
					foreach ($name as $i => $n) {
						$db_prefix_ = DB_PREFIX_;
						if (isset($v['db_prefix'])) {
							$db_prefix_ = $v['db_prefix'];
						}
						if (!empty($must[$v['must']]) && !empty($n)) {
							$sql = 'INSERT INTO `' . $db_prefix_ . $v['table'] . '`
								(`' . $v['left_index'] . '`, `' . $v['val'] . '`) VALUES (\'' . $must[$v['must']] . '\', \'' . $n . '\')';
                            //因切換資料庫而變動的參數
//				$db                          = new db_mysql($sql);
                            $db                          = new db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm',$this->conn);
						}
					}
				} else {
					if (!empty($must[$v['must']]) && !empty($name)) {
						$db_prefix_ = DB_PREFIX_;
						if (isset($v['db_prefix'])) {
							$db_prefix_ = $v['db_prefix'];
						}
						$sql = 'INSERT INTO `' . $db_prefix_ . $v['table'] . '`
							(`' . $v['left_index'] . '`, `' . $v['val'] . '`) VALUES (\'' . $must[$v['must']] . '\', \'' . $name . '\')';
                        //因切換資料庫而變動的參數
//				$db                          = new db_mysql($sql);
                        $db                          = new db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm',$this->conn);
					}
				}
			}
		}

		if (count($arr_signature) > 0) {    //簽名
			$json = new JSON();
			foreach ($arr_signature as $key => $v) {
				$img_data   = Tools::getValue($v['name']);
				$db_prefix_ = DB_PREFIX_;
				if (isset($v['db_prefix'])) {
					$db_prefix_ = $v['db_prefix'];
				}
				if (!empty($must[$v['must']]) && !empty($img_data)) {    //空的就是新增
					if (empty($v['datetime'])) {
						$v['datetime'] = now();
					}
					$arr_content = [];
					foreach ($v['content'] as $key => $post) {
						$arr_content[$post] = $_POST[$post];
					}
					$content         = $json->encode($arr_content);
					$content         = str_replace(':null', ':\'\'', $content);
					$HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
					$hash            = Signature::hash($content, $img_data, $HTTP_USER_AGENT, $v['datetime']);
					$sql             = sprintf('INSERT INTO `' . $db_prefix_ . $v['table'] . '`
							(`' . $v['left_index'] . '`, `img_data`, `content`, `HTTP_USER_AGENT`, `datetime`, `hash`) VALUES (%s, %s, %s, %s, %s, %s)',
						GetSQL($must[$v['must']], 'text'),
						GetSQL($img_data, 'text'),
						GetSQL($content, 'text'),
						GetSQL($HTTP_USER_AGENT, 'text'),
						GetSQL($v['datetime'], 'text'),
						GetSQL($hash, 'text'));
                    Db::rowSQL($sql,false,0,$this->conn);
				}
			}
		}
		return $num;
	}

	public function updateDB()
	{
		$num            = 0;
		$must           = [];
		$this->index_id = Tools::getValue($this->fields['index']);
		$arr_take_photo = [];
		foreach ($this->fields['form'] as $key => $form) {
			if (empty($form['table']))
				$form['table'] = $this->table;

			$db_prefix_ = DB_PREFIX_;
			if (isset($form['db_prefix_'])) {
				$db_prefix_ = $form['db_prefix_'];
			}

			$arr_form[$db_prefix_ . $form['table']][] = $key;
			foreach ($this->fields['form'][$key]['input'] as $key2 => $v) {
				if ($this->fields['form'][$key]['input'][$key2]['type'] == 'take_photo') {
					$this->fields['form'][$key]['input'][$key2]['key']['name'] = $this->fields['form'][$key]['input'][$key2]['name'];
					$this->fields['form'][$key]['input'][$key2]['no_action']   = true;
					$arr_take_photo[]                                          = $this->fields['form'][$key]['input'][$key2]['key'];
				}

				if ($v['type'] == 'signature') {    //簽名
					$this->fields['form'][$key]['input'][$key2]['key']['name'] = $this->fields['form'][$key]['input'][$key2]['name'];
					$this->fields['form'][$key]['input'][$key2]['no_action']   = true;
					$arr_signature[]                                           = $this->fields['form'][$key]['input'][$key2]['key'];
				}
			}
		}
		foreach ($arr_form as $table => $table_key) {
			$index    = '';
			$arr_set  = [];
			$arr_name = [];
			foreach ($table_key as $k => $tab) {
				foreach ($this->fields['form'][$tab]['input'] as $i => $v) {
					if ($v['auto_datetime'] == 'edit') {    //有自動加入時間 (修改)
						$_POST[$v['name']]                               = now();
						$this->fields['form'][$tab]['input'][$i]['type'] = 'datetime';
						$v['type']                                       = 'datetime';
					}

					if (!empty($this->fields['form'][$tab]['input'][$i]['options']['table']) && $v['auto_id'] == 'edit') {
						$this->fields['form'][$tab]['input'][$i]['type'] = 'select';
						$v['type']                                       = 'select';
						//有自動加入管理者ID
						if ($this->fields['form'][$tab]['input'][$i]['options']['table'] == 'admin') {
							$_POST[$v['name']] = $_SESSION['id_admin'];
						}
						//有自動加入使用者ID
						if ($this->fields['form'][$tab]['input'][$i]['options']['table'] == 'member') {
							$_POST[$v['name']] = $_SESSION['id_member'];
						}
					}
					if (!empty($v['name']) && !in_array($v['name'], $arr_name)) {
						$arr_name[] = $v['name'];
						$post       = Tools::getValue($v['name']);

						if (!empty($v['filter_key'])) {
							$arr_filter_key = explode('!', $v['filter_key']);
							if (count($arr_filter_key) == 1) {
								$name = $arr_filter_key[0];            //SELEST搜尋表
							} else {
								$name = $arr_filter_key[1];            //SELEST搜尋表
							}
						} else {
							$name = $v['name'];
						}
						if (is_array($post)) {
							$post = json_encode($post, JSON_UNESCAPED_UNICODE);
						}

//						$post = str_repeat("'", "\'", $post);
						if (isset($v['configuration']) && $v['configuration'])
							$num += Configuration::set($name, $post);            //組態
						if (!empty($v['name']) && (!isset($v['no_action']) || !$v['no_action']) && $v['type'] != 'view' && $v['type'] != 'hr' && !isset($v['view']) && !isset($v['configuration'])) {
							if (empty($index) && ($v['index'] || $this->fields['index'] == $name)) {      //索引鍵不更新
								$index                       = $name;
								$must[$table . '.' . $index] = $post;
							} else {
								if ($v['type'] == 'change-password') {      //更改密碼
									if (!empty($post)) {      //查看有無輸入密碼 有就修改
										$arr_set[] = '`' . $name . '` = \'' . $post . '\'';
									}
								} else {
									if ($post !== null && ($post === 0 || $post === '0' || !empty($post)) && $post !== '') {
										$post = '\'' . $post . '\'';
									} elseif (!empty($index_POST[$form['table'] . $v['name']])) {
										$post = '\'' . $index_POST[$form['table'] . $v['name']] . '\'';
									} elseif (!empty($v['must'])) {
										$post = $must[$v['must']];
									} elseif (($v['type'] == 'date') || ($v['type'] == 'time') || ($v['type'] == 'datetime') || ($v['type'] == 'number') || ($v['type'] == 'hidden')) {
										$post = 'NULL';
									} elseif ($post === null) {
										$post = 'NULL';
									} else {
//										$post = '\'\'';
                                        $post = 'NULL';
									}
									if ($v['type'] != 'view')
										$arr_set[] = '`' . $name . '` = ' . $post;
								}
							}
						}
					}
				}

			}

			if (count($arr_set) > 0) {
				$sql = 'UPDATE `' . $table . '`
					SET ' . implode(', ', $arr_set) . '
					WHERE `' . $index . '` = \'' . Tools::getValue($index) . '\'';
                Db::rowSQL($sql,false,0,$this->conn);
                //因切換資料庫而變動的參數
//				$db                          = new db_mysql($sql);
                $db                          = new db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm',$this->conn);
				$num += Db::getContext()->num();
			}
		}

		if (count($arr_take_photo) > 0) {        //照相
			foreach ($arr_take_photo as $key => $v) {
				$id         = Tools::getValue('id_img_' . $v['name']);
				$name       = Tools::getValue($v['name']);
				$db_prefix_ = DB_PREFIX_;
				if (isset($v['db_prefix'])) {
					$db_prefix_ = $v['db_prefix'];
				}
				if (is_array($id)) {
					//刪除不存在的
					$sql = 'DELETE FROM `' . $db_prefix_ . $v['table'] . '` WHERE `' . $v['left_index'] . '` = ' . $must[$v['must']] . ' AND `' . $v['key'] . '` NOT IN(' . implode(', ', $id) . ')';
                    Db::rowSQL($sql,false,0,$this->conn);
					foreach ($id as $i => $d) {
						if (empty($d) && !empty($name[$i])) {    //空的就是新增
							$sql = 'INSERT INTO `' . $db_prefix_ . $v['table'] . '`
							(`' . $v['left_index'] . '`, `' . $v['val'] . '`) VALUES (\'' . $must[$v['must']] . '\', \'' . $name[$i] . '\')';
                            Db::rowSQL($sql,false,0,$this->conn);
						}
					}
				} else {    //全部刪除
					if (empty($id)) {
						$sql = 'DELETE FROM `' . $db_prefix_ . $v['table'] . '` WHERE `' . $v['left_index'] . '` = ' . $must[$v['must']];
                        Db::rowSQL($sql,false,0,$this->conn);
					}
					if (empty($id) && !empty($name)) {    //空的就是新增
						$sql = 'INSERT INTO `' . DB_PREFIX_ . $v['table'] . '`
							(`' . $v['left_index'] . '`, `' . $v['val'] . '`) VALUES (\'' . $must[$v['must']] . '\', \'' . $name . '\')';
                        Db::rowSQL($sql,false,0,$this->conn);
					}

				}
			}
		}

		if (count($arr_signature) > 0) {    //簽名
			$json = new JSON();
			foreach ($arr_signature as $key => $v) {
				$img_data   = Tools::getValue($v['name']);
				$db_prefix_ = DB_PREFIX_;
				if (isset($v['db_prefix'])) {
					$db_prefix_ = $v['db_prefix'];
				}
				if (!empty($must[$v['must']]) && !empty($img_data)) {    //空的就是新增
					if (empty($v['datetime'])) {
						$v['datetime'] = now();
					}
					$arr_content = [];
					foreach ($v['content'] as $key => $post) {
						$arr_content[$post] = $_POST[$post];
					}
					$content         = $json->encode($arr_content);
					$content         = str_replace(':null', ':""', $content);
					$HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
					$hash            = Signature::hash($content, $img_data, $HTTP_USER_AGENT, $v['datetime']);
					$sql             = sprintf('INSERT INTO `' . $db_prefix_ . $v['table'] . '`
							(`' . $v['left_index'] . '`, `img_data`, `content`, `HTTP_USER_AGENT`, `datetime`, `hash`) VALUES (%s, %s, %s, %s, %s, %s)',
						GetSQL($must[$v['must']], 'text'),
						GetSQL($img_data, 'text'),
						GetSQL($content, 'text'),
						GetSQL($HTTP_USER_AGENT, 'text'),
						GetSQL($v['datetime'], 'text'),
						GetSQL($hash, 'text'));
                    Db::rowSQL($sql,false,0,$this->conn);
				}
			}
		}

//		foreach ($this->fields['form'] as $key => $form) {
//			$index = '';
//			$arr_set = array();
//			$arr_name = array();
//			$arr_configuration = array();    //組態
//			if (empty($form['table'])) $form['table'] = $this->table;
//
//			foreach ($form['input'] as $i => $v) {
//				if (!empty($v['name']) && !in_array($v['name'], $arr_name)) {
//					$arr_name[] = $v['name'];
//					$post = Tools::getValue($v['name']);
//					if (!empty($v['filter_key'])) {
//						$arr_filter_key = explode('!', $v['filter_key']);
//						if (count($arr_filter_key) == 1) {
//							$name = $arr_filter_key[0];            //SELEST搜尋表
//						} else {
//							$name = $arr_filter_key[1];            //SELEST搜尋表
//						}
//					} else {
//						$name = $v['name'];
//					}
//					if (is_array($post)) {
//						$post = json_encode($post, JSON_UNESCAPED_UNICODE);
//					}
//					if (isset($v['configuration']) && $v['configuration']) $num += Configuration::set($name, $post);            //組態
//					if (!empty($v['name']) && (!isset($v['no_action']) || !$v['no_action']) && $v['type'] != 'view' && $v['type'] != 'hr' && !isset($v['view']) && !isset($v['configuration'])) {
//						if (empty($index) && ($v['index'] || $this->fields['index'] == $name)) {      //索引鍵不更新
//							$index = $name;
//						} else {
//							if ($v['type'] == 'change-password') {      //更改密碼
//								if (!empty($post)) {      //查看有無輸入密碼 有就修改
//									$arr_set[] = '`' . $name . '` = \'' . $post . '\'';
//								}
//							} else {
//								$post = '\'' . $post . '\'';
//								if ($v['type'] != 'view') $arr_set[] = '`' . $name . '` = ' . $post;
//							}
//						}
//					}
//				}
//			}
//
//			if (count($arr_set) > 0) {
//				$sql = 'UPDATE `' . DB_PREFIX_ . $form['table'] . '`
//					SET ' . implode(', ', $arr_set) . '
//					WHERE `' . $index . '` = \'' . Tools::getValue($index) . '\'';
//				$db = new db_mysql($sql);
//				$num += $db->num();
//			}
//		}
		return $num;
	}

	public function deleteDB()
	{
		if (!isset($this->fields['index'])) {
			foreach ($this->fields['form'] as $key => $form) {
				foreach ($form['input'] as $i => $v) {
					if (!empty($v['name'])) {
						$name = substr($v['name'], strchr($v['name'], '!'));
						if ($v['index']) {    //索引鍵不更新
							$this->fields['index'] = $name;
							break;
						}
					}
				}
			}
		}

		$db_prefix_ = DB_PREFIX_;
		if (isset($this->fields['db_prefix_'])) {
			$db_prefix_ = $this->fields['db_prefix_'];
		}

		$this->index_id = Tools::getValue($this->fields['index']);
		if (is_array($this->index_id)) {
			$WHERE = ' IN (' . implode(', ', $this->index_id) . ')';
		} else {
			$WHERE = ' = ' . $this->index_id;
		}

		$sql = 'DELETE FROM `' . $db_prefix_ . $this->table . '` WHERE `' . $this->fields['index'] . '` ' . $WHERE;

        Db::rowSQL($sql,false,0,$this->conn);
		return Db::getContext()->num();
	}


	public function renderCalendar()
	{
		$Context = Context::getContext();
		if (empty($this->fields['calendar']['header_right'])) {
			$this->fields['calendar']['header_right'] = 'month,agendaWeek,agendaDay,listMonth';    //month:月曆, agendaWeek:週顯示, agendaDay:天顯示, listMonth:工作列表
		}
		if (empty($this->fields['calendar']['dayNamesShort'])) {
			$this->fields['calendar']['dayNamesShort'] = '[\'日\', \'一\', \'二\', \'三\', \'四\', \'五\', \'六\']';
		}
		if (empty($this->fields['calendar']['business_hours_start'])) {
			$this->fields['calendar']['business_hours_start'] = '00:00';
		}
		if (empty($this->fields['calendar']['business_hours_end'])) {
			$this->fields['calendar']['business_hours_end'] = '24:00';
		}
		$Context->smarty->assign(['fields' => $this->fields]);
	}

	public function renderList()        //todo 更改到 controllers
	{
		$json = new Json();
		$this->ini();
		$filter_sql  = [];
		$filter_sql2 = [];
		$arr_related = [];    //關聯key
		$related_var = [];
//		$arr_values = array();
		if (!empty($this->fields['list']) && is_array($this->fields['list'])) {
			if (!isset($this->fields['list_heading_display']))
				$this->fields['list_heading_display'] = true;
			if (!isset($this->fields['list_body_display']))
				$this->fields['list_body_display'] = true;
			if (!isset($this->fields['list_footer_display']))
				$this->fields['list_footer_display'] = true;
			if (!isset($this->list_id)) {
				$this->list_id = $this->table;
			}

			if (!in_array($this->fields['index'], array_keys($this->fields['list'])) && !in_array($this->_as . '!' . $this->fields['index'], array_keys($this->fields['list']))) {        //列表內沒有索引鍵
				//加入索引鍵
				$this->fields['list'][$this->fields['index']] = [
					'index' => true,
					'type'  => 'hidden',
				];
			}
			$arr_HAVING_key = [];
			$arr_SELECT     = [];
			foreach ($this->fields['list'] as $key_i => $se_v) {
				if (!empty($se_v['filter_key'])) {
					$arr_se_v = explode('!', $se_v['filter_key']);
					if (count($arr_se_v) == 1) {
						$arr_SELECT[] = '`' . $arr_se_v[0] . '` AS ' . $key_i;
					} else {
						$arr_SELECT[] = $arr_se_v[0] . '.`' . $arr_se_v[1] . '` AS ' . $key_i;
					}
				} elseif (!empty($se_v['filter_sql'])) {
					$arr_SELECT[]     = $se_v['filter_sql'] . ' AS ' . $key_i;
					$arr_HAVING_key[] = $key_i;
				} elseif (empty($se_v['no_action'])) {        //2018-06-29 加入顯示、隱藏欄位
					$arr_SELECT[] = '`' . $key_i . '`';
				}
			}

			$as = '';
			if (!empty($this->_as))
				$as = ' AS ' . $this->_as;
			$db_prefix_ = DB_PREFIX_;
			if (isset($this->fields['db_prefix_'])) {
				$db_prefix_ = $this->fields['db_prefix_'];
			}
			$sql = 'SELECT ' . implode(', ', $arr_SELECT) . '
				FROM `' . $db_prefix_ . $this->table . '`' . $as . $this->_join;    //主要list
			foreach ($this->fields['list'] as $i => $v) {
				if ($i == 'position') {    //手機或APP取消顯示列表
					$this->fields['list_num']      = 0;        //可以排順序就不能有頁數(會混亂)
					$this->fields['show_list_num'] = false;
				}
				if ($i == 'active') {    //啟用狀態
					unset($this->fields['list'][$i]['multiple']);
					if (!isset($this->fields['list'][$i]['values'])) {
						$this->fields['list'][$i]['values'] = [
							[
								'class' => 'active_off',
								'value' => 0,
								'title' => $this->l('關閉'),
							],
							[
								'class' => 'active_on',
								'value' => 1,
								'title' => $this->l('啟用'),
							],
						];
					}
				}
				if (isset($v['key'])) {        //下拉捲軸
					$related_where  = ' WHERE TRUE';
					$related_select = '';
					$text_select    = '';
					if (!empty($v['key']['where']))
						$related_where .= $v['key']['where'];
					if (!empty($v['key']['parent']))
						$related_select = ', `' . $v['key']['parent'] . '`';
					$related_order = '';
					if (!empty($v['key']['order'])) {
						$related_order = ' ORDER BY ' . $v['key']['order'];
					}
					$arr_related[$i]               = [];
					$arr_related[$i]['key']        = $v['key']['key'];
					$arr_related[$i]['key_v']      = $v['key']['val'];
					$arr_related[$i]['val_divide'] = $v['key']['val_divide'];
					$arr_related[$i]['data']       = $v['key']['data'];
					if (!empty($v['key']['parent']))
						$arr_related[$i]['key_p'] = $v['key']['parent'];

					if (is_array($arr_related[$i]['key_v'])) {
						$val_select = ', `' . implode('`, `', $arr_related[$i]['key_v']) . '`';
					} else {
						$val_select = ', `' . $arr_related[$i]['key_v'] . '`';
					}

					if (!empty($v['key']['text'])) {
						$arr_related[$i]['text'] = $v['key']['text'];
						if (is_array($arr_related[$i]['text'])) {
							$text_select = ', `' . implode('`, `', $arr_related[$i]['text']) . '`';
						} else {
							$text_select = ', `' . $arr_related[$i]['text'] . '`';
						}
					}
					if (!empty($v['key']['data'])) {
						$arr_related[$i]['data'] = $v['key']['data'];
						if (is_array($arr_related[$i]['data'])) {
							$text_select = ', `' . implode('`, `', $arr_related[$i]['data']) . '`';
						} else {
							$text_select = ', `' . $arr_related[$i]['data'] . '`';
						}
					}
					if (!empty($v['key']['class'])) {
						$arr_related[$i]['class'] = $v['key']['class'];
						if (is_array($arr_related[$i]['class'])) {
							$text_select = ', `' . implode('`, `', $arr_related[$i]['class']) . '`';
						} else {
							$text_select = ', `' . $arr_related[$i]['class'] . '`';
						}
					}
					if (!empty($v['key']['text_divide']))
						$arr_related[$i]['text_divide'] = $v['key']['text_divide'];
					$t_db_prefix_ = DB_PREFIX_;
					if (isset($v['key']['db_prefix_'])) {
						$t_db_prefix_ = $v['key']['db_prefix_'];
					}
					$filter_sql[$i] = 'SELECT `' . $v['key']['key'] . '`' . $val_select . $related_select . $text_select . '
							FROM `' . $t_db_prefix_ . $v['key']['table'] . '`' . $related_where . $related_order;    //有篩選


					$filter_sql2[$i] = 'SELECT `' . $v['key']['key'] . '`' . $val_select . $related_select . $text_select . '
							FROM `' . $t_db_prefix_ . $v['key']['table'] . '`';                          //全顯示做下拉捲軸
				}
//				if (isset($v['values'])) {
//					$arr_values[$i] = array();
//					$arr_values[$i]['value'] = $v['values'];
//				}

				if (isset($v['filter']) && !empty($v['filter'])) {
					$this->fields['list_filter'][] = $i;
					if ($v['values'] || $v['key']) {
						$this->fields['list_filter_local'][] = $i;
					}
				}

				if (isset($v['index']) && $v['index'] && empty($this->fields['index']))
					$this->fields['index'] = $i;
				if (isset($v['index2']) && $v['index2'] && empty($this->fields['index2']))
					$this->fields['index2'] = $i;
			}
			$sql2      = $sql;
			$arr_order = [];
			/*
			if(in_array('position', array_keys($this->fields['list']))){	//有順序

			}
			*/
			/*
			 * 一頁顯示數量 (每一頁)
			 */

			if (Tools::isSubmit($this->list_id . 'Filter_m') && (trim(Tools::getValue($this->list_id . 'Filter_m')) != '') && isints(trim(Tools::getValue($this->list_id . 'Filter_m')))) {
				Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'] = trim(Tools::getValue($this->list_id . 'Filter_m'));
			} elseif (empty(Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'])) {
				Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'] = $this->fields['list_num'];
			}

			if ($this->fields['show_list_num'])
				$this->fields['list_num'] = Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'];

			if (empty($this->fields['where'])) {
				$this->fields['where'] = ' WHERE TRUE';
			} else {
				$this->fields['where'] = ' WHERE TRUE' . $this->fields['where'];
			}

			if (count($this->fields['list_filter']) > 0) {
				foreach ($this->fields['list_filter'] as $ki => $list_f) {
					$list_f_key = $list_f;
					$list_f     = $this->list_id . 'Filter_' . $list_f;
					if (Tools::isSubmit($list_f)) {        //有值用GET
						$T_list_f = Tools::getValue($list_f);
						if (trim($T_list_f) == '' && !is_array($T_list_f)) {        //清除
							unset($_GET[$list_f]);
							unset($_POST[$list_f]);
							Context::getContext()->cookie['list_filter'][$list_f] = '';
						} else {                            //設定

							if (is_array($T_list_f)) {
								Context::getContext()->cookie['list_filter'][$list_f] = $T_list_f;
							} else {
								Context::getContext()->cookie['list_filter'][$list_f] = trim($T_list_f);
							}
						}
					} elseif (!Tools::isSubmit($this->list_id . 'Filter_m')) {                    //沒有值用COOKIE
						$list_f_v = Context::getContext()->cookie['list_filter'][$list_f];
						switch (Configuration::get('list_Cookie')) {
							case 2:    //全部
							case'2':
								$_GET[$list_f] = $list_f_v;
							break;
							case 1:    //局部
							case'1':
								if (in_array($list_f_key, $this->fields['list_filter_local'])) {
									$_GET[$list_f] = $list_f_v;
								}
							break;
							case 0:
							case'0':    //關閉
							default:
							break;
						}
					} else {
						unset($_GET[$list_f]);
						unset($_POST[$list_f]);
						Context::getContext()->cookie['list_filter'][$list_f] = '';
					}
					$cookie_list_f = $_GET[$list_f];


					if ($cookie_list_f != '') {
						$where  = '';
						$having = '';
						$filter = $this->fields['list'][$list_f_key]['filter'];

						$this->fields['list'][$list_f_key]['string_format'] = $cookie_list_f;
						if ($this->fields['list'][$list_f_key]['no_filter'] != true) {
							if (isset($this->fields['list'][$list_f_key]['replace']) && is_array($this->fields['list'][$list_f_key]['replace'])) {
								$replace = $this->fields['list'][$list_f_key]['replace'];
								if (is_array($cookie_list_f)) {
									foreach ($cookie_list_f as $i => $v) {
										if ($v == $replace[1]) {
											$cookie_list_f[$i] = $replace[0];
										}
									}
								} else {
									if ($cookie_list_f == $replace[1]) {
										$cookie_list_f = $replace[0];
									}
								}
							}


							if (isset($this->fields['list'][$list_f_key]['filter_sql'])) {
								$where_i = '(' . $this->fields['list'][$list_f_key]['filter_sql'] . ')';
							} else {
								$key = $list_f_key;
								if ($this->fields['list'][$list_f_key]['filter_key'])
									$key = $this->fields['list'][$list_f_key]['filter_key'];
								$arr_filter_key = explode('!', $key);
								if (count($arr_filter_key) == 1) {
									$where_i = '`' . str_replace($this->list_id . 'Filter_', '', $arr_filter_key[0]) . '`';
								} else {
									$where_i = str_replace($this->list_id . 'Filter_', '', $arr_filter_key[0]) . '.`' . $arr_filter_key[1] . '`';
								}
							}
							//有選項

							if ((isset($this->fields['list'][$list_f_key]['key']) || isset($this->fields['list'][$list_f_key]['values']))) {

								if (isset($this->fields['list'][$list_f_key]['operation']) && !empty($this->fields['list'][$list_f_key]['operation'])) {
									$operation = $this->fields['list'][$list_f_key]['operation'];
								} else {
									$operation = 'OR';
								}

								if (isset($this->fields['list'][$list_f_key]['in_table']) && $this->fields['list'][$list_f_key]['in_table']) {    //陣列存在資料表裡面
									if ($this->fields['list'][$list_f_key]['filter'] == 'text') {        //文字搜尋
										if (isset($this->fields['list'][$list_f_key]['key'])) {        //下拉選單	todo
											$cookie_list_f_inv = $cookie_list_f;
										}
										if (isset($this->fields['list'][$list_f_key]['values'])) {    //todo
											$cookie_list_f_inv = $cookie_list_f;
										}
										if (!is_array($cookie_list_f_inv)) {
											if (in_array($list_f_key, $arr_HAVING_key)) {
												$having .= sprintf(' AND ' . $where_i . ' LIKE %s', GetSQL($cookie_list_f_inv, 'defined', '\'%"' . _addslashes($cookie_list_f_inv) . '"%\'', '"" AND FALSE'));
											} else {
												$where .= sprintf(' AND ' . $where_i . ' LIKE %s', GetSQL($cookie_list_f_inv, 'defined', '\'%"' . _addslashes($cookie_list_f_inv) . '"%\'', '"" AND FALSE'));
											}
										} else {
											$arr_where = [];
											foreach ($cookie_list_f_inv as $i => $v) {
												if ($v != '')
													$arr_where[] = sprintf($where_i . ' LIKE %s', GetSQL($v, 'defined', '\'%"' . _addslashes($v) . '"%\'', '"" AND FALSE'));
											}
											if (count($arr_where) > 0) {
												$where .= ' AND ((' . implode(') ' . $operation . ' (', $arr_where) . '))';
												if (in_array($list_f_key, $arr_HAVING_key)) {
													$having .= ' AND ((' . implode(') ' . $operation . ' (', $arr_where) . '))';
												} else {
													$where .= ' AND ((' . implode(') ' . $operation . ' (', $arr_where) . '))';
												}
											}
										}
									} else {
										if (!is_array($cookie_list_f)) {
											$where .= sprintf(' AND ' . $where_i . ' LIKE %s', GetSQL($cookie_list_f, 'defined', '\'%"' . _addslashes($cookie_list_f) . '"%\'', '"" AND FALSE'));
										} else {
											$arr_where = [];
											foreach ($cookie_list_f as $i => $v) {
												if ($v != '')
													$arr_where[] = sprintf($where_i . ' LIKE %s', GetSQL($v, 'defined', '\'%"' . _addslashes($v) . '"%\'', '"" AND FALSE'));
											}
											if (count($arr_where) > 0) {
												if (in_array($list_f_key, $arr_HAVING_key)) {
													$having .= ' AND ((' . implode(') ' . $operation . ' (', $arr_where) . '))';
												} else {
													$where .= ' AND ((' . implode(') ' . $operation . ' (', $arr_where) . '))';
												}
											}
										}
									}

								} else {    //陣列不存在資料表裡面
									if ($this->fields['list'][$list_f_key]['filter'] === 'text') {        //文字搜尋
										if (isset($this->fields['list'][$list_f_key]['key'])) {        //下拉選單
											$arr_key    = $this->fields['list'][$list_f_key]['key'];
											$db_prefix_ = DB_PREFIX_;
											if (isset($arr_key['db_prefix_'])) {
												$db_prefix_ = $arr_key['db_prefix_'];
											}
											$sql_k = sprintf('SELECT `' . $arr_key['key'] . '`
												FROM `' . $db_prefix_ . $arr_key['table'] . '`
												WHERE TRUE ' . $arr_key['where'] . ' AND`' . $arr_key['val'] . '`
												LIKE %s', GetSQL($cookie_list_f, 'defined', '\'%' . _addslashes($cookie_list_f) . '%\'', '"" AND FALSE'));
											$row   = Db::rowSQL($sql_k,false,0,$this->conn);
											$arr_v = [];
											foreach ($row as $i => $v) {
												$arr_v[] = $v[$arr_key['key']];
											}
											if (count($arr_v) > 0) {
												$where .= ' AND ' . $where_i . ' IN (' . implode(', ', $arr_v) . ')';
											} else {
												$where .= sprintf(' AND ' . $where_i . ' = %s', GetSQL($arr_v, 'text'));
											}
										}
										if (isset($this->fields['list'][$list_f_key]['values'])) {    //todo

										}
									} else {                                                            //下拉搜尋
										if (!is_array($cookie_list_f)) {
											$where .= sprintf(' AND ' . $where_i . ' = %s', GetSQL($cookie_list_f, 'text'));
										} else {
											$arr_where = [];
											foreach ($cookie_list_f as $i => $v) {
												if ($v != '')
													$arr_where[] = sprintf($where_i . ' = %s', GetSQL($v, 'text'));
											}
											if (count($arr_where) > 0) {
												if (in_array($list_f_key, $arr_HAVING_key)) {
													$having .= sprintf(' AND ' . $where_i . ' = %s', GetSQL($cookie_list_f, 'text'));
												} else {
													$where .= ' AND (' . implode(' OR ', $arr_where) . ')';
												}
											}
										}
									}
								}
							} elseif ($filter === 'range' || $filter === 'time_range' || $filter === 'date_range' || $filter === 'datetime_range') {
								if (!empty($cookie_list_f[0]))
									$where .= sprintf(' AND %s <= ' . $where_i, GetSQL($cookie_list_f[0], 'text'));
								if (!empty($cookie_list_f[1]))
									$where .= sprintf(' AND ' . $where_i . ' <= %s', GetSQL($cookie_list_f[1], 'text'));
							} elseif ($filter === 'number') {
								$where .= sprintf(' AND %d = ' . $where_i, GetSQL($cookie_list_f, 'int'));
							} else {
								$cookie_list_f_str = str_replace("'", "''", $cookie_list_f);        //跳脫單引號
								$where             .= sprintf(' AND ' . $where_i . ' LIKE %s', GetSQL($cookie_list_f_str, 'defined', "'%" . $cookie_list_f_str . "%'"));
							}
							$this->fields['where'] .= $where;
						}
					} else {
						$this->fields['list'][$list_f_key]['string_format'] = '';
					}
				}
			}
			$orderby = Tools::getValue('orderby');

			if (isset($this->fields['where']))
				$sql2 .= $this->fields['where'];
			$orderway = (Tools::getValue('orderway')) ? Tools::getValue('orderway') : 'ASC';

			if (!empty($orderby)) {
				$this->fields['order'] = ' ORDER BY `' . $orderby . '` ' . $orderway;
			} elseif (in_array('position', array_keys($this->fields['list']))) {
				$this->fields['order'] = ' ORDER BY `position` ' . $orderway;
			} elseif (empty($this->fields['order'])) {
				$this->fields['order'] = ' ORDER BY `' . $this->fields['index'] . '` ' . $orderway;
				if (!empty($this->_as))
					$this->fields['order'] = ' ORDER BY ' . $this->_as . '.`' . $this->fields['index'] . '` ' . $orderway;
			}
			if (isset($this->_group)) {
				$sql2 .= $this->_group;
			}

			if (isset($having)) {
				$sql2 .= ' HAVING TRUE' . $having;
			}

			if (isset($this->fields['order'])) {
				$sql2 .= $this->fields['order'];
			}

			$db        = new db_mysql($sql2, $this->fields['list_num'],'','p','m',$this->conn);
			$this->sql = $sql2;
			/*
			 * 存到cookie 給Ajax用
			 */
			setcookie('list_filter', '', time() - 3600, '/');
			setcookie('list_filter', serialize(Context::getContext()->cookie['list_filter']), time() + 3600, '/');
			$num        = $db->num();
			$total_page = $db->total_page();
			if ($num > 0) {
				while ($val = $db->next_row()) {
					foreach ($val as $key => $val_v) {
						if (in_array($key, array_keys($arr_related))) {
							$arr_related[$key]['val'][] = $val[$key];
						}
					}
					if (isset($val['position'])) {
						$val['position'] = '<div class="drag_group glyphicon glyphicon-move" data-id="' . $val[$this->fields['index']] . '"><div class="positions">' . $val['position'] . '</div></div>';
					}

					if (in_array($val[$this->fields['index']], $this->fields['no_preview'])) {
						$val['no_preview'] = true;
					}
					if (in_array($val[$this->fields['index']], $this->fields['no_edit'])) {
						$val['no_edit'] = true;
					}
					if (in_array($val[$this->fields['index']], $this->fields['no_view'])) {
						$val['no_view'] = true;
					}
					if (in_array($val[$this->fields['index']], $this->fields['no_del'])) {
						$val['no_del'] = true;
					}
					$this->fields['list_val'][] = $val;
				}
			}
			foreach ($arr_related as $i => $related_v) {    //套內容
				$related_var[$i]  = [];
				$related_var2[$i] = [];
				$related_v['val'] = array_unique($related_v['val']);
				$sql              = $filter_sql[$i];
                //因切換資料庫而變動的參數
//				$db                          = new db_mysql($sql);
//                echo $sql;
                $db                          = new db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm',$this->conn);
				$arr_key_p        = [];
				if ($db->num() > 0) {
					while ($db_v = $db->next_row()) {
//todo
						if (!is_array($related_v['key_v'])) {
							$related_var[$i][$db_v[$related_v['key']]]['v'] = $db_v[$related_v['key_v']];
						} else {
							if (empty($related_v['val_divide']))
								$related_v['val_divide'] = '%s - %s';
							$arr_val = [];
							foreach ($related_v['key_v'] as $it => $av) {
								$arr_val[] = $db_v[$av];
							}
							$related_var[$i][$db_v[$related_v['key']]]['v'] = vsprintf($related_v['val_divide'], $arr_val);
						}

						if (!is_array($related_v['text'])) {
							$related_var[$i][$db_v[$related_v['key']]]['text'] = $db_v[$related_v['text']];
						} else {
							if (empty($related_v['text_divide']))
								$related_v['text_divide'] = '%s - %s';
							$arr_text = [];
							foreach ($related_v['text'] as $it => $related_vvv) {
								$arr_text[] = $db_v[$related_vvv];
							}
							$related_var[$i][$db_v[$related_v['key']]]['text'] = vsprintf($related_v['text_divide'], $arr_text);
						}

						if (!empty($related_v['key_p'])) {
							if ($this->fields['list'][$related_v['key_p']]['multiple']) {
								$related_var[$i][$db_v[$related_v['key']]]['p_n'] = $related_v['key_p'] . '[]';        //父層名稱
							} else {
								$related_var[$i][$db_v[$related_v['key']]]['p_n'] = $related_v['key_p'];            //父層名稱
							}
							$arr_key_p[$db_v[$related_v['key']]][] = $db_v[$related_v['key_p']];            //父層值
						}
						if (!empty($related_v['data'])) {
							$related_var[$i][$db_v[$related_v['key']]]['data'] = [];
							if (is_array($related_v['data'])) {
								foreach ($related_v['data'] as $di => $ddv) {
									$related_var[$i][$db_v[$related_v['key']]]['data'][$ddv] = $db_v[$ddv];
								}
							} else {
								$related_var[$i][$db_v[$related_v['key']]]['data'][$related_v['data']] = $db_v[$related_v['data']];
							}
						}
						if (!empty($related_v['class'])) {
							$related_var[$i][$db_v[$related_v['key']]]['class'] = [];
							if (is_array($related_v['class'])) {
								foreach ($related_v['class'] as $di => $ddv) {
									$related_var[$i][$db_v[$related_v['key']]]['class'][] = $db_v[$ddv];
								}
							} else {
								$related_var[$i][$db_v[$related_v['key']]]['class'][] = $db_v[$related_v['class']];
							}
						}
					}
					//todo
					foreach ($arr_key_p as $key_p_i => $item) {
						if (count($item) > 1) {
							$related_var[$i][$key_p_i]['p_v'] = '_' . implode('_', $item) . '_';
						} else {
							$related_var[$i][$key_p_i]['p_v'] = $item[0];
						}
					}
				}
				if ((count($this->fields['list_filter']) == 0) || (!in_array($i, $this->fields['list_filter'])) || (Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_' . $i] === '')) {        //沒有篩選欄位 全部顯示
					$sql       = $filter_sql2[$i] . '
						WHERE `' . $related_v['key'] . '` IN (' . implode(', ', $related_v['val']) . ')
						LIMIT 0, ' . count($related_v['val']);
                    //因切換資料庫而變動的參數
//				$db                          = new db_mysql($sql);
                    $db                          = new db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm',$this->conn);
					$arr_key_p = [];
					if ($db->num() > 0) {
						while ($db_v = $db->next_row()) {
							$related_var2[$i][$db_v[$related_v['key']]] = [];

							if (!is_array($related_v['key_v'])) {
								$related_var2[$i][$db_v[$related_v['key']]]['v'] = $db_v[$related_v['key_v']];
							} else {
								if (empty($related_v['val_divide']))
									$related_v['val_divide'] = '%s - %s';
								$arr_val = [];
								foreach ($related_v['key_v'] as $it => $av) {
									$arr_val[] = $db_v[$av];
								}
								$related_var2[$i][$db_v[$related_v['key']]]['v'] = vsprintf($related_v['val_divide'], $arr_val);
							}

							if (!is_array($related_v['text'])) {
								$related_var2[$i][$db_v[$related_v['key']]]['text'] = $db_v[$related_v['text']];
							} else {
								if (empty($related_v['text_divide']))
									$related_v['text_divide'] = '%s - %s';
								$arr_text = [];
								foreach ($related_v['text'] as $it => $related_vvv) {
									$arr_text[] = $db_v[$related_vvv];
								}
								$related_var2[$i][$db_v[$related_v['key']]]['text'] = vsprintf($related_v['text_divide'], $arr_text);
							}
							if (!empty($related_v['key_p'])) {
								if ($this->fields['list'][$related_v['key_p']]['multiple']) {
									$related_var2[$i][$db_v[$related_v['key']]]['p_n'] = $related_v['key_p'] . '[]';        //父層名稱
								} else {
									$related_var2[$i][$db_v[$related_v['key']]]['p_n'] = $related_v['key_p'];            //父層名稱
								}
								$arr_key_p[$db_v[$related_v['key']]][] = $db_v[$related_v['key_p']];            //父層值
							}
							if (!empty($related_v['data'])) {
								$related_var2[$i][$db_v[$related_v['key']]]['data'] = [];
								if (is_array($related_v['data'])) {
									foreach ($related_v['data'] as $di => $ddv) {
										$related_var2[$i][$db_v[$related_v['key']]]['data'][$ddv] = $db_v[$ddv];
									}
								} else {
									foreach ($related_v['data'] as $di => $ddv) {
										$related_var2[$i][$db_v[$related_v['key']]]['data'][$related_v['data']] = $db_v[$related_v['data']];
									}
								}
							}
							if (!empty($related_v['class'])) {
								$related_var2[$i][$db_v[$related_v['key']]]['class'] = [];
								if (is_array($related_v['class'])) {
									foreach ($related_v['class'] as $di => $ddv) {
										$related_var2[$i][$db_v[$related_v['key']]]['class'][$ddv] = $db_v[$ddv];
									}
								} else {
									foreach ($related_v['class'] as $di => $ddv) {
										$related_var2[$i][$db_v[$related_v['key']]]['class'][$related_v['class']] = $db_v[$related_v['data']];
									}
								}
							}
						}
						//todo
						foreach ($arr_key_p as $key_p_i => $item) {
							if (count($item) > 1) {
								$related_var2[$i][$key_p_i]['p_v'] = '_' . implode('_', $item) . '_';
							} else {
								$related_var2[$i][$key_p_i]['p_v'] = $item[0];
							}
						}
					} else {
						$related_var2[$i] = $related_var[$i];
					}
				} else {
					$related_var2[$i] = $related_var[$i];
				}
				unset($arr_related[$i]);
			}
			/*
			 * 顯示值轉換
			 */
			foreach ($this->fields['list_val'] as $i => $v) {
				$tr_class                                 = [];
				$this->fields['list_val'][$i]['tr_class'] = '';
				foreach ($this->fields['list'] as $j => $w) {
					$space = $this->fields['list'][$j]['space'];
					if (empty($space)) {
						$this->fields['list'][$j]['space'] = ',';
						$space                             = ',';
					}
					//自訂 下拉選單
					if (isset($this->fields['list'][$j]['values'])) {
						if ($this->fields['list'][$j]['in_table']) {      //陣列存表單
							$this->fields['list_val'][$i][$j] = $json->decode($this->fields['list_val'][$i][$j]);
							$arr_lit                          = [];
							foreach ($this->fields['list'][$j]['values'] as $i2 => $v2) {
								$arr_lit[$v2['value']] = $v2['title'];
							}
							foreach ($this->fields['list_val'][$i][$j] as $lv_key => $lv_v) {
								$this->fields['list_val'][$i][$j][$lv_key] = $arr_lit[$lv_v];
							}
							$this->fields['list_val'][$i][$j] = implode($space, $this->fields['list_val'][$i][$j]);
							if (empty($this->fields['list_val'][$i][$j]))
								$this->fields['list_val'][$i][$j] = '-';
						} else {
							foreach ($this->fields['list'][$j]['values'] as $i2 => $v2) {
								if (($this->fields['list_val'][$i][$j] == $v2['value']) && isset($v2['tr_class'])) {
									$tr_class[] = $v2['tr_class'];
								}
								if (($this->fields['list_val'][$i][$j] == $v2['value']) && isset($v2['class'])) {
									$tr_class[] = $v2['class'];
								}
							}
						}
					}
					//單選
					if (isset($this->fields['list'][$j]['key'])) {        //table 下拉選單
						if ($this->fields['list'][$j]['in_table']) {    //陣列存表單
							$arr_v = $json->decode($this->fields['list_val'][$i][$j]);
							foreach ($arr_v as $ai => $av) {
								$arr_v[$ai] = ($av) ? $related_var2[$j][$av]['v'] : '-';        //取代值
							}
							$arr_v                            = array_filter($arr_v);
							$this->fields['list_val'][$i][$j] = $arr_v;
							$this->fields['list_val'][$i][$j] = implode($space, $this->fields['list_val'][$i][$j]);
						} else {
							$val = $this->fields['list_val'][$i][$j];
							if (!empty($val)) {
								$this->fields['list_val'][$i][$j] = '<samp';
								if (isset($related_var2[$j][$val]['class'])) {    //選單有class
									$this->fields['list_val'][$i][$j] .= ' class="';
									foreach ($related_var2[$j][$val]['class'] as $c_key => $c_val) {
										$this->fields['list_val'][$i][$j] .= ' ' . $c_val;
									}
									$this->fields['list_val'][$i][$j] .= '"';
								}
								if (isset($related_var2[$j][$val]['data'])) {    //選單有data
									foreach ($related_var2[$j][$val]['data'] as $d_key => $d_val) {
										$this->fields['list_val'][$i][$j] .= ' data-' . $d_key . '="' . $d_val . '"';
									}
								}
								$this->fields['list_val'][$i][$j] .= '>' . $related_var2[$j][$val]['v'] . '</samp>';
							} else {
								$this->fields['list_val'][$i][$j] = '<samp>-</samp>';
							}
						}
					}

//					if (isset($this->fields['list'][$j]['multiple'])
//						&& $this->fields['list'][$j]['multiple']
//						&& !isset($this->fields['list'][$j]['values'])) {
//						if($this->fields['list'][$j]['in_table']){	//陣列存表單
//							$this->fields['list_val'][$i][$j] = $json->decode($this->fields['list_val'][$i][$j]);
//							if(isset($this->fields['list'][$j]['key'])){
//								$arr_v = $this->fields['list_val'][$i][$j];
//								foreach($arr_v as $ai => $av){
//									$arr_v[$ai] = ($av) ? $related_var2[$j][$av]['v'] : '-';		//取代值
//								}
//								$arr_v = array_filter($arr_v);
//								$this->fields['list_val'][$i][$j] = $arr_v;
//							}
//							$this->fields['list_val'][$i][$j] = implode($space, $this->fields['list_val'][$i][$j]);
//						}else{
//							if(isset($this->fields['list'][$j]['key'])){
//								$val = $this->fields['list_val'][$i][$j];
//								$this->fields['list_val'][$i][$j] = ($val) ? $related_var2[$j][$val]['v'] : '-';		//取代值
//							}
//						}
//					}
					if (isset($this->fields['list'][$j]['multiple'])
						&& $this->fields['list'][$j]['multiple']
						&& !isset($this->fields['list'][$j]['key'])
						&& !isset($this->fields['list'][$j]['values'])) {
						$space = $this->fields['list'][$j]['space'];
						if (empty($space))
							$space = ',';
						$this->fields['list_val'][$i][$j] = $json->decode($this->fields['list_val'][$i][$j]);
						$this->fields['list_val'][$i][$j] = implode($space, $this->fields['list_val'][$i][$j]);
					}
					/*
					 * 取代內容
					 */
					if (is_array($this->fields['list'][$j]['replace'])) {
						$this->fields['list_val'][$i][$j] = implode($this->fields['list'][$j]['replace'][1], explode($this->fields['list'][$j]['replace'][0], $this->fields['list_val'][$i][$j]));
					}

					//todo 加入內容
					if (is_array($this->fields['list'][$j]['text_divide']) && !empty($this->fields['list_val'][$i][$j])) {
						$this->fields['list_val'][$i][$j] = sprintf($this->fields['list'][$j]['text_divide'], $this->fields['list_val'][$i][$j]);
					}

					//todo 合併欄位內容
					if (is_array($this->fields['list'][$j]['merge'])) {
						foreach ($this->fields['list'][$j]['merge'] as $merge) {

						}
					}
				}

				//狀態顏色
				$this->fields['list_val'][$i]['tr_class'] = implode(' ', array_unique($tr_class));
			}
			foreach ($_GET as $key => $v) {
				if (($key != 'p') && ($key != 'm')) {
					if (is_array($v)) {        //複選篩選
						foreach ($v as $i => $vv) {
							$arr_form_list_pagination_url[] = $key . '[]=' . $vv;
						}
					} else {
						$arr_form_list_pagination_url[] = $key . '=' . $v;
					}
				}
			}
			if (empty($this->fields['list_footer'])) {
				$this->fields['list_footer_display'] = false;
			}
			$Context = Context::getContext();

			FormTable::getContext()->fields = $this->fields;
			$Context->smarty->assign([
					'table_num'        => $num,
					'list_id'          => $this->list_id,
					'related_var'      => $related_var,
					'has_actions'      => !empty($this->actions),
					'actions'          => $this->actions,
					'post_processing'  => $this->post_processing,
					'display_edit_div' => $this->display_edit_div,
					'bulk_actions'     => $this->bulk_actions,
				]
			);

			$p = Tools::getValue('p');
			if ($p < 1) {
				$p = 1;
			}
			$Context->smarty->assign([
					'form_list_pagination_min' => max(min($total_page, max(($p + 4), PAGE_NUM)) - 9, 1),
					'form_list_pagination_max' => $total_page,
					'form_list_pagination_num' => PAGE_NUM,
					'form_list_pagination_url' => implode('&', $arr_form_list_pagination_url),
				]
			);
		}
	}

	public function renderExcel()
	{
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader->setIncludeCharts(true);
		$objPHPExcel = $objReader->load($this->fields['excel']['file']);

		//直接輸出到瀏覽器
		$sql = sprintf('SELECT `first_name` , `first_name`, `user`
			FROM `' . DB_PREFIX_ . 'admin`
			WHERE `id_admin` = %d
			LIMIT 0, 1',
			$_SESSION['id_admin']);
		$row = Db::rowSQL($sql,true,0,$this->conn);

		$last_name      = $row['last_name'];
		$first_name     = $row['first_name'];
		$filename       = sprintf($this->l('%s_%s %s'), $this->fields['excel']['title'], WEB_NAME, date('YmdHis', time()));
		$Creator        = sprintf($this->l('%s %s'), $first_name, $last_name);
		$LastModifiedBy = '';
		$Title          = sprintf($this->l('%s-%s'), WEB_NAME, $this->fields['excel']['title']);
		$Subject        = sprintf($this->l('%s %s'), WEB_NAME, now());
		$Description    = sprintf($this->l('%s 匯出資料,匯出時間: %s'), WEB_NAME, now());
		$Category       = sprintf($this->l('%s'), $this->fields['excel']['title']);
		//設置excel的屬性：

		//創建人
		$objPHPExcel->getProperties()->setCreator($Creator);
		//最後修改人
		$objPHPExcel->getProperties()->setLastModifiedBy($LastModifiedBy);
		//標題
		$objPHPExcel->getProperties()->setTitle($Title);
		//題目
		$objPHPExcel->getProperties()->setSubject($Subject);
		//描述
		$objPHPExcel->getProperties()->setDescription($Description);
		//關鍵字
		$objPHPExcel->getProperties()->setKeywords($Title);
		//種類
		$objPHPExcel->getProperties()->setCategory($Category);

		$sheet_index_no = 0;
		foreach ($this->fields['excel']['assign'] as $sheet_index => $assign) {
			if (isint($sheet_index)) {
				$objPHPExcel->setActiveSheetIndex($sheet_index);           //設制第幾頁
			} else {
				$objPHPExcel->setActiveSheetIndex($sheet_index_no);        //設制第幾頁
			}

			if (isset($assign['title']) && !empty($assign['title'])) {
				$objPHPExcel->setActiveSheetIndex($sheet_index)->setTitle($assign['title']);  //分頁名稱
			} elseif (!empty($this->fields['excel']['title'])) {
				$objPHPExcel->setActiveSheetIndex($sheet_index)->setTitle($this->fields['title']);  //分頁名稱
			}

			if (count($assign['add_row'])) {                        //增加欄
				foreach ($assign['add_row'] as $i => $arr) {
					$objPHPExcel->getActiveSheet($sheet_index)->insertNewRowBefore($arr[0], $arr[1]);     //$arr[0] => 數字(第幾欄), $arr[1] => 數字 新增幾欄
				}
			}

			if (count($assign['add_col'])) {                        //增加列
				foreach ($assign['add_col'] as $i => $arr) {
					$objPHPExcel->getActiveSheet()->insertNewColumnBefore($arr[0], $arr[1]);      //$arr[0] => 英文(第幾列), $arr[1] => 數字 新增幾列
				}
			}

			if (count($assign['add_col_id'])) {                       //增加列
				foreach ($assign['add_col_id'] as $i => $arr) {
					$objPHPExcel->getActiveSheet()->insertNewColumnBeforeByIndex($arr[0], $arr[1]);      //$arr[0] => 數字, $arr[1] => 數字 新增幾列
				}
			}

			if (count($assign['merge_cells'])) {                        //合併欄位
				foreach ($assign['merge_cells'] as $i => $arr) {
					$objPHPExcel->getActiveSheet()->mergeCells($arr[0] . ':' . $arr[1]);     //(第$arr[0]欄 ~ $arr[1] 合併)
				}
			}
			//內容
			foreach ($assign['coordinate'] as $c => $v) {
				$objPHPExcel->getActiveSheet()->setCellValue($c, $v);
			}

			if (count($assign['border'])) {                            //邊框
				foreach ($assign['border'] as $i => $arr) {
					switch ($arr[1]) {
						case 'thin':                                    //細線
						case 'THIN':
							$style = PHPExcel_Style_Border::BORDER_THIN;
						break;
						case 'hair':                                    //極細線
						case 'HAIR':
							$style = PHPExcel_Style_Border::BORDER_HAIR;
						break;
						case 'thick':                                    //粗線
						case 'THICK':
							$style = PHPExcel_Style_Border::BORDER_THICK;
						break;
						case 'double':                                //二重線
						case 'DOUBLE':
							$style = PHPExcel_Style_Border::BORDER_DOUBLE;
						break;
						case 'dotted':                                //點線(細線)(短,.......）
						case 'DOTTED':
							$style = PHPExcel_Style_Border::BORDER_DOTTED;
						break;
						case 'dashed':                                //點線(細線)(長,-------）
						case 'DASHED':
							$style = PHPExcel_Style_Border::BORDER_DASHED;
						break;
						case 'mediumdashed':                            //點線(普通線)(長）
						case 'MEDIUMDASHED':
							$style = PHPExcel_Style_Border::BORDER_MEDIUMDASHED;
						break;
						case 'dashdot':                                //一點鎖線(細線)(-.-.-.-)
						case 'DASHDOT':
							$style = PHPExcel_Style_Border::BORDER_DASHDOT;
						break;
						case 'mediumdashdot':                            //一點鎖線(普通線)
						case 'MEDIUMDASHDOT':
							$style = PHPExcel_Style_Border::BORDER_MEDIUMDASHDOT;
						break;
						case 'dashdotdot':                                //二點鎖線(細線)(-..-..-..-)
						case 'DASHDOTDOT':
							$style = PHPExcel_Style_Border::BORDER_DASHDOTDOT;
						break;
						case 'mediumdashdotdot':                        //二點鎖線(普通線)
						case 'MEDIUMDASHDOTDOT':
							$style = PHPExcel_Style_Border::BORDER_MEDIUMDASHDOTDOT;
						break;
						case 'slantdashdot':                            //斜邊一點鎖線
						case 'SLANTDASHDOT':
							$style = PHPExcel_Style_Border::BORDER_SLANTDASHDOT;
						break;
						case 'medium':                                //普通線
						case 'MEDIUM':
							$style = PHPExcel_Style_Border::BORDER_MEDIUM;
						break;
						case 'none':                                    //無框線
						case 'NONE':
						default:
							$style = PHPExcel_Style_Border::BORDER_NONE;
						break;
					}
					$styleArray = [
						'borders' => [
							'allborders' => [
								'style' => $style,
								'color' => [
									'argb' => $arr[2],
								],
							],
						],
					];
					$objPHPExcel->getActiveSheet()->getStyle($arr[0])->applyFromArray($styleArray);
				}
			}

			if (count($assign['font'])) {                    //字體
				foreach ($assign['font'] as $i => $arr) {
					$styleArray = [
						'font' => [
							'bold'  => $arr[1],
							'size'  => $arr[2],
							'color' => [
								'argb' => $arr[3],
							],
							'name'  => $arr[4],
						],
					];
					$objPHPExcel->getActiveSheet()->getStyle($arr[0])->applyFromArray($styleArray);
				}
			}

			//列高
			if (count($assign['row_height'])) {
				foreach ($assign['row_height'] as $c => $arr) {
					$objPHPExcel->getActiveSheet()->getRowDimension($arr[0])->setRowHeight($arr[1]);
				}
			}
			if (count($assign['horizontal'])) {                //水平對齊
				foreach ($assign['horizontal'] as $i => $arr) {
					switch ($arr[1]) {
						case 'center':                                    //中央
						case 'CENTER':
							$style = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
						break;
						case 'left':                                        //左
						case 'LEFT':
							$style = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
						break;
						case 'right':                                        //右
						case 'RIGHT':
							$style = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
						break;
						case 'justify':                                    //左右對齊
						case 'JUSTIFY':
							$style = PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY;
						break;
						case 'general':                                    //標準
						case 'GENERAL':
						default:
							$style = PHPExcel_Style_Alignment::HORIZONTAL_GENERAL;
						break;
					}
					$objPHPExcel->getActiveSheet()->getStyle($arr[0])->getAlignment()->setHorizontal($style);
				}
			}

			if (count($assign['vertical'])) {                //垂直對齊
				foreach ($assign['vertical'] as $i => $arr) {
					switch ($arr[1]) {
						case 'top':                                        //上
						case 'TOP':
							$style = PHPExcel_Style_Alignment::VERTICAL_TOP;
						break;
						case 'center':                                    //中央
						case 'CENTER':
							$style = PHPExcel_Style_Alignment::VERTICAL_CENTER;
						break;
						case 'bottom':                                    //下
						case 'BOTTOM':
							$style = PHPExcel_Style_Alignment::VERTICAL_BOTTOM;
						break;
						case 'justify':                                    //上下對齊
						case 'JUSTIFY':
							$style = PHPExcel_Style_Alignment::VERTICAL_JUSTIFY;
						break;
					}
					$objPHPExcel->getActiveSheet()->getStyle($arr[0])->getAlignment()->setVertical($style);
				}
			}

			if (count($assign['fill_color'])) {                                    //背景顏色
				foreach ($assign['fill_color'] as $i => $arr) {
					switch ($arr[2]) {
						default:
							$type = PHPExcel_Style_Fill::FILL_SOLID;    //設定背景顏色單色
						break;
					}
					$objPHPExcel->getActiveSheet()->getStyle($arr[0])->applyFromArray(
						[
							'fill' =>
								[
									'type'  => $type,
									'color' => ['argb' => $arr[1]],
								],
						]
					);
				}
			}

			if (count($assign['visible'])) {                                    //隱藏列
				foreach ($assign['visible'] as $i => $v) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($v)->setVisible(false);
				}
			}

			if (count($assign['charts'])) {                                    //圖表
				foreach ($assign['charts'] as $i => $charts) {
					$arr_labels = [];
					foreach ($charts['labels'] as $li => $lv) {
						$arr_labels[] = new PHPExcel_Chart_DataSeriesValues($lv[0], $lv[1], $lv[2], $lv[3]);
					}
					$labels = $arr_labels;

					$arr_xLabels = [];
					foreach ($charts['xLabels'] as $li => $lv) {
						$arr_xLabels[] = new PHPExcel_Chart_DataSeriesValues($lv[0], $lv[1], $lv[2], $lv[3]);
					}
					$xLabels = $arr_xLabels;

					$arr_datas = [];
					foreach ($charts['datas'] as $li => $lv) {
						$arr_datas[] = new PHPExcel_Chart_DataSeriesValues($lv[0], $lv[1], $lv[2], $lv[3]);
					}
					$datas = $arr_datas;

					switch ($charts['series']['type']) {
						case 'barchart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_BARCHART;
						break;
						case 'barchart_3d' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_BARCHART_3D;
						break;
						case 'linechart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_LINECHART;
						break;
						case 'linechart_3d' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_LINECHART_3D;
						break;
						case 'areachart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_AREACHART;
						break;
						case 'areachart_3d' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_AREACHART_3D;
						break;
						case 'piechart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_PIECHART;
						break;
						case 'piechart_3d' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_PIECHART_3D;
						break;
						case 'doughtnutchart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_DOUGHTNUTCHART;
						break;
						case 'donutchart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_DONUTCHART;
						break;
						case 'scatterchart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_SCATTERCHART;
						break;
						case 'surfacechart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_SURFACECHART;
						break;
						case 'surfacechart_3d' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_SURFACECHART_3D;
						break;
						case 'radarchart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_RADARCHART;
						break;
						case 'bubblechart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_BUBBLECHART;
						break;
						case 'stockchart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_STOCKCHART;
						break;
						case 'candlechart' :
							$typs = PHPExcel_Chart_DataSeries::TYPE_CANDLECHART;
						break;
					}

					switch ($charts['series']['type']) {
						case 'clustered' :
							$grouping = PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED;
						break;
						case 'stacked' :
							$grouping = PHPExcel_Chart_DataSeries::GROUPING_STACKED;
						break;
						case 'percent_stacked' :
							$grouping = PHPExcel_Chart_DataSeries::GROUPING_PERCENT_STACKED;
						break;
						case 'standard' :
							$grouping = PHPExcel_Chart_DataSeries::GROUPING_STANDARD;
						break;
					}

					//图表框架
					$series = [
						new PHPExcel_Chart_DataSeries(
							$typs,
							$grouping,
							range(0, count($labels) - 1),
							$labels,
							$xLabels,
							$datas
						),
					]; //图表框架

					$layout = new PHPExcel_Chart_Layout();
					$layout->setShowVal($charts['show_val']);            //顯示數值
					$layout->setShowPercent($charts['show_percent']);    //顯示百分比
					$areas = new PHPExcel_Chart_PlotArea($layout, $series);

					$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, $layout, false);
					$title  = new PHPExcel_Chart_Title($charts['title']);        //表單標題
					$ytitle = new PHPExcel_Chart_Title($charts['ytitle']);        //表單數值標題
					$chart  = new PHPExcel_Chart('line_chart', $title, $legend, $areas, false, false, null, $ytitle);
					$chart->setTopLeftPosition($charts['position'][0])->setBottomRightPosition($charts['position'][1]); //图表位置

					$objPHPExcel->getActiveSheet()->addChart($chart);
				}
			}

			$sheet_index_no++;
		}
		$objPHPExcel->createSheet();
//		$objPHPExcel->removeSheetByIndex(1);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setIncludeCharts(true);    //圖表必須

		header("Expires: 0");
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
		header("Content-Type:application/force-download");
		header("Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Type:application/octet-stream");
		header("Content-Type:application/download");
		header('Content-Disposition:attachment;filename="' . $filename . '.xlsx"');
		header("Content-Transfer-Encoding:binary");
		$objWriter->save('php://output');
		exit;
	}

//	public function renderExcel()
//	{
//
////		$objPHPExcel = new PHPExcel();
////		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
////		$objPHPExcel = PHPExcel_IOFactory::load($this->fields['excel']['file']);
////		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
////		$reader->setIncludeCharts(true);
////		$spreadsheet = $reader->load($this->fields['excel']['file']);
////
////		$worksheet->getCell('A1')->setValue('John');
////		$worksheet->getCell('A2')->setValue('Smith');
////通过工厂模式来写内容
//		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
//		$reader->setIncludeCharts(true);
//		$spreadsheet = $reader->load($this->fields['excel']['file']);
//		$objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
//		$objWriter->setIncludeCharts(true);
//		header("Expires: 0");
//		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
//		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//		header('Content-Disposition:attachment;filename="' . 'etse' . '.xlsx"');
//		header("Content-Transfer-Encoding:binary");
//		$objWriter->save('php://output');
//		$spreadsheet->disconnectWorksheets();
//		unset($spreadsheet);
//		exit;
//	}

	public
	function renderExcelList()
	{
		$json = new Json();
		$this->ini();
		$filter_sql  = [];
		$arr_related = [];    //關聯key
		$related_var = [];
//		$arr_values = array();
		if (!empty($this->fields['list']) && is_array($this->fields['excel']['list'])) {
			if (!isset($this->fields['list_heading_display']))
				$this->fields['list_heading_display'] = true;
			if (!isset($this->fields['list_body_display']))
				$this->fields['list_body_display'] = true;
			if (!isset($this->fields['list_footer_display']))
				$this->fields['list_footer_display'] = true;

			if (!isset($this->list_id)) {
				$this->list_id = $this->table;
			}

			if (!in_array($this->fields['index'], array_keys($this->fields['excel']['list'])) && !in_array($this->_as . '!' . $this->fields['index'], array_keys($this->fields['excel']['list']))) {        //列表內沒有索引鍵
				//加入索引鍵
				$this->fields['excel']['list'][$this->fields['index']] = [
					'index'  => true,
					'hidden' => true,
				];
			}

			$arr_SELECT = [];

			foreach ($this->fields['excel']['list'] as $key_i => $se_v) {
				if (!empty($se_v['filter_key'])) {
					$arr_se_v = explode('!', $se_v['filter_key']);
					if (count($arr_se_v) == 1) {
						$arr_SELECT[] = '`' . $arr_se_v[0] . '` AS ' . $key_i;
					} else {
						$arr_SELECT[] = $arr_se_v[0] . '.`' . $arr_se_v[1] . '` AS ' . $key_i;
					}
				} elseif (!empty($se_v['filter_sql'])) {
					$arr_SELECT[] = $se_v['filter_sql'] . ' AS ' . $key_i;
				} elseif (empty($se_v['no_action'])) {
					$arr_SELECT[] = '`' . $key_i . '`';
				}
			}

			$as = '';
			if (!empty($this->_as))
				$as = ' AS ' . $this->_as;
			$db_prefix_ = DB_PREFIX_;
			if (isset($this->fields['excel']['db_prefix_'])) {
				$db_prefix_ = $this->fields['excel']['db_prefix_'];
			}
			$sql = 'SELECT ' . implode(', ', $arr_SELECT) . '
				FROM `' . $db_prefix_ . $this->table . '`' . $as . $this->_join;    //主要list
			foreach ($this->fields['excel']['list'] as $i => $v) {
				if ($i == 'position') {
					$this->fields['list_num']      = 0;        //可以排順序就不能有頁數(會混亂)
					$this->fields['show_list_num'] = false;
				}
				if ($i == 'active') {    //啟用狀態
					unset($this->fields['excel']['list'][$i]['multiple']);
					if (!isset($this->fields['excel']['list'][$i]['values'])) {
						$this->fields['excel']['list'][$i]['values'] = [
							[
								'class' => 'active_off',
								'value' => 0,
								'title' => $this->l('關閉'),
							],
							[
								'class' => 'active_on',
								'value' => 1,
								'title' => $this->l('啟用'),
							],
						];
					}
				}
				if (isset($v['key'])) {        //下拉捲軸
					$related_select = '';
					$text_select    = '';
					if (!empty($v['key']['parent']))
						$related_select = ', `' . $v['key']['parent'] . '`';
					$arr_related[$i]               = [];
					$arr_related[$i]['key']        = $v['key']['key'];
					$arr_related[$i]['key_v']      = $v['key']['val'];
					$arr_related[$i]['val_divide'] = $v['key']['val_divide'];
					$val_select                    = ', `' . $v['key']['val'] . '`';
					if (!empty($v['key']['parent']))
						$arr_related[$i]['key_p'] = $v['key']['parent'];

					if (is_array($arr_related[$i]['key_v'])) {
						$val_select = ', `' . implode('`, `', $arr_related[$i]['key_v']) . '`';
					} else {
						$val_select = ', `' . $arr_related[$i]['key_v'] . '`';
					}

					if (!empty($v['key']['text'])) {
						$arr_related[$i]['text'] = $v['key']['text'];
						if (is_array($arr_related[$i]['text'])) {
							$text_select = ', `' . implode('`, `', $arr_related[$i]['text']) . '`';
						} else {
							$text_select = ', `' . $arr_related[$i]['text'] . '`';
						}
					}
					if (!empty($v['key']['text_divide']))
						$arr_related[$i]['text_divide'] = $v['key']['text_divide'];

					$t_db_prefix_ = DB_PREFIX_;
					if (isset($v['key']['db_prefix_'])) {
						$t_db_prefix_ = $v['key']['db_prefix_'];
					}

					$filter_sql[$i] = 'SELECT `' . $v['key']['key'] . '`' . $val_select . $related_select . $text_select . '
							FROM `' . $t_db_prefix_ . $v['key']['table'] . '`';                          //全顯示做下拉捲軸
				}
//				if (isset($v['values'])) {
//					$arr_values[$i] = array();
//					$arr_values[$i]['value'] = $v['values'];
//				}
				if (isset($v['filter']) && !empty($v['filter'])) {
					$this->fields['list_filter'][] = $i;
					if ($v['values'] || $v['key']) {
						$this->fields['list_filter_local'][] = $i;
					}
				}
				if (isset($v['index']) && $v['index'] && empty($this->fields['index']))
					$this->fields['index'] = $i;
			}
			$sql2      = $sql;
			$arr_order = [];
			/*
			if(in_array('position', array_keys($this->fields['excel']['list']))){	//有順序

			}
			*/
			/*
			 * 一頁顯示數量 (每一頁)
			 */
			if (Tools::isSubmit($this->list_id . 'Filter_m') && (trim(Tools::getValue($this->list_id . 'Filter_m')) != '') && isints(trim(Tools::getValue($this->list_id . 'Filter_m')))) {
				Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'] = trim(Tools::getValue($this->list_id . 'Filter_m'));
			} elseif (empty(Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'])) {
				Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'] = $this->fields['list_num'];
			}
			$this->fields['list_num'] = Context::getContext()->cookie['list_filter'][$this->list_id . 'Filter_m'];

			if (empty($this->fields['where'])) {
				$this->fields['where'] = ' WHERE TRUE';
			} else {
				$this->fields['where'] = ' WHERE TRUE' . $this->fields['where'];
			}
			if (count($this->fields['list_filter']) > 0) {
				foreach ($this->fields['list_filter'] as $ki => $list_f) {
					$list_f_key            = $list_f;
					$list_filter_local_key = $list_f;
					$list_f                = $this->list_id . 'Filter_' . $list_f;
					if (Tools::isSubmit($list_f)) {        //有值用GET
						$T_list_f = Tools::getValue($list_f);
						if (trim($T_list_f) == '' && !is_array($T_list_f)) {        //清除
							unset($_GET[$list_f]);
							unset($_POST[$list_f]);
							Context::getContext()->cookie['list_filter'][$list_f] = '';
						} else {                            //設定
							if (is_array($T_list_f)) {
								Context::getContext()->cookie['list_filter'][$list_f] = $T_list_f;
							} else {
								Context::getContext()->cookie['list_filter'][$list_f] = trim($T_list_f);
							}
						}
					} elseif (!Tools::isSubmit($this->list_id . 'Filter_m')) {                    //沒有值用COOKIE
						$list_f_v = Context::getContext()->cookie['list_filter'][$list_f];
						switch (Configuration::get('list_Cookie')) {
							case 2:    //全部
							case'2':
								$_GET[$list_f] = $list_f_v;
							break;
							case 1:    //局部
							case'1':
								if (in_array($list_filter_local_key, $this->fields['list_filter_local'])) {
									$_GET[$list_f] = $list_f_v;
								}
							break;
							case 0:
							case'0':    //關閉
							default:
							break;
						}
					} else {
						unset($_GET[$list_f]);
						unset($_POST[$list_f]);
						Context::getContext()->cookie['list_filter'][$list_f] = '';
					}
					$cookie_list_f = Context::getContext()->cookie['list_filter'][$list_f];
					if ($cookie_list_f != '') {
						$filter                                                      = $this->fields['list'][$list_f_key]['filter'];
						$this->fields['excel']['list'][$list_f_key]['string_format'] = $cookie_list_f;

						if (isset($this->fields['excel']['list'][$list_f_key]['filter_sql'])) {
							$where_i = '(' . $this->fields['excel']['list'][$list_f_key]['filter_sql'] . ')';
						} else {
							$key = $list_f_key;
							if ($this->fields['excel']['list'][$list_f_key]['filter_key'])
								$key = $this->fields['excel']['list'][$list_f_key]['filter_key'];
							$arr_filter_key = explode('!', $key);
							if (count($arr_filter_key) == 1) {
								$where_i = '`' . str_replace($this->list_id . 'Filter_', '', $arr_filter_key[0]) . '`';
							} else {
								$where_i = str_replace($this->list_id . 'Filter_', '', $arr_filter_key[0]) . '.`' . $arr_filter_key[1] . '`';
							}
						}

						//有選項
						if ((isset($this->fields['excel']['list'][$list_f_key]['key']) || isset($this->fields['excel']['list'][$list_f_key]['values'])) && ($filter !== 'text')) {


//							if (!is_array($cookie_list_f)) {
//								$this->fields['where'] .= sprintf(' AND ' . $where_i . ' = %s', GetSQL($cookie_list_f, 'text'));
//							} else {
//								$arr_where = array();
//								foreach ($cookie_list_f as $i => $v) {
//									if ($v != '') $arr_where[] = sprintf($where_i . ' = %s', GetSQL($v, 'text'));
//								}
//								if (count($arr_where) > 0) $this->fields['where'] .= ' AND (' . implode(' OR ', $arr_where) . ')';
//							}

							if (isset($this->fields['list'][$list_f_key]['operation']) && !empty($this->fields['list'][$list_f_key]['operation'])) {
								$operation = $this->fields['list'][$list_f_key]['operation'];
							} else {
								$operation = 'OR';
							}

							if (isset($this->fields['list'][$list_f_key]['in_table']) && $this->fields['list'][$list_f_key]['in_table']) {    //陣列存在資料表裡面
								if (!is_array($cookie_list_f)) {
									$this->fields['where'] .= sprintf(' AND ' . $where_i . ' LIKE %s', GetSQL($cookie_list_f, 'defined', '\'%"' . _addslashes($cookie_list_f) . '"%\'', '"" AND FALSE'));
								} else {
									$arr_where = [];
									foreach ($cookie_list_f as $i => $v) {
										if ($v != '') {
											$arr_where[] = sprintf($where_i . ' LIKE %s', GetSQL($v, 'defined', '\'%"' . _addslashes($v) . '"%\'', '"" AND FALSE'));
										}
									}
									if (count($arr_where) > 0) {
										$this->fields['where'] .= ' AND ((' . implode(') ' . $operation . ' (', $arr_where) . '))';
									}
								}
							} else {    //陣列不存在資料表裡面
								if (!is_array($cookie_list_f)) {
									$this->fields['where'] .= sprintf(' AND ' . $where_i . ' = %s', GetSQL($cookie_list_f, 'text'));
								} else {
									$arr_where = [];
									foreach ($cookie_list_f as $i => $v) {
										if ($v != '') {
											$arr_where[] = sprintf($where_i . ' = %s', GetSQL($v, 'text'));
										}
									}
									if (count($arr_where) > 0) {
										$this->fields['where'] .= ' AND (' . implode(' ' . $operation . ' ', $arr_where) . ')';
									}
								}
							}


						} elseif ($filter === 'range' || $filter === 'time_range' || $filter === 'date_range' || $filter === 'datetime_range') {
							if (!empty($cookie_list_f[0]))
								$this->fields['where'] .= sprintf(' AND %s <= ' . $where_i, GetSQL($cookie_list_f[0], 'text'));
							if (!empty($cookie_list_f[1]))
								$this->fields['where'] .= sprintf(' AND ' . $where_i . ' <= %s', GetSQL($cookie_list_f[1], 'text'));
						} else {
							$this->fields['where'] .= sprintf(' AND ' . $where_i . ' LIKE %s', GetSQL($cookie_list_f, 'defined', "'%" . $cookie_list_f . "%'"));
						}

					} else {
						$this->fields['excel']['list'][$list_f_key]['string_format'] = '';
					}
				}
			}
			$orderby = Tools::getValue('orderby');
			if (isset($this->fields['where']))
				$sql2 .= $this->fields['where'];
			$orderway = (Tools::getValue('orderway')) ? Tools::getValue('orderway') : 'ASC';

			if (!empty($orderby)) {
				$this->fields['order'] = ' ORDER BY `' . $orderby . '` ' . $orderway;
			} elseif (in_array('position', array_keys($this->fields['excel']['list']))) {
				$this->fields['order'] = ' ORDER BY `position` ' . $orderway;
			} elseif (empty($this->fields['order'])) {
				$this->fields['order'] = ' ORDER BY `' . $this->fields['index'] . '` ' . $orderway;
				if (!empty($this->_as))
					$this->fields['order'] = ' ORDER BY ' . $this->_as . '.`' . $this->fields['index'] . '` ' . $orderway;
			}
			if (isset($this->_group))
				$sql2 .= $this->_group;
			if (isset($this->fields['order']))
				$sql2 .= $this->fields['order'];
			$db        = new db_mysql($sql2, 0,'','p','m',$this->conn);
			$this->sql = $sql2;
			/*
			 * 存到cookie 給Ajax用
			 */
			setcookie('list_filter', '', time() - 3600, '/');
			setcookie('list_filter', serialize(Context::getContext()->cookie['list_filter']), time() + 3600, '/');
			$num        = $db->num();
			$total_page = $db->total_page();
			if ($num > 0) {
				while ($val = $db->next_row()) {
					foreach ($val as $key => $val_v) {
						if (in_array($key, array_keys($arr_related))) {
							$arr_related[$key]['val'][] = $val[$arr_related[$key]['key']];
						}
					}
					if (isset($val['position']))
						$val['position'] = '<div class="drag_group glyphicon glyphicon-move" data-id="' . $val[$this->fields['index']] . '"><div class="positions">' . $val['position'] . '</div></div>';
					$this->fields['list_val'][] = $val;
				}
			}
			foreach ($arr_related as $i => $related_v) {    //套內容
				$related_var[$i]        = [];
				$arr_related[$i]['val'] = array_unique($related_v['val']);
				$sql                    = $filter_sql[$i];

				if ((count($this->fields['list_filter']) == 0) || (!in_array($i, $this->fields['list_filter']))) {        //沒有篩選
					$sql .= '
						WHERE `' . $related_v['key'] . '` IN (' . implode(', ', $related_v['val']) . ')
						LIMIT 0, ' . count($related_v['val']);
				}

                //因切換資料庫而變動的參數
//				$db                          = new db_mysql($sql);
                $db                          = new db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm',$this->conn);
				if ($db->num() > 0) {
					$related_var[$i] = [];
					while ($db_v = $db->next_row()) {
						$related_var[$i][$db_v[$related_v['key']]] = [];

						if (!is_array($related_v['key_v'])) {
							$related_var[$i][$db_v[$related_v['key']]]['v'] = $db_v[$related_v['key_v']];
						} else {
							if (empty($related_v['val_divide']))
								$related_v['val_divide'] = '%s - %s';
							$arr_val = [];
							foreach ($related_v['key_v'] as $it => $av) {
								$arr_val[] = $db_v[$av];
							}
							$related_var[$i][$db_v[$related_v['key']]]['v'] = vsprintf($related_v['val_divide'], $arr_val);
						}


						if (!is_array($related_v['text'])) {
							$related_var[$i][$db_v[$related_v['key']]]['text'] = $db_v[$related_v['text']];
						} else {
							if (empty($related_v['text_divide']))
								$related_v['text_divide'] = '%s - %s';
							$arr_text = [];
							foreach ($related_v['text'] as $it => $related_v_t) {
								$arr_text[] = $db_v[$related_v_t];
							}
							$related_var[$i][$db_v[$related_v['key']]]['text'] = vsprintf($related_v['text_divide'], $arr_text);
						}
					}
				}
				unset($arr_related[$i]);
			}
			foreach ($this->fields['list_val'] as $i => $v) {
				$this->fields['list_val'][$i]['tr_class'] = '';
				foreach ($this->fields['excel']['list'] as $j => $excel_list_v) {
					$space = $excel_list_v['space'];
					if (empty($space))
						$space = ',';
					if (isset($excel_list_v['key'])) {        //table 下拉選單
						if ($excel_list_v['in_table']) {    //陣列存表單
							$arr_v = $json->decode($this->fields['list_val'][$i][$j]);
							foreach ($arr_v as $ai => $av) {
								$arr_v[$ai] = ($av) ? $related_var[$j][$av]['v'] : '-';        //取代值
							}
							$this->fields['list_val'][$i][$j] = $arr_v;
							$this->fields['list_val'][$i][$j] = implode($space, $this->fields['list_val'][$i][$j]);
						} else {
							$val                              = $this->fields['list_val'][$i][$j];
							$this->fields['list_val'][$i][$j] = ($val) ? $related_var[$j][$val]['v'] : '-';        //取代值
						}
					}

//					if (isset($excel_list_v['multiple'])
//						&& $excel_list_v['multiple']
//						&& !isset($excel_list_v['values'])) {
//						$this->fields['list_val'][$i][$j] = $json->decode($this->fields['list_val'][$i][$j]);
//						if(isset($this->fields['list'][$j]['key'])){
//							$arr_v = $this->fields['list_val'][$i][$j];
//							foreach($arr_v as $ai => $av){
//								$arr_v[$ai] = ($av) ? $related_var[$j][$av]['v'] : '-';		//取代值
//							}
//							$this->fields['list_val'][$i][$j] = $arr_v;
//						}
//						$this->fields['list_val'][$i][$j] = implode($space, $this->fields['list_val'][$i][$j]);
//					}
					/*
					 * 取代內容
					 */
					if (is_array($excel_list_v['replace'])) {
						$this->fields['list_val'][$i][$j] = implode($excel_list_v['replace'][1], explode($excel_list_v['replace'][0], $this->fields['list_val'][$i][$j]));
					}
				}
			}
			if (empty($this->fields['list_footer']))
				$this->fields['list_footer_display'] = false;
			$Context                        = Context::getContext();
			FormTable::getContext()->fields = $this->fields;
			$file                           = WEB_DIR . DS . FILE_DIR . DS . 'excel' . DS . 'excel.xlsx';
			$row                            = 1;
			$column                         = 0;
			if (filemtime($this->fields['excel']['file'])) {
				$file = $this->fields['excel']['file'];
				if (isset($this->fields['excel']['row'])) {
					$row = $this->fields['excel']['row'];
				}
				if (isset($this->fields['excel']['column'])) {
					$column = $this->fields['excel']['column'];
				}
			}
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			$r           = $row;
			$c           = $column;
			if ($this->fields['excel']['show_list_title']) {
				foreach ($this->fields['excel']['list'] as $thead) {
					if (!$thead['hidden']) {
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $r, $thead['title']);
						$c++;
					}
				}
				$r++;
			}
			foreach ($this->fields['list_val'] as $list) {
				$c = $column;
				foreach ($this->fields['excel']['list'] as $i => $thead) {
					if (!$thead['hidden']) {
						$v = '';
						if ($thead['values']) {
							$in_arr = false;
							foreach ($thead['values'] as $thv_i => $val_v) {
								if ($list[$i] == $val_v['value'])
									$v = $val_v['title'];
								$in_arr = true;
							}
							if (!$in_arr)
								$v = '-';
						} else {
							if (isset($thead['number_format'])) {
								$v = number_format($list[$i], $thead['number_format'], '.', ',');
							} else {
								$v = $list[$i];
							}
						}
						if ($v == '0000-00-00' || $v == '00.00.00')
							$v = '';
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $r, $v);
						$c++;
					}
				}
				$r++;
			}
			//直接輸出到瀏覽器
			$sql        = sprintf('SELECT `first_name` , `first_name`
				FROM `' . DB_PREFIX_ . 'admin`
				WHERE `id_admin` = %d
				LIMIT 0, 1',
				$_SESSION['id_admin']);
			$row        = Db::rowSQL($sql,true,0,$this->conn);
			$last_name  = $row['last_name'];
			$first_name = $row['first_name'];

			$Creator        = sprintf($this->l('%s %s'), $first_name, $last_name);
			$LastModifiedBy = '';
			$Title          = sprintf($this->l('%s-%s'), $this->fields['title'], WEB_NAME);
			$Subject        = sprintf($this->l('%s %s'), WEB_NAME, now());
			$Description    = sprintf($this->l('%s_%s 匯出資料,匯出時間: %s'), $this->fields['title'], WEB_NAME, now());
			$Category       = sprintf($this->l('%s %s'), WEB_NAME, now());
			$filename       = sprintf($this->l('%s_%s %s'), $this->fields['excel']['title'], WEB_NAME, date('YmdHis', time()));
//			for($i=0;$i<=$c;$i++){
//				$objPHPExcel->getActiveSheet()->getColumnDimension(chr(65))->setAutoSize(true);
//			}

			//$objPHPExcel->getActiveSheet()->setCellValue('Q1', $this->l('匯入狀態'));
			//創建人
			$objPHPExcel->getProperties()->setCreator($Creator);
			//最後修改人
			$objPHPExcel->getProperties()->setLastModifiedBy($LastModifiedBy);
			//標題
			$objPHPExcel->getProperties()->setTitle($Title);
			//題目
			$objPHPExcel->getProperties()->setSubject($Subject);
			//描述
			$objPHPExcel->getProperties()->setDescription($Description);
			//關鍵字
			$objPHPExcel->getProperties()->setKeywords($filename);
			//種類
			$objPHPExcel->getProperties()->setCategory('文件表格');
			//設置工作表名稱
			$objPHPExcel->getActiveSheet()->setTitle($this->fields['title']);

			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control:must-revalidate, post-check=0, pre-check=0');
			header('Content-Type:application/force-download');
			header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Type:application/octet-stream');
			header("Content-Type:application/download");
			header('Content-Disposition:attachment;filename="' . $filename . '.xlsx"');
			header('Content-Transfer-Encoding:binary');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->setIncludeCharts(true); //圖表
			$objWriter->save('php://output');
			exit;
		}
	}

	public
	function l($str)
	{
		return $str;
	}
}