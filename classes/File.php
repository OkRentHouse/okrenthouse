<?php
class File{
	
	protected static $instance;
	public $_errors	= array();		//錯誤訊息
	public $_msgs	= array();
	public $msg		= '';
	public function __construct(){
		
	}
	
	public function competence(){
		$competences = array(
			array('file'		=>'檔案管理',		'view'=>'觀看','add'=>'新增','update'=>'修改','del'=>'刪除'),
			array('file_group'	=>'檔案群組',		'view'=>'觀看','add'=>'新增','update'=>'修改','del'=>'刪除')
		);
		return $competences;
	}
	
	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new File();
		};
		return self::$instance;
	}

	public static function get($id_file){
		$sql = sprintf('SELECT *
			FROM `'.DB_PREFIX_.'file`
			WHERE `id_file` = %d ',
			GetSQL($id_file, 'int'));
		$row = Db::rowSQL($sql, true);
		if(count($row)){
			$url = str_replace(WEB_DIR, '', $row['file_url'] . $row['filename']);
			$url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
			$row['url'] = $url;
		}
		return $row;
	}

	public static function check_post(){
		switch($_POST['action']){
			case 'update_file':
			
			case 'add_file':
			break;
			case 'del_file':
			break;
			case 'update_file_group':
				if(!isint($_POST['id_file_group'])) File::getContext()->_errors[] = '找不到檔案';
			case 'add_file_group':
				if(empty($_POST['group_name'])) File::getContext()->_errors[] = '請輸入檔案群組名稱';
				File::check_group_name($_POST['group_name']);
			break;
		};
	}
	
	public static function check_group_name($group_name, $id_file_group=NULL){
		if(!empty($id_file_group)) $WHERE = ' AND `id_file_group` != '.$id_file_group;
		$sql = 'SELECT `id_file_group`
			FROM `file_group`
			WHERE `group_name` = "'.$group_name.'"'.$WHERE.'
			LIMIT 0, 1';
		$SELECT_file_group = new db_mysql($sql);
		if($SELECT_file_group->num()){
			File::getContext()->_errors[] = '檔案群組名稱重複!';
			return false;
		};
		return true;
	}
	
	public static function get_files_date($start, $end=NULL, $id_file_group=NULL){
		if(empty($id_language)){
			$Language = new Language();
			$id_language = $Language->id_by_code();
		};
		$WHERE = ' AND fd.`file_date` >= "'.$start.'"';
		if(!empty($end)) $WHERE .= ' AND fd.`file_date` < "'.$end.'"';
		if(!empty($id_file_group)){
			$LEFT = 'LEFT JOIN `file_in_group` AS fig ON fig.`id_file` = f.`id_file`';
			if(!is_array($id_file_group)) $id_file_group = array($id_file_group);
			$WHERE .= ' AND fig.`id_file_group` IN ('.implode(', ', $id_file_group).')';
		};
		$sql = 'SELECT f.`id_file`, ft.`id_language`, f.`action`, ft.`file_name`, f.`type`, f.`file_extension`, fd.`file_date`
			FROM `file` AS f
			LEFT JOIN `file_name` AS ft ON ft.`id_file` = f.`id_file`
			LEFT JOIN `file_date` AS fd ON fd.`id_file` = f.`id_file`'.$LEFT.'
			WHERE f.`id_file` '.$WHERE.'
			AND ft.`id_language` = '.$id_language.'
			AND f.`action` = 1
			GROUP BY f.`id_file`';
		$SELECT_file = new db_mysql($sql);
		$num = $SELECT_file->num();
		$arr_file = array();
		if($num > 0){
			while($file = $SELECT_file->next_row()){
				$arr_file[] = $file;
			};
		};
		return array($num, $arr_file);
	}
	
	//新增多數檔案
	public static function add_files($file_name,$titls_name,$action = 1){
		if(!is_array($_FILES[$file_name]['tmp_name'])){
			$type = $_FILES[$file_name]['type'];
			$type = $_FILES[$file_name]['type'];		//檔案形態
			$exy = ext($_FILES[$file_name]['name']);		//副檔名
			$title = $_POST[$titls_name];				//自訂檔名
			$id_file = File::add_file($type,$exy,$action);			//建立檔案資料庫
			File::add_file_title($id_file,$_POST[$titls_name],$id_language);	//建立檔案自訂名稱
			list($str_dir,$str_src) = str_dir_change($id_file);
			$dir = FILE_DIR.DS.$str_dir;
			add_dir($dir);
			move_uploaded_file($_FILES[$file_name]['tmp_name'], $dir.'x');	//上傳檔案
			return array(1,array($id_file));
		};
		$arr_id_file = array();
		foreach($_FILES[$file_name]['name'] as $i => $name){
			$type = $_FILES[$file_name]['type'][$i];		//檔案形態
			$exy = ext($name);			//副檔名
			$title = $_POST[$titls_name][$i];	//自訂檔名
			$id_file = File::add_file($type,$exy,$action);			//建立檔案資料庫
			$arr_id_file[] = $id_file;
			File::add_file_title($id_file,$title,$id_language);	//建立檔案自訂名稱
			list($str_dir,$str_src) = str_dir_change($id_file);
			$dir = FILE_DIR.DS.$str_dir;
			add_dir($dir);
			move_uploaded_file($_FILES[$file_name]['tmp_name'][$i], $dir.'x');	//上傳檔案
		};
		return array(count($arr_id_file),$arr_id_file);
	}
	
	public static function get_file_date($id_file){
		if(empty($id_file)) return false;
		$sql = 'SELECT `file_date` FROM `file_date` WHERE `id_file` = '.$id_file;
		$SELECT_file_date = new db_mysql($sql);
		if($SELECT_file_date->num() > 0){
			$file_date = $SELECT_file_date->row();
		};
		return $file_date;
	}

	//新增檔案時間
	public static function add_file_date($id_file, $file_date){
		$sql = sprintf('INSERT INTO `file_date` (`id_file`, `file_date`) VALUES (%d, %s)',
					GetSQL($id_file,'int'),
					GetSQL($file_date,'text'));
		$INSERT_file_date = new db_mysql($sql);
		$num = $INSERT_file_date->num();
		return ($num) ? $num : false;
	}
	
	public static function add($type, $exy ,$action=1){
		File::add_file($type,$exy,$action);
	}
	
	//新增檔案
	public static function add_file($type,$exy,$action=1){
		$sql = sprintf('INSERT INTO `file` (`type`, `file_extension`, `action`) VALUES (%s, %s, %d)',
					GetSQL($type,'text'),
					GetSQL($exy,'text'),
					GetSQL($action,'int'));
		$INSERT_file = new db_mysql($sql);
		$num = $INSERT_file->num();
		return ($num) ? $num : false;
	}
	
	public static function update($id_file, $action){
		File::update_file($id_file, $action);
	}
	
	public static function update_file($id_file, $action){
		$sql = sprintf('UPDATE `file` SET `action` = %d
					WHERE `id_file` = %d',
					GetSQL($action,'int'),
					GetSQL($id_file,'int'));
		$db_file = new db_mysql($sql);
		$num = $db_file->num();
		return ($num) ? $num : false;
	}
	
	public static function update_file_date($id_file, $file_date){
		$sql = sprintf('UPDATE `file_date` SET `file_date` = %s
					WHERE `id_file` = %d',
					GetSQL($file_date,'text'),
					GetSQL($id_file,'int'));
		$db_file_date = new db_mysql($sql);
		$num = $db_file_date->num();
		return ($num) ? $num : false;
	}
	
	//新增檔案
	public static function add_file_title($id_file,$file_name,$id_language=NULL){
		if(empty($id_language)){
			$Language = new Language();
			$id_language = $Language->id_by_code();
		};
		$sql = sprintf('INSERT INTO `file_name` (`id_language`, `id_file`, `file_name`) VALUES (%d, %d, %s)',
					GetSQL($id_language,'int'),
					GetSQL($id_file,'int'),
					GetSQL($file_name,'text'));
		$INSERT_file = new db_mysql($sql);
		$num = $INSERT_file->num();
		return ($num) ? $num : false;
	}
	
	//新增檔案群組
	public static function chech_group_name($group_name){
		$sql = sprintf('SELECT `id_file_group`
					FROM `file_group`
					WHERE `group_name` = %s',
					GetSQL($group_name,'text'));
		$SELECT_file_group = new db_mysql($sql);
		return ($SELECT_file_group->num()) ? false : true;
	}
	
	public static function add_group($group_name, $id_language=NULL){
		return File::add_file_group($group_name, $id_language);
	}
	
	//新增檔案群組
	public static function add_file_group($group_name, $id_language=NULL){
		if(!File::chech_group_name($group_name)) return false;
		$sql = sprintf('INSERT INTO `file_group` (`group_name`) VALUES (%s)',
					GetSQL($group_name,'text'));
		$INSERT_file_group = new db_mysql($sql);
		$num = $INSERT_file_group->num();
		return ($num) ? $num : false;
	}
	
	public static function update_group($id_file_group, $group_name){
		return File::update_file_group($id_file_group, $group_name);
	}
	
	//修改檔案群組
	public static function update_file_group($id_file_group, $group_name){
		$sql = sprintf('UPDATE `file_group` SET `group_name` = %s
					WHERE `id_file_group` = %d',
					GetSQL($group_name,'text'),
					GetSQL($id_file_group,'int'));
		$UPDATE_file_group = new db_mysql($sql);
		$num = $UPDATE_file_group->num();
		File::getContext()->msg = '沒有修改任何資料!';
		return ($num) ? $num : false;
	}
	
	//修改檔案群組
	public static function update_file_in_group($id_file, $id_file_group, $msg_type=true){
		$sql = 'INSERT INTO `file_in_group` (`id_file`, `id_file_group`)
			VALUES ('.$id_file.', '.$id_file_group.')
			ON DUPLICATE KEY
			UPDATE `id_file_group` = '.$id_file_group;
		$UPDATE_file_in_group = new db_mysql($sql);
		$num = $UPDATE_file_in_group->num();
		if($msg_type){
			if($num > 0){
				File::getContext()->msg = '修改成功!';
			}else{
				File::getContext()->msg = '沒有修改任何資料!';
			};
		};
		return ($num) ? $num : false;
	}
	
	public static function del_file_date($id_file){
		if(!isint($id_file)) return false;
		$sql = 'DELETE FROM `file_date` WHERE `id_file` = '.$id_file;	//刪除檔案
		$DELETE_file_date = new db_mysql($sql);
		return $DELETE_file_date->num();
	}
	
	public static function del_file_name($id_file){
		if(!isint($id_file)) return false;
		$sql = 'DELETE FROM `file_name` WHERE `id_file` = '.$id_file;	//刪除檔案
		$DELETE_file_name = new db_mysql($sql);
		return $DELETE_file_name->num();
	}
	
	
	//刪除檔案
	public static function del_file($id_file){
		if(!is_array($id_file)) $id_file = array($id_file);
		$num = 0;
		foreach($id_file as $k=>$del){		//刪除檔案
			list($str_dir,$str_src) = str_dir_change($del);
			$dir = FILE_DIR.DS.$str_dir;
			unlink($dir.'x');
			$sql = 'DELETE FROM `file` WHERE `id_file` = '.$del;	//刪除檔案
			$DELETE_file = new db_mysql($sql);
			$num += $DELETE_file->num();
			File::del_file_date($id_file);
			File::del_file_name($id_file);
			File::del_file_in_group($id_file);
		};
		return $num;
	}
	
	public static function update_file_name($id_file,$file_name,$id_language=NULL){
		if(empty($id_language)){
			$Language = new Language();
			$id_language = $Language->id_by_code();
		};
		$sql = sprintf('UPDATE `file_name` SET `file_name` = %s
					WHERE `id_file` = %d',
					GetSQL($file_name,'text'),
					GetSQL($id_file,'int'));
		$db_news = new db_mysql($sql);
		$num = $db_news->num();
		return ($num) ? $num : false;
	}
	
	public static function del_group($id_file_group, $msg_type=true){
		return File::del_file_group($id_file_group, $msg_type);
	}
	
	public static function del_file_group($id_file_group, $msg_type=true){
		if(!isint($id_file_group)) return false;
		$sql = 'DELETE FROM `file_group` WHERE `id_file_group` = '.$id_file_group;	//刪除檔案
		$DELETE_file_group = new db_mysql($sql);
		$num = $DELETE_file_group->num();
		if($num > 0){
			File::del_file_in_group('id_file_group', $id_file_group);
		};
		if($msg_type){
			if($num > 0) {
				File::getContext()->_msgs[] = '刪除成功!';
			}else{
				File::getContext()->_errors[] = '沒有刪除任何群組!';
			};
		};
		return $num;
	}
	
	//下載路徑
	public static function download_link($id_file,$none_url=NULL){
		list($str_dir,$str_src) = str_dir_change($id_file);
		$file_dir = FILE_DIR.DS.$str_dir.'x';
		$download_link = WEB_DNS.URL_DOWNLOAD.$id_file;
		if(!is_file($file_dir) && !empty($none_url))$download_link = $none_url;
		return $download_link;
	}
	
	public static function download_file($id_file){
		if(!empty($id_file)){
			list($num,$arr_file) = File::get_file($id_file);
			list($str_dir,$str_src) = str_dir_change($id_file);
			if($num > 0 && is_file(FILE_DIR.DS.$str_dir.'x')){
				$file = $arr_file[0];
				if(!empty($file['file_name'])){
					print_file(FILE_DIR.DS.$str_dir.'x',$file['file_name'].'.'.$file['file_extension']);
					exit;
				};
			};
		};
		gotourl('/download/找不到檔案');
	}
	
	//取出檔案資料
	public static function get_file($id_file,$id_language=NULL){
		if(empty($id_language)){
			$Language = new Language();
			$id_language = $Language->id_by_code();
		};
		
		$arr_file = array();
		if(!is_array($id_file)){
			if(empty($id_file)) return array(0, $arr_file);
			$WHERE = '= '.$id_file;
			$LIMIT = 'LIMIT 0, 1';
		}else{
			if(count($id_file) == 0) return array(0, $arr_file);
			$WHERE = 'IN ('.implode(',',$id_file).')';
			$LIMIT = '';
		};
		
		$sql = 'SELECT f.`id_file`, ft.`id_language`, f.`action`, ft.`file_name`, f.`type`, f.`file_extension`, fd.`file_date`
			FROM `file` AS f
			LEFT JOIN `file_name` AS ft ON ft.`id_file` = f.`id_file`
			LEFT JOIN `file_date` AS fd ON fd.`id_file` = f.`id_file`
			WHERE f.`id_file` '.$WHERE.'
			AND ft.`id_language` = '.$id_language.'
			GROUP BY f.`id_file`
			'.$LIMIT;
		$SELECT_file = new db_mysql($sql);
		$num = $SELECT_file->num();
		if($num > 0){
			while($file = $SELECT_file->next_row()){
				$file['link'] = File::download_link($file['id_file']);
				$arr_file[] = $file;
			};
		};
		return array($num,$arr_file);
	}
	
	public static function get_group($id_file_group=NULL){
		return File::get_file_group($id_file_group);
	}
	
	public static function check_member_file($id_file){
		if($_SESSION['id_group'] == 1) return true;
		if(empty($_SESSION['id_member_group'])) return false;
		$arr_file_member_group = File::get_file_member_group('id_member_group', $_SESSION['id_member_group']);
		$arr_id_file_group = array();
		if(count($arr_file_member_group) == 0) return false;
		foreach($arr_file_member_group as $i => $v){
			$arr_id_file_group[] = $v['id_file_group'];
		};
		$arr_file_group = File::get_file_in_group($id_file);
		foreach($arr_file_group as $i => $file_in_group){
			if(in_array($file_in_group['id_file_group'], $arr_id_file_group)) return true;
		};
		return false;
	}
	
	public static function get_file_group($id_file_group=NULL){
		$sql = 'SELECT *
			FROM `file_group`';
		if(!empty($id_file_group)){
			if(is_array($id_file_group)){
				$sql .= ' WHERE `id_file_group` IN ('.implode(',', $id_file_group).')
				LIMIT 0, '.count($id_file_group);
			}else{
				if(!isint($id_file_group)) return false;
				$sql .= ' WHERE `id_file_group` = '.$id_file_group.'
				LIMIT 0, 1';
			};
		};
		$SELECT_file_group = new db_mysql($sql);
		$num = $SELECT_file_group->num();
		$arr_file_group = array();
		if($num > 0){
			while($file_group = $SELECT_file_group->next_row()){
				$arr_file_group[] = $file_group;
			};
		};
		return $arr_file_group;
	}
	
	public static function get_file_in_group($id_file=NULL){
		$sql = 'SELECT `id_file_group`
			FROM `file_in_group`';
		if(!empty($id_file)){
			if(is_array($id_file)){
				$sql .= ' WHERE `id_file` IN ('.implode(',', $id_file).')
				LIMIT 0, '.count($id_file);
			}else{
				if(!isint($id_file)) return false;
				$sql .= ' WHERE `id_file` = '.$id_file.'
				LIMIT 0, 1';
			};
		};
		$SELECT_file_in_group = new db_mysql($sql);
		$num = $SELECT_file_in_group->num();
		$arr_file_in_group = array();
		if($num > 0){
			while($file_in_group = $SELECT_file_in_group->next_row()){
				$arr_file_in_group[] = $file_in_group;
			};
		};
		return $arr_file_in_group;
	}
	
	public static function del_file_in_group($type, $id=NULL, $msg_type=true){
		$sql = 'DELETE FROM `file_in_group`';
		if(empty($type)) return false;
		if(is_array($id)){
			$sql .= ' WHERE `'.$type.'` IN ('.implode(', ', $id).')';
		}else{
			if(!isint($id)) return false;
			$sql .= ' WHERE `'.$type.'` = '.$id;
		};
		$DELETE_file_in_group = new db_mysql($sql);
		return $DELETE_file_in_group->num();
	}
	
	public static function get_file_member_group($type=NULL, $id=NULL){
		$sql = 'SELECT `id_file_group`, `id_member_group`
				FROM `file_member_group`';
		if(!empty($type)){
			if(is_array($id)){
				$sql .= ' WHERE `'.$type.'` IN ('.implode(', ', $id).')';
			}else{
				if(!isint($id)) return false;
				$sql .= ' WHERE `'.$type.'` = '.$id;
			};
		};
		$SELECT_file_member_group = new db_mysql($sql);
		$arr_file_member_group = array();
		if($SELECT_file_member_group->num() > 0){
			while($file_member_group = $SELECT_file_member_group->next_row()){
				$arr_file_member_group[] = $file_member_group;
			};
		};
		return $arr_file_member_group;
	}
	
	public static function del_file_member_group($type ,$id){
		$sql = 'DELETE FROM `file_member_group`';
		if(empty($type)) return false;
		if(is_array($id)){
			$sql .= ' WHERE `'.$type.'` IN ('.implode(', ', $id).')';
		}else{
			if(!isint($id)) return false;
			$sql .= ' WHERE `'.$type.'` = '.$id;
		};
		$DELETE_file_member_group = new db_mysql($sql);
		return $DELETE_file_member_group->num();
	}
	
	public static function update_file_member_group($id_member_group ,$id_file_group){
		if(empty($id_member_group)) return false;
		$num = 0;
		$num += File::del_file_member_group('id_member_group', $id_member_group);
		if(!is_array($id_file_group)) $id_file_group = array($id_file_group);
		if(count($id_file_group) == 0) return $num;
		$INSERT = implode(') ,('.$id_member_group.', ', $id_file_group);
		$sql = 'INSERT INTO `file_member_group` (`id_member_group`, `id_file_group`)
			VALUES ('.$id_member_group.', '.$INSERT.')';
		$INSERT_file_member_group = new db_mysql($sql);
		$num += $INSERT_file_member_group->num();
		return $num;
	}
	
	public function getFileList ($dir=null){
		$arr_file = array();
		if(!empty($dir)){
			$file_ex = '.php';
			if(is_dir($dir)){
				if ($dh = opendir($dir)) {
					while(($file = readdir($dh)) !== false) {
						//只讀過取出php 的檔案
						if ($file != '.' && $file != '..') {
							if (strpos($file, $file_ex)) {      //是檔案
								$arr_file[] = $file;
								
							}
						}
					}
				}
			}
		}
		return $arr_file;
	}
}