<?php

class Google_api{
    protected static $instance;

    public function __construct(){

    }

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new Google_api();
        };
        return self::$instance;
    }

    public static function get_lat_and_long($address,$key="AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs"){//取得經緯度 key預設使用生活樂租
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$key}";//取得經緯度的網址
        return Google_api::get_curl($url);
    }

    public static function get_data_lat_and_long($json){//回傳json的經緯值
        $de_json = json_decode($json,true);
        $return_arr = "";
        //print_r($de_json);
        if($de_json["status"]=="OK"){
            foreach($de_json["results"] as $key => $value){
                $return_arr[] = [
                                    "lat"=>$de_json["results"][$key]["geometry"]["location"]["lat"],
                                    "lng"=>$de_json["results"][$key]["geometry"]["location"]["lng"]
                                ];
            }
        }
        return $return_arr;
    }


    public static function get_curl($url,$type="GET"){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}

?>