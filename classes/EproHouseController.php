<?php

include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');

class EproHouseController extends FrontController

{
    public function __construct()
    {
        parent::__construct();
        $this->context->smarty->assign([

            'footer_link'   => [

                [

                    'txt'  => '生活集團',

                    'link' => 'https://www.lifegroup.house/',

                ],

                [

                    'txt'  => '生活房屋',

                    'link' => 'https://www.life-house.tw',

                ],

                [

                    'txt'  => '生活好科技',

                    'link' => '',

                ],

                [

                    'txt'  => '生活居家修繕',

                    'link' => '',

                ],


                [

                    'txt'  => '生活樂購',

                    'link' => 'https://www.lifegroup.house/LifeGo',

                ],

                [

                    'txt'  => '共好夥伴',

                    'link' => '',

                ],

                [

                    'txt'  => '相關聲明',

                    'link' => '/statement',

                ],

            ],

            'footer_txt1'   => Config::get('footer_txt1'),

            'footer_txt2'   => Config::get('footer_txt2'),

            'footer_txt3'   => Config::get('footer_txt3'),

            'footer_qrcode' => 'https://www.life-house.com.tw',

            //			'qrcod_img'     => '/img/okrent/footer_qr_code.png',

        ]);
    }



    public function setMedia()

    {



//		$this->addHeaderCSS('/' . MEDIA_URL . '/uikit-2.25.0/css/uikit.gradient.min.css');

//		$this->addHeaderCSS('/' . MEDIA_URL . '/uikit-2.25.0/css/uikit.almost-flat.min.css');

//		$this->addHeaderJS('/' . MEDIA_URL . '/uikit-2.25.0/js/uikit.min.js');

//

//		$this->addHeaderCSS('/' . MEDIA_URL . '/uikit-2.25.0/css/components/slideshow.min.css');

//		$this->addHeaderJS('/' . MEDIA_URL . '/uikit-2.25.0/js/components/slideshow.min.js');

        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        parent::setMedia();

//		$this->addCSS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/css/bootstrap-select.min.css');

//		$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/js/bootstrap-select.min.js');

    }



    /**

     * 加入我的最愛

     */

//    public function ajaxProcessAddFavorite() {
//        $id   = Tools::getValue('id');
//        $type = Tools::getValue('type');
//        $on   = Tools::getValue('on');
//        switch ($type) {
//            case 'house':    //收藏房屋
//                break;
//            case 'store':    //收藏店家
//                break;
//        }
//    }

}