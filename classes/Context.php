<?php
class Context{
	public $cookie;	/*array(
					'no_mobile' => true, //使否不開放手機版
					'id_guest' => 訪客ID
				)*/
	public $link;
	public $js_list;
	public $css_list;
	public $language;
	public $tab;
	public $web;
	public $theme;
	public $smarty;
	public $mode;
	public $controller;
	public $mobile_device;						//網站是否開放手機版本
	public $isMobile;		//是否是手機
	public $isTablet;		//是否是平版
	
	public $display_header = true;
	public $display_footer = true;
	public $display_header_javascript = true;
	public $display_footer_javascript = true;
	
	protected static $instance		= NULL;		//是否實立

	public function __construct() {
		global $Smarty;
		if(empty($this->smarty)) $this->smarty = $Smarty;
	}

	public static function getContext(){
		if(!isset(self::$instance)){
			self::$instance = new Context();
		};
		return self::$instance;
	}

	public function reser(){
		self::$instance->cookie	= '';
		self::$instance->link	= '';
		self::$instance->language	= '';
		self::$instance->mode	= '';
		self::$instance->smarty	= '';
		self::$instance->tab		= '';
		self::$instance->theme	= '';
		self::$instance->web		= '';
		return $this;
	}

	public function cloneContext(){
		return clone($this);
	}
	
	
	/**
	 * Sets Mobile_Detect tool object
	 *
	 * @return Mobile_Detect
	 */
	public function getMobileDetect()
	{
		if ($this->mobile_detect === null) {
			$this->mobile_detect = new Mobile_Detect();
		}
		return $this->mobile_detect;
	}
	
	/**
	 *  判別是不是手機
	 *
	 * @return bool
	 */
	public function isMobile()
	{
		if ($this->is_mobile === null) {
			$mobile_detect = $this->getMobileDetect();
			$this->is_mobile = $mobile_detect->isMobile();
		}
		return $this->is_mobile;
	}
	
	/**
	 * 判別是不是平版
	 * @return bool
	 */
	public function isTablet()
	{
		if ($this->is_tablet === null) {
			$mobile_detect = $this->getMobileDetect();
			$this->is_tablet = $mobile_detect->isTablet();
		}
		return $this->is_tablet;
	}
	
	//判斷網站是否開放手機版本
	public function getMobileDevice(){

		if($this->mobile_device === null){
			$this->mobile_device = false;
			if(Dispatcher::getContext()->is_media || Dispatcher::getContext()->is_app) {		//手機網址 不管如何都是手機樣版
				$this->mobile_device = true;
			}else if($this->checkMobileContext()){		//判別使用者是否指定電腦版
				$arr_WEB_ALLOW_MOBILE_DEVICE = JSON::getContext()->decode(Configuration::get('WEB_ALLOW_MOBILE_DEVICE'));
				if(isset(Context::getContext()->cookie->no_mobile) && Context::getContext()->cookie->no_mobile == false && count($arr_WEB_ALLOW_MOBILE_DEVICE)){
					if($this->isMobile() && in_array(1, $arr_WEB_ALLOW_MOBILE_DEVICE)){	//允許手機
						$this->mobile_device = true;
					}
					if($this->isTablet() && in_array(2, $arr_WEB_ALLOW_MOBILE_DEVICE)){	//允許平版
						$this->mobile_device = true;
					}
				};
			};
			
		};
		return $this->mobile_device;
	}

	//判斷瀏覽者樣版設訂
	protected function checkMobileContext(){
		if(Tools::isSubmit('off_mobile')){
			Context::getContext()->cookie->no_mobile = true;
			/*	//儲存訪客資訊
			if (Context::getContext()->cookie->id_guest) {
			    $guest = new Guest(Context::getContext()->cookie->id_guest);
			    $guest->mobile_theme = false;
			    $guest->update();
			}*/
		}elseif(Tools::isSubmit('on_mobile')){
			Context::getContext()->cookie->no_mobile = false;
			/*	//儲存訪客資訊
			if (Context::getContext()->cookie->id_guest) {
			    $guest = new Guest(Context::getContext()->cookie->id_guest);
			    $guest->mobile_theme = false;
			    $guest->update();
			}*/
		};
		$themes = Themes::getContext()->getTheme();
		return isset($_SERVER['HTTP_USER_AGENT'])
		&& isset(Context::getContext()->cookie)
		&& (bool)Config::get('WEB_ALLOW_MOBILE_DEVICE')
		&& @filemtime(THEME_DIR)
		&& !Context::getContext()->cookie->no_mobile;
	}
}