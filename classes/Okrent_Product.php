<?php

class Okrent_Product{
    protected static $instance;
    public function __construct(){
    }
    public $conn = "ilifehou_okrent";

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new Okrent_Product();
        };
        return self::$instance;
    }

    public static function get_data($where=null,$status=1,$order_by=null,$limit=null){//item用的 status 預設上架
        $conn = "ilifehou_okrent";

        $where .=' AND status='.GetSQL($status,"int");//狀態
        if($limit!=null){
            $limit = 'LIMIT '.$limit;
        }
        if($order_by!=null){
            $order_by = 'ORDER BY '.$order_by;
        }else{
            $order_by = 'ORDER BY update_time DESC,create_time DESC';
        }

        $sql = "SELECT * FROM ".Okrent_Product::getContext()->conn.".`product` WHERE TRUE ".$where.' '.$order_by.' '.$limit;
        $data = Db::rowSQL($sql);


        return $data;
    }

    public static function get_id_data($id=null,$status=1,$order_by=null,$limit=null){//item用的 status 預設上架

        if($id==null) return false;//沒給值
        if($limit!=null){
            $limit = 'LIMIT '.$limit;
        }
        if($order_by!=null){
            $order_by = 'ORDER BY '.$order_by;
        }else{
            $order_by = 'ORDER BY update_time DESC,create_time DESC';
        }

        $sql = "SELECT * FROM ".Okrent_Product::getContext()->conn.".`product` WHERE id_product=".GetSQL($id,"int").' AND status='.GetSQL($status,"int").' '.$order_by.' '.$limit;
        $data = Db::rowSQL($sql,true);
        //取得圖片
        $sql = "SELECT * FROM ".Okrent_Product::getContext()->conn.".`product_file` WHERE file_type=0 AND id_product=".GetSQL($data,"int");
        $img = Db::rowSQL($sql);
        $img_url = [];
        if(!empty($img)){
            foreach($img as $i_k =>$i_v){
                $img_file = File::get($img[$i_k]['id_file']);
                $img_url[] = $img_file["url"];//塞進去圖片
            }
            $data['url'] = $img_url;//存入讀出的圖片
        }


        $data = Okrent_Product::getContext()->data_environment($data);
        return $data;
    }

    public function data_environment($data){//基本上就是幫你取得周邊的東西

        $sql = "SELECT * FROM ".Okrent_Product::getContext()->conn.".product_class WHERE active=1 AND id_product_class=".GetSQL($data["id_product_class"],"int");
        $product_class = Db::rowSQL($sql,true);
        $data['product_class'] = $product_class;

        $sql = "SELECT * FROM ".Okrent_Product::getContext()->conn.".product_class_item WHERE active=1 AND id_product_class_item=".GetSQL($data["id_product_class_item"],"int");

        $product_class_item = Db::rowSQL($sql,true);
        $data['product_class_item'] = $product_class_item;

        $sql = "SELECT * FROM county WHERE id_county=".GetSQL($data["id_county"],"int");
        $county = Db::rowSQL($sql,true);
        $data['county'] = $county;

        $sql = "SELECT * FROM city WHERE id_city=".GetSQL($data["id_city"],"int");
        $city = Db::rowSQL($sql,true);
        $data['city'] = $city;

        $sql = "SELECT * FROM ".Okrent_Product::getContext()->conn.".pay WHERE active=1 AND id_pay IN(".Db::antonym_array($data['id_pay']).") ORDER BY position ASC";
        $pay = Db::rowSQL($sql);
        $data['pay'] = $pay;

        $sql = "SELECT * FROM ".Okrent_Product::getContext()->conn.".delivery as d 
        LEFT JOIN ".Okrent_Product::getContext()->conn.".delivery_file as d_f ON d.`id_delivery` = d_f.`id_delivery`
        WHERE d.`active`=1 AND d.`id_delivery` IN(".Db::antonym_array($data['id_delivery']).")  ORDER BY d.`position` ASC";
        $delivery = Db::rowSQL($sql);
        foreach($delivery as $k=>$v){
            if(!empty($v['id_file'])){//取得file url
                $img = File::get($delivery[$k]['id_file']);
                $delivery[$k]['url'] = $img["url"];//塞進去圖片
            }
        }
        $data['delivery'] = $delivery;

        return $data;
    }


}