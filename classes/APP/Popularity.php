<?php


class Popularity
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Popularity();
		};
		return self::$instance;
	}

	/**
	 * 取得星星原始碼
	 * @param $num	星星數
	 *
	 * @return string 原始碼
	 */
	public static function getPopularityImg($id, $num, $rounding=false){
		$img = '';
		for($j=1;$j<=5;$j++){
			if($j <= $num){				//一顆
				$img .= '<img src="'.THEME_URL.'/img/popularity2.svg" data-id="'.$id.'">';
			}elseif($num - $j >= 0.5){	//半顆
				if($rounding){	//顯示整顆星
					$img .= '<img src="'.THEME_URL.'/img/popularity2.svg" data-id="'.$id.'">';
				}else{	//顯示半顆星
					$img .= '<img src="'.THEME_URL.'/img/popularity2_half.svg" data-id="'.$id.'">';
				}
			}else{
				$img .= '<img src="'.THEME_URL.'/img/popularity2_gray.svg" data-id="'.$id.'">';
			}							//灰色
		}
		return $img;
	}
}