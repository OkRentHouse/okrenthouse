<?php
class PhpMail{
	public function __construct(){
		"CREATE TABLE IF NOT EXISTS `php_mailer` (
		  `id_mailer` int(11) NOT NULL AUTO_INCREMENT COMMENT 'mailer ID',
		  `php_mail` tinyint(1) NOT NULL COMMENT '使用PHP mail(1:true,0:false)',
		  `smtp_auth` tinyint(1) NOT NULL COMMENT 'SMTP 驗證(1:true,0:false)',
		  `smtp_secure` varchar(3) COLLATE utf8_unicode_ci NOT NULL COMMENT 'SMTP 加密',
		  `host` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'SMTP 主機',
		  `port` varchar(6) COLLATE utf8_unicode_ci NOT NULL COMMENT 'SMTP Port',
		  `char_set` varchar(5) COLLATE utf8_unicode_ci NOT NULL COMMENT '郵件編碼',
		  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'SMTP 帳號',
		  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'SMTP 密碼',
		  `from_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '計件者信箱',
		  `from_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '計件者名稱',
		  `subject` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT '郵件標題',
		  `address` text COLLATE utf8_unicode_ci NOT NULL COMMENT '收件者地址',
		  `body_before` text COLLATE utf8_unicode_ci COMMENT '郵件內容前文',
		  `body` text COLLATE utf8_unicode_ci COMMENT '郵件預設內容',
		  `body_after` text COLLATE utf8_unicode_ci COMMENT '郵件內容後文',
		  PRIMARY KEY (`id_mailer`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";

	}
}