<?php

class Hook
{
	public $name;                    //Hook name
	public $title;                    //Hook 名稱
	public $description;                //Hook 說明
	public $position = false;            //Hooke 位至 (有顯示才有位置及Live Edit線上編輯)
	public $live_edit = false;            //線上編輯
	public static $arr_hooks = [];        //Hook 陣列
	public static $executed_hooks = [];    //啟用的Hook

	//新增Hook
	public function add($autodate = true, $null_values = false)
	{
		return parent::add($autodate, $null_values);
	}

	//取得所有Hook
	public static function getHooks($position = false)
	{
		return Db::rowSQL('SELECT * FROM `' . DB_PREFIX_ . 'hook`' .
			($position ? 'WHERE `position` = 1' : '') . '
			ORDER BY `name`');
	}

	//以Hook名稱取得Hook ID
	public static function getIdByName($hook_name)
	{
		//測試
		$hook_name = strtolower($hook_name);        //全部轉小寫
		if (!isHookName($hook_name))
			return false;
		$cache_id = 'HookIdByName_' . $hook_name;
		if (!Cache::is_set($cache_id)) {
			//取得所有Hook ID存入Cache
			$arr_hook_id = [];
			$sql         = 'SELECT `id_hook`, `name`
				FROM `' . DB_PREFIX_ . 'hook`';
			$arr_row     = Db::rowSQL($sql);
			foreach ($arr_row as $i => $row) {
				$arr_hook_id[strtolower($row['name'])] = $row['id_hook'];
			}
			Cache::set($cache_id, $arr_hook_id);
		} else {
			$arr_hook_id = Cache::get($cache_id);
		}
		return (isset($arr_hook_id[$hook_name]) ? $arr_hook_id[$hook_name] : false);
	}

	public static function getId($hook_name)
	{
		return Hook::getIdByName($hook_name);
	}

	//以Hook ID取得Hook名稱
	public static function getNameById($hook_id)
	{
		$cache_id = 'HookNameById_' . $hook_id;
		if (!Cache::is_set($cache_id)) {
			$row = Db::rowSQL('SELECT `name`
				FROM `' . DB_PREFIX_ . 'hook`
				WHERE `id_hook` = ' . (int)$hook_id .
				'LIMIT 0, 1');
			Cache::set($cache_id, $row['name']);
			return $row['name'];
		};
		return Cache::retrieve($cache_id);
	}

	public static function getName($hook_id)
	{
		return Hook::getNameById($hook_id);
	}

	//以Hook ID取得Hook Live Edit
	public static function getLiveEditById($hook_id)
	{
		if (!Cache::is_set('live_edit_' . $hook_id)) {
			$row = Db::rowSQL('SELECT `live_edit`
				FROM `' . DB_PREFIX_ . 'hook`
				WHERE `id_hook` = ' . (int)$hook_id .
				'LIMIT 0, 1');
			Cache::set('live_edit_' . $hook_id, $row['name']);
			return $row['name'];
		};
		return Cache::retrieve('live_edit_' . $hook_id);
	}

	//取得所有Modul列表
	public static function getHookModuleList()
	{
		$cache_id = 'HookModuleList';
		if (!Cache::is_set($cache_id)) {
			$rows = Db::rowSQL('
				SELECT h.`id_hook`, h.`name` as hook_name, `title` AS hook_title, `description`, `position`, m.`id_module`, m.`name` AS module_name, `live_edit`, m.`active`
				FROM `' . DB_PREFIX_ . 'hook_module` hm
				STRAIGHT_JOIN `' . DB_PREFIX_ . 'hook` h ON h.`id_hook` = hm.`id_hook`
				STRAIGHT_JOIN `' . DB_PREFIX_ . 'module` as m ON m.`id_module` = hm.`id_module`
				ORDER BY `position` ASC');
			$list = [];
			foreach ($rows as $row) {
				if (!isset($list[$row['id_hook']]))
					$list[$row['id_hook']] = [];
				$list[$row['id_hook']][$row['id_module']] = [
					'id_hook'     => $row['id_hook'],
					'hook_name'   => $row['hook_name'],
					'hook_title'  => $row['hook_title'],
					'description' => $row['description'],
					'position'    => $row['position'],
					'live_edit'   => $row['live_edit'],
					'id_module'   => $row['id_module'],
					'module_name' => $row['module_name'],
					'active'      => $row['active'],
				];
			};
			Cache::set($cache_id, $list);
			return $list;
		};
		return Cache::retrieve($cache_id);
	}

	//取得該Hook Modul列表
	public static function getModulesFromHook($id_hook, $id_module = null)
	{
		$module_lists = Hook::getHookModuleList();                                        //取得所有Modul列表
		$module_list  = (isset($module_lists[$id_hook])) ? $module_lists[$id_hook] : [];            //取得該Hook Modul列表
		if ($id_module)
			return (isset($module_list[$id_module])) ? [$module_list[$id_module]] : [];    //取得該Modul列表
		return $module_list;
	}

	public static function exec($hook_name, $hook_args=null, $id_module = null, $array_return = false)
	{

		//檢查所有有使用到的模組
		if (!$module_list = Hook::getModuleListByHookName($hook_name, true)) {
			return '';
		}
		//檢查id_hook
		if (!$id_hook = Hook::getIdByName($hook_name)) {
			return false;
		}
		Hook::$executed_hooks[$id_hook] = $hook_name;
		//線上編輯
		$live_edit = false;

		$context = Context::getContext();
		if (!isset($hook_args['cookie']) || !$hook_args['cookie']) {
			$hook_args['cookie'] = $context->cookie;
		}

		$display = '';
		foreach ($module_list as $array) {
			//檢查錯誤
			if ($id_module && $id_module != $array['id_module']) {
				continue;
			}

			//檢查模組有沒有安裝
			if (!($moduleInstance = Module::getInstanceByName($array['module']))) {
				continue;
			}

			//檢查此模組有沒有裝到此hook
			$hook_callable = is_callable([$moduleInstance, 'hook' . $hook_name]);

			// 調用Hook方法
			if ($hook_callable) {
				$display .= Hook::CallHook($moduleInstance, 'hook' . $hook_name, $hook_args);
			}
		}
		return $display;
	}

	public static function CallHook($module, $method, $params)
	{
		//紀錄模組效能
		if (Module::$_log_modules_perfs === null) {
			$Lilyhouse                  = new LilyHouse();
			$modulo                     = $Lilyhouse->debug ? 1 : Configuration::get('log_modules_performances');
			Module::$_log_modules_perfs = ($modulo && mt_rand(0, $modulo - 1) == 0);
			if (Module::$_log_modules_perfs) {
				Module::$_log_modules_perfs_session = mt_rand();
			}
		}
		// Immediately return the result if we do not log performances
		if (!Module::$_log_modules_perfs) {
			return $module->{$method}($params);
			exit;
		}
		// Store time and memory before and after hook call and save the result in the database
		$time_start   = microtime(true);
		$memory_start = memory_get_usage(true);

		// Call hook
		$r = $module->{$method}($params);
		if (Module::$_log_modules_perfs) {
			$time_end   = microtime(true);
			$memory_end = memory_get_usage(true);

			//存入資料庫
			$sql = '';
		}

		return $r;
	}


	public static function getModuleListByHookName($hook_name = null, $active = null)
	{
		$list = [];
//		//建構中
//		$list['displayBodyAfter'] = array(
//			array(
//				'id_hook' => 1,
//				'id_module' => 1,
//				'module' => 'HelpBook',
//				'live_edit' => false,
//			)
//		);


		$context  = Context::getContext();
		$cache_id = 'HookModuleList_' . ((isset($_SESSION['id_group'])) ? $hook_name . '_' . $_SESSION['id_group'] : $hook_name);
		//取得該角色可使用的模組列表
		if (!Cache::is_set($cache_id)) {
			// SQL Request

			if ($_SESSION['id_group'] > 0) {
				$WHERE .= ' OR mg.`id_group` = ' . $_SESSION['id_group'];
			}

			$WHERE .= ')';

			if ($active !== null) {
				$WHERE .= ' AND m.`active` = ' . $active;
			}

			$WHERE .= sprintf(' AND h.`name` = %s', GetSQL($hook_name, 'text'));

			$sql  = 'SELECT h.`id_hook`, h.`name` as hook_name, m.`id_module`, m.`active`, m.`name` AS module_name, h.`live_edit`
				FROM `' . DB_PREFIX_ . 'hook` AS h
				INNER JOIN `' . DB_PREFIX_ . 'hook_module` AS hm ON hm.`id_hook` = h.`id_hook`
				INNER JOIN `' . DB_PREFIX_ . 'module` AS m ON m.`id_module` = hm.`id_module`
				LEFT JOIN `' . DB_PREFIX_ . 'module_group` AS mg ON mg.`id_module` = m.`id_module`
				WHERE (mg.`id_group` IS NULL ' . $WHERE . '
				GROUP BY hm.id_module
				ORDER BY hm.`position` ASC';
			$list = [];
			if ($rows = Db::rowSQL($sql)) {
				foreach ($rows as $row) {
					$row['hook_name'] = strtolower($row['hook_name']);
					if (!isset($list[$row['hook_name']]))
						$list[$row['hook_name']] = [];
					$list[$row['hook_name']][] = [
						'id_hook'   => $row['id_hook'],
						'id_module' => $row['id_module'],
						'module'    => $row['module_name'],
						'live_edit' => $row['live_edit'],
						'active'    => $row['active'],
					];
				}
			}
			Cache::set($cache_id, $list);
		} else {
			$list = Cache::retrieve($cache_id);
		}

		if ($hook_name) {
			$hook_name        = strtolower($hook_name);
			$return           = [];
			$inserted_modules = [];
			if (isset($list[$hook_name])) {
				$return = $list[$hook_name];
			}
			foreach ($return as $module) {
				$inserted_modules[] = $module['id_module'];
			}
			if (isset($list[$hook_name])) {
				foreach ($list[$hook_name] as $module) {
					if (!in_array($module['id_module'], $inserted_modules)) {
						$return[] = $module;
					}
				}
			}
			return (count($return) > 0 ? $return : false);        //回傳指定Hook Module ID
		} else {
			return $list;                            //回傳所有Hook Module ID
		}
	}

	public static function wrapLiveEdit($display, $moduleInstance, $id_hook)
	{
		return '<script type="text/javascript"> modules_list.push(\'' . Tools::safeOutput($moduleInstance->name) . '\');</script>
				<div id="hook_' . (int)$id_hook . '_module_' . (int)$moduleInstance->id . '_moduleName_' . str_replace('_', '-', Tools::safeOutput($moduleInstance->name)) . '"
				class="dndModule" style="border: 1px dotted red;' . (!strlen($display) ? 'height:50px;' : '') . '">
					<span style="font-family: Georgia;font-size:13px;font-style:italic;">
						<img style="padding-right:5px;" src="' . _MODULE_DIR_ . Tools::safeOutput($moduleInstance->name) . '/logo.gif">'
			. Tools::safeOutput($moduleInstance->displayName) . '<span style="float:right">
				<a href="#" id="' . (int)$id_hook . '_' . (int)$moduleInstance->id . '" class="moveModule">
					<img src="' . _PS_ADMIN_IMG_ . 'arrow_out.png"></a>
				<a href="#" id="' . (int)$id_hook . '_' . (int)$moduleInstance->id . '" class="unregisterHook">
					<img src="' . _PS_ADMIN_IMG_ . 'delete.gif"></a></span>
				</span>' . $display . '</div>';
	}
}
