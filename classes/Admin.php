<?php
class Admin
{
	public $_errors = array();
	public function __construct(){
		
	}

	public static function get($id_admin){
		if(!empty($id_admin)){
			$sql = sprintf('SELECT * 
				FROM '.DB_PREFIX_.'`admin`
				WHERE `id_admin` = %d
				LIMIT 0, 1',
				GetSQL($id_admin, 'int')
			);

			$row = Db::rowSQL($sql, true);
			return $row;
		}
		return false;
	}

	public function competence(){
		$competences = array(
			array('admin'	=>'管理者',		'view'=>'觀看','add'=>'新增','update'=>'修改','del'=>'刪除'),
			array('myadmin'	=>'個人資料',		'view'=>'觀看','add'=>'新增','update'=>'修改','del'=>'刪除')
		);
		return $competences;
	}
	
	public static function is_group_loging($id_group=NULL){
		if(empty($_SESSION['first_name'])) gotourl('/'.MANAGE_URL.'/?pre='.base64_encode(str_replace('?logout', '', $_SERVER['REQUEST_URI'])));	//沒有登入 跳到登入頁面
		if(!empty($id_group)){														//沒有權限 跳到自己有權限的頁面或沒有權限頁面
		}
	}
	
	//取得管理者資料
	public function get_db_admin($nMax = 20,$id_admin = NULL){
		$sql = 'SELECT * FROM `admin`';
		if(!empty($id_admin)){
			$sql = 'SELECT * FROM `admin` WHERE `id_admin` = '.$id_admin.' LIMIT 0, 1';
			$nMax = NULL;
		}
		$db_admin = new db_mysql($sql,$nMax);
		$arr_admin = array();
		$num = $db_admin->num();
		if($num > 0){
			while($admin = $db_admin->next_row()){
				$arr_admin[] = $admin;
			}
		}
		return array($num,$arr_admin);
	}
	
	//新增管理者資料
	public function add_db_admin($last_name,$first_name,$email,$password,$id_group,$active){
		$sql = sprintf('INSERT INTO `admin` (`last_name`, `first_name`, `email`, `password`, `id_group`, `active`) VALUES (%s, %s, %s, %s, %d, %d)',
					GetSQL($last_name,'text'),
					GetSQL($first_name,'text'),
					GetSQL($email,'text'),
					GetSQL($password,'text'),
					GetSQL($id_group,'int'),
					GetSQL($active,'int'));
		$INSERT_admin = new db_mysql($sql);
		return ($INSERT_admin->num()) ? true : false;
	}
	
	//修改管理者資料
	public function update_db_admin($id_admin,$last_name,$first_name,$password,$id_group,$active){
		if(!empty($password)) $UPDATE = ', `password` = '.GetSQL($password,'text');
		$sql = sprintf('UPDATE `admin` SET `last_name` = %s, `first_name` = %s'.$UPDATE.', `id_group` = %d, `active` = %d
					WHERE `id_admin` = %d',
					GetSQL($last_name,'text'),
					GetSQL($first_name,'text'),
					GetSQL($id_group,'int'),
					GetSQL($active,'int'),
					GetSQL($id_admin,'int'));
		$db_admin = new db_mysql($sql);
		return ($db_admin->num()) ? true : false;
	}
	
	//刪除管理者資料
	public function del_db_admin($id_admin){
		$sql = sprintf('DELETE FROM `admin` WHERE `id_admin` = %d',
			GetSQL($id_admin,'int'));
		$db_admin = new db_mysql($sql);
		return ($db_admin->num()) ? true : false;
	}
	
	//是否登入
	public static function logout($logout=false){
		if(isset($_GET['logout']) || $logout){
			$_SESSION['id_admin'] = NULL;
			$_SESSION['first_name'] = NULL;
			$_SESSION['email'] = NULL;
			$_SESSION['id_group'] = NULL;
			$_SESSION['id_web'] = NULL;
			$_SESSION['name'] = NULL;
            $_SESSION['ag'] = NULL;
			setcookie('adm_login', '0', time() -1000, '/');
			setcookie('adm_cookie', '0', time() -1000, '/');
		}
	}

	//是否登入
	public static function login($email, $pass, $auto_loging=false){
		$sql = 'SELECT *
			FROM `admin` AS a
			LEFT JOIN `web` AS w ON w.`id_web` = a.`id_web`
			LEFT JOIN `group` AS g ON g.`id_group` = a.`id_group`
			WHERE a.`email` = "'.$email.'"
			AND a.`password` = "'.$pass.'"
			AND a.`active` = 1
			AND ((w.`id_web` IS NULL OR w.`id_web` = 0) OR (w.`id_web` IS NOT NULL AND w.`id_web` > 0 AND w.`active` = 1))
			LIMIT 0, 1';
		$admin = Db::rowSQL($sql, true);

		if(count($admin) > 0){
			$_SESSION['id_admin'] = $admin['id_admin'];
			$_SESSION['first_name'] = $admin['first_name'];
			$_SESSION['email'] = $admin['email'];
			$_SESSION['admin'] = $admin['admin'];
			$_SESSION['id_group'] = $admin['id_group'];
			$_SESSION['id_web'] = $admin['id_web'];
			$_SESSION['name'] = $admin['last_name'].$admin['first_name'];
            $_SESSION['ag'] = $admin['ag'];
			Log::set('LOGIN');
			$AdminAutoLoginCookieDate = Configuration::get('AdminAutoLoginCookieDate');
			if($auto_loging && $AdminAutoLoginCookieDate > 0){		//記住我
				//todo
				$cookie_hash = sha1($admin['id_admin'].$email.$_SERVER['HTTP_USER_AGENT'].date().$pass);
				$sql = 'REPLACE INTO `admin_auto_login` (`id_admin`, `effective_time`, `cookie`) VALUES ('.$admin['id_admin'].', NOW() + INTERVAL '.$AdminAutoLoginCookieDate.' DAY, "'.$cookie_hash.'");';
				$DB = new db_mysql($sql);
				$cookie_time = (3600 * 24 * (int)$AdminAutoLoginCookieDate);
				setcookie('adm_login', '1', time() + $cookie_time, '/');
				setcookie('adm_cookie', $cookie_hash, time() + $cookie_time, '/');
			}
			return true;
		}else{
			return false;
		}
	}

	//是否登入
	public static function flash_login($flash_barcode, $auto_loging=false){
		$sql = 'SELECT *
			FROM `admin` AS s
			LEFT JOIN `web` AS w ON w.`id_web` = a.`id_web`
			WHERE a.`flash_barcode` = "'.$flash_barcode.'"
			AND a.`active` = 1
			AND ((w.`id_web` IS NULL OR w.`id_web` = 0) OR (w.`id_web` IS NOT NULL AND w.`id_web` > 0 AND w.`active` = 1))
			LIMIT 0, 1';
		$admin = Db::rowSQL($sql, true);
		if(count($admin) > 0){
			$_SESSION['id_admin'] = $admin['id_admin'];
			$_SESSION['first_name'] = $admin['first_name'];
			$_SESSION['email'] = $admin['email'];
			$_SESSION['admin'] = $admin['admin'];
			$_SESSION['id_group'] = $admin['id_group'];
			$_SESSION['id_web'] = $admin['id_web'];
			$_SESSION['name'] = $admin['last_name'].$admin['first_name'];
            $_SESSION['name'] = $admin['ag'];
			Log::set('LOGIN');
			$AdminAutoLoginCookieDate = Configuration::get('AdminAutoLoginCookieDate');
			if($auto_loging && $AdminAutoLoginCookieDate > 0){		//記住我
				//todo
				$cookie_hash = sha1($admin['id_admin'].$admin['email'].$_SERVER['HTTP_USER_AGENT'].date().$admin['password']);
				$sql = 'REPLACE INTO `admin_auto_login` (`id_admin`, `effective_time`, `cookie`) VALUES ('.$admin['id_admin'].', NOW() + INTERVAL '.$AdminAutoLoginCookieDate.' DAY, "'.$cookie_hash.'");';
				$DB = new db_mysql($sql);
				$cookie_time = (3600 * 24 * (int)$AdminAutoLoginCookieDate);
				setcookie('adm_login', '1', time() + $cookie_time, '/');
				setcookie('adm_cookie', $cookie_hash, time() + $cookie_time, '/');
			}
			return true;
		}else{
			return false;
		}
	}

	//自動登入
	public static function autologoing(){//todo
		$AdminAutoLoginCookieDate = Configuration::get('AdminAutoLoginCookieDate');
		if(isset($_COOKIE['adm_login']) && $_COOKIE['adm_login'] == 1 && isset($_COOKIE['adm_cookie']) && $AdminAutoLoginCookieDate > 0){
			$sql = 'SELECT a.`id_admin`, a.`first_name`, a.`email`, a.`id_group`, a.`id_admin`
				FROM `admin_auto_login` AS aa,a.`ag` as ag
				LEFT JOIN `admin` AS a ON a.`id_admin` =  aa.`id_admin`
				LEFT JOIN `web` AS w ON w.`id_web` = a.`id_web`
				WHERE aa.`cookie` = "'.$_COOKIE['adm_cookie'].'"
				AND ((w.`id_web` IS NULL OR w.`id_web` = 0) OR (w.`id_web` IS NOT NULL AND w.`id_web` > 0 AND w.`active` = 1))
				LIMIT 0, 1';
			$row = Db::rowSQL($sql, true);
			if($row['id_admin'] > 0){
				if($_SESSION['id_admin'] != $row['id_admin']){
					$_SESSION['id_admin'] = $row['id_admin'];
					Log::set('LOGIN');
				}else{
					$_SESSION['id_admin'] = $row['id_admin'];
				}
				$_SESSION['first_name'] = $row['first_name'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['id_group'] = $row['id_group'];
				$_SESSION['id_web'] = $row['id_web'];
				$_SESSION['name'] = $row['last_name'].$row['first_name'];
                $_SESSION['ag'] = $row['ag'];
				$cookie_time = (3600 * 24 * $AdminAutoLoginCookieDate);
				setcookie('adm_login', '1', time() + $cookie_time, '/');
				setcookie('adm_cookie', $_COOKIE['adm_cookie'], time() + $cookie_time, '/');
			}
		}
	}

	//確認表單
	public static function check_admin_email($email){
		$sql = 'SELECT `id_admin` FROM `admin`
			WHERE `email` = "'.$email.'"
			LIMIT 0, 1';
		$SELECT_admin = new db_mysql($sql);
		if($SELECT_admin->num() > 0){
			return false;
		}
		return true;
	}
	
	public static function ch_del($id_admin){
		$sql = 'SELECT `id_admin` FROM `admin`
			WHERE `id_admin` <> '.$id_admin.'
			AND `id_group` = 1';
		$SELECT_admin = new db_mysql($sql);
		if($SELECT_admin->num() > 0){
			return true ;
		}
		return false ;
	}
	
	//確認表單
	public function check_admin($action){
		switch($_POST['action']){
			case 'edit_management' :
			case 'edit_admin' :
				if(!isint($_POST['id_admin'])) $this->_errors[] = '資料錯誤!找不到管理者資料';
			case 'add_management' :
			case 'add_admin' :
				//if(empty($_POST['nema']))	$this->_errors[] = '請輸入管理者名稱';
				if(empty($_POST['last_name']))$this->_errors[] = '請輸入管理者姓氏';
				if(empty($_POST['first_name']))$this->_errors[] = '請輸入管理者名子';
				//if(!empty($_POST['email']) && !isEmail($_POST['email']))	$this->_errors[] = 'e-mail格式錯誤';
				if($_POST['action'] == 'add_management' || $_POST['action'] == 'add_admin'){
					if(empty($_POST['email']))	$this->_errors[] = '請輸入帳號';
					if(empty($_POST['password']))	$this->_errors[] = '請輸入密碼';
				}
				if(!empty($_POST['password']) && $_POST['password2'] != $_POST['password']) $this->_errors[] = '確認密碼錯誤!';
				
				//if(empty($_POST['id_group']))	$this->_errors[] = '請選擇管理者群組';
				if(!isTrue($_POST['active']))	$this->_errors[] = '啟用狀態錯誤';
			break;
			case 'del_management' :
			case 'del_admin' :
				if(!isint($_POST['id_admin'])) $this->_errors[] = '資料錯誤!找不到管理者資料';
				if(!ch_del($_POST['id_admin'])) $this->_errors[] = '無法刪除!(最高系統管理者最少要有一位!)';
			break;
		}
		return $this->_errors;
	}

	public static function flash_barcode_login($flash_barcode){

	}
}