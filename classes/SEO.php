<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/10/16
 * Time: 下午 04:02
 */

class SEO
{
	protected static $instance = null;
	public $url = '';
	public $data = null;
	public $page = null;

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new SEO();
		}
		return self::$instance;
	}

	public static function get($id_web = null, $page = null, $id_language = null)
	{
		if(empty($id_web)){
			$id_web = Web::getThisWebId();
		}
		$WHERE = sprintf(' WHERE `id_web` = %d', GetSQL($id_web, 'int'));
		if (empty(SEO::getContext()->data[$id_web])) {
			$arr = [];
			if($page !== null){
				$WHERE .= sprintf(' AND `page` = %s', GetSQL($page, 'text'));
			}
			$sql = 'SELECT `page`, `url`, `title`, `keywords`, `description`, `index_ation`
			FROM `' . DB_PREFIX_ . 'seo`'.$WHERE;

			$arr_row = Db::rowSQL($sql);
			foreach ($arr_row as $i => $row) {
				$arr[$row['page']] = [
					'title'       => $row['title'],
					'description' => $row['description'],
					'keywords'    => $row['keywords'],
					'nobots'      => !$row['index_ation'],
					'nofollow'    => !$row['index_ation'],
				];
			}
			SEO::getContext()->data[$id_web] = $arr;
		}
		return SEO::getContext()->data[$id_web];
	}

	public static function getPageOnURL($id_web = null, $url = null, $id_language = null)
	{
		if(empty($id_web)){
			$id_web = Web::getThisWebId();
		}
		$WHERE = sprintf(' WHERE `id_web` = %d', GetSQL($id_web, 'int'));
		if (empty(SEO::getContext()->page[$url])) {
			$arr = [];
			if($url !== null){
				$WHERE .= sprintf(' AND `url` = %s', GetSQL($url, 'text'));
			}
			$sql = 'SELECT `page`
			FROM `' . DB_PREFIX_ . 'seo`'.$WHERE.'
			LIMIT 0, 1';
			
			$row = Db::rowSQL($sql, true);
			SEO::getContext()->page[$url] = $row['page'];
		}
		return SEO::getContext()->page[$url];
	}
}