<?php
class Configuration
{
	/*
	public $id;
	public $name;
	public $value;
	public $date_add;
	public $date_upd;
	*/

	public static function get($name, $json=false, $id_lang = null, $db_prefix_=DB_PREFIX_){		
		$sql = sprintf('SELECT `value` FROM `'.$db_prefix_.'configuration`
			WHERE `name` = %s
			LIMIT 0, 1',
			GetSQL($name, 'text'));
		$row = Db::rowSQL($sql, true);
		$values = $row['value'];
		if($json){
			$json = new Services_JSON();
			$values = (array)$json->decode($row['value']);
		}
		return $values;
	}

	public static function getIdByName($name, $id_lang = null, $db_prefix_=DB_PREFIX_){
		$sql = sprintf('SELECT `id_configuration` FROM `'.$db_prefix_.'configuration`
			WHERE `name` = %s
			LIMIT 0, 1',
			GetSQL($name, 'text'));
		Db::rowSQL($sql);
		return Db::getContext()->num();
	}

	public static function set($name, $values, $id_lang = null, $db_prefix_=DB_PREFIX_){
		if(is_array($values)){
			$json = new Services_JSON();
			$values = $json->encode($values);
		};
		$now = date('Y-m-d H:i:s');
		$sql = sprintf('INSERT INTO `'.$db_prefix_.'configuration` (`name`, `value`) VALUES (%s, %s) ON DUPLICATE KEY UPDATE `name` = %s, `value` = %s, `date_upd` = %s',
				GetSQL($name, 'text'),
				GetSQL($values, 'text'),
				GetSQL($name, 'text'),
				GetSQL($values, 'text'),
				GetSQL($now, 'text'));
		Db::rowSQL($sql);
		return Db::getContext()->num();
	}

	public static function del($name, $id_lang = null, $db_prefix_=DB_PREFIX_){
		$sql = sprintf('DELETE FROM `'.$db_prefix_.'configuration`
					WHERE `name` = %s
					LIMIT 1',
				GetSQL($name,'text'));
		Db::rowSQL($sql);
		return Db::getContext()->num();
	}
}
