<?php
/**
 * Created by PhpStorm.
 * User: 陳哲儒
 * Date: 2020/04/24
 * Time: 上午 10:30
 */
class SMS
{
    protected static $instance = null;        //是否實體化

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new SMS();
        }
        return self::$instance;
    }

    public static function sms_status_text($str,$vendor='mitake'){
        if($vendor=='mitake'){
            switch($str){
                case '*':return '系統發生錯誤，請聯絡三竹資訊窗口人員';break;
                case 'a':return '簡訊發送功能暫時停止服務，請稍候再試';break;
                case 'b':return '簡訊發送功能暫時停止服務，請稍候再試';break;
                case 'c':return '請輸入帳號';break;
                case 'd':return '請輸入密碼';break;
                case 'e':return '帳號、密碼錯誤';break;
                case 'f':return '帳號已過期';break;
                case 'h':return '帳號已被停用';break;
                case 'k':return '無效的連線位址';break;
                case 'm':return '必須變更密碼，在變更密碼前，無法使用簡訊發送服務';break;
                case 'n':return '密碼已逾期，在變更密碼前，將無法使用簡訊發送服務';break;
                case 'p':return '沒有權限使用外部Http程式';break;
                case 'r':return '系統暫停服務，請稍後再試';break;
                case 's':return '帳務處理失敗，無法發送簡訊';break;
                case 't':return '簡訊已過期';break;
                case 'u':return '簡訊內容不得為空白';break;
                case 'v':return '無效的手機號碼';break;
                case 'x':return '發送檔案過⼤，無法發送簡訊';break;
                case 'y':return '參數錯誤';break;
                case 'z':return '查無資料';break;
                case '0':return '預約傳送中';break;
                case '1':return '已送達業者';break;
                case '2':return '已送達業者';break;
//                case '3':return '已送達業者';break;
                case '4':return '已送達手機';break;
                case '5':return '內容有錯誤';break;
                case '6':return '門號有錯誤';break;
                case '7':return '簡訊已停用';break;
                case '8':return '逾時無送達';break;
                case '9':return '預約已取消';break;
                default:return '無法辨識的錯誤';break;
            }
        }else{
            return "您沒有輸入SMS公司或是沒使用";
        }
    }

    public  function send_sms($mobile,$text,$user_id='',$sms_id='',$vendor='mitake',$sql_log='on'){//$sql_log如果不輸入或是輸入on則會自動記錄
        if($vendor=='mitake'){
            //$mobile受訊方手機號碼。請填入09帶頭的手機號碼。

            //三竹帳密
            $username = '28888788';
            $password = 'life318568';

            $dlvtime='';//簡訊預約時間。也就是希望簡訊何時送達手機，格式為YYYY-MM-DD HH:NN:SS或YYYYMMDDHHNNSS，或是整數值代表幾秒後傳送。空白則為即時簡訊
            $vldtime='';//簡訊有效期限。格式為YYYY-MM-DD HH:NN:SS或YYYYMMDDHHNNSS，或是整數值代表傳送後幾秒後內有效。請勿超過大哥大業者預設之24小時期限
            $smbody=urlencode($text);//長度70個中文字或是160個英數字。
            $smbody=str_replace('+','%20',$smbody);
//            $response="https://www.lifegroup.house/manage/sms.php";//狀態回報網址
            $response ="";
            $clientID=$sms_id;//客戶簡訊ID。用於避免重複發送，若有提供此參數，則會判斷該簡訊ID是否曾經發送，若曾發送過，則直接回覆之前發送的回覆值，並加上Duplicate=Y。必須維持唯一性
            $mobile=str_replace('+','%20',$mobile);
            $apiUrl="https://smsapi.mitake.com.tw/api/mtk/SmSend?username={$username}&password={$password}&CharsetURL=utf-8&dstaddr={$mobile}&DestName={$user_id}&dlvtime={$dlvtime}&vldtime={$vldtime}&smbody={$smbody}&response={$response}&clientID={$clientID}";//換長簡訊

//            echo $apiUrl;
            $sms_curl = new SMS;

            $result=$sms_curl->cURL($apiUrl);
            $result = "clientid=".$result;
//            echo "<br>";
//            echo $result;
//            $result = str_replace(" ","&",$result);
            $result=str_replace(array("[1]",chr(13).chr(10)),array("1","&"),$result);
            $result=mb_convert_encoding($result,"utf-8","big5");
            parse_str($result,$ret);

            if($sql_log =='on'){
            $sql = "INSERT INTO sms_log (`session_id`, `tel`, `captcha`, `ip`, `msgid`, `statuscode`,
                            `return_status`, `accountPoint`)
                            VALUES (".GetSQL(session_id(), 'text').",".GetSQL($mobile, 'text').",
                            ".GetSQL($text, 'text').",".GetSQL($_SERVER['REMOTE_ADDR'], 'text').",".GetSQL($ret['msgid'],
                    'text').",".GetSQL($ret['statuscode'], 'text').",
                            ".GetSQL(SMS::sms_status_text($ret['statuscode'],"mitake"), 'text').",".GetSQL($ret['AccountPoint'], 'int').")";
            Db::rowSQL($sql);//存入資料庫
}
            return $ret;
            //簡訊序號。發送失敗，則不會有此欄位。
            //發送狀態。請參考附錄二的說明。
            //剩餘點數。本次發送後的剩餘額度。
            //是否為重複發送的簡訊。Y代表重複發送。
        }
    }

    public  function cURL($url){
        $curl = curl_init();
// 設定curl網址
        curl_setopt($curl, CURLOPT_URL, $url);
// 設定Header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER ,true);
        curl_setopt($curl, CURLOPT_ENCODING ,"");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST ,"GET");
// 執行
        $output = curl_exec($curl);
        curl_close($curl);
       return $output;
    }
}