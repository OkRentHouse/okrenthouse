<?php
class FileUpload{
	/*
	 * type: "video",
            size: 375000,
	office
	gdocs
	pdf
	text
	text
	 *  'jpg': function(ext) {
		return ext.match(/(jpg|jpeg)$/i);
	  },
	  'doc': function(ext) {
		return ext.match(/(doc|docx)$/i);
	  },
	  'xls': function(ext) {
		return ext.match(/(xls|xlsx)$/i);
	  },
	  'ppt': function(ext) {
		return ext.match(/(ppt|pptx)$/i);
	  },
	  'zip': function(ext) {
		return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
	  },
	  'htm': function(ext) {
		return ext.match(/(htm|html)$/i);
	  },
	  'txt': function(ext) {
		return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
	  },
	  'mov': function(ext) {
		return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
	  },
	  'mp3': function(ext) {
		return ext.match(/(mp3|wav)$/i);
	  }
	 */
	public static function set($arr, $dir, $url, $id_admin=NULL, $id_member=NULL){
		$Y = date('Y');
		$m = date('m');
		$arr_id = array();
		foreach($arr as $i => $v){
		    if(empty($v['position'])){
                $v['position'] = '0';
            }
			$INSERT = '("'.$v['name'].'", "'.$v['type'].'", "'.$v['size'].'", "'.$v['ext'].'", "'.urlencode($dir).'", "'.urlencode($url).'", "'.$v['position'].'", "'.$Y.'", "'.$m.'", "'.$id_admin.'", "'.$id_member.'")';
			$sql = 'INSERT INTO `file` (`filename`, `type`, `size`, `ext`, `file_dir`, `file_url`, `position`, `year`, `month`, `id_admin`, `id_member`) VALUES '.$INSERT;
			Db::rowSQL($sql);
//			$sql = "SELECT MAX(id_file) as max_num FROM `file`";
//            $row = Db::rowSQL($sql,true);
//            $arr_id[] = $row['max_num'];
            $arr_id[] = Db::getContext()->num();
		};
		return $arr_id;
	}
	
	public static function get_by_url($url){
		if(empty($url)) return false;
		$arr_url = explode('/', $url);			//網址切割成陣列
		$file_name = array_pop( $arr_url);
		$url = urlencode(implode('/', $arr_url)).'%2F';
		
		$sql = sprintf('SELECT *
			FROM `file` AS f
			LEFT JOIN `file_in_type` AS fit ON fit.`id_file` = f.`id_file`
			LEFT JOIN `file_type` AS ft ON ft.`id_file_type` = fit.`id_file_type`
			WHERE f.`file_url` = %s
			AND f.`filename` = %s
			LIMIT 0, 1',
			GetSQL($url, 'text'),
			GetSQL($file_name, 'text'));
		
		return Db::rowSQL($sql, true);
	}
	
	public static function get($arr_id_file){
		if(!is_array($arr_id_file)) $arr_id_file = array($arr_id_file);
		$arr_id_file = array_filter($arr_id_file);
		if(count($arr_id_file)){
			$sql = 'SELECT f.`id_file`, f.`id_file`, f.`filename`, f.`type`, f.`size`, f.`ext`, f.`file_dir`, f.`file_url`, f.`year`, f.`month`, f.`position`, f.`id_admin`, f.`id_member`, ft.`id_file_type`, ft.`file_type`
			FROM `file` AS f
			LEFT JOIN `file_in_type` AS fit ON fit.`id_file` = f.`id_file`
			LEFT JOIN `file_type` AS ft ON ft.`id_file_type` = fit.`id_file_type`
			WHERE f.`id_file` IN ('.implode(', ', $arr_id_file).')
			ORDER BY f.`position` ASC, f.`id_file` DESC';
			return Db::rowSQL($sql);
		}else{
			return array();
		}
	}
	
	public static function check_name($new_file_name, $file_dir){		//檔名, 副檔名, 檔案路徑
		$main_name = substr($new_file_name, 0, strrpos($new_file_name, '.'));//取得檔案名稱
		$ext = substr($new_file_name, strrpos($new_file_name,'.')+1, strlen($new_file_name)-strlen($main_name)+1);//取得副檔名名稱
		$count = 0;
		while(true)
		{
			$count++;	
			$sql= 'SELECT `id_file`
				FROM `file`
				WHERE `filename` = "'.$new_file_name.'"
				AND `ext` = "'.$ext.'"
				AND `file_dir` = "'.urlencode($file_dir).'"
				LIMIT 0, 1';
			if(count(Db::rowSQL($sql)) > 0)//如果重複的話
			{
				$tmp_main_name = $main_name.' ('.$count.')';	//在檔案名稱後面加上序號ex:aa.jpg(1)
				$new_file_name = $tmp_main_name.'.'.$ext;			//把檔案名稱跟副檔名重新組合
			}else{
				break;
			};
		};
		return $new_file_name;
	}
}