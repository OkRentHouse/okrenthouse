<?php
    class LifeAssets{
        //1/15 要處理5大外連表 $other_multiple_table 這五張 還有一堆ajax 跟+號....
        protected static $instance = null;        //是否實體化

        public static function getContext()
        {
            if (!self::$instance) {
                self::$instance = new LifeAssets();
            }
            return self::$instance;
        }

        public static $multiple_data = ['training_class','training_skill','id_skill'];//這兩個是用陣列表達 id_skill也是
        public static $admin_data = ['id_admin','phone','first_name','last_name','en_name','switchboard','attendance_number','share_number','admin_number','gender',
                                     'birthday','identity','line_id','private_email','health','disease','religion','id_address_domicile','same_address','id_address_contact',
                                     'domicile_tel','contact_tel','id_skill','interest','training_class','training_skill','id_admin_source','id_department',
                                     'job','on_duty_date','resign_date','resign_reason','stay_date','stay_reason','reinstatement_date',
                                     'career_work','career_future','career_expect','career_partner'];//表示admin所要拿取的資料(其他的資料做其他的處置)
        public static $address_data = ['id_address_domicile','id_address_contact'];//二大地址

        public static $other_multiple_table =['id_admin_family','id_education','id_language','id_experience','id_license','id_people_group'];
        public static $table= ['id_admin_family'=>'admin_family','id_education'=>'education', 'id_language'=>'language',
                               'id_experience'=>'experience','id_license'=>'license','id_people_group'=>'people_group'];
        public static $personnel_insurance =['labor','health','accident','personnel_ensure'];

        public static $address =['id_address','postal','id_county','id_city','village','neighbor','road','segment','lane',
            'alley','no','no_her','floor','floor_her','address_room','address'];

        public static function Admin_multiple_table($id_admin,$data,$personnel_special){//id 與$_post 這邊沒有新增
            //先建立or跟新 admin主表在延伸去處理關連表



            $other_multiple_data = [];
            $personnel_insurance = [];
            foreach(LifeAssets::$address_data as $a_d_k=>$a_d_v){//推address 之後可能會將其做為通用模組因此拉出去
                $address =[];
                foreach($data as $d_k=>$d_v){//推post進來取得要取得的資料
                    if(strpos($d_k,$a_d_v) !==false){//配合到的
                        if($a_d_v==$d_k){
                            $address["id_address"] = $d_v;
                        }else{
                            $address[str_replace($a_d_v.'_','',$d_k)] = $d_v;
                        }
                    }
                }
                if($a_d_v=='id_address_contact' && $data['same_address']==1){//有點選擇同上
                    $data[$a_d_v] = $data['id_address_domicile'];
                }else{
                    $data[$a_d_v] = LifeAssets::address_work($address);//幾乎只要丟過去就可以了..
                }
            }

           foreach($data as $d_k=>$d_v){
               foreach(LifeAssets::$other_multiple_table as $k_o_m_t_k =>$k_o_m_t_v){
                   if(strpos($d_k,$k_o_m_t_v) !==false){//配合到的
                       if($k_o_m_t_v==$d_k){
                           $other_multiple_data[LifeAssets::$table[$k_o_m_t_v]][$k_o_m_t_v] = $d_v;
                       }else{
                           $other_multiple_data[LifeAssets::$table[$k_o_m_t_v]][str_replace($k_o_m_t_v.'_','',$d_k)] = $d_v;
                       }
                   }
               }
           }

            //這邊是做保險那塊因為是獨立出來因此要額外處理
            foreach($data as $d_k=>$d_v){
                if(strpos($d_k,'id_personnel_insurance') !==false){//配合到的
                    if($d_k=='id_personnel_insurance'){
                        $personnel_insurance['personnel_insurance']['id_personnel_insurance'] = $d_v;
                    }else{
                        $personnel_insurance['personnel_insurance'][str_replace('id_personnel_insurance_','',$d_k)] = $d_v;
                    }
                }
            }

            LifeAssets::input_personnel_insurance($personnel_insurance,$id_admin,'admin');//外連表處理
            //測試保險

            LifeAssets::other_multiple_data($other_multiple_data,$id_admin,'admin');//外連表處理



            $update_data = '';
//            print_r($data);
            foreach(LifeAssets::$admin_data as $a_k => $a_v){
               if(!in_array($a_v,LifeAssets::$multiple_data)){
                   $update_data .= $a_v.'='.GetSQL($data[$a_v],"text").',';
               }else{
                   //這邊先去除空值 在轉成 json_encode
                   $update_data .= $a_v.'='.GetSQL(json_encode(array_filter($data[$a_v]), JSON_UNESCAPED_UNICODE),"text").',';
               }
            }
            $update_data = substr($update_data,0,-1);
            $sql = "UPDATE admin SET {$update_data} WHERE id_admin=".GetSQL(Tools::getValue("id_admin"),"int");
            //echo "<br>LifeAssets sql :".$sql."<br>";            
            //已經可以存入admin了
            Db::rowSQL($sql);
        }

        public static function input_personnel_insurance($data,$id_admin,$role){
            //其實這支跟下面的一模一樣只是因為他不一定被呼叫
            //且有些不同因此這樣處理
            foreach($data as $d_k=>$d_v){
                $table_id = 'id_personnel_insurance';//table id
                foreach($d_v[$table_id] as $k=>$v){//id頭的
                    $table_arr=[];
                    foreach($data[$d_k] as $x_k=>$x_v){
                        $table_arr[$x_k] =$x_v[$k];
                    }
                    //這邊做對應未填入彈出迴圈
                    if(empty($table_arr['id_company'])) continue;
                    /////上面處理完畢address的了 下面開始處理之後的流程 insert or update
                    $sql = "";
                    if(!empty($table_arr[$table_id])){//update or insert
                        $update_data ='';//重置
                        foreach($table_arr as $t_a_k=>$t_a_v){
                            if($t_a_k==$table_id) continue;
                            $update_data .= '`'.$t_a_k.'`='.GetSQL($t_a_v,"text").',';
                        }
                        $update_data .='id='.GetSQL($id_admin,"int").',role='.GetSQL($role,"text");
                        $sql = "UPDATE {$d_k} set {$update_data} WHERE {$table_id}=".GetSQL($table_arr[$table_id],"int");
                             Db::rowSQL($sql);
                    }else{
                        $insert_key ='';
                        $insert_value ='';
                        foreach($table_arr as $t_a_k=>$t_a_v){
                            if($t_a_k=='id_personnel_insurance') continue;//搜索是否有key
                            $insert_key .='`'.$t_a_k.'`,';
                            $insert_value .= GetSQL($t_a_v,"text").',';
                        }
                        $insert_key .='id,role';
                        $insert_value .=$id_admin.','.GetSQL($role,"text");
                        $sql = "INSERT INTO {$d_k} ({$insert_key}) VALUES ({$insert_value})";
                          Db::rowSQL($sql);
                    }
                }
            }
        }

        public static function other_multiple_data($data,$id_admin,$role){//寫入上面的資料 //POST的資料 主表的id table
            $table= ['id_admin_family','id_education','id_language','id_experience','id_license','id_people_group'];
            $input_data = [];
            $insert_key = '';
            $insert_value = '';
            $update_data = '';
            $update_where = '';
            foreach($data as $d_k=>$d_v){
                switch($d_k){
                    case 'admin_family':
                        foreach($d_v['id_admin_family'] as $k=>$v){//id頭的
                            $admin_family_arr=[];
                            $address =[];
                            foreach($data[$d_k] as $x_k=>$x_v){
                                $admin_family_arr[$x_k] =$x_v[$k];
                            }
                            //$admin_family_arr continue 條件 以防惡意增加 必填關係 姓名 relation跟name
                            if(empty($admin_family_arr['relation']) || empty($admin_family_arr['name'])) continue;

                            //這邊做完之後將address相關資料建一個新的陣列丟去address_work return回來再放入主輸入表
                            foreach(LifeAssets::$address as $l_a_k =>$l_a_v){
                                $address[$l_a_v] = $admin_family_arr[$l_a_v];//做成一張新的表
                                unset($admin_family_arr[$l_a_v]);
                            }
                            $admin_family_arr['id_address'] = LifeAssets::address_work($address);//將表丟去address的function 做處理
                            /////上面處理完畢address的了 下面開始處理之後的流程 insert or update
                            $sql = "";
                            if(!empty($admin_family_arr['id_admin_family'])){//update or insert
                                $update_data ='';//重置
                                foreach($admin_family_arr as $a_f_a_k=>$a_f_a_v){
                                    if($a_f_a_k=='id_admin_family') continue;
                                    $update_data .= $a_f_a_k.'='.GetSQL($a_f_a_v,"text").',';
                                }
                                $update_data = substr($update_data,0,-1);
                                $sql = "UPDATE {$d_k} set {$update_data} WHERE id_admin_family=".GetSQL($admin_family_arr['id_admin_family'],"int");
                                Db::rowSQL($sql);
                            }else{
                                $insert_key ='';
                                $insert_value ='';
                                foreach($admin_family_arr as $a_f_a_k=>$a_f_a_v){
                                    if(in_array($a_f_a_k,$table)) continue;//搜索是否有key
                                    $insert_key .=$a_f_a_k.',';
                                    $insert_value .= GetSQL($a_f_a_v,"text").',';
                                }
                                $insert_key .='id_admin';
                                $insert_value .=$id_admin;
                                $sql = "INSERT INTO {$d_k} ({$insert_key}) VALUES ({$insert_value})";
                                Db::rowSQL($sql);
                            }
                        }
                        break;
                    case 'education':
                    case 'language':
                    case 'experience':
                    case 'license':
                    case 'people_group':
                        $table_id = 'id_'.$d_k;//table id
                    foreach($d_v[$table_id] as $k=>$v){//id頭的
                        $table_arr=[];
                        foreach($data[$d_k] as $x_k=>$x_v){
                            $table_arr[$x_k] =$x_v[$k];
                        }
                        //4個外連table的 continue 條件 以防惡意增加
                        //這邊做對應未填入彈出迴圈
                        if($d_k=='education' && (is_null($table_arr['education_level']) || empty($table_arr['department']))) continue;
                        if($d_k=='language' && empty($table_arr['name'])) continue;
                        if($d_k=='experience' && empty($table_arr['company'])) continue;
                        if($d_k=='license' && empty($table_arr['name'])) continue;
                        if($d_k=='people_group' && empty($table_arr['id_company'])) continue;//這邊用empty是因為資料表id未有0

                        /////上面處理完畢address的了 下面開始處理之後的流程 insert or update
                        $sql = "";
                        if(!empty($table_arr[$table_id])){//update or insert
                            $update_data ='';//重置
                            foreach($table_arr as $t_a_k=>$t_a_v){
                                if($t_a_k==$table_id) continue;
                                $update_data .= '`'.$t_a_k.'`='.GetSQL($t_a_v,"text").',';
                            }
                            $update_data .='id='.GetSQL($id_admin,"int").',role='.GetSQL($role,"text");
                            $sql = "UPDATE {$d_k} set {$update_data} WHERE {$table_id}=".GetSQL($table_arr[$table_id],"int");
                            Db::rowSQL($sql);
                        }else{
                            $insert_key ='';
                            $insert_value ='';
                            foreach($table_arr as $t_a_k=>$t_a_v){
                                if(in_array($t_a_k,$table)) continue;//搜索是否有key
                                $insert_key .='`'.$t_a_k.'`,';
                                $insert_value .= GetSQL($t_a_v,"text").',';
                            }
                            $insert_key .='id,role';
                            $insert_value .=$id_admin.','.GetSQL($role,"text");
                            $sql = "INSERT INTO {$d_k} ({$insert_key}) VALUES ({$insert_value})";
                            Db::rowSQL($sql);
                        }
                    }
                        break;

                    default:

                        break;
                }
            }

            return $input_data;
        }


        public static function address_work($address){//主id 新成立的data 計數器
            $id_main ='';//負責回傳
            $address_arr = array("county_name"=>"", "city_name"=>"", "village"=>"里", "neighbor"=>"鄰", "road"=>"", "segment"=>"段",
                "lane"=>"巷", "alley"=>"弄", "no"=>"號", "no_her"=>"之", "floor"=>"樓", "floor_her"=>"之", "address_room"=>"室");
            //先建立一張新表
            $insert_key = '';
            $insert_value = '';
            $update_data = '';

            $address['address'] = '';//避免之前的影響先清掉
            $sql = "SELECT * FROM `county` WHERE `id_county` =".GetSQL($address['id_county'],"int");
            $row = Db::rowSQL($sql,true); //主要要取出county_name
            $address['county_name'] = $row["county_name"];//縣市名稱
            $sql = "SELECT * FROM `city` WHERE id_city=".GetSQL($address['id_city'],"int");
            $row_city = Db::rowSQL($sql,true); //主要要取出city_name
            $address['city_name'] = $row_city["city_name"];//市鄉政
            foreach($address_arr as $k => $i){
                if(!empty($address[$k])){
                    if($i=="之"){
                        $address['address'] .= $i.$address[$k];
                    }else{
                        $address['address'] .= $address[$k].$i;
                    }
                }
            }

            if(!empty($address['id_address'])){//原本就有的
                foreach($address as $key => $value){
                    if($key=="id_address" || $key=="county_name" || $key=="city_name") continue;//跳過這次
                    $update_data .= $key.'='.GetSQL($value,"text").',';
                }
                $update_data = substr($update_data,0,-1);
                $sql = "UPDATE address SET {$update_data} WHERE id_address =".GetSQL($address['id_address'],"int");
                Db::rowSQL($sql);
                $id_main = $address["id_address"];
            }else{
                foreach($address as $key => $value){
                    if($key=="id_address" || $key=="county_name" || $key=="city_name") continue;//跳過這次
                    $insert_key .=$key.',';
                    $insert_value .= GetSQL($value,"text").',';
                }
                $insert_key = substr($insert_key,0,-1);
                $insert_value = substr($insert_value,0,-1);
                $sql = "INSERT INTO address({$insert_key})VALUES ({$insert_value})";
                $row = Db::rowSQL($sql);
                $id_main = Db::getContext()->num;
            }
            return $id_main;
        }


        public static function get_data($id_admin){
            $data = [];//主data
            $id_skill =[];
            $sql = "SELECT * FROM admin WHERE active=1 AND id_admin=".GetSQL($id_admin,"int");
            $return_data = Db::rowSQL($sql,true);//因為只有一行XD
            foreach(LifeAssets::$multiple_data as $m_a_k => $m_a_v){
                $return_data[$m_a_v] = json_decode($return_data[$m_a_v],true);
            }

            //personnel_insurance 保險的值
            $personnel_insurance_arr = ["0"=>"labor","1"=>"health","2"=>"accident","3"=>"personnel_ensure"];
            foreach($personnel_insurance_arr as $p_i_a_k => $p_i_a_v){
                $return_data["personnel_insurance"][$p_i_a_v] = LifeAssets::get_personnel_insurance($id_admin,"admin",$p_i_a_k);//保險
            }


            if(!empty($return_data["id_skill"])){
                foreach($return_data["id_skill"] as $s_k => $s_v){
                    $id_skill[] = Skill::get_skill($s_v);
                }
                $return_data["id_skill"] = $id_skill;
            }
            foreach($return_data as $key => $value){
                foreach(LifeAssets::$address_data as $a_d_k=>$a_d_v){
                    if($key==$a_d_v && !empty($value)){//遇到並且不為空
                        $return_data[str_replace('id_','',$key)] = LifeAssets::get_address($value);
                    }
                }
                if($key=="id_admin" && !empty($value)){
                    $return_data["admin_family"] = LifeAssets::admin_family($value);
                    foreach(LifeAssets::$table as $l_t_k=>$l_t_v){
                        if($l_t_v=='admin_family') continue;
                        $return_data[$l_t_v] = LifeAssets::get_out_table_link($value,"admin",$l_t_v);
                    }
                }
            }
            return $return_data;
        }

        public static function get_out_table_link($id_admin,$role,$table){
            if(empty($id_admin) || empty($role) || empty($table)) return "";
            $sql = "SELECT * FROM {$table} WHERE id=".GetSQL($id_admin,"int").' AND role='.GetSQL($role,"text");
            return Db::rowSQL($sql);
        }

        public static function admin_family($id_admin){
            if(empty($id_admin)) return "";
            $sql = "SELECT * FROM admin_family WHERE id_admin=".GetSQL($id_admin,"int");
            $row = Db::rowSQL($sql);
            if(empty($row)) return "";
            foreach($row as $k=>$v){
                $row[$k]["address"] = LifeAssets::get_address($v["id_address"]);
            }
            return $row;
        }

        public static function get_personnel_insurance($id,$role,$type){//保險
            $row = [];
            if(empty($id) && empty($role) && !is_null($type)) return $row;
            $sql = "SELECT * FROM personnel_insurance WHERE id=".GetSQL($id,"int")." AND
             role=".GetSQL($role,"text")." AND type=".GetSQL($type,"int")." ORDER BY id_personnel_insurance ASC";
            return Db::rowSQL($sql);
        }

        public static function admin_source(){
            $sql = "SELECT * FROM admin_source WHERE active=1 ORDER BY id_admin_source ASC";
            return Db::rowSQL($sql);
        }

        public static function company($type=null){
            $where ='';
            if($type !=null){
                $where =  " AND type=".GetSQL($type,"int");
            }
            $sql = "SELECT * FROM company WHERE active=1 ".$where." ORDER BY id_company ASC";
            return Db::rowSQL($sql);
        }

        public static function department($id_company=3){//預設租管
            $sql = "SELECT * FROM department WHERE active=1 AND id_company=".$id_company." ORDER BY id_department ASC";
            return Db::rowSQL($sql);
        }

        public static function get_address($id){
            if(empty($id)) return "";
            $sql = "SELECT * FROM address WHERE id_address=".GetSQL($id,"int");
            return Db::rowSQL($sql,true);
        }

    }
