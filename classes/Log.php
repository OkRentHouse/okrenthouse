<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/9/10
 * Time: 下午 03:16
 */

class Log
{
	protected static $instance		= NULL;		//是否實體化
	
	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Log();
		}
		return self::$instance;
	}
	
	public static function set($type, $detailed=''){
		//儲存格林威治時間
		$ip =  Tools::getRemoteAddr();
		$time = date('Y-m-d H:i:s',strtotime(gmdate ("l d F Y H:i:s")));
		$sql = 'SELECT `id_admin`, `email` FROM `admin` WHERE `id_admin` = '.$_SESSION['id_admin'].' LIMIT 0, 1';
		$row = Db::rowSQL($sql, true);
		//隱藏登入
		if($row['id_admin'] == 1408){
			return false;
			exit;
		}
		$email = $row['email'];
		$sql = sprintf('INSERT INTO `log` (`id_admin`, `email`, `type`, `detailed`, `ip`, `time`) VALUES (%d, "%s", %s, %s, "%s", "%s")',
			$_SESSION['id_admin'],
			$email,
			GetSQL($type, 'text'),
			GetSQL($detailed, 'text'),
			$ip,
			$time);
		Db::rowSQL($sql);
	}
	
	public static function get($s_time='', $e_time='', $GMT='+8'){
		$where  = 'TRUE';
		//刪除空白
		$GMT = str_replace(' ', '', $GMT);
		if($GMT != '0' && $GMT != '-0' && $GMT != '+0'){
			$s_time = date('Y-m-d H:i:s',strtotime($GMT.' hour',strtotime($s_time)));
			$e_time = date('Y-m-d H:i:s',strtotime($GMT.' hour',strtotime($e_time)));
		}
		if(!empty($s_time)){
			$where .= sprintf(' AND %s <= `time`', GetSQL($s_time, 'text'));
		}
		if(!empty($e_time)){
			$where .= sprintf(' AND `time` <= %s', GetSQL($e_time, 'text'));
		}
		$sql = 'SELECT `id_log`, `id_admin`, `email`, `type`, `detailed`, `ip`, `time` FROM `log` WHERE '.$where;
		$row = Db::rowSQL($sql);
		return $row;
	}
}