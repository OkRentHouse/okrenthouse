<?php
/**
 * Created by PhpStorm.
 * User: 陳哲儒
 * Date: 2020/04/24
 * Time: 上午 10:30
 */
class LineApi
{
    protected static $instance = null;        //是否實體化

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new LineApi();
        }
        return self::$instance;
    }

    public static function Authorization($data){
        $url = "https://access.line.me/oauth2/v2.1/authorize?";//官方授權網址
//        $response_type=null,$client_id=null,$redirect_uri=null,$state=null,
//    $state=null,$scope="openid%20profile%20email",$nonce=null,$prompt=true,$max_age=null,$ui_locales=null,$bot_prompt=null
        if(empty($data['scope'])){
            $data['scope'] = 'openid%20profile%20email';
        }else if(empty($data['response_type'])){
            $data['scope'] = 'code';
        }
        foreach($data as $k =>$v){
            $url .= '&'.$k.'='.$v;
        }

//        return LineApi::getContext()->cURL($url,'1');
        return $url;
    }

    public  function cURL($url,$type=1,$header=null){
        $curl = curl_init();
// 設定curl網址
        curl_setopt($curl, CURLOPT_URL, $url);
// 設定Header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER ,true);
        curl_setopt($curl, CURLOPT_ENCODING ,"");
        if($type==2){
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST ,"POST");
        }else{
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST ,"GET");
        }
        if(!empty($header)){
            curl_setopt($curl, CURLOPT_HTTPHEADER ,$header);
        }
// 執行
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
}