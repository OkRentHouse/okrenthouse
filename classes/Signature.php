<?php
/**
 * Created by PhpStorm.
 * User: Allen
 * Date: 2019/3/18
 * Time: 上午 11:27
 */

class Signature
{
	protected static $instance;
	public $_errors = array();
	public $_msgs = array();
	public $msg = '';

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Signature();
		};
		return self::$instance;
	}

	public static function hash($content, $img_data, $HTTP_USER_AGENT, $datetime){
		$datetime = str_replace('T', ' ', $datetime);
		if(is_array($content)){
			$json = new JSON();
			$content = $json->encode($content);
		}
		$content = str_replace(':null', ':\'\'', $content);
		$str = BEFORE_HASH.$content.$img_data.$HTTP_USER_AGENT.$datetime.AFTER_HASH;
		return hash('sha512', $str);
	}
}