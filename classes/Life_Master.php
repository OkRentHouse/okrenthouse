<?php

class Life_Master{
    protected static $instance;

    public function __construct(){
        define('Master_IMG_DIR', 'img'.DS.'master');							//Master 資料夾位置
        define('Master_IMG_URL', '/img/master/');							//Master 網址路徑
    }
    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new Life_Master();
        };
        return self::$instance;
    }
    public  $arr_file_type = [
            'id_img' => '0',
            'account_img' => '1',
    ];
    public $complete_address_arr = [
        "county_name" => "",
        "city_name" => "",
        "village" => "",
        "neighbor" => "鄰",
        "road" => "",
        "segment" => "段",
        "lane" => "巷",
        "alley" => "弄",
        "no" => "號",
        "no_her" => "之",
        "floor" => "樓",
        "floor_her" => "之",
        "address_room" => "室"];

    public static function validation($data,$file_data){//後台驗證
        $return = [];
//        $id_member = $_SESSION['id_member'];//id member
        $id_county = "";
        $id_city = "";
        $postal = "";
        $bank_account = "";
        foreach($data as $k =>$v){
            $$k = $v;//將吃到的資料變成個別變數
            if(strpos($k,'postal_') !==false) $postal .=$v;
            if(strpos($k,'bank_account_') !==false) $bank_account.=$v;
        }

//        $sql = "SELECT count(*) count_num FROM ilifehou_ilife_house.`master` WHERE id_member=".GetSQL($id_member,'int');
//        $count_num =Db::rowSQL($sql,true);
        //驗證是否有存在該id_county
        $sql = "SELECT count(*) count_num,county_name FROM county WHERE id_county=".GetSQL($id_county,'int');
        $county_num =Db::rowSQL($sql,true);

        //驗證是否有存在該id_city
        $sql = "SELECT count(*) city_num,city_name FROM city WHERE id_county=".GetSQL($id_county,"int").' AND id_city='.GetSQL($id_city,"int");
        $city_num =Db::rowSQL($sql,true);

//        if(empty($id_member)){
//            $return['error'][] = '請登入帳號!';
//            return $return;
//        }
//        if($count_num['count_num']>0){
//            $return['error'][] = '您已申請過,請等待審查結果!';
//            return $return;
//        }
        if(($name!= $_SESSION['name']) || empty($name) || mb_strlen($name,"utf-8")>'20'){
            $return['error'][] = '姓名輸入與申請不符或是過長!';
        }
        if(($nickname!= $_SESSION['nickname']) || empty($nickname) || mb_strlen($nickname,"utf-8")>'20'){
            $return['error'][] = '暱稱輸入與申請不符或是過長!';
        }
        if(!checkdate($birthday_m,$birthday_d,$birthday_y)){
            $return['error'][] = '時間不符合規定!';
        }
        if(!preg_match("/09[0-9]{8}/",$phone) || mb_strlen($nickname,"utf-8")>'20'){
            $return['error'][] = '手機不符合格式';
        }
        if(empty($line_id) || mb_strlen($line_id,"utf-8")>'32'){
            $return['error'][] = 'line_id不符合格式';
        }
        if(mb_strlen($mail,"utf-8")>'128'){
            $return['error'][] = 'email不符合格式';
        }
        if(mb_strlen($postal,"utf-8")!='5'){
            $return['error'][] = '通訊郵遞';
        }
        if($county_num['count_num'] !='1'){
            $return['error'][] = '未選擇正確的縣市';
        }
        if($city_num['city_num'] !='1'){
            $return['error'][] = '未選擇正確的鄉鎮區';
        }

        if(empty($address) || mb_strlen($address,"utf-8")>'255'){
            $return['error'][] = '地址格式不符';
        }
        if($message_source!='0' && $message_source!='1' && $message_source!='2' && $message_source!='3'){
            $return['error'][] = '訊息來源不正確';
        }
        if( strlen($identity) > 12 || strlen($identity) < 10) {
            $return['error'][] = '身分證或居留碼輸入不正確';
        }
        if(empty($bank) || mb_strlen($address,"utf-8")>'32'){
            $return['error'][] = '銀行格式不符';
        }
        if(empty($branch) || mb_strlen($branch,"utf-8")>'32'){
            $return['error'][] = '分行格式不符';
        }
        if(empty($bank_code) || strlen($bank_code)>'4'){
            $return['error'][] = '銀行代碼不正確';
        }
        if(strlen($bank_account) !='14'){
            $return['error'][] = '存款帳號格式不符';
        }

        foreach($file_data as $f_k=>$f_v){
            foreach($f_v as $k=>$v){
                if($k=='type'){
                    for($i=0;$i<=1;$i++){
                        if($v[$i]!='image/png' && $v[$i]!='image/jpeg'){
                            $return['error'][] = '上傳圖片不存在或是格式不符!';
                            break 3;
                        }
                    }
                }
            }
        }

        if(empty($file_data['error'])){
            $return['success']='1';
        }

        return $return;
    }

    public static function insert_data($data,$file_data,$application_source=0){//$data==$_POST,$file==$_FILES
//        $id_member = $_SESSION['id_member'];//id member
        $id_member = empty($_SESSION['id_member'])?'':$_SESSION['id_member'];//有登入給 沒登入就給空白
        $id_county = "";
        $id_city = "";
        $other_url ="";
        $other_intermediary ="";
        $other ="";
        $postal = "";
        $bank_account = "";
        $birthday = "";

        foreach($data as $k =>$v){
            $$k = $v;//將吃到的資料變成個別變數
            if(strpos($k,'postal_') !==false) $postal .=$v;
            if(strpos($k,'bank_account_') !==false) $bank_account.=$v;

        }

        if(empty($birthday)){
            $birthday =$birthday_y.'-'.$birthday_m.'-'.$birthday_d;
        }

        $sql = "SELECT * FROM `county` WHERE `id_county` =" . GetSQL($id_county, "int");//$id_county 會自動產生
        $row = Db::rowSQL($sql, true); //主要要取出county_name
        $county_name = $row["county_name"];//縣市名稱
        $county_code = $row["county_code"];//縣市名稱code
        $sql = "SELECT * FROM `city` WHERE id_city=" . GetSQL($id_city, "int");
        $row_city = Db::rowSQL($sql, true); //主要要取出city_name
        $city_name = $row_city["city_name"];//市鄉政
        $city_code = $row_city["city_code"];//市鄉政code

        if(empty($master_number)){//不存在則自動產生建議讓他自己產生
            $rand_code = rand(1000,9999);//生成隨機的4碼
            $number_prefix = empty($id_member)?rand(1000,9999):$id_member;
            $master_number = $county_code.$city_code.'-'.$number_prefix.date("m").date("d").$rand_code;
        }

        $sql = 'INSERT INTO ilifehou_ilife_house.`master`
        (master_number,name,nickname,identity,birthday,postal,id_county,id_city,address,phone,phone_2,line_id,local_calls,
        email,bank_account,bank,branch,bank_code,message_source,recommend_id_member,id_member,status,application_source,other_url,other_intermediary,other)
        VALUES('.GetSQL($master_number,'text').','.GetSQL($name,'text').','.GetSQL($nickname,"text").','.GetSQL($identity,"text").',
        '.GetSQL($birthday,"date").','.GetSQL($postal,"int").','.GetSQL($id_county,"int").','.GetSQL($id_city,'int').',
        '.GetSQL($address,'text').','.GetSQL($phone,'text').','.GetSQL($phone_2,'text').','.GetSQL($line_id,'text').','.GetSQL($local_calls,'text').',
        '.GetSQL($email,"text").','.GetSQL($bank_account,'text').','.GetSQL($bank,'text').','.GetSQL($branch,'text').','.GetSQL($bank_code,'int').',
        '.GetSQL($message_source,"int").','.GetSQL($recommend_id_member,'int').','.GetSQL($id_member,'int').',0,'.GetSQL($application_source,'int').','.GetSQL($other_url,'text').',
        '.GetSQL($other_intermediary,'text').','.GetSQL($other,'text').')';
        
        Db::rowSQL($sql);
        $id = Db::getContext()->num;

        if(!empty($file_data)){//有上傳檔案
            $sql = "SELECT * FROM `master` WHERE id_master=".GetSQL($id,"text");
            $data = Db::rowSQL($sql,true);//取得相關資料
            $dir      = Life_Master::getContext()->iniUpFileDir($data['id_master']);
            add_dir($dir);
            $url = Life_Master::getContext()->iniUpFileUrl($data['id_master']);
            foreach($file_data as $file_key=>$file){
                $filenames = $file['name'];
                // loop and process files
                $arr = [];
                for ($i = 0; $i < count($filenames); $i++) {
                    $ext = explode('.', basename($filenames[$i]));
                    $file_name = FileUpload::check_name($file['name'][$i], $dir);
                    $target = $dir . DS . $file_name;
                    if (move_uploaded_file($file['tmp_name'][$i], $target)) {
                        $success = true;
                        $arr[] = [
                            'ext' => array_pop($ext),
                            'name' => $file_name,
                            'type' => $file['type'][$i],
                            'size' => $file['size'][$i],
                            //						'downloadUrl' => $url.$file_name,
                            'paths' => $target,
                        ];
//存檔
                    } else {
                        //todo 單黨上傳 2019-12-18
                        $ext = explode('.', basename($filenames));
                        $file_name = FileUpload::check_name($file['name'], $dir);
                        $target = $dir . DS . $file_name;

                        if (move_uploaded_file($file['tmp_name'], $target)) {
                            $success = true;
                            $arr[] = [
                                'ext' => array_pop($ext),
                                'name' => $file_name,
                                'type' => $file['type'],
                                'size' => $file['size'],
                                //						'downloadUrl' => $url.$file_name,
                                'paths' => $target,
                            ];
                        } else {
                            $success = false;
                            break;
                        }
                    }
                }
                Life_Master::getContext()->displayAjaxUpFileAction($arr, $dir, $url, $data['id_master'],$file_key);
            }
        }
    }

    public function iniUpFileDir($id_master){//master 跟要取得的file key
        list($dir, $url) = str_dir_change($id_master);     //切割資料夾
        $dir = WEB_DIR . DS . Master_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl($id_master){
        list($dir, $url) = str_dir_change($id_master);
        $url = Master_IMG_URL . $url;
        return $url;
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $id_master,$file_key){
        $type = Life_Master::getContext()->arr_file_type[$file_key];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `master_file` (`id_master`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_master, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql,false,0);
            if (!Db::getContext()->num)
                return false;
                $sql = "SELECT  CONCAT(file_dir,filename) img_url FROM file WHERE id_file=".GetSQL($id_file,"int");
                $data_img = Db::rowSQL($sql,true);//取得檔案位置
                $after_img = urldecode($data_img["img_url"]);
                Watermark::get_watermark_text($after_img,'僅供申請生活達人之用,不得移為他用',$after_img);
        }
        return true;
    }
}

