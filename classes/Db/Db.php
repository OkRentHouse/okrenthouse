<?php

class Db
{
	public $db;                            //$db
	public $des = false;        //Class說明
	public $cache = true;            //快取
	public $debug = false;        //debug
	public $table = '';            //資料庫表單
	public $version = '';            //版本
	public $author = '';            //作者
	public $explanation = '';            //說明
	public $key = [];        //索引鍵
	public $insert = [];        //新增的欄位
	public $update = [];        //修改的欄位
	public $delete = [];        //刪除的欄位
	public $select = '*';            //查詢的欄位
	public $db_mysql;                    //MySQL連接物件
	public $num = 0;            //查詢數量
	public $sql = '';            //MySQL原始碼
	public $row = [];        //查詢結果
	public $limit = null;        //MySQL查詢數量
	public $LIMIT = '';            //MySQL原始碼
	public $WHERE = '';            //MySQL原始碼
	protected static $instance = null;        //是否實立

	public function __construct()
	{
		$this->cache = CACHE;    //Config::get(WEB_CACHE);
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Db();
		};
		return self::$instance;
	}

	public static function getClass()
	{
		$class = 'MySQL';
		if (PHP_VERSION_ID >= 50200 && extension_loaded('pdo_mysql')) {
			$class = 'DbPDO';
		} elseif (extension_loaded('mysqli')) {
			$class = 'DbMySQLi';
		}

		return $class;
	}

	public static function rowSQL($sql, $limit = false, $num = 0, $conn = '')
	{
		Db::getContext()->sql = $sql;
		$db                   = new db_mysql($sql, $num, '', 'p', 'm', $conn);
		$arr_d                = [];
		Db::getContext()->db = $db;
		Db::getContext()->num = $db->num();
		if ($db->num() > 0) {
			if (!$limit) {
				while ($d = $db->next_row()) {
					$arr_d[] = $d;
				};
			} else {
				return $db->row();
			};
		};
		return $arr_d;
	}

    public static function antonym_array($str,$int=false){//去除 一些如 ["123","4565","87"] => "123","4565","87" 使她能夠用IN() 搜索
//        $str =  str_replace('"','',$str);
        $str =  str_replace("[",'',$str);
        $str =  str_replace("]",'',$str);
        if($int==true){
            $str =  str_replace('"','',$str);
        }
        return $str;
    }

    public static function all_antonym_array($arr,$int=false){//上面的加強版包括驗證是否陣列處理
        if(isset($arr)){//存在做這道工序
            foreach($arr as $key => $value){
                if(strstr($value,'[') && strstr($value,']') && strstr($value,'"')){//找到標的物將它變成陣列形式
                    $arr[$key] = Db::antonym_array($arr[$key],$int);
                    $arr[$key] = explode(",",$arr[$key]);
                }
            }
        }
        return $arr;
    }

	//資料表是否存在
	public static function if_table($table, $db_prefix_ = DB_PREFIX_)
	{
		$sql         = 'SHOW TABLES LIKE "' . $db_prefix_ . $table . '"';
		$SHOW_TABLES = new db_mysql($sql);
		return $SHOW_TABLES->num();
	}

	//建立資料庫
	public function found()
	{
		return true;
	}

	public function row()
	{
		return $this->row;
	}

	//顯示資料內容
	public function show()
	{
		if ($this->cache) {
			add_dir(CACHE_DIR . DS . 'db', 0755);
			$file = CACHE_DIR . DS . 'db' . DS . 'db_' . DB_PREFIX_ . $this->table . '_show.php';
			if (is_file($file))
				return include($file);
		};
		$this->sql      = 'SHOW FULL FIELDS FROM `' . DB_PREFIX_ . $this->table . '`';
		$this->db_mysql = new db_mysql($this->sql);
		$this->num      = $this->db_mysql->num();
		if ($this->num > 0) {
			while ($full = $this->db_mysql->next_row()) {
				$this->row[] = $full;
			};
		};

		if ($this->cache) {
			$fp = fopen($file, 'w');        //新增、覆蓋檔案
			fwrite($fp, "<?php return array (\r\n");
			$i = 0;
			foreach ($this->row as $key => $v) {
				fwrite($fp, "\tarray(\r\n");
				foreach ($v as $j => $jv) {
					if ($j == 'Key') {
						fwrite($fp, "\t\t'" . $j . "'\t\t\t=> '" . str_replace("'", "\'", $jv) . "',\r\n");
					} elseif ($j == 'Privileges') {
						fwrite($fp, "\t\t'" . $j . "'\t=> '" . str_replace("'", "\'", $jv) . "',\r\n");
					} else {
						fwrite($fp, "\t\t'" . $j . "'\t\t=> '" . str_replace("'", "\'", $jv) . "',\r\n");
					};
				};
				fwrite($fp, "\t),\r\n");
			};
			fwrite($fp, "); ?>");
			fclose($fp);
		};
		return $this->row;
	}

	//取得資料
	public function get()
	{
		if (empty($this->select))
			$this->select = '*';
		if (is_array($this->select)) {
			$arr_key = [];
			$arr_v   = [];
			foreach ($this->select as $key => $v) {
				$arr_v[]      = '`' . $v . '`';
				$this->select = implode(',', $arr_v);
			};
		};
		if (count($this->key) > 0) {
			if (empty($this->WHERE))
				$this->WHERE = ' WHERE';
			$arr_key = [];
			foreach ($this->key as $key => $v) {
				$arr_key[] = '`' . $key . '` = "' . $v . '"';
			};
			$this->WHERE .= ' ' . implode(' AND ', $arr_key);
		};
		$this->sql      = 'SELECT ' . $this->select . ' FROM `' . DB_PREFIX_ . $this->table . '` ' . $this->WHERE . ' ' . $this->LIMIT;
		$this->db_mysql = new db_mysql($this->sql);
		$this->num      = $this->db_mysql->num();
		if ($this->limit == 1) {
			$this->row = $this->db_mysql->row();
		} else {
			while ($row = $this->db_mysql->next_row()) {
				$this->row[] = $row;
			};
		};
		return $this;
	}



	//資料數
	public function num()
	{
		return $this->num;
	}

	//資料數
	public function num_row()
	{
		return $this->num_row;
	}

	//顯示欄位註解
	public function field()
	{
		if ($this->cache) {
			$file = CACHE_DIR . DS . db . DS . 'db_' . DB_PREFIX_ . $this->table . '_field.php';
			if (is_file($file))
				return include($file);
		};
		$arr_full = $this->show();
		$full     = [];
		foreach ($arr_full as $key => $v) {
			$full[$v['Field']] = $v['Comment'];
		};
		if ($this->cache) {
			$fp = fopen($file, 'w');        //新增、覆蓋檔案
			fwrite($fp, "<?php return array (\r\n");
			foreach ($full as $key => $v) {
				fwrite($fp, "\t'" . $key . "'\t=> '" . $v . "',\r\n");
			};
			fwrite($fp, "); ?>");
			fclose($fp);
		};
		return $full;
	}

	//新增資料
	public function add($db_prefix = DB_PREFIX_)
	{
		if (!is_array($this->insert) || count($this->insert) == 0)
			return false;
		$arr_key = [];
		$arr_v   = [];
		foreach ($this->insert as $key => $v) {
			$arr_key[] = '`' . $key . '`';
			$arr_v[]   = (empty($v)) ? 'NULL' : '\'' . $v . '\'';
		};
		$this->sql      = 'INSERT INTO `' . $db_prefix . $this->table . '` (' . implode(',', $arr_key) . ') VALUES (' . implode(',', $arr_v) . ')';
		$this->db_mysql = new db_mysql($this->sql);
		$this->num      = $this->db_mysql->num();
		return $this;
	}

	//修改資料
	public function up()
	{

	}

	//刪除資料
	public function del()
	{

	}

	public function show2($table)
	{
		$this->sql      = 'SHOW FULL FIELDS FROM `' . DB_PREFIX_ . $table . '`';
		$this->db_mysql = new db_mysql($this->sql);
		$this->num      = $this->db_mysql->num();
		$arr_full       = [];
		if ($this->num > 0) {
			while ($full = $this->db_mysql->row()) {
				/*
				Field
				Type
				Collation
				Null
				Key
				Default
				Extra
				Privileges
				Comment
				*/
				'array(
					\'type\'		=> \'\',
					\'class\'		=> \'\',
					\'id\'		=> \'' . $full['Field'] . '\',
					\'name\'		=> \'' . $full['Field'] . '\',
					\'value\'		=> \'\',
					\'length\'		=> \'\',
					\'cols\'		=> \'\',
					\'rows\'		=> \'\',
					\'title\'		=> \'\',
					\'required\'	=> \'\',
					\'placeholder\' 	=> \'\',
					\'attr\'		=> \'\'
				)';
				$arr_full[] = $full;
			};
		};
		return 'array();';
	}

	//整理	刪除資料庫註解
	public function sort_out()
	{

	}

	public static function getJumpPage($show_page_num=10, $p_ley='p', $m_ley='m')
	{
		foreach ($_GET as $key => $v) {
			if (($key != $p_ley) && ($key != $m_ley)) {
				if (is_array($v)) {        //複選篩選
					foreach ($v as $i => $vv) {
						$arr_pagination_url[] = $key . '[]=' . $vv;
					}
				} else {
					$arr_pagination_url[] = $key . '=' . $v;
				}
			}
		}
		$p = Tools::getValue($p_ley, 1);

		$total_page = Db::getContext()->db->total_page();

		Context::getContext()->smarty->assign([
			'pagination_p' => $p_ley,
			'pagination_m' => $m_ley,
			'pagination_min' => max(min($total_page, max(($p + floor($show_page_num)), $show_page_num)) - $show_page_num, 1),
			'pagination_max' => $total_page,
			'pagination_num' => $show_page_num,
			'pagination_url' => implode('&', $arr_pagination_url),
		]);

	}
}