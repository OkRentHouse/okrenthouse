<?php
class NewsDb{
	public function __construct(){
		$this->table = 'news';
		$this->version = '1.0.0';
		$this->author = 'LilyHouse';
		$this->explanation = '最新消息資料庫';
	}
	
	//建立資料庫
	public function found(){
		$sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX_."news` (
			  `id_news` int(11) NOT NULL COMMENT '最新消息ID',
			  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '最新消息標題',
			  `date` datetime NOT NULL COMMENT '日期',
			  `contents` text COLLATE utf8_unicode_ci NOT NULL COMMENT '內容',
			  `active` bit(1) NOT NULL COMMENT '啟用狀態'
			) ENGINE=".DB_ENGINE_." DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
		$CREATE_news = new db_mysql($sql);
			
		$sql = "ALTER TABLE `".DB_PREFIX_."news`
			  ADD PRIMARY KEY (`id_news`);";
		$CREATE_news = new db_mysql($sql);
			
		$sql = "ALTER TABLE `".DB_PREFIX_."news`
			  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT COMMENT '最新消息ID';";
		$CREATE_news = new db_mysql($sql);
	}
	
	//取得
	
	public function get($id_news){
		$sql = 'SELECT *
			FROM `'.DB_PREFIX_.'news`
			WHERE nb.`id_news` = '.$id_news.'
			LIMIT 0, 1';
		$SELECT_building_group = new db_mysql($sql);
	}
	
	//新增
	/*
	*	$title	: 標題
	*	$date		: 日期時間
	*	$contents	: 訊息內容
	*	$active	: 啟用狀態 (1:啟用, 0:關閉)
	*	回傳key
	*/
	public function add($title,$date,$contents,$active=1){
		$sql = sprintf('INSERT INTO `'.DB_PREFIX_.'news` (`title`, `date`, `contents`, `active`) VALUES (%s, %s, %s, %d)',
					GetSQL($title,'text'),
					GetSQL($date,'text'),
					GetSQL($contents,'text'),
					GetSQL($active,'int'));
		$INSERT_news = new db_mysql($sql);
		return $INSERT_news->num();
	}
	
	//修改
	/*
	*
	*/
	public function edit($id_news,$title,$date,$contents,$active=1){
		$sql = sprintf('INSERT INTO `'.DB_PREFIX_.'news` (`title`, `date`, `contents`, `active`) VALUES (%s, %s, %s, %d)',
					GetSQL($title,'text'),
					GetSQL($date,'text'),
					GetSQL($contents,'text'),
					GetSQL($active,'int'));
		$INSERT_news = new db_mysql($sql);
		return $INSERT_news->num();
	}
	
	//刪除
	/*
	*	$id_news = int or array(int)
	*/
	public function del($id_news){
		if(!is_array($id_news)){
			$sql = 'DELETE FROM `'.DB_PREFIX_.'news` WHERE `id_news` = '.$id_news.'LIMIT 0, 1';
		}else{
			$id_news = implode(',',$id_news);
			$sql = 'DELETE FROM `'.DB_PREFIX_.'news` WHERE `id_news` IN ('.$id_news.')';
		};
		$DELETE_news = new db_mysql($sql);
		return $DELETE_news->num();
	}
	
	public function show(){
		return parent::show();
	}
}