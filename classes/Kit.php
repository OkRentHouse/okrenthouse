<?php
abstract class Kit{
	public $_errors = array();		//錯誤
	public $css_files = array();		//css 檔案
	public $js_files = array();		//js 檔案
	protected $template;			//樣版
	abstract public function viewAccess();
	/**
	* Assigns Smarty variables for the page header
	*/
	abstract public function initHeader();
	
	/**
	* Assigns Smarty variables for the page main content
	*/
	abstract public function display();
	public $id;		//id 樣式
	public $class;	//class 樣式
	public $name;	//元件名稱
	
	
}