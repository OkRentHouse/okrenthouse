<?php

class Skill{

    protected static $instance = null;        //是否實體化

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new Skill();
        }
        return self::$instance;
    }

    static function get_skill_class($id,$active=1){
        $WHERE = ' WHERE TRUE';
        $only_one = false;
        if($id!=null){
            $WHERE .= ' AND id_skill_class ='.GetSQL($id,"int");
            $only_one =true;
        }
        if($active !=null){
            $WHERE .= ' AND active ='.GetSQL($active,"int");
        }

        $sql = "SELECT * FROM skill_class ".$WHERE." ORDER BY position ASC,id_skill_class ASC";
        $row = Db::rowSQL($sql,$only_one);//只有一個id代表定值只會有一個
        return $row;
    }

    static function get_skill_classification ($id,$active=1){
        $WHERE = ' WHERE TRUE';
        $only_one = false;
        if($id!=null){
            $WHERE .= ' AND s_c_i.`id_skill_classification`  ='.GetSQL($id,"int");
            $only_one =true;
        }
        if($active !=null){
            $WHERE .= ' AND s_c_i.`active` ='.GetSQL($active,"int");
        }

        $sql = "SELECT s_c_i.`id_skill_classification` as id_skill_classification,s_c_i.`id_skill_class` as id_skill_class,
                s_c_i.`title` as title,s_c.`title` as s_c_title,
                s_c_i.`position` as position,s_c_i.`active` as active
                FROM skill_classification  as s_c_i
                LEFT JOIN `skill_class` AS s_c ON s_c.`id_skill_class` = s_c_i.`id_skill_class`
                ".$WHERE." ORDER BY s_c_i.`position` ASC,s_c_i.`id_skill_classification`  ASC";
        $row = Db::rowSQL($sql,$only_one);//只有一個id代表定值只會有一個
        return $row;
    }

    static function get_skill($id,$active=1){
        $WHERE = ' WHERE TRUE';
        $only_one = false;
        if($id!=null){
            $WHERE .= ' AND s.`id_skill`  ='.GetSQL($id,"int");
            $only_one =true;
        }
        if($active !=null){
            $WHERE .= ' AND s.`active` ='.GetSQL($active,"int");
        }

        $sql = "SELECT s.`id_skill` as id_skill,s.`id_skill_classification` as id_skill_classification,s_c_i.`id_skill_class` as s_c_i_id_skill_class,
                s.`title` as title,s_c_i.`title` as s_c_i_title,s_c.`title` as s_c_title,
                s.`show` as `show`, s.`position` as position
                FROM skill  as s 
                LEFT JOIN `skill_classification` AS s_c_i ON s_c_i.`id_skill_classification` = s.`id_skill_classification`
                LEFT JOIN `skill_class` AS s_c ON s_c.`id_skill_class` = s_c_i.`id_skill_class` 
                ".$WHERE." ORDER BY s.`position` ASC,s.`id_skill` ASC";
        $row = Db::rowSQL($sql,$only_one);//只有一個id代表定值只會有一個
        return $row;

    }
}