<?php
class Cache
{
	protected static $instance;
	protected $keys = array();
	protected static $local = array();

	public static function getContext(){
		if (!self::$instance) {
			self::$instance = new Cache();
		};
		return self::$instance;
	}
	
	//儲存
	public static function set($key, $value){
		// PHP 不是一個很好的儲存方式
		// 超過1000筆就刪除
		if (count(Cache::$local) > 1000) {
			Cache::$local = array();
		}
		Cache::$local[$key] = $value;
	}

	//取回
	public static function get($key){
		return isset(Cache::$local[$key]) ? Cache::$local[$key] : NULL;
	}

	//取回全部
	public static function getAll(){
		return Cache::$local;
	}

	//是否有儲存
	public static function is_set($key){
		return isset(Cache::$local[$key]);
	}
}
