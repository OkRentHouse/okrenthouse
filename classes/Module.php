<?php
/*
*	模組
*/
class Module{
	public 		$id;					//ID
	public 		$_errors	= array();		//錯誤
	public			$dir	= '';				//模組線在目錄
	public			$web_versions_compliancy = array();		//比對模組版本
	public 		$url;					//模組url
	public 		$name;				//模組名稱
	public 		$tab;					//模組標籤
	public			$table;				//資料庫表單名稱
	public 		$version;				//模組版本
	public 		$author;				//模組作者
	public 		$warning;				//模組警告
	public 		$displayName;			//顯示名稱
	public 		$description;			//描述
	public			$context;				//
	public 		$hook_list	= array();		//可掛載的Hook List列表
	public	 static		$_log_modules_perfs = null;		//紀錄模組效能
	public	 static		$modules_cache = null;			//快取
	protected static	$_INSTANCE = array();
	
	
	protected static	$instance	= NULL;		//是否實體化
	
	public static function getContext(){
		if (!self::$instance) {
			//eval('self::$instance = new '.get_class($this).'()');
		};
		return self::$instance;
	}
	
	//數值
	public function __construct(){
		
		//預設最低版本
		if (isset($this->web_versions_compliancy) && !isset($this->web_versions_compliancy['min'])) $this->web_versions_compliancy['min'] = '1.0.0.0';
		
		//預設最高版本
		if (isset($this->web_versions_compliancy) && !isset($this->web_versions_compliancy['max'])) $this->web_versions_compliancy['max'] = WEB_VERSION;
		
		//最低版本重寫
		if (strlen($this->web_versions_compliancy['min']) == 3) {
			$this->web_versions_compliancy['min'] .= '.0.0';
		}
		
		//最高版本重寫
		if (strlen($this->web_versions_compliancy['max']) == 3) {
			$this->web_versions_compliancy['max'] .= '.999.999';
		}

		$this->context = Context::getContext();

		$this->name = get_called_class();

		if ($this->name != null) {
			if (!isset(self::$modules_cache) && count(self::$modules_cache) == 0) {
				$sql = 'SELECT `id_module`, `name`, `active`
				FROM `' . DB_PREFIX_ . 'module`';
				$arr_row = Db::rowSQL($sql);
				foreach ($arr_row as $row) {
					self::$modules_cache[$row['name']] = $row;
				}
			}
			if (isset(self::$modules_cache[$this->name])) {
				if (isset(self::$modules_cache[$this->name]['id_module'])) {
					$this->id = self::$modules_cache[$this->name]['id_module'];
				}
				foreach (self::$modules_cache[$this->name] as $key => $value) {
					if (array_key_exists($key, $this)) {
						$this->{$key} = $value;
					}
				}
				$this->dir = MODULE_DIR.DS.$this->name;
			}
		}
		$this->url = MODULE_URL.'/'.$this->name.'/';
	}
	
	//安裝資料表
	public function install_table(){
		if(!empty($this->table)){
			if(is_array($this->table)){
				foreach($this->table as $i => $table){
					if(is_file(MODULE_DIR.DS.'Db'.'Db'.DB_PREFIX_.$table.'.php')){	//是否有資料庫檔
						eval('Db'.$table.'::found();');	//安裝資料庫
					};
				};
			}else{
				if(is_file(MODULE_DIR.DS.'Db'.'Db'.DB_PREFIX_.$this->table.'.php')){//是否有資料庫檔
					eval('Db'.$this->table.'::found();');	//安裝資料庫
				};
			};
		};
		return true;
	}
	
	//安裝admin樣版
	public function install_admin_theme(){
		if(is_file(ADMIN_DIR)){
			$from_dir = ADMIN_DIR.DS.'templates'.DS.'admin';
			if(is_file($from_dir)){
				$to_dir = 'template'.DS.'default'.DS.'modules'.DS.$this->name;
				add_dir($to_dir);
				copy_dir($from_dir, $to_dir);
			};
		};
		return true;
	}
	
	//安裝front樣版
	public function install_front_theme(){
		$from_dir = MANAGE_DIR.DS.'templates'.DS.'front';
		if(is_file($from_dir)){
			//$to_dir = THEME_DIR.'default'.DS.'modules'.DS.$this->name;
			add_dir($to_dir);
			copy_dir($from_dir, $to_dir);
		};
		return true;
	}
	
	//安裝
	public function install(){
		
		//模組名稱比對
		if(!isModuleName($this->name)){
			 $this->_errors[] = $this->l('模組名稱錯誤! (模組名稱必須為a-zA-Z0-9_-)');
			 return false;
		};
		
		//檢察模組是否符合版本
		if(!$this->checkCompliancy()) {
			$this->_errors[] = $this->l('您的模組不符合您的版本!');
			return false;
		};
		
		if($this->install_table() ||
			$this->install_admin_theme() ||
			$this->install_front_theme()){
				
		};
	}
	
	//模組與網站版本比較
	public function checkCompliancy()
	{
		if (version_compare(WEB_VERSION, $this->web_versions_compliancy['min'], '<') || version_compare(WEB_VERSION, $this->web_versions_compliancy['max'], '>')) {
			return false;
		}else{
			return true;
		};
	}
	
	//卸載
	public function uninstall(){
		
	}
	
	//取得模組是否有安裝
	public static function getInstanceByName($module_name){
		if (!isset(self::$_INSTANCE[$module_name])) {
			if (!Tools::file_exists_no_cache(MODULE_DIR.DS.$module_name.DS.$module_name.'.php')) {
				return false;
			}
			return Module::LoadModule($module_name);
		}
		return self::$_INSTANCE[$module_name];
	}
	
	public static function active($name = NULL, $active = 0){
		//if(empty($name)) $name = $this->name;
		$sql = sprintf('UPDATE `'.DB_PREFIX_.'module` SET `active` = %d
			WHERE `name` = %s',
			GetSQL($active, 'int'),
			GetSQL($name, 'text'));
		$db = new db_mysql($sql);
		return $db->num();
	}
	
	//啟用
	public function enable($name = NULL){
		Module::active($name, 1);
	}
	
	//關閉
	public function disable($name = NULL){
		Module::active($name, 0);
	}
	
	//權限
	public function competence(){
		
	}
	
	//控制
	public function controller(){
		
	}
	
	//呈現形式
	public function renderForm(){
		
	}
	
	//套件
	public function kit($kit){
		
	}
	
	protected static function LoadModule($module_name){
		//紀錄模組效能
		if (Module::$_log_modules_perfs === null) {
			$Lilyhouse = new LilyHouse();
			
			$modulo = $Lilyhouse->debug ? 1 : Configuration::get('log_modules_performances');
			Module::$_log_modules_perfs = ($modulo && mt_rand(0, $modulo - 1) == 0);
			if (Module::$_log_modules_perfs) {
				Module::$_log_modules_perfs_session = mt_rand();
			}
		}
		
		if (Module::$_log_modules_perfs) {
			$time_start = microtime(true);
			$memory_start = memory_get_usage(true);
		}
		
		$r = false;
		if (Tools::file_exists_no_cache(MODULE_DIR.DS.$module_name.DS.$module_name.'.php')) {
			if(is_file(MODULE_DIR.DS.$module_name.DS.'Autoload.php')){		//Autoload
				include_once(MODULE_DIR.DS.$module_name.DS.'Autoload.php');
			}
			include_once(MODULE_DIR.DS.$module_name.DS.$module_name.'.php');
			$override = $module_name.'Override';	//覆蓋
			
			if (class_exists($override, false)) {
				$r = self::$_INSTANCE[$module_name] = new $override();
			}
		}
		
		if (!$r && class_exists($module_name, false)) {
			$r = self::$_INSTANCE[$module_name] = new $module_name();
		}
		if (Module::$_log_modules_perfs) {
			$time_end = microtime(true);
			$memory_end = memory_get_usage(true);
			
			//存入資料庫
			$sql = '';
		}
		
		return $r;
	}
	
	public function getContextByName($module_name){
		return Module::LoadModule($module_name);
	}
	
	//掛勾
	/*	將套件掛载到掛勾上
	*	$hook	掛勾名稱
	*	$kit	套件名稱
	*/
	public function hook($hook, $modul=NULL){
		
	}
	
	//語言
	public function l($string, $specific = false){
		return Translate::getModuleTranslation($this, $string, $specific);
	}
	
	public static function getIdByName($module_name){
	
	}
	
	//取出模組Class	無視啟用
	public function getClass($Class=null){
		if(!empty($Class)){
			if(is_file(MODULE_DIR.DS.$Class.DS.$Class.'.php')){
				include_once(MODULE_DIR.DS.$Class.DS.$Class.'.php');
				return new $Class();
			}
			return false;
		}else{
			return $this;
		}
	}
}