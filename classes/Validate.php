<?php

class Validate
{
	public $_errors = [];    //錯誤訊息
	public $_errors_name = [];    //錯誤欄位
	public $_errors_name_i = [];    //錯誤欄位地幾個
	public $table;
	public $index;
	public $definition;

	public $excel_title_row;
	public $excel_index;
	public $excel_definition;
	public $excel_data;
	public $excel_this_row = 0;
	public $_excel_errors = [];        //excel錯誤訊息
	public $_excel_errors_txt = [];        //excel錯誤訊息(註解)
	public $_excel_errors_key = [];        //excel錯誤位置
	public $_excel_errors_num = 0;            //excel錯誤筆數


	protected static $instance = null;

	public $arr_excel_validate_key = ['val', 'name', 'prefix', 'suffix', 'values', 'table', 'options', 'required', 'minlength', 'maxlength', 'rangelength', 'min', 'max', 'range', 'unique', 'validate', 'isdatetime', 'istime', 'isdate', 'isemail', 'isEmail', 'isValidateImg'];

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Validate();
		};
		return self::$instance;
	}

	/*
	 * 2017/09/21
	 * 增加重副表單判別
	*/
	public function validateRules()
	{
		foreach ($this->definition as $key => $v) {
			$arr_post = [];
			if ($v['type'] != 'hidden') {
				$multiple_type = false;       //無複選
				//判別有複選
				if (strpos($v['name'], '[]') > -1) {
					$multiple_type = true;  //有複選
					$name          = str_replace('[]', '', $v['name']);
					$arr_post      = Tools::getValue($name);
				} else {
					$name = $v['name'];
					$post = Tools::getValue($v['name']);
				}
				$key = $name . '_' . $v['validate'];
				if (count($arr_post) == 0)
					$arr_post = [$post];

				//沒驗證過 驗證一次
				if (!Cache::get($key)) {
					foreach ($arr_post AS $i => $post) {
						switch ($v['validate']) {
							case 'required':    //必填欄位
								if (empty($post) && !($post === '0')) {
									if ($v['type'] == 'textarea' || $v['type'] == 'text' || $v['type'] == 'number' || $v['type'] == 'password' || $v['type'] == 'confirm-password') {
										$this->_errors[]      = sprintf($this->l('%s 必須填寫!'), $v['label']);
										$this->_errors_name[] = $v['name'];

										//有複選
										if ($multiple_type) {  //第幾個錯
											$this->_errors_name_i[$v['name']][] = $i;
										}
									} elseif ($v['type'] == 'file' && empty($_FILES[$v['name']]['tmp_name'])) {
										$this->_errors[]      = sprintf($this->l('%s 必須上傳!'), $v['label']);
										$this->_errors_name[] = $v['name'];

										//有複選
										if ($multiple_type) {  //第幾個錯
											$this->_errors_name_i[$v['name']][] = $i;
										}
									} elseif ($v['type'] != 'file') {
										$this->_errors[]      = sprintf($this->l('%s 必須選擇!'), $v['label']);
										$this->_errors_name[] = $v['name'];

										//有複選
										if ($multiple_type) {  //第幾個錯
											$this->_errors_name_i[$v['name']][] = $i;
										}
									};
								};
							break;
							case 'minlength':    //字串長度最小限制
								if (!(empty($post) && !($post === '0')) && $v['val'] < mb_strlen($post, 'utf-8')) {
									$this->_errors[]      = sprintf($this->l('%s 字串長度必須大於 %d!'), $v['label'], $v['val']);
									$this->_errors_name[] = $v['name'];

									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								};
							break;
							case 'maxlength':    //字串長度最大限制
								if (!(empty($post) && !($post === '0')) && $v['val'] < mb_strlen($post, 'utf-8')) {
									$this->_errors[]      = sprintf($this->l('%s 字串長度必須小於 %d!'), $v['label'], $v['val']);
									$this->_errors_name[] = $v['name'];

									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								};
							break;
							case 'rangelength':    //數字範圍 (min-max)
								if (strpos($v['val'], '~') > 0) {
									$arr_v = explode('~', $v['val']);
									$min   = (int)$arr_v[0];
									$max   = (int)$arr_v[1];
								} else {
									$arr_v = explode('-', $v['val']);
									$min   = (int)$arr_v[0];
									$max   = (int)$arr_v[1];
								};
								if (!(empty($post) && !($post === '0')) && (mb_strlen($post, 'utf-8') < $min || $max < mb_strlen($post, 'utf-8'))) {
									$this->_errors[]      = sprintf($this->l('%s 字串長度必須介於 %s ~ %s 之間!'), $v['label'], $min, $max);
									$this->_errors_name[] = $v['name'];

									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								};
							break;
							case 'min':        //數字最小長度限制
								if (!(empty($post) && !($post === '0')) && (($v['val'] > (float)$post))) {
									$this->_errors[]      = sprintf($this->l('%s 必須大於 %s!'), $v['label'], $v['val']);
									$this->_errors_name[] = $v['name'];

									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								};
							break;
							case 'max':        //數字最大長度限制
								if (!(empty($post) && !($post === '0')) && (($v['val'] < (float)$post))) {
									$this->_errors[]      = sprintf($this->l('%s 必須小於 %s!'), $v['label'], $v['val']);
									$this->_errors_name[] = $v['name'];

									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								};
							break;
//							case 'step':
//								if((!(empty($post) && !($post === '0')) && (($post % (float)post) > 0))){
//									$this->_errors[] = sprintf($this->l('%s 必須是 %s 的倍數!'), $v['label'], $v['val']);
//									$this->_errors_name[] = $v['name'];
//
//									//有複選
//									if($multiple_type){  //第幾個錯
//										$this->_errors_name_i[$v['name']][] = $i;
//									}
//								}
//								break;
							case 'range':    //數字範圍 (min-max)
								if (strpos($v['val'], '~') > 0) {
									$arr_v = explode('~', $v['val']);
									$min   = floatval((float)$arr_v[0]);
									$max   = floatval((float)$arr_v[1]);
								} else {
									$arr_v = explode('-', $v['val']);
									$min   = floatval((float)$arr_v[0]);
									$max   = floatval((float)$arr_v[1]);
								};
								if ((!(empty($post) && !($post === '0')) && ((float)$post < $min || $max < (float)$post)) || (!is_ints($post) && !empty($post))) {
									$this->_errors[]      = sprintf($this->l('%s 必須介於 %s ~ %s 之間!'), $v['label'], $min, $max);
									$this->_errors_name[] = $v['name'];

									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								};
							break;
							case 'change-password': //修改密碼
								$old_passwd = Tools::getValue('old_passwd');
								$name1      = Tools::getValue($v['name']);
								$name2      = Tools::getValue($v['name'] . '2');

//								if(!empty($old_passwd) || !empty($name1) || !empty($name2)){
								if (!empty($name1) || !empty($name2)) {
//									$old_passwd = Tools::getValue('old_passwd');
//									if(empty($old_passwd)){
//										$this->_errors[] = $this->l('當前密碼 必須填寫!');
//									}else{
//										$sql = 'SELECT `'.$this->index.'`
//									FROM `'.$this->table.'`
//									WHERE `'.$this->index.'` = \''.Tools::getValue($this->index).'\'
//										AND `'.substr($v['name'], strchr($v['name'], '!')).'` = \''.Tools::getValue('old_passwd').'\'
//									LIMIT 0, 1';
//										$db = new db_mysql($sql);
//										if($db->num() == 0){
//											$this->_errors[] = $this->l('當前密碼 錯誤!');
//											$this->_errors_name[] = 'old_passwd';
//										};
//									};

									if (empty($name1)) {
										$this->_errors[]      = $this->l('新密碼 必須填寫!');
										$this->_errors_name[] = $v['name'];
									};
									if (empty($name2)) {
										$this->_errors[]      = $this->l('確認密碼 必須填寫!');
										$this->_errors_name[] = $v['name'] . '2';
									};
									if ($name1 !== $name2) {
										$this->_errors[]      = $this->l('新密碼 與 確認密碼 必須一樣!');
										$this->_errors_name[] = $v['name'];
										$this->_errors_name[] = $v['name'] . '2';
									};
								};
							break;
							case 'confirm':
								if (Tools::getValue($v['name']) != Tools::getValue($v['val'])) {
									$this->_errors[]      = sprintf($this->l('%s 與 %s 必須一樣!'), $v['label'][0], $v['label'][1]);
									$this->_errors_name[] = $v['name'];
									$this->_errors_name[] = $v['val'];
								};
							break;
							case 'unique':
								if (Tools::getValue($v['name'])) {
									$num   = 0;
									$WHERE = '';
									$index = Tools::getValue($this->index);
									if ($index) {
										$WHERE = sprintf(' AND `' . $this->index . '` != %d',
											getSQL($index, 'int'));
									};
									$sql = sprintf('SELECT `' . $v['name'] . '`
										FROM `' . DB_PREFIX_ . $this->table . '`
										WHERE `' . $v['name'] . '` = %s' . $WHERE . '
										LIMIT 0, 1',
										getSQL(Tools::getValue($v['name']), 'text'));
									Db::rowSQL($sql);
									$num = Db::getContext()->num();
									if ($num) {
										$this->_errors[]      = sprintf($this->l('%s 不能重複!'), $v['label']);
										$this->_errors_name[] = $v['name'];
										//有複選
										if ($multiple_type) {  //第幾個錯
											$this->_errors_name_i[$v['name']][] = $i;
										}
									};
								};
							break;
							case 'isdatetime':
								if (!empty($post) && !isdatetime($post)) {
									$this->_errors[]      = sprintf($this->l('%s 格式必須為 YYYY-MM-DD hh:mm:ss 或 YYYY-MM-DD hh:mm (ex:2016:01:02 23:45:01)'), $v['label']);
									$this->_errors_name[] = $v['name'];
									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								}
							break;
							case 'isdate':
								if (!empty($post) && !isdate($post)) {
									$this->_errors[]      = sprintf($this->l('%s 格式必須為 hh:mm:ss 或 hh:mm (ex:23:45:01)'), $v['label']);
									$this->_errors_name[] = $v['name'];
									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								}
							break;
							case 'istime':
								if (!empty($post) && !istime($post)) {
									$this->_errors[]      = sprintf($this->l('%s 格式必須為 yyyy-mm-dd (ex:2016-06-01)'), $v['label']);
									$this->_errors_name[] = $v['name'];
									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								}
							break;
							case 'isemail':
							case 'isEmail':
								if (!empty($post) && !isEmail($post)) {
									$this->_errors[]      = sprintf($this->l('%s 格式必須為 e-mail'), $v['label']);
									$this->_errors_name[] = $v['name'];
									//有複選
									if ($multiple_type) {  //第幾個錯
										$this->_errors_name_i[$v['name']][] = $i;
									}
								}
							break;
							case 'validate':
								if (!empty($post)) {
									if (!is_array($v['val']))
										$v['val'] = [$v['val']];
									foreach ($v['val'] as $k => $val) {
										switch ($val) {
											case 'isdatetime':
												if (!isdatetime($post)) {
													$this->_errors[]      = sprintf($this->l('%s 格式必須為 YYYY-MM-DD hh:mm:ss 或 YYYY-MM-DD hh:mm (ex:2016:01:02 23:45:01)'), $v['label']);
													$this->_errors_name[] = $v['name'];
													//有複選
													if ($multiple_type) {  //第幾個錯
														$this->_errors_name_i[$v['name']][] = $i;
													}
												};
											break;
											case 'istime':
												if (!istime($post)) {
													$this->_errors[]      = sprintf($this->l('%s 格式必須為 hh:mm:ss 或 hh:mm (ex:23:45:01)'), $v['label']);
													$this->_errors_name[] = $v['name'];
													//有複選
													if ($multiple_type) {  //第幾個錯
														$this->_errors_name_i[$v['name']][] = $i;
													}
												};
											break;
											case 'isdate':
												if (!isdate($post)) {
													$this->_errors[]      = sprintf($this->l('%s 格式必須為 yyyy-mm-dd (ex:2016-06-01)'), $v['label']);
													$this->_errors_name[] = $v['name'];
													//有複選
													if ($multiple_type) {  //第幾個錯
														$this->_errors_name_i[$v['name']][] = $i;
													}
												};
											break;
											case 'isemail':
											case 'isEmail':
												if (!isEmail($post)) {
													$this->_errors[]      = sprintf($this->l('%s 格式必須為 e-mail'), $v['label']);
													$this->_errors_name[] = $v['name'];
													//有複選
													if ($multiple_type) {  //第幾個錯
														$this->_errors_name_i[$v['name']][] = $i;
													}
												};
											break;
											case 'isValidateImg':
												if ($_SESSION['verification2'] != Tools::getValue('verification')) {
													$this->_errors[]      = $this->l('圖形驗證碼輸入錯誤!');
													$this->_errors_name[] = $v['name'];
													//有複選
													if ($multiple_type) {  //第幾個錯
														$this->_errors_name_i[$v['name']][] = $i;
													}
												};
											break;
										};
									};
								};
							break;
						}
					}
				}

				//已驗證
				Cache::set($key, true);
			};
		};
		if (in_array('id_member', $this->_errors_name))
			$this->_errors_name[] = 'member_search';
		$this->_errors_name = array_unique($this->_errors_name);
	}

	//2018-07-05 excel 匯入驗證, 還未加入"不重複"
	public function validateExcelRules()
	{
		$err_n = 0;
		foreach ($this->excel_definition as $i => $v) {
			$value = trim($this->excel_data[$v['key']]);
			$key   = $v['key'] . $this->excel_this_row;
			switch ($v['validate']) {
				case 'required':    //必填欄位
					if (empty($value) && ($value !== '0')) {
						$error_txt                     = sprintf($this->l('[%s] 必須填寫!'), $key);
						$this->_excel_errors[]         = $error_txt;
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
						$err_n++;
					};
				break;
				case 'values':    //選單
					if (!empty($value)) {
						if (!isset($v['val'][$value])) {
							$error_txt                     = sprintf($this->l('[%s] 必須是選項清單值相符!'), $key);
							$this->_excel_errors[]         = $error_txt;
							$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
							$this->_excel_errors_key[]     = $key;
							$err_n++;
						} else {
							$this->excel_data[$v['key']] = $v['val'][$value];
						}
					} else {
						$this->excel_data[$v['key']] = $v['default'];
					}
				break;
				case 'minlength':    //字串長度最小限制
					if (!(empty($value) && ($value !== '0')) && $v['val'] < mb_strlen($value, 'utf-8')) {
						$error_txt                     = sprintf($this->l('[%s] 字串長度必須大於 %d!'), $key, $v['val']);
						$this->_excel_errors[]         = $error_txt;
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
						$err_n++;
					};
				break;
				case 'maxlength':    //字串長度最大限制
					if (!(empty($value) && ($value !== '0')) && $v['val'] < mb_strlen($value, 'utf-8')) {
						$error_txt                     = sprintf($this->l('[%s] 字串長度必須小於 %d!'), $key, $v['val']);
						$this->_excel_errors[]         = $error_txt;
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
						$err_n++;
					};
				break;
				case 'rangelength':    //數字範圍 (min-max)
					if (strpos($v['val'], '~') > 0) {
						$arr_v = explode('~', $v['val']);
						$min   = (int)$arr_v[0];
						$max   = (int)$arr_v[1];
					} else {
						$arr_v = explode('-', $v['val']);
						$min   = (int)$arr_v[0];
						$max   = (int)$arr_v[1];
					};
					if (!(empty($value) && ($value !== '0')) && (mb_strlen($value, 'utf-8') < $min || $max < mb_strlen($value, 'utf-8'))) {
						$error_txt                     = sprintf($this->l('[%s] 字串長度必須介於 %s ~ %s 之間!'), $key, $min, $max);
						$this->_excel_errors[]         = $error_txt;
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
						$err_n++;
					};
				break;
				case 'min':        //數字最小長度限制
					if (!(empty($value) && ($value !== '0')) && (($v['val'] > (float)$value) || (!empty($value) && !is_ints($value)))) {
						$error_txt                     = sprintf($this->l('[%s] 必須大於 %s!'), $key, $v['val']);
						$this->_excel_errors[]         = $error_txt;
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
						$err_n++;
					};
				break;
				case 'max':        //數字最大長度限制
					if (!(empty($value) && ($value !== '0')) && (($v['val'] < (float)$value) || (!empty($value) && !is_ints($value)))) {
						$error_txt                     = sprintf($this->l('[%s] 必須小於 %s!'), $key, $v['val']);
						$this->_excel_errors[]         = $error_txt;
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
						$err_n++;
					};
				break;
				case 'range':    //數字範圍 (min-max)
					if (strpos($v['val'], '~') > 0) {
						$arr_v = explode('~', $v['val']);
						$min   = floatval((float)$arr_v[0]);
						$max   = floatval((float)$arr_v[1]);
					} else {
						$arr_v = explode('-', $v['val']);
						$min   = floatval((float)$arr_v[0]);
						$max   = floatval((float)$arr_v[1]);
					};
					if ((!(empty($value) && ($value !== '0')) && ((float)$value < $min || $max < (float)$value)) || (!is_ints($value) && !empty($value))) {
						$error_txt                     = sprintf($this->l('[%s] 必須介於 %s ~ %s 之間!'), $key, $min, $max);
						$this->_excel_errors[]         = $error_txt;
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
						$err_n++;
					};
				break;
				case 'isdatetime':
					if (!empty($post) && !isdatetime($post)) {
						$error_txt                     = sprintf('%s 格式必須為 YYYY-MM-DD hh:mm:ss 或 YYYY-MM-DD hh:mm (ex:2016:01:02 23:45:01)', $key);
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
					}
				break;
				case 'isdate':
					if (!empty($post) && !isdate($post)) {
						$error_txt                     = sprintf($this->l('%s 格式必須為 hh:mm:ss 或 hh:mm (ex:23:45:01)'), $key);
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
					}
				break;
				case 'istime':
					if (!empty($post) && !istime($post)) {
						$error_txt                     = sprintf($this->l('%s 格式必須為 yyyy-mm-dd (ex:2016-06-01)'), $key);
						$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
						$this->_excel_errors_key[]     = $key;
					}
				break;
				case 'validate':
					if (!empty($value)) {
						if (!is_array($v['val']))
							$v['val'] = [$v['val']];
						foreach ($v['val'] as $k => $val) {
							switch ($val) {
								case 'isdatetime':
									if (!isdatetime($value)) {
										$error_txt                     = sprintf($this->l('[%s] 格式必須為 YYYY-MM-DD hh:mm:ss 或 YYYY-MM-DD hh:mm (ex:2016:01:02 23:45:01)'), $key);
										$this->_excel_errors[]         = $error_txt;
										$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
										$this->_excel_errors_key[]     = $key;
										$err_n++;
									};
								break;
								case 'istime':
									if (!istime($value)) {
										$error_txt                     = sprintf($this->l('[%s] 格式必須為 hh:mm:ss 或 hh:mm (ex:23:45:01)'), $key);
										$this->_excel_errors[]         = $error_txt;
										$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
										$this->_excel_errors_key[]     = $key;
										$err_n++;
									};
								break;
								case 'isdate':
									if (!isdate($value)) {
										$error_txt                     = sprintf($this->l('[%s] 格式必須為 yyyy-mm-dd (ex:2016-06-01)'), $key);
										$this->_excel_errors[]         = $error_txt;
										$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
										$this->_excel_errors_key[]     = $key;
										$err_n++;
									};
								break;
								case 'isemail':
								case 'isEmail':
									if (!isEmail($value)) {
										$error_txt                     = sprintf($this->l('[%s] 格式必須為 e-mail'), $key);
										$this->_excel_errors[]         = $error_txt;
										$this->_excel_errors_txt[$key] .= "\r\n" . $error_txt;
										$this->_excel_errors_key[]     = $key;
										$err_n++;
									};
								break;
							};
						};
					};
				break;
			}
		}
		$this->_errors_name = array_unique($this->_excel_errors_key);
	}

	public function getImg()
	{
		ValidateImg::getContext()->getImg();
	}

	public function checkImg()
	{

	}

	public static function isLoadedObject($object)
	{
		return is_object($object) && $object->id;
	}

	public function l($str)
	{
		return $str;
	}
}