<?php

class Competence
{
	protected static $instance = null;        //是否實體化
	protected static $db_competence = null;
	protected static $db_competence_by_id_group = null;
	public $competence_list = null;
	public $admin_competence_list = null;
	public $front_competence_list = null;
	public $type = ['view', 'add', 'edit', 'del'];

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Competence();
		};
		return self::$instance;
	}

	public function getAccess($id_tab = null, $id_group = null)
	{
		if (empty($id_tab))
			return false;            //沒有選單 = 沒有開放權限
		if (empty($id_group))
			$id_group = $_SESSION['id_group'];
		if ($_SESSION['id_group'] == 1) {
			if ($_SESSION['id_admin'] == 1408) {
				return [
					'id_group' => 1,
					'id_tab'   => $id_tab,
					'view'     => 1,
					'add'      => 0,
					'edit'     => 0,
					'del'      => 0,
				];
			}
			return [
				'id_group' => 1,
				'id_tab'   => $id_tab,
				'view'     => 1,
				'add'      => 1,
				'edit'     => 1,
				'del'      => 1,
			];
		};
		$arr_competence = Competence::getContext()->getCompetence();
		foreach ($arr_competence as $i => $tab) {
			if ($tab['id_group'] == $id_group && $tab['id_tab'] == $id_tab) {
				return $tab;
				break;
			};
		};
		return false;
	}

	public function getAdminCompetenceList()
	{
		Competence::getContext()->getCompetenceList();
		return Competence::getContext()->admin_competence_list;
	}

	/*
	 * 列出所有控制項
	 */
	public function getCompetenceList()
	{
		if (empty(Competence::getContext()->competence_list)) {
			$arr_type = ['admin', 'front'];
			foreach ($arr_type as $j => $type) {
				$dir2 = CONTROLLERS_DIR . DS . $type . DS . 'controller';
				if (is_dir($dir2)) {
					if ($dh = opendir($dir2)) {
						while (($file = readdir($dh)) !== false) {
							//只讀過取出php 的檔案
							if ($file != '.' && $file != '..') {
								$str_n = strpos($file, 'Controller.php');
								if ($str_n) {      //是檔案
									$competence = substr($file, 0, $str_n);
									if ($type == 'admin')
										Competence::getContext()->admin_competence_list[$competence] = '';    //將ControllerClass 存入KEY(去重複)
									if ($type == 'front')
										Competence::getContext()->front_competence_list[$competence] = '';    //將ControllerClass 存入KEY(去重複)
									Competence::getContext()->competence_list[$competence] = '';                    //將ControllerClass 存入KEY(去重複)
								}
							}
						}
					}
				}
			}
			Competence::getContext()->admin_competence_list = array_keys(Competence::getContext()->admin_competence_list);
			Competence::getContext()->front_competence_list = array_keys(Competence::getContext()->front_competence_list);
			Competence::getContext()->competence_list       = array_keys(Competence::getContext()->competence_list);
		};
		return Competence::getContext()->competence_list;
	}

	/**
	 * 取得所有權限
	 * @return array|null
	 */
	public function getCompetence()
	{
		if (empty(self::$db_competence)) {
			$sql                 = 'SELECT `id_group`, `id_tab`, `view`, `add`, `edit`, `del`
				FROM `' . DB_PREFIX_ . 'competence`';
			$arr_row             = Db::rowSQL($sql);
			self::$db_competence = $arr_row;
		};
		return self::$db_competence;
	}

	//權限代碼
	public function competences_code($type)
	{
		switch ($type) {
			case 'view':
				return Competence::getContext()->l('觀看');
			break;
			case 'add':
				return Competence::getContext()->l('新增');
			break;
			case 'edit':
				return Competence::getContext()->l('修改');
			break;
			case 'del':
				return Competence::getContext()->l('刪除');
			break;
		};
	}

	//語言
	public function l($string, $specific = false)
	{
		return $string;
	}
}