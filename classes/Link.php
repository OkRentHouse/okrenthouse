<?php
class Link{
	public function __construct(){
		
	}
	
	//新增鏈結
	public static function add_link($title,$url){
		$sql = sprintf('INSERT INTO `link` (`url`) VALUES (%s)',
					GetSQL($url,'text'));
		$INSERT_link = new db_mysql($sql);
		$id_link = $INSERT_link->num();
		Link::add_title($id_link,$title);
		return ($id_link) ? $id_link : false;
	}
	
	//修改鏈結
	public static function update_link($id_link,$title,$url){
		$sql = sprintf('UPDATE `link` SET  `url` = %s
					WHERE `id_link` = %d',
					GetSQL($url,'text'),
					GetSQL($id_link,'int'));
		$db_link = new db_mysql($sql);
		$num = $db_link->num();
		Link::update_link_title($id_link,$title);
		return ($num) ? $num : false;
	}
	
	//修改鏈結標題	
	public static function update_link_title($id_link,$title,$id_language=NULL){
		if(empty($id_language)){
			$Language = new Language();
			$id_language = $Language->id_by_code();
		};
		$sql = sprintf('UPDATE `link_title` SET  `title` = %s
					WHERE `id_link` = %d',
					GetSQL($title,'text'),
					GetSQL($id_link,'int'));
		$db_link = new db_mysql($sql);
		$num = $db_link->num();
		return ($num) ? $num : false;
	}
	
	//刪除鏈結
	public static function del_link($id_link){
		if(!is_array($id_link)) $id_link = array($id_link);
		if(count($id_link) > 0){
			$del = implode(',',$id_link);
			$sql = 'DELETE FROM `link` WHERE `id_link` IN ('.$del.')';
			$DELETE_news_link = new db_mysql($sql);
		};
	}
	
	public static function add_title($id_link,$title,$id_language=NULL){
		if(empty($id_language)){
			$Language = new Language();
			$id_language = $Language->id_by_code();
		};
		$sql = sprintf('INSERT INTO `link_title` (`id_link`, `id_language`, `title`) VALUES (%d, %d, %s)',
					GetSQL($id_link,'int'),
					GetSQL($id_language,'int'),
					GetSQL($title,'text'));
		$INSERT_link_title = new db_mysql($sql);
	}
	
	public static function get_link($id_link,$id_language=NULL){
		$arr_id_link = array();
		$arr_link = array();
		if(empty($id_language)){
			$Language = new Language();
			$id_language = $Language->id_by_code();
		};
		if(!is_array($id_link)){
			if(empty($id_link)) return array($arr_id_link,$arr_link);
			$WHERE = '= '.$id_link;
			$LIMIT = 'LIMIT 0, 1';
		}else{
			if(count($id_link) == 0) return array($arr_id_link,$arr_link);
			$WHERE = 'IN ('.implode(',',$id_link).')';
		};
		$sql = 'SELECT l.`id_link`, l.`url`, lt.`title`
			FROM `link` AS l
			LEFT JOIN `link_title` AS lt ON lt.`id_link` = l.`id_link`
			WHERE l.`id_link` '.$WHERE.'
			AND lt.`id_language` = '.$id_language.'
			'.$LIMIT;
		$SELECT_link = new db_mysql($sql);
		$num = $SELECT_link->num();
		if($num > 0){
			while($link = $SELECT_link->next_row()){
				$arr_link[] = $link;
			};
		};
		return array($num,$arr_link);
	}
	
	//取得模組鏈結
	public function getModuleLink($module, $controller = 'default')
	{
		
	}
	
	//取得圖片鏈結
	public function getImageLink($name, $ids, $type = null)
	{
		
	}
	
	//取得媒體鏈結		Js、CSS
	public function getMediaLink($filepath)
	{
		
	}
	
	//取得網頁鏈結
	public function getPageLink($controller)
	{
		
	}
}