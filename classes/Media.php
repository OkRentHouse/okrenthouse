<?php
class Media {
	protected static $js_def = array();
	
	//
	public function getCSSPath($css_uri)
	{
		return Media::getMediaPath($css_uri,'css');
	}
	
	public static function getJSPath($js_uri)
	{
		return Media::getMediaPath($js_uri,'js');
	}
	
	public function getMediaPath($media_uri, $css_media_type)
	{
		return array($media_uri => $css_media_type);
	}
	
	public static function getJsDef()
	{
		ksort(Media::$js_def);
		return Media::$js_def;
	}
	
	public static function getJqueryPath($version = NULL, $js_dir = NULL, $minifier = true)
	{
		$add_no_conflict = false;	//加入版本有無衝突
		if ($version === NULL) {
			$version = JQUERY_VERSION;		//預設版本
		}elseif (preg_match('/^([0-9\.]+)$/Ui', $version)) {
			$add_no_conflict = true;
		}else{
			return false;
		};
		
		if ($js_dir === NULL) {		//是否有js資料夾
			$js_dir = JS_DIR.DS.'jquery/';
		}; 
		
		$file = $js_dir.'jquery-'.$version.($minifier ? '.min.js' : '.js');

		$url_data = parse_url($file);
		$file_uri = lin_win_dir(WEB_DIR.$url_data['path']);
		$return = array();
		
		if(@filemtime($file_uri) || defined('WEB_DNS')) {	//伺服器有檔案就啟用伺服器路徑
			$return[] = Media::getJSPath($file);
		}else{								//沒有就啟用google路徑
			$return[] = Media::getJSPath(Tools::getCurrentUrlProtocolPrefix().'ajax.googleapis.com/ajax/libs/jquery/'.$version.'/jquery'.($minifier ? '.min.js' : '.js'));
		};
		
		return $return;
	}
}