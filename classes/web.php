<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2019/4/16
 * Time: 上午 09:33
 */

class Web
{
	protected static $instance;
	public $_errors = array();		//錯誤訊息
	public $_errors_name = array();	//錯誤欄位
	public $_errors_name_i = array();	//錯誤欄位地幾個
	public $_msgs = array();
	public $msg = '';
	
	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Web();
		}

		return self::$instance;
	}

	public static function getThisWebId(){
		$arr_web = Web::get();
		foreach($arr_web as $i => $web){
			if (strpos($_SERVER['HTTP_HOST'], $web['domain_name']) > -1) {
				return $web['id_web'];
				break;
			}
		}
	}

	public static function get($id_web=null){
		if(!empty($id_web)){
			$WHERE = ' AND `id_web` = '.$id_web;
		}else{
			$WHERE = '';
		}

		$sql = 'SELECT *
			FROM `web2` WHERE (`dir` = "web_app" OR `dir` = "web_life_house" OR `dir` = "web_rent")'.$WHERE.'
			ORDER BY `position` ASC';	//todo
		return Db::rowSQL($sql);
	}

	/**
	 * 網站是否啟用
	 * @param $id_web
	 *
	 * @return mixed|null
	 */
	public static function is_active($id_web){
		if(Cache::is_set('web_is_active_'.$id_web)){
			return Cache::get('web_is_active_'.$id_web);
		}else{
			$sql = sprintf('SELECT `active`
					FROM `'.DB_PREFIX_.'web2`
					WHERE `id_web` = %d
					LIMIT 0, 1',
				GetSQL($id_web,'int'));
			$row = Db::rowSQL($sql, true);
			Cache::set('web_is_active_'.$id_web, $row['active']);
			return $row['active'];
		}
	}
}