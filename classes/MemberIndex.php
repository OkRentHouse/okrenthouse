<?php
class MemberIndex{
	public static function add($id_member, $page){
		MemberIndex::del($id_member);
		$sql = sprintf('INSERT INTO `member_index_page` (`id_member`, `page`) VALUES (%d, %s)',
			GetSQL($id_member, 'int'),
			GetSQL($page, 'text'));
		Db::rowSQL($sql);
	}

	public static function del($id_member){
		$sql = 'DELETE FROM `member_index_page` WHERE `id_member` = '.$id_member;
		Db::rowSQL($sql);
	}
}