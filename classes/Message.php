<?php
class Message
{
	protected static $instance;

	public static function getContext(){
		if (!self::$instance) {
			self::$instance = new Message();
		}
		return self::$instance;
	}

	public function setMessage($title, $message, $id_admin=NULL, $id_member=NULL, $id_reply=NULL){
		$sql = sprintf('INSERT INTO `message` (`title`, `message`, `id_admin`, `id_member`, `id_reply`) VALUES (%s, %s, %d, %d, %d)',
				GetSQL($title, 'text'),
				GetSQL($message, 'text'),
				GetSQL($id_admin, 'int'),
				GetSQL($id_member, 'int'),
				GetSQL($id_reply, 'int'));
		Db::rowSQL($sql, false);
		return Db::getContext()->num();
	}

	public static function getMessageByid_message($id_message){
		$sql = sprintf('SELECT `id_message`, `title`, `message`, `id_admin`, `id_member`, `id_reply`, `time`
				FROM `message`
				WHERE `id_message` = %d
				LIMIT 0, 1',
				GetSQL($id_message, 'int')
			);
		return Db::rowSQL($sql, true);
	}

	public static function getMessageByid_admin($id_admin){
		$sql = sprintf('SELECT `id_message`, `title`, `message`, `id_admin`, `id_member`, `id_reply`, `time`, IF(`id_message` = 0, `id_reply`, `id_message`) AS reply
				FROM `message`
				WHERE `id_admin` = %d
				ORDER BY reply ASC, `time`',
			GetSQL($id_admin, 'int')
		);
		return Db::rowSQL($sql, false);
	}

	public static function getMessageByid_member($id_member){
		$sql = sprintf('SELECT `id_message`, `title`, `message`, `id_admin`, `id_member`, `id_reply`, `time`, IF(`id_message` = 0, `id_reply`, `id_message`) AS reply
				FROM `message`
				WHERE `id_admin` = %d
				ORDER BY reply ASC, `time`',
			GetSQL($id_member, 'int')
		);
		return Db::rowSQL($sql, false);
	}

	/**
	 * 系統管理者寫入訊息
	 * @param $id_message
	 * @param null $id_admin
	 * @return int
	 */
	public static function set_admin_read($id_message, $id_admin=NULL){
		if(empty($id_admin)) $id_admin = $_SESSION['id_admin'];
		$sql = sprintf('INSERT INTO `message__admin_read` (`id_message`, `id_admin`) VALUES (%d, %d)',
			GetSQL($id_message, 'int'),
			$id_admin);
		Db::rowSQL($sql, false);
		return Db::getContext()->num();
	}

	/**
	 * 會員寫入訊息
	 * @param $id_message
	 * @param null $id_member
	 * @return int
	 */
	public static function set_member_read($id_message, $id_member=NULL){
		if(empty($id_member)) $id_member = $_SESSION['id_member'];
		$sql = sprintf('INSERT INTO `message__member_read` (`id_message`, `id_member`) VALUES (%d, %d)',
			GetSQL($id_message, 'int'),
			$id_member);
		Db::rowSQL($sql, false);
		return Db::getContext()->num();
	}

	//todo
	public static function getAllMemberNum(){

	}

	/**
	 * 取得會員訊息數
	 * @param null $id_member
	 * @return int|null
	 */
	public static function getMemberNum($id_member=NULL){
		if(empty($id_member)) $id_member = $_SESSION['id_member'];
		if(!empty($id_member)){
			$sql = sprintf('SELECT `id_message`
				FROM `message__member_read`
				WHERE `id_member` = %d',
				GetSQL($id_member, 'int'));
			$msg_row = Db::rowSQL($sql, false);
			$arr_id_message = array(0);
			foreach($msg_row AS $i => $v){
				$arr_id_message[] = $v['id_message'];
			}

			$sql = sprintf('SELECT msg.`id_message`
				FROM `'.DB_PREFIX_.'member` AS m
				LEFT JOIN `houses` AS h ON h.`id_houses` = m.`id_houses`
				LEFT JOIN `service` AS s ON s.`house_code` = h.`house_code`
				LEFT JOIN `service_message` AS sm ON sm.`id_service` = s.`id_service`
				LEFT JOIN `message` AS msg ON msg.`id_message` = sm.`id_message`
				WHERE m.`id_member` = %d
				AND msg.`id_member` != %d
				AND msg.`id_message` NOT IN (%s)',
				GetSQL($id_member, 'int'),
				GetSQL($id_member, 'int'),
				implode(',', $arr_id_message), 'text');
			$row = Db::rowSQL($sql, false);
			return count($row);
		}else{
			return null;
		}
	}

	/**
	 * 取得系統管理者已讀訊息ID
	 * @param $id_admin
	 * @return array
	 */
	public static function getReadByid_admin($id_admin){
		$sql = sprintf('SELECT `id_message`
				FROM `message__admin_read`
				WHERE `id_admin` = %d',
			GetSQL($id_admin, 'int')
		);
		return Db::rowSQL($sql, false);
	}

	/**
	 * 取得會員已讀訊息ID
	 * @param $id_member
	 * @return array
	 */
	public static function getReadByid_member($id_member){
		$sql = sprintf('SELECT `id_message`
				FROM `message__admin_read`
				WHERE `id_member` = %d',
			GetSQL($id_member, 'int')
		);
		return Db::rowSQL($sql, false);
	}
}
