<?php

class Member
{
	protected static $instance;
	public $_errors = [];        //錯誤訊息
	public $_msgs = [];

	public $arr_my_id_web = [0];
	public $arr_my_id_houses = [];

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Member();
		}
		return self::$instance;
	}

	public function competence()
	{
		$competences = [
			['member' => '會員', 'view' => '觀看', 'add' => '新增', 'update' => '修改', 'del' => '刪除'],
		];
		return $competences;
	}

	public function  validateRules($data){

        $user    = (substr($data['user'],0,1)=='0')?substr($data['user'],1):$data['user'];
        $international_code = $data['international_code'];
        $user    = $international_code.$data['user'];
        $password = $data['password'];
        $check_password = $data['check_password'];
        $name = $data['name'];
        $nickname = $data['nickname'];
        $gender = $data['gender'];
        $agree = $data['agree'];
        $captcha = $data['captcha'];

        $sql = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($user, 'text');
        $row    = Db::rowSQL($sql, true);
        $captcha_log = $row['captcha']; //驗證碼
        if (empty($agree)) {
            $this->_errors[] = '您必須同意[服務條款]與[隱私權政策]';
        }
        if(strlen($name) < '2' || strlen($name) > '20'){
            $this->_errors[] = '未輸入姓名或是姓名長度不符';
        }
        if(strlen($nickname) < '2' || strlen($nickname) > '20'){
            $this->_errors[] = '未輸入暱稱或是暱稱長度不符';
        }
        if($gender !='0' && $gender !='1'){
            $this->_errors[] = '請選擇性別';
        }
        if(empty($user)){
            $this->_errors[] = '您必須填寫帳號';
        }else if(strlen($user)<'6' || strlen($user)>20){
            $this->_errors[] = '您的帳號不符合長度';
        }else if(empty($international_code)){
            $this->_errors[] = '未選擇國碼';
        }
        if (empty($captcha)) {
            $this->_errors[] = '您必須輸入驗證碼';
        }else if($captcha != $captcha_log){
            $this->_errors[] = '輸入驗證碼錯誤';
        }
        if(empty($password)){
            $this->_errors[] = '您必須填寫密碼';
        }else if(strlen($password)<'6' || strlen($password)>20){
            $this->_errors[] = '您的密碼不符合長度';
        }
        if(empty($check_password)){
            $this->_errors[] = '您必須填寫密碼';
        }else if($check_password != $password){
            $this->_errors[] = '再次輸入密碼必須與密碼相同';
        }

    }


    public static function login_work($id_member_additional=1){
        $id_member = $_SESSION['id_member'];
        //判斷是否存在不存在回傳false 存在回傳true
        if(empty($id_member)){
            return 0;
        }
        if(explode($id_member_additional,$_SESSION['id_member_additional'])>1) {
            return 1;
        }else{
            return 0;
        }
    }


	//取得會員資料
	public static function get_db_member($nMax = 20, $id_member = null)
	{
		$sql        = 'SELECT * FROM `member`';
		$db_member  = new db_mysql($sql, $nMax);
		$arr_member = [];
		$num        = $db_member->num();
		if ($num > 0) {
			while ($member = $db_member->next_row()) {
				$arr_member[] = $member;
			}
		}
		return [$num, $arr_member];
	}

	public static function chk_member_group_name($member_group, $type = true)
	{
		$WHERE = '';
		if (!empty($_POST['id_member_group']))
			$WHERE .= ' AND `id_member_group` != ' . $_POST['id_member_group'];
		$sql                 = 'SELECT `id_member_group`
			FROM `member_group`
			WHERE `member_group` = "' . $member_group . '"' . $WHERE . '
			LIMIT 0, 1';
		$SELECT_member_group = new db_mysql($sql);
		$num                 = $SELECT_member_group->num();
		return ($num == 0);
	}

	public function check_post()
	{
		switch ($_POST['action']) {
			case 'update_member_group':
				if (empty($_POST['id_member_group']))
					$this->_errors[] = '資料錯誤!找不到資料!';
			case 'add_member_group':
				if (empty($_POST['member_group']))
					$this->_errors[] = '請輸入會員群組';
				if (!Member::chk_member_group_name($_POST['member_group']))
					$this->_errors[] = '會員群組名稱重複';;
				break;
			case 'del_member_group':
				if (!isint($_POST['id_member_group']) || empty($_POST['id_member_group']))
					$this->_errors[] = '請選擇刪除的群組';
				break;
		}
	}

	//取得會員資料
	public static function get_member($id_member)
	{
		$sql       = 'SELECT a.`id_member`, a.`user`, a.`pass`, mig.`id_member_group`, mg.`member_group`, a.`active`, i.`name`, i.`tel`, i.`phone`, i.`gender`, i.`birthday`, i.`address`
			FROM `member` AS a
			LEFT JOIN `information` AS i ON i.`id_member` = a.`id_member`
			LEFT JOIN `member_in_group` AS mig ON mig.`id_member` = a.`id_member`
			LEFT JOIN `member_group` AS mg ON mg.`id_member_group` = mig.`id_member_group`
			WHERE a.`id_member` = ' . $id_member . '
			GROUP BY `id_member`
			LIMIT 0, 1';
		$db_member = new db_mysql($sql);
		if ($db_member->num() > 0) {
			return $db_member->row();
		}
		return [];
	}

	//啟用帳號/關密帳號
	public static function member_on($user, $active)
	{
		$sql           = 'UPDATE `member` SET active = ' . $active . '
				WHERE `user` = "' . $user . '"';
		$UPDATE_member = new db_mysql($sql);
	}

	//新增工程進度帳號
	public static function add_db_member2($user, $pass, $active = 0)
	{
		$sql           = sprintf('INSERT INTO `member` (`user`, `pass`, `active`) VALUES (%s ,%s, %d)',
			GetSQL($user, 'text'),
			GetSQL($pass, 'text'),
			GetSQL($active, 'int'));
		$INSERT_member = new db_mysql($sql);
		return $INSERT_member->num();
	}

	public function get($id_member)
	{
		$m   = [];
		$sql = 'SELECT *
			FROM `member`
			WHERE `id_member` = ' . $id_member . '
			LIMIT 0, 1';
		$db  = new db_mysql($sql);
		if ($db->num() > 0) {
			$m = $db->row();
		}
		return $m;
	}

	//修改會員資料
	public static function update_db_member($id_member, $pass, $active)
	{
		if (!empty($pass))
			$SET = ', `pass` = "' . $pass . '"';
		$sql       = sprintf('UPDATE `member` SET `active` = %d' . $SET . '
					WHERE `id_member` = %d',
			GetSQL($active, 'int'),
			GetSQL($id_member, 'int'));
		$db_member = new db_mysql($sql);
		return ($db_member->num()) ? true : false;
	}

	//設定會員群組
	public static function add_member_in_group($id_member, $id_member_group)
	{
		$sql                    = sprintf('INSERT INTO `member_in_group` (`id_member`, `id_member_group`) VALUES (%d, %d)',
			GetSQL($id_member, 'int'),
			GetSQL($id_member_group, 'int'));
		$INSERT_member_in_group = new db_mysql($sql);
		return $INSERT_member_in_group->num();
	}

	//修改會員群組
	public static function update_member_in_group($id_member, $id_member_group)
	{
		$sql                    = sprintf('UPDATE `member_in_group` SET `id_member_group` = %d
					WHERE `id_member` = %d',
			GetSQL($id_member_group, 'int'),
			GetSQL($id_member, 'int'));
		$UPDATE_member_in_group = new db_mysql($sql);
		return $UPDATE_member_in_group->num();
	}

	public static function del_member_in_group($id_member)
	{
		$sql                = sprintf('DELETE FROM `member_in_group` WHERE `id_member` = %d',
			GetSQL($id_member, 'int'));
		$db_member_in_group = new db_mysql($sql);
		return ($db_member_in_group->num()) ? true : false;
	}

	//新增會員全資料
	public static function add_db_member($user, $pass, $active, $id_member_group, $name, $tel, $phone, $gender, $birthday, $address)
	{
		$id_member = Member::add_db_member2($user, $pass, $active);
		$Member    = new Member;
		$Member->add_member_in_group($id_member, $id_member_group);
		$Member->add_db_information($id_member, $name, $tel, $phone, $gender, $birthday, $address);
		return $id_member;
	}

	//修改會員全資料
	public static function update_member($id_member, $pass, $active, $id_member_group, $name, $tel, $phone, $gender, $birthday, $address)
	{
		Member::update_db_member($id_member, $pass, $active);
		Member::update_member_in_group($id_member, $id_member_group);
		Member::update_db_information($id_member, $name, $tel, $phone, $gender, $birthday, $address);
	}

	//新增會員資料
	public function add_db_information($id_member, $name, $tel, $phone, $gender, $birthday, $address)
	{
		$sql                = sprintf('INSERT INTO `information` (`id_member`, `name`, `tel`, `phone`, `gender`, `birthday`, `address`) VALUES (%d, %s, %s, %s, %d, %s, %s)',
			GetSQL($id_member, 'int'),
			GetSQL($name, 'text'),
			GetSQL($tel, 'text'),
			GetSQL($phone, 'text'),
			GetSQL($gender, 'int'),
			GetSQL($birthday, 'text'),
			GetSQL($address, 'text'));
		$INSERT_information = new db_mysql($sql);
		return $INSERT_information->num();
	}

	//判別是否驗證過
	public static function is_aging($user)
	{
		$sql               = 'SELECT `id_member_app` FROM `member_app`
			WHERE `user` = "' . $user . '"
			AND `approve` = 1
			LIMIT 0, 1';
		$SELECT_member_app = new db_mysql($sql);
		return $SELECT_member_app->num();
	}

	//判別是否有註冊
	public static function is_member($user, $id_member = null)
	{
		$WHERE = '';
		if (!empty($id_member)) {
			$WHERE = ' AND `in_member` != ' . $id_member;
		}
		$sql           = 'SELECT `id_member` FROM `member`
			WHERE `user` = "' . $user . '"' . $WHERE . '
			LIMIT 0, 1';
		$SELECT_member = new db_mysql($sql);
		return $SELECT_member->num();
	}

	//比對驗證碼
	public static function ch_member_app($security, $user)
	{
		if (!Member::is_aging($user)) {    //沒認證過
			$sql               = 'SELECT `id_member_app`, `time`, `aging`
				FROM `member_app`
				WHERE `user` = "' . $user . '"
				AND `approve` = 0
				ORDER BY `id_member_app` DESC
				LIMIT 0, 1';
			$SELECT_member_app = new db_mysql($sql);
			if ($SELECT_member_app->num() > 0) {
				$member_app    = $SELECT_member_app->row();
				$id_member_app = $member_app['id_member_app'];
				$time          = $member_app['time'];
				$aging         = $member_app['aging'];
				$security2     = crc32(md5(sha1($id_member_app . $user . $time . $aging)));
				if ($security == $security2) {
					$sql               = 'UPDATE `member_app`
						SET `approve` = 1
						WHERE `id_member_app` = ' . $id_member_app;
					$UPDATE_member_app = new db_mysql($sql);
					return true;
				}
			}
			return false;
		}
		include(WEB_DIR . DS . '404.php');    //驗證過就不用驗證	(沒有驗證)
		exit;
	}

	//取得驗證碼
	public static function get_member_app($user, $time, $aging)
	{
		$security = '';
		if (!Member::is_aging($user)) {    //判別是否有註冊過 (註冊過就不能有驗證碼,因被鎖帳號)
			$id_member_app = Member::add_member_app($user, $time, $aging);
			$security      = crc32(md5(sha1($id_member_app . $user . $time . $aging)));
		}
		return $security;
	}

	//新增驗證碼
	public static function add_member_app($user, $time, $aging, $approve = 0)
	{
		$sql               = sprintf('INSERT INTO `member_app` (`user`,`time`, `aging`, `approve`) VALUES (%s, %s, %d, %d)',
			GetSQL($user, 'text'),
			GetSQL($time, 'text'),
			GetSQL($aging, 'int'),
			GetSQL($approve, 'int'));
		$INSERT_member_app = new db_mysql($sql);
		return $INSERT_member_app->num();

	}

	//修改會員資料
	public static function update_db_information($id_member, $name, $tel, $phone, $gender, $birthday, $address)
	{
		$sql                = sprintf('UPDATE `information` SET `name` = %s,`tel` = %s,`phone` = %s,`gender` = %d,`birthday` = %s,`address` = %s
					WHERE `id_member` = %d',
			GetSQL($name, 'text'),
			GetSQL($tel, 'text'),
			GetSQL($phone, 'text'),
			GetSQL($gender, 'int'),
			GetSQL($birthday, 'text'),
			GetSQL($address, 'text'),
			GetSQL($id_member, 'int'));
		$INSERT_information = new db_mysql($sql);
		return ($INSERT_information->num()) ? true : false;
	}

	//刪除會員資料
	public function del_db_member($id_member)
	{
		$sql       = sprintf('DELETE FROM `member` WHERE `id_member` = %d',
			GetSQL($id_member, 'int'));
		$db_member = new db_mysql($sql);
		if ($db_member->num()) {
			Member::del_member_in_group($id_member);
			return true;
		}
		return false;
	}

	//登出
	public static function logout()
	{
		if (isset($_GET['logout'])) {
			$_SESSION['id_member']       = null;
			$_SESSION['id_member_group'] = null;
            $_SESSION['name'] = null;
            $_SESSION['nickname'] = null;
			$_SESSION['id_build_case']   = null;
            $_SESSION['id_member_additional']   = null;
			$_SESSION                    = [];
			unset($_SESSION);
			setcookie('ma_login', '0', time() - 10000, '/');
			setcookie('ma_cookie', 0, time() - 10000, '/');
			Tools::redirectLink('/index');
			return true;
		}
		return false;
	}

	//驗證建案
	public function check_member($action)
	{
		switch ($action) {
			case 'edit_member':
				if (!isint($_POST['id_member']))
					$this->_errors[] = '資料錯誤!找不到資料!';
			case 'add_member':
			case 'register':
				if (!empty($_POST['user']) && Member::is_member($_POST['user']))
					$this->_errors[] = '此帳號已申請過';
				if ($action == 'add_member')
					if (!isint($_POST['id_member_group']))
						$this->_errors[] = '請選擇帳號類別';
				if (($action == 'add_member') || ($action == 'register')) {
					if (empty($_POST['user']))
						$this->_errors[] = '請輸入帳號';
					if (!empty($_POST['user']) && !isEmail($_POST['user']) && $action == 'register')
						$this->_errors[] = '帳號必須為e-mail';
					if (empty($_POST['pass']))
						$this->_errors[] = '請輸入密碼';
				}
				if (empty($_POST['phone']))
					$this->_errors[] = '請輸入手機號碼';
				if (!empty($_POST['pass']) && ($_POST['pass'] != $_POST['pass2']))
					$this->_errors[] = '確認密碼錯誤!';
				if (empty($_POST['name']))
					$this->_errors[] = '請輸入姓名';
				if (!isTrue($_POST['gender']))
					$this->_errors[] = '請選擇性別';
				if (!empty($_POST['birthday']) && !isdate($_POST['birthday']))
					$this->_errors[] = '生日格式錯誤 (西元年/月/日)';
				break;
			case 'del_member':
			case 'del_member':
				if (!isint($_POST['id_member']))
					$this->_errors[] = '資料錯誤!找不到會員資料!';
				break;
		}
		return $this->_errors;
	}

	//註冊時間
	public static function get_register($user)
	{
		$sql               = 'SELECT i.`name`, a.`pass`, aa.`time`
				FROM `member` AS a
				LEFT JOIN `information` AS i ON i.`id_member` = a.`id_member`
				LEFT JOIN `member_app` AS aa ON aa.`user` = a.`user`
				WHERE a.`user` = "' . $user . '"
				ORDER BY aa.`id_member_app` ASC
				LIMIT 0,1';
		$SELECT_member_app = new db_mysql($sql);
		if ($SELECT_member_app->num() > 0) {
			$member_app = $SELECT_member_app->row();
			$name       = $member_app['name'];
			$pass       = $member_app['pass'];
			$time2      = $member_app['time'];
		}
		return [$name, $pass, $time2];
	}

	//驗證信
	public static function member_mail($user, $time2 = null)
	{    //$time2 = 註冊時間
		$sql               = 'SELECT `php_mail`, `smtp_auth`, `smtp_secure`, `host`, `port`, `char_set`, `username`, `password`, `from_email`, `from_name`, `subject`, `address`, `body_before`, `body`, `body_after`
				FROM `php_mailer`
				WHERE `id_mailer` = 1
				LIMIT 0,1';
		$SELECT_php_mailer = new db_mysql($sql);
		$num               = $SELECT_php_mailer->num();
		if ($num > 0)
			$mailer = $SELECT_php_mailer->row();
		list($name, $pass, $time2) = Member::get_register($user);

		$t     = time();
		$aging = $t + '172800';
		$time  = date('Y-m-d H:i:s', $t);
		if (empty($time2)) {
			$time2 = $time;
		}

		$security  = Member::get_member_app($user, $time, $aging);    //新的驗證碼
		$mail_body = '親愛的 <font style="color:#2222ff">' . $name . '</font> 您好<br>
			您於<form style="color:green">' . $time2 . '</form> ' . WEB_NAME . '註冊成為會員<br>
			<br>
			您的帳密資料如下：<br>
			帳號<form style="font-weight:bold;color:#ff2222"><a href="mailto:' . $user . '" target="_blank">' . $user . '</a></form><br>
			密碼：<form style="font-weight:bold;color:#ff2222">' . $pass . '</form><br>
			<br>
			請您連結至以下網址認證<br>
			進行您的帳號啟用動作並妥善保存您的帳號與密碼<br>
			認證網址： http://' . $_SERVER['HTTP_HOST'] . '/security.php?c=' . $security . '&u=' . $user;
		if ($mailer['php_mail'] == 0) {
			@$mail = new PHPMailer();            //建立新物件
			@$mail->IsSMTP();                //設定使用SMTP方式寄信
			@$mail->SMTPAuth = $mailer['smtp_auth'];    //設定SMTP需要驗證
			@$mail->SMTPSecure = $mailer['smtp_secure'];    //Gmail的SMTP主機需要使用SSL連線
			@$mail->Host = $mailer['host'];        //Gamil的SMTP主機
			@$mail->Port = $mailer['port'];        //Gamil的SMTP主機的埠號(Gmail為465)。
			@$mail->CharSet = $mailer['char_set'];        //郵件編碼
			@$mail->Membername = $mailer['username'];        //Gamil帳號
			@$mail->Password = $mailer['password'];        //Gmail密碼
			@$mail->From = $mailer['from_email'];    //寄件者信箱
			@$mail->FromName = $mailer['from_name'];    //寄件者姓名
			@$mail->IsHTML(true);                        //郵件內容為html ( true || false)
			@$mail->Subject = WEB_NAME . '會員認證信';    //郵件標題
			@$mail->Body = $mail_body;            //郵件內容

			@$mail->AddAddress($user);
			if (@$mail->Send()) {
				$_msgs = '認證信送出成功!';
			} else {
				$_errors = '認證信送出失敗!';
			}
		} else {    //用PHP mail()寄
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			if (@mail($user, WEB_NAME . '會員認證信', $mail_body, $headers)) {
				$_msgs = '認證信送出成功!';
			} else {
				$_errors = '認證信送出失敗!';
			}
		}
		return [$_msgs, $_errors];
	}

	//信件驗證完登入
	public static function security_login($user)
	{
		$sql           = 'SELECT * FROM `member` AS m
			LEFT JOIN `member_in_group` AS mig ON mig.`id_member` = m.`id_member`
			WHERE m.`user` = "' . $user . '"
			AND m.`active` = 1
			LIMIT 0, 1';
		$SELECT_member = new db_mysql($sql);
		if ($SELECT_member->num() > 0) {
			$member                      = $SELECT_member->row();
			$_SESSION['id_member']       = $member['id_member'];
			$_SESSION['id_member_group'] = $member['id_member_group'];
			return true;
		} else {
			return false;
		}
	}

	//是否登入
	public static function login($user, $password, $auto_loging = false)
	{        //$auto_loging = 自動登入
		$sql    = 'SELECT * FROM `member`
			WHERE `user` = "' . $user . '"
			AND `password` = "' . $password . '"
			AND `active` = 1
			LIMIT 0, 1';
		$member = Db::rowSQL($sql, true);
		if (count($member) > 0) {
			$_SESSION['id_member'] = $member['id_member'];
            $_SESSION['user']      = $member['user'];
			$_SESSION['name']      = $member['name'];
            $_SESSION['nickname']   = $member['nickname'];
            $_SESSION['id_member_additional']      = $member['id_member_additional'];

//			Houses::getHouseSESSION(false);	//todo

			$MemberAutoLoginCookieDate = Configuration::get('MemberAutoLoginCookieDate');
			if ($auto_loging && $MemberAutoLoginCookieDate > 0) {        //記住我
				//todo
				$check_mobile  = (int)check_mobile();
				$mobile_device = '';    //手機裝置
				if ($check_mobile) {
					$mobile_device = get_mobile();
				}
				$cookie_hash = sha1($member['id_member'] . $user . $check_mobile . $mobile_device . date() . $password);
				//刪除過期
				if ($check_mobile == 1) {
					//查看是否有資料
					$sql = 'SELECT `id_mal`
						  FROM `mamber_auto_login`
						  WHERE `id_member` = ' . $member['id_member'] . '
						  AND `device` = 1
						  LIMIT 0 ,1';
					$row = Db::rowSQL($sql, true);
					if ($row['id_mal']) {        //更新
						$sql = 'UPDATE `mamber_auto_login`
							SET `effective_time` = NOW() + INTERVAL ' . $MemberAutoLoginCookieDate . ' DAY,
							`cookie` = "' . $cookie_hash . '"
							WHERE `id_mal` = ' . $row['id_mal'];
					} else {                    //新增
						$sql = 'INSERT INTO `mamber_auto_login` (`id_member`, `device`, `effective_time`, `cookie`) VALUES (' . $member['id_member'] . ', 1, NOW() + INTERVAL ' . $MemberAutoLoginCookieDate . ' DAY, "' . $cookie_hash . '");';
					}
				} else {
					$sql = 'INSERT INTO `mamber_auto_login` (`id_member`, `device`, `effective_time`, `cookie`) VALUES (' . $member['id_member'] . ', 0, NOW() + INTERVAL ' . $MemberAutoLoginCookieDate . ' DAY, "' . $cookie_hash . '");';
				}
				$DB          = new db_mysql($sql);
				$cookie_time = (3600 * 24 * (int)$MemberAutoLoginCookieDate);
				setcookie('ma_login', '1', time() + $cookie_time, '/');
				setcookie('ma_cookie', $cookie_hash, time() + $cookie_time, '/');
			}
			return true;
		} else {
			return false;
		}
	}

	public function forgot($email){
		$email = GetSQL($email ,'text');
		$sql = sprintf('SELECT `id_member`
						FROM `member`
						WHERE (`email` = %s OR `user` = %s)
						AND `active` = 1
						LIMIT 0, 1',
					$email,
					$email);
		$row = Db::rowSQL($sql, true);
		if(!empty($sql['id_member'])){	//有帳號
			//寄出密碼重設信件
			Tools::redirectLink('//' . WEB_DNS . '/forgot?con=1');
		}else{
			$this->_errors[] = $this->l('email尚未註冊');
		}
	}

	public static function autologoing()
	{        //todo
		$MemberAutoLoginCookieDate = Configuration::get('MemberAutoLoginCookieDate');
		if (isset($_COOKIE['ma_login']) && $_COOKIE['ma_login'] == 1 && isset($_COOKIE['ma_cookie']) && $MemberAutoLoginCookieDate > 0) {
			$sql = 'SELECT mbl.`id_member`, m.`user`, m.`name` , m.`nickname` ,
                        m.`id_member_additional`
						FROM `mamber_auto_login` AS mbl
						LEFT JOIN `member` AS m ON m.`id_member` = mbl.`id_member`
						WHERE mbl.`cookie` = "' . $_COOKIE['ma_cookie'] . '" AND `active` = 1
						LIMIT 0, 1';

			$row = Db::rowSQL($sql, true);
			if ($row['id_member'] > 0) {        //相同自動登入
				$_SESSION['id_member'] = $row['id_member'];
				$_SESSION['name']      = $row['name'];
                $_SESSION['user']      = $row['user'];
                $_SESSION['nickname']  = $row['nickname'];
                $_SESSION['id_member_additional']      = $row['id_member_additional'];
				$cookie_time           = (3600 * 24 * $MemberAutoLoginCookieDate);
				setcookie('ma_login', '1', time() + $cookie_time, '/');
				setcookie('ma_cookie', $_COOKIE['ma_cookie'], time() + $cookie_time, '/');
			} else {                            //不相同登出
				$_SESSION['id_member']       = null;
				$_SESSION['id_member_group'] = null;
                $_SESSION['id_member_additional']      = null;
				$_SESSION                    = [];
				unset($_SESSION);
				setcookie('ma_login', '0', time() - 1000, '/');
				setcookie('ma_cookie', 0, time() - 1000, '/');
				$check_mobile = (int)check_mobile();
				if ($check_mobile == 1) {        //是手機
					Tools::redirectLink('/login?conf2=8');        //跳出一支手機限制一人登入標語
					exit;
				}
			}
		} else {        //判別帳號是否啟用
			$sql = sprintf('SELECT `id_member`
						FROM `member`
						WHERE `id_member` = %d AND `active` = 1
						LIMIT 0, 1',
				$_SESSION['id_member']
			);
			$row = Db::rowSQL($sql, true);
			if (!$row['id_member'] || empty($_SESSION['id_member'])) {
				$_SESSION['id_member']       = null;
				$_SESSION['id_member_group'] = null;
				$_SESSION['id_houses']       = null;
                $_SESSION['id_member_additional']       = null;
			}
		}
//		Houses::getHouseSESSION(false);	//todo
	}

	//修改用戶群組
	public static function update_member_group($id_member_group, $name, $msg_type = true)
	{
		$num = 0;
		if (!in_array($id_member_group, [1, 2])) {
			$sql                 = sprintf('UPDATE `member_group` SET  `member_group` = %s
						WHERE `id_member_group` = %d
						AND `id_member_group` NOT IN (1, 2)',
				GetSQL($name, 'text'),
				GetSQL($id_member_group, 'int'));
			$UPDATE_member_group = new db_mysql($sql);
			$num                 = $UPDATE_member_group->num();
		}
		if ($msg_type) {
			if ($num > 0) {
				Member::getContext()->msg = '會員群組修改成功!';
			} else {
				Member::getContext()->msg = '沒有修改任何資料!';
			}
		}
		return $num;
	}

	public static function del_group($id_member_group, $msg_type = true)
	{
		return Member::del_member_group($id_member_group, $msg_type);
	}

	//刪除用戶
	public static function del_member_group($id_member_group, $msg_type = true)
	{
		$num = 0;
		if (!is_array($id_member_group))
			$id_member_group = [$id_member_group];
		if (in_array(1, $id_member_group) || in_array(2, $id_member_group)) {
			if ($msg_type)
				Member::getContext()->_errors[] = '無法刪除該會員群組';
			return false;
		}
		if (count($id_member_group) > 0) {
			$id_member_group     = implode(',', $id_member_group);
			$sql                 = 'DELETE FROM `member_group`
				WHERE `id_member_group` IN (' . $id_member_group . ')
				AND `id_member_group` NOT IN (1, 2)';
			$DELETE_member_group = new db_mysql($sql);
			$num                 = $DELETE_member_group->num();
		}
		if ($msg_type) {
			if ($num > 0) {
				Member::getContext()->_msgs[] = '刪除成功';
			} else {
				Member::getContext()->_errors[] = '沒有刪除任何資料';
			}
		}
		return $num;
	}

	//新增會員群組
	public static function add_group($member_group)
	{
		return Member::add_member_group($member_group);
	}

	//新增會員群組
	public static function add_member_group($member_group)
	{
		$sql                 = sprintf('INSERT INTO `member_group` (`member_group`) VALUES (%s)',
			GetSQL($member_group, 'text'));
		$INSERT_member_group = new db_mysql($sql);
		$num                 = $INSERT_member_group->num();
		return ($num) ? $num : false;
	}

	public static function get_group($id_member = null, $SELECT = '*')
	{
		return Member::get_member_group($id_member, $SELECT);
	}

	//取得用戶群組
	public static function get_member_group($id_member = null, $SELECT = '*')
	{
		$arr_member = [];
		$WHILE      = '';
		if (!empty($id_member)) {
			$WHILE     = 'WHERE `id_member_group` = ' . $id_member;
			$LIMIT_num = 1;
			if (is_array($id_member)) {
				$LIMIT_num = count($id_member);
				$id_member = implode(',', $id_member);
				$WHILE     = 'WHERE `id_member_group` IN (' . $id_member . ')';
			}
			$LIMIT = ' LIMIT 0, ' . $LIMIT_num;
		}
		$sql                 = 'SELECT ' . $SELECT . ' FROM `member_group`' .
			$WHILE . $LIMIT;
		$SELECT_member_group = new db_mysql($sql);
		if ($SELECT_member_group->num() > 0) {
			if ($LIMIT_num == 1) {
				$arr_member = $SELECT_member_group->row();
			} else {
				while ($user = $SELECT_member_group->next_row()) {
					$arr_member[] = $user;
				}
			}
		}
		return $arr_member;
	}

	public static function update_group($id_member_group, $member_name, $msg_type = true)
	{
		return Member::update_member_group($id_member_group, $member_name, $msg_type);
	}

	//切換社區
	public static function change()
	{

	}

	public static function getMemberSetUp($id_member)
	{
		$sql = 'SELECT * FROM `member_set_up` WHERE `id_member` = ' . $id_member . ' LIMIT 0, 1';
		$row = Db::rowSQL($sql, true);
		return $row;
	}

	public static function setMemberSetUp($id_member, $fcm_on = null, $email_on = null)
	{
		if (!empty($id_member) && ($fcm_on !== null || $email_on !== null)) {
			$sql    = 'SELECT `id_member` FROM `member_set_up` WHERE `id_member` = ' . $id_member . ' LIMIT 0, 1';
			$row    = Db::rowSQL($sql, true);
			$SET    = '';
			$INSERT = '';
			$VALUES = '';
			if ($fcm_on !== null) {
				$SET    = ' SET `fcm_on` = ' . $fcm_on;
				$INSERT .= ', `fcm_on`';
				$VALUES .= ', ' . GetSQL($fcm_on, 'int');
			}

			if ($email_on !== null) {
				if (!empty($SET)) {
					$SET .= ', `email_on` = ' . $email_on;
				} else {
					$SET = ' SET `email_on` = ' . $email_on;
				}
				$INSERT .= ', `email_on`';
				$VALUES .= ', ' . GetSQL($email_on, 'int');
			}

			if ($row['id_member']) {    //修改
				$sql = 'UPDATE `member_set_up`' . $SET . ' WHERE `id_member` = ' . $id_member;
			} else {                    //新增
				$sql = sprintf('INSERT INTO `member_set_up` (`id_member`' . $INSERT . ') VALUES (%d' . $VALUES . ')',
					GetSQL($id_member, 'int'));
			}
			Db::rowSQL($sql);
			return true;
		}
		return false;
	}
}