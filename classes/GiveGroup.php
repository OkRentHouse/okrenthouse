<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2017/11/17
 * Time: 下午 03:32
 */

class GiveGroup
{
	protected static $instance = null;        //是否實體化
	public $admin = false;

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new GiveGroup();
		};
		return self::$instance;
	}

	static public function setGroup($id_group, $id_gopur2, $enabled)
	{
		//$id_gopur2 是否存在、是否有$id_group編輯權限
		if (GiveGroup::haveGroup($id_gopur2)
			&& in_array($id_group, GiveGroup::getGroup($_SESSION['id_group'], true))
			&& in_array($id_gopur2, GiveGroup::getGroup($_SESSION['id_group'], true))) {
			$arr = GiveGroup::getGiveGroup($id_group);		//$id_group是否有GiveGroup紀錄
			$json = new JSON();
			if ((count($arr) || $arr == []) && $arr !== false) {
				$arr  = array_filter($arr);    //刪除空白
				$arr = array_unique($arr);		//去除重複
				if (empty($enabled)) {    //移除
					foreach ($arr as $i => $v) {
						if ($id_gopur2 == $v){
							unset($arr[$i]);
							break;
						}
					}
				} else {                //加入
					if(!in_array($id_gopur2, $arr)){
						$arr[] = $id_gopur2;
					}
				}
				$arr = array_values($arr);
				$sql = sprintf('UPDATE `' . DB_PREFIX_ . 'give_group` SET `groups`= %s WHERE `id_group` = %d', GetSQL($json->encode($arr), 'text'), $id_group);
			} elseif ($enabled) {
				$sql = sprintf('INSERT INTO `' . DB_PREFIX_ . 'give_group` (`id_group`, `groups`) VALUES (%d, %s)', $id_group, GetSQL($json->encode([$id_gopur2]), 'text'));
			}
			Db::rowSQL($sql);
			return Db::getContext()->num();
		}
		return false;
	}

	static public function getGiveGroup($id_group){
		$sql  = 'SELECT g.`admin`, gg.`groups`
			FROM `' . DB_PREFIX_ . 'group` AS g
			INNER JOIN `' . DB_PREFIX_ . 'give_group` AS gg ON gg.`id_group` = g.`id_group`
			WHERE g.`id_group` = ' . $id_group . '
			LIMIT 0, 1';
		$row  = Db::rowSQL($sql, true);
		if(count($row) == 0){
			return false;
		}
		$json = new JSON();
		$arr = $json->decode($row['groups']);
		return $arr;

	}

	static public function getGroup($id_group = null, $type = true)
	{
		if (empty($id_group)) {
			(int)$id_group = $_SESSION['id_group'];
		}
		$WHERE = ' WHERE g.`id_group` = ' . $id_group;

		$sql  = 'SELECT g.`admin`, gg.`groups`
			FROM `' . DB_PREFIX_ . 'group` AS g
			LEFT JOIN `' . DB_PREFIX_ . 'give_group` AS gg ON gg.`id_group` = g.`id_group`' . $WHERE . ' LIMIT 0, 1';
		$row  = Db::rowSQL($sql, true);
		$json = new JSON();
		$arr  = [];
		if ($row['admin'] || $_SESSION['id_group'] == 1) {            //是系統管理者的話  //todo
			$sql      = 'SELECT `id_group`
				FROM `' . DB_PREFIX_ . 'group`
				WHERE `id_group` <> 1';
			$arrr_row = Db::rowSQL($sql);
			foreach ($arrr_row as $i => $v) {
				$arr[] = $v['id_group'];
			}
			GiveGroup::getContext()->admin = true;
		} else {
			$arr = $json->decode($row['groups']);
		}
		if (count($arr) == 0 && $type) {
			$arr = [0];
		}
		return $arr;
	}

	static public function haveGroup($id_group)
	{
		$sql = sprintf('SELECT `id_group`
			FROM `' . DB_PREFIX_ . 'group`
			WHERE `id_group` = %d
			LIMIT 0, 1',
			GetSQL($id_group, 'int'));
		$row = Db::rowSQL($sql, true);
		return ($row['id_group']) ? true : false;
	}

	static public function getAllGroup()
	{
		$sql     = 'SELECT g.`id_group`, g.`admin`, gg.`groups`
			FROM `' . DB_PREFIX_ . 'group` AS g
			LEFT JOIN `' . DB_PREFIX_ . 'give_group` AS gg ON gg.`id_group` = g.`id_group`';
		$arr_row = Db::rowSQL($sql);
		$arr     = [];
		$json    = new JSON();
		foreach ($arr_row as $i => $v) {
			if ($v['admin']) {
				$arr2     = [];
				$sql      = 'SELECT `id_group`
					FROM `' . DB_PREFIX_ . 'group`';
				$arrr_row = Db::rowSQL($sql);
				foreach ($arrr_row as $i => $vv) {
					if ($vv['id_group'] != 1) {        //除"最高系統管理者"外，擁有所有群組權限
						$arr2[] = $vv['id_group'];
					}
				}
				$arr[$v['id_group']] = $arr2;
			} else {
				$arr[$v['id_group']] = $json->decode($v['groups']);
			}
		}
		return $arr;
	}

	static public function getGroupFunction($id_group, $fun)
	{
		if (empty($id_group))
			(int)$id_group = $_SESSION['id_group'];

		$sql = sprintf('SELECT g.`admin`, cf.`val`
			FROM `' . DB_PREFIX_ . 'group` AS g
			LEFT JOIN `' . DB_PREFIX_ . 'competence_function` AS cf ON cf.`id_group` = g.`id_group` AND cf.`fun` = %s
			WHERE g.`id_group` = %d
			LIMIT 0, 1', GetSQL($fun, 'text'), $id_group);
		$row = Db::rowSQL($sql, true);
		$v   = $row['val'];
		return $v;
	}

	static public function getGroupFunctionList($fun)
	{
		$arr     = [];
		$sql     = sprintf('SELECT g.`id_group`, cf.`val`
			FROM `' . DB_PREFIX_ . 'group` AS g
			LEFT JOIN `' . DB_PREFIX_ . 'competence_function` AS cf ON cf.`id_group` = g.`id_group`
			AND cf.`fun` = %s',
			GetSQL($fun, 'text'));
		$arr_row = Db::rowSQL($sql);
		foreach ($arr_row as $i => $row) {
			$arr[$row['id_group']] = $row['val'];
		}
		return $arr;
	}

	static public function setGroupFunctin($id_group, $fun, $val)
	{
		$v = GiveGroup::getGroupFunction($id_group, $fun);
		if (GiveGroup::getContext()->admin)
			return false;
		if ($v !== null) {
			$sql = sprintf('UPDATE `' . DB_PREFIX_ . 'competence_function` SET `val`= %s WHERE `id_group` = %d AND `fun` = %s', GetSQL($val, 'text'), $id_group, GetSQL($fun, 'text'));
		} else {
			$sql = sprintf('INSERT INTO `' . DB_PREFIX_ . 'competence_function` (`id_group`, `fun`, `val`) VALUES (%d, %s, %s)', $id_group, GetSQL($fun, 'text'), GetSQL($val, 'text'));
		}
		Db::rowSQL($sql);
		return Db::getContext()->num();
	}
}
