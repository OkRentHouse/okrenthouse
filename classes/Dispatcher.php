<?php
//URL網址解析
class Dispatcher
{
	public $request_url;
	public $redirect_url;
	public $http_host;
	public $page;
	public $query_string;
	public $controller_type;
	public $controller;
	public $module = '';
	public $admin_controllers;
	public $front_controllers;
	public $admin_module_controllers;
	public $front_module_controllers;
	public $url_parameter = []; //URL 參數陣列
	public $is_media = false;
	public $is_app = false;

	protected static $instance = null;

	protected function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Dispatcher();
		};
		return self::$instance;
	}

	public function verification_img()
	{
		$strl = strlen(URL_VERIFICATION);
		if (substr($_SERVER['REDIRECT_URL'], -$strl, $strl) === URL_VERIFICATION) {    //驗證碼圖
			try {
				Validate::getContext()->getImg();
				exit;
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
	}

	public function barcode_img($str = null, $code = null, $font = null, $resolution = 2, $thickness = 20, $file_name = null)
	{
		if (empty($str)) {
			$str = $this->url_parameter[0];
		}
		if (empty($code)) {
			$code = $this->url_parameter[1];
		}
		if (empty($font)) {
			$font = $this->url_parameter[2];
		}
		if (!empty($this->url_parameter[3])) {
			$resolution = $this->url_parameter[3];
		}
		if (!empty($this->url_parameter[4])) {
			$thickness = $this->url_parameter[4];
		}
		if (empty($file_name)) {
			$file_name = $this->url_parameter[5];
		}
		$strl = strlen(URL_BARCODE);
		if (substr($_SERVER['REDIRECT_URL'], 1, $strl) === URL_BARCODE) {    //一維條碼
			try {
				$bc = new Barcode($str, $code, $font, $resolution, $thickness);
				die($bc->draw($file_name));
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
	}

	public function download()
	{
		if (strpos($_SERVER['REDIRECT_URL'], URL_DOWNLOAD) !== false) {            //檔案下載
			$url_len   = (int)strlen(URL_DOWNLOAD);
			$url_len_n = (int)strpos($_SERVER['REDIRECT_URL'], URL_DOWNLOAD);
			$id        = substr($_SERVER['REDIRECT_URL'], $url_len + $url_len_n);
			if (File::check_member_file($id)) {
				File::download_file($id);
			} else {
				gotourl('/#member');
			};
		};
	}

	//請求URL
	public function setURL()
	{
		if (isset($_SERVER['REQUEST_URI'])) {
			$this->request_url = $_SERVER['REQUEST_URI'];
		} elseif (isset($_SERVER['HTTP_X_REWRITE_URL'])) {
			$this->request_url = $_SERVER['HTTP_X_REWRITE_URL'];
		};
		$this->request_url  = rawurldecode($this->request_url);
		$arr = explode('?', $_SERVER['REQUEST_URI']);
		$this->redirect_url = rawurldecode($arr[0]);


		$arr_url = explode('/', $this->redirect_url);            //網址切割成陣列
		$arr_url = array_filter($arr_url);                            //清除空的陣列

		$i = 1;
		if ($_SERVER['HTTP_HOST'] == Configuration::get('MediaURL')) {        //手機版網頁
			$this->is_media = true;
		} elseif ($_SERVER['HTTP_HOST'] == Configuration::get('APP_URL')) {    //APP版網頁
			$this->is_app = true;
        } elseif ($_SERVER['HTTP_HOST'] == Configuration::get('APP_Okrent_URL')) {    //共享圈App版網頁
            $this->is_app = true;
        }
		switch ($arr_url[$i]) {
			case ADMIN_URL :                            //系統管理者後台
				$i++;
				$this->controller_type = 'admin';
			break;
			case MANAGE_URL :                            //一般管理者後台
				$i++;
				$this->controller_type = 'member';
			break;
			case MODULE_URL :
			default:
				$this->controller_type = 'front';
			break;
		};
		
		if ($arr_url[$i] == str_replace('/', '', MODULE_URL) || $arr_url[$i] == MIN_MODULE_URL) {
			$this->controller_type .= 'module';
			$i++;
			$this->module = $arr_url[$i];
//			$this->url_parameter = array_slice($arr_url, $i+1);
		}

		$controller = '';

		if (!empty($arr_url[$i])) {
			if($this->controller_type == 'front'){
				//自訂URL網址
				$url = SEO::getContext()->getPageOnURL(null, $arr_url[$i]);
				if(!empty($url)){
					$arr_url[$i] = $url;
				}
			}
			$arr_controller = explode('_', $arr_url[$i]);
			foreach ($arr_controller as $w) {
				$arr = explode('-', $w);
				foreach ($arr as $v) {
					$controller .= ucfirst($v);
				}
			}

			switch ($this->controller_type) {
				case 'adminmodule' :                        //系統管理者模組
				case 'admin' :                            //系統管理者後台
					$this->controller = 'Admin' . $controller . 'Controller';
				break;
				case 'member' :                            //一般管理者後台
					$this->controller = 'Member' . $controller . 'Controller';
				break;
				case 'frontmodule' :                        //前台模組
				default:
					$this->controller = $controller . 'Controller';
				break;
			};

			if ($controller == 'CMS') {
				CMS::getContext()->url = $arr_url[$i + 1];
			} else {
				//todo
				$arr_web = [
					''               => '',
					'web_app'        => 1,
					'web_house'      => 2,
                    'web_life_house' => 3,
                    'web_rent'       => 4,
                    'web_repair'     => 5,
                    'web_okmaster'      => 6,
                    'web_okrent'      => 7,
                    'web_okpt' => 8,
                    'web_life_house'       => 9,
                    'web_epro'     => 10,
                    'web_rent'      => 11,
                    'web_app_okrent' => 12,
				];
				$id_web  = $arr_web[LilyHouse::getContext()->web];

				if (CMS::isCMS($id_web, $arr_url[$i])) {
					$this->controller      = 'CMSController';
					CMS::getContext()->url = $arr_url[$i];
				}
			}

			$this->url_parameter = array_slice($arr_url, $i);

			foreach ($this->url_parameter as $url) {
				if (in_array($url, RESERVED_URL)) {
					if (!Tools::isSubmit('ajax'))
						$_POST['ajax'] = 1;
					if (!Tools::isSubmit('action'))
						$_POST['action'] = $url;
				}
				if (!Tools::isSubmit($url)) {
					$_GET[$url] = true;
				}
			}
		}
		return $this;
	}

	public function getController($web = null)
	{
		if (empty($this->controller)) {
			switch ($this->controller_type) {
				case 'admin' :
					$arr_tab = Tab::getContext()->getTab(true);    //取出所有啟用的選單
					if ($_SESSION['id_group'] != 1) {
						if (empty($_SESSION['id_group']))
							Tools::redirectLink('//' . WEB_DNS . '/' . ADMIN_URL . '/Loging');
						foreach ($arr_tab as $i => $tab) {
							$parameter  = '';
							$arr_Access = Competence::getContext()->getAccess($tab['id_tab']);
							if ($arr_Access['view']) {
								$this->controller = $arr_tab[$i]['controller_name'];
								$parameter        = $arr_tab[$i]['parameter'];
								break;
							}
						}
						if (empty($this->controller))
							$this->controller = $arr_tab[0]['controller_name'];
					} else {
						$this->controller = $arr_tab[0]['controller_name'];
						$parameter        = $arr_tab[0]['parameter'];
					}
					$url = str_replace('Controller', '', $this->controller);
					$url = str_replace('Admin', '', $url);
					Tools::redirectLink($url . $parameter);
				break;
				case 'member' :
				break;
				default:
					$this->controller = 'IndexController';
				break;
			}
		}
		return $this->controller;
	}

	public function dispatch()
	{
		$this->controller = $this->getController();
		switch ($this->controller_type) {
			case 'admin':    //系統管理者
				if (is_file(OVERRIDE_DIR . DS . 'admin' . DS . 'controller' . DS . $this->controller . '.php')) {
					include_once(OVERRIDE_DIR . DS . 'admin' . DS . 'controller' . DS . $this->controller . '.php');
				} elseif (is_file(CONTROLLERS_DIR . DS . 'admin' . DS . $this->controller . '.php')) {
					include_once(CONTROLLERS_DIR . DS . 'admin' . DS . $this->controller . '.php');
				};
				if (isset($this->module) && $this->module) {
					//執行 admin_module
				};
			break;
			case 'member':    //一般管理者
				//member 找不到網頁
				/*
				if($this->module 有沒有 && $this->module 啟用){
					執行 membern_module
				}else{
					尚未啟用夜面
				};
				*/
			break;
			case 'frontmodule':
				if (is_file(OVERRIDE_CONTROLLER_DIR . DS . 'modules' . DS . $this->module . DS . 'controllers' . DS . 'front' . DS . $this->controller . '.php')) {
					include_once(OVERRIDE_CONTROLLER_DIR . DS . 'modules' . DS . $this->module . DS . 'controllers' . DS . 'front' . DS . $this->controller . '.php');
				} elseif (is_file(MODULE_DIR . DS . $this->module . DS . 'controllers' . DS . 'front' . DS . $this->controller . '.php')) {
					include_once(MODULE_DIR . DS . $this->module . DS . 'controllers' . DS . 'front' . DS . $this->controller . '.php');
				};
			break;
			case 'adminmodule':
				if (is_file(OVERRIDE_CONTROLLER_DIR . DS . 'modules' . DS . $this->module . DS . 'controllers' . DS . 'admin' . DS . $this->controller . '.php')) {
					include_once(OVERRIDE_CONTROLLER_DIR . DS . 'modules' . DS . $this->module . DS . 'controllers' . DS . 'admin' . DS . $this->controller . '.php');
				} elseif (is_file(MODULE_DIR . DS . $this->module . DS . 'controllers' . DS . 'admin' . DS . $this->controller . '.php')) {
					include_once(MODULE_DIR . DS . $this->module . DS . 'controllers' . DS . 'admin' . DS . $this->controller . '.php');
				};
			break;
			case 'front':
			default:        //一般使用者 (訪客)
				if (is_file(OVERRIDE_DIR . DS . 'front' . DS . 'controller' . DS . $this->controller . '.php')) {
					include_once(OVERRIDE_DIR . DS . 'front' . DS . 'controller' . DS . $this->controller . '.php');
				} elseif (is_file(CONTROLLERS_DIR . DS . 'front' . DS . $this->controller . '.php')) {
					include_once(CONTROLLERS_DIR . DS . 'front' . DS . $this->controller . '.php');
				};
				/*
				if($this->module 有沒有 && $this->module 啟用){
					執行 membern_module
				}else{
					尚未啟用夜面
				};
				*/
			break;
		}

		if ($controller = Controller::getController($this->controller)) {        //有就執行
			$controller->run();
			exit;
		} else {                                            //沒有跳到404畫面
			header('HTTP/1.1 404 Not Found');
			header('Status: 404 Not Found');
			$this->page_not_found();
		};
	}

	public function page_not_found()
	{
		$type             = 'front';
		$this->controller = 'PageNotFoundController';
		if ($this->controller_type == 'admin') {
			$type             = 'admin';
			$this->controller = 'AdminPageNotFoundController';
		};
		if (is_file(OVERRIDE_DIR . DS . $type . DS . 'controller' . DS . $this->controller . '.php')) {
			include_once(OVERRIDE_DIR . DS . $type . DS . 'controller' . DS . $this->controller . '.php');
		} elseif (is_file(CONTROLLERS_DIR . DS . $type . DS . $this->controller . '.php')) {
			include_once(CONTROLLERS_DIR . DS . $type . DS . $this->controller . '.php');
		};
		Controller::getController($this->controller)->run();
		exit;
	}

	public function planned_img()
	{
		if (strpos($_SERVER['REDIRECT_URL'], URL_PLANNED) !== false) {
			$file = strrchr($_SERVER['REDIRECT_URL'], '/');
			$file = substr($file, 1, strlen($file));
			$id   = ereg_replace('[^0-9]', '', $file);
			if (Planned::have_planned_img($id)) {
				list($str_dir, $str_src) = str_dir_change($id);
				print_file(WEB_DIR . DS . URL_PLANNED . DS . $str_dir . $file, $file, false);
			};
			include(WEB_DIR . DS . '404.php');
			exit;
		};
	}
}
