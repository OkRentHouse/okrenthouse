<?php
class InLife
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new InLife();
		};
		return self::$instance;
	}

	public function getList(){

	}

	public function get($id=null, $active=null, $date=null, $num=false,$file_type=0 ,$top=true){
		if($top){
			$ORDER = ' ORDER BY il.`top` DESC, il.`date` DESC';
		}else{
			$ORDER = ' ORDER BY il.`date` DESC';
		}
		$WHERE = '';
		if($active !== null){
			$WHERE .= sprintf(' AND il.`active` = %d', GetSQL($active, 'int'));
		}
		if($date !== null){
			$WHERE .= sprintf(' AND il.`date` <= %s', GetSQL($date, 'text'));
		}
		if(!empty($id)){
			$WHERE.= sprintf(' AND il.`id_in_life` = %d', GetSQL($id, 'int'));
		}

        $WHERE.= sprintf(' AND ilf.`file_type` = %d', GetSQL($file_type, 'int'));//預設呼叫

		$sql = 'SELECT il.`id_in_life`, il.`date`, il.`top`, il.`title`, il.`title2`, il.`exp`, il.`date_s`, il.`date_e`, il.`weekday`, il.`time_s`, il.`time_e`, il.`address`, il.`add_name`, il.`content`, il.`description`, il.`keywords`, il.`views`, il.`active`,
				il.`url`,ilf.`id_file`
				FROM `in_life` AS il 
				LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = il.`id_in_life`
				WHERE TRUE'.$WHERE.'
				GROUP BY il.`id_in_life`
				'.$ORDER;
		$row = Db::rowSQL($sql, false, $num);
		foreach($row as $i => $v){
			$date_t_s = dateTo_c($v['date_s']);
			$date_t_e = dateTo_c($v['date_e']);
			if($date_t_s != '00.00.00'){
				$row[$i]['data_t_s'] = $date_t_s;
			}else{
				$row[$i]['data_t_s'] = '';
				$date_t_s = '';
			}
			if($date_t_e != '00.00.00'){
				$row[$i]['data_t_e'] = $date_t_e;
			}else{
				$row[$i]['data_t_e'] = '';
				$date_t_e = '';
			}
			if(
				(($date_t_s == $date_t_e) && !empty($date_t_s))
				|| (!empty($date_t_s) && empty($date_t_e))
			){
				$row[$i]['date_t'] = $date_t_s;
			}else if(!empty($date_t_s) && !empty($date_t_e)) {
				$row[$i]['date_t'] = $date_t_s . ' ~ ' . $date_t_e;
			}else if(empty($date_t_s) && !empty($date_t_e)){
				$row[$i]['date_t'] = '即日起至 ' . $date_t_e;
			}else{
				$row[$i]['date_t'] = null;
			}

			$row[$i]['weekday_t'] = get_weekday_range(JSON::getContext()->decode($row[$i]['weekday']));
			$time_s = substr($v['time_s'], 0, 5);
			$time_e = substr($v['time_e'], 0, 5);
			if(
				(($time_s == $time_e) && !empty($time_s))
				|| (!empty($time_s) && empty($time_e))
			){
				$row[$i]['time_t'] = $time_s;
			}else if(!empty($time_s) && !empty($time_e)) {
				$row[$i]['time_t'] = $time_s . ' ~ ' . $time_e;
			}else if(empty($time_s) && !empty($time_e)){
				$row[$i]['time_t'] = $time_e;
			}else{
				$row[$i]['time_t'] = null;
			}
		}
		return $row;
	}

	public function getType($active = false){
		if($active){
			$WHERE = ' AND `active` = 1';
		}
		$sql = 'SELECT *
			FROM `in_life_type`
			WHERE true '.$WHERE.'
			ORDER BY `position` ASC
			';
		$row = Db::rowSQL($sql);
		return $row;
	}

	public function setViews($id){
		$sql = sprintf(' UPDATE `in_life`
				SET `views` = 0
				WHERE `id_in_life` = %d
				AND `views` IS NULL',
			GetSQL($id, 'int'));
		Db::rowSQL($sql);

		$sql = sprintf(' UPDATE `in_life`
				SET `views` = `views` + 1
				WHERE `id_in_life` = %d',
		GetSQL($id, 'int'));
		Db::rowSQL($sql);
		return (Db::getContext()->num()) ? true : false;
	}

	public static function getFile($id_in_life){
		$sql = sprintf('SELECT `id_file`, `file_type`
				FROM `in_life_file`
				WHERE `id_in_life` = %d',
			GetSQL($id_in_life, 'int'));
		$arr_row = Db::rowSQL($sql);
		return $arr_row;
	}
}