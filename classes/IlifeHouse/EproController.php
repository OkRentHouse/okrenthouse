<?php
include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');
class EproController extends FrontController{
    public function __construct()
    {


        $this->meta_description          = '全方位居家裝修服務~設計裝潢、維修保養免費到府估價|價格透明|責任施工|後續保固|24hr|24小時|專業貼心，是專業可靠的裝修專家。提供裝潢設計|衛浴|廚具|水電|冷氣空調|電器|熱水器|油漆粉刷|壁紙|管線|通管|燈飾照明|木作|泥作|鐵作|地板|石材及美化|玻璃|磁磚|防水抓漏|門窗門鎖|智能鎖|安控監視|消防保全|園藝植栽|環境清潔|沙發.床墊.窗簾.地毯除螨清洗|消毒滅菌|除蟲除味|窗簾地毯|淨水設備|升降設備|機電|弱電|節能|綠建材|智慧居家|房屋健檢|驗屋檢測|無障礙空間等裝修服務。生活居家裝修企業社';
//        $this->conn = 'ilifehou_epro';
        parent::__construct();
        $this->context->smarty->assign([

        ]);

    }

    public function initProcess(){

        $footer_txt1='';
        $footer_txt2='<span>生活居家裝修企業社版權所有</span> copyright © LIFE HOUSE REPAIR COMPANY 2020 All right reserd';
        $footer_txt2 = "<a href='/Aboutokmaster'><img src='/themes/Okmaster/img/index/footer_logo.png'></a>生活好科技有限公司 <span class='spacing'></span> copyright ©LIFE MASTER TECHNOLOGY COMPANY <span class='spacing'></span> <!--<a style='margin-left: 20%;' href='https://www.okmaster.life/Aboutokmaster'>合作提案</a> |--> <a href='/State'>相關聲明</a>";
        $footer_txt3='';
        $meta_description='全方位居家裝修服務~設計裝潢、維修保養免費到府估價|價格透明|責任施工|後續保固|24hr|24小時|專業貼心，是專業可靠的裝修專家。提供裝潢設計|衛浴|廚具|水電|冷氣空調|電器|熱水器|油漆粉刷|壁紙|管線|通管|燈飾照明|木作|泥作|鐵作|地板|石材及美化|玻璃|磁磚|防水抓漏|門窗門鎖|智能鎖|安控監視|消防保全|園藝植栽|環境清潔|沙發.床墊.窗簾.地毯除螨清洗|消毒滅菌|除蟲除味|窗簾地毯|淨水設備|升降設備|機電|弱電|節能|綠建材|智慧居家|房屋健檢|驗屋檢測|無障礙空間等裝修服務。生活居家裝修企業社';
        $sid = Tools::getValue("sid");

        $sql = "SELECT * FROM ilifehou_epro.`epro_windows` WHERE active=1 ORDER BY position ASC,id_epro_windows ASC";
        $windows_data = Db::rowSQL($sql);
        foreach($windows_data as $i => $v){
            $sql = "SELECT * FROM ilifehou_epro.`epro_windows_file` WHERE file_type=0 AND id_epro_windows=".$v["id_epro_windows"].' ORDER BY id_epro_windows_file ASC LIMIT 1';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $windows_data[$i]['img'] = $row['url'];
            if($v["id_epro_windows"]==$sid){
              $sidname=$v["title"];
            }
        }
        $windows_count = count($windows_data)-1;
js;
				//輪播
				        $group_link_main_js =<<<hmtl
				        <script>


						    $(".slider_center_epro").slick({
						        dots: false,
						        infinite: true,
						        centerMode: true,
						        slidesToShow: 5,
						        slidesToScroll: 9
						    });

                $(document).ready(
                  function(){
                    $(".slick-slide").css("width","auto");
                  }
                );



				</script>
hmtl;
//        print_r($windows_data);

        $this->context->smarty->assign([
            'footer_txt1'=>$footer_txt1,
            'footer_txt2'=>$footer_txt2,
            'footer_txt3'=>$footer_txt3,
            'windows_data'=>$windows_data,
            'windows_count'=>$windows_count,
            'sid' => $sid,
            'sidname' => $sidname,
            'group_link_main_js' => $group_link_main_js,
            'search_county'=>County::getContext()->database_county(7),//桃園的預設
            'search_epro_windows'=>Epro::get_epro_windows(),
        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        parent::setMedia();
    }
}
