<?php
class ListUse
{
    protected static $instance;
    public $_errors = [];
    public $_msgs = [];
    public $msg = '';

    public function __construct()
    {
    }

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new ListUse();
        };
        return self::$instance;
    }

    public function getList(){

    }

    public function setViews($id){
        $sql = sprintf(' UPDATE `rent_house`
				SET `views` = 0
				WHERE `id_rent_house` = %d
				AND `views` IS NULL',
            GetSQL($id, 'int'));
        Db::rowSQL($sql);

        $sql = sprintf(' UPDATE `rent_house`
				SET `views` = `views` + 1
				WHERE `id_rent_house` = %d',
            GetSQL($id, 'int'));
        Db::rowSQL($sql);
        return (Db::getContext()->num()) ? true : false;
    }


    public function get($id_rent_house=null,$active=null,$num=0,$county=null,$city=null,$id_type=null,$id_types=null,
                        $id_device=null,$id_device_class=null,$id_other=null,$max_price=null,$min_price=null,$max_ping=null,
                        $min_ping=null,$max_room=null,$min_room=null,$search=null,$house_choose=null,$special=null,$limit=null,$free_where=null){

        $WHERE = '';

        if($id_rent_house !== null){
            $WHERE .= " AND r_h.`id_rent_house`=".GetSQL($id_rent_house,'int');
        }

        if($active !== null){
            $WHERE .= " AND r_h.`active`=".GetSQL($active,'int');
        }

        if($county != null){
            $WHERE .=" AND (r_h.`part_address` LIKE ".GetSQL("%".$county."%",'text')." OR
                        (cy.`county_name`=".GetSQL($county,'text').") )";
//              $WHERE .= " AND address LIKE ".GetSQL($county,"text");
        }

        if($city != null){
            $city_str_in = '';//本身物件的連接值
            $city_like =[];//針對匯入完整地址使用

            if(is_array($city)){//如果是用點選的話則是一般的字串
                foreach ($city as $k => $v){
                    if($city_str_in==''){
                        $city_str_in .=GetSQL($v,"text");
                    }else{
                        $city_str_in .=','.GetSQL($v,"text");
                    }
                    $city_like[] =GetSQL("%".$v."%","text");//like使用
                }
                $city_out = "";
                foreach($city_like as $k => $v){
                    if($city_out==""){
                        $city_out .=" r_h.`part_address` LIKE ".$v;// LIKE '%XXXX%' 這樣
                    }else{
                        $city_out .=" OR r_h.`part_address` LIKE ".$v;
                    }
                }
                $WHERE .=" AND (ci.`city_name` IN(".$city_str_in.") OR (".$city_out.")  )";
            }else{//一般字串
                $WHERE .=" AND (ci.`city_name` = {$city} OR 
                (r_h.`part_address` LIKE ".GetSQL("%{$city}%","text").")  )";
           }
        }

        if($id_types !=null){
            $id_types_in = '';
            if(is_array($id_types)){
                foreach ($id_types as $k => $v){
                    if($id_types_in==""){
                        $id_types_in .= GetSQL($v,"int");
                    }else{
                        $id_types_in .= ','.GetSQL($v,"int");
                    }
                }
                $WHERE .=" AND r_h.`id_rent_house_types` IN(".$id_types_in.")  ";
            }else{
                $WHERE .=" AND r_h.`id_rent_house_types` = {$id_types} ";
            }
        }

        if($id_type !=null){
            $id_type_like = [];
            foreach($id_type as $k => $v){
                $id_type_like[] = GetSQL('%"'.$v.'"%',"text");
            }
            $id_type_out ="";
            foreach($id_type_like as $k => $v){
                if($id_type_out==""){
                    $id_type_out .=" r_h.`id_rent_house_type` LIKE ".$v;// LIKE '%XXXX%' 這樣
                }else{
                    $id_type_out .=" OR r_h.`id_rent_house_type` LIKE ".$v;
                }
            }
            $WHERE .=" AND (".$id_type_out.") ";
        }

        if($id_device !=null){
            $id_device_like = [];
            foreach($id_device as $k => $v){
                $id_device_like[] = GetSQL('%'.$v.'%',"text");
            }
            $id_device_out ="";
            foreach($id_device_like as $k => $v){
                if($id_device_out==""){
                    $id_device_out .=" r_h.`id_device_category` LIKE ".$v;// LIKE '%XXXX%' 這樣
                }else{
                    $id_device_out .=" OR r_h.`id_device_category` LIKE ".$v;
                }
            }
            $WHERE .=" AND (".$id_device_out.") ";
        }

        if($id_device_class !=null){    //這樣我先求出該IN的值再來跑裡面的東西
            $id_device_category = "";
            foreach($id_device_class  as $k => $v){
               if(empty($id_device_category)){
                   $id_device_category .= GetSQL($v,"int");
               }else{
                   $id_device_category .= ",".GetSQL($v,"int");
               }
            }//作成1,5,7這樣
          $sql = "SELECT * FROM device_category WHERE id_device_category_class IN({$id_device_category})";
            $row = Db::rowSQL($sql);//取得一串東西

            $id_device_category_like = "";

            foreach($row as $k => $v){
                if(empty($id_device_category_like)){
                    $id_device_category_like .= " r_h.`id_device_category` LIKE '%".$row[$k]["id_device_category"]."%'";// LIKE '%XXXX%' 這樣
                }else{
                    $id_device_category_like .=" OR r_h.`id_device_category` LIKE '%".$row[$k]["id_device_category"]."%'";
                }
            }
//            echo $id_device_category_like;
            $WHERE .=" AND (".$id_device_category_like.") ";
        }



        if($id_other !=null){
            $id_other_like = [];
            foreach($id_other as $k => $v){
                $id_other_like[] = GetSQL('%'.$v.'%',"text");
            }
            $id_other_out ="";
            foreach($id_other_like as $k => $v){
                if($id_other_out==""){
                    $id_other_out .=" r_h.`id_other_conditions` LIKE ".$v;// LIKE '%XXXX%' 這樣
                }else{
                    $id_other_out .=" OR r_h.`id_other_conditions` LIKE ".$v;
                }
            }
            $WHERE .=" AND (".$id_other_out.") ";
        }


        if($max_ping != null || $min_ping != null){//如果有選擇則觸發
            if($max_ping>=6 && $min_ping==0){
                //全選
            }else if($max_ping == $min_ping){//相同處發
                $WHERE .=" AND r_h.`ping` <= ".GetSQL($max_ping,"int");//兩者相同則取以下
            }else{
                if($max_ping != null && $max_ping>=6){

                }else if($max_ping != null){
                    $WHERE .=" AND r_h.`ping` <= ".GetSQL($max_ping,"int");
                }

                if($min_ping != null){
                    $WHERE .=" AND r_h.`ping` >= ".GetSQL($min_ping,"int");
                }
            }
        }

        if($max_price != null || $min_price != null){//如果有選擇則觸發
            if($max_price>=100000 && $max_price==0){//等於全選

            }if($max_price == $min_price){//相同處發
                $WHERE .=" AND r_h.`rent_price_m` <= ".GetSQL($max_price,"int");//兩者相同則取以下
            }else{

                if($max_price != null && $max_price >= 100000){

                }else if($max_price != null){
                    $WHERE .=" AND r_h.`rent_price_m` <= ".GetSQL($max_price,"int");
                }
                if($min_price != null){
                    $WHERE .=" AND r_h.`rent_price_m` >= ".GetSQL($min_price,"int");
                }
            }
        }



        if($max_room != null || $min_room != null){
            if($max_room>=6 && $min_room==0){ //這邊視為全選

            }else{
                if($max_room != null && $max_room >=6){

                }else if($max_room != null){
                    $WHERE .=" AND r_h.`room` <= ".GetSQL($max_room,"int");
                }

                if($min_room != null){//0有時候是null(開放格局)
                    if($min_room !=0){
                        $WHERE .=" AND r_h.`room` >= ".GetSQL($min_room,"int");
                    }
                }
            }
        }


        if($search != null){
            $WHERE .=" AND ( 
                r_h.`rent_house_code`= ".GetSQL($search,"text")." OR 
                r_h.`case_name` LIKE ".GetSQL("%".$search."%","text")." OR
                r_h.`part_address` LIKE ".GetSQL("%".$search."%","text")." OR
                r_h.`community` LIKE ".GetSQL("%".$search."%","text")." OR
                r_h_c.`community` LIKE ".GetSQL("%".$search."%","text")."
             ) ";
        }

        if($house_choose !=null){
            $house_choose = explode(",",$house_choose);//先切成陣列
            $house_choose_like = [];
            foreach($house_choose as $k => $v){
                $house_choose_like[] = GetSQL('%'.$v.'%',"text");
            }
            $house_choose_out ="";
            foreach($house_choose_like as $k => $v){
                if($house_choose_out==""){
                    $house_choose_out .=" r_h.`house_choose` LIKE ".$v;// LIKE '%XXXX%' 這樣
                }else{
                    $house_choose_out .=" OR r_h.`house_choose` LIKE ".$v;
                }
            }
            $WHERE .=" AND (".$house_choose_out.") ";
        }

        if($special!=null){
            $WHERE .=" AND r_h.`featured`=1 ";
        }

        if($free_where!=null){
            $WHERE .= $free_where;
        }

        if($WHERE==''){//啥都沒有
//            $WHERE .=' AND r_h.`featured`=1 ';
            $WHERE = " AND r_h.`active`=1";
        }

        if($limit !=null){
            $limit =' LIMIT '.$limit;
            $num = '';
        }

        $ORDER = ' ORDER BY r_h.`featured` DESC, r_h.`id_rent_house` DESC';

        $sql = "SELECT r_h.`id_rent_house`,r_h.`rent_house_code`,r_h.`title` as rent_house_title,r_h.`price` as rent_house_price,r_h.`kitchen` as kitchen, 
                r_h.`case_name`,r_h.`cleaning_fee`,m.`name` as memeber_name,w.`id_web` as id_web,r_h.`id_member` as id_member,
                m.`user` as member_phone,m.`tel` as member_tel,r_h.`id_other_conditions` as id_other_conditions,
                r_h_t.`title` as type_title,r_h_ts.`title` as types_title,
                r_h.`id_rent_house_types` as id_rent_house_types, r_h.`id_rent_house_type` as id_rent_house_type,
                r_h.`longitude` as longitude,r_h.`latitude` as latitude,
                IF(r_h.`rent_price_m` ,r_h.`rent_price_m`,r_h.`reserve_price_m`) as rent_cash,
                IFNULL((CASE a.`phone` WHEN '' THEN r_h.`ag_phone`
                                    WHEN null THEN r_h.`ag_phone`
                                    ELSE a.`phone`
                                    END ),'0939-688089')as ag_phone,
                    IFNULL((CASE a.`first_name` WHEN ''   THEN r_h.`develop_ag`
                                         WHEN null THEN r_h.`develop_ag`
                                         ELSE CONCAT(a.`last_name`,a.`first_name`)
                                        END ),'房似錦')  as develop_ag,          
        (CASE m.`gender` WHEN '0' THEN '小姐' WHEN '1' THEN '先生' END) as member_gender,
        (CASE r_h.`part_address` WHEN '' THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
            IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,'鄰'),''),
            r_h.`road`,r_h.`segment`,
             IF(r_h.`lane`,CONCAT(r_h.`lane`,'巷'),'')  )
             ELSE r_h.`part_address` 
             END) rent_address,
             r_h_t.`title` as t1,r_h_ts.`title` as t2, 
             r_h.`cleaning_fee_type` as s1,
             r_h.`room` as t3, r_h.`hall` as t4, r_h.`bathroom` as t5, r_h.`muro` as t6,  r_h.`balcony_front` t7,
             r_h.`balcony_back` t8, r_h.`room_balcony` t9,
             r_h.`ping` as ping,
             IF(((r_h.`rental_floor_s` = r_h.`all_floor`) && 
             (r_h.`rental_floor_e` = r_h.`underground` || 
                ((r_h.`rental_floor_e`='' || r_h.`rental_floor_e`='0' || r_h.`rental_floor_e`='1') 
                && (r_h.`underground`='' || r_h.`underground`='0'))
             )),1,0) whole_building,
            (CASE r_h.`rental_floor_e`  WHEN r_h.`rental_floor_s` THEN r_h.`rental_floor_s` 
                                    WHEN ''                   THEN r_h.`rental_floor_s`
                                    ELSE CONCAT(r_h.`rental_floor_s`,'~',r_h.`rental_floor_e`) END) rental_floor,
            r_h.`all_floor` as floor, o_c.`title`, w.`web` AS w_title
                         FROM rent_house AS r_h 
                         LEFT JOIN `web` AS w ON w.`id_web` = r_h.`id_web` 
                         LEFT JOIN `member` AS m ON m.`id_member` = r_h.`id_member`
                         LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                         LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                         LEFT JOIN `rent_house_type` AS r_h_t ON r_h_t.`id_rent_house_type` = r_h.`id_rent_house_type`
                         LEFT JOIN `rent_house_types` AS r_h_ts ON r_h_ts.`id_rent_house_types` = r_h.`id_rent_house_types`
                         LEFT JOIN `device_category` AS d_c ON d_c.`id_device_category` = r_h.`id_device_category`
                         LEFT JOIN `device_category_class` AS d_c_c ON d_c.`id_device_category_class` = d_c_c.`id_device_category_class`
                         LEFT JOIN `rent_house_community` AS r_h_c ON r_h_c.`id_rent_house_community` = r_h.`id_rent_house_community`
                         LEFT JOIN `disgust_facility` AS d_f ON d_f.`id_disgust_facility` = r_h.`id_disgust_facility` 
                         LEFT JOIN `disgust_facility_class` AS d_f_c ON d_f_c.`id_disgust_facility_class` = d_f.`id_disgust_facility_class`
                         LEFT JOIN `other_conditions` AS o_c ON o_c.`id_other_conditions`  = r_h.`id_other_conditions`
                         LEFT JOIN admin as a ON a.ag = r_h.`ag`
                         WHERE TRUE ".$WHERE.$ORDER.$limit;

//        echo $sql.'<br>';
        
        $row = Db::rowSQL($sql, false, $num);
        foreach($row as $i => $v) {
            $row[$i]["rent_cash"] = number_format($row[$i]["rent_cash"]);
        }
        return $row;
    }

    public function commodity_data($id_rent_house=null,$active=null,$num=0,$sreach=null,$order_by=null){
        $WHERE = ' WHERE TRUE ';

        if($id_rent_house !== null){
            $WHERE .= " AND r_h.`id_rent_house`=".GetSQL($id_rent_house,'int');
        }

        if($active !== null){
            $WHERE .= " AND r_h.`active`=".GetSQL($active,'int');
        }

        if($sreach !== null){
            $WHERE .= $sreach;
        }

        $sql = "SELECT r_h.`id_rent_house`,r_h.`title` as title, r_h.`case_name` as case_name,r_h.`kitchen` as kitchen, 
                IF(r_h.`rent_price_m` ,r_h.`rent_price_m`,r_h.`reserve_price_m`) as rent_cash,
                r_h_t.`title` as type_title, r_h.`id_rent_house_types` as id_rent_house_types,
                 r_h_ts.`title` as types_title,r_h.`id_rent_house_type` as id_rent_house_type,
                r_h.`ping` as ping, r_h.`room` as room, r_h.`hall` as hall, r_h.`bathroom` as bathroom, r_h.`muro` as muro,
                IF(((r_h.`rental_floor_s` = r_h.`all_floor`) && 
             (r_h.`rental_floor_e` = r_h.`underground` || 
                ((r_h.`rental_floor_e`='' || r_h.`rental_floor_e`='0' || r_h.`rental_floor_e`='1') 
                && (r_h.`underground`='' || r_h.`underground`='0'))
             )),1,0) whole_building,
             (CASE r_h.`part_address` WHEN '' THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
            IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,'鄰'),''),
            r_h.`road`,r_h.`segment`,
             IF(r_h.`lane`,CONCAT(r_h.`lane`,'巷'),'')  )
             ELSE r_h.`part_address` 
             END) rent_address,
                (CASE r_h.`rental_floor_e`  WHEN r_h.`rental_floor_s` THEN r_h.`rental_floor_s` 
                                    WHEN ''                   THEN r_h.`rental_floor_s`
                                    ELSE CONCAT(r_h.`rental_floor_s`,'~',r_h.`rental_floor_e`) END) rental_floor,
            r_h.`all_floor` as floor
                FROM rent_house as r_h  
                LEFT JOIN `rent_house_type` AS r_h_t ON r_h_t.`id_rent_house_type` = r_h.`id_rent_house_type` 
                LEFT JOIN `rent_house_types` AS r_h_ts ON r_h_ts.`id_rent_house_types` = r_h.`id_rent_house_types`
                LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                LEFT JOIN admin as a ON a.ag = r_h.`ag`
                ".$WHERE.$order_by;

//        echo $sql.'<br/>';

        $row = Db::rowSQL($sql, false, $num);

        $id_member = $_SESSION["id_member"];//favorite要用的
        $session_id = session_id();//favorite要用的
        $favorite_data = "";//favorite要用的

        if(!empty($id_member)){//有
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL("rent_house","text")." AND is_member=".GetSQL($id_member,"int");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個is_member
        }else{//只有session_id
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL("rent_house","text")." AND session_id=".GetSQL($session_id,"text");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個session_id
        }



        foreach($row as $i => $v){

            $row[$i]["rent_cash"] = number_format($row[$i]["rent_cash"]);

            //我的最愛start
            foreach($favorite_data as $key => $value){
                if($row[$i]["id_rent_house"] == $favorite_data[$key]["table_id"]){//上面搜索了該table與本次使用者的資訊 這邊濾同id
                    $row[$i]["favorite"] = $favorite_data[$key]["active"];
                }
            }
            //end

            //這邊要做處理圖片
            $sql = "SELECT * FROM rent_house_file WHERE file_type=0 AND id_rent_house=".$v['id_rent_house'].' ORDER BY id_rhf ASC ';
            $sql_url =  Db::rowSQL($sql, true);
            $row_pic = File::get($sql_url['id_file']);
            $row[$i]['img'] = $row_pic['url'];

            //物件型態
            $sql = "SELECT * FROM rent_house_types WHERE id_rent_house_types IN (".Db::antonym_array($row[$i]['id_rent_house_types']).")";
//            echo $sql.'</br>';
            $sql_types = Db::rowSQL($sql, false);
            $row[$i]['sql_types'] = $sql_types;//整批搜索到的丟進去

            //物件類別
            $sql = "SELECT * FROM rent_house_type WHERE id_rent_house_type IN (".Db::antonym_array($row[$i]['id_rent_house_type']).")";
//            echo $sql.'</br>';
            $sql_type = Db::rowSQL($sql, false);
            $row[$i]['sql_type'] = $sql_type;//整批搜索到的丟進去

        }

        return $row;
    }


//    public static function getFile($id_rent_house){
//        $sql = sprintf('SELECT `id_file`
//				FROM `rent_house_file`
//				WHERE `id_rhf` = %d',
//            GetSQL($id_rent_house, 'int'));
//        $arr_row = Db::rowSQL($sql);
//        $arr = [];
//        foreach($arr_row as $i => $v){
//            $arr[] = $v['id_file'];
//        }
//        return $arr;
//    }
}