<?php

include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');

class AppOkrentController extends FrontController
{
	public $page = 'index';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
//		$this->fields['page']['class'] = '';
		$this->className               = 'AppOkrentController';
		$this->no_FormTable            = true;
//		$this->display_main_menu       = true;
		$this->display_header          = true;
		$this->display_footer          = false;
        $this->fields['page']['cache']   = false;
		parent::__construct();
		$this->no_page = true;
		$this->page_header_toolbar_img = THEME_URL . '/img/APP-logo.svg';

		
	}

	public function initProcess()
	{
        $server_url =  $_SERVER['REQUEST_URI'];
        $buoy_url = $server_url;
        $x = strpos($server_url,"?");
        if(!empty($x)){
            $buoy_url = substr($server_url,0,$x);
        }
        $menu_buoy = [
            [
                'title'=>'房地專區',
                'active' => ['/index','/list','/RentHouse'],
                'url' =>'/index'
            ],
            [
                'title'=>'樂租管理',
                'active' => [''],
                'url' =>'/index'
            ],
            [
                'title'=>'NEWS好康',
                'active' => ['/coupon'],
                'url' =>'/coupon'
            ],
            [
                'title'=>'生活家族',
                'active' => [''],
                'url' =>'/index'
            ]
        ];

		$this->context->smarty->assign([
			'default_career'  => 38,        //租屋業
            'menu_buoy'       => $menu_buoy,
            'buoy_url'        => $buoy_url
		]);
		parent::initProcess();
	}

	public function setMedia()
	{
		$this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
		$this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
		$this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');

		parent::setMedia();
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/css/bootstrap-select.min.css');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/js/bootstrap-select.min.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/js/bootstrap-select.min.js');
		$this->addJS(THEME_URL . '/js/search_store.js');

        $this->addCSS( '/css/fontawesome.min.css');
        $this->addJS( '/js/fontawesome.min.js');
        $this->addJS( '/js/solid.min.js');
        $this->addJS( '/js/regular.min.js');
	}

	public function initToolbar()
	{        //覆蓋
		$this->back_url = '#';
		if ($this->controller_name == 'Index' || $this->controller_name == 'Welcome') {
			$this->back_url = null;
		}
	}
}