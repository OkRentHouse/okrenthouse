<?php


class RentHouse
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new RentHouse();
		};
		return self::$instance;
	}

	public function get()
	{

	}

	/**
	 * 取得物件縣市
	 *
	 * @param string $type none: 取得全部縣市(不管物件有無啟用), on: 取得物件有啟用縣市, off: 取得物件已關閉縣市, all: 取得所有物件縣市
	 *
	 * @return array|mixed|null
	 */
	public function getCounty($type = 'none')
	{
		//todo 全部取得
		switch ($type) {
			case 'on';

			break;
			case 'off';

			break;
			case 'all';

			break;
			case 'none';
			default:
				return County::getContext()->form_list_county();
			break;
		}
	}

	/**
	 * 取得物件城市
	 *
	 * @param string $type none: 取得全部城市(不管物件有無啟用), on: 取得物件有啟用城市, off: 取得物件已關閉城市, all: 取得所有物件城市
	 *
	 * @return array
	 */
	public function getCity($type = 'none')
	{
		//todo 全部取得
		switch ($type) {
			case 'on';

			break;
			case 'off';

			break;
			case 'all';

			break;
			case 'none';
			default:
				return County::getContext()->arr_city;
			break;
		}
	}


	/**
	 * 取得物件用途
	 *
	 * @param string $type none: 取得全部用途(不管物件有無啟用), on: 取得物件有啟用用途, off: 取得物件已關閉用途, all: 取得所有物件用途
	 *
	 * @return array
	 */
	public function getUse($type = 'none')
	{
		//todo 全部取得
		switch ($type) {
			case 'on';
				$WHERE = ' INNER JOIN `rent_house` AS rh ON rh.`id_use` = u.`id_use`
						WHERE rh.`id_rent_house` > 0 AND rh.`active` = 1';
			break;
			case 'off';
				$WHERE = ' INNER JOIN `rent_house` AS rh ON rh.`id_use` = u.`id_use`
						WHERE rh.`id_rent_house` > 0 AND rh.`active` = 0';
			break;
			case 'all';
				$WHERE = ' INNER JOIN `rent_house` AS rh ON rh.`id_use` = u.`id_use`
						WHERE rh.`id_rent_house` > 0';
			break;
			case 'none';
			default:
				$WHERE = '';
			break;
		}
		$sql = 'SELECT u.`id_use`, u.`use`
					FROM `use` AS u
					' . $WHERE . '
					ORDER BY u.`position` ASC';
		$arr = Db::rowSQL($sql);
		return $arr;
	}

	/**
	 * 物件型態
	 * @param int $active
	 * @param string $type
	 *
	 * @return array
	 */
	public function getType($active = 1, $type = 'all', $show=true)
	{
		$WHERE = '';
		if($show){
			$WHERE = ' AND rht.`show` = 1';
		}
		$sql = sprintf('SELECT *
			FROM `rent_house_type` AS rht
			WHERE rht.`active` = %d'.$WHERE.'
			ORDER BY rht.`position` ASC',
			GetSQL($active, 'int'));
		$arr = Db::rowSQL($sql);
        foreach ($arr as $i => $v) {
            //這邊要做處理圖片
            $sql = "SELECT * FROM `rent_house_type_file` WHERE file_type=0 AND id_rent_house_type=" . $v['id_rent_house_type'] . ' ORDER BY id_rhtf DESC ';
            $sql_url = Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $arr[$i]['img'] = $row['url'];
            //圖片end
        }
		return $arr;
	}

	/**
	 * 物件類別
	 * @param int $active
	 * @param string $type
	 *
	 * @return array
	 */
	public function getTypes($active = 1, $type = 'all', $show=true)
	{
		$WHERE = '';
		if($show){
			$WHERE = ' AND rht.`show` = 1';
		}
		$sql = sprintf('SELECT *
			FROM `rent_house_types` AS rht
			WHERE rht.`active` = %d'.$WHERE.'
			ORDER BY rht.`position` ASC',
			GetSQL($active, 'int'));
		$arr = Db::rowSQL($sql);
        foreach ($arr as $i => $v) {
            //這邊要做處理圖片
            $sql = "SELECT * FROM `rent_house_types_file` WHERE file_type=0 AND id_rent_house_types=" . $v['id_rent_house_types'] . ' ORDER BY id_rhtsf DESC ';

            $sql_url = Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $arr[$i]['img'] = $row['url'];
            //圖片end
        }
		return $arr;
	}

    /**
     * 物件主類別
     * @param string $type
     *
     * @return array
     */


	public function getDeviceCategoryClass($type = 'all'){
        $sql = sprintf('SELECT *
			FROM `device_category_class` AS dcc
			ORDER BY dcc.`position` ASC');
        $arr = Db::rowSQL($sql);

        foreach ($arr as $i => $v) {
            //這邊要做處理圖片
            $sql = "SELECT * FROM `device_category_class_file` WHERE file_type=0 AND id_device_category_class=" . $v['id_device_category_class'] . ' ORDER BY id_dccf DESC ';
            $sql_url = Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $arr[$i]['img'] = $row['url'];
            //圖片end
        }
        return $arr;
    }

	/**
	 * 物件類別
	 * @param string $type
	 *
	 * @return array
	 */
	public function getDeviceCategory($type = 'all'){
		$sql = sprintf('SELECT *
			FROM `device_category` AS dc
			ORDER BY dc.`position` ASC');
		$arr = Db::rowSQL($sql);

        foreach ($arr as $i => $v) {
            //這邊要做處理圖片
            $sql = "SELECT * FROM `device_category_file` WHERE file_type=0 AND id_device_category=" . $v['id_device_category'] . ' ORDER BY id_dcf DESC ';
            $sql_url = Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $arr[$i]['img'] = $row['url'];
            //圖片end
        }
		return $arr;
	}


	/**
	 * 其他條件
	 * @param string $type
	 *
	 * @return array
	 */
	public function getOtherConditionsy($type = 'all'){
		$sql = 'SELECT *
			FROM `other_conditions` AS oc
			WHERE oc.`show`=1 ORDER BY oc.`position` ASC';
		$arr = Db::rowSQL($sql);
        foreach ($arr as $i => $v) {
            //這邊要做處理圖片
            $sql = "SELECT * FROM `other_conditions_file` WHERE file_type=0 AND id_other_conditions=" . $v['id_other_conditions'] . ' ORDER BY id_ocf DESC ';
            $sql_url = Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $arr[$i]['img'] = $row['url'];
            //圖片end

            $arr[$i]['id_main_house_class'] = Db::antonym_array($arr[$i]['id_main_house_class'],true);
            
        }




		return $arr;
	}
}