<?php
include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');
class OkmasterController extends FrontController

{
    public function __construct()
    {
        $this->meta_title                = $this->l('生活好科技 科技好生活-各類平台服務優化您的生活品質 l清淨對策I 好幫手生活服務I共享圈 租換平台I智能門鎖 I生活樂購 I生活好康');
        $this->meta_icon='/themes/Okmaster/img/index/logo_footer.png';
        parent::__construct();
        $this->context->smarty->assign([

          'footer_link'   => [
              [
                  'txt'  => '生活集團',
                  'link' => 'https://www.lifegroup.house/',
                  'img' =>  '/themes/Rent/img/index/logo-b-06.png',
              ],
              [
                  'txt'  => '生活房屋',
                  'link' => 'https://www.life-house.tw',
                  'img' =>  '/themes/Rent/img/index/logo-b-06.png',
              ],
              [
                  'txt'  => '生活好科技',
                  'link' => '',
                  'img' =>  '/themes/Rent/img/index/logo-b-06.png',
              ],
              [
                  'txt'  => '生活居家修繕',
                  'link' => '',
                  'img' =>  '/themes/Rent/img/index/logo-b-06.png',
              ],
              [
                  'txt'  => '生活樂購',
                  'link' => 'https://www.lifegroup.house/LifeGo',
                  'img' =>  '/themes/Rent/img/index/logo-b-06.png',
              ],
              [
                  'txt'  => '共好夥伴',
                  'link' => '',
                  'img' =>  '/themes/Rent/img/index/logo-b-06.png',
              ],
              [
                  'txt'  => '相關聲明',
                  'link' => '/statement',
                  'img' =>  '/themes/Rent/img/index/logo-b-06.png',
              ],
          ],

        ]);
    }

    public function initProcess(){

        $footer_txt1='';
        $footer_txt2='生活好科技有限公司   LIFE  MASTER  TECHNOLOGY  COMPANY   生活好科技·科技好生活';
        $footer_txt3='';
        $sql = "SELECT * FROM epro_windows WHERE active=1 ORDER BY position ASC,id_epro_windows ASC";
        $windows_data = Db::rowSQL($sql);
        foreach($windows_data as $i => $v){
            $sql = "SELECT * FROM epro_windows_file WHERE file_type=0 AND id_epro_windows=".$v["id_epro_windows"].' ORDER BY id_epro_windows_file ASC LIMIT 1';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $windows_data[$i]['img'] = $row['url'];
        }
        $windows_count = count($windows_data)-1;
//        print_r($windows_data);

        $this->context->smarty->assign([
            'footer_txt1'=>$footer_txt1,
            'footer_txt2'=>$footer_txt2,
            'footer_txt3'=>$footer_txt3,
            'windows_data'=>$windows_data,
            'windows_count'=>$windows_count,
        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/footer.css');
    }
}
