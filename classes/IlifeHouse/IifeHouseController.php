<?php
include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');
class IifeHouseController extends FrontController

{
	public function __construct()
	{
		parent::__construct();
		$this->context->smarty->assign([
			'footer_link'   => [
				[
					'txt'  => '生活集團',
					'link' => 'https://www.lifegroup.house/',
				],
				[
					'txt'  => '生活房屋',
					'link' => 'https://www.life-house.tw',
				],
				[
					'txt'  => '生活好科技',
					'link' => '',
				],
				[
					'txt'  => '生活居家修繕',
					'link' => '',
				],
				[
					'txt'  => '生活樂購',
					'link' => 'https://www.lifegroup.house/LifeGo',
				],
				[
					'txt'  => '共好夥伴',
					'link' => '',
				],
                [
					'txt'  => '相關聲明',
					'link' => '/statement',
				],

//                [
//                    'txt'  => '友好分享',
//                    'link' => '',
//                ],
			],

			'footer_txt1'   => Config::get('footer_txt1'),
			'footer_txt2'   => Config::get('footer_txt2'),
			'footer_txt3'   => Config::get('footer_txt3'),
			'footer_qrcode' => 'https://www.life-house.com.tw',
			//			'qrcod_img'     => '/img/okrent/footer_qr_code.png',
		]);

	}



	public function initProcess()

	{
		$arr_main_menu = [
			'a' => [
				'text'    => $this->l('吉屋快搜'),
				'href'    => '/list',
				'submenu' => [
					[
						'text' => $this->l('條件搜尋'),
						'href' => '/list',
					],
					[
						'text' => $this->l('地圖搜尋'),
						'href' => '/HouseMap',
					],
				],
			],

			'b' => [
				'text'    => $this->l('樂租服務'),
				'href'    => '/tenant',
				'submenu' => [
					[
						'text' => $this->l('　租客　'),
						'href' => '/tenant',
					],

					[
						'text' => $this->l('　房東　'),
						'href' => '/landlord',
					],
				],
			],

			'c' => [
				'text'    => $this->l('租事順利'),
				'href'    => '/tax_benefit',
			],

			'd' => [
				'text' => $this->l('新聞資訊'),
				'href' => '/geely_for_rent',
			],

			'e' => [
				'text' => $this->l('in生活'),
				'href' => '/in_life_index',
			],

			'f' => [
				'text'    => $this->l('加入生活'),
				'href'    => '/JoinLife',
				'submenu' => [
					[
						'text' => $this->l('生活家族'),
						'href' => '/ExpertAdvisor',
					],
                    [
                        'text' => $this->l('菁英招募'),
                        'href' => '/Joinrenthouse',
                    ],
					[
						'text' => $this->l('加盟樂租'),
						'href' => '#',
					],
				],
			],

			'g' => [
				'text'    => $this->l('關於生活'),
				'href'    => '/about',
			],

//            'h' => [
//                'text'    => $this->l('友好分享'),
//                'href'    => '/',
//            ],
		];


		$arr           = GeelyForRent::getContext()->getType(1);



		$arr_cmd = [];
        $sql = "SELECT title,url,id_cms FROM `cms` WHERE `related_tabs`='1' AND id_cms !=12 AND active=1 AND id_web=4 ORDER BY position ASC";
        $arr_sql = Db::rowSQL($sql);

        foreach($arr_sql as $i=>$v){
            $arr_cmd[]=[
            'text' => $v['title'],
            'href' => '/'.$v['url'],
                ];
        }
        $arr_main_menu['c']['submenu'] = $arr_cmd;

		$arr_gfr       = [];

		foreach ($arr as $i => $v) {
			$arr_gfr[] = [
				'text' => $v['title'],
				'href' => '/geely_for_rent?id_type=' . $v['id_geely_for_rent_type'],
			];
		}

		$arr_main_menu['d']['submenu'] = $arr_gfr;

		$arr_main_menu['e']['submenu'] = [
			[
				'text' => '活動分享',
				'href' => '/in_life',
			],

            [
                'text' => '創富分享',
                'href' => '/WealthSharing',
            ],

            [
				'text' => '生活好康',
				'href' => '/Store',
			],

			[
				'text' => '生活樂購',
				'href' => 'https://www.lifegroup.house/LifeGo',
			],
		];

//        $arr_main_menu['h']['submenu'] = [
//            [
//                'text' => '聯絡我們',
//                'href' => '/',
//                'class' =>'icon_block',
//            ],
//
//            [
//                'text' => 'APP下載',
//                'href' => '/',
//                'class' =>'icon_block open',
//            ],
//
//            [
//                'text' => 'QR Code',
//                'href' => '/',
//                'class' =>'icon_block',
//            ],
//
//            [
//                'text' => 'FB分享',
//                'href' => '',
//                'class' =>'icon_block FB',
//            ],
//
//            [
//                'text' => 'Line分享',
//                'href' => '',
//                'class' =>'icon_block LINE',
//            ],
//
//            [
//                'text' => '寄給朋友',
//                'href' => '',
//                'class' =>'icon_block MAIL',
//            ],
//        ];
//給顏色start
        $server_url =  $_SERVER['REQUEST_URI'];
        foreach($arr_main_menu as $key => $value){

            if($arr_main_menu[$key]['href'] == $server_url){
                $arr_main_menu[$key]['color'] =  'color';
            }


            if(!empty($arr_main_menu[$key]['submenu'])){
                foreach($arr_main_menu[$key]['submenu'] as $k => $v){
                    if($server_url==$v['href']){
                        if($v['href'] == $arr_main_menu[$key]['href']){
                            break;
                        }
                        $arr_main_menu[$key]['submenu'][$k]['color'] = 'color';
                        $arr_main_menu[$key]['color'] =  'color';
                    }
                    if($arr_main_menu[$key]['submenu'][$k]['href'] =='/Store' &&
                        ($server_url=='/GoodMember' || $server_url=='/GoodStore')){//特例因為好康店家跟會員不會顯示在上面
                        $arr_main_menu[$key]['submenu'][$k]['color'] = 'color';
                        $arr_main_menu[$key]['color'] =  'color';
                    }
//                    echo $arr_main_menu[$key]['submenu'][$k]['href'].'<br>';
//                   echo  $v['href'].'<br>';
                }
            }
//            echo $arr_main_menu[$value]['submenu']['href'].'<br>';
//           echo $key.'=>'.$value.'<br>';
        }


//        print_r($arr_main_menu);


//        echo $server_url;
//        //給顏色end


		//價位

		$default_min_price  = 1;

		$default_max_price  = 30000;


//		if (empty($_GET['min_price_v'])) {
//			$_GET['min_price'] = $default_min_price;
//		}
//		if (empty($_GET['max_price'])) {
//			$_GET['max_price'] = $default_max_price;
//		}

		//坪數
		$default_min_ping  = 15;
		$default_max_ping  = 35;

//		if (empty($_GET['min_ping'])) {
//			$_GET['min_ping'] = $default_min_ping;
//		}
//		if (empty($_GET['max_ping'])) {
//			$_GET['max_ping'] = $default_max_ping;
//		}
		//房型
		$default_min_room = 0;
		$default_max_room = 6;
//		if (empty($_GET['min_room']) && empty($_GET['min_room'])) {
//			$_GET['min_room'] = $default_min_room;
//			$_GET['max_room'] = $default_max_room;
//		}

		//縣市

//		$default_county = '桃園市';
//		$default_city   = '桃園區';
//		if (empty($_GET['county'])) {
//			$_GET['county'] = $default_county;
//			$_GET['city']   = $default_city;
//		}



		$this->main_menu = $arr_main_menu;

		$this->context->smarty->assign([
			'default_county' => '桃園市',
			'default_city'   => '桃園區',
			'default_type'   => 18,
			'default_types'  => 1,
			'default_career' => 38,        //租屋業
			//			'arr_news_t'     => GeelyForRent::getContext()->getType(1),
			//			'arr_if_t'       => InLife::getContext()->getType(1),
		]);
		parent::initProcess();
	}



	public function setMedia()
	{
		$this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
		$this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
		$this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS('/themes/Rent/js/gtag.js');
		parent::setMedia();
        $this->addCSS('/themes/Rent/css/goto_header.css');
//		$this->addCSS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/css/bootstrap-select.min.css');
//		$this->addJS('/' . MEDIA_URL . '/bootstrap-select-1.13.9/js/bootstrap-select.min.js');
	}
	/**
	 * 加入我的最愛
	 */
	public function ajaxProcessAddFavorite()
	{
		$id   = Tools::getValue('id');
		$type = Tools::getValue('type');
		$on   = Tools::getValue('on');
		switch ($type) {
			case 'house':    //收藏房屋
			break;
			case 'store':    //收藏店家
			break;
		}
	}
}
