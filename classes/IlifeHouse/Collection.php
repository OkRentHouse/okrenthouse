<?php


class Collection
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Collection();
		};
		return self::$instance;
	}

	public function getList(){

	}

	public function get($id_type=null, $id=null, $active=null, $date=null, $num=false, $top=false){
		$ORDER = 'ORDER BY c.`date` DESC';
		if($top){
			$ORDER = ' ORDER BY c.`date` DESC';
		}else{
			$ORDER = ' ORDER BY c.`top` DESC, c.`date` DESC';
		}
		$WHERE = '';
		if($active !== null){
			$WHERE .= sprintf(' AND c.`active` = %d', GetSQL($active, 'int'));
		}
		if($date !== null){
			$WHERE .= sprintf(' AND c.`date` <= %s', GetSQL($date, 'text'));
		}
		if(!empty($id)){
			$WHERE.= sprintf(' AND c.`id_collection` = %d', GetSQL($id, 'int'));
		}
		if(!empty($id_type)){
			$WHERE.= sprintf(' AND c.`id_collection_type` = %d', GetSQL($id_type, 'int'));
		}

		$sql = 'SELECT c.`id_collection`, c.`id_collection_type`, c.`date`, c.`top`, c.`title`, c.`author`, c.`source`, c.`content`, c.`description`, c.`keywords`, c.`views`, c.`active`,
				ct.`title` AS type,
				cf.`id_file`
				FROM `collection` AS c 
				INNER JOIN `collection_type` AS ct ON ct.`id_collection_type` = c.`id_collection_type`
				LEFT JOIN `collection_file` AS cf ON cf.`id_collection` = c.`id_collection`
				WHERE TRUE'.$WHERE.'
				GROUP BY c.`id_collection`
				'.$ORDER;
		$row = Db::rowSQL($sql, false, $num);
		return $row;
	}

	public function getType($active = false){
		if($active){
			$WHERE = ' AND `active` = 1';
		}
		$sql = 'SELECT *
			FROM `collection_type`
			WHERE true '.$WHERE.'
			ORDER BY `position` ASC
			';
		$row = Db::rowSQL($sql);
		return $row;
	}

	public function setViews($id){
		$sql = sprintf(' UPDATE `collection`
				SET `views` = 0
				WHERE `id_collection` = %d
				AND `views` IS NULL',
			GetSQL($id, 'int'));
		Db::rowSQL($sql);

		$sql = sprintf(' UPDATE `collection`
				SET `views` = `views` + 1
				WHERE `id_collection` = %d',
		GetSQL($id, 'int'));
		Db::rowSQL($sql);
		return (Db::getContext()->num()) ? true : false;
	}

	public static function getFile($id_collection){
		$sql = sprintf('SELECT `id_file`
				FROM `collection_file`
				WHERE `id_collection` = %d',
			GetSQL($id_collection, 'int'));
		$arr_row = Db::rowSQL($sql);
		$arr = [];
		foreach($arr_row as $i => $v){
			$arr[] = $v['id_file'];
		}
		return $arr;
	}
}