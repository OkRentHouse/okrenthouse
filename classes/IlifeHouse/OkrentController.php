<?php
include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');
class OkrentController extends FrontController
{
    public $fb_app_id ='1319773841697579';//fb登入的id
    public $fb_api_version='v9.0';//fb登入的版本
    public function __construct()
    {
        $this->display_header            = true;//首頁不用共用表頭
        $this->meta_title                = $this->l('租換共享圈|物品 租借．收租．交換．社交．公益捐贈．資源環保|何必買?! 以租代買 共享效益多 環保又經濟|出租閒置物品 多了淨空空間 輕鬆賺取被動式收入|以物會友 拓展社交圈|公益捐贈 發揮愛心做公益 愛護地球做環保');
        $this->conn = 'ilifehou_okrent';

        if($_SERVER['HTTP_HOST'] =='www.okrent.tw'|| $_SERVER['HTTP_HOST'] =='okrent.tw'){
            header('Location: https://shar.help');
            exit;
        }

        parent::__construct();
    }

    public function initProcess(){
        $id_main_class = Tools::getValue("id_main_class");
        $county = Tools::getValue("county");
        $city = Tools::getValue("city");
        $id_product_class = Tools::getValue("id_product_class");
        $id_product_class_item = Tools::getValue("id_product_class_item");
        $keyword = Tools::getValue("keyword");
        //for header用的


        $sql = "SELECT * FROM main_class WHERE active=1 ORDER BY position ASC";
        $main_class = Db::rowSQL($sql, false,0,$this->conn);

        $id_main_class_sql = empty($id_main_class)?"":"AND id_main_class LIKE ".GetSQL('%'.$id_main_class.'%','text');

        $sql = "SELECT * FROM `product_class` WHERE active=1 {$id_main_class_sql} ORDER BY position ASC";
        $head_product_class = Db::rowSQL($sql, false,0,$this->conn);
        if(!empty($id_product_class)){//是否選擇大類別
            $sql = "SELECT * FROM `product_class_item` WHERE active=1 AND id_product_class=".GetSQL($id_product_class,"int")." {$id_main_class_sql}  ORDER BY position ASC";
            $product_class_item = Db::rowSQL($sql, false,0,$this->conn);
        }

        //後面用模組代出
        $other_look_product_1125 = [
            [
                'img'=>'/themes/Okrent/img/index/1090918_13.png',
                'name'=>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組內 組裝簡易內裝組裝簡易內裝組內',
            ],
            [
                'img'=>'/themes/Okrent/img/index/1090918_13.png',
                'name'=>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組內 組裝簡易內裝組裝簡易內裝組內',
            ],
        ];


        $other_look_product = [
            [
                'img'=>'/themes/Okrent/img/list/camara2_im2x.png',
                'name'=>'類單眼相機',
                'title'=>'',
            ],
            [
                'img'=>'/themes/Okrent/img/list/camara2_im2x.png',
                'name'=>'類單眼相機',
                'title'=>'',
            ],
            [
                'img'=>'/themes/Okrent/img/list/camara2_im2x.png',
                'name'=>'類單眼相機',
                'title'=>'',
            ],
            [
                'img'=>'/themes/Okrent/img/list/camara2_im2x.png',
                'name'=>'類單眼相機',
                'title'=>'',
            ],
        ];


        $foor_tet = '<img src="themes/LifeHouse/img/lifego/logo-b-03_onlyLogo.png">生活好科技有限公司 &nbsp; <b>LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</b>';
        $foor_tet = "<div class='footer' style='width: 100%;'><a href='/Aboutokmaster'><img src='/themes/Okmaster/img/index/footer_logo.png'></a>生活好科技有限公司 <span class='spacing'></span> copyright ©LIFE MASTER TECHNOLOGY COMPANY <span class='spacing'></span> <!--<a style='margin-left: 20%;' href='https://www.okmaster.life/Aboutokmaster'>合作提案</a> |--> <a href='/State'>相關聲明</a></div>";
        $foor_tet2 = '生活集團版權所有&nbsp; copyright <i class="far fa-copyright"></i> Life Group 2000 All right reserde';
        $foor_tet2 = '';

        $this->context->smarty->assign([
            'group_link' => 1,
            'head_product_class'=>$head_product_class,
            'head_main_class'=>$main_class,
            'head_product_class_item'=>$product_class_item,
            'footer_txt' =>  $foor_tet,
            'footer_txt2'  => $foor_tet2,
            'other_look_product'=>$other_look_product,
        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        parent::setMedia();
        $this->addJS('/js/slick.min.js');
        $this->addJS('/media/uikit-3.2.0/js/uikit.min.js');
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');
        $this->addCSS('/media/uikit-3.2.0/css/uikit.min.css');
        $this->addJS(THEME_URL .'/js/list_header.js');
        $this->addCSS(THEME_URL .'/css/page_left.css');
    }
}
