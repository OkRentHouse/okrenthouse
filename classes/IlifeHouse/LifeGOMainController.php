<?php
include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');

class LifeGOMainController extends FrontController{
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;
    public $display_header = false;
    public $display_footer = false;
    public $lifego_left = true;//控制左側選單


    public function __construct(){
        $this->conn = 'ilifehou_life_go';
        $this->meta_icon ='/themes/LifeHouse/img/lifego/favicon.png';//請填入網址icon
        $this->WEB_NAME = '聰明消費 精采生活 享紅利回饋 新奇豐富 • 實惠好康 盡在生活樂購';
        parent::__construct();
    }

    public function initProcess(){
        $id_product_class = Tools::getValue("id_product_class");
        $id_product_class_item = Tools::getValue("id_product_class_item");

        $sql = "SELECT * FROM product_class WHERE active=1 ORDER BY position ASC";

        $product_class_arr = Db::rowSQL($sql, false,0,$this->conn);
        foreach($product_class_arr as $i => $v){
            $sql = "SELECT * FROM product_class_file WHERE file_type=0 AND id_product_class=".$v['id_product_class'].' ORDER BY id_pcf ASC ';
            $sql_url =  Db::rowSQL($sql, true,0,$this->conn);
            $row = File::get($sql_url['id_file']);
            $product_class_arr[$i]['img'] = $row['url'];

            $sql = "SELECT * FROM product_class_file WHERE file_type=1 AND id_product_class=".$v['id_product_class'].' ORDER BY id_pcf ASC ';
            $sql_url =  Db::rowSQL($sql, true,0,$this->conn);
            $row = File::get($sql_url['id_file']);
            $product_class_arr[$i]['img_header'] = $row['url'];
        }

//        print_r($product_class_arr);

        $header_css = 100/(count($product_class_arr));
        $header_css = '.header_img{width:'.$header_css.'%;}';


        if(!empty($id_product_class)){
            $sql = "SELECT id_product_class,title FROM product_class WHERE active=1 AND id_product_class=".$id_product_class;
            $product_class_title = Db::rowSQL($sql,true,0,$this->conn);
            //取得file
            $sql = "SELECT * FROM product_class_file WHERE file_type=0 AND id_product_class=".$product_class_title['id_product_class'].' ORDER BY id_pcf ASC ';
            $sql_url =  Db::rowSQL($sql, true,0,$this->conn);
            $row = File::get($sql_url['id_file']);
            $product_class_title['img'] = $row['url'];

            $sql = "SELECT *,p_c_i.title as item_title FROM product_class_item AS p_c_i
            LEFT JOIN product_class AS p_c ON p_c_i.`id_product_class` = p_c.`id_product_class`
            WHERE p_c_i.`active`=1 AND p_c.`active`=1 AND p_c_i.`id_product_class`=".$id_product_class;
            $product_class_item_arr = Db::rowSQL($sql,false,0,$this->conn);
        }
        $left_bottom='';
        //預設
       $left_bottom = [
           [
               'title' =>'精選',
               'img'   =>'/themes/LifeHouse/img/lifego/left_01.png',
               'url'   =>'####',
           ],
           [
               'title' =>'回購',
               'img'   =>'/themes/LifeHouse/img/lifego/left_02.png',
               'url'   =>'####',
           ],
           // [
           //     'title' =>'熱購',
           //     'img'   =>'/themes/LifeHouse/img/lifego/left_03.png',
           //     'url'   =>'####',
           // ],
           // [
           //     'title' =>'滿額送',
           //     'img'   =>'/themes/LifeHouse/img/lifego/left_04.png',
           //     'url'   =>'####',
           // ],

       ];

        $marquee = [
            'img'=>'/themes/LifeHouse/img/lifego/star.svg',
            'title'=>'歡慶聖誕佳節　全館限時優惠',
        ];


        $group_link_data =[
            [
                'url'=>'https://www.lifegroup.house/',
                'img'=>'/img/logos/logo-b-01.png',
            ],
            [
                'url'=>'https://www.okmaster.life/',
                'img'=>'/img/logos/logo-b-03.png',
            ],
            [
                'url'=>'https://www.okmaster.life/',
                'img'=>'/img/logos/logo-b-03.png',
            ],
            [
                'url'=>'#',
                'img'=>'/img/logos/logo-b-04.png',
            ],
            [
                'url'=>'#',
                'img'=>'/img/logos/logo-b-05.png',
            ],
            [
                'url'=>'#',
                'img'=>'/img/logos/logo-b-06.png',
            ],
            [
                'url'=>'https://www.okpt.life',
                'img'=>'/img/logos/logo-b-07.png',
            ],
            [
                'url'=>'https://www.okrent.tw',
                'img'=>'/img/logos/logo-b-08.png',
            ],
            [
                'url'=>'https://www.epro.house',
                'img'=>'/img/logos/logo-b-09.png',
            ],
        ];

        $footer_txt1 = '<img src="/themes/LifeHouse/img/lifego/logo-b-03_onlyLogo.png">';
        $footer_txt2 = '生活好科技有限公司 LIFE MASTER TECHNOLOGY COMPANY &nbsp &nbsp &nbsp &nbsp <span>相關聲明</span>';
        $footer_txt3 = '<b>生活集團版權所有 Copyright <i class="far fa-copyright"></i> Life Group 2000 ALL right reserde</b>';

        $this->context->smarty->assign([
            'header_css'        => $header_css,
            'product_class_arr' => $product_class_arr,
            'product_class_item_arr' => $product_class_item_arr,//暫時不用
            'product_class_title' => $product_class_title,
            'marquee'           => $marquee,
            'lifego_left'=>$this->lifego_left,
            'left_bottom'=>$left_bottom,
            'footer_txt1'=>$footer_txt1,
            'footer_txt2'=>$footer_txt2,
            'footer_txt3'=>$footer_txt3,
            'group_link'        => 1,
//            'group_link_data'  =>$group_link_data,
        ]);

        parent::initProcess();
    }

    public function setMedia(){
        parent::setMedia();
        $id_product_class = Tools::getValue("id_product_class");
//        $this->addCSS('/css/slick.css');
//        $this->addCSS('/css/slick-theme.css');
//        $this->addJS('/js/slick.min.js');
//        $this->addCSS('/css/fontawesome.css');
//        $this->addCSS('/css/all.css');
        $this->addCSS('/css/bootstrap-grid.min.css');

        $this->addCSS(THEME_URL . '/css/life_go_main.css');
        $this->addCSS(THEME_URL . '/css/life_go_tab.css');
        if(!empty($id_product_class)){
          $this->addCSS(THEME_URL . '/css/LifeGo_product_class.css');
        }
    }
}
