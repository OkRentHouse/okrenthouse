<?php


class Store
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';
	public $arr_type = [
		'logo',
		'photo',
		'product',
		'application',
		'contrat',
	];


	public function __construct()
	{

//        print_r($_GET);

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Store();
		};
		return self::$instance;
	}

	public function get_store_file_url($id_store, $type=1){
		$arr_file = [];
		if(in_array($type, [0, 1, 2])){		//(0:商標LOGO, 1:店家圖檔, 2:產品服務介紹圖檔, 3:申請書, 4:契約書)
			$sql = sprintf('SELECT f.`file_url`, f.`filename`
				FROM `store_file` AS sf
				LEFT JOIN `file` AS f ON f.`id_file` = sf.`id_file`
				WHERE sf.`id_store` = %d
				AND sf.`id_type` = %d',
					GetSQL($id_store, 'int'),
					GetSQL($type, 'int'));
			$row = Db::rowSQL($sql);
			foreach($row as $i => $v){
				$arr_file[] = urldecode($v['file_url']).$v['filename'];
			}
			return $arr_file;
		}
		return false;
	}

	public function get($id_store=null, $active=null, $num=0, $county=null, $area=null, $type=null, $order=null, $title=null){
		$WHERE = '';
		if($active !== null){
			$WHERE .= sprintf(' AND s.`active` = %d', GetSQL($active, 'int'));
		}
		if($id_store !== null){
			$WHERE.= sprintf(' AND s.`id_store` = %d', GetSQL($id_store, 'int'));
		}
		if(!empty($county)){
			$WHERE.= sprintf(' AND s.`county` = %s', GetSQL($county, 'text'));
		}
		if(!empty($area)){//未切割 必須自己切割
            $area_arr = explode(',',$area);
		    $area_sql = '';
		    foreach($area_arr as $k => $v){
		        if(empty($area_sql)){
                    $area_sql = GetSQL($v, 'text');
                }else{
                    $area_sql = $area_sql.','.GetSQL($v, 'text');
                }
            }
            $WHERE.=  " AND s.`area` IN ({$area_sql})";
//			$WHERE.= sprintf(' AND s.`area` IN (%s)', GetSQL($area, 'text'));
		}
		if(!empty($type)){
			$WHERE.= " AND s.`id_store_type` IN ({$type})";
		}

        if (!empty($title)) {   //新增的
            $title = '%'.$title.'%';
            $WHERE .= sprintf(' AND s.`title` LIKE %s', GetSQL($title, 'text'));
        }

        if($order == 'head'){
            $order = ' ORDER BY s.`top` DESC, if(s.`sort` > 0, 0, 1) ASC, s.`sort` ASC, s.`id_store` DESC';
        }else if($order == 'news'){
			$order = ' ORDER BY s.`id_store` DESC';
		}else{
			$order = ' ORDER BY if(s.`sort` > 0, 0, 1) ASC, s.`sort` ASC, s.`id_store` DESC';
		}

		$sql = 'SELECT *
			FROM `store` AS s
			LEFT JOIN `store_type` AS st ON st.`id_store_type` = s.`id_store_type`
			WHERE TRUE '.$WHERE.$order;

//        echo $sql.'<br/>';

//		$sql = sprintf('SELECT *
//			FROM `store` AS s
//			LEFT JOIN `store_type` AS st ON st.`id_store_type` = s.`id_store_type`
//			WHERE TRUE'.$WHERE.$order);

        $row = Db::rowSQL($sql, false, $num);
        return $row;

//		$db = new db_mysql($sql, $num);
//		$arr_d = array();
//		if($db->num() > 0){
//			if(!$id_store){
//				while($d = $db->next_row()){
//					$arr_d[] = $d;
//				};
//			}else{
//				return $db->row();
//			};
//		};
//		return $arr_d;
	}

	/**
	 * 取得商家資訊
	 * @param $id_store			商家ID
	 * @param null $id_type		商家附檔類別	(0:商標LOGO, 1:店家圖檔, 2:產品服務介紹圖檔, 3:申請書, 4:契約書)
	 *
	 * @return array
	 */
	public static function getFile($id_store, $id_type=null){
		$WHERE = '';
		if($id_type !== null){
			$WHERE = sprintf(' AND `id_type` = %d',
				GetSQL($id_type, 'int'));
		}
		$sql = sprintf('SELECT `id_file`, `id_type`
				FROM `store_file`
				WHERE `id_store` = %d'.$WHERE,
			GetSQL($id_store, 'int'));
		$arr_row = Db::rowSQL($sql);
		$arr = [];
		foreach($arr_row as $i => $v){
			$arr[$v['id_file']] = $v['id_type'];
		}
		return $arr;
	}

	/**
	 * 取得星星評價
	 *
	 * @param $id_store
	 *
	 * @param null $id_member
	 *
	 * @return array
	 */
	public function get_popularity($id_store, $id_member=null){
		$WHERE = '';
		if($id_member > 0){
			$WHERE = sprintf(' AND `id_member` = %d'.GetSQL($id_member, 'int'));
		}
		$sql = sprintf('SELECT *
			FROM `store_popularity`
			WHERE `id_store` = %d'.$WHERE,
			GetSQL($id_store, 'int'));
		$arr = Db::rowSQL($sql);
		return $arr;
	}

	/**
	 * 取得星星評價個數值
	 * @param $id_store
	 *
	 * @return array
	 */
	public function get_popularity_val($id_store){
		$arr = $this->get_popularity($id_store);
		$arr_popularity_val = [
			'all' => 0,
			'1' => 0,
			'2' => 0,
			'3' => 0,
			'4' => 0,
			'5' => 0,
		];
		$num = 0;
		$total = 0;
		foreach($arr as $i => $v){
			$total += $v['popularity'];
			$arr_popularity_val[$v['popularity']] ++;
			$num++;
		}
		if($total > 0){
			$arr_popularity_val['all'] = round(($total/$num), 1);
		}
		return $arr_popularity_val;
	}

	/**
	 * 取得店家類別
	 * @param null $active
	 *
	 * @return array
	 */
	public function getStoreType($active = null){
		$WHERE = '';
		$arr = [];

		if($active != null){
			$WHERE = sprintf(' AND s.`active` = %d', GetSQL($active, 'int'));
		}
		$arr_county = County::getContext()->arr_county;
		$arr_county_key = array_flip($arr_county);
		$FIELD = implode('\', \'', $arr_county);
		$sql = sprintf('SELECT s.`id_store_type`, st.`store_type`
			FROM `store` AS s
			INNER JOIN  `store_type` AS st ON  st.`id_store_type` =  s.`id_store_type`
			WHERE TRUE'.$WHERE.'
			GROUP BY s.`id_store_type`
			ORDER BY st.`position` ASC');
		$row = Db::rowSQL($sql);
		foreach($row as $i => $v){
			$arr[] = [
				'class' => 'store_type_' . $v['id_store_type'],
				'value' => $v['id_store_type'],
				'title' => $v['store_type'],
			];
		}
		return $arr;
	}

	/**
	 * 取的縣市
	 * @param null $active
	 *
	 * @return array
	 */
	public function getCounty($active = null, $county=null, $area=null){
		$Cache = Cache::get('Store_getCounty_'.$active);
		if (!empty($Cache)) {
			return $Cache;
		}
		$WHERE = '';
		$arr = [];
		if($active != null){
			$WHERE .= sprintf(' AND `active` = %d', GetSQL($active, 'int'));
		}
		$arr_county = County::getContext()->arr_county;
		$arr_county_key = array_flip($arr_county);
		$FIELD = implode('\', \'', $arr_county);
		$sql = sprintf('SELECT `county`
			FROM `store`
			WHERE TRUE'.$WHERE.'
			GROUP BY `county`
			ORDER BY FIELD(`county`,\''.$FIELD.'\')');
		$row = Db::rowSQL($sql);
		foreach($row as $i => $v){
			$arr[] = [
				'value' => $v['county'],
				'title' => $v['county'],
			];
		}
		Cache::set('Store_getCounty_'.$active, $arr);
		return $arr;
	}

	/**
	 * 取得行政區
	 * @param null $active
	 * @param string $parent_name
	 *
	 * @return array|mixed|null
	 */
	public function get_city($active = null, $parent_name='county'){
		$Cache = Cache::get('Store_get_area_'.$active);
		if (!empty($Cache)) {
			return $Cache;
		}
		$WHERE = '';
		$arr = [];
		if($active != null){
			$WHERE .= sprintf(' AND `active` = %d', GetSQL($active, 'int'));
		}

		$arr_county = County::getContext()->arr_county;
		$FIELD = implode('\', \'', $arr_county);
		$arr_district = County::getContext()->arr_district;
		$arr_area = [];
		foreach($arr_district as $county => $area){
			$arr_area[] = $area;
		}
		$FIELD2 = implode('\', \'', $arr_area);
		$sql = sprintf('SELECT `county`, `area`
			FROM `store`
			WHERE TRUE'.$WHERE.'
			GROUP BY `county`, `area`
			ORDER BY FIELD(`county`,\''.$FIELD.'\'), FIELD(`area`,\''.$FIELD2.'\')');
		$row = Db::rowSQL($sql);

		foreach($row as $i => $v){
			if(!empty($v['area'])){
				$title = $v['area'];
				$arr[] = [
					'value'       => $v['area'],
					'parent_name' => $parent_name,
					'parent'      => $v['county'],
					'title'       => $v['area'],
				];
			}
		}
		Cache::set('Store_get_area_'.$active, $arr);
		return $arr;
	}

	public function addView($id_store){
		$sql = sprintf('UPDATE `store` SET `views` = `views` + 1 WHERE `id_store` = %d',
			GetSQL($id_store, 'int'));
		Db::rowSQL($sql);
	}
}