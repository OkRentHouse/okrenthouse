<?php
class GeelyForRent
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new GeelyForRent();
		};
		return self::$instance;
	}

	public function getList(){

	}

	public function get($id_type=null, $id=null, $active=null, $date=null, $num=false, $top=false){
		$ORDER = 'ORDER BY gfr.`date` DESC';
		if($top){
			$ORDER = ' ORDER BY gfr.`date` DESC';
		}else{
			$ORDER = ' ORDER BY gfr.`top` DESC, gfr.`date` DESC';
		}
		$WHERE = '';
		if($active !== null){
			$WHERE .= sprintf(' AND gfr.`active` = %d', GetSQL($active, 'int'));
		}
		if($date !== null){
			$WHERE .= sprintf(' AND gfr.`date` <= %s', GetSQL($date, 'text'));
		}
		if(!empty($id)){
			$WHERE.= sprintf(' AND gfr.`id_geely_for_rent` = %d', GetSQL($id, 'int'));
		}
		if(!empty($id_type)){
			$WHERE.= sprintf(' AND gfr.`id_geely_for_rent_type` = %d', GetSQL($id_type, 'int'));
		}

		$sql = 'SELECT gfr.`id_geely_for_rent`, gfr.`id_geely_for_rent_type`, gfr.`date`, gfr.`top`, gfr.`title`, gfr.`author`, gfr.`identity`, gfr.`source`, gfr.`content`, gfr.`description`, gfr.`keywords`, gfr.`views`, gfr.`active`,
				gfrt.`title` AS type,
				gfrf.`id_file`
				FROM `geely_for_rent` AS gfr 
				INNER JOIN `geely_for_rent_type` AS gfrt ON gfrt.`id_geely_for_rent_type` = gfr.`id_geely_for_rent_type`
				LEFT JOIN `geely_for_rent_file` AS gfrf ON gfrf.`id_gfr` = gfr.`id_geely_for_rent`
				WHERE TRUE'.$WHERE.'
				GROUP BY gfr.`id_geely_for_rent`
				'.$ORDER;
		$row = Db::rowSQL($sql, false, $num);
		return $row;
	}

	public function getType($active = false){
		if($active){
			$WHERE = ' AND `active` = 1';
		}
		$sql = 'SELECT *
			FROM `geely_for_rent_type`
			WHERE true '.$WHERE.'
			ORDER BY `position` ASC
			';
		$row = Db::rowSQL($sql);
		return $row;
	}

	public function setViews($id){
		$sql = sprintf(' UPDATE `geely_for_rent`
				SET `views` = 0
				WHERE `id_geely_for_rent` = %d
				AND `views` IS NULL',
			GetSQL($id, 'int'));
		Db::rowSQL($sql);

		$sql = sprintf(' UPDATE `geely_for_rent`
				SET `views` = `views` + 1
				WHERE `id_geely_for_rent` = %d',
		GetSQL($id, 'int'));
		Db::rowSQL($sql);
		return (Db::getContext()->num()) ? true : false;
	}

	public static function getFile($id_geely_for_rent){
		$sql = sprintf('SELECT `id_file`
				FROM `geely_for_rent_file`
				WHERE `id_gfr` = %d',
			GetSQL($id_geely_for_rent, 'int'));
		$arr_row = Db::rowSQL($sql);
		$arr = [];
		foreach($arr_row as $i => $v){
			$arr[] = $v['id_file'];
		}
		return $arr;
	}
}