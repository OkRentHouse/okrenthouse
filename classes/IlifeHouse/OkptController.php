<?php
include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');
class OkptController extends FrontController

{
    public function __construct()
    {
        parent::__construct();
        $this->context->smarty->assign([
            'footer_link'   => [
                [
                    'txt'  => '生活集團',
                    'link' => 'https://www.lifegroup.house/',
                ],
                [
                    'txt'  => '生活房屋',
                    'link' => 'https://www.life-house.tw',
                ],
                [
                    'txt'  => '生活好科技',
                    'link' => '',
                ],
                [
                    'txt'  => '生活居家修繕',
                    'link' => '',
                ],
                [
                    'txt'  => '生活樂購',
                    'link' => 'https://www.lifegroup.house/LifeGo',
                ],
                [
                    'txt'  => '共好夥伴',
                    'link' => '',
                ],
                [
                    'txt'  => '相關聲明',
                    'link' => '/statement',
                ],
            ],

//            'footer_txt1'   => Config::get('footer_txt1'),
//            'footer_txt2'   => Config::get('footer_txt2'),
//            'footer_txt3'   => Config::get('footer_txt3'),
//            'footer_qrcode' => 'https://www.life-house.com.tw',
            //			'qrcod_img'     => '/img/okrent/footer_qr_code.png',
        ]);

    }



    public function initProcess()

    {
        $arr_main_menu = [
            'a' => [
                'text'    => $this->l('吉屋快搜'),
                'href'    => '/list',
                'submenu' => [
                    [
                        'text' => $this->l('條件搜尋'),
                        'href' => '/list',
                    ],
                    [
                        'text' => $this->l('地圖搜尋'),
                        'href' => '/HouseMap',
                    ],
                ],
            ],

            'b' => [
                'text'    => $this->l('樂租服務'),
                'href'    => '/tenant',
                'submenu' => [
                    [
                        'text' => $this->l('租客'),
                        'href' => '/tenant',
                    ],

                    [
                        'text' => $this->l('房東'),
                        'href' => '/landlord',
                    ],
                ],
            ],

            'c' => [
                'text'    => $this->l('租事順利'),
                'href'    => '/tax_benefit',
            ],

            'd' => [
                'text' => $this->l('新聞資訊'),
                'href' => '/geely_for_rent',
            ],

            'e' => [
                'text' => $this->l('in生活'),
                'href' => '/in_life_index',
            ],

            'f' => [
                'text'    => $this->l('加入生活'),
                'href'    => '/JoinLife',
                'submenu' => [
                    [
                        'text' => $this->l('生活家族'),
                        'href' => '#',
                    ],

                    [
                        'text' => $this->l('加盟樂租'),
                        'href' => '#',
                    ],

                    [
                        'text' => $this->l('菁英招募'),
                        'href' => '#',
                    ],
                ],
            ],

            'g' => [
                'text'    => $this->l('關於生活'),
                'href'    => '/about',
            ],

        ];

        $this->main_menu = $arr_main_menu;

        $this->context->smarty->assign([

        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS('/themes/Rent/js/gtag.js');
        parent::setMedia();
        $this->addCSS('/themes/Rent/css/goto_header.css');
    }
}