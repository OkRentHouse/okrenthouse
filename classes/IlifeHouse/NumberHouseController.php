<?php
include_once(dirname(dirname(__FILE__)) . DS . 'Controller' . DS . 'FrontController.php');
class NumberHouseController extends FrontController{
	public function __construct()
	{
		$this->meta_description = $this->l('網搜全台灣各房屋租售網站-出租、頂讓、售屋、新建案等資訊，所有房地產租售資訊一次看只要輸入想要的找屋條件，輕鬆快速找到你要的，也可以生活圈使用地圖找屋超便利。新進案件即時通知不錯過#實價登錄及開價資訊，行情一手抓 議價心有數。提供各種實用好幫手-房貸專區#稅費試算#新聞資訊。');
		$this->meta_keywords = $this->l('售屋網;售屋網桃園;售屋廣告;售屋所得稅;售屋小姐;售屋平台;售屋網台中;售屋廣告詞;售屋 財產交易所得稅;售屋 稅;售屋 仲介費;售屋 地圖;售屋 綜合所得稅;售屋 舊制;售屋 ptt;售屋 高雄;售屋 app;售屋 新屋;售屋 彰化;售屋 頭城;售屋 安和路二段;售屋 房屋稅;售屋 文林苑;朴子 售屋;文山區 售屋;文智路 售屋;文化中心 售屋;文揚街 售屋;美術館 售屋;售屋 土增稅;售屋 代書;售屋 大同區;大直 售屋;土城 售屋;大溪 售屋;大安區 售屋;大湖公園 售屋;售屋 英文;售屋 永慶;售屋 益民路;永龍2 售屋;永和 售屋;永慶房屋 售屋;永康 售屋;永安市場 售屋;售屋 五股;售屋 銀座;義華路 售屋;藝術南街 售屋;五妃街 售屋;郡安路五段 售屋;漁村街 售屋;合江街 售屋;售屋 報稅;售屋 比價;售屋 東區;售屋 北斗;售屋 花蓮;售屋 太原路二段;售屋 花都;售屋 節稅;售屋 印鑑證明;售屋 員林;售屋 板橋;售屋 高雄市 苓雅區;售屋 樹梅坑;售屋 鹿野;售屋 瑕疵擔保;售屋 貸款;售屋 教孝街;售屋 興隆路三段;售屋 雲世紀;售屋 實價登錄;售屋 免費刊登;售屋 桃園;售屋 摩登家庭;售屋 宜誠;售屋 宜蘭;售屋 木柵路一段;售屋 民生東路二段;售屋 岡山 忠誠街;售屋 岡山;售屋 鳳山;售屋 大竹;高雄 售屋 鳳山;大甲 售屋;阿鴻當家 售屋;大里 售屋;售屋 漏水;售屋 聯上世界;售屋 羅東;台南 售屋 露台;龍潭 售屋;漁光島 售屋;六張犁 售屋;斗六 龍潭路 售屋;售屋 所得稅;售屋 新中街;售屋 新竹;售屋 試算;售屋網;售屋網站;售屋平台;售房屋');
		parent::__construct();
		$this->context->smarty->assign([
		]);
	}

	public function initProcess()
    {
		$this->context->smarty->assign([

		]);
		parent::initProcess();
	}



	public function setMedia()
	{
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS('/themes/Rent/js/gtag.js');
				$this->addJS('/modules/FloatShare/js/float_share.js');
        parent::setMedia();
	}
	/**
	 * 加入我的最愛
	 */
	public function ajaxProcessAddFavorite()
	{
		$id   = Tools::getValue('id');
		$type = Tools::getValue('type');
		$on   = Tools::getValue('on');
		switch ($type) {
			case 'house':    //收藏房屋
			break;
			case 'store':    //收藏店家
			break;
		}
	}
}
