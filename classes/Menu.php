<?php


class Menu
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Menu();
		};
		return self::$instance;
	}

	public function getMenuCode($id_menu_code){
		$WHERE = '';
		if(!empty($id_menu_code)){
			$WHERE = sprintf(' WHERE mc.`id_menu_code` = %d', GetSQL($id_menu_code, 'int'));
		}

		$sql = sprintf('SELECT *
			FROM `'.DB_PREFIX_.'menu_code` AS mc
			LEFT JOIN `'.DB_PREFIX_.'web2` AS w ON w.`id_web` = mc.`id_web`
			'.$WHERE.'
			ORDER BY mc.`id_web` ASC, mc.`code` ASC');
		$row = Db::rowSQL($sql);
		return $row;
	}
}