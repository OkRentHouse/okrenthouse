<?php
//這邊主要放房東代理人關係人相關的東西
class Member_landlord{
    protected static $instance = null;        //是否實體化

    public static function getContext()
    {
    if (!self::$instance) {
        self::$instance = new Member_landlord();
        }
        return self::$instance;
    }

    public static $multiple_data = ['active_off_admin','active_off_reason','active_off_date','ag','id_other_conditions'];//目前只有這四個

    public static function Admin_multiple_table($id_rent_house,$data){//最後回傳新增orupdate 的 id_member_landlord 值
      echo "<hr>Admin_multiple_table<br>";
        $new_data = [];//新的一個data
        $multiple_data = ['active_off_admin','active_off_reason','active_off_date','ag','id_other_conditions'];//目前只有這四個
        $member_data = ['id_member','user','name','en_name','appellation','id_source','birthday','identity','line_id','email','local_tel_1','local_tel_2',
            'id_address_domicile','id_address_contact','id_address_house','id_address_company','company','job'];
        $member_landlord =['id_member_landlord','id_member','id_rent_house','bank_account','bank_code','bank','branch','bank_account_2','bank_code_2','bank_2',
            'branch_2','id_other_conditions','reserve_price','buy_price','ag','active_off_date','active_off_reason','active_off_admin'];
        $address_data = ['id_address_domicile','id_address_contact','id_address_house','id_address_company'];//四大地址
        $member_landlord_data=[];//存入最後的data直

//        $x = count($data['id_member_landlord'])-1;//有一些是用陣列表達要吃進去所以這樣處理 看表有幾張
        foreach($data as $k=>$v){
            if(strpos($k,"member_landlord") !==false){
                if($k=='id_member_landlord'){//id不改變
                    $new_data[$k] = $v;
                }else{
                    $catch_k = str_ireplace("member_landlord_","",$k);//這邊有因為多選的要做一段處理
                    $put_array = 0;//單純拿來put已否搜尋到
                    $put_name ='';
                    foreach($multiple_data as $search_k =>$search_v){
                        if(strpos($k,'member_landlord_'.$search_v) !==false){//搜尋
                            $put_name = $search_v;
                            $put_array = 1;//找到
                        }
                    }
                    if($put_array>='1'){//找到
                        $new_data[$put_name][str_replace($put_name.'_','',$catch_k)] = $v;
                    }else{
                        $new_data[$catch_k] = $v;
                    }
                }
            }
        }

        for($x=0;$x<count($new_data['id_member_landlord']);$x++){
            $sql_data = "";
            ///這邊要先行處理不存在的address or 存在的
            //先行處理部分data
            foreach($address_data as $k =>$v){
                $new_data[$v][$x] = Member_landlord::address_work($v,$new_data,$x);//出去玩 結果存回來不論insert 或是 update
                if($new_data[$v][$x]==0) $new_data[$v][$x]='';//0代表沒建立成功給他空值
                //這邊刪除非必要的值
            }

            for($i=0;$i<count($new_data['id_member']);$i++){
                $new_data['id_member'][$i] = Member_landlord::member_work($new_data['id_member'][$i],$new_data,$i);
                if($new_data['id_member'][$i]==0) $new_data['id_member'][$i]='';//0代表沒建立成功給他空值
            }

            if(!empty($new_data['id_member_landlord'][$x])){
                $update_data = '';
                foreach($member_landlord as $k => $v){
                    if($v=='id_rent_house'){
                        $update_data .='`'.$v.'` = '.GetSQL($id_rent_house,'text').',';
                        continue;
                    }
                    if(in_array($v,$multiple_data)){//有些是陣列要直接塞
                        $get_array = '';
                        foreach($new_data[$v][$x] as $a_k=>$a_v){
                            $get_array .= '"'.$a_v.'",';
                        }
                        $get_array = "'[".substr($get_array,0,-1)."]'";
                        $update_data .='`'.$v.'` = '.$get_array.',';
                    }else{
                        $update_data .='`'.$v.'` = '.GetSQL($new_data[$v][$x],'text').',';
                    }
                }
                $update_data = substr($update_data,0,-1);
                $sql = "UPDATE `member_landlord` SET ".$update_data." WHERE id_member_landlord=".GetSQL($new_data['id_member_landlord'][$x],'int');
                $member_landlord_data[] = $sql;
            }else{
                $insert_key = '';
                $insert_value ='';
                foreach($member_landlord as $k => $v){
                    $insert_key .= '`'.$v.'`,';
                    if($v=='id_rent_house'){
                        $insert_value .= $id_rent_house.',';
                        continue;
                    }
                    if(in_array($v,$multiple_data)){//有些是陣列要直接塞
                        $get_array = '';
                        foreach($new_data[$v][$x] as $a_k=>$a_v){
                            $get_array .= '"'.$a_v.'",';
                        }
                        $get_array = "'[".substr($get_array,0,-1)."]'";
                        $insert_value .= $get_array.',';
                    }else{
                        $insert_value .= GetSQL($new_data[$v][$x],'text').',';
                    }
                }

                $insert_key = substr($insert_key,0,-1);
                $insert_value = substr($insert_value,0,-1);
                $sql = "INSERT INTO `member_landlord` ({$insert_key}) VALUES ({$insert_value})";
                $member_landlord_data[] = $sql;
            }
        }
        foreach($member_landlord_data as $sql){
            Db::rowSQL($sql);
        }
        echo "<hr>";
    }


    public static function Admin_multiple_table_role($id_rent_house,$id_member_landlord,$cut,$data){//關係人版本 $id_member_landlord 先用單個之後可能會換成陣列
        $new_data = [];//新的一個data
        $multiple_data = ['active_off_admin','active_off_reason','active_off_date','ag','id_other_conditions'];//目前只有這四個
        $member_data = ['id_member','user','name','en_name','appellation','id_source','birthday','identity','line_id','email','local_tel_1','local_tel_2',
            'id_address_domicile','id_address_contact','id_address_house','id_address_company','company','job'];
        $member_role =['id_member_role','id_member_landlord','id_member','id_rent_house','bank_account','bank_code','bank','branch','bank_account_2','bank_code_2','bank_2',
            'branch_2','id_other_conditions','reserve_price','buy_price','ag','active_off_date','active_off_reason','active_off_admin','type_role','type'];
        $address_data = ['id_address_domicile','id_address_contact','id_address_house','id_address_company'];//四大地址
        $member_role_data=[];//存入最後的data直

//        $x = count($data['id_member_landlord'])-1;//有一些是用陣列表達要吃進去所以這樣處理 看表有幾張
        foreach($data as $k=>$v){
            if(strpos($k,$cut) !==false){
                if($k=='id_member_landlord'){//id不改變
                    $new_data[$k] = $v;
                }else{
                    $catch_k = str_ireplace($cut."_","",$k);//這邊有因為多選的要做一段處理
                    $put_array = 0;//單純拿來put已否搜尋到
                    $put_name ='';
                    foreach($multiple_data as $search_k =>$search_v){
                        if(strpos($k,$cut.'_'.$search_v) !==false){//搜尋
                            $put_name = $search_v;
                            $put_array = 1;//找到
                        }
                    }
                    if($put_array>='1'){//找到
                        $new_data[$put_name][str_replace($put_name.'_','',$catch_k)] = $v;
                    }else{
                        $new_data[$catch_k] = $v;
                    }
                }
            }
        }
        $new_data['id_member_landlord']['0'] = $id_member_landlord;

        //這個先做一次因此只抓一個
        for($x=0;$x<count($new_data['id_member_landlord']);$x++){
            if(empty($new_data['name'][$x])){//代表連名子都沒有就不建立
                continue;
            }
            $sql_data = "";
            ///這邊要先行處理不存在的address or 存在的
            //先行處理部分data
            foreach($address_data as $k =>$v){
                $new_data[$v][$x] = Member_landlord::address_work($v,$new_data,$x);//出去玩 結果存回來不論insert 或是 update
                if($new_data[$v][$x]==0) $new_data[$v][$x]='';//0代表沒建立成功給他空值
                //這邊刪除非必要的值
            }

            for($i=0;$i<count($new_data['id_member']);$i++){
                $new_data['id_member'][$i] = Member_landlord::member_work($new_data['id_member'][$i],$new_data,$i);
                if($new_data['id_member'][$i]==0) $new_data['id_member'][$i]='';//0代表沒建立成功給他空值
            }

            if(!empty($new_data['id_member_role'][$x])){
                $update_data = '';
                foreach($member_role as $k => $v){
                    if($v=='id_rent_house'){
                        $update_data .='`'.$v.'` = '.GetSQL($id_rent_house,'text').',';
                        continue;
                    }

                    if(in_array($v,$multiple_data)){//有些是陣列要直接塞
                        $get_array = '';
                        foreach($new_data[$v][$x] as $a_k=>$a_v){
                            $get_array .= '"'.$a_v.'",';
                        }
                        $get_array = "'[".substr($get_array,0,-1)."]'";
                        $update_data .='`'.$v.'` = '.$get_array.',';
                    }else{
                        $update_data .='`'.$v.'` = '.GetSQL($new_data[$v][$x],'text').',';
                    }
                }
                $update_data = substr($update_data,0,-1);
                $sql = "UPDATE `member_role` SET ".$update_data." WHERE id_member_role=".GetSQL($new_data['id_member_role'][$x],'int');
                $member_role_data[] = $sql;
            }else{
                $insert_key = '';
                $insert_value ='';
                foreach($member_role as $k => $v){
                    $insert_key .= '`'.$v.'`,';
                    if($v=='id_rent_house'){
                        $insert_value .= $id_rent_house.',';
                        continue;
                    }

                    if(in_array($v,$multiple_data)){//有些是陣列要直接塞
                        $get_array = '';
                        foreach($new_data[$v][$x] as $a_k=>$a_v){
                            $get_array .= '"'.$a_v.'",';
                        }
                        $get_array = "'[".substr($get_array,0,-1)."]'";
                        $insert_value .= $get_array.',';
                    }else{
                        $insert_value .= GetSQL($new_data[$v][$x],'text').',';
                    }
                }

                $insert_key = substr($insert_key,0,-1);
                $insert_value = substr($insert_value,0,-1);
                $sql = "INSERT INTO `member_role` ({$insert_key}) VALUES ({$insert_value})";
                $member_role_data[] = $sql;
            }
        }
        foreach($member_role_data as $sql){
//            echo $sql.'<br/>';
            Db::rowSQL($sql);
        }
    }

    public static function conditions_work($id_main,$main_data,$count){
      //$conditions_data = ;
    }

    public static function address_work($id_main,$main_data,$count){//主id 新成立的data 計數器
        $address_arr = array("county_name"=>"", "city_name"=>"", "village"=>"里", "neighbor"=>"鄰", "road"=>"", "segment"=>"段",
            "lane"=>"巷", "alley"=>"弄", "no"=>"號", "no_her"=>"之", "floor"=>"樓", "floor_her"=>"之", "address_room"=>"室");
        $address_data = [];
        //先建立一張新表
        echo "<hr>address_work<br>";
        foreach($main_data as $k => $v){//來自這張
            if(strpos($k,$id_main) !==false){//$main_data[$k][$count]
                if($k==$id_main){ //自己的id
                    $address_data[$k] = $main_data[$k][$count];
                }else{
                    $address_data[str_replace($id_main."_","",$k)] = $main_data[$k][$count];
                }
            }
        }

        $address_data['address'] = '';//避免之前的影響先清掉
        $sql = "SELECT * FROM `county` WHERE `id_county` =".GetSQL($address_data['id_county'],"int");
        $row = Db::rowSQL($sql,true); //主要要取出county_name
        $address_data['county_name'] = $row["county_name"];//縣市名稱
        $sql = "SELECT * FROM `city` WHERE id_city=".GetSQL($address_data['id_city'],"int");
        $row_city = Db::rowSQL($sql,true); //主要要取出city_name
        $address_data['city_name'] = $row_city["city_name"];//市鄉政
        foreach($address_arr as $k => $i){
            if(!empty($address_data[$k])){
                if($i=="之"){
                    $address_data['address'] .= $i.$address_data[$k];
                }else{
                    $address_data['address'] .= $address_data[$k].$i;
                }
            }
        }
        //這邊有了一張新表

        //先決定該id_address是否存在 存在update 不存在insert

        foreach ($main_data as $key => $value) {
          if( stristr(  $key , "id_address_domicile_") ){
              $newkey="";
              $newkey=str_replace("id_address_domicile_","",$key);
              $main_data[$newkey] = $main_data[$key];
              unset($main_data[$key]);
          }
        }
        //print_r($main_data);
        if(!empty($main_data[$id_main][$count])){//存在
            $update_data = '';
            foreach($address_data as $k => $v){
                if($k=='id_address_domicile' || $k=='id_address_contact'  || $k=='id_address_house'  || $k=='id_address_company' || $k=='county_name' || $k=='city_name') continue;
                //$update_data .='`'.$k.'` = '.GetSQL($v,'text').',';
                $update_data .='`'.$k.'` = '.GetSQL($main_data[$k][0],'text').',';
            }
            $update_data = substr($update_data,0,-1);
            $sql = "UPDATE `address` SET ".$update_data." WHERE id_address=".GetSQL($main_data[$id_main][$count],'int');
            Db::rowSQL($sql);
            $id_main = $main_data[$id_main][$count];
        }else{  //不存在
            $insert_key = '';
            $insert_value ='';
            foreach($address_data as $k => $v){
                if($k=='id_address_domicile' || $k=='id_address_contact'  || $k=='id_address_house'  || $k=='id_address_company' || $k=='county_name' || $k=='city_name') continue;
                $insert_key .= '`'.$k.'`,';
                //$insert_value .= GetSQL($v,'text').',';
                $insert_value .= GetSQL($main_data[$k][0],'text').',';
            }
            $insert_key = substr($insert_key,0,-1);
            $insert_value = substr($insert_value,0,-1);
            $sql = "INSERT INTO `address` ({$insert_key}) VALUES ({$insert_value})";

            $row = Db::rowSQL($sql);

            $id_main = Db::getContext()->num;
        }
        //echo "<br>".$sql."<hr>";
        return $id_main;
    }


    public static function member_work($id_main,$main_data,$num){
        $member_data = ['id_member','user','name','en_name','appellation','id_source','birthday','identity','line_id','email','local_tel_1','local_tel_2',
            'id_address_domicile','id_address_contact','id_address_house','id_address_company','company','job'];
        $member_preset =[
            'password'=>'033581098',
            'id_member_additional'=>'["1","4"]',
            'active'=>'1',
        ];

        if(!empty($id_main)){
            $update_data = '';
            foreach($member_data as $k=>$v){
                $update_data .='`'.$v.'` = '.GetSQL($main_data[$v][$num],'text').',';
            }
            $update_data = substr($update_data,0,-1);
            $sql = "UPDATE `member` SET ".$update_data." WHERE id_member=".GetSQL($id_main,'int');
            Db::rowSQL($sql);
            return $main_data['id_member'][$num];
        }else{
            $insert_key = '';
            $insert_value ='';
            foreach($member_data as $k => $v){
                $insert_key .= '`'.$v.'`,';
                $insert_value .= GetSQL($main_data[$v][$num],'text').',';
            }
            foreach($member_preset as $k => $v){
                $insert_key .= '`'.$k.'`,';
                $insert_value .= "'".$v."',";
            }
            $insert_key = substr($insert_key,0,-1);
            $insert_value = substr($insert_value,0,-1);
            $sql = "INSERT INTO `member` ({$insert_key}) VALUES ({$insert_value})";
            $row = Db::rowSQL($sql);
            $id_main = Db::getContext()->num;
        }
        return $id_main;
    }


    public static function get_data($id_rent_house){
        $address_data = ['id_address_domicile','id_address_contact','id_address_house','id_address_company'];//四大地址
        $where = 'TRUE';
        if(!empty($id_rent_house)){
            $where .=' AND id_rent_house='.GetSQL($id_rent_house,'int');
        }else{
           return;
        }
        $sql = "SELECT * FROM ilifehou_ilife_house.member_landlord as m_l
        LEFT JOIN ilifehou_ilife_house.member as m ON m_l.`id_member` = m.`id_member`
        WHERE {$where} order by m.`id_member` ASC";
        $row = Db::rowSQL($sql);

        foreach($row as $k =>$v){
            $multiple_data = Member_landlord::$multiple_data;
            $address_data_put ='';
            foreach($address_data as $a_k=>$a_v){
                $sql = "SELECT * FROM address WHERE id_address =".GetSQL($row[$k][$a_v],"int");
                $address_data_put = Db::rowSQL($sql,true);
                foreach($address_data_put as $a_d_k => $a_d_v){
                    if($a_d_v=='id_address') continue;
                    $row[$k][$a_v.'_'.$a_d_k] = $a_d_v;
                }
            }
            $row[$k] = Db::all_antonym_array($row[$k],true);
        }

       return $row;
}

    public static function get_conditions($id_rent_house){
      if(empty($id_rent_house)){
          return;
      }

      $sql = "SELECT * as oc FROM `rent_house_conditions` WHERE `id_rent_house`=".GetSQL($id_rent_house,"int");
      $row = Db::rowSQL($sql,true);
      if(empty($row)){
        echo "rent_house_conditions no infor !!";
        return;
      }

      foreach($row as $k =>$v){
          $multiple_data = Member_landlord::$multiple_data;
          $address_data_put ='';
          foreach($address_data as $a_k=>$a_v){
              $sql = "SELECT * FROM address WHERE id_address =".GetSQL($row[$k][$a_v],"int");
              $address_data_put = Db::rowSQL($sql,true);
              foreach($address_data_put as $a_d_k => $a_d_v){
                  if($a_d_v=='id_address') continue;
                  $row[$k][$a_v.'_'.$a_d_k] = $a_d_v;
              }
          }
          $row[$k] = Db::all_antonym_array($row[$k],true);
      }

     return $row;

    }

      public static function get_data_role($id_rent_house,$type,$id_member_landlord){

        $address_data = ['id_address_domicile','id_address_contact','id_address_house','id_address_company'];//四大地址
        $where = 'TRUE';
        if(!empty($id_rent_house) && $type !='' && !empty($id_member_landlord)){
            $where .=' AND id_rent_house='.GetSQL($id_rent_house,'int').' AND type='.GetSQL($type,'int').' AND id_member_landlord='.GetSQL($id_member_landlord,'int');
        }else{
            return;
        }
        $sql = "SELECT * FROM ilifehou_ilife_house.member_role as m_r
        LEFT JOIN ilifehou_ilife_house.member as m ON m_r.`id_member` = m.`id_member`
        WHERE {$where} order by m.`id_member` ASC";

        $row = Db::rowSQL($sql);

        foreach($row as $k =>$v){
            $multiple_data = Member_landlord::$multiple_data;
            $address_data_put ='';
            foreach($address_data as $a_k=>$a_v){
                $sql = "SELECT * FROM address WHERE id_address =".GetSQL($row[$k][$a_v],"int");
                $address_data_put = Db::rowSQL($sql,true);
                foreach($address_data_put as $a_d_k => $a_d_v){
                    if($a_d_v=='id_address') continue;
                    $row[$k][$a_v.'_'.$a_d_k] = $a_d_v;
                }
            }
            $row[$k] = Db::all_antonym_array($row[$k],true);




        }
        return $row;
    }



    public static function get_source(){
        $sql = "SELECT * FROM source ORDER BY position ASC";
        return Db::rowSQL($sql);
    }

    public static function get_rent_mange_program(){
        $sql = "SELECT * FROM rent_mange_program WHERE active=1 ORDER BY id_rent_mange_program ASC";
        return Db::rowSQL($sql);
    }

    public static function get_intermediary_program(){
        $sql = "SELECT * FROM intermediary_program WHERE active=1 ORDER BY id_intermediary_program ASC";
        return Db::rowSQL($sql);
    }

    public static function get_other_conditions(){
        $sql = "SELECT * FROM other_conditions ORDER BY position ASC";
        return Db::rowSQL($sql);
    }

    public static function get_rent_union_pay(){
        $sql = "SELECT * FROM rent_union_pay";
        return Db::rowSQL($sql);
}

    public static function get_rent_union_type(){
        $sql = "SELECT * FROM rent_union_type";
        return Db::rowSQL($sql);
    }

    public static function get_pay_type(){
        $sql = "SELECT * FROM pay_type WHERE active=1";
        return Db::rowSQL($sql);
    }


}
