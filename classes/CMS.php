<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/10/16
 * Time: 下午 04:02
 */

class CMS
{
	protected static $instance = null;
	public $url = '';
	public $id_web = '';

//	public $arr_web = [
//		''               => '',
//		'web_app'        => 1,
//		'web_house'      => 2,
//		'web_life_house' => 3,
//		'web_rent'       => 4,
//		'web_repair'     => 5,
//		'web_ptpro'      => 6,
//	];
    public $arr_web = [
        ''               => '',
        'web_app'        => 1,
        'web_house'      => 2,
        'web_life_house' => 3,
        'web_rent'       => 4,
        'web_repair'     => 5,
        'web_okmaster'      => 6,
        'web_okrent'      => 7,
        'web_okpt' => 8,
        'web_life_house'       => 9,
        'web_epro'     => 10,
        'web_rent'      => 11,
        'web_app_okrent' => 12,
    ];
	public function __construct()
	{
		$this->id_web = $this->arr_web[LilyHouse::getContext()->web];
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new CMS();
		};
		return self::$instance;
	}

	public static function getMeta($id_web, $active = null)
	{
		if (empty($id_web)) {
			$id_web = CMS::getContext()->id_web;
		}
		$where = ' AND `id_web` = ' . GetSQL($id_web, 'int');
		$arr   = [];
		if ($active !== null) {
			$where = sprintf(' AND `active` = %d', GetSQL($active, 'int'));
		}

		$sql = 'SELECT `url`, `title`, `keywords`, `description`, `index_ation`
			FROM `' . DB_PREFIX_ . 'cms`
			WHERE TRUE' . $where . ' AND `id_web` = ' . $id_web;

		$arr_row = Db::rowSQL($sql);
		foreach ($arr_row as $i => $row) {
			$arr[$row['url']] = [    //一半OK 頁面險種及公司沒辦法新增
									 'title'       => $row['title'],
									 'description' => $row['description'],
									 'keywords'    => $row['keywords'],
									 'nobots'      => !$row['index_ation'],
									 'nofollow'    => !$row['index_ation'],
			];
		}
		return $arr;
	}

	public static function isCMS($id_web, $url)
	{
		if (empty($id_web)) {
			$id_web = CMS::getContext()->id_web;
		}
		$where = ' AND `id_web` = ' . GetSQL($id_web, 'int');
		$sql   = sprintf('SELECT `id_cms`
			FROM `' . DB_PREFIX_ . 'cms`
			WHERE `url` = %s AND `active` = 1
			LIMIT 0, 1',
			GetSQL($url, 'text'));
		$row   = Db::rowSQL($sql, true);
		return (count($row) > 0);
	}

	//todo 加入網站分別
	public static function getByUrl($id_web, $url, $active = null, $web = null)
	{
		if (empty($id_web)) {
			$id_web = CMS::getContext()->id_web;
		}
		$where = ' AND `id_web` = ' . GetSQL($id_web, 'int');
		if ($active !== null) {
			$where = sprintf(' AND `active` = %d', GetSQL($active, 'int'));
		}
		$sql = sprintf('SELECT `id_cms`,`title`, `page_title`, `keywords`, `description`, `html`, `css`, `js`, `index_ation`, `display_header`,
            `display_footer`,`related_tabs`,`internal_introduce`,`introduce_id_cms`,`customer_message`
			FROM `' . DB_PREFIX_ . 'cms`
			WHERE `url` = %s' . $where . '
			LIMIT 0, 1',
			GetSQL($url, 'text'));
		$row = Db::rowSQL($sql, true);
	    if(!empty($row['introduce_id_cms'])){
	        $sql = 'SELECT `id_cms`,`html`,`css`,`js`,`internal_introduce`,introduce_id_cms FROM `' . DB_PREFIX_ . 'cms`
	         WHERE  `active`=1 AND `id_cms`='.GetSQL($row['introduce_id_cms'],"int");
	        $substitute  = Db::rowSQL($sql, true);
	        foreach($substitute as $k => $v){
	            if($k=='css' || $k =='js'){//將原始有些的css與js引入
	                $v .=$row[$k];
                }
                $row[$k] = $v;
            }
        }
		return $row;
	}

	public static function get_model($row){
        $model_html =<<<modal
        <!-- modal 彈窗 -->
				<div id="modal-message" class="more_understand modal fade" role="dialog" aria-labelledby="myModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<div class="info">
									<span id="title">{$row['title']}</span>
								</div>
							</div>
							<div class="modal-body">
								<div class="content_wrap">
									<div class="content">
										<div class="form">
											<div class="row list">
												<div class="col-md-5 name_frame">
													<div class="floating">
														<i class="fas fa-user"></i>
														<input id="name" data-role="none" class="floating_input name "
															name="name" type="text" placeholder="姓 名" value=""
															minlength="2" maxlength="20" required="">
													</div>
												</div>
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="radio_wrap">
														<div class="radio">
															<input id="miss" type="radio" name="sex" value="0" checked>
															<label class="caption" for="miss">女 士</label>
														</div>
														<div class="radio">
															<input id="mister" type="radio" name="sex" value="1">
															<label class="caption" for="mister">先 生</label>
														</div>
													</div>
												</div>
											</div>

											<div class="row list">
												<div class="col-md-5">
													<div class="floating">
														<i class="fas fa-mobile-alt"></i>
														<input id="phone" data-role="none" class="floating_input"
															name="phone" type="text" placeholder="手 機" value=""
															maxlength="20" required="">
													</div>
												</div>
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="floating">
														<i class="fas fa-phone-volume"></i>
														<input id="tel" data-role="none" class="floating_input"
															name="tel" type="text" placeholder="市 話" value=""
															maxlength="20" required="">
													</div>
												</div>
											</div>

											<div class="row list">
												<div class="col-md-5">
													<div class="floating">
														<i class="far fa-envelope"></i>
														<input id="email" data-role="none" class="floating_input "
															name="email" type="text" placeholder="電 郵" value=""
															required="">
													</div>
												</div>
												<div class="col-md-1"></div>
												<div class="col-md-6">
													<div class="contact_time_wrap">
														<div class="caption">方便聯絡時間</div>
														<div class="radio">
															<input checked="checked" name="contact_time" type="radio"
																value="0">早上
														</div>
														<div class="radio"><input name="contact_time" type="radio"
																value="1">下午
														</div>
														<div class="radio"><input name="contact_time" type="radio"
																value="2">晚上
														</div>
														<div class="radio"><input name="contact_time" type="radio"
																value="3">其他
														</div>
													</div>
												</div>
											</div>

											<div class="message">
												<div class="caption">
													<img class="" src="/themes/Rent/img/msg.svg" alt="">
													留言 ( 限100個字內 )</div>
												<textarea class="form-control" aria-label="With textarea" id="message"
													name="message"></textarea>
											</div>
											<div class="agree">
												<label><input type="checkbox" name="agree" value="1">
													我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button class="btn" type="button" id="active" data-dismiss="modal"> 送 出 </button>
							</div>
						</div>
					</div>
				</div>
modal;

        $model_js =<<<js
        $('#active').click(function () {		//按鈕
            if($('#modal-message input[name="name"]').val()=='' || $('#modal-message input[name="phone"]').val() ==''){
                $('body').css("padding-right","");
               alert("需要填寫手機跟姓名");
            }else{
                $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': true,
                    'action': 'CustomerMessage',
                    'title': $('#title').text(),
                    'name' : $('#modal-message input[name="name"]').val(),
                    'sex' : $('#modal-message input[name="sex"]:checked').val(),
                    'phone' : $('#modal-message input[name="phone"]').val(),
                    'tel' : $('#modal-message input[name="tel"]').val(),
                    'email' : $('#modal-message input[name="email"]').val(),
                    'contact_time' : $('#modal-message input[name="contact_time"]:checked').val(),
                    'message' : $('#message').val()
                },
                dataType: 'json',
                success: function (data) {
                    if(data=='ok'){
                        alert("已成功留言 我們會盡快處理");
                    }else{
                        alert("請重新留言或確認是否有輸入資料");
                        }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
            }
    });
js;
        return ['model_html'=>$model_html,'model_js'=>$model_js];
    }

    public static function model_ajax(){
        $action = Tools::getValue('action');
        $title = Tools::getValue('title');
        $name = Tools::getValue('name');
        $sex = Tools::getValue('sex');
        $phone = Tools::getValue('phone');
        $tel = Tools::getValue('tel');
        $email = Tools::getValue('email');
        $contact_time = Tools::getValue('contact_time');
        $message = Tools::getValue('message');


        $sql = "INSERT INTO `customer_message`(`title`,`name`,`sex`,`phone`,`tel`,`email`,`contact_time`,`message`,`schedule`)
                        VALUES(".GetSQL($title,"text").",".GetSQL($name,"text").",
                        ".GetSQL($sex,"int").",".GetSQL($phone,"text").",
                        ".GetSQL($tel,"text").",".GetSQL($email,"text").",
                ".GetSQL($contact_time,"int").",".GetSQL($message,"text").",0)";
        Db::rowSQL($sql);
        return json_encode("ok");

    }


}