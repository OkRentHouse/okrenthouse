<?php
require "./media/PHPMailer/src/PHPMailer.php";
require "./media/PHPMailer/src/POP3.php";
require "./media/PHPMailer/src/SMTP.php";
require "./media/PHPMailer/src/Exception.php";

class GmailAPI
{
    protected static $instance = null;        //是否實體化

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new GmailAPI();
        }
        return self::$instance;
    }

    public function sendmail($mail_to,$subject="",$message="",$mail_from="ilifeproperty@gmail.com",$mail_paword="og8g234867"){
        if(!empty($mail_to)){
            // 產生 Mailer 實體
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            // 設定為 SMTP 方式寄信
            $mail->IsSMTP();
            $mail->Host       = gethostbyname("smtp.gmail.com");
            // SMTP 伺服器的設定，以及驗證資訊
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "ssl";
            $mail->Port = 465;
            // 信件內容的編碼方式
            $mail->CharSet = "utf-8";
            // 信件處理的編碼方式
            $mail->Encoding = "base64";
            // SMTP 驗證的使用者資訊
            $mail->Username = $mail_from;
            $mail->Password = $mail_paword;
            // 信件內容設定
            $mail->From = $mail_from;
            $mail->FromName = "生活房屋";
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->IsHTML(true);

            // 收件人
            if(stristr($mail_to,",")){
                $mail_to_array=explode(",",$mail_to);
                foreach($mail_to_array as $m2){
                    if(!empty($m2)){
                        $mail->AddAddress($m2);
                    }
                }
            }else{
                $mail->AddAddress($mail_to);
            }

//            print_r($mail);

            // 顯示訊息
            if(!$mail->Send()) {
                echo "Mail error: " . $mail->ErrorInfo;
                return 0;
            }else {
                return 1;
            }
        }
    }

}