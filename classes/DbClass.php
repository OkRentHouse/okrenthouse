<?php
class DbClass{
	protected static $instance	= NULL;		//是否實立
	public $table			= NULL;
	public $select			= array();
	public $id				= NULL;
	public $id_key			= NULL;
	public $val				= array();
	public $_errors			= array();		//錯誤訊息
	public $_msgs			= array();
	public $msg				= '';
	public $in_msg			= '';
	public $type			= true;
	public $num				= 0;
	
	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new DbClass();
		};
		return self::$instance;
	}
	
	//縮寫 錯誤訊息
	public function s_err($str){
		$this->_errors[] = $this->l($str);
	}
	
	public function db_get(){
		$WHERE = '';
		$row = NULL;
		if(!empty($this->id)){
			if(is_array($this->id)){
				$WHERE = ' WHERE `'.$this->id_key.'` IN ('.implode(',', $this->id).')';
			}elseif(isint($this->id)){
				$WHERE = ' WHERE `'.$this->id_key.'` != "'.$this->id.'" LIMIT 0, 1';
			}
		};
		$sql = 'SELECT `'.implode('`, `',$this->select).'`
			FROM `'.$this->table.'`'.$WHERE;
		$db = new db_mysql($sql);
		$num = $db->num();
		if($num > 0){
			if(empty($id) || is_array($id)){
				$row = array();
				while($r = $db->next_row()){
					$row[] = $r;
				};
			}else{
				$row = $db->row();
			};
		};
		$this->num = $num;
		return $row;
	}
	
	public function db_add(){
		$sql ='INSERT INTO `'.$this->table.'` (`'.implode('`, `',$this->select).'`) VALUES ("'.implode('", "', $this->val).'")';

		$db = new db_mysql($sql);
		$num = $db->num();
		$this->num = $num;
		return $num;
	}
	
	public function db_edit(){
		if(!empty($this->id)){
			if(is_array($this->id)){
				$WHERE = ' WHERE `'.$this->id_key.'` IN ("'.implode('", "', $this->id).'")';
			}elseif(isint($this->id)){
				$WHERE = ' WHERE `'.$this->id_key.'` = "'.$this->id.'"';
			};
		};
		$arr_SET = array();
		foreach($this->select as $i => $v){
			$arr_SET[] = '`'.$this->select[$i].'` = "'.$this->val[$i].'"';
		};
		$SET = implode(', ', $arr_SET);
		$sql = 'UPDATE `'.$this->table.'` SET '.$SET.$WHERE;
		$db = new db_mysql($sql);
		$num = $db->num();
		$this->num = $num;
		return $num;
	}
	
	public function db_del(){
		if(!empty($this->id)){
			if(is_array($this->id)){
				$WHERE = ' WHERE `'.$this->id_key.'` IN ("'.implode('", "', $this->id).'")';
			}elseif(isint($this->id)){
				$WHERE = ' WHERE `'.$this->id_key.'` = "'.$this->id.'"';
			};
		};
		$sql = 'DELETE FROM `'.$this->table.'` '.$WHERE;
		$db = new db_mysql($sql);
		$num = $db->num();
		$this->num = $num;
		return $num;
	}
}