<?php

/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2017/1/4
 * Time: 下午 05:35
 * 2019-09-25 加入 LilyHouse::getContext()->web.'_' 來判別網站樣式
 */
class Themes
{
	protected static $instance;
	public $AllManageThemes;
	public $ManageTheme;
	public $MyManageTheme;
	public $AllThemes;
	public $Theme;


	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Themes();
		};
		return self::$instance;
	}

	/*
	 * 取得所有後臺樣版
	 */
	public function getAllManageThemes()
	{
		$json = new JSON();
		if(!empty($this->AllManageThemes)) return $this->AllManageThemes;
		$dir = ADMIN_THEME_DIR;
		$this->AllManageThemes = array();
		if(is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if ($file != '.' && $file != '..' && is_file($dir.$file.DS.'config.xml')){
						$data = new XMLReader();
						$data->open($dir.$file.DS.'config.xml');
						/*
						 * 深度1
						 */
						if($data->depth == 1 && $data->nodeType == 1){
							$data->name;
						}
						while($data->read()){
							/*
							 * 深度2
							 */
							if($data->depth == 2 && $data->nodeType == 1) {
								//如果xml資料深度是2 節點類型是元素類型就繼續讀取
								$name = $data->name;
								$data->read();
								$this->AllManageThemes[$file][$name] = $data->value;
							}
						}
					}
				}
			}
		}
		Cache::set('AllManageThemes', $json->encode($this->AllManageThemes));
		return $this->AllManageThemes;
	}

	/*
	 * 取得現在使用後臺樣版
	 */
	public function getManageThemes()
	{
		$this->ManageTheme = Configuration::get(LilyHouse::getContext()->web.'_ManageTheme');
		if (!empty($this->ManageTheme)) return $this->ManageTheme;
		return ADMIN_DEFAULT_THEME;
	}

	/*
	 * 取得現在使用後臺樣版
	 */
	public function getAdminTheme()
	{
		$this->MyManageTheme = Configuration::get(LilyHouse::getContext()->web.'_MyManageTheme_'.$_SESSION['id_admin']);
		if (!empty($this->MyManageTheme)) return $this->MyManageTheme;
		return $this->getManageThemes();
	}

	/*
	 * 取得所有前臺樣版
	 */
	public function getAllThemes()
	{
		$json = new JSON();
		if(!empty($this->AllThemes)) return $this->AllThemes;
		$dir = WEB_DIR.DS.'themes'.DS;
		$this->AllThemes = array();
		if(is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if ($file != '.' && $file != '..' && is_file($dir.$file.DS.'config.xml')){
						$data = new XMLReader();
						$data->open($dir.$file.DS.'config.xml');
						if($data->depth == 1 && $data->nodeType == 1){
							$data->name;
						}
						while($data->read()){
							if($data->depth == 2 && $data->nodeType == 1) {
								//如果xml資料深度是2 節點類型是元素類型就繼續讀取
								$name = $data->name;
								$data->read();
								$this->AllThemes[$file][$name] = $data->value;
							}
						}
					}
				}
			}
		}
		Cache::set('AllThemes', $json->encode($this->AllThemes));
		return $this->AllThemes;
	}

	/*
	 * 取得現在使用前臺樣版
	 */
	public function getTheme()
	{
		//todo
		$this->Theme = Configuration::get(LilyHouse::getContext()->web.'_Theme');
		if (!empty($this->Theme)) return $this->Theme;
		return DEFAULT_THEME;
	}

	public function l($string, $themes=null, $type='front')
	{
		return $string;
	}
}