<?php

class Epro{
    protected static $instance = null;        //是否實體化

    public function __construct(){
        define('Master_IMG_DIR', 'img'.DS.'epro');							//Epro 資料夾位置
        define('Master_IMG_URL', '/img/epro/');							//Epro 網址路徑
    }

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new Epro();
        }
        return self::$instance;
    }

    public static $epro =['id_member','fb_share','line_share','server_cycle','server_features','honorarium','server_time_s','server_date_s',
        'server_time_e','server_date_e','phone_1','phone_2','line_id','local_calls','address'];//epro輸入值
    public static $file =['0'=>'work_photo','1'=>'my_photo','2'=>'license'];//這塊是key是file_type的儲存值   license的備註會丟進去
    public static $file_remarks =['work_photo'=>'','my_photo'=>'','license'=>'license_remarks'];//前兩個沒備註因此給無
    public static $epro_server_item =['id_epro_server_item','id_epro','id_epro_windows','id_epro_windows_item','price','unit'];
    public static $epro_server_address =['id_epro_server_address','id_epro','id_county','id_city'];

    public static function validation($data,$file_data){//目前還不確定是否必填項目 先建設
        $return = [];
        foreach($data as $k =>$v){
            $$k = $v;//將吃到的資料變成個別變數
        }
        return array("success"=>"1");//模擬
    }

    public static function post_put_data($id_member,$data,$file_data){//會員id,寫入資料($_POST),圖片($_FILES)
        if(empty($id_member) || empty($data)) return array("error"=>"會員帳號或是資料不存在");
        //先搜索是否存在該資料
        $id_epro = "";//查看是否有id
        $epro_server_item_arr =[];//這個是用來處理 item的陣列
        $epro_server_address_arr = [];//這個是用來處理 address的陣列
        $sql = "SELECT * FROM ilifehou_ilife_house.`epro` WHERE id_member=".GetSQL($id_member,"int");
        $row = DB::rowSQL($sql,"true");
        $id_epro = $row["id_epro"];

        $data['id_epro'] = $id_epro;//塞回去post方便作業
        $data['id_member'] = $id_member;//塞回去post方便作業

        if(!empty($id_epro)){//insert 跟 update分野
            $update_data =' ';
            foreach(Epro::$epro as $k=>$v){
                $update_data .='`'.$v.'`'.'='.GetSQL($data[$v],"text").',';
            }
            $update_data = substr($update_data,0,-1);
            $sql = "UPDATE ilifehou_ilife_house.`epro` SET {$update_data} WHERE id_epro=".GetSQL($id_epro,"int");
//            echo $sql;
            Db::rowSQL($sql);
        }else{
            $insert_key = '';
            $insert_value = '';
            foreach(Epro::$epro as $k=>$v){
                $insert_key .= '`'.$v.'`,';
                $insert_value .= GetSQL($data[$v],"text").',';
            }
            $insert_key = substr($insert_key,0,-1);
            $insert_value = substr($insert_value,0,-1);

            $sql = "INSERT INTO ilifehou_ilife_house.`epro` ({$insert_key})VALUES({$insert_value})";
            Db::rowSQL($sql);
            $id_epro = Db::getContext()->num; //取得id_epro
            $data['id_epro'] = $id_epro; //將id_epro寫回去
//            echo $sql;
        }

            foreach($data as $k =>$v){
                if($data['id_epro_server_item']){//將id用arr處理
                    for($i=0;$i<count($data['id_epro_server_item']);$i++){//代表新增或是修改幾個
                        foreach(Epro::$epro_server_item as $e_s_i_k => $e_s_i_v){
                            $epro_server_item_arr[$i][$e_s_i_v] = $data[$e_s_i_v][$i];
                        }
                    }
                }
                if($data['id_epro_server_address']){//將id用arr處理
                    for($i=0;$i<count($data['id_epro_server_address']);$i++){//代表新增或是修改幾個
                        foreach(Epro::$epro_server_address as $e_s_a_k => $e_s_a_v){
                            $epro_server_address_arr[$i][$e_s_a_v] = $data[$e_s_a_v][$i];
                        }
                    }
                }
            }

            Epro::post_put_epro_out($id_epro,"epro_server_item","id_epro_server_item",$epro_server_item_arr);
            Epro::post_put_epro_out($id_epro,"epro_server_address","id_epro_server_address",$epro_server_address_arr);

        return array("success"=>"1");
    }

    public static function post_put_epro_out($id_epro,$table,$table_id,$data){//$id_epro,陣列資料(輸入用)
        $put_sql =[];
        foreach($data as $k =>$v){
            //先check 是否有該值因為怕有人用惡搞的方式影響他人的資料
            $sql = "SELECT * FROM ilifehou_ilife_house.`{$table}` WHERE {$table_id}=".GetSQL($v[$table_id],"int").' AND id_epro='.GetSQL($id_epro,"int");
            $row = Db::rowSQL($sql,true);
            if(!empty($row[$table_id])){//搜索該table_id 是否存在
                $update_data ='';
                foreach($v as $v_k=>$v_v){
                    if($v_k==$table_id) continue;
                    $update_data .='`'.$v_k.'`='.GetSQL($v_v,"text");

                }
                $update_data = substr($update_data,0,-1);
                $put_sql[] = "UPDATE ilifehou_ilife_house.`{$table}` SET {$update_data} WHERE {$table_id}=".GetSQL($v[$table_id],"text");
            }else{
                $insert_key = '';
                $insert_value = '';

                foreach($v as $v_k=>$v_v){
                    if($v_k==$table_id) continue;
                    $insert_key .= '`'.$v_k.'`,';
                    $insert_value .= GetSQL($v_v,"text").',';
                }
                $insert_key = substr($insert_key,0,-1);
                $insert_value = substr($insert_value,0,-1);
                $put_sql[] = "INSERT INTO ilifehou_ilife_house.`{$table}` ({$insert_key}) VALUES({$insert_value})";
            }
        }

        foreach($put_sql as $sql){
            Db::rowSQL($sql);
//            echo $sql."<br/>";
        }
    }

    public static function get_data($id_member=null,$id_epro=null){//裝修達人的資料 外連的還未寫入進來
        if($id_member==null && $id_epro==null) return "";
        $WHERE = "WHERE TRUE ";
        if($id_member !=null) $WHERE .=" AND id_member=".GetSQL($id_member,"int");
        if($id_epro !=null) $WHERE .=" AND id_epro=".GetSQL($id_epro,"int");

        $sql = "SELECT * FROM epro {$WHERE}";
        $row = Db::rowSQL($sql,true);

        //外連的兩張表
        $row['epro_server_item'] = Epro::get_epro_out($row['id_epro'],"epro_server_item");
        $row['epro_server_address'] = Epro::get_epro_out($row['id_epro'],"epro_server_address");

        return $row;
    }

    public static function get_epro_out($id_epro,$table){//取得跟epro外連的
        if($id_epro==null) return "";
        $sql = "SELECT * FROM ilifehou_ilife_house.`{$table}` WHERE id_epro=".GetSQL($id_epro,"int");
        return Db::rowSQL($sql);
    }

    public static function get_epro_windows(){//百葉窗
        $sql = "SELECT * FROM ilifehou_epro.`epro_windows` WHERE active=1 ORDER BY position ASC";
        return Db::rowSQL($sql);
    }

    public static function get_epro_windows_item($id_epro_windows=null){//百葉窗的項目
        $WHERE = 'WHERE TRUE AND e_w_i.`active`=1 AND e_w.`active`=1 ';
        if($id_epro_windows !=null){
            $WHERE .= ' AND e_w_i.`id_epro_windows`='.GetSQL($id_epro_windows,"int");
        }
        $sql = "SELECT e_w.`title` as e_w_title,e_w_i.`id_epro_windows` as id_epro_windows, 
        e_w_i.`id_epro_windows_item` as id_epro_windows_item,e_w_i.`title` as title
        FROM ilifehou_epro.`epro_windows_item` as e_w_i
        LEFT JOIN ilifehou_epro.`epro_windows` as e_w ON e_w.`id_epro_windows` = e_w_i.`id_epro_windows` {$WHERE} ORDER BY 
        e_w.`position` ASC, e_w_i.`position` ASC";
        return Db::rowSQL($sql);
    }

}