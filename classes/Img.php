<?php
class Img{
	public $_errors	= array();		//錯誤訊息
	public $IMG_DIR	= IMG_DIR;		//圖片路徑
	public $FILE_name	= NULL;		//input type="file" 的name
	public $title	= NULL;		//input type="file" 的title
	public $width	= NULL;		//圖片寬度
	public $max_size	= 5242880;		//圖片上傳大小限制 (預設5M)
	public $heigh	= NULL;		//圖片高度
	public $soft	= NULL;		//圖片類型
	public $cdde	= NULL;		//圖片代碼

	public function __construct(){		//
		if(!is_dir($IMG_DIR)) add_dir($IMG_DIR);
	}
	
	public function check_img(){
		foreach($_FILES[$this->$FILE_name]['type'] as $k => $type){
			if(isset($_FILES[$this->$FILE_name]['type'][$k]) && $_FILES[$this->$FILE_name]['size'][$k] > 5242880) $this->_errors[] = '上傳圖片太大(限制在5M以下)';
			if(!is_img($_FILES[$this->$FILE_name]['type'][$k]) && $_FILES[$this->$FILE_name]['type'][$k] != '') $this->_errors[] = '上傳第 '.($k+1).' 張圖片格式錯誤';
		};
	}
	
	public function update_img($soft=NULL,$FILE_name=NULL){	//上傳圖片
		$this->check_img();
		if(!empty($soft)) $this->soft = $soft;
		if(!empty($cdde)) $this->cdde = $cdde;
		if(!empty($this->soft)){
			$sql = 'SELECT `id_img_soft`, `img_soft`
				FROM `img_size` AS i_s
				LEFT JPIN `img_soft_size` AS i_s_s ON i_s._s`id_img_soft` = i_s.`id_img_soft`
				WHERE `img_soft` = "'.$soft.'"';
		};
	}
	
	public static function get_size_for_soft($soft){
		$sql = 'SELECT `code`, `width`, `high`, `scaling`
			FROM `img_soft` AS i_s
			LEFT JOIN `img_soft_size` AS i_s_s ON i_s._s`id_img_soft` = i_s.`id_img_soft`
			LEFT JOIN `img_size` AS i_z ON i_z.`id_img_size` = i_s.`id_img_size`
			WHERE `img_soft` = "'.$soft.'"';
		$SELECT_img_size = new db_mysql($sql);
		if($SELECT_img_size->num() > 0){
			while($img_size = $SELECT_img_size->next_row()){
				
			};
		};
	}
	
	public static function add_img($id_img_soft,$type){		//新增圖片
		$sql = 'INSERT INTO `img` (`id_img_soft`, `type`) VALUES (%d, %s)';
		$INSERT_img = new db_mysql($sql);
	}
	
	public function add_code_in_scaling($code,$width,$high,$scaling,$img_soft){
		
	}
	
	public function code_scaling($code=NULL){
		$sql = 'SELECT `width`, `high`, `scaling` FROM `img_size`
		WHERE `code` = "'.$code.'"
		LOMIT 0, 1';
	}
	
	public function add_img_size(){
		
	}
	
	public function add_img_title(){
		
	}

	public function test(){

		$src = imagecreatefromjpeg($_FILES['pic']['tmp_name']);
// get the source image's widht and hight
		$src_w = imagesx($src);
		$src_h = imagesy($src);

// assign thumbnail's widht and hight
		if ($src_w > $src_h) {
			$thumb_w = 100;
			$thumb_h = intval($src_h / $src_w * 100);
		} else {
			$thumb_h = 100;
			$thumb_w = intval($src_w / $src_h * 100);
		}

// if you are using GD 1.6.x, please use imagecreate()
		$thumb = imagecreatetruecolor($thumb_w, $thumb_h);

// start resize
		imagecopyresized($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);


// save thumbnail
		imagejpeg($thumb, "/var/www/html/uploads/thumb/" . $_FILES['pic']['name']);
	}
}