<?php
class Meta{
	public static function getMetaByPage($page, $id_language=NULL){
		$arr_meta = SEO::get(null, $page);
		return $arr_meta[$page];
	}

	public static function getPageMetas($context, $id_lang=NULL){
		$metas = Meta::getMetaByPage($context->page, $id_lang);
		$WEB_NAME = !empty($context->WEB_NAME)?$context->WEB_NAME:WEB_NAME;
		$ret['meta_title'] = $WEB_NAME;
		if(!empty($context->meta_title)) $ret['meta_title'] =  $context->meta_title.NAVIGATION_PIPE.$WEB_NAME;
		if(isset($metas['title']) && !empty($metas['title'])){
			$ret['meta_title'] =  $metas['title'].NAVIGATION_PIPE.$WEB_NAME;
		}
		$ret['meta_description'] = (isset($metas['description']) && $metas['description']) ? $metas['description'] : $context->meta_description;
		$ret['meta_keywords'] = (isset($metas['keywords']) && $metas['keywords']) ? $metas['keywords'] :  $context->meta_keywords;
		return $ret;
	}
}