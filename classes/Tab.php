<?php
class Tab {
	protected static $instance;
	
	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new Tab();
		};
		return self::$instance;
	}
	
	protected static $db_Tab = NULL;
	public function getTab($type=false){	//使用者權限選單
		if(self::$db_Tab === NULL){
			$sql = 'SELECT * FROM `tab` ORDER BY `id_parent` ASC, `position` ASC';
			$arr_row =  Db::rowSQL($sql);
			self::$db_Tab[0] = $arr_row;
			foreach($arr_row as $i => $row){
				if($row['active']) self::$db_Tab[1][] = $row;
			}
		};
		return self::$db_Tab[$type];
	}
	
	protected static $_showTabList = NULL;
	public function showTabList(){
		if(self::$_getIdFromClassName === NULL){
			$dir = CONTROLLERS_DIR.DS.'admin';
			if(is_dir($dir)){
				if ($dh = opendir($dir)) {
					while(($file = readdir($dh)) !== false) {
						//只讀過取出php 的檔案
						if ($file != '.' && $file != '..'){
							if (strpos( $file, '.php')){
								self::$_getIdFromClassName[] = substr($file, 5, -14);
							};
						};
					};
				};
			};
			$dir = OVERRIDE_DIR.DS.'admin'.DS.'controllers';
			if(is_dir($dir)){
				if ($dh = opendir($dir)) {
					while(($file = readdir($dh)) !== false) {
						//只讀過取出php 的檔案
						if ($file != '.' && $file != '..'){
							if (strpos( $file, '.php')){
								self::$_getIdFromClassName[] = substr($file, 5, -14);
							};
						};
					};
				};
			};
		};
		return self::$_getIdFromClassName;
	}

	/*
	 * 取出後台選單
	 */

	public function getTabList(){
		$arr_Tab = Tab::getContext()->getTab(true);
		$arr_TabList = array();
		$arr_parent = array();
		foreach($arr_Tab as $i => $Tab){
			$arr_access = Competence::getContext()->getAccess($Tab['id_tab']);
			if($Tab['module']){
				$Tab['controller_name'] = 'm/'.$Tab['controller_name'];
			}


//            if($arr_access['view']){		//查看是否有瀏覽權限

			if($arr_access['view']){		//查看是否有瀏覽權限
				if(empty($Tab['id_parent'])){	//父層
				    if(($_SESSION["id_admin"]==1 || $_SESSION["id_group"]==2) && $_SESSION["id_diversion"]=='admin' || strstr($Tab["id_diversion"],'"'.$_SESSION["id_diversion"].'"')){//這邊是用判斷法來濾掉父層資料
                        $arr_parent[$Tab['id_tab']] = $Tab['controller_name'];
                        $arr_TabList[$Tab['controller_name']] = array(
                            'controller_name' => $Tab['controller_name'],
                            'parameter' => $Tab['parameter'],
                            'icon' => $Tab['icon'],
                            'title' => $Tab['title']
                        );
                    }
				}else{				//子層
					$arr_TabList[$arr_parent[$Tab['id_parent']]]['list'][$Tab['controller_name']] = array(
						'controller_name' => $Tab['controller_name'],
						'parameter' => $Tab['parameter'],
						'icon' => $Tab['icon'],
						'title' => $Tab['title']
					);
				};
			};
		};
		return $arr_TabList;
	}

	public function sequenceTab($id_tab=0){
		$arr_Tab = Tab::getContext()->getTab();
		$tabs = array();
		foreach($arr_Tab as $i => $tab){
			if($tab['id_parent'] == $id_tab){
				$tabs[] = $tab;
				foreach($arr_Tab as $j => $tab2){
					if($tab['id_tab'] == $tab2['id_parent']){
						$tabs[] = $tab2;
					};
				};
			};
		};
		return $tabs;
	}
	public function recursiveTab($id_tab){
		$arr_Tab = Tab::getContext()->getTab();
		$tabs = array();
		foreach($arr_Tab as $i => $tab){
			if($tab['id_tab'] == $id_tab){
				if(!empty($tab['id_parent'])){
					foreach($arr_Tab as $j => $tab2){
						if($tab2['id_tab'] == $tab['id_parent']){
							$tabs[] = $tab2;
							break;
						};
					};
				};
				$tabs[] = $tab;
				return $tabs;
				break;
			};
		};
		return false;
	}

	protected static $_getIdFromClassName = array();
	public static function getIdFromClassName($controller_name){
		if(self::$_getIdFromClassName[$controller_name] === NULL){
			$arr_Tab = Tab::getContext()->getTab();
			foreach($arr_Tab as $i => $tab){
				if($tab['controller_name'] == $controller_name){
					self::$_getIdFromClassName[$controller_name] = $tab['id_tab'];
					if(!empty($tab['id_parent'])) break;
				};
			};
		};
		return self::$_getIdFromClassName[$controller_name];
	}
}