<?php


class ShortGet
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new ShortGet();
		};
		return self::$instance;
	}

	public static function get(){
		foreach($_GET as $i => $v){
			if(substr($i, -2, 2) == '_s'){
				$get = substr($i, 0, -2);
				if(!isset($_GET[$get])){
					$_GET[$get] = explode(',', urldecode($v));
				}
			}
		}

		if(isset($_GET['price'])){
			$price = explode(',', urldecode($_GET['price']));
			$_GET['min_price'] = $price[0];
			$_GET['max_price'] = $price[1];
		}

		if(isset($_GET['ping'])){
			$ping = explode(',', urldecode($_GET['ping']));
			$_GET['min_ping'] = $ping[0];
			$_GET['max_ping'] = $ping[1];
		}

		if(isset($_GET['room'])){
			$room = explode(',', urldecode($_GET['room']));
			$_GET['min_room'] = $room[0];
			$_GET['max_room'] = $room[1];
		}
	}
}