<?php
//翻譯

class Translate{

	public function __construct(){
		add_dir(TRANSLATE_DIR);		//新增譯文資料夾
	}

//取得模組翻譯
	public static function getModuleTranslation($module, $string, $source=NULL){
		global $_MODULES, $_MODULE, $_LANGADM;
		static $translations_merged = array();		//合併翻譯
		static $lang_cache = array();
		static $translationsMerged = array();			//暫存
		$language = Context::getContext()->language;								//語言
		$name = $module instanceof Module ? $module->name : $module;				//查看$module是不是有繼承Module

		if (!isset($translationsMerged[$name][$language->iso_code])) {	//沒有暫存就更新
//檔案載入優先順序
			$files_by_priority = array(
				THEME_DIR . 'modules' . DS . $name . DS . 'translations' . DS . $language->iso_code . '.php',
				MODULE_DIR . DS . $name . DS . 'translates' . DS . $language->iso_code . '.php',
			);

			foreach ($files_by_priority as $file) {
				if (file_exists($file)) {
					include_once($file);	//有檔案就引入
					$_MODULES = !empty($_MODULES) ? array_merge($_MODULES, $_MODULE) : $_MODULE;
					$translations_merged[$name] = true;
				}
			}
		}

		$string = preg_replace("/\\\*'/", "\'", $string);
		$key = md5($string);
		$cache_key = $name.'|'.$string.'|'.$source;

		if(!isset($lang_cache[$cache_key])){
			if($_MODULES == null) return str_replace('"', '&quot;', $string);			//如果沒有模組翻譯,直接回傳字串

			$current_key = strtolower('<{'.$name.'}'.THEME.'>'.$source).'_'.$key;					//模組當前key
			$default_key = strtolower('<{'.$name.'}>'.$source).'_'.$key;							//模組預設key

			if('Controller' == substr($source, 0, 10)){									//控制項
				$file = substr($source, 10, -1);											//取得控制項名稱
				$current_key_file = strtolower('<{'.$name.'}'.THEME.'>'.$file).'_'.$key;			//控制項當前key
				$default_key_file = strtolower('<{'.$name.'}LilyHouse>'.$file).'_'.$key;			//控制項預設key
			};

			if(isset($current_key_file) && !empty($_MODULES[$current_key_file])){						//控制項當前key
				$ret = stripslashes($_MODULES[$current_key_file]);
			}elseif(isset($default_key_file) && !empty($_MODULES[$default_key_file])){					//控制項預設key
				$ret = stripslashes($_MODULES[$default_key_file]);
			}elseif(!empty($_MODULES[$current_key])){													//模組當前key
				$ret = stripslashes($_MODULES[$current_key]);
			}elseif(!empty($_MODULES[$default_key])){													//模組預設key
				$ret = stripslashes($_MODULES[$default_key]);
			}elseif(!empty($_LANGADM)){																	//模組找不到資料就到Admin裡找
				$ret = stripslashes(Translate::getAdminTranslation($string, $key, $_LANGADM));
			}else{
				$ret = stripslashes($string);
			};
			$lang_cache[$cache_key] = $ret;
		};
		return $lang_cache[$cache_key];
	}

	public static function getAdminTranslation($string, $class = 'AdminTab', $addslashes = false, $htmlentities = true, $sprintf = null)
	{
		return $string;
		static $modules_tabs = null;

// @todo remove global keyword in translations files and use static
		global $_LANGADM;

		if ($modules_tabs === null) {
			$modules_tabs = Tab::getContext()->getModuleTabList();
		}

		if ($_LANGADM == null) {
			$iso = Context::getContext()->language->iso_code;
			if (empty($iso)) {
				$iso = Language::getIsoById((int)Configuration::get('PS_LANG_DEFAULT'));
			}
			if (file_exists(_PS_TRANSLATIONS_DIR_.$iso.'/admin.php')) {
				include_once(_PS_TRANSLATIONS_DIR_.$iso.'/admin.php');
			}
		}

		if (isset($modules_tabs[strtolower($class)])) {
			$class_name_controller = $class.'controller';
// if the class is extended by a module, use modules/[module_name]/xx.php lang file
			if (class_exists($class_name_controller) && Module::getModuleNameFromClass($class_name_controller)) {
				return Translate::getModuleTranslation(Module::$classInModule[$class_name_controller], $string, $class_name_controller, $sprintf, $addslashes);
			}
		}

		$string = preg_replace("/\\\*'/", "\'", $string);
		$key = md5($string);
		if (isset($_LANGADM[$class.$key])) {
			$str = $_LANGADM[$class.$key];
		} else {
			$str = Translate::getGenericAdminTranslation($string, $key, $_LANGADM);
		}

		if ($htmlentities) {
			$str = htmlspecialchars($str, ENT_QUOTES, 'utf-8');
		}
		$str = str_replace('"', '&quot;', $str);

		if ($sprintf !== null) {
			$str = Translate::checkAndReplaceArgs($str, $sprintf);
		}

		return ($addslashes ? addslashes($str) : stripslashes($str));
	}

//取得Admin翻譯
	public function getClassDir($class,$string,$source){
		if(is_dir(ADMIN_DIR.DS.'translations'.DS.$class)){
			$name = $class;
			return $string;
		};
		return $string;
	}
}