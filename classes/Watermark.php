<?php
/**
 * Created by PhpStorm.
 * User: 陳哲儒
 * Date: 2020/08/03
 * Time: 上午 15:00
 */
class Watermark
{
    protected static $instance = null;        //是否實體化

    public static function getContext()
    {
        if (!self::$instance) {
            self::$instance = new Watermark();
        }
        return self::$instance;
    }

    public static function get_watermark($img,$water_img,$save){
        $allow_format = array('jpeg', 'png', 'gif');
        $sub_name = $t = '';

        // 原圖
        $img_info = getimagesize($img);
        $width    = $img_info['0'];
        $height   = $img_info['1'];
        $mime     = $img_info['mime'];

        list($t, $sub_name) = explode('/', $mime);
        if ($sub_name == 'jpg') $sub_name = 'jpeg';

        if (!in_array($sub_name, $allow_format))
            return false;
//
        $function_name = 'imagecreatefrom' . $sub_name;
        $image     = $function_name($img);

////                額外加的測試做法 start 拉大寬度至800px
//        if($width<'800'){//寬小於800要進行重置圖片作業
//            $height_zoom = round($width/800,2);//高度放大倍率
//            $height_big = round($height*$height_zoom);//四捨五入整數
//            $dst_im = imagecreatetruecolor(800, $height_big);
//            imagecopyresampled($dst_im, $image, 0, 0, 0, 0, 800, $height_big, $width, $height);
//            $image = $dst_im;
//            imagedestroy($dst_im);//刪除掉
//            $width = 800;
//            $height = $height_big;
//        }
////                  end

        // 浮水印
        $img_info = getimagesize($water_img);

        $w_width  = $img_info['0'];
        $w_height = $img_info['1'];
        $w_mime   = $img_info['mime'];

        list($t, $sub_name) = explode('/', $w_mime);
        if ($sub_name == 'jpg') $sub_name = 'jpeg';
        if (!in_array($sub_name, $allow_format))
            return false;
//

        $function_name = 'imagecreatefrom' . $sub_name;

        $watermark = $function_name($water_img);

//        $watermark_pos_x = 0;
//        $watermark_pos_y = 0;
        //圖800*600 水印501*160
//        $watermark_pos_x = $width-($w_width/1.25);
//        $watermark_pos_y = $height-($w_height*2);
        $watermark_pos_x = $width/2;
        $watermark_pos_y = $height/2;

//        echo "width:{$width} , {$height} <br/>";
//        echo "w_width:{$w_width} , {$w_height} <br/>";
//        echo "watermark_pos:{$watermark_pos_x} , {$watermark_pos_y} <br/>";

//        $watermark_pos_x = $width-($w_width/2);
//        $watermark_pos_y = $height-($w_height/2);


        // imagecopymerge($image, $watermark, $watermark_pos_x, $watermark_pos_y, 0, 0, $w_width, $w_height, 100);

        // 浮水印的圖若是透明背景、透明底圖, 需要用下述兩行
        imagesetbrush($image, $watermark);
        imageline($image, $watermark_pos_x, $watermark_pos_y, $watermark_pos_x, $watermark_pos_y, IMG_COLOR_BRUSHED);
        
        if(strstr($save,".jpg") || strstr($save,".jpeg") || strstr($save,".JPG") || strstr($save,".JPEG")){
            imagejpeg($image,$save);
        }else if(strstr($save,".png") || strstr($save,".PNG")){
            imagepng($image,$save);
        }else if(strstr($save,".gif") || strstr($save,".GIF")){
            imagegif($image,$save);
        }else{
            return false;
        }
        return true;
    }

    public static function get_watermark_text($img,$text,$save){
        $allow_format = array('jpeg', 'png', 'gif');
        $sub_name = $t = '';
        // 原圖
        $img_info = getimagesize($img);
        $width    = $img_info['0'];
        $height   = $img_info['1'];
        $mime     = $img_info['mime'];

        list($t, $sub_name) = explode('/', $mime);
        if ($sub_name == 'jpg') $sub_name = 'jpeg';

        if (!in_array($sub_name, $allow_format))
            return false;
//
        $function_name = 'imagecreatefrom' . $sub_name;
        $image     = $function_name($img);

        $textcolor = imagecolorallocate($image, 250, 250, 250);

        $str_long = mb_strlen($text);//字串長度
        //為了好看左右各家3字大小

        $str_long = $str_long+6;
        $string_size = floor($width/$str_long);

        $text_height = $height/2;//高度
        $text_width = $string_size;//寬度


//        imagestring($image, 5, $text_width, $text_height, $text, $textcolor);
        imagettftext($image,$string_size,0,$text_width,$text_height,$textcolor,WEB_DIR.DS.'fonts'.DS.'microsoft_true_black.ttf' ,$text);

        if(strstr($save,".jpg") || strstr($save,".jpeg") || strstr($save,".JPG") || strstr($save,".JPEG")){
            imagejpeg($image,$save);
        }else if(strstr($save,".png") || strstr($save,".PNG")){
            imagepng($image,$save);
        }else if(strstr($save,".gif") || strstr($save,".GIF")){
            imagegif($image,$save);
        }else{
            return false;
        }
        return true;
    }






}