<?php

namespace web_number_house;
use \NumberHouseController;
use \Tools;
use \ListUse;

class IndexController extends NumberHouseController
{

    public $page = 'index';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;
    public function __construct(){
        $this->meta_title = $this->l('1786房屋市集|一站到位的全台房地產資訊平台');
        $this->page_header_toolbar_title = $this->l('1786房屋市集');
        $this->className = 'IndexController';
        $this->no_FormTable = true;

//        註解  CTRL + /

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/about',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('1786房屋市集'),
            ],
            //這塊?　對
        ];
        parent::__construct();
    }


    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function initProcess()
    {
        parent::initProcess();

		$commodity_order_by = " ORDER by r_h.`id_rent_house` DESC";//精選優先 之後則是其他條件
    $commodity_sreach = " AND r_h.`featured`=1 ";
  	$commodity_notesbook_data = ListUse::commodity_data(null,1,4,$commodity_sreach,$commodity_order_by);

		//精選的物件
    $commodity_order_by = " ORDER by r_h.`featured` DESC,r_h.`id_rent_house` DESC";//精選優先 之後則是其他條件
		$commodity_sreach = " AND r_h.`featured`=1 ";
		$commodity_data = ListUse::commodity_data(null,1,999999,$commodity_sreach,$commodity_order_by);
js;
				//輪播
				        $js =<<<hmtl
				        <script>
				        $(".commodity_data").slick({
    				        dots: false,
    				        infinite: true,
    				        autoplay: true,
    				        autoplaySpeed: 3000,
    				        slidesToShow: 5,
    				        slidesToScroll: 5,

    				    });

				</script>
hmtl;

js;
				//輪播
		        $js1786bg =<<<hmtl
				        <script>
                $(window).scroll(function(){
                    $(".bg").css({backgroundPositionY:($(this).scrollTop()*-0.4)})
                        })
                        $(document).ready(function() {

                          $("#b_87_banner1_031074005").click(function(){
                              if($("#img_77").hasClass("hidden")){
                                  $("#img_77").hide()
                                  $("#img_77").removeClass("hidden")
                                  $("#img_77").show(200)
                              }else{
                                  $("#img_77").hide(200,function(){ $("#img_77").prop("class","hidden") });

                              }
                          })

                        })
        				</script>
hmtl;


		$this->context->smarty->assign([
		    'A1786_HTML' => $A1786_HTML,
		    'A1786_JS'   => $A1786_JS,
		    'A1786_CSS'  => $A1786_CSS,
		    'commodity_data' => $commodity_data,
        'commodity_notesbook_data' => $commodity_notesbook_data,
		    'js'               => $js,
            'js1786bg'               => $js1786bg,
            'group_link'    =>  0,
		]);

    }

		public function initContent()
		{
		  parent::initContent();
			$this->context->smarty->assign([
			]);

		}

    public function setMedia()
    {
//        $this->addJS('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js');
//        $this->addJS('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js');
        $this->addJS('https://unpkg.com/aos@2.3.1/dist/aos.js');
        $this->addCSS('https://unpkg.com/aos@2.3.1/dist/aos.css');
        parent::setMedia();
        $this->addJS('/js/slick.min.js');
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');
        $this->addCSS(THEME_URL . '/css/about-animate.css');
        $this->addCSS(THEME_URL . '/css/about-normalize.css');
    }

}
