<?php
/* Smarty version 3.1.28, created on 2021-03-20 17:47:16
  from "/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6055c4a4004a43_88006889',
  'file_dependency' => 
  array (
    '005fa90180617dd2887e7c2747b5e0b0f7587e03' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/footer.tpl',
      1 => 1616233616,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../tpl/group_link.tpl' => 1,
  ),
),false)) {
function content_6055c4a4004a43_88006889 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['group_link']->value == 1) {?>
  <section class="slider_center">
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../tpl/group_link.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </section>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['display_footer']->value) {?>

<?php $_smarty_tpl->tpl_vars['logo'] = new Smarty_Variable("<img style='width: 3em;margin: 0 5px;color:#f0047f;' src='/themes/Rent/img/1786/logo.png'>聯絡我們", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'logo', 0);
$_smarty_tpl->tpl_vars['logo'] = new Smarty_Variable("聯絡我們", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'logo', 0);
$_smarty_tpl->tpl_vars['share_icons'] = new Smarty_Variable("<div class='linkto share_infor_box'><div class='l'><a class='icon_block FB'><img src='/themes/Rent/img/1786/fb.svg' style-top='margin:1em;'></a></div><div class='l'><a class='icon_block LINE'><img src='/themes/Rent/img/1786/line.svg' style='margin-top:1em;'></a></div></div>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'share_icons', 0);
$_smarty_tpl->tpl_vars['webmap_t'] = new Smarty_Variable(array("租售好幫手","友好連結","相關聲明","關於我們",$_smarty_tpl->tpl_vars['logo']->value), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_t', 0);?>

<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value["關於我們"] = array("品牌理念","歷史軌跡","媒體公關","合作提案");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>

<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value["租售好幫手"] = array("房貸專區","稅費試算##https://www.etax.nat.gov.tw/etwmain/front/ETW118W/VIEW/23","市價行情","實價登錄##http://210.65.131.75/");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>


<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value["友好連結"] = array("裝修達人##https://www.epro.house/","好幫手##https://okpt.life/","共享圈##https://www.okrent.tw/","生活樂購##https://www.lifegroup.house/LifeGo");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>

<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value["相關聲明"] = array("免責聲明","隱私權聲明","服務條款");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>

<?php $_smarty_tpl->tpl_vars['logo'] = new Smarty_Variable("<br>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'logo', 0);?>

<?php $_smarty_tpl->tpl_vars['webmap_t'] = new Smarty_Variable(array("買賣好幫手","租屋好幫手","關於我們",$_smarty_tpl->tpl_vars['logo']->value), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_t', 0);?>

<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value["買賣好幫手"] = array("買賣照過來","房貸專區","稅費試算","市價行情","實價登錄");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>

<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value["租屋好幫手"] = array("租屋照過來","租金補貼","社會住宅","包租代管");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>

<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value["關於我們"] = array("合作提案","服務條款","隱私權聲明","免責聲明");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>

<!--<?php $_smarty_tpl->tpl_vars['mail_icon'] = new Smarty_Variable("<svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='envelope' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' class='svg-inline--fa fa-envelope fa-w-16 fa-3x' style='font-size: 1em;margin: 0 5px;color:#f0047f;'><path fill='currentColor' d='M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z' class=''></path></svg>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'mail_icon', 0);?>-->
<?php $_smarty_tpl->tpl_vars['mail_icon'] = new Smarty_Variable("<img class='bnfy-enter' width='37'  src='/themes/Rent/img/1786/images/210315/1786_index_210315_40.jpg' style='margin-top:2em 0'>　", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'mail_icon', 0);
$_smarty_tpl->tpl_vars['clock_icon'] = new Smarty_Variable("<svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='clock' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' class='svg-inline--fa fa-clock fa-w-16 fa-3x' style='font-size: 1em;margin: 0 5px;color:#f0047f;'><path fill='currentColor' d='M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z' class=''></path></svg>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'clock_icon', 0);?>


<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'webmap_c', null);
$_smarty_tpl->tpl_vars['webmap_c']->value[$_smarty_tpl->tpl_vars['logo']->value] = array("若對平台使用上有任何建議與疑問請洽：",($_smarty_tpl->tpl_vars['mail_icon']->value).("mail | 1786@lifegroup.house##mailto:1786@lifegroup.house"),$_smarty_tpl->tpl_vars['share_icons']->value);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c', 0);?>





<div class="webmap">
  <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['webmap_t']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['webmap_t']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
  <ul>
    <li><?php echo $_smarty_tpl->tpl_vars['webmap_t']->value[$_smarty_tpl->tpl_vars['i']->value];?>

      <ul>
        <?php
$_smarty_tpl->tpl_vars['ix'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['ix']->value = 0;
if ($_smarty_tpl->tpl_vars['ix']->value < sizeof($_smarty_tpl->tpl_vars['webmap_c']->value[$_smarty_tpl->tpl_vars['webmap_t']->value[$_smarty_tpl->tpl_vars['i']->value]])) {
for ($_foo=true;$_smarty_tpl->tpl_vars['ix']->value < sizeof($_smarty_tpl->tpl_vars['webmap_c']->value[$_smarty_tpl->tpl_vars['webmap_t']->value[$_smarty_tpl->tpl_vars['i']->value]]); $_smarty_tpl->tpl_vars['ix']->value++) {
?>
        <?php $_smarty_tpl->tpl_vars['webmap_c_0'] = new Smarty_Variable($_smarty_tpl->tpl_vars['webmap_c']->value[$_smarty_tpl->tpl_vars['webmap_t']->value[$_smarty_tpl->tpl_vars['i']->value]][$_smarty_tpl->tpl_vars['ix']->value], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c_0', 0);?>
        <?php $_smarty_tpl->tpl_vars['webmap_c_0f'] = new Smarty_Variable("#", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c_0f', 0);?>
        <?php if (stripos($_smarty_tpl->tpl_vars['webmap_c_0']->value,'##')) {?>
          <?php $_smarty_tpl->tpl_vars['webmap_c_0s'] = new Smarty_Variable(explode("##",$_smarty_tpl->tpl_vars['webmap_c']->value[$_smarty_tpl->tpl_vars['webmap_t']->value[$_smarty_tpl->tpl_vars['i']->value]][$_smarty_tpl->tpl_vars['ix']->value]), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c_0s', 0);?>
          <?php $_smarty_tpl->tpl_vars['webmap_c_0f'] = new Smarty_Variable($_smarty_tpl->tpl_vars['webmap_c_0s']->value[1], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c_0f', 0);?>
          <?php $_smarty_tpl->tpl_vars['webmap_c_0'] = new Smarty_Variable($_smarty_tpl->tpl_vars['webmap_c_0s']->value[0], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'webmap_c_0', 0);?>
        <?php }?>
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['webmap_c_0f']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['webmap_c_0']->value;?>
</a>
        <?php }
}
?>

      </ul>
    </li>
  </ul>
  <?php }
}
?>

</div>
    <footer>
        <div class="footer A1786_footer">
            <table>
                <tbody>
                <tr>
                    <td>
                        <div class="footer_txt2"><a href="/Aboutokmaster" class="hidden"><img src="/themes/Okmaster/img/index/footer_logo.png">生活好科技有限公司 </a><span class="spacing"></span> Copyright ©2021 by Life Master Technology Co., Ltd All Rights reserved <span class="spacing"></span> <!--<a style='margin-left: 20%;' href='https://www.okmaster.life/Aboutokmaster'>合作提案</a> |--> <a href="/State">相關聲明</a></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </footer>
    <?php }
$_from = $_smarty_tpl->tpl_vars['css_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_0_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_0_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css" />
<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_local_item;
}
}
if ($__foreach_css_uri_0_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_item;
}
if ($__foreach_css_uri_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_0_saved_key;
}
$_from = $_smarty_tpl->tpl_vars['js_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_1_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_1_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>
<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_local_item;
}
}
if ($__foreach_js_uri_1_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_item;
}
if ($__foreach_js_uri_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_1_saved_key;
}
?>
</div>
</body>

<?php echo '<script'; ?>
>
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 9
    });
<?php echo '</script'; ?>
>

</html>
<?php }
}
