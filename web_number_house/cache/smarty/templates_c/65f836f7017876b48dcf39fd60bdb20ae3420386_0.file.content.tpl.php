<?php
/* Smarty version 3.1.28, created on 2021-03-20 17:57:46
  from "/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6055c71a637f02_85288343',
  'file_dependency' => 
  array (
    '65f836f7017876b48dcf39fd60bdb20ae3420386' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/controllers/Index/content.tpl',
      1 => 1616234250,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:house_item_list.tpl' => 1,
    'file:house_salesitem_list.tpl' => 2,
  ),
),false)) {
function content_6055c71a637f02_85288343 ($_smarty_tpl) {
?>

<style>

img#img_87_banner1_031074005 {
    position: absolute;
    left: 0px;
}

#img_92_banner1_031074005 {
    top: 48px;
    left: 80px;
    z-index: 92;
    position: absolute;
}

#img_93_banner1_031074005 {
    position: absolute;
    top: 48px;
    /*top: 1541px;*/
    left: 155px;
    /*left: 912px;*/
    z-index: 93;
}



#img_90_banner1_031074005 {
    position: absolute;
    top: 52px;
    left: -99px;
    z-index: 90;
}

#img_89_banner1_031074005 {
    position: absolute;
    top: -6px;
    left: 301px;
    z-index: 89;
}

#img_91_banner1_031074005 {
    position: absolute;
    top: calc(1711.6666679382324px - 1541px);
    left: calc(779px - 912px);
    z-index: 91;
}

#img_88_banner1_031074005 {
    position: absolute;
    top: calc(1685px - 1541px);
    left: calc(1242px - 912px);
    z-index: 88;
}

div#b_87 > div {
    width: 50%;
}

.type_class label a {
    font-size: 18pt;
}

.type_class label a.active {
    color: #d9167d;
    padding: 0px;
    font-size: 26pt;
}

.imitate_btn {
    background: #FFF;
    color: #d9167d;
    border-radius: 0;
    padding: 0;
    width: 9rem !important;
}

.type_class label {
  width: 7rem;
  word-break: keep-all;
}

.img_table img {
    margin: .5%;
    width: 25%;
}

.webmap a {
    margin: 0;
}

.webmap > ul > li > ul > li {
    padding-left: 0;
}

.webmap > ul {
    margin: 1em 10%;
    margin-left: 0px;
}

div#b_87 div#b_99_banner1_031074005 {
    margin: auto;
}

img#img_69_banner1_031074005, img#img_70_banner1_031074005, img#img_68_banner1_031074005  {
    margin-left: 3rem;
}

img#img_59_banner1_031074005 {
  position: absolute;
  top: 76.733597px;
  left: 47px;
  z-index: 59;
  width: 1200px;
}

img#img_61_banner1_031074005 {
    position: absolute;
    top: calc(2309px - 2271px);
    left: calc(1107px - 67px);
    z-index: 61;
}

img#img_60_banner1_031074005 {
    position: absolute;
    top: calc(2298px - 2271px);
    left: calc(118px - 67px);
    z-index: 60;
}
img#img_136_banner1_031074005 {
    position: absolute;
    top: calc(2281px - 2271px);
    left: calc(239px - 67px);
    z-index: 136;
}
img#img_139_banner1_031074005 {
    position: absolute;
    top: calc(2284px - 2271px);
    left: calc(1080px - 67px);
    z-index: 139;
}
img#img_137_banner1_031074005 {
    position: absolute;
    top: calc(2261px - 2271px);
    left: calc(268px - 67px);
    z-index: 137;
}
img#img_135_banner1_031074005 {
    position: absolute;
    top: calc(2278.75px - 2271px);
    left: calc(93.00779724121094px - 67px);
    z-index: 135;
}

img#img_122_banner1_031074005 {
position: absolute;
    top: calc(2237.5px - 2238px);
    left: calc(87px - 73px);
    z-index: 122;
}
img#img_141_banner1_031074005 {
position: absolute;
    top: calc(2241.7844314575195px - 2238px);
    left: calc(97px - 73px);
    z-index: 141;
}
img#img_123_banner1_031074005 {
position: absolute;
    top: calc(2224px - 2238px);
    left: calc(134px - 73px);
    z-index: 123;
}
img#img_140_banner1_031074005 {
position: absolute;
top: calc(0px);
left: calc(0px);
z-index: 140;
}

.type_class {
    margin-top: 5em;
}

.display_content {
    min-width: 1330px;
}

.A1786_row.notesbook {
    margin: unset;
    margin-left: inherit;
}

.A1786_row.notesbook .slick-track {
    width: 100% !important;
    margin-left: 4rem;
}

.A1786_row .commodity_list.commodity_data.notesbook.slick-initialized.slick-slider .slick-slide{
    margin-right: 6em;
    width: 180px !important;
    margin-left: 0px;
}

.A1786_row .commodity_list.commodity_data.notesbook.slick-initialized.slick-slider .slick-slide:last-child{
    margin-right: 0px;
}

.notes_item_1 {
  margin: 1em 0px;
  font-size: x-large;
  text-align: start;
}
.notes_item_2 {
  font-size: x-large;
  color: #f1209e;
  text-align: start;
}
.notes_item_3 {
  text-align: start;
}
div#notesbook {
  position: relative;
  width: 1400px;
  margin: auto;
  padding-left: 7rem;
}

div#notesbook > div {
    width: 18%;
    vertical-align: top;
    margin-left: 6%;
}

img#img_77_banner4_647241971 {
    top: 0px;
    left: 349.8263244628906px;
    z-index: 82;
    position: absolute;
}

img#img_82_banner4_647241971 {
    top: calc(743.99462890625px - 586.5815734863281px);
    left: calc(349.8263244628906px + 96px);
    z-index: 82;
    position: absolute;
}

img#img_80_banner4_647241971 {
    top: calc(811px - 586.5815734863281px);
    left: calc(893px + 96px);
    z-index: 82;
    position: absolute;
}


img#img_78_banner4_647241971 {
    top: calc(960px - 586.5815734863281px);
    left: calc(874px + 96px);
    z-index: 82;
    position: absolute;
}

img#img_81_banner4_647241971 {
    top: calc(1115px - 586.5815734863281px);
    left: calc(871px + 96px);
    z-index: 82;
    position: absolute;
}

img#img_79_banner4_647241971 {
    top: calc(1273px - 586.5815734863281px);
    left: calc(866px + 96px);
    z-index: 82;
    position: absolute;
}
</style>

<div class="head_div hidden">
  <div class="col-sm-3 l"><img src="/themes/Rent/img/1786/logo.png"></div>
  <div class="col-sm-5 m">
    <div class="row tab">
      <?php $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable(array("中古屋","新建案","店辦","辦公室","廠房","倉庫","土地"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'tab', 0);?>
      <?php $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable(array("租屋","中古屋","新建案","店面","辦公室","廠房倉庫","土地"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'tab', 0);?>
      <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['tab']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['tab']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
      <label><a><?php echo $_smarty_tpl->tpl_vars['tab']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</a></label>|
      <?php }
}
?>

      </div>
    <div class="row search">
      <select class="sales"><option>出租</option><option>出售</option></select>
      <select class="city" onchange="cell_window_append_cityurl(this.value)">
        <option disabled="" selected="">桃園市</option>
        <option data-content="台北市" class="county_0" value="county_0">台北市</option><option data-content="基隆市" class="county_1" value="county_1">基隆市</option><option data-content="新北市" class="county_2" value="county_2">新北市</option><option data-content="連江縣" class="county_3" value="county_3">連江縣</option><option data-content="宜蘭縣" class="county_4" value="county_4">宜蘭縣</option><option data-content="新竹市" class="county_5" value="county_5">新竹市</option><option data-content="新竹縣" class="county_6" value="county_6">新竹縣</option><option data-content="桃園市" class="county_7" value="county_7">桃園市</option><option data-content="苗栗縣" class="county_8" value="county_8">苗栗縣</option><option data-content="台中市" class="county_9" value="county_9">台中市</option><option data-content="彰化縣" class="county_10" value="county_10">彰化縣</option><option data-content="南投縣" class="county_11" value="county_11">南投縣</option><option data-content="嘉義市" class="county_12" value="county_12">嘉義市</option><option data-content="嘉義縣" class="county_13" value="county_13">嘉義縣</option><option data-content="雲林縣" class="county_14" value="county_14">雲林縣</option><option data-content="台南市" class="county_15" value="county_15">台南市</option><option data-content="高雄市" class="county_16" value="county_16">高雄市</option><option data-content="澎湖縣" class="county_17" value="county_17">澎湖縣</option><option data-content="金門縣" class="county_18" value="county_18">金門縣</option><option data-content="屏東縣" class="county_19" value="county_19">屏東縣</option><option data-content="台東縣" class="county_20" value="county_20">台東縣</option><option data-content="花蓮縣" class="county_21" value="county_21">花蓮縣</option></select>
      <input type="text" placeholder="請輸入社區或街道名稱">
      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16 fa-3x"><path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" class=""></path></svg>
    </div>
  </div>
  <div class="col-sm-4 r">
    <div class="row tab">
    <label class="b_right_line"><a>登入</a></label>
    <label class="b_right_line"><a>註冊</a></label>
    <!-- <label class="b_right_line"><a>客服</a></label>
    <label class="b_right_line"><a>分享</a></label>
    <label><a>免費刊登</a></label> -->
    <label class="no_width hidden"><a><img style="width: 18pt;" class="share" src="/img/share-alt-solid-777.svg"></a></label>
    <label class="no_width hidden"><a><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve" style="fill: #777;width: 42pt;">
                    <g>
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,21.992h97.498c2.363,0,4.296,1.933,4.296,4.509l0,0c0,2.578-1.934,4.512-4.296,4.512H9.75c-2.363,0-4.295-1.934-4.295-4.512   l0,0C5.455,23.925,7.387,21.992,9.75,21.992z"></path>
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,85.773h97.498c2.363,0,4.296,2.147,4.296,4.511l0,0c0,2.576-1.934,4.725-4.296,4.725H9.75c-2.363,0-4.295-2.149-4.295-4.725   l0,0C5.455,87.921,7.387,85.773,9.75,85.773z"></path>
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,53.774h97.498c2.363,0,4.296,2.15,4.296,4.726l0,0c0,2.362-1.934,4.51-4.296,4.51H9.75c-2.363,0-4.295-2.147-4.295-4.51l0,0   C5.455,55.924,7.387,53.774,9.75,53.774z"></path>
                    </g>
          <g>
            <defs>
              <path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"></path>
            </defs>
            <clipPath id="SVGID_2_">
              <use xlink:href="#SVGID_51_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"></rect>
            </defs>
            <clipPath id="SVGID_4_">
              <use xlink:href="#SVGID_81_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"></rect>
            </defs>
            <clipPath id="SVGID_6_">
              <use xlink:href="#SVGID_105_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"></rect>
            </defs>
            <clipPath id="SVGID_8_">
              <use xlink:href="#SVGID_129_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"></rect>
            </defs>
            <clipPath id="SVGID_10_">
              <use xlink:href="#SVGID_155_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"></rect>
            </defs>
            <clipPath id="SVGID_12_">
              <use xlink:href="#SVGID_183_" overflow="visible"></use>
            </clipPath>
          </g>
          <image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
          </image>
                    </svg></a></label>
        </div>
        <div class="row search">
          <label><a>免費刊登</a></label>
        </div>
  </div>
</div>
<div class="bg">
  <!-- <img src="/themes/Rent/img/1786/1100120-1.JPG">
  <img src="/themes/Rent/img/1786/1100120-2.JPG"> -->
</div>
<!-- <div class="introduction hidden">
  <div class="col-sm-6 l">
    <ul>
      <li class="nostyle"><label class="H_light">租客</label> 省時省力</li>
      <li>房源眾多 找屋快速</li>
      <li>以條件搜尋屋源 找屋好輕鬆</li>
      <li>需求登錄留言 系統自動配對回報</li>
      <li>雲總機保護您 免除干擾或被迫接收</li>
      <li class="nostyle">推銷訊息而疲勞轟炸</li>
    </ul>
  </div>
  <div class="col-sm-6 r">
    <ul>
      <li class="nostyle"><label class="H_light">房東</label> 省心樂租</li>
      <li>客源超廣 出租快速</li>
      <li>不止刊登還主動找租客</li>
      <li>系統自動媒合租客。主動出擊</li>
      <li>預約看屋及租客提問 能掌握市況反應</li>
      <li>雲總機保護您 免除干擾或被迫接收</li>
      <li class="nostyle">推銷訊息而疲勞轟炸</li>
    </ul>
  </div>
</div> -->
<div class="bg" style="height: 260px;"></div>
<div class="text_box hidden">
  <label class="imitate_btn">樂租推薦</label>
</div>

<!--
<div style="text-align: center;" id="b_66_banner1_031074005" class="js-bnfy type_class">
			<img id="img_66_banner1_031074005" class="bnfy-enter" alt="比記本" width="155" height="65" src="/themes/Rent/img/1786/images/_363347991.svg">
		</div>
-->

    <div style="text-align: center;margin-bottom:0;" id="b_67_banner1_031074005" class="js-bnfy type_class">
    			<img id="img_67_banner1_031074005" class="bnfy-enter" alt="Component 37" width="1283" height="125" src="/themes/Rent/img/1786/images/210315/1786_index_210315_03.jpg">
    		</div>
<!--
<?php if (count($_smarty_tpl->tpl_vars['commodity_notesbook_data']->value)) {
} else { ?>
  <div class="A1786_row notesbook">
    <div class="commodity_list commodity_data notesbook">
      <?php
$_from = $_smarty_tpl->tpl_vars['commodity_notesbook_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
} else {
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>
    </div>
  </div>
<?php }
$_smarty_tpl->tpl_vars['notes_item_1'] = new Smarty_Variable(array("格林公園綠意景觀","昭揚越","昭揚越優質露...","柏悅景觀三房..."), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'notes_item_1', 0);
$_smarty_tpl->tpl_vars['notes_item_2'] = new Smarty_Variable(array("1280 ","1980 ","1588 ","1620 "), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'notes_item_2', 0);
$_smarty_tpl->tpl_vars['notes_item_3'] = new Smarty_Variable(array("桃園市桃園力行路","桃園市桃園保...","桃園市桃園保...","桃園市桃園吉..."), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'notes_item_3', 0);
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
<div style="" id="notesbook" class="notesbook js-bnfy">
    <div style="display: inline-grid;text-align: start;" id="img_51" class="img_51 js-bnfy">
      <img id="img_51_banner2_243078050" class="bnfy-enter" alt="Component 31" width="191" height="146" src="/themes/Rent/img/1786/images/component-31_489457476.png">
      <label class="notes_item_1"><?php echo $_smarty_tpl->tpl_vars['notes_item_1']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label>
      <label class="notes_item_2"><?php echo $_smarty_tpl->tpl_vars['notes_item_2']->value[$_smarty_tpl->tpl_vars['i']->value];?>
<label style="color: #000;">萬</label></label>
      <label class="notes_item_3"><?php echo $_smarty_tpl->tpl_vars['notes_item_3']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
    </div>
    <div style="display: inline-grid;text-align: start;" id="img_53" class="img_53 js-bnfy">
      <img id="img_62_banner2_243078050" class="bnfy-enter" alt="Component 32" width="191" height="146" src="/themes/Rent/img/1786/images/component-32_471065242.png">
      <label class="notes_item_1"><?php echo $_smarty_tpl->tpl_vars['notes_item_1']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label>
      <label class="notes_item_2"><?php echo $_smarty_tpl->tpl_vars['notes_item_2']->value[$_smarty_tpl->tpl_vars['i']->value];?>
<label style="color: #000;">萬</label></label>
      <label class="notes_item_3"><?php echo $_smarty_tpl->tpl_vars['notes_item_3']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
    </div>
    <div style="display: inline-grid;text-align: start;" id="img_63" class="img_63 js-bnfy">
      <img id="img_63_banner2_243078050" class="bnfy-enter" alt="Component 33" width="191" height="146" src="/themes/Rent/img/1786/images/component-33_796062894.png">
      <label class="notes_item_1"><?php echo $_smarty_tpl->tpl_vars['notes_item_1']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label>
      <label class="notes_item_2"><?php echo $_smarty_tpl->tpl_vars['notes_item_2']->value[$_smarty_tpl->tpl_vars['i']->value];?>
<label style="color: #000;">萬</label></label>
      <label class="notes_item_3"><?php echo $_smarty_tpl->tpl_vars['notes_item_3']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
    </div>
    <div style="display: inline-grid;text-align: start;" id="img_63" class="img_63 js-bnfy">
      <div style="overflow: hidden;    height: 146px;" id="img_63img" class="img_63img js-bnfy"><img id="img_64_banner2_243078050" class="bnfy-enter" alt="Component 36" width="192" height="260" src="/themes/Rent/img/1786/images/component-36_812920553.png"></div>
      <label class="notes_item_1"><?php echo $_smarty_tpl->tpl_vars['notes_item_1']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label>
      <label class="notes_item_2"><?php echo $_smarty_tpl->tpl_vars['notes_item_2']->value[$_smarty_tpl->tpl_vars['i']->value];?>
<label style="color: #000;">萬</label></label>
      <label class="notes_item_3"><?php echo $_smarty_tpl->tpl_vars['notes_item_3']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</label><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
    </div>
</div>
-->
<div style="text-align: center;" id="b_57_banner1_031074005" class="js-bnfy">
	<img id="img_57_banner1_031074005" class="bnfy-enter" alt="Component 27" width="1295"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_05.jpg">
</div>
<br><br><br><br><br><br>
<div style="text-align: center;" id="b_57_banner1_031074005" class="js-bnfy">
	<img id="img_57_banner1_031074005" class="bnfy-enter" alt="Component 27" width="1283"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_07.jpg">
</div>
<div style="text-align: center;" id="b_57_banner1_031074005" class="js-bnfy">
	<img id="img_57_banner1_031074005" class="bnfy-enter" alt="Component 27" width="1283"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_08.jpg">
</div>

        <!--
        <div style="display: block;position: relative;margin-left: 200px;height: 470px;cursor: pointer;" id="b_87_banner1_031074005" class="js-bnfy">
    			<img id="img_87_banner1_031074005" class="bnfy-enter" alt="image 45" width="345" height="470" src="/themes/Rent/img/1786/images/image-45_061274478.jpg">
          <img id="img_92_banner1_031074005" class="bnfy-enter" alt="Rectangle 99" width="165" height="14" src="/themes/Rent/img/1786/images/rectangle-99_711161221.svg">
          <img id="img_93_banner1_031074005" class="bnfy-enter" alt="image 49" width="20" height="15" src="/themes/Rent/img/1786/images/image-49_879180726.png">
          <img id="img_90_banner1_031074005" class="bnfy-enter" alt="image 47" width="110" height="108" src="/themes/Rent/img/1786/images/image-47_475609276.jpg">
          <img id="img_89_banner1_031074005" class="bnfy-enter" alt="image 46" width="110" height="108" src="/themes/Rent/img/1786/images/image-46_247744415.jpg">
          <img id="img_91_banner1_031074005" class="bnfy-enter" alt="image 48" width="120" height="110" src="/themes/Rent/img/1786/images/image-48_059824320.jpg">
          <img id="img_88_banner1_031074005" class="bnfy-enter" alt="Component 4" width="120" height="123" src="/themes/Rent/img/1786/images/component-4_996228709.jpg">
    		</div>
        -->
        <div style="display: block;position: absolute;z-index: 999;width: 100%;margin-top: 10rem;" id="img_77" class="js-bnfy hidden">
          <img id="img_77_banner4_647241971" class="bnfy-enter" alt="Rectangle 100" width="935" height="998" src="/themes/Rent/img/1786/images/rectangle-100_665297734.png">
          <img id="img_82_banner4_647241971" class="bnfy-enter" alt="Rectangle 101" width="456" height="657" src="/themes/Rent/img/1786/images/rectangle-101_958009275.png">
          <img id="img_80_banner4_647241971" class="bnfy-enter" alt="AppStore" width="136" height="133" src="/themes/Rent/img/1786/images/appstore_799602723.svg">
          <img id="img_78_banner4_647241971" class="bnfy-enter" alt="App Store" width="175" height="65" src="/themes/Rent/img/1786/images/app-store_188530515.svg">
          <img id="img_81_banner4_647241971" class="bnfy-enter" alt="GooglePlay" width="181" height="167" src="/themes/Rent/img/1786/images/googleplay_154459242.svg">
          <img id="img_79_banner4_647241971" class="bnfy-enter" alt="Google Pay" width="200" height="65" src="/themes/Rent/img/1786/images/google-pay_781112242.svg">
        </div>


<!--
    <div style="display: block;text-align: center;" id="b_71_banner1_031074005" class="js-bnfy type_class">
    			<img id="img_71_banner1_031074005" class="bnfy-enter" alt="街頭巷尾" width="208" height="65" src="/themes/Rent/img/1786/images/_668625957.svg">
    		</div>

    <div style="text-align: center;position: relative;" id="b_74" class="js-bnfy">
      <img id="img_70_banner1_031074005" class="bnfy-enter" alt="Ellipse 37" width="41" height="39" src="/themes/Rent/img/1786/images/ellipse-37_797596308.png">
      <img id="img_73_banner1_031074005" class="bnfy-enter" alt="社區評價參考" width="196" height="65" src="/themes/Rent/img/1786/images/_130732494.svg">
      <img id="img_68_banner1_031074005" class="bnfy-enter" alt="Ellipse 36" width="41" height="39" src="/themes/Rent/img/1786/images/ellipse-36_071057995.png">
      <img id="img_72_banner1_031074005" class="bnfy-enter" alt="社群討論分享" width="196" height="65" src="/themes/Rent/img/1786/images/_082018611.svg">
      <img id="img_69_banner1_031074005" class="bnfy-enter" alt="Ellipse 38" width="41" height="39" src="/themes/Rent/img/1786/images/ellipse-38_278708083.png">
    	<img id="img_74_banner1_031074005" class="bnfy-enter" alt="看看大家怎麼說" width="229" height="65" src="/themes/Rent/img/1786/images/_574636438.svg">
    </div>

    <style>

    </style>

        <div style="display: block;text-align: center;position: relative;" id="b_125_banner1_031074005" class="js-bnfy">
          <div style="display: block;text-align: center;position: relative;width: 1289px;margin: auto;height: 30px;" id="b_125" class="js-bnfy">
            <img id="img_140_banner1_031074005" class="bnfy-enter" alt="Rectangle 175" width="15" height="30" src="/themes/Rent/img/1786/images/rectangle-175_927821950.svg">
            <img id="img_141_banner1_031074005" class="bnfy-enter" alt="image 50" width="35" height="26" src="/themes/Rent/img/1786/images/image-50_964949297.jpg">
            <img id="img_123_banner1_031074005" class="bnfy-enter" alt="1786房屋市集" width="97" height="65" src="/themes/Rent/img/1786/images/1786_423639498.svg">
            <img id="img_122_banner1_031074005" class="bnfy-enter" alt="Rectangle 169" width="140" height="31" src="/themes/Rent/img/1786/images/rectangle-169_880976148.svg">
    			<img style="width: 1133px;position: absolute;right: calc(0px);" id="img_125_banner1_031074005" class="bnfy-enter" alt="Rectangle 170" width="" height="30" src="/themes/Rent/img/1786/images/rectangle-170_491128063.svg">
    		</div>
      </div>



            <div style="display: block;text-align: center;position: relative;" id="b_58_banner1_031074005" class="js-bnfy">
              <div style="display: block;text-align: center;position: relative;width: 1289px;margin: auto;" id="b_58" class="js-bnfy">
                <img id="img_139_banner1_031074005" class="bnfy-enter" alt="Star 9" width="20" height="20" src="/themes/Rent/img/1786/images/star-9_530248033.svg">
                <img id="img_136_banner1_031074005" class="bnfy-enter" alt="Rectangle 173" width="872" height="24" src="/themes/Rent/img/1786/images/rectangle-173_934664168.svg">
                <img id="img_137_banner1_031074005" class="bnfy-enter" alt="https://1786.house/" width="158" height="65" src="/themes/Rent/img/1786/images/https-1786house_721137245.svg">
                <img id="img_135_banner1_031074005" class="bnfy-enter" alt="Component 77" width="128" height="28" src="/themes/Rent/img/1786/images/component-77_397850193.svg">
                <img id="img_60_banner1_031074005" class="bnfy-enter" alt="最新回覆" width="130" height="65" src="/themes/Rent/img/1786/images/_479425107.svg">
                <img id="img_61_banner1_031074005" class="bnfy-enter" alt="Component 74" width="214" height="40" src="/themes/Rent/img/1786/images/component-74_113817818.png">
          			<img id="img_58_banner1_031074005" class="bnfy-enter" alt="Rectangle 168" width="" height="555" src="/themes/Rent/img/1786/images/rectangle-168_008557193.svg" style="width: 1289px;">
                <img id="img_59_banner1_031074005" class="bnfy-enter" alt="Component 59" width="" height="462" src="/themes/Rent/img/1786/images/component-59_688475345.svg">
              </div>
        		</div>
-->

    <div class="type_class" style="padding-left:4em;">
    <a class="active imitate_btn">
      <img id="img_57_banner1_031074005" class="bnfy-enter" alt="Component 27" width="275"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_10.jpg"  style="float:left;">
      </a>
    </div>
    <br>
    <div class="type_class">
      <!--
      <label class="b_right_linex imitate_btn"><a class="active imitate_btn">我要租屋</a></label>
      -->
      <label class="b_right_linex"><a >補助津貼 | </a></label>
      <label class="b_right_linex"><a>老幼安居 | </a></label>
      <label class="b_right_linex"><a>寵物友善 | </a></label>
      <label class="b_right_linex"><a>友愛無礙 | </a></label>
      <label class="b_right_linex"><a>社會住宅 | </a></label>
      <label class="b_right_linex"><a>短期季租 | </a></label>
      <label class="b_right_linex"><a>共享商辦 | </a></label>
      <label><a>盤出頂讓</a></label>
    </div>

<?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
  <div class="A1786_row">
    <div class="commodity_list commodity_data">
      <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:house_salesitem_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
} else {
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_1_saved_key;
}
?>
    </div>
  </div>
<?php }?>

    <div class="type_class" style="padding-left:4em;">
    <a class="active imitate_btn">
      <img id="img_57_banner1_031074005" class="bnfy-enter" alt="Component 27" width="275"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_13.jpg"  style="float:left;">
      </a>
    </div>
    <br>
    
<?php $_smarty_tpl->tpl_vars['type_class'] = new Smarty_Variable(array('',"換 屋　|　","投 資　|　","捷 運 宅　|　","小 資 族　|　","銀 髮 族　|　","商 用　|　","土 地　　　"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'type_class', 0);?>
<div class="type_class">
  <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['type_class']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['type_class']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
    <label <?php if ($_smarty_tpl->tpl_vars['i']->value+1 < sizeof($_smarty_tpl->tpl_vars['type_class']->value)) {
if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>style=" height: 2em;" <?php }?>class="b_right_linex <?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>imitate_btn<?php }?>"<?php }?>><a <?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>class="active imitate_btn"<?php }?>><?php echo $_smarty_tpl->tpl_vars['type_class']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</a></label>


  <?php }
}
?>

</div>

<?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
  <div class="A1786_row">
    <div class="commodity_list commodity_data">
      <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:house_salesitem_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
} else {
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_2_saved_key;
}
?>
    </div>
  </div>
<?php }?>


<div class="type_class" style="padding-left:8em;float:left;width:100%;text-align: left;">
    <a class="active imitate_btn">
      <img id="img_57_banner1_031074005" class="bnfy-enter" alt="Component 27" width="275" src="/themes/Rent/img/1786/images/210315/1786_index_210315_15.jpg">
      </a>
    </div>



<div class="img_table" style="margin-bottom:5em;">
  <img id="img_113_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-1 1" width="419"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_18.jpg">
  <img id="img_114_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-2 1" width="419"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_20.jpg">
  <img id="img_115_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-3 1" width="418"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_22.jpg">
  <img id="img_116_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-4 1" width="419"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_27.jpg">
  <img id="img_117_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-5 1" width="419"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_28.jpg">
  <img id="img_118_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-6 1" width="418"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_29.jpg">
  <img id="img_119_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-7 1" width="419"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_33.jpg">
  <img id="img_120_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-8 1" width="419"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_34.jpg">
  <img id="img_121_banner1_031074005" class="bnfy-enter" alt="1786分享讚Butten-1100302-9 1" width="418"  src="/themes/Rent/img/1786/images/210315/1786_index_210315_35.jpg">
</div>

<!-- <img src="/themes/Rent/img/1786/1100120-3.JPG">
<img src="/themes/Rent/img/1786/1100120-4.JPG">
<img src="/themes/Rent/img/1786/1100120-5.JPG">
<img src="/themes/Rent/img/1786/1100120-6.JPG"> -->

<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php echo $_smarty_tpl->tpl_vars['js1786bg']->value;?>

<?php }
}
