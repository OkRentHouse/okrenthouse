<?php

class Bootstrap
{
	public $_msgs = array();
	public $_infos = array();
	public $_warnings = array();
	public $_errors = array();
	public $show_type = 'single';        //single:單一顯示、groups:群組顯示

	public function __construct()
	{

	}
	/**
	 * success    成功    綠色
	 * info    訊息    藍色
	 * warning    警示    黃色
	 * danger    危險    紅色
	 */

	/**
	 * @param $msg		顯示的訊息
	 * @param string $type	訊息類型
	 * 	success 	成功 	綠色
	 *	 info 	訊息	藍色
	 * 	warning 	警示	黃色
	 * 	danger 	危險	紅色
	 * @param null $strong	強調
	 * @return string		輸出HTML
	 */
	public function sow_msg($msg, $type = 'info', $strong = NULL)
	{
		if (!empty($show_type)) $class = '';
		if (!empty($strong)) $strong = '<strong>' . $strong . '</strong>';
		if (!empty($msg)) $html = '<div class="alert alert-' . $type . ' ' . $this->show_type . '" role="alert">' . $strong . ' ' . $msg . '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>';
		return $html;
	}

	/**
	 * @param $cookie_msg
	 * @param string $type
	 * @param null $strong
	 */
	public function cookie_msg($cookie_msg, $type = 'info', $strong = NULL)
	{
		$html = '';
		if (is_array($_COOKIE[$cookie_msg]) && count($_COOKIE[$cookie_msg]) > 0) {
			;
			foreach ($_COOKIE[$cookie_msg] as $k => $msg) {
				eval('\$this->_' . $cookie_msg . 's[] = \$msg');
			};
		} elseif (!empty($_COOKIE[$cookie_msg])) {
			eval('$this->_' . $cookie_msg . 's[] = $_COOKIE[\'' . $cookie_msg . '\'];');
		};
		setcookie($cookie_msg, '', time() - 360);
	}

	//成功訊息
	public function msg()
	{
		$this->_msgs = array_filter($this->_msgs);
		$this->cookie_msg('msg', 'success', $strong);
		if (count($this->_msgs) > 0) {
			if ($this->show_type == 'single') {
				foreach ($this->_msgs as $k => $msg) {
					$html .= $this->sow_msg($msg, 'success', $strong);
				};
			} else {
				$html = $this->sow_msg(implode('<br>', $this->_msgs), 'success', $strong);
			};
		};
		return $html;
	}

	//一般訊息
	public function info()
	{
		$this->_infos = array_filter($this->_infos);
		$html = $this->cookie_msg('info', 'info', $strong);
		if (count($this->_infos) > 0) {
			if ($this->show_type == 'single') {
				foreach ($this->_infos as $k => $info) {
					$html .= $this->sow_msg($info, 'info', $strong);
				};
			} else {
				$html = $this->sow_msg(implode('<br>', $this->_infos), 'info', $strong);
			};
		};
		return $html;
	}

	//警示訊息
	public function warning()
	{
		$this->_warnings = array_filter($this->_warnings);
		$html = $this->cookie_msg('warning', 'warning', $strong);
		if (count($this->_warnings) > 0) {
			if ($this->show_type == 'single') {
				foreach ($this->_warnings as $k => $warning) {
					$html .= $this->sow_msg($warning, 'warning', $strong);
				};
			} else {
				$html = $this->sow_msg(implode('<br>', $this->_warnings), 'warning', $strong);
			};
		};
		return $html;
	}

	//錯誤訊息

	public function error()
	{
		$this->_errors = array_filter($this->_errors);
		$html = $this->cookie_msg('error', 'danger', $strong);
		if (count($this->_errors) > 0) {
			if ($this->show_type == 'single') {
				foreach ($this->_errors as $k => $error) {
					$html .= $this->sow_msg($error, 'danger', $strong);
				};
			} else {
				$html = $this->sow_msg(implode('<br>', $this->_errors), 'danger', $strong);
			};
		};
		return $html;
	}

	/**
	 * @return string
	 */
	public function sow_all_msg()
	{
		$html = '';
		$html .= $this->msg();
		$html .= $this->info();
		$html .= $this->warning();
		$html .= $this->error();
		return $html;
	}
}