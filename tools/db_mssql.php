<?php
class db_mssql{
  //MSSQL資料庫相關設定
  public $pageNum,$nMax,$page_jmp_name,$pageNum_text,$nMax_text;
  public $conn,$query,$row,$num,$in_num,$up_num,$close,$total_page,$next_row,$pageNum_htm; //相關變數
  private $showmax=1,$conn_run=false;
  function db_mssql($sql,$nMax=0,$nMin='',$pageNum_text='p',$nMax_text='m',$conn=''){//(MSSQL語法、顯示幾筆資料、第幾頁)MSSQLdb($sql)顯示全部,MSSQLdb($sql,$nMax)只顯示0~$nMax筆資料,MSSQLdb($sql,$nMax,$nMin)顯示第$nMin~$nMax筆資料
		$this->pageNum_text=$pageNum_text;
		$this->nMax_text=$nMax_text;
		//連接資料庫
		if($conn==false){
			$this->conn($conn);
			$conn_run=true;
		};

		//如果只new 就不跑裡面
		if(isset($sql)){
			if(($nMax<0)&&(!is_int($nMax)))$nMax=1;//若不為數字,轉換成整數1
			$this->nMax=floor($nMax);//將數字(浮點數)調整為整數
			if((Defaultn($this->pageNum_text,1)!=null)&&(Defaultn($this->pageNum_text,1)>0)){
				$this->pageNum=floor(Defaultn($this->pageNum_text,1));//目前頁數
			}else{
				$this->pageNum=1;//目前頁數
			};
			//if(isset($_GTT[$this->pageNum_text]))$this->pageNum=$_GTT[$this->pageNum_text];//目前頁數
			if($this->pageNum=='')$this->pageNum=1;
			if($this->nMax!=0){//判別最多要顯示幾筆資料,$nMax=0就顯示全部
				$this->queryall=sqlsrv_query($this->conn,$sql, array(), array("scrollable" => 'keyset')) or die(sqlsrv_errors());//全部筆資料
				$max_pageNum=max(ceil($this->num()/$this->nMax)-1,0)+1;
				$this->pageNum=floor(min($this->pageNum,$max_pageNum));
				if(empty($nMin)){//判別是第幾頁
					$nMin=floor(($this->pageNum-1)*$this->nMax);//$nMin起始值,第一頁第一個為0
				};
				$sql=sprintf("%s LIMIT %d, %d",$sql,$nMin,$nMax);//從第$nMin筆資料往後顯示$nMax筆資料
			};
			//$this->query=MSSQL_query($sql,$this->conn) or die(MSSQL_error());
			$this->query = sqlsrv_query($this->conn,$sql, array(), array("scrollable" => 'keyset')) or die(sqlsrv_errors());
		};		
  }
	function conn($conn){
		//MSSQL資料庫相關設定
		switch($conn){//連線
			default:
			case ''://預設
				$MSSQL_HOST			= MSSQL_HOST;
				$MSSQL_PORT			= MSSQL_PORT;
				$MSSQL_USER			= MSSQL_USER;
				$MSSQL_PASS			= MSSQL_PASS;
				$MSSQL_E_USER_ERROR	= MSSQL_E_USER_ERROR;
				$MSSQL_DATABASE		= MSSQL_DATABASE;
				$MSSQL_CHARACTER_SET	= MSSQL_CHARACTER_SET;
			break;
		};
		$connectionInfo = array(
					'Database'		=> $MSSQL_DATABASE,
					'UID'			=> $MSSQL_USER,
					'PWD'			=> $MSSQL_PASS,
					'CharacterSet'	=> $MSSQL_CHARACTER_SET);
		$serverName = (!empty($MSSQL_PORT)) ? $MSSQL_HOST.','.$MSSQL_PORT : $MSSQL_HOST;
		$this->conn = sqlsrv_connect($serverName,$connectionInfo); 
	}
  function row(){ //顯示資料(用法:$a=$this->row; $a['資料欄位'];
    return sqlsrv_fetch_array($this->query, SQLSRV_FETCH_ASSOC);
  }
  
	function getRow(){
		//如果只new 就不跑裡面
		if(isset($sql)){
			if(($nMax<=0)&&(!is_int($nMax)))$nMax=1;//若不為數字,轉換成整數1
			$this->nMax=floor($nMax);//將數字(浮點數)調整為整數
			if((Defaultn($this->pageNum_text,1)!=null)&&(Defaultn($this->pageNum_text,1)>0)){
				$this->pageNum=floor(Defaultn($this->pageNum_text,1));//目前頁數
			}else{
				$this->pageNum=1;//目前頁數
			};
			//if(isset($_GTT[$this->pageNum_text]))$this->pageNum=$_GTT[$this->pageNum_text];//目前頁數
			if($this->pageNum=='')$this->pageNum=1;
			if($this->nMax!=0){//判別最多要顯示幾筆資料,$nMax=0就顯示全部
				$this->queryall=sqlsrv_query($this->conn,$sql, array(), array("scrollable" => 'keyset')) or die(sqlsrv_errors());//全部筆資料
				$max_pageNum=max(ceil($this->num()/$this->nMax)-1,0)+1;
				$this->pageNum=floor(min($this->pageNum,$max_pageNum));
				if(empty($nMin)){//判別是第幾頁
					$nMin=floor(($this->pageNum-1)*$this->nMax);//$nMin起始值,第一頁第一個為0
				};
				$sql=sprintf("%s LIMIT %d, %d",$sql,$nMin,$nMax);//從第$nMin筆資料往後顯示$nMax筆資料
			};
			//$this->query=MSSQL_query($sql,$this->conn) or die(MSSQL_error());
			$this->query = sqlsrv_query($this->conn,$sql, array(), array("scrollable" => 'keyset')) or die(sqlsrv_errors());
		};	
		if($this->num() > 0){
			if($this->num() == 1){
				return $this->row();
			}else{
				$arr_r = array();
				while($r = $this->next_row()){
					array_push($arr_r,$r);
				};
				return $arr_r;
			};
		}
		return array();
	}
	function insert_id(){
		$slq = 'select @@IDENTITY as insert_id';
		$mssql = db_mssql($sql);
		return $mssql->row();
	}
	function num(){ //查詢總數量
		if($this->nMax != 0){
			return max((int)sqlsrv_num_rows($this->queryall),db_mssql::in_num(),db_mssql::up_num());
		}else{
			return max((int)sqlsrv_num_rows($this->query),db_mssql::in_num(),db_mssql::up_num());
		};
	}
	function in_num(){ //查詢總數量
		$sql = 'select @@IDENTITY as insert_id';
		$mssql = db_mssql($sql);
		return (int)$mssql->row();
	}
	function up_num(){ //查詢總數量
		return (int)sqlsrv_rows_affected();
	}
	function close(){ //關閉MSSQL連線
		sqlsrv_close($this->conn);
		$conn_run=false;
	}
	function free(){ //釋放記憶
		if($this->nMax!=0){
			sqlsrv_free_stmt($this->queryall);
			sqlsrv_free_stmt($this->query);
		}else{
			sqlsrv_free_stmt($this->query);
		};
	} 
	function total_page(){ //查詢總頁數
		if($this->nMax!=0){
			return max(1, ceil($this->num()/$this->nMax));
		}else{
			return 1;
		}
	}
	function next_row(){ //下一筆資料(有下一筆回傳true,沒有下一筆回傳false)
		return sqlsrv_fetch_array($this->query);
	}
	function pageNum_htm(){
		if($this->num()>0){
			return $this->pageNum.'/'.$this->total_page();
		}else{
			return 0;
		};
	}
  //跳頁(未完成)
  //如:
	function jumppage($page_first='第一頁',$page_pre='上一頁',$page_next='下一頁',$page_end='最後一頁',$page_jmp_name=''){ //跳頁顯示$page_first='第一頁文字',$page_pre='上一頁文字',$page_next='下一頁文字',$page_end='最後一頁文字',$pageNum_text=跳頁的判斷"跳葉欄"文字,$page_jmp_name='跳至錨點'
		if($this->num() >0 ){
			$queryStringr="";
			if (!empty($_SERVER['QUERY_STRING'])){//判斷有沒有 $_$GET
				$params=explode("&", $_SERVER['QUERY_STRING']); //將每個$_GET拆開
				$newParams=array();
				foreach($params as $param){ //判斷$_GET裡有沒有不是$_GET['pageNum']的放進去
					if(@!stristr($param,$this->pageNum_text."=")&&@!stristr($param,$this->nMax_text."=")&&@!stristr($param,str_replace('&','',$page_jmp_name))) {
						array_push($newParams,$param);
					}
				}
				if(count($newParams)!=0){
					$queryStringr=htmlentities(implode("&", $newParams))."&";
				}
			}
			$currentPage=$_SERVER["PHP_SELF"];
			$currentPage=str_replace('/DH_index.php','',$currentPage);
			$pagesmin=$this->pageNum-3;
			if($this->pageNum<3)$pagesmin=1; //1-7
			$pagesmax=$pagesmin+5;
			if($pagesmax>=$this->total_page()){
				$pagesmax=$this->total_page();
				$pagesmin=$pagesmin-(($pagesmin+5)-$this->total_page());
			}
			$pagesmin=max($pagesmin,1);
			if($pagesmax-5>1)$pagesmin=$pagesmax-5;
			$page_html='';
			if($pagesmin!=$pagesmax){//只有一頁就不顯是跳頁選單
				if($pagesmax!=1){
					$page_html.='<ul>';
					$page_html.='<li><a alt="第一頁" title="第一頁" class="first" href="'.$currentPage.'?'.$queryStringr.$this->pageNum_text.'=1&'.$this->nMax_text.'='.$this->nMax.$page_jmp_name.'">'.$page_first.'</a></li>';//第一頁
					$page_html.='<li><a alt="上一頁" title="上一頁" class="pre" href="'.$currentPage.'?'.$queryStringr.$this->pageNum_text.'='.max(1, $this->pageNum-1).'&'.$this->nMax_text.'='.$this->nMax.$page_jmp_name.'">'.$page_pre.'</a></li>';//上一頁
					$page_html.='<div id="page_n">';
					for($i=$pagesmin;$i<=$pagesmax;$i++){
						$i_page='';
						if($i==$this->pageNum){
							$i_page=' active';
						};
						$page_html.='<li class="'.$i_page.'"><a alt="第'.$i.'頁" title="第'.$i.'頁" class="page_hover pageNum'.$i.$i_page.'" id="pageNum'.$i.'" href="'.$currentPage.'?'.$queryStringr.$this->pageNum_text.'='.$i.'&'.$this->nMax_text.'='.$this->nMax.$page_jmp_name.'">'.$i.'</a></li>'; //跳到第N頁,最多顯示七筆,1、2、3、本頁、5、6、7
					}
					$page_html.='</div>';
					$page_html.='<li><a alt="下一頁" title="下一頁" class="next" href="'.$currentPage.'?'.$queryStringr.$this->pageNum_text.'='.min($this->total_page(), $this->pageNum+1).'&'.$this->nMax_text.'='.$this->nMax.$page_jmp_name.'">'.$page_next.'</a></li>';//下一頁
					$page_html.='<li><a alt="最後一頁" title="最後一頁" class="end" href="'.$currentPage.'?'.$queryStringr.$this->pageNum_text.'='.$this->total_page().'&'.$this->nMax_text.'='.$this->nMax.$page_jmp_name.'">'.$page_end.'</a></li></ul><input name="'.$this->pageNum_text.'" id="'.$this->pageNum_text.'" type="hidden" value="'.$this->pageNum.'" />';//最後一頁
					$page_html.='</ul>';	
				}
			}
			$this->page_jmp_name=$page_jmp_name;
		};
		return $page_html;
	}
	function jamp_nMax($r1='',$r2=''){//一頁顯示幾筆資料輸入話框  $r1=前面文字,$r2=後面文字  $r1.<input>.$r2
		if($this->pageNum>200){$this->pageNum=200;};
		$html = $r1.'<input size="4" name="nMax" id="nMax" value="'.$this->nMax.'" type="text" />'.$r2;
 		$html .= '<script type="text/javascript">';
		//$html .= '$(document).ready(function() {';
		$html .=  "$('.pageNum".$this->pageNum."').addClass('thispage');";
		$html .=  "$('#nMax').blur(function(){";
		$html .=  "$('#storimg').submit();";
		$html .=  "});";
		//$html .=  "});";
		$html .=  '</script>';
		return $html;
	}
	function jamp_pageNum(){//跳到第幾頁輸入話框  $r1=前面文字,$r2=後面文字  $r1.<input>.$r2
		if($this->total_page()==0)$this->pageNum=0;
		$html = '<input size="4" name="'.$this->pageNum_text.'" class="'.$this->pageNum_text.'" value="'.$this->pageNum.'" type="text" />';
		$html .='<script type="text/javascript">';
		//$html.='$(document).ready(function() {'; 
		$html.="$('#".$this->pageNum_text."').blur(function(){";
		$html.="".$this->page_jmp_name."";
		$html.="$('#storimg').submit();";
		$html.="});";
		//$html.="});";
		$html.='</script>';
		return $html;
	}
}
//$nMax=Defaultn('nMax',20);//跳頁顯示判斷 預設20
//要新增一個目前頁面的跳頁按鈕顏色壯到 .thispage{}
/*$('.pageNum<?php echo $standard_stock->pageNum;?>').addClass('thispage');//跳頁變色*/

/*
//新增
  $sql=sprintf("INSERT INTO menu_class (帳號, 密碼) VALUES (%s, %s)",
                       GetSQL($_POST['帳號'], "text"),
                       GetSQL($_POST['密碼'], "text"));
	$a=db_mssql($sql);
  header('Location:index.php');//跳頁
	
//修改
  $updateSQL=sprintf("UPDATE menu_class SET MenuClassName=%s, MenuDIVClassName=%s WHERE ID=%s",
                       GetSQL($_POST['MenuClassName'], "text"),
                       GetSQL($_POST['MenuDIVClassName'], "text"),
                       GetSQL($_POST['ID'], "int"));
  $a=db_mssql($sql);
  header('Location:index.php');//跳頁
	
//刪除
  $deleteSQL=sprintf("DELETE FROM menu_class WHERE ID=%s",
                       GetSQL($_POST['deltmenuid'], "int"));
  $a=db_mssql($sql);
  header('Location:index.php');//跳頁
*/
//header("Location:index.php");
?>