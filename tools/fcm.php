<?php
class FCM{
	protected static $instance;

	/*
	 * 傳送的URL
	 */
	public $URL = 'https://fcm.googleapis.com/fcm/send';

	/*
	 * 授權 Key
	 */
	public $authorization = '';

	public $post = '';

	/*
	 *
	 */

	public $to			= '';		//接收的 RegID
	public $title		= '';
	public $body			= '';
	public $sound		= 'true';
	public $num			= null;
	public $data			= array();
	public $notification	= array();
	public $type 		= null;

	public function __construct(){
		$this->authorization = Configuration::get('FirebaseCloudMessagingKEY');
	}

	public function getPost(){

		if(count($this->to) > 0){
			$this->post['to'] = $this->to;
		};

		/*
		 * notification
		 * Android 會沒有震動跟數字
		 * IOS 必須使用 notification 才會推播、數字
		*/
		if(!isset($this->notification['sound'])) $this->notification['sound'] = $this->sound;
		$this->post['notification'] = $this->notification;
		if(!isset($this->notification['sound'])) {
			$this->notification['sound'] = $this->sound;
		}
		if(!isset($this->notification['badge']) && $this->num !== null) {
			$this->notification['badge'] = $this->num;
		}
		$this->post['notification'] = $this->notification;


		/*
		 * data
		 * Android 使用 Data 傳送
		 *
		*/
		if(!isset($this->data['sound'])) {
			$this->data['sound'] = $this->sound;
		}
		if(!isset($this->data['msg_num']) && $this->num !== null) {
			$this->data['msg_num'] = $this->num;
		}
		$this->post['data'] = $this->data;

		switch ($this->type){
			case 'Android':
				unset($this->post['notification']);
				break;
			case 'IOS':

				break;
			case 'WEB':

				break;
			default:
				unset($this->post['notification']);
				break;
		}

		return $this->post;
	}

	public function setType(){

	}

	public function getType(){

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new FCM();
		};
		return self::$instance;
	}

	public function sendMessage(){
		if(empty($this->to)) return false;
		$this->getPost();
		$json = new JSON();
		$headers = array('content-type:application/json',
			'Authorization:key='.$this->authorization);
		$ch = curl_init();
		$options = array(
			CURLOPT_URL			=> $this->URL,
			CURLOPT_HTTPHEADER		=> $headers,
			CURLOPT_SSL_VERIFYHOST	=> 0,
			CURLOPT_SSL_VERIFYPEER	=> false,
			CURLOPT_RETURNTRANSFER	=> true,	// CURLOPT_RETURNTRANSFER (true 會傳回網頁回應, false 時只回傳成功與否)
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> $json->encode($this->post),
		);
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	public function sendAllMessage(){
		$this->to = $this->authorization;
		return $this->sendMessage();
	}
}