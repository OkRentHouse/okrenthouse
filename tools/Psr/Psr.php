<?php
spl_autoload_register(function ($class_name) {
	$preg_match = preg_match('/^Psr\\\/', $class_name);
	if (1 === $preg_match) {
		$class_name = preg_replace('/\\\/', '/', $class_name);
		$class_name = preg_replace('/^Psr\\//', '', $class_name);
		require_once( $class_name. '.php');
	} else if (false === $preg_match) {
		assert(false, 'Error de preg_match().');
	}
});