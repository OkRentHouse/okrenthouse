<?php
//收到的GET或POST,沒收到的預設值
function defaultn($POST_GET, $num = '')
{
	if (empty($_POST[$POST_GET]) && empty($_GET[$POST_GET]) && is_null($_POST[$POST_GET]) && is_null($_GET[$POST_GET])) {
		return $num;//跳頁顯示判斷 預設
	} else {
		if (!empty($_POST[$POST_GET])) {
			return $_POST[$POST_GET];
		} else {
			return $_GET[$POST_GET];
		};
	};
}

//數字轉英文(0=>A、1=>B、26=>AA...以此類推)
function num2alpha($n)
{
	for ($r = ""; $n >= 0; $n = intval($n / 26) - 1)
		$r = chr($n % 26 + 0x41) . $r;
	return $r;
}

;
//英文轉數字(A=>0、B=>1、AA=>26...以此類推)
function alpha2num($a)
{
	$l = strlen($a);
	$n = 0;
	for ($i = 0; $i < $l; $i++)
		$n = $n * 26 + ord($a[$i]) - 0x40;
	return $n - 1;

}

;
//插入陣列
function array_insert(&$array, $position, $insert_array)
{
	$first_array = array_splice($array, 0, $position);
	$array       = array_merge($first_array, $insert_array, $array);
}

//中文新台幣
function number_format_tw($number, $str = '萬', $decimals = 0)
{
	switch ($str) {
		case '萬':
		break;
	}
}

//民國轉西元
function dateTo_ad($in_date, $in_txt = '.')
{
	if (!empty($in_date)) {
		if (empty($in_txt)) {
			$cyear = substr($in_date, 0, -4);
			$year  = ((int)$cyear) + 1911;
			$mon   = substr($in_date, -4, 2);
			$day   = substr($in_date, -2);
		} else {
			$arr  = explode($in_txt, $in_date);
			$year = ((int)$arr[0]) + 1911;
			$mon  = (int)$arr[1];
			$day  = (int)$arr[2];
		}
		$date = date("Y-m-d", mktime(0, 0, 0, $mon, $day, date($year)));
		return $date;
	} else {
		return '';
	}
}

//西元轉民國，後面參數為分隔符號自訂
function dateTo_c($in_date, $in_txt = '.')
{
	$ch_date    = explode('-', $in_date);
	$ch_date[0] = $ch_date[0] - 1911;
	$date       = '00.00.00';

	if ($in_txt == "") {
		$date = '000000';
		if ($ch_date[0] > 0)
			$date = $ch_date[0] . '' . $ch_date[1] . '' . $ch_date[2];
	} else {
		if ($ch_date[0] > 0)
			$date = $ch_date[0] . $in_txt . $ch_date[1] . $in_txt . $ch_date[2];
	}
	return $date;
}

//寫入快取
function set_cache_index($arr_file, $file)
{
	mkdir(CACHE_DIR, '0777', true);
	$fp = fopen(CACHE_DIR . DS . $file . '.php', 'w');            //新增、覆蓋檔案
	fwrite($fp, "<?php return array (\r\n");

	foreach ($arr_file as $i => $v) {
		fwrite($fp, "'" . $i . "' => '" . $v . "',\r\n");
	}

	fwrite($fp, ");");
	fclose($fp);
}

//讀取快取
function get_cache_index($file)
{
	if (is_file(CACHE_DIR . DS . $file . '.php'))
		$_indes = include_once(CACHE_DIR . DS . $file . '.php');
	return $_indes;
}

//跳頁
function gotourl($_url = 'index.php')
{
	$r_url = $_url;
	$page  = Defaultn('p');
	if (!empty($page)) {
		$_url = base64_decode($page);      //是否有上一頁,有就跳回上一頁
	} else {
		if ($r_url != 'index.php')
			$_url = $r_url;
	};
	ob_start();
	header("Location:" . $_url);
	ob_end_flush();
	exit;
}

function trimzero($str)
{
	list($int, $dec) = explode('.', (string)$str); //拆解字串,格式為: 整數.小數
	$dec = rtrim($dec, '0'); //將小數點 ,右邊的0去除
	if (empty($dec))
		return trim($int . $dec);
	return trim($int . '.' . $dec); //重組格式 ,並返回
}

//取得手機版本
function get_mobile()
{
	$http_user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
	$e               = strpos($http_user_agent, ')');
	return trim(substr(strrchr(substr($http_user_agent, 0, $e), '; '), 1));
}

//判別是不是手機版
function check_mobile()
{
	$regex_match = "/(nokia|iphone|android|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|";
	$regex_match .= "htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|";
	$regex_match .= "blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|";
	$regex_match .= "symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|";
	$regex_match .= "jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220";
	$regex_match .= ")/i";
	return preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT']));
}

//牆置刪除資料夾內所有檔案與資料夾
function del_dir($dir)
{
	if (!file_exists($dir))
		return true;
	if (!is_dir($dir) || is_link($dir))
		return unlink($dir);
	foreach (scandir($dir) as $item) {
		if ($item == '.' || $item == '..')
			continue;
		if (!del_dir($dir . "/" . $item)) {
			chmod($dir . "/" . $item, 0755);
			if (!del_dir($dir . "/" . $item))
				return false;
		};
	}
	return rmdir($dir);
}

function serialize_to_data($form)
{
	$data = [];
	foreach (explode('&', $form) as $value) {
		$value1 = explode('=', $value);
		if (strpos($value1[0], '%5B%5D') > 0) {
			$value1[0]          = str_replace('%5B%5D', '', $value1[0]);
			$data[$value1[0]][] = $value1[1];
		} else {
			$data[$value1[0]] = $value1[1];
		}
	}
	return $data;
}

//付制整個資料夾
function copy_dir($from_dir, $to_dir)
{
	if (!is_dir($from_dir)) {
		return false;
	};

	$from_files = scandir($from_dir);

	if (!file_exists($to_dir)) {
		mkdir($to_dir); // @mkdir($to_dir) << 跟這樣差異在哪!?
	};
	if (!empty($from_files)) {
		foreach ($from_files as $file) {
			if ($file == '.' || $file == '..') {
				continue;
			};

			if (is_dir($from_dir . DS . $file)) {
				copy_dir($from_dir . DS . $file, $to_dir . DS . $file);
			} else {
				copy($from_dir . DS . $file, $to_dir . DS . $file);
			};
		};
	};
	return true;
}

//
function v_to_psot($arr_post, $arr_v)
{
	foreach ($arr_post as $i => $v) {
		$_POST[${$v}] = (!isset($_POST[${$v}])) ? $_POST[${$v}] : $arr_v[$i];
	};
}

function mysql_to_psot($arr_v)
{
	foreach ($arr_v as $key => $v) {
		if (!isints($key)) {
			$_POST[$key] = (isset($_POST[$key])) ? $_POST[$key] : $v;
		};
	};
}

//權限比較 isAuthorized(頁面權限等級,使用者權限等級); //有權限就回傳true,是寫給permissions()用的
function isAuthorized($str_groups, $user_group)
{ //$strGroups可以為陣列如: 3,2,4,>6
	$str_groups = explode(",", $str_groups); //把頁面權限"字串"轉成陣列開始比對
	if (in_array($user_group, $str_groups))
		return true;  //陣列裡有沒有登入的權限?有傳回true
	return false;
}

//是否為正整數的字串或數字
function isint($v)
{
	return preg_match("/^\+?[1-9][0-9]*$/", $v);
}

//正負整數、浮點數、實數
function isints($v)
{
	return preg_match("/^\+?[0-9]*$/", $v);
}

function is_ints($v)
{
	return preg_match("/^[-+]?\d+(.\d+)?$/", $v);
}

function isdatetime($birthday)
{
	return preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}[ T]{1}[0-2]{1}[0-9]{1}:{1}[0-5]{1}[0-9]{1}[:]{0,1}[0-5]{0,1}[0-9]{0,1}$/', $birthday);
}

function isdate($day)
{
	return preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/', $day);
}

function istime($time)
{
	return preg_match('/^[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}[:]{0,1}[0-5]{0,1}[0-9]{0,1}$/', $time);
}

function isTrue($v)
{
	return preg_match('/^(0|1)$/', $v);
}

//檢查統一編號
function isUniform($address)
{ //20120817
	return preg_match('/^[0-9]{1,20}$/', $address);
}

//檢查密碼格式
function isPassword($password)
{
	return preg_match('/^(?!.*[^a-zA-Z0-9])(?=.*\d)(?=.*[a-zA-Z]).{8,20}$/', $password);
}

//檢查email格式
function isEmail($email)
{
	return preg_match('/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/', $email);
}

//檢查是否為module名稱
function isModuleName($module_name)
{
	return (is_string($module_name) && preg_match('/^[a-zA-Z0-9_-]+$/', $module_name));
}

//檢查是否為Hook名稱
function isHookName($hook_name)
{
	return (is_string($hook_name) && preg_match('/^[a-zA-Z0-9_-]+$/', $hook_name));
}

//檢查是否為中文URL
function isTwUrl($url)
{
	return preg_match('/^[\x{4e00}-\x{9fa5}a-zA-Z0-9_-]+$/u', $url);
}

//---------------4
function isfile($v)
{
	return preg_match('/[^\/:?"<>|]', $v);
}

//不能有符號//-------
function notsigned($v)
{
	return preg_match('/^[0-9a-zA-Z]+$/', $v);
}

function isIntArray($v)
{
	return preg_match('/^[0-9,]+$/', $v);
}

//檢查身份證號格式
function isIdentity($identity)
{
	return preg_match('/^[a-zA-Z](1|2)\d{8}$/', $identity);
}

//檢查聯絡電話格式
function isTel($tel)
{
	//preg_match('/^[0]{1}[0-9]{1,3}[-]{1}[0-9]{5,8}$/',$_POST['tel'])&&preg_match('/^[0-9-]{10,11}$/',$tel)
	$a = preg_match('/^[0-9()-]{5,12}$/', $tel);
	$b = preg_match('/^[09]{2}[0-9]{8}$/', $tel);
	if ($a || $b) {
		return true;
	} else {
		return false;
	};
}

//輸出檔案名稱(去除以下符號)
function file_name($name)
{
	return implode('', array_diff(str_split($name), ['\\', '/', ':', '*', '?', '\'', '"', '<', '>', '|']));
}

function restrictGoTo_v($restrictGoTo)
{
	if (strpos($restrictGoTo, '?')) {//輸入的GET是否有'?'
		$url_v = '&';
	} else {
		$url_v = '?';
	};
	return $restrictGoTo . $url_v;
}

//nl換行轉陣列
function nl2array($string)
{
	return preg_split('/\n|\r\n/', $string);
}

//刪除自首字尾
function del_prefix_and_suffix($str, $prefix = '', $suffix = '')
{
	$str = trim($str);
	if ($prefix == mb_substr($str, 0, mb_strlen($prefix), 'UTF-8')) {
		$str = mb_substr($str, mb_strlen($prefix), mb_strlen($str), 'UTF-8');
	}
	if ($suffix == mb_substr($str, -mb_strlen($suffix), mb_strlen($suffix), 'UTF-8')) {
		$str = mb_substr($str, 0, -mb_strlen($suffix), 'UTF-8');
	}
	return trim($str);
}

function lin_win_dir($dir, $type = '')
{
	if (OS == 'Windows' && $type == '') {//Windods 版
		return str_replace('/', '\\', $dir);//圖片儲存的目錄
	} else {//Linx 版
		return str_replace('\\', '/', $dir);//圖片儲存的目錄
	};
}

function isMove($file)
{//判斷影片格式 ( 回傳 0 or 1)
	$legal_values = ['video/flv', 'video/x-flv', 'video/mp4', 'video/quicktime', 'video/mpeg', 'video/avi', 'video/msvideo', 'video/x-msvideo', 'video/x-ms-asf', 'video/x-ms-wmv'];//影片格式
	return count(array_intersect([$_FILES[$file]['type']], $legal_values));
}

function extension($type)
{ //回傳檔案副檔名
	$legal_ext = ['video/flv' => 'flv', 'video/x-flv' => 'flv', 'video/mp4' => 'mp4', 'video/quicktime' => 'mov', 'video/mpeg' => 'mpeg', 'video/avi' => 'avi', 'video/msvideo' => 'msv', 'video/x-msvideo' => 'msv', 'video/x-ms-asf' => 'asf', 'video/x-ms-wmv' => 'wmv'];//影片格式
	return $legal_ext[$type];
}

function add_dir($dir, $t = 0777)
{ //新增資料夾
	$path = lin_win_dir($dir);
	if (is_dir($path)) {//已经是目录了就不用创建
		return true;
	}
	if (is_dir(dirname($path))) {//父目录已经存在，直接创建
		$type = mkdir($path, $t, true);//不存在就新增
		if ($type) {
			chmod($path, $t);
			return true;
		}
		return false;
	}
	add_dir(dirname($path));//从子目录往上创建
	return mkdir($path, $t, true);//因为有父目录，所以可以创建路径

}

function rrmdir($dir)
{            //刪除根資料夾
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir . "/" . $object) == "dir")
					rrmdir($dir . "/" . $object); else unlink($dir . "/" . $object);
			};
		};
		reset($objects);
		rmdir($dir);
	};
}

function add_file($filename, $str = '', $t = 'w', $t2 = 'a')
{ //新增檔案
	$filename = lin_win_dir($filename);
	if (is_file($filename)) {
		$file = fopen($filename, $t);
	} else {
		$file = fopen($filename, $t2);
	};
	fwrite($file, $str);

	fclose($file);
}

//數字下拉選單
function option_num($i, $j, $v)
{
	$html = '';
	if (is_int($i) && is_int($j)) {
		if ($j >= $i) {
			for ($x = $i; $x <= $j; $x++) {
				$selected = ($v == $x) ? ' selected="selected"' : '';
				$html     .= '<option value="' . $x . '"' . $selected . '>' . $x . '</option>';
			};
		} else {
			for ($x = $i; $x >= $j; $x--) {
				$selected = ($v == $x) ? ' selected="selected"' : '';
				$html     .= '<option value="' . $x . '"' . $selected . '>' . $x . '</option>';
			};
		};
	};
	return $html;
}

//顯示資料夾裡所有檔案
function get_dir_file($dir, $order = 'asc')
{
	$arr_file = [];
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				//只過讀取出php 的檔案
				if ($file != '.' && $file != '..') {
					$arr_file[] = $file;
				}
			}
			closedir($dh);
		}
	}

	switch ($order) {        //生序
		case 'asc':
		case 'ASC':
			sort($arr_file);
		break;
		case 'DES':        //降序
		case 'desc':
			rsort($arr_file);
		break;
	}
	return $arr_file;
}

//刪除資料夾底下某副檔名的所有檔案
function del_all_file($dir, $file_ex)
{
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				//只過讀取出php 的檔案
				if ($file != '.' && $file != '..') {
					if (preg_match("/." . $file_ex . "$/", $file)) {
						unlink($dir . $file);
					};
				};
			};
			closedir($dh);
		};
	};
}

//下拉選單
function option_arr()
{
	$html = '';
	switch (func_num_args()) {
		case 3:
			$arr_key = func_get_arg(0);
			$arr_v   = func_get_arg(1);
			$vv      = func_get_arg(2);
			foreach ($arr_v as $i => $v) {
				$selected = ($vv == $v) ? ' selected="selected"' : '';
				$html     .= '<option value="' . $v . '"' . $selected . '>' . $i . '</option>';
			};
		break;
		case 2:
			$arr_v = func_get_arg(0);
			$vv    = func_get_arg(1);
			foreach ($arr_v as $key => $v) {
				$selected = ($vv == $v) ? ' selected="selected"' : '';
				$html     .= '<option value="' . $v . '"' . $selected . '>' . $key . '</option>';
			};
		break;
		case 1:
			$arr_v = func_get_arg(0);
			foreach ($arr_v as $key => $v) {
				$html .= '<option value="' . $v . '">' . $v . '</option>';
			};
		break;
	};
	return $html;
}

//隨機產生大小寫英文+數字  (預設12碼)
function rand_code($num = 12, $type = false)
{
	$pass_phrase = chr(rand(97, 122)) . rand(0, 9); //imgext(因為密碼有1位英文+1位數字的限制,所以先給初值
	for ($i = 0; $i < $num - 2; $i++) {
		$ri = 3;
		if ($type)
			$ri = 4;
		$j = rand(1, $ri);        //亂數1~4
		switch ($j) {
			case 1:
				$pass_phrase .= chr(rand(97, 122)); //chr()將ASCII碼轉成單一個字  97~122為小寫
			break;
			case 2:
				$pass_phrase .= chr(rand(65, 90)); //65~90 為大寫
			break;
			case 3:
				$pass_phrase .= rand(0, 10); //0~9數字
			break;
			case 4:
				['!', '#', '$', '%', '&', '*', '+'];
				$pass_phrase .= chr(rand(33, 47)); // !"#$%&'()*+,-./ 符號
			break;
		};
	};
	return substr($pass_phrase, 0, $num);
}

//隨機產生16數字亂碼
function random16number()
{
	return substr(uniqid('', true), 15) . substr(microtime(), 2, 6) . rand(0, 9) . rand(0, 9);
}

function FTP($fileName, $ftp_path = './')
{
	$ftp_path = lin_win_dir($ftp_path);
	$ftp_connect = ftp_connect(FTP_IP, FTP_PO, FTP_TIME) or die ("FTP 連線失敗");
	ftp_login($ftp_connect, FTP_USER, FTP_PASS) or die ("FTP 登入失敗");
	ftp_chdir($ftp_connect, $ftp_path) or die ("FTP 目錄切換失敗");
	ftp_put($ftp_connect, $_FILES[$fileName]["name"], $_FILES[$fileName]["tmp_name"], FTP_ASCII) or die ("FTP 上傳失敗");
	if (ftp_put($conn, $_FILES[$fileName]['name'], $_FILES[$fileName]['tmp_name'], FTP_ASCII)) {
		return "影片上傳成功!\n";
	} else {
		return "無法同時上傳影片!\n";
	};
	ftp_close($conn);
}

function second_to_time($second, $type = 'time')
{//秒數轉時間
	switch ($type) {
		case 'time':
			$H      = floor($second / 3600);
			$second = $second - ($H * 3600);
			$M      = floor($second / 60);
			$S      = $second - ($M * 60);
			$v      = sprintf('%02d:%02d:%02d', $H, $M, $S);
		break;
		default:
			$v = false;
		break;
	};
	return $v;
}

function re_int($v)
{//字串裡只顯示數字
	return implode('', array_intersect(str_split($v), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]));
}

//陣列裡重複的key去掉只留一個
function arr_key_not_repeat($arr)
{
	array_flip($arr);
	array_unique($arr);
	array_flip($arr);
	return $arr;
}

//訊息
function msg_cookie($msg, $second = 10)
{//一般訊息存到cookie傳遞
	if ($second < 0) {
		setcookie('msg', null, time() - 3600);
	} else {
		if (!empty($msg))
			setcookie('msg', $msg, time() + $second);
	};
}

function error_cookie($error, $second = 10)
{//錯誤訊息存到cookie傳遞
	if ($second < 0) {
		setcookie('error', null, time() - 3600);
	} else {
		if (!empty($error))
			setcookie('error', $error, time() + $second);
	};
}

function today($type = '-')
{
	return date('Y' . $type . 'm' . $type . 'd', time());
}

function now_time()
{
	return date('H:i:s', time());
}

function now($t = null)
{
	if ($t === null) {
		return date('Y-m-d H:i:s', time());
	} else {
		return date('Y-m-d\TH:i:s', time());
	}
}

//星期
function get_weekday($datetime, $language = 'zh-TW')
{
	$l       = Language::getContext();
	$arr_w   = [
		$l->l('日'),
		$l->l('一'),
		$l->l('二'),
		$l->l('三'),
		$l->l('四'),
		$l->l('五'),
		$l->l('六'),
	];
	$set_w   = $l->l('星期');
	$weekday = date('w', strtotime($datetime));
	return $set_w . $arr_w[$weekday];
}

//範圍
function get_weekday_range($arr=[], $pipe='、', $to='~', $language = 'zh-TW'){
	$str = '';
	$count = count($arr);
	$l       = Language::getContext();
	$arr_w   = [
		$l->l('日'),
		$l->l('一'),
		$l->l('二'),
		$l->l('三'),
		$l->l('四'),
		$l->l('五'),
		$l->l('六'),
		$l->l('日'),
	];
	foreach($arr as $i => $v){
		if($v == 0){
			unset($arr[$i]);
			$arr[] = 7;
		}
	}
	if($count > 0){
		$min = min($arr);
		$max = max($arr);
		if((($max - $min + 1) == $count) && ($min != $max)){
			$str = $l->l('週').$arr_w[$min].$to.$arr_w[$max];
		}else{
			foreach($arr as $i => $v){
				$arr[$i] = $arr_w[$v];
			}
			$str = $l->l('每週').implode($pipe, $arr);
		}
	}
	return $str;
}

function diffDateTime($datetime1, $datetime2)
{
	$preg = "/^\d{4}\-\d{2}\-\d{2}(\s)+\d{2}:\d{2}:\d{2}$/";        //验证时间格式是否为YYYY-MM-DD HH:ii:ss
	if (!preg_match($preg, $datetime1) || !preg_match($preg, $datetime2))
		exit('Format Error');
	if (strtotime($datetime1) > strtotime($datetime2)) {
		$tmp       = $datetime2;
		$datetime2 = $datetime1;
		$datetime1 = $tmp;
	}
	list($date1, $time1) = explode(' ', $datetime1);
	list($date2, $time2) = explode(' ', $datetime2);
	list($year1, $month1, $day1) = explode('-', $date1);
	list($year2, $month2, $day2) = explode('-', $date2);
	if (!checkdate($month1, $day1, $year1) || !checkdate($month2, $day2, $year2))
		exit('Invalid Date');
	$diff_year  = $year2 - $year1;
	$diff_month = $month2 - $month1;
	$diff_day   = $day2 - $day1;
	if ($diff_day < 0) {
		$diff_day += (int)date('t', strtotime("-1 month $date2"));
		$diff_month--;
	}
	if ($diff_month < 0) {
		$diff_month += 12;
		$diff_year--;
	}
	//计算时间差
	$timediff = abs(strtotime($time2) - strtotime($time1));
	//计算小时数
	$diff_hour = intval($timediff / 3600);
	//计算分钟数
	$remain      = $timediff % 3600;
	$diff_minute = intval($remain / 60);
	//计算秒数
	$diff_second = $remain % 60;
	return [
		'year'   => $diff_year,
		'month'  => $diff_month,
		'day'    => $diff_day,
		'hour'   => $diff_hour,
		'minute' => $diff_minute,
		'second' => $diff_second,
	];
}

function ip()
{
	if (empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$record_ip = $_SERVER['REMOTE_ADDR'];
	} else {
		$record_ip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
		$record_ip = $record_ip[0];
	};
	return $record_ip;
}

//下拉選單
function select_selected($v, $v2)
{
	if ($v == $v2)
		return ' selected="selected"';
}

if (!function_exists('mime_content_type')) {

	function mime_content_type($filename)
	{

		$mime_types = [

			'txt'  => 'text/plain',
			'htm'  => 'text/html',
			'html' => 'text/html',
			'php'  => 'text/html',
			'css'  => 'text/css',
			'js'   => 'application/javascript',
			'json' => 'application/json',
			'xml'  => 'application/xml',
			'swf'  => 'application/x-shockwave-flash',
			'flv'  => 'video/x-flv',

			// images
			'png'  => 'image/png',
			'jpe'  => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'jpg'  => 'image/jpeg',
			'gif'  => 'image/gif',
			'bmp'  => 'image/bmp',
			'ico'  => 'image/vnd.microsoft.icon',
			'tiff' => 'image/tiff',
			'tif'  => 'image/tiff',
			'svg'  => 'image/svg+xml',
			'svgz' => 'image/svg+xml',

			// archives
			'zip'  => 'application/zip',
			'rar'  => 'application/x-rar-compressed',
			'exe'  => 'application/x-msdownload',
			'msi'  => 'application/x-msdownload',
			'cab'  => 'application/vnd.ms-cab-compressed',

			// audio/video
			'mp3'  => 'audio/mpeg',
			'qt'   => 'video/quicktime',
			'mov'  => 'video/quicktime',

			// adobe
			'pdf'  => 'application/pdf',
			'psd'  => 'image/vnd.adobe.photoshop',
			'ai'   => 'application/postscript',
			'eps'  => 'application/postscript',
			'ps'   => 'application/postscript',

			// ms office
			'doc'  => 'application/msword',
			'rtf'  => 'application/rtf',
			'xls'  => 'application/vnd.ms-excel',
			'ppt'  => 'application/vnd.ms-powerpoint',

			// open office
			'odt'  => 'application/vnd.oasis.opendocument.text',
			'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
		];

		$ext = strtolower(array_pop(explode('.', $filename)));
		if (array_key_exists($ext, $mime_types)) {
			return $mime_types[$ext];
		} elseif (function_exists('finfo_open')) {
			$finfo    = finfo_open(FILEINFO_MIME);
			$mimetype = finfo_file($finfo, $filename);
			finfo_close($finfo);
			return $mimetype;
		} else {
			return 'application/octet-stream';
		}
	}
}
//檔案下載
/**
 *  $file
 *  $file, $file_name
 *  $file, $download (bool)
 *  $file, $file_name, $download (bool)
 */
function print_file()
{
	$file     = '404.jpg';
	$filename = substr(func_get_arg(0), strrpos(func_get_arg(0), DS) + 1);
	$download = true;
	$file     = func_get_arg(0);
	switch (func_num_args()) {
		case 2:
			if (func_get_arg(1) === true || func_get_arg(1) === false) {
				$download = func_get_arg(1);
			} else {
				$filename = func_get_arg(1);
			};
		break;
		case 3:
			$filename = func_get_arg(1);
			$download = func_get_arg(2);
		break;
	};
	$type = mime_content_type($file);
	if ($download) {        //告訴瀏覽器 為下載
		header('Content-type:application/force-download');
		header('Content-Disposition:attachment;filename=' . $filename); //檔名
	}
	header('Content-Transfer-Encoding: Binary'); //編碼方式
	if (!empty($type)) {
		header('Content-Type: ' . $type);
	}
	@readfile($file);
	exit;
}

function br2nl($string)
{
	return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}

//圖片
function print_img($img = '404.jpg', $type = 'image/jpg')
{
	if (file_exists($img)) {
		header("Content-Type: $type");
		$file         = fopen($img, 'rb');//開啟檔案
		$fileContents = fread($file, filesize($img));//讀入檔案資料
		fclose($file);//關閉檔案
		$fileContents = base64_encode($fileContents);//檔案資料編碼
		echo base64_decode($fileContents);
	} else {
		gotourl('/找不到圖片');
	};
	exit;
}

//字串切割成資料夾路徑
function str_dir_change($str)
{
	$str_dir = '';
	$str_src = '';
	$strlen  = strlen($str);
	for ($i = 0; $i < $strlen; $i++) {
		$str_i   = substr($str, $i, 1);
		$str_dir .= $str_i . DS;
		$str_src .= $str_i . '/';
	};
	return [$str_dir, $str_src];      //檔案資料夾路徑,網址路徑
}

//取副檔名
function ext($filename)
{
	return substr(strrchr($filename, '.'), 1);
}

//file欄位顯是類型
function est_to_type($ext)
{
	switch ($ext) {
		case 'jpg':
		case 'jpeg':
		case 'png':
		case 'gif':
			return 'image';
		break;
		case 'doc':
		case 'docx':
		case 'xls':
		case 'xlsx':
		case 'ppt':
		case 'pptx':
			return 'office';
		break;
		case 'zip':
		case 'rar':
		case 'tar':
		case 'gzip':
		case 'gz':
		case '7z':
			return 'image';
		break;
		case 'htm':
		case 'html':
			return 'html';
		break;
		case 'txt':
		case 'ini':
		case 'csv':
		case 'java':
		case 'php':
		case 'js':
		case 'css':
			return 'text';
		break;
		case 'avi':
		case 'mpg':
		case 'mkv':
		case 'mov':
		case 'mp4':
		case '3gp':
		case 'webm':
		case 'wmv':
		case 'mp3':
		case 'wav':
			return 'video';
		break;
		case 'pdf':
			return 'pdf';
		break;
		case 'ai':
		case 'eps':
		case 'tif':
			return 'gdocs';
		break;

	}
	return 'image';
}

//上傳圖片
function updateimg($uploadedfile, $filename, $type, $newwidth = '', $newheight = '', $fixed = false, $compression = 100, $uploadX = 0, $uploadY = 0, $fileX = 0, $fileY = 0)
{//上傳的圖檔 圖片儲存路徑/檔名 修改後寬度 修改後高度 圖片壓縮的品質
	$uploadedfile = lin_win_dir($uploadedfile);
	$filename     = lin_win_dir($filename);
	list($width, $height) = getimagesize($uploadedfile);//讀取圖片的長寬
	for ($i = 0; $i < 1; $i++) {
		if ($newwidth == '' && $newheight == '') {//長寬都有設定
			$newwidth  = $width;
			$newheight = $height;
			break;
		};
		if ($newwidth != '' && $newheight == '') {//只設定寬度
			if ($width > $newwidth) {
				$newheight = $height * ($newwidth / $width);//高度依比例縮放
			} else {
				$newwidth  = $width;
				$newheight = $height;
			};
			break;
		};
		if ($newwidth == '' && $newheight != '') {//只設定高度
			if ($height > $newheight) {
				$newwidth = $width * ($newheight / $height);//寬度一比例縮放
			} else {
				$newwidth  = $width;
				$newheight = $height;
			};
			break;
		};
		if ($newwidth != '' && $newheight != '') {//長寬都有設定
			if (!$fixed) {
				if ($width <= $height) {//以寬度最小的做基準
					$Xheight = $height * ($newwidth / $width);//計算比例新高度
					if ($Xheight > $newheight) {//查看高度是否超過
						$newwidth = $width * ($newheight / $height);//超過以高度為基準，寬度依比例縮放
					} else {
						$newheight = $Xheight;//沒有超過高度一比例縮放
					};
				};
				if ($height < $width) {//以高度最小的做基準
					$Xwidth = $width * ($newheight / $height);//計算比例新寬度
					if ($Xwidth > $newwidth) {//查看寬度度是否超過
						$newheight = $height * ($newwidth / $width);//超過以寬度為基準，高度依比例縮放
					} else {
						$newheight = $Xwidth;//沒有超過寬度一比例縮放
					};
				};
				break;
			};
		};
	};
	$tmp = imagecreatetruecolor($newwidth, $newheight);//新圖規格
	imagealphablending($tmp, false);//意思是不合併顏色,直接顏色替換,包括透明色;
	imagesavealpha($tmp, true); //意思是不要丟了圖像的透明色;
	$type_name = strrchr($filename, '.');
	switch ($type) {
		case "image/jpeg":
		case "image/pjpeg":
			$type_name = '.jpg';
			if (strlen(strrchr($filename, '.')) <= 5) {
				$type_name = strrchr($filename, '.');
				$filename  = substr($filename, 0, strrpos($filename, $type_name));
			};//如果有自訂副檔名就以副檔名存檔
			$src = imagecreatefromjpeg($uploadedfile);//讀取圖片
			imagecopyresampled($tmp, $src, $fileX, $fileY, $uploadX, $uploadY, $newwidth, $newheight, $width, $height);//產生圖片
			imagejpeg($tmp, $filename . $type_name, $compression);//圖片壓縮的品質，並產生圖片 0最差、100最
		break;
		case "image/gif":
			$type_name = '.gif';
			if (strlen(strrchr($filename, '.')) <= 5) {
				$type_name = strrchr($filename, '.');
				$filename  = substr($filename, 0, strrpos($filename, $type_name));
			};//如果有自訂副檔名就以副檔名存檔
			$src = imagecreatefromgif($uploadedfile);//讀取圖片
			imagecopyresampled($tmp, $src, $fileX, $fileY, $uploadX, $uploadY, $newwidth, $newheight, $width, $height);//產生圖片
			imagegif($tmp, $filename . $type_name, $compression);//圖片壓縮的品質，並產生圖片 0最差、100最
		break;
		case "image/png":
		case "image/x-png":
			$type_name = '.png';
			if (strlen(strrchr($filename, '.')) <= 5) {
				$type_name = strrchr($filename, '.');
				$filename  = substr($filename, 0, strrpos($filename, $type_name));
			};//如果有自訂副檔名就以副檔名存
			$src = imagecreatefrompng($uploadedfile);//讀取圖片
			imagecopyresampled($tmp, $src, $fileX, $fileY, $uploadX, $uploadY, $newwidth, $newheight, $width, $height);//產生圖片
			imagepng($tmp, $filename . $type_name);//圖片壓縮的品質，並產生圖片 0最差、100最
		break;
		/*
		case "bmp":
		  $src = imagecreatefromwbmp($uploadedfile);//讀取圖片
			imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);//產生圖片
			imagewbmp($tmp,$filename);//圖片壓縮的品質，並產生圖片 0最差、100最
		break;
		*/
	};
	imagedestroy($src);//關閉圖片物件 
	imagedestroy($tmp);
}

function is_img($type)
{
	$s = false;
	switch ($type) {
		case "image/jpeg":
		case "image/pjpeg":
		case "image/gif":
		case "image/png":
		case "image/x-png":
		case "bmp":
			$s = true;
		break;
	};
	return $s;
}

function javascript_msg($msg = '', $error = '')
{
	if (empty($msg))
		$msg = $_SESSION['msg'];
	if (empty($error))
		$error = $_SESSION['error'];
	if (!empty($error))
		$msg = $error;
	$htm = '';
	if (!empty($msg)) {
		$htm .= '<script type="text/javascript">';
		$htm .= '$(document).ready(function(e) {';
		$htm .= '_msg="' . $msg . '";';
		$htm .= 'open_msg(_msg);';
		$htm .= '};);';
		$htm .= '</script>';
	};
	unset($_SESSION['msg']);
	unset($_SESSION['error']);
	return $htm;
}

function javascript_msg2($_edit_f_html)
{
	$htm = '<script type="text/javascript">
		var _msg_border_click = 0;
		$(document).ready(function(e) {
			$("#msg_border").click(function(){
				if(_msg_border_click == 0){
					db_msg_border.stop(true).fadeIn();
					db_msg.stop(false,true).fadeIn();
					open_msg(' . $_edit_f_html . ',false);
					_msg_border_click++;
				};
			};);
		};);
		</script>';
	return $htm;
}

//隱藏中間字
function hide_str($in_str, $before = 0, $after = 0, $str = '*')
{
	$strlen = mb_strlen($in_str, 'UTF-8');
	if ($strlen <= ($before + $after)) {
		return $in_str;
	}
	$str1 = '';
	$str2 = '';
	$str3 = '';
	for ($i = 0; $i < $strlen - $before - $after; $i++) {
		$str2 .= $str;
	};

	if ($before > 0)
		$str1 = mb_substr($in_str, 0, $before, 'utf-8');
	if ($after > 0)
		$str3 = mb_substr($in_str, -$after, $strlen);
	return $str1 . $str2 . $str3;
}

//寄信專用
//send_mail(郵件標題郵件內容,寄件者姓名,寄件者信箱,收件者郵件,附加檔案路徑,附加檔案名稱,寄件成功與否回應(true,flase))
function send_mail($mail_title, $body, $sender_name, $sender_email, $AddAddress, $file = '', $filenem = '', $send = true)
{
	global $arr_mail;
	//匯入PHPMailer類別
	include_once('phpmailer/class.phpmailer.php');  //一定要用絕對路徑,否則其他資料夾檔案引入會出問題
	@$mail = new PHPMailer();                  //建立新物件
	@$mail->IsSMTP();                        //設定使用SMTP方式寄信
	@$mail->SMTPAuth = true;                  //設定SMTP需要驗證
	@$mail->SMTPSecure = 'ssl';      //Gmail的SMTP主機需要使用SSL連線
	@$mail->Host = 'smtp.gmail.com';            //Gamil的SMTP主機
	@$mail->Port = 465;            //Gamil的SMTP主機的埠號(Gmail為465)。
	@$mail->CharSet = $arr_mail['CharSet'];            //郵件編碼
	@$mail->Username = $arr_mail['smtp_username'];      //Gamil帳號
	@$mail->Password = $arr_mail['smtp_password'];      //Gmail密碼
	@$mail->From = $sender_email;                  //寄件者信箱
	@$mail->FromName = $sender_name;            //寄件者姓名
	@$mail->IsHTML(true);                        //郵件內容為html ( true || false)
	@$mail->Subject = $mail_title;            //郵件標題
	@$mail->Body = $body;                        //郵件內容
	/*
	if($file!=NULL){
		if($filenem!=NULL){
			@$mail->AddAttachment($file,$filenem);
		}else{
			@$mail->AddAttachment($file);
		};
	};
	*/
	@$mail->AddAddress($AddAddress); //收件者郵件及名稱
	//傳送附檔的另一種格式，可替附檔重新命名
	//$mail->AddAttachment("upload/temp/filename.zip", "newname.zip");
	//print $mail->ErrorInfo();//錯誤訊息
	/*如果使用phpMailer发送html网页只需添加代码：
  $body = file_get_contents('tpl.html'); //获取html网页内容
  $mail->MsgHTML(eregi_replace("[\]",'',$body));
注：使用MsgHTML()方法时发送的邮件内容不仅仅是html代码,而且可以将html代码中的图片等内容作为附件嵌入到页面中一起发送，这在发送图文并茂的信息时比较常用。并且使用该方法时无需对Body属性赋值也无需使用IsHTML()方法（即使使用也没有效果）。
*/
	if ($send) {
		if (@$mail->Send()) {
			return true;
		} else {
			return false;
		};
	} else {
		return true;
	};
	/*
		$mail->Host = "mail.yourdomain.com"; // SMTP server
		$mail->SMTPDebug = 2; // enables SMTP debug information (for testing)
		$mail->SMTPAuth = true; // enable SMTP authentication
		$mail->SMTPSecure = "ssl"; // sets the prefix to the servier
		$mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
		$mail->Port = 465; // set the SMTP port for the GMAIL server
		$mail->Username = "yourusername@gmail.com"; // GMAIL username
		$mail->Password = "yourpassword"; // GMAIL password
		$mail->AddReplyTo('name@yourdomain.com', 'First Last');
		$mail->AddAddress('whoto@otherdomain.com', 'John Doe');
		$mail->SetFrom('name@yourdomain.com', 'First Last');
		$mail->AddReplyTo('name@yourdomain.com', 'First Last');
		$mail->Subject = 'PHPMailer Test Subject via mail(), advanced';
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
		$mail->MsgHTML(file_get_contents('contents.html'));
		$mail->AddAttachment('images/phpmailer.gif'); // attachment
		$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
		$mail->Send();
	*/
}

function chackauthnum()
{ //圖形驗證碼比對
	if ($_SESSION['verification2'] == $_POST['verification']) { //驗證暫存
		$sss = true;
	} else {
		$sss = false;
	};
	unset($_SESSION['verification2']);//將圖形驗證碼的session消除
	return $sss;
}

function debug()
{
	$backtrace = debug_backtrace();
	array_shift($backtrace);
	for ($i = 0; $i < $start; ++$i) {
		array_shift($backtrace);
	};

	echo '
	<script type="text/javascript">';
	$i = 0;
	foreach ($backtrace as $id => $trace) {
		if ((int)$limit && (++$i > $limit)) {
			break;
		};
		$relative_file = (isset($trace['file'])) ? 'in /' . ltrim(str_replace([_PS_ROOT_DIR_, '\\'], ['', '/'], $trace['file']), '/') : '';
		$current_line  = (isset($trace['line'])) ? ':' . $trace['line'] : '';

		echo 'console.log("' . ((isset($trace['class'])) ? $trace['class'] : '') . ((isset($trace['type'])) ? $trace['type'] : '') . $trace['function'] . ' ' . $relative_file . $current_line . '")';
	};
	echo '</script>';

}

function ClassAutoload($className) {
	$arr_class_dir = [];
	$dir = WEB_DIR.DS.SYSTEM_FOLDER.DS.'classes';
	if(is_dir($dir)){
		$arr_class_dir[] = $dir;
		if ($dh = opendir($dir)) {
			while(($file = readdir($dh)) !== false) {
				//只讀過取出php 的檔案
				if ($file != '.' && $file != '..'){
					if(is_dir($dir.DS.$file)){	//是資料夾
						$arr_class_dir[] = $dir.DS.$file;
					}
				}
			}
		}
	}

	$arr_class_dir[] = CLASS_DIR;
	if ($dh = opendir(CLASS_DIR)) {
		while(($file = readdir($dh)) !== false) {
			//只讀過取出php 的檔案
			if ($file != '.' && $file != '..'){
				if(is_dir(CLASS_DIR.DS.$file)){	//是資料夾
					$arr_class_dir[] = CLASS_DIR.DS.$file;
				}
			}
		}
	}

	foreach($arr_class_dir as $i => $class_dir){
		$filename = $class_dir . DS .$className . ".php";
		if (is_readable($filename)) {
			require $filename;
		}
	}
}

function d()
{
}
/*
function count_log(){
	$logfile = "counter.log";
 if (! file_exists($logfile))
   { $file = fopen($logfile,"w");
     fwrite($file, "1162");
     fclose($file);
     chmod($logfile, 0600);
   };

 $ip   = $_SERVER['REMOTE_ADDR'];
 $sec  = microtime(true);

 $file = fopen($logfile,"r");
 $log  = fread($file, 100);
 fclose($file);

 list($oldip, $oldsec, $count) = explode(" ", $log);

 if ( $oldip!=$ip or $sec>$oldsec+900 )
  {  $count ++;
     $file = fopen($logfile,"w");
     fwrite($file, "$ip $sec $count");
     fclose($file);
  };
 return $count;
};
*/