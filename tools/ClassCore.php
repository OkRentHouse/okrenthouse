<?php
/**
 * Created by PhpStorm.
 * User: Allen
 * Date: 2019/4/28
 * Time: 下午 06:18
 */

class ClassCore
{
	protected static $instance;
	public $_errors = array();
	public $_msgs = array();
	public $msg = '';

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new ClassCore();
		};
		return self::$instance;
	}

	public function l($str){
		return $str;
	}
}