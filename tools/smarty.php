<?php
include_once(dirname(__FILE__).DS.'smarty-3.1.28'.DS.'libs'.DS.'Smarty.class.php');

global $Smarty;
$Smarty = new Smarty();
function smartyHook($params, &$smarty)
{
    if (!empty($params['h'])) {
        $id_module = null;
        $hook_params = $params;
        $hook_params['smarty'] = $smarty;
        if (!empty($params['mod'])) {
        	$module = Module::getContextByName($params['mod']);

            if ($module && $module->id) {
                $id_module = $module->id;
            }

            unset($hook_params['mod']);
        }
        unset($hook_params['h']);
        return Hook::exec($params['h'], $hook_params, $id_module);
    }
}

function l($arr){
	if(!empty($arr['module'])){
		$str = Translate::getModuleTranslation($arr['mod'],$arr['s'],$arr['source']);
	}else if(!empty($arr['class'])){
		$str = Translate::getAdminTranslation($arr['class'],$arr['s']);
	}else{
		$str = $arr['s'];
	}
	return $str;
}

function smartyConfiguration($params){
	$str = $params['name'];
	$id_lang = $params['lang'];
	$db_prefix_ = $params['db_prefix'];
	return Configuration::get($str, $id_lang, $db_prefix_);
}
//Smarty 3.0以上設定方法
//$Smarty->caching = _SMARTY_CACHING;
$Smarty->setTemplateDir(_SNARTY_TEMPLATE_DIR);	//樣版路徑 (根目錄)
$Smarty->setCacheDir(_SMARTY_CACHE_DIR);		//快取路徑
$Smarty->setCompileDir(_SMARTY_COMPILE_DIR);	//編譯路徑
$Smarty->setConfigDir(_SMARTY_CONFIG_DIR); 	//配置檔案路徑


$Smarty->setLeftDelimiter('{');
$Smarty->setRightDelimiter('}');
$Smarty->registerPlugin('function','l','l');
$Smarty->registerPlugin('function', 'hook', 'smartyHook');
$Smarty->registerPlugin('function', 'Configuration', 'smartyConfiguration');

/*
smartyRegisterFunction($smarty, 'modifier', 'truncate', 'smarty_modifier_truncate');
smartyRegisterFunction($smarty, 'modifier', 'secureReferrer', array('Tools', 'secureReferrer'));

smartyRegisterFunction($smarty, 'function', 't', 'smartyTruncate'); // unused
smartyRegisterFunction($smarty, 'function', 'm', 'smartyMaxWords'); // unused
smartyRegisterFunction($smarty, 'function', 'p', 'smartyShowObject'); // Debug only
smartyRegisterFunction($smarty, 'function', 'd', 'smartyDieObject'); // Debug only
smartyRegisterFunction($smarty, 'function', 'l', 'smartyTranslate', false);
smartyRegisterFunction($smarty, 'function', 'toolsConvertPrice', 'toolsConvertPrice');
smartyRegisterFunction($smarty, 'modifier', 'json_encode', array('Tools', 'jsonEncode'));
smartyRegisterFunction($smarty, 'modifier', 'json_decode', array('Tools', 'jsonDecode'));
smartyRegisterFunction($smarty, 'block', 'addJsDefL', array('Media', 'addJsDefL'));

function smartyDieObject($params, &$smarty)
{
    return Tools::d($params['var']);
}

function smartyShowObject($params, &$smarty)
{
    return Tools::p($params['var']);
}

function smartyMaxWords($params, &$smarty)
{
    Tools::displayAsDeprecated();
    $params['s'] = str_replace('...', ' ...', html_entity_decode($params['s'], ENT_QUOTES, 'UTF-8'));
    $words = explode(' ', $params['s']);

    foreach ($words as &$word) {
        if (Tools::strlen($word) > $params['n']) {
            $word = Tools::substr(trim(chunk_split($word, $params['n']-1, '- ')), 0, -1);
        }
    }

    return implode(' ',  Tools::htmlentitiesUTF8($words));
}

function smartyTruncate($params, &$smarty)
{
    Tools::displayAsDeprecated();
    $text = isset($params['strip']) ? strip_tags($params['text']) : $params['text'];
    $length = $params['length'];
    $sep = isset($params['sep']) ? $params['sep'] : '...';

    if (Tools::strlen($text) > $length + Tools::strlen($sep)) {
        $text = Tools::substr($text, 0, $length).$sep;
    }

    return (isset($params['encode']) ? Tools::htmlentitiesUTF8($text, ENT_NOQUOTES) : $text);
}

function smarty_modifier_truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false, $charset = 'UTF-8')
{
    if (!$length) {
        return '';
    }

    $string = trim($string);

    if (Tools::strlen($string) > $length) {
        $length -= min($length, Tools::strlen($etc));
        if (!$break_words && !$middle) {
            $string = preg_replace('/\s+?(\S+)?$/u', '', Tools::substr($string, 0, $length+1, $charset));
        }
        return !$middle ? Tools::substr($string, 0, $length, $charset).$etc : Tools::substr($string, 0, $length/2, $charset).$etc.Tools::substr($string, -$length/2, $length, $charset);
    } else {
        return $string;
    }
}

function smarty_modifier_htmlentitiesUTF8($string)
{
    return Tools::htmlentitiesUTF8($string);
}
function smartyMinifyHTML($tpl_output, &$smarty)
{
    $context = Context::getContext();
    if (isset($context->controller) && in_array($context->controller->php_self, array('pdf-invoice', 'pdf-order-return', 'pdf-order-slip'))) {
        return $tpl_output;
    }
    $tpl_output = Media::minifyHTML($tpl_output);
    return $tpl_output;
}

function smartyPackJSinHTML($tpl_output, &$smarty)
{
    $context = Context::getContext();
    if (isset($context->controller) && in_array($context->controller->php_self, array('pdf-invoice', 'pdf-order-return', 'pdf-order-slip'))) {
        return $tpl_output;
    }
    $tpl_output = Media::packJSinHTML($tpl_output);
    return $tpl_output;
}

function smartyRegisterFunction($smarty, $type, $function, $params, $lazy = true)
{
    if (!in_array($type, array('function', 'modifier', 'block'))) {
        return false;
    }

    // lazy is better if the function is not called on every page
    if ($lazy) {
        $lazy_register = SmartyLazyRegister::getContext();
        $lazy_register->register($params);

        if (is_array($params)) {
            $params = $params[1];
        }

        // SmartyLazyRegister allows to only load external class when they are needed
        $smarty->registerPlugin($type, $function, array($lazy_register, $params));
    } else {
        $smarty->registerPlugin($type, $function, $params);
    }
}

function smartyHook($params, &$smarty)
{
    if (!empty($params['h'])) {
        $id_module = null;
        $hook_params = $params;
        $hook_params['smarty'] = $smarty;
        if (!empty($params['mod'])) {
            $module = Module::getContextByName($params['mod']);
            if ($module && $module->id) {
                $id_module = $module->id;
            }
            unset($hook_params['mod']);
        }
        unset($hook_params['h']);
        return Hook::exec($params['h'], $hook_params, $id_module);
    }
}

function smartyCleanHtml($data)
{
    // Prevent xss injection.
    if (Validate::isCleanHtml($data)) {
        return $data;
    }
}
*/