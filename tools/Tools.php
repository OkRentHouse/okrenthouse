<?php
class Tools
{
	public static function passwdGen($length = 8, $flag = 'ALPHANUMERIC')
	{
		$length = (int)$length;

		if ($length <= 0) {
			return false;
		}

		switch ($flag) {
			case 'NUMERIC':
				$str = '0123456789';
				break;
			case 'NO_NUMERIC':
				$str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'RANDOM':
				$num_bytes = ceil($length * 0.75);
				$bytes = self::getBytes($num_bytes);
				return substr(rtrim(base64_encode($bytes), '='), 0, $length);
			case 'ALPHANUMERIC':
			default:
				$str = 'abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
		}

		$bytes = Tools::getBytes($length);
		$position = 0;
		$result = '';

		for ($i = 0; $i < $length; $i++) {
			$position = ($position + ord($bytes[$i])) % strlen($str);
			$result .= $str[$position];
		}

		return $result;
	}

	/**
	 * Random bytes generator
	 *
	 * Thanks to Zend for entropy
	 *
	 * @param $length Desired length of random bytes
	 * @return bool|string Random bytes
	 */
	public static function getBytes($length)
	{
		$length = (int)$length;

		if ($length <= 0) {
			return false;
		}

		if (function_exists('openssl_random_pseudo_bytes')) {
			$bytes = openssl_random_pseudo_bytes($length, $crypto_strong);

			if ($crypto_strong === true) {
				return $bytes;
			}
		}

		if (function_exists('mcrypt_create_iv')) {
			$bytes = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);

			if ($bytes !== false && strlen($bytes) === $length) {
				return $bytes;
			}
		}

// Else try to get $length bytes of entropy.
// Thanks to Zend

		$result         = '';
		$entropy        = '';
		$msec_per_round = 400;
		$bits_per_round = 2;
		$total          = $length;
		$hash_length    = 20;

		while (strlen($result) < $length) {
			$bytes  = ($total > $hash_length) ? $hash_length : $total;
			$total -= $bytes;

			for ($i=1; $i < 3; $i++) {
				$t1 = microtime(true);
				$seed = mt_rand();

				for ($j=1; $j < 50; $j++) {
					$seed = sha1($seed);
				}

				$t2 = microtime(true);
				$entropy .= $t1 . $t2;
			}

			$div = (int) (($t2 - $t1) * 1000000);

			if ($div <= 0) {
				$div = 400;
			}

			$rounds = (int) ($msec_per_round * 50 / $div);
			$iter = $bytes * (int) (ceil(8 / $bits_per_round));

			for ($i = 0; $i < $iter; $i ++) {
				$t1 = microtime();
				$seed = sha1(mt_rand());

				for ($j = 0; $j < $rounds; $j++) {
					$seed = sha1($seed);
				}

				$t2 = microtime();
				$entropy .= $t1 . $t2;
			}

			$result .= sha1($entropy, true);
		}

		return substr($result, 0, $length);
	}

	public static function strReplaceFirst($search, $replace, $subject, $cur = 0)
	{
		return (strpos($subject, $search, $cur))?substr_replace($subject, $replace, (int)strpos($subject, $search, $cur), strlen($search)):$subject;
	}


	/**
	 * Redirect URLs already containing PS_BASE_URI
	 *
	 * @param string $url Desired URL
	 */
	public static function redirectLink($url)
	{
		if(!preg_match('@^https?://@i', $url)) {
			if(strpos($url, WEB_DNS) !== false && strpos($url, WEB_DNS) == 0) {
				$url = substr($url, strlen(WEB_DNS));
			};
			$explode = explode('?', $url);
			$url = $explode[0];
			if(isset($explode[1])){
				$url .= '?'.$explode[1];
			};
		};
		header('Location: '.$url) or die("<script>window.open('".$url."','_self');</script>");
		//header('Location: '.$url);

		exit;
	}

	/**
	 * Redirect user to another admin page
	 *
	 * @param string $url Desired URL
	 */
	public static function redirectAdmin($url)
	{
		header('Location: '.$url);
		exit;
	}

	public static function htmlentitiesUTF8($string, $type = ENT_QUOTES)
	{
		if (is_array($string)) {
			return array_map(array('Tools', 'htmlentitiesUTF8'), $string);
		}

		return htmlentities((string)$string, $type, 'utf-8');
	}

	/**
	 * getHttpHost return the <b>current</b> host used, with the protocol (http or https) if $http is true
	 * This function should not be used to choose http or https domain name.
	 * Use Tools::getShopDomain() or Tools::getShopDomainSsl instead
	 *
	 * @param bool $http
	 * @param bool $entities
	 * @return string host
	 */
	public static function getHttpHost($http = false, $entities = false, $ignore_port = false)
	{
		$host = (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST']);
		if ($ignore_port && $pos = strpos($host, ':')) {
			$host = substr($host, 0, $pos);
		}
		if ($entities) {
			$host = htmlspecialchars($host, ENT_COMPAT, 'UTF-8');
		}
		if ($http) {
			$host = (Config::get('WEB_SSL_ENABLED') ? 'https://' : 'http://').$host;
		}
		return $host;
	}
	public static function strtolower($str)
	{
		if(is_array($str)) return false;
		if(function_exists('mb_strtolower')) return mb_strtolower($str, 'utf-8');
		return strtolower($str);
	}

	public static function getCurrentUrlProtocolPrefix()
	{
		if(Tools::usingSecureMode()) {
			return 'https://';
		}else{
			return 'http://';
		};
//		return 'http://';
	}

	public static function file_exists_no_cache($filename)
	{
		clearstatcache();
		return file_exists($filename);
	}
//使用安全模式
	public static function usingSecureMode()
	{
		if (isset($_SERVER['HTTPS'])) {
			return in_array(Tools::strtolower($_SERVER['HTTPS']), array(1, 'on'));
		}
// $_SERVER['SSL'] exists only in some specific configuration
		if (isset($_SERVER['SSL'])) {
			return in_array(Tools::strtolower($_SERVER['SSL']), array(1, 'on'));
		}
// $_SERVER['REDIRECT_HTTPS'] exists only in some specific configuration
		if (isset($_SERVER['REDIRECT_HTTPS'])) {
			return in_array(Tools::strtolower($_SERVER['REDIRECT_HTTPS']), array(1, 'on'));
		}
		if (isset($_SERVER['HTTP_SSL'])) {
			return in_array(Tools::strtolower($_SERVER['HTTP_SSL']), array(1, 'on'));
		}
		if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
			return Tools::strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https';
		}
//		return true;
		return false;
	}
	public static function getValue($key, $default_value = false){
		if(!isset($key) || empty($key) || !is_string($key)) return false;
		$ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default_value));
		if(is_string($ret)) return stripslashes(urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret))));
		return $ret;
	}

	public static function isSubmit($submit){
		return isset($_POST[$submit]) || isset($_GET[$submit]);
	}

//取得IP
	public static function getRemoteAddr()
	{
		if (function_exists('apache_request_headers')) {
			$headers = apache_request_headers();
		} else {
			$headers = $_SERVER;
		}

		if (array_key_exists('X-Forwarded-For', $headers)) {
			$_SERVER['HTTP_X_FORWARDED_FOR'] = $headers['X-Forwarded-For'];
		}

		if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] && (!isset($_SERVER['REMOTE_ADDR'])
				|| preg_match('/^127\..*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^172\.16.*/i', trim($_SERVER['REMOTE_ADDR']))
				|| preg_match('/^192\.168\.*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^10\..*/i', trim($_SERVER['REMOTE_ADDR'])))) {
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')) {
				$ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				return $ips[0];
			} else {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}
	protected function l($string, $class = null, $addslashes = false, $htmlentities = true)
	{
		if ($class === null || $class == 'AdminTab') {
			$class = substr(get_class($this), 0, -10);
		} elseif (strtolower(substr($class, -10)) == 'controller') {
			/* classname has changed, from AdminXXX to AdminXXXController, so we remove 10 characters and we keep same keys */
			$class = substr($class, 0, -10);
		}
		return Translate::getAdminTranslation($string, $class, $addslashes, $htmlentities);
	}

	public static function displayBugLog($message = null)
	{
		$backtrace = debug_backtrace();
		$callee = next($backtrace);
		$class = isset($callee['class']) ? $callee['class'] : null;
		$error = 'Function <b>'.$callee['function'].'()</b> is deprecated in <b>'.$callee['file'].'</b> on line <b>'.$callee['line'].'</b><br />';
	}
}

/**
 * Compare 2 prices to sort products
 *
 * @param float $a
 * @param float $b
 * @return int
 */
/* Externalized because of a bug in PHP 5.1.6 when inside an object */
function cmpPriceAsc($a, $b)
{
	if ((float)$a['price_tmp'] < (float)$b['price_tmp']) {
		return (-1);
	} elseif ((float)$a['price_tmp'] > (float)$b['price_tmp']) {
		return (1);
	}
	return 0;
}

function cmpPriceDesc($a, $b)
{
	if ((float)$a['price_tmp'] < (float)$b['price_tmp']) {
		return 1;
	} elseif ((float)$a['price_tmp'] > (float)$b['price_tmp']) {
		return -1;
	}
	return 0;
}
