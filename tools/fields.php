<?php
function fields_floor($f, $b=0, $rf=0){
	$arr_floor = array();
	for($i=$b; $i>0; $i--){
		$arr_floor[] = array('val' => 'B'.$i, 'text' => 'B'.$i);
	}
	for($i=1; $i<=$f; $i++){
		$arr_floor[] = array('val' => $i.'F', 'text' => $i.'F');
	}
	for($i=1; $i<=$rf; $i++){
		$arr_floor[] = array('val' => $i.'RF', 'text' => $i.'RF');
	}
	return $arr_floor;
}

function fields_floor2($f, $b=0, $rf=0){
	$arr_floor = array();
	$arr_floor[] = array('value' => 'XF', 'title' => 'XF');
	for($i=$b; $i>0; $i--){
		$arr_floor[] = array('value' => 'B'.$i, 'title' => 'B'.$i);
	}
	for($i=1; $i<=$f; $i++){
		$arr_floor[] = array('value' => $i.'F', 'title' => $i.'F');
	}
	for($i=1; $i<=$rf; $i++){
		$arr_floor[] = array('value' => $i.'RF', 'title' => $i.'RF');
	}
	return $arr_floor;
}

function fields_floor3($id, $type=true){
	$arr_floor = array();
	$row = array();
	if($type){
		$sql = sprintf('SELECT w.`build_floor`
			FROM `'.DB_PREFIX_.'houses` AS h
			LEFT JOIN `'.DB_PREFIX_.'web` AS w ON w.`id_web` = h.`id_web`
			WHERE `id_houses` = %d
			LIMIT 0, 1', GetSQL($id, 'int'));
		$row = Db::rowSQL($sql, true);
	}else{
		$sql = sprintf('SELECT `build_floor`
			FROM `'.DB_PREFIX_.'web`
			WHERE `id_web` = %d
			LIMIT 0, 1', GetSQL($id, 'int'));
		$row = Db::rowSQL($sql, true);
	}
	$json = new JSON();
	$arr_build_floor = $json->decode($row['build_floor']);
	foreach($arr_build_floor as $i => $v){
		$arr_floor[] = array('val' => $v, 'text' => $v);
	}
	return $arr_floor;
}

function fields_floor4($id){
	$arr_floor = array();
	$row = array();
	$sql = sprintf('SELECT w.`build_floor`
		FROM `repair__service` AS s
		LEFT JOIN `'.DB_PREFIX_.'web` AS w ON w.`id_web` = s.`id_build_case`
		WHERE s.`id_service` = %d
		LIMIT 0, 1', GetSQL($id, 'int'));
	$row = Db::rowSQL($sql, true);
	$json = new JSON();
	$arr_build_floor = $json->decode($row['build_floor']);
	foreach($arr_build_floor as $i => $v){
		$arr_floor[] = array('val' => $v, 'text' => $v);
	}
	return $arr_floor;
}