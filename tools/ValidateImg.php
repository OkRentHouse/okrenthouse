<?php

/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2017/1/17
 * Time: 上午 10:01
 */
class ValidateImg
{
	protected static $instance = null;
	public $img_height = 22;		// 圖形高度
	public $img_width = 60;		// 圖形寬度
	public $num = '';			// 數字
	public $mass = 100;		// 雜點的數量，數字愈大愈不容易辨識
	public $num_max = 4;		// 產生6個驗證碼
	public $font_size = 24;		//字體大小
	public $text_color = '';		//字體顏色
	public $use_font = false;	//引用字體
	public $word = '';			//顯示文字
	public $angle = 60;			//角度
	public $strx_type = true;		//啟用雜點
	public $font_path = 'arial.ttf';	//使用字體

	protected function __construct(){

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new ValidateImg();
		};
		return self::$instance;
	}

	public function getImg(){
		for($i=0;$i<$this->num_max;$i++)  //驗證產生6碼
		{
			$this->num .= rand(0,9);
		}
		if(!session_id()){
			session_start();
		}
		$_SESSION['verification2'] = $this->num;
// 創造圖片，定義圖形和文字顏色
		header("Content-type: image/PNG");
		srand((double)microtime() * 1000000);
		$im = imagecreate($this->img_width, $this->img_height);
		$black = ImageColorAllocate($im, 85,85,85);     // 底色
		$this->text_color = ImageColorAllocate($im, 255, 255, 255); //文字顏色
		imagefill($im, 0, 0, $black);
// 在圖形產上黑點，起干擾作用;


		if ($this->use_font == false)
		{
			for( $i=0; $i<$this->mass; $i++ )
			{
				if(false) $this->text_color = ImageColorAllocate($im, 255, 255, 255); //文字顏色
				imagesetpixel($im, rand(0, $this->img_width), rand(0, $this->img_height), $this->text_color);
			}
			$font_width      = imagefontwidth( $this->font_size ) * strlen( $this->word ) /2;
			$font_height     = imagefontheight( $this->font_size ) / 2;
		}else{
			if(false) $this->angle = 0;		//角度
			$bbox            = imagettfbbox($this->font_size, $this->angle, $this->font_path, $this->word );
			$font_width      = $bbox[4] / 2;
			$font_height     = $bbox[5] / 2;
		}
		$x  = $this->img_width / 2 - $font_width;
		$y  = $this->img_height / 2 - $font_height;

// 將數字隨機顯示在圖形上,文字的位置都按一定波動範圍隨機生成
		if($this->strx_type){
			$strx = rand(0, 8);
			for($i=0; $i<$this->num_max; $i++){
				$strpos = rand(1, 8);
				imagestring($im, 5, $strx, $strpos, substr($this->num, $i, 1), $this->text_color);
				$strx += rand(15, 8);
			}
		}


		if ($this->use_font == false){
			imagestring($im, $this->font_size, $x, $y, $this->word, $this->text_color);
		}else{
			imagettftext($im, $this->font_size, $this->angle, $x, $y, $this->text_color, $this->font_path, $this->word);
		}

		ImagePNG($im);
		ImageDestroy($im);
		exit;
		return $this;
	}
}