<?php
/*/
mysql_query("SET CHARACTER_SET_DATABASE='utf8'"); 
mysql_query("SET CHARACTER_SET_CLIENT='utf8'"); 
mysql_query("SET CHARACTER_SET_RESULTS='utf8'");
//*/
function uuid()
{
	$sql = 'SELECT UUID()';
	$SELECT_UUID = new db_mysql($sql);
	$uuid = $SELECT_UUID->row();
	return str_replace('-', '', $uuid[0]);
};

//MySQL字串取代 MySQL資安,反有心者以字串轉指令攻擊
if (!function_exists("GetSQL")) { //判別function GetSQL()是否已經定義過,沒定義才設(防止重複設定function名稱)
	function GetSQL($theValue, $theType = 'text', $theDefinedValue = "", $theNotDefinedValue = "")
	{//Get SQL Value String 獲取SQL字串值
		$theValue = _addslashes($theValue);
		switch ($theType) {
			case "text": //字串
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long"://常整數
			case "int": //整數
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double": //被俘點數
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "defined": //自訂
			default:
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue; //有就是$theDefinedValue,沒有就是$theNotDefinedValue
				break;
		};
		return $theValue;
	};
};

function _addslashes($s)
{  //懶人包:經過這個function後,不管是php5還是6,單雙引號都會加上反斜線
    return addslashes($s);
//	if (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) {
//		return $s;
//	} else {
//		return addslashes($s);
//	};
};

class db_mysql
{
	//MySQL資料庫相關設定
	public $pageNum, $nMax, $page_jmp_name, $pageNum_text, $nMax_text;
	public $conn, $query, $row, $num, $in_num, $up_num, $close, $total_page, $next_row, $pageNum_htm; //相關變數
	private $showmax = 1, $conn_run = false;

	function db_mysql($sql, $nMax = 0, $nMin = '', $pageNum_text = 'p', $nMax_text = 'm', $conn = '')
	{//(MySQL語法、顯示幾筆資料、第幾頁)MySQLdb($sql)顯示全部,MySQLdb($sql,$nMax)只顯示0~$nMax筆資料,MySQLdb($sql,$nMax,$nMin)顯示第$nMin~$nMax筆資料
		$this->pageNum_text = $pageNum_text;
		$this->nMax_text = $nMax_text;
		//連接資料庫
		$this->conn($conn);

		//如果只new 就不跑裡面
		if (isset($sql)) {
			if (($nMax < 0) && (!is_int($nMax))) $nMax = 1; //若不為數字,轉換成整數1
			$this->nMax = floor($nMax);//將數字(浮點數)調整為整數
			if ((Defaultn($this->pageNum_text, 1) != null) && (Defaultn($this->pageNum_text, 1) > 0)) {
				$this->pageNum = floor(Defaultn($this->pageNum_text, 1));//目前頁數
			} else {
				$this->pageNum = 1;//目前頁數
			};
			//if(isset($_GTT[$this->pageNum_text]))$this->pageNum=$_GTT[$this->pageNum_text];//目前頁數
			if ($this->pageNum == '') $this->pageNum = 1;
			if ($this->nMax != 0) {//判別最多要顯示幾筆資料,$nMax=0就顯示全部
				//$this->queryall = mysql_query($sql, $this->conn) or die(mysql_error());//全部筆資料
				$this->queryall = mysqli_query($this->conn,$sql) or die(mysqli_error($this->conn));//全部筆資料
				$max_pageNum = max(ceil($this->num() / $this->nMax) - 1, 0) + 1;
				$this->pageNum = floor(min($this->pageNum, $max_pageNum));
				if (empty($nMin)) {//判別是第幾頁
					$nMin = floor(($this->pageNum - 1) * $this->nMax);//$nMin起始值,第一頁第一個為0
				};
				$sql = sprintf("%s LIMIT %d, %d", $sql, $nMin, $nMax);//從第$nMin筆資料往後顯示$nMax筆資料
			};
			//$this->query=mysql_query($sql,$this->conn) or die(mysql_error());
			//$this->query = mysql_query($sql, $this->conn);
			$this->query = mysqli_query($this->conn,$sql);
		};
	}

	function conn($conn)
	{
		//MySQL資料庫相關設定
		if(empty($conn)){		//預設
			$DB_HOST = DB_HOST;
			$DB_USER = DB_USER;
			$DB_PASS = DB_PASS;
			$E_USER_ERROR = E_USER_ERROR;
			$DB_DATABASE = DB_DATABASE;
		}else{		//指定資料庫
            $DB_HOST = DB_HOST;
            $DB_USER = DB_USER;
            $DB_PASS = DB_PASS;
            $E_USER_ERROR = E_USER_ERROR;
            $DB_DATABASE = $conn;
		}

		$this->conn = mysqli_connect($DB_HOST, $DB_USER, $DB_PASS) or trigger_error(mysqli_error($this->conn)); //連到MySQL資料庫
		mysqli_query($this->conn,"SET NAMES 'utf8'"); //一定要加這個,不然新增時 中文會是亂碼
		mysqli_select_db($this->conn,$DB_DATABASE);

	}

	function row()
	{ //顯示資料(用法:$a=$this->row; $a['資料欄位'];
		return mysqli_fetch_assoc($this->query);
		//return mysql_fetch_array($this->query);		//數字+字串key
	}

	function getRow($nMax)
	{
		//如果只new 就不跑裡面
		if (isset($sql)) {
			if (($nMax <= 0) && (!is_int($nMax))) $nMax = 1;//若不為數字,轉換成整數1
			$this->nMax = floor($nMax);//將數字(浮點數)調整為整數
			if ((Defaultn($this->pageNum_text, 1) != null) && (Defaultn($this->pageNum_text, 1) > 0)) {
				$this->pageNum = floor(Defaultn($this->pageNum_text, 1));//目前頁數
			} else {
				$this->pageNum = 1;//目前頁數
			};
			//if(isset($_GTT[$this->pageNum_text]))$this->pageNum=$_GTT[$this->pageNum_text];//目前頁數
			if ($this->pageNum == '') $this->pageNum = 1;
			if ($this->nMax != 0) {//判別最多要顯示幾筆資料,$nMax=0就顯示全部
				$this->queryall = mysqli_query($this->conn,$sql) or die(mysqli_error($this->conn));//全部筆資料
				$max_pageNum = max(ceil($this->num() / $this->nMax) - 1, 0) + 1;
				$this->pageNum = floor(min($this->pageNum, $max_pageNum));
				if (empty($nMin)) {//判別是第幾頁
					$nMin = floor(($this->pageNum - 1) * $this->nMax);//$nMin起始值,第一頁第一個為0
				};
				$sql = sprintf("%s LIMIT %d, %d", $sql, $nMin, $nMax);//從第$nMin筆資料往後顯示$nMax筆資料
			};
			//$this->query=mysql_query($sql,$this->conn) or die(mysql_error());
			$this->query = mysqli_query($this->conn,$sql);
		};
		if ($this->num() > 0) {
			if ($this->num() == 1) {
				return $this->row();
			} else {
				$arr_r = array();
				while ($r = $this->next_row()) {
					array_push($arr_r, $r);
				};
				return $arr_r;
			};
		}
		return array();
	}

	function num()
	{ //查詢總數量
		if ($this->nMax != 0) {
			return max((int)mysqli_num_rows($this->queryall), (int)mysqli_insert_id($this->conn), (int)mysqli_affected_rows($this->conn));
		} else {
			return max((int)mysqli_num_rows($this->query), (int)mysqli_insert_id($this->conn), (int)mysqli_affected_rows($this->conn));
		}

//        if ($this->nMax != 0) {
//            if((int)mysqli_num_rows($this->queryall)!=0 && (int)mysqli_insert_id($this->conn)==0){
//                return (int)mysqli_num_rows($this->queryall);
//            }else{
//                return (int)mysqli_affected_rows($this->conn);
//            }
////            return (int)mysqli_num_rows($this->queryall);
////            return (int)mysqli_affected_rows($this->queryall);
//        } else {
//            if((int)mysqli_num_rows($this->query)!=0){
//                return (int)mysqli_num_rows($this->query);
//            }else{
//                return (int)mysqli_affected_rows($this->conn);
//            }
////            return (int)mysqli_num_rows($this->query);
////            return (int)mysqli_affected_rows($this->query);
//        }
	}

	function in_num()
	{ //查詢總數量
		return mysqli_insert_id($this->conn);
	}

	function up_num()
	{ //查詢總數量
		return mysqli_affected_rows($this->conn);
	}

	function close()
	{ //關閉MySQL連線
		mysqli_close($this->conn);
		$conn_run = false;
	}

	function free()
	{ //釋放記憶
		if ($this->nMax != 0) {
			mysqli_free_result($this->queryall);
			mysqli_free_result($this->query);
		} else {
			mysqli_free_result($this->query);
		};
	}

	function total_page()
	{ //查詢總頁數
		if ($this->nMax != 0) {
			return max(1, ceil($this->num() / $this->nMax));
		} else {
			return 1;
		}
	}

	function next_row()
	{ //下一筆資料(有下一筆回傳true,沒有下一筆回傳false)
		return mysqli_fetch_assoc($this->query);
	}

	function pageNum_htm()
	{
		if ($this->num() > 0) {
			return $this->pageNum . '/' . $this->total_page();
		} else {
			return 0;
		};
	}
	//跳頁(未完成)
	//如:
	function jumppage($page_first = '第一頁', $page_pre = '上一頁', $page_next = '下一頁', $page_end = '最後一頁', $page_jmp_name = '')
	{ //跳頁顯示$page_first='第一頁文字',$page_pre='上一頁文字',$page_next='下一頁文字',$page_end='最後一頁文字',$pageNum_text=跳頁的判斷"跳葉欄"文字,$page_jmp_name='跳至錨點'
		if ($this->num() > 0) {
			$queryStringr = "";
			if (!empty($_SERVER['QUERY_STRING'])) {//判斷有沒有 $_$GET
				$params = explode("&", $_SERVER['QUERY_STRING']); //將每個$_GET拆開
				$newParams = array();
				foreach ($params as $param) { //判斷$_GET裡有沒有不是$_GET['pageNum']的放進去
					if (@!stristr($param, $this->pageNum_text . "=") && @!stristr($param, $this->nMax_text . "=") && @!stristr($param, str_replace('&', '', $page_jmp_name))) {
						array_push($newParams, $param);
					}
				}
				if (count($newParams) != 0) {
					$queryStringr = htmlentities(implode("&", $newParams)) . "&";
				}
			}
			$currentPage = $_SERVER["PHP_SELF"];
			$currentPage = str_replace('/DH_index.php', '', $currentPage);
			$pagesmin = $this->pageNum - 3;
			if ($this->pageNum < 3) $pagesmin = 1; //1-7
			$pagesmax = $pagesmin + 5;
			if ($pagesmax >= $this->total_page()) {
				$pagesmax = $this->total_page();
				$pagesmin = $pagesmin - (($pagesmin + 5) - $this->total_page());
			}
			$pagesmin = max($pagesmin, 1);
			if ($pagesmax - 5 > 1) $pagesmin = $pagesmax - 5;
			$page_html = '';
			if ($pagesmin != $pagesmax) {//只有一頁就不顯是跳頁選單
				if ($pagesmax != 1) {
					$page_html .= '<ul>';
					$page_html .= '<li><a alt="第一頁" title="第一頁" class="first" href="' . $currentPage . '?' . $queryStringr . $this->pageNum_text . '=1&' . $this->nMax_text . '=' . $this->nMax . $page_jmp_name . '">' . $page_first . '</a></li>';//第一頁
					$page_html .= '<li><a alt="上一頁" title="上一頁" class="pre" href="' . $currentPage . '?' . $queryStringr . $this->pageNum_text . '=' . max(1, $this->pageNum - 1) . '&' . $this->nMax_text . '=' . $this->nMax . $page_jmp_name . '">' . $page_pre . '</a></li>';//上一頁
					for ($i = $pagesmin; $i <= $pagesmax; $i++) {
						$i_page = '';
						if ($i == $this->pageNum) {
							$i_page = ' active';
						};
						$page_html .= '<li class="' . $i_page . '"><a alt="第' . $i . '頁" title="第' . $i . '頁" class="page_hover pageNum' . $i . $i_page . '" id="pageNum' . $i . '" href="' . $currentPage . '?' . $queryStringr . $this->pageNum_text . '=' . $i . '&' . $this->nMax_text . '=' . $this->nMax . $page_jmp_name . '">' . $i . '</a></li>'; //跳到第N頁,最多顯示七筆,1、2、3、本頁、5、6、7
					}
					$page_html .= '<li><a alt="下一頁" title="下一頁" class="next" href="' . $currentPage . '?' . $queryStringr . $this->pageNum_text . '=' . min($this->total_page(), $this->pageNum + 1) . '&' . $this->nMax_text . '=' . $this->nMax . $page_jmp_name . '">' . $page_next . '</a></li>';//下一頁
					$page_html .= '<li><a alt="最後一頁" title="最後一頁" class="end" href="' . $currentPage . '?' . $queryStringr . $this->pageNum_text . '=' . $this->total_page() . '&' . $this->nMax_text . '=' . $this->nMax . $page_jmp_name . '">' . $page_end . '</a></li></ul><input name="' . $this->pageNum_text . '" id="' . $this->pageNum_text . '" type="hidden" value="' . $this->pageNum . '" />';//最後一頁
					$page_html .= '</ul>';
				}
			}
			$this->page_jmp_name = $page_jmp_name;
		};
		return $page_html;
	}

	function jamp_nMax($r1 = '', $r2 = '')
	{//一頁顯示幾筆資料輸入話框  $r1=前面文字,$r2=後面文字  $r1.<input>.$r2
		if ($this->pageNum > 200) {
			$this->pageNum = 200;
		};
		$html = $r1 . '<input size="4" name="nMax" id="nMax" value="' . $this->nMax . '" type="text" />' . $r2;
		$html .= '<script type="text/javascript">';
		//$html .= '$(document).ready(function() {';
		$html .= "$('.pageNum" . $this->pageNum . "').addClass('thispage');";
		$html .= "$('#nMax').blur(function(){";
		$html .= "$('#storimg').submit();";
		$html .= "});";
		//$html .=  "});";
		$html .= '</script>';
		return $html;
	}

	function jamp_pageNum()
	{//跳到第幾頁輸入話框  $r1=前面文字,$r2=後面文字  $r1.<input>.$r2
		if ($this->total_page() == 0) $this->pageNum = 0;
		$html = '<input size="4" name="' . $this->pageNum_text . '" class="' . $this->pageNum_text . '" value="' . $this->pageNum . '" type="text" />';
		$html .= '<script type="text/javascript">';
		//$html.='$(document).ready(function() {'; 
		$html .= "$('#" . $this->pageNum_text . "').blur(function(){";
		$html .= "" . $this->page_jmp_name . "";
		$html .= "$('#storimg').submit();";
		$html .= "});";
		//$html.="});";
		$html .= '</script>';
		return $html;
	}
}

//$nMax=Defaultn('nMax',20);//跳頁顯示判斷 預設20
//要新增一個目前頁面的跳頁按鈕顏色壯到 .thispage{}
/*$('.pageNum<?php echo $standard_stock->pageNum;?>').addClass('thispage');//跳頁變色*/


//新增
//$sql = sprintf("INSERT INTO `menu_class` (帳號, 密碼) VALUES (%s, %d)",
//	GetSQL($_POST['帳號'], "text"),
//	GetSQL($_POST['密碼'], "text"));
//
//$a = db_mysql($sql);
//header('Location:index.php');//跳頁
//
////修改
//$updateSQL = sprintf("UPDATE `menu_class` SET MenuClassName=%s, MenuDIVClassName=%s WHERE ID=%s",
//	GetSQL($_POST['MenuClassName'], "text"),
//	GetSQL($_POST['MenuDIVClassName'], "text"),
//	GetSQL($_POST['ID'], "int"));
//$a = db_mysql($sql);
//header('Location:index.php');//跳頁
//
////刪除
//$deleteSQL = sprintf("DELETE FROM` menu_class` WHERE ID=%s",
//	GetSQL($_POST['deltmenuid'], "int"));
//$a = db_mysql($sql);
//header('Location:index.php');//跳頁

//header("Location:index.php");