<?php

class AdminCMSController extends AdminController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'cms';
	public $ed;

	public function __construct()
	{
		$this->className       = 'AdminCMSController';
		$this->fields['title'] = '網頁管理';
		$this->table           = 'cms';
		$this->fields['index'] = 'id_cms';
		$this->fields['list']  = [
			'id_cms'         => [
				'index'  => true,
				'hidden' => true,
				'type'   => 'checkbox',
				'class'  => 'text-center',
			],
			'url'            => [
				'title'  => $this->l('URL'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'title'          => [
				'title'  => $this->l('Title'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'page_title'     => [
				'title'  => $this->l('頁面標題'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'description'    => [
				'title'  => $this->l('Description'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'keywords'       => [
				'title'  => $this->l('Keywords'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'display_header' => [
				'title'  => $this->l('顯示Header'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'display_h_1',
						'value' => '1',
						'title' => $this->l('顯示'),
					],
					[
						'class' => 'display_h_0',
						'value' => '0',
						'title' => $this->l('隱藏'),
					],
				],
			],
			'display_footer' => [
				'title'  => $this->l('顯示Foote'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'display_f_1',
						'value' => '1',
						'title' => $this->l('顯示'),
					],
					[
						'class' => 'display_f_0',
						'value' => '0',
						'title' => $this->l('隱藏'),
					],
				],
			],
			'index_ation'    => [
				'title'  => $this->l('開啟索引'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'index_a_1',
						'value' => '1',
						'title' => $this->l('開啟'),
					],
					[
						'class' => 'index_a_0',
						'value' => '0',
						'title' => $this->l('關閉'),
					],
				],
			],
			'active'         => [
				'title'  => $this->l('啟用'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('網頁管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'     => 'id_cms',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					[
						'name'      => 'url',
						'type'      => 'text',
						'required'  => true,
						'maxlength' => 200,
						'label'     => $this->l('URL'),
						'hint'      => $this->l('網址符號只能用 _- 字元'),
					],
					[
						'name'      => 'title',
						'type'      => 'text',
						'maxlength' => 100,
						'label'     => $this->l('Title'),
					],
					[
						'name'      => 'page_title',
						'type'      => 'text',
						'maxlength' => 50,
						'label'     => $this->l('頁面標題'),
					],
					[
						'name'      => 'description',
						'type'      => 'text',
						'maxlength' => 300,
						'label'     => $this->l('Description'),
					],
					[
						'name'      => 'keywords',
						'type'      => 'text',
						'class'     => 'no_enter',
						'maxlength' => 300,
						'data'      => ['role' => 'tagsinput'],
						'label'     => $this->l('Keywords'),
					],
					[
						'name'     => 'html',
						'type'     => 'textarea',
						'col'      => 8,
						'rows'     => 12,
						'required' => true,
						'class'    => 'tinymce',
						'label'    => $this->l('網頁內容'),
					],
					[
						'type'     => 'switch',
						'label'    => $this->l('顯示Header'),
						'name'     => 'display_header',
						'required' => true,
						'val'      => 1,
						'values'   => [
							[
								'id'    => 'display_h_1',
								'value' => 1,
								'label' => $this->l('顯示'),
							],
							[
								'id'    => 'display_h_0',
								'value' => 0,
								'label' => $this->l('隱藏'),
							],
						],
					],
					[
						'type'     => 'switch',
						'label'    => $this->l('顯示Foote'),
						'name'     => 'display_footer',
						'required' => true,
						'val'      => 1,
						'values'   => [
							[
								'id'    => 'display_f_1',
								'value' => 1,
								'label' => $this->l('顯示'),
							],
							[
								'id'    => 'display_f_0',
								'value' => 0,
								'label' => $this->l('隱藏'),
							],
						],
					],
					[
						'type'     => 'switch',
						'label'    => $this->l('開啟索引'),
						'name'     => 'index_ation',
						'required' => true,
						'val'      => 1,
						'hint'     => $this->l('若關閉搜尋引擎將無法搜尋到此網頁'),
						'values'   => [
							[
								'id'    => 'index_a_1',
								'value' => 1,
								'label' => $this->l('開啟'),
							],
							[
								'id'    => 'index_a_0',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
					[
						'type'     => 'switch',
						'label'    => $this->l('啟用狀態'),
						'name'     => 'active',
						'required' => true,
						'val'      => 1,
						'values'   => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
					[
						'title' => $this->l('儲存並繼續編輯'),
						'stay'  => true,
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
		];
		parent::__construct();
	}

	public function validateRules()
	{
		parent::validateRules();
		$url = Tools::getValue('url');
		if (!isTwUrl($url)) {
			$this->_errors[] = $this->l('網址符號只能用 _- 字元');
		}
		if (isints($url)) {
			$this->_errors[] = $this->l('網址不能只是數字');
		}
	}

	public function setMedia()
	{
		$this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
		parent::setMedia();
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
	}
}