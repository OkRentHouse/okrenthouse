<?php
/* Smarty version 3.1.28, created on 2020-12-12 00:56:24
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okpt/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd3a4b801aab0_88794489',
  'file_dependency' => 
  array (
    'b0cc768a096f615db88bda23773a21a721cfa4e5' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okpt/msg_box.tpl',
      1 => 1607678487,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd3a4b801aab0_88794489 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
<div class="alert alert-success <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
<div class="alert alert-danger <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
}
}
