<?php
/* Smarty version 3.1.28, created on 2020-12-11 17:32:43
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/pagination.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd33cbb5c48d0_43102370',
  'file_dependency' => 
  array (
    '46122a25f0aff08305718a80e4560983322eeb84' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/pagination.tpl',
      1 => 1607678486,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd33cbb5c48d0_43102370 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['pagination_max']->value > 1) {?><div class="pagination_wrap text-center ajax_pagination"><ul class="pagination"><?php if ($_GET['p'] > 1) {?><li><a href="?<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
=1&<?php echo $_smarty_tpl->tpl_vars['pagination_url']->value;?>
" data-p="1"><?php echo l(array('s'=>"第一頁"),$_smarty_tpl);?>
</a></li><li><a href="?<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
=<?php echo max($_GET['p']-1,1);?>
&<?php echo $_smarty_tpl->tpl_vars['pagination_url']->value;?>
" data-p="<?php echo max($_GET['p']-1,1);?>
"><?php echo l(array('s'=>"上一頁"),$_smarty_tpl);?>
</a></li><?php }
$_smarty_tpl->tpl_vars['p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['p']->step = 1;$_smarty_tpl->tpl_vars['p']->total = (int) min(ceil(($_smarty_tpl->tpl_vars['p']->step > 0 ? $_smarty_tpl->tpl_vars['pagination_max']->value+1 - ($_smarty_tpl->tpl_vars['pagination_min']->value) : $_smarty_tpl->tpl_vars['pagination_min']->value-($_smarty_tpl->tpl_vars['pagination_max']->value)+1)/abs($_smarty_tpl->tpl_vars['p']->step)),$_smarty_tpl->tpl_vars['pagination_num']->value);
if ($_smarty_tpl->tpl_vars['p']->total > 0) {
for ($_smarty_tpl->tpl_vars['p']->value = $_smarty_tpl->tpl_vars['pagination_min']->value, $_smarty_tpl->tpl_vars['p']->iteration = 1;$_smarty_tpl->tpl_vars['p']->iteration <= $_smarty_tpl->tpl_vars['p']->total;$_smarty_tpl->tpl_vars['p']->value += $_smarty_tpl->tpl_vars['p']->step, $_smarty_tpl->tpl_vars['p']->iteration++) {
$_smarty_tpl->tpl_vars['p']->first = $_smarty_tpl->tpl_vars['p']->iteration == 1;$_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration == $_smarty_tpl->tpl_vars['p']->total;?><li<?php if (($_GET['p'] == $_smarty_tpl->tpl_vars['p']->value) || (($_smarty_tpl->tpl_vars['p']->value == 1) && ($_GET['p'] <= 1)) || (($_smarty_tpl->tpl_vars['p']->value == $_smarty_tpl->tpl_vars['pagination_max']->value) && ($_GET['p'] >= $_smarty_tpl->tpl_vars['pagination_max']->value))) {?> class="active"<?php }?>><a href="?<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
=<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['pagination_url']->value;?>
" data-p="<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</a></li><?php }
}
if (($_GET['p'] < $_smarty_tpl->tpl_vars['pagination_max']->value)) {?><li><a href="?<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
=<?php echo min($_GET['p']+1,$_smarty_tpl->tpl_vars['pagination_max']->value);?>
&<?php echo $_smarty_tpl->tpl_vars['pagination_url']->value;?>
" data-p="<?php echo min($_GET['p']+1,$_smarty_tpl->tpl_vars['pagination_max']->value);?>
"><?php echo l(array('s'=>"下一頁"),$_smarty_tpl);?>
</a></li><li><a href="?<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
=<?php echo $_smarty_tpl->tpl_vars['pagination_max']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['pagination_url']->value;?>
" data-p="<?php echo $_smarty_tpl->tpl_vars['pagination_max']->value;?>
"><?php echo l(array('s'=>"最後一頁"),$_smarty_tpl);?>
</a></li><?php }?></ul></div><?php }
}
}
