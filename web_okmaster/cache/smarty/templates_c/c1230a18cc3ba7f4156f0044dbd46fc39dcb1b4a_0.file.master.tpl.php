<?php
/* Smarty version 3.1.28, created on 2020-12-17 20:50:20
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/master/master.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fdb540c2e6fb4_35137087',
  'file_dependency' => 
  array (
    'c1230a18cc3ba7f4156f0044dbd46fc39dcb1b4a' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/master/master.tpl',
      1 => 1607678512,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fdb540c2e6fb4_35137087 ($_smarty_tpl) {
?>
<hr>
<input type="hidden" id="start_form" value="<?php if ($_SESSION['id_member']) {?>start<?php }?>">
<form action="/ExpertAdvisor"  enctype="multipart/form-data" method="post" id="sumbit_data" >
    <div class="join_life_wrap step1" style="margin: 0 auto;">
        <div class="step_01">
            <span style="color:#3f0d81">Step1.加入申請</span>
            <img src="/themes/Rent/img/joinmaster/banner_01_arrow_an.svg"> Step2.上傳資料
        </div>
        <div class="applyform">
            <div class="row_134">
                <div class="row_1row_1_1">
                    <div class="row_1">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">
                                <img src="/themes/Rent/img/Jointalent/head.svg" class="svg">姓名
                            </span>
                            <input class="inp no_border" type="text" name="name"  maxlength="20" value="<?php echo $_SESSION['name'];?>
" maxlength="20" placeholder="" required="required">
                        </div>
                        <div class="form_item">
                            <span class="title">暱稱</span>
                            <input class="inp no_border" type="text" name="nickname" value="<?php echo $_SESSION['nickname'];?>
" maxlength="20" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="row_1_1">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">生日</span>
                            <input class="inp no_border" type="number" name="birthday_y" maxlength="4" placeholder="" required="required">年
                            <input class="inp no_border" type="number" name="birthday_m" maxlength="2" placeholder="" required="required">月
                            <input class="inp no_border" type="number" name="birthday_d" maxlength="2" placeholder="" required="required">日
                        </div>
                    </div>
                </div>
                <div class="row_3row_4">
                    <div class="row_3">
                        <div class="form_item"><span class="must">*</span>
                            <span class="title"><img src="/themes/Rent/img/Jointalent/telephone.svg" class="svg">手機</span>
                            <input class="inp no_border" type="text" name="phone" value="<?php echo $_SESSION['user'];?>
" placeholder="0912345678" maxlength="20"  required="required">
                        </div>
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">
                                <img src="/themes/Rent/img/Jointalent/line.svg" class="svg">Line lD
                            </span>
                            <input class="inp no_border" type="text" name="line_id" maxlength="32" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="row_4">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">電郵</span>
                            <input class="inp no_border" type="email" name="email" maxlength="128" placeholder="　　@　　　　　　　　" required="required">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_2">
                <div class="form_item">
                    <div class="post_ID">
                        <input type="hidden" name="postal" maxlength="6">
                        <span class="must">*</span><span class="title">通訊地址</span>
                        <input class="post_ID_n no_border" name="postal_1" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[1].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_2" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[2].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_3" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[3].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_4" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[4].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_5" type="number" maxlength="1" >
                        <select class="inp no_border" name="id_county" id="id_county">
                            <?php
$_from = $_smarty_tpl->tpl_vars['county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_county'] == 7) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value['county_name'];?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
                        </select>
                        <select class="inp no_border" name="id_city" id="id_city">
                        </select>
                        <input type="text" class="inp _10em no_border" name="address" placeholder="" maxlength="255" required="required">
                    </div>
                </div>
            </div>
            <div class="row_6">
                <div class="form_item">
                    <span>
                        <img src="/themes/Rent/img/Jointalent/speaker.svg" class="svg">訊息來源
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="0" checked="">友人分享
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="1">其他網站APP<input type="text" class="inp_bottom_line no_border" name="other_url" maxlength="128" placeholder="">
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="2">人力銀行<input type="text" class="inp_bottom_line no_border" name="other_intermediary" maxlength="128" placeholder="">
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="3">其他<input type="text" class="inp_bottom_line no_border" name="	other" maxlength="128" placeholder="">
                    </span>
                </div>
            </div>
            <div class="row_6_1" style="display:none">
                <span class="must">*</span>
                <span>
                          舉薦顧問
                    <input class="inp no_border" type="text" name="recommend_id_member" maxlength="20" placeholder="請填入分享代碼">
                          ※填入本欄資訊您將獲得紅利點數200點的獎勵喔!
                </span>
            </div>
            <div class="row_7">
                <div class="form_item" style="display: flex;">
                    <div class="submit" onclick="sub_next()">
                        下一步
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="join_life_wrap step2" style="margin: 0 auto;">
        <div id="step_02" class="step_02">Step1.加入申請
            <img src="/themes/Rent/img/joinmaster/banner_02_arrow_an.svg">
            <span style="color:#3f0d81">Step2.上傳資料</span>
        </div>
        <div class="applyform">
            <div class="row_134">
                <div class="row_1row_1_1">
                    <div class="row_1">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">身分證字號</span>
                            <input class="inp" type="text" name="identity" maxlength="10" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="row_1_1">
                        <div class="attachment">
                            <div class="row_1_1_1">

                                <input type="file" id="file_0" name="id_img[]" onchange="get_file_img('file_0')" required="required"  style="display:none">
                                <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">
                                <span>上傳身分證正面</span>
                                <div class="view file_0">
                                    <span>僅供申請生活達人之用，不得移為他用</span>
                                </div>
                                <img class="view" id="file_0_0" src="" style="display: none">
                            </div>
                            <div class="row_1_1_2">

                                <input type="file" id="file_1" name="id_img[]" onchange="get_file_img('file_1')" required="required"  style="display:none">
                                <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="1" class="get_file">
                                <span>上傳身分證反面</span>
                                <div class="view file_1">
                                    <span>僅供申請生活達人之用，不得移為他用</span>
                                </div>
                                <img class="view" id="file_1_0" src="" style="display: none">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_2">
                <div class="form_item">
                    <div class="post_ID">
                        <span class="must">*</span>
                        <span class="title">獎金撥人帳戶(限本人帳戶)</span>
                    </div>
                </div>
            </div>
            <div class="row_6 form2">
                <div class="form_item">
                    <div class="bank_tb">
                        <table class="bank_tb">
                            <tr>
                                <td class="row_5_td">
                                    <div class="row_5_1">
                                    <span class="title">
                                        <select style="width: 10rem!important;" class="inp no_border" name="bank" id="bank">
                                            <option>中國信託</option>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['bank']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
" data-code="<?php echo $_smarty_tpl->tpl_vars['v']->value['code'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_1_saved_key;
}
?>
                                        </select>銀行
                                    </span>
                                        <span class="title">
                                        <input class="inp" type="text" name="branch" maxlength="32" placeholder="" required="required" style="border: 1px solid;"><span class="title">分行<a target="_blank" href="/JoinMaster?p=list"><i class="fas fa-external-link-alt"></i></a>
                                    </span>
                                    <span class="title">銀行代碼：
                                        <input class="inp" type="text" name="bank_code" placeholder="822" maxlength="4" required="required">
                                    </span>
                                    <span class="title"></span>
                                    </div>
                                    <br>
                                    <span class="bank_txt_0">金融機構存款帳號(分行別、科目、編號、檢查號碼)</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="bank_ID">
                                        <table class="bank_ID_tb">
                                            <input type="hidden" name="bank_account" maxlength="32">
                                            <tr>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_1" type="number" maxlength="1" onkeyup="this.value=this.value.toUpperCase();document.getElementsByClassName('bank_ID_n')[1].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_2" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[2].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_3" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[3].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_4" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[4].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_5" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[5].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_6" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[6].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_7" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[7].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_8" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[8].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_9" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[9].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_10" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[10].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_11" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[11].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_12" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[12].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_13" type="number" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[13].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_14" type="number" maxlength="1" >
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row_6_1">
                <div class="attachment">
                    <div class="row_1_1_1">

                        <input type="file" id="file_2" name="account_img[]" onchange="get_file_img('file_2')" required="required"  style="display:none">
                        <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="account_img[]" data-value="0" class="get_file">
                        <span>上傳存摺正面</span>
                        <div class="view file_2">
                            <span>僅供申請生活達人之用，不得移為他用</span>
                        </div>
                        <img class="view" id="file_2_0" src="" style="display: none">
                    </div>
                    <div class="row_1_1_2">

                        <input type="file" id="file_3" name="account_img[]" onchange="get_file_img('file_3')" required="required"  style="display:none">
                        <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="account_img[]" data-value="1" class="get_file">
                        <span>上傳存摺反面</span>
                        <div class="view file_3">
                            <span>僅供申請生活達人之用，不得移為他用</span>
                        </div>
                        <img class="view" id="file_3_0" src="" style="display: none">
                    </div>
                </div>
            </div>
            <div class="row_6_1_1">
                <div class="form_item">
                    <div class="post_ID">
                        <input type="checkbox" name="agree" id="agree"><span class="title">我已閲讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</span>
                    </div>
                </div>
            </div>
            <div class="row_7">
                <div class="form_item" style="display: flex;">
                    <div class="submit" onclick="sub_per()">
                        返回
                    </div>
                </div>
                <p>&nbsp</p>
                <div class="form_item" style="display: flex;">
                    <div class="submit" onclick="submit_data()" class="submit_img">
                        送出
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php echo $_smarty_tpl->tpl_vars['master_message_js']->value;?>

<?php }
}
