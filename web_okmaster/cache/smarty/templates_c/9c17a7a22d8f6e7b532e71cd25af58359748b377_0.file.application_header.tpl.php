<?php
/* Smarty version 3.1.28, created on 2021-03-29 15:37:29
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/application_header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_606183b926e9e2_57389640',
  'file_dependency' => 
  array (
    '9c17a7a22d8f6e7b532e71cd25af58359748b377' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/application_header.tpl',
      1 => 1617003437,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606183b926e9e2_57389640 ($_smarty_tpl) {
?>
<div class="row topbanner">
  <div class="cell-1"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="#ABABAB" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg></div>
  <div class="cell-10 logo"></div>
  <div class="cell-1"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="hb_line" class="hb_line not_select" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve">
<g>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#ABABAB" d="M6.17,19.312h104.659c2.536,0,4.612,2.074,4.612,4.84l0,0   c0,2.767-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.075-4.61-4.842l0,0C1.56,21.386,3.634,19.312,6.17,19.312z"></path>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#ABABAB" d="M6.17,87.777h104.659c2.536,0,4.612,2.306,4.612,4.842l0,0   c0,2.766-2.076,5.071-4.612,5.071H6.17c-2.536,0-4.61-2.306-4.61-5.071l0,0C1.56,90.083,3.634,87.777,6.17,87.777z"></path>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#ABABAB" d="M6.17,53.428h104.659c2.536,0,4.612,2.306,4.612,5.072l0,0   c0,2.536-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.306-4.61-4.842l0,0C1.56,55.734,3.634,53.428,6.17,53.428z"></path>
</g>
<g>
	<defs>
		<path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"></path>
	</defs>
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_51_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"></rect>
	</defs>
	<clipPath id="SVGID_4_">
		<use xlink:href="#SVGID_81_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"></rect>
	</defs>
	<clipPath id="SVGID_6_">
		<use xlink:href="#SVGID_105_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"></rect>
	</defs>
	<clipPath id="SVGID_8_">
		<use xlink:href="#SVGID_129_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"></rect>
	</defs>
	<clipPath id="SVGID_10_">
		<use xlink:href="#SVGID_155_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"></rect>
	</defs>
	<clipPath id="SVGID_12_">
		<use xlink:href="#SVGID_183_" overflow="visible"></use>
	</clipPath>
</g>
<image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
</image>
</svg></div>
</div>
<?php }
}
