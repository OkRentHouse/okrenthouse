<?php
/* Smarty version 3.1.28, created on 2020-12-10 20:19:27
  from "/home/ilifehou/life-house.com.tw/themes/Okmaster/page_head.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd2124feddb97_58962936',
  'file_dependency' => 
  array (
    '02e1c748296416ecfa23db9d018610f70ccc4004' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Okmaster/page_head.tpl',
      1 => 1607594031,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd2124feddb97_58962936 ($_smarty_tpl) {
?>
<div class="head_group">
  
  <?php if ($_smarty_tpl->tpl_vars['page']->value) {?>
  <?php $_smarty_tpl->tpl_vars['page_name'] = new Smarty_Variable($_smarty_tpl->tpl_vars['page']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'page_name', 0);?>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['cms_url']->value) {?>
  <?php $_smarty_tpl->tpl_vars['page_name'] = new Smarty_Variable($_smarty_tpl->tpl_vars['cms_url']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'page_name', 0);?>
  <?php }?>

  <div class="head">
  <div class="logo" onclick="window.open('https://www.okmaster.life','_blank ')"></div>
  <div class="hyperlink">
    <label class="wall h-menu <?php if ($_smarty_tpl->tpl_vars['page_name']->value == 'Sterilization' || $_smarty_tpl->tpl_vars['page_name']->value == 'Housecheck' || $_smarty_tpl->tpl_vars['page_name']->value == 'Cleansolution') {?>now_here<?php }?>">
      <a href="#" class="dropdown-toggle">清淨對策</a>
        <ul class="d-menu" data-role="dropdown">
            <li><a href="/Sterilization">清淨滅菌</a></li>
            <li><a href="/Housecheck">健康宅檢測</a></li>
            <li><a href="/Cleansolution">清淨方案</a></li>
        </ul>
    </label>
    <label class="wall h-menu"><a href="https://okpt.life/" class="dropdown-toggle">好幫手</a></label>
    <label class="wall h-menu"><a href="https://www.okrent.tw/" class="dropdown-toggle">共享圈</a></label>
    <label class="wall h-menu <?php if ($_smarty_tpl->tpl_vars['page_name']->value == 'smart_lock') {?>now_here<?php }?>"><a href="/smart_lock" class="dropdown-toggle">租樂智能鎖</a></label>
    <label class="wall h-menu"><a href="https://www.lifegroup.house/LifeGo" class="dropdown-toggle">生活樂購</a></label>
    <label class="wall h-menu <?php if ($_smarty_tpl->tpl_vars['page_name']->value == 'store') {?>now_here<?php }?>"><a href="/Store" class="dropdown-toggle">生活好康</a></label>
    <label class="wall h-menu <?php if ($_smarty_tpl->tpl_vars['page_name']->value == 'okmaster_ExpertAdvisor') {?>now_here<?php }?>"><a href="/okmaster_ExpertAdvisor" class="dropdown-toggle">生活家族</a></label>
    <label class="h-menu <?php if ($_smarty_tpl->tpl_vars['page_name']->value == 'Aboutokmaster') {?>now_here<?php }?>"><a href="/Aboutokmaster" class="dropdown-toggle">關於我們</a></label>
  </div>
  <div class="logon">登入 | 註冊</div>
  <div class="share"><i class="fas fa-share-alt"></i></div>
  </div>
  <div class="hr_d">
    <div class="hr_light"></div>
    <img class="hr" src="/themes/Okmaster/img/page/hr.png">
  </div>
  <!-- <img class="hr" src="/themes/Okmaster/img/page/hr.png"> -->
</div>
<div class="head_group_zoon">
</div>
<?php echo $_smarty_tpl->tpl_vars['Breadcrumb']->value;?>

<style>

:root{
    --sbox:#c3d700;
    --a_color:#56a2ad;
}

.head_group {
position: fixed;
  top: 0px;
  z-index: 9;
  background: #ffffffd6;
  margin-top: 0px;
  width: 100%;
  left: 0px;
}
.head .hyperlink label {
    background: unset;
}
.head_group_zoon {
    height: 150px;
}
.head_group .hr {
  width: 100%;
}

.hr_d {
    position: relative;
}

.hr_light {
    position: absolute;
    height: 6px;
    width: 100%;
    top: 11px;
}

a.dropdown-toggle:hover {
    color: var(--a_color) !important;
    font-size: 18pt;
    transform: scale(2) !important;
    transition: color 0.5s,font-size 0.5s,text-shadow 0.3s linear;
    text-shadow: var(--sbox) 0.1em 0.1em 0.2em, var(--sbox) -0.1em -0.1em 0.2em, var(--sbox) -0.1em 0.1em 0.2em, var(--sbox) 0.1em -0.1em 0.2em;
}


a.dropdown-toggle::before {
    display: none !important;
}

/* .now_here a {
    background: #eee;
    font-size: 18pt;
    color: rgb(42 130 142) !important;
} */

label.wall.h-menu.now_here:before {
    background: rgb(230 245 195);
    width: -webkit-fill-available;
    content: " ";
    height: 440px;
    position: absolute;
    left: 0px;
    bottom: -15px;
    z-index: 0;
    /* margin-right: -10px; */
}

</style>
<?php echo '<script'; ?>
>
$(document).ready(function () {
  //hr_light_flash();
  wallhmenu(0)
  $(".h-menu").find("a").css("color","#fff")



 } )

function wallhmenu(i){
  var hmenu=$(".h-menu").eq(i).css("font-size");
  var hmenu_cl=$(".h-menu").eq(i).css("color");
  hmenu_cl="#3BB3C3";
  $(".h-menu").eq(i).find("a").css("color","#fff")
  $(".h-menu").eq(i).animate( {

  "font-size":24

},100,function() { $(this).find("a").animate( { "color":"#c3d700" },50,function() { $(this).css( "filter", "blur(5px)" );$(this).animate( { "color":"#5cb8c5" },50,function() { $(this).css( "filter", "blur(0px)" );$(this).css("color",hmenu_cl); } ) } );
$(this).animate( { "font-size":hmenu },300,function( ) {  } );if ( $(".h-menu").length > i+1 ) { wallhmenu(i+1) } else { hr_light_flash() }   } )


}

 function hr_light_flash(){

   bg=0;
   $(".hr_light").animate( {
       ba: '+=1'
     }, {
     duration:2000,
   step: function( now, fx ) {
   bg = bg + 1;
       // console.log(bg);
       $(this).css(
         // "background","linear-gradient(135deg, #00000000 "+(0+bg)+"%,#ffffff "+(14+bg)+"%,#ffffff "+(21+bg)+"%,#00000000 "+(33+bg)+"%,#00000000 "+(44+bg)+"%,#ffffff "+(51+bg)+"%,#00000000 "+(61+bg)+"%)"
         "background","linear-gradient(135deg, #00000000 "+(-61+bg)+"%,#ffffff "+(-47+bg)+"%,#ffffff "+(-40+bg)+"%,#00000000 "+(-28+bg)+"%,#00000000 "+(-17+bg)+"%,#ffffff "+(-10+bg)+"%,#00000000 "+(0+bg)+"%)"
       );
     }
     } );
     clearTimeout(hr_light_flash);
     setTimeout(hr_light_flash,3000);
 }
<?php echo '</script'; ?>
>
<?php }
}
