<?php
/* Smarty version 3.1.28, created on 2021-01-26 16:11:03
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/group_link.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_600fce97e03052_71404402',
  'file_dependency' => 
  array (
    '87a6de121733fa7ea991ae91d1e0e05a836304a4' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/group_link.tpl',
      1 => 1611647244,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../tpl/group_link.tpl' => 1,
  ),
),false)) {
function content_600fce97e03052_71404402 ($_smarty_tpl) {
?>
<section class="slider_center">
    <?php if (!empty($_smarty_tpl->tpl_vars['group_link_data']->value)) {?>
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../tpl/group_link.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php }?>
    <?php
$_from = $_smarty_tpl->tpl_vars['group_link_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
    <?php break 1;?>
    <div>
        <a href="<?php echo $_smarty_tpl->tpl_vars['v']->value['url'];?>
"><img alt="<?php echo $_smarty_tpl->tpl_vars['v']->value['txt'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['v']->value['img'];?>
"></a>
    </div>
    <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
</section>
<?php echo '<script'; ?>
>
    $(".<?php echo $_smarty_tpl->tpl_vars['slick_data']->value['class'];?>
").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        autoplay : true,
        slidesToShow: <?php echo $_smarty_tpl->tpl_vars['slick_data']->value['slidesToShow'];?>
,
        slidesToScroll: <?php echo $_smarty_tpl->tpl_vars['slick_data']->value['slidesToScroll'];?>
,
    });
<?php echo '</script'; ?>
>
<?php }
}
