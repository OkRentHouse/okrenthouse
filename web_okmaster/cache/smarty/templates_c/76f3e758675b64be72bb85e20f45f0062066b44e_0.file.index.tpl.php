<?php
/* Smarty version 3.1.28, created on 2021-04-02 19:25:54
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/SterilizationBuy/index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6066ff4221e6d2_75508819',
  'file_dependency' => 
  array (
    '76f3e758675b64be72bb85e20f45f0062066b44e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/SterilizationBuy/index.tpl',
      1 => 1617362752,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6066ff4221e6d2_75508819 ($_smarty_tpl) {
?>
<style>
.product_ID {
    position: absolute;
    right: 0px;
    top: 0px;
}

.main__top-right .info {
    position: relative;
}
.main__title * {
    vertical-align: sub;
    display: contents;
}
.radiobox li {
  margin: 0 .2em;
}

.radiobox li input[type="radio"] {
    margin: 0 .2em;
}

ul.dleveily img {
    width: 1.3em;
}


ul.info li {
    line-height: 28pt;
    margin-top: 1.2em;
    font-size: 12pt;
    list-style: none;
}

.shopping {
    margin-top: 2em !important;
}
.shopping > div {
    position: relative;
    margin-left: 0px;
}

.shopping .fa-plus {
  position: absolute;
  left: 0.6em;
  font-size: 21pt;
  border-right: 3px solid #66C3B7;
  border-radius: 0px;
  padding: .2em;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  width: 1.5em;
  top: 0px;
}

.shopping .fa-minus {
    position: absolute;
    right: 0.6em;
    font-size: 21pt;
    border-left: 3px solid #66C3B7;
    border-radius: 0px;
    padding: .2em;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    width: 1.5em;
    top: 0px;
}

.shopping svg {
    cursor: pointer;
    margin: 0px;
}

.shopping svg:hover, .shopping button:hover {
    background: #66C3B7;
    color: #fff;
}

.shopping * {
    border-radius: 10px;
    margin: 0 1em;
    height: 26pt;
}

.shopping input, .shopping button {
    border: 3px solid #66C3B7;
    background: #fff;
    padding: .3em;
    line-height: initial;
    width: 10em;
}

.shopping input {
    /* padding-left: 2.8em; */
    text-align: center;
}

.shopping input::placeholder {
    text-align: center;
}

.info-mainbox {
    margin-top: 3em;
}

.info-mainbox img {
    border-radius: 0px;
}

.info-mainbox ul, .info-mainbox ul li, .info-mainbox img {
    margin: 5em 0;
    list-style: none;
}

.d_left img {
    margin: 3em;
    width: 80%;
}

.d_right span {
    line-height: 3em;
    font-weight: bold;
}

.d_right {
    margin-top: 2em;
    width: 50%;
}

.d_right .fa-check {
    color: red;
    width: .5em;
    vertical-align: middle;
    margin: .1em;
    margin-left: -.7em;
}

.d_right .c99CF16{
  color: #99CF16;
}

.flex {
    padding: 0px;
    margin: 0px;
}



.t_container.main, .t_container.top, .t_container.wrap, .t_container.main {
    margin-top: 0px;
    margin: 0%;
}

.display_content {
    width: 100%;
}

</style>

<div class="main">

  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<div class="house_check_wrap">

  <div id="Web_1280__2" style="transform-origin: 0px 0px;">
    <?php echo $_smarty_tpl->tpl_vars['Breadcrumb']->value;?>


    <div class="t_container main">
        <div class="row">
            <?php if ($_smarty_tpl->tpl_vars['lifego_left']->value) {?>
                
            <?php }?>

            <div class="center-block col col-11 right t_col_10">
              <form method="post"><div class="main__top">
                <div class="main__top-left">
                  <img style="    border-radius: 30px;" src="/themes/LifeHouse/img/lifegoproduct/product_photo_1_040221.jpg">
                </div>
                <div class="main__top-right">
                  <ul class="info">
                    <li>
                      <span class="main__title"><?php echo $_smarty_tpl->tpl_vars['productTitle']->value;?>
<h6>系列</h6><span style="color:#66C3B7;font-size: 32px;"><?php echo $_smarty_tpl->tpl_vars['productInfo']->value;?>
</span></span>
                      <div class="product_ID"><?php echo $_smarty_tpl->tpl_vars['productID']->value;?>
</div>
                    </li>
                    <li><span class="main__slogan"><?php ob_start();
echo $_smarty_tpl->tpl_vars['productDescription']->value;
$_tmp1=ob_get_clean();
echo $_tmp1;?>
</span></li>
                    <li style="font-size:14px">使用方法<br/>將一錠超立淨放入裝滿 1 公升的 PET 材質噴瓶容器中，鎖緊噴頭，並靜置 3 分鐘後即成為可使用之消毒原液。將消毒原液直接噴灑至欲消毒之物品上來進行消毒，或以擦拭的方式來進行消毒。</li>
                    <li style="font-size:10px">每包15錠，每錠 1 克.</li>
                    <li style="font-size:8px">產品組合：超立淨 * 2</li>
                    <li style="font-size:8px">贈品：隨身霧化機 X 1 (市價 399)</li>
                    <li><span class="main__slogan">好康價 <span class="main__title" style="color:#FF6600"><?php echo $_smarty_tpl->tpl_vars['productPrice']->value;?>
</span>元/套<label style="    margin: 0 3em;"></label>紅利點數 <span class="main__title" style="color:#66C3B7"><?php echo $_smarty_tpl->tpl_vars['productBonus']->value;?>
</span>點</span>
                      <input name="pr_price" class="pr_price hidden" type="text" value="1000"></input>
                    </li>
                    
                    <?php $_smarty_tpl->tpl_vars['pay'] = new Smarty_Variable(array("付款","信用卡","貨到付款","ATM","超商","LINE Pay","Google Pay"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'pay', 0);?>
                    <li>
                    <ul class="flex pay radiobox"><?php
$_from = $_smarty_tpl->tpl_vars['pay']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_0_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_0_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                      <li><?php if ($_smarty_tpl->tpl_vars['key']->value > 0) {?><input type="radio" id="pay" name="pay" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php }
echo $_smarty_tpl->tpl_vars['value']->value;?>
</li>
                    <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_local_item;
}
}
if ($__foreach_value_0_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_item;
}
if ($__foreach_value_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_0_saved_key;
}
?></ul>
                    </li>
                    <?php $_smarty_tpl->tpl_vars['dleveily'] = new Smarty_Variable(array("配送","60元","60元","70元","70元","65元","65元"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'dleveily', 0);?>
                    <li>
                    <ul class="flex dleveily radiobox"><?php
$_from = $_smarty_tpl->tpl_vars['dleveily']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_1_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_1_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                      <li><?php if ($_smarty_tpl->tpl_vars['key']->value > 0) {?><input type="radio" id="dleveily" name="dleveily" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><img src="/themes/LifeHouse/img/lifegoproduct/dleveily_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
-07.svg"><?php }
echo $_smarty_tpl->tpl_vars['value']->value;?>
</li>
                    <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_local_item;
}
}
if ($__foreach_value_1_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_item;
}
if ($__foreach_value_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_1_saved_key;
}
?></ul>
                    </li>
                    <li class="flex media shopping">
                      數量
                      <div><input name="pr_q" class="pr_q" type="text" placeholder="0"></input>
                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-plus fa-w-12 fa-3x"><path fill="currentColor" d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z" class=""></path></svg>
                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="minus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-minus fa-w-12 fa-3x"><path fill="currentColor" d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z" class=""></path></svg>
                      </div>
                      <!-- <input type="button" value="放入購物車"></input> -->
                      <button type="submit" value="" onclick="if($('.pr_q').val()==0){ alert('數量不可以為 0');return false; }">放入購物車</button>
                    </li>
                  </ul>
                </div>

              </div></form>
              <div class="info-mainbox">
                <img src="/themes/LifeHouse/img/lifegoproduct/product_banner_1-03.svg">
                <ul>
                  <li>
                    <h1 style="
                        text-align: center;
                        color: #66C3B7;
                        font-weight: bold;
                    ">形成防護力 最給力</h1>
                    <h1 style="
                        text-align: center;
                        color: #66C3B7;
                        font-weight: bold;
                    ">WHO推薦A1級最安全的『第四代』環保滅菌消毒產品</h1>
                  </li>
                  <li>
                    <div style="    display: flex;">
                      <div style="
                                      width: 50%;
                                  " class="d_left">
                        <img src="/themes/LifeHouse/img/lifegoproduct/product_banner_img_1-04.svg">
                        <img src="/themes/LifeHouse/img/lifegoproduct/product_banner_img_2-05.svg">
                      </div>
                      <div class="d_right">
                        <div>
                          <span>
                            <?php $_smarty_tpl->tpl_vars['check'] = new Smarty_Variable("<svg aria-hidden='true' focusable='false' data-prefix='fal' data-icon='check' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512' class='svg-inline--fa fa-check fa-w-14 fa-2x'><path fill='currentColor' d='M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z' class=''></path></svg>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'check', 0);?>
                          <?php $_smarty_tpl->tpl_vars['txt'] = new Smarty_Variable(array("微量的二氧化氯就可以殺滅細菌、微生物,經由氧化反應,不會產生有毒的副產物;使用后之水劑是微量鹽份和水,所以不會對環境造成任何負擔或汙染。世界衛生組織(WHO)和世界糧食組織(FA0)都大力推薦","<label class='c99CF16'>三無</label>(無致癌、無致畸、無致突變性)","<label class='c99CF16'>三效</label>(全面、高效、快速)","<label class='c99CF16'>具殺菌、消毒、除臭、去黴、保鮮、除藻</label>等功效的環保消毒劑,又稱為「<label class='c99CF16'>綠色消毒劑</label>」。"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'txt', 0);?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['txt']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_2_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_2_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_2_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                            <?php if ($_smarty_tpl->tpl_vars['key']->value == 1 || $_smarty_tpl->tpl_vars['key']->value == 2) {?>
                            <?php echo $_smarty_tpl->tpl_vars['check']->value;?>

                            <?php }?>
                          <?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<br>
                            <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_local_item;
}
}
if ($__foreach_value_2_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_item;
}
if ($__foreach_value_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_2_saved_key;
}
?>
                            </span>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li></li>
                </ul>
                <img src="/themes/LifeHouse/img/lifegoproduct/product_banner_2-06.svg">
              </div>
              <label class="h4" name="check_pick" id="check_pick" style="color:red" >退換貨須知</label>
              <div class="row_4 border" name="check_pick1" id="check_pick1" >
                <ul>
                  <li>提醒您，申請退貨時，請務必將您所訂購之完整商品、配件、贈品及包裝置入於原外包裝箱內，以利後續退換貨作業。如您已收到發票，請依發票背面之銷退折讓單上說明，簽章後寄回生活好科技，以便儘速為您辦理退款。若您是以其他方式(電話/語音/型錄)訂購則恕不提供本項服務。請注意，退貨的商品必須為全新狀態且完整包裝(包含主機、附件、內外包裝、隨機文件、贈品等)。
                    此外，下列情形可能影響您的退貨權限：</li>
                  <li>(1)隨商品已附上相同之試用品，或在收到影音光碟及軟體前已提供您試聽、試用機會。</li>
                  <li>(2)在不影響您檢查商品情形下，您將商品包裝毀損、封條移除、吊牌拆除、貼膠移除或標籤拆除等情形。</li>
                  <li>(3)在您收到商品之前，已提供您檢查商品之機會。</li>
                  <li>(4)其他逾越檢查之必要或可歸責於您之事由，致商品有毀損、滅失或變更者。</li>
                </ul>
                <ul>
                  <li>退款方式：<br/>
(1)付款方式為貨到付款、超商取貨付款、IBON付款、ATM付款之訂單，確認退貨後將款項以支票方式掛號寄送訂購人。<br/>
(2)付款方式為信用卡者，確認退貨後將款項退至原信用卡帳戶中，如一筆訂單中包含多項商品，辦理部份商品退貨者，待退回物品確認回數將再向原發卡行申請支付剩餘款項，並進行全額款項刷退作業。</li>
                </ul>
              </div>
            </div>
        </div>

</div>


<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</div>
<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php echo $_smarty_tpl->tpl_vars['js_return']->value;?>

<?php }
}
