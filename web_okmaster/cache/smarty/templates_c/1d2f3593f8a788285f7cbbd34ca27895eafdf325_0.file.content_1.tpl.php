<?php
/* Smarty version 3.1.28, created on 2021-04-20 13:56:11
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_1.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_607e6cfb757286_91217528',
  'file_dependency' => 
  array (
    '1d2f3593f8a788285f7cbbd34ca27895eafdf325' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_1.tpl',
      1 => 1618898167,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_607e6cfb757286_91217528 ($_smarty_tpl) {
?>
<style>
.carousel {
  margin: auto 2rem;
}

</style>
<form action="?application_page=2" method="post">
  <div class="row form_row">
    <div class="cell-12">
      <div class="row form_title_row">
        <div class="cell-1"></div>
        <div class="cell-11 form_title"><span>店家資訊</span></div>
      </div>

      <?php $_smarty_tpl->tpl_vars['input'] = new Smarty_Variable(array("連鎖類別","商號登記","法定代理人","登記地址","總公司電話","經辦聯絡人","聯絡手機","請輸入驗證碼","Email"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'input', 0);?>
      <?php $_smarty_tpl->tpl_vars['input2'] = new Smarty_Variable(array("store_category","store_business_number","store_law_rep","store_regi_address","store_tel","touch_people","store_mobile",'',"store_email"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'input2', 0);?>
      <?php ob_start();
echo $_SESSION['store_category'];
$_tmp1=ob_get_clean();
ob_start();
echo $_SESSION['store_business_number'];
$_tmp2=ob_get_clean();
ob_start();
echo $_SESSION['store_law_rep'];
$_tmp3=ob_get_clean();
ob_start();
echo $_SESSION['store_regi_address'];
$_tmp4=ob_get_clean();
ob_start();
echo $_SESSION['store_tel'];
$_tmp5=ob_get_clean();
ob_start();
echo $_SESSION['touch_people'];
$_tmp6=ob_get_clean();
ob_start();
echo $_SESSION['store_mobile'];
$_tmp7=ob_get_clean();
ob_start();
echo $_SESSION['store_email'];
$_tmp8=ob_get_clean();
$_smarty_tpl->tpl_vars['input3'] = new Smarty_Variable(array($_tmp1,$_tmp2,$_tmp3,$_tmp4,$_tmp5,$_tmp6,$_tmp7,'',$_tmp8), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'input3', 0);?>
      <?php
$_from = $_smarty_tpl->tpl_vars['input']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_x_0_saved_item = isset($_smarty_tpl->tpl_vars['x']) ? $_smarty_tpl->tpl_vars['x'] : false;
$__foreach_x_0_saved_key = isset($_smarty_tpl->tpl_vars['n']) ? $_smarty_tpl->tpl_vars['n'] : false;
$_smarty_tpl->tpl_vars['x'] = new Smarty_Variable();
$__foreach_x_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_x_0_total) {
$_smarty_tpl->tpl_vars['n'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['n']->value => $_smarty_tpl->tpl_vars['x']->value) {
$__foreach_x_0_saved_local_item = $_smarty_tpl->tpl_vars['x'];
?>
      <?php if ($_smarty_tpl->tpl_vars['x']->value == "聯絡手機") {?>
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8">
                <div class="row" style="margin-left: -5px;">
                  <div class="cell-8 input"> <input id="user" data-role="none" class="floating_input" name="user" type="text"
                               placeholder="手機號碼" value="<?php echo $_POST['user'];?>
" minlength="6" maxlength="10" onkeyup="value=value.replace(/[^\d]/g,'') " required > </div>
                  <div class="cell-4" style="padding: 0 1rem;"><button type="button" id="tel_code_submit" class="btn ">取得驗證碼</button></div>
                </div>
              </div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "請輸入驗證碼") {?>
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8">
                <div class="row" style="margin-left: -5px;">
                  <div class="cell-8 input"> <input id="captcha" data-role="none" class="floating_input" name="captcha" type="text"
                               placeholder="手機號碼" value="<?php echo $_POST['user'];?>
" minlength="6" maxlength="10" onkeyup="value=value.replace(/[^\d]/g,'') " required > </div>
                  <div class="cell-4" style="padding: 0 1rem;"></div>
                </div>
              </div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "連鎖類別") {?>
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input">
              <select class="input" id="<?php echo $_smarty_tpl->tpl_vars['input2']->value[$_smarty_tpl->tpl_vars['n']->value];?>
" name="<?php echo $_smarty_tpl->tpl_vars['input2']->value[$_smarty_tpl->tpl_vars['n']->value];?>
" value="<?php echo $_smarty_tpl->tpl_vars['input3']->value[$_smarty_tpl->tpl_vars['n']->value];?>
">
                <?php if ($_smarty_tpl->tpl_vars['select_type']->value) {?>
                <?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                  <?php echo $_smarty_tpl->tpl_vars['v']->value['store_type'];?>

                </option>
                <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_1_saved_key;
}
?>
                <?php }?>
              </select></div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      <?php } else { ?>
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input"><input type="text" id="<?php echo $_smarty_tpl->tpl_vars['input2']->value[$_smarty_tpl->tpl_vars['n']->value];?>
" name="<?php echo $_smarty_tpl->tpl_vars['input2']->value[$_smarty_tpl->tpl_vars['n']->value];?>
" value="<?php echo $_smarty_tpl->tpl_vars['input3']->value[$_smarty_tpl->tpl_vars['n']->value];?>
"></div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      <?php }?>
      <?php
$_smarty_tpl->tpl_vars['x'] = $__foreach_x_0_saved_local_item;
}
}
if ($__foreach_x_0_saved_item) {
$_smarty_tpl->tpl_vars['x'] = $__foreach_x_0_saved_item;
}
if ($__foreach_x_0_saved_key) {
$_smarty_tpl->tpl_vars['n'] = $__foreach_x_0_saved_key;
}
?>


      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label>密碼</label></div>
            <div class="cell-8 input"><input type="text" id="password" name="password"></div>
            <div class="cell-4 title"></div>
            <div><span style="color:red">＊生活好康商店管理登入之用途</span></div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>


      <div class="row form_submit">
      <input type="hidden" id="t2" name="t2" value="2">
        <div class="cell-12"><button class="carousel"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg> <a href="?application_page=0&s=0" target="_self">Previous</a> </button>
          <button class="carousel" type="submit"> Next <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg></button></div>
      </div>
      <div class="row form_end">
        <div class="cell-12"></div>
      </div>
    </div>
  </div>
</form>
</div>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./controllers/StoreApplication/process.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
