<?php
/* Smarty version 3.1.28, created on 2021-03-08 16:17:42
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreIntroduction/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6045dda6949ec9_77734612',
  'file_dependency' => 
  array (
    'ac99fa97b172f96bd5dcd118fc23f6a330b0301c' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreIntroduction/content.tpl',
      1 => 1615191457,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6045dda6949ec9_77734612 ($_smarty_tpl) {
?>
<div class="main">
    <?php echo $_smarty_tpl->tpl_vars['Breadcrumb']->value;?>

    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="store_name">
    <div class="title"><?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
</div>
    <i></i>
    <div class="category">
        <p class="" href=""><?php echo $_smarty_tpl->tpl_vars['store']->value['store_type'];?>
</p>
    </div>
</div>
<div class="special_store_main_wrap">
    <hr>
    <br>
    <div class="row">
        <div class="col-sm-4 text-left">
            <div class="img_frame">
              <?php if (sizeof($_smarty_tpl->tpl_vars['store1']->value['img']) == 1) {?>
                <img src="<?php echo $_smarty_tpl->tpl_vars['store']->value['img']["1"];?>
">
              <?php } else { ?>
                    <div id="myCarousel" class="carousel slide">
                      <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                          <?php $_smarty_tpl->tpl_vars['store_i'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'store_i', 0);?>
                          <?php
$_from = $_smarty_tpl->tpl_vars['store1']->value['img'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_0_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_0_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['store_i']->value == 0) {?>class="active"<?php }?>></li>
                          <?php $_smarty_tpl->tpl_vars['store_i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['store_i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'store_i', 0);?>
                          <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_local_item;
}
}
if ($__foreach_value_0_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_item;
}
if ($__foreach_value_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_0_saved_key;
}
?>
                        </ol>
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                          <?php $_smarty_tpl->tpl_vars['store_i'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'store_i', 0);?>
                          <?php
$_from = $_smarty_tpl->tpl_vars['store1']->value['img'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_1_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_1_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                          <div class="item<?php if ($_smarty_tpl->tpl_vars['store_i']->value == 0) {?> active<?php }?>">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" alt="First slide">
                          </div>
                          <?php $_smarty_tpl->tpl_vars['store_i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['store_i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'store_i', 0);?>
                          <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_local_item;
}
}
if ($__foreach_value_1_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_item;
}
if ($__foreach_value_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_1_saved_key;
}
?>
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>
            <?php }?>


            </div>
        </div>
        <div class="col-sm-4  text-center">
            <div class="store_content">
                <!-- <div class="title"></div>
                <div class="career"></div> -->
                <div class="store_info">
                    <img src="/themes/Rent/img/storeintroduction/icon_star.svg" alt="" class="">
                    店家資訊
                </div>
                <div class="list add">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/map_tag.svg">
                        ADD
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['address1'];?>

                    </div>
                </div>

                <div class="list tel">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/phone.svg">
                        TEL
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['tel1'];?>

                        <?php if ($_smarty_tpl->tpl_vars['store']->value['tel2']) {?><br><?php echo $_smarty_tpl->tpl_vars['store']->value['tel2'];
}?>
                    </div>
                </div>

                <div class="list time">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/time.svg">
                        TIME
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['open_time_w'];?>

                    </div>
                </div>

                <div class="list hot">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/hot.svg">
                        HOT
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['service'];?>

                        <br><?php echo $_smarty_tpl->tpl_vars['store']->value['service'];?>

                    </div>
                </div>

                <div class="list offer">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/offer.svg">
                        OFFER
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['discount'];?>

                    </div>
                </div>
                <div class="popularity"><label><img class="icon" src="/themes/Rent/img/storeintroduction/star.svg">人氣星等</label><div class="div_popularity" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['popularity'];?>
</div></div>
                <?php if (!empty($_SESSION['id_member'])) {?>
                <div class="star"><label><img class="icon" src="/themes/Rent/img/storeintroduction/star.svg"></i>給星星</label><div class="div_star give_star" data-title="<?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
" data-star="<?php echo $_smarty_tpl->tpl_vars['store']->value['star'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['star_img'];?>
</div></div>
                <?php }?>




























            </div>
        </div>
        <div class="col-sm-4 text-right">
            <div class="img_frame">
              <?php if (sizeof($_smarty_tpl->tpl_vars['store2']->value['img']) == 1) {?>
                <img src="<?php echo $_smarty_tpl->tpl_vars['store']->value['img']["2"];?>
">
              <?php } else { ?>
              <div id="myCarousel" class="carousel slide">
                <!-- 轮播（Carousel）指标 -->
                  <ol class="carousel-indicators">
                    <?php $_smarty_tpl->tpl_vars['store_i'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'store_i', 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['store2']->value['img'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_2_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_2_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_2_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['store_i']->value == 1) {?>class="active"<?php }?>></li>
                    <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_local_item;
}
}
if ($__foreach_value_2_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_item;
}
if ($__foreach_value_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_2_saved_key;
}
?>
                  </ol>
                  <!-- 轮播（Carousel）项目 -->
                  <div class="carousel-inner">
                    <?php $_smarty_tpl->tpl_vars['store_i'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'store_i', 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['store2']->value['img'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_3_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_3_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_3_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_3_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                    <div class="item<?php if ($_smarty_tpl->tpl_vars['store_i']->value == 0) {?> active<?php }?>">
                      <img src="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" alt="First slide <?php echo $_smarty_tpl->tpl_vars['key']->value;?>
">
                    </div>
                    <?php $_smarty_tpl->tpl_vars['store_i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['store_i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'store_i', 0);?>
                    <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_3_saved_local_item;
}
}
if ($__foreach_value_3_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_3_saved_item;
}
if ($__foreach_value_3_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_3_saved_key;
}
?>
                  </div>
                  <!-- 轮播（Carousel）导航 -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                  </a>
              </div>
              <?php }?>

                <!-- <img src="./生活好康-特約商店_files/default.jpg" alt="" class=""> -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center">
            <div class="main_img img_frame">
                <img src="<?php echo $_smarty_tpl->tpl_vars['store']->value['img']["0"];?>
">

            </div>
        </div>
    </div>

    <div class="introduction_wrap">
        <div class="caption">
            <img src="/themes/Rent/img/storeintroduction/icon_star.svg" alt="" class="">
            店家介紹
        </div>
       <?php echo $_smarty_tpl->tpl_vars['store']->value['introduction'];?>

    </div>
</div>
</div>
<?php }
}
