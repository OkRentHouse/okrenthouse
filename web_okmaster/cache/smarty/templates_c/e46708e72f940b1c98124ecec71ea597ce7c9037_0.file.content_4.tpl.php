<?php
/* Smarty version 3.1.28, created on 2021-04-20 11:23:14
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_4.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_607e49226f7ae8_27338755',
  'file_dependency' => 
  array (
    'e46708e72f940b1c98124ecec71ea597ce7c9037' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_4.tpl',
      1 => 1618888982,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_607e49226f7ae8_27338755 ($_smarty_tpl) {
?>

<style>

.carousel {
    background: #79ccf3;
    width: 40%;
}

.carousel:hover {
    background: #fed23d;
}

.form_footer {
    display: none;
}

</style>

  <div class="row form_row">
    <div class="cell-12">


      <div class="row form_body">
        <div class="cell-0"></div>
        <div class="cell-12 agree">
          <img style="width: 100%;" src="\themes\Okmaster\img\storeapplication\page_4.jpg">
        </div>
        <div class="cell-0"></div>
      </div>

      <div class="row form_submit">
        <div class="cell-12">
          <button class="carousel">
            <a href="https://okrent.house/Store?" target="_self">回首頁</a> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="home-lg-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-home-lg-alt fa-w-18 fa-3x"><path fill="currentColor" d="M288 115L69.47 307.71c-1.62 1.46-3.69 2.14-5.47 3.35V496a16 16 0 0 0 16 16h128a16 16 0 0 0 16-16V368a16 16 0 0 1 16-16h96a16 16 0 0 1 16 16v128a16 16 0 0 0 16 16h128a16 16 0 0 0 16-16V311.1c-1.7-1.16-3.72-1.82-5.26-3.2zm282.69 121.28l-255.94-226a39.85 39.85 0 0 0-53.45 0l-256 226a16 16 0 0 0-1.21 22.6L25.5 282.7a16 16 0 0 0 22.6 1.21L277.42 81.63a16 16 0 0 1 21.17 0L527.91 283.9a16 16 0 0 0 22.6-1.21l21.4-23.82a16 16 0 0 0-1.22-22.59z" class=""></path></svg></button></div>
      </div>
      <div class="row form_end">
        <div class="cell-12"></div>
      </div>
    </div>
  </div>

</div>

    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./controllers/StoreApplication/process.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
