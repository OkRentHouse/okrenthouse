<?php
/* Smarty version 3.1.28, created on 2021-04-16 15:17:22
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_2.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60793a02bf2817_51930571',
  'file_dependency' => 
  array (
    '17ce203863bed96ebb68b6cab01af7a4ecea19d6' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_2.tpl',
      1 => 1618557439,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60793a02bf2817_51930571 ($_smarty_tpl) {
echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"><?php echo '</script'; ?>
>
<style>
.carousel {
  margin: auto 2rem;
}
.image_container{
  height:120px;
  width:200px;
  border-radius:6px;
  overflow:hidden;
}
.image_container img{
  height:100px;
  width:auto;
  object-fit:cover;
}
.image_container span{
  top:-6px;
  right:8px;
  color:red;
  font-size:28px;
  font-weight:normal;
  cursor:pointer;
}
</style>
<?php echo '<script'; ?>
>

function displayImage(e){
  if(e.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#logodisplay').setAttribute('src',e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < 3; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
      $(".gallery").hide();
        imagesPreview(this, 'div.gallery_1');
        
    });
    
});

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < 3; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add1').on('change', function() {
      $(".gallery1").hide();
        imagesPreview(this, 'div.gallery_2');
    });
});




<?php echo '</script'; ?>
>
<form action="?application_page=3" method="post" id="form" name="form" enctype="multipart/form-data">
  <div class="row form_row">
    <div class="cell-12">
      <div class="row form_title_row">
        <div class="cell-1"></div>
        <div class="cell-11 form_title"><span>檔案上傳</span></div>
      </div>

      <?php $_smarty_tpl->tpl_vars['input'] = new Smarty_Variable(array("LOGO<span>(檔案大小5MB內Jpeg/heic/png)<span>","店家照片<span>(檔案大小5MB內Jpeg/heic/png)</span>","服務/產品介紹<span>(檔案大小5MB內Jpeg/heic/png)</span>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'input', 0);?>
      <?php $_smarty_tpl->tpl_vars['qty'] = new Smarty_Variable(array(1,3,3), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'qty', 0);?>
      
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-2 title"><label>LOGO</label></div>
            <div class="cell-10 photo_upload">
              
              <div>
                <img class="view" id="logodisplay" onclick="triggerClick()" name="logodisplay" width="182" height="182" src="/themes/Okmaster/img/image_default.png">
                <input type="file" id="uploadedfile" name="uploadedfile" onchange="displayImage(this)" accept="image/*" required>
              </div>
              
            </div>
          </div>
          <div class="row">
            <div class="cell-2 title"><label>店家照片(最多三張圖片)</label></div>
            <div class="cell-10 photo_upload">
              
              <div>
                <div class="gallery"><img width="182" height="182" src="/themes/Okmaster/img/image_default.png"></div> 
                <input type="file" id="gallery-photo-add" name="files[]" multiple  accept="image/*" required>
                
<div class="gallery_1"></div>
              </div>
              
              
              
            </div>
          </div>
          <div class="row">
            <div class="cell-2 title"><label>服務/產品介紹(最多三張圖片)</label></div>
            <div class="cell-10 photo_upload">
            <div>
                <div class="gallery1"><img width="182" height="182" src="/themes/Okmaster/img/image_default.png"></div>
                <input type="file" id="gallery-photo-add1" name="files1[]" multiple  accept="image/*" required>
                
<div class="gallery_2"></div>

              </div>
              
              

              
              
            </div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      
      <div class="row form_submit">
      <input type="hidden" id="t3" name="t3" value="3">
        <div class="cell-12"><button class="carousel"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg> <a href="?application_page=1&s=0" target="_self">Previous</a> </button>
          <button class="carousel" id="but_upload" name="but_upload">Next <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg></button></div>
      </div>
      <div class="row form_end">
        <div class="cell-12"></div>
      </div>
    </div>
  </div>

</div>
</form>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./controllers/StoreApplication/process.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
