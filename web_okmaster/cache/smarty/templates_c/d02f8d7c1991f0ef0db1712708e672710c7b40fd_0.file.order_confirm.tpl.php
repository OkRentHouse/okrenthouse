<?php
/* Smarty version 3.1.28, created on 2021-03-09 10:41:59
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/SterilizationBuy/order_confirm.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6046e0770657f7_71113120',
  'file_dependency' => 
  array (
    'd02f8d7c1991f0ef0db1712708e672710c7b40fd' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/SterilizationBuy/order_confirm.tpl',
      1 => 1615257693,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046e0770657f7_71113120 ($_smarty_tpl) {
?>
<div class="main">

  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<div class="house_check_wrap">

  <div id="Web_1280__2" style="transform-origin: 0px 0px;">
    <?php echo $_smarty_tpl->tpl_vars['Breadcrumb']->value;?>

    <br/><br/>
<table class="ordercheck" border="0">
  <tr>
    <td align="left"><img src="/themes/LifeHouse/img/lifegoproduct/super_logo.jpg" width="50" height="125"></td>
  </tr>
  <tr>
    <td align="center"><img src="/themes/LifeHouse/img/lifegoproduct/order_check.jpg" width="121" height="125"><br/>已經收到您的訂單<br/><span style="color:red;">您的訂單編號：<?php echo $_smarty_tpl->tpl_vars['orderID2']->value;?>
</span>
    </td>
  </tr>
  <?php if ($_smarty_tpl->tpl_vars['payway_check']->value == "3") {?>
  <tr><td align="center" style="color:red;">
  <br/>ATM 匯款資訊：<br/>
  銀行:台灣中小企銀 (050)<br/>
  匯款帳號：<?php echo $_smarty_tpl->tpl_vars['orderID2']->value;?>
<br/><br/>
  </td></tr>
  <?php }?>
  <tr>
    <td>誠摯感謝您的惠顧：<br/>
1.	商品訂單確認明細，已發送至您的手機，請查看簡訊內容。<br/>
2.	商品將於3日內出貨，若有訂單相關問題，請撥打免付費客服專線：0800-010-568。<br/>
3.	二氧化氯為消耗型產品，故本產品售出既不退貨，若有產品問題請撥打客服專線。<br/>
4.	【詐騙猖獗，小心受騙】本店絕對不會以交易失敗或付款錯誤等理由，讓您至ATM操作任意行為，請小心不要受騙。<br/>
</td>
  </tr>
  <tr><td><a href="<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
">回首頁</td></tr>
</table>
</div>



</div<?php }
}
