<?php
/* Smarty version 3.1.28, created on 2020-12-11 17:32:31
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd33caf365781_38806396',
  'file_dependency' => 
  array (
    '099f0665cf81dde67cd6ab8478627ed0e4c1bc31' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/msg_box.tpl',
      1 => 1607678486,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd33caf365781_38806396 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
<div class="alert alert-success <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
<div class="alert alert-danger <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
}
}
