<?php
/* Smarty version 3.1.28, created on 2021-04-20 11:41:30
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_0.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_607e4d6acdbf99_79338595',
  'file_dependency' => 
  array (
    '4b45ccc5123273f8ce3b9d06faeebd1ac235b5be' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/StoreApplication/content_0.tpl',
      1 => 1618890089,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_607e4d6acdbf99_79338595 ($_smarty_tpl) {
?>
<form action="?application_page=1" method="post">
  <div class="row form_row">
    <div class="cell-12">
      <div class="row form_title_row">
        <div class="cell-1"></div>
        <div class="cell-11 form_title"><span>生活好康特約商店註冊申請書</span></div>
      </div>
      
      <?php $_smarty_tpl->tpl_vars['input'] = new Smarty_Variable(array("店家名稱","店址","電話","營業時間","店休日","商品服務","好康優惠","店家介紹","官網"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'input', 0);?>
      <?php
$_from = $_smarty_tpl->tpl_vars['input']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_x_0_saved_item = isset($_smarty_tpl->tpl_vars['x']) ? $_smarty_tpl->tpl_vars['x'] : false;
$__foreach_x_0_saved_key = isset($_smarty_tpl->tpl_vars['n']) ? $_smarty_tpl->tpl_vars['n'] : false;
$_smarty_tpl->tpl_vars['x'] = new Smarty_Variable();
$__foreach_x_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_x_0_total) {
$_smarty_tpl->tpl_vars['n'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['n']->value => $_smarty_tpl->tpl_vars['x']->value) {
$__foreach_x_0_saved_local_item = $_smarty_tpl->tpl_vars['x'];
?>
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <?php if ($_smarty_tpl->tpl_vars['x']->value == "店家名稱") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input"><input type="text" value="<?php echo $_SESSION['store_name_new'];?>
" id="store_name_new" name="store_name_new"  required ></div>
          </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "店址") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input_list">
            <input type="text" id="store_address_postcode" name="store_address_postcode" maxlength="6" value="<?php echo $_SESSION['store_address_postcode'];?>
" placeholder="郵遞區號" onkeyup="value=value.replace(/[^\d]/g,'')">
            <input type="text" id="store_address_city" name="store_address_city" value="<?php echo $_SESSION['store_address_city'];?>
" placeholder="縣市">
            <input type="text" id="store_address_sub" name="store_address_sub" value="<?php echo $_SESSION['store_address_sub'];?>
" placeholder="鄉鎮市區"></div>
          </div>
          <div class="row">
            <div class="cell-4 title"></div>
            <div class="cell-8 input"><input type="text" id="store_address_details" name="store_address_details" value="<?php echo $_SESSION['store_address_details'];?>
" placeholder="詳細地址"></div>
          </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "電話") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input"><input type="text" id="store_phone" name="store_phone" maxlength="10" value="<?php echo $_SESSION['store_phone'];?>
" placeholder="手機號碼或市話" onkeyup="value=value.replace(/[^\d]/g,'')">
              
            </div>
          </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "營業時間") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input_list"><input type="time" id="store_open_time" name="store_open_time" value="<?php echo $_SESSION['store_open_time'];?>
"> <svg style="width: 1rem;" aria-hidden="true" focusable="false" data-prefix="far" data-icon="wave-sine" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-wave-sine fa-w-20 fa-3x"><path fill="currentColor" d="M628.41 261.07L613 256.63a15.88 15.88 0 0 0-19.55 10.16C572.85 329.76 511.64 432 464 432c-52.09 0-87.41-93.77-121.53-184.45C302.56 141.58 261.31 32 176 32 87.15 32 17.77 178.46.78 230.69a16 16 0 0 0 10.81 20.23L27 255.36a15.87 15.87 0 0 0 19.55-10.15C67.15 182.24 128.36 80 176 80c52.09 0 87.41 93.77 121.53 184.45C337.44 370.42 378.69 480 464 480c88.85 0 158.23-146.46 175.22-198.7a16 16 0 0 0-10.81-20.23z" class=""></path></svg>
              <input type="time" id="store_close_time" name="store_close_time" value="<?php echo $_SESSION['store_close_time'];?>
"></div>
          </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "店休日") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input"><input type="text" id="store_closed" name="store_closed" value="<?php echo $_SESSION['store_closed'];?>
"></div>
          </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "商品服務") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input"><input type="text" id="store_product" name="store_product" value="<?php echo $_SESSION['store_product'];?>
"></div>
            <div class="cell-8" style="margin-left: 33.7%;padding: 0px;margin-bottom: 10px;margin-top: -6px;"><span style="color:#c50606;">範例:超立淨環境消毒服務,營業場所殺菌,二氧化氯滅菌產品</span></div>
          </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "好康優惠") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input"><input type="text" id="store_coupons" name="store_coupons" placeholder="持生活好康電子卡可享：" value="<?php echo $_SESSION['store_coupons'];?>
"></div>
          </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['x']->value == "店家介紹") {?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input textarea"><textarea id="store_details" name="store_details" value=""><?php echo $_SESSION['store_details'];?>
</textarea>
            </div>
          </div>
          <?php } else { ?>
          <div class="row">
            <div class="cell-4 title"><label><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</label></div>
            <div class="cell-8 input"><input type="text" id="store_website" name="store_website" value="<?php echo $_SESSION['store_website'];?>
"></div>
          </div>
          <?php }?>
        </span></div>
        <div class="cell-3"></div>
      </div>
      <?php
$_smarty_tpl->tpl_vars['x'] = $__foreach_x_0_saved_local_item;
}
}
if ($__foreach_x_0_saved_item) {
$_smarty_tpl->tpl_vars['x'] = $__foreach_x_0_saved_item;
}
if ($__foreach_x_0_saved_key) {
$_smarty_tpl->tpl_vars['n'] = $__foreach_x_0_saved_key;
}
?>
      <div class="row form_submit">
        <div class="cell-12">
        <input type="hidden" id="t1" name="t1" value="1">
        <button class="carousel" type="submit"> Next <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg></button>
        </div>
      </div>
      <div class="row form_end">
        <div class="cell-12"></div>
      </div>
      
    </div>
    
  </div>

</div>
</form>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./controllers/StoreApplication/process.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
