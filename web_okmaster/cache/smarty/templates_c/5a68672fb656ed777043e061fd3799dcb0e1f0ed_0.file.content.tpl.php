<?php
/* Smarty version 3.1.28, created on 2020-12-11 14:47:48
  from "/home/ilifehou/life-house.com.tw/themes/Okmaster/controllers/State/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd31614d62521_24153896',
  'file_dependency' => 
  array (
    '5a68672fb656ed777043e061fd3799dcb0e1f0ed' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Okmaster/controllers/State/content.tpl',
      1 => 1607669266,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd31614d62521_24153896 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['css']->value;?>

<div class="main">
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <div class="row">
    <p>&nbsp</p>
    <h3 style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-family: 微軟正黑體, cwTeXYen, sans-serif; color: #333333;"><span style="font-size: 18pt;"><strong>隱私權政策</strong></span></h3>
<p style="padding-left: 40px;">&nbsp;</p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">生活好科技有限公司(下稱「我們」)承諾保護及尊重您的個人隱私。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">我們的營運活動均遵循法規要求，來保護會員的隱私及個人資料安全。我們所訂定的隱私權政策(下稱「本政策」)將讓您了解，我們可能蒐集之個人資料、如何利用與保護。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">您可自行決定是否登錄、訂購商品、請求產品支援服務；當您選擇使用本服務時，我們將會要求您提供特定資訊，使我們可以提供服務。若您將個人資料提供予我們，將被視為您接受本政策以及隨時更新後的內容。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">蒐集的個人資料之範圍與使用目的</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;">&nbsp;</p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;"><strong>個人資料蒐集項目</strong>：</span></p>
<p style="padding-left: 40px;"><strong><span style="font-size: 18.6667px;">您的姓名、郵寄地址(包含郵遞區號)、電子郵件地址、電話及其他可識別您的身份的「個人資料」</span></strong><span style="font-family: 微軟正黑體, cwTeXYen, sans-serif; font-size: 18.6667px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400;">。</span></p>
<p style="padding-left: 40px;">&nbsp;</p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;"><strong>利用目的</strong>：</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">(1)用於寄送該行銷資料，當您訂閱我們的商品及其他特別優惠之行銷資料時；</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">(2)用於完成付款及產品運送等契約義務或其他優惠，當您完成在我們網站的產品訂購時；</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">(3)用於確認您的參加資格、告知活動動態或連繫贈獎事宜，當您參加我們舉辦的活動時；</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">(4)提供更符合您個人需求的服務、回覆您的提問或資訊查詢、滿意度調查及獲取意見回饋與使用者經驗調查及分析。</span></p>
<p style="padding-left: 40px;">&nbsp;</p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;"><strong>承諾事項</strong>：</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">(1)在上述之特定使用目的範圍內，我們可能將您所提供的部分個人資料，告知我們的供應商或協力廠商，且我們會確保該供應商或協力廠商嚴格遵守本政策。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">(2)除為配合法令、主管機關或司法單位之要求、或為保全法律上的請求、抗辯或為防止詐騙或其他不法行為所必要以外，我們不會在與蒐集目的無關的範圍內，使用或移轉您的個人資料予第三人。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">(3)我們不會要求您提供具有敏感性的個人資料，譬如您的政治、宗教信仰、醫療、健康紀錄、性取向、性生活、犯罪紀錄或基因等。也請您不要主動提供這類的訊息給我們。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><strong><span style="font-size: 18.6667px;">Cookies的使用</span></strong></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">我們使用「Cookies」(「用於辨識用戶資訊且儲存於您電腦的程式」)儲存您使用本服務網頁的習慣及偏好，讓您不用耗費時間重新輸入相同資訊，使您快速進入我們的網站來閱讀或是選購適合您的商品。若您將您的網頁瀏覽器設定為拒絕使用Cookies時，您可能因此無法使用我們網站的完整功能。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><strong><span style="font-size: 18.6667px;">第三人網站及服務</span></strong></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">我們的服務可能會提供他人網站之連結，或使用第三人提供之服務（包括但不限於Google），第三人若有蒐集個人資料的行為概與我們無涉。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">若您對您所提供給我們的個人資料有其他需求，歡迎利用本政策最後提供之電子郵件與我們聯繫。</span></p>
<p>&nbsp;</p>
<h3 style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-family: 微軟正黑體, cwTeXYen, sans-serif; color: #333333;"><strong>服務條款(含智權條款)</strong></h3>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;">&nbsp;</p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">由生活好科技有限公司(下稱「我們」)所提供之產品及服務（下稱本服務），都適用本使用條款。本服務包括但不限於我們或我們的關聯企業所屬品牌之產品、網站、資訊等。&nbsp; &nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">1. 您了解我們的產品與服務，只能在我們所指定的平台上進行運作。我們會因應情況隨時修改本服務、停止本服務所舉辦之活動，且所有修改將於本條款刊登後生效，而不另行對您做特別的通知。若您在我們修改服務條款後仍持續使用本服務，將被視為您同意接受所有修改後之條款。若不同意遵守本條款及相關法令的約束，還請您應立即停止使用本服務。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">2.透過我們網站所獲悉或購買的商品，您應依該商品的製造商所提供的使用說明規定使用。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">3.請您遵守以下事項：</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">(a)禁止任何使用侵入或損害網路系統或資源之行為；(b) 禁止散布電腦病毒；(c)禁止使用自動機器人軟體或任何不正當的方式或設備，在我們的網站內謀取利益；(d)禁止上傳任何脅迫性、猥褻性或違害他人利益或公共秩序之資訊。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">4. 智慧財產權</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">(a)您所上傳、輸入或提供至我們網站的各種資料，包括但不限於文字、軟體、音樂、音訊、照片、圖形、視訊、信息、資訊、資料、或其他資料，您應確認有權為前述行為，若有任何相關智權爭議均由上傳之人自負責任。此外，您於本網站所表達的任何資訊，並不代表這是我們的意見。</span><span style="font-size: 18.6667px;">(b)經您上載、傳送、輸入或提供各式資訊至我們的網站時，視為您同意並授權我們可以不限使用方式(包括但不限於修改、重製、公開播送、公開傳輸、改作、散布、發行、公開發表、或其他方式)使用該資料且無償地，於世界各地以任何形式，在任何媒體平台或管道發佈。同時我們有權將前述權利轉授權予任何第三人。</span><span style="font-size: 18.6667px;">(c)若知悉您所提供的資訊有侵權疑慮時，我們有權直接將相關資訊移除之權或其他處置方法。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">5. 註冊與密碼</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">若遇有須註冊成為本服務會員方得使用之服務時，請您提供真實、正確的個人資料完成註冊，您也能隨時進行更新、修改。會員應自行保管帳號密碼等任何關於帳密的機密性。若我們知悉您的帳密被盜用時，我們會立即暫停或終止該會員的密碼、帳號的使用，以確保您的資訊安全。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">&nbsp;</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">6. 免責聲明與責任限制</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">在法律允許的範圍內，我們不提供任何明示或默示的擔保及保證，包括但不限於商業適銷性、特定目的之適用性、未侵害任何他人權利及任何得使用本服務或無法使用本服務的保證。</span></p>
<p>&nbsp;</p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;">&nbsp;</p>


  </div>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</div>
<?php }
}
