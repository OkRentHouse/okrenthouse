<?php
/* Smarty version 3.1.28, created on 2020-12-08 20:32:06
  from "/home/ilifehou/life-house.com.tw/themes/Okmaster/cms.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fcf7246c5e555_66222237',
  'file_dependency' => 
  array (
    'af7d14e9fe34b18d0933c88d5e58d70f282caf62' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Okmaster/cms.tpl',
      1 => 1607430725,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fcf7246c5e555_66222237 ($_smarty_tpl) {
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="cms<?php if ($_smarty_tpl->tpl_vars['display_header']->value) {?> cms_header<?php }
if ($_smarty_tpl->tpl_vars['display_footer']->value) {?> cms_foote<?php }?>">
	<?php if ($_smarty_tpl->tpl_vars['html']->value) {
echo $_smarty_tpl->tpl_vars['html']->value;
} else {
echo $_smarty_tpl->tpl_vars['html_0']->value;
if ($_smarty_tpl->tpl_vars['introduce_tpl']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, $_smarty_tpl->tpl_vars['introduce_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}
echo $_smarty_tpl->tpl_vars['html_1']->value;
}?>

<?php if (!empty($_smarty_tpl->tpl_vars['id_cms']->value)) {?>
	<?php echo $_smarty_tpl->tpl_vars['model_html']->value;?>

<?php }?>
</div>
<?php if (!empty($_smarty_tpl->tpl_vars['cms_css']->value)) {?>
	<style>
		<?php echo $_smarty_tpl->tpl_vars['cms_css']->value;?>

	</style>
<?php }
echo '<script'; ?>
>
<?php if (!empty($_smarty_tpl->tpl_vars['id_cms']->value)) {?>
	<?php echo $_smarty_tpl->tpl_vars['model_js']->value;?>

<?php }
if (!empty($_smarty_tpl->tpl_vars['cms_js']->value)) {?>
		<?php echo $_smarty_tpl->tpl_vars['cms_js']->value;?>

<?php }
echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
