<?php
/* Smarty version 3.1.28, created on 2021-01-18 13:36:57
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/Aboutokmaster/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60051e790d76a1_86271069',
  'file_dependency' => 
  array (
    'aee8990617335ce9d369e2a4e69db3c8eac61952' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/Aboutokmaster/content.tpl',
      1 => 1610948212,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60051e790d76a1_86271069 ($_smarty_tpl) {
?>

<div class="main">

  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<!-- <ol class="breadcrumb">
<li class="breadcrumb-item"><a href="https://okrent.house/">首頁</a></li>
<li class="breadcrumb-item active"><a href="../tax_benefit">租事順利</a></li>
<li class="breadcrumb-item active"><a>健康好宅</a></li> -->

<!-- 健康宅檢測框架 -->
<div class="house_check_wrap">

  <div id="Web_1280__2" style="transform-origin: 0px 0px;">
    <?php echo $_smarty_tpl->tpl_vars['Breadcrumb']->value;?>

    <svg class="certificateTech">
  		<rect id="certificateTech" rx="0" ry="0" x="0" y="0" width="1801" height="860">
  		</rect>
  	</svg>

	<img id="Rectangle_1888" src="\themes\Okmaster\img\aboutokmaster\Rectangle_1888.png" srcset="\themes\Okmaster\img\aboutokmaster\Rectangle_1888.png 1x, \themes\Okmaster\img\aboutokmaster\Rectangle_1888@2x.png 2x">

	<img id="Rectangle_1884" src="\themes\Okmaster\img\aboutokmaster\Rectangle_1884.png" srcset="\themes\Okmaster\img\aboutokmaster\Rectangle_1884.png 1x, \themes\Okmaster\img\aboutokmaster\Rectangle_1884@2x.png 2x">

  <div id="Group_1237-1">

	</div>

	<div id="Group_1237">
		<div id="__">
			<span>生活好科技 ‧ 科技好生活</span>
		</div>
		<div id="__title" style="">
			<span>企業理念</span>
		</div>
	</div>
  <div id="Group_1236-1">

	</div>
	<div id="Group_1236">
		<div id="_bb">
			<span>願景展望</span>
		</div>
		<div id="_bc">
			<span>藉由科技優化生活的質量，帶來幸福感受。</span>
		</div>
	</div>

  <div class="dialogbox">
  <form action="/Aboutokmaster" enctype="multipart/form-data" method="post" id="form_work">
    <input type="hidden" name="put" value="1">
    <div class="title">合作提案</div>
    <div class="title_2">歡迎分享您的合作構想,留下您的聯絡方式及提案,讓我們能夠進一步討論!</div>
    <div class="border wid80"><div>　提案人<label></label><input type="text" name="name" maxlength="32"></div><div>性別<label></label><input type="text" name="sex" maxlength="20"></div></div>
    <div class="border wid80"><div>公司單位<label></label><input type="text" name="company" maxlength="32"></div><div>職稱<label></label><input type="text" name="job" maxlength="32"></div></div>
    <div class="border wid80"><div>公司電話<label></label><input type="text" name="company_phone" maxlength="20"></div><div>分機<label></label><input type="text" name="extension" maxlength="20"></div></div>
    <div class="border wid80"><div>　　手機<label></label><input type="text" name="phone" maxlength="20"></div><div>電郵<label></label><input type="text" name="email" maxlength="128"></div></div>
    <div class="border txt_area wid80"><textarea name="content"></textarea></div>


		  <div class="wid80 attach"><input type="file" name="data" class="fileupload"><i class="fas fa-paperclip"></i><a class="upload">檔案附件上傳</a></div>
    <div class="SMS"><div id="put" class="but_1" onclick="submit(this);" style="display:none">送出</div><div class="but_1" id="get_ver">
      <button type="button"  onclick="MessageVerification()" style="background: unset;">進行驗證</button></div><div class="rec_msm"><input name="verification" type="text" value="" placeholder="驗證碼"></div></div>
    <i id="cancel" class="far fa-times-circle"></i>
  </form>
  </div>


	<!-- <div id="Component_22__5" class="Component_22___5">
		<svg class="Rectangle_847">
			<rect id="Rectangle_847" rx="23.5" ry="23.5" x="0" y="0" width="194" height="47">
			</rect>
		</svg>
		<div id="_cb">
			<span>送出</span>
		</div>
	</div> -->
	<div id="_cc">
		<span>許可證照</span>
	</div>
	<div id="Group_1304">
		<img id="_cd" src="\themes\Okmaster\img\aboutokmaster\_cd.png" srcset="\themes\Okmaster\img\aboutokmaster\_cd.png 1x, \themes\Okmaster\img\aboutokmaster\_cd@2x.png 2x">

		<img id="_ce" src="\themes\Okmaster\img\aboutokmaster\_ce.png" srcset="\themes\Okmaster\img\aboutokmaster\_ce.png 1x, \themes\Okmaster\img\aboutokmaster\_ce@2x.png 2x">

	</div>
	<img id="puzzle2" src="\themes\Okmaster\img\aboutokmaster\Group 1388.png" srcset="\themes\Okmaster\img\aboutokmaster\Group 1388.png 1x, \themes\Okmaster\img\aboutokmaster\Group 1388.png 2x">
  <!-- <svg class="Path_20816" viewBox="-27 -86 346 332">
		<path id="Path_20816" d="M 11 -86 L 319 -86 L 273 246 L -27 246 L 11 -86 Z">
		</path>
	</svg> -->
	<!-- <div id="_cf">
		<span>核心價值</span>
	</div>
	<div id="_cg">
		<span>誠信</span>
	</div>
	<div id="_ch">
		<span>專業</span>
	</div>
	<div id="_ci">
		<span>創新</span>
	</div>
	<div id="_cj">
		<span>服務</span>
	</div> -->
	<div id="Group_1316">
		<svg class="Rectangle_852_cm">
			<linearGradient id="Rectangle_852_cm" spreadMethod="pad" x1="0.5" x2="0.5" y1="0.385" y2="1">
				<stop offset="0" stop-color="#0f6387" stop-opacity="1"></stop>
				<stop offset="1" stop-color="#083244" stop-opacity="1"></stop>
			</linearGradient>
			<rect id="Rectangle_852_cm" rx="0" ry="0" x="0" y="0" width="1281" height="692">
			</rect>
		</svg>
		<div id="Map">
			<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.3820905847438!2d121.29609951395032!3d25.021104083978383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34681fb738d97c5b%3A0xf6a4b6cfb3b1cdcd!2zMzMw5qGD5ZyS5biC5qGD5ZyS5Y2A5paw5Z-U5YWr6KGXMTEz6Jmf!5e0!3m2!1szh-TW!2stw!4v1605703092732!5m2!1szh-TW!2stw" width="387px" height="390.204px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.8453689594771!2d121.29772647869879!3d25.021124956479902!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34681fd1ee0370d9%3A0xcc282c80f830998a!2z55Sf5rS75aW956eR5oqA5pyJ6ZmQ5YWs5Y-4!5e0!3m2!1szh-TW!2stw!4v1610941055717!5m2!1szh-TW!2stw" width="387px" height="390.204px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		</div>

		<div id="_0800-010-568____________03358">
			<span>免付費專線〡 0800-010-568<br>
        電話〡（03）358-1098    <br>
        傳真〡（03）326-1098<br>
        電郵〡<a href="mailTo:master@lifegroup.house">master@lifegroup.house</a><br>
        地址〡<div id="ID330-71_1132F____2F_No113_Xin" onclick="window.open('https://www.google.com/maps?ll=25.021273,121.298146&z=18&t=m&hl=zh-TW&gl=TW&mapclient=embed&cid=14711057115237095818','_blank')">330-71桃園市桃園區新埔八街 113號2F<br>No.113, Xinpu 8th St., Taoyuan Dist., <br>Taoyuan City 330, Taiwan (R.O.C.)</div></span>
		</div>
		<div id="_cu">
			<span>聯絡我們</span>
		</div>
		<!-- <div id="ID330-71_1132F____2F_No113_Xin">
			<span>330-71桃園市桃園區新埔八街 113號2F   <br>2F, No.113, Xinpu 8th St., Taoyuan Dist., Taoyuan City 330, Taiwan (R.O.C.)</span><br>
		</div> -->
	</div>

</div>

<div class="bottom message">


</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</div>
<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php echo $_smarty_tpl->tpl_vars['js_return']->value;?>

<?php }
}
