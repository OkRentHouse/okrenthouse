<?php
/* Smarty version 3.1.28, created on 2021-03-09 10:43:47
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/SterilizationBuy/search.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6046e0e3259dd3_22605625',
  'file_dependency' => 
  array (
    'ebb20b6815c44963b477fe3c33bd0c8de50e981d' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okmaster/controllers/SterilizationBuy/search.tpl',
      1 => 1615257824,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046e0e3259dd3_22605625 ($_smarty_tpl) {
?>
<style>

.border,
.file, .input, .select, .spinner, .tag-input, .textarea, input[type=datetime-local], input[type=email], input[type=file], input[type=month], input[type=number], input[type=password], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week], select, textarea,
.row_1 img {
    border: 2px solid #dfdfdf;
}

label.h4 {
    margin-top: 2em;
    color: #66c3b7;
}

.shopping {
    margin: 5em auto !important;
}

.shopping * {
    border-radius: 10px;
    margin: 0 1em;
    height: 26pt;
}

.shopping input, .shopping button {
    border: 3px solid #66C3B7;
    /* background: #fff; */
    padding: .3em;
    line-height: initial;
    width: 10em;
}

.shopping {
    display: flex;
    line-height: 28pt;
    max-width: fit-content;
    margin: auto;
}

.shopping input {
    padding: .5em;
}

.shopping button, .row_3 [type=button], .row_4 [type=button] {
    border: 3px solid #66C3B7;
    background: #66C3B7;
    color: #FFF;
}

.yellow[type=button] {
    border: 3px solid yellow;
    background: yellow;
    color: #333;
}

.shopping button:hover, .row_3 [type=button]:hover, .row_4 [type=button]:hover {
    background: #FFF;
    color: #66C3B7;
}

.yellow[type=button]:hover {
  background: #333;
  color: yellow;
}

.col-10.main {
    margin-top: 2em;
}

.row_1_div {
    width: 10%;
    text-align: center;
}

.row_1 *, .row_2 input, .row_3 input, .row_4 input, .row_5 input {
    border-radius: 8px;
}

.row_1 span, .row_2 span  {
    font-weight: bolder;
}

.row_1 {
  padding: 5%;
}

.row_1 .col-6.gap-24:first-child {
    padding-left: 0px !important;
}

.col-6.gap-24:first-child input[type="text"] {
    width: 90%;
}

.row_2 {
    padding: 1em;
}

.row_1 img {
    border: 1px solid #ccc;
    width: 100px;
    height: 100px;
}

.p_name {
  width: 30%;
}

.p_name input::placeholder {

}

.p_name label {
    color: orange;
    position: absolute;
    top: 1.9em;
}

.p_name .disabled {
    background: white !important;
}

.row_b {
    display: flex;
}

.row.b div {
    display: -webkit-box;
    line-height: 28pt;
}
.row_3 span {
    line-height: 38px;
}

.row_3 .col-6 > div, .row_3 span {
    padding: 0px;
}

.row_3 [type=button], .row_4 [type=button] {
    margin: 0px 1em !important;
    height: fit-content;
}

.row_3 input[type="radio"], .row_4 input[type="radio"], .row_5 input[type="radio"] {
    margin: 0px .3em !important;

}

.row_4 img {
  height: 32px;
  width: auto;
}

.row_4 span {
  height: 32px;
  line-height: 21px;
}

.row_4 .row .col-10 input[type="radio"], .row_4 .row .col-10 input[type="checkbox"], .row_5 .row .col-10 input[type="radio"] {
  height: 25px;
  margin-right: 1em;
  margin-left: .3em;
}

.row_4.border .row .col-10, .row_5.border .row .col-10 {
    padding: 0px;
}

.row_4.border .row input.col-sm, .row_5.border .row input.col-sm {
    max-height: 1.5em;
    margin: 0 .5em;
}

.row.b div input[type="text"],.row_2 input[type="text"],.row_3 input[type="text"] {
    width: 70%;
    margin: 0 .5em;
}


.row_2 > .col-6.gap-24 {
    padding: .3em 1em !important;
    padding-left: 2em !important;
}

</style>

<div class="main">

  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./page_head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<div class="house_check_wrap">

  <div id="Web_1280__2" style="transform-origin: 0px 0px;">
    <?php echo $_smarty_tpl->tpl_vars['Breadcrumb']->value;?>

    <br/><br/>
<form action="" method="post" name="form">
<div class="row_2 border">
                <div class="col-6 gap-24">
                  <div class="row">手機號碼
                    <input class="col-6" type="text" name="searchphone" value="<?php echo $_smarty_tpl->tpl_vars['searchphone']->value;?>
" placeholder="請輸入手機號碼" required></div></div>
                <div class="col-6 gap-24">
                  <div class="row">訂單編號
                    <input class="col-6" type="text" name="searchorderid" value="<?php echo $_smarty_tpl->tpl_vars['search_orderid']->value;?>
" placeholder="請輸入訂單號碼" ></div></div></div>
                <div class="col-6 gap-24"><div class="row">
                  <div class="floating" id="captcha_hidden"></div></div></div>
                <div class="col-6 gap-24">
                <input type="hidden" name="oocheck" value="1">
                  <div class="row"><button type="submit" name="checkorder" value="1">送出</button></div></div>
<div class="col-6 gap-24">
                  <div class="row"></div></div></div>
</form>
</div>



</div><?php }
}
