<style>

.border,
.file, .input, .select, .spinner, .tag-input, .textarea, input[type=datetime-local], input[type=email], input[type=file], input[type=month], input[type=number], input[type=password], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week], select, textarea,
.row_1 img {
    border: 2px solid #dfdfdf;
}

label.h4 {
    margin-top: 2em;
    color: #66c3b7;
}

.shopping {
    margin: 5em auto !important;
}

.shopping * {
    border-radius: 10px;
    margin: 0 1em;
    height: 26pt;
}

.shopping input, .shopping button {
    border: 3px solid #66C3B7;
    /* background: #fff; */
    padding: .3em;
    line-height: initial;
    width: 10em;
}

.shopping {
    display: flex;
    line-height: 28pt;
    max-width: fit-content;
    margin: auto;
}

.shopping input {
    padding: .5em;
}

.shopping button, .row_3 [type=button], .row_4 [type=button] {
    border: 3px solid #66C3B7;
    background: #66C3B7;
    color: #FFF;
}

.yellow[type=button] {
    border: 3px solid yellow;
    background: yellow;
    color: #333;
}

.shopping button:hover, .row_3 [type=button]:hover, .row_4 [type=button]:hover {
    background: #FFF;
    color: #66C3B7;
}

.yellow[type=button]:hover {
  background: #333;
  color: yellow;
}

.col-10.main {
    margin-top: 2em;
}

.row_1_div {
    width: 10%;
    text-align: center;
}

.row_1 *, .row_2 input, .row_3 input, .row_4 input, .row_5 input {
    border-radius: 8px;
}

.row_1 span, .row_2 span  {
    font-weight: bolder;
}

.row_1 {
  padding: 5%;
}

.row_1 .col-6.gap-24:first-child {
    padding-left: 0px !important;
}

.col-6.gap-24:first-child input[type="text"] {
    width: 90%;
}

.row_2 {
    padding: 1em;
}

.row_1 img {
    border: 1px solid #ccc;
    width: 100px;
    height: 100px;
}

.p_name {
  width: 30%;
}

.p_name input::placeholder {

}

.p_name label {
    color: orange;
    position: absolute;
    top: 1.9em;
}

.p_name .disabled {
    background: white !important;
}

.row_b {
    display: flex;
}

.row.b div {
    display: -webkit-box;
    line-height: 28pt;
}
.row_3 span {
    line-height: 38px;
}

.row_3 .col-6 > div, .row_3 span {
    padding: 0px;
}

.row_3 [type=button], .row_4 [type=button] {
    margin: 0px 1em !important;
    height: fit-content;
}

.row_3 input[type="radio"], .row_4 input[type="radio"], .row_5 input[type="radio"] {
    margin: 0px .3em !important;

}

.row_4 img {
  height: 32px;
  width: auto;
}

.row_4 span {
  height: 32px;
  line-height: 21px;
}

.row_4 .row .col-10 input[type="radio"], .row_4 .row .col-10 input[type="checkbox"], .row_5 .row .col-10 input[type="radio"] {
  height: 25px;
  margin-right: 1em;
  margin-left: .3em;
}

.row_4.border .row .col-10, .row_5.border .row .col-10 {
    padding: 0px;
}

.row_4.border .row input.col-sm, .row_5.border .row input.col-sm {
    max-height: 1.5em;
    margin: 0 .5em;
}

.row.b div input[type="text"],.row_2 input[type="text"],.row_3 input[type="text"] {
    width: 70%;
    margin: 0 .5em;
}


.row_2 > .col-6.gap-24 {
    padding: .3em 1em !important;
    padding-left: 2em !important;
}

</style>



<div class="main">

  {include file="$tpl_dir./page_head.tpl"}

<div class="house_check_wrap">

  <div id="Web_1280__2" style="transform-origin: 0px 0px;">
    {$Breadcrumb}
  <form action="" method="post" name="form">
    <div class="t_container main">
    
        <div class="row">
          <div class="t_col_1 col-1 left">

          </div>
            <div class="t_col_10 col-10 main">

              <label class="h4">訂單詳情 | 結帳</label>
              <div class="row_1 border">
                <div class="row a">
                    <img src="/themes/LifeHouse/img/lifegoproduct/product_photo_1_022321-02.svg" alt="#">
                    <div class="center-block row_1_div p_name">
                      <span>商品名稱</span>
                      <input type="hidden" value="{$productID}" name="productID">
                      <input type="text" name="productTitle" value={$productTitle} placeholder="{$productTitle}" class="disabled">
                      <label>{$productInfo}</label>
                    </div>
                    <div class="center-block row_1_div">
                      <span>單價</span>
                      <input type="text" name="productPrice" value="{$productPrice}" readonly>
                    </div>
                    <div class="center-block row_1_div">
                      <span>數量</span>
                      <input type="text" name="order" value="{$order}" readonly>
                    </div>
                    <div class="center-block row_1_div">
                      <span>總價</span>
                      <input type="text" name="totalPrice" value="{$productPrice*$order}" readonly>
                    </div>
                </div>
                <div class="row b">
                  <div class="col-6 gap-24"><span>留言給</span><input type="text" value="{$message}" name="message"></div>
                  <div class="col-3 gap-24"><span>訂單總額</span><input type="text" name="order_item_q" value="{$order_item_q}" readonly>件</div>
                  <div class="col-3 gap-24"><span>商品</span><input type="text" name="order_price_total" value="{$order_price_total}" readonly>元 </div>

                </div>
            </div>

              <label class="h4">訂購人資料</label>
              <div class="row_2 border">
                <div class="col-6 gap-24"><div class="row"><span class="col-3">姓名</span><input class="col-6" type="text" name="username" value="{$username}" placeholder="請輸入姓名" required ></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">手機號碼</span><input maxlength="10" class="col-6" type="text" name="mobilephone" value="{$mobilephone}" placeholder="請輸入手機號碼" required >
                  <button type="button" class="btn-primary btn-sm yellow">發送驗證碼</button>
                  <!-- <span>發送驗證碼</span> -->
                </div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">驗證碼</span><input class="col-6" type="text" placeholder="請輸入驗證碼" required maxlength="4"></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">連絡電話</span><input maxlength="10" class="col-6" type="text" name="tel" value="{$tel}" placeholder="請輸入連絡電話" required ></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">地址</span><input class="col-6" type="text" name="address" value="{$address}" placeholder="請輸入地址" required ></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">身分證字號</span><input class="col-6" type="text" name="Identity" value="{$id}" placeholder="請輸入身分證字號" maxlength="10" required ></div></div>
                <div class="col-6 gap-24"><div class="row">*若非會員身分,資料填寫完送出,將自動註冊會員資料。</div></div>
            </div>

              <label class="h4">付款方式</label>
              <div class="row_3 border">
              <div class="col-12 gap-24"><div class="row">
                <span class="col-2"><input class="" type="radio" name="payway" value="1" >信用卡</span>
                <div class="col-6">
                  <div class="row">
                  <span class="col-sm text-right" >信用卡號</span>
                  <input class="col-sm" type="text" maxlength="4" name="cd1" value="{$cd1}">
                  <input class="col-sm" type="text" maxlength="4" name="cd2" value="{$cd2}">
                  <input class="col-sm" type="text" maxlength="4" name="cd3" value="{$cd3}">
                  <input class="col-sm" type="text" maxlength="4" name="cd4" value="{$cd4}">
                  </div>
                </div>
                <script>
                payway.checked = true
                // 如果你在使用 jQuery，下面的用法等价：
                $('input[value=2]').prop('checked', true)
                </script>
                 <span class="col-sm text-right" >有效日期</span><input class="col-1" name="yearmonth" value="{$yearmonth}" type="date" onchange="console.log(this.value)" >
                 <span class="col-sm text-right" >CVS</span><input class="col-1" name="cvs" value="{$cvs}" type="text" maxlength="3"></div></div>
              <div class="col-12 gap-24"><div class="row"><span class="col-2"><input class="" type="radio" name="payway" value="2">貨到付款</span></div></div>
              <div class="col-12 gap-24"><div class="row"><span class="col-2"><input class="" type="radio" name="payway" value="5">LINE Pay</span><span class="col-2"> (請於4小時内付款)</span></div></div>
              <div class="col-12 gap-24"><div class="row"><span class="col-2"><input class="" type="radio" name="payway" value="3">ATM無卡分期</span> <span>申請人同訂購人</span><span style="margin-right: 1em;color:red">信用卡</span> <span>選擇無卡分期期數</span>
                <button type="button" class="btn btn-primary btn-sm" name="ped" value="3">3期 X $ {round($order_price_total/3)}</button>
                <button type="button" class="btn btn-primary btn-sm" name="ped" value="6">6期 X $ {round($order_price_total/6)}</button>
                <button type="button" class="btn btn-primary btn-sm" name="ped" value="9">9期 X $ {round($order_price_total/9)}</button>
                <button type="button" class="btn btn-primary btn-sm" name="ped" value="12">10期 X $ {round($order_price_total/10)}</button>
              </div></div>
              <div class="col-12 gap-24"><div class="row"><span class="col-2"><input id='radiostore' type="radio" name="payway" value="3" />超商付款</span></div></div>
              </div>

              <script>
              window.onload = function() {
                /* you can assign a listener like this:

                        var radio = document.getElementById('radio');    
                        radio.onclick = myFunction;
                        
                or like this: */
                          document.getElementById("radiostore").addEventListener("click", myFunction);
                }
                function myFunction() {
                  var check_pick = document.getElementById("check_pick");
                  var check_pick1 = document.getElementById("check_pick1");
                  if (check_pick.style.display === "none") {
                    check_pick.style.display = "block";
                  } else {
                    check_pick.style.display = "none";
                  }
                  if (check_pick1.style.display === "none") {
                    check_pick1.style.display = "block";
                  } else {
                    check_pick1.style.display = "none";
                  } 
                }
              </script>


              <label class="h4" name="check_pick" id="check_pick" style="display: none;">取貨方式</label>
              <div class="row_4 border" name="check_pick1" id="check_pick1" style="display: none;">
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" name="delway" value="7">超商取貨</span>
                  <div class="col-10">
                    <div class="row">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_3-07.svg"><input class="" type="radio" name="delway" value="3">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_4-07.svg"><input class="" type="radio" name="delway" value="4">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_5-07.svg"><input class="" type="radio" name="delway" value="5">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_6-07.svg"><input class="" type="radio" name="delway" value="6">
                    <span class="col-2 text-right-sm">門市名稱</span><button type="button" class="btn btn-primary btn-sm yellow">查詢門市名稱</button>
                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" name="delway" value="1">宅配</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>收件人姓名:</span><input class="col-sm" type="text" name="pickusername" value={$pickusername}></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>手機號碼:</span><input class="col-sm" type="text" name="pickphone" value="{$pickphone}"></div></span>
                      <input class="" type="checkbox" name="asrow_4"><span class="col-sm-2">同訂購人資料</span>
                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" name="delway" value="2">郵寄</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>收件人姓名:</span><input class="col-sm" type="text" name="pickusername" value={$pickusername}></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>手機號碼:</span><input class="col-sm" type="text" name="pickphone" value="{$pickphone}"></div></span>
                      <input class="" type="checkbox" name="asrow_4"><span class="col-sm-2">同訂購人資料</span>
                    </div>
                  </div>
                </div>
              </div></div>

              <label class="h4">發票資料</label>
              <div class="row_5 border">
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" name="receipt" value="1">公益捐贈</span>
                  <div class="col-10">
                    <div class="row">

                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" name="receipt" value="2">電子發票</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>Emai:</span><input class="col-sm" type="text" name="receiptname" value="{$receiptname}"></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>載具:</span><input class="col-sm" type="text" name="receiptnum" value="{$receiptnum}"></div></span>

                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" name="receipt" value="3">公司發票</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>公司名稱:</span><input class="col-sm" type="text" name="receiptname" value="{$receiptname}"></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>統編:</span><input class="col-sm" type="text" name="receiptnum" value="{$receiptnum}"></div></span>

                    </div>
                  </div>
                </div></div>
              </div>

              <div class="shopping">
                本次訂單金額: <input type="text" value="{$order_price_total}" readonly></input>
                <button type="submit" name="checkorder" value="1">確認結帳</button>
              </div>

            </div>
            <div class="t_col_1 col-1 right">

            </div>
        </div>
        </form>
  
</div>


{include file="$tpl_dir./page_footer.tpl"}
</div>
{$js}
{$js_return}
