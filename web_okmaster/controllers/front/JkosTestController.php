<?php

namespace web_okmaster;
use \OkmasterController;
use \FrontController;

class JkosTestController extends OkmasterController

{

    public $page = 'JkosTest';

//	public $tpl_folder;    //樣版資料夾

    public $tpl_folder;

    public $definition;

    public $_errors_name;



    public function __construct()

    {

        $this->meta_title                = $this->l('Jkos API');

        $this->page_header_toolbar_title = $this->l('Jkos API');

        $this->className                 = 'JkosTestController';

        $this->no_FormTable              = true;

        $this->display_header = false;

        $this->display_footer = false;

        parent::__construct();

    }



	public function initProcess()

	{

    $platform_order_id=1; #電商平台端交易序號需為唯一值，不可重複。
    $store_id="bd0e0dd4-8607-11eb-948d-0050568403ed"; #商店編號請依街口提供的測試/正式商店代碼更新。
    $currency="TWD"; #付款貨幣[ISO 4217]，請帶入TWD。
    $totalprice=999; #訂單原始金額。
    $final_price=9999; #訂單實際消費金額
    $unredeem=""; #不可折抵金
    $valid_time=""; #訂單有效期限，依 UTC+8 時區。格式 : YYYY-mm-dd HH:MM
    $confirmurl=""; #由商家實作此callback URL(https)。買家在街口確認付款頁面輸入密碼後，街口服務器訪問此電商平台服務器網址確認訂單正確性與存貨彈性。
    $resulturl=""; #由電商平台實作此callback URL(https)。消費者付款完成後，街口服務器訪問此電商平台服務器網址，並在參數中提供街口交易序號與訂單交易狀態代碼。
    $result_display_url=""; #由電商平台實作此客戶端http/s url。消費者付款完成後點選完成按鈕，將消費者導向此電商平台客戶端付款結果頁網址。
    $payment_type="onetime"; #付款模式 : “onetime”為一次性付款，”regular”為定期定額付款；預設為一次性付款。
    $escrow="false"; #是否支持價金保管，預設為False 不支持。
    $productsfields=""; #支援 JSON/String 格式，陣列帶入以下資訊。當使用 products 欄位時，則除了 products.img 其餘皆為必要欄位。
    $productsname=""; #商品名稱（charset=utf-8）
    $productsimg=""; #商品網址
    $productsunit_count=""; #商品數量
    $productsunitprice=""; #商品單價（原價）
    $productsunit_final_price=""; #商品單價（付款價格）



    //$pr=Google_api::get_curl("http://api.opencube.tw/twzipcode/get-citys");
    $entry_url="https://uat-onlinepay.jkopay.app/platform/entry";

    $data = array(
      "platform_order_id" => $platform_order_id,
      "store_id" => $store_id,
      "currency" => $currency,
      "total_price" => $totalprice,
      "final_price" => $final_price,
      "unredeem" => $unredeem,
      "valid_time" => $valid_time,
      "confirm_url" => $confirmurl,
      "result_url" => $resulturl,
      "result_display_url" => $result_display_url,
      "payment_type" => $payment_type,
      "escrow" => $escrow,
      "products fields" => $productsfields,
      "products.name" => $productsname,
      "products.img" => $productsimg,
      "products.unit_count" => $productsunit_count,
      "products.unit_price" => $productsunitprice,
      "products.unit_final_price" => $productsunit_final_price,
    );


    $payload = utf8_encode(json_encode($data));
    $APIKEY="CE9YLfHiVCbi4u2ceMXdqD4LqURTlCNTbocy";
    $SECERT_KEY="Ad4wu-yBCfJQrRiQrP7KmQN_TEQrKbTYWM_ehYk1vOuaaF6Yc443CG8QqRYU3cww5d1VQ0acHD3oskw4ek8QBw";
    $DIGEST=hash_hmac("SHA256",$payload,utf8_encode($SECERT_KEY));

		$this->context->smarty->assign([
      'pr'      => $pr,
      'entry_url'      => $this->curl_post_https($entry_url,$data,$APIKEY,$DIGEST,$payload),
      'APIKEY' => $APIKEY,
      'SECERT_KEY' => $SECERT_KEY,
      'payload' => $payload,
      'DIGEST' => $DIGEST,
		]);

		parent::initProcess();

	}

  public function curl_post_https($url,$data,$APIKEY,$DIGEST,$payload){ // 模擬提交數據函數
    $curl = curl_init(); // 啟動一個CURL會話
    curl_setopt($curl, CURLOPT_URL, $url); // 要訪問的地址
    curl_setopt($curl, CURLOPT_POST, 1); // 發送一個常規的Post請求
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的數據包
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 獲取的信息以文件流的形式返回
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Content-Type:application/json',
      'Api-Key: ' .  $APIKEY,
      'DIGEST: ' .  $data
      )
    );
    $tmpInfo = curl_exec($curl); // 執行操作
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);//捕抓異常
    }
    curl_close($curl); // 關閉CURL會話
    return $tmpInfo; // 返回數據，json格式
  }

	public function setMedia()

	{

		parent::setMedia();


	}

}
