<?php
namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Db;
use \File;

class IndexController extends OkmasterController
{
    public $page = 'index';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->page_header_toolbar_title = $this->l('首頁');
		$this->className                 = 'IndexController';
		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
		parent::__construct();

	}

	public function initProcess()
	{
//        $banner = [
//            [
//                'img'=>'',
//            ],
//            [
//                'img'=>'',
//            ],
//            [
//                'img'=>'',
//            ],
//        ];

        $index_ratio      = '900:450';

        $arr_index_banner =[
            [
                'img'=>'/themes/Okmaster/img/index/epro_banner_1.jpg',
                'href'=>'',
            ],

            [
                'img'=>'/themes/Okmaster/img/index/epro_banner_2.jpg',
                'href'=>'',
            ],

            [
                'img'=>'/themes/Okmaster/img/index/epro_banner_3.jpg',
                'href'=>'',
            ],
        ];


        //2021.01.05 起 不使用 改 \themes\Okmaster\group_link.tpl 控制
        //2021.01.05 不可清空，仍有判斷
        $group_link_data =[
            [
                'txt'  => '生活集團',
                'url' => 'https://www.lifegroup.house/',
                'img' =>  '/themes/Rent/img/index/logo-b-01_202101.png',
            ],
        ];

        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/index/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data="<div class='footer' style=''><img src='/themes/Okmaster/img/index/footer_logo_202101.png'>生活好科技有限公司 生活好科技·科技好生活 copyright ©LIFE MASTER TECHNOLOGY COMPANY <a href='##'>關於我們</a> | <a href='##'>合作提案</a> | <a href='##'>相關聲明</a></div>";
        $footer_data="<div class='footer' style='width: 100%;'><img src='/themes/Okmaster/img/index/footer_logo_202101.png'>生活好科技有限公司 <span style='font-size: 10pt;margin: 10px;'>生活好科技·科技好生活</span> copyright ©LIFE MASTER TECHNOLOGY COMPANY &nbsp&nbsp&nbsp<a href='/Aboutokmaster'>關於我們</a> | <a href='/Aboutokmaster'>合作提案</a> | <a href='##'>相關聲明</a></div>";
        $footer_data="<a href='/Aboutokmaster'><img src='/themes/Okmaster/img/index/footer_logo.png'></a>生活好科技有限公司 <span class='spacing'></span> copyright ©LIFE MASTER TECHNOLOGY COMPANY <span class='spacing'></span> <!--<a href='/Aboutokmaster'>合作提案</a> | --><a href='/State'>相關聲明</a>";

        $css =<<<css
        <style>
        .head_group_zoon {
              height: 70px !important;
          }
        .head_group {
            z-index: 9999 !important;
          }
          .row.top_fn ul.logo li img {
              opacity: 0 !important;
          }
        </style>
css;

		$this->context->smarty->assign([
            'index_ratio'      => $index_ratio,
            'arr_index_banner'=>$arr_index_banner,
            'slick_data'    =>['class'=>'slider_center','slidesToShow'=>'5','slidesToScroll'=>'8',],
            'group_link'        => 1,
            'group_link_data'   =>$group_link_data,
            'footer_data'   =>$footer_data,
            'group_link_data' => $group_link_data,
            'css' => $css,
		]);
		parent::initProcess();
	}


	public function setMedia()
	{

		parent::setMedia();
//        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
//        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
//        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
//        $this->addJS('/js/slick.min.js');
//        $this->addCSS('/css/slick-theme.css');
//        $this->addCSS('/css/slick.css');
//        $this->addCSS('/css/metro-all.css');
//        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/bootstrap-grid.min.css');

        $this->addJS('/js/m4q.js');
        $this->addJS('/js/metro.js');
        $this->addCSS('/css/metro-all.min.css');
        $this->addCSS( THEME_URL .'/css/all_page.css');
        $this->addCSS( THEME_URL .'/css/head_item.css');


	}

}
