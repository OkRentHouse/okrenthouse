<?php
namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Db;
use \File;

class LoginController extends OkmasterController
{
    public $page = 'Login';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->page_header_toolbar_title = $this->l('首頁');
		$this->className                 = 'LoginController';
		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
		parent::__construct();

	}

	public function initProcess()
	{
//        $banner = [
//            [
//                'img'=>'',
//            ],
//            [
//                'img'=>'',
//            ],
//            [
//                'img'=>'',
//            ],
//        ];

        $index_ratio      = '900:450';

        $arr_index_banner =[
            [
                'img'=>'/themes/Okmaster/img/index/epro_banner_1.jpg',
                'href'=>'',
            ],

            [
                'img'=>'/themes/Okmaster/img/index/epro_banner_2.jpg',
                'href'=>'',
            ],

            [
                'img'=>'/themes/Okmaster/img/index/epro_banner_3.jpg',
                'href'=>'',
            ],
        ];



        $group_link_data =[
            [
                'txt'  => '生活集團',
                'url' => 'https://www.lifegroup.house/',
                'img' =>  '/themes/Rent/img/index/logo-b-01.png',
            ],
            [
                'txt'  => '生活房屋',
                'url' => 'https://www.life-house.tw',
                'img' =>  '/themes/Rent/img/index/logo-b-02.png',
            ],
            [
                'txt'  => '生活樂租',
                'url' => 'https://www.okrent.house/',
                'img' =>  '/themes/Rent/img/index/logo-b-11.png',
            ],
            [
                'txt'  => '共享圈',
                'url' => 'https://www.okrent.tw',
                'img' =>  '/themes/Rent/img/index/logo-b-08.png',
            ],
            // [
            //     'txt'  => '生活好科技',
            //     'url' => '',
            //     'img' =>  '/themes/Rent/img/index/logo-b-03.png',
            // ],
            [
                'txt'  => '生活好康',
                'url' => '',
                'img' =>  '/themes/Rent/img/index/logo-b-04.png',
            ],
            // [
            //     'txt'  => '##',
            //     'url' => '##',
            //     'img' =>  '/themes/Rent/img/index/logo-b-05.png',
            // ],
            [
                'txt'  => '生活樂購',
                'url' => 'https://www.lifegroup.house/LifeGo',
                'img' =>  '/themes/Rent/img/index/logo-b-06.png',
            ],
            [
                'txt'  => '好幫手',
                'url' => 'https://www.okpt.life',
                'img' =>  '/themes/Rent/img/index/logo-b-07.png',
            ],
            [
                'txt'  => '裝修達人',
                'url' => 'https://www.epro.house',
                'img' =>  '/themes/Rent/img/index/logo-b-09.png',
            ],
        ];

        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/index/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data="<div class='footer' style=''><img src='/themes/Okmaster/img/index/footer_logo.png'>生活好科技有限公司 生活好科技·科技好生活 copyright ©LIFE MASTER TECHNOLOGY COMPANY <a href='##'>關於我們</a><!-- | <a href='##'>合作提案</a>--> | <a href='/State'>相關聲明</a></div>";
        

		$this->context->smarty->assign([
            'index_ratio'      => $index_ratio,
            'arr_index_banner'=>$arr_index_banner,
            'slick_data'    =>['class'=>'slider_center','slidesToShow'=>'8','slidesToScroll'=>'8',],
            'group_link'        => 1,
            'group_link_data'   =>$group_link_data,
            'footer_data'   =>$footer_data,
            'group_link_data' => $group_link_data,
		]);
		parent::initProcess();
	}


	public function setMedia()
	{

		parent::setMedia();
//        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
//        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
//        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
//        $this->addJS('/js/slick.min.js');
//        $this->addCSS('/css/slick-theme.css');
//        $this->addCSS('/css/slick.css');
//        $this->addCSS('/css/metro-all.css');
//        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/bootstrap-grid.min.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addJS('/js/m4q.js');
        $this->addJS('/js/metro.js');

	}

}
