<?php

namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Tools;

class JkosrefundController extends OkmasterController

{

    public $page = 'Jkosrefund';

//	public $tpl_folder;    //樣版資料夾

    public $tpl_folder;

    public $definition;

    public $_errors_name;



    public function __construct()

    {

        $this->meta_title                = $this->l('Jkosrefund API');

        $this->page_header_toolbar_title = $this->l('Jkosrefund API');

        $this->className                 = 'JkosrefundController';

        $this->no_FormTable              = true;

        $this->display_header = false;

        $this->display_footer = false;

        parent::__construct();

    }



	public function initProcess()

	{
    // https://okmaster.life/Jkosrefund?platform_order_ids=1&refund_amount=99
    if(Tools::getValue('platform_order_id')){
      $platform_order_id=Tools::getValue('platform_order_id'); #電商平台端交易序號需為唯一值，不可重複。
    }else{
      $platform_order_id=1; #電商平台端交易序號需為唯一值，不可重複。
    }

    if(Tools::getValue('refund_amount')){
      $refund_amount=Tools::getValue('refund_amount'); #電商平台端交易序號需為唯一值，不可重複。
    }else{
      $refund_amount=999; #電商平台端交易序號需為唯一值，不可重複。
    }

    //$pr=Google_api::get_curl("http://api.opencube.tw/twzipcode/get-citys");
    $entry_url="https://uat-onlinepay.jkopay.app/platform/refund";

    $data = array(
      "platform_order_id" => $platform_order_id,
      "refund_amount" => $refund_amount,
    );


    $payload = utf8_encode(json_encode($data));
    //$payload = json_encode('{"store_id": "d7120db2-8c76-4124-bf08-02e5b775d8fe", "platform_order_id": 87, "currency": "TWD","total_price": 1000, "final_price": 1000, "escrow": false, "payment_type": "onetime"}');
    $APIKEY="CE9YLfHiVCbi4u2ceMXdqD4LqURTlCNTbocy";
    $SECERT_KEY="Ad4wu-yBCfJQrRiQrP7KmQN_TEQrKbTYWM_ehYk1vOuaaF6Yc443CG8QqRYU3cww5d1VQ0acHD3oskw4ek8QBw";
    //$SERCERT_KEY="QxN1X";
    $DIGEST=hash_hmac("SHA256",$payload,utf8_encode($SECERT_KEY));

    echo strval($DIGEST);
    echo "<hr>";
		$this->context->smarty->assign([
      'pr'      => $pr,
      'entry_url'      => $this->curl_post_https($entry_url,$data,$APIKEY,$DIGEST,$payload),
      'APIKEY' => $APIKEY,
      'SECERT_KEY' => $SECERT_KEY,
      'payload' => $payload,
      'DIGEST' => $DIGEST,
		]);

		parent::initProcess();

	}

  public function curl_post_https($url,$data,$APIKEY,$DIGEST,$payload){ // 模擬提交數據函數
    echo strval(json_encode($data));
    echo "<hr>";
    $curl = curl_init(); // 啟動一個CURL會話
    curl_setopt($curl, CURLOPT_URL, $url); // 要訪問的地址
    // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 對認證證書來源的檢查
    // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1); // 從證書中檢查SSL加密算法是否存在
    // curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER[‘HTTP_USER_AGENT‘]); // 模擬用戶使用的瀏覽器
    // curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自動跳轉
    // curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自動設置Referer
    curl_setopt($curl, CURLOPT_POST, 1); // 發送一個常規的Post請求
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data)); // Post提交的數據包
    // curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 設置超時限制防止死循環
    // curl_setopt($curl, CURLOPT_HEADER, 0); // 顯示返回的Header區域內容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 獲取的信息以文件流的形式返回
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Content-Type:application/json',
      'Api-Key: ' .  $APIKEY,
      'Digest: ' .  $DIGEST,
      )
    );
    $tmpInfo = curl_exec($curl); // 執行操作
    echo strval($APIKEY);
    echo "<hr>";
    if (curl_errno($curl)) {
        echo ‘Errno‘.curl_error($curl);//捕抓異常
    }
    curl_close($curl); // 關閉CURL會話
    return $tmpInfo; // 返回數據，json格式
  }

	public function setMedia()

	{

		parent::setMedia();


	}

}
