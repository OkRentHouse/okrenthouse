<?php

namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Db;
use \Tools;
use \SMS;
use \FileUpload;

class AboutokmasterController extends OkmasterController
{
    public $page = 'Aboutokmaster';

//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->page_header_toolbar_title = $this->l('首頁');
		$this->className                 = 'AboutokmasterController';
//		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
        $this->conn ='ilifehou_okmaster';
		parent::__construct();
	}

	public function initProcess()
	{



        $js =<<<js
<script>
$(document).ready(function(){
  $("#_da").click(function(){
    $(".dialogbox").show(100);
    $(".dialogboxbk").show(100);
  });
  $("#cancel").click(function(){
    $(".dialogbox").hide(300);
    $(".dialogboxbk").hide(300);
  });
  $(".upload").click(function(){
      $(".fileupload").click();
  });
})
</script>
js;
        $Breadcrumb                = '首頁 > 生活好科技 > 清淨對策 > 關於我們';
        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/Aboutokmaster/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data='';

		$this->context->smarty->assign([
            'slick_data'    =>['class'=>'slider_center','slidesToShow'=>'7','slidesToScroll'=>'7',],
            'footer_data'   =>$footer_data,
            'PGWD'   => $_SERVER['QUERY_STRING'],
            'Breadcrumb'   => $Breadcrumb,
            'js' =>$js,
		]);
		parent::initProcess();
	}

    public function validateRules()
    {
        $put =Tools::getValue("put");//是否送出
        $name = Tools::getValue("name");//名稱
        $sex = Tools::getValue("sex");//性別
        $company = Tools::getValue("company");//公司
        $job = Tools::getValue("job");//職業
        $company_phone = Tools::getValue("company_phone");//公司電話
        $extension = Tools::getValue("extension");//分機
        $phone = Tools::getValue("phone");//手機號碼
        $email = Tools::getValue("email");//電子信箱
        $content = Tools::getValue("content");//提案內容
        $verification = Tools::getValue("verification");//提案內容

        //驗證碼
        $sql = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($phone, 'text');
        $row    = Db::rowSQL($sql, true);
        $phone_verification = $row['captcha']; //驗證碼

        if(strlen($name) < '2' || strlen($name) > '32'){
            $this->_errors[] = $this->l('未輸入提案人或是提案人長度不符');
        }
        if(empty($company) || strlen($company) > '32'){
            $this->_errors[] = $this->l('未輸入公司或是公司長度不符');
        }
        if(empty($job) || strlen($job) > '32'){
            $this->_errors[] = $this->l('未輸入職業或是職業長度不符');
        }
        if(empty($company_phone) || strlen($company_phone) > '20'){
            $this->_errors[] = $this->l('未輸入公司電話或是公司電話長度不符');
        }
        if(!preg_match("/09[0-9]{8}/",$phone)){
            $this->_errors[] = $this->l('請輸入手機號碼');
        }
        if(strlen($email) > '128'){
            $this->_errors[] = $this->l('電子信箱過長');
        }
        if(empty($content)){
            $this->_errors[] = $this->l('請填寫提案內容');
        }
        if(empty($verification) || $verification !=$phone_verification){
            $this->_errors[] = $this->l('請填寫手機驗證碼錯誤');
        }


        parent::validateRules();
    }
    public function postProcess(){
        if(Tools::getValue("put")==1){//有上傳了
            $put =Tools::getValue("put");//是否送出
            $name = Tools::getValue("name");//名稱
            $sex = Tools::getValue("sex");//性別
            $company = Tools::getValue("company");//公司
            $job = Tools::getValue("job");//職業
            $company_phone = Tools::getValue("company_phone");//公司電話
            $extension = Tools::getValue("extension");//分機
            $phone = Tools::getValue("phone");//手機號碼
            $email = Tools::getValue("email");//電子信箱
            $content = Tools::getValue("content");//提案內容
            $status = '0';//提案後狀態
            $file = $_FILES["data"];//上傳檔案
            $this->validateRules();
            if (count($this->_errors)) return;

            $sql = 'INSERT INTO ilifehou_okmaster.`proposal` (`name`,`sex`,`company`,`job`,`company_phone`,`extension`,`phone`,`email`,`content`,`status`)
            VALUES('.GetSQL($name,"text").','.GetSQL($sex,"text").','.GetSQL($company,"text").','.GetSQL($job,"text").',
            '.GetSQL($company_phone,"text").','.GetSQL($extension,"text").','.GetSQL($phone,'text').','.GetSQL($email,"text").'
            ,'.GetSQL($content,"text").','.GetSQL($status,"int").')';
            Db::rowSQL($sql);

            if(!empty($_FILES)){//有上傳檔案 目前限定單檔
               $sql = 'SELECT * FROM ilifehou_okmaster.`proposal` WHERE phone='.GetSQL($phone,"text")."AND name=".GetSQL($name,"text");
               $data = Db::rowSQL($sql,true);//取得相關資料
                $dir      = $this->iniUpFileDir($data['id_proposal']);
                add_dir($dir);
                $url = $this->iniUpFileUrl($data['id_proposal']);
                $filenames = $file['name'];
                // loop and process files
                $arr = [];
                for ($i = 0; $i < count($filenames); $i++) {
                    $ext       = explode('.', basename($filenames[$i]));
                    $file_name = FileUpload::check_name($file['name'][$i], $dir);
                    $target    = $dir . DS . $file_name;
                    if (move_uploaded_file($file['tmp_name'][$i], $target)) {
                        $success = true;
                        $arr[]   = [
                            'ext'   => array_pop($ext),
                            'name'  => $file_name,
                            'type'  => $file['type'][$i],
                            'size'  => $file['size'][$i],
                            //						'downloadUrl' => $url.$file_name,
                            'paths' => $target,
                        ];
//存檔
                    } else {
                        //todo 單黨上傳 2019-12-18
                        $ext       = explode('.', basename($filenames));
                        $file_name = FileUpload::check_name($file['name'], $dir);
                        $target    = $dir . DS . $file_name;

                        if (move_uploaded_file($file['tmp_name'], $target)) {
                            $success = true;
                            $arr[]   = [
                                'ext'   => array_pop($ext),
                                'name'  => $file_name,
                                'type'  => $file['type'],
                                'size'  => $file['size'],
                                //						'downloadUrl' => $url.$file_name,
                                'paths' => $target,
                            ];
                        } else {
                            $success = false;
                            break;
                        }
                    }
                }
                $this->displayAjaxUpFileAction($arr, $dir, $url,$data['id_proposal']);
            }
            $js_return = <<<js
        <script>
        $(document).ready(function(){
            alert("感謝您的回應 我們會盡快予以答覆!");
})
        </script>
js;
            unset($_POST);//work之後刪光 不然會很麻煩
            $this->context->smarty->assign([
                'js_return'    =>$js_return,
            ]);
        }

        parent::postProcess();
    }

    public function displayAjaxTel()//發送簡訊給
    {
        $action = Tools::getValue('action');    //action = 'Tel';
        $mobile = Tools::getValue('phone');    //手機號碼
        switch($action){
            case 'Tel':
                $sql = "select count(*) coun from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($mobile, 'text');
                //避免10分鐘內重複撥打同支手機

                $row_session     = Db::rowSQL($sql, true);
                $session_count = $row_session["coun"];

                if(!preg_match("/09[0-9]{8}/",$mobile)){
                    echo json_encode(array("error"=>"tel error","return"=>"請輸入手機號碼 如:09XXXXXXXX123"));
                    break;
                }else if($session_count > '0'){
                    echo json_encode(array("error"=>"tel wait","return"=>"請等待10分鐘後再輸入手機號碼"));
                    break;
                }

                $user_ip = $_SERVER['REMOTE_ADDR'];//如果有異常的發送則能夠透過log得知
                $text = rand(1000,9999);//生成隨機的4碼
                $text_sms = '您的驗證碼為:'.$text;
                $user_id = '';
                $sms_id= '';
                $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
                //自己輸入否則會連 您的驗證碼為:都有影響輸入
                $sql = "INSERT INTO sms_log (`session_id`, `tel`, `captcha`, `ip`, `msgid`, `statuscode`,
                            `return_status`, `accountPoint`)
                            VALUES (".GetSQL(session_id(), 'text').",".GetSQL($mobile, 'text').",
                            ".GetSQL($text, 'text').",".GetSQL($user_ip, 'text').",".GetSQL($sms_return['msgid'],
                        'text').",".GetSQL($sms_return['statuscode'], 'text').",
                            ".GetSQL(SMS::sms_status_text($sms_return['statuscode'],"mitake"), 'text').",".GetSQL($sms_return['AccountPoint'], 'int').")";
                Db::rowSQL($sql);//存入資料庫

                if($sms_return['statuscode'] =='0' ||$sms_return['statuscode'] =='1' || $sms_return['statuscode'] =='2'|| $sms_return['statuscode'] =='4'){
                    echo json_encode(array("error"=>"","return"=>"請等候簡訊發送"));
                    break;
                }else{
                    echo json_encode(array("error"=>"","return"=>"簡訊發送有誤 請洽管理人員"));
                    break;
                }
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

    public function iniUpFileDir($id_proposal){
        list($dir, $url) = str_dir_change($id_proposal);     //切割資料夾
        $dir = WEB_DIR . DS . Proposal_FILE_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl($id_proposal){
        list($dir, $url) = str_dir_change($id_proposal);
        $url = Proposal_FILE_URL . $url;
        return $url;
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $id_proposal){
        $type = '0';
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `proposal_file` (`id_proposal`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_proposal, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql,false,0,$this->conn);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

	public function setMedia()
	{
		parent::setMedia();
        $WD = substr(strtok(strchr($_SERVER['QUERY_STRING'],"&"),"="),1);
        $PGWD = $_SERVER['QUERY_STRING'];
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addCSS( THEME_URL .'/css/'.$PGWD.'.css');
        $this->addJS(THEME_URL .'/message/message.js');
        $this->addCSS( THEME_URL .'/css/all_page.css');
	}
}
