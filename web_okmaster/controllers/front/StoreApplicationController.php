<?php

namespace web_okmaster;
use \OkmasterController;
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;

class StoreApplicationController extends OkmasterController
{

    public $page = 'StoreApplication';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;
    public function __construct()
    {
        $this->className = 'StoreApplicationController';
        $this->meta_title                = $this->l('生活好康|生活好康特約商店註冊申請書');
        $this->page_header_toolbar_title = $this->l('生活好康|生活好康特約商店註冊申請書');
        $this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
        $this->no_FormTable              = true;
        $this->table = 'store';

        parent::__construct();
    }

    //初始化麵包屑        //參考


    public function getStoreType($type = 'all'){
        $sql = 'SELECT * FROM `store_type` ORDER BY `position` ASC';
        $arr = Db::rowSQL($sql);
        return $arr;
    }

    public function initProcess(){
        session_start();
        // if(Tools::getValue("cs") == "0"){
        // unset($_SESSION['store_name_new']);
        // unset($_SESSION['store_address_postcode']);
        // unset($_SESSION['store_address_city']);
        // unset($_SESSION['store_address_sub']);
        // unset($_SESSION['store_address_details']);
        // unset($_SESSION['store_phone']);
        // unset($_SESSION['store_open_time']);
        // unset($_SESSION['store_close_time']);
        // unset($_SESSION['store_product']);
        // unset($_SESSION['store_coupons']);
        // unset($_SESSION['store_details']);
        // unset($_SESSION['store_website']);
        // unset($_SESSION['store_closed']);
        // unset($_SESSION['store_category']);
        // unset($_SESSION['store_business_number']);
        // unset($_SESSION['store_law_rep']);
        // unset($_SESSION['store_regi_address']);
        // unset($_SESSION['store_tel']);
        // unset($_SESSION['touch_people']);
        // unset($_SESSION['store_mobile']);
        // unset($_SESSION['store_email']);
        // unset($_SESSION['password']);
        // unset($_SESSION['store_logo']);
        // unset($_SESSION['store_img'][0]);
        // unset($_SESSION['store_img'][1]);
        // unset($_SESSION['store_img'][2]);
        // unset($_SESSION['store_server'][0]);
        // unset($_SESSION['store_server'][1]);
        // unset($_SESSION['store_server'][2]);
        // }

        $application_process = 0;
        $application_page = "content_".$application_process.".tpl";
        if(Tools::getValue("application_page")){
          $application_process = Tools::getValue("application_page");
          $application_page = "content_".$application_process.".tpl";
        };

        $db_link = mysqli_connect("localhost", "ilifehou_ilife", "S^,HO%bh^$&NF3dab", "ilifehou_okmaster");
        $conn2 = mysqli_connect("localhost", "ilifehou_ilife", "S^,HO%bh^$&NF3dab", "ilifehou_ilife_house");


        $js =<<<js
<script>
$(".mutileplus").on("click",function(){
    p=$(this).parent().parent().parent();
    o=$(p).html();
    n=$(p).parent().append(o);
   $(n).find(".mutileplus").last().parent().parent().find(".title").css({opacity: 0});
   $(n).find(".mutileplus").last().css({opacity: 0});

})
</script>
js;

        if($_POST['t1'] == "1"){
        $_SESSION['store_name_new'] = $_POST['store_name_new'];
        $_SESSION['store_address_postcode'] = $_POST['store_address_postcode'];
        $_SESSION['store_address_city'] = $_POST['store_address_city'];
        $_SESSION['store_address_sub'] = $_POST['store_address_sub'];
        $_SESSION['store_address_details'] = $_POST['store_address_details'];
        $_SESSION['store_phone'] = $_POST['store_phone'];
        $_SESSION['store_open_time'] = $_POST['store_open_time'];
        $_SESSION['store_close_time'] = $_POST['store_close_time'];
        $_SESSION['store_product'] = $_POST['store_product'];
        $_SESSION['store_coupons'] = $_POST['store_coupons'];
        $_SESSION['store_details'] = $_POST['store_details'];
        $_SESSION['store_website'] = $_POST['store_website'];
        $_SESSION['store_closed'] = $_POST['store_closed'];
        }
        if($_POST['t2'] == "2"){
            $this->validateRules();
        $_SESSION['store_category'] = $_POST['store_category'];
        $_SESSION['store_business_number'] = $_POST['store_business_number'];
        $_SESSION['store_law_rep'] = $_POST['store_law_rep'];
        $_SESSION['store_regi_address'] = $_POST['store_regi_address'];
        $_SESSION['store_tel'] = $_POST['store_tel'];
        $_SESSION['touch_people'] = $_POST['touch_people'];
        $_SESSION['store_mobile'] = $_POST['user'];
        $_SESSION['store_email'] = $_POST['store_email'];
        $_SESSION['password'] = $_POST['password'];
        }


        if($_POST['t2'] == "2"){

        //     $sql="select * from member where user='".$_SESSION['store_mobile']."'";
        //     $result=mysqli_query($sql);
        //     $row = mysqli_fetch_array($result, MYSQL_ASSOC);
        //     if (!mysqli_num_rows($result)) {
        //         $insert_sql = "INSERT INTO appointed_store
        // (store_name,store_address_postcode,store_address_city,store_address_sub,store_address_details,store_phone,store_open_time,store_close_time,store_closed,store_product,store_coupons,store_details,store_website,store_category,store_business_number,store_law_rep,store_regi_address,store_tel,touch_people,store_mobile,store_email)
        // VALUES
        // ('".$_SESSION['store_name_new']."','".$_SESSION['store_address_postcode']."','".$_SESSION['store_address_city']."','".$_SESSION['store_address_sub']."','".$_SESSION['store_address_details']."','".$_SESSION['store_phone']."','".$_SESSION['store_open_time']."','".$_SESSION['store_close_time']."','".$_SESSION['store_closed']."','".$_SESSION['store_product']."','".$_SESSION['store_coupons']."','".$_SESSION['store_details']."','".$_SESSION['store_website']."','".$_SESSION['store_category']."','".$_SESSION['store_business_number']."','".$_SESSION['store_law_rep']."','".$_SESSION['store_regi_address']."','".$_SESSION['store_tel']."','".$_SESSION['touch_people']."','".$_SESSION['store_mobile']."','".$_SESSION['store_email']."')";
        // //echo $insert_sql;
        // mysqli_query($db_link, $insert_sql);
        // Db::rowSQL($insert_sql);

        // $insert_member = "INSERT INTO member (user,password)
        // VALUES ('".$_SESSION['store_mobile']."','".$_SESSION['password']."')";
        // mysqli_query($conn2, $insert_member);
        // Db::rowSQL($insert_member);
        //     }
        // //         $insert_sql = "INSERT INTO appointed_store
        // // (store_name,store_address_postcode,store_address_city,store_address_sub,store_address_details,store_phone,store_open_time,store_close_time,store_closed,store_product,store_coupons,store_details,store_website,store_category,store_business_number,store_law_rep,store_regi_address,store_tel,touch_people,store_mobile,store_email)
        // // VALUES
        // // ('".$_SESSION['store_name_new']."','".$_SESSION['store_address_postcode']."','".$_SESSION['store_address_city']."','".$_SESSION['store_address_sub']."','".$_SESSION['store_address_details']."','".$_SESSION['store_phone']."','".$_SESSION['store_open_time']."','".$_SESSION['store_close_time']."','".$_SESSION['store_closed']."','".$_SESSION['store_product']."','".$_SESSION['store_coupons']."','".$_SESSION['store_details']."','".$_SESSION['store_website']."','".$_SESSION['store_category']."','".$_SESSION['store_business_number']."','".$_SESSION['store_law_rep']."','".$_SESSION['store_regi_address']."','".$_SESSION['store_tel']."','".$_SESSION['touch_people']."','".$_SESSION['store_mobile']."','".$_SESSION['store_email']."')";
        // // //echo $insert_sql;
        // // mysqli_query($db_link, $insert_sql);
        // // Db::rowSQL($insert_sql);

        // $update_member = "UPDATE member set password='".$_SESSION['password']."' where user='".$_SESSION['store_mobile']."'";
        // mysqli_query($conn2, $update_member);
        // Db::rowSQL($update_member);

        }

        if($_POST['t3'] == "3"){
            unset($_SESSION['store_logo']);
            $target_path = "./img/store_img/";
            $target_path2 = "./img/store_img/";
            $getDate= date("Ymdhis");
            $target_path = $target_path . basename( $getDate.$_FILES['uploadedfile']['name']);
            $url = str_replace(WEB_DIR, '', $target_path . $_FILES['uploadedFile']['name']);
            $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
            $fileTmpPath = $_FILES['uploadedfile']['tmp_name'];
            $fileName = $_FILES['uploadedFile']['name'];
            $_SESSION['store_logo'] = $url;  // Session Logo url
            if(move_uploaded_file($fileTmpPath, $target_path)) {
                //echo "The file ".  basename( $_FILES['uploadedfile']['name']). " has been uploaded";
            } else{
                //echo "There was an error uploading the file, please try again!";
            }

            //店家照片
            $error=array();
            $extension=array("jpeg","jpg","png","gif");
            if($_FILES["files"] != ''){
                unset($_SESSION['store_img'][0]);
                unset($_SESSION['store_img'][1]);
                unset($_SESSION['store_img'][2]);
                foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
                    $target_path2 = "./img/store_img/";
                    $file_name=$_FILES["files"]["name"][$key];
                    $file_tmp=$_FILES["files"]["tmp_name"][$key];
                    $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                    $target_path2 = $target_path2 . basename( $getDate.$_FILES['files']['name'][$key]);
                    $url2 = str_replace(WEB_DIR, '', $target_path2);
                    $url2 = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url2);
                    $_SESSION['store_img'][$key] = $url2;
                    //echo $_SESSION['store_img'][$key] ."<br/>";
                    move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],$target_path2);

                }
            }


            //服務產品圖片
            $error=array();
            $extension=array("jpeg","jpg","png","gif");
            if($_FILES["files1"] !=''){
                unset($_SESSION['store_server'][0]);
                unset($_SESSION['store_server'][1]);
                unset($_SESSION['store_server'][2]);
                foreach($_FILES["files1"]["tmp_name"] as $key11=>$tmp_name) {
                    $target_path2 = "./img/store_img/";
                    $file_name1=$_FILES["files1"]["name"][$key11];
                    $file_tmp1=$_FILES["files1"]["tmp_name"][$key11];
                    $ext=pathinfo($file_name1,PATHINFO_EXTENSION);
                    $target_path2 = $target_path2 . basename( $getDate.$_FILES['files1']['name'][$key11]);
                    $url3 = str_replace(WEB_DIR, '', $target_path2);
                    $url3 = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url3);
                    $_SESSION['store_server'][$key11] = $url3;
                    //echo $_SESSION['store_server'][$key11] ."<br/>";
                    move_uploaded_file($file_tmp1=$_FILES["files1"]["tmp_name"][$key11],$target_path2);
                }
            }


        }

        if($_POST['t4'] == "4"){



            $sql="select * from member where user='".$_SESSION['store_mobile']."'";
            $result=mysqli_query($sql);
            $row = mysqli_fetch_array($result, MYSQL_ASSOC);
            if (!mysqli_num_rows($result)) {

                $insert_member = "INSERT INTO member (user,password)
                VALUES ('".$_SESSION['store_mobile']."','".$_SESSION['password']."')";
                mysqli_query($conn2, $insert_member);
                Db::rowSQL($insert_member);

                $sql1="select id_member from member where user='".$_SESSION['store_mobile']."'";
                $result = $conn2->query($sql1);
                while($row1 = $result->fetch_assoc()) {
                    $mem = $row1["id_member"];
                }
                //exit;
                // $insert_sql = "INSERT INTO appointed_store
                // (store_name,store_address_postcode,store_address_city,store_address_sub,store_address_details,store_phone,store_open_time,store_close_time,store_closed,store_product,store_coupons,store_details,store_website,store_category,store_business_number,store_law_rep,store_regi_address,store_tel,touch_people,store_mobile,store_email)
                // VALUES
                // ('".$_SESSION['store_name_new']."','".$_SESSION['store_address_postcode']."','".$_SESSION['store_address_city']."','".$_SESSION['store_address_sub']."','".$_SESSION['store_address_details']."','".$_SESSION['store_phone']."','".$_SESSION['store_open_time']."','".$_SESSION['store_close_time']."','".$_SESSION['store_closed']."','".$_SESSION['store_product']."','".$_SESSION['store_coupons']."','".$_SESSION['store_details']."','".$_SESSION['store_website']."','".$_SESSION['store_category']."','".$_SESSION['store_business_number']."','".$_SESSION['store_law_rep']."','".$_SESSION['store_regi_address']."','".$_SESSION['store_tel']."','".$_SESSION['touch_people']."','".$_SESSION['store_mobile']."','".$_SESSION['store_email']."')";
                //echo $insert_sql;
                $insert_sql = "INSERT INTO store (id_member,account,password,title,id_store_type,county,area,address1,tel1,close_time,service,discount,firm,legal_agent,address,contact_person,phone,email,introduction,id_admin,active,website,submit_date)
                VALUES
                ('".$mem."','".$_SESSION['store_mobile']."','".$_SESSION['password']."','".$_SESSION['store_name_new']."','".$_SESSION['store_category']."','".$_SESSION['store_address_city']."','".$_SESSION['store_address_sub']."','".$_SESSION['store_address_details']."','".$_SESSION['store_phone']."','".$_SESSION['store_closed']."','".$_SESSION['store_product']."','".$_SESSION['store_coupons']."','".$_SESSION['store_business_number']."','".$_SESSION['store_law_rep']."','".$_SESSION['store_regi_address']."','".$_SESSION['touch_people']."','".$_SESSION['store_mobile']."','".$_SESSION['store_email']."','".$_SESSION['store_details']."','1','0','".$_SESSION['store_website']."',now())";

                // echo $insert_sql;
                // exit;

        

        if ($db_link->query($insert_sql) === TRUE) {
            $last_id = $db_link->insert_id;
            //echo "New record created successfully. Last inserted ID is: " . $last_id;
          }
        //mysqli_query($db_link, $insert_sql);
        Db::rowSQL($insert_sql);

        $insert_img = "INSERT INTO appointed_store_img (appointed_store_id,img_logo,img_store_img1,img_store_img2,img_store_img3,img_product_img1,img_product_img2,img_product_img3)
        VALUES
        ('$last_id','".$_SESSION['store_logo']."','".$_SESSION['store_img'][0]."','".$_SESSION['store_img'][1]."','".$_SESSION['store_img'][2]."','".$_SESSION['store_server'][0]."','".$_SESSION['store_server'][1]."','".$_SESSION['store_server'][2]."')";
        //echo $insert_img;
        mysqli_query($db_link, $insert_img);
        Db::rowSQL($insert_img);


        }
        //         $insert_sql = "INSERT INTO appointed_store
        // (store_name,store_address_postcode,store_address_city,store_address_sub,store_address_details,store_phone,store_open_time,store_close_time,store_closed,store_product,store_coupons,store_details,store_website,store_category,store_business_number,store_law_rep,store_regi_address,store_tel,touch_people,store_mobile,store_email)
        // VALUES
        // ('".$_SESSION['store_name_new']."','".$_SESSION['store_address_postcode']."','".$_SESSION['store_address_city']."','".$_SESSION['store_address_sub']."','".$_SESSION['store_address_details']."','".$_SESSION['store_phone']."','".$_SESSION['store_open_time']."','".$_SESSION['store_close_time']."','".$_SESSION['store_closed']."','".$_SESSION['store_product']."','".$_SESSION['store_coupons']."','".$_SESSION['store_details']."','".$_SESSION['store_website']."','".$_SESSION['store_category']."','".$_SESSION['store_business_number']."','".$_SESSION['store_law_rep']."','".$_SESSION['store_regi_address']."','".$_SESSION['store_tel']."','".$_SESSION['touch_people']."','".$_SESSION['store_mobile']."','".$_SESSION['store_email']."')";
        // //echo $insert_sql;
        // mysqli_query($db_link, $insert_sql);
        // Db::rowSQL($insert_sql);

        $update_member = "UPDATE member set password='".$_SESSION['password']."' where user='".$_SESSION['store_mobile']."'";
        //echo $update_member;
        mysqli_query($conn2, $update_member);
        Db::rowSQL($update_member);




        $this->cleanSession();
        }



        $this->context->smarty->assign([
          'PGWD'   => $_SERVER['QUERY_STRING'],
          'application_page' => $application_page,
          'application_process' => $application_process,
          'js' =>$js,
          'select_type' => StoreApplicationController::getStoreType(),
        ]);

        parent::initProcess();
    }

    public function cleanSession(){
        unset($_SESSION['store_name_new']);
        unset($_SESSION['store_address_postcode']);
        unset($_SESSION['store_address_city']);
        unset($_SESSION['store_address_sub']);
        unset($_SESSION['store_address_details']);
        unset($_SESSION['store_phone']);
        unset($_SESSION['store_open_time']);
        unset($_SESSION['store_close_time']);
        unset($_SESSION['store_product']);
        unset($_SESSION['store_coupons']);
        unset($_SESSION['store_details']);
        unset($_SESSION['store_website']);
        unset($_SESSION['store_closed']);
        unset($_SESSION['store_category']);
        unset($_SESSION['store_business_number']);
        unset($_SESSION['store_law_rep']);
        unset($_SESSION['store_regi_address']);
        unset($_SESSION['store_tel']);
        unset($_SESSION['touch_people']);
        unset($_SESSION['store_mobile']);
        unset($_SESSION['store_email']);
        unset($_SESSION['password']);
        unset($_SESSION['store_logo']);
        unset($_SESSION['store_img'][0]);
        unset($_SESSION['store_img'][1]);
        unset($_SESSION['store_img'][2]);
        unset($_SESSION['store_server'][0]);
        unset($_SESSION['store_server'][1]);
        unset($_SESSION['store_server'][2]);
    }

    public function validateRules(){    //驗證基本資料的輸入
        $user = substr($_POST['user'], -9);
        $mobile = $_POST['user'];
        //$captcha = Tools::getValue('captcha');
        $captcha = $_POST['captcha'];
        $password = $_POST['password'];
        $sql_sms = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel='".$user."'";
        $row    = Db::rowSQL($sql_sms);
        $captcha_log = $row[0]['captcha']; //驗證碼

        $sql_check = "select * from member where user='".$mobile."'";
        $row1    = Db::rowSQL($sql_check);
        $check_mobile = $row1[0]['user'];

        if($check_mobile !='')
        {
            echo "<script>window.alert('此手機已經被註冊');window.location.href='https://okmaster.life/StoreApplication?';</script>";
        }


            if (empty($password)) {
                //$this->_errors[] = $this->l('您必須輸入驗證碼');
                echo "<script>window.alert('請輸入密碼');window.location.href='https://okmaster.life/StoreApplication?application_page=1';</script>";
            }
            if (empty($captcha)) {
                //$this->_errors[] = $this->l('您必須輸入驗證碼');
                echo "<script>window.alert('您必須輸入驗證碼');</script>";
            }else if($captcha != $captcha_log){
                //$this->_errors[] = $this->l('輸入驗證碼錯誤');
                echo "<script>window.alert('輸入驗證碼錯誤');window.location.href='https://okmaster.life/StoreApplication?application_page=1';</script>";

            }
        //}
        parent::validateRules();
    }



    public function setMedia()
    {
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addCSS('/themes/Rent/css/store.css');
        parent::setMedia();

        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS(THEME_URL .'/message/message.js');


        $this->addCSS(THEME_URL .'/css/head_item.css');
        $this->addCSS('/themes/Rent/css/search_house.css');
        $this->addJS('/themes/Rent/js/search_house_store.js');
        $this->addCSS( THEME_URL .'/css/all_page.css');
        $this->addCSS( THEME_URL .'/css/application.css');
    }

}
