<?php

namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Db;
use \File;
use \Tools;

class SterilizationController extends OkmasterController
{
    public $page = 'Sterilization';

//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->page_header_toolbar_title = $this->l('首頁');
		$this->className                 = 'SterilizationController';
		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
		parent::__construct();
	}

	public function initProcess()
	{
        $type = Tools::getValue("type");//種類
        $name = Tools::getValue("name");//姓名
        $sex = Tools::getValue("sex");//性別
//        $time = Tools::getValue("time");//連絡時段
        $tel = Tools::getValue("tel");//手機
        $message = Tools::getValue("message");//訊息
        $agree = Tools::getValue("agree");//同意
        $captcha = Tools::getValue("captcha");//驗證碼
        $id_member = $_SESSION['id_member'];//member id
        $put_data = Tools::getValue("put_data");//表示為送出資料
        $Response = '';//點選後的回應
        $sql = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($tel, 'text');
        $row    = Db::rowSQL($sql, true);
        $captcha_log = $row['captcha']; //驗證碼


        if((!empty($id_member) || (!empty($captcha) && ($captcha_log==$captcha))) && $put_data==1){
            if(!empty($agree) && !empty($name) && !empty($tel) && !empty($message) && !empty($agree)){//至少這些要有
                $sql = "INSERT INTO ilifehou_okmaster.`message` (`type`,`name`,`sex`,`tel`,`message`,`id_member`)
            VALUES (".GetSQL($type,"int").",".GetSQL($name,"text").",".GetSQL($sex,"int").",
           ".GetSQL($tel,"text").",".GetSQL($message,"text").",".GetSQL($id_member,"int").")";
                Db::rowSQL($sql);
                $Response = "留言成功";
            }else{
                $Response = "留言失敗 請在確認驗證碼";
            }
        }else if($put_data==1){//代表有送出 但是驗證碼為錯誤
            $Response = "留言失敗 請在確認驗證碼";
        }

        $js =<<<js
<script>
        $(document).ready(function(){
            var Response = '{$Response}';
            if(Response !='' && Response !=undefined){
               alert(Response);
            }
        });
</script>
js;

//        $banner = [
//            [
//                'img'=>'',
//            ],
//            [
//                'img'=>'',
//            ],
//            [
//                'img'=>'',
//            ],
//        ];
        $Breadcrumb                = '首頁 > 清淨對策 > 健康宅檢測';

        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/Sterilization/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data='';

		$this->context->smarty->assign([
            'slick_data'    =>['class'=>'slider_center','slidesToShow'=>'7','slidesToScroll'=>'7',],
            'footer_data'   =>$footer_data,
            'PGWD'   => $_SERVER['QUERY_STRING'],
            'Breadcrumb'   => $Breadcrumb,
            'js' =>$js,
		]);
		parent::initProcess();
	}


	public function setMedia()
	{
		parent::setMedia();
        $WD = substr(strtok(strchr($_SERVER['QUERY_STRING'],"&"),"="),1);
        $PGWD = $_SERVER['QUERY_STRING'];
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addCSS( THEME_URL .'/css/'.$PGWD.'.css');
        $this->addJS(THEME_URL .'/message/message.js');
        $this->addCSS( THEME_URL .'/css/all_page.css');
	}
}
