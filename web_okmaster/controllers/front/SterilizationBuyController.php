<?php

namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Db;
use \Tools;
use \SMS;
use \FileUpload;
use \Context;
use \PHPMailer;

class SterilizationBuyController extends OkmasterController
{
    public $page = 'SterilizationBuy';

//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->page_header_toolbar_title = $this->l('首頁');
		$this->className                 = 'SterilizationBuyController';
//		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
        //$this->conn ='ilifehou_okmaster';
        $this->conn ='ilifehou_ilife_house';
        $this->table = 'SterilizationBuy_Product';
        
		parent::__construct();
  }

  public function orderConfirm($orderID2){ //發送訂單完成簡訊
    $mobile = $_POST['user'];
    $captcha = Tools::getValue('captcha');
    $text_sms = "超立淨系列隨身寶禮盒體驗組 訂單已完成，您的訂單號碼為: ".$orderID2."。 若需取消訂單請至以下連結取消訂單：https://www.okmaster.life/SterilizationBuy?order=search&orderId=".$orderID2."";
    $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
  }
  
  
  

	public function initProcess()
	{

    $order_search = Tools::getValue('order');
    $search_orderid = Tools::getValue('orderId');

    $sql = "SELECT * FROM SterilizationBuy_Product ORDER BY ProductID ASC";
    $product = Db::rowSQL($sql);

    //print_r(array_values($product));
    
    $productID = $product[0]["ProductNumber"];
    $productNumber = $product[0]["ProductNumber"];
    $productTitle = $product[0]["ProductTitle"];
    $productInfo = $product[0]["ProductInfo"];
    $productDescription = $product[0]["ProductDescription"];
    $productPrice = $product[0]["ProductPrice"];
    $productBonus = $product[0]["ProductBonus"];

    


    


        $js =<<<js
<script>
$(document).ready(function(){
  $("#_da").click(function(){
    $(".dialogbox").show(100);
    $(".dialogboxbk").show(100);
  });
  $("#cancel").click(function(){
    $(".dialogbox").hide(300);
    $(".dialogboxbk").hide(300);
  });
  $(".upload").click(function(){
      $(".fileupload").click();
  });
})
</script>
js;
        $Breadcrumb                = "";//'首頁 > 生活好科技 > 清淨對策 > 關於我們';
        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/Aboutokmaster/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/Aboutokmaster/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data='';

    $order="";
    $order_price="";
    if($_POST['pr_q']){
      $order=$_POST['pr_q'];
      $order_price=$_POST['pr_price']; // 對應 index.pl 裡面的 pr_price

    }


    $getDate = substr(date("Ymd"), -6);
    
    //$getDate= date("Ymd"); //訂單編號 OrderID2
    $randnew = rand(1000,9999);//生成隨機的4碼 OrderID2 用 $getDate + $randnew 串起來
    $new_order_id = "8285".$getDate.$randnew;

    $paymethod = $_POST['pay'];
    $dleveilyfee = $_POST['dleveily'];
    if($dleveilyfee =="1" || $dleveilyfee =="2"){
      $deliveryfee ="60";
    } else if($dleveilyfee =="3" || $dleveilyfee =="4"){
      $deliveryfee ="70";
    } else{
      $deliveryfee ="65";
    }

    $db_link = mysqli_connect("localhost", "ilifehou_ilife", "S^,HO%bh^$&NF3dab", "ilifehou_ilife_house");

    $captcha = Tools::getValue('captcha');
    
    if($_POST['checkorder'] == "1"){

      $this->validateRules();

      if($_POST['payway'] == "1"){
        $insert_sql = "INSERT INTO 
        SterilizationBuy_Order (`OrderID2`,`ProductID`,`OrderUserName`,`OrderUserEmail`,`OrderUserPhone`,`OrderUserAddress`,`OrderUserIdentity`,`OrderEachPrice`,`OrderEachAmount`,`OrderTotalPrice`,`OrderMessage`,`OrderTotalAmount`,`OrderMuiltPrice`,`CreditCard`,`PCYM`,`CVS`,`PaymentATMPeriod`,`PaymentATMMoney`,`PaymentType`,`PaymentFlag`,`PickUpStore`,`PickUpName`,`PickUpPhone`,`PickUpRecipt`,`PickupReciptName`,`PickupReciptNum`) 
        VALUES 
        ('$new_order_id','$_POST[productID]','$_POST[username]','$_POST[email]','$_POST[user]','$_POST[address]','$_POST[Identity]','$_POST[productPrice]','$_POST[order]','$_POST[totalPrice]','$_POST[message]','$_POST[order_item_q]','$_POST[order_price_total]','$_POST[cd1]-$_POST[cd2]-$_POST[cd3]-$_POST[cd4]','$_POST[yearmonth]','$_POST[cvs]','0','0','$_POST[payway]','0','$_POST[delway]','$_POST[pickusername]','$_POST[pickphone]','$_POST[receipt]','$_POST[receiptname]','$_POST[receiptname]')";
      } else if($_POST['payway'] == "2" || $_POST['payway'] == "3" || $_POST['payway'] == "5"){
        $insert_sql = "INSERT INTO 
        SterilizationBuy_Order (`OrderID2`,`ProductID`,`OrderUserName`,`OrderUserEmail`,`OrderUserPhone`,`OrderUserAddress`,`OrderUserIdentity`,`OrderEachPrice`,`OrderEachAmount`,`OrderTotalPrice`,`OrderMessage`,`OrderTotalAmount`,`OrderMuiltPrice`,`CreditCard`,`PCYM`,`CVS`,`PaymentATMPeriod`,`PaymentATMMoney`,`PaymentType`,`PaymentFlag`,`PickUpStore`,`PickUpName`,`PickUpPhone`,`PickUpRecipt`,`PickupReciptName`,`PickupReciptNum`) 
        VALUES 
        ('$new_order_id','$_POST[productID]','$_POST[username]','$_POST[email]','$_POST[user]','$_POST[address]','$_POST[Identity]','$_POST[productPrice]','$_POST[order]','$_POST[totalPrice]','$_POST[message]','$_POST[order_item_q]','$_POST[order_price_total]','0','0','0','0','0','$_POST[payway]','0','$_POST[delway]','$_POST[pickusername]','$_POST[pickphone]','$_POST[receipt]','$_POST[receiptname]','$_POST[receiptname]')";
       
      }  else if($_POST['payway'] == "4") {
        $insert_sql = "INSERT INTO 
        SterilizationBuy_Order (`OrderID2`,`ProductID`,`OrderUserName`,`OrderUserEmail`,`OrderUserPhone`,`OrderUserAddress`,`OrderUserIdentity`,`OrderEachPrice`,`OrderEachAmount`,`OrderTotalPrice`,`OrderMessage`,`OrderTotalAmount`,`OrderMuiltPrice`,`CreditCard`,`PCYM`,`CVS`,`PaymentATMPeriod`,`PaymentATMMoney`,`PaymentType`,`PaymentFlag`,`PickUpStore`,`PickUpName`,`PickUpPhone`,`PickUpRecipt`,`PickupReciptName`,`PickupReciptNum`) 
        VALUES 
        ('$new_order_id','$_POST[productID]','$_POST[username]','$_POST[email]','$_POST[user]','$_POST[address]','$_POST[Identity]','$_POST[productPrice]','$_POST[order]','$_POST[totalPrice]','$_POST[message]','$_POST[order_item_q]','$_POST[order_price_total]','0','0','0','$_POST[ped]','0','$_POST[payway]','0','$_POST[delway]','$_POST[pickusername]','$_POST[pickphone]','$_POST[receipt]','$_POST[receiptname]','$_POST[receiptname]')";
       
      } 

      
      $payway_check = $_POST['payway'];

    

      $db_link->query($insert_sql);
      $order_last_id = mysqli_insert_id($db_link);

      $order_confirm = "order_confirm";

      $sql_lastid = "SELECT OrderID2 FROM SterilizationBuy_Order where OrderID = '".$order_last_id."'";
      $order_last = Db::rowSQL($sql_lastid);
      
      $orderID2 = $order_last[0]["OrderID2"];


      $mail= new PHPMailer();
      $mail->setLanguage('zh', 'language/phpmailer.lang-zh');
      $mail->CharSet='utf-8';
      try {
          $mail->isSMTP();                                            //Send using SMTP
          $mail->Host       = 'mail.lifegroup.house';                     //Set the SMTP server to send through
          $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
          $mail->Username   = 'ilifehou';                     //SMTP username
          $mail->Password   = 'ilifE@hOuse318568';                               //SMTP password
          $mail->Port       = 587;                                    //TCP port to connect to, use 465 for PHPMailer::ENCRYPTION_SMTPS above
          //$send_mail = 'ben@lifegroup.house';
          //Recipients
          //print_r($_POST['email']);
          $mail->From  = 'ok@lifegroup.house';
          $mail->FromName = 'LifeGroup';
          $mail->addAddress(''.$_POST['email'].'', 'Ben');     //Add a recipient
          //$mail->addAddress('ellen@example.com');               //Name is optional
          //$mail->addReplyTo('info@example.com', 'Information');
          //$mail->addCC('cc@example.com');
          //$mail->addBCC('bcc@example.com');

          //Attachments
          //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
          //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

          //Content
          $mail->isHTML(true);                                  //Set email format to HTML
          $mail->Subject = '生活好科技：超立淨系列隨身寶禮盒體驗組 - 您的訂單已完成';
          $mail->Body    = '生活好科技：超立淨系列隨身寶禮盒體驗組 - 您的訂單已完成<br/>訂單編號為：'.$orderID2.'，若要取消訂單，請點以下連結：https://www.okmaster.life/SterilizationBuy?order=search&orderId='.$orderID2;
          //$mail->AltBody = '生活好科技';

          $mail->send();
          //echo 'Message has been sent';
      } catch (Exception $e) {
          //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
      }

      $this->orderConfirm($orderID2);


        $this->context->smarty->assign([
          'order_confirm' => $order_confirm,
          'payway_check' => $payway_check,
          'orderID2' => $orderID2,
        ]);


    } else {

    }

    
    //print_r($search_orderid);
    if($order_search == "search"){
      if($_POST['oocheck'] == '1'){

        $searchphone = $_POST['searchphone'];
        $searchorderid = $_POST['searchorderid'];

        $sql_order_search = "SELECT * FROM SterilizationBuy_Order where OrderUserPhone='".$searchphone."' and OrderID2 ='".$searchorderid."' and PaymentFlag <> '2'";
        $search_details = Db::rowSQL($sql_order_search);
        //print_r($sql_order_search);

        
        $detail_orderID2 = $search_details[0]["OrderID2"];

        if($detail_orderID2 == ''){
          echo "<script>window.alert('未找尋到此訂單');</script>";
          $this->context->smarty->assign([
            'order_search' => $order_search,
        ]);
        }
        $search_productid = $search_details[0]["ProductID"];
        $detail_OrderCreateTime = $search_details[0]["OrderCreateTime"];
        $detail_OrderUserName = $search_details[0]["OrderUserName"];
        $detail_OrderUserPhone = $search_details[0]["OrderUserPhone"];
        $detail_PaymentFlag = $search_details[0]["PaymentFlag"];
        $detail_OrderEachAmountt = $search_details[0]["OrderEachAmount"];
        $detail_OrderTotalPrice = $search_details[0]["OrderTotalPrice"];
        

        if ($detail_PaymentFlag == "0"){
          $detail_PaymentFlag = "未付款";
        } else {
          $detail_PaymentFlag = "已付款";
        }
        $detail_PickUpStore = $search_details[0]["PickUpStore"];
        if ($detail_PickUpStore == "1"){
          $detail_PickUpStore = "宅配";
        } else if ($detail_PickUpStore == "2"){
          $detail_PickUpStore = "郵寄";
        } else if ($detail_PickUpStore == "3"){
          $detail_PickUpStore = "7-11";
        } else if ($detail_PickUpStore == "4"){
          $detail_PickUpStore = "全家";
        } else if ($detail_PickUpStore == "5"){
          $detail_PickUpStore = "萊爾富";
        } else if ($detail_PickUpStore == "6"){
          $detail_PickUpStore = "OK 超商";
        } else if ($detail_PickUpStore == "7"){
          $detail_PickUpStore = "超商取貨";
        } else {
          $detail_PickUpStore = "未定義";
        }
        $detail_OrderMessage = $search_details[0]["OrderMessage"];


        $sql_product_detail = "SELECT * FROM SterilizationBuy_Product where ProductNumber='".$search_productid."'";
        $produce_details = Db::rowSQL($sql_product_detail);
        //print_r($sql_product_detail);
        
        $product_title = $produce_details[0]["ProductTitle"];
        $product_info = $produce_details[0]["ProductInfo"];
        $product_price = $produce_details[0]["ProductPrice"];

        $order_search_confire = "order_search_confire";
          $this->context->smarty->assign([
            'order_search_confire' => $order_search_confire,
            'detail_orderID2' => $detail_orderID2,
            'product_title' => $product_title,
            'product_info' => $product_info,
            'product_price' => $product_price,
            'detail_OrderTotalPrice' => $detail_OrderTotalPrice,
            'detail_OrderEachAmountt' => $detail_OrderEachAmountt,
            'detail_OrderCreateTime' => $detail_OrderCreateTime,
            'detail_OrderUserName' => $detail_OrderUserName,
            'detail_OrderUserPhone' => $detail_OrderUserPhone,
            'detail_PaymentFlag' => $detail_PaymentFlag,
            'detail_PickUpStore' => $detail_PickUpStore,
            'detail_OrderMessage' => $detail_OrderMessage,
        ]);
      } else {
        $this->context->smarty->assign([
          'order_search' => $order_search,
          'search_orderid' => $search_orderid,
      ]);
      }
    }    

    if($_POST['ccancel'] == "OK"){
      $sql_order_delete = "Update SterilizationBuy_Order SET PaymentFlag ='2' where OrderID2 ='".$_POST['cancel']."'";
      $order_delete = Db::rowSQL($sql_order_delete);
      //print_r($sql_order_delete);
      //echo "<script>window.alert('訂單已取消');</script>";
      $order_cancel = "order_cancel";
      $this->context->smarty->assign([
        'order_cancel' => $order_cancel,
    ]);
    
    }


    
    


		$this->context->smarty->assign([
            'slick_data'    =>['class'=>'slider_center','slidesToShow'=>'7','slidesToScroll'=>'7',],
            'footer_data'   =>$footer_data,
            'PGWD'   => $_SERVER['QUERY_STRING'],
            'Breadcrumb'   => $Breadcrumb,
            'js' =>$js,
            'order' => $order,
            'productID' => $productID,
            'productNumber' => $productNumber,
            'productTitle' => $productTitle,
            'productInfo' => $productInfo,
            'productBonus' => $productBonus,
            'productDescription' => $productDescription,
            'productPrice' => $productPrice,
            'order_price' => $order_price,
            'order_item_q' => sizeof($_POST['pr_q']),
            'order_price_total' => $order_price*$order,
            'paymethod' => $paymethod ,
            'deliveryfee' => $deliveryfee,
            
    ]);
    


		parent::initProcess();
  }
  
  public function validateRules(){    //驗證基本資料的輸入
    $user = substr($_POST['user'], -9);
    $mobile = $_POST['user'];
    
    $captcha = Tools::getValue('captcha');
    
    

    $sql_sms = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel='".$user."'";
    $row    = Db::rowSQL($sql_sms);
    $captcha_log = $row[0]['captcha']; //驗證碼
    if($oocheck =="1"){
      if (empty($captcha)) {
          //$this->_errors[] = $this->l('您必須輸入驗證碼');
          echo "<script>window.alert('您必須輸入驗證碼');</script>";
      }else if($captcha != $captcha_log){
          //$this->_errors[] = $this->l('輸入驗證碼錯誤');
          echo "<script>window.alert('輸入驗證碼錯誤');window.location.href='https://www.okmaster.life/SterilizationBuy';</script>";

      }
    }
    

    parent::validateRules();
    
}

	public function setMedia()
	{
		parent::setMedia();
        $WD = substr(strtok(strchr($_SERVER['QUERY_STRING'],"&"),"="),1);
        $PGWD = $_SERVER['QUERY_STRING'];

        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addCSS( THEME_URL .'/css/'.$PGWD.'.css');
        $this->addJS(THEME_URL .'/message/message.js');
        $this->addCSS( THEME_URL .'/css/all_page.css');
        $this->addCSS( THEME_URL .'/css/SterilizationBuy.css');

        $this->addCSS( '/themes/LifeHouse/css/life_go_tab.css');
        $this->addCSS( '/themes/LifeHouse/css/life_go_main.css');
        $this->addCSS( '/themes/LifeHouse/css/lifegoproduct.css');
        $this->addCSS( '/themes/LifeHouse/css/main.css');
        $this->addCSS('/css/metro-all.min.css');



	}
}
