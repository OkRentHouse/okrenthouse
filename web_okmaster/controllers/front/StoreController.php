<?php

namespace web_okmaster;
use \OkmasterController;
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;

class StoreController extends OkmasterController
{

    public $page = 'store';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;
    public function __construct()
    {
        $this->className = 'StoreController';
        $this->meta_title                = $this->l('生活好康');
        $this->page_header_toolbar_title = $this->l('生活好康');
        $this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
        $this->no_FormTable              = true;
        $this->table = 'store';

        parent::__construct();
    }

    //初始化麵包屑        //參考


    public function getStoreType($type = 'all'){
        $sql = 'SELECT * FROM `store_type` ORDER BY `position` ASC';
        $arr = Db::rowSQL($sql);
        return $arr;
    }

    public function initProcess(){
        $country =Tools::getValue("county");
        $area =Tools::getValue("city_s");
        $type =Tools::getValue("id_store_type_s");
        $title = Tools::getValue('search');
//        print_r($_GET);

//        $_GET["id_store_type_s"] = $type;
        $arr_store = Store::getContext()->get(null, 1, 30,$country,$area,$type,'head',$title);
        $pagination = Db::getContext()->getJumpPage(5);


        //先實驗性寫 favorite相關的start  OK
        $id_member = $_SESSION["id_member"];
        $session_id = session_id();
        $favorite_data = "";
        if(!empty($id_member)){//有
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND is_member=".GetSQL($id_member,"int");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個is_member
        }else{//只有session_id
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND session_id=".GetSQL($session_id,"text");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個session_id
        }

//        print_r($favorite_data);

        //由於要塞入$favorite_data進去因而要用兩層foreach

        foreach($arr_store as $k => $v){
            foreach($favorite_data as $key => $value){
                if($arr_store[$k]["id_store"] == $favorite_data[$key]["table_id"]){//上面搜索了該table與本次使用者的資訊 這邊濾同id
                    $arr_store[$k]["favorite"] = $favorite_data[$key]["active"];
                }
            }
        }

        //end

        foreach ($arr_store as $i => $v) {
            // $arr_img                         = Store::getContext()->get_store_file_url($v['id_store'],0);
            $arr_img_get="";
            $arr_img                         = Store::getContext()->get_store_file_url($v['id_store'],0);if($arr_img[0]!="" && $arr_img_get==""){$arr_img_get=$arr_img[0];}
            $arr_img                         = Store::getContext()->get_store_file_url($v['id_store'],1);if($arr_img[0]!="" && $arr_img_get==""){$arr_img_get=$arr_img[0];}
            $arr_img                         = Store::getContext()->get_store_file_url($v['id_store'],2);if($arr_img[0]!="" && $arr_img_get==""){$arr_img_get=$arr_img[0];}


            // $arr_store[$i]['img']            = $arr_img[0];
            $arr_store[$i]['img']            = $arr_img_get;
            $arr_store[$i]['discount']       = array_filter(explode(',', $v['discount']));
            $arr_store[$i]['service']        = array_filter(explode(',', $v['service']));
        }



        $js =<<<js
	$(".class_select").click(function () {
		$(".category").show();
	});

	$(document).click(function (e) {
		if (!$(e.target).hasClass("class_select") && !$(e.target).hasClass("category")
			&& $(e.target).parents(".category").length === 0) {
			$(".category").hide();
		}
	});
		$('input[name="id_store_type[]"]').click(function(){
	    var id_store_type_text='';
        for (var i=0;i<document.getElementsByName('id_store_type[]').length;i++){
            if(document.getElementsByName('id_store_type[]')[i].checked){
                var id_store_type_val = document.getElementsByName('id_store_type[]')[i].value;
                if(id_store_type_text==''){
                    id_store_type_text = document.getElementById('store_type_name_'+id_store_type_val).innerHTML;
                }else{
                    id_store_type_text = id_store_type_text+','+document.getElementById('store_type_name_'+id_store_type_val).innerHTML;
                }
            }
         }
        $("#id_store_type_name").text(id_store_type_text);
        $("#id_store_type_name").append('<i class="fas fa-chevron-down"></i>');
});

		$('#id_store_type').click(function(){
            $("#id_store_type_name").text('全部');
            $("#id_store_type_name").append('<i class="fas fa-chevron-down"></i>');
});

js;
        $ip = $_SERVER['REQUEST_URI'];//頁籤active判斷

        $this->context->smarty->assign([
            'arr_store'              => $arr_store,
            'select_county'          => RentHouse::getContext()->getCounty('none'),
            'arr_city'               => RentHouse::getContext()->getCity('none'),
            'select_type'            => StoreController::getStoreType(),
            'js'                     =>  $js,
            'ip'                     =>  $ip,
            'Breadcrumb' => '首頁 > '.$this->page_header_toolbar_title,
        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addCSS('/themes/Rent/css/store.css');
        parent::setMedia();

        $this->addCSS(THEME_URL .'/css/head_item.css');
        $this->addCSS('/themes/Rent/css/search_house.css');
        $this->addJS('/themes/Rent/js/search_house_store.js');
        $this->addCSS( THEME_URL .'/css/all_page.css');
    }
}
