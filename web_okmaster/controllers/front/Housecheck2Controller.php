<?php

namespace web_okmaster; 
use \OkmasterController;

class Housecheck2Controller extends OkmasterController
{

    public $page = 'Housecheck2';
    public function __construct()
    {

        $arr_store = "Housecheck2";

        parent::__construct();
    }

    public function initProcess(){


        $this->context->smarty->assign([
            'arr_store'              => $arr_store,
        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addCSS('/themes/Rent/css/store.css');
        parent::setMedia();

        $this->addCSS(THEME_URL .'/css/head_item.css');
        $this->addCSS('/themes/Rent/css/search_house.css');
        $this->addJS('/themes/Rent/js/search_house_store.js');
        $this->addCSS( THEME_URL .'/css/all_page.css');
    }
}
