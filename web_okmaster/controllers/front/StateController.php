<?php

namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Db;
use \File;
use \Tools;

class StateController extends OkmasterController
{
    public $page = 'State';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;
//      public $fb_app_id = '1319773841697579';
//    public $fb_api_version = 'v9.0';


    public function __construct()
    {
      $this->page_header_toolbar_title = $this->l('首頁');
  		$this->className                 = 'State';
  //		$this->no_FormTable              = true;
  		$this->display_header            = false;//首頁不用共用表頭
          $this->display_footer            = false;
          $this->conn ='ilifehou_okmaster';
        parent::__construct();
    }



	public function initProcess()
	{




    $css =<<<js

    <style>

    p {
      width: 100%;
    }

    .main {
    width: 1280px;
    margin: auto;
    }

    .head_group {
        width: 100%;
    }

    .head {
        display: flex;
            color: #ccc;
            margin-top: 20px;
            z-index: 9;
    }
    .head .logo {
        height: 80px;
        overflow: hidden;
        background: url('/img/logos/logo-b-03.png');
        background-repeat: no-repeat;
        background-position: center;
        width: 20%;
        cursor: pointer;
    }
    .head .logoxb {
        height: 80px;
        overflow: hidden;
        background-repeat: no-repeat;
        background-position: center;
        width: 20%;
    }
    .head .hyperlink {
        width: 70%;
        text-align: center;
    }
    .head .hyperlink label {
      vertical-align: bottom;
      padding-top: 48px;
      font-size: 14pt;
      padding-left: 10px;
      position: relative;
      display: inline-block;
      margin: 0rem;
    }
    .head .hyperlink label .l_2 {
      position: absolute;
    width: 15em;
    text-align: start;
    display: flex;
    color: #999;
    }
    .head .hyperlink label .l_2 label {
        height: 12pt;
    }
    .head .hyperlink label.wall:after {
      content: " ";
      border-right: 1px solid #ccc;
      padding-right: 10px;
      display: unset;
    }
    .head .hyperlink label .dropdown-toggle {
        padding: 0px !important;
            color: #666;
    }
    .head .hyperlink .d-menu {
        margin-top: 22px;
        margin-left: -50px;
    }
    .dropdown-toggle::before {
        content: none;
    }
    .head .hyperlink .d-menu a {
        font-size: 14pt;
    }
    .head .logon {
        width: 15%;
        margin: auto;
        text-align: center;
    }
    .head .share {
        width: 5%;
        text-align: center;
        margin: auto;
    }
    .head .share i{
      font-size: 16pt;
    }

    .h-menu.now_here:before {
        background: rgb(230 245 195);
        width: -webkit-fill-available;
        content: " ";
        height: 440px;
        position: absolute;
        left: 0px;
        bottom: -15px;
        z-index: 0;
        margin-right: -10px;
    }

    </style>

js;

    $Breadcrumb                = '首頁 > 相關聲明';
    $footer_data='<div class="footer"><img src="/themes/Okmaster/img/Sterilization/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
    $footer_data='';


		$this->context->smarty->assign([
      'slick_data'    =>['class'=>'slider_center','slidesToShow'=>'7','slidesToScroll'=>'7',],
      'footer_data'   =>$footer_data,
      'PGWD'   => $_SERVER['QUERY_STRING'],
      'Breadcrumb'   => $Breadcrumb,
            'css'=>$css,
		]);

		parent::initProcess();

	}





	public function setMedia(){
    parent::setMedia();
    $WD = substr(strtok(strchr($_SERVER['QUERY_STRING'],"&"),"="),1);
    $PGWD = $_SERVER['QUERY_STRING'];
    $this->addCSS('/css/metro-all.min.css');
    $this->addJS('/js/metro.min.js');
    $this->addCSS('/css/font-awesome.css');
    $this->addCSS('/css/fontawesome.css');
    $this->addCSS('/css/all.css');
    $this->addCSS( THEME_URL .'/css/'.$PGWD.'.css');
    $this->addJS(THEME_URL .'/message/message.js');
    $this->addCSS( THEME_URL .'/css/all_page.css');
	}
}
