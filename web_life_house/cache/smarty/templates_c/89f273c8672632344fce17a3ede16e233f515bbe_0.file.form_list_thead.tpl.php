<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:35:15
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/ShowNews/form/form_list_thead.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599a3345fb91_37827128',
  'file_dependency' => 
  array (
    '89f273c8672632344fce17a3ede16e233f515bbe' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/ShowNews/form/form_list_thead.tpl',
      1 => 1592288974,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60599a3345fb91_37827128 ($_smarty_tpl) {
?>
<thead> <tr><?php
$_from = $_smarty_tpl->tpl_vars['fields']->value['list'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_thead_0_saved_item = isset($_smarty_tpl->tpl_vars['thead']) ? $_smarty_tpl->tpl_vars['thead'] : false;
$__foreach_thead_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['thead'] = new Smarty_Variable();
$__foreach_thead_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_thead_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['thead']->value) {
$__foreach_thead_0_saved_local_item = $_smarty_tpl->tpl_vars['thead'];
?><th class="<?php echo $_smarty_tpl->tpl_vars['thead']->value['class'];
if ($_smarty_tpl->tpl_vars['thead']->value['hidden']) {?> hidden<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['width']) && !empty($_smarty_tpl->tpl_vars['thead']->value['width'])) {?> width="<?php echo $_smarty_tpl->tpl_vars['thead']->value['width'];?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['thead']->value['title'];?>
<div class="th_title"><?php echo $_smarty_tpl->tpl_vars['thead']->value['title'];?>
</div><?php echo $_smarty_tpl->tpl_vars['thead']->value['search'];?>
</th><?php
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_0_saved_local_item;
}
}
if ($__foreach_thead_0_saved_item) {
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_0_saved_item;
}
if ($__foreach_thead_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_thead_0_saved_key;
}
if ($_smarty_tpl->tpl_vars['has_actions']->value && $_smarty_tpl->tpl_vars['display_edit_div']->value) {?><th class="text-center edit_div"><?php echo l(array('s'=>'編輯'),$_smarty_tpl);?>
<div class="th_title"><?php echo l(array('s'=>'編輯'),$_smarty_tpl);?>
</div></th><?php }?></tr><?php if (count($_smarty_tpl->tpl_vars['fields']->value['list_filter']) > 0) {?><tr class="search"><?php
$_from = $_smarty_tpl->tpl_vars['fields']->value['list'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_thead_1_saved_item = isset($_smarty_tpl->tpl_vars['thead']) ? $_smarty_tpl->tpl_vars['thead'] : false;
$__foreach_thead_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['thead'] = new Smarty_Variable();
$__foreach_thead_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_thead_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['thead']->value) {
$__foreach_thead_1_saved_local_item = $_smarty_tpl->tpl_vars['thead'];
?><th class="<?php echo $_smarty_tpl->tpl_vars['thead']->value['class'];
if ($_smarty_tpl->tpl_vars['thead']->value['hidden']) {?> hidden<?php }
if (strpos($_smarty_tpl->tpl_vars['thead']->value['filter'],'range') > -1) {?> range<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['width']) && !empty($_smarty_tpl->tpl_vars['thead']->value['width'])) {?> width="<?php echo $_smarty_tpl->tpl_vars['thead']->value['width'];?>
"<?php }?>><?php if (in_array($_smarty_tpl->tpl_vars['i']->value,$_smarty_tpl->tpl_vars['fields']->value['list_filter']) && $_smarty_tpl->tpl_vars['thead']->value['filter']) {
if ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'date_range') {?><input type="date" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][0];?>
" placeholder="<?php echo l(array('s'=>"最小"),$_smarty_tpl);?>
"/><div class="to"><?php echo l(array('s'=>"至"),$_smarty_tpl);?>
</div><input type="date" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][1];?>
" placeholder="<?php echo l(array('s'=>"最大 "),$_smarty_tpl);?>
"/><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'time_range') {?><input type="time" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][0];?>
" placeholder="<?php echo l(array('s'=>"最小"),$_smarty_tpl);?>
"/><div class="to"><?php echo l(array('s'=>"至"),$_smarty_tpl);?>
</div><input type="time" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][1];?>
" placeholder="<?php echo l(array('s'=>"最大 "),$_smarty_tpl);?>
"/><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'datetime_range') {?><input type="datetime-local" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][0];?>
" placeholder="<?php echo l(array('s'=>"最小"),$_smarty_tpl);?>
"/><div class="to"><?php echo l(array('s'=>"至"),$_smarty_tpl);?>
</div><input type="datetime-local" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][1];?>
" placeholder="<?php echo l(array('s'=>"最大"),$_smarty_tpl);?>
"/><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'date_range') {?><input type="date" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][0];?>
" placeholder="<?php echo l(array('s'=>"最小"),$_smarty_tpl);?>
"/><div class="to"><?php echo l(array('s'=>"至"),$_smarty_tpl);?>
</div><input type="date" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][1];?>
" placeholder="<?php echo l(array('s'=>"最大"),$_smarty_tpl);?>
"/><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'range') {?><input type="number" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][0];?>
"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['step'])) {?> step="<?php echo $_smarty_tpl->tpl_vars['thead']->value['step'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['min'])) {?> min="<?php echo $_smarty_tpl->tpl_vars['thead']->value['min'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['max'])) {?> max="<?php echo $_smarty_tpl->tpl_vars['thead']->value['max'];?>
"<?php }?> placeholder="<?php echo l(array('s'=>"最小"),$_smarty_tpl);?>
"/><div class="to"><?php echo l(array('s'=>"至"),$_smarty_tpl);?>
</div><input type="number" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'][1];?>
"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['step'])) {?> step="<?php echo $_smarty_tpl->tpl_vars['thead']->value['step'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['min'])) {?> min="<?php echo $_smarty_tpl->tpl_vars['thead']->value['min'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['max'])) {?> max="<?php echo $_smarty_tpl->tpl_vars['thead']->value['max'];?>
"<?php }?> placeholder="<?php echo l(array('s'=>"最大 "),$_smarty_tpl);?>
"/><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'time') {?><input type="time" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'];?>
" placeholder="<?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
"/><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'datetime') {?><input type="datetime-local" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'];?>
" placeholder="<?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
"/><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'date') {?><input type="date" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'];?>
" placeholder="<?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
"/><?php } elseif ((isset($_smarty_tpl->tpl_vars['thead']->value['key']) || isset($_smarty_tpl->tpl_vars['thead']->value['values']) || $_smarty_tpl->tpl_vars['thead']->value['title'] == 'active') && ($_smarty_tpl->tpl_vars['thead']->value['filter'] !== 'text')) {?><select id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;
if (isset($_smarty_tpl->tpl_vars['thead']->value['multiple']) && !empty($_smarty_tpl->tpl_vars['thead']->value['multiple'])) {?>[]<?php }?>" size="<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['size']) && !empty($_smarty_tpl->tpl_vars['thead']->value['size'])) {
echo $_smarty_tpl->tpl_vars['thead']->value['size'];
}?>" class="form-control<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['multiple']) && !empty($_smarty_tpl->tpl_vars['thead']->value['multiple'])) {?> selectpicker<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['multiple']) && !empty($_smarty_tpl->tpl_vars['thead']->value['multiple'])) {?> multiple="multiple" data-actions-box="true" data-select-all-text="<?php echo l(array('s'=>"全選"),$_smarty_tpl);?>
" data-deselect-all-text="<?php echo l(array('s'=>"取消"),$_smarty_tpl);?>
" data-none-selected-text="<?php echo l(array('s'=>"全選"),$_smarty_tpl);?>
" data-actions-box="true" data-selected-text-format="count > 1" <?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['thead']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_thead_data_2_saved_item = isset($_smarty_tpl->tpl_vars['thead_data']) ? $_smarty_tpl->tpl_vars['thead_data'] : false;
$__foreach_thead_data_2_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['thead_data'] = new Smarty_Variable();
$__foreach_thead_data_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_thead_data_2_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['thead_data']->value) {
$__foreach_thead_data_2_saved_local_item = $_smarty_tpl->tpl_vars['thead_data'];
?> data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['thead_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['thead_data'] = $__foreach_thead_data_2_saved_local_item;
}
}
if ($__foreach_thead_data_2_saved_item) {
$_smarty_tpl->tpl_vars['thead_data'] = $__foreach_thead_data_2_saved_item;
}
if ($__foreach_thead_data_2_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_thead_data_2_saved_key;
}
}?>><?php if (empty($_smarty_tpl->tpl_vars['thead']->value['multiple'])) {?><option value=""<?php if ($_smarty_tpl->tpl_vars['thead']->value['string_format'][0] == '' && count($_smarty_tpl->tpl_vars['thead']->value['string_format']) == 1) {?> selected="selected"<?php }?>><?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
</option><?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['key'])) {
$_from = $_smarty_tpl->tpl_vars['thead']->value['values'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_fv_3_saved_item = isset($_smarty_tpl->tpl_vars['fv']) ? $_smarty_tpl->tpl_vars['fv'] : false;
$__foreach_fv_3_saved_key = isset($_smarty_tpl->tpl_vars['fi']) ? $_smarty_tpl->tpl_vars['fi'] : false;
$_smarty_tpl->tpl_vars['fv'] = new Smarty_Variable();
$__foreach_fv_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_fv_3_total) {
$_smarty_tpl->tpl_vars['fi'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['fi']->value => $_smarty_tpl->tpl_vars['fv']->value) {
$__foreach_fv_3_saved_local_item = $_smarty_tpl->tpl_vars['fv'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['fv']->value['value'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['multiple']) && !empty($_smarty_tpl->tpl_vars['thead']->value['multiple'])) {
$_from = $_smarty_tpl->tpl_vars['thead']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_s_f_4_saved_item = isset($_smarty_tpl->tpl_vars['s_f']) ? $_smarty_tpl->tpl_vars['s_f'] : false;
$_smarty_tpl->tpl_vars['s_f'] = new Smarty_Variable();
$__foreach_s_f_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_s_f_4_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['s_f']->value) {
$__foreach_s_f_4_saved_local_item = $_smarty_tpl->tpl_vars['s_f'];
if (sprintf("%s",$_smarty_tpl->tpl_vars['s_f']->value) === sprintf("%s",$_smarty_tpl->tpl_vars['fv']->value['value'])) {?> selected="selected"<?php }
$_smarty_tpl->tpl_vars['s_f'] = $__foreach_s_f_4_saved_local_item;
}
}
if ($__foreach_s_f_4_saved_item) {
$_smarty_tpl->tpl_vars['s_f'] = $__foreach_s_f_4_saved_item;
}
} else {
if (sprintf("%s",$_smarty_tpl->tpl_vars['thead']->value['string_format']) === sprintf("%s",$_smarty_tpl->tpl_vars['fv']->value['value'])) {?> selected="selected"<?php }
}?>><?php echo $_smarty_tpl->tpl_vars['fv']->value['title'];?>
</option><?php
$_smarty_tpl->tpl_vars['fv'] = $__foreach_fv_3_saved_local_item;
}
}
if ($__foreach_fv_3_saved_item) {
$_smarty_tpl->tpl_vars['fv'] = $__foreach_fv_3_saved_item;
}
if ($__foreach_fv_3_saved_key) {
$_smarty_tpl->tpl_vars['fi'] = $__foreach_fv_3_saved_key;
}
$_from = $_smarty_tpl->tpl_vars['related_var']->value[$_smarty_tpl->tpl_vars['i']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_fv_5_saved_item = isset($_smarty_tpl->tpl_vars['fv']) ? $_smarty_tpl->tpl_vars['fv'] : false;
$__foreach_fv_5_saved_key = isset($_smarty_tpl->tpl_vars['fi']) ? $_smarty_tpl->tpl_vars['fi'] : false;
$_smarty_tpl->tpl_vars['fv'] = new Smarty_Variable();
$__foreach_fv_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_fv_5_total) {
$_smarty_tpl->tpl_vars['fi'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['fi']->value => $_smarty_tpl->tpl_vars['fv']->value) {
$__foreach_fv_5_saved_local_item = $_smarty_tpl->tpl_vars['fv'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['fi']->value;?>
"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['multiple']) && !empty($_smarty_tpl->tpl_vars['thead']->value['multiple'])) {
$_from = $_smarty_tpl->tpl_vars['thead']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_s_f_6_saved_item = isset($_smarty_tpl->tpl_vars['s_f']) ? $_smarty_tpl->tpl_vars['s_f'] : false;
$_smarty_tpl->tpl_vars['s_f'] = new Smarty_Variable();
$__foreach_s_f_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_s_f_6_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['s_f']->value) {
$__foreach_s_f_6_saved_local_item = $_smarty_tpl->tpl_vars['s_f'];
if (sprintf("%s",$_smarty_tpl->tpl_vars['s_f']->value) === sprintf("%s",$_smarty_tpl->tpl_vars['fi']->value)) {?> selected="selected"<?php }
$_smarty_tpl->tpl_vars['s_f'] = $__foreach_s_f_6_saved_local_item;
}
}
if ($__foreach_s_f_6_saved_item) {
$_smarty_tpl->tpl_vars['s_f'] = $__foreach_s_f_6_saved_item;
}
} else {
if (sprintf("%s",$_smarty_tpl->tpl_vars['thead']->value['string_format']) === sprintf("%s",$_smarty_tpl->tpl_vars['fi']->value)) {?> selected="selected"<?php }
}
if (isset($_smarty_tpl->tpl_vars['fv']->value['p_n'])) {?> data-parent="<?php echo $_smarty_tpl->tpl_vars['fv']->value['p_v'];?>
" data-parent_name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['fv']->value['p_n'];?>
"<?php }?>><?php if (isset($_smarty_tpl->tpl_vars['fv']->value['text']) && !empty($_smarty_tpl->tpl_vars['fv']->value['text'])) {
echo $_smarty_tpl->tpl_vars['fv']->value['text'];
} else {
echo $_smarty_tpl->tpl_vars['fv']->value['v'];
}?></option><?php
$_smarty_tpl->tpl_vars['fv'] = $__foreach_fv_5_saved_local_item;
}
}
if ($__foreach_fv_5_saved_item) {
$_smarty_tpl->tpl_vars['fv'] = $__foreach_fv_5_saved_item;
}
if ($__foreach_fv_5_saved_key) {
$_smarty_tpl->tpl_vars['fi'] = $__foreach_fv_5_saved_key;
}
} else {
$_from = $_smarty_tpl->tpl_vars['thead']->value['values'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_fv_7_saved_item = isset($_smarty_tpl->tpl_vars['fv']) ? $_smarty_tpl->tpl_vars['fv'] : false;
$__foreach_fv_7_saved_key = isset($_smarty_tpl->tpl_vars['fi']) ? $_smarty_tpl->tpl_vars['fi'] : false;
$_smarty_tpl->tpl_vars['fv'] = new Smarty_Variable();
$__foreach_fv_7_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_fv_7_total) {
$_smarty_tpl->tpl_vars['fi'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['fi']->value => $_smarty_tpl->tpl_vars['fv']->value) {
$__foreach_fv_7_saved_local_item = $_smarty_tpl->tpl_vars['fv'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['fv']->value['value'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['multiple']) && !empty($_smarty_tpl->tpl_vars['thead']->value['multiple'])) {
$_from = $_smarty_tpl->tpl_vars['thead']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_s_f_8_saved_item = isset($_smarty_tpl->tpl_vars['s_f']) ? $_smarty_tpl->tpl_vars['s_f'] : false;
$_smarty_tpl->tpl_vars['s_f'] = new Smarty_Variable();
$__foreach_s_f_8_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_s_f_8_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['s_f']->value) {
$__foreach_s_f_8_saved_local_item = $_smarty_tpl->tpl_vars['s_f'];
if (sprintf("%s",$_smarty_tpl->tpl_vars['s_f']->value) === sprintf("%s",$_smarty_tpl->tpl_vars['fv']->value['value'])) {?> selected="selected"<?php }
$_smarty_tpl->tpl_vars['s_f'] = $__foreach_s_f_8_saved_local_item;
}
}
if ($__foreach_s_f_8_saved_item) {
$_smarty_tpl->tpl_vars['s_f'] = $__foreach_s_f_8_saved_item;
}
} else {
if (sprintf("%s",$_smarty_tpl->tpl_vars['thead']->value['string_format']) === sprintf("%s",$_smarty_tpl->tpl_vars['fv']->value['value'])) {?> selected="selected"<?php }
}
if (isset($_smarty_tpl->tpl_vars['fv']->value['parent'])) {?> data-parent="<?php echo $_smarty_tpl->tpl_vars['fv']->value['parent'];?>
" data-parent_name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['fv']->value['parent_name'];?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['fv']->value['title'];?>
</option><?php
$_smarty_tpl->tpl_vars['fv'] = $__foreach_fv_7_saved_local_item;
}
}
if ($__foreach_fv_7_saved_item) {
$_smarty_tpl->tpl_vars['fv'] = $__foreach_fv_7_saved_item;
}
if ($__foreach_fv_7_saved_key) {
$_smarty_tpl->tpl_vars['fi'] = $__foreach_fv_7_saved_key;
}
}?></select><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['filter'] === 'number') {?><input type="number" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['step'])) {?> step="<?php echo $_smarty_tpl->tpl_vars['thead']->value['step'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['min'])) {?> min="<?php echo $_smarty_tpl->tpl_vars['thead']->value['min'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['thead']->value['max'])) {?> max="<?php echo $_smarty_tpl->tpl_vars['thead']->value['max'];?>
"<?php }?> placeholder="<?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
"/><?php } else { ?><input type="<?php echo $_smarty_tpl->tpl_vars['thead']->value['type'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['thead']->value['string_format'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['thead']->value['maxlength'])) {?> maxlength="<?php echo $_smarty_tpl->tpl_vars['thead']->value['maxlength'];?>
"<?php }?> placeholder="<?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
"/><?php }
} else { ?>-<?php }?></th><?php
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_1_saved_local_item;
}
}
if ($__foreach_thead_1_saved_item) {
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_1_saved_item;
}
if ($__foreach_thead_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_thead_1_saved_key;
}
if ($_smarty_tpl->tpl_vars['has_actions']->value && $_smarty_tpl->tpl_vars['display_edit_div']->value) {?><th class="text-center edit_div"><button type="submit" class="btn btn-default"><?php echo l(array('s'=>'搜尋'),$_smarty_tpl);?>
</button></th><?php }?></tr><?php }?></thead><?php }
}
