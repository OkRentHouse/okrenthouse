<?php
/* Smarty version 3.1.28, created on 2021-03-24 15:31:37
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/SystemInformation/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_605aead9ee0804_94667769',
  'file_dependency' => 
  array (
    '572ef42ff0674b7c0b0fac8b8f934a37ddcfdb1e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/SystemInformation/content.tpl',
      1 => 1592288974,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605aead9ee0804_94667769 ($_smarty_tpl) {
?>
<div class="col-lg-6">
	<div>
		<div class="panel panel-default">
			<div class="panel-heading"><?php echo l(array('s'=>"伺服器資訊"),$_smarty_tpl);?>
</div>
			<div class="panel-body">
				<p><strong><?php echo l(array('s'=>"伺服器資訊:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['server']['server_information'];?>
</p>
				<p><strong><?php echo l(array('s'=>"伺服器軟體版本:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['server']['server'];?>
</p>
				<p><strong><?php echo l(array('s'=>"PHP版本:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['server']['php'];?>
</p>
				<p><strong><?php echo l(array('s'=>"Memory limit:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['server']['memory_limit'];?>
</p>
				<p><strong><?php echo l(array('s'=>"Max execution time:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['server']['max_execution_time'];?>
</p>
			</div>
		</div>
	</div><div>
		<div class="panel panel-default">
			<div class="panel-heading"><?php echo l(array('s'=>"資料庫資訊"),$_smarty_tpl);?>
</div>
			<div class="panel-body">
				<p><strong><?php echo l(array('s'=>"MySQL 版本:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['data']['version'];?>
</p>
				<p><strong><?php echo l(array('s'=>"MySQL 伺服器:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['data']['server'];?>
</p>
				<p><strong><?php echo l(array('s'=>"MySQL 資料庫名稱:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['data']['name'];?>
</p>
				<p><strong><?php echo l(array('s'=>"MySQL 使用者名稱:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['data']['user'];?>
</p>
				<p><strong><?php echo l(array('s'=>"MySQL Tables prefix:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['data']['tables_prefix'];?>
</p>
				<p><strong><?php echo l(array('s'=>"MySQL 引擎:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['data']['engine'];?>
</p>
				<p><strong><?php echo l(array('s'=>"MySQL 驅動程序:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['data']['driver'];?>
</p>
			</div>
		</div>
	</div><div>
		<div class="panel panel-default">
			<div class="panel-heading"><?php echo l(array('s'=>"MAIL 資訊"),$_smarty_tpl);?>
</div>
			<div class="panel-body">
				<p><strong><?php echo l(array('s'=>"郵件方法:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['mail']['driver'];?>
</p>
				<p><strong><?php echo l(array('s'=>"SMTP服務器:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['mail']['driver'];?>
</p>
				<p><strong><?php echo l(array('s'=>"SMTP用戶名:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['mail']['driver'];?>
</p>
				<p><strong><?php echo l(array('s'=>"SMTP密碼:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['mail']['driver'];?>
</p>
				<p><strong><?php echo l(array('s'=>"加密:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['mail']['driver'];?>
</p>
				<p><strong><?php echo l(array('s'=>"SMTP端口:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['mail']['driver'];?>
</p>
			</div>
		</div>
	</div>
</div><div class="col-lg-6">
	<div>
		<div class="panel panel-default">
			<div class="panel-heading"><?php echo l(array('s'=>"網站資訊"),$_smarty_tpl);?>
</div>
			<div class="panel-body">
				<p><strong><?php echo l(array('s'=>"網站名稱:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['web']['web_name'];?>
</p>
				<p><strong><?php echo l(array('s'=>"網站版本:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['web']['web_version'];?>
</p>
				<p><strong><?php echo l(array('s'=>"Dinaub name:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['web']['web_dns'];?>
</p>
			</div>
		</div>
	</div><div>
		<div class="panel panel-default">
			<div class="panel-heading"><?php echo l(array('s'=>"您的資訊"),$_smarty_tpl);?>
</div>
			<div class="panel-body">
				<p><strong><?php echo l(array('s'=>"您的網路瀏覽器:"),$_smarty_tpl);?>
</strong><?php echo $_smarty_tpl->tpl_vars['information']->value['user']['agent'];?>
</p>
			</div>
		</div>
	</div>
</div><?php }
}
