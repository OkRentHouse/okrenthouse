<?php
/* Smarty version 3.1.28, created on 2021-03-23 16:01:50
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Competence/tabs_file.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6059a06ef1b008_79140124',
  'file_dependency' => 
  array (
    '3d1315e86a68114645f74be2b24b268c5bd7ffa1' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Competence/tabs_file.tpl',
      1 => 1592288973,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6059a06ef1b008_79140124 ($_smarty_tpl) {
?>
<div class="tab-count file_tab tabs_group_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
 col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="icon-cogs"></i><?php echo l(array('s'=>"檔案權限"),$_smarty_tpl);?>
</div>
		<div class="panel-body">
			<table>
				<tbody>
				<div class="btn-group" data-toggle="buttons">
					<tr class="h6">
						<td><?php echo l(array('s'=>"上傳"),$_smarty_tpl);?>
</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_up']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> active<?php }?>" for="file_up_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0"><input type="radio" id="file_up_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0" name="file_up_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_up" value="0"<?php if ($_smarty_tpl->tpl_vars['arr_file_up']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"關閉"),$_smarty_tpl);?>
</label>
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_up']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> active<?php }?>" for="file_up_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1"><input type="radio" id="file_up_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1" name="file_up_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_up" value="1"<?php if ($_smarty_tpl->tpl_vars['arr_file_up']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"啟用"),$_smarty_tpl);?>
</label>
							</div>
						</td>
					</tr>
					<tr class="h6">
						<td><?php echo l(array('s'=>"下載"),$_smarty_tpl);?>
</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_download']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> active<?php }?>" for="file_download_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0"><input type="radio" id="file_download_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0" name="file_download_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_download" value="0"<?php if ($_smarty_tpl->tpl_vars['arr_file_move']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"關閉"),$_smarty_tpl);?>
</label>
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_download']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> active<?php }?>" for="file_download_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1"><input type="radio" id="file_download_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1" name="file_download_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_download" value="1"<?php if ($_smarty_tpl->tpl_vars['arr_file_move']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"啟用"),$_smarty_tpl);?>
</label>
							</div>
						</td>
					</tr>
					<tr class="h6">
						<td><?php echo l(array('s'=>"移動"),$_smarty_tpl);?>
</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_move']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> active<?php }?>" for="file_move_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0"><input type="radio" id="file_move_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0" name="file_move_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_move" value="0"<?php if ($_smarty_tpl->tpl_vars['arr_file_move']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"關閉"),$_smarty_tpl);?>
</label>
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_move']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> active<?php }?>" for="file_move_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1"><input type="radio" id="file_move_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1" name="file_move_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_move" value="1"<?php if ($_smarty_tpl->tpl_vars['arr_file_move']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"啟用"),$_smarty_tpl);?>
</label>
							</div>
						</td>
					</tr>
					<tr class="h6">
						<td><?php echo l(array('s'=>"刪除"),$_smarty_tpl);?>
</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_del']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> active<?php }?>" for="file_del_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0"><input type="radio" id="file_del_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_0" name="file_del_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_del" value="0"<?php if ($_smarty_tpl->tpl_vars['arr_file_del']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '0') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"關閉"),$_smarty_tpl);?>
</label>
								<label class="btn btn btn-default on_ajax_function<?php if ($_smarty_tpl->tpl_vars['arr_file_del']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> active<?php }?>" for="file_del_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1"><input type="radio" id="file_del_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
_1" name="file_del_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" data-fun="file_del" value="1"<?php if ($_smarty_tpl->tpl_vars['arr_file_del']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']] == '1') {?> checked="checked"<?php }?>><?php echo l(array('s'=>"啟用"),$_smarty_tpl);?>
</label>
							</div>
						</td>
					</tr>
				</div>
				</tbody>
			</table>
		</div>
	</div>
</div><?php }
}
