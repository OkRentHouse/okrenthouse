<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:41:27
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/group_link.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599ba78a4765_77690342',
  'file_dependency' => 
  array (
    '93ce4ab36d27b7f65b024cf36e85bcbc6365203c' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/group_link.tpl',
      1 => 1611647222,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../tpl/group_link.tpl' => 1,
  ),
),false)) {
function content_60599ba78a4765_77690342 ($_smarty_tpl) {
?>
<section class="slider_center">
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../tpl/group_link.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</section>

<?php echo '<script'; ?>
>
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 7,
        slidesToScroll: 7
    });
<?php echo '</script'; ?>
>
<?php }
}
