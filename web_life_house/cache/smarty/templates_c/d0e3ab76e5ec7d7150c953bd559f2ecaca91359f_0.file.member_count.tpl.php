<?php
/* Smarty version 3.1.28, created on 2021-04-09 13:56:51
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/member_count.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_606feca3621cf1_17113487',
  'file_dependency' => 
  array (
    'd0e3ab76e5ec7d7150c953bd559f2ecaca91359f' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/member_count.tpl',
      1 => 1617940441,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606feca3621cf1_17113487 ($_smarty_tpl) {
$_from = $_smarty_tpl->tpl_vars['member_additional_row']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>

  <?php $_smarty_tpl->tpl_vars['new_row'] = new Smarty_Variable(json_decode($_smarty_tpl->tpl_vars['v']->value['id_member_additional']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'new_row', 0);?>
  <?php if (sizeof($_smarty_tpl->tpl_vars['new_row']->value) > 1) {?>
    <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'new_row', null);
$_smarty_tpl->tpl_vars['new_row']->value[0] = null;
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'new_row', 0);?>
    <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'new_member_additional_row', null);
$_smarty_tpl->tpl_vars['new_member_additional_row']->value[$_smarty_tpl->tpl_vars['new_row']->value[1]] = $_smarty_tpl->tpl_vars['v']->value['additional_COUNT'];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'new_member_additional_row', 0);?>
    <?php $_smarty_tpl->tpl_vars['count_new_member_additional_row'] = new Smarty_Variable($_smarty_tpl->tpl_vars['count_new_member_additional_row']->value+$_smarty_tpl->tpl_vars['v']->value['additional_COUNT'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_new_member_additional_row', 0);?>
    <?php } else { ?>
    <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'new_member_additional_row', null);
$_smarty_tpl->tpl_vars['new_member_additional_row']->value[$_smarty_tpl->tpl_vars['new_row']->value[0]] = $_smarty_tpl->tpl_vars['v']->value['additional_COUNT'];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'new_member_additional_row', 0);?>
    <?php $_smarty_tpl->tpl_vars['count_new_member_additional_row'] = new Smarty_Variable($_smarty_tpl->tpl_vars['count_new_member_additional_row']->value+$_smarty_tpl->tpl_vars['v']->value['additional_COUNT'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_new_member_additional_row', 0);?>
  <?php }
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_v_0_saved_key;
}
$_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'new_member_additional_row', null);
$_smarty_tpl->tpl_vars['new_member_additional_row']->value[1] = $_smarty_tpl->tpl_vars['count_new_member_additional_row']->value;
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'new_member_additional_row', 0);?>
<style>
  .id_member_additional_table td {
      border: 1px solid #CCC;
      text-align: center;
  }
  .id_member_additional_table th {
    position: relative;
    white-space: nowrap;
    background-color: #2e6ea5;
    color: #fff;
  }
</style>
<div class="panel-default">
  <div class="panel-heading">
        會員統計
      </div>
		</div>
<table style="width: 100%;" class="id_member_additional_table">

    <tr>
      <?php
$_from = $_smarty_tpl->tpl_vars['fields']->value['list']['id_member_additional']['values'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
        <th class="text-center"><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
<div class="th_title" style="top: 0px;"><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</div></th>
      <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_v_1_saved_key;
}
?>
    </tr>

  <tr>
    <?php
$_from = $_smarty_tpl->tpl_vars['fields']->value['list']['id_member_additional']['values'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
      <td class=""><?php echo $_smarty_tpl->tpl_vars['new_member_additional_row']->value[$_smarty_tpl->tpl_vars['v']->value['value']];?>
</td>
    <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_v_2_saved_key;
}
?>
  </tr>
</table>
<?php }
}
