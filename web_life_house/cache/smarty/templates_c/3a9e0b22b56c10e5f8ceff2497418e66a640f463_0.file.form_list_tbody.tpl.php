<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:35:15
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/ShowNews/form/form_list_tbody.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599a3346dbb4_35153490',
  'file_dependency' => 
  array (
    '3a9e0b22b56c10e5f8ceff2497418e66a640f463' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/ShowNews/form/form_list_tbody.tpl',
      1 => 1592288974,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60599a3346dbb4_35153490 ($_smarty_tpl) {
if (count($_smarty_tpl->tpl_vars['fields']->value['list_val']) > 0) {
$_from = $_smarty_tpl->tpl_vars['fields']->value['list_val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_list_0_saved_item = isset($_smarty_tpl->tpl_vars['list']) ? $_smarty_tpl->tpl_vars['list'] : false;
$__foreach_list_0_saved_key = isset($_smarty_tpl->tpl_vars['fi']) ? $_smarty_tpl->tpl_vars['fi'] : false;
$_smarty_tpl->tpl_vars['list'] = new Smarty_Variable();
$__foreach_list_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_list_0_total) {
$_smarty_tpl->tpl_vars['fi'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['fi']->value => $_smarty_tpl->tpl_vars['list']->value) {
$__foreach_list_0_saved_local_item = $_smarty_tpl->tpl_vars['list'];
?>
	<div class="news_div news_<?php echo $_smarty_tpl->tpl_vars['list']->value['id_news'];?>
 <?php if ($_smarty_tpl->tpl_vars['list']->value['top']) {?>top<?php }?>">
			<?php ob_start();
echo $_smarty_tpl->tpl_vars['list']->value['id_news_type'];
$_tmp1=ob_get_clean();
if (!empty($_tmp1)) {?><samp class="type"><?php echo $_smarty_tpl->tpl_vars['list']->value['id_news_type'];?>
</samp><?php }?>
			<samp class="time">發佈時間：<?php echo $_smarty_tpl->tpl_vars['list']->value['date_time'];?>
</samp>
			<?php if ($_smarty_tpl->tpl_vars['list']->value['top']) {?><i class="glyphicon glyphicon-bookmark top" title="置頂" alt="置頂"></i><?php }?>
			<h1 class="title"><?php echo $_smarty_tpl->tpl_vars['list']->value['title'];?>
</h1>
			<div class="content"><?php echo $_smarty_tpl->tpl_vars['list']->value['content'];?>
</div>
			<samp class="release">發佈：<?php echo $_smarty_tpl->tpl_vars['list']->value['id_add'];
if ($_smarty_tpl->tpl_vars['list']->value['id_edit'] != '-') {?>　最後修改：<?php echo $_smarty_tpl->tpl_vars['list']->value['id_edit'];?>
 <?php echo $_smarty_tpl->tpl_vars['list']->value['edit_time'];
}?></samp>
	</div>
<?php
$_smarty_tpl->tpl_vars['list'] = $__foreach_list_0_saved_local_item;
}
}
if ($__foreach_list_0_saved_item) {
$_smarty_tpl->tpl_vars['list'] = $__foreach_list_0_saved_item;
}
if ($__foreach_list_0_saved_key) {
$_smarty_tpl->tpl_vars['fi'] = $__foreach_list_0_saved_key;
}
} else { ?>
<div class="text-center" colspan="<?php if ($_smarty_tpl->tpl_vars['has_actions']->value && $_smarty_tpl->tpl_vars['display_edit_div']->value) {
echo count($_smarty_tpl->tpl_vars['fields']->value['list'])+1;
} else {
echo count($_smarty_tpl->tpl_vars['fields']->value['list']);
}?>"><?php echo l(array('s'=>"沒有最新消息!"),$_smarty_tpl);?>
</div>
<?php }
}
}
