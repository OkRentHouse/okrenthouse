<?php
/* Smarty version 3.1.28, created on 2021-05-07 14:12:39
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/new_field_history.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6094da57de5079_28150908',
  'file_dependency' => 
  array (
    '8b6fc2f6fb8fa89323e45adb7ba66855edb38151' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/new_field_history.tpl',
      1 => 1620367894,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6094da57de5079_28150908 ($_smarty_tpl) {
?>
<div class="panel panel-default">
        <div class="panel-heading" <?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
 onclick="collapse_data(this)"><?php echo $_smarty_tpl->tpl_vars['form']->value['title'];?>
(點選收合)
        </div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">
<?php
$_from = $_smarty_tpl->tpl_vars['catch_history']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">房東</td>
    <td colspan="7">
    <input type="text" id="read_landlord_name" name="read_landlord_name" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['landlord_name'];?>
" size="10" readonly>
    <input type="hidden" id="read_landlord_id_member" name="read_landlord_id_member" value="<?php echo $_smarty_tpl->tpl_vars['read_landlord_id_member']->value;?>
" size="10" readonly>
    </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租約:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['lease'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon" style="color:red">目前合約編號:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['leaseSequence'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">來源:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['source'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['userName'];?>
" size="10" disabled="disabled"></td>
  </tr>
    <tr>
  <td class="main_t_2 input-group-addon" style="color:red">是否黑名單:</td>
  <td><input type="checkbox" id="black_add" name="black_add" value="1" <?php if ($_smarty_tpl->tpl_vars['v']->value['blacktype'] == "1") {?> checked <?php }?> onclick="return false;"></td>
  <td class="main_t_2 input-group-addon">原因:</td>
  <td><input type="text" id="black_reason" name="black_reason" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['blackreason'];?>
" style="width:100%;"></td>
  <td class="main_t_2 input-group-addon">紀錄時間:</td>
  <td><input type="date" id="recodeTime" name="recodeTime" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['recodeTime'];?>
"></td>
  <td class="main_t_2 input-group-addon">紀錄人:</td>
  <td><input type="text" id="black_cp" name="black_cp" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['create_people'];?>
" style="width:100%;" readonly></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['leasetitle'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">生日:</td>
    <td><input type="date" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['birthday'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['userID'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">手機1:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['mobile1'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['mobile2'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['lineID'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['tel_h'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['tel_o'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['residenceAddress'];?>
" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">聯絡地址:</td>
    <td colspan="3"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['contractAddress'];?>
" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['homeAddress'];?>
" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['businessAddress'];?>
" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['email'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bussinessNumber'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['worktitle'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">租約條件:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['leaseCondition'];?>
" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入帳帳戶1:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bankAccount1'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">入帳帳戶2:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bankAccount2'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">繳租記錄:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['payRecord'];?>
" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租期:</td>
    <td>起<input type="date" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['leaseRangeStart'];?>
" size="10" disabled="disabled"><br/>結<input type="date" id="leaseRangeEnd" name="leaseRangeEnd" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['leaseRangeEnd'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">租金:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['rentMoney'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">其他：</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['rent_other'];?>
" size="10" disabled="disabled"><br/>
    <?php if ($_smarty_tpl->tpl_vars['v']->value['rent_other1'] == "0") {?>
      車位
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['v']->value['rent_other1'] == "1") {?>
      管理費
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['v']->value['rent_other1'] == "2") {?>
      兩者
      <?php }?>
    </td>
    <td class="main_t_2 input-group-addon">繳租日:</td>
    <td><input type="test"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['rentPayDate'];?>
" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">帶看配合:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['lookOther'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">押金金額:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bonds'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">押金繳交記錄:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bondsRecord'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">押金擔保:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bondsPer'];?>
" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">期滿退租:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['leasefull'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">AG:</td>
    <td><input type="text"  value="" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">仲介方案:</td>
    <td> 
      <?php if ($_smarty_tpl->tpl_vars['v']->value['agencyPlan'] == "A") {?>
      
      甲
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['v']->value['agencyPlan'] == "B") {?>
      乙
      <?php }?> </td>
    <td class="main_t_2 input-group-addon">代管方案:</td>
    <td>
      <?php if ($_smarty_tpl->tpl_vars['v']->value['managePlan'] == "A") {?>      
      A
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['v']->value['managePlan'] == "B") {?>
      B
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['v']->value['managePlan'] == "C") {?>
      C
      <?php }?> </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租售同步:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['rentAndSale'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">點 in 日期:</td>
    <td><input type="date" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['checkin'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">點交人員:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['checkinPeople'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">中途解約:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['breaklease'];?>
" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租管工會:</td> 
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['rentAssoc'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">加值 - 租霸補助:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['addRent'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">加值 - 信用認證:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['addCredit'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">加值 - 安心履約:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['addCare'];?>
" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入手價:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['buyPrice'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">桃協會:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['taoAssoc'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">台協會:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['twAssoc'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon" style="color:red">契約編號</td>
    <td><input style="color:red" type="text" id="lease_number" name="lease_number" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['lease_number'];?>
" size="10" disabled="disabled"/></td>
  </tr>

  <tr>
    <td class="main_t_2 input-group-addon">租屋條件:</td>
    <td colspan="5"><input type="text" id="member_landlord_id_other_conditions" name="member_landlord_id_other_conditions" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_id_other_conditions'];?>
" size="120" disabled="disabled"/></td>
    <td class="main_t_2 input-group-addon" style="color:red">更新人員</td>
    <td><input style="color:red" type="text" id="update_people2" name="update_people" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['updatePeople'];?>
" size="10" disabled="disabled"/></td>
  </tr>

</table>
<br/>
<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">新擔保人合約</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['userName_relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['leasetitle_relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['relation_relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">生日:</td>
    <td><input type="date"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['birthday_relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['userID_relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon" style="color:red">手機1:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['mobile1_relation'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['mobile2_relation'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['lineID_relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['tel_h_relation'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['tel_o_relation'];?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['email_relation'];?>
" size="10" disabled="disabled"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['residenceAddress_relation'];?>
" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">聯絡地址:</td>
    <td colspan="3"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['homeAddress_relation'];?>
" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['homeAddress_relation'];?>
" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['businessAddress_relation'];?>
" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bussinessNumber_relation'];?>
" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['worktitle_relation'];?>
" size="10" disabled="disabled"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
</table>
<br/><br/>
<hr>
<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
</div>
</div>
</div>
<?php }
}
