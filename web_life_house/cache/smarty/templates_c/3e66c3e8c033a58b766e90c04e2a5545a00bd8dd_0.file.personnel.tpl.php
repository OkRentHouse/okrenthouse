<?php
/* Smarty version 3.1.28, created on 2021-04-01 12:29:23
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/personnel.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60654c23b43ec1_08913405',
  'file_dependency' => 
  array (
    '3e66c3e8c033a58b766e90c04e2a5545a00bd8dd' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/personnel.tpl',
      1 => 1617251361,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60654c23b43ec1_08913405 ($_smarty_tpl) {
?>
<input type="hidden" name="id_admin" value="">
<label for="email">公司帳號</label>
<div class="form-group col-lg-12" >
    <div class="col-lg-10 no_padding">
        <div class="input-group fixed-width-lg">
            <span class="input-group-addon">公司帳號:</span>
            <input class="form-control" type="text" name="email" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['email'];?>
" maxlength="100" disabled="disabled">
        </div>
    </div>
</div>
    <label>狀態</label>
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">來源:</span>
                <select name="id_admin_source" id="id_admin_source" class="form-control">
                    <option value="">請選擇來源</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['database_source']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_v_0_saved_item = isset($_smarty_tpl->tpl_vars['d_s_v']) ? $_smarty_tpl->tpl_vars['d_s_v'] : false;
$__foreach_d_s_v_0_saved_key = isset($_smarty_tpl->tpl_vars['d_s_k']) ? $_smarty_tpl->tpl_vars['d_s_k'] : false;
$_smarty_tpl->tpl_vars['d_s_v'] = new Smarty_Variable();
$__foreach_d_s_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_v_0_total) {
$_smarty_tpl->tpl_vars['d_s_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_k']->value => $_smarty_tpl->tpl_vars['d_s_v']->value) {
$__foreach_d_s_v_0_saved_local_item = $_smarty_tpl->tpl_vars['d_s_v'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['id_admin_source'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_s_v']->value['id_admin_source'] == $_smarty_tpl->tpl_vars['database_data']->value['id_admin_source']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['name'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_0_saved_local_item;
}
}
if ($__foreach_d_s_v_0_saved_item) {
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_0_saved_item;
}
if ($__foreach_d_s_v_0_saved_key) {
$_smarty_tpl->tpl_vars['d_s_k'] = $__foreach_d_s_v_0_saved_key;
}
?>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">單位:</span>
                <select name="id_company" id="id_company" class="form-control">
                    <option value="">請選擇單位</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_1_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_1_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_1_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_1_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_company'] == $_smarty_tpl->tpl_vars['database_data']->value['id_company']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_1_saved_local_item;
}
}
if ($__foreach_d_c_v_1_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_1_saved_item;
}
if ($__foreach_d_c_v_1_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_1_saved_key;
}
?>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">單位:</span>
                <select name="id_department" id="id_department" class="form-control">
                    <option value="">請選擇部門</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['database_department']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_d_v_2_saved_item = isset($_smarty_tpl->tpl_vars['d_d_v']) ? $_smarty_tpl->tpl_vars['d_d_v'] : false;
$__foreach_d_d_v_2_saved_key = isset($_smarty_tpl->tpl_vars['d_d_k']) ? $_smarty_tpl->tpl_vars['d_d_k'] : false;
$_smarty_tpl->tpl_vars['d_d_v'] = new Smarty_Variable();
$__foreach_d_d_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_d_v_2_total) {
$_smarty_tpl->tpl_vars['d_d_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_d_k']->value => $_smarty_tpl->tpl_vars['d_d_v']->value) {
$__foreach_d_d_v_2_saved_local_item = $_smarty_tpl->tpl_vars['d_d_v'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_d_v']->value['id_department'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_d_v']->value['id_department'] == $_smarty_tpl->tpl_vars['database_data']->value['id_department']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_d_v']->value['name'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['d_d_v'] = $__foreach_d_d_v_2_saved_local_item;
}
}
if ($__foreach_d_d_v_2_saved_item) {
$_smarty_tpl->tpl_vars['d_d_v'] = $__foreach_d_d_v_2_saved_item;
}
if ($__foreach_d_d_v_2_saved_key) {
$_smarty_tpl->tpl_vars['d_d_k'] = $__foreach_d_d_v_2_saved_key;
}
?>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">職稱:</span>
                <input class="form-control" type="text"  name="job" id="job" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['job'];?>
" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">到職日期:</span>
                <input class="form-control" type="date"  name="on_duty_date" id="on_duty_date" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['on_duty_date'];?>
">
            </div>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">離職日期:</span>
                <input class="form-control" type="date"  name="resign_date" id="resign_date" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['resign_date'];?>
">
            </div>
        </div>
        <div class="col-lg-7 no_padding" style="padding-left: 60px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">離職原因:</span>
                <input class="form-control" type="text"  name="resign_reason" id="resign_reason" maxlength="128" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['resign_reason'];?>
">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">離職確認:</span>
                <?php if ($_smarty_tpl->tpl_vars['database_data']->value['active'] == "1") {?>
                <input class="form-control" type="checkbox"  name="active" id="active" value="1" >
                <?php } else { ?>
                <input class="form-control" type="checkbox"  name="active" id="active" value="0" checked>
                <?php }?>
            </div>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">停留日期:</span>
                <input class="form-control" type="date"  name="stay_date" id="stay_date" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['stay_date'];?>
">
            </div>
        </div>
        <div class="col-lg-7 no_padding" style="padding-left: 60px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">停留原因:</span>
                <input class="form-control" type="text"  name="stay_reason" id="stay_reason" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['stay_reason'];?>
" maxlength="128">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">復職日期:</span>
                <input class="form-control" type="date"  name="reinstatement_date" id="reinstatement_date" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['reinstatement_date'];?>
">
            </div>
        </div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['page']->value != 'PersonnelUnit' && $_smarty_tpl->tpl_vars['page']->value != 'LifeAssets') {?>
    <label>資訊帳戶相關處理 <span class="mark text-primary"> (本設定畫面不適用) </span></label><p>
    <?php } else { ?>
    <label>資訊帳戶相關處理，<span class="mark text-primary"> (人員完成離職手續後、請人事通知資訊管理員刪除帳戶程序) </span></label>
    <div class="form-group col-lg-12">
        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <label style="vertical-align: sub;" class="navbar-collapse">到職：</label>
            </div>
        </div>
        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">後台帳號:</span>
                <input class="form-control" type="text" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['email'];?>
" placeholder="同公司帳號" readonly>
            </div>
        </div>
        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">email account:</span>
                <input class="form-control" type="text" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['email'];?>
" placeholder="同公司帳號" readonly>
            </div>
        </div>
        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">建立日期:</span>
                <input class="form-control" type="date"  name="create_date" id="create_date" value="<?php if ($_smarty_tpl->tpl_vars['database_data']->value['create_date']) {
echo $_smarty_tpl->tpl_vars['database_data']->value['create_date'];
} else {
echo date('Y-m-d');
}?>" readonly>
            </div>
        </div>

    </div>

    <div class="form-group col-lg-12">
        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <label style="vertical-align: sub;" class="navbar-collapse">離職：</label>
            </div>
        </div>
        <?php if (sizeof($_smarty_tpl->tpl_vars['database_adminuser']->value) == 0) {
$_smarty_tpl->tpl_vars['RLY'] = new Smarty_Variable('readonly', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'RLY', 0);
}?>
        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
              <?php if (sizeof($_smarty_tpl->tpl_vars['database_adminuser']->value) == 0) {?>
                <span class="input-group-addon label-danger">處理人：僅帳戶管理員可輸入</span>
                <?php $_smarty_tpl->tpl_vars['RLY'] = new Smarty_Variable('readonly', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'RLY', 0);?>
                <?php } else { ?>
                <span class="input-group-addon">處理人</span>
              <?php }?>

                <!-- <input class="form-control" type="text"  name="disable_by" id="disable_by" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['disable_by'];?>
" > -->

                  <?php
$_from = $_smarty_tpl->tpl_vars['database_adminuser']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_d_v_3_saved_item = isset($_smarty_tpl->tpl_vars['d_d_v']) ? $_smarty_tpl->tpl_vars['d_d_v'] : false;
$__foreach_d_d_v_3_saved_key = isset($_smarty_tpl->tpl_vars['d_d_k']) ? $_smarty_tpl->tpl_vars['d_d_k'] : false;
$_smarty_tpl->tpl_vars['d_d_v'] = new Smarty_Variable();
$__foreach_d_d_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_d_v_3_total) {
$_smarty_tpl->tpl_vars['d_d_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_d_k']->value => $_smarty_tpl->tpl_vars['d_d_v']->value) {
$__foreach_d_d_v_3_saved_local_item = $_smarty_tpl->tpl_vars['d_d_v'];
?>
                      <label class="form-control input-group-addon"><?php if ($_smarty_tpl->tpl_vars['database_data']->value['disable_by']) {?>最後處理:<?php echo $_smarty_tpl->tpl_vars['database_data']->value['disable_by'];
} else {
echo $_smarty_tpl->tpl_vars['d_d_v']->value['first_name_en'];
}?></label>
                      <input class="form-control" type="text"  name="disable_by" id="disable_by" value="<?php echo $_smarty_tpl->tpl_vars['d_d_v']->value['first_name_en'];?>
" style="opacity: 0;padding: 0px;height: 0px;" <?php echo $_smarty_tpl->tpl_vars['RLY']->value;?>
>
                  <?php
$_smarty_tpl->tpl_vars['d_d_v'] = $__foreach_d_d_v_3_saved_local_item;
}
}
if ($__foreach_d_d_v_3_saved_item) {
$_smarty_tpl->tpl_vars['d_d_v'] = $__foreach_d_d_v_3_saved_item;
}
if ($__foreach_d_d_v_3_saved_key) {
$_smarty_tpl->tpl_vars['d_d_k'] = $__foreach_d_d_v_3_saved_key;
}
?>

            </div>
        </div>

        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">處理狀況 / 備註</span>
                <input class="form-control" type="text"  name="disable_note" id="disable_note" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['disable_note'];?>
" placeholder="移除後臺帳號 / email / 其他帳號" <?php echo $_smarty_tpl->tpl_vars['RLY']->value;?>
>
            </div>
        </div>

        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">刪除日期:</span>
                <input class="form-control" type="date"  name="disable_date" id="disable_date" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['disable_date'];?>
" <?php echo $_smarty_tpl->tpl_vars['RLY']->value;?>
>
            </div>
        </div>


    </div>
    <?php if (sizeof($_smarty_tpl->tpl_vars['database_adminuser']->value) != 0) {?>
    <div class="form-group col-lg-12">
      <div class="col-lg-1 no_padding"></div>
      <div class="col-lg-4 no_padding"></div>
      <div class="col-lg-4 no_padding">快捷:<a onclick="$('#disable_note').val($('#disable_note').val() + this.innerText)" href="javascript:void(0)">刪除後台帳戶,</a>
        <a onclick="$('#disable_note').val($('#disable_note').val() + this.innerText)" href="javascript:void(0)">刪除mail帳戶,</a>
      <a onclick="$('#disable_note').val($('#disable_note').val() + this.innerText)" href="javascript:void(0)">無後台帳戶勿須處理</a></div>
      <div class="col-lg-1 no_padding"></div>
    </div>
    <?php }?>
    <p> </p>
    <?php }
if ($_smarty_tpl->tpl_vars['personnel_special']->value == 1) {?>

    
    <label for="labor">勞保</label>
    <div class="form-group col-lg-12">
        <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['labor'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['labor'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_p_l_v_4_saved_item = isset($_smarty_tpl->tpl_vars['p_l_v']) ? $_smarty_tpl->tpl_vars['p_l_v'] : false;
$__foreach_p_l_v_4_saved_key = isset($_smarty_tpl->tpl_vars['p_l_k']) ? $_smarty_tpl->tpl_vars['p_l_k'] : false;
$_smarty_tpl->tpl_vars['p_l_v'] = new Smarty_Variable();
$__foreach_p_l_v_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_p_l_v_4_total) {
$_smarty_tpl->tpl_vars['p_l_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['p_l_k']->value => $_smarty_tpl->tpl_vars['p_l_v']->value) {
$__foreach_p_l_v_4_saved_local_item = $_smarty_tpl->tpl_vars['p_l_v'];
?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['id_personnel_insurance'];?>
">
            <input type="hidden" name="id_personnel_insurance_type[]" value="0">
            <input type="hidden" name="id_personnel_insurance_e_date[]" >
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_5_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_5_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_5_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_5_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
" <?php if ($_smarty_tpl->tpl_vars['p_l_v']->value['id_company'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_company']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_5_saved_local_item;
}
}
if ($__foreach_d_c_v_5_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_5_saved_item;
}
if ($__foreach_d_c_v_5_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_5_saved_key;
}
?>
                    </select>
                </div>
            </div>
             <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['s_date'];?>
">
                </div>
            </div>
            <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['p_l_v']->value['type'] == 0) {?>selected="selected"<?php }?>>公司轉帳</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['p_l_v']->value['type'] == 1) {?>selected="selected"<?php }?>>憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['handler'];?>
" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['update_time'];?>
" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['price'];?>
">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['insurance_price'];?>
">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">公司負擔:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]"  value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['company_price'];?>
">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">自負額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_l_v']->value['personal_price'];?>
">
                </div>
            </div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['p_l_v'] = $__foreach_p_l_v_4_saved_local_item;
}
}
if ($__foreach_p_l_v_4_saved_item) {
$_smarty_tpl->tpl_vars['p_l_v'] = $__foreach_p_l_v_4_saved_item;
}
if ($__foreach_p_l_v_4_saved_key) {
$_smarty_tpl->tpl_vars['p_l_k'] = $__foreach_p_l_v_4_saved_key;
}
?>
        <?php }?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" >
            <input type="hidden" name="id_personnel_insurance_type[]" value="0">
            <input type="hidden" name="id_personnel_insurance_e_date[]" >
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_6_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_6_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_6_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_6_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
" ><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_6_saved_local_item;
}
}
if ($__foreach_d_c_v_6_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_6_saved_item;
}
if ($__foreach_d_c_v_6_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_6_saved_key;
}
?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" >
                </div>
            </div>
            <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0">公司轉帳</option>
                        <option value="1">憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]"  disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">公司負擔:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">自負額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="0"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
    <label for="health">健保</label>
    <div class="form-group col-lg-12">
        <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['health'])) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['health'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_p_h_v_7_saved_item = isset($_smarty_tpl->tpl_vars['p_h_v']) ? $_smarty_tpl->tpl_vars['p_h_v'] : false;
$__foreach_p_h_v_7_saved_key = isset($_smarty_tpl->tpl_vars['p_h_k']) ? $_smarty_tpl->tpl_vars['p_h_k'] : false;
$_smarty_tpl->tpl_vars['p_h_v'] = new Smarty_Variable();
$__foreach_p_h_v_7_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_p_h_v_7_total) {
$_smarty_tpl->tpl_vars['p_h_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['p_h_k']->value => $_smarty_tpl->tpl_vars['p_h_v']->value) {
$__foreach_p_h_v_7_saved_local_item = $_smarty_tpl->tpl_vars['p_h_v'];
?>
                <div class="form-group col-lg-12">
                    <input type="hidden" name="id_personnel_insurance[]" value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['id_personnel_insurance'];?>
">
                    <input type="hidden" name="id_personnel_insurance_type[]" value="1">
                    <input type="hidden" name="id_personnel_insurance_e_date[]">
                    <input type="hidden" name="id_personnel_deduction_type[]">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保單位:</span>
                            <select class="form-control" name="id_personnel_insurance_id_company[]">
                                <option value="">請選擇單位</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_8_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_8_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_8_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_8_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_8_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
" <?php if ($_smarty_tpl->tpl_vars['p_h_v']->value['id_company'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_company']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_8_saved_local_item;
}
}
if ($__foreach_d_c_v_8_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_8_saved_item;
}
if ($__foreach_d_c_v_8_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_8_saved_key;
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保日期:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['s_date'];?>
">
                        </div>
                    </div>
                    <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">扣款方式:</span>
                            <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                                <option value="">請選擇單位</option>
                                <option value="0" <?php if ($_smarty_tpl->tpl_vars['p_h_v']->value['deduction_type'] == 0) {?>selected="selected"<?php }?>>公司轉帳</option>
                                <option value="1" <?php if ($_smarty_tpl->tpl_vars['p_h_v']->value['deduction_type'] == 1) {?>selected="selected"<?php }?>>憑單繳費</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">承辦人:</span>
                            <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['handler'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">修改時間:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['update_time'];?>
"  disabled="disabled">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">眷屬:</span>
                            <input class="form-control" type="text"  name="id_personnel_insurance_dependents[]" value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['dependents'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">投保金額:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['price'];?>
">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">保費:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]"  value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['insurance_price'];?>
">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">公司負擔:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['company_price'];?>
">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">自負額:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]"  value="<?php echo $_smarty_tpl->tpl_vars['p_h_v']->value['personal_price'];?>
">
                        </div>
                    </div>
                </div>
            <?php
$_smarty_tpl->tpl_vars['p_h_v'] = $__foreach_p_h_v_7_saved_local_item;
}
}
if ($__foreach_p_h_v_7_saved_item) {
$_smarty_tpl->tpl_vars['p_h_v'] = $__foreach_p_h_v_7_saved_item;
}
if ($__foreach_p_h_v_7_saved_key) {
$_smarty_tpl->tpl_vars['p_h_k'] = $__foreach_p_h_v_7_saved_key;
}
?>
        <?php }?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="">
            <input type="hidden" name="id_personnel_insurance_type[]" value="1">
            <input type="hidden" name="id_personnel_insurance_e_date[]">
            <input type="hidden" name="id_personnel_insurance_deduction_type[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_9_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_9_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_9_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_9_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_9_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_9_saved_local_item;
}
}
if ($__foreach_d_c_v_9_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_9_saved_item;
}
if ($__foreach_d_c_v_9_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_9_saved_key;
}
?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">
                </div>
            </div>
            <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0">公司轉帳</option>
                        <option value="1">憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">眷屬:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_dependents[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">公司負擔:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">自負額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="1"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
    <label for="accident">意外險</label>
    <div class="form-group col-lg-12">
        <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['accident'])) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['accident'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_p_a_v_10_saved_item = isset($_smarty_tpl->tpl_vars['p_a_v']) ? $_smarty_tpl->tpl_vars['p_a_v'] : false;
$__foreach_p_a_v_10_saved_key = isset($_smarty_tpl->tpl_vars['p_a_k']) ? $_smarty_tpl->tpl_vars['p_a_k'] : false;
$_smarty_tpl->tpl_vars['p_a_v'] = new Smarty_Variable();
$__foreach_p_a_v_10_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_p_a_v_10_total) {
$_smarty_tpl->tpl_vars['p_a_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['p_a_k']->value => $_smarty_tpl->tpl_vars['p_a_v']->value) {
$__foreach_p_a_v_10_saved_local_item = $_smarty_tpl->tpl_vars['p_a_v'];
?>
                <div class="form-group col-lg-12">
                    <input type="hidden" name="id_personnel_insurance[]" value="<?php echo $_smarty_tpl->tpl_vars['p_a_v']->value['id_personnel_insurance'];?>
">
                    <input type="hidden" name="id_personnel_insurance_type[]" value="2">
                    <input type="hidden" name="id_personnel_insurance_company_price[]">
                    <input type="hidden" name="id_personnel_insurance_personal_price[]">
                    <input type="hidden" name="id_personnel_insurance_dependents[]">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保單位:</span>
                            <select class="form-control" name="id_personnel_insurance_id_company[]">
                                <option value="">請選擇單位</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_11_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_11_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_11_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_11_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_11_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
" <?php if ($_smarty_tpl->tpl_vars['p_a_v']->value['id_company'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_company']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_11_saved_local_item;
}
}
if ($__foreach_d_c_v_11_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_11_saved_item;
}
if ($__foreach_d_c_v_11_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_11_saved_key;
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保日期:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="<?php echo $_smarty_tpl->tpl_vars['p_a_v']->value['s_date'];?>
">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding" style="padding-left: 60px">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">退保日期:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]" value="<?php echo $_smarty_tpl->tpl_vars['p_a_v']->value['e_date'];?>
">
                        </div>
                    </div>
                    <div class="col-lg-3 no_padding" style="padding-left: 122px">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">承辦人:</span>
                            <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" value="<?php echo $_smarty_tpl->tpl_vars['p_a_v']->value['handler'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">修改時間:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="<?php echo $_smarty_tpl->tpl_vars['p_a_v']->value['update_time'];?>
"  disabled="disabled">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">投保金額:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_a_v']->value['price'];?>
">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">保費:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_a_v']->value['insurance_price'];?>
">
                        </div>
                    </div>
                </div>
            <?php
$_smarty_tpl->tpl_vars['p_a_v'] = $__foreach_p_a_v_10_saved_local_item;
}
}
if ($__foreach_p_a_v_10_saved_item) {
$_smarty_tpl->tpl_vars['p_a_v'] = $__foreach_p_a_v_10_saved_item;
}
if ($__foreach_p_a_v_10_saved_key) {
$_smarty_tpl->tpl_vars['p_a_k'] = $__foreach_p_a_v_10_saved_key;
}
?>
        <?php }?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="">
            <input type="hidden" name="id_personnel_insurance_type[]" value="2">
            <input type="hidden" name="id_personnel_insurance_company_price[]">
            <input type="hidden" name="id_personnel_insurance_personal_price[]">
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_12_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_12_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_12_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_12_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_12_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_12_saved_local_item;
}
}
if ($__foreach_d_c_v_12_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_12_saved_item;
}
if ($__foreach_d_c_v_12_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_12_saved_key;
}
?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="padding-left: 60px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">退保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]">
                </div>
            </div>
            <div class="col-lg-3 no_padding" style="padding-left: 122px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="2"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
    <label for="personnel_ensure">人事保證險</label>
    <div class="form-group col-lg-12">
        <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['personnel_ensure'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['personnel_insurance']['personnel_ensure'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_p_p_e_v_13_saved_item = isset($_smarty_tpl->tpl_vars['p_p_e_v']) ? $_smarty_tpl->tpl_vars['p_p_e_v'] : false;
$__foreach_p_p_e_v_13_saved_key = isset($_smarty_tpl->tpl_vars['p_p_e_k']) ? $_smarty_tpl->tpl_vars['p_p_e_k'] : false;
$_smarty_tpl->tpl_vars['p_p_e_v'] = new Smarty_Variable();
$__foreach_p_p_e_v_13_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_p_p_e_v_13_total) {
$_smarty_tpl->tpl_vars['p_p_e_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['p_p_e_k']->value => $_smarty_tpl->tpl_vars['p_p_e_v']->value) {
$__foreach_p_p_e_v_13_saved_local_item = $_smarty_tpl->tpl_vars['p_p_e_v'];
?>
            <div class="form-group col-lg-12">
                <input type="hidden" name="id_personnel_insurance[]" value="<?php echo $_smarty_tpl->tpl_vars['p_p_e_v']->value['id_personnel_insurance'];?>
">
                <input type="hidden" name="id_personnel_insurance_type[]" value="3">
                <input type="hidden" name="id_personnel_insurance_company_price[]">
                <input type="hidden" name="id_personnel_insurance_personal_price[]">
                <input type="hidden" name="id_personnel_insurance_dependents[]">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">加保單位:</span>
                        <select class="form-control" name="id_personnel_insurance_id_company[]">
                            <option value="">請選擇單位</option>
                            <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_14_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_14_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_14_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_14_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_14_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
" <?php if ($_smarty_tpl->tpl_vars['p_p_e_v']->value['id_company'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_company']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_14_saved_local_item;
}
}
if ($__foreach_d_c_v_14_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_14_saved_item;
}
if ($__foreach_d_c_v_14_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_14_saved_key;
}
?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">加保日期:</span>
                        <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="<?php echo $_smarty_tpl->tpl_vars['p_p_e_v']->value['s_date'];?>
">
                    </div>
                </div>
                <div class="col-lg-2 no_padding" style="padding-left: 60px">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">退保日期:</span>
                        <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]" value="<?php echo $_smarty_tpl->tpl_vars['p_p_e_v']->value['e_date'];?>
">
                    </div>
                </div>
                <div class="col-lg-3 no_padding" style="padding-left: 122px">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">承辦人:</span>
                        <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32" value="<?php echo $_smarty_tpl->tpl_vars['p_p_e_v']->value['handler'];?>
">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">修改時間:</span>
                        <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="<?php echo $_smarty_tpl->tpl_vars['p_p_e_v']->value['update_time'];?>
" disabled="disabled">
                    </div>
                </div>
                <div class="col-lg-3 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">扣款方式:</span>
                        <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                            <option value="">請選擇單位</option>
                            <option value="0" <?php if ($_smarty_tpl->tpl_vars['p_p_e_v']->value['type'] == 0) {?>selected="selected"<?php }?>>公司轉帳</option>
                            <option value="1" <?php if ($_smarty_tpl->tpl_vars['p_p_e_v']->value['type'] == 1) {?>selected="selected"<?php }?>>憑單繳費</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">投保金額:</span>
                        <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_p_e_v']->value['price'];?>
">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">保費:</span>
                        <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]" value="<?php echo $_smarty_tpl->tpl_vars['p_p_e_v']->value['insurance_price'];?>
">
                    </div>
                </div>
            </div>
        <?php
$_smarty_tpl->tpl_vars['p_p_e_v'] = $__foreach_p_p_e_v_13_saved_local_item;
}
}
if ($__foreach_p_p_e_v_13_saved_item) {
$_smarty_tpl->tpl_vars['p_p_e_v'] = $__foreach_p_p_e_v_13_saved_item;
}
if ($__foreach_p_p_e_v_13_saved_key) {
$_smarty_tpl->tpl_vars['p_p_e_k'] = $__foreach_p_p_e_v_13_saved_key;
}
?>
        <?php }?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="">
            <input type="hidden" name="id_personnel_insurance_type[]" value="3">
            <input type="hidden" name="id_personnel_insurance_company_price[]">
            <input type="hidden" name="id_personnel_insurance_personal_price[]">
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_15_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_15_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_15_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_15_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_15_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_15_saved_local_item;
}
}
if ($__foreach_d_c_v_15_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_15_saved_item;
}
if ($__foreach_d_c_v_15_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_15_saved_key;
}
?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="padding-left: 60px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">退保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]">
                </div>
            </div>
            <div class="col-lg-3 no_padding" style="padding-left: 122px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0">公司轉帳</option>
                        <option value="1">憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="3"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
<?php }?>
<label>基本資料</label>
<div class="form-group col-lg-12">
    <div class="form-group col-lg-12">
        <?php if ($_smarty_tpl->tpl_vars['personnel_special']->value == 1) {?>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">人力編號:</span>
                    <input class="form-control" type="text"  name="admin_number" id="admin_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['admin_number'];?>
" maxlength="32">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">分享代碼:</span>
                    <input class="form-control" type="text"  name="share_number" id="share_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['share_number'];?>
" maxlength="32">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">雲總機:</span>
                    <input class="form-control" type="text"  name="switchboard" id="switchboard" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['switchboard'];?>
" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">出勤代碼:</span>
                    <input class="form-control" type="text"  name="attendance_number" id="attendance_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['attendance_number'];?>
" maxlength="32">
                </div>
            </div>
        <?php } else { ?>

            <input class="form-control" type="hidden"  name="admin_number" id="admin_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['admin_number'];?>
" maxlength="32">
            <input class="form-control" type="hidden"  name="share_number" id="share_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['share_number'];?>
" maxlength="32">
            <input class="form-control" type="hidden"  name="switchboard" id="switchboard" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['switchboard'];?>
" maxlength="32">
            <input class="form-control" type="hidden"  name="attendance_number" id="attendance_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['attendance_number'];?>
" maxlength="32">

            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">人力編號:</span>
                    <input class="form-control" type="text"  name="admin_number" id="admin_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['admin_number'];?>
" maxlength="32" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">分享代碼:</span>
                    <input class="form-control" type="text"  name="share_number" id="share_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['share_number'];?>
" maxlength="32" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">雲總機:</span>
                    <input class="form-control" type="text"  name="switchboard" id="switchboard" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['switchboard'];?>
" maxlength="32" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">出勤代碼:</span>
                    <input class="form-control" type="text"  name="attendance_number" id="attendance_number" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['attendance_number'];?>
" maxlength="32" disabled="disabled">
                </div>
            </div>
        <?php }?>


        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">姓:</span>
                <input class="form-control" type="text"  name="last_name" id="last_name" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['last_name'];?>
" maxlength="10" required="required" >
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">名:</span>
                <input class="form-control" type="text"  name="first_name" id="first_name" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['first_name'];?>
" maxlength="10" required="required">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">英文姓名:</span>
                <input class="form-control" type="text"  name="en_name" id="en_name" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['en_name'];?>
" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">性別:</span>
                <select class="form-control" name="gender" id="gender">
                    <option value="">請選擇</option>
                    <option value="0" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['gender'] == 0) {?>selected="selected"<?php }?>>男</option>
                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['gender'] == 1) {?>selected="selected"<?php }?>>女</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">身分證號碼:</span>
                <input class="form-control" type="text"  name="identity" id="identity" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['identity'];?>
" maxlength="16">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">生日:</span>
                <input class="form-control" type="date"  name="birthday" id="birthday" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['birthday'];?>
">
            </div>
        </div>

        <div class="col-lg-4 no_padding" style="left: 37px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">Line id:</span>
                <input class="form-control" type="text"  name="line_id" id="line_id" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['line_id'];?>
" maxlength="128">
            </div>
        </div>
        <div class="col-lg-5 no_padding" style="left: 35px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">email:</span>
                <input class="form-control" type="text"  name="private_email" id="private_email"  value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['private_email'];?>
" maxlength="128">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">健康狀況:</span>
                <select class="form-control" name="health" id="health">
                    <option value="">請選擇狀況</option>
                    <option value="0" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['health'] == 0) {?>selected="selected"<?php }?>>良好</option>
                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['health'] == 1) {?>selected="selected"<?php }?>>有疾病</option>
                    <option value="2" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['health'] == 2) {?>selected="selected"<?php }?>>領有身心障礙手冊</option>
                </select>
            </div>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">疾病:</span>
                <input class="form-control" type="text"  name="disease" id="disease" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['disease'];?>
" maxlength="128">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">宗教:</span>
                <select class="form-control" name="religion" id="religion">
                    <option value="">請選擇信仰</option>
                    <option value="0" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['religion'] == 0) {?>selected="selected"<?php }?>>道教</option>
                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['religion'] == 1) {?>selected="selected"<?php }?>>佛教</option>
                    <option value="2" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['religion'] == 2) {?>selected="selected"<?php }?>>天主教</option>
                    <option value="3" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['religion'] == 3) {?>selected="selected"<?php }?>>基督教(新教)</option>
                    <option value="4" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['religion'] == 4) {?>selected="selected"<?php }?>>回教</option>
                    <option value="5" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['religion'] == 5) {?>selected="selected"<?php }?>>印度教</option>
                    <option value="6" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['religion'] == 6) {?>selected="selected"<?php }?>>摩門教</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">戶籍電話:</span>
                <input class="form-control" type="text"  name="domicile_tel" id="domicile_tel" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['domicile_tel'];?>
" maxlength="32">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">連絡電話:</span>
                <input class="form-control" type="text"  name="contact_tel" id="contact_tel" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['contact_tel'];?>
" maxlength="32">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">手機:</span>
                <input class="form-control" type="text"  name="phone" id="contact_tel" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['contact_tel'];?>
" maxlength="32">
            </div>
        </div>
    </div>
</div>

<div class="form-group col-lg-12" >
    <label>連絡地址</label>
    <input type="hidden" name="id_address_domicile" class="" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['id_address_domicile'];?>
">
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_address_domicile_postal"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['postal'];?>
" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_domicile_id_county" class="form-control county">
                <option value="">請選擇縣市</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_16_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_16_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_16_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_16_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_16_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_county'] == $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['id_county']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_16_saved_local_item;
}
}
if ($__foreach_d_c_v_16_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_16_saved_item;
}
if ($__foreach_d_c_v_16_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_16_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_domicile_id_city" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_17_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_17_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_17_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_17_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_17_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
"  <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_city'] == $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['id_city']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_17_saved_local_item;
}
}
if ($__foreach_d_c_v_17_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_17_saved_item;
}
if ($__foreach_d_c_v_17_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_17_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_address_domicile_village" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_address_domicile_neighbor" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_address_domicile_road" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['road'];?>
" maxlength="20"><span class="input-group-addon">填全名</span>
            </div>
        </div>
        <div class="form-group col-lg-12">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_address_domicile_segment" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_address_domicile_lane" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_address_domicile_alley" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_address_domicile_no" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']["no"];?>
" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_domicile_no_her" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_domicile_floor" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_domicile_floor_her" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_domicile_address_room" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9">
            <input type="text" name="id_address_domicile_address" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_domicile']['address'];?>
" maxlength="255" placeholder="完整地址">
        </div>
    </div>
</div>
<div class="form-group col-lg-12" >
    <label>居住地址</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input same_address" type="checkbox" id="same_address" name="same_address" value="1" <?php if ($_smarty_tpl->tpl_vars['database_data']->value['same_address']) {?>checked<?php }?> >
        <label class="form-check-label" >同上地址</label>
    </div>
    <input type="hidden" name="id_address_contact" class="" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['id_address_contact'];?>
">
    <div class="form-group col-lg-12 same_address">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg" >
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_address_contact_postal"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['postal'];?>
" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_contact_id_county" class="form-control county">
                <option value="">請選擇縣市</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_18_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_18_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_18_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_18_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_18_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_county'] == $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['id_county']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_18_saved_local_item;
}
}
if ($__foreach_d_c_v_18_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_18_saved_item;
}
if ($__foreach_d_c_v_18_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_18_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_contact_id_city" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_19_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_19_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_19_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_19_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_19_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_city'] == $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['id_city']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_19_saved_local_item;
}
}
if ($__foreach_d_c_v_19_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_19_saved_item;
}
if ($__foreach_d_c_v_19_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_19_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_address_contact_village" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_address_contact_neighbor" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_address_contact_road" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['road'];?>
" maxlength="20"><span class="input-group-addon">填全名</span>
            </div>
        </div>
        <div class="form-group col-lg-12">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_address_contact_segment" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_address_contact_lane" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_address_contact_alley" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_address_contact_no" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['no'];?>
" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_contact_no_her" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_contact_floor" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_contact_floor_her" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_contact_address_room" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9">
            <input type="text" name="id_address_contact_address" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['address_contact']['address'];?>
" maxlength="255" placeholder="完整地址">
        </div>
    </div>
</div>

<div class="form-group col-lg-12" >
    <label>家庭成員</label>
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['admin_family'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['admin_family'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_d_a_f_v_20_saved_item = isset($_smarty_tpl->tpl_vars['d_d_a_f_v']) ? $_smarty_tpl->tpl_vars['d_d_a_f_v'] : false;
$__foreach_d_d_a_f_v_20_saved_key = isset($_smarty_tpl->tpl_vars['d_d_a_f_k']) ? $_smarty_tpl->tpl_vars['d_d_a_f_k'] : false;
$_smarty_tpl->tpl_vars['d_d_a_f_v'] = new Smarty_Variable();
$__foreach_d_d_a_f_v_20_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_d_a_f_v_20_total) {
$_smarty_tpl->tpl_vars['d_d_a_f_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_d_a_f_k']->value => $_smarty_tpl->tpl_vars['d_d_a_f_v']->value) {
$__foreach_d_d_a_f_v_20_saved_local_item = $_smarty_tpl->tpl_vars['d_d_a_f_v'];
?>
            <?php if ($_smarty_tpl->tpl_vars['d_d_a_f_v']->value['urgent_type'] == 0) {?>
                <div class="form-group col-lg-12">
                <input type="hidden" name="id_admin_family[]" class="" value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['id_admin_family'];?>
">
                <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="0">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['relation'];?>
" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['name'];?>
" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['tel'];?>
" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['phone'];?>
" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['job'];?>
" maxlength="32">
                    </div>
                </div>
                <input type="hidden" name="id_admin_family_id_address[]" value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['id_address'];?>
">
                <div class="col-lg-2 no_padding" style="display:none;">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['postal'];?>
" maxlength="11">
                    </div>
                </div>
                <div class="col-lg-2 no_padding" style="display:none;">
                    <select name="id_admin_family_id_county[]" class="form-control county">
                        <option value="">請選擇縣市</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_21_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_21_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_21_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_21_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_21_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_county'] == $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['id_county']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_21_saved_local_item;
}
}
if ($__foreach_d_c_v_21_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_21_saved_item;
}
if ($__foreach_d_c_v_21_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_21_saved_key;
}
?>
                    </select>
                </div>
                <div class="col-lg-2 no_padding" style="display:none;">
                    <select name="id_admin_family_id_city[]" class="form-control city">
                        <option value="">請選擇鄉鎮區</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_22_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_22_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_22_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_22_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_22_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_city'] == $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['id_city']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_22_saved_local_item;
}
}
if ($__foreach_d_c_v_22_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_22_saved_item;
}
if ($__foreach_d_c_v_22_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_22_saved_key;
}
?>
                    </select>
                </div>
                <div class="col-lg-5 no_padding" style="display:none;">
                    <div class="input-group fixed-width-lg">
                        <input type="text"   name="id_admin_family_village[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                        <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                        <input type="text"   name="id_admin_family_road[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['road'];?>
" maxlength="20"><span class="input-group-addon">填全名</span>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="display:none;">
                    <div class="input-group fixed-width-lg">
                        <input type="text" name="id_admin_family_segment[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                        <input type="text" name="id_admin_family_lane[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                        <input type="text" name="id_admin_family_alley[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                        <input type="text" name="id_admin_family_no[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['no'];?>
" maxlength="10"><span class="input-group-addon">號</span>
                        <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                        <input type="text" name="id_admin_family_floor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                        <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                        <input type="text" name="id_admin_family_address_room[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
                    </div>
                </div>
                <div class="form-group col-lg-9" style="display:none;">
                    <input type="text" name="id_admin_family_address[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['address'];?>
" maxlength="255" placeholder="完整地址">
                </div>
            </div>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['d_d_a_f_v'] = $__foreach_d_d_a_f_v_20_saved_local_item;
}
}
if ($__foreach_d_d_a_f_v_20_saved_item) {
$_smarty_tpl->tpl_vars['d_d_a_f_v'] = $__foreach_d_d_a_f_v_20_saved_item;
}
if ($__foreach_d_d_a_f_v_20_saved_key) {
$_smarty_tpl->tpl_vars['d_d_a_f_k'] = $__foreach_d_d_a_f_v_20_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_admin_family[]" class="" value="">
        <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="0">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>

        <input type="hidden" name="id_admin_family_id_address[]" value="" style="display:none;">
        <div class="col-lg-2 no_padding" style="display:none;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="display:none;">
            <select name="id_admin_family_id_county[]" class="form-control county">
                <option value="">請選擇縣市</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_23_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_23_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_23_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_23_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_23_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_23_saved_local_item;
}
}
if ($__foreach_d_c_v_23_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_23_saved_item;
}
if ($__foreach_d_c_v_23_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_23_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-2 no_padding" style="display:none;">
            <select name="id_admin_family_id_city[]" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_24_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_24_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_24_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_24_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_24_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_24_saved_local_item;
}
}
if ($__foreach_d_c_v_24_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_24_saved_item;
}
if ($__foreach_d_c_v_24_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_24_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-5 no_padding" style="display:none;">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_admin_family_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_admin_family_road[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">填全名</span>
            </div>
        </div>
        <div class="form-group col-lg-12" style="display:none;">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_admin_family_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_admin_family_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_admin_family_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_admin_family_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9" style="display:none;">
            <input type="text" name="id_admin_family_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="admin_family"  data-type="0"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<div class="form-group col-lg-12" >
    <label>緊急聯絡人</label>
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['admin_family'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['admin_family'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_d_a_f_v_25_saved_item = isset($_smarty_tpl->tpl_vars['d_d_a_f_v']) ? $_smarty_tpl->tpl_vars['d_d_a_f_v'] : false;
$__foreach_d_d_a_f_v_25_saved_key = isset($_smarty_tpl->tpl_vars['d_d_a_f_k']) ? $_smarty_tpl->tpl_vars['d_d_a_f_k'] : false;
$_smarty_tpl->tpl_vars['d_d_a_f_v'] = new Smarty_Variable();
$__foreach_d_d_a_f_v_25_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_d_a_f_v_25_total) {
$_smarty_tpl->tpl_vars['d_d_a_f_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_d_a_f_k']->value => $_smarty_tpl->tpl_vars['d_d_a_f_v']->value) {
$__foreach_d_d_a_f_v_25_saved_local_item = $_smarty_tpl->tpl_vars['d_d_a_f_v'];
?>
            <?php if ($_smarty_tpl->tpl_vars['d_d_a_f_v']->value['urgent_type'] == 1) {?>
                <div class="form-group col-lg-12">
                    <input type="hidden" name="id_admin_family[]" class="" value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['id_admin_family'];?>
">
                    <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="1">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['relation'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['name'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['tel'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['phone'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['job'];?>
" maxlength="32">
                        </div>
                    </div>
                    <input type="hidden" name="id_admin_family_id_address[]" value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['id_address'];?>
">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['postal'];?>
" maxlength="11">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <select name="id_admin_family_id_county[]" class="form-control county">
                            <option value="">請選擇縣市</option>
                            <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_26_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_26_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_26_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_26_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_26_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_county'] == $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['id_county']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_26_saved_local_item;
}
}
if ($__foreach_d_c_v_26_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_26_saved_item;
}
if ($__foreach_d_c_v_26_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_26_saved_key;
}
?>
                        </select>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <select name="id_admin_family_id_city[]" class="form-control city">
                            <option value="">請選擇鄉鎮區</option>
                            <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_27_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_27_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_27_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_27_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_27_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['id_city'] == $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['id_city']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_27_saved_local_item;
}
}
if ($__foreach_d_c_v_27_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_27_saved_item;
}
if ($__foreach_d_c_v_27_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_27_saved_key;
}
?>
                        </select>
                    </div>
                    <div class="col-lg-5 no_padding">
                        <div class="input-group fixed-width-lg">
                            <input type="text"   name="id_admin_family_village[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                            <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                            <input type="text"   name="id_admin_family_road[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['road'];?>
" maxlength="20"><span class="input-group-addon">填全名</span>
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="input-group fixed-width-lg">
                            <input type="text" name="id_admin_family_segment[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                            <input type="text" name="id_admin_family_lane[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                            <input type="text" name="id_admin_family_alley[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                            <input type="text" name="id_admin_family_no[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['no'];?>
" maxlength="10"><span class="input-group-addon">號</span>
                            <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                            <input type="text" name="id_admin_family_floor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                            <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                            <input type="text" name="id_admin_family_address_room[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
                        </div>
                    </div>
                    <div class="form-group col-lg-9">
                        <input type="text" name="id_admin_family_address[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['d_d_a_f_v']->value['address']['address'];?>
" maxlength="255" placeholder="完整地址">
                    </div>
                </div>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['d_d_a_f_v'] = $__foreach_d_d_a_f_v_25_saved_local_item;
}
}
if ($__foreach_d_d_a_f_v_25_saved_item) {
$_smarty_tpl->tpl_vars['d_d_a_f_v'] = $__foreach_d_d_a_f_v_25_saved_item;
}
if ($__foreach_d_d_a_f_v_25_saved_key) {
$_smarty_tpl->tpl_vars['d_d_a_f_k'] = $__foreach_d_d_a_f_v_25_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_admin_family[]" class="" value="">
        <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="1">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>

        <input type="hidden" name="id_admin_family_id_address[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_admin_family_id_county[]" class="form-control county">
                <option value="">請選擇縣市</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_28_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_28_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_28_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_28_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_28_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_28_saved_local_item;
}
}
if ($__foreach_d_c_v_28_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_28_saved_item;
}
if ($__foreach_d_c_v_28_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_28_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_admin_family_id_city[]" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_29_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_29_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_29_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_29_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_29_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_29_saved_local_item;
}
}
if ($__foreach_d_c_v_29_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_29_saved_item;
}
if ($__foreach_d_c_v_29_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_29_saved_key;
}
?>
            </select>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_admin_family_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_admin_family_road[]" class="form-control " value="" maxlength="20">
            </div>
        </div>
        <div class="form-group col-lg-12">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_admin_family_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_admin_family_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_admin_family_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_admin_family_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9">
            <input type="text" name="id_admin_family_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="admin_family"  data-type="1"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>

<label>學歷</label>
<div class="form-group col-lg-12">
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['education'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['education'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_e_v_30_saved_item = isset($_smarty_tpl->tpl_vars['d_e_v']) ? $_smarty_tpl->tpl_vars['d_e_v'] : false;
$__foreach_d_e_v_30_saved_key = isset($_smarty_tpl->tpl_vars['d_e_k']) ? $_smarty_tpl->tpl_vars['d_e_k'] : false;
$_smarty_tpl->tpl_vars['d_e_v'] = new Smarty_Variable();
$__foreach_d_e_v_30_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_e_v_30_total) {
$_smarty_tpl->tpl_vars['d_e_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_e_k']->value => $_smarty_tpl->tpl_vars['d_e_v']->value) {
$__foreach_d_e_v_30_saved_local_item = $_smarty_tpl->tpl_vars['d_e_v'];
?>
            <div class="form-group col-lg-12">
                <input type="hidden" name="id_education[]" value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['id_education'];?>
">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">學歷:</span>
                        <select class="form-control" name="id_education_education_level[]">
                            <option value="">請選擇學歷</option>
                            <option value="0" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['education_level'] == 0) {?>selected="selected"<?php }?>>高中以下</option>
                            <option value="1" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['education_level'] == 1) {?>selected="selected"<?php }?>>二技</option>
                            <option value="2" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['education_level'] == 2) {?>selected="selected"<?php }?>>四技</option>
                            <option value="3" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['education_level'] == 3) {?>selected="selected"<?php }?>>大學</option>
                            <option value="4" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['education_level'] == 4) {?>selected="selected"<?php }?>>碩士</option>
                            <option value="5" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['education_level'] == 5) {?>selected="selected"<?php }?>>博士</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">學校:</span>
                        <input class="form-control" type="text"  name="id_education_school[]" value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['school'];?>
"   maxlength="64">
                    </div>
                </div>
                <div class="col-lg-3 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">科系:</span>
                        <input class="form-control" type="text"  name="id_education_department[]" value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['department'];?>
"  maxlength="64">
                    </div>
                </div>　
                <div class="col-lg-2 no_padding" style="left: -15px;">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">畢/肄業:</span>
                        <select class="form-control" name="id_education_graduation[]">
                            <option value="">是否畢業</option>
                            <option value="0" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['graduation'] == 0) {?>selected="selected"<?php }?>>畢業</option>
                            <option value="1" <?php if ($_smarty_tpl->tpl_vars['d_e_v']->value['graduation'] == 1) {?>selected="selected"<?php }?>>肄業</option>
                        </select>
                    </div>
                </div>
            </div>
        <?php
$_smarty_tpl->tpl_vars['d_e_v'] = $__foreach_d_e_v_30_saved_local_item;
}
}
if ($__foreach_d_e_v_30_saved_item) {
$_smarty_tpl->tpl_vars['d_e_v'] = $__foreach_d_e_v_30_saved_item;
}
if ($__foreach_d_e_v_30_saved_key) {
$_smarty_tpl->tpl_vars['d_e_k'] = $__foreach_d_e_v_30_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_education[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">學歷:</span>
                <select class="form-control" name="id_education_education_level[]">
                    <option value="">請選擇學歷</option>
                    <option value="0">高中以下</option>
                    <option value="1">二技</option>
                    <option value="2">四技</option>
                    <option value="3">大學</option>
                    <option value="4">碩士</option>
                    <option value="5">博士</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">學校:</span>
                <input class="form-control" type="text"  name="id_education_school[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">科系:</span>
                <input class="form-control" type="text"  name="id_education_department[]"   maxlength="64">
            </div>
        </div>　
        <div class="col-lg-2 no_padding" style="left: -15px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">畢/肄業:</span>
                <select class="form-control" name="id_education_graduation[]">
                    <option value="">是否畢業</option>
                    <option value="0">畢業</option>
                    <option value="1">肄業</option>
                </select>
            </div>
        </div>
        <div class="col-lg-1 no_padding" style="left: -15px;">
            <button type="button" class="btn btn-secondary " data-table="education" data-type="" onclick="add_personnel_data(this)">+</button>
        </div>
    </div>
</div>

<label>語言</label>
<div class="form-group col-lg-12">
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['language'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['language'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_l_v_31_saved_item = isset($_smarty_tpl->tpl_vars['d_l_v']) ? $_smarty_tpl->tpl_vars['d_l_v'] : false;
$__foreach_d_l_v_31_saved_key = isset($_smarty_tpl->tpl_vars['d_l_k']) ? $_smarty_tpl->tpl_vars['d_l_k'] : false;
$_smarty_tpl->tpl_vars['d_l_v'] = new Smarty_Variable();
$__foreach_d_l_v_31_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_l_v_31_total) {
$_smarty_tpl->tpl_vars['d_l_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_l_k']->value => $_smarty_tpl->tpl_vars['d_l_v']->value) {
$__foreach_d_l_v_31_saved_local_item = $_smarty_tpl->tpl_vars['d_l_v'];
?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_language[]" value="<?php echo $_smarty_tpl->tpl_vars['d_l_v']->value['id_language'];?>
">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">語言:</span>
                    <select class="form-control" name="id_language_name[]">
                        <option value="">選擇語言</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['name'] == 1) {?>selected="selected"<?php }?>>中文</option>
                        <option value="2" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['name'] == 2) {?>selected="selected"<?php }?>>台語</option>
                        <option value="3" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['name'] == 3) {?>selected="selected"<?php }?>>英文</option>
                        <option value="4" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['name'] == 4) {?>selected="selected"<?php }?>>日文</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">聽:</span>
                    <select class="form-control" name="id_language_listen[]">
                        <option value="">選擇程度</option>
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['listen'] == 0) {?>selected="selected"<?php }?>>不懂</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['listen'] == 1) {?>selected="selected"<?php }?>>略懂</option>
                        <option value="2" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['listen'] == 2) {?>selected="selected"<?php }?>>中等</option>
                        <option value="3" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['listen'] == 3) {?>selected="selected"<?php }?>>精通</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">說:</span>
                    <select class="form-control" name="id_language_speak[]">
                        <option value="">選擇程度</option>
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['speak'] == 0) {?>selected="selected"<?php }?>>不懂</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['speak'] == 1) {?>selected="selected"<?php }?>>略懂</option>
                        <option value="2" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['speak'] == 2) {?>selected="selected"<?php }?>>中等</option>
                        <option value="3" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['speak'] == 3) {?>selected="selected"<?php }?>>精通</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">讀:</span>
                    <select class="form-control" name="id_language_read[]">
                        <option value="">選擇程度</option>
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['read'] == 0) {?>selected="selected"<?php }?>>不懂</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['read'] == 1) {?>selected="selected"<?php }?>>略懂</option>
                        <option value="2" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['read'] == 2) {?>selected="selected"<?php }?>>中等</option>
                        <option value="3" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['read'] == 3) {?>selected="selected"<?php }?>>精通</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">寫:</span>
                    <select class="form-control" name="id_language_write[]">
                        <option value="">選擇程度</option>
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['write'] == 0) {?>selected="selected"<?php }?>>不懂</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['write'] == 1) {?>selected="selected"<?php }?>>略懂</option>
                        <option value="2" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['write'] == 2) {?>selected="selected"<?php }?>>中等</option>
                        <option value="3" <?php if ($_smarty_tpl->tpl_vars['d_l_v']->value['write'] == 3) {?>selected="selected"<?php }?>>精通</option>
                    </select>
                </div>
            </div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['d_l_v'] = $__foreach_d_l_v_31_saved_local_item;
}
}
if ($__foreach_d_l_v_31_saved_item) {
$_smarty_tpl->tpl_vars['d_l_v'] = $__foreach_d_l_v_31_saved_item;
}
if ($__foreach_d_l_v_31_saved_key) {
$_smarty_tpl->tpl_vars['d_l_k'] = $__foreach_d_l_v_31_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_language[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">語言:</span>
                <select class="form-control" name="id_language_name[]">
                    <option value="">選擇語言</option>
                    <option value="1">中文</option>
                    <option value="2">台語</option>
                    <option value="3">英文</option>
                    <option value="4">日文</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">聽:</span>
                <select class="form-control" name="id_language_listen[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">說:</span>
                <select class="form-control" name="id_language_speak[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">讀:</span>
                <select class="form-control" name="id_language_read[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">寫:</span>
                <select class="form-control" name="id_language_write[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="language"  data-type=""  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>經歷</label>
<div class="form-group col-lg-12">
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['experience'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['experience'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_e_v_32_saved_item = isset($_smarty_tpl->tpl_vars['d_e_v']) ? $_smarty_tpl->tpl_vars['d_e_v'] : false;
$__foreach_d_e_v_32_saved_key = isset($_smarty_tpl->tpl_vars['d_e_k']) ? $_smarty_tpl->tpl_vars['d_e_k'] : false;
$_smarty_tpl->tpl_vars['d_e_v'] = new Smarty_Variable();
$__foreach_d_e_v_32_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_e_v_32_total) {
$_smarty_tpl->tpl_vars['d_e_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_e_k']->value => $_smarty_tpl->tpl_vars['d_e_v']->value) {
$__foreach_d_e_v_32_saved_local_item = $_smarty_tpl->tpl_vars['d_e_v'];
?>
            <div class="form-group col-lg-12">
                <input type="hidden" name="id_experience[]" value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['id_experience'];?>
">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">公司:</span>
                        <input class="form-control" type="text"  name="id_experience_company[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['company'];?>
" maxlength="64">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">工作:</span>
                        <input class="form-control" type="text"  name="id_experience_job[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['job'];?>
" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">待遇:</span>
                        <input class="form-control" type="number"  name="id_experience_salary[]" value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['salary'];?>
">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">開始時間:</span>
                        <input class="form-control" type="date"  name="id_experience_s_date[]" value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['s_date'];?>
">
                    </div>
                </div>
                <div class="col-lg-2 no_padding" style="left: 65px;">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">結束時間:</span>
                        <input class="form-control" type="date"  name="id_experience_e_date[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_e_v']->value['e_date'];?>
">
                    </div>
                </div>
            </div>
        <?php
$_smarty_tpl->tpl_vars['d_e_v'] = $__foreach_d_e_v_32_saved_local_item;
}
}
if ($__foreach_d_e_v_32_saved_item) {
$_smarty_tpl->tpl_vars['d_e_v'] = $__foreach_d_e_v_32_saved_item;
}
if ($__foreach_d_e_v_32_saved_key) {
$_smarty_tpl->tpl_vars['d_e_k'] = $__foreach_d_e_v_32_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_experience[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">公司:</span>
                <input class="form-control" type="text"  name="id_experience_company[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">工作:</span>
                <input class="form-control" type="text"  name="id_experience_job[]"   maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">待遇:</span>
                <input class="form-control" type="number"  name="id_experience_salary[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">開始時間:</span>
                <input class="form-control" type="date"  name="id_experience_s_date[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">結束時間:</span>
                <input class="form-control" type="date"  name="id_experience_e_date[]">
            </div>
        </div>
        <div class="col-lg-1 no_padding" style="left: 133px;">
            <button type="button" class="btn btn-secondary " data-table="experience"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>專業證照</label>
<div class="form-group col-lg-12">
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['license'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['license'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_l_v_33_saved_item = isset($_smarty_tpl->tpl_vars['d_l_v']) ? $_smarty_tpl->tpl_vars['d_l_v'] : false;
$__foreach_d_l_v_33_saved_key = isset($_smarty_tpl->tpl_vars['d_l_k']) ? $_smarty_tpl->tpl_vars['d_l_k'] : false;
$_smarty_tpl->tpl_vars['d_l_v'] = new Smarty_Variable();
$__foreach_d_l_v_33_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_l_v_33_total) {
$_smarty_tpl->tpl_vars['d_l_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_l_k']->value => $_smarty_tpl->tpl_vars['d_l_v']->value) {
$__foreach_d_l_v_33_saved_local_item = $_smarty_tpl->tpl_vars['d_l_v'];
?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_license[]" value="<?php echo $_smarty_tpl->tpl_vars['d_l_v']->value['id_license'];?>
">
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">證照名稱:</span>
                    <input class="form-control" type="text"  name="id_license_name[]" value="<?php echo $_smarty_tpl->tpl_vars['d_l_v']->value['name'];?>
"  maxlength="64">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">開始時間:</span>
                    <input class="form-control" type="date"  name="id_license_s_date[]" value="<?php echo $_smarty_tpl->tpl_vars['d_l_v']->value['s_date'];?>
">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px;">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">結束時間:</span>
                    <input class="form-control" type="date"  name="id_license_e_date[]" value="<?php echo $_smarty_tpl->tpl_vars['d_l_v']->value['e_date'];?>
">
                </div>
            </div>
            <div class="col-lg-3 no_padding" style="left: 130px;">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">備註:</span>
                    <input class="form-control" type="text"  name="id_license_remarks[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_l_v']->value['remarks'];?>
"   maxlength="128">
                </div>
            </div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['d_l_v'] = $__foreach_d_l_v_33_saved_local_item;
}
}
if ($__foreach_d_l_v_33_saved_item) {
$_smarty_tpl->tpl_vars['d_l_v'] = $__foreach_d_l_v_33_saved_item;
}
if ($__foreach_d_l_v_33_saved_key) {
$_smarty_tpl->tpl_vars['d_l_k'] = $__foreach_d_l_v_33_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_license[]" value="">
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">證照名稱:</span>
                <input class="form-control" type="text"  name="id_license_name[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">開始時間:</span>
                <input class="form-control" type="date"  name="id_license_s_date[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">結束時間:</span>
                <input class="form-control" type="date"  name="id_license_e_date[]">
            </div>
        </div>
        <div class="col-lg-3 no_padding" style="left: 130px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">備註:</span>
                <input class="form-control" type="text"  name="id_license_remarks[]"   maxlength="128">
            </div>
        </div>
        <div class="col-lg-1 no_padding"  style="left: 128px;">
            <button type="button" class="btn btn-secondary " data-table="license"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>人民團體</label>
<div class="form-group col-lg-12">
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['people_group'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['people_group'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_p_v_34_saved_item = isset($_smarty_tpl->tpl_vars['d_p_v']) ? $_smarty_tpl->tpl_vars['d_p_v'] : false;
$__foreach_d_p_v_34_saved_key = isset($_smarty_tpl->tpl_vars['d_p_k']) ? $_smarty_tpl->tpl_vars['d_p_k'] : false;
$_smarty_tpl->tpl_vars['d_p_v'] = new Smarty_Variable();
$__foreach_d_p_v_34_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_p_v_34_total) {
$_smarty_tpl->tpl_vars['d_p_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_p_k']->value => $_smarty_tpl->tpl_vars['d_p_v']->value) {
$__foreach_d_p_v_34_saved_local_item = $_smarty_tpl->tpl_vars['d_p_v'];
?>
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_people_group[]" value="<?php echo $_smarty_tpl->tpl_vars['d_p_v']->value['id_people_group'];?>
">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">團體:</span>
                    <select name="id_people_group_id_company[]"  class="form-control">
                        <option value="">請選擇團體</option>
                        <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_35_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_35_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_35_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_35_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_35_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                            <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['type'] == 1) {?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_p_v']->value['id_company'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_company']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                            <?php }?>
                        <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_35_saved_local_item;
}
}
if ($__foreach_d_c_v_35_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_35_saved_item;
}
if ($__foreach_d_c_v_35_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_35_saved_key;
}
?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">入會日期:</span>
                    <input class="form-control" type="date"  name="id_people_group_s_date[]" value="<?php echo $_smarty_tpl->tpl_vars['d_p_v']->value['s_date'];?>
">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">會員編號:</span>
                    <input class="form-control" type="text"  name="id_people_group_number[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_p_v']->value['number'];?>
"  maxlength="64">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">會費:</span>
                    <input class="form-control" type="number"  name="id_people_group_due[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_p_v']->value['due'];?>
">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">年度:</span>
                    <input class="form-control" type="text"  name="id_people_group_year[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_p_v']->value['year'];?>
" maxlength="8">
                </div>
            </div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['d_p_v'] = $__foreach_d_p_v_34_saved_local_item;
}
}
if ($__foreach_d_p_v_34_saved_item) {
$_smarty_tpl->tpl_vars['d_p_v'] = $__foreach_d_p_v_34_saved_item;
}
if ($__foreach_d_p_v_34_saved_key) {
$_smarty_tpl->tpl_vars['d_p_k'] = $__foreach_d_p_v_34_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_people_group[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">團體:</span>
                <select name="id_people_group_id_company[]"  class="form-control">
                    <option value="">請選擇團體</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['database_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_36_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_36_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_36_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_36_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_36_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                        <?php if ($_smarty_tpl->tpl_vars['d_c_v']->value['type'] == 1) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_company'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['name'];?>
</option>
                        <?php }?>
                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_36_saved_local_item;
}
}
if ($__foreach_d_c_v_36_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_36_saved_item;
}
if ($__foreach_d_c_v_36_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_36_saved_key;
}
?>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">入會日期:</span>
                <input class="form-control" type="date"  name="id_people_group_s_date[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">會員編號:</span>
                <input class="form-control" type="text"  name="id_people_group_number[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">會費:</span>
                <input class="form-control" type="number"  name="id_people_group_due[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">年度:</span>
                <input class="form-control" type="text"  name="id_people_group_year[]" maxlength="8">
            </div>
        </div>
        <div class="col-lg-1 no_padding" style="left: 65px">
            <button type="button" class="btn btn-secondary " data-table="people_group"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>專長/技能</label>
<div class="form-group col-lg-12">
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['id_skill'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['id_skill'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_d_s_v_37_saved_item = isset($_smarty_tpl->tpl_vars['d_d_s_v']) ? $_smarty_tpl->tpl_vars['d_d_s_v'] : false;
$__foreach_d_d_s_v_37_saved_key = isset($_smarty_tpl->tpl_vars['d_d_s_k']) ? $_smarty_tpl->tpl_vars['d_d_s_k'] : false;
$_smarty_tpl->tpl_vars['d_d_s_v'] = new Smarty_Variable();
$__foreach_d_d_s_v_37_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_d_s_v_37_total) {
$_smarty_tpl->tpl_vars['d_d_s_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_d_s_k']->value => $_smarty_tpl->tpl_vars['d_d_s_v']->value) {
$__foreach_d_d_s_v_37_saved_local_item = $_smarty_tpl->tpl_vars['d_d_s_v'];
?>
        <div class="form-group col-lg-12">
            <div class="col-lg-12 no_padding" >
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">專長/技能:</span>
                    <span class="input-group-addon">類別:</span>
                    <select name="id_skill_class[]" class="form-control skill_class" >
                        <?php
$_from = $_smarty_tpl->tpl_vars['datebase_skill_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_c_v_38_saved_item = isset($_smarty_tpl->tpl_vars['d_s_c_v']) ? $_smarty_tpl->tpl_vars['d_s_c_v'] : false;
$__foreach_d_s_c_v_38_saved_key = isset($_smarty_tpl->tpl_vars['d_s_c_k']) ? $_smarty_tpl->tpl_vars['d_s_c_k'] : false;
$_smarty_tpl->tpl_vars['d_s_c_v'] = new Smarty_Variable();
$__foreach_d_s_c_v_38_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_c_v_38_total) {
$_smarty_tpl->tpl_vars['d_s_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_c_k']->value => $_smarty_tpl->tpl_vars['d_s_c_v']->value) {
$__foreach_d_s_c_v_38_saved_local_item = $_smarty_tpl->tpl_vars['d_s_c_v'];
?>
                            <option data-child="id_skill_classification[]" data-child_value="<?php echo $_smarty_tpl->tpl_vars['d_s_c_v']->value['id_skill_class'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['d_s_c_v']->value['id_skill_class'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_d_s_v']->value['s_c_i_id_skill_class'] == $_smarty_tpl->tpl_vars['d_s_c_v']->value['id_skill_class']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_s_c_v']->value['title'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_s_c_v'] = $__foreach_d_s_c_v_38_saved_local_item;
}
}
if ($__foreach_d_s_c_v_38_saved_item) {
$_smarty_tpl->tpl_vars['d_s_c_v'] = $__foreach_d_s_c_v_38_saved_item;
}
if ($__foreach_d_s_c_v_38_saved_key) {
$_smarty_tpl->tpl_vars['d_s_c_k'] = $__foreach_d_s_c_v_38_saved_key;
}
?>
                    </select>
                    <span class="input-group-addon">分類:</span>
                    <select name="id_skill_classification[]" class="form-control skill_classification">
                        <?php
$_from = $_smarty_tpl->tpl_vars['datebase_skill_classification']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_c_i_v_39_saved_item = isset($_smarty_tpl->tpl_vars['d_s_c_i_v']) ? $_smarty_tpl->tpl_vars['d_s_c_i_v'] : false;
$__foreach_d_s_c_i_v_39_saved_key = isset($_smarty_tpl->tpl_vars['d_s_c_i_k']) ? $_smarty_tpl->tpl_vars['d_s_c_i_k'] : false;
$_smarty_tpl->tpl_vars['d_s_c_i_v'] = new Smarty_Variable();
$__foreach_d_s_c_i_v_39_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_c_i_v_39_total) {
$_smarty_tpl->tpl_vars['d_s_c_i_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_c_i_k']->value => $_smarty_tpl->tpl_vars['d_s_c_i_v']->value) {
$__foreach_d_s_c_i_v_39_saved_local_item = $_smarty_tpl->tpl_vars['d_s_c_i_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_s_c_i_v']->value['id_skill_classification'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_d_s_v']->value['id_skill_classification'] == $_smarty_tpl->tpl_vars['d_s_c_i_v']->value['id_skill_classification']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_s_c_i_v']->value['title'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_s_c_i_v'] = $__foreach_d_s_c_i_v_39_saved_local_item;
}
}
if ($__foreach_d_s_c_i_v_39_saved_item) {
$_smarty_tpl->tpl_vars['d_s_c_i_v'] = $__foreach_d_s_c_i_v_39_saved_item;
}
if ($__foreach_d_s_c_i_v_39_saved_key) {
$_smarty_tpl->tpl_vars['d_s_c_i_k'] = $__foreach_d_s_c_i_v_39_saved_key;
}
?>
                    </select>
                    <span class="input-group-addon">技能:</span>
                    <select name="id_skill[]"  class="form-control skill">
                        <?php
$_from = $_smarty_tpl->tpl_vars['datebase_skill']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_v_40_saved_item = isset($_smarty_tpl->tpl_vars['d_s_v']) ? $_smarty_tpl->tpl_vars['d_s_v'] : false;
$__foreach_d_s_v_40_saved_key = isset($_smarty_tpl->tpl_vars['d_s_k']) ? $_smarty_tpl->tpl_vars['d_s_k'] : false;
$_smarty_tpl->tpl_vars['d_s_v'] = new Smarty_Variable();
$__foreach_d_s_v_40_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_v_40_total) {
$_smarty_tpl->tpl_vars['d_s_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_k']->value => $_smarty_tpl->tpl_vars['d_s_v']->value) {
$__foreach_d_s_v_40_saved_local_item = $_smarty_tpl->tpl_vars['d_s_v'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['id_skill'];?>
"  <?php if ($_smarty_tpl->tpl_vars['d_d_s_v']->value['id_skill'] == $_smarty_tpl->tpl_vars['d_s_v']->value['id_skill']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['title'];?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_40_saved_local_item;
}
}
if ($__foreach_d_s_v_40_saved_item) {
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_40_saved_item;
}
if ($__foreach_d_s_v_40_saved_key) {
$_smarty_tpl->tpl_vars['d_s_k'] = $__foreach_d_s_v_40_saved_key;
}
?>
                    </select>
                </div>
            </div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['d_d_s_v'] = $__foreach_d_d_s_v_37_saved_local_item;
}
}
if ($__foreach_d_d_s_v_37_saved_item) {
$_smarty_tpl->tpl_vars['d_d_s_v'] = $__foreach_d_d_s_v_37_saved_item;
}
if ($__foreach_d_d_s_v_37_saved_key) {
$_smarty_tpl->tpl_vars['d_d_s_k'] = $__foreach_d_d_s_v_37_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <div class="col-lg-12 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">專長/技能:</span>
                <span class="input-group-addon">類別:</span>
                <select name="id_skill_class[]" class="form-control skill_class">
                    <option >請選擇技能類別</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['datebase_skill_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_c_v_41_saved_item = isset($_smarty_tpl->tpl_vars['d_s_c_v']) ? $_smarty_tpl->tpl_vars['d_s_c_v'] : false;
$__foreach_d_s_c_v_41_saved_key = isset($_smarty_tpl->tpl_vars['d_s_c_k']) ? $_smarty_tpl->tpl_vars['d_s_c_k'] : false;
$_smarty_tpl->tpl_vars['d_s_c_v'] = new Smarty_Variable();
$__foreach_d_s_c_v_41_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_c_v_41_total) {
$_smarty_tpl->tpl_vars['d_s_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_c_k']->value => $_smarty_tpl->tpl_vars['d_s_c_v']->value) {
$__foreach_d_s_c_v_41_saved_local_item = $_smarty_tpl->tpl_vars['d_s_c_v'];
?>
                        <option data-child="id_skill_classification[]" data-child_value="<?php echo $_smarty_tpl->tpl_vars['d_s_c_v']->value['id_skill_class'];?>
"  value="<?php echo $_smarty_tpl->tpl_vars['d_s_c_v']->value['id_skill_class'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_s_c_v']->value['title'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['d_s_c_v'] = $__foreach_d_s_c_v_41_saved_local_item;
}
}
if ($__foreach_d_s_c_v_41_saved_item) {
$_smarty_tpl->tpl_vars['d_s_c_v'] = $__foreach_d_s_c_v_41_saved_item;
}
if ($__foreach_d_s_c_v_41_saved_key) {
$_smarty_tpl->tpl_vars['d_s_c_k'] = $__foreach_d_s_c_v_41_saved_key;
}
?>
                </select>
                <span class="input-group-addon">分類:</span>
                <select name="id_skill_classification[]"  class="form-control skill_classification">
                    <option >請選擇技能分類</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['datebase_skill_classification']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_c_i_v_42_saved_item = isset($_smarty_tpl->tpl_vars['d_s_c_i_v']) ? $_smarty_tpl->tpl_vars['d_s_c_i_v'] : false;
$__foreach_d_s_c_i_v_42_saved_key = isset($_smarty_tpl->tpl_vars['d_s_c_i_k']) ? $_smarty_tpl->tpl_vars['d_s_c_i_k'] : false;
$_smarty_tpl->tpl_vars['d_s_c_i_v'] = new Smarty_Variable();
$__foreach_d_s_c_i_v_42_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_c_i_v_42_total) {
$_smarty_tpl->tpl_vars['d_s_c_i_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_c_i_k']->value => $_smarty_tpl->tpl_vars['d_s_c_i_v']->value) {
$__foreach_d_s_c_i_v_42_saved_local_item = $_smarty_tpl->tpl_vars['d_s_c_i_v'];
?>
                        <option  value="<?php echo $_smarty_tpl->tpl_vars['d_s_c_i_v']->value['id_skill_classification'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_s_c_i_v']->value['title'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['d_s_c_i_v'] = $__foreach_d_s_c_i_v_42_saved_local_item;
}
}
if ($__foreach_d_s_c_i_v_42_saved_item) {
$_smarty_tpl->tpl_vars['d_s_c_i_v'] = $__foreach_d_s_c_i_v_42_saved_item;
}
if ($__foreach_d_s_c_i_v_42_saved_key) {
$_smarty_tpl->tpl_vars['d_s_c_i_k'] = $__foreach_d_s_c_i_v_42_saved_key;
}
?>
                </select>
                <span class="input-group-addon">技能:</span>
                <select name="id_skill_classification[]"  class="form-control skill">
                    <option >請選擇技能</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['datebase_skill']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_v_43_saved_item = isset($_smarty_tpl->tpl_vars['d_s_v']) ? $_smarty_tpl->tpl_vars['d_s_v'] : false;
$__foreach_d_s_v_43_saved_key = isset($_smarty_tpl->tpl_vars['d_s_k']) ? $_smarty_tpl->tpl_vars['d_s_k'] : false;
$_smarty_tpl->tpl_vars['d_s_v'] = new Smarty_Variable();
$__foreach_d_s_v_43_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_v_43_total) {
$_smarty_tpl->tpl_vars['d_s_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_k']->value => $_smarty_tpl->tpl_vars['d_s_v']->value) {
$__foreach_d_s_v_43_saved_local_item = $_smarty_tpl->tpl_vars['d_s_v'];
?>
                        <option  value="<?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['id_skill'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['title'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_43_saved_local_item;
}
}
if ($__foreach_d_s_v_43_saved_item) {
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_43_saved_item;
}
if ($__foreach_d_s_v_43_saved_key) {
$_smarty_tpl->tpl_vars['d_s_k'] = $__foreach_d_s_v_43_saved_key;
}
?>
                </select>
                <span class="input-group-addon" data-table="skill" data-type=""  onclick="add_personnel_data(this)" >+</span>
            </div>
        </div>
    </div>
</div>


<label>興趣</label>
<div class="form-group col-lg-12">
    <div class="form-group col-lg-12">
        <div class="col-lg-12 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">興趣:</span>
                <input class="form-control" type="text"  name="interest" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['interest'];?>
" maxlength="255">
            </div>
        </div>
    </div>
</div>

<label>專業訓練</label>
<div class="form-group col-lg-12">
    <?php if (!empty($_smarty_tpl->tpl_vars['database_data']->value['training_class'])) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value['training_class'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_t_v_44_saved_item = isset($_smarty_tpl->tpl_vars['d_t_v']) ? $_smarty_tpl->tpl_vars['d_t_v'] : false;
$__foreach_d_t_v_44_saved_key = isset($_smarty_tpl->tpl_vars['d_t_k']) ? $_smarty_tpl->tpl_vars['d_t_k'] : false;
$_smarty_tpl->tpl_vars['d_t_v'] = new Smarty_Variable();
$__foreach_d_t_v_44_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_t_v_44_total) {
$_smarty_tpl->tpl_vars['d_t_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_t_k']->value => $_smarty_tpl->tpl_vars['d_t_v']->value) {
$__foreach_d_t_v_44_saved_local_item = $_smarty_tpl->tpl_vars['d_t_v'];
?>
        <div class="form-group col-lg-12">
            <div class="col-lg-4 no_padding" >
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">課程名稱:</span>
                    <input class="form-control" type="text" name="training_class[]" value="<?php echo $_smarty_tpl->tpl_vars['d_t_v']->value;?>
" maxlength="64">
                </div>
            </div>
            <div class="col-lg-4 no_padding" >
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">技能:</span>
                    <input class="form-control" type="text" name="training_skill[]" value="<?php echo $_smarty_tpl->tpl_vars['database_data']->value['training_skill'][$_smarty_tpl->tpl_vars['d_t_k']->value];?>
" maxlength="64">
                </div>
            </div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['d_t_v'] = $__foreach_d_t_v_44_saved_local_item;
}
}
if ($__foreach_d_t_v_44_saved_item) {
$_smarty_tpl->tpl_vars['d_t_v'] = $__foreach_d_t_v_44_saved_item;
}
if ($__foreach_d_t_v_44_saved_key) {
$_smarty_tpl->tpl_vars['d_t_k'] = $__foreach_d_t_v_44_saved_key;
}
?>
    <?php }?>
    <div class="form-group col-lg-12">
        <div class="col-lg-4 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">課程名稱:</span>
                <input class="form-control" type="text" name="training_class[]" maxlength="64">
            </div>
        </div>
        <div class="col-lg-4 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">技能:</span>
                <input class="form-control" type="text" name="training_skill[]" maxlength="64">
            </div>
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="training"  data-type=""  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>生涯觀點</label>
<br>
<label>一.在您過去的工經驗中獲得甚麼?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_work" id="career_work"><?php echo $_smarty_tpl->tpl_vars['database_data']->value['career_work'];?>
</textarea>
</div>
<label>二.個人對於未來的生涯規劃?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_future" id="career_future"><?php echo $_smarty_tpl->tpl_vars['database_data']->value['career_future'];?>
</textarea>
</div>
<label>三.形容一下您所期待的工作環境及工作性質?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_expect" id="career_expect"><?php echo $_smarty_tpl->tpl_vars['database_data']->value['career_expect'];?>
</textarea>
</div>
<label>四.如果我們有緣成為工作夥伴,您認為和工作團隊間彼此的共創空間是甚麼?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_partner" id="career_partner"><?php echo $_smarty_tpl->tpl_vars['database_data']->value['career_partner'];?>
</textarea>
</div>
<?php }
}
