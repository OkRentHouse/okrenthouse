<?php
/* Smarty version 3.1.28, created on 2021-03-25 02:12:42
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/controllers/LifeGoProduct/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_605b811a6dcfe7_94784939',
  'file_dependency' => 
  array (
    '3c85f83ce9837edb1057b903f0f29e45d6d227e2' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/controllers/LifeGoProduct/content.tpl',
      1 => 1614072454,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605b811a6dcfe7_94784939 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./life_go_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<div class="t_container main">
    <div class="row">
        <?php if ($_smarty_tpl->tpl_vars['lifego_left']->value) {?>
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./LifeGo_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

        <?php }?>

        <div class="t_col_10 right">
            <div class="main__top">
                <div class="main__top-left">
                    <i class="far fa-heart"></i>
                    <div class="main__top-left-img">
                        <div class="main__top-left-img-inner"></div> 
                    </div>
                    <div class="main__top-left-nav">
                        <div class="main__top-left-nav-img"></div>
                        <div class="main__top-left-nav-img"></div>
                        <div class="main__top-left-nav-img"></div>
                        <div class="main__top-left-nav-img"></div>
                    </div>
                </div>
                <div class="main__top-right">

                    <div class="main__top-right-inner">
                        <div class="main__title">健康御守 除菌消味錠 <span>一盒10錠</span></div>
                        <div class="main__slogan">
                            <ul>
                                <li>消毒殺菌 防疫第一選擇 消毒 殺菌 除臭</li><li>消毒殺菌 防疫第一選擇 消毒 殺菌 除臭</li><li>消毒殺菌 防疫第一選擇 消毒 殺菌 除臭</li>
                            </ul>
                        </div>


                        <div class="detail-box">
                            <div class="detail-box-left">
                                <div>滿5送1</div>
                                <div>紅利點數<span class="accent">5</span>點</div>
                            </div>

                        </div>

                        <div class="detail-order">
                            <div class="inp_txt"><input type="text" value="0"><i class="fas fa-plus"></i><i class="fas fa-minus"></i></div>
                            <div class="btn">放入購物車</div>

                        </div>
                        <div class="price">
                            <div class="price__origin">
                                <div class="price__origin-label">原價</div>
                                <div class="price__origin-price">2,100</div>
                                <div class="price__origin-unit">元</div>
                            </div>
                            <div class="price__special">
                                <div class="price__special-label">好康價</div>
                                <div class="price__special-price">1,350</div>
                                <div class="price__special-unit">元</div>
                            </div>
                        </div>
                        <div class="detail-box">
                            <div class="detail-box-left">
                                <div>信用卡．貨到付款．LINE Pay．ATM無卡分期．超取付款</div>
                            </div>

                        </div>

                        <div class="choose-way">
                            <div class="choose-way__single">
                                <div class="choose-way__single-icon icon-down"><i class="fas fa-truck"></i></div>
                                <div class="choose-way__single-text">宅配</div>
                            </div>
                            <div class="choose-way__single">
                                <div class="choose-way__single-icon icon-down"><i class="fas fa-archive"></i></div>
                                <div class="choose-way__single-text">郵寄</div>
                            </div>
                            <div class="choose-way__single">
                                <div class="choose-way__single-icon icon-down"><i class="fas fa-store"></i></div>
                                <div class="choose-way__single-text">超商取貨</div>
                            </div>
                        </div>
                    </div>

                    <div class="main__top-right-right-inner">
                        <div>
                            <div class="star"><span class="icon"><i class="far fa-star"></i></span>5.0</div>
                            <div class="buy"><span class="icon"><i class="fas fa-shopping-bag"></i></span>36</div>
                        </div>
                    </div>
                </div><!-- <div class="main__top-right"> -->
            </div>

            <div class="info-box">
                <div class="info-box__tab">
                    <div class="info-box__tab-left">商品資訊</div>
                    <div class="info-box__tab-right">留言問答</div>
                </div>
                <div class="info-box-content">
                    <div class="info-box-content-question">
                        <div class="info-box-content-question-left -name">
                            <div class="info-box-content-question-name">小凱</div>
                            <div class="info-box-content-question-date">2020/5/7</div>
                        </div>
                        <div class="info-box-content-question-right">
                            請問殺菌效果好嗎
                        </div>
                    </div>
                    <div class="info-box-content-answer">
                        <div class="info-box-content-answer-left">
                            您好~經實證證實滅菌率可達99.9%喔
                        </div>
                        <div class="info-box-content-answer-right -name">
                            <div class="info-box-content-answer-name">樂購客服</div>
                            <div class="info-box-content-answer-date">2020/5/8</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
























































































































































<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./LifeGo_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
