<?php
/* Smarty version 3.1.28, created on 2021-05-07 09:18:09
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/landlord.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60949551ddf236_89817036',
  'file_dependency' => 
  array (
    'fedf440c77d89afd709e6a87eb7b7fbfab92bd5b' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/landlord.tpl',
      1 => 1620350284,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60949551ddf236_89817036 ($_smarty_tpl) {
if (empty($_smarty_tpl->tpl_vars['database_data']->value)) {?>
    <div class="panel panel-default member_landlord">
        <div class="panel-heading">房東
        </div>
        <div class="panel-body">
            <div class="form_data">
                <input type="hidden" name="id_member_landlord[]"  class="" value="">
                <input type="hidden" name="member_landlord_id_member[]"  class="" value="">

                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_name">房東姓名</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_name[]"  value="" maxlength="20">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_user">手機</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_user[]" onKeyUp="value=value.replace(/[^\d]/g,'')"  value=""  maxlength="20">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_en_name">英文名字</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_en_name[]"  value="" maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_tel">備用手機</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_tel[]"  onKeyUp="value=value.replace(/[^\d]/g,'')" value=""  maxlength="20">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_appellation">稱謂</label>
                    <div class="form-group col-lg-6">
                        <select class="form-control" name="member_landlord_appellation[]" >
                            <option value="">請選擇稱謂</option>
                            <option value="0">先生</option>
                            <option value="1">小姐</option>
                            <option value="2">太太</option>
                            <option value="3">媽媽</option>
                            <option value="4">伯伯</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_id_source">來源</label>
                    <div class="form-group col-lg-6">
                        <select class="form-control" name="member_landlord_id_source[]" >
                            <option value="">請選擇來源</option>
                            <?php
$_from = $_smarty_tpl->tpl_vars['database_source']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_v_0_saved_item = isset($_smarty_tpl->tpl_vars['d_s_v']) ? $_smarty_tpl->tpl_vars['d_s_v'] : false;
$__foreach_d_s_v_0_saved_key = isset($_smarty_tpl->tpl_vars['d_s_k']) ? $_smarty_tpl->tpl_vars['d_s_k'] : false;
$_smarty_tpl->tpl_vars['d_s_v'] = new Smarty_Variable();
$__foreach_d_s_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_v_0_total) {
$_smarty_tpl->tpl_vars['d_s_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_k']->value => $_smarty_tpl->tpl_vars['d_s_v']->value) {
$__foreach_d_s_v_0_saved_local_item = $_smarty_tpl->tpl_vars['d_s_v'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['id_source'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['source'];?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_0_saved_local_item;
}
}
if ($__foreach_d_s_v_0_saved_item) {
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_0_saved_item;
}
if ($__foreach_d_s_v_0_saved_key) {
$_smarty_tpl->tpl_vars['d_s_k'] = $__foreach_d_s_v_0_saved_key;
}
?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_birthday">出生年月日</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="date" name="member_landlord_birthday[]"  value="">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_identity">ID</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_identity[]" value=""  maxlength="16">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="black_add_land" style="color:red">是否黑名單</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="checkbox" name="black_add_land"  value="1">
                        </div>
                </div>
                <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="recodeTime" style="color:red">紀錄時間</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="date" name="recodeTime"  value="">
                        </div>
                </div>
                <div class="form-group col-lg-6" style="margin: auto -23px;">
                    <label class="control-label col-lg-6" for="blackreason" style="color:red">紀錄原因</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="blackreason" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['blackreason'];?>
"  maxlength="64">
                    </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="create_people" style="color:red">紀錄人</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="create_people"  value="<?php echo $_SESSION['name'];?>
" readonly>
                        </div>
                    </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_landlord_details" style="color:red">房東備註</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_landlord_details[]" value=""  maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_line_id">Line ID</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_line_id[]" value=""  maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-12"  style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_email">email</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_email[]" value=""  maxlength="100">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_local_tel_1">市話1</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_local_tel_1[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_local_tel_2">市話2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_local_tel_2[]" value=""  maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_id_address_domicile">戶籍地址</label>
                    <input type="hidden" name="member_landlord_id_address_domicile[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input id="member_landlord_id_address_domicile_postal" type="text" name="member_landlord_id_address_domicile_postal[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="member_landlord_id_address_domicile_id_county" name="member_landlord_id_address_domicile_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_1_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_1_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_1_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_1_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_1_saved_local_item;
}
}
if ($__foreach_d_c_v_1_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_1_saved_item;
}
if ($__foreach_d_c_v_1_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_1_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="member_landlord_id_address_domicile_id_city" name="member_landlord_id_address_domicile_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_2_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_2_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_2_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_2_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_2_saved_local_item;
}
}
if ($__foreach_d_c_v_2_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_2_saved_item;
}
if ($__foreach_d_c_v_2_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_2_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   id="member_landlord_id_address_domicile_village" name="member_landlord_id_address_domicile_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" id="member_landlord_id_address_domicile_neighbor" name="member_landlord_id_address_domicile_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   id="member_landlord_id_address_domicile_road" name="member_landlord_id_address_domicile_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" id="member_landlord_id_address_domicile_segment" name="member_landlord_id_address_domicile_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" id="member_landlord_id_address_domicile_lane" name="member_landlord_id_address_domicile_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" id="member_landlord_id_address_domicile_alley" name="member_landlord_id_address_domicile_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" id="member_landlord_id_address_domicile_no" name="member_landlord_id_address_domicile_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_domicile_no_her" name="member_landlord_id_address_domicile_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="member_landlord_id_address_domicile_floor" name="member_landlord_id_address_domicile_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input id="member_landlord_id_address_domicile_floor_her" type="text" name="member_landlord_id_address_domicile_floor_her[]"  class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="member_landlord_id_address_domicile_address_room" name="member_landlord_id_address_domicile_address_room[]"  class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" id="member_landlord_id_address_domicile_address" name="member_landlord_id_address_domicile_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_id_address_contact">連絡地址</label>
                    <input type="hidden" name="member_landlord_id_address_contact[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text" id="member_landlord_id_address_contact_postal" name="member_landlord_id_address_contact_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="member_landlord_id_address_contact_id_county" name="member_landlord_id_address_contact_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_3_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_3_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_3_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_3_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_3_saved_local_item;
}
}
if ($__foreach_d_c_v_3_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_3_saved_item;
}
if ($__foreach_d_c_v_3_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_3_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="member_landlord_id_address_contact_id_city" name="member_landlord_id_address_contact_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_4_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_4_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_4_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_4_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_4_saved_local_item;
}
}
if ($__foreach_d_c_v_4_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_4_saved_item;
}
if ($__foreach_d_c_v_4_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_4_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   id="member_landlord_id_address_contact_village" name="member_landlord_id_address_contact_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" id="member_landlord_id_address_contact_neighbor" name="member_landlord_id_address_contact_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   id="member_landlord_id_address_contact_road" name="member_landlord_id_address_contact_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" id="member_landlord_id_address_contact_segment" name="member_landlord_id_address_contact_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" id="member_landlord_id_address_contact_lane" name="member_landlord_id_address_contact_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" id="member_landlord_id_address_contact_alley" name="member_landlord_id_address_contact_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" id="member_landlord_id_address_contact_no" name="member_landlord_id_address_contact_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_contact_no_her" name="member_landlord_id_address_contact_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="member_landlord_id_address_contact_floor" name="member_landlord_id_address_contact_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_contact_floor_her" name="member_landlord_id_address_contact_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="" name="member_landlord_id_address_contact_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" id="member_landlord_id_address_contact_address" name="member_landlord_id_address_contact[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_id_address_house">住家地址</label>
                    <input type="hidden" name="member_landlord_id_address_house[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text" id="member_landlord_id_address_house_postal" name="member_landlord_id_address_house_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="member_landlord_id_address_house_id_county" name="member_landlord_id_address_house_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_5_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_5_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_5_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_5_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_5_saved_local_item;
}
}
if ($__foreach_d_c_v_5_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_5_saved_item;
}
if ($__foreach_d_c_v_5_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_5_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="member_landlord_id_address_house_id_city" name="member_landlord_id_address_house_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_6_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_6_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_6_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_6_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_6_saved_local_item;
}
}
if ($__foreach_d_c_v_6_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_6_saved_item;
}
if ($__foreach_d_c_v_6_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_6_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   id="member_landlord_id_address_house_village" name="member_landlord_id_address_house_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" id="member_landlord_id_address_house_neighbor" name="member_landlord_id_address_house_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   id="member_landlord_id_address_house_road" name="member_landlord_id_address_house_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" id="member_landlord_id_address_house_segment" name="member_landlord_id_address_house_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" id="member_landlord_id_address_house_lane" name="member_landlord_id_address_house_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" id="member_landlord_id_address_house_alley" name="member_landlord_id_address_house_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" id="member_landlord_id_address_house_no" name="member_landlord_id_address_house_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_house_no_her" name="member_landlord_id_address_house_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="member_landlord_id_address_house_floor" name="member_landlord_id_address_house_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_house_floor_her" name="member_landlord_id_address_house_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="member_landlord_id_address_house_address_room" name="member_landlord_id_address_house_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" id="member_landlord_id_address_house_address" name="member_landlord_id_address_house_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_id_address_company">公司地址</label>
                    <input type="hidden" name="member_landlord_id_address_company[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text" id="" name="member_landlord_id_address_company_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="" name="member_landlord_id_address_company_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_7_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_7_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_7_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_7_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_7_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_7_saved_local_item;
}
}
if ($__foreach_d_c_v_7_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_7_saved_item;
}
if ($__foreach_d_c_v_7_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_7_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select id="" name="member_landlord_id_address_company_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_8_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_8_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_8_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_8_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_8_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_8_saved_local_item;
}
}
if ($__foreach_d_c_v_8_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_8_saved_item;
}
if ($__foreach_d_c_v_8_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_8_saved_key;
}
?>
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   id="member_landlord_id_address_company_village" name="member_landlord_id_address_company_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" id="member_landlord_id_address_company_neighbor" name="member_landlord_id_address_company_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   id="member_landlord_id_address_company_road" name="member_landlord_id_address_company_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" id="member_landlord_id_address_company_segment" name="member_landlord_id_address_company_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" id="member_landlord_id_address_company_lane" name="member_landlord_id_address_company_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" id="member_landlord_id_address_company_alley" name="member_landlord_id_address_company_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" id="member_landlord_id_address_company_no" name="member_landlord_id_address_company_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_company_no_her" name="member_landlord_id_address_company_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="member_landlord_id_address_company_floor" name="member_landlord_id_address_company_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_company_floor_her" name="member_landlord_id_address_company_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" id="member_landlord_id_address_company_address_room" name="member_landlord_id_address_company_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" id="member_landlord_id_address_company_address" name="member_landlord_id_address_company_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_company">公司名/戶名</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_company[]"  value="" maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_job">公司職稱</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_job[]"  value=""  maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_bank_account">銀行帳號</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_bank_account[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_bank_code">銀行代碼</label>
                    <div class="form-group col-lg-3">
                        <input class="form-control" type="text" name="member_landlord_bank_code[]"  value=""  maxlength="4">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_bank">銀行</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_bank[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_branch">分行</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_branch[]"  value=""  maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_bank_account_2">銀行帳號2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_bank_account_2[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_bank_code_2">銀行代碼2</label>
                    <div class="form-group col-lg-3">
                        <input class="form-control" type="text" name="member_landlord_bank_code_2[]"  value=""  maxlength="4">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_bank_2">銀行2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_bank_2[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_branch_2">分行2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_branch_2[]"  value=""  maxlength="32">
                    </div>
                </div>
                
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="member_landlord_reserve_price">底價</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="number" name="member_landlord_reserve_price[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="member_landlord_buy_price">入手價</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="number" name="member_landlord_buy_price[]"  value=""  maxlength="4">
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_ag">AG</label>
                    <div class="col-lg-3 no_padding">
                        <div class="input-group fixed-width-lg">
                            <input class="form-control" type="text" name="member_landlord_ag_0[]"  value=""  maxlength="32">
                            <span class="input-group-addon" data-name="member_landlord_ag" data-count="0" onclick="add_field_landlord(this)">+</span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_ag">下架記錄</label>
                    <div class="col-lg-8 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">下架時間</span>
                            <input class="form-control" type="date" name="member_landlord_active_off_date_0[]"  value="">
                            <span class="input-group-addon">原因</span>
                            <input class="form-control" type="text" name="member_landlord_active_off_reason_0[]"  value=""  maxlength="64">
                            <span class="input-group-addon">人員</span>
                            <input class="form-control" type="text" name="member_landlord_active_off_admin_0[]" value="" maxlength="32">
                            <span class="input-group-addon" data-name="active_off" data-count="0" onclick="add_field_landlord(this)" >+</span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_ag" style="color:red">是否更換新房東資料:</label>
                    <div class="col-lg-8 no_padding">
                        <div class="input-group fixed-width-lg">
                        <input type="radio" id="land_update" name="land_update" value="0" checked>不更新
                        <input type="radio" id="land_update" name="land_update" value="1"><span style="color:red">新增</span>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <?php
$_from = $_smarty_tpl->tpl_vars['database_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_9_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_9_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_9_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_9_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_9_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
        <div class="panel panel-default member_landlord">
            <div class="panel-heading">房東
            </div>
            <div class="panel-body">
                <div class="form_data">
                    <input type="hidden" name="id_member_landlord[]"  class="" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_member_landlord'];?>
">
                    <input type="hidden" name="member_landlord_id_member[]"  class="" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_member'];?>
">

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_name">房東姓名</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_name[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
" maxlength="20">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_user">手機</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_user[]" onKeyUp="value=value.replace(/[^\d]/g,'')"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['user'];?>
"  maxlength="20">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_en_name">英文名字</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_en_name[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['en_name'];?>
" maxlength="64">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_tel">備用手機</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_tel[]" onKeyUp="value=value.replace(/[^\d]/g,'')"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['tel'];?>
"  maxlength="20">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_appellation">稱謂</label>
                        <div class="form-group col-lg-6">
                            <select class="form-control" name="member_landlord_appellation[]">
                                <option value="">請選擇稱謂</option>
                                <option value="0" <?php if ($_smarty_tpl->tpl_vars['v']->value['appellation'] == 0) {?> selected <?php }?>>先生</option>
                                <option value="1" <?php if ($_smarty_tpl->tpl_vars['v']->value['appellation'] == 1) {?> selected <?php }?>>小姐</option>
                                <option value="2" <?php if ($_smarty_tpl->tpl_vars['v']->value['appellation'] == 2) {?> selected <?php }?>>太太</option>
                                <option value="3" <?php if ($_smarty_tpl->tpl_vars['v']->value['appellation'] == 3) {?> selected <?php }?>>媽媽</option>
                                <option value="4" <?php if ($_smarty_tpl->tpl_vars['v']->value['appellation'] == 4) {?> selected <?php }?>>伯伯</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_id_source">來源</label>
                        <div class="form-group col-lg-6">
                            <select class="form-control" name="member_landlord_id_source[]" >
                                <option value="">請選擇來源</option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['database_source']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_v_10_saved_item = isset($_smarty_tpl->tpl_vars['d_s_v']) ? $_smarty_tpl->tpl_vars['d_s_v'] : false;
$__foreach_d_s_v_10_saved_key = isset($_smarty_tpl->tpl_vars['d_s_k']) ? $_smarty_tpl->tpl_vars['d_s_k'] : false;
$_smarty_tpl->tpl_vars['d_s_v'] = new Smarty_Variable();
$__foreach_d_s_v_10_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_v_10_total) {
$_smarty_tpl->tpl_vars['d_s_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_k']->value => $_smarty_tpl->tpl_vars['d_s_v']->value) {
$__foreach_d_s_v_10_saved_local_item = $_smarty_tpl->tpl_vars['d_s_v'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['id_source'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_source'] == $_smarty_tpl->tpl_vars['d_s_v']->value['id_source']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['source'];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_10_saved_local_item;
}
}
if ($__foreach_d_s_v_10_saved_item) {
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_10_saved_item;
}
if ($__foreach_d_s_v_10_saved_key) {
$_smarty_tpl->tpl_vars['d_s_k'] = $__foreach_d_s_v_10_saved_key;
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_birthday">出生年月日</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="date" name="member_landlord_birthday[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['birthday'];?>
">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_identity">ID</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_identity[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['identity'];?>
"  maxlength="16">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="black_add_land" style="color:red">是否黑名單</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="checkbox" name="black_add_land"  value="1" <?php if ($_smarty_tpl->tpl_vars['black_ckk']->value == "1") {?> checked <?php }?> >
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="recodeTime" style="color:red">紀錄時間</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="date" name="recodeTime"  value="<?php echo $_smarty_tpl->tpl_vars['black_ckk_rt']->value;?>
">
                        </div>
                    </div>
                    <div class="form-group col-lg-6" style="margin: auto -23px;">
                    <label class="control-label col-lg-6" for="blackreason" style="color:red">紀錄原因</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="blackreason" value="<?php echo $_smarty_tpl->tpl_vars['black_ckk_reason']->value;?>
"  maxlength="64">
                    </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="create_people" style="color:red">紀錄人</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="create_people"  value="<?php echo $_SESSION['name'];?>
" readonly>
                        </div>
                    </div>

                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_landlord_details" style="color:red">房東備註</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="member_landlord_landlord_details[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['landlord_details'];?>
"  maxlength="64">
                    </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_line_id">Line ID</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_line_id[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['line_id'];?>
"  maxlength="64">
                        </div>
                    </div>
                    <div class="form-group col-lg-12"  style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_email">email</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_email[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['email'];?>
"  maxlength="100">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_local_tel_1">市話1</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_local_tel_1[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['local_tel_1'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_local_tel_2">市話2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_local_tel_2[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['local_tel_2'];?>
"  maxlength="32">
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_id_address_domicile">戶籍地址</label>
                        <input type="hidden" name="member_landlord_id_address_domicile[]" class="" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile'];?>
">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_domicile_postal" name="member_landlord_id_address_domicile_postal[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_postal'];?>
" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_domicile_id_county" name="member_landlord_id_address_domicile_id_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_11_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_11_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_11_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_11_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_11_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_address_domicile_id_county'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_county']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_11_saved_local_item;
}
}
if ($__foreach_d_c_v_11_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_11_saved_item;
}
if ($__foreach_d_c_v_11_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_11_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_domicile_id_city" name="member_landlord_id_address_domicile_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_12_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_12_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_12_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_12_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_12_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_address_domicile_id_city'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_city']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_12_saved_local_item;
}
}
if ($__foreach_d_c_v_12_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_12_saved_item;
}
if ($__foreach_d_c_v_12_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_12_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   id="member_landlord_id_address_domicile_village" name="member_landlord_id_address_domicile_village[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" id="member_landlord_id_address_domicile_neighbor" name="member_landlord_id_address_domicile_neighbor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   id="member_landlord_id_address_domicile_road" name="member_landlord_id_address_domicile_road[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_road'];?>
" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_domicile_segment" name="member_landlord_id_address_domicile_segment[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" id="member_landlord_id_address_domicile_lane" name="member_landlord_id_address_domicile_lane[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" id="member_landlord_id_address_domicile_alley" name="member_landlord_id_address_domicile_alley[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" id="member_landlord_id_address_domicile_no" name="member_landlord_id_address_domicile_no[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_no'];?>
" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_domicile_no_her" name="member_landlord_id_address_domicile_no_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_domicile_floor" name="member_landlord_id_address_domicile_floor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_domicile_floor_her" name="member_landlord_id_address_domicile_floor_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_domicile_address_room" name="member_landlord_id_address_domicile_address_room[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" id="member_landlord_id_address_domicile_address" name="member_landlord_id_address_domicile_address[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_domicile_address'];?>
" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_id_address_contact">連絡地址</label>
                        <input type="hidden" name="member_landlord_id_address_contact[]" class="" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact'];?>
">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_contact_postal" name="member_landlord_id_address_contact_postal[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_postal'];?>
" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_contact_id_county" name="member_landlord_id_address_contact_id_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_13_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_13_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_13_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_13_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_13_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_address_contact_id_county'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_county']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_13_saved_local_item;
}
}
if ($__foreach_d_c_v_13_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_13_saved_item;
}
if ($__foreach_d_c_v_13_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_13_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_contact_id_city" name="member_landlord_id_address_contact_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_14_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_14_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_14_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_14_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_14_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_address_contact_id_city'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_city']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_14_saved_local_item;
}
}
if ($__foreach_d_c_v_14_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_14_saved_item;
}
if ($__foreach_d_c_v_14_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_14_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   id="member_landlord_id_address_contact_village" name="member_landlord_id_address_contact_village[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" id="member_landlord_id_address_contact_neighbor" name="member_landlord_id_address_contact_neighbor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   id="member_landlord_id_address_contact_road" name="member_landlord_id_address_contact_road[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_road'];?>
" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_contact_segment" name="member_landlord_id_address_contact_segment[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" id="member_landlord_id_address_contact_lane" name="member_landlord_id_address_contact_lane[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" id="member_landlord_id_address_contact_alley" name="member_landlord_id_address_contact_alley[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" id="member_landlord_id_address_contact_no" name="member_landlord_id_address_contact_no[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_no'];?>
" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_contact_no_her" name="member_landlord_id_address_contact_no_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_contact_floor" name="member_landlord_id_address_contact_floor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_contact_floor_her" name="member_landlord_id_address_contact_floor_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_contact_address_room" name="member_landlord_id_address_contact_address_room[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" id="member_landlord_id_address_contact_address" name="member_landlord_id_address_contact_address[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_contact_address'];?>
" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_id_address_house">住家地址</label>
                        <input type="hidden" name="member_landlord_id_address_house[]" class="" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house'];?>
">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_house_postal" name="member_landlord_id_address_house_postal[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_postal'];?>
" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_house_county" name="member_landlord_id_address_house_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_15_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_15_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_15_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_15_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_15_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['iid_address_house_id_county'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_county']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_15_saved_local_item;
}
}
if ($__foreach_d_c_v_15_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_15_saved_item;
}
if ($__foreach_d_c_v_15_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_15_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_house_id_city" name="member_landlord_id_address_house_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_16_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_16_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_16_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_16_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_16_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_address_house_id_city'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_city']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_16_saved_local_item;
}
}
if ($__foreach_d_c_v_16_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_16_saved_item;
}
if ($__foreach_d_c_v_16_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_16_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   id="member_landlord_id_address_house_village" name="member_landlord_id_address_house_village[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" id="member_landlord_id_address_house_neighbor" name="member_landlord_id_address_house_neighbor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   id="member_landlord_id_address_house_road" name="member_landlord_id_address_house_contact_road[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_road'];?>
" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_house_segment" name="member_landlord_id_address_house_segment[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" id="member_landlord_id_address_house_lane" name="member_landlord_id_address_house_lane[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" id="member_landlord_id_address_house_alley" name="member_landlord_id_address_house_alley[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" id="member_landlord_id_address_house_no" name="member_landlord_id_address_house_no[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_no'];?>
" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_house_no_her" name="member_landlord_id_address_house_no_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_house_floor" name="member_landlord_id_address_house_floor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_house_floor_her" name="member_landlord_id_address_house_floor_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_house_address_room" name="member_landlord_id_address_house_address_room[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" id="member_landlord_id_address_house_address" name="member_landlord_id_address_house_address[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_house_address'];?>
" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_id_address_company">公司地址</label>
                        <input type="hidden" name="member_landlord_id_address_company[]" class="" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company'];?>
">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_company_postal" name="member_landlord_id_address_company_postal[]"  class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_postal'];?>
" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_company_county" name="member_landlord_id_address_company_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_17_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_17_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_17_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_17_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_17_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_county'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['iid_address_company_id_county'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_county']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['county_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_17_saved_local_item;
}
}
if ($__foreach_d_c_v_17_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_17_saved_item;
}
if ($__foreach_d_c_v_17_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_17_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select id="member_landlord_id_address_company_id_city" name="member_landlord_id_address_company_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['database_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_c_v_18_saved_item = isset($_smarty_tpl->tpl_vars['d_c_v']) ? $_smarty_tpl->tpl_vars['d_c_v'] : false;
$__foreach_d_c_v_18_saved_key = isset($_smarty_tpl->tpl_vars['d_c_k']) ? $_smarty_tpl->tpl_vars['d_c_k'] : false;
$_smarty_tpl->tpl_vars['d_c_v'] = new Smarty_Variable();
$__foreach_d_c_v_18_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_c_v_18_total) {
$_smarty_tpl->tpl_vars['d_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_c_k']->value => $_smarty_tpl->tpl_vars['d_c_v']->value) {
$__foreach_d_c_v_18_saved_local_item = $_smarty_tpl->tpl_vars['d_c_v'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['id_city'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['id_address_company_id_city'] == $_smarty_tpl->tpl_vars['d_c_v']->value['id_city']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['d_c_v']->value['city_name'];?>
</option>
                                    <?php
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_18_saved_local_item;
}
}
if ($__foreach_d_c_v_18_saved_item) {
$_smarty_tpl->tpl_vars['d_c_v'] = $__foreach_d_c_v_18_saved_item;
}
if ($__foreach_d_c_v_18_saved_key) {
$_smarty_tpl->tpl_vars['d_c_k'] = $__foreach_d_c_v_18_saved_key;
}
?>
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   id="member_landlord_id_address_company_village" name="member_landlord_id_address_company_village[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_village'];?>
" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" id="member_landlord_id_address_company_neighbor" name="member_landlord_id_address_company_neighbor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_neighbor'];?>
" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   id="member_landlord_id_address_company_road" name="member_landlord_id_address_company_contact_road[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_road'];?>
" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" id="member_landlord_id_address_company_segment" name="member_landlord_id_address_company_segment[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_segment'];?>
" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" id="member_landlord_id_address_company_lane" name="member_landlord_id_address_company_lane[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_lane'];?>
" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" id="member_landlord_id_address_company_alley" name="member_landlord_id_address_company_alley[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_alley'];?>
" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" id="member_landlord_id_address_company_no" name="member_landlord_id_address_company_no[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_no'];?>
" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_company_no_her" name="member_landlord_id_address_company_no_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_no_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_company_floor" name="member_landlord_id_address_company_floor[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_floor'];?>
" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" id="member_landlord_id_address_company_floor_her" name="member_landlord_id_address_company_floor_her[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_floor_her'];?>
" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" id="member_landlord_id_address_company_address_room" name="member_landlord_id_address_company_address_room[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_address_room'];?>
" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" id="member_landlord_id_address_company_address" name="member_landlord_id_address_company_address[]" class="form-control " value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_address_company_address'];?>
" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_company">公司名/戶名</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_company[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['company'];?>
" maxlength="64">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_job">公司職稱</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_job[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['job'];?>
"  maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_bank_account">銀行帳號</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_bank_account[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bank_account'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_bank_code">銀行代碼</label>
                        <div class="form-group col-lg-3">
                            <input class="form-control" type="text" name="member_landlord_bank_code[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bank_code'];?>
"  maxlength="4">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_bank">銀行</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_bank[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bank'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_branch">分行</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_branch[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['branch'];?>
"  maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_bank_account_2">銀行帳號2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_bank_account_2[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bank_account_2'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_bank_code_2">銀行代碼2</label>
                        <div class="form-group col-lg-3">
                            <input class="form-control" type="text" name="member_landlord_bank_code_2[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['landlord_bank_code_2'];?>
"  maxlength="4">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_bank_2">銀行2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_bank_2[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['bank_2'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_branch_2">分行2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="member_landlord_branch_2[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['branch_2'];?>
"  maxlength="32">
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="member_landlord_reserve_price">底價</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="number" name="member_landlord_reserve_price[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['reserve_price'];?>
" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="member_landlord_buy_price">入手價</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="number" name="member_landlord_buy_price[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['buy_price'];?>
"  maxlength="11">
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_ag">AG</label>
                            <div class="col-lg-3 no_padding">
                                <?php if (empty($_smarty_tpl->tpl_vars['v']->value['ag'])) {?>
                                <div class="input-group fixed-width-lg">
                                    <input class="form-control" type="text" name="member_landlord_ag_0[]"  value=""  maxlength="32">
                                    <span class="input-group-addon" data-name="member_landlord_ag" data-count="0" onclick="add_field_landlord(this)">+</span>
                                </div>
                                <?php } else { ?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['v']->value['ag'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ag_v_19_saved_item = isset($_smarty_tpl->tpl_vars['ag_v']) ? $_smarty_tpl->tpl_vars['ag_v'] : false;
$__foreach_ag_v_19_saved_key = isset($_smarty_tpl->tpl_vars['ag_k']) ? $_smarty_tpl->tpl_vars['ag_k'] : false;
$_smarty_tpl->tpl_vars['ag_v'] = new Smarty_Variable();
$__foreach_ag_v_19_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_ag_v_19_total) {
$_smarty_tpl->tpl_vars['ag_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['ag_k']->value => $_smarty_tpl->tpl_vars['ag_v']->value) {
$__foreach_ag_v_19_saved_local_item = $_smarty_tpl->tpl_vars['ag_v'];
?>
                                    <div class="input-group fixed-width-lg">
                                        <input class="form-control" type="text" name="member_landlord_ag_0[]"  value="<?php echo $_smarty_tpl->tpl_vars['ag_v']->value;?>
"  maxlength="32">
                                        <?php if ($_smarty_tpl->tpl_vars['ag_k']->value == 0) {?>
                                        <span class="input-group-addon" data-name="member_landlord_ag" data-count="<?php echo $_smarty_tpl->tpl_vars['ag_k']->value;?>
" onclick="add_field_landlord(this)">+</span>
                                        <?php } else { ?>
                                            <span class="input-group-addon" data-name="member_landlord_ag" data-count="<?php echo $_smarty_tpl->tpl_vars['ag_k']->value;?>
" onclick="delete_field_landlord(this)">-</span>
                                        <?php }?>
                                    </div>
                                    <?php
$_smarty_tpl->tpl_vars['ag_v'] = $__foreach_ag_v_19_saved_local_item;
}
}
if ($__foreach_ag_v_19_saved_item) {
$_smarty_tpl->tpl_vars['ag_v'] = $__foreach_ag_v_19_saved_item;
}
if ($__foreach_ag_v_19_saved_key) {
$_smarty_tpl->tpl_vars['ag_k'] = $__foreach_ag_v_19_saved_key;
}
?>
                                <?php }?>
                            </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="member_landlord_ag">下架記錄</label>
                        <div class="col-lg-8 no_padding">
                            <?php if (empty($_smarty_tpl->tpl_vars['v']->value['active_off_date'])) {?>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">下架時間</span>
                                <input class="form-control" type="date" name="member_landlord_active_off_date_0[]"  value="">
                                <span class="input-group-addon">原因</span>
                                <input class="form-control" type="text" name="member_landlord_active_off_reason_0[]"  value=""  maxlength="64">
                                <span class="input-group-addon">人員</span>
                                <input class="form-control" type="text" name="member_landlord_active_off_admin_0[]" value="" maxlength="32">
                                <span class="input-group-addon" data-name="active_off" data-count="0" onclick="add_field_landlord(this)" >+</span>
                            </div>
                            <?php } else { ?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['v']->value['active_off_date'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_active_off_date_v_20_saved_item = isset($_smarty_tpl->tpl_vars['active_off_date_v']) ? $_smarty_tpl->tpl_vars['active_off_date_v'] : false;
$__foreach_active_off_date_v_20_saved_key = isset($_smarty_tpl->tpl_vars['active_off_date_k']) ? $_smarty_tpl->tpl_vars['active_off_date_k'] : false;
$_smarty_tpl->tpl_vars['active_off_date_v'] = new Smarty_Variable();
$__foreach_active_off_date_v_20_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_active_off_date_v_20_total) {
$_smarty_tpl->tpl_vars['active_off_date_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['active_off_date_k']->value => $_smarty_tpl->tpl_vars['active_off_date_v']->value) {
$__foreach_active_off_date_v_20_saved_local_item = $_smarty_tpl->tpl_vars['active_off_date_v'];
?>
                                    <div class="input-group fixed-width-lg">
                                        <span class="input-group-addon">下架時間</span>
                                        <input class="form-control" type="date" name="member_landlord_active_off_date_0[]"  value="<?php echo $_smarty_tpl->tpl_vars['active_off_date_v']->value;?>
">
                                        <span class="input-group-addon">原因</span>
                                        <input class="form-control" type="text" name="member_landlord_active_off_reason_0[]"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['active_off_reason'][$_smarty_tpl->tpl_vars['active_off_date_k']->value];?>
"  maxlength="64">
                                        <span class="input-group-addon">人員</span>
                                        <input class="form-control" type="text" name="member_landlord_active_off_admin_0[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['active_off_admin'][$_smarty_tpl->tpl_vars['active_off_date_k']->value];?>
" maxlength="32">
                                        <?php if ($_smarty_tpl->tpl_vars['active_off_date_k']->value == 0) {?>
                                            <span class="input-group-addon" data-name="active_off" data-count="<?php echo $_smarty_tpl->tpl_vars['active_off_date_k']->value;?>
" onclick="add_field_landlord(this)" >+</span>
                                            <?php } else { ?>
                                            <span class="input-group-addon" data-name="active_off" data-count="<?php echo $_smarty_tpl->tpl_vars['active_off_date_k']->value;?>
" onclick="delete_field_landlord(this)" >-</span>
                                            <?php }?>
                                    </div>
                                <?php
$_smarty_tpl->tpl_vars['active_off_date_v'] = $__foreach_active_off_date_v_20_saved_local_item;
}
}
if ($__foreach_active_off_date_v_20_saved_item) {
$_smarty_tpl->tpl_vars['active_off_date_v'] = $__foreach_active_off_date_v_20_saved_item;
}
if ($__foreach_active_off_date_v_20_saved_key) {
$_smarty_tpl->tpl_vars['active_off_date_k'] = $__foreach_active_off_date_v_20_saved_key;
}
?>
                            <?php }?>
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="member_landlord_ag" style="color:red">是否更換新房東資料:</label>
                    <div class="col-lg-8 no_padding">
                        <div class="input-group fixed-width-lg">
                        <input type="radio" id="land_update" name="land_update" value="0" checked>不更新
                        <input type="radio" id="land_update" name="land_update" value="1"><span style="color:red">新增</span>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_9_saved_local_item;
}
}
if ($__foreach_v_9_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_9_saved_item;
}
if ($__foreach_v_9_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_9_saved_key;
}
}?>












































































































































































































































































































































































































<?php }
}
