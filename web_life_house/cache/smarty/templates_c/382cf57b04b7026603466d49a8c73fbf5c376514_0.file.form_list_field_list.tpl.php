<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:35:23
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_list_field_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599a3bbc6cb9_92850775',
  'file_dependency' => 
  array (
    '382cf57b04b7026603466d49a8c73fbf5c376514' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_list_field_list.tpl',
      1 => 1592289280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60599a3bbc6cb9_92850775 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['fields']->value['field_display']) && !empty($_smarty_tpl->tpl_vars['fields']->value['field_display'])) {?>
<div class="field_list">
	<form action="" method="post" class="list" accept-charset="utf-8">
		<div class="text-center up"><i class="fas fa-angle-double-up"></i></div>
		<div class="title"><?php echo l(array('s'=>"顯示欄位"),$_smarty_tpl);?>
</div>
		<div class="field">
			<div class="checkbox"><label for="all_field_name_val"><input type="checkbox" name="all_field_name_val[]" id="all_field_name_val" value=""><?php echo l(array('s'=>"全選"),$_smarty_tpl);?>
</label></div>
			<?php
$_from = $_smarty_tpl->tpl_vars['fields']->value['all_field_list'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_val_0_saved_item = isset($_smarty_tpl->tpl_vars['val']) ? $_smarty_tpl->tpl_vars['val'] : false;
$__foreach_val_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['val'] = new Smarty_Variable();
$__foreach_val_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_val_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['val']->value) {
$__foreach_val_0_saved_local_item = $_smarty_tpl->tpl_vars['val'];
?>
				<div class="checkbox"><label for="field_name_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><input type="checkbox" name="field_name_val[]" id="field_name_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['val']->value['show'] == 1) {?>checked<?php }?>><?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['val']->value['title']);?>
</label><input type="hidden" name="field_name[]" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"></div>
			<?php
$_smarty_tpl->tpl_vars['val'] = $__foreach_val_0_saved_local_item;
}
}
if ($__foreach_val_0_saved_item) {
$_smarty_tpl->tpl_vars['val'] = $__foreach_val_0_saved_item;
}
if ($__foreach_val_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_val_0_saved_key;
}
?>
			<button type="submit" class="btn btn-default"><?php echo l(array('s'=>"送出"),$_smarty_tpl);?>
</button>
		</div>
		<input type="hidden" name="SetMyFieldList<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
">
	</form>
</div>
<?php }
}
}
