<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:35:11
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599a2f051e27_71498805',
  'file_dependency' => 
  array (
    'ce04916be35a7c19478558e71d2aba1b261fc9f9' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/header.tpl',
      1 => 1602645175,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60599a2f051e27_71498805 ($_smarty_tpl) {
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php if (!empty($_smarty_tpl->tpl_vars['t_color']->value)) {?><meta name="theme-color" content="<?php echo $_smarty_tpl->tpl_vars['t_color']->value;?>
"><?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['mn_color']->value)) {?><meta name="msapplication-navbutton-color" content="<?php echo $_smarty_tpl->tpl_vars['mn_color']->value;?>
"><?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['amwasb_style']->value)) {?><meta name="apple-mobile-web-app-status-bar-style" content="<?php echo $_smarty_tpl->tpl_vars['amwasb_style']->value;?>
"><?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['meta_viewport']->value)) {?><meta name="viewport" content="<?php echo $_smarty_tpl->tpl_vars['meta_viewport']->value;?>
"><?php }?>
	<title><?php if (isset($_smarty_tpl->tpl_vars['meta_title']->value)) {
echo $_smarty_tpl->tpl_vars['meta_title']->value;
echo $_smarty_tpl->tpl_vars['NAVIGATION_PIPE']->value;
}
echo $_smarty_tpl->tpl_vars['web_name']->value;?>
</title>
	<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
	<!--[if lt IE 9]>
	<?php echo '<script'; ?>
 src="/css/html5shiv.min.js"><?php echo '</script'; ?>
>
	<![endif]-->
	<?php echo $_smarty_tpl->tpl_vars['GoogleAnalytics']->value;?>

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./table_title.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

	<?php
$_from = $_smarty_tpl->tpl_vars['css_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_0_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_0_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css"/>
	<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_local_item;
}
}
if ($__foreach_css_uri_0_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_item;
}
if ($__foreach_css_uri_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_0_saved_key;
}
?>
	<?php if (!isset($_smarty_tpl->tpl_vars['display_header_javascript']->value) || $_smarty_tpl->tpl_vars['display_header_javascript']->value) {?>
		<?php echo '<script'; ?>
 type="text/javascript">
			var display  = '<?php echo $_smarty_tpl->tpl_vars['display']->value;?>
';
			var THEME_URL = '<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
';
			var ADMIN_THEME_URL = '<?php echo $_smarty_tpl->tpl_vars['ADMIN_THEME_URL']->value;?>
';
			var content_css = <?php echo $_smarty_tpl->tpl_vars['front_css_files']->value;?>
;
		<?php echo '</script'; ?>
>
		<?php echo smartyHook(array('h'=>'displayAdminJavaScript'),$_smarty_tpl);?>

	<?php
$_from = $_smarty_tpl->tpl_vars['js_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_1_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_1_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>
		<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>
	<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_local_item;
}
}
if ($__foreach_js_uri_1_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_item;
}
if ($__foreach_js_uri_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_1_saved_key;
}
?>
	<?php if (!$_smarty_tpl->tpl_vars['ENTER_SUBMIT']->value) {?>
		<?php echo '<script'; ?>
 type="text/javascript">
			$(document).keypress(function(e) {
				if(e.which == 13) {
					return false;
				}
			});
		<?php echo '</script'; ?>
>
	<?php }?>
	<?php if (count($_smarty_tpl->tpl_vars['_errors_name']->value) > 0) {?>
		<?php echo '<script'; ?>
 type="text/javascript">
			$(document).ready(function(e) {
				<?php
$_from = $_smarty_tpl->tpl_vars['_errors_name']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_err_name_2_saved_item = isset($_smarty_tpl->tpl_vars['err_name']) ? $_smarty_tpl->tpl_vars['err_name'] : false;
$_smarty_tpl->tpl_vars['err_name'] = new Smarty_Variable();
$__foreach_err_name_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_err_name_2_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['err_name']->value) {
$__foreach_err_name_2_saved_local_item = $_smarty_tpl->tpl_vars['err_name'];
?>
				<?php if (count($_smarty_tpl->tpl_vars['_errors_name_i']->value[$_smarty_tpl->tpl_vars['err_name']->value]) > 0) {?>
				<?php
$_from = $_smarty_tpl->tpl_vars['_errors_name_i']->value[$_smarty_tpl->tpl_vars['err_name']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ei_3_saved_item = isset($_smarty_tpl->tpl_vars['ei']) ? $_smarty_tpl->tpl_vars['ei'] : false;
$_smarty_tpl->tpl_vars['ei'] = new Smarty_Variable();
$__foreach_ei_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_ei_3_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['ei']->value) {
$__foreach_ei_3_saved_local_item = $_smarty_tpl->tpl_vars['ei'];
?>
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parents('.form-group').addClass('has-error');
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parent('td').addClass('has-error');
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"][type="checkbox"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parent('label').addClass('has-error');
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"][type="checkbox"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).focus(function(){
					$(this).parent('label').removeClass('has-error');
				});
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').focus(function(){
					$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parents('.form-group').removeClass('has-error');
					$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parent('td').removeClass('has-error');
				});
				<?php
$_smarty_tpl->tpl_vars['ei'] = $__foreach_ei_3_saved_local_item;
}
}
if ($__foreach_ei_3_saved_item) {
$_smarty_tpl->tpl_vars['ei'] = $__foreach_ei_3_saved_item;
}
?>
				<?php } else { ?>
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parents('.form-group').addClass('has-error');
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parent('td').addClass('has-error');
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"][type="checkbox"]').parent('label').addClass('has-error');
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').focus(function(){
					$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parents('.form-group').removeClass('has-error');
					$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parent('td').removeClass('has-error');
					$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"][type="checkbox"]').parent('label').removeClass('has-error');
				});
				<?php }?>
				<?php
$_smarty_tpl->tpl_vars['err_name'] = $__foreach_err_name_2_saved_local_item;
}
}
if ($__foreach_err_name_2_saved_item) {
$_smarty_tpl->tpl_vars['err_name'] = $__foreach_err_name_2_saved_item;
}
?>
			});
		<?php echo '</script'; ?>
>
	<?php }?>
		<?php echo '<script'; ?>
 type="text/javascript">
			$(document).ready(function(e) {
				$('#<?php if (isset($_smarty_tpl->tpl_vars['fields']->value['form']['id'])) {
echo $_smarty_tpl->tpl_vars['fields']->value['form']['id'];
} elseif (isset($_smarty_tpl->tpl_vars['fields']->value['form']['name'])) {
echo $_smarty_tpl->tpl_vars['fields']->value['form']['name'];
} else {
echo $_smarty_tpl->tpl_vars['table']->value;
}?>').validate({
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block'
				});
			});
		<?php echo '</script'; ?>
>
	<?php }?>
</head>
<?php if ($_smarty_tpl->tpl_vars['display_header']->value) {?>
<body class="<?php if ($_smarty_tpl->tpl_vars['menu_close']->value) {?>menu_close <?php echo $_smarty_tpl->tpl_vars['menu_close']->value;
}?>">
<?php echo smartyHook(array('h'=>'displayAdminBodyBefore'),$_smarty_tpl);?>

	<?php echo $_smarty_tpl->tpl_vars['minbar']->value;?>

	<div id="main">
		<?php echo $_smarty_tpl->tpl_vars['admin_menu']->value;?>

		<div id="content" class="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
">
			<?php if (isset($_smarty_tpl->tpl_vars['page_header_toolbar']->value) && $_smarty_tpl->tpl_vars['display_page_header_toolbar']->value) {
echo $_smarty_tpl->tpl_vars['page_header_toolbar']->value;
}?>
            <?php echo $_smarty_tpl->tpl_vars['msg_box']->value;?>

			<div class="content no-floa row">
				<?php } else { ?>
				<body>
					<?php if ($_smarty_tpl->tpl_vars['minbar_no']->value) {
} else { ?><div class="minbar col-lg-12"></div><?php }?>
					<div id="main">
						<div id="content" class="row <?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
">
							<?php echo $_smarty_tpl->tpl_vars['msg_box']->value;?>

<?php }
}
}
