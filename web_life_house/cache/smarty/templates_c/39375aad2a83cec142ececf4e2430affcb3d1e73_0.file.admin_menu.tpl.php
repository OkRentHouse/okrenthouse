<?php
/* Smarty version 3.1.28, created on 2021-03-24 15:44:17
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/admin_menu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_605aedd1d366c5_91176716',
  'file_dependency' => 
  array (
    '39375aad2a83cec142ececf4e2430affcb3d1e73' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/admin_menu.tpl',
      1 => 1616571845,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605aedd1d366c5_91176716 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_escape')) require_once '/opt/lampp/htdocs/life-house.com.tw/tools/smarty-3.1.28/libs/plugins/modifier.escape.php';
if (!$_smarty_tpl->tpl_vars['display_admin_menu']->value) {?>
<nav id="admin_menu">
	
	<ul>
		<div class="logo"></div>
		<?php
$_from = $_smarty_tpl->tpl_vars['tab_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tab_0_saved_item = isset($_smarty_tpl->tpl_vars['tab']) ? $_smarty_tpl->tpl_vars['tab'] : false;
$__foreach_tab_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable();
$__foreach_tab_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_tab_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['tab']->value) {
$__foreach_tab_0_saved_local_item = $_smarty_tpl->tpl_vars['tab'];
?>
			<?php if (!empty($_smarty_tpl->tpl_vars['k']->value)) {?>
			<li class="<?php echo $_smarty_tpl->tpl_vars['tab']->value['controller_name'];
if ($_smarty_tpl->tpl_vars['page']->value == $_smarty_tpl->tpl_vars['tab']->value['controller_name']) {?> active<?php }?>"><a<?php if (isset($_smarty_tpl->tpl_vars['tab']->value['list'])) {?> href="#" role="button"<?php } else { ?> href="/<?php echo $_smarty_tpl->tpl_vars['ADMIN_URL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['tab']->value['controller_name'];
echo $_smarty_tpl->tpl_vars['tab']->value['parameter'];?>
"href="/<?php echo $_smarty_tpl->tpl_vars['ADMIN_URL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['tab']->value['controller_name'];
echo $_smarty_tpl->tpl_vars['tab']->value['parameter'];?>
"<?php }?>><i class="<?php echo $_smarty_tpl->tpl_vars['tab']->value['icon'];?>
"></i><span><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['tab']->value['title'], 'UTF-8');
if (isset($_smarty_tpl->tpl_vars['tab']->value['list'])) {
}?></span></a>
				<?php if (isset($_smarty_tpl->tpl_vars['tab']->value['list'])) {?><ul class="submenu"><?php
$_from = $_smarty_tpl->tpl_vars['tab']->value['list'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tab2_1_saved_item = isset($_smarty_tpl->tpl_vars['tab2']) ? $_smarty_tpl->tpl_vars['tab2'] : false;
$__foreach_tab2_1_saved_key = isset($_smarty_tpl->tpl_vars['k2']) ? $_smarty_tpl->tpl_vars['k2'] : false;
$_smarty_tpl->tpl_vars['tab2'] = new Smarty_Variable();
$__foreach_tab2_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_tab2_1_total) {
$_smarty_tpl->tpl_vars['k2'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k2']->value => $_smarty_tpl->tpl_vars['tab2']->value) {
$__foreach_tab2_1_saved_local_item = $_smarty_tpl->tpl_vars['tab2'];
if (!empty($_smarty_tpl->tpl_vars['k2']->value)) {?><li class="<?php echo $_smarty_tpl->tpl_vars['tab2']->value['controller_name'];
if ($_smarty_tpl->tpl_vars['page']->value == $_smarty_tpl->tpl_vars['k2']->value) {?> active<?php }?>"><a href="/<?php echo $_smarty_tpl->tpl_vars['ADMIN_URL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['tab2']->value['controller_name'];
echo $_smarty_tpl->tpl_vars['tab2']->value['parameter'];?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['tab2']->value['title'], 'UTF-8');?>
</a></li><?php }
$_smarty_tpl->tpl_vars['tab2'] = $__foreach_tab2_1_saved_local_item;
}
}
if ($__foreach_tab2_1_saved_item) {
$_smarty_tpl->tpl_vars['tab2'] = $__foreach_tab2_1_saved_item;
}
if ($__foreach_tab2_1_saved_key) {
$_smarty_tpl->tpl_vars['k2'] = $__foreach_tab2_1_saved_key;
}
?></ul>
				<?php }?>
				</li><?php }?>
		<?php
$_smarty_tpl->tpl_vars['tab'] = $__foreach_tab_0_saved_local_item;
}
}
if ($__foreach_tab_0_saved_item) {
$_smarty_tpl->tpl_vars['tab'] = $__foreach_tab_0_saved_item;
}
if ($__foreach_tab_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_tab_0_saved_key;
}
?>
	</ul>
	<span class="menu-collapse"><i class="glyphicon glyphicon-menu-hamburger rotate-90"></i></span>
</nav>
<?php }
}
}
