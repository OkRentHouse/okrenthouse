<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:35:11
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/page_header_toolbar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599a2f01c0d5_76361529',
  'file_dependency' => 
  array (
    'aee1c863ca1b2c2a5d528ee26a7bedcc30f11790' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/page_header_toolbar.tpl',
      1 => 1592288971,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60599a2f01c0d5_76361529 ($_smarty_tpl) {
?>
<div id="top_bar"><?php echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;
if (!isset($_smarty_tpl->tpl_vars['title']->value) && isset($_smarty_tpl->tpl_vars['page_header_toolbar_title']->value)) {
$_smarty_tpl->tpl_vars['title'] = new Smarty_Variable($_smarty_tpl->tpl_vars['page_header_toolbar_title']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'title', 0);
}
echo $_smarty_tpl->tpl_vars['top_bar_before_htm']->value;
if (count($_smarty_tpl->tpl_vars['bulk_actions']->value) || count($_smarty_tpl->tpl_vars['toolbar_btn']->value)) {?><div id="top_bar_button"><?php if (count($_smarty_tpl->tpl_vars['bulk_actions']->value) > 0) {?><select name="action" id="action" class="form-control"><?php
$_from = $_smarty_tpl->tpl_vars['bulk_actions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ba_0_saved_item = isset($_smarty_tpl->tpl_vars['ba']) ? $_smarty_tpl->tpl_vars['ba'] : false;
$__foreach_ba_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['ba'] = new Smarty_Variable();
$__foreach_ba_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_ba_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['ba']->value) {
$__foreach_ba_0_saved_local_item = $_smarty_tpl->tpl_vars['ba'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['ba']->value['text'];?>
</option><?php
$_smarty_tpl->tpl_vars['ba'] = $__foreach_ba_0_saved_local_item;
}
}
if ($__foreach_ba_0_saved_item) {
$_smarty_tpl->tpl_vars['ba'] = $__foreach_ba_0_saved_item;
}
if ($__foreach_ba_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_ba_0_saved_key;
}
?></select><?php }
if (count($_smarty_tpl->tpl_vars['toolbar_btn']->value) > 0) {
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['print'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['print']['href'];?>
" class="print btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['print']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['print']['desc'];?>
"><i class="icon glyphicon glyphicon-print"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['print']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['excelimport'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excelimport']['href'];?>
" class="excelimport btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excelimport']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excelimport']['desc'];?>
"><span class="icon glyphicon glyphicon-import"></span><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excelimport']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['excel'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excel']['href'];?>
" class=" excel btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excel']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excel']['desc'];?>
"><i class="icon fa fa-file-excel"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['excel']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['view'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['view']['href'];?>
" class="view btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['view']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['view']['desc'];?>
"><i class="icon glyphicon glyphicon-eye-open"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['view']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['back'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['back']['href'];?>
" class="back btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['back']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['back']['desc'];?>
"><i class="icon glyphicon  glyphicon-chevron-left"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['back']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['del'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['del']['href'];?>
" class="del btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['del']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['del']['desc'];?>
"><i class="icon glyphicon glyphicon-trash"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['del']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['edit'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['edit']['href'];?>
" class="edit btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['edit']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['edit']['desc'];?>
"><i class="icon glyphicon glyphicon-pencil"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['edit']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['new'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['new']['href'];?>
" class="new btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['new']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['new']['desc'];?>
"><i class="icon glyphicon glyphicon-plus"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['new']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['cancel'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['cancel']['href'];?>
" class="cancel btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['cancel']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['cancel']['desc'];?>
"><i class="icon glyphicon glyphicon-remove"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['cancel']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['save'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save']['href'];?>
" class="href btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save']['class'];?>
 save" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save']['desc'];?>
"><i class="icon glyphicon glyphicon-floppy-disk"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['save_and_stay'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save_and_stay']['href'];?>
" class="save_and_stay btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save_and_stay']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save_and_stay']['desc'];?>
"><i class="icon glyphicon glyphicon-floppy-disk"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['save_and_stay']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['search'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['search']['href'];?>
" class="search btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['search']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['search']['desc'];?>
"><i class="icon glyphicon glyphicon-search"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['search']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['btn'])) {
$_from = $_smarty_tpl->tpl_vars['toolbar_btn']->value['btn'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_btn_1_saved_item = isset($_smarty_tpl->tpl_vars['btn']) ? $_smarty_tpl->tpl_vars['btn'] : false;
$__foreach_btn_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['btn'] = new Smarty_Variable();
$__foreach_btn_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_btn_1_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['btn']->value) {
$__foreach_btn_1_saved_local_item = $_smarty_tpl->tpl_vars['btn'];
?><a href="<?php echo $_smarty_tpl->tpl_vars['btn']->value['href'];?>
" class="btn btn-default pull-left<?php echo $_smarty_tpl->tpl_vars['btn']->value['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['btn']->value['title'];?>
"><?php if ($_smarty_tpl->tpl_vars['btn']->value['icon']) {?><i class="<?php echo $_smarty_tpl->tpl_vars['btn']->value['icon'];?>
"></i><?php }
echo $_smarty_tpl->tpl_vars['btn']->value['desc'];?>
</a><?php
$_smarty_tpl->tpl_vars['btn'] = $__foreach_btn_1_saved_local_item;
}
}
if ($__foreach_btn_1_saved_item) {
$_smarty_tpl->tpl_vars['btn'] = $__foreach_btn_1_saved_item;
}
if ($__foreach_btn_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_btn_1_saved_key;
}
}
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['media_search'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['media_search']['href'];?>
" class="media_search btn btn-default pull-left visible-sm-inline-block visible-xs-inline-block<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['media_search']['class'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['media_search']['desc'];?>
"><i class="icon glyphicon glyphicon-search"></i><?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['media_search']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']) || isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['list'])) {?><div class="btn-group btn-group" role="group" aria-label="Large button group"><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']['href'];?>
" class="view btn btn-default pull-left<?php if (array_key_exists(("calendar").($_smarty_tpl->tpl_vars['table']->value),$_GET)) {?> active<?php }?>" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']['desc'];?>
"><?php if (!isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']['icon'])) {?><i class="icon glyphicon glyphicon-calendar"></i><?php } elseif ($_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']['icon'] != '') {?><i class="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']['icon'];?>
"></i><?php }
echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']['desc'];?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['list'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['list']['href'];?>
" class="view btn btn-default pull-left<?php if (!array_key_exists(("calendar").($_smarty_tpl->tpl_vars['table']->value),$_GET)) {?> active<?php }?>" title="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['list']['desc'];?>
"><?php if (!isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['list']['icon'])) {?><i class="icon glyphicon glyphicon-list-alt"></i><?php } elseif ($_smarty_tpl->tpl_vars['toolbar_btn']->value['list']['icon'] != '') {?><i class="<?php echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['list']['icon'];?>
"><?php }
echo $_smarty_tpl->tpl_vars['toolbar_btn']->value['list']['desc'];?>
</i></a><?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['calendar']) || isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['list'])) {?></div><?php }
}?></div><?php }
echo $_smarty_tpl->tpl_vars['top_bar_after_htm']->value;?>
</div><?php if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['save']) || isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['save_and_stay']) || isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['search'])) {
echo '<script'; ?>
 type="text/javascript"><?php if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['save'])) {?>$('.save').click(function(){$('#content>.content form').submit()});<?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['save_and_stay'])) {?>$('.save_and_stay').click(function(){$('input[name="<?php echo $_smarty_tpl->tpl_vars['submit_action']->value;?>
"]').attr('name', '<?php echo $_smarty_tpl->tpl_vars['submit_action']->value;?>
AndStay');$('#content>.content form').submit();});<?php }
if (isset($_smarty_tpl->tpl_vars['toolbar_btn']->value['search'])) {?>$('a.search').click(function(){$('.content>form.list').submit()});<?php }
echo '</script'; ?>
><?php }
}
}
