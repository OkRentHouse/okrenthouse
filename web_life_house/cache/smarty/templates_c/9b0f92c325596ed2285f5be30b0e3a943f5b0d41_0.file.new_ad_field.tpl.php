<?php
/* Smarty version 3.1.28, created on 2021-04-09 11:22:40
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/new_ad_field.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_606fc880ac5512_54753754',
  'file_dependency' => 
  array (
    '9b0f92c325596ed2285f5be30b0e3a943f5b0d41' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/new_ad_field.tpl',
      1 => 1617938552,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606fc880ac5512_54753754 ($_smarty_tpl) {
?>

<table width="100%" border="0">
  <tr>
    <td class="main_t_2 input-group-addon">精選</td>
    <td class="main_t_2 input-group-addon"><?php if ($_smarty_tpl->tpl_vars['ad_choose_top']->value == "首頁精選") {?>
    <input type="radio" id="ad_choose_top" name="ad_choose_top" value="首頁精選" checked>首頁精選
    <?php } else { ?>
    <input type="radio" id="ad_choose_top" name="ad_choose_top" value="首頁精選">首頁精選
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_top']->value == "內頁推薦") {?>
    <input type="radio" id="ad_choose_top" name="ad_choose_top" value="內頁推薦" checked>內頁推薦
    <?php } else { ?>
    <input type="radio" id="ad_choose_top" name="ad_choose_top" value="內頁推薦">內頁推薦
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_top']->value == "置頂") {?>
    <input type="radio" id="ad_choose_top" name="ad_choose_top" value="置頂" checked>置頂
    <?php } else { ?>
    <input type="radio" id="ad_choose_top" name="ad_choose_top" value="置頂">置頂
    <?php }?></td>
    <td class="main_t_2 input-group-addon">排序:</td>
    <td class="main_t_2 input-group-addon"><input type="text" id="position1" name="position1" value="<?php echo $_smarty_tpl->tpl_vars['position1']->value;?>
"></td>
    <td class="main_t_2 input-group-addon">刊登期間:</td>
    <td class="main_t_2 input-group-addon"><input type="date" id="ad_sdate1" name="ad_sdate1" value="<?php echo $_smarty_tpl->tpl_vars['ad_sdate1']->value;?>
"> ~ <input type="date" id="ad_edate1" name="ad_edate1" value="<?php echo $_smarty_tpl->tpl_vars['ad_edate1']->value;?>
">
    
    </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">刊登方案</td>
    <td class="main_t_2 input-group-addon"><?php if ($_smarty_tpl->tpl_vars['ad_choose_type']->value == "首頁精選") {?>
    <input type="radio" id="ad_choose_type" name="ad_choose_type" value="首頁精選" checked>首頁精選
    <?php } else { ?>
    <input type="radio" id="ad_choose_type" name="ad_choose_type" value="首頁精選">首頁精選
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_type']->value == "內頁推薦") {?>
    <input type="radio" id="ad_choose_type" name="ad_choose_type" value="內頁推薦" checked>內頁推薦
    <?php } else { ?>
    <input type="radio" id="ad_choose_type" name="ad_choose_type" value="內頁推薦">內頁推薦
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_type']->value == "置頂") {?>
    <input type="radio" id="ad_choose_type" name="ad_choose_type" value="置頂" checked>置頂
    <?php } else { ?>
    <input type="radio" id="ad_choose_type" name="ad_choose_type" value="置頂">置頂
    <?php }?></td>
    <td class="main_t_2 input-group-addon">排序:</td>
    <td class="main_t_2 input-group-addon"><input type="text" id="position2" name="position2" value="<?php echo $_smarty_tpl->tpl_vars['position2']->value;?>
"></td>
    <td class="main_t_2 input-group-addon">刊登期間:</td>
    <td class="main_t_2 input-group-addon"><input type="date" id="ad_sdate2" name="ad_sdate2" value="<?php echo $_smarty_tpl->tpl_vars['ad_sdate2']->value;?>
"> ~ <input type="date" id="ad_edate2" name="ad_edate2" value="<?php echo $_smarty_tpl->tpl_vars['ad_edate2']->value;?>
">
    
    </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">刊登類別</td>
    <td class="main_t_2 input-group-addon">免費刊登：
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_payment_type']->value == "房東") {?>
    <input type="radio" id="ad_choose_payment_type" name="ad_choose_payment_type" value="房東" checked>房東
    <?php } else { ?>
    <input type="radio" id="ad_choose_payment_type" name="ad_choose_payment_type" value="房東">房東
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_payment_type']->value == "代理人") {?>
    <input type="radio" id="ad_choose_payment_type" name="ad_choose_payment_type" value="代理人" checked>代理人
    <?php } else { ?>
    <input type="radio" id="ad_choose_payment_type" name="ad_choose_payment_type" value="代理人">代理人
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_payment_type']->value == "仲介") {?>
    <input type="radio" id="ad_choose_payment_type" name="ad_choose_payment_type" value="仲介" checked>仲介
    <?php } else { ?>
    <input type="radio" id="ad_choose_payment_type" name="ad_choose_payment_type" value="仲介">仲介
    <?php }?></td>
    <td class="main_t_2 input-group-addon">費用:</td>
    <td class="main_t_2 input-group-addon"><input type="text" id="ad_price" name="ad_price" value="<?php echo $_smarty_tpl->tpl_vars['ad_price']->value;?>
"></td>
    <td class="main_t_2 input-group-addon">付費:</td>
    <td class="main_t_2 input-group-addon"><?php if ($_smarty_tpl->tpl_vars['ad_payment']->value == "超商") {?>
      <input type="radio" id="ad_payment" name="ad_payment" value="超商" checked>
      超商
      <?php } else { ?>
      <input type="radio" id="ad_payment" name="ad_payment" value="超商">
      超商
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['ad_payment']->value == "刷卡") {?>
      <input type="radio" id="ad_payment" name="ad_payment" value="刷卡" checked>
      刷卡
      <?php } else { ?>
      <input type="radio" id="ad_payment" name="ad_payment" value="刷卡">
      刷卡
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['ad_payment']->value == "匯款") {?>
      <input type="radio" id="ad_payment" name="ad_payment" value="匯款" checked>
      匯款
      <?php } else { ?>
      <input type="radio" id="ad_payment" name="ad_payment" value="匯款">
      匯款
      <?php }?> </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">收費刊登</td>
    <td class="main_t_2 input-group-addon">
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_payment_type1']->value == "系統") {?>
    <input type="radio" id="ad_choose_payment_type1" name="ad_choose_payment_type1" value="系統" checked>系統
    <?php } else { ?>
    <input type="radio" id="ad_choose_payment_type1" name="ad_choose_payment_type1" value="系統">系統
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ad_choose_payment_type1']->value == "加盟店") {?>
    <input type="radio" id="ad_choose_payment_type1" name="ad_choose_payment_type1" value="加盟店" checked>加盟店
    <?php } else { ?>
    <input type="radio" id="ad_choose_payment_type1" name="ad_choose_payment_type1" value="加盟店">加盟店
    <?php }?></td>
    <td class="main_t_2 input-group-addon">&nbsp;</td>
    <td class="main_t_2 input-group-addon"></td>
    <td class="main_t_2 input-group-addon"></td>
    <td class="main_t_2 input-group-addon">
    
    </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">總機</td>
    <td class="main_t_2 input-group-addon"><input type="text" id="clode_number" name="clode_number" value="<?php echo $_smarty_tpl->tpl_vars['clode_number']->value;?>
"></td>
    <td class="main_t_2 input-group-addon">&nbsp;</td>
    <td class="main_t_2 input-group-addon">&nbsp;</td>
    <td class="main_t_2 input-group-addon">更新:</td>
    <td class="main_t_2 input-group-addon">
    <input type="radio" id="new_ad_update" name="new_ad_update" value="0" checked>不更新
    <input type="radio" id="new_ad_update" name="new_ad_update" value="1">更新
    <input type="radio" id="new_ad_update" name="new_ad_update" value="2">新增
    </td>
  </tr>
</table>
<?php }
}
