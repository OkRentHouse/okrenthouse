<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:49:50
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/main_menu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599d9e6fcc70_38164444',
  'file_dependency' => 
  array (
    '5ca10b146337d8a02729e6c36b6b3747feb3c8a0' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/main_menu.tpl',
      1 => 1616147996,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60599d9e6fcc70_38164444 ($_smarty_tpl) {
?>
<style>

form#search_box_big {
}

form#search_box_big {
    margin: auto;
    width: calc(100% + 30px);
    margin-left: -15px;
    display: flex;
    padding: 1% 2%;
    padding-top: .5%;
}

header.minbar2 {
    font-size: 8pt;
    height: 100%;
    margin: auto;
    text-align: right;
}

header.minbar2 a {
    font-size: 20pt !important;
    padding: 0 1em;
}

header.minbar2 a {
    color: #FFF;
    line-height: 45px;
    padding: 0 10px;
    display: inline-block;
    position: relative;
    font-size: 14pt !important;
}

svg.svg-inline--fa.fa-share-alt.fa-w-14, svg#hb_line {
    vertical-align: middle;
    transform: scale(1.5);
    color: #FFF;
}

svg#hb_line, svg.svg-inline--fa.fa-share-alt.fa-w-14, svg#hb_line {
    font-size: 18pt;
    width: 18pt;
    height: auto;
}


svg#hb_line path {
    fill: #FFF;
}

header.minbar2 .menu a:after {
    position: absolute;
    top: 50%;
    right: 0;
    width: 1px;
    height: 20px;
    content: "";
    border-right: 1px solid #bebebe;
    display: block;
    margin-top: -10px;
}

header.minbar2 .menu a.noborder:after,
header.minbar2 .menu a:last-child:after {
    border-right: 0px solid #bebebe;
}

</style>

<form action="https://okrent.house/list" method="get" id="search_box_big">


    <header class="minbar2 col-sm-4 l">
      <nav class="main_menu2 not_select">
      <div class="content_width">

      </div>
      </nav>
    </header>

    <input type="hidden" name="transfer_in" value="1">
    <div class="search_list_wrap col-sm-4">
      <!-- <img style="    margin: auto;" src="/themes/Rent/img/logo_main.svg"> -->

    </div>

    <header class="minbar2 col-sm-4 r">
                <nav class="main_menu2 not_select">

      <div class="content_width">



        <nav class="menu">


          <div class="menu_right" style="display: block;">


            <a href="https://www.okrent.house/Register">註冊</a>
            <a class="noborder" href="https://www.okrent.house/login">登入</a>
            <a class="noborder"><svg class="svg-inline--fa fa-share-alt fa-w-14 not_select" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="share-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M352 320c-22.608 0-43.387 7.819-59.79 20.895l-102.486-64.054a96.551 96.551 0 0 0 0-41.683l102.486-64.054C308.613 184.181 329.392 192 352 192c53.019 0 96-42.981 96-96S405.019 0 352 0s-96 42.981-96 96c0 7.158.79 14.13 2.276 20.841L155.79 180.895C139.387 167.819 118.608 160 96 160c-53.019 0-96 42.981-96 96s42.981 96 96 96c22.608 0 43.387-7.819 59.79-20.895l102.486 64.054A96.301 96.301 0 0 0 256 416c0 53.019 42.981 96 96 96s96-42.981 96-96-42.981-96-96-96z"></path></svg><!-- <i class="fas fa-share-alt"></i> Font Awesome fontawesome.com --></a>
            <a class="no_border not_select" href="#">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="hb_line" class="hb_line not_select" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve">
<g>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,19.312h104.659c2.536,0,4.612,2.074,4.612,4.84l0,0   c0,2.767-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.075-4.61-4.842l0,0C1.56,21.386,3.634,19.312,6.17,19.312z"></path>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,87.777h104.659c2.536,0,4.612,2.306,4.612,4.842l0,0   c0,2.766-2.076,5.071-4.612,5.071H6.17c-2.536,0-4.61-2.306-4.61-5.071l0,0C1.56,90.083,3.634,87.777,6.17,87.777z"></path>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,53.428h104.659c2.536,0,4.612,2.306,4.612,5.072l0,0   c0,2.536-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.306-4.61-4.842l0,0C1.56,55.734,3.634,53.428,6.17,53.428z"></path>
</g>
<g>
	<defs>
		<path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"></path>
	</defs>
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_51_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"></rect>
	</defs>
	<clipPath id="SVGID_4_">
		<use xlink:href="#SVGID_81_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"></rect>
	</defs>
	<clipPath id="SVGID_6_">
		<use xlink:href="#SVGID_105_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"></rect>
	</defs>
	<clipPath id="SVGID_8_">
		<use xlink:href="#SVGID_129_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"></rect>
	</defs>
	<clipPath id="SVGID_10_">
		<use xlink:href="#SVGID_155_" overflow="visible"></use>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"></rect>
	</defs>
	<clipPath id="SVGID_12_">
		<use xlink:href="#SVGID_183_" overflow="visible"></use>
	</clipPath>
</g>
<image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
</image>
</svg></a>


          </div>

          </nav>

      </div>

    </nav>

    		</header>

</form>
<?php }
}
