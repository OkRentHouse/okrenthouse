<?php
/* Smarty version 3.1.28, created on 2021-04-29 20:01:35
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_608aa01f9e1b69_51768473',
  'file_dependency' => 
  array (
    '6aa977699d5fa31658685d792ae5a02f0d954e3d' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/controllers/Index/content.tpl',
      1 => 1619697686,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608aa01f9e1b69_51768473 ($_smarty_tpl) {
?>

    <div id="greybg"></div>

    <nav class="change">
        <div class="logo exist mo">
        <a href="index.html" title="生活集團"></a>
    </div>
        <div id="mobile_menu_labIcon">
            <div class="hamburger" id="hamburger-1">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>
        </div>

        <div class="width91">
            <div class="logo pc">
                <a href="index.html" title="生活集團">生活集團</a>
            </div>

            <nav class="navbar js-navbar">
                <ul class="menu">

                    <li>
                        <a class="hasDropdown" href="https://okrent.house/about">關於生活</a>

                        <ul class="container">
                            <div class="container__list">
                                <a class="container__listItem" href="#">理念展望</a>
                                <a class="container__listItem" href="#">歷史軌跡</a>
                                <a class="container__listItem" href="#">合作提案</a>
                                <a class="container__listItem" href="#">聯絡我們</a>
                        </ul>
                    </li>
                    <li>
                        <a class="hasDropdown" href="#">分享共好</a>
                        <ul class="container has-multi">
                            <div class="container__list container__list-multi">
                                <a class="container__listItem" href="https://okrent.house/">生活樂租 </a>

                                <a class="container__listItem" href="https://okrent.house/land">生活房屋</a>

                                <a class="container__listItem" href="https://okmaster.life/Store">生活好康</a>

                                <a class="container__listItem" href="#">共好夥伴</a>

                            </div>
                        </ul>
                    </li>
                    <li>
                        <a class="hasDropdown" href="https://okrent.house/geely_for_rent">最新訊息 </a>

                    </li>
                    <li>
                        <a class="hasDropdown" href="https://okrent.house/JoinLife">加入生活</a>
                        <ul class="container has-multi">
                            <div class="container__list container__list-multi">
                                <a class="container__listItem" href="https://okrent.house/ExpertAdvisor">生活家族</a>
                                <a class="container__listItem" href="https://okrent.house/Joinrenthouse">菁英招募</a>
                            </div>
                        </ul>
                    </li>
                    <li class="page-link page-link2"><a href="https://okrent.house/Register">註冊</a><span class="line">|</span><a href="https://okrent.house/login">登入</a>
                    </li>


                    <li class="page-link"><a href="#"><i class="share_white"></i>分享</a></li>
                </ul>


                </div>

            </nav>
            <section class="main animated fadeIn">
                <div class="black flex">

                    <!--<div data-wow-delay="0.3s" class="main_logo wow bounceIn "></div>-->
					<svg class="main_logo www bounceIn" xmlns="http://www.w3.org/2000/svg" width="278.781" height="176.764" viewBox="0 0 278.781 176.764">
  <g id="main_logo" transform="translate(-186.12 -137.85)">
    <g class="logo_icon" id="组_205" data-name="组 205" transform="translate(283.12 137.85)">
      <path id="路径_619" data-name="路径 619" d="M329.328,163.523l-9.261-5.051v-6.384a2.752,2.752,0,0,0-.631-1.684,2.357,2.357,0,0,0-1.684-.772h-3.508a2.324,2.324,0,0,0-1.684.7,2.512,2.512,0,0,0-.772,1.754v3.578c-1.4-.561-2.806-1.052-4.209-1.614a14.637,14.637,0,0,0-4.841-1.543,11.147,11.147,0,0,0-4.981,1.052c-5.332,2.1-10.243,4.139-15.224,6.244-3.017,1.263-6.033,2.526-8.98,3.718a2.93,2.93,0,0,0-1.614,2.175,3.169,3.169,0,0,0-.07,1.4,2.778,2.778,0,0,0,.491,1.333,1.737,1.737,0,0,0,1.473.772H279.1v12.558a2.6,2.6,0,0,0,.421,2.035c.421.561,1.263.912,2.806.982H288.6a3.617,3.617,0,0,1,2.07-3.859l39.94-16.055A2.329,2.329,0,0,0,329.328,163.523Z" transform="translate(-259.243 -119.044)" fill="#fff"/>
      <path id="路径_620" data-name="路径 620" d="M304.872,184.765c-4.7,1.824-6.735,2.526-11.3,4.35-1.795.731-3.468-.37-4.274-1.964h-7.092a6.4,6.4,0,0,1-1.1-.094,39.349,39.349,0,0,0,6.849,4.233c3.367,1.684,5.472,1.824,9.05.561l9.261-3.438C308.239,187.5,306.906,183.993,304.872,184.765Z" transform="translate(-259.481 -119.941)" fill="#fff"/>
      <path id="路径_621" data-name="路径 621" d="M301.076,118.24A42.164,42.164,0,1,0,343.169,160.4,42.247,42.247,0,0,0,301.076,118.24ZM282.063,126.1c6.945-3.859,15.084-5.823,22.38-4.841A39.951,39.951,0,0,1,335.031,140.9a3.028,3.028,0,0,1-5.4,2.736c-3.508-5.753-9.12-10.173-14.943-13.049-3.367-1.614-5.472-1.824-9.05-.491l-9.261,3.438c-1.964.842-.631,4.42,1.473,3.578,4.7-1.754,6.735-2.455,11.225-4.28,3.788-1.543,7.086,5.051,2.666,7.3L262,160.123C262.42,146.373,270.488,132.482,282.063,126.1Zm38.025,67.981c-7.016,3.929-15.084,5.823-22.38,4.911a40.038,40.038,0,0,1-30.658-19.714c-1.754-3.087,3.087-6.525,5.4-2.736a31.041,31.041,0,0,0,4.868,6.039,5.465,5.465,0,0,1-.1-1.619V169.945l-4.069-.07-.842-.14-.7-.421a3.52,3.52,0,0,1-.982-1.052,4.942,4.942,0,0,1-.631-2.035,5.951,5.951,0,0,1,.491-2.876l.281-.491a4.124,4.124,0,0,1,1.894-1.543c8.068-3.367,16.066-6.805,24.2-9.962a14.042,14.042,0,0,1,4.771-1.193,10.787,10.787,0,0,1,3.648.631c1.052.351,2.175.912,3.3,1.333.421.14.912.351,1.333.491V151a4.208,4.208,0,0,1,3.017-3.578,4.141,4.141,0,0,1,.982-.14h3.929l.772.21a4.41,4.41,0,0,1,1.824,1.333,4.5,4.5,0,0,1,.842,2.455v5.472l8.489,4.63a3.733,3.733,0,0,1,1.543,1.4,5.9,5.9,0,0,1,.358.715l8.412-3.381C339.661,173.874,331.594,187.765,320.088,194.079Z" transform="translate(-258.912 -118.24)" fill="#fff"/>
    </g>
    <g class="logo_text" id="组_206" data-name="组 206" transform="translate(186.12 236.272)">
      <path id="路径_622" data-name="路径 622" d="M310.708,272.921v12.8h-3.286l-2.335-3.372c-2.421,2.508-6.139,3.718-11.241,3.718q-8.431,0-12.711-3.891a11.906,11.906,0,0,1-4.151-8.993,11.31,11.31,0,0,1,4.669-9.079c3.113-2.508,7.436-3.8,13.057-3.8a21.455,21.455,0,0,1,10.376,2.248c2.681,1.47,4.41,3.286,5.1,5.448l-6.485.692c-1.211-3.113-4.151-4.583-8.82-4.583a11.259,11.259,0,0,0-7.177,2.162c-1.816,1.383-2.767,3.632-2.767,6.658,0,6.312,3.286,9.425,9.858,9.425a12.1,12.1,0,0,0,6.4-1.556,5.005,5.005,0,0,0,2.508-4.237h-8.647l2.075-3.632Z" transform="translate(-167.858 -207.722)" fill="#fff"/>
      <path id="路径_623" data-name="路径 623" d="M340.37,285.646H332.5l-6.485-10.29h-1.9v-3.632h.692a14.707,14.707,0,0,0,5.966-.951,3.071,3.071,0,0,0,1.989-2.853,2.98,2.98,0,0,0-1.9-2.681,11.732,11.732,0,0,0-5.448-1.038h-1.3v-3.545h1.9c7.264,0,12.971.692,12.971,7.35,0,3.2-.865,5.361-5.88,6.572Zm-16.256-10.29h-8.647v10.29h-7.523v-24.99h16.17V264.2h-8.647v7.523h8.647Z" transform="translate(-161.636 -207.65)" fill="#fff"/>
      <path id="路径_624" data-name="路径 624" d="M396.456,260.656V276.74a9.14,9.14,0,0,1-1.211,5.015,10.479,10.479,0,0,1-4.756,3.026,27.019,27.019,0,0,1-8.474,1.211,27.994,27.994,0,0,1-8.388-1.038,10.519,10.519,0,0,1-4.756-3.026A8.1,8.1,0,0,1,367.488,277V260.656H375.1V276.74c0,2.162.778,3.632,2.335,4.324a14.493,14.493,0,0,0,10.982-.086c1.384-.778,2.075-2.162,2.075-4.237V260.656Z" transform="translate(-149.669 -207.65)" fill="#fff"/>
      <path id="路径_625" data-name="路径 625" d="M409.419,260.656h1.9c7.869,0,11.76,1.729,11.76,7.436,0,5.275-3.8,7.609-10.982,7.609h-2.681v-3.8h1.211a8.3,8.3,0,0,0,4.669-1.038,3.145,3.145,0,0,0,1.816-2.767,2.969,2.969,0,0,0-1.643-2.594c-1.124-.778-2.594-1.124-5.534-1.124h-.519Zm-7.7,24.99H394.2v-24.99h15.219v3.718h-7.7V271.9h7.7v3.8h-7.7Z" transform="translate(-144.3 -207.65)" fill="#fff"/>
      <rect id="矩形_85" data-name="矩形 85" width="6.312" height="24.99" transform="translate(28.622 53.006)" fill="#fff"/>
      <path id="路径_626" data-name="路径 626" d="M209.279,285.646H187.056v-24.99h6.4v20.926h19.283Z" transform="translate(-185.932 -207.65)" fill="#fff"/>
      <path id="路径_627" data-name="路径 627" d="M224.271,271.234h20.58l-2.335,3.372H224.271c0,6.918-.259,6.053,0,11.068h-6.4V273.05h0a12.547,12.547,0,0,1,12.538-12.538h14.354l-2.594,3.632H231.707C226.951,264.836,224.963,266.824,224.271,271.234Z" transform="translate(-179.738 -207.679)" fill="#fff"/>
      <path id="路径_628" data-name="路径 628" d="M254.6,260.512h14.354l-2.421,3.632c-3.632-.087-4.929,0-8.561,0-5.534,0-9.512,1.989-9.685,7.091h20.493l-2.421,3.372-17.986.087c.086,4.842,4.237,7,9.6,7h10.982l-2.767,3.8H254.6a12.529,12.529,0,0,1-12.538-12.452h0A12.547,12.547,0,0,1,254.6,260.512Z" transform="translate(-174.876 -207.679)" fill="#fff"/>
      <path id="路径_629" data-name="路径 629" d="M353.246,260.656h4.929a12.511,12.511,0,0,1,12.452,12.452h0a12.529,12.529,0,0,1-12.452,12.538h-4.929v-4.41h3.113a8.119,8.119,0,0,0,8.128-8.128h0a8.1,8.1,0,0,0-8.128-8.042h-3.113Zm-4.842,0h4.842v4.41H350.22a8.1,8.1,0,0,0-8.128,8.042h0a8.119,8.119,0,0,0,8.128,8.128h3.026v4.41H348.4a12.529,12.529,0,0,1-12.452-12.538h0A12.511,12.511,0,0,1,348.4,260.656Z" transform="translate(-156.007 -207.65)" fill="#fff"/>
      <path id="路径_630" data-name="路径 630" d="M424.477,260.533v-4.151h9.166v-33.9h0c0-.432-.259-.605-.778-.605h-8.388v-4.237H434.94c3.372,0,5.1,2.162,5.1,5.448v39h-6.4v-1.556Zm0-4.583a3.314,3.314,0,0,0,2.248-2.421v-3.718h3.026l2.335-3.978h-5.361v-1.556l1.3-.173a4.084,4.084,0,0,0,1.038,1.556l4.583-1.9a22.51,22.51,0,0,0-3.286-4.41c0-.173.086-.259.259-.432v-8.474c0-1.556-1.124-2.854-3.8-2.854h-2.335V240.04l.259.259h-.259v15.651Zm0-29.313h4.237l2.94-3.8h-7.177Zm0-8.993v4.237h-6.572v-4.237Zm0,5.188v3.8h-6.572v-3.8Zm0,4.756V240.04c-.432-.259-.605-.432-.778-.519h-5.794v-3.459h6.312c.173-.173.259-.173.259-.173v-.951h-6.572v-2.681h6.572v-.951a.229.229,0,0,0-.259-.259h-6.312v-3.459Zm0,12.711v15.651a6.706,6.706,0,0,1-1.556.432h1.556v4.151h-6.572v-8.82c1.038.086,1.729.086,1.989.086.432-.432.692-.692.692-.778v-1.211h-2.681v-3.978h2.681v-1.556l-2.681.086v-3.978Zm-6.572-22.655v4.237h-2.681v.951h2.681v3.8h-2.681v.951h2.681v3.459h-2.681v1.211h2.681v2.681h-2.681v1.124h2.681v3.459h-2.681v.951l2.681-.086v3.978l-12.711.346v-3.978l3.891-.086v-1.124h-3.891v-3.459h3.891v-1.124h-3.891v-2.681h3.891v-1.211h-3.891v-3.459h3.891v-.951h-3.891v-3.8h3.891v-.951h-3.891v-4.237Zm0,28.189v3.978H405.28A44.05,44.05,0,0,1,409.6,253.1l-4.064,2.854c-.086-.086-.259-.173-.346-.259v-9.858Zm0,5.88c-.951-.173-2.421-.432-4.151-.692l.519,4.756a7.285,7.285,0,0,0,3.026.605H405.194v4.151h12.711Zm-84.049-1.643c-1.124.432-2.248.778-3.372,1.124v5.015c2.421-.778,4.756-1.729,7.177-2.681v7.609h6.4v-7.609a110.642,110.642,0,0,0,22.742,6.485l3.026-4.583c-8.82-.951-16.429-2.681-22.742-5.361h18.851l2.681-4.583H344.059v-1.729H363l2.248-4.237H344.059v-1.9h18.678l1.47-4.237H344.059v-1.729h18.678l1.47-4.237H344.059v-1.9h19.2l1.989-4.237h-19.11a10.343,10.343,0,0,1,1.729-2.421l-5.88-2.335a40.945,40.945,0,0,1-3.026,4.756h-8.474v4.237h7.177v1.9h-7.177v4.237h7.177v1.729h-7.177v4.237h7.177v1.9h-7.177v4.237h7.177v1.729h-7.177v4.583Zm71.338-32.426v4.237H392.742c-2.335,0-3.026,1.038-3.026,2.854v31.648h15.478v4.151H389.716v1.556h-6.4V222.832c0-3.286,1.556-5.188,4.669-5.188Zm0,5.188H391.7v3.8h13.489v-3.8Zm0,4.756h-8.82c-2.421,0-3.113,1.384-3.113,3.286v5.534c0,1.729,1.3,3.113,3.632,3.113h8.3v-3.459H399.4v-1.124h5.794v-2.681H399.4v-.951q-.259-.259,0-.259h5.794v-3.459Zm0,13.144-12.711.346.778,3.978,11.933-.346v-3.978Zm0,5.1H391.7v3.978h10.031L399.4,251.8a24.064,24.064,0,0,1,5.794,3.891Zm-74.71,5.361a88.657,88.657,0,0,1-20.148,4.237l1.989,4.756a77.9,77.9,0,0,0,18.159-3.978v-5.015Zm0-29.919h-5.621c.519-.778,1.124-1.556,1.816-2.421l-5.88-2.162a34.048,34.048,0,0,1-12.279,11.674l5.361,3.286,3.113-2.335v12.711c0,1.124.692,1.729,2.248,1.729h11.241v-4.237h-6.4a.688.688,0,0,1-.778-.778v-1.124h7.177v-4.237h-7.177v-1.729h7.177v-4.237h-7.177v-1.3c0-.432.259-.605.778-.605h6.4v-4.237Zm0,24.212H311.114v4.583h19.369Z" transform="translate(-161.52 -216.52)" fill="#fff"/>
      <path id="路径_631" data-name="路径 631" d="M286,240.8V235.1h15.219l3.372-5.1H286v-6.485h1.47q10.376-.778,15.305-1.556l-1.038-4.929c-3.8.519-9.771.951-17.813,1.384-7.782.519-14.7.778-20.666.778l.778,4.929c4.064,0,9.166-.086,15.219-.173v6.053H260.928v5.1H279.26V240.8Z" transform="translate(-171.085 -216.419)" fill="#fff"/>
      <path id="路径_632" data-name="路径 632" d="M259.325,238.12c-2.248,6.485-6.4,12.192-12.365,17.381l6.139,2.508c5.188-4.323,9.425-10.376,12.711-18.245Z" transform="translate(-173.892 -212.179)" fill="#fff"/>
      <path id="路径_633" data-name="路径 633" d="M247.968,232a66.812,66.812,0,0,1,11.674,5.88l3.891-4.151a69.14,69.14,0,0,0-11.674-6.053Z" transform="translate(-173.69 -214.277)" fill="#fff"/>
      <path id="路径_634" data-name="路径 634" d="M248.184,221.679a73.65,73.65,0,0,1,11.673,6.658l4.151-3.978a58.093,58.093,0,0,0-11.933-6.831Z" transform="translate(-173.646 -216.317)" fill="#fff"/>
      <path id="路径_635" data-name="路径 635" d="M215.347,256.469H188.455v5.1h55.168l3.2-5.1H222.265V245.487H239.04l3.459-5.1H222.265V229.922h19.024l2.94-5.1H222.265v-7.436h-6.918v7.436H202.636a20.362,20.362,0,0,0,2.075-6.485l-6.658-.951a36.357,36.357,0,0,1-4.151,10.2,41.066,41.066,0,0,1-7.782,7.7l5.707,3.286a43.3,43.3,0,0,0,8.215-8.647h15.305v10.463H194.681v5.1h20.666Z" transform="translate(-186.12 -216.346)" fill="#fff"/>
      <path id="路径_636" data-name="路径 636" d="M283.15,242.358h10.636c2.767,0,5.015,2.681,5.015,5.621h0a4.932,4.932,0,0,1-5.015,5.1H283.15v5.534h11.328c5.361,0,9.771-4.756,9.771-10.636h0c0-5.794-4.41-11.155-9.771-11.155H283.15Zm-10.549,0H283.15v-5.534H272c-5.361,0-9.771,5.361-9.771,11.155h0c0,5.88,4.41,10.636,9.771,10.636H283.15V253.08H272.6a4.932,4.932,0,0,1-5.015-5.1h0C267.585,245.039,269.833,242.358,272.6,242.358Z" transform="translate(-170.825 -212.439)" fill="#fff"/>
    </g>
  </g>
</svg>
                    <div class="Beehive_form  animated fadeInUp ">
                        <!-- 好幫手 -->
                        <a class="good_helper flex" href="https://okpt.life/" target="_blank">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1s" class="good_helper_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/good_helper.svg" alt="好幫手">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/good_helper_pink.svg" alt="好幫手"></div>

                        </a>
                        <!-- 共享圈 -->
                        <a class="Sharing_circle flex" href="https://okrent.tw" target="_blank">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1.1s" class="Sharing_circle_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/Sharing_circle.svg" alt="共享圈">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/Sharing_circle_green.svg" alt="共享圈"></div>
                        </a>
                        <!-- 裝修達人 -->
                        <a class="decoration_expert flex" href="https://epro.house/" target="_blank">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1.2s" class="decoration_expert_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/decoration_expert.svg" alt="">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/decoration_expert_color.svg" alt=""></div>
                        </a>
                        <!-- 生活房屋 -->
                        <a class="house flex" href="https://okrent.house/land">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1.3s" class="house_color_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/house.svg" alt="">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/house_color.svg" alt=""></div>
                        </a>
                        <!-- 生活樂租 -->
                        <a class="life_lease flex" href="https://okrent.house/" target="_blank">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1.4s" class="life_lease_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/life_lease.svg" alt="">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/life_lease_color.svg" alt=""></div>
                        </a>
                        <!-- 生活好康 -->
                        <a class="Good_life flex" href="https://okmaster.life/Store" target="_blank">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1.5s" class="Good_life_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/Good_life.svg" alt="">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/Good_life_color.svg" alt=""></div>
                        </a>
                        <!-- 歡樂購 -->
                      <a class="happy_shopping flex" href="https://176.shopping/" target="_blank">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani vedr stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1.6s" class="happy_shopping_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/happy_shopping.svg" alt="">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/happy_shopping_color.svg" alt=""></div>
                        </a>

                        <!-- 超立淨 -->
                        <a class="Ultra_clean flex" href="https://okmaster.life/Cleansolution" target="_blank">
                            <svg width="150.101" height="130" viewBox="0 0 150.101 130">
                        <path class="ani stroke" id="Path_511" data-name="Path 511"
                            d="M355.79,728.72l18.53-32.247L392.8,664.54,374.32,632.607,355.79,600.72H281.761l-18.215,31.888-18.53,31.932,18.53,31.932,18.215,32.247H355.79Z"
                            transform="translate(-243.86 -599.72)" fill="none" stroke="#c9caca" stroke-miterlimit="10"
                            stroke-width="2" fill-rule="evenodd" />
                    </svg>
                            <img data-wow-delay="1.7s" class="Ultra_clean_ico wow bounceIn " src="themes/LifeHouse/img/welcom/042021/Ultra_clean.svg" alt="">
                            <div class="select flex"><img src="themes/LifeHouse/img/welcom/042021/Ultra_clean_color.svg" alt=""></div>
                        </a>
                    </div>
                    <div data-wow-delay="1.1s" class="mouse wow fadeInDown">
                        <svg class="stroke" height="78" width="78">
                    <circle cx="38" cy="38" r="36" stroke="#ffffff" stroke-width="1" fill="none" ;></circle>
                </svg>
                        <div class="mouse_icon fadeIn animated"><img src="themes/LifeHouse/img/welcom/042021/mouse.png" width="25" height="36"></div>
                        <div class="arrow arrow_ani" style="opacity: 1;"><img src="themes/LifeHouse/img/welcom/042021/arrow.svg" width="10" height="17"></div>
                        <div class="mouse_line" style="height: 57px;"></div>
                        <div class="mouse_line2" style="height: 61px;"></div>
                    </div>
                </div>


            </section>


            <!--  黑色陰影 -->


            <div class="gray453"> </div>
            <div data-wow-delay="0.1s" class="news wow fadeIn ">
                <div class="title flex ">
                    <h1 data-wow-delay="0.2s" class="wow fadeInDown ">最新訊息</h1>
                    <h2 data-wow-delay="0.3s" class="wow fadeInDown ">Event message</h2>
                    <div data-wow-delay="0.35s" class="line2 wow fadeInDown "></div>
                </div>
                <div class="new_boder">
                    <a data-wow-delay="0.4s" class="banner wow fadeInUp" href="https:/okrent.house/joinlife" target="_blank">
                        <div class="black flex">
                            <p>多份工作<br /> 不如多份收入
                            </p>
                        </div>
                        <img class="banner_img " src="themes/LifeHouse/img/welcom/042021/banner1@2x.jpg">

                    </a>
                    <a data-wow-delay="0.5s" class="banner wow fadeInDown" href="https:/okrent.house/tenant" target="_blank">
                        <div class="black flex">
                            <p>租約健檢 <br /> 免費諮詢
                            </p>
                        </div>
                        <img class="banner_img " src="themes/LifeHouse/img/welcom/042021/banner2@2x.jpg">

                    </a>
                    <a data-wow-delay="0.6s" class="banner wow fadeInUp" href="https:/okrent.house/goodstore" target="_blank">
                        <div class="black flex">
                            <p>生活好康特約商店<br /> 店家熱力招募中 </p>
                        </div>
                        <img class="banner_img " src="themes/LifeHouse/img/welcom/042021/banner3@2x.jpg">

                    </a>
                    <a data-wow-delay="0.7s" class="banner wow fadeInDown" href="https:/okrent.house/authenticate_main" target="_blank">
                        <div class="black flex">
                            <p>超立淨防疫神器<br /> 分享會
                            </p>
                        </div>
                        <img class="banner_img " src="themes/LifeHouse/img/welcom/042021/clean@2x.jpg">

                    </a>
                    <a data-wow-delay="0.8s" class="banner wow fadeInUp" href="https:/okmaster.life/sterilization" target="_blank">
                        <div class="black flex">
                            <p>
                                租賃信用認證<br /> 有認證才安全
                            </p>
                        </div>
                        <img class="banner_img " src="themes/LifeHouse/img/welcom/042021/credit@2x.jpg">

                    </a>
                </div>
            </div>

            <div class="title flex ">
                <h1 data-wow-delay="0.2s" class="wow fadeInDown ">友好連結</h1>
                <h2 data-wow-delay="0.3s" class="wow fadeInDown ">Related Link</h2>
                <div data-wow-delay="0.4s" class="line2 wow fadeInDown "></div>
            </div>
            <section class="RelatedLink owl-carousel owl-theme" id="owl-demo">

                <div class="item">
                    <a href="https://www.facebook.com/桃園市租任管理服務業從業人員職業工會-600320820503706" target="_blank">
                        <img data-wow-delay="0.8s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_01.svg" alt="桃園市租任管理服務業從業人員職業工會">
                    </a>
                </div>
                <div class="item">
                    <a href="https://www.cathay-ins.com.tw/insurance" target="_blank">
                        <img data-wow-delay="0.9s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_02.svg" alt="國泰產險">
                    </a>
                </div>
                <div class="item">
                    <a href="http://www.tycrehda.org.tw/" target="_blank">
                        <img data-wow-delay="1s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_03.svg" alt="桃園市樂安居住宅發展促進協會">
                    </a>
                </div>
                <div class="item">
                    <a href="http://www.pcss.com.tw/" target="_blank">
                        <img data-wow-delay="1.1s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_04.svg" alt="保成務業保全">
                    </a>
                </div>
                <div class="item">
                    <a href="https://www.hoan.com.tw/" target="_blank">
                        <img data-wow-delay="1.2s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_05.svg" alt="和安保險">
                    </a>
                </div>
                <div class="item">
                    <a href="http://www.da-i.com.tw/" target="_blank">
                        <img data-wow-delay="1.3s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_06.svg" alt="大愛搬家">
                    </a>
                </div>
                <div class="item">
                    <a href="https://www.hoan.com.tw/" target="_blank">
                        <img data-wow-delay="1.4s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_07.svg" alt="僑馥建經">
                    </a>
                </div>
                <div class="item">
                    <a href="http://www.tbm.com.tw/" target="_blank">
                        <img data-wow-delay="1.5s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_08.svg" alt="桃園市公寓大廈管理維護商業公會">
                    </a>
                </div>
                <div class="item">
                    <a href="http://lawyeratticus.com/" target="_blank">
                        <img data-wow-delay="1.6s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_09.svg" alt="鄭三川律師">
                    </a>
                </div>
                <div class="item">
                    <a href="#" target="_blank">
                        <img data-wow-delay="1.7s" class="wow fadeInDown" src="themes/LifeHouse/img/welcom/042021/welcome_company_10.svg" alt="生活好康">
                    </a>
                </div>
            </section>
            <footer data-wow-delay="0.5s" class="wow fadeInUp">生活資產管理股份有限公司 Life Property Investment Management CO.,LTD. <br/> 生活集團版權所有 Copyright © Life Group 2000 ALL right reserde
            </footer>
<?php echo $_smarty_tpl->tpl_vars['js_0419']->value;?>

<?php }
}
