<?php
/* Smarty version 3.1.28, created on 2021-05-04 10:08:20
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/landlord_history.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6090ac94599835_62458596',
  'file_dependency' => 
  array (
    '1559439b7e3f0a15ac09cac94e3bc82b16590464' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/landlord_history.tpl',
      1 => 1620094095,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6090ac94599835_62458596 ($_smarty_tpl) {
?>
<div class="panel panel-default">
        <div class="panel-heading" <?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
 onclick="collapse_data(this)"><?php echo $_smarty_tpl->tpl_vars['form']->value['title'];?>
(點選收合)
        </div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">
            <?php
$_from = $_smarty_tpl->tpl_vars['landlord_history']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
            <table width="100%" border="1">
                <tr>
                    <td class="main_t_2 input-group-addon">房東名字:</td>
                    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_name'];?>
" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">手機:</td>
                    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_user'];?>
" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">備用手機:</td>
                    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_tel'];?>
" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">房東英文:</td>
                    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_en_name'];?>
" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">稱謂:</td>
                    <td>
                    <?php if ($_smarty_tpl->tpl_vars['v']->value['member_landlord_appellation'] == "0") {?>
                    <input type="text"  value="先生" size="10" disabled="disabled">
                    <?php } else { ?>
                    <input type="text"  value="小姐" size="10" disabled="disabled">
                    <?php }?>
                    </td>
                </tr>
                <tr>
                <td class="main_t_2 input-group-addon" style="color:red">是否黑名單:</td>
                <td><input type="checkbox"  value="0" size="10" <?php if ($_smarty_tpl->tpl_vars['v']->value['blacktype'] == "1") {?> checked <?php }?> onclick="return false;"></td>
                <td class="main_t_2 input-group-addon">原因:</td>
                <td><input type="text" id="black_reason" name="black_reason" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['blackreason'];?>
" style="width:100%;"></td>
                <td class="main_t_2 input-group-addon">紀錄時間:</td>
                <td><input type="date" id="recodeTime" name="recodeTime" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['recodeTime'];?>
"></td>
                <td class="main_t_2 input-group-addon">紀錄人:</td>
                <td><input type="text" id="black_cp" name="black_cp" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['create_people'];?>
" style="width:100%;" readonly></td>
                </tr>
                <tr>
                  <td class="main_t_2 input-group-addon">來源:</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_id_source'];?>
" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">生日:</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_birthday'];?>
" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">ID:</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_identity'];?>
" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">Line ID:</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_line_id'];?>
" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">Email:</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_email'];?>
" size="10" disabled="disabled"></td>
                </tr>
                <tr>
                  <td class="main_t_2 input-group-addon">房東備註</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_landlord_details'];?>
" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">市話1</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_local_tel_1'];?>
" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">市話2:</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_local_tel_2'];?>
" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">更新日期:</td>
                  <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_active_off_date_0'];?>
" size="10" disabled="disabled"></td>
                  <td><span class="main_t_2 input-group-addon">原因:</span></td>
                    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_active_off_reason_0'];?>
" size="10" disabled="disabled"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><span class="main_t_2 input-group-addon">更新者:</span></td>
                    <td><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value['member_landlord_active_off_admin_0'];?>
" size="10" disabled="disabled"></td>
                </tr>
                </table>
            <hr>
            <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
            </div>
</div>
</div><?php }
}
