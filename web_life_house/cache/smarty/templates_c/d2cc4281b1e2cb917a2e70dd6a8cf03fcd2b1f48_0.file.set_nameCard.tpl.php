<?php
/* Smarty version 3.1.28, created on 2021-03-31 11:39:25
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/set_nameCard.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6063eeed6837b4_89717489',
  'file_dependency' => 
  array (
    'd2cc4281b1e2cb917a2e70dd6a8cf03fcd2b1f48' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/set_nameCard.tpl',
      1 => 1612154099,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063eeed6837b4_89717489 ($_smarty_tpl) {
?>
<div style="width: 650px;">
<img style="width: 150px;padding: 0px;" src="https://www.okrent.house/themes/Rent/img/logo_main.svg"><br>
<lebel style="font-size: 14pt;font-weight: bold;">生活資產管理股份有限公司</lebel><br>
<lebel>Life Property Investment Management CO.,LTD.</lebel><br>
<lebel style="font-size: 12pt;color: #4b1c89;font-weight: bold;"><?php if ($_smarty_tpl->tpl_vars['form']->value['input']['ag_title_zh']['val']) {
echo $_smarty_tpl->tpl_vars['form']->value['input']['ag_title_zh']['val'];
} else {
echo $_smarty_tpl->tpl_vars['form']->value['input']['ag_title_en']['val'];
}?> <span style="text-decoration: underline;"><?php if ($_smarty_tpl->tpl_vars['form']->value['input']['last_name_en']['val']) {
echo $_smarty_tpl->tpl_vars['form']->value['input']['first_name_en']['val'];?>
 <?php echo $_smarty_tpl->tpl_vars['form']->value['input']['last_name_en']['val'];
} else {
echo $_smarty_tpl->tpl_vars['form']->value['input']['last_name']['val'];
echo $_smarty_tpl->tpl_vars['form']->value['input']['first_name']['val'];
}?></span></lebel><br>
<lebel style="font-weight: bold;">電話：<a href="tel:033581095">(03)358-1095</a><br>
<lebel style="font-weight: bold;">傳真：</lebel>(03)358-1095<br>
<lebel style="font-weight: bold;">地址：</lebel><a href="https://www.google.com/maps?ll=25.021273,121.298146&amp;z=18&amp;t=m&amp;hl=zh-TW&amp;gl=TW&amp;mapclient=embed&amp;cid=14711057115237095818">330-71桃園市桃園區新埔八街113號</a><br>
<lebel style="font-weight: bold;">Email：</lebel><a href="mail:okrent@lifegroup.house">okrent@lifegroup.house</a><br>
<lebel style="font-weight: bold;">FaceBook：</lebel><a href="https://www.facebook.com/211268312718567">生活房屋～ 桃園 託租 代管 租屋專家</a><br>
<lebel style="font-weight: bold;">網站：</lebel><a href="https://www.lifegroup.house">www.lifegroup.house</a><br></lebel><br>
</div>


<textarea style="margin-top: 2em; height: 20em;width: 100%;" aria-invalid="false">
  <div style="width: 650px;">
  <img style="width: 150px;padding: 0px;" src="https://www.okrent.house/themes/Rent/img/logo_main.svg"><br>
  <lebel style="font-size: 14pt;font-weight: bold;">生活資產管理股份有限公司</lebel><br>
  <lebel>Life Property Investment Management CO.,LTD.</lebel><br>
  <lebel style="font-size: 12pt;color: #4b1c89;font-weight: bold;"><?php if ($_smarty_tpl->tpl_vars['form']->value['input']['ag_title_zh']['val']) {
echo $_smarty_tpl->tpl_vars['form']->value['input']['ag_title_zh']['val'];
} else {
echo $_smarty_tpl->tpl_vars['form']->value['input']['ag_title_en']['val'];
}?> <span style="text-decoration: underline;"><?php if ($_smarty_tpl->tpl_vars['form']->value['input']['last_name_en']['val']) {
echo $_smarty_tpl->tpl_vars['form']->value['input']['first_name_en']['val'];?>
 <?php echo $_smarty_tpl->tpl_vars['form']->value['input']['last_name_en']['val'];
} else {
echo $_smarty_tpl->tpl_vars['form']->value['input']['last_name']['val'];
echo $_smarty_tpl->tpl_vars['form']->value['input']['first_name']['val'];
}?></span></lebel><br>
  <lebel style="font-weight: bold;">電話：<a href="tel:033581095">(03)358-1095</a><br>
  <lebel style="font-weight: bold;">傳真：</lebel>(03)358-1095<br>
  <lebel style="font-weight: bold;">地址：</lebel><a href="https://www.google.com/maps?ll=25.021273,121.298146&amp;z=18&amp;t=m&amp;hl=zh-TW&amp;gl=TW&amp;mapclient=embed&amp;cid=14711057115237095818">330-71桃園市桃園區新埔八街113號</a><br>
  <lebel style="font-weight: bold;">Email：</lebel><a href="mail:okrent@lifegroup.house">okrent@lifegroup.house</a><br>
  <lebel style="font-weight: bold;">FaceBook：</lebel><a href="https://www.facebook.com/211268312718567">生活房屋～ 桃園 託租 代管 租屋專家</a><br>
  <lebel style="font-weight: bold;">網站：</lebel><a href="https://www.lifegroup.house">www.lifegroup.house</a><br></lebel><br>
  </div>
</textarea>
<?php }
}
