<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:41:27
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599ba78de8e5_85346303',
  'file_dependency' => 
  array (
    '058a7ca7a89ff0d8175f705e103c0733198b3977' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/footer.tpl',
      1 => 1616153621,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../../../modules/DesigningFooter/designingfooter.tpl' => 1,
  ),
),false)) {
function content_60599ba78de8e5_85346303 ($_smarty_tpl) {
?>

      </article>
      <?php if ($_smarty_tpl->tpl_vars['display_footer']->value) {?>
            <footer>
                  <div class="footer" style="background-color:#98027f">
                        <table>

                              <tr>
                                    <td>
                                          <div class="footer_txt1"><span>生活資產管理股份有限公司</span> Life Property Investment Management CO.,LTD.</div>
                                          <div class="footer_txt2"><span>生活集團版權所有</span> Copyright © Life Group 2000 ALL right reserde</div>
                                          <div colspan="0" rowspan="0" style="vertical-align: middle;color: #fff;position: absolute;right: 23%;top: 28%;">
                                              <a href="/statement" style="color: #fff;font-size: 8pt;">相關聲明</a>
                                          </div>
                                    </td>

                              </tr>
                        </table>
                  </div>
                  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../../../modules/DesigningFooter/designingfooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </footer>
      <?php }?>
      </div>
</body>
</html>
<?php }
}
