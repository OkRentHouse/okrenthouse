<?php
/* Smarty version 3.1.28, created on 2021-03-23 15:41:27
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/breadcrumbs.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60599ba785f419_90706387',
  'file_dependency' => 
  array (
    'd6acc2711e8ba586bc5a3545b58a5ff1214ffb85' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/breadcrumbs.tpl',
      1 => 1607678485,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60599ba785f419_90706387 ($_smarty_tpl) {
?>
<ol class="breadcrumb">
	<?php
$_from = $_smarty_tpl->tpl_vars['breadcrumbs2']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_breadcrumbs2_val_0_saved_item = isset($_smarty_tpl->tpl_vars['breadcrumbs2_val']) ? $_smarty_tpl->tpl_vars['breadcrumbs2_val'] : false;
$__foreach_breadcrumbs2_val_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['breadcrumbs2_val'] = new Smarty_Variable();
$__foreach_breadcrumbs2_val_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_breadcrumbs2_val_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['breadcrumbs2_val']->value) {
$__foreach_breadcrumbs2_val_0_saved_local_item = $_smarty_tpl->tpl_vars['breadcrumbs2_val'];
?><li class="breadcrumb-item<?php if (count($_smarty_tpl->tpl_vars['breadcrumbs2']->value) == $_smarty_tpl->tpl_vars['i']->value+1) {?> active<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['breadcrumbs2_val']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['breadcrumbs2_val']->value['title'];?>
</a></li><?php
$_smarty_tpl->tpl_vars['breadcrumbs2_val'] = $__foreach_breadcrumbs2_val_0_saved_local_item;
}
}
if ($__foreach_breadcrumbs2_val_0_saved_item) {
$_smarty_tpl->tpl_vars['breadcrumbs2_val'] = $__foreach_breadcrumbs2_val_0_saved_item;
}
if ($__foreach_breadcrumbs2_val_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_breadcrumbs2_val_0_saved_key;
}
?>
</ol><?php }
}
