<?php
/* Smarty version 3.1.28, created on 2021-03-23 16:28:53
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/cms.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6059a6c50c50d5_82026267',
  'file_dependency' => 
  array (
    '8b59c653812030a6210f1bc63495a501cf551069' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/cms.tpl',
      1 => 1607678485,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6059a6c50c50d5_82026267 ($_smarty_tpl) {
?>
<div class="cms<?php if ($_smarty_tpl->tpl_vars['display_header']->value) {?> cms_header<?php }
if ($_smarty_tpl->tpl_vars['display_footer']->value) {?> cms_foote<?php }?>"><?php echo $_smarty_tpl->tpl_vars['html']->value;?>
</div>
<?php if (!empty($_smarty_tpl->tpl_vars['cms_css']->value)) {?>
	<style>
		<?php echo $_smarty_tpl->tpl_vars['cms_css']->value;?>

	</style>
<?php }
if (!empty($_smarty_tpl->tpl_vars['cms_js']->value)) {?>
	<?php echo '<script'; ?>
>
        <?php echo $_smarty_tpl->tpl_vars['cms_js']->value;?>

	<?php echo '</script'; ?>
>
<?php }
}
}
