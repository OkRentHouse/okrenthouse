<?php
/* Smarty version 3.1.28, created on 2021-05-06 10:26:25
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_609353d11ebeb1_92378725',
  'file_dependency' => 
  array (
    '1299f5a0101c9c4a1f0f34c4dd1d42e69f38a7e6' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_list.tpl',
      1 => 1620267975,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_609353d11ebeb1_92378725 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['form_list_pagination']->value;?>

<?php echo $_smarty_tpl->tpl_vars['form_list_field_list']->value;
if (count($_smarty_tpl->tpl_vars['fields']->value['list']) > 0) {?><form action="" method="get" class="list col-lg-12" accept-charset="utf-8"><?php }?>
<div class="panel panel-default">
	<?php if ($_smarty_tpl->tpl_vars['fields']->value['list_heading_display']) {?>
		
		<?php if (isset($_smarty_tpl->tpl_vars['fields']->value['show_list_num']) && !empty($_smarty_tpl->tpl_vars['fields']->value['show_list_num'])) {
}?>
		<div class="panel-heading">
			<?php echo $_smarty_tpl->tpl_vars['fields']->value['title'];?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<span class=""><?php echo l(array('s'=>"顯示 | "),$_smarty_tpl);?>
</span><input style="width:70px;" type="number" min="0" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_m" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_m" class="input-sm" value="<?php echo $_smarty_tpl->tpl_vars['fields']->value['list_num'];?>
" title="<?php echo l(array('s'=>"顯示幾筆資料"),$_smarty_tpl);?>
" placeholder="<?php echo l(array('s'=>"50"),$_smarty_tpl);?>
"><span class=""><?php echo l(array('s'=>"筆資料"),$_smarty_tpl);
echo l(array('s'=>",共"),$_smarty_tpl);?>
<samp><?php echo $_smarty_tpl->tpl_vars['table_num']->value;?>
</samp><?php echo l(array('s'=>"筆"),$_smarty_tpl);?>

		</div>
	<?php }?>
      <?php if ($_smarty_tpl->tpl_vars['fields']->value['list_body_display']) {?>
	<div class="panel-body">
      <table class="table table-bordered"><?php echo $_smarty_tpl->tpl_vars['form_list_thead']->value;
echo $_smarty_tpl->tpl_vars['form_list_tbody']->value;
echo $_smarty_tpl->tpl_vars['form_list_tfoot']->value;?>
</table>
			<?php if ($_smarty_tpl->tpl_vars['fields']->value['list_footer_include_tpl']) {?><div class="table table-bordered"><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, $_smarty_tpl->tpl_vars['fields']->value['list_footer_include_tpl'], $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
</div><?php }?> 
	</div>
	<?php }?>
      <?php if ($_smarty_tpl->tpl_vars['fields']->value['list_footer_display']) {?><div class="panel-footer"><?php echo $_smarty_tpl->tpl_vars['fields']->value['list_footer'];?>
</div><?php }?>
</div>
<?php echo $_smarty_tpl->tpl_vars['form_list_pagination']->value;?>

<input type="hidden" name="p" value="<?php echo $_GET['p'];?>
" />
<input type="hidden" name="m" value="<?php echo $_GET['m'];?>
" />
<?php if (count($_smarty_tpl->tpl_vars['fields']->value['list']) > 0) {?></form><?php }
}
}
