<?php
/* Smarty version 3.1.28, created on 2021-04-07 10:47:04
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Menu/page.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_606d1d28f24071_91282494',
  'file_dependency' => 
  array (
    'e2cc70ec40d65ac66e1ccf095328e010f6e81d0e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Menu/page.tpl',
      1 => 1592288973,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606d1d28f24071_91282494 ($_smarty_tpl) {
?>
<div class="list col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				網站選單
			</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<thead>
					<tr><th class="text-center">網站名稱<div class="th_title">網站名稱</div></th><th class="text-center">Menu Code<div class="th_title">Menu Code</div></th></tr>
					</thead>
					<tbody>
					<?php
$_from = $_smarty_tpl->tpl_vars['arr_web']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_web_0_saved_item = isset($_smarty_tpl->tpl_vars['web']) ? $_smarty_tpl->tpl_vars['web'] : false;
$__foreach_web_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['web'] = new Smarty_Variable();
$__foreach_web_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_web_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['web']->value) {
$__foreach_web_0_saved_local_item = $_smarty_tpl->tpl_vars['web'];
?>
						<tr id="tr_<?php echo $_smarty_tpl->tpl_vars['web']->value['id_menu_code'];?>
" onclick="document.location='/manage/Menu?&id_menu_code=<?php echo $_smarty_tpl->tpl_vars['web']->value['id_menu_code'];?>
'"><td class="text-center"><?php echo $_smarty_tpl->tpl_vars['web']->value['web'];?>
</td><td class="text-center"><?php echo $_smarty_tpl->tpl_vars['web']->value['code'];?>
</td></tr>
					<?php
$_smarty_tpl->tpl_vars['web'] = $__foreach_web_0_saved_local_item;
}
}
if ($__foreach_web_0_saved_item) {
$_smarty_tpl->tpl_vars['web'] = $__foreach_web_0_saved_item;
}
if ($__foreach_web_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_web_0_saved_key;
}
?>
					</tbody>
					<tfoot></tfoot>
				</table>
			</div>
		</div>
</div><?php }
}
