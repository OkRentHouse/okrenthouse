<?php
/* Smarty version 3.1.28, created on 2021-03-31 16:37:12
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_form.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_606434b8008780_89758103',
  'file_dependency' => 
  array (
    '782e5029598a57e322b75b6cd4ae7da482cfcb38' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_form.tpl',
      1 => 1617179830,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606434b8008780_89758103 ($_smarty_tpl) {
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "defaultTab", array (
  0 => 'block_1897345877606434b7e2cc34_02534327',
  1 => false,
  3 => 0,
  2 => 0,
));
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "defaultForm", array (
  0 => 'block_196836327606434b7e36000_54593432',
  1 => false,
  3 => 0,
  2 => 0,
));
?>

<?php }
/* {block 'defaultTab'}  file:form/form_form.tpl */
function block_1897345877606434b7e2cc34_02534327($_smarty_tpl, $_blockParentStack) {
if (isset($_smarty_tpl->tpl_vars['fields']->value['tabs'])) {?><div class="count_tabs col-lg-2<?php if (isset($_smarty_tpl->tpl_vars['fields']->value['tabs']['class'])) {
echo $_smarty_tpl->tpl_vars['fields']->value['tabs']['class'];
}?>"><ul class="list-group"><?php
$_from = $_smarty_tpl->tpl_vars['fields']->value['tabs'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tab_title_0_saved_item = isset($_smarty_tpl->tpl_vars['tab_title']) ? $_smarty_tpl->tpl_vars['tab_title'] : false;
$__foreach_tab_title_0_saved_key = isset($_smarty_tpl->tpl_vars['link_id']) ? $_smarty_tpl->tpl_vars['link_id'] : false;
$_smarty_tpl->tpl_vars['tab_title'] = new Smarty_Variable();
$__foreach_tab_title_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_tab_title_0_total) {
$_smarty_tpl->tpl_vars['link_id'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['link_id']->value => $_smarty_tpl->tpl_vars['tab_title']->value) {
$__foreach_tab_title_0_saved_local_item = $_smarty_tpl->tpl_vars['tab_title'];
if ($_smarty_tpl->tpl_vars['link_id']->value != 'class') {?><li id="tabs_<?php echo $_smarty_tpl->tpl_vars['link_id']->value;?>
" class="list-group-item"><?php echo $_smarty_tpl->tpl_vars['tab_title']->value;?>
</li><?php }
$_smarty_tpl->tpl_vars['tab_title'] = $__foreach_tab_title_0_saved_local_item;
}
}
if ($__foreach_tab_title_0_saved_item) {
$_smarty_tpl->tpl_vars['tab_title'] = $__foreach_tab_title_0_saved_item;
}
if ($__foreach_tab_title_0_saved_key) {
$_smarty_tpl->tpl_vars['link_id'] = $__foreach_tab_title_0_saved_key;
}
?></ul></div><?php }
}
/* {/block 'defaultTab'} */
/* {block 'legend'}  file:form/form_form.tpl */
function block_105812057606434b7e43880_27101297($_smarty_tpl, $_blockParentStack) {
$_smarty_tpl->tpl_vars['body_display'] = new Smarty_Variable(true, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'body_display', 0);
if ($_smarty_tpl->tpl_vars['form']->value['heading_display']) {?><div class="panel-heading"><?php if (isset($_smarty_tpl->tpl_vars['form_v']->value['image']) && isset($_smarty_tpl->tpl_vars['field']->value['title'])) {?><img src="<?php echo $_smarty_tpl->tpl_vars['form_v']->value['image'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_v']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" /><?php }
if (isset($_smarty_tpl->tpl_vars['form_v']->value['icon'])) {?><i class="<?php echo $_smarty_tpl->tpl_vars['form_v']->value['icon'];?>
"></i><?php }
echo $_smarty_tpl->tpl_vars['form_v']->value['title'];?>
</div><?php }?><div class="panel-body"><?php
}
/* {/block 'legend'} */
/* {block 'label'}  file:form/form_form.tpl */
function block_2134848807606434b7e54562_38202581($_smarty_tpl, $_blockParentStack) {
if (isset($_smarty_tpl->tpl_vars['input']->value['label']) && !$_smarty_tpl->tpl_vars['input']->value['no_label']) {?><label class="main_t control-label col-lg-<?php if (isset($_smarty_tpl->tpl_vars['input']->value['label_col'])) {
echo $_smarty_tpl->tpl_vars['input']->value['label_col'];
} else { ?>3<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['label_class']) && $_smarty_tpl->tpl_vars['input']->value['label_class']) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['label_class'];
}
if (isset($_smarty_tpl->tpl_vars['input']->value['required']) && $_smarty_tpl->tpl_vars['input']->value['required'] && $_smarty_tpl->tpl_vars['input']->value['type'] != 'radio') {?> required<?php }?>"for="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"><?php if (isset($_smarty_tpl->tpl_vars['input']->value['hint'])) {?><span class="main_t label-tooltip" data-toggle="tooltip" data-html="true" title="<?php if (is_array($_smarty_tpl->tpl_vars['input']->value['hint'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['hint'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_hint_4_saved_item = isset($_smarty_tpl->tpl_vars['hint']) ? $_smarty_tpl->tpl_vars['hint'] : false;
$_smarty_tpl->tpl_vars['hint'] = new Smarty_Variable();
$__foreach_hint_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_hint_4_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['hint']->value) {
$__foreach_hint_4_saved_local_item = $_smarty_tpl->tpl_vars['hint'];
if (is_array($_smarty_tpl->tpl_vars['hint']->value)) {
echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['hint']->value['text']);
} else {
echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['hint']->value);
}
$_smarty_tpl->tpl_vars['hint'] = $__foreach_hint_4_saved_local_item;
}
}
if ($__foreach_hint_4_saved_item) {
$_smarty_tpl->tpl_vars['hint'] = $__foreach_hint_4_saved_item;
}
} else {
echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['input']->value['hint']);
}?>"><?php }
echo $_smarty_tpl->tpl_vars['input']->value['label'];
if (isset($_smarty_tpl->tpl_vars['input']->value['hint'])) {?></span><?php }?></label><?php }
}
/* {/block 'label'} */
/* {block 'field'}  file:form/form_form.tpl */
function block_979613148606434b7e5c611_70138120($_smarty_tpl, $_blockParentStack) {
if (($_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix']) || (!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix'])) {?><div class="col-lg-<?php if (isset($_smarty_tpl->tpl_vars['input']->value['col'])) {
echo intval($_smarty_tpl->tpl_vars['input']->value['col']);
} else { ?>3<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['div_class'])) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['div_class'];
}
if (!isset($_smarty_tpl->tpl_vars['input']->value['label']) && !$_smarty_tpl->tpl_vars['input']->value['no_label']) {?> col-lg-offset-3<?php }?> <?php echo $_smarty_tpl->tpl_vars['input']->value['col_class'];?>
"><?php }
if (((isset($_smarty_tpl->tpl_vars['input']->value['prefix']) || isset($_smarty_tpl->tpl_vars['input']->value['suffix']) || isset($_smarty_tpl->tpl_vars['input']->value['prefix_btn']) || isset($_smarty_tpl->tpl_vars['input']->value['suffix_btn'])) && (!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix'])) || ($_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix'])) {?><div class="input-group fixed-width-lg"><?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['prefix'])) {?><span class="main_t_2 input-group-addon<?php if (isset($_smarty_tpl->tpl_vars['input']->value['prefix_class'])) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['prefix_class'];
}?>"><?php echo $_smarty_tpl->tpl_vars['input']->value['prefix'];?>
</span><?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['prefix_btn'])) {?><div class="input-group-btn"><?php
$_from = $_smarty_tpl->tpl_vars['input']->value['prefix_btn'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_p_btn_5_saved_item = isset($_smarty_tpl->tpl_vars['p_btn']) ? $_smarty_tpl->tpl_vars['p_btn'] : false;
$_smarty_tpl->tpl_vars['p_btn'] = new Smarty_Variable();
$__foreach_p_btn_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_p_btn_5_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['p_btn']->value) {
$__foreach_p_btn_5_saved_local_item = $_smarty_tpl->tpl_vars['p_btn'];
?><a class="btn btn-default <?php echo $_smarty_tpl->tpl_vars['p_btn']->value['class'];?>
" href="<?php if ($_smarty_tpl->tpl_vars['p_btn']->value['href']) {
echo $_smarty_tpl->tpl_vars['p_btn']->value['href'];
} else { ?>javascript:void(0);<?php }?>"><span class="<?php echo $_smarty_tpl->tpl_vars['p_btn']->value['icon'];?>
"></span><?php echo $_smarty_tpl->tpl_vars['p_btn']->value['title'];?>
</a><?php
$_smarty_tpl->tpl_vars['p_btn'] = $__foreach_p_btn_5_saved_local_item;
}
}
if ($__foreach_p_btn_5_saved_item) {
$_smarty_tpl->tpl_vars['p_btn'] = $__foreach_p_btn_5_saved_item;
}
?></div><?php }
if ($_smarty_tpl->tpl_vars['input']->value['type'] == 'tpl') {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['file'], $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
} elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'html') {
echo $_smarty_tpl->tpl_vars['input']->value['data'];
} elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'text' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'number' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'color' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'email' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'tel' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'url' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'date' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'datetime' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'datetime-local' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'time') {?><input type="<?php echo $_smarty_tpl->tpl_vars['input']->value['type'];?>
"id="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>"class="form-control <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"value="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
echo htmlspecialchars(sprintf($_smarty_tpl->tpl_vars['input']->value['string_format'],$_smarty_tpl->tpl_vars['input']->value['val']), ENT_QUOTES, 'UTF-8', true);
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['val'], ENT_QUOTES, 'UTF-8', true);
}?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['size'])) {?> size="<?php echo $_smarty_tpl->tpl_vars['input']->value['size'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['min']) && ($_smarty_tpl->tpl_vars['input']->value['min'] || $_smarty_tpl->tpl_vars['input']->value['min'] == 0)) {?> min="<?php echo $_smarty_tpl->tpl_vars['input']->value['min'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['max'])) {?> max="<?php echo $_smarty_tpl->tpl_vars['input']->value['max'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxchar']) && $_smarty_tpl->tpl_vars['input']->value['maxchar']) {?> data-maxchar="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxchar']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxlength']) && $_smarty_tpl->tpl_vars['input']->value['maxlength']) {?> maxlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['minlength']) && $_smarty_tpl->tpl_vars['input']->value['minlength']) {?> minlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['minlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['readonly']) && $_smarty_tpl->tpl_vars['input']->value['readonly']) {?> readonly="readonly"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['autocomplete']) && !$_smarty_tpl->tpl_vars['input']->value['autocomplete']) {?> autocomplete="off"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['required']) && $_smarty_tpl->tpl_vars['input']->value['required']) {?> required="required" <?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['placeholder']) && $_smarty_tpl->tpl_vars['input']->value['placeholder']) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['input']->value['placeholder'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['autocorrect']) && $_smarty_tpl->tpl_vars['input']->value['autocorrect']) {?> autocorrect="<?php echo $_smarty_tpl->tpl_vars['input']->value['autocorrect'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_6_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_6_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_6_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_6_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_6_saved_local_item;
}
}
if ($__foreach_input_data_6_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_6_saved_item;
}
if ($__foreach_input_data_6_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_6_saved_key;
}
}
if (isset($_smarty_tpl->tpl_vars['input']->value['step'])) {?> step="<?php echo $_smarty_tpl->tpl_vars['input']->value['step'];?>
"<?php }?>/><?php if ($_smarty_tpl->tpl_vars['input']->value['button']) {
echo $_smarty_tpl->tpl_vars['input']->value['button'];
}
} elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'select') {?><select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>"class="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['class'], ENT_QUOTES, 'utf-8', true);
}
if ($_smarty_tpl->tpl_vars['input']->value['live_search']) {?> selectpicker<?php }?> form-control"id="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['id'], ENT_QUOTES, 'utf-8', true);
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);
}?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?> multiple="multiple"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['size'])) {?> size="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['size'], ENT_QUOTES, 'utf-8', true);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['onchange'])) {?> onchange="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['onchange'], ENT_QUOTES, 'utf-8', true);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['readonly']) && $_smarty_tpl->tpl_vars['input']->value['readonly']) {?> readonly="readonly"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['js']) && $_smarty_tpl->tpl_vars['input']->value['js']) {?> onchange="<?php echo $_smarty_tpl->tpl_vars['input']->value['js'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['required']) && $_smarty_tpl->tpl_vars['input']->value['required']) {?> required="required"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['live_search']) && $_smarty_tpl->tpl_vars['input']->value['live_search']) {?> data-live-search="true"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_7_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_7_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_7_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_7_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_7_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_7_saved_local_item;
}
}
if ($__foreach_input_data_7_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_7_saved_item;
}
if ($__foreach_input_data_7_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_7_saved_key;
}
}?>><?php if (isset($_smarty_tpl->tpl_vars['input']->value['options']['default'])) {?><option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['options']['default']['val'], ENT_QUOTES, 'utf-8', true);?>
"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_string_format_8_saved_item = isset($_smarty_tpl->tpl_vars['input_string_format']) ? $_smarty_tpl->tpl_vars['input_string_format'] : false;
$_smarty_tpl->tpl_vars['input_string_format'] = new Smarty_Variable();
$__foreach_input_string_format_8_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_string_format_8_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['input_string_format']->value) {
$__foreach_input_string_format_8_saved_local_item = $_smarty_tpl->tpl_vars['input_string_format'];
if ($_smarty_tpl->tpl_vars['input_string_format']->value == $_smarty_tpl->tpl_vars['input']->value['options']['default']['val']) {?> selected="selected"<?php }
$_smarty_tpl->tpl_vars['input_string_format'] = $__foreach_input_string_format_8_saved_local_item;
}
}
if ($__foreach_input_string_format_8_saved_item) {
$_smarty_tpl->tpl_vars['input_string_format'] = $__foreach_input_string_format_8_saved_item;
}
} else {
if ($_smarty_tpl->tpl_vars['input']->value['string_format'] == $_smarty_tpl->tpl_vars['input']->value['options']['default']['val']) {?> selected="selected"<?php }
}
} elseif (isset($_smarty_tpl->tpl_vars['input']->value['val'])) {
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_val_9_saved_item = isset($_smarty_tpl->tpl_vars['input_val']) ? $_smarty_tpl->tpl_vars['input_val'] : false;
$_smarty_tpl->tpl_vars['input_val'] = new Smarty_Variable();
$__foreach_input_val_9_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_val_9_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['input_val']->value) {
$__foreach_input_val_9_saved_local_item = $_smarty_tpl->tpl_vars['input_val'];
if ($_smarty_tpl->tpl_vars['input_val']->value == $_smarty_tpl->tpl_vars['input']->value['options']['default']['val']) {?> selected="selected"<?php }
$_smarty_tpl->tpl_vars['input_val'] = $__foreach_input_val_9_saved_local_item;
}
}
if ($__foreach_input_val_9_saved_item) {
$_smarty_tpl->tpl_vars['input_val'] = $__foreach_input_val_9_saved_item;
}
} else {
if ($_smarty_tpl->tpl_vars['input']->value['val'] == $_smarty_tpl->tpl_vars['input']->value['options']['default']['val']) {?> selected="selected"<?php }
}
}?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['options']['default']['text'], ENT_QUOTES, 'utf-8', true);?>
</option><?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['options']['val'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['options']['val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_option_10_saved_item = isset($_smarty_tpl->tpl_vars['option']) ? $_smarty_tpl->tpl_vars['option'] : false;
$_smarty_tpl->tpl_vars['option'] = new Smarty_Variable();
$__foreach_option_10_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_option_10_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['option']->value) {
$__foreach_option_10_saved_local_item = $_smarty_tpl->tpl_vars['option'];
?><option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['val'], ENT_QUOTES, 'utf-8', true);?>
"<?php if (isset($_smarty_tpl->tpl_vars['option']->value['parent'])) {?> data-parent="<?php echo $_smarty_tpl->tpl_vars['option']->value['parent'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['option']->value['parent_name'])) {?> data-parent_name="<?php echo $_smarty_tpl->tpl_vars['option']->value['parent_name'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['option']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['option']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_option_data_11_saved_item = isset($_smarty_tpl->tpl_vars['option_data']) ? $_smarty_tpl->tpl_vars['option_data'] : false;
$__foreach_option_data_11_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['option_data'] = new Smarty_Variable();
$__foreach_option_data_11_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_option_data_11_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['option_data']->value) {
$__foreach_option_data_11_saved_local_item = $_smarty_tpl->tpl_vars['option_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['option_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['option_data'] = $__foreach_option_data_11_saved_local_item;
}
}
if ($__foreach_option_data_11_saved_item) {
$_smarty_tpl->tpl_vars['option_data'] = $__foreach_option_data_11_saved_item;
}
if ($__foreach_option_data_11_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_option_data_11_saved_key;
}
}
if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_string_format_12_saved_item = isset($_smarty_tpl->tpl_vars['input_string_format']) ? $_smarty_tpl->tpl_vars['input_string_format'] : false;
$_smarty_tpl->tpl_vars['input_string_format'] = new Smarty_Variable();
$__foreach_input_string_format_12_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_string_format_12_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['input_string_format']->value) {
$__foreach_input_string_format_12_saved_local_item = $_smarty_tpl->tpl_vars['input_string_format'];
if ($_smarty_tpl->tpl_vars['input_string_format']->value == $_smarty_tpl->tpl_vars['option']->value['val']) {?> selected="selected"<?php }
$_smarty_tpl->tpl_vars['input_string_format'] = $__foreach_input_string_format_12_saved_local_item;
}
}
if ($__foreach_input_string_format_12_saved_item) {
$_smarty_tpl->tpl_vars['input_string_format'] = $__foreach_input_string_format_12_saved_item;
}
} else {
if ($_smarty_tpl->tpl_vars['input']->value['string_format'] == $_smarty_tpl->tpl_vars['option']->value['val']) {?> selected="selected"<?php }
}
} elseif (isset($_smarty_tpl->tpl_vars['input']->value['val'])) {
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_val_13_saved_item = isset($_smarty_tpl->tpl_vars['input_val']) ? $_smarty_tpl->tpl_vars['input_val'] : false;
$_smarty_tpl->tpl_vars['input_val'] = new Smarty_Variable();
$__foreach_input_val_13_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_val_13_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['input_val']->value) {
$__foreach_input_val_13_saved_local_item = $_smarty_tpl->tpl_vars['input_val'];
if ($_smarty_tpl->tpl_vars['input_val']->value == $_smarty_tpl->tpl_vars['option']->value['val']) {?> selected="selected"<?php }
$_smarty_tpl->tpl_vars['input_val'] = $__foreach_input_val_13_saved_local_item;
}
}
if ($__foreach_input_val_13_saved_item) {
$_smarty_tpl->tpl_vars['input_val'] = $__foreach_input_val_13_saved_item;
}
} else {
if ($_smarty_tpl->tpl_vars['input']->value['val'] == $_smarty_tpl->tpl_vars['option']->value['val']) {?> selected="selected"<?php }
}
}?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['text'], ENT_QUOTES, 'utf-8', true);?>
</option><?php
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_10_saved_local_item;
}
}
if ($__foreach_option_10_saved_item) {
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_10_saved_item;
}
}?></select><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'change-password') {?><div class="row"> <div class="col-lg-12"> <button type="button" id="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-btn-change"class="btn btn-default"> <i class="icon-lock"></i><?php echo l(array('s'=>'更改密碼'),$_smarty_tpl);?>
</button> <div id="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-change-container"class="form-password-change well hide"> <div class="form-group"> <label for="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
"class="main_t required control-label col-lg-2"> <span class="main_t label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="<?php echo l(array('s'=>'密碼最少要8個字元'),$_smarty_tpl);?>
"><?php echo l(array('s'=>'新密碼'),$_smarty_tpl);?>
</span> </label> <div class="col-lg-6"> <div class="input-group fixed-width-lg"> <span class="main_t_2 input-group-addon"> <i class="glyphicon glyphicon-lock"></i> </span> <input type="password" id="<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
"class="form-control<?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"required autocomplete="off"placeholder="<?php echo l(array('s'=>'新密碼'),$_smarty_tpl);?>
"/> </div> <span id="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-output"></span> </div> </div> <div class="form-group"> <label for="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
2"class="main_t required control-label col-lg-2"><?php echo l(array('s'=>'確認密碼'),$_smarty_tpl);?>
</label> <div class="col-lg-6"> <div class="input-group fixed-width-lg"> <span class="main_t_2 input-group-addon"> <i class="glyphicon glyphicon-lock"></i> </span> <input type="password" id="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
2"name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
2"class="form-control<?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"value="" autocomplete="off"placeholder="<?php echo l(array('s'=>'確認密碼'),$_smarty_tpl);?>
"/> </div> </div> </div> <div class="row"> <div class="col-lg-12"> <button type="button" id="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-cancel-btn"class="btn btn-default"> <i class="icon-remove"></i><?php echo l(array('s'=>'取消'),$_smarty_tpl);?>
</button> </div> </div> </div> </div> </div>
								<?php echo '<script'; ?>
 type="text/javascript">
									$(function () {
										var $oldPwd = $('#old_passwd');
										var $passwordField = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
');
										var $output = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-output');
										var $generateBtn = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-generate-btn');
										var $generateField = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-generate-field');
										var $cancelBtn = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-cancel-btn');
										var $container = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-change-container');
										var $changeBtn = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
-btn-change');
										var $confirmPwd = $('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
2');
										$changeBtn.on('click', function () {
											$container.removeClass('hide');
											$changeBtn.addClass('hide');
										});
										$cancelBtn.on('click', function () {
											$container.find("input").val("");
											$container.addClass('hide');
											$changeBtn.removeClass('hide');
										});
										var email_type = true;
										if ($('input[name="email"]').attr('type') != 'email') email_type = false;
										$('#<?php if (isset($_smarty_tpl->tpl_vars['form']->value['id'])) {
echo $_smarty_tpl->tpl_vars['form']->value['id'];
} elseif (isset($_smarty_tpl->tpl_vars['form']->value['name'])) {
echo $_smarty_tpl->tpl_vars['form']->value['name'];
} else {
echo $_smarty_tpl->tpl_vars['table']->value;
}?>').validate({
											rules: {
												"email": {
													email: email_type
												},
												"<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
": {
													minlength: 8
												},
												"<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
2": {
													password_same: true
												},
												"old_passwd": {},
											},
											// override jquery validate plugin defaults for bootstrap 3
											highlight: function (element) {
												$(element).closest('.form-group').addClass('has-error');
											},
											unhighlight: function (element) {
												$(element).closest('.form-group').removeClass('has-error');
											},
											errorElement: 'span',
											errorClass: 'help-block',
											errorPlacement: function (error, element) {
												if (element.parent('.input-group').length) {
													error.insertAfter(element.parent());
												} else {
													error.insertAfter(element);
												};
											}
										});
									});
								<?php echo '</script'; ?>
><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'password' || $_smarty_tpl->tpl_vars['input']->value['type'] == 'new-password') {?><input type="password" id="<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
"class="form-control <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['size'])) {?> size="<?php echo $_smarty_tpl->tpl_vars['input']->value['size'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxchar']) && $_smarty_tpl->tpl_vars['input']->value['maxchar']) {?> data-maxchar="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxchar']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxlength']) && $_smarty_tpl->tpl_vars['input']->value['maxlength']) {?> maxlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['minlength']) && $_smarty_tpl->tpl_vars['input']->value['minlength']) {?> minlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['minlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['readonly']) && $_smarty_tpl->tpl_vars['input']->value['readonly']) {?> readonly="readonly"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['placeholder']) && $_smarty_tpl->tpl_vars['input']->value['placeholder']) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['input']->value['placeholder'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_14_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_14_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_14_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_14_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_14_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_14_saved_local_item;
}
}
if ($__foreach_input_data_14_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_14_saved_item;
}
if ($__foreach_input_data_14_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_14_saved_key;
}
}?>autocomplete="off"/><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'confirm-password') {?><input type="password" id="<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
"class="form-control <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['size'])) {?> size="<?php echo $_smarty_tpl->tpl_vars['input']->value['size'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxchar']) && $_smarty_tpl->tpl_vars['input']->value['maxchar']) {?> data-maxchar="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxchar']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxlength']) && $_smarty_tpl->tpl_vars['input']->value['maxlength']) {?> maxlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['minlength']) && $_smarty_tpl->tpl_vars['input']->value['minlength']) {?> minlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['minlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['readonly']) && $_smarty_tpl->tpl_vars['input']->value['readonly']) {?> readonly="readonly"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['placeholder']) && $_smarty_tpl->tpl_vars['input']->value['placeholder']) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['input']->value['placeholder'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_15_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_15_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_15_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_15_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_15_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_15_saved_local_item;
}
}
if ($__foreach_input_data_15_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_15_saved_item;
}
if ($__foreach_input_data_15_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_15_saved_key;
}
}?>autocomplete="off"/><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'switch') {
if ($_smarty_tpl->tpl_vars['input']->value['is_prefix'] || $_smarty_tpl->tpl_vars['input']->value['is_suffix']) {?><div class="input-group-btn"><?php }?><div class="btn-group<?php if (isset($_smarty_tpl->tpl_vars['input']->value['btn_group'])) {?> btn-group-<?php echo $_smarty_tpl->tpl_vars['input']->value['btn_group'];
}?>" data-toggle="buttons"><?php
$_from = $_smarty_tpl->tpl_vars['input']->value['values'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_16_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_16_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_16_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_16_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?><label class="btn btn-default<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_string_format_v_17_saved_item = isset($_smarty_tpl->tpl_vars['string_format_v']) ? $_smarty_tpl->tpl_vars['string_format_v'] : false;
$_smarty_tpl->tpl_vars['string_format_v'] = new Smarty_Variable();
$__foreach_string_format_v_17_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_string_format_v_17_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['string_format_v']->value) {
$__foreach_string_format_v_17_saved_local_item = $_smarty_tpl->tpl_vars['string_format_v'];
if ($_smarty_tpl->tpl_vars['string_format_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> active<?php }
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_17_saved_local_item;
}
}
if ($__foreach_string_format_v_17_saved_item) {
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_17_saved_item;
}
} else {
$_from = $_smarty_tpl->tpl_vars['input']->value['val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_value_v_18_saved_item = isset($_smarty_tpl->tpl_vars['value_value_v']) ? $_smarty_tpl->tpl_vars['value_value_v'] : false;
$_smarty_tpl->tpl_vars['value_value_v'] = new Smarty_Variable();
$__foreach_value_value_v_18_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_value_v_18_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value_value_v']->value) {
$__foreach_value_value_v_18_saved_local_item = $_smarty_tpl->tpl_vars['value_value_v'];
if ($_smarty_tpl->tpl_vars['value_value_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> active<?php }
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_18_saved_local_item;
}
}
if ($__foreach_value_value_v_18_saved_item) {
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_18_saved_item;
}
}
if (isset($_smarty_tpl->tpl_vars['value']->value['label_class'])) {?> <?php echo $_smarty_tpl->tpl_vars['value']->value['label_class'];
}?>"for="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"> <input type="radio" autocomplete="off" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"value="<?php echo $_smarty_tpl->tpl_vars['value']->value['value'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_string_format_v_19_saved_item = isset($_smarty_tpl->tpl_vars['string_format_v']) ? $_smarty_tpl->tpl_vars['string_format_v'] : false;
$_smarty_tpl->tpl_vars['string_format_v'] = new Smarty_Variable();
$__foreach_string_format_v_19_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_string_format_v_19_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['string_format_v']->value) {
$__foreach_string_format_v_19_saved_local_item = $_smarty_tpl->tpl_vars['string_format_v'];
if ($_smarty_tpl->tpl_vars['string_format_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> checked="checked"<?php }
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_19_saved_local_item;
}
}
if ($__foreach_string_format_v_19_saved_item) {
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_19_saved_item;
}
} else {
$_from = $_smarty_tpl->tpl_vars['input']->value['val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_value_v_20_saved_item = isset($_smarty_tpl->tpl_vars['value_value_v']) ? $_smarty_tpl->tpl_vars['value_value_v'] : false;
$_smarty_tpl->tpl_vars['value_value_v'] = new Smarty_Variable();
$__foreach_value_value_v_20_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_value_v_20_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value_value_v']->value) {
$__foreach_value_value_v_20_saved_local_item = $_smarty_tpl->tpl_vars['value_value_v'];
if ($_smarty_tpl->tpl_vars['value_value_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> checked="checked"<?php }
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_20_saved_local_item;
}
}
if ($__foreach_value_value_v_20_saved_item) {
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_20_saved_item;
}
}
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }?>/><?php echo $_smarty_tpl->tpl_vars['value']->value['label'];?>
</label><?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_16_saved_local_item;
}
}
if ($__foreach_value_16_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_16_saved_item;
}
?></div><?php if ($_smarty_tpl->tpl_vars['input']->value['is_prefix'] || $_smarty_tpl->tpl_vars['input']->value['is_suffix']) {?></div><?php }
} elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'checkbox') {
if ($_smarty_tpl->tpl_vars['input']->value['is_prefix'] || $_smarty_tpl->tpl_vars['input']->value['is_suffix']) {?><div class="input-group-btn"><?php }?><div class="btn-group<?php if (isset($_smarty_tpl->tpl_vars['input']->value['btn_group'])) {?> btn-group-<?php echo $_smarty_tpl->tpl_vars['input']->value['btn_group'];
}
if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"<?php if (!is_null($_smarty_tpl->tpl_vars['input']->value['data']['toggle'])) {?> data-toggle="<?php echo $_smarty_tpl->tpl_vars['input']->value['data']['toggle'];?>
"<?php } else { ?> data-toggle="buttons"<?php }?>><?php
$_from = $_smarty_tpl->tpl_vars['input']->value['values'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_21_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_21_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_21_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_21_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?><label class="<?php if (!isset($_smarty_tpl->tpl_vars['input']->value['checkbox']['class'])) {?>btn btn-default checkbox<?php } else {
echo $_smarty_tpl->tpl_vars['input']->value['checkbox']['class'];
}
if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_string_format_v_22_saved_item = isset($_smarty_tpl->tpl_vars['string_format_v']) ? $_smarty_tpl->tpl_vars['string_format_v'] : false;
$_smarty_tpl->tpl_vars['string_format_v'] = new Smarty_Variable();
$__foreach_string_format_v_22_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_string_format_v_22_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['string_format_v']->value) {
$__foreach_string_format_v_22_saved_local_item = $_smarty_tpl->tpl_vars['string_format_v'];
if ($_smarty_tpl->tpl_vars['string_format_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> active<?php }
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_22_saved_local_item;
}
}
if ($__foreach_string_format_v_22_saved_item) {
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_22_saved_item;
}
} else {
$_from = $_smarty_tpl->tpl_vars['input']->value['val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_value_v_23_saved_item = isset($_smarty_tpl->tpl_vars['value_value_v']) ? $_smarty_tpl->tpl_vars['value_value_v'] : false;
$_smarty_tpl->tpl_vars['value_value_v'] = new Smarty_Variable();
$__foreach_value_value_v_23_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_value_v_23_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value_value_v']->value) {
$__foreach_value_value_v_23_saved_local_item = $_smarty_tpl->tpl_vars['value_value_v'];
if ($_smarty_tpl->tpl_vars['value_value_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> active<?php }
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_23_saved_local_item;
}
}
if ($__foreach_value_value_v_23_saved_item) {
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_23_saved_item;
}
}
if (isset($_smarty_tpl->tpl_vars['value']->value['label_class'])) {?> <?php echo $_smarty_tpl->tpl_vars['value']->value['label_class'];
}?>"for="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"> <input type="checkbox" autocomplete="off" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];
if ((isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) || (count($_smarty_tpl->tpl_vars['input']->value['val']) > 1)) {?>[]<?php }?>" id="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" data-class_id="<?php echo $_smarty_tpl->tpl_vars['value']->value['class_id'];?>
"value="<?php echo $_smarty_tpl->tpl_vars['value']->value['value'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['string_format'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_string_format_v_24_saved_item = isset($_smarty_tpl->tpl_vars['string_format_v']) ? $_smarty_tpl->tpl_vars['string_format_v'] : false;
$_smarty_tpl->tpl_vars['string_format_v'] = new Smarty_Variable();
$__foreach_string_format_v_24_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_string_format_v_24_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['string_format_v']->value) {
$__foreach_string_format_v_24_saved_local_item = $_smarty_tpl->tpl_vars['string_format_v'];
if ($_smarty_tpl->tpl_vars['string_format_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> checked="checked"<?php }
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_24_saved_local_item;
}
}
if ($__foreach_string_format_v_24_saved_item) {
$_smarty_tpl->tpl_vars['string_format_v'] = $__foreach_string_format_v_24_saved_item;
}
} else {
$_from = $_smarty_tpl->tpl_vars['input']->value['val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_value_v_25_saved_item = isset($_smarty_tpl->tpl_vars['value_value_v']) ? $_smarty_tpl->tpl_vars['value_value_v'] : false;
$_smarty_tpl->tpl_vars['value_value_v'] = new Smarty_Variable();
$__foreach_value_value_v_25_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_value_v_25_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value_value_v']->value) {
$__foreach_value_value_v_25_saved_local_item = $_smarty_tpl->tpl_vars['value_value_v'];
if ($_smarty_tpl->tpl_vars['value_value_v']->value == $_smarty_tpl->tpl_vars['value']->value['value']) {?> checked="checked"<?php }
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_25_saved_local_item;
}
}
if ($__foreach_value_value_v_25_saved_item) {
$_smarty_tpl->tpl_vars['value_value_v'] = $__foreach_value_value_v_25_saved_item;
}
}
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }?>/><?php echo $_smarty_tpl->tpl_vars['value']->value['label'];?>
</label><?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_21_saved_local_item;
}
}
if ($__foreach_value_21_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_21_saved_item;
}
?></div><?php if ($_smarty_tpl->tpl_vars['input']->value['is_prefix'] || $_smarty_tpl->tpl_vars['input']->value['is_suffix']) {?></div><?php }
} elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'textarea') {?><textarea type="text" id="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>"class="form-control <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}
if ($_smarty_tpl->tpl_vars['input']->value['type'] == 'tags') {?> tagify<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['size'])) {?> size="<?php echo $_smarty_tpl->tpl_vars['input']->value['size'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxchar']) && $_smarty_tpl->tpl_vars['input']->value['maxchar']) {?> data-maxchar="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxchar']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxlength']) && $_smarty_tpl->tpl_vars['input']->value['maxlength']) {?> maxlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['minlength']) && $_smarty_tpl->tpl_vars['input']->value['minlength']) {?> minlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['minlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['rows'])) {?> rows="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['rows']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['cols'])) {?> cols="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['cols']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['readonly']) && $_smarty_tpl->tpl_vars['input']->value['readonly']) {?> readonly="readonly"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['autocomplete']) && !$_smarty_tpl->tpl_vars['input']->value['autocomplete']) {?> autocomplete="off"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['required']) && $_smarty_tpl->tpl_vars['input']->value['required']) {?> required="required" <?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['autocorrect']) && $_smarty_tpl->tpl_vars['input']->value['autocorrect']) {?> autocorrect="autocorrect" <?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['placeholder']) && $_smarty_tpl->tpl_vars['input']->value['placeholder']) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['input']->value['placeholder'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_26_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_26_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_26_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_26_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_26_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_26_saved_local_item;
}
}
if ($__foreach_input_data_26_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_26_saved_item;
}
if ($__foreach_input_data_26_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_26_saved_key;
}
}?>><?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
echo htmlspecialchars(sprintf($_smarty_tpl->tpl_vars['input']->value['string_format'],$_smarty_tpl->tpl_vars['input']->value['val']), ENT_QUOTES, 'UTF-8', true);
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['val'], ENT_QUOTES, 'UTF-8', true);
}?></textarea><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'signature') {?><div id="signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_div" class="signature_div"> <div id="signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" class="img_div" style="max-width:<?php echo $_smarty_tpl->tpl_vars['input']->value['signature']['width']+2;?>
px;max-height:<?php echo $_smarty_tpl->tpl_vars['input']->value['signature']['height']+2;?>
px;height:<?php echo $_smarty_tpl->tpl_vars['input']->value['signature']['height']+2;?>
px;"><?php if ($_smarty_tpl->tpl_vars['arr_signature_data']->value[$_smarty_tpl->tpl_vars['input']->value['name']]['data'] != '') {?><img src="data:<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
echo $_smarty_tpl->tpl_vars['input']->value['string_format'];
} else {
echo $_smarty_tpl->tpl_vars['arr_signature_data']->value[$_smarty_tpl->tpl_vars['input']->value['name']]['data'];
}?>"><?php }?></div><?php if ($_smarty_tpl->tpl_vars['display']->value != 'view' && $_smarty_tpl->tpl_vars['input']->value['key']['type'] != 'view' && empty($_smarty_tpl->tpl_vars['arr_signature_data']->value[$_smarty_tpl->tpl_vars['input']->value['name']]['data']) && empty($_smarty_tpl->tpl_vars['arr_signature_data']->value[$_smarty_tpl->tpl_vars['input']->value['name']]['data'])) {?><input type="hidden" id="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" value="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
echo $_smarty_tpl->tpl_vars['input']->value['string_format'];
} else {
echo $_smarty_tpl->tpl_vars['arr_signature_data']->value[$_smarty_tpl->tpl_vars['input']->value['name']]['data'];
}?>"> <div class="signature_btn"> <button type="button" class="open_signature btn btn-default" title="<?php echo l(array('s'=>"簽名"),$_smarty_tpl);?>
"><i class="glyphicon glyphicon-pencil"></i></button> <button type="button" class="close_signature btn btn-default" style="display:none;" title="<?php echo l(array('s'=>"關閉"),$_smarty_tpl);?>
"><i class="glyphicon glyphicon-remove"></i></button> <button type="button" class="reset_signature btn btn-default" style="display:none;" title="<?php echo l(array('s'=>"清除"),$_smarty_tpl);?>
"><i class="glyphicon glyphicon-refresh"></i></button> <button type="button" class="ok_signature btn btn-default pull-right" style="display:none;" title="<?php echo l(array('s'=>"確定"),$_smarty_tpl);?>
"><i class="glyphicon glyphicon-ok"></i></button> </div><?php } else {
}?></div><?php if ($_smarty_tpl->tpl_vars['display']->value != 'view') {
echo '<script'; ?>
 type="text/javascript">var $div = '#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_div';function close_signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
(){$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
', $div).jSignature('destroy');if(screenfull.enabled){screenfull.exit();};$('.reset_signature', $div).hide();$('.close_signature', $div).hide();$('.ok_signature', $div).hide();$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').removeClass('full');$('.open_signature', $div).show();$('.signature_btn', $div).removeClass('full');};$('.open_signature', $div).click(function(){if(screenfull.enabled && <?php echo $_smarty_tpl->tpl_vars['input']->value['signature']['screen_full'];?>
) {screenfull.request();setTimeout(function(){$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
', $div).jSignature({ lineWidth: Math.max(1, $(window).height() / 180), width: $(window).width(), height: $(window).height() });$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').show().addClass('full');$('.signature_btn', $div).addClass('full');}, 500);}else{$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
', $div).jSignature({ lineWidth: <?php echo $_smarty_tpl->tpl_vars['input']->value['signature']['lineWidth'];?>
, width: Math.min(<?php echo $_smarty_tpl->tpl_vars['input']->value['signature']['width'];?>
, $(window).width()), height: Math.min(<?php echo $_smarty_tpl->tpl_vars['input']->value['signature']['height'];?>
, $(window).height()) });$(this).hide();$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').show();};$(this).hide();$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 img', $div).addClass('hidden');$('.reset_signature', $div).show();$('.close_signature', $div).show();$('.ok_signature', $div).show();});$('.reset_signature', $div).click(function(){$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
', $div).jSignature('clear');});$('.close_signature', $div).click(function(){close_signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
();$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 img', $div).removeClass('hidden');if($('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 img', $div).length == 0){$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').hide();};});$('.ok_signature', $div).click(function(){var img_data = $('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
', $div).jSignature('getData', 'image');$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 img', $div).remove();if (typeof img_data === 'string'){$('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').val(img_data);$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
', $div).html('<img src="data:'+img_data+'">');} else if($.isArray(img_data) && img_data.length === 2){$('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').val(img_data.join(','));$('#signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
', $div).html('<img src="data:'+img_data.join(',')+'">');} else {try {$('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').val(JSON.stringify(img_data));} catch (ex) {$('#<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').val('Not sure how to stringify this, likely binary, format.');};};close_signature_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
();});<?php echo '</script'; ?>
><?php }
} elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'take_photo') {?><div id="take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
_div" class="take_photo_div"> <div class="img_list"><?php
$_from = $_smarty_tpl->tpl_vars['arr_take_photo_data']->value[$_smarty_tpl->tpl_vars['input']->value['name']];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_img_data_27_saved_item = isset($_smarty_tpl->tpl_vars['img_data']) ? $_smarty_tpl->tpl_vars['img_data'] : false;
$__foreach_img_data_27_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['img_data'] = new Smarty_Variable();
$__foreach_img_data_27_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_img_data_27_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['img_data']->value) {
$__foreach_img_data_27_saved_local_item = $_smarty_tpl->tpl_vars['img_data'];
?><div class="img_div file-preview-frame krajee-default kv-preview-thumb"> <img class="img" src="<?php echo $_smarty_tpl->tpl_vars['img_data']->value['data'];?>
"/><?php if ($_smarty_tpl->tpl_vars['img_data']->value['type'] != 'view') {?><input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>" value=""> <input type="hidden" name="id_img_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>" value="<?php echo $_smarty_tpl->tpl_vars['img_data']->value['index'];?>
"><?php if ($_smarty_tpl->tpl_vars['input']->value['key']['type'] != 'view') {?><div class="file-actions"><div class="file-footer-buttons"> <bottom type="button" class="kv-file-remove btn btn-kv btn-default btn-outline-secondary del" title="<?php echo l(array('s'=>"刪除"),$_smarty_tpl);?>
"><i class="glyphicon glyphicon-trash"></i></bottom> </div> </div><?php }
}?></div><?php
$_smarty_tpl->tpl_vars['img_data'] = $__foreach_img_data_27_saved_local_item;
}
}
if ($__foreach_img_data_27_saved_item) {
$_smarty_tpl->tpl_vars['img_data'] = $__foreach_img_data_27_saved_item;
}
if ($__foreach_img_data_27_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_img_data_27_saved_key;
}
?></div><?php if ($_smarty_tpl->tpl_vars['display']->value != 'view' && $_smarty_tpl->tpl_vars['input']->value['key']['type'] != 'view') {?><div class="img_div file-preview-frame krajee-default kv-preview-thumb"> <div id="take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
"></div> <div class="file-footer-buttons"> <button type="button" class="open_take btn" title="<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['open_txt'];?>
" style="width:<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['width'];?>
px;height:<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['height'];?>
px;"><i class="glyphicon glyphicon-facetime-video" style="font-size:<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['font_size'];?>
;"></i></button> <button type="button" class="take btn btn-default" style="display:none;" title="<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['take_txt'];?>
"><i class="glyphicon glyphicon-camera"></i></button> <button type="button" class="close_take btn btn-default" style="display:none;" title="<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['close_txt'];?>
"><i class="glyphicon glyphicon-off"></i></button> </div> </div><?php }?></div> <?php echo '<script'; ?>
 type="text/javascript">var $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 = $('#take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_div');function open_take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
(){Webcam.reset();Webcam.set({width: <?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['width'];?>
,height: <?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['height'];?>
,image_format: 'png',jpeg_quality: 100});Webcam.attach('#take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
');$('#take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
').stop(true, false).fadeIn();$('.take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).stop(true, false).fadeIn();$('.close_take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).stop(true, false).fadeIn();$('.open_take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).hide();};function close_take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
(){$('#take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
').hide();$('.take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).hide();$('.close_take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).hide();$('.open_take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).stop(true, false).fadeIn();Webcam.reset();};function take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
(){shutter.play();Webcam.snap( function(data_uri) {$('.img_list', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).append('<div class="img_div file-preview-frame krajee-default  kv-preview-thumb"><img class="img" src="' + data_uri + '"/><input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>" value="' + data_uri + '"><input type="hidden" name="id_img_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>" value=""><div class="file-actions"><div class="file-footer-buttons"><button type="button" class="kv-file-remove btn btn-kv btn-default btn-outline-secondary del" title="<?php echo l(array('s'=>"刪除"),$_smarty_tpl);?>
"><i class="glyphicon glyphicon-trash"></i></button></div></div></div>');});};$('.open_take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).click(function(){open_take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
();$('#take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').addClass('pointer').attr('title', '<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['take_txt'];?>
');});$('.close_take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).click(function(){close_take_photo_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);?>
();$('#take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
').removeClass('pointer').attr('title', '');});$('.take', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).click(function(){<?php if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
();<?php } else { ?>$('.img_list .img_div', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).remove();take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
();<?php }?>});$(document).on('click', '#take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_div .del', function(){if(confirm('<?php echo $_smarty_tpl->tpl_vars['input']->value['take_photo']['confirm'];?>
')){$(this).parents('.img_div').remove();};});$(document).on('click', '#take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_div video', function(){<?php if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
();<?php } else { ?>$('.img_list .img_div', $take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
).remove();take_photo_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
();<?php }?>});<?php echo '</script'; ?>
><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'map') {?><div id="map" data-draggable="<?php if ($_smarty_tpl->tpl_vars['display']->value == 'edit' || $_smarty_tpl->tpl_vars['display']->value == 'add') {?>true<?php } else { ?>false<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {?> class="google_map <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"<?php
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_28_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_28_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_28_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_28_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_28_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_28_saved_local_item;
}
}
if ($__foreach_input_data_28_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_28_saved_item;
}
if ($__foreach_input_data_28_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_28_saved_key;
}
}?>></div><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'file') {?><input type="file" id="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['name'], ENT_QUOTES, 'utf-8', true);
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?>[]<?php }?>"class="form-control <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"value="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
echo htmlspecialchars(sprintf($_smarty_tpl->tpl_vars['input']->value['string_format'],$_smarty_tpl->tpl_vars['input']->value['val']), ENT_QUOTES, 'UTF-8', true);
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['val'], ENT_QUOTES, 'UTF-8', true);
}?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['size'])) {?> size="<?php echo $_smarty_tpl->tpl_vars['input']->value['size'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['min']) && ($_smarty_tpl->tpl_vars['input']->value['min'] || $_smarty_tpl->tpl_vars['input']->value['min'] == 0)) {?> min="<?php echo $_smarty_tpl->tpl_vars['input']->value['min'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['max'])) {?> max="<?php echo $_smarty_tpl->tpl_vars['input']->value['max'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxchar']) && $_smarty_tpl->tpl_vars['input']->value['maxchar']) {?> data-maxchar="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxchar']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['maxlength']) && $_smarty_tpl->tpl_vars['input']->value['maxlength']) {?> maxlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['maxlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['minlength']) && $_smarty_tpl->tpl_vars['input']->value['minlength']) {?> minlength="<?php echo intval($_smarty_tpl->tpl_vars['input']->value['minlength']);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['readonly']) && $_smarty_tpl->tpl_vars['input']->value['readonly']) {?> readonly="readonly"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['disabled']) && $_smarty_tpl->tpl_vars['input']->value['disabled']) {?> disabled="disabled"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['autocomplete']) && !$_smarty_tpl->tpl_vars['input']->value['autocomplete']) {?> autocomplete="off"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['required']) && $_smarty_tpl->tpl_vars['input']->value['required']) {?> required="required" <?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['multiple']) && $_smarty_tpl->tpl_vars['input']->value['multiple']) {?> multiple="multiple"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['placeholder']) && $_smarty_tpl->tpl_vars['input']->value['placeholder']) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['input']->value['placeholder'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['autocorrect']) && $_smarty_tpl->tpl_vars['input']->value['autocorrect']) {?> autocorrect="<?php echo $_smarty_tpl->tpl_vars['input']->value['autocorrect'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['file']['type'])) {?> data-allowed-file-extensions="<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['type'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_29_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_29_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_29_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_29_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_29_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_29_saved_local_item;
}
}
if ($__foreach_input_data_29_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_29_saved_item;
}
if ($__foreach_input_data_29_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_29_saved_key;
}
}?>/>
								<?php echo '<script'; ?>
 type="text/javascript">
									<?php if (isset($_smarty_tpl->tpl_vars['input']->value['file']['advanced']) && $_smarty_tpl->tpl_vars['input']->value['file']['advanced']) {?>

									<?php } else { ?>
									var initialPreview_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 = [];
									var initialPreviewConfig_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 = [];
									var UpFileVal_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 = [];
									<?php if (!empty($_smarty_tpl->tpl_vars['initialPreview']->value[$_smarty_tpl->tpl_vars['input']->value['name']])) {?>initialPreview_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 = <?php echo $_smarty_tpl->tpl_vars['initialPreview']->value[$_smarty_tpl->tpl_vars['input']->value['name']];?>
;<?php }?>
									<?php if (!empty($_smarty_tpl->tpl_vars['initialPreviewConfig']->value[$_smarty_tpl->tpl_vars['input']->value['name']])) {?>initialPreviewConfig_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 = <?php echo $_smarty_tpl->tpl_vars['initialPreviewConfig']->value[$_smarty_tpl->tpl_vars['input']->value['name']];?>
;<?php }?>
									<?php if (!empty($_smarty_tpl->tpl_vars['UpFileVal']->value[$_smarty_tpl->tpl_vars['input']->value['name']])) {?>UpFileVal_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
 = <?php echo $_smarty_tpl->tpl_vars['UpFileVal']->value[$_smarty_tpl->tpl_vars['input']->value['name']];?>
;<?php }?>
									$('#<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>').fileinput({
										<?php if (!isset($_smarty_tpl->tpl_vars['input']->value['file']['ajax'])) {?>
										uploadUrl: document.location.pathname,
										<?php } else { ?>
										<?php if ($_smarty_tpl->tpl_vars['input']->value['file']['ajax']) {?>uploadUrl: document.location.pathname,<?php }?>
										<?php }?>
										initialPreviewAsData: true,
										autoUpload: true,
										validateInitialCount: true,
										<?php if ($_smarty_tpl->tpl_vars['input']->value['file']['auto_upload']) {?>
									   		showUpload: false,
									    	showRemove: false,
										<?php }?>
										<?php if ($_smarty_tpl->tpl_vars['input']->value['file']['size'] > 0) {?>maxFileSize:<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['size'];?>
,<?php }?>
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['class'])) {?>mainClass: '<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['class'];?>
',<?php }?>
										language: '<?php if (isset($_smarty_tpl->tpl_vars['input']->value['file']['language'])) {
echo $_smarty_tpl->tpl_vars['input']->value['file']['language'];
} else { ?>zh-TW<?php }?>',
										minFileCount: <?php if (isset($_smarty_tpl->tpl_vars['input']->value['file']['min'])) {
echo $_smarty_tpl->tpl_vars['input']->value['file']['min'];
} else { ?>'auto'<?php }?>,
										maxFileCount: <?php if (isset($_smarty_tpl->tpl_vars['input']->value['file']['max'])) {
echo $_smarty_tpl->tpl_vars['input']->value['file']['max'];
} else { ?>'auto'<?php }?>,
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['class'])) {?>mainClass: '<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['class'];?>
',<?php }?>
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['resize']) && $_smarty_tpl->tpl_vars['input']->value['file']['resize']) {?>resizeImage: true,<?php }?>
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['resizePreference'])) {?>resizePreference: '<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['resizePreference'];?>
',<?php }?>
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['min_width'])) {?>minImageWidth: '<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['min_width'];?>
',<?php }?>
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['min_hight'])) {?>minImageHeight: '<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['min_hight'];?>
',<?php }?>
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['max_width'])) {?>maxImageWidth: '<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['max_width'];?>
',<?php }?>
										<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['file']['max_height'])) {?>maxImageHeight: '<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['max_height'];?>
',<?php }?>
										initialPreview: initialPreview_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
,
										overwriteInitial: <?php if (isset($_smarty_tpl->tpl_vars['input']->value['file']['overwrite'])) {
echo $_smarty_tpl->tpl_vars['input']->value['file']['overwrite'];
} else { ?>false<?php }?>,
										initialPreviewFileType: 'image',
										initialPreviewConfig: initialPreviewConfig_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
,
										<?php if (isset($_smarty_tpl->tpl_vars['input']->value['file']['allowed'])) {?>allowedFileExtensions:<?php echo json_encode($_smarty_tpl->tpl_vars['input']->value['file']['allowed']);?>
,<?php }?>
										uploadExtraData: function(){
										return {
											ajax: true,
											action: '<?php if (isset($_smarty_tpl->tpl_vars['input']->value['file']['action'])) {
echo $_smarty_tpl->tpl_vars['input']->value['file']['action'];
} else { ?>UpFile<?php }?>',
											UpFileVal: UpFileVal_<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
,
											input_name:'<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
',
											data:<?php if ($_smarty_tpl->tpl_vars['input']->value['file']['data'] != '') {
echo $_smarty_tpl->tpl_vars['input']->value['file']['data'];
} else { ?>''<?php }?>
										};},
										<?php if (!isset($_smarty_tpl->tpl_vars['input']->value['file']['icon']) || $_smarty_tpl->tpl_vars['input']->value['file']['icon']) {?>
										preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
									    previewFileIconSettings: { // configure your icon file extensions
										  'doc': '<i class="fa fa-file-word text-primary"></i>',
										  'xls': '<i class="fa fa-file-excel text-success"></i>',
										  'ppt': '<i class="fa fa-file-powerpoint text-danger"></i>',
										  'pdf': '<i class="fa fa-file-pdf text-danger"></i>',
										  'zip': '<i class="fa fa-file-archive text-muted"></i>',
										  'htm': '<i class="fa fa-file-code text-info"></i>',
										  'txt': '<i class="fa fa-file-text text-info"></i>',
										  'mov': '<i class="fa fa-file-video text-warning"></i>',
										  'mp3': '<i class="fa fa-file-audio text-warning"></i>',
										  // note for these file types below no extension determination logic
										  // has been configured (the keys itself will be used as extensions)
										  // 'jpg': '<i class="fa fa-file-image text-danger"></i>',
										  // 'gif': '<i class="fa fa-file-image text-muted"></i>',
										  // 'png': '<i class="fa fa-file-image text-primary"></i>'
									    },
									    previewFileExtSettings: { // configure the logic for determining icon file extensions
										  'jpg': function(ext) {
											return ext.match(/(jpg|jpeg)$/i);
										  },
										  'doc': function(ext) {
											return ext.match(/(doc|docx)$/i);
										  },
										  'xls': function(ext) {
											return ext.match(/(xls|xlsx)$/i);
										  },
										  'ppt': function(ext) {
											return ext.match(/(ppt|pptx)$/i);
										  },
										  'zip': function(ext) {
											return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
										  },
										  'htm': function(ext) {
											return ext.match(/(htm|html)$/i);
										  },
										  'txt': function(ext) {
											return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
										  },
										  'mov': function(ext) {
											return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
										  },
										  'mp3': function(ext) {
											return ext.match(/(mp3|wav)$/i);
										  },
									    }
										<?php }?>
									}).on('filesorted', function (event, data, previewId, index) {
										<?php if ($_smarty_tpl->tpl_vars['display']->value != 'view') {?>
										var arr_id = new Array();
										for(var i in data.stack){
											arr_id.push(data.stack[i].key);
										};
										$.ajax({
											url : document.location.pathname,
											method : 'POST',
											data : {
												'ajax' : true,
												'action' : 'MoveFile',
												'arr_id' : arr_id,
											},
											dataType : 'json',
											success: function(data){
												if(data.error == '' || data.error == undefined){
													$.growl.notice({
														title: '',
														message: data.msg,
													});
												}else{
													$.growl.error({
														title: '',
														message: data.error,
													});
												};
											},
											error:function(xhr, ajaxOptions, thrownError){
											},
										});
										<?php }?>
									}).on('fileuploaded', function (event, data) {
										<?php echo $_smarty_tpl->tpl_vars['input']->value['file']['loaded'];?>

									}).on("filebatchselected", function(event, data) {
										<?php if ($_smarty_tpl->tpl_vars['input']->value['file']['auto_upload']) {?>
									 	$(this).fileinput('upload');
									 	<?php }?>
									});
									<?php }?>
									$('button.kv-file-zoom', '.file-footer-buttons').removeAttr('disabled').attr('disabled', false);
									<?php if ($_smarty_tpl->tpl_vars['display']->value == 'view') {?>
									$('button.kv-file-remove', '.file-footer-buttons').remove();
									$('.file-drag-handle', '.file-thumbnail-footer').remove();
									$('.input-group.file-caption-main', '.file-input').remove();
									<?php }?>
								<?php echo '</script'; ?>
>
								<?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'view') {
if ($_smarty_tpl->tpl_vars['input']->value['multiple'] && isset($_smarty_tpl->tpl_vars['input']->value['options'])) {?><textarea type="text" id="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"class="view"disabled style="width:100%;" rows="8"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_30_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_30_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_30_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_30_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_30_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?>data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_30_saved_local_item;
}
}
if ($__foreach_input_data_30_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_30_saved_item;
}
if ($__foreach_input_data_30_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_30_saved_key;
}
}?>><?php if (!empty($_smarty_tpl->tpl_vars['input']->value['val']) || $_smarty_tpl->tpl_vars['input']->value['val'] === 0 || $_smarty_tpl->tpl_vars['input']->value['val'] === '0') {
echo $_smarty_tpl->tpl_vars['input']->value['val'];
}?></textarea><?php } else { ?><samp class="form-control view <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['rows']) && $_smarty_tpl->tpl_vars['input']->value['rows'] > 0) {?> style="min-height:<?php echo $_smarty_tpl->tpl_vars['input']->value['rows']*1.5;?>
em;overflow:auto;"<?php }?>id="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"<?php if (isset($_smarty_tpl->tpl_vars['input']->value['data'])) {
$_from = $_smarty_tpl->tpl_vars['input']->value['data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_data_31_saved_item = isset($_smarty_tpl->tpl_vars['input_data']) ? $_smarty_tpl->tpl_vars['input_data'] : false;
$__foreach_input_data_31_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['input_data'] = new Smarty_Variable();
$__foreach_input_data_31_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_data_31_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['input_data']->value) {
$__foreach_input_data_31_saved_local_item = $_smarty_tpl->tpl_vars['input_data'];
?> data-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['input_data']->value;?>
"<?php
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_31_saved_local_item;
}
}
if ($__foreach_input_data_31_saved_item) {
$_smarty_tpl->tpl_vars['input_data'] = $__foreach_input_data_31_saved_item;
}
if ($__foreach_input_data_31_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_input_data_31_saved_key;
}
}?>><?php if (((!empty($_smarty_tpl->tpl_vars['input']->value['val']) || $_smarty_tpl->tpl_vars['input']->value['val'] === 0 || $_smarty_tpl->tpl_vars['input']->value['val'] === '0') && !in_array($_smarty_tpl->tpl_vars['input']->value['validate'],array('isdate','isdatetime'))) || ($_smarty_tpl->tpl_vars['input']->value['validate'] == 'isdate' && $_smarty_tpl->tpl_vars['input']->value['val'] != '0000-00-00') || ($_smarty_tpl->tpl_vars['input']->value['validate'] == 'isdatetime' && $_smarty_tpl->tpl_vars['input']->value['val'] != '0000-00-00 00:00:00')) {
if (stristr($_smarty_tpl->tpl_vars['input']->value['class'],'tinymce') === false) {
echo nl2br($_smarty_tpl->tpl_vars['input']->value['val']);
} else {
echo $_smarty_tpl->tpl_vars['input']->value['val'];
}
}?></samp><?php }
}
if (isset($_smarty_tpl->tpl_vars['input']->value['suffix_btn'])) {?><div class="input-group-btn"><?php
$_from = $_smarty_tpl->tpl_vars['input']->value['suffix_btn'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_s_btn_32_saved_item = isset($_smarty_tpl->tpl_vars['s_btn']) ? $_smarty_tpl->tpl_vars['s_btn'] : false;
$_smarty_tpl->tpl_vars['s_btn'] = new Smarty_Variable();
$__foreach_s_btn_32_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_s_btn_32_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['s_btn']->value) {
$__foreach_s_btn_32_saved_local_item = $_smarty_tpl->tpl_vars['s_btn'];
?><a class="btn btn-default <?php echo $_smarty_tpl->tpl_vars['s_btn']->value['class'];?>
" href="<?php if ($_smarty_tpl->tpl_vars['s_btn']->value['href']) {
echo $_smarty_tpl->tpl_vars['s_btn']->value['href'];
} else { ?>javascript:void(0);<?php }?>"><span class="<?php echo $_smarty_tpl->tpl_vars['s_btn']->value['icon'];?>
"></span><?php echo $_smarty_tpl->tpl_vars['s_btn']->value['title'];?>
</a><?php
$_smarty_tpl->tpl_vars['s_btn'] = $__foreach_s_btn_32_saved_local_item;
}
}
if ($__foreach_s_btn_32_saved_item) {
$_smarty_tpl->tpl_vars['s_btn'] = $__foreach_s_btn_32_saved_item;
}
?></div><?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['suffix'])) {?><span class="main_t_2 input-group-addon<?php if (isset($_smarty_tpl->tpl_vars['input']->value['suffix_class'])) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['suffix_class'];
}?>"><?php echo $_smarty_tpl->tpl_vars['input']->value['suffix'];?>
</span><?php }
if (((isset($_smarty_tpl->tpl_vars['input']->value['prefix']) || isset($_smarty_tpl->tpl_vars['input']->value['suffix']) || isset($_smarty_tpl->tpl_vars['input']->value['prefix_btn']) || isset($_smarty_tpl->tpl_vars['input']->value['suffix_btn'])) && (!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix'])) || (!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && $_smarty_tpl->tpl_vars['input']->value['is_suffix'])) {?></div><?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['p']) && $_smarty_tpl->tpl_vars['input']->value['p']) {?><p class="help-block<?php if (isset($_smarty_tpl->tpl_vars['input']->value['p_class']) && ($_smarty_tpl->tpl_vars['input']->value['p_class'] != '')) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['p_class'];
}?>"><?php echo $_smarty_tpl->tpl_vars['input']->value['p'];?>
</p><?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['html']) && $_smarty_tpl->tpl_vars['input']->value['html']) {
echo $_smarty_tpl->tpl_vars['input']->value['html'];
}
if ((!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && $_smarty_tpl->tpl_vars['input']->value['is_suffix']) || (!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix'])) {?></div><?php }
}
/* {/block 'field'} */
/* {block 'input_row'}  file:form/form_form.tpl */
function block_64105413606434b7e4ba12_18162523($_smarty_tpl, $_blockParentStack) {
if (($_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix']) || (!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix'])) {?><div class="form-group<?php if (isset($_smarty_tpl->tpl_vars['input']->value['form_col'])) {?> col-lg-<?php echo $_smarty_tpl->tpl_vars['input']->value['form_col'];
} else { ?> col-lg-12<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['form_group_class'])) {?> <?php echo $_smarty_tpl->tpl_vars['input']->value['form_group_class'];
}
if ($_smarty_tpl->tpl_vars['input']->value['type'] == 'hidden') {?> hide<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['tabs']->value) && isset($_smarty_tpl->tpl_vars['input']->value['tab'])) {?> data-tab-id="<?php echo $_smarty_tpl->tpl_vars['input']->value['tab'];?>
"<?php }?>><?php }
if ($_smarty_tpl->tpl_vars['input']->value['type'] == 'hr') {?><hr<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {?> id="<?php echo $_smarty_tpl->tpl_vars['input']->value['id'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {?> class="<?php echo $_smarty_tpl->tpl_vars['input']->value['class'];?>
"<?php }?>><?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'hidden') {?><input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" id="<?php if (!empty($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo $_smarty_tpl->tpl_vars['input']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['input']->value['name'];
}?>"class="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {
echo $_smarty_tpl->tpl_vars['input']->value['class'];
}?>"value="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['string_format'])) {
echo htmlspecialchars(sprintf($_smarty_tpl->tpl_vars['input']->value['string_format'],$_smarty_tpl->tpl_vars['input']->value['val']), ENT_QUOTES, 'UTF-8', true);
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['input']->value['val'], ENT_QUOTES, 'UTF-8', true);
}?>"/><?php } else {
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "label", array (
  0 => 'block_2134848807606434b7e54562_38202581',
  1 => false,
  3 => 0,
  2 => 0,
), $_blockParentStack);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "field", array (
  0 => 'block_979613148606434b7e5c611_70138120',
  1 => false,
  3 => 0,
  2 => 0,
), $_blockParentStack);
}
if ((!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && $_smarty_tpl->tpl_vars['input']->value['is_suffix']) || (!$_smarty_tpl->tpl_vars['input']->value['is_prefix'] && !$_smarty_tpl->tpl_vars['input']->value['is_suffix'])) {?></div><?php }
}
/* {/block 'input_row'} */
/* {block 'footer'}  file:form/form_form.tpl */
function block_1422392169606434b7f331a8_70428042($_smarty_tpl, $_blockParentStack) {
?>
<div class="panel-footer"><?php if (isset($_smarty_tpl->tpl_vars['form']->value['submit']) && !empty($_smarty_tpl->tpl_vars['form']->value['submit'])) {
$_from = $_smarty_tpl->tpl_vars['form']->value['submit'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_sub_33_saved_item = isset($_smarty_tpl->tpl_vars['sub']) ? $_smarty_tpl->tpl_vars['sub'] : false;
$_smarty_tpl->tpl_vars['sub'] = new Smarty_Variable();
$__foreach_sub_33_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_sub_33_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->value) {
$__foreach_sub_33_saved_local_item = $_smarty_tpl->tpl_vars['sub'];
if ($_smarty_tpl->tpl_vars['id_group']->value != 1 && $_smarty_tpl->tpl_vars['id_group']->value != 2 && $_smarty_tpl->tpl_vars['id_group']->value != 3 && isset($_smarty_tpl->tpl_vars['sub']->value['keepsave']) && $_smarty_tpl->tpl_vars['sub']->value['keepsave']) {?><!-- 除了管理員 不開放 keepsave--><?php } else { ?><button type="submit" value="1" id="<?php if (isset($_smarty_tpl->tpl_vars['sub']->value['id'])) {
echo $_smarty_tpl->tpl_vars['sub']->value['id'];
} else {
echo $_smarty_tpl->tpl_vars['table']->value;?>
_form_submit_btn<?php }?>"name="<?php if (isset($_smarty_tpl->tpl_vars['sub']->value['name'])) {
echo $_smarty_tpl->tpl_vars['sub']->value['name'];
} else { ?>submit<?php echo $_smarty_tpl->tpl_vars['submit_display']->value;
if (!empty($_smarty_tpl->tpl_vars['form']->value['table'])) {
echo $_smarty_tpl->tpl_vars['form']->value['table'];
} else {
echo $_smarty_tpl->tpl_vars['form']->value['tab'];
}
}
if (isset($_smarty_tpl->tpl_vars['sub']->value['stay']) && $_smarty_tpl->tpl_vars['sub']->value['stay']) {?>AndStay<?php }
if (isset($_smarty_tpl->tpl_vars['sub']->value['keepsave']) && $_smarty_tpl->tpl_vars['sub']->value['keepsave']) {?>AndKeepsave<?php }?>"class="<?php if (isset($_smarty_tpl->tpl_vars['sub']->value['class'])) {
echo $_smarty_tpl->tpl_vars['sub']->value['class'];
} else { ?>btn btn-default pull-right<?php }?>"> <i class="<?php if (isset($_smarty_tpl->tpl_vars['sub']->value['icon'])) {
echo $_smarty_tpl->tpl_vars['sub']->value['icon'];
} else { ?>icon glyphicon glyphicon-floppy-disk<?php }?>"></i><?php echo $_smarty_tpl->tpl_vars['sub']->value['title'];?>
</button><?php }
$_smarty_tpl->tpl_vars['sub'] = $__foreach_sub_33_saved_local_item;
}
}
if ($__foreach_sub_33_saved_item) {
$_smarty_tpl->tpl_vars['sub'] = $__foreach_sub_33_saved_item;
}
}
if (isset($_smarty_tpl->tpl_vars['form']->value['cancel']) && $_smarty_tpl->tpl_vars['form']->value['cancel']) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['back_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="btn btn-default"><i class="<?php if (isset($_smarty_tpl->tpl_vars['form']->value['submit']['icon'])) {
echo $_smarty_tpl->tpl_vars['form']->value['submit']['icon'];
} else { ?>icon glyphicon glyphicon-remove<?php }?>"></i><?php echo l(array('s'=>'取消'),$_smarty_tpl);?>
</a><?php }
if (isset($_smarty_tpl->tpl_vars['form']->value['reset'])) {?><button type="reset" id="<?php if (isset($_smarty_tpl->tpl_vars['form']->value['reset']['id'])) {
echo $_smarty_tpl->tpl_vars['form']->value['reset']['id'];
} else {
echo $_smarty_tpl->tpl_vars['table']->value;?>
_form_reset_btn<?php }?>"  class="<?php if (isset($_smarty_tpl->tpl_vars['form']->value['reset']['class'])) {
echo $_smarty_tpl->tpl_vars['form']->value['reset']['class'];
} else { ?>btn btn-default<?php }?>"> <i class="<?php if (isset($_smarty_tpl->tpl_vars['form']->value['submit']['icon'])) {
echo $_smarty_tpl->tpl_vars['form']->value['submit']['icon'];
} else { ?>icon glyphicon glyphicon-share-alt<?php }?>"></i><?php echo $_smarty_tpl->tpl_vars['form']->value['reset']['title'];?>
</button><?php }
if (isset($_smarty_tpl->tpl_vars['form']->value['buttons'])) {
$_from = $_smarty_tpl->tpl_vars['form']->value['buttons'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_btn_34_saved_item = isset($_smarty_tpl->tpl_vars['btn']) ? $_smarty_tpl->tpl_vars['btn'] : false;
$__foreach_btn_34_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['btn'] = new Smarty_Variable();
$__foreach_btn_34_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_btn_34_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['btn']->value) {
$__foreach_btn_34_saved_local_item = $_smarty_tpl->tpl_vars['btn'];
if (isset($_smarty_tpl->tpl_vars['btn']->value['href']) && trim($_smarty_tpl->tpl_vars['btn']->value['href']) != '') {?><a href="<?php echo $_smarty_tpl->tpl_vars['btn']->value['href'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['btn']->value['id'])) {?>id="<?php echo $_smarty_tpl->tpl_vars['btn']->value['id'];?>
"<?php }?>class="btn btn-default<?php if (isset($_smarty_tpl->tpl_vars['btn']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['btn']->value['class'];
}?>" <?php if (isset($_smarty_tpl->tpl_vars['btn']->value['js']) && $_smarty_tpl->tpl_vars['btn']->value['js']) {?> onclick="<?php echo $_smarty_tpl->tpl_vars['btn']->value['js'];?>
"<?php }?>><?php if (isset($_smarty_tpl->tpl_vars['btn']->value['icon'])) {?><i class="<?php echo $_smarty_tpl->tpl_vars['btn']->value['icon'];?>
"></i><?php }
echo $_smarty_tpl->tpl_vars['btn']->value['title'];?>
</a><?php } else { ?><button type="<?php if (isset($_smarty_tpl->tpl_vars['btn']->value['type'])) {
echo $_smarty_tpl->tpl_vars['btn']->value['type'];
} else { ?>button<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['btn']->value['id'])) {?>id="<?php echo $_smarty_tpl->tpl_vars['btn']->value['id'];?>
"<?php }?>class="btn btn-default<?php if (isset($_smarty_tpl->tpl_vars['btn']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['btn']->value['class'];
}?>"name="<?php if (isset($_smarty_tpl->tpl_vars['btn']->value['name'])) {
echo $_smarty_tpl->tpl_vars['btn']->value['name'];
} else { ?>submitOptions<?php echo $_smarty_tpl->tpl_vars['table']->value;
}?>"<?php if (isset($_smarty_tpl->tpl_vars['btn']->value['js']) && $_smarty_tpl->tpl_vars['btn']->value['js']) {?> onclick="<?php echo $_smarty_tpl->tpl_vars['btn']->value['js'];?>
"<?php }?>><?php if (isset($_smarty_tpl->tpl_vars['btn']->value['icon'])) {?><i class="<?php echo $_smarty_tpl->tpl_vars['btn']->value['icon'];?>
"></i><?php }
echo $_smarty_tpl->tpl_vars['btn']->value['title'];?>
</button><?php }
$_smarty_tpl->tpl_vars['btn'] = $__foreach_btn_34_saved_local_item;
}
}
if ($__foreach_btn_34_saved_item) {
$_smarty_tpl->tpl_vars['btn'] = $__foreach_btn_34_saved_item;
}
if ($__foreach_btn_34_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_btn_34_saved_key;
}
}?></div><?php
}
/* {/block 'footer'} */
/* {block 'defaultForm'}  file:form/form_form.tpl */
function block_196836327606434b7e36000_54593432($_smarty_tpl, $_blockParentStack) {
if (count($_smarty_tpl->tpl_vars['fields']->value['form']) > 0) {?><form method="post"
      name="<?php if (isset($_smarty_tpl->tpl_vars['fields']->value['form']['name'])) {
echo $_smarty_tpl->tpl_vars['fields']->value['form']['name'];
} elseif (isset($_smarty_tpl->tpl_vars['fields']->value['form']['id'])) {
echo $_smarty_tpl->tpl_vars['fields']->value['form']['id'];
} else {
echo $_smarty_tpl->tpl_vars['table']->value;
}?>"
      id="<?php if (isset($_smarty_tpl->tpl_vars['fields']->value['form']['id'])) {
echo $_smarty_tpl->tpl_vars['fields']->value['form']['id'];
} elseif (isset($_smarty_tpl->tpl_vars['fields']->value['form']['name'])) {
echo $_smarty_tpl->tpl_vars['fields']->value['form']['name'];
} else {
echo $_smarty_tpl->tpl_vars['table']->value;
}?>"
      class="defaultForm form-horizontal <?php if (isset($_smarty_tpl->tpl_vars['fields']->value['tabs'])) {?>row<?php }?> <?php if ($_smarty_tpl->tpl_vars['view']->value) {?> view<?php }
if (isset($_smarty_tpl->tpl_vars['fields']->value['tabs'])) {?> col-lg-10<?php }
if (isset($_smarty_tpl->tpl_vars['fields']->value['form']['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['fields']->value['form']['class'];
}?>"
      novalidate><?php }
$_from = $_smarty_tpl->tpl_vars['fields']->value['form'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_form_1_saved_item = isset($_smarty_tpl->tpl_vars['form']) ? $_smarty_tpl->tpl_vars['form'] : false;
$__foreach_form_1_saved_key = isset($_smarty_tpl->tpl_vars['form_tab_id']) ? $_smarty_tpl->tpl_vars['form_tab_id'] : false;
$_smarty_tpl->tpl_vars['form'] = new Smarty_Variable();
$__foreach_form_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_form_1_total) {
$_smarty_tpl->tpl_vars['form_tab_id'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['form_tab_id']->value => $_smarty_tpl->tpl_vars['form']->value) {
$__foreach_form_1_saved_local_item = $_smarty_tpl->tpl_vars['form'];
if ($_smarty_tpl->tpl_vars['key']->value == 'name') {
continue 1;
}
if ($_smarty_tpl->tpl_vars['form_tab_id']->value !== 'class') {?><div<?php if ($_smarty_tpl->tpl_vars['form']->value['tab_id']) {?> id="<?php echo $_smarty_tpl->tpl_vars['form']->value['tab_id'];?>
"<?php }?> class="tab-count tabs_<?php echo $_smarty_tpl->tpl_vars['form']->value['tab'];
if ($_smarty_tpl->tpl_vars['form']->value['tab_class']) {?> <?php echo $_smarty_tpl->tpl_vars['form']->value['tab_class'];
}
if ($_smarty_tpl->tpl_vars['form']->value['tab_col']) {?> col-lg-<?php echo $_smarty_tpl->tpl_vars['form']->value['tab_col'];
} else { ?> col-lg-12<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['fields']->value['tabs'])) {?> style="display:none"<?php }?>enctype="multipart/form-data"><div class="panel panel-default<?php if (isset($_smarty_tpl->tpl_vars['form']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['form']->value['class'];
}?>"><?php $_smarty_tpl->tpl_vars['body_display'] = new Smarty_Variable(false, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'body_display', 0);
$_from = $_smarty_tpl->tpl_vars['form']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_form_v_2_saved_item = isset($_smarty_tpl->tpl_vars['form_v']) ? $_smarty_tpl->tpl_vars['form_v'] : false;
$__foreach_form_v_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['form_v'] = new Smarty_Variable();
$__foreach_form_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_form_v_2_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['form_v']->value) {
$__foreach_form_v_2_saved_local_item = $_smarty_tpl->tpl_vars['form_v'];
if ($_smarty_tpl->tpl_vars['key']->value == 'html') {
echo $_smarty_tpl->tpl_vars['form_v']->value;
} elseif ($_smarty_tpl->tpl_vars['key']->value == 'tpl') {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, $_smarty_tpl->tpl_vars['form_v']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
} elseif ($_smarty_tpl->tpl_vars['key']->value == 'legend') {
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "legend", array (
  0 => 'block_105812057606434b7e43880_27101297',
  1 => false,
  3 => 0,
  2 => 0,
), $_blockParentStack);
} elseif ($_smarty_tpl->tpl_vars['key']->value == 'description' && $_smarty_tpl->tpl_vars['form_v']->value) {?><div class="alert alert-info"><?php echo $_smarty_tpl->tpl_vars['form_v']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button> </div><?php } elseif ($_smarty_tpl->tpl_vars['key']->value == 'warning' && $_smarty_tpl->tpl_vars['form_v']->value) {?><div class="alert alert-warning"><?php echo $_smarty_tpl->tpl_vars['form_v']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button> </div><?php } elseif ($_smarty_tpl->tpl_vars['key']->value == 'success' && $_smarty_tpl->tpl_vars['form_v']->value) {?><div class="alert alert-success"><?php echo $_smarty_tpl->tpl_vars['form_v']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button> </div><?php } elseif ($_smarty_tpl->tpl_vars['key']->value == 'error' && $_smarty_tpl->tpl_vars['form_v']->value) {?><div class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['form_v']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button> </div><?php } elseif ($_smarty_tpl->tpl_vars['key']->value == 'input') {?><div class="form-wrapper"><?php
$_from = $_smarty_tpl->tpl_vars['form_v']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_3_saved_item = isset($_smarty_tpl->tpl_vars['input']) ? $_smarty_tpl->tpl_vars['input'] : false;
$_smarty_tpl->tpl_vars['input'] = new Smarty_Variable();
$__foreach_input_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_input_3_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['input']->value) {
$__foreach_input_3_saved_local_item = $_smarty_tpl->tpl_vars['input'];
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "input_row", array (
  0 => 'block_64105413606434b7e4ba12_18162523',
  1 => false,
  3 => 0,
  2 => 0,
), $_blockParentStack);
$_smarty_tpl->tpl_vars['input'] = $__foreach_input_3_saved_local_item;
}
}
if ($__foreach_input_3_saved_item) {
$_smarty_tpl->tpl_vars['input'] = $__foreach_input_3_saved_item;
}
?></div><?php }
$_smarty_tpl->tpl_vars['form_v'] = $__foreach_form_v_2_saved_local_item;
}
}
if ($__foreach_form_v_2_saved_item) {
$_smarty_tpl->tpl_vars['form_v'] = $__foreach_form_v_2_saved_item;
}
if ($__foreach_form_v_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_form_v_2_saved_key;
}
?>
				<?php if ($_smarty_tpl->tpl_vars['body_display']->value) {?></div><?php }?>
			<?php if ($_smarty_tpl->tpl_vars['form']->value['footer_display']) {?>
				<?php 
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "footer", array (
  0 => 'block_1422392169606434b7f331a8_70428042',
  1 => false,
  3 => 0,
  2 => 0,
), $_blockParentStack);
}?></div>
	</div><?php if (!empty($_smarty_tpl->tpl_vars['submit_action']->value)) {?><input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['submit_action']->value;?>
" value="1"/><?php }
}
$_smarty_tpl->tpl_vars['form'] = $__foreach_form_1_saved_local_item;
}
}
if ($__foreach_form_1_saved_item) {
$_smarty_tpl->tpl_vars['form'] = $__foreach_form_1_saved_item;
}
if ($__foreach_form_1_saved_key) {
$_smarty_tpl->tpl_vars['form_tab_id'] = $__foreach_form_1_saved_key;
}
if (count($_smarty_tpl->tpl_vars['fields']->value['form']) > 0) {?><input type="hidden" name="post_code" value="<?php echo $_smarty_tpl->tpl_vars['post_code']->value;?>
"></form><?php }
}
/* {/block 'defaultForm'} */
}
