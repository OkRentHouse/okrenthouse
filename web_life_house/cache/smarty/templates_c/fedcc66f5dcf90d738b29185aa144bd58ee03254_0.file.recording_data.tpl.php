<?php
/* Smarty version 3.1.28, created on 2021-05-07 10:53:07
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/recording_data.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6094ab938eba87_72602854',
  'file_dependency' => 
  array (
    'fedcc66f5dcf90d738b29185aa144bd58ee03254' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/recording_data.tpl',
      1 => 1620355975,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6094ab938eba87_72602854 ($_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['recording_data']->value)) {?>
    
    <?php
$_from = $_smarty_tpl->tpl_vars['recording_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
        <div class="input-group fixed-width-lg">
            <span class="input-group-addon">狀態記錄</span>
            <input type="text" style="color:red" class="form-control" name="newtype" id="newtype" value="<?php echo $_smarty_tpl->tpl_vars['new_houst_type']->value;?>
" disabled="disable">
            <span class="input-group-addon">執行時間</span>
            <input class="form-control" type="date" name="note[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['note'];?>
" disabled="disabled">
            <span class="input-group-addon">記錄人:<?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</span>
            <input class="form-control" type="hidden" name="first_name[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_admin'];?>
" disabled="disabled">
            <span class="input-group-addon">記錄時間</span>
            <input class="form-control" type="date" name="create_time[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['create_time'];?>
" disabled="disabled">
            <input class="form-control" type="hidden" name="create_time[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['create_time'];?>
" disabled="disabled">
        </div>
        <div class="input-group fixed-width-lg">
            <span class="input-group-addon">原因</span>
            <input class="form-control" type="text" name="description[]" style="color:red" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['reason'];?>
" maxlength="64" disabled="disabled">
            <input class="form-control" type="hidden" name="log_type[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['record'];?>
" disabled="disabled">
        </div>
        <div class="input-group fixed-width-lg">
            <span class="input-group-addon">敘述</span>
            <textarea  class="form-control"  name="description[]" disabled="disabled"><?php echo $_smarty_tpl->tpl_vars['v']->value['description'];?>
</textarea>
            <input class="form-control" type="hidden" name="log_type[]" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['record'];?>
" disabled="disabled">
        </div>
    <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
}?>
<br/><br/>
<div class="input-group fixed-width-lg">
    <span class="input-group-addon">狀態記錄</span>
    <input type="text" style="color:red" class="form-control" name="newtype" id="newtype" value="<?php echo $_smarty_tpl->tpl_vars['new_houst_type']->value;?>
" disabled="disable">
    <input type="hidden" style="color:red" class="form-control" name="newtype1" id="newtype1" value="<?php echo $_smarty_tpl->tpl_vars['new_houst_type']->value;?>
">
    
    <span class="input-group-addon">執行時間</span>
    <input class="form-control" type="date" name="note[]">
    <span class="input-group-addon">記錄人:<?php echo $_SESSION['name'];?>
</span>
    <input class="form-control" type="hidden" name="first_name[]" value="<?php echo $_SESSION['id_admin'];?>
">
    <span class="input-group-addon">記錄時間</span>
    <input class="form-control" type="date" name="create_time[]" value="<?php echo date("Y-m-d");?>
" disabled="disabled">
    <input class="form-control" type="hidden" name="create_time[]" value="<?php echo date("Y-m-d");?>
">
    <span class="input-group-addon" data-date="<?php echo date("Y-m-d");?>
" data-id_admin="<?php echo $_SESSION['id_admin'];?>
" data-name="<?php echo $_SESSION['name'];?>
"  onclick="add_data_record(this)" >+</span>
</div>
<div class="input-group fixed-width-lg">
    <span class="input-group-addon">原因</span>
    <input type="radio" name="reason" value="新建案">新建案
    <input type="radio" name="reason" value="重新上架">重新上架
    <input type="radio" id="reason" name="reason" value="成交">成交
      <input type="radio" id="reason" name="reason" value="邀約暫停">邀約暫停
      <input type="radio" id="reason" name="reason" value="出租下架">出租下架
      <input type="radio" id="reason" name="reason" value="停租轉售">停租轉售
      <input type="radio" id="reason" name="reason" value="黑名單">黑名單
    <input type="radio" name="reason" value="其他">其他

</div>
<div class="input-group fixed-width-lg">
    <span class="input-group-addon">敘述</span>
    <textarea class="form-control" type="text" name="description[]"></textarea>
    <input class="form-control" type="hidden" name="log_type[]" value="record" disabled="disabled">
</div>
<?php }
}
