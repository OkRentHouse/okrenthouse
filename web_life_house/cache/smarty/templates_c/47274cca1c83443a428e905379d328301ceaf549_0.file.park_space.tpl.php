<?php
/* Smarty version 3.1.28, created on 2021-03-31 12:13:43
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/park_space.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6063f6f749d2c8_35094201',
  'file_dependency' => 
  array (
    '47274cca1c83443a428e905379d328301ceaf549' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/park_space.tpl',
      1 => 1617163355,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f6f749d2c8_35094201 ($_smarty_tpl) {
?>
        <?php if (!empty($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']])) {?>
            <?php if (!empty($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_park_position")])) {?>
                <?php
$_from = $_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_park_position")];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_rv_0_saved_item = isset($_smarty_tpl->tpl_vars['rv']) ? $_smarty_tpl->tpl_vars['rv'] : false;
$__foreach_rv_0_saved_key = isset($_smarty_tpl->tpl_vars['rk']) ? $_smarty_tpl->tpl_vars['rk'] : false;
$_smarty_tpl->tpl_vars['rv'] = new Smarty_Variable();
$__foreach_rv_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_rv_0_total) {
$_smarty_tpl->tpl_vars['rk'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['rk']->value => $_smarty_tpl->tpl_vars['rv']->value) {
$__foreach_rv_0_saved_local_item = $_smarty_tpl->tpl_vars['rv'];
?>
                <div class="input-group fixed-width-lg">
                    <?php if ($_smarty_tpl->tpl_vars['rk']->value == 0) {?>
                    <span class="input-group-addon">位置</span>
                    <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_park_position[]">
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_park_position")][$_smarty_tpl->tpl_vars['rk']->value] == "0") {?>selected="selected"<?php }?>>社區內</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_park_position")][$_smarty_tpl->tpl_vars['rk']->value] == "1") {?>selected="selected"<?php }?>>社區外</option>
                    </select>
                    <span class="input-group-addon">類型</span>
                    <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_type[]">
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "0") {?>selected="selected"<?php }?>>坡道平面</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "1") {?>selected="selected"<?php }?>>坡道機械</option>
                        <option value="2" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "2") {?>selected="selected"<?php }?>>升降平面</option>
                        <option value="3" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "3") {?>selected="selected"<?php }?>>升降機械</option>
                        <option value="4" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "4") {?>selected="selected"<?php }?>>一樓平面</option>
                    </select>
                    <span class="input-group-addon">編號</span><input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_serial[]" value="<?php echo $_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_serial")][$_smarty_tpl->tpl_vars['rk']->value];?>
" maxlength="20">
                    <span class="input-group-addon" data-name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" onclick="add_data_park_space(this)" >+</span>
                </div>
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">租金</span><input class="form-control" type="number" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent[]" value="<?php echo $_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent")][$_smarty_tpl->tpl_vars['rk']->value];?>
">
                    <span class="input-group-addon">繳費方式</span>
                    <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent_type[]">
                        <option value="0" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "0") {?>selected="selected"<?php }?>>月</option>
                        <option value="1" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "1") {?>selected="selected"<?php }?>>季</option>
                        <option value="2" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "2") {?>selected="selected"<?php }?>>半年</option>
                        <option value="3" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "3") {?>selected="selected"<?php }?>>年</option>
                        <option value="4" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == '') {?>selected="selected"<?php }?>>年</option>
                       <option value="4" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "4") {?>selected="selected"<?php }?>>包含管理費</option>
                    </select>
                    <span class="input-group-addon">備註</span>
                    <input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_remarks[]" value="<?php echo $_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_remarks")][$_smarty_tpl->tpl_vars['rk']->value];?>
" maxlength="64">
                </div>
                <?php } else { ?>
            <span class="input-group-addon">位置</span>
            <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_park_position[]">
                <option value="0" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_park_position")][$_smarty_tpl->tpl_vars['rk']->value] == "0") {?>selected="selected"<?php }?>>社區內</option>
                <option value="1" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_park_position")][$_smarty_tpl->tpl_vars['rk']->value] == "1") {?>selected="selected"<?php }?>>社區外</option>
            </select>
            <span class="input-group-addon">類型</span>
            <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_type[]">
                <option value="0" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "0") {?>selected="selected"<?php }?>>坡道平面</option>
                <option value="1" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "1") {?>selected="selected"<?php }?>>坡道機械</option>
                <option value="2" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "2") {?>selected="selected"<?php }?>>升降平面</option>
                <option value="3" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "3") {?>selected="selected"<?php }?>>升降機械</option>
                <option value="4" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_type")][$_smarty_tpl->tpl_vars['rk']->value] == "4") {?>selected="selected"<?php }?>>一樓平面</option>
            </select>
            <span class="input-group-addon">編號</span><input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_serial[]" value="<?php echo $_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_serial")][$_smarty_tpl->tpl_vars['rk']->value];?>
" maxlength="20">
            <span class="input-group-addon" data-name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" onclick="delete_data_park_space(this)" >-</span>
        </div>
        <div class="input-group fixed-width-lg">
            <span class="input-group-addon">租金</span><input class="form-control" type="number" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent[]" value="<?php echo $_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent")][$_smarty_tpl->tpl_vars['rk']->value];?>
">
            <span class="input-group-addon">繳費方式</span>
            <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent_type[]">
                <option value="0" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "0") {?>selected="selected"<?php }?>>月</option>
                <option value="1" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "1") {?>selected="selected"<?php }?>>季</option>
                <option value="2" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "2") {?>selected="selected"<?php }?>>半年</option>
                <option value="3" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "3") {?>selected="selected"<?php }?>>年</option>
               <option value="4" <?php if ($_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_rent_type")][$_smarty_tpl->tpl_vars['rk']->value] == "4") {?>selected="selected"<?php }?>>包含管理費</option>
            </select>
            <span class="input-group-addon">備註</span>
            <input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_remarks[]" value="<?php echo $_smarty_tpl->tpl_vars['park_space']->value[$_smarty_tpl->tpl_vars['input']->value['name']][($_smarty_tpl->tpl_vars['input']->value['name']).("_remarks")][$_smarty_tpl->tpl_vars['rk']->value];?>
" maxlength="64">
        </div>
        <?php }?>
        <?php
$_smarty_tpl->tpl_vars['rv'] = $__foreach_rv_0_saved_local_item;
}
}
if ($__foreach_rv_0_saved_item) {
$_smarty_tpl->tpl_vars['rv'] = $__foreach_rv_0_saved_item;
}
if ($__foreach_rv_0_saved_key) {
$_smarty_tpl->tpl_vars['rk'] = $__foreach_rv_0_saved_key;
}
?>
    <?php } else { ?>
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">位置</span>
                    <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_park_position[]">
                        <option value="0">社區內</option>
                        <option value="1">社區外</option>
                    </select>
                    <span class="input-group-addon">類型</span>
                    <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_type[]">
                        <option value="0">坡道平面</option>
                        <option value="1">坡道機械</option>
                        <option value="2">升降平面</option>
                        <option value="3">升降機械</option>
                        <option value="4">一樓平面</option>
                    </select>
                    <span class="input-group-addon">編號</span><input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_serial[]" value="" maxlength="20">
                    <span class="input-group-addon" data-name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" onclick="add_data_park_space(this)" >+</span>
                </div>
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">租金</span><input class="form-control" type="number" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent[]" value="">
                    <span class="input-group-addon">繳費方式</span>
                    <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent_type[]">
                        <option value="0">月</option>
                        <option value="1">季</option>
                        <option value="2">半年</option>
                        <option value="3">年</option>
                       <option value="4">包含管理費</option>
                    </select>
                    <span class="input-group-addon">備註</span>
                    <input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_remarks[]" value="" maxlength="64">
                </div>
    <?php }?>
        <?php } else { ?>
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">位置</span>
                <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_park_position[]">
                    <option value="0">社區內</option>
                    <option value="1">社區外</option>
                </select>
                <span class="input-group-addon">類型</span>
                <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_type[]">
                    <option value="0">坡道平面</option>
                    <option value="1">坡道機械</option>
                    <option value="2">升降平面</option>
                    <option value="3">升降機械</option>
                    <option value="4">一樓平面</option>
                </select>
                <span class="input-group-addon">編號</span><input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_serial[]" value="" maxlength="20">
                <span class="input-group-addon" data-name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
" onclick="add_data_park_space(this)" >+</span>
            </div>
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">租金</span><input class="form-control" type="number" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent[]" value="">
                <span class="input-group-addon">繳費方式</span>
                <select class="form-control" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_rent_type[]">
                    <option value="0">月</option>
                    <option value="1">季</option>
                    <option value="2">半年</option>
                    <option value="3">年</option>
                   <option value="4">包含管理費</option>
                </select>
                <span class="input-group-addon">備註</span>
                <input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
_remarks[]" value="" maxlength="64">
            </div>
        <?php }
}
}
