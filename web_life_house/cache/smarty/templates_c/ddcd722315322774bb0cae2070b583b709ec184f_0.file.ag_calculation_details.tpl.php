<?php
/* Smarty version 3.1.28, created on 2021-03-31 15:09:15
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/ag_calculation_details.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6064201bd7f249_67701633',
  'file_dependency' => 
  array (
    'ddcd722315322774bb0cae2070b583b709ec184f' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/ag_calculation_details.tpl',
      1 => 1617174547,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6064201bd7f249_67701633 ($_smarty_tpl) {
?>
<div class="panel panel-default">
        <div class="panel-heading" <?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
 onclick="collapse_data(this)"><?php echo $_smarty_tpl->tpl_vars['form']->value['title'];?>
(點選收合)</div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">
                <table width="100%" border="1">
                <tr>
                    <td>開發 AG</td>
                    <td>詳情</td>
                </tr>
                <?php
$_from = $_smarty_tpl->tpl_vars['ag_recode2']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['v']->value['ag'];?>
 <span style=color:red>(<?php echo $_smarty_tpl->tpl_vars['v']->value['total'];?>
)</span></td>
                    <td><a href="https://lifegroup.house/manage/RentHouseAGDetails" target="_blank">連結</a></td>
                </tr>
                <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
                </table>
                <table>
                    <tr align="right">
                    <td style="color:red">總計：<?php echo $_smarty_tpl->tpl_vars['v']->value['total2'];?>
</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php }
}
