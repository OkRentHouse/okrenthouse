<?php
/* Smarty version 3.1.28, created on 2021-05-07 14:19:16
  from "/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/new_field.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6094dbe4becb18_46181376',
  'file_dependency' => 
  array (
    '0874129d583e2674bed3557726052f35eca72e1c' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/LifeHouse/new_field.tpl',
      1 => 1620368345,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6094dbe4becb18_46181376 ($_smarty_tpl) {
?>
<div class="panel panel-default">
        <div class="panel-heading" <?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
 onclick="collapse_data(this)"><?php echo $_smarty_tpl->tpl_vars['form']->value['title'];?>
(點選收合)
        </div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">

<?php echo '<script'; ?>
>
function clean(){
  document.getElementById("userName").value = '';
  document.getElementById("black_reason").value = '';
  document.getElementById("black_add").checked = false;
  document.getElementById("leasetitle").value = '';
  document.getElementById("birthday").value = '';
  document.getElementById("userID").value = '';
  document.getElementById("mobile1").value = '';
  document.getElementById("mobile2").value = '';
  document.getElementById("lineID").value = '';
  document.getElementById("tel_h").value = '';
  document.getElementById("tel_o").value = '';
  document.getElementById("residenceAddress").value = '';
  document.getElementById("contractAddress").value = '';
  document.getElementById("homeAddress").value = '';
  document.getElementById("businessAddress").value = '';
  document.getElementById("email").value = '';
  document.getElementById("bussinessNumber").value = '';
  document.getElementById("worktitle").value = '';
  document.getElementById("leaseCondition").value = '';
  document.getElementById("bankAccount1").value = '';
  document.getElementById("bankAccount2").value = '';
  document.getElementById("relation").value = '';
  document.getElementById("payRecord").value = '';
  document.getElementById("leaseRangeStart").value = '';
  document.getElementById("leaseRangeEnd").value = '';
  document.getElementById("rentMoney").value = '';
  document.getElementById("rent_other").value = '';
  document.getElementById("rent_other1").value = '';
  document.getElementById("rentPayDate").value = '';
  document.getElementById("lookOther").value = '';
  document.getElementById("bonds").value = '';
  document.getElementById("bondsRecord").value = '';
  document.getElementById("bondsPer").value = '';
  document.getElementById("leasefull").value = '';
  document.getElementById("ag123").value = '';
  document.getElementById("agencyPlan").value = '';
  document.getElementById("managePlan").value = '';
  document.getElementById("rentAndSale").value = '';
  document.getElementById("checkin").value = '';
  document.getElementById("checkinPeople").value = '';
  document.getElementById("breaklease").value = '';
  document.getElementById("rentAssoc").value = '';
  document.getElementById("addRent").value = '';
  document.getElementById("addCredit").value = '';
  document.getElementById("addCare").value = '';
  document.getElementById("buyPrice").value = '';
  document.getElementById("taoAssoc").value = '';
  document.getElementById("twAssoc").value = '';
  document.getElementById("lease_number").value = '';
  document.getElementById("userName_relation").value = '';
  document.getElementById("leasetitle_relation").value = '';
  document.getElementById("relation_relation").value = '';
  document.getElementById("birthday_relation").value = '';
  document.getElementById("userID_relation").value = '';
  document.getElementById("mobile1_relation").value = '';
  document.getElementById("mobile2_relation").value = '';
  document.getElementById("lineID_relation").value = '';
  document.getElementById("tel_h_relation").value = '';
  document.getElementById("tel_o_relation").value = '';
  document.getElementById("email_relation").value = '';
  
  
}

function clean2(){
  document.getElementById("lease_number").value = '';
  document.getElementById("userName_relation").value = '';
  document.getElementById("leasetitle_relation").value = '';
  document.getElementById("relation_relation").value = '';
  document.getElementById("birthday_relation").value = '';
  document.getElementById("userID_relation").value = '';
  document.getElementById("mobile1_relation").value = '';
  document.getElementById("mobile2_relation").value = '';
  document.getElementById("lineID_relation").value = '';
  document.getElementById("tel_h_relation").value = '';
  document.getElementById("tel_o_relation").value = '';
  document.getElementById("email_relation").value = '';
  document.getElementById("residenceAddress_relation").value = '';
  document.getElementById("homeAddress_relation").value = '';
  document.getElementById("businessAddress_relation").value = '';
  document.getElementById("bussinessNumber_relation").value = '';
  document.getElementById("worktitle_relation").value = '';
}
<?php echo '</script'; ?>
>
<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">房東</td>
    <td colspan="7">
    <input type="text" id="read_landlord_name" name="read_landlord_name" value="<?php echo $_smarty_tpl->tpl_vars['read_landlord_name']->value;?>
" size="10" readonly>
    <input type="hidden" id="read_landlord_id_member" name="read_landlord_id_member" value="<?php echo $_smarty_tpl->tpl_vars['read_landlord_id_member']->value;?>
" size="10" readonly>
    </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租約:</td>
    <td><input type="text" id="lease" name="lease" value="<?php echo $_smarty_tpl->tpl_vars['lease_house_code']->value;?>
" size="10" readonly></td>
    <td class="main_t_2 input-group-addon" style="color:red">順序:<br/>目前合約編號:<?php echo $_smarty_tpl->tpl_vars['leaseSequence']->value;?>
<input type="hidden" id="update_lease_id" name="update_lease_id" value="<?php echo $_smarty_tpl->tpl_vars['leaseSequence']->value;?>
"></td>
    <td><input type="text" id="leaseSequence" name="leaseSequence" value="" size="10"></td>
    <td class="main_t_2 input-group-addon">來源:</td>
    <td><select class="form-control" name="source" >
                            <option value="">請選擇來源</option>
                            <?php
$_from = $_smarty_tpl->tpl_vars['database_source']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_s_v_0_saved_item = isset($_smarty_tpl->tpl_vars['d_s_v']) ? $_smarty_tpl->tpl_vars['d_s_v'] : false;
$__foreach_d_s_v_0_saved_key = isset($_smarty_tpl->tpl_vars['d_s_k']) ? $_smarty_tpl->tpl_vars['d_s_k'] : false;
$_smarty_tpl->tpl_vars['d_s_v'] = new Smarty_Variable();
$__foreach_d_s_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_s_v_0_total) {
$_smarty_tpl->tpl_vars['d_s_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_s_k']->value => $_smarty_tpl->tpl_vars['d_s_v']->value) {
$__foreach_d_s_v_0_saved_local_item = $_smarty_tpl->tpl_vars['d_s_v'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['source'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_s_v']->value['source'];?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_0_saved_local_item;
}
}
if ($__foreach_d_s_v_0_saved_item) {
$_smarty_tpl->tpl_vars['d_s_v'] = $__foreach_d_s_v_0_saved_item;
}
if ($__foreach_d_s_v_0_saved_key) {
$_smarty_tpl->tpl_vars['d_s_k'] = $__foreach_d_s_v_0_saved_key;
}
?>
                        </select></td>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text" id="userName" name="userName" value="<?php echo $_smarty_tpl->tpl_vars['userName']->value;?>
" size="10"></td>
  </tr>
  <tr>
  <td class="main_t_2 input-group-addon" style="color:red">是否黑名單:</td>
  <td><input type="checkbox" id="black_add" name="black_add" value="1" <?php if ($_smarty_tpl->tpl_vars['black_ck']->value == "1") {?> checked <?php }?> style="margin:5px;"></td>
  <td class="main_t_2 input-group-addon">原因:</td>
  <td><input type="text" id="black_reason" name="black_reason" value="<?php echo $_smarty_tpl->tpl_vars['black_reason']->value;?>
" style="width:100%;"></td>
  <td class="main_t_2 input-group-addon">紀錄時間:</td>
  <td><input type="date" id="recodeTime" name="recodeTime" value="<?php echo $_smarty_tpl->tpl_vars['black_recodeTime']->value;?>
"></td>
  <td class="main_t_2 input-group-addon">紀錄人:</td>
  <td><input type="text" id="black_cp" name="black_cp" value="<?php echo $_SESSION['name'];?>
" style="width:100%;" readonly></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text" id="leasetitle" name="leasetitle" value="<?php echo $_smarty_tpl->tpl_vars['leasetitle']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">生日:</td>
    <td><input type="date" id="birthday" name="birthday" value="<?php echo $_smarty_tpl->tpl_vars['birthday']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text" id="userID" name="userID" value="<?php echo $_smarty_tpl->tpl_vars['userID']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">手機1:</td>
    <td><input type="text" id="mobile1" name="mobile1" value="<?php echo $_smarty_tpl->tpl_vars['mobile1']->value;?>
" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text" id="mobile2" name="mobile2" value="<?php echo $_smarty_tpl->tpl_vars['mobile2']->value;?>
" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text" id="lineID" name="lineID" value="<?php echo $_smarty_tpl->tpl_vars['lineID']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" id="tel_h" name="tel_h" value="<?php echo $_smarty_tpl->tpl_vars['tel_h']->value;?>
" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text" id="tel_o" name="tel_o" value="<?php echo $_smarty_tpl->tpl_vars['tel_o']->value;?>
" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text" id="residenceAddress" name="residenceAddress" value="<?php echo $_smarty_tpl->tpl_vars['residenceAddress']->value;?>
" size="50"></td>
    <td class="main_t_2 input-group-addon">聯絡地址:</td>
    <td colspan="3"><input type="text" id="contractAddress" name="contractAddress" value="<?php echo $_smarty_tpl->tpl_vars['contractAddress']->value;?>
" size="50"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text" id="homeAddress" name="homeAddress" value="<?php echo $_smarty_tpl->tpl_vars['homeAddress']->value;?>
" size="50"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" id="businessAddress" name="businessAddress" value="<?php echo $_smarty_tpl->tpl_vars['businessAddress']->value;?>
" size="50"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" id="email" name="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text" id="bussinessNumber" name="bussinessNumber" value="<?php echo $_smarty_tpl->tpl_vars['bussinessNumber']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text" id="worktitle" name="worktitle" value="<?php echo $_smarty_tpl->tpl_vars['worktitle']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">租約條件:</td>
    <td><input type="text" id="leaseCondition" name="leaseCondition" value="<?php echo $_smarty_tpl->tpl_vars['leaseCondition']->value;?>
" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入帳帳戶1:</td>
    <td><input type="text" id="bankAccount1" name="bankAccount1" value="<?php echo $_smarty_tpl->tpl_vars['bankAccount1']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">入帳帳戶2:</td>
    <td><input type="text" id="bankAccount2" name="bankAccount2" value="<?php echo $_smarty_tpl->tpl_vars['bankAccount2']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text" id="relation" name="relation" value="<?php echo $_smarty_tpl->tpl_vars['relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">繳租記錄:</td>
    <td><input type="text" id="payRecord" name="payRecord" value="<?php echo $_smarty_tpl->tpl_vars['payRecord']->value;?>
" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租期:</td>
    <td>起<input type="date" id="leaseRangeStart" name="leaseRangeStart" value="<?php echo $_smarty_tpl->tpl_vars['leaseRangeStart']->value;?>
" size="10"><br/>結<input type="date" id="leaseRangeEnd" name="leaseRangeEnd" value="<?php echo $_smarty_tpl->tpl_vars['leaseRangeEnd']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">租金:</td>
    <td><input type="text" id="rentMoney" name="rentMoney" value="<?php echo $_smarty_tpl->tpl_vars['rentMoney']->value;?>
" onKeyUp="value=value.replace(/[^\d]/g,'')" size="10"></td>
    <td class="main_t_2 input-group-addon">其他：</td>
    <td><input type="text" id="rent_other" name="rent_other" value="<?php echo $_smarty_tpl->tpl_vars['rent_other']->value;?>
" size="10"><br/>
    <?php if ($_smarty_tpl->tpl_vars['rent_other1']->value == "0") {?>
      <input type="checkbox" id="rent_other1" name="rent_other1" value="0" checked/>
      車位
      <?php } else { ?>
  <input type="radio" id="rent_other1" name="rent_other1" value="0" />
      車位
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['rent_other1']->value == "1") {?>
      <input type="radio" id="rent_other1" name="rent_other1" value="1" checked/>
      管理費
      <?php } else { ?>
  <input type="radio" id="rent_other1" name="rent_other1" value="1" />
      管理費
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['rent_other1']->value == "2") {?>
      <input type="radio" id="rent_other1" name="rent_other1" value="2" checked/>
      兩者
      <?php } else { ?>
  <input type="radio" id="rent_other1" name="rent_other1" value="2" />
      兩者
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['rent_other1']->value == "3") {?>
      <input type="radio" id="rent_other1" name="rent_other1" value="3" checked/>
      取消
      <?php } else { ?>
  <input type="radio" id="rent_other1" name="rent_other1" value="3" />
      取消
      <?php }?>
    </td>
    <td class="main_t_2 input-group-addon">繳租日:</td>
    <td><input type="text" id="rentPayDate" name="rentPayDate" value="<?php echo $_smarty_tpl->tpl_vars['rentPayDate']->value;?>
" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">帶看配合:</td>
    <td><input type="text" id="lookOther" name="lookOther" value="<?php echo $_smarty_tpl->tpl_vars['lookOther']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">押金金額:</td>
    <td><input type="text" id="bonds" name="bonds" value="<?php echo $_smarty_tpl->tpl_vars['bonds']->value;?>
" onKeyUp="value=value.replace(/[^\d]/g,'')" size="10"></td>
    <td class="main_t_2 input-group-addon">押金繳交記錄:</td>
    <td><input type="text" id="bondsRecord" name="bondsRecord" value="<?php echo $_smarty_tpl->tpl_vars['bondsRecord']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">押金擔保:</td>
    <td><input type="text" id="bondsPer" name="bondsPer" value="<?php echo $_smarty_tpl->tpl_vars['bondsPer']->value;?>
" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">期滿退租:</td>
    <td><input type="text" id="leasefull" name="leasefull" value="<?php echo $_smarty_tpl->tpl_vars['leasefull']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">AG:</td>
    <td><input type="text" id="ag123" name="ag123" value="<?php echo $_smarty_tpl->tpl_vars['ag']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">仲介方案:</td>
    <td> 
      <?php if ($_smarty_tpl->tpl_vars['agencyPlan']->value == "A") {?>
      <input type="radio" id="agencyPlan" name="agencyPlan" value="甲" checked/>
      甲
      <?php } else { ?>
  <input type="radio" id="agencyPlan" name="agencyPlan" value="甲" />
      甲
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['agencyPlan']->value == "B") {?>
      <input type="radio" id="agencyPlan" name="agencyPlan" value="乙" checked/>
      乙
      <?php } else { ?>
  <input type="radio" id="agencyPlan" name="agencyPlan" value="乙" />
      乙
      <?php }?> </td>
    <td class="main_t_2 input-group-addon">代管方案:</td>
    <td> 
      <?php if ($_smarty_tpl->tpl_vars['managePlan']->value == "A") {?>
      <input type="radio" id="managePlan" name="managePlan" value="A" checked/>
      A
      <?php } else { ?>
  <input type="radio" id="managePlan" name="managePlan" value="A" />
      A
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['managePlan']->value == "B") {?>
      <input type="radio" id="managePlan" name="managePlan" value="B" checked/>
      B
      <?php } else { ?>
  <input type="radio" id="managePlan" name="managePlan" value="B" />
      B
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['managePlan']->value == "C") {?>
      <input type="radio" id="managePlan" name="managePlan" value="C" checked/>
      C
      <?php } else { ?>
  <input type="radio" id="managePlan" name="managePlan" value="C" />
      C
      <?php }?> </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租售同步:</td>
    <td><input type="text" id="rentAndSale" name="rentAndSale" value="<?php echo $_smarty_tpl->tpl_vars['rentAndSale']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">點 in 日期:</td>
    <td><input type="date" id="checkin" name="checkin" value="<?php echo $_smarty_tpl->tpl_vars['checkin']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">點交人員:</td>
    <td><input type="text" id="checkinPeople" name="checkinPeople" value="<?php echo $_smarty_tpl->tpl_vars['checkinPeople']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">中途解約:</td>
    <td><input type="text" id="breaklease" name="breaklease" value="<?php echo $_smarty_tpl->tpl_vars['breaklease']->value;?>
" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租管工會:</td>
    <td><input type="text" id="rentAssoc" name="rentAssoc" value="<?php echo $_smarty_tpl->tpl_vars['rentAssoc']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">加值 - 租霸補助:</td>
    <td><input type="text" id="addRent" name="addRent" value="<?php echo $_smarty_tpl->tpl_vars['addRent']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">加值 - 信用認證:</td>
    <td><input type="text" id="addCredit" name="addCredit" value="<?php echo $_smarty_tpl->tpl_vars['addCredit']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">加值 - 安心履約:</td>
    <td><input type="text" id="addCare" name="addCare" value="<?php echo $_smarty_tpl->tpl_vars['addCare']->value;?>
" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入手價:</td>
    <td><input type="text" id="buyPrice3" name="buyPrice2" value="<?php echo $_smarty_tpl->tpl_vars['buyPrice']->value;?>
" onKeyUp="value=value.replace(/[^\d]/g,'')" size="10"></td>
    <td class="main_t_2 input-group-addon">桃協會:</td>
    <td><input type="text" id="taoAssoc3" name="taoAssoc2" value="<?php echo $_smarty_tpl->tpl_vars['taoAssoc']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">台協會:</td>
    <td><input type="text" id="twAssoc3" name="twAssoc2" value="<?php echo $_smarty_tpl->tpl_vars['twAssoc']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon" style="color:red">契約編號</td>
    <td><input style="color:red" type="text" id="lease_number" name="lease_number" value="<?php echo $_smarty_tpl->tpl_vars['lease_number']->value;?>
" size="10"/></td>
  </tr>
  <tr>
    <td colspan="6">

      <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label for="member_landlord_id_other_conditions">其他條件(複選)</label>
                    <div>
                        <div class="btn-group" data-toggle="buttons">
                        
                            <?php
$_from = $_smarty_tpl->tpl_vars['database_other_conditions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_o_c_v_1_saved_item = isset($_smarty_tpl->tpl_vars['d_o_c_v']) ? $_smarty_tpl->tpl_vars['d_o_c_v'] : false;
$__foreach_d_o_c_v_1_saved_key = isset($_smarty_tpl->tpl_vars['d_o_c_k']) ? $_smarty_tpl->tpl_vars['d_o_c_k'] : false;
$_smarty_tpl->tpl_vars['d_o_c_v'] = new Smarty_Variable();
$__foreach_d_o_c_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_o_c_v_1_total) {
$_smarty_tpl->tpl_vars['d_o_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_o_c_k']->value => $_smarty_tpl->tpl_vars['d_o_c_v']->value) {
$__foreach_d_o_c_v_1_saved_local_item = $_smarty_tpl->tpl_vars['d_o_c_v'];
?>
                                <label class="btn btn-default checkbox" for="member_landlord_id_other_conditions_<?php echo $_smarty_tpl->tpl_vars['d_o_c_v']->value['id_other_conditions'];?>
"> <input type="checkbox" autocomplete="off" name="member_landlord_id_other_conditions[]"  value="<?php echo $_smarty_tpl->tpl_vars['d_o_c_v']->value['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['d_o_c_v']->value['title'];?>
</label>
                            <?php
$_smarty_tpl->tpl_vars['d_o_c_v'] = $__foreach_d_o_c_v_1_saved_local_item;
}
}
if ($__foreach_d_o_c_v_1_saved_item) {
$_smarty_tpl->tpl_vars['d_o_c_v'] = $__foreach_d_o_c_v_1_saved_item;
}
if ($__foreach_d_o_c_v_1_saved_key) {
$_smarty_tpl->tpl_vars['d_o_c_k'] = $__foreach_d_o_c_v_1_saved_key;
}
?>
                        
                        
                        </div>
                    </div>
                </div>
    </td>
    <td class="main_t_2 input-group-addon" style="color:red">更新人員</td>
    <td><input style="color:red" type="text" id="update_people2" name="update_people" value="<?php echo $_SESSION['name'];?>
" size="10" disabled="disabled"/></td>
  </tr>

</table>

<br/>
<br/>
<br/>

<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">新擔保人合約</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text" id="userName_relation" name="userName_relation" value="<?php echo $_smarty_tpl->tpl_vars['userName_relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text" id="leasetitle_relation" name="leasetitle_relation" value="<?php echo $_smarty_tpl->tpl_vars['leasetitle_relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text" id="relation_relation" name="relation_relation" value="<?php echo $_smarty_tpl->tpl_vars['relation_relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">生日:</td>
    <td><input type="date" id="birthday_relation" name="birthday_relation" value="<?php echo $_smarty_tpl->tpl_vars['birthday_relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text" id="userID_relation" name="userID_relation" value="<?php echo $_smarty_tpl->tpl_vars['userID_relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">手機1:</td>
    <td><input type="text" id="mobile1_relation" name="mobile1_relation" value="<?php echo $_smarty_tpl->tpl_vars['mobile1_relation']->value;?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text" id="mobile2_relation" name="mobile2_relation" value="<?php echo $_smarty_tpl->tpl_vars['mobile2_relation']->value;?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text" id="lineID_relation" name="lineID_relation" value="<?php echo $_smarty_tpl->tpl_vars['lineID_relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" id="tel_h_relation" name="tel_h_relation" value="<?php echo $_smarty_tpl->tpl_vars['tel_h_relation']->value;?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text" id="tel_o_relation" name="tel_o_relation" value="<?php echo $_smarty_tpl->tpl_vars['tel_o_relation']->value;?>
" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" id="email_relation" name="email_relation" value="<?php echo $_smarty_tpl->tpl_vars['email_relation']->value;?>
" size="10"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text" id="residenceAddress_relation" name="residenceAddress_relation" value="<?php echo $_smarty_tpl->tpl_vars['residenceAddress_relation']->value;?>
" size="50"></td>
    <td ></td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text" id="homeAddress_relation" name="homeAddress_relation" value="<?php echo $_smarty_tpl->tpl_vars['homeAddress_relation']->value;?>
" size="50"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" id="businessAddress_relation" name="businessAddress_relation" value="<?php echo $_smarty_tpl->tpl_vars['businessAddress_relation']->value;?>
" size="50"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text" id="bussinessNumber_relation" name="bussinessNumber_relation" value="<?php echo $_smarty_tpl->tpl_vars['bussinessNumber_relation']->value;?>
" size="10"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text" id="worktitle_relation" name="worktitle_relation" value="<?php echo $_smarty_tpl->tpl_vars['worktitle_relation']->value;?>
" size="10"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td>&nbsp;</td>
    <td style="color:red"><input type="button" onclick="clean();" value="清空租客所有欄位"></td>
    <td style="color:red"><input type="button" onclick="clean2();" value="清空履保人所有欄位"></td>
    <td class="main_t_2 input-group-addon" style="color:red">是否更新：</td>
    <td><input type="radio" id="updd" name="updd" value="0" checked />
      更新
      <input type="radio" id="updd" name="updd" value="1" />
      新增 </td>
  </tr>
</table>
</div>
</div>
</div>
<?php }
}
