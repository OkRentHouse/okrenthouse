<?php
/* Smarty version 3.1.28, created on 2021-04-20 17:31:07
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_list_tbody.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_607e9f5b3771b7_98339136',
  'file_dependency' => 
  array (
    '7947786df43339666d503dd36f079375696d4eff' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/form/form_list_tbody.tpl',
      1 => 1618911010,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_607e9f5b3771b7_98339136 ($_smarty_tpl) {
?>
<tbody><?php if (count($_smarty_tpl->tpl_vars['fields']->value['list_val']) > 0) {
$_from = $_smarty_tpl->tpl_vars['fields']->value['list_val'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_list_0_saved_item = isset($_smarty_tpl->tpl_vars['list']) ? $_smarty_tpl->tpl_vars['list'] : false;
$__foreach_list_0_saved_key = isset($_smarty_tpl->tpl_vars['fi']) ? $_smarty_tpl->tpl_vars['fi'] : false;
$_smarty_tpl->tpl_vars['list'] = new Smarty_Variable();
$__foreach_list_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_list_0_total) {
$_smarty_tpl->tpl_vars['fi'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['fi']->value => $_smarty_tpl->tpl_vars['list']->value) {
$__foreach_list_0_saved_local_item = $_smarty_tpl->tpl_vars['list'];
?><tr id="tr_<?php echo $_smarty_tpl->tpl_vars['fi']->value;?>
"<?php if (isset($_smarty_tpl->tpl_vars['list']->value['tr_class'])) {?> class="<?php echo $_smarty_tpl->tpl_vars['list']->value['tr_class'];?>
"<?php }?>><?php $_smarty_tpl->tpl_vars['td_i'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'td_i', 0);
$_from = $_smarty_tpl->tpl_vars['fields']->value['list'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_thead_1_saved_item = isset($_smarty_tpl->tpl_vars['thead']) ? $_smarty_tpl->tpl_vars['thead'] : false;
$__foreach_thead_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['thead'] = new Smarty_Variable();
$__foreach_thead_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_thead_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['thead']->value) {
$__foreach_thead_1_saved_local_item = $_smarty_tpl->tpl_vars['thead'];
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(str_replace('!','_',$_smarty_tpl->tpl_vars['i']->value), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if (isset($_smarty_tpl->tpl_vars['thead']->value['index'])) {
$_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'list', null);
$_smarty_tpl->tpl_vars['list']->value['index'] = $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'list', 0);
}
if (isset($_smarty_tpl->tpl_vars['thead']->value['index2'])) {
$_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'list', null);
$_smarty_tpl->tpl_vars['list']->value['index2'] = $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'list', 0);
}?><td class="<?php echo $_smarty_tpl->tpl_vars['thead']->value['class'];
if ($_smarty_tpl->tpl_vars['thead']->value['hidden']) {?> hidden<?php }
if (!$_smarty_tpl->tpl_vars['thead']->value['no_link'] && !$_smarty_tpl->tpl_vars['no_link']->value && !isset($_smarty_tpl->tpl_vars['thead']->value['type'])) {?> pointer<?php }?>" <?php if (!$_smarty_tpl->tpl_vars['thead']->value['no_link'] && !$_smarty_tpl->tpl_vars['no_link']->value && !isset($_smarty_tpl->tpl_vars['thead']->value['type']) && ($_smarty_tpl->tpl_vars['i']->value != 'position')) {?>onclick="document.location='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_action']->value, ENT_QUOTES, 'UTF-8', true);
if (in_array('list',$_smarty_tpl->tpl_vars['actions']->value)) {?>&amp;list<?php echo $_smarty_tpl->tpl_vars['table']->value;
} elseif (in_array('view',$_smarty_tpl->tpl_vars['actions']->value) && in_array('view',$_smarty_tpl->tpl_vars['post_processing']->value)) {?>&amp;view<?php echo $_smarty_tpl->tpl_vars['table']->value;
} elseif (in_array('edit',$_smarty_tpl->tpl_vars['actions']->value)) {?>&amp;edit<?php echo $_smarty_tpl->tpl_vars['table']->value;
} elseif (in_array('view',$_smarty_tpl->tpl_vars['actions']->value)) {?>&amp;view<?php echo $_smarty_tpl->tpl_vars['table']->value;
}?>&amp;<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];?>
'"<?php }?> style="<?php if ($_smarty_tpl->tpl_vars['thead']->value['style'] == true && isset($_smarty_tpl->tpl_vars['thead']->value['style'])) {
echo $_smarty_tpl->tpl_vars['thead']->value['style'];
}?>"><?php if (isset($_smarty_tpl->tpl_vars['thead']->value['type'])) {
if ($_smarty_tpl->tpl_vars['thead']->value['type'] == 'icon') {?><i class="<?php echo $_smarty_tpl->tpl_vars['list']->value['icon'];?>
"></i><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['type'] == 'img') {?><img name="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" src="<?php echo urldecode($_smarty_tpl->tpl_vars['list']->value['filename']);?>
" style="width: 200px;"><?php } elseif ($_smarty_tpl->tpl_vars['thead']->value['type'] == 'buttonx') {?><button type="<?php echo $_smarty_tpl->tpl_vars['thead']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['list']->value['values']['val'];?>
"><?php } else { ?><input type="<?php echo $_smarty_tpl->tpl_vars['thead']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];?>
"><?php }
} else {
if (isset($_smarty_tpl->tpl_vars['thead']->value['values'])) {
$_smarty_tpl->tpl_vars['in_arr'] = new Smarty_Variable(false, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'in_arr', 0);
if (isset($_smarty_tpl->tpl_vars['thead']->value['in_table']) || $_smarty_tpl->tpl_vars['thead']->value['in_table']) {
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value];
} else {
$_from = $_smarty_tpl->tpl_vars['thead']->value['values'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_val_v_2_saved_item = isset($_smarty_tpl->tpl_vars['val_v']) ? $_smarty_tpl->tpl_vars['val_v'] : false;
$__foreach_val_v_2_saved_key = isset($_smarty_tpl->tpl_vars['thv_i']) ? $_smarty_tpl->tpl_vars['thv_i'] : false;
$_smarty_tpl->tpl_vars['val_v'] = new Smarty_Variable();
$__foreach_val_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_val_v_2_total) {
$_smarty_tpl->tpl_vars['thv_i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['thv_i']->value => $_smarty_tpl->tpl_vars['val_v']->value) {
$__foreach_val_v_2_saved_local_item = $_smarty_tpl->tpl_vars['val_v'];
if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value] == $_smarty_tpl->tpl_vars['val_v']->value['value'] || preg_match($_smarty_tpl->tpl_vars['val_v']->value['like'],$_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value])) {
if ($_smarty_tpl->tpl_vars['thead']->value['getway'] == true && isset($_smarty_tpl->tpl_vars['thead']->value['getway'])) {?><button title="<?php if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value] == 1) {?>按下後 關閉<?php } else { ?>按下後 開啟<?php }?>" class="getway_<?php echo $_smarty_tpl->tpl_vars['fi']->value;?>
 glyphicon-getway <?php echo $_smarty_tpl->tpl_vars['val_v']->value['class'];
if ($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value] == 1) {?> btn-success<?php } else { ?> btn-danger<?php }?>" data-getway="<?php echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];?>
"><?php if ($_smarty_tpl->tpl_vars['val_v']->value['append']) {
$_smarty_tpl->tpl_vars['th'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['th']->value = 0;
if ($_smarty_tpl->tpl_vars['th']->value < sizeof($_smarty_tpl->tpl_vars['thead']->value['values'])) {
for ($_foo=true;$_smarty_tpl->tpl_vars['th']->value < sizeof($_smarty_tpl->tpl_vars['thead']->value['values']); $_smarty_tpl->tpl_vars['th']->value=$_smarty_tpl->tpl_vars['th']->value+1) {
if (preg_match($_smarty_tpl->tpl_vars['thead']->value['values'][$_smarty_tpl->tpl_vars['th']->value]['like'],$_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value])) {
echo $_smarty_tpl->tpl_vars['thead']->value['values'][$_smarty_tpl->tpl_vars['th']->value]['title'];
if ($_smarty_tpl->tpl_vars['th']->value < sizeof($_smarty_tpl->tpl_vars['thead']->value['values'])) {?><br><?php }
}
}
}
} else {
echo $_smarty_tpl->tpl_vars['val_v']->value['title'];
}?></button><?php $_smarty_tpl->tpl_vars['in_arr'] = new Smarty_Variable(true, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'in_arr', 0);
} else { ?><samp class="<?php echo $_smarty_tpl->tpl_vars['val_v']->value['class'];?>
"><?php if ($_smarty_tpl->tpl_vars['val_v']->value['append']) {
$_smarty_tpl->tpl_vars['th'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['th']->value = 0;
if ($_smarty_tpl->tpl_vars['th']->value < sizeof($_smarty_tpl->tpl_vars['thead']->value['values'])) {
for ($_foo=true;$_smarty_tpl->tpl_vars['th']->value < sizeof($_smarty_tpl->tpl_vars['thead']->value['values']); $_smarty_tpl->tpl_vars['th']->value=$_smarty_tpl->tpl_vars['th']->value+1) {
if (preg_match($_smarty_tpl->tpl_vars['thead']->value['values'][$_smarty_tpl->tpl_vars['th']->value]['like'],$_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value])) {
echo $_smarty_tpl->tpl_vars['thead']->value['values'][$_smarty_tpl->tpl_vars['th']->value]['title'];
if ($_smarty_tpl->tpl_vars['th']->value < sizeof($_smarty_tpl->tpl_vars['thead']->value['values'])) {?><br><?php }
}
}
}
} else {
echo $_smarty_tpl->tpl_vars['val_v']->value['title'];
}?></samp><?php $_smarty_tpl->tpl_vars['in_arr'] = new Smarty_Variable(true, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'in_arr', 0);
}
break 1;
}
$_smarty_tpl->tpl_vars['val_v'] = $__foreach_val_v_2_saved_local_item;
}
}
if ($__foreach_val_v_2_saved_item) {
$_smarty_tpl->tpl_vars['val_v'] = $__foreach_val_v_2_saved_item;
}
if ($__foreach_val_v_2_saved_key) {
$_smarty_tpl->tpl_vars['thv_i'] = $__foreach_val_v_2_saved_key;
}
if ($_smarty_tpl->tpl_vars['in_arr']->value == false) {?><samp>-</samp><?php }
}
} else {
if (isset($_smarty_tpl->tpl_vars['thead']->value['number_format'])) {
echo number_format($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value],$_smarty_tpl->tpl_vars['thead']->value['number_format'],"0",",");
} elseif (empty($_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value]) || $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value] == '0000-00-00' || $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value] == '0000-00-00 00:00:00') {?><samp>-</samp><?php } else {
echo $_smarty_tpl->tpl_vars['list']->value[$_smarty_tpl->tpl_vars['i']->value];
}
}
}?></td><?php if ($_smarty_tpl->tpl_vars['Edit_at_first']->value && $_smarty_tpl->tpl_vars['td_i']->value == 0) {?><td class="text-center edit_div"><?php if (in_array('preview',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_preview']) {?><a class="btn btn-default preview" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&preview<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"預覽"),$_smarty_tpl);?>
</a><?php }
if (in_array('edit',$_smarty_tpl->tpl_vars['actions']->value) && in_array('view',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_edit']) {?><a class="btn btn-default edit" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&edit<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"修改"),$_smarty_tpl);?>
</a><?php } elseif (in_array('view',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_view']) {?><a class="btn btn-default view" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&view<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"檢視"),$_smarty_tpl);?>
</a><?php }
if (in_array('del',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_del']) {?><a class="btn btn-default del" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&del<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"刪除"),$_smarty_tpl);?>
</a><?php }?></td><?php }
$_smarty_tpl->tpl_vars['td_i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['td_i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'td_i', 0);
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_1_saved_local_item;
}
}
if ($__foreach_thead_1_saved_item) {
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_1_saved_item;
}
if ($__foreach_thead_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_thead_1_saved_key;
}
if ($_smarty_tpl->tpl_vars['has_actions']->value && $_smarty_tpl->tpl_vars['display_edit_div']->value && !$_smarty_tpl->tpl_vars['Edit_at_first']->value) {?><td class="text-center edit_div"><?php if (in_array('preview',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_preview']) {?><a class="btn btn-default preview" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&preview<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"預覽"),$_smarty_tpl);?>
</a><?php }
if (in_array('edit',$_smarty_tpl->tpl_vars['actions']->value) && in_array('view',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_edit']) {?><a class="btn btn-default edit" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&edit<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"修改"),$_smarty_tpl);?>
</a><?php } elseif (in_array('view',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_view']) {?><a class="btn btn-default view" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&view<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"檢視"),$_smarty_tpl);?>
</a><?php }
if (in_array('del',$_smarty_tpl->tpl_vars['actions']->value) && !$_smarty_tpl->tpl_vars['list']->value['no_del']) {?><a class="btn btn-default del" href="<?php echo $_smarty_tpl->tpl_vars['form_action']->value;?>
&del<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index'];
if (isset($_smarty_tpl->tpl_vars['fields']->value['index2'])) {?>&<?php echo $_smarty_tpl->tpl_vars['fields']->value['index2'];?>
=<?php echo $_smarty_tpl->tpl_vars['list']->value['index2'];
}?>"><?php echo l(array('s'=>"刪除"),$_smarty_tpl);?>
</a><?php }?></td><?php }?></tr><?php
$_smarty_tpl->tpl_vars['list'] = $__foreach_list_0_saved_local_item;
}
}
if ($__foreach_list_0_saved_item) {
$_smarty_tpl->tpl_vars['list'] = $__foreach_list_0_saved_item;
}
if ($__foreach_list_0_saved_key) {
$_smarty_tpl->tpl_vars['fi'] = $__foreach_list_0_saved_key;
}
} else { ?><tr><td class="text-center" colspan="<?php if ($_smarty_tpl->tpl_vars['has_actions']->value && $_smarty_tpl->tpl_vars['display_edit_div']->value) {
echo count($_smarty_tpl->tpl_vars['fields']->value['list'])+1;
} else {
echo count($_smarty_tpl->tpl_vars['fields']->value['list']);
}?>"><?php echo $_smarty_tpl->tpl_vars['no_information']->value;?>
</td><tr><?php }?>
</tbody>
<?php }
}
