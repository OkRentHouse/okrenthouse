<?php
/* Smarty version 3.1.28, created on 2021-03-23 16:01:50
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Competence/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6059a06eef30c0_29851151',
  'file_dependency' => 
  array (
    '538be5fc7756edb7e9019da72078b54e91081658' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Competence/content.tpl',
      1 => 1592288973,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6059a06eef30c0_29851151 ($_smarty_tpl) {
?>
<form method="post" class="defaultForm form-horizontal col-lg-10" novalidate="novalidate">
<?php
$_from = $_smarty_tpl->tpl_vars['arr_group']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_group_0_saved_item = isset($_smarty_tpl->tpl_vars['group']) ? $_smarty_tpl->tpl_vars['group'] : false;
$__foreach_group_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['group'] = new Smarty_Variable();
$__foreach_group_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_group_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['group']->value) {
$__foreach_group_0_saved_local_item = $_smarty_tpl->tpl_vars['group'];
?>
	<?php if ($_smarty_tpl->tpl_vars['group']->value['id_group'] == $_SESSION['id_group']) {?>
		<div class="col-lg-12 panel-body tab-count tabs_group_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
"><?php echo l(array('s'=>"無法更改本身的群組權限!"),$_smarty_tpl);?>
</div>
	<?php } elseif ($_smarty_tpl->tpl_vars['group']->value['id_group'] != 1) {?>
	<div class="tab-count competence_tab tabs_group_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
 col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-cogs"></i><?php echo l(array('s'=>"功能表"),$_smarty_tpl);?>
</div>
			<div class="panel-body">
				<table>
					<thead>
					<tr class="h5" data-id_tab="all" data-id_parent="all">
						<td></td><?php
$_from = $_smarty_tpl->tpl_vars['type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_v_1_saved_item = isset($_smarty_tpl->tpl_vars['type_v']) ? $_smarty_tpl->tpl_vars['type_v'] : false;
$__foreach_type_v_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['type_v'] = new Smarty_Variable();
$__foreach_type_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_v_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['type_v']->value) {
$__foreach_type_v_1_saved_local_item = $_smarty_tpl->tpl_vars['type_v'];
?><td><label><input type="checkbox" class="on_ajax all_<?php echo $_smarty_tpl->tpl_vars['type_v']->value['code'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['type_v']->value['code'];?>
" data-id_group="<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
"<?php if (false) {?> checked="checked"<?php }?>><?php echo $_smarty_tpl->tpl_vars['type_v']->value['text'];?>
</label></td><?php
$_smarty_tpl->tpl_vars['type_v'] = $__foreach_type_v_1_saved_local_item;
}
}
if ($__foreach_type_v_1_saved_item) {
$_smarty_tpl->tpl_vars['type_v'] = $__foreach_type_v_1_saved_item;
}
if ($__foreach_type_v_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_type_v_1_saved_key;
}
?><td><label><input type="checkbox" class="on_ajax all_all" data-id_group="<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
" value="all"<?php if (false) {?> checked="checked"<?php }?>>全部</label></td>
					</tr>
					</thead>
					<tbody>
					<?php
$_from = $_smarty_tpl->tpl_vars['arr_tab']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tab_2_saved_item = isset($_smarty_tpl->tpl_vars['tab']) ? $_smarty_tpl->tpl_vars['tab'] : false;
$__foreach_tab_2_saved_key = isset($_smarty_tpl->tpl_vars['j']) ? $_smarty_tpl->tpl_vars['j'] : false;
$_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable();
$__foreach_tab_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_tab_2_total) {
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['tab']->value) {
$__foreach_tab_2_saved_local_item = $_smarty_tpl->tpl_vars['tab'];
?>
						<?php if (!in_array($_smarty_tpl->tpl_vars['tab']->value['id_tab'],$_smarty_tpl->tpl_vars['arr_no_tab']->value)) {?>
							<tr class="h6<?php if ($_smarty_tpl->tpl_vars['tab']->value['id_parent'] == 0) {?> parent<?php } else { ?> child<?php }?>" data-id_tab="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id_tab'];?>
" data-id_parent="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id_parent'];?>
">
								<td><?php echo $_smarty_tpl->tpl_vars['tab']->value['title'];?>
</td><?php
$_from = $_smarty_tpl->tpl_vars['type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_v_3_saved_item = isset($_smarty_tpl->tpl_vars['type_v']) ? $_smarty_tpl->tpl_vars['type_v'] : false;
$__foreach_type_v_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['type_v'] = new Smarty_Variable();
$__foreach_type_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_v_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['type_v']->value) {
$__foreach_type_v_3_saved_local_item = $_smarty_tpl->tpl_vars['type_v'];
?><td><?php if (($_smarty_tpl->tpl_vars['arr_competence']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']][$_smarty_tpl->tpl_vars['tab']->value['id_tab']][$_smarty_tpl->tpl_vars['type_v']->value['code']] != 2) && ($_smarty_tpl->tpl_vars['arr_competence']->value[$_SESSION['id_group']][$_smarty_tpl->tpl_vars['tab']->value['id_tab']][$_smarty_tpl->tpl_vars['type_v']->value['code']] != 2)) {?><label><input type="checkbox" class="on_ajax type_<?php echo $_smarty_tpl->tpl_vars['type_v']->value['code'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['type_v']->value['code'];?>
"<?php if ($_smarty_tpl->tpl_vars['arr_competence']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']][$_smarty_tpl->tpl_vars['tab']->value['id_tab']][$_smarty_tpl->tpl_vars['type_v']->value['code']]) {?> checked="checked"<?php }?>></label><?php }?></td><?php
$_smarty_tpl->tpl_vars['type_v'] = $__foreach_type_v_3_saved_local_item;
}
}
if ($__foreach_type_v_3_saved_item) {
$_smarty_tpl->tpl_vars['type_v'] = $__foreach_type_v_3_saved_item;
}
if ($__foreach_type_v_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_type_v_3_saved_key;
}
?><td><label><input type="checkbox" class="on_ajax type_all" value="all"<?php if ($_smarty_tpl->tpl_vars['arr_competence']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']][$_smarty_tpl->tpl_vars['tab']->value['id_tab']]['all']) {?> checked="checked"<?php }?>><?php echo $_smarty_tpl->tpl_vars['arr_competence']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']][$_smarty_tpl->tpl_vars['type_v']->value];?>
</label></td>
							</tr>
						<?php }?>
					<?php
$_smarty_tpl->tpl_vars['tab'] = $__foreach_tab_2_saved_local_item;
}
}
if ($__foreach_tab_2_saved_item) {
$_smarty_tpl->tpl_vars['tab'] = $__foreach_tab_2_saved_item;
}
if ($__foreach_tab_2_saved_key) {
$_smarty_tpl->tpl_vars['j'] = $__foreach_tab_2_saved_key;
}
?>
					</tbody>
				</table>
			</div>
		</div>
		</div><?php if (count($_smarty_tpl->tpl_vars['arr_features']->value) > 0) {?><div class="tab-count features_tab tabs_group_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
 col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-cogs"></i><?php echo l(array('s'=>"功能"),$_smarty_tpl);?>
</div>
			<div class="panel-body">
				<table>
					<thead>
					<tr class="h5" data-id_tab="all" data-id_parent="all">
						<td></td><td><label></label></td>
					</tr>
					</thead>
					<tbody>
					<?php
$_from = $_smarty_tpl->tpl_vars['arr_features']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_features_4_saved_item = isset($_smarty_tpl->tpl_vars['features']) ? $_smarty_tpl->tpl_vars['features'] : false;
$__foreach_features_4_saved_key = isset($_smarty_tpl->tpl_vars['j']) ? $_smarty_tpl->tpl_vars['j'] : false;
$_smarty_tpl->tpl_vars['features'] = new Smarty_Variable();
$__foreach_features_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_features_4_total) {
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['features']->value) {
$__foreach_features_4_saved_local_item = $_smarty_tpl->tpl_vars['features'];
?>
						<?php if ($_smarty_tpl->tpl_vars['features']->value['id_features'] != 1) {?>
							<tr class="h6" data-id_tab="<?php echo $_smarty_tpl->tpl_vars['features']->value['features'];?>
" data-id_parent="<?php echo $_smarty_tpl->tpl_vars['features']->value['features'];?>
">
								<td><?php echo $_smarty_tpl->tpl_vars['features']->value['features'];?>
</td><td><label><input type="checkbox" class="on_ajax type_give" value="give" checked="checked"></label></td>
							</tr>
						<?php }?>
					<?php
$_smarty_tpl->tpl_vars['features'] = $__foreach_features_4_saved_local_item;
}
}
if ($__foreach_features_4_saved_item) {
$_smarty_tpl->tpl_vars['features'] = $__foreach_features_4_saved_item;
}
if ($__foreach_features_4_saved_key) {
$_smarty_tpl->tpl_vars['j'] = $__foreach_features_4_saved_key;
}
?>
					</tbody>
				</table>
			</div>
		</div>
		</div><?php }
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./tabs_group.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./tabs_file.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
if (count($_smarty_tpl->tpl_vars['arr_module']->value)) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./tabs_module.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}?>
	<?php } else { ?>
		<div class="col-lg-12 panel-body tab-count competence_tab tabs_group_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
"><?php echo l(array('s'=>"最高系統管理者 權限無法更改!"),$_smarty_tpl);?>
</div>
	<?php }
$_smarty_tpl->tpl_vars['group'] = $__foreach_group_0_saved_local_item;
}
}
if ($__foreach_group_0_saved_item) {
$_smarty_tpl->tpl_vars['group'] = $__foreach_group_0_saved_item;
}
if ($__foreach_group_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_group_0_saved_key;
}
?>
	<?php echo smartyHook(array('h'=>'displayCompetenceTabs'),$_smarty_tpl);?>

</form><?php }
}
