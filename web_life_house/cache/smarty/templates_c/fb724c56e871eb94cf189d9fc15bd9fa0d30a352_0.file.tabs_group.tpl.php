<?php
/* Smarty version 3.1.28, created on 2021-03-23 16:01:50
  from "/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Competence/tabs_group.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6059a06ef075b9_94224074',
  'file_dependency' => 
  array (
    'fb724c56e871eb94cf189d9fc15bd9fa0d30a352' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/manage/themes/default/templates/controllers/Competence/tabs_group.tpl',
      1 => 1592288973,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6059a06ef075b9_94224074 ($_smarty_tpl) {
?>
<div class="tab-count for_group_tab tabs_group_<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
 col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="<?php echo $_smarty_tpl->tpl_vars['group']->value['id_group'];?>
">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="icon-cogs"></i><?php echo l(array('s'=>"賦予帳號管理"),$_smarty_tpl);?>
</div>
		<div class="panel-body">
			<?php echo l(array('s'=>"(管理帳號時可以管理的群組)"),$_smarty_tpl);?>

			<table>
				<thead>
				<tr class="h5" data-id_tab="all" data-id_parent="all">
					<td></td><td><label></label></td>
				</tr>
				</thead>
				<tbody>
				<?php
$_from = $_smarty_tpl->tpl_vars['arr_group']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_group2_0_saved_item = isset($_smarty_tpl->tpl_vars['group2']) ? $_smarty_tpl->tpl_vars['group2'] : false;
$__foreach_group2_0_saved_key = isset($_smarty_tpl->tpl_vars['j']) ? $_smarty_tpl->tpl_vars['j'] : false;
$_smarty_tpl->tpl_vars['group2'] = new Smarty_Variable();
$__foreach_group2_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_group2_0_total) {
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['group2']->value) {
$__foreach_group2_0_saved_local_item = $_smarty_tpl->tpl_vars['group2'];
?>
					<?php if ($_smarty_tpl->tpl_vars['group2']->value['id_group'] != 1) {?>
						<tr class="h6" data-give="<?php echo $_smarty_tpl->tpl_vars['group2']->value['id_group'];?>
">
							<td><?php echo $_smarty_tpl->tpl_vars['group2']->value['name'];?>
</td><td><label><input type="checkbox" class="on_ajax2 type_give" value="give"<?php if (in_array($_smarty_tpl->tpl_vars['group2']->value['id_group'],$_smarty_tpl->tpl_vars['arr_give_grupe']->value[$_smarty_tpl->tpl_vars['group']->value['id_group']])) {?> checked="checked"<?php }?>></label></td>
						</tr>
					<?php }?>
				<?php
$_smarty_tpl->tpl_vars['group2'] = $__foreach_group2_0_saved_local_item;
}
}
if ($__foreach_group2_0_saved_item) {
$_smarty_tpl->tpl_vars['group2'] = $__foreach_group2_0_saved_item;
}
if ($__foreach_group2_0_saved_key) {
$_smarty_tpl->tpl_vars['j'] = $__foreach_group2_0_saved_key;
}
?>
				</tbody>
			</table>
		</div>
	</div>
</div><?php }
}
