{include file="../../modules/FloatShare/float_share.tpl"}
</article>
      {if $display_footer}<footer>
            <nav>
                  {foreach from=$footer_link key=i item=link}<a href="{if !empty($link.link)}{$link.link}{else}javascript:void(0);{/if}">{$link.txt}</a>{/foreach}
            </nav>
            <div class="footer">
                  <table>
                        <tr>
                              <td>
                                    {if !empty($footer_txt1)}<div class="footer_txt1">{$footer_txt1}</div>{/if}
                                    {if !empty($footer_txt2)}<div class="footer_txt2">{$footer_txt2}</div>{/if}
                                    {if !empty($footer_txt3)}<div class="footer_txt3">{$footer_txt3}</div>{/if}
                              </td>
                              {if !empty($qrcod_img)}
                              <td class="qr_code_td">
                              <a href="{if !empty($footer_qrcode)}{$footer_qrcode}{else}{$qrcod_img}{/if}" target="_blank" class="qrcode"><img src="{$qrcod_img}"></a>
                              </td>{/if}
                        </tr>
                  </table>
            </div>
      </footer>{/if}
      {foreach from=$css_footer_files key=key item=css_uri}
            <link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
      {/foreach}
      {foreach from=$js_footer_files key=key item=js_uri}
            <script src="{$js_uri}"></script>
      {/foreach}
      </div>
</body>
</html>