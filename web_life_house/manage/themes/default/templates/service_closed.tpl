<button type="button" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btn btn-primary dropdown-toggle btn-lg" id="service_closed"><i class="glyphicon glyphicon-pencil"></i> {l s="我要結案"}</button>
<div style="display:none" id="c_back"></div>
<div id="service_closed_div" style="display:none">
<div class="form-group col-lg-12">
	{assign var=arr_f value=array('years', 'number', 'acceptance_date', 'status', 'id_company', 'id_build_case', 'house_code', 'project_number', 'contact', 'client', 'address', 'tel_h', 'tel_p', 'tel_o', 'id_repair_class', 'floor', 'id_location', 'problem')}
	{foreach from=$fields.form[$service_closed_num] key=key item=form_v}
		{if $key == 'input'}
			{foreach $form_v as $input}
				{if in_array($input.name, $arr_f) && !empty($input.label)}
					{if isset($input.values)}
						{foreach from=$input.values key=i item=vv}
							{if $vv.value == $input.val}
								{$input.val = $vv.label}
							{/if}
						{/foreach}
					{/if}
					{if isset($input.options)}
						{foreach from=$input.options.val key=i item=vv}
						{if $vv.val == $input.val}
						{$input.val = $vv.text}
						{/if}
						{/foreach}
					{/if}
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1"><samp>{$input.label} : </samp>{$input.val}</div>
				{/if}
			{/foreach}
		{/if}
	{/foreach}
	<div class="red text-center">您確定要結案!　結案後將無法更改資料!</div>
</div>
<button type="submit" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 btn btn-primary dropdown-toggle btn-lg" id="service_closed_sub" name="service_closed_sub"><i class="glyphicon glyphicon-thumbs-up"></i> {l s="確定"}</button>
</div>
<script type="text/javascript">
	$('#service_closed').click(function(){
		var c_back = $('#c_back');
		if(c_back.css('display') == 'none'){
			$('#c_back').stop(true, false).fadeIn();
			$('#service_closed_div').stop(true, false).fadeIn();
		}else{
			$('#c_back').stop(true, false).fadeOut();
			$('#service_closed_div').stop(true, false).fadeOut();
		};
	});
	$('#c_back').click(function(){
		$('#c_back').stop(true, false).fadeOut();
		$('#service_closed_div').stop(true, false).fadeOut();
	});
	$('#service_closed_sub').click(function(){
		if(confirm('{l s="您確定要結案\\n結案後將無法更改資料!"}')){
			return true;
		}else{
			return false;
		};
	});
</script>
<style type="text/css">
#c_back {
	position: fixed;
	width: 100%;
	height: 100%;
	background-color: rgba(0,0,0,0.8);
	left: 0px;
	top: 0px;
	z-index: 110;
}
#service_closed_div{
    width: 90%;
    margin: 0px;
    left: 5%;
    z-index: 120;
    background-color: #FFF;
    position: fixed;
    top: 5%;
    padding-bottom: 35px;
min-height: 90%;
	padding:1%;
}
</style>