{strip}<div id="top_bar">
      {$breadcrumbs_htm}
      {if !isset($title) && isset($page_header_toolbar_title)}
            {assign var=title value=$page_header_toolbar_title}
      {/if}{$top_bar_before_htm}{if count($bulk_actions) ||count($toolbar_btn)}<div id="top_bar_button">
            {if count($bulk_actions)>0}
            <select name="action" id="action" class="form-control">
            {foreach from=$bulk_actions key=i item=ba}
            <option value="{$i}">{$ba['text']}</option>
            {/foreach}
            </select>
            {/if}
            {if count($toolbar_btn)>0}
	            {if isset($toolbar_btn['print'])}<a href="{$toolbar_btn['print']['href']}" class="print btn btn-default pull-left{$toolbar_btn['print']['class']}" title="{$toolbar_btn['print']['desc']}"><i class="icon glyphicon glyphicon-print"></i>{$toolbar_btn['print']['desc']}</a>{/if}
			{if isset($toolbar_btn['excelimport'])}<a href="{$toolbar_btn['excelimport']['href']}" class="excelimport btn btn-default pull-left{$toolbar_btn['excelimport']['class']}" title="{$toolbar_btn['excelimport']['desc']}"><span class="icon glyphicon glyphicon-import"></span>{$toolbar_btn['excelimport']['desc']}</a>{/if}
	            {if isset($toolbar_btn['excel'])}<a href="{$toolbar_btn['excel']['href']}" class=" excel btn btn-default pull-left{$toolbar_btn['excel']['class']}" title="{$toolbar_btn['excel']['desc']}"><i class="icon fa fa-file-excel"></i>{$toolbar_btn['excel']['desc']}</a>{/if}
	            {if isset($toolbar_btn['view'])}<a href="{$toolbar_btn['view']['href']}" class="view btn btn-default pull-left{$toolbar_btn['view']['class']}" title="{$toolbar_btn['view']['desc']}"><i class="icon glyphicon glyphicon-eye-open"></i>{$toolbar_btn['view']['desc']}</a>{/if}
			{if isset($toolbar_btn['back'])}<a href="{$toolbar_btn['back']['href']}" class="back btn btn-default pull-left{$toolbar_btn['back']['class']}" title="{$toolbar_btn['back']['desc']}"><i class="icon glyphicon  glyphicon-chevron-left"></i>{$toolbar_btn['back']['desc']}</a>{/if}
			{if isset($toolbar_btn['del'])}<a href="{$toolbar_btn['del']['href']}" class="del btn btn-default pull-left{$toolbar_btn['del']['class']}" title="{$toolbar_btn['del']['desc']}"><i class="icon glyphicon glyphicon-trash"></i>{$toolbar_btn['del']['desc']}</a>{/if}
			{if isset($toolbar_btn['edit'])}<a href="{$toolbar_btn['edit']['href']}" class="edit btn btn-default pull-left{$toolbar_btn['edit']['class']}" title="{$toolbar_btn['edit']['desc']}"><i class="icon glyphicon glyphicon-pencil"></i>{$toolbar_btn['edit']['desc']}</a>{/if}
			{if isset($toolbar_btn['new'])}<a href="{$toolbar_btn['new']['href']}" class="new btn btn-default pull-left{$toolbar_btn['new']['class']}" title="{$toolbar_btn['new']['desc']}"><i class="icon glyphicon glyphicon-plus"></i>{$toolbar_btn['new']['desc']}</a>{/if}
	            {if isset($toolbar_btn['cancel'])}<a href="{$toolbar_btn['cancel']['href']}" class="cancel btn btn-default pull-left{$toolbar_btn['cancel']['class']}" title="{$toolbar_btn['cancel']['desc']}"><i class="icon glyphicon glyphicon-remove"></i>{$toolbar_btn['cancel']['desc']}</a>{/if}
			{if isset($toolbar_btn['save'])}<a href="{$toolbar_btn['save']['href']}" class="href btn btn-default pull-left{$toolbar_btn['save']['class']} save" title="{$toolbar_btn['save']['desc']}"><i class="icon glyphicon glyphicon-floppy-disk"></i>{$toolbar_btn['save']['desc']}</a>{/if}
			{if isset($toolbar_btn['save_and_stay'])}<a href="{$toolbar_btn['save_and_stay']['href']}" class="save_and_stay btn btn-default pull-left{$toolbar_btn['save_and_stay']['class']}" title="{$toolbar_btn['save_and_stay']['desc']}"><i class="icon glyphicon glyphicon-floppy-disk"></i>{$toolbar_btn['save_and_stay']['desc']}</a>{/if}
			{if isset($toolbar_btn['search'])}<a href="{$toolbar_btn['search']['href']}" class="search btn btn-default pull-left{$toolbar_btn['search']['class']}" title="{$toolbar_btn['search']['desc']}"><i class="icon glyphicon glyphicon-search"></i>{$toolbar_btn['search']['desc']}</a>{/if}
		      {if isset($toolbar_btn['btn'])}
			      {foreach from=$toolbar_btn['btn'] key=k item=btn}
	                        <a href="{$btn.href}" class="btn btn-default pull-left{$btn['class']}" title="{$btn.title}">{if $btn.icon}<i class="{$btn.icon}"></i>{/if}{$btn.desc}</a>
	                  {/foreach}
	            {/if}
			{if isset($toolbar_btn['media_search'])}<a href="{$toolbar_btn['media_search']['href']}" class="media_search btn btn-default pull-left visible-sm-inline-block visible-xs-inline-block{$toolbar_btn['media_search']['class']}" title="{$toolbar_btn['media_search']['desc']}"><i class="icon glyphicon glyphicon-search"></i>{$toolbar_btn['media_search']['desc']}</a>{/if}

			{if isset($toolbar_btn['calendar']) || isset($toolbar_btn['list'])}<div class="btn-group btn-group" role="group" aria-label="Large button group">{/if}
			{if isset($toolbar_btn['calendar'])}<a href="{$toolbar_btn['calendar']['href']}" class="view btn btn-default pull-left{if array_key_exists("calendar"|cat:$table, $smarty.get)} active{/if}" title="{$toolbar_btn['calendar']['desc']}">{if !isset($toolbar_btn['calendar']['icon'])}<i class="icon glyphicon glyphicon-calendar"></i>{elseif $toolbar_btn['calendar']['icon'] != ''}<i class="{$toolbar_btn['calendar']['icon']}"></i>{/if}{$toolbar_btn['calendar']['desc']}</a>{/if}
			{if isset($toolbar_btn['list'])}<a href="{$toolbar_btn['list']['href']}" class="view btn btn-default pull-left{if !array_key_exists("calendar"|cat:$table, $smarty.get)} active{/if}" title="{$toolbar_btn['list']['desc']}">{if !isset($toolbar_btn['list']['icon'])}<i class="icon glyphicon glyphicon-list-alt"></i>{elseif $toolbar_btn['list']['icon'] != ''}<i class="{$toolbar_btn['list']['icon']}">{/if}{$toolbar_btn['list']['desc']}</i></a>{/if}
			{if isset($toolbar_btn['calendar']) || isset($toolbar_btn['list'])}</div>{/if}
            {/if}
      </div>{/if}{$top_bar_after_htm}
</div>
{if isset($toolbar_btn['save']) || isset($toolbar_btn['save_and_stay']) || isset($toolbar_btn['search'])}
      <script type="text/javascript">
		{if isset($toolbar_btn['save'])}
		$('.save').click(function(){
			$('#content>.content form').submit()
		});
		{/if}
		{if isset($toolbar_btn['save_and_stay'])}
		$('.save_and_stay').click(function(){
			$('input[name="{$submit_action}"]').attr('name', '{$submit_action}AndStay');
			$('#content>.content form').submit();
		});
		{/if}
		{if isset($toolbar_btn['search'])}
		$('a.search').click(function(){
			$('.content>form.list').submit()
		});
		{/if}
      </script>
{/if}{/strip}