{if isset($fields.field_display) && !empty($fields.field_display)}
<div class="field_list">
	<form action="" method="post" class="list" accept-charset="utf-8">
		<div class="text-center up"><i class="fas fa-angle-double-up"></i></div>
		<div class="title">{l s="顯示欄位"}</div>
		<div class="field">
			<div class="checkbox"><label for="all_field_name_val"><input type="checkbox" name="all_field_name_val[]" id="all_field_name_val" value="">{l s="全選"}</label></div>
			{foreach from=$fields.all_field_list key=key item=val}
				<div class="checkbox"><label for="field_name_{$key}"><input type="checkbox" name="field_name_val[]" id="field_name_{$key}" value="{$key}" {if $val.show==1}checked{/if}>{$val.title|strip_tags:true}</label><input type="hidden" name="field_name[]" value="{$key}"></div>
			{/foreach}
			<button type="submit" class="btn btn-default">{l s="送出"}</button>
		</div>
		<input type="hidden" name="SetMyFieldList{$admin_url}">
	</form>
</div>
{/if}