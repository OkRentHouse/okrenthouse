{if isset($_msg) && $_msg != ''}
<div class="alert alert-success {$this->show_type}" role="alert"><strong>{$strong}</strong>{$_msg}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
{/if}
{if isset($from_error) && $from_error != ''}
<div class="alert alert-danger" role="alert"><strong>{$strong}</strong>{$_error}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
{/if}
{if isset($from_error) && $from_error != ''}
<div class="alert alert-warning" role="alert"><strong>{$strong}</strong>{$from_error}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
{/if}