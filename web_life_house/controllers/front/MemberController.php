<?php

namespace web_life_house;
use \FrontController;
use \Context;
use \Db;
use \File;
use \Tools;

class memberController extends FrontController
{
	public $page = 'member';

	public $tpl_folder;	//樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct(){
		$this->meta_title = $this->l('歡迎頁');		//網頁titlel
		$this->page_header_toolbar_title = $this->l('歡迎頁');	//手機APP title
		$this->className = 'memberController';
		$this->display_main_menu = false;
//		$this->display_header = false;
//		$this->display_footer = false;
		parent::__construct();
	}

	public function initProcess()
	{
	    $js_slick =<<<js
<script>
        $(".list_wrap").slick({
		dots: false,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 4000,
		slidesToShow: 10,
		slidesToScroll: 5,
	});
</script>
js;

	    $sql = "SELECT * FROM nested_group ORDER BY position ASC";
        $nested_group_arr = Db::rowSQL($sql);


        //圖片處理start
        $img_data = ['0','1'];
        foreach($nested_group_arr as $key => $value){
            foreach($img_data as $k_img => $v_img){
                $sql = "SELECT * FROM nested_group_file WHERE file_type=".$img_data[$v_img]." AND id_nested_group=".GetSQL($nested_group_arr[$key]["id_nested_group"],"int");
                $nested_group_file = Db::rowSQL($sql,true);//因為只有一張圖
                $nested_group_arr[$key]["img_{$v_img}"] = File::get($nested_group_file['id_file']);
            }
        }
        //圖片處理end

//        print_r($nested_group_arr);

        $js =<<<javscript
<script>
    var msg_icon_a_type=0;
var msg_icon_top=$(".msg_icon").css("top");
console.log(msg_icon_top);
$(".msg_icon_a").click(
	function () {
		if(msg_icon_a_type == 0){
			$(".msg_icon").animate(
				{top: 99,}, 500, function() {msg_icon_a_type=1;$(".msg_icon_a img").attr("src","themes/LifeHouse/img/welcom/contact_us_3.png")}
				)
		}else{
			$(".msg_icon").animate(
				{top: msg_icon_top,}, 500, function() {msg_icon_a_type=0;$(".msg_icon_a img").attr("src","themes/LifeHouse/img/welcom/contact_us_2.png")}
				)
			}
		}
);


for(var wi=1;wi<=14;wi++){
	welcome_icon_atc($("#welcome_icon_"+wi),wi)
	}

$(".prev img").mouseover(function(){
    $(".prev").animate({opacity: 1,}, 100, function() {})
});
$(".next img").mouseover(function(){
    $(".next").animate({opacity: 1,}, 100, function() {})
});

$(".prev img").mouseout(function(){
    $(".prev").animate({opacity: 0.4,}, 1000, function() {})
});
$(".next img").mouseout(function(){
    $(".next").animate({opacity: 0.4,}, 1000, function() {})
});

function welcome_icon_atc(e,wix){
	$(e).mouseover(
	function() {
		$("#welcome_icon_00").animate({
			opacity: 0,
		  }, 100, function() {
			  for(var wi=1;wi<=14;wi++){
				if(wi != wix){ $("#welcome_icon_"+wi).css("display","none") };
			  }


	    $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'BackGround',
                'wix' : wix
            },
            dataType: 'json',
            success: function (data) {
                if(data["error"] !=""){
                    alert(data["error"]);
                }else{
                    console.log(data["return"]["url"]);
                    if(data["return"]!=''){
                       $(".entrance_wrap_bk > img").attr("src",data["return"]["url"]);
                    }else{
                      $("#welcome_icon_00").animate({
                            opacity: 1,
                          }, 100, function() {
                      });
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
			// Animation complete.
		  });
		}
	);
	//welcome_icon_8.jpg
	$(e).mouseout(
	function() {
		$("#welcome_icon_00").animate({
			opacity: 0,
		  }, 100, function() {

			  $("#welcome_icon_00").attr("src","themes/LifeHouse/img/welcom/welcome_honeycomb.svg");
			  $(".entrance_wrap_bk > img").attr("src","");
			  $("#welcome_icon_00").animate({
					opacity: 1,
				  }, 100, function() {

					  for(var wi=1;wi<=14;wi++){
						if(wi != wix){ $("#welcome_icon_"+wi).css("display","block") };
					  }
					  $("#welcome_icon_00").attr("src","themes/LifeHouse/img/welcom/welcome_honeycomb.svg");
			  });
			// Animation complete.
		  });
		}
	);
}
</script>
javscript;




		$this->fields['page']['class'] = 'welcome_wrap';
		$Context              = Context::getContext();
		$Context->smarty->assign([
			'fields'   => $this->fields,
            'js_slick' => $js_slick,
            'nested_group'=>$nested_group_arr,
//            'js'=>$js,
		]);
		parent::initProcess();
	}


    public function displayAjaxBackGround(){

        $action = Tools::getValue('action');    //action = 'BackGround';
        $wix =Tools::getValue('wix');
        switch ($action){
            case 'BackGround':
                $sql = "SELECT * FROM nested_group as n_g
                        LEFT JOIN nested_group_file as n_g_f ON n_g.`id_nested_group` = n_g_f.`id_nested_group`
                        WHERE file_type=1 AND n_g.`position`=".GetSQL($wix,"int");
                $row = Db::rowSQL($sql,true);//丟回去~
                $file = File::get($row["id_file"]);
                echo json_encode(array("error"=>"","return"=>$file));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }


    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('css/slick.css');
        $this->addCSS('css/slick-theme.css');
        $this->addJS('themes/LifeHouse/js/base.js');
        $this->addJS('js/slick.min.js');
    }
}
