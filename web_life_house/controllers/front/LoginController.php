<?php

namespace web_life_house;

use \FrontController;
use \Tools;

class LoginController extends FrontController
{
	public $page = 'login';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('會員登入');
		$this->page_header_toolbar_title = $this->l('會員登入');
		$this->fields['page']['class']   = 'orange_page';
		$this->className                 = 'LoginController';
		$this->display_main_menu         = false;
		$this->display_header            = false;
		$this->display_footer            = false;
		parent::__construct();
		$this->fields['form'] = [
			'member' => [
				'input' => [
					'email'    => [
						'name'        => 'email',
						'type'        => 'email',
						'label'       => $this->l('帳號'),
						'placeholder' => $this->l('帳號'),
						'maxlength'   => '100',
						'required'    => true,
					],
					'password' => [
						'type'        => 'new-password',
						'label'       => $this->l('密碼'),
						'placeholder' => $this->l('密碼'),
						'required'    => true,
						'minlength'   => '8',
						'maxlength'   => '20',
						'name'        => 'password',
					],
				],
			],
		];
	}

	public function postProcess()
	{
		$login       = Tools::getValue('login');
		$email       = Tools::getValue('email');
		$password    = Tools::getValue('password');
		$auto_loging = Tools::getValue('auto_loging');
		if (!empty($login)) {
			$this->validateRules();
			if (count($this->_errors) == 0) {
				if (!Member::login($email, $password, $auto_loging)) {
					$this->_errors[] = $this->l('帳號密碼錯誤!');
				} else {
					Tools::redirectLink('/home');
					exit;
				}
			}
		}
	}
}