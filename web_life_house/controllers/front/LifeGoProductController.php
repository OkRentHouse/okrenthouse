<?php

namespace web_life_house;
use \LifeGOMainController;
use \Context;
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;

class LifeGoProductController extends LifeGOMainController{

    public $page = 'LifeGoProduct';
    public function __construct(){

        $this->className = 'LifeGoProductController';
        $this->meta_title                = $this->l('生活樂購');
        $this->page_header_toolbar_title = $this->l('生活樂購');
        $this->no_FormTable              = true;

//        $this->breadcrumbs = [
//            [
//                'href' => '/',
//                'hint' => $this->l(''),
//                'icon' => '',
//                'title' => $this->l('首頁'),
//            ],
//            [
//                'href' => '/in_life_index',
//                'hint' => $this->l(''),
//                'icon' => '',
//                'title' => $this->l('in 生活'),
//            ],
//            [
//                'href' => '/Store',
//                'hint' => $this->l(''),
//                'icon' => '',
//                'title' => $this->l('生活好康'),
//            ],
//            [
//                'href' => '/LifeBuy',
//                'hint' => $this->l(''),
//                'icon' => '',
//                'title' => $this->l('生活樂購'),
//            ],
//            //這塊?　對
//        ];

        parent::__construct();
    }

    public function initProcess(){
        $id_product_class = Tools::getValue("id_product_class");
        $id_product_class_item = Tools::getValue("id_product_class_item");

        $ip = $_SERVER['REQUEST_URI'];//頁籤active判斷

        $add_left_bottom="";
        for($i=0;$i<4;$i++){
            $add_left_bottom .='<div class="items"><a href="####" target="_blank"><img src="/themes/LifeHouse/img/lifego/p1.png"></a></div>';
        }
        $add_left_bottom='<li class="actions hot_product">
                            <div class="title"><img src="themes/LifeHouse/img/lifego/star.svg" alt="" class="">其他人也看了</div>
                           '.$add_left_bottom.'
                          </li>';


        $product = [
            'id_product'=>'1234567',
            'img'=>[
                ['url'=>'/themes/LifeHouse/img/lifegoproduct/product_photo_1.jpg'],
                ['url'=>'/themes/LifeHouse/img/lifegoproduct/product_photo_2.jpg'],
                ['url'=>'/themes/LifeHouse/img/lifegoproduct/product_photo_3.jpg'],
                ['url'=>'/themes/LifeHouse/img/lifegoproduct/product_photo_4.jpg'],
                ['url'=>'/themes/LifeHouse/img/lifegoproduct/product_photo_5.jpg'],
            ],
        ];




        $this->context->smarty->assign([
            'ip'                    => $ip,
            'id_product_class'      => $id_product_class,
            'id_product_class_item' => $id_product_class_item,
            'add_left_bottom'      => $add_left_bottom,
            'left_bottom'           =>'',
            'product'               =>$product,
        ]);


        parent::initProcess();
    }

    public function setMedia(){
        parent::setMedia();
//        $this->addCSS('/css/fontawesome-all.css');
        $this->addJS('/js/solid.min.js');
        $this->addJS('/js/regular.min.js');
        //$this->addCSS(THEME_URL.'/css/LifeGo.css');
    }
}
