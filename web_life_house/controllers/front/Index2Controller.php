<?php

namespace web_life_house;
use \FrontController;
use \Context;

class Index2Controller extends FrontController
{
	public $page = 'index2';
	
	public $tpl_folder;	//樣版資料夾
	public $definition;
	public $_errors_name;
	
	public function __construct(){
		$this->meta_title = $this->l('歡迎頁');		//網頁titlel
		$this->page_header_toolbar_title = $this->l('歡迎頁');	//手機APP title
		$this->className = 'Index2Controller';
		$this->display_main_menu = false;
		$this->display_header = true;
		$this->display_footer = true;
		parent::__construct();
	}

	public function initProcess()
	{
		$this->fields['page']['class'] = 'welcome_wrap';
		$Context              = Context::getContext();
		$Context->smarty->assign([
			'fields'   => $this->fields,
		]);
		parent::initProcess();
	}
}