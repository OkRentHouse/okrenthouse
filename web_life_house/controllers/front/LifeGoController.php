<?php
namespace web_life_house;

use \LifeGOMainController;
use \Context;
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;

class LifeGoController extends LifeGOMainController{

    public $page = 'LifeGo';

    public function __construct(){
        $this->className = 'LifeGoController';
        $this->meta_title                = $this->l('生活樂購 歡樂購');
        $this->page_header_toolbar_title = $this->l('生活樂購 歡樂購');
        $this->no_FormTable              = true;
        $this->breadcrumb ='生活集團〉生活樂購〉Home';
        parent::__construct();
    }

    public function initProcess(){
        $id_product_class = Tools::getValue("id_product_class");
        $id_product_class_item = Tools::getValue("id_product_class_item");

        $ip = $_SERVER['REQUEST_URI'];//頁籤active判斷

        $caption_title1 =[
            'big_tile' => '熱銷榜',
            'big_img'  => '/themes/LifeHouse/img/lifego/star_pink.gif',
            'class'    => 'hot',
            ];
        $caption_data1 = [
                [
                    'item_number'=>'L1234567',
                    'main_title'=>'健康御守 二氧化氯清淨錠套組',
                    'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                    'price_original'=>'2,100',
                    'price'=>'1,350',
                    'quantity'=>'123',
                    'favorite'=>'123',
                    'item_id'=>'1',
                ],
                [
                    'item_number'=>'L1234567',
                    'main_title'=>'健康御守 二氧化氯清淨錠套組',
                    'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                    'price_original'=>'2,100',
                    'price'=>'1,350',
                    'quantity'=>'123',
                    'favorite'=>'123',
                    'item_id'=>'1',
                ],
                [
                    'item_number'=>'L1234567',
                    'main_title'=>'健康御守 二氧化氯清淨錠套組',
                    'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                    'price_original'=>'2,100',
                    'price'=>'1,350',
                    'quantity'=>'123',
                    'favorite'=>'123',
                    'item_id'=>'1',
                ],
                [
                    'item_number'=>'L1234567',
                    'main_title'=>'健康御守 二氧化氯清淨錠套組',
                    'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                    'price_original'=>'2,100',
                    'price'=>'1,350',
                    'quantity'=>'123',
                    'favorite'=>'123',
                    'item_id'=>'1',
                ],
                [
                    'item_number'=>'L1234567',
                    'main_title'=>'健康御守 二氧化氯清淨錠套組',
                    'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                    'price_original'=>'2,100',
                    'price'=>'1,350',
                    'quantity'=>'123',
                    'favorite'=>'123',
                    'item_id'=>'1',
                ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            ];
        $caption_title2 =[
            'big_tile' => '好康到',
            'big_img'  => '/themes/LifeHouse/img/lifego/star_new_blue.gif',
            'class'    => 'offer',
        ];
        $caption_data2 = [
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
        ];

        $caption_title3 =[
            'big_tile' => '新貨色',
            'big_img'  => '/themes/LifeHouse/img/lifego/star_orange.gif',
            'class'    => 'new',
        ];

        $caption_data3 = [
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
        ];

        $js_ready ="";

        if(!empty($id_product_class)){
            $js_ready ='$(document).ready(function(){
            $(".t_col_10 table").hide();
});';
        }


        $js =<<<js
<script>
{$js_ready}
    $(".regular").slick({
        dots: true,
        autoplay : false,
        infinite: false,
        // autoplaySpeed: 3000,
        slidesToShow: 4,
        slidesToScroll: 4,
    });
</script>
js;

        $this->context->smarty->assign([
//           'ip' => $ip,
           'id_product_class'  => $id_product_class,
           'id_product_class_item' => $id_product_class_item,
            'caption_title1'    => $caption_title1,
            'caption_data1'           => $caption_data1,
            'caption_title2'    => $caption_title2,
            'caption_data2'           => $caption_data2,
            'caption_title3'    => $caption_title3,
            'caption_data3'           => $caption_data3,
            'js'                => $js,
        ]);
        parent::initProcess();
    }

    public function setMedia(){
        parent::setMedia();
        $this->addJS('/js/solid.min.js');
        $this->addJS('/js/regular.min.js');
        //$this->addCSS(THEME_URL.'/css/LifeGo.css');
    }
}
