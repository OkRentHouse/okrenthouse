<?php

class AdminFooterSetController extends AdminController
{
	public $tpl_folder;    //樣版資料夾

	public function __construct()
	{
		$this->className       = 'AdminFooterSetController';

		$this->fields['form'] = [
			'footer_set' => [
				'tab'    => 'footer_set',
				'legend' => [
					'title' => $this->l('Footer 設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'footer_txt1'                               => [
						'name'          => 'footer_txt1',
						'type'          => 'text',
						'label'         => $this->l('Footer 行一'),
						'col'           => 6,
						'rows'          => 12,
						'placeholder'   => '',
						'configuration' => true,
					],
					'footer_txt2'                               => [
						'name'          => 'footer_txt2',
						'type'          => 'text',
						'label'         => $this->l('Footer 行二'),
						'col'           => 6,
						'rows'          => 12,
						'placeholder'   => '',
						'configuration' => true,
					],
					'footer_txt3'                               => [
						'name'          => 'footer_txt3',
						'type'          => 'text',
						'label'         => $this->l('Footer 行三'),
						'col'           => 6,
						'rows'          => 12,
						'placeholder'   => '',
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'options';
		parent::initContent();
	}
}