<?php


class AdminRentHouseController0107 extends AdminController
{
    public $arr_file_type = [
        '0' => 'img',
        '1' => 'web_carousel',
        '2' => 'app_img',
        '3' => 'app_carousel',
        '4' => 'building_power',
        '5' => 'land_power',
    ];
    public function __construct()
    {

        $this->className = 'AdminRentHouseController0107';
        $this->table = 'rent_house';
        $this->fields['index'] = 'id_rent_house';
        $this->_as = 'r_h';
        $this->fields['title'] = '租售屋管理';
        $this->_join = ' LEFT JOIN `web` AS w ON w.`id_web` = r_h.`id_web`
                         LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                         LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                         LEFT JOIN `rent_house_type` AS r_h_t ON r_h_t.`id_rent_house_type` = r_h.`id_rent_house_type`
                         LEFT JOIN `rent_house_types` AS r_h_ts ON r_h_ts.`id_rent_house_types` = r_h.`id_rent_house_types`
                         LEFT JOIN `device_category` AS d_c ON d_c.`id_device_category` = r_h.`id_device_category`
                         LEFT JOIN `rent_house_community` AS r_h_c ON r_h_c.`rent_house_community_code` = r_h.`rent_house_community_code`
                         LEFT JOIN `disgust_facility` AS d_f ON d_f.`id_disgust_facility` = r_h.`id_disgust_facility`
                         LEFT JOIN `disgust_facility_class` AS d_f_c ON d_f_c.`id_disgust_facility_class` = d_f.`id_disgust_facility_class`
                         LEFT JOIN `public_utilities` AS p_u ON p_u.`id_public_utilities` = r_h.`id_public_utilities` ';
        $this->_group = ' GROUP BY r_h.`id_rent_house`';
        $this->fields['order'] = ' ORDER BY `featured` DESC, `id_rent_house` DESC';
        $this->fields['list_num'] = 50;
        $this->fields['where']='';


//        $disabled_id_web = false;
//        $id_web_daf = '1';//預設
//        if(!empty($_SESSION['id_web'])){
//            $this->fields['where'] = ' AND w.`id_web` = '.$_SESSION['id_web'];
////            $id_web_daf = $_SESSION['id_web'];
////            $disabled_id_web = true;
//        }

        $this->fields['list'] = [
            'id_rent_house' => [
                'filter_key' => 'r_h!id_rent_house',
                'index' => true,
                'title' => $this->l('ID'),
//                'type' => 'checkbox',
//                'hidden' => true,
                'class' => 'text-center',
                'order' => true,
                'filter' => true,
            ],
            'featured' => [
                'filter_key' => 'r_h!featured',
                'title' => $this->l('精選'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'featured',
                        'value' => 1,
                        'title' => $this->l('精選'),
                    ],
                    [
                        'class' => 'featured',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
                'class' => 'text-center',
            ],
            'web' => [
                'filter_key' => 'w!web',
                'title' => $this->l('加盟店'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'rent_house_code' => [
                'filter_key' => 'r_h!rent_house_code',
                'title' => $this->l('物件編號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'case_name' => [
                'filter_key' => 'r_h!case_name',
                'title' => $this->l('案名(主標)'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'title' => [
                'filter_key' => 'r_h!title',
                'title' => $this->l('標題(副標)'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'id_county' => [
                'filter_key' => 'r_h!id_county',
                'title' => $this->l('縣市'),
                'order' => true,
                'filter' => true,
                'multiple' => true,
                'key' => [
                    'table' => 'county',
                    'key' => 'id_county',
                    'val' => 'county_name',
                    'order' => '`id_county` ASC',
                ],
            ],
            'id_city' => [
                'filter_key' => 'r_h!id_city',
                'title' => $this->l('鄉鎮區'),
                'order' => true,
                'filter' => true,
                'multiple' => true,
                'key' => [
                    'table' => 'city',
                    'key'   => 'id_city',
                    'parent'=> 'id_county',
                    'val' => 'city_name',
                    'order' => '`id_county` ASC',
                ],
            ],
            'price' => [
                'filter_key' => 'r_h!price',
                'title' => $this->l('總價(萬)'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'ping' => [
                'filter_key' => 'r_h!ping',
                'title' => $this->l('坪數'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'complete_address'  =>[
                'title' => $this->l('地址'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                'filter_sql'   =>'CASE r_h.`complete_address`
                 WHEN "" THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
                    IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,"鄰"),""),
                    r_h.`road`,
                    r_h.`segment`,
                    IF(r_h.`lane`,CONCAT(r_h.`lane`,"巷"),""),
                    IF(r_h.`alley`,CONCAT(r_h.`alley`,"弄"),""),
                    IF(r_h.`no`,CONCAT(r_h.`no`,"號"),""),
                    IF(r_h.`no_her`,CONCAT("之",r_h.`no_her`),""),
                    IF(r_h.`floor`,CONCAT(r_h.`floor`,"樓"),""),
                    IF(r_h.`floor_her`,CONCAT("之",r_h.`floor_her`),""),
                    IF(r_h.`address_room`,CONCAT(r_h.`address_room`,"室"),"")
                 )
                 ELSE r_h.`complete_address`
                 END',
            ],
            'rent_house_types_title' => [
                'filter_key' => 'r_h_ts!title',
                'title' => $this->l('物件型態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'pattern' => [
                'title' => $this->l('格局(房/廳/衛/室)'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                'filter_sql'    => 'CONCAT(r_h.`room`,"/",r_h.`hall`,"/",r_h.`bathroom`,"/",r_h.`muro`)',
            ],
            'contract_time_e' => [
                'filter_key' => 'r_h!contract_time_e',
                'title' => $this->l('仲介契約時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'contract_time2_e' => [
                'filter_key' => 'r_h!contract_time2_e',
                'title' => $this->l('租管契約時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'advertise_date_e' => [
                'filter_key' => 'r_h!advertise_date_e',
                'title' => $this->l('廣告時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'submit_date' => [
                'filter_key' => 'r_h!submit_date',
                'title' => $this->l('建檔日期'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'advertise_date_e_call'  =>[//這邊是要做判斷是否低過30天
                'title' => $this->l('廣告快到期'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                'filter_sql'   =>'(CASE r_h.active
                WHEN 0 THEN "否"
                WHEN 1 THEN IF((r_h.advertise_date_e is null ||
                (r_h.advertise_date_e = "0000-00-00") ||
                (r_h.advertise_date_e="") ||
                 (DATE_SUB(CURDATE(),INTERVAL 1 WEEK)>=r_h.advertise_date_e)),
                "否","是")
                ELSE "否"
                END)',
                'values' => [
                    [
                        'value' => "是",
                        'title' => $this->l('是'),
                    ],
                    [
                        'value' => '否',
                        'title' => $this->l('否'),
                    ],
                ],
            ],
            'contract_time_e_call'  =>[//這邊是要做判斷是否低過30天
                'title' => $this->l('委託快到期'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                'filter_sql'   =>'IF((r_h.contract_time_e is null ||
                (r_h.contract_time_e = "0000-00-00") ||
                (r_h.contract_time_e="") ||
                 (DATE_SUB(CURDATE(),INTERVAL 1 MONTH)>=r_h.contract_time_e)),
                "否","是")',
                'values' => [
                    [
                        'value' => "是",
                        'title' => $this->l('是'),
                    ],
                    [
                        'value' => '否',
                        'title' => $this->l('否'),
                    ],
                ],
            ],
            'active' => [
                'filter_key' => 'r_h!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
        ];

        //rent_house_community 運作陣列
        $this->rent_house_community_arr = ['id_rent_house_community','community','all_num','id_exterior_wall','household_num','storefront_num','property_management','community_phone','community_fax','builders','redraw_area',
            'clean_time','time_s','time_e','id_public_utilities','e_school','j_school','park','market','night_market','supermarket','shopping_center','hospital','bus','bus_station','passenger_transport','passenger_transport_station',
            'train','mrt','mrt_station','high_speed_rail','interchange'];//所需要取出並修改的

        //網站縮圖 網站輪播 app縮圖 app輪播 建築權狀 土地權狀


        $sql = 'SELECT * FROM rent_house_type WHERE `active`=1';
        $rent_house_type_arr = Db::rowSQL($sql);
        foreach($rent_house_type_arr as $value){
            $rent_house_type[] = [
                'id' => 'rent_house_type_'.$value['id_rent_house_type'],
                'value' => $value['id_rent_house_type'],
                'label' => $this->l($value['title'])
            ];
        }

        $sql = 'SELECT * FROM other_conditions ORDER BY `position` ASC';
        $other_conditions_arr = Db::rowSQL($sql);
        foreach($other_conditions_arr as $value){
            $other_conditions[] = [
                'id' => 'other_conditions_'.$value['id_other_conditions'],
                'value' => $value['id_other_conditions'],
                'label' => $this->l($value['title'])
            ];
        }

        $sql = 'SELECT * FROM disgust_facility as d_f
        LEFT JOIN `disgust_facility_class` AS d_f_c ON d_f_c.`id_disgust_facility_class` = d_f.`id_disgust_facility_class`
        ORDER BY d_f_c.`position`,d_f.`position`,d_f.`id_disgust_facility` ASC ';
        $disgust_facility_arr = Db::rowSQL($sql);
        foreach($disgust_facility_arr as $value){
            $disgust_facility[] = [
                'id' => 'disgust_facility_'.$value['id_disgust_facility'],
                'value' => $value['id_disgust_facility'],
                'label' => $this->l($value['disgust_facility_class_name'].'-'.$value['disgust_facility_name'])
            ];
        }

        $sql = 'SELECT *,p_u_c.`title` as p_u_c_title,p_u.`title` as title,p_u.`id_public_utilities` as id_public_utilities,
                p_u_c.`id_public_utilities_class` as id_public_utilities_class FROM public_utilities as p_u
                LEFT JOIN `public_utilities_class` as p_u_c ON p_u_c.`id_public_utilities_class` = p_u.`id_public_utilities_class`
                ORDER BY p_u.`position` ASC';
        $public_utilities_arr = Db::rowSQL($sql);
        foreach($public_utilities_arr as $value){
            $public_utilities[$value['id_public_utilities_class']][] = [
                'id' => 'public_utilities_'.$value['id_public_utilities'],
                'value' => $value['id_public_utilities'],
                'class' =>"public_utilities",
                'label' => $this->l($value['p_u_c_title'].'-'.$value['title'])
            ];
        }

//        $this->fields['tabs'] = [
//            'base_data' => $this->l('物件資料'),
//            'variety_data' =>$this->l('其他條件'),
//            'advertise'=>$this->l('廣告相關'),
//            'home_appliances'=>$this->l('家具/家電/設備(後台管理)'),
//            'community_data' =>$this->l('社區資料(如選擇後任意更改會影響整體)'),
//            'file'=>$this->l('照片'),
//        ];

        $this->fields['form'] = [

            'in_rent_house' => [
//                'tab' => 'base_data',
                'legend' => [
                    'title' => $this->l('租件影響'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'featured' => [
                        'name' => 'featured',
                        'type' => 'checkbox',
                        'form_col'=>6,
                        'col'=>6,
                        'label_col' => 6,
                        'label' => $this->l('精選'),
                        'values' => [
                            [
                                'id' => 'featured',
                                'value' => 1,
                                'label' => $this->l('精選'),
                            ],
                        ],
                    ],

                    'active' => [
                        'type' => 'switch',
                        'form_col'=>6,
                        'label_col' => 3,
                        'col'=>6,
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],

                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    'on_date' => [
                        'name' => 'on_date',
                        'type' => 'date',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'label' => $this->l('上架時間'),
                        'disabled' =>true,
                    ],

                    'off_date' => [
                        'name' => 'off_date',
                        'type' => 'date',
                        'form_col' =>6,
                        'label_col' => 3,
                        'col'=>6,
                        'label' => $this->l('手動下架時間'),
                        'disabled' =>true,
                    ],

                    'create_name' => [
                        'name' => 'create_name',
                        'type' => 'text',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'label' => $this->l('建立人'),
                        'disabled' =>true,
                    ],

                    'update_name' => [
                        'name' => 'update_name',
                        'type' => 'text',
                        'form_col' =>6,
                        'label_col' => 3,
                        'col'=>6,
                        'label' => $this->l('更新者'),
                        'disabled' =>true,
                    ],

                    'submit_date' => [
                        'name' => 'submit_date',
                        'type' => 'view',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'auto_datetime' => 'add',
                        'label' => $this->l('建立時間'),
                    ],

                    'update_date' => [
                        'name' => 'update_date',
                        'type' => 'text',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'label' => $this->l('更新時間'),
                        'disabled' =>true,
                    ],

                    'id_admin' => [
                        'name' => 'id_admin',
                        'type' => 'hidden',
//                        'form_col' =>6,
//                        'label_col' => 3,
//                        'col'=>6,
                        'disabled' =>true,
                    ],


                    'update_id_admin' => [
                        'name' => 'update_id_admin',
                        'type' => 'hidden',
//                        'form_col' =>6,
//                        'label_col' => 3,
//                        'col'=>6,
                        'disabled' =>true,
                    ],

                    'views' => [
                        'name' => 'views',
                        'label' => $this->l('瀏覽次數'),
                        'val' => 0,
                        'type' => 'view',
                        'label_col' => 3,
                    ],
                ],

            ],

            'rent_house_base' => [
//                'tab' => 'base_data',
                'legend' => [
                    'title' => $this->l('基本資料'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'house_choose' => [
                        'name' => 'house_choose',
                        'type' => 'checkbox',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'multiple' => true,
                        'label' => $this->l('房屋租售'),
                        'values' => [
                            [
                                'id' => 'house_choose_0',
                                'value' => 0,
                                'label' => $this->l('租'),
                            ],
                            [
                                'id' => 'house_choose_1',
                                'value' => 1,
                                'label' => $this->l('售'),
                            ],
                        ],
                        'required' => true,
                    ],

                    'rent_house_code' => [
                        'name' => 'rent_house_code',
                        'type' => 'text',
                        'form_col' =>6,
                        'label_col' => 3,
                        'col'=>6,
                        'label' => $this->l('內部物件編號'),
                        'maxlength' => '20',
                        'required' => true,
                        'unique'    => true,
                    ],

                    'case_name' => [
                        'name' => 'case_name',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('案名(主標)'),
                        'maxlength' => '14',
                        'required' => true,
                        'placeholder'=>'您可以輸入14個字',
                        'p'=>'<span id="case_name_remind">寧剩餘多少字數</span>',
                    ],

                    'title' => [
                        'name' => 'title',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('標題(副標)'),
                        'maxlength' => '14',
                        'required' => true,
                        'placeholder'=>'您可以輸入14個字',
                        'p'=>'<span id="title_remind">寧剩餘多少字數</span>',
                    ],

                    'id_county'          => [
                        'name'      => 'id_county',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'col_class'     =>'no_padding_right width_control',
                        'label'     => $this->l('地址'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇縣市',
                                'val'  => '',
                            ],
                            'table'   => 'county',
                            'text'    => 'county_name',
                            'value'   => 'id_county',
                            'order'   => ' `id_county` ASC',
                        ],
//                        'prefix' => $this->l('縣市鄉鎮'),
                        'required'  => true,
                        'no_active' => true,
                        'is_prefix' => true,
                    ],

                    'id_city' => [
                        'name'      => 'id_city',
                        'type'      => 'select',
                        'options'   => [
                            'default'     => [
                                'text' => '請選擇市鄉鎮',
                                'val'  => '',
                            ],
                            //自訂值
                            'parent'      => 'id_county',            //關聯鍵名稱要跟SQL資料表一樣
                            'table'       => 'city',
                            'text'        => 'city_name',
                            'value'       => 'id_city',
                            'order'       => ' `id_city` ASC',
                        ],
                        'col_class'     =>'no_padding',
                        'no_label'  => true,
                        'required'  => true,
//                        'prefix' => $this->l('市鄉政區'),
                        'is_suffix' => true,
                    ],

                    'village' => [
                        'name' => 'village',
                        'type' => 'text',
                        'form_col'  => 2,
                        'col'       => 12,
                        'maxlength' => '20',
                        'no_label'  => true,
                        'col_class'     =>'no_padding',
                        'suffix' => $this->l('里'),
                    ],

                    'neighbor' => [
                        'name' => 'neighbor',
                        'type' => 'number',
                        'form_col'  => 1,
                        'col'       => 12,
                        'maxlength' => '11',
                        'no_label'  => true,
                        'col_class'     =>'no_padding',
                        'suffix' => $this->l('鄰'),
                    ],

                    'road' => [
                        'name' => 'road',
                        'type' => 'text',
                        'form_col'  => 2,
                        'col'       => 12,
                        'maxlength' => '20',
                        'no_label'  => true,
                        'col_class'     =>'no_padding',
                    ],

                    'segment' => [
                        'name' => 'segment',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l(' '),
                        'maxlength' => '20',
                        'suffix' => $this->l('段'),
                        'is_prefix' => true,
                        'col_class'     =>'no_padding_right',
                    ],

                    'lane' => [
                        'id'   =>'lane',
                        'name' => 'lane',
                        'type' => 'text',
                        'suffix' => $this->l('巷'),
                        'is_suffix' => true,
                        'col_class'     =>'no_padding',
                    ],

                    'alley' => [
                        'id'   =>'alley',
                        'name' => 'alley',
                        'type' => 'text',
                        'form_col'  => 1,
                        'col'       => 12,
                        'no_label'  =>true,
                        'suffix' => $this->l('弄'),
                        'col_class'     =>'no_padding',
                    ],

                    'no' => [
                        'id'   =>'no',
                        'name' => 'no',
                        'type' => 'text',
                        'form_col'  => 1,
                        'col'       => 12,
                        'no_label'  =>true,
                        'suffix' => $this->l('號'),
                        'col_class'     =>'no_padding',
                    ],

                    'no_her' => [
                        'id' => 'no_her',
                        'name' => 'no_her',
                        'type' => 'text',
                        'form_col'  => 1,
                        'col'       => 12,
                        'no_label'  =>true,
                        'prefix' => $this->l('之'),
                        'col_class'     =>'no_padding',
                    ],

                    'floor' => [
                        'id'   =>'floor',
                        'name' => 'floor',
                        'type' => 'text',
                        'form_col'  => 1,
                        'col'       => 12,
                        'no_label'  =>true,
                        'suffix' => $this->l('樓'),
                        'col_class'     =>'no_padding',
                    ],

                    'floor_her' => [
                        'id' => 'floor_her',
                        'name' => 'floor_her',
                        'type' => 'text',
                        'form_col'  => 1,
                        'col'       => 12,
                        'no_label'  =>true,
                        'prefix' => $this->l('之'),
                        'col_class'     =>'no_padding',
                    ],

                    'address_room' => [
                        'name' => 'address_room',
                        'type' => 'text',
                        'form_col'  => 1,
                        'col'       => 12,
                        'maxlength' => '8',
                        'no_label'  => true,
                        'col_class'     =>'no_padding',
                        'suffix' => $this->l('室'),
                    ],

//                    'part_address' => [
//                        'name' => 'part_address',
//                        'label_col' => 3,
//                        'col' => 9,
//                        'label' => $this->l('部分地址(由前面欄位組成)'),
//                        'type' => 'text',
//                        'disabled'=>true,
//                    ],

                    'complete_address' => [
                        'name' => 'complete_address',
                        'label_col' => 3,
                        'col' => 8,
                        'label' => $this->l('完整地址(外部匯入或地址組成)'),
                        'type' => 'text',
                        'button'=>'',//function
                        'disabled'=>false,
                    ],

                    'latitude' => [
                        'name' => 'latitude',
                        'type' => 'hidden',
                    ],

                    'longitude' => [
                        'name' => 'longitude',
                        'type' => 'hidden',
                    ],
//
//                    'map' => [
//                        'name' => 'map',
//                        'label' => $this->l(''),
//                        'no_action' => true,
//                        'type' => 'map',
//                        'data' => [
//                            'address_key' => 'part_address',
//                            'lat_key' => 'latitude',
//                            'lng_key' => 'longitude',
//                            'zoom' => '14',
//                        ],
//                        'label_col' => 7,
//                        'col' => 4,
//                    ],

                    'households' => [
                        'name' => 'households',
                        'type' => 'number',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('戶數/層'),
                        'suffix' => $this->l('戶'),
                    ],

//                    'dompletion_date' => [
//                        'name' => 'dompletion_date',
//                        'type' => 'date',
//                        'form_col'  => 6,
//                        'label_col' => 3,
//                        'col'       => 6,
//                        'label' => $this->l('建築完成日'),
//                    ],

                    'dompletion_date_y' => [
                        'id'   => 'dompletion_date_y',
                        'name' => 'dompletion_date_y',
                        'type' => 'text',
                        'form_col'  => 4,
                        'label_col' => 5,
                        'col'       => 7,
                        'maxlength' => '8',
                        'label' => $this->l('建築完成日'),
                        'suffix' => $this->l('年'),
                        'is_prefix' => true,
                        'col_class'     =>'no_padding',
                    ],

                    'dompletion_date_m' => [
                        'id'   => 'dompletion_date_m',
                        'name' => 'dompletion_date_m',
                        'type' => 'text',
                        'suffix' => $this->l('月'),
                        'is_suffix' => true,
                        'maxlength' => '8',
                        'col_class'     =>'no_padding',
                    ],

                    'dompletion_date_d' => [
                        'id'   => 'dompletion_date_d',
                        'name' => 'dompletion_date_d',
                        'type' => 'text',
                        'form_col'  => 1,
                        'col'       => 12,
                        'no_label'  =>true,
                        'maxlength' => '8',
                        'suffix' => $this->l('日'),
                        'col_class'     =>'no_padding',
                    ],


                    'rental_floor_s' => [
                        'id'   =>'rental_floor_s',
                        'name' => 'rental_floor_s',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('出租樓層'),
                        'suffix' => $this->l('起'),
                        'is_prefix' => true,
                    ],

                    'rental_floor_e' => [
                        'id' => 'rental_floor_e',
                        'name' => 'rental_floor_e',
                        'type' => 'text',
                        'suffix' => $this->l('迄'),
                        'is_suffix' => true,
                    ],

                    'all_floor' => [
                        'id'   =>'all_floor',
                        'name' => 'all_floor',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'label' => $this->l('總樓層'),
                        'prefix' => $this->l('地上'),
                        'suffix' => $this->l('樓'),
                        'is_prefix' => true,
                    ],

                    'underground' => [
                        'id' => 'underground',
                        'name' => 'underground',
                        'type' => 'text',
                        'prefix' => $this->l('地下'),
                        'suffix' => $this->l('樓'),
                        'is_suffix' => true,
                    ],

                    'ping' => [
                        'name' => 'ping',
                        'type' => 'text',
                        'form_col'=>6,
                        'label_col'=>6,
                        'col'  => 6,
                        'label' => $this->l('坪數(總坪)'),
                    ],

                    'land_ping' => [
                        'id'   => 'land_ping',
                        'name' => 'land_ping',
                        'type' => 'text',
                        'form_col'=>6,
                        'label_col'=>3,
                        'col'  => 6,
                        'label' => $this->l('土地坪'),
                        'suffix' => $this->l('坪'),
                        'is_prefix' => true,
//                        'no_label'  => true,
                    ],

                    'land_ping_ratio' => [
                        'id'   => 'land_ping_ratio',
                        'name' => 'land_ping_ratio',
                        'type' => 'text',
                        'suffix' => $this->l('持比數'),
                        'is_suffix' => true,
                    ],

                    'main_construction_ping' => [
                        'id'   => 'main_construction_ping',
                        'name' => 'main_construction_ping',
                        'type' => 'text',
                        'col_class' => 'no_padding_right',
                        'label' => $this->l('建坪'),
                        'form_col'=>6,
                        'label_col'=>6,
                        'col'  => 6,
                        'prefix' => $this->l('主建坪'),
                        'is_prefix' => true,
                    ],

                    'accessory_ping' => [
                        'id'   => 'accessory_ping',
                        'name' => 'accessory_ping',
                        'type' => 'text',
                        'prefix' => $this->l('附屬坪'),
                        'is_suffix' => true,
                    ],

                    'axiom_ping' => [
                        'id'   => 'axiom_ping',
                        'name' => 'axiom_ping',
                        'type' => 'text',
                        'col_class' => 'no_padding',
                        'form_col' => 3,
                        'col'=>12,
                        'prefix' => $this->l('公設坪'),
                        'no_label' => true,
                        'is_prefix' => true,
                    ],

                    'car_space_ping' => [
                        'id'   => 'car_space_ping',
                        'name' => 'car_space_ping',
                        'type' => 'text',
                        'prefix' => $this->l('車位坪'),
                        'is_suffix' => true,
                    ],



                    'indoor_ping' => [
                        'id'   => 'indoor_ping',
                        'name' => 'indoor_ping',
                        'type' => 'text',
                        'form_col'=>2,
                        'col'  => 12,
                        'col_class' => 'no_padding',
                        'prefix' => $this->l('室內坪'),
                        'no_label'  => true,
                    ],


                    'id_web'    => [
                        'name'      => 'id_web',
                        'type'      => 'select',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'label'     => $this->l('加盟店'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇加盟店',
                                'val'  => '',
                            ],
                            'table'   => 'web',
                            'text'    => 'web',
                            'value'   => 'id_web',
                            'order'   => ' `id_web` DESC',
                            'where'   => ' AND `active` = 1',
                        ],
                        'required'  => true,
                        'no_active' => true,
//                        'disabled'  =>$disabled_id_web,
                    ],
                    'develop_ag' => [
                        'name' => 'develop_ag',
                        'type' => 'hidden',
                        'maxlength' => '20',
                    ],

                    'ag_email' => [
                        'name' => 'ag_email',
                        'type' => 'hidden',
                        'maxlength' => '128',
                    ],

                    'ag_phone'          => [
                        'name'      => 'ag_phone',
                        'type'      => 'hidden',
                        'maxlength' => '16',
                    ],

                    'ag' => [
                        'name' => 'ag',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('AG'),
                        'maxlength' => '20',
//                        'required' => true,
                    ],

                    'id_source'          => [
                        'name'      => 'id_source',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label'     => $this->l('開發來源'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇開發來源',
                                'val'  => '0',
                            ],
                            'table'   => 'source',
                            'text'    => 'source',
                            'value'   => 'id_source',
                            'order'   => ' `id_source` ASC',
                        ],
//                        'required'  => true,
                        'no_active' => true,
                    ],

                    'id_deed' => [
                        'name'      => 'id_deed',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label'     => $this->l('契據'),
                        'options'   => [
                            'default'     => [
                                'text' => '請選擇契據',
                                'val'  => '',
                            ],
                            //自訂值
                            'parent'      => 'id_web',            //關聯鍵名稱要跟SQL資料表一樣
                            'table'       => 'deed',
                            'text'        => 'deed_number',
                            'value'       => 'id_deed',
                            'order'       => '`id_web` ASC',
                        ],
                    ],

                    'look_consent' => [
                        'name' => 'look_consent',
                        'type' => 'checkbox',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 3,
                        'label' => $this->l('帶看同意書'),
                        'values' => [
                            [
                                'id' => 'look_consent',
                                'value' => 0,
                                'label' => $this->l('無'),
                            ],
                            [
                                'id' => 'look_consent',
                                'value' => 1,
                                'label' => $this->l('有'),
                            ],
                        ],
                    ],

                    'contract_time_s' => [
                        'name' => 'contract_time_s',
                        'type' => 'date',
                        'form_col'=>12,
                        'label_col' => 3,
                        'col'   =>6,
                        'label' => $this->l('仲介契約期間'),
                        'is_prefix' => true,
                    ],

                    'contract_time_e' => [
                        'name' => 'contract_time_e',
                        'type' => 'date',
                        'prefix' => $this->l('~'),
                        'is_suffix' => true,
                    ],

                    'contract_time2_s' => [
                        'name' => 'contract_time2_s',
                        'type' => 'date',
                        'form_col'=>12,
                        'label_col' => 3,
                        'col'   => 6,
                        'label' => $this->l('租管契約期間'),
                        'is_prefix' => true,
                    ],

                    'contract_time2_e' => [
                        'name' => 'contract_time2_e',
                        'type' => 'date',
                        'prefix' => $this->l('~'),
                        'is_suffix' => true,
                    ],

                    'lease_period' => [
                        'name'      => 'lease_period',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('現況'),
                        'options'   => [
                            'default'     => [
                                'text' => '選擇現況',
                                'val'  => '',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '待租',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => '租客租到',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '屋主租到',
                                    'val'  => '2',
                                ],
                            ],
                        ],
                        'is_prefix' => true,
                    ],

                    'other_status' => [
                        'id'   => 'other_status',
                        'name' => 'other_status',
                        'type' => 'text',
                        'no_label' => true,
                        'maxlongth' => "20",
                        'prefix' => $this->l('-'),
                        'is_suffix' => true,
                    ],

                    'look_time_e' => [
                        'name' => 'look_time_e',
                        'type' => 'date',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('開案I帶看期間'),
                    ],

                    'housing_time_s' => [
                        'name' => 'housing_time_s',
                        'type' => 'date',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'label' => $this->l('入住時間'),
                    ],

                    'pause' => [
                        'name' => 'pause',
                        'type' => 'checkbox',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 4,
                        'col_class'     =>'no_padding_right',
                        'label' => $this->l('暫停帶看'),
                        'values' => [
                            [
                                'id' => 'pause',
                                'value' => 1,
                                'label' => $this->l(''),
                            ],
                        ],
                        'no_active' => true,
                        'is_prefix' => true,
                    ],

                    'pause_type' => [
                        'name'      => 'pause_type',
                        'type'      => 'select',
                        'options'   => [
                            'default'     => [
                                'text' => '暫停帶看原因',
                                'val'  => '0',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '出租中',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '收訂中',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '暫自用',
                                    'val'  => '3',
                                ],
                                [
                                    'text' => '其他',
                                    'val'  => '4',
                                ],
                            ],
                        ],
                        'col_class'     =>'no_padding',
                        'no_label'  => true,
                        'is_suffix' => true,
                    ],

                    'pause_type_other' => [
                        'name' => 'pause_type_other',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'label' => $this->l('備註'),
                        'maxlength' => '50',
                    ],

//                    'borrow' => [
//                        'name' => 'borrow',
//                        'type' => 'text',
//                        'label_col' => 3,
//                        'label' => $this->l('另借聯絡'),
//                        'maxlength' => '20',
//                    ],

                    'id_main_house_class'          => [
                        'name'      => 'id_main_house_class',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 3,
                        'label'     => $this->l('物件用途'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇用途',
                                'val'  => '',
                            ],
                            'table'   => 'main_house_class',
                            'text'    => 'title',
                            'value'   => 'id_main_house_class',
                            'order'   => ' `id_main_house_class` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'id_rent_house_types'          => [
                        'name'      => 'id_rent_house_types',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 3,
                        'label'     => $this->l('物件型態'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇型態',
                                'val'  => '',
                            ],
                            'table'   => 'rent_house_types',
                            'text'    => 'title',
                            'value'   => 'id_rent_house_types',
                            'order'   => ' `id_rent_house_types` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'id_rent_house_type' => [
                        'type' => 'checkbox',
                        'label' => $this->l('物件類別(複選)'),
                        'name' => 'id_rent_house_type',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $rent_house_type,
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'genre' => [
                        'name' => 'genre',
                        'type' => 'checkbox',
                        'label_col' => 3,
                        'col'=> 9,
                        'label' => $this->l('隔間材質'),
                        'values' => [
                            [
                                'id' => 'genre_3',
                                'value' => 3,
                                'label' => $this->l('夾層'),
                            ],
                            [
                                'id' => 'genre_4',
                                'value' => 4,
                                'label' => $this->l('木板'),
                            ],
                            [
                                'id' => 'genre_5',
                                'value' => 5,
                                'label' => $this->l('水泥'),
                                'checked' =>true
                            ],
                        ],
                    ],

                    'width' => [
                        'id'   =>'width',
                        'name' => 'width',
                        'type' => 'text',
                        'col_class'=>'no_padding_right',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col'  => 6,
                        'label' => $this->l('寬深高'),
                        'suffix' => $this->l('寬'),
                        'is_prefix' => true,
                    ],

                    'depth' => [
                        'id' => 'depth',
                        'name' => 'depth',
                        'type' => 'text',
                        'suffix' => $this->l('深'),
                        'is_suffix' => true,
                    ],

                    'height' => [
                        'id' => 'height',
                        'name' => 'height',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col' => 12,
                        'suffix' => $this->l('高'),
                        'no_label' => true,
                    ],

                    'road_width' => [
                        'id' => 'road_width',
                        'name' => 'road_width',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'form_col' => 3,
                        'col'  => 12,
                        'suffix' => $this->l('道路寬度(面前道路)'),
                        'no_label' => true,
                    ],

                    'patio_name'=>[
                        'form_col' => 3,
                        'label_col' => 12,
                        'label' => $this->l('庭院'),
                        'label_class'=> 'patio_name',
                    ],

                    'patio' => [
                        'id'        => 'patio',
                        'name'      => 'patio',
                        'type'      => 'select',
                        'form_col' => 1,
                        'col'  => 12,
                        'options'   => [
                            'val'         => [
                                [
                                    'text' => 'N',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => 'Y',
                                    'val'  => '1',
                                ],
                            ],
                        ],
                        'no_label'=>true,
                    ],

                    'patio_remarks' => [
                        'id' => 'patio_remarks',
                        'name' => 'patio_remarks',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'label' => $this->l('備註'),
                        'form_col' => 6,
                        'label_col' => 2,
                        'col' => 6,
                    ],

                    'elevator' => [
                        'type' => 'switch',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('電梯'),
                        'name' => 'elevator',
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'elevator_off',
                                'value' => 0,
                                'label' => $this->l('無電梯'),
                            ],
                            [
                                'id' => 'elevator_on',
                                'value' => 1,
                                'label' => $this->l('有電梯'),
                            ],
                        ],
                    ],

                ],
            ],

            'variety_data' => [
//                'tab' => 'variety_data',
                'legend' => [
                    'title' => $this->l('基本資料(可變)'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'client_elevator' => [
                        'id'   =>'client_elevator',
                        'name' => 'client_elevator',
                        'type' => 'text',
                        'label_col'=> 3,
                        'col'=> 6,
                        'label' => $this->l('客梯貨梯'),
                        'prefix' => $this->l('客梯'),
                        'is_prefix' => true,
                    ],

                    'freight_elevator' => [
                        'id' => 'freight_elevator',
                        'name' => 'freight_elevator',
                        'type' => 'text',
                        'prefix' => $this->l('貨梯'),
                        'is_suffix' => true,
                    ],

                    'room' => [
                        'id'   =>'room',
                        'name' => 'room',
                        'type' => 'number',
                        'form_col' => 6,
                        'label_col'=> 6,
                        'col'=> 6,
                        'col_class'=>'no_padding_right',
                        'label' => $this->l('房廳衛室廚'),
                        'suffix' => $this->l('房'),
                        'is_prefix' => true,
                    ],

                    'hall' => [
                        'id' => 'hall',
                        'name' => 'hall',
                        'type' => 'number',
                        'suffix' => $this->l('廳'),
                        'is_suffix' => true,
                    ],

                    'bathroom' => [
                        'id'   =>'bathroom',
                        'name' => 'bathroom',
                        'type' => 'number',
                        'col_class'=>'no_padding',
                        'form_col' => 3,
                        'col'=> 12,
                        'suffix' => $this->l('衛'),
                        'no_label'=> true,
                        'is_prefix' => true,
                    ],

                    'muro' => [
                        'id' => 'muro',
                        'name' => 'muro',
                        'type' => 'number',
                        'suffix' => $this->l('室'),
                        'is_suffix' => true,
                    ],

                    'kitchen' => [
                        'id'   =>'kitchen',
                        'name' => 'kitchen',
                        'type' => 'number',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col'=> 12,
                        'suffix' => $this->l('廚房'),
                        'no_label'=> true,
                    ],

                    'balcony_front' => [
                        'id'   =>'balcony_front',
                        'name' => 'balcony_front',
                        'type' => 'number',
                        'col_class'=>'no_padding_right',
                        'form_col' => 6,
                        'label_col'=> 6,
                        'col'=> 6,
                        'label' => $this->l('陽台'),
                        'suffix' => $this->l('前陽台'),
//                        'is_prefix' => true,
                    ],

                    'balcony_back' => [
                        'id' => 'balcony_back',
                        'name' => 'balcony_back',
                        'type' => 'number',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col'=> 12,
                        'no_label' =>true,
                        'suffix' => $this->l('後陽台'),
                    ],

                    'room_balcony' => [
                        'id' => 'room_balcony',
                        'name' => 'room_balcony',
                        'type' => 'number',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col'=> 12,
                        'no_label' =>true,
                        'suffix' => $this->l('房間陽台'),
                    ],

                    'pick_high' => [
                        'id'   =>'pick_high',
                        'name' => 'pick_high',
                        'type' => 'text',
                        'col_class'=>'no_padding_right',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col'  => 4,
                        'label' => $this->l('挑高夾層(米)'),
                        'prefix' => $this->l('挑高'),
                        'suffix' => $this->l('米'),
                        'is_prefix' => true,
                    ],

                    'mezzanine' => [
                        'id' => 'mezzanine',
                        'name' => 'mezzanine',
                        'type' => 'text',
                        'prefix' => $this->l('夾層'),
                        'suffix' => $this->l('米'),
                        'is_suffix' => true,
                    ],

                    'compartment' => [
                        'id' => 'compartment',
                        'name' => 'compartment',
                        'col_class'=>'no_padding_right',
                        'type' => 'text',
                        'label' => $this->l('房間數'),
                        'form_col' => 12,
                        'label_col' =>3,
                        'col'  => 3,
                        'prefix' => $this->l('隔間'),
                        'suffix' => $this->l('數'),
                    ],


                    'lighting_num' => [
                        'id' => 'lighting_num',
                        'name' => 'lighting_num',
                        'col_class'=>'no_padding_right',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' =>6,
                        'col' => 6,
                        'label' => $this->l('採光'),
                        'prefix' => $this->l('採光'),
                        'suffix' => $this->l('面'),
                    ],

                    'is_darkroom' => [
                        'id' => 'is_darkroom',
                        'name' => 'is_darkroom',
                        'type' => 'select',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col' => 12,
                        'prefix' => $this->l('暗房'),
                        'options'   => [
                            'val'         => [
                                [
                                    'text' => 'N',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => 'Y',
                                    'val'  => '1',
                                ],
                            ],
                        ],
                        'no_label'=>true,
                    ],

                    'is_sideroom' => [
                        'id' => 'is_sideroom',
                        'name' => 'is_sideroom',
                        'type' => 'select',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col' => 12,
                        'prefix' => $this->l('邊間'),
                        'options'   => [
                            'val'         => [
                                [
                                    'text' => 'N',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => 'Y',
                                    'val'  => '1',
                                ],
                            ],
                        ],
                        'no_label'=>true,
                    ],

                    'is_window' => [
                        'id' => 'is_window',
                        'name' => 'is_window',
                        'type' => 'select',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col' => 12,
                        'prefix' => $this->l('對外窗戶'),
                        'options'   => [
                            'val'         => [
                                [
                                    'text' => 'N',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => 'Y',
                                    'val'  => '1',
                                ],
                            ],
                        ],
                        'no_label'=>true,
                    ],

//
//                    'sideroom_num' => [
//                        'id' => 'sideroom_num',
//                        'name' => 'sideroom_num',
//                        'type' => 'text',
//                        'col_class'=>'no_padding',
//                        'form_col' => 3,
//                        'col' => 12,
//                        'no_label'=>true,
//                        'prefix' => $this->l('邊間'),
//                        'suffix' => $this->l('數'),
//                    ],
//
//                    'window_num' => [
//                        'id' => 'window_num',
//                        'name' => 'window_num',
//                        'type' => 'text',
//                        'col_class'=>'no_padding_right',
//                        'form_col' => 6,
//                        'label_col' =>3,
//                        'col' => 6,
//                        'label' => $this->l('對外窗戶(數)'),
//                        'prefix' => $this->l('對外窗戶'),
//                        'suffix' => $this->l('數'),
//                    ],
//
//

                    'community_seat' => [
                        'id' => 'community_seat',
                        'name' => 'community_seat',
                        'type' => 'text',
                        'col_class'=>'no_padding_right',
                        'form_col' => 6,
                        'label_col' =>6,
                        'col' => 6,
                        'label' => $this->l('方位座向'),
                        'prefix' => $this->l('社區'),
                        'maxlength' => '20',
                        'is_prefix' => true,
                    ],

                    'building_seat' => [
                        'id' => 'building_seat',
                        'name' => 'building_seat',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'prefix' => $this->l('一樓'),
                        'maxlength' => '20',
                        'is_suffix' => true,
                        'no_label'  => true,
                    ],

                    'house_door_seat' => [
                        'id' => 'house_door_seat',
                        'name' => 'house_door_seat',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'form_col' => 6,
                        'col' => 6,
                        'prefix' => $this->l('大門'),
                        'maxlength' => '20',
                        'is_prefix' => true,
                        'no_label'  => true,
                    ],

                    'balcony_seat' => [
                        'id' => 'balcony_seat',
                        'name' => 'balcony_seat',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'prefix' => $this->l('前陽台'),
                        'maxlength' => '20',
                        'is_suffix' => true,
                        'no_label'  => true,
                    ],

//                    'exterior_wall_other' => [
//                        'id' => 'exterior_wall_other',
//                        'name' => 'exterior_wall_other',
//                        'type' => 'text',
//                        'form_col'  => 6,
//                        'label_col' => 3,
//                        'col'       => 6,
//                        'label' => $this->l('外牆材質其他'),
//                        'maxlength' => '50',
//                    ],

                    'hr_community_end' => [
                        'type' => 'hr',
                    ],

                    'id_other_conditions' => [
                        'type' => 'checkbox',
                        'label' => $this->l('其他條件(複選)'),
                        'name' => 'id_other_conditions',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $other_conditions,
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'situation' => [
                        'name'      => 'situation',
                        'type'      => 'select',
                        'form_col' => 6,
                        'label_col'=> 6,
                        'col' => 3,
                        'label' => $this->l('租期'),
                        'options'   => [
                            'default'     => [
                                'text' => '選擇租期週期',
                                'val'  => '',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '空屋',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => '自助',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '租客',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '其他',
                                    'val'  => '3',
                                ],
                            ],
                        ],
                    ],

                    'm_s' => [
                        'name' => 'm_s',
                        'type' => 'number',
                        'form_col' => 6,
                        'label_col'=> 3,
                        'col' => 6,
                        'label' => $this->l('短租時間'),
                        'is_prefix' => true,
                    ],

                    'm_e' => [
                        'name' => 'm_e',
                        'type' => 'number',
                        'prefix' => $this->l('~'),
                        'suffix' => $this->l('月'),
                        'is_suffix' => true,
                    ],

                    'age' => [
                        'id'   => 'age',
                        'name' => 'age',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col'=> 6,
                        'col' => 6,
                        'label' => $this->l('年齡限制(歲以上)'),
                        'maxlongth' => "20",
                    ],

                    'year_old'=> [
                        'id'   => 'year_old',
                        'name' => 'year_old',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col'=> 3,
                        'col' => 9,
                        'label' => $this->l('幾歲以下幼童限制'),
                        'maxlongth' => "20",
                    ],

                    'pet_remarks'=> [
                        'id'   => 'pet_remarks',
                        'name' => 'pet_remarks',
                        'type' => 'text',
                        'form_col' => 12,
                        'label_col'=> 3,
                        'col' => 9,
                        'label' => $this->l('寵物備註'),
                        'maxlongth' => "64",
                    ],


                    'limit_industry'=> [
                        'id'   => 'limit_industry',
                        'name' => 'limit_industry',
                        'type' => 'text',
                        'label' => $this->l('限制行業'),
                        'maxlongth' => "128",
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],
                ],

            ],

            'member_landlord' => [
                'tpl'=>'themes/LifeHouse/landlord.tpl',
            ],

            'rent_related' => [
//                'tab' => 'variety_data',
                'legend' => [
                    'title' => $this->l('租金相關'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                    'rent_price_m' => [
                        'name' => 'rent_price_m',
                        'type' => 'number',
                        'label' => $this->l('租金開價(月)'),
                        'suffix' => '/月',
                        'form_col' =>6,
                        'label_col' =>6,
                        'col'=>6,
                    ],

                    'reserve_price_m' => [
                        'name' => 'reserve_price_m',
                        'type' => 'number',
                        'label' => $this->l('租金底價(月)'),
                        'suffix' => '/月',
                        'form_col' =>3,
                        'label_col' =>5,
                        'col'=>7,
                    ],

                    'deposit' => [
                        'id'   => 'deposit',
                        'name' => 'deposit',
                        'type' => 'number',
                        'label' => $this->l('押金'),
                        'form_col' =>3,
                        'label_col' =>4,
                        'col'=>8,
                        'suffix' => '月',
                    ],

                    'cleaning_fee_type' => [
                        'type' => 'switch',
                        'form_col' =>6,
                        'label_col' =>6,
                        'col'=>6,
                        'label' => $this->l('清潔費(管理費)'),
                        'name' => 'cleaning_fee_type',
                        'val' => '',
                        'values' => [
                            [
                                'id' => 'cleaning_fee_type_off',
                                'value' => 0,
                                'label' => $this->l('不包含'),
                            ],
                            [
                                'id' => 'cleaning_fee_type_on',
                                'value' => 1,
                                'label' => $this->l('包含'),
                            ],
                        ],
                    ],

                    'cleaning_fee' => [
                        'name' => 'cleaning_fee',
                        'type' => 'text',
                        'class' =>'cleaning_fee_type',
                        'label' => $this->l('清潔費(管理費)'),
                        'form_col' =>6,
                        'label_col' =>3,
                        'col'=>6,
                    ],

                    'cleaning_fee_cycle' => [
                        'name'      => 'cleaning_fee_cycle',
                        'type'      => 'select',
                        'class' =>'cleaning_fee_type',
                        'form_col' =>6,
                        'label_col' =>6,
                        'col'=>6,
                        'label' => $this->l('清潔費收費週期'),
                        'required' => true,
                        'options'   => [
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '月',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '季',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '半年',
                                    'val'  => '3',
                                ],
                                [
                                    'text' => '年',
                                    'val'  => '4',
                                ],
                            ],
                        ],
                    ],

                    'cleaning_fee_unit' => [
                        'name'      => 'cleaning_fee_unit',
                        'type'      => 'select',
                        'class' =>'cleaning_fee_type',
                        'form_col' =>6,
                        'label_col' =>3,
                        'col'=>6,
                        'label' => $this->l('清潔費分攤'),
                        'options'   => [
                            'default'     => [
                                'text' => '選擇清潔費分攤方式',
                                'val'  => '0',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '坪',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '戶',
                                    'val'  => '2',
                                ],
                            ],
                        ],
                    ],


                    'water_fee_type' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('水費'),
                        'name' => 'water_fee_type',
                        'val' => '',
                        'values' => [
                            [
                                'id' => 'water_fee_type_off',
                                'value' => 0,
                                'label' => $this->l('不包含'),
                            ],
                            [
                                'id' => 'water_fee_type_on',
                                'value' => 1,
                                'label' => $this->l('包含'),
                            ],
                        ],
                    ],

                    'water_fee_cycle' => [
                        'name'      => 'water_fee_cycle',
                        'type'      => 'select',
                        'water_fee_type' =>'water_fee_type',
                        'form_col' =>6,
                        'label_col' =>6,
                        'col'=>6,
                        'label' => $this->l('水費收費週期'),
                        'options'   => [
                            'default'     => [
                                'text' => '選擇水費收費週期',
                                'val'  => '0',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '月',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '季',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '半年',
                                    'val'  => '3',
                                ],
                                [
                                    'text' => '年',
                                    'val'  => '4',
                                ],
                            ],
                        ],
                    ],

                    'water_fee_m' => [
                        'id'   => 'water_fee_m',
                        'name' => 'water_fee_m',
                        'type' => 'text',
                        'water_fee_type' =>'water_fee_type',
                        'label' => $this->l('水費(度)'),
                        'suffix' => $this->l('每度'),
                        'is_prefix' => true,
                        'form_col' =>6,
                        'label_col' =>3,
                        'col'=>9,
                    ],

                    'water_fee' => [
                        'id'   => 'water_fee',
                        'name' => 'water_fee',
                        'type' => 'text',
                        'water_fee_type' =>'water_fee_type',
                        'suffix' => $this->l('戶/元'),
                        'is_suffix' => true,
                    ],


                    'electricity_fee_type' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('電費'),
                        'name' => 'electricity_fee_type',
                        'val' => '',
                        'values' => [
                            [
                                'id' => 'electricity_fee_type_off',
                                'value' => 0,
                                'label' => $this->l('不包含'),
                            ],
                            [
                                'id' => 'electricity_fee_type_on',
                                'value' => 1,
                                'label' => $this->l('包含'),
                            ],
                        ],
                    ],

                    'electricity_fee_cycle' => [
                        'name'      => 'electricity_fee_cycle',
                        'type'      => 'select',
                        'class' =>'electricity_fee_type',
                        'form_col' =>6,
                        'label_col' =>6,
                        'col'=>6,
                        'label' => $this->l('電費收費週期'),
                        'options'   => [
                            'default'     => [
                                'text' => '選擇電費收費週期',
                                'val'  => '0',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '月',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '季',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '半年',
                                    'val'  => '3',
                                ],
                                [
                                    'text' => '年',
                                    'val'  => '4',
                                ],
                            ],
                        ],
                    ],

                    'electricity_fee_m' => [
                        'id'   => 'electricity_fee_m',
                        'name' => 'electricity_fee_m',
                        'type' => 'text',
                        'class' =>'electricity_fee_type',
                        'label' => $this->l('電費(度)'),
                        'suffix' => $this->l('每度'),
                        'is_prefix' => true,
                        'form_col' =>6,
                        'label_col' =>3,
                        'col'=>9,
                    ],

                    'electricity_fee' => [
                        'id'   => 'electricity_fee',
                        'name' => 'electricity_fee',
                        'type' => 'text',
                        'class' =>'electricity_fee_type',
                        'suffix' => $this->l('戶/元'),
                        'is_suffix' => true,
                    ],


                    'gas_fee_type' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('瓦斯費'),
                        'name' => 'gas_fee_type',
                        'val' => '',
                        'values' => [
                            [
                                'id' => 'gas_fee_type_off',
                                'value' => 0,
                                'label' => $this->l('不包含'),
                            ],
                            [
                                'id' => 'gas_fee_type_on',
                                'value' => 1,
                                'label' => $this->l('包含'),
                            ],
                        ],
                    ],

                    'gas_fee_cycle' => [
                        'name'      => 'gas_fee_cycle',
                        'type'      => 'select',
                        'class' =>'gas_fee_type',
                        'form_col' =>6,
                        'label_col' =>6,
                        'col'=>6,
                        'label' => $this->l('瓦斯費收費週期'),
                        'options'   => [
                            'default'     => [
                                'text' => '選擇瓦斯費收費週期',
                                'val'  => '0',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '月',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '季',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '半年',
                                    'val'  => '3',
                                ],
                                [
                                    'text' => '年',
                                    'val'  => '4',
                                ],
                            ],
                        ],
                    ],

                    'gas_fee_m' => [
                        'id'   => 'gas_fee_m',
                        'name' => 'gas_fee_m',
                        'type' => 'text',
                        'class' =>'gas_fee_type',
                        'form_col' =>6,
                        'label_col' =>3,
                        'col'=>9,
                        'label' => $this->l('瓦斯費(度)'),
                        'suffix' => $this->l('每度'),
                        'is_prefix' => true,
                    ],

                    'gas_fee' => [
                        'id'   => 'gas_fee',
                        'name' => 'gas_fee',
                        'type' => 'text',
                        'class' =>'gas_fee_type',
                        'suffix' => $this->l('戶/元'),
                        'is_suffix' => true,
                    ],

                    'pay_tv' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'col'=> 9,
                        'label' => $this->l('第四台'),
                        'name' => 'pay_tv',
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'pay_tv_off',
                                'value' => 0,
                                'label' => $this->l('不包含'),
                            ],
                            [
                                'id' => 'pay_tv_on_1',
                                'value' => 1,
                                'label' => $this->l('包含:申裝'),
                            ],
                            [
                                'id' => 'pay_tv_on_2',
                                'value' => 2,
                                'label' => $this->l('包含:收視'),
                            ],
                        ],
                    ],

                    'net_fee_type' => [
                        'type' => 'switch',
                        'label' => $this->l('網路'),
                        'name' => 'net_fee_type',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'net_fee_type_off',
                                'value' => 0,
                                'label' => $this->l('不包含'),
                            ],
                            [
                                'id' => 'net_fee_type_on',
                                'value' => 1,
                                'label' => $this->l('包含'),
                            ],
                        ],
                    ],


                    'net_remarks' => [
                        'id' => 'net_remarks',
                        'name' => 'net_remarks',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 9,
                        'label' => $this->l('網路備註'),
                        'maxlongth' => 64,
                    ],

                    'park_fee_type' => [
                        'type' => 'switch',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('停車位租金(汽機車)'),
                        'name' => 'park_fee_type',
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'park_fee_type_off',
                                'value' => 0,
                                'label' => $this->l('不包含'),
                            ],
                            [
                                'id' => 'park_fee_type_on',
                                'value' => 1,
                                'label' => $this->l('包含'),
                            ],
                        ],
                    ],

                    'car_space' =>[
                        'name'      => 'car_space',
                        'type'      => 'select',
                        'label' => $this->l('車位'),
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'options'   => [
                            'default'     => [
                                'text' => '選擇停車方式',
                                'val'  => '',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '門口',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => '庭院',
                                    'val'  => '1',
                                ],
                            ],
                        ],
                    ],

                    'cars'=>[
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/park_space.tpl',
                        'label' => $this->l('汽車位'),
                        'no_action'=>true,
                        'name' => 'cars',
                        'form_col' =>12,
                        'label_col' =>3,
                        'col'=>9,
                    ],

                    'motorcycle'=>[
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/park_space.tpl',
                        'label' => $this->l('機車位'),
                        'no_action'=>true,
                        'name' => 'motorcycle',
                        'form_col' =>12,
                        'label_col' =>3,
                        'col'=>9,
                    ],

                ],

            ],

            'device' => [
//                'tab' => 'variety_data',
                'legend' => [
                    'title' => $this->l('家具/家電/設備(前台顯示)'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                ],
            ],

            'other_conditions' => [
//                'tab' => 'variety_data',
                'legend' => [
                    'title' => $this->l('其他條件'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'remarks' =>[
                        'id' => 'remarks',
                        'name' => 'remarks',
                        'type' => 'textarea',
                        'col' => 9,
                        'label' => $this->l('備註'),
                        'rows' =>   '10',
                        'cols'=>'5',
                    ],
                ],


            ],

            'advertise_data' => [
//                'tab' => 'advertise',
                'legend' => [
                    'title' => $this->l('廣告相關'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'time_s' => [
                        'id'   => 'advertise_date_s',
                        'name' => 'advertise_date_s',
                        'type' => 'date',
                        'label' => $this->l('廣告時間'),
                        'is_prefix' => true,
                    ],

                    'advertise_date_e' => [
                        'id'   => 'advertise_date_e',
                        'name' => 'advertise_date_e',
                        'type' => 'date',
                        'prefix' => $this->l('~'),
                        'is_suffix' => true,
                    ],

                    'ad_from' => [
                        'name' => 'ad_from',
                        'type' => 'checkbox',
                        'label_col' => 3,
                        'col' => 9,
                        'label' => $this->l('廣告'),
                        'values' => [
                            [
                                'id' => 'ad_from_l',
                                'value' => 1,
                                'label' => $this->l('自上廣告'),
                            ],
                        ],
                        'is_prefix' => true,
                    ],

                    'ad_name' => [
                        'name' => 'ad_name',
                        'type' => 'text',
                        'prefix' => $this->l('廣告名稱'),
                        'is_suffix' => true,
                        'maxlength'=>'20',
                    ],

                    'plate' => [
                        'name' => 'plate',
                        'type' => 'checkbox',
                        'label_col' => 3,
                        'label' => $this->l('字牌'),
                        'values' => [
                            [
                                'id' => 'plate_l',
                                'value' => 1,
                                'label' => $this->l('有'),
                            ],
                        ],
                    ],

                    'red_paper' => [
                        'name' => 'red_paper',
                        'type' => 'checkbox',
                        'label_col' => 3,
                        'label' => $this->l('紅紙'),
                        'values' => [
                            [
                                'id' => 'red_paper_l',
                                'value' => 1,
                                'label' => $this->l('有'),
                            ],
                        ],
                    ],

                    'canvas' => [
                        'name' => 'canvas',
                        'type' => 'checkbox',
                        'label_col' => 3,
                        'label' => $this->l('帆布'),
                        'values' => [
                            [
                                'id' => 'canvas_l',
                                'value' => 1,
                                'label' => $this->l('有'),
                            ],
                        ],
                    ],

                    'features'                => [
                        'name'      => 'features',
                        'type'      => 'textarea',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'     => $this->l('特色'),
                        'class'     => 'tinymce',
//                        'required'  => true,
                        'cols'      =>5,
                        'rows'      => 10,
                    ],

                ],

            ],

            'community_data'=>[
//                'tab'=>'community_data',
                'legend' => [
                    'title' => $this->l('社區相關資料'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],
                'input'=>[
                    'id_rent_house' => [
                        'name' => 'id_rent_house',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

//                    'id_rent_house_community'          => [
//                        'name'      => 'id_rent_house_community',
//                        'type'      => 'select',
//                        'label_col' => 3,
//                        'label'     => $this->l('社區相關資料(選擇之後會自動讀入如有修改會與選擇的作連動)'),
//                        'options'   => [
//                            'default' => [
//                                'text' => '請選擇社區',
//                                'val'  => '',
//                            ],
//                            'table'   => 'rent_house_community',
//                            'text'    => 'community',
//                            'value'   => 'id_rent_house_community',
//                            'order'   => ' `position` ASC',
//                        ],
//                        'no_active' => true,
//                    ],
                    'id_rent_house_community'=>[
                        'name'      => 'id_rent_house_community',
                        'id'      => 'id_rent_house_community',
                        'type' => 'hidden',
                        'label_col' => 3,
                    ],

                    'rent_house_community_code' => [
                        'name' => 'rent_house_community_code',
                        'type' => 'text',
                        'form_col'=>6,
                        'label_col'=>6,
                        'col'=>6,
                        'label' => $this->l('社區流水號'),
                        'maxlength' => '20',
                    ],


                    'community' => [
                        'name' => 'community',
                        'id'   => 'community',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'=>6,
                        'label_col'=>3,
                        'col'=>6,
                        'label' => $this->l('社區名稱'),
                        'maxlength' => '20',
                    ],

                    'all_num' => [
                        'name' => 'all_num',
                        'type' => 'text',
                        'maxlength' =>'20',
                        'class' => 'community_data',
                        'form_col'=>6,
                        'label_col'=>6,
                        'col'=>6,
                        'label' => $this->l('總戶數'),
                    ],

                    'household_num' => [
                        'id'   => 'household_num',
                        'name' => 'household_num',
                        'type' => 'number',
                        'class' => 'community_data',
                        'form_col'=>6,
                        'label_col'=>3,
                        'col'=>7,
                        'label' => $this->l('戶數'),
                        'prefix' => $this->l('住家'),
                        'is_prefix' => true,
                    ],

                    'storefront_num' => [
                        'id'   => 'storefront_num',
                        'name' => 'storefront_num',
                        'type' => 'number',
                        'class' => 'community_data',
                        'prefix' => $this->l('店面'),
                        'is_suffix' => true,
                    ],

                    'id_exterior_wall'          => [
                        'name'      => 'id_exterior_wall',
                        'type'      => 'select',
                        'class' => 'community_data',
                        'form_col'=>6,
                        'label_col'=>6,
                        'col'=>6,
                        'label'     => $this->l('外牆材質'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇材質',
                                'val'  => '0',
                            ],
                            'table'   => 'exterior_wall',
                            'text'    => 'exterior_wall',
                            'value'   => 'id_exterior_wall',
                            'order'   => ' `position` ASC',
                        ],
//                        'required'  => true,
                        'no_active' => true,
                    ],

//                    'building_number' => [
//                        'name' => 'building_number',
//                        'type' => 'text',
//                        'label' => $this->l('區名I棟號'),
//                        'maxlength' => '20',
//                    ],

                    'property_management' => [
                        'name' => 'property_management',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'=>6,
                        'label_col'=>3,
                        'col'=>6,
                        'label' => $this->l('物管公司'),
                        'maxlength' => '20',
                    ],

                    'community_phone' => [
                        'id'   => 'community_phone',
                        'name' => 'community_phone',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 6,
                        'is_prefix' => true,
                        'label' => $this->l('社區電話/傳真'),
                        'prefix' => $this->l('電話'),
                    ],

                    'community_fax' => [
                        'id'   => 'community_fax',
                        'name' => 'community_fax',
                        'type' => 'text',
                        'class' => 'community_data',
                        'is_suffix' => true,
                        'prefix' => $this->l('傳真'),
                    ],

                    'builders' => [
                        'name' => 'builders',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('建商'),
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'maxlength'=>'20'
                    ],

                    'redraw_area' => [
                        'label' => $this->l('計劃/重劃區'),
                        'name' => 'redraw_area',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'maxlongth' =>'20',
                    ],

                    'clean_time'=>[
                        'id'   => 'clean_time',
                        'name' => 'clean_time',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('清潔隊收取時間'),
                        'maxlongth' =>100,
                    ],

                    'id_public_utilities' =>[
                        'type' => 'checkbox',
                        'label' => $this->l('物管消防(複選)'),
                        'name' => 'id_public_utilities',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $public_utilities['3'],
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'time_s' => [
                        'id'   => 'time_s',
                        'name' => 'time_s',
                        'type' => 'time',
                        'class' => 'community_data',
                        'label' => $this->l('保全時間'),
                        'is_prefix' => true,
                        'maxlongth' =>16,
                    ],

                    'time_e' => [
                        'id'   => 'time_e',
                        'name' => 'time_e',
                        'type' => 'time',
                        'class' => 'community_data',
                        'prefix' => $this->l('~'),
                        'is_suffix' => true,
                    ],

                    'id_public_utilities_two' =>[
                        'type' => 'checkbox',
                        'label' => $this->l('設施(複選)'),
                        'name' => 'id_public_utilities',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $public_utilities['1'],
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'swimming_pool_type'=>[
                        'type' => 'switch',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'label' => $this->l('泳池'),
                        'name' => 'swimming_pool_type',
                        'values' => [
                            [
                                'id' => 'swimming_pool_type_off',
                                'value' => 0,
                                'label' => $this->l('室外'),
                            ],
                            [
                                'id' => 'swimming_pool_type_on',
                                'value' => 1,
                                'label' => $this->l('室內'),
                            ],
                        ],
                    ],

                    'e_school' => [
                        'name' => 'e_school',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label'=> $this->l('國小:'),
                        'maxlength'  => '20',
                    ],

                    'j_school' => [
                        'name' => 'j_school',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'=> $this->l('國中:'),
                        'maxlength'  => '20',
                    ],

                    'bus' => [
                        'name' => 'bus',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('公車'),
                        'maxlength' => '20',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'bus_station' => [
                        'name' => 'bus_station',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('站牌'),
                        'maxlength' => '64',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'passenger_transport' => [
                        'name' => 'passenger_transport',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('客運'),
                        'maxlength' => '20',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'passenger_transport_station' => [
                        'name' => 'passenger_transport_station',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('站牌'),
                        'maxlength' => '64',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'high_speed_rail' => [
                        'name' => 'high_speed_rail',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'label' => $this->l('高鐵站'),
                        'maxlength' => '20',
                    ],

                    'train' => [
                        'name' => 'train',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 9,
                        'label' => $this->l('火車站'),
                        'maxlength' => '20',
                    ],

                    'mrt' => [
                        'name' => 'mrt',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('捷運線'),
                        'maxlength' => '20',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'mrt_station' => [
                        'name' => 'mrt_station',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('捷運站'),
                        'maxlength' => '64',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'interchange' => [
                        'name' => 'interchange',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('交流道'),
                        'maxlength' => '20',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'hospital' => [
                        'name' => 'hospital',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('醫院'),
                        'maxlength' => '64',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    'park' => [
                        'name' => 'park',
                        'type' => 'text',
                        'class' => 'community_data',
//                        'form_col'  => 4,
//                        'label_col' => 3,
//                        'col'       => 9,
                        'label' => $this->l('公園:'),
                        'maxlength' => '20',
                    ],

                    'supermarket' => [
                        'name' => 'supermarket',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('超商/超市'),
                        'maxlength' => '20',
                    ],

                    'shopping_center' => [
                        'name' => 'shopping_center',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'label' => $this->l('百貨公司'),
                        'maxlength' => '20',
                    ],

                    'market' => [
                        'name' => 'market',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('傳統市場'),
                        'maxlength' => '20',
                    ],

                    'night_market' => [
                        'name' => 'night_market',
                        'type' => 'text',
                        'class' => 'community_data',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'label' => $this->l('夜市'),
                        'maxlength' => '20',
                    ],

//                    'id_disgust_facility' => [
//                        'type' => 'checkbox',
//                        'label' => $this->l('嫌惡設施(複選)'),
//                        'name' => 'id_disgust_facility',
//                        'in_table' => true,
//                        'multiple' => true,
//                        'values' => $disgust_facility,
//                        'form_col' => 12,
//                        'label_col' => 3,
//                        'col' => 9,
//                    ],
                ],
            ],

            'img' => [
//                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('[網站]列表縮圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 800,
                            'min_width' => 600,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],

                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
                        'p' => $this->l('圖片建議大小 800*600px 或寬度最少超過600px'),
                    ],

                    'web_carousel' => [
                        'name' => 'web_carousel',
                        'label_class' => 'text-left',
                        'label' => $this->l('[網站]輪播圖片'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 20,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 800,
                            'min_width' => 600,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],

                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 900*506'),
                        'p' => $this->l('圖片建議大小 800*600px 或寬度最少超過600px'),
                    ],

//                    'app_img' => [
//                        'name' => 'app_img',
//                        'label_class' => 'text-left',
//                        'label' => $this->l('[APP]列表縮圖'),
//                        'type' => 'file',
//                        'language' => 'zh-TW',
//
//                        'file' => [
//                            'icon' => false,
//                            'auto_upload' => true,
//                            'max' => 1,
//                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 428,
//                            'max_height' => 285,
//                            'resize' => true,
//                            'resizePreference' => 'height',
//                        ],
//                        'multiple' => true,
//                        'col' => 12,
//                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
//                    ],
//
//                    'app_carousel' => [
//                        'name' => 'app_carousel',
//                        'label_class' => 'text-left',
//                        'label' => $this->l('[APP]輪播圖片'),
//                        'type' => 'file',
//                        'language' => 'zh-TW',
//                        'file' => [
//                            'icon' => false,
//                            'auto_upload' => true,
//                            'max' => 20,
//                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 428,
//                            'max_height' => 285,
//                            'resize' => true,
//                            'resizePreference' => 'width',
//                        ],
//                        'multiple' => true,
//                        'col' => 12,
//                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
//                    ],

                    'building_power' => [
                        'name' => 'building_power',
                        'label_class' => 'text-left',
                        'label' => $this->l('建築權狀'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 20,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 428,
//                            'max_height' => 285,
//                            'resize' => true,
//                            'resizePreference' => 'width',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('原圖上傳'),
                    ],

                    'land_power' => [
                        'name' => 'land_power',
                        'label_class' => 'text-left',
                        'label' => $this->l('土地權狀'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 20,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 428,
//                            'max_height' => 285,
//                            'resize' => true,
//                            'resizePreference' => 'width',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('原圖上傳'),
                    ],
                ],
            ],
        ];

        $sql = 'SELECT *,d_c_c.`title` as d_c_c_title,d_c.`title` as title,d_c.`id_device_category_class` as i_d_c_c FROM device_category as d_c
                LEFT JOIN `device_category_class` as d_c_c ON d_c.`id_device_category_class` = d_c_c.`id_device_category_class`
                ORDER BY d_c.`position` ASC';
        $device_category_arr = Db::rowSQL($sql);
        foreach($device_category_arr as $value){
            $device_category[] = [
                'id' => 'device_category_'.$value['id_device_category'],
                'value' => $value['id_device_category'],
                'class' =>"device_category",
                'label' => $this->l($value['d_c_c_title'].'-'.$value['title']),
                'class_id' => $value['i_d_c_c']
            ];
        }

        $sql = "SELECT * FROM device_category_group ORDER BY position ASC";
        $device_category_group_arr = Db::rowSQL($sql);
        foreach($device_category_group_arr as $ke =>$val){
            $temp = array();
            foreach($device_category_arr as $value) {
                if(strstr($value["id_device_category_group"],$val["id_device_category_group"])){
                    $temp[] = [
                        'id' => 'device_category_'.$value['id_device_category'],
                        'value' => $value['id_device_category'],
                        'class' =>"device_category",
                        'label' => $this->l($value['d_c_c_title'].'-'.$value['title']),
                        'class_id' => $value['i_d_c_c']
                    ];
                }
            }
            $this->fields['form']['device']['input']["id_device_category_group_{$val['id_device_category_group']}"]=[
                'type' => 'checkbox',
                'label' => $this->l($val['title']),
                'name' => 'id_device_category',
                'class' => 'device_category_'.$val["id_device_category_group"],
                'in_table' => true,
                'multiple' => true,
                'values' => $temp,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
            ];
        }
        $this->fields['form']['device']['input']['power_electric'] =[
            'type' => 'number',
            'form_col'  => 12,
            'label_col' => 3,
            'col'       => 2,
            'label' => $this->l('動力電'),
            'suffix' => $this->l('匹馬力'),
            'name' => 'power_electric',
        ];



        //tpl 1 是指 數量,品牌,擺放,狀態
        //2 是 數量,椅,品牌,擺放,狀態
        //3 是 數量,類型,品牌,擺放,狀態
        //4 是 數量,類型(select的),品牌,擺放,狀態
        //5 是 數量,擺放,狀態
        //6 數量,狀態
        //7 數量,類型(select的),擺放,狀態
        //8 數量,類型(select的),狀態
        //這邊規則tpl是代表我這邊所使用的模板方式
//        $this->tpl_data = [
//            'id_home_appliances' => [
//                'name' => 'id_home_appliances',
//                'type' => 'hidden',
//                'label_col' => 3,
//                'index' => true,
//                'required' => true,
//            ],
//
//            'id_rent_house' => [
//                'name' => 'id_rent_house',
//                'type' => 'hidden',
//                'label_col' => 3,
//                'link' =>true,
//            ],
//
//            'single_bed_bottom'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'單人床底',
//                'name'=>'single_bed_bottom',
//            ],
//
//            'single_bed_mattress'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'單人床墊',
//                'name'=>'single_bed_mattress',
//            ],
//
//            'single_bed_box'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'單人床頭箱',
//                'name'=>'single_bed_box',
//            ],
//
//            'single_bed_board'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'單人床頭板',
//                'name'=>'single_bed_board',
//            ],
//
//            'double_bed_bottom'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'雙人床底',
//                'name'=>'double_bed_bottom',
//            ],
//
//            'double_bed_mattress'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'雙人床墊',
//                'name'=>'double_bed_mattress',
//            ],
//
//            'double_bed_box'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'雙人床頭箱',
//                'name'=>'double_bed_box',
//            ],
//
//            'double_bed_board'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'雙人床頭板',
//                'name'=>'double_bed_board',
//            ],
//
//            'side_cabinet'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'側櫃',
//                'name'=>'side_cabinet',
//            ],
//
//            'dresser'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'斗櫃',
//                'name'=>'dresser',
//            ],
//
//            'dressing_table'=>[
//                'tpl'=>2,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'梳妝台',
//                'name'=>'dressing_table',
//            ],
//
//            'wardrobe'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'衣櫃',
//                'name'=>'wardrobe',
//            ],
//
//            'desk'=>[
//                'tpl'=>2,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'書桌',
//                'name'=>'desk',
//            ],
//
//            'dining_table'=>[
//                'tpl'=>2,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'餐桌',
//                'name'=>'dining_table',
//            ],
//            'sofa'=>[
//                'tpl'=>4,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇沙發材質',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'布',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'皮',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'2',
//                        'text'=>'實木',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'3',
//                        'text'=>'藤',
//                        'id'=>'',
//                    ]
//                ],
//                'select'=>[
//                    'val'=>',0,1,2,3',
//                    'text'=>'選擇沙發材質,布,皮,實木,藤'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'沙發',
//                'name'=>'sofa',
//            ],
//
//            'pillow'=>[
//                'form_col' => 6,
//                'label_col' => 6,
//                'col' => 6,
//                'type'=>'number',
//                'label'=>'抱枕',
//                'name'=>'pillow',
//            ],
//
//            'cushion'=>[
//                'form_col' => 3,
//                'label_col' => 3,
//                'col' => 9,
//                'type'=>'number',
//                'label'=>'靠墊',
//                'name'=>'cushion',
//            ],
//
//            'footstool'=>[
//                'form_col' => 3,
//                'label_col' => 3,
//                'col' => 9,
//                'type'=>'number',
//                'label'=>'腳凳',
//                'name'=>'footstool',
//            ],
//
//            'tea_table'=>[
//                'tpl'=>3,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'茶几',
//                'name'=>'tea_table',
//            ],
//
//            'tv_cabinet'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'TV櫃',
//                'name'=>'tv_cabinet',
//            ],
//
//            'cupboard'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'櫥櫃',
//                'name'=>'cupboard',
//            ],
//
//            'shoebox'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'鞋櫃',
//                'name'=>'shoebox',
//            ],
//
//            'curtain'=>[
//                'tpl'=>8,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇窗簾材質',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'布',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'紗',
//                        'id'=>'',
//                    ],
//                ],
//                'select'=>[
//                    'val'=>',0,1',
//                    'text'=>'選擇窗簾材質,布,紗'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'窗簾',
//                'name'=>'curtain',
//            ],
//
//            'shower_curtain'=>[
//                'tpl'=>6,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'浴簾',
//                'name'=>'shower_curtain',
//            ],
//
//            'door_curtain'=>[
//                'tpl'=>6,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'門簾',
//                'name'=>'door_curtain',
//            ],
//
//            'carpet'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'地毯',
//                'name'=>'carpet',
//            ],
//
//            'doily_cloth'=>[
//                'tpl'=>6,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'桌巾布',
//                'name'=>'doily_cloth',
//            ],
//
//            'lcd'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'液晶電視',
//                'name'=>'lcd',
//            ],
//
//            'flat_screen_tv'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'平面電視',
//                'name'=>'flat_screen_tv',
//            ],
//
//            'window_type'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'窗型冷氣',
//                'name'=>'window_type',
//            ],
//
//            'separate_type'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'分離式冷氣',
//                'name'=>'separate_type',
//            ],
//
//            'refrigerator'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'冰箱',
//                'name'=>'refrigerator',
//            ],
//
//            'washer'=>[
//                'tpl'=>4,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇洗衣機模式',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'洗',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'洗烘',
//                        'id'=>'',
//                    ]
//                ],
//                'select'=>[
//                    'val'=>',0,1',
//                    'text'=>'選擇洗衣機模式,洗,洗烘'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'洗衣機',
//                'name'=>'washer',
//            ],
//
//            'dryer'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'烘衣機',
//                'name'=>'dryer',
//            ],
//
//            'geyser'=>[
//                'tpl'=>4,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇熱水器運作方式',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'瓦斯',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'電熱',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'2',
//                        'text'=>'太陽能',
//                        'id'=>'',
//                    ]
//                ],
//                'select'=>[
//                    'val'=>',0,1,2',
//                    'text'=>'選擇熱水器運作方式,瓦斯,電熱,太陽能'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'熱水器',
//                'name'=>'geyser',
//            ],
//
//            'induction_cooker'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'電磁爐',
//                'name'=>'induction_cooker',
//            ],
//
//            'gas_cooktop'=>[
//                'tpl'=>4,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇瓦斯爐運作方式',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'天然氣',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'桶裝',
//                        'id'=>'',
//                    ],
//                ],
//                'select'=>[
//                    'val'=>',0,1',
//                    'text'=>'選擇瓦斯爐運作方式,天然氣,桶裝'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'瓦斯爐',
//                'name'=>'gas_cooktop',
//            ],
//
//            'microwave_oven'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'微波爐',
//                'name'=>'microwave_oven',
//            ],
//
//            'oven'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'烤箱',
//                'name'=>'oven',
//            ],
//
//            'microwave_oven_stove'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'微波烤箱爐',
//                'name'=>'microwave_oven_stove',
//            ],
//
//            'range_hood'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'油煙機',
//                'name'=>'range_hood',
//            ],
//
//            'dish_dryer'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'烘碗機',
//                'name'=>'dish_dryer',
//            ],
//
//            'dishwasher'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'洗碗機',
//                'name'=>'dishwasher',
//            ],
//
//            'fluid_table'=>[
//                'tpl'=>6,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'流理臺',
//                'name'=>'fluid_table',
//            ],
//
//            'kitchen_cupboard'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'廚房櫥櫃',
//                'name'=>'kitchen_cupboard',
//            ],
//
//            'lamp_stand'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'燈座',
//                'name'=>'lamp_stand',
//            ],
//
//            'bulb'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'燈泡',
//                'name'=>'bulb',
//            ],
//
//            'cabinet_light'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'櫃燈',
//                'name'=>'cabinet_light',
//            ],
//
//            'standing_lamp'=>[
//                'tpl'=>5,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'立燈',
//                'name'=>'standing_lamp',
//            ],
//
//            'clotheshorse'=>[
//                'tpl'=>7,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇曬衣架運作方式',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'電動',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'手動',
//                        'id'=>'',
//                    ],
//                ],
//                'select'=>[
//                    'val'=>',0,1',
//                    'text'=>'選擇曬衣架運作方式,電動,手動'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'曬衣架',
//                'name'=>'clotheshorse',
//            ],
//
//            'commode'=>[
//                'tpl'=>4,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇馬桶類型',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'免治',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'超級',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'2',
//                        'text'=>'一般',
//                        'id'=>'',
//                    ],
//                ],
//                'select'=>[
//                    'val'=>',0,1,2',
//                    'text'=>'選擇馬桶類型,免治,超級,一般'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'馬桶',
//                'name'=>'commode',
//            ],
//
//            'bathtub'=>[
//                'tpl'=>4,
//                'values'=>[
//                    [
//                        'val'=>'',
//                        'text'=>'選擇浴缸類型',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'0',
//                        'text'=>'按摩',
//                        'id'=>'',
//                    ],
//                    [
//                        'val'=>'1',
//                        'text'=>'一般',
//                        'id'=>'',
//                    ],
//                ],
//                'select'=>[
//                    'val'=>',0,1',
//                    'text'=>'選擇浴缸類型,按摩,一般'
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'浴缸',
//                'name'=>'bathtub',
//            ],
//
//            'oven_room'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'烤箱房',
//                'name'=>'oven_room',
//            ],
//
//            'steam_room'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'蒸氣室',
//                'name'=>'steam_room',
//            ],
//
//            'dehumidifier'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'除濕機',
//                'name'=>'dehumidifier',
//            ],
//
//            'electric_fan'=>[
//                'tpl'=>1,
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//                'label'=>'電風扇',
//                'name'=>'electric_fan',
//            ],
//
//            'crane'=>[
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 3,
//                'type' =>'number',
//                'label'=>'天車',
//                'name'=>'crane',
//            ],
//
//            'power_supply' =>[
//                'type' => 'checkbox',
//                'label' => '動力電',
//                'name' => 'power_supply',
//                'values' => [
//                    [
//                        'val'=>'0',
//                        'label'=>'沒有',
//                    ],
//                    [
//                        'val'=>'1',
//                        'label'=>'有',
//                    ]
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//            ],
//
//            'air_conditioning' =>[
//                'type' => 'checkbox',
//                'label' => '空調設備',
//                'name' => 'air_conditioning',
//                'values' => [
//                    [
//                        'val'=>'0',
//                        'label'=>'沒有',
//                    ],
//                    [
//                        'val'=>'1',
//                        'label'=>'有',
//                    ]
//                ],
//                'form_col' => 12,
//                'label_col' => 3,
//                'col' => 9,
//            ],
//
//            'other_bathroom_equipment' => [
//                'name' => 'other_bathroom_equipment',
//                'type' => 'textarea',
//                'form_col' => 12,
//                'label_col'=> 3,
//                'col' => 9,
//                'label' =>'其他衛浴設備',
//                'rows' =>'10',
//                'cols'=>'5',
//            ],
//
//            'other_furniture' => [
//                'name' => 'other_furniture',
//                'type' => 'textarea',
//                'form_col' => 12,
//                'label_col'=> 3,
//                'col' => 9,
//                'label' =>'其他家具',
//                'rows' =>'10',
//                'cols'=>'5',
//            ],
//
//            'other_home_appliance' => [
//                'name' => 'other_home_appliance',
//                'type' => 'textarea',
//                'form_col' => 12,
//                'label_col'=> 3,
//                'col' => 9,
//                'label' =>'其他家電',
//                'rows' =>'10',
//                'cols'=>'5',
//            ],
//
//            'free_article' => [
//                'name' => 'free_article',
//                'type' => 'textarea',
//                'form_col' => 12,
//                'label_col'=> 3,
//                'col' => 9,
//                'label' =>'無償使用',
//                'rows' =>'10',
//                'cols'=>'5',
//            ],
//
//        ];//由於我這塊有各種不同的格是因此要這樣寫

//        print_r($_SESSION);

        $this->park_space_arr = [
            "cars_park_position","cars_type","cars_serial","cars_rent","cars_rent_type","cars_remarks",
            "motorcycle_park_position","motorcycle_type","motorcycle_serial","motorcycle_rent","motorcycle_rent_type","motorcycle_remarks",
        ];
        parent::__construct();
    }

//    public function processDel(){
//        $this->_errors[] = $this->l('無法刪除此資料');
//    }

    public function initProcess()//處理上下架問題
    {
        //rent_house_code 內部自編ID 之後要寫

        $active = Tools::getValue('active');
        $date = date("Y-m-d");
        if($active=='1'){
            $_POST['on_date'] = $date;
        }else if($active=='0'){
            $_POST['off_date'] = $date;
        }
        $id_rent_house = Tools::getValue('id_rent_house');
        $complete_address = Tools::getValue('complete_address');
        $part_address = Tools::getValue('part_address');
        $id_county = Tools::getValue('id_county');
        $id_city = Tools::getValue('id_city');
        $village = Tools::getValue('village');
        $neighbor = Tools::getValue('neighbor');
        $road = Tools::getValue('road');
        $segment = Tools::getValue('segment');
        $lane = Tools::getValue('lane');
        $alley = Tools::getValue('alley');
        $no = Tools::getValue('no');
        $no_her = Tools::getValue('no_her');
        $floor = Tools::getValue('floor');
        $floor_her = Tools::getValue('floor_her');
        $address_room = Tools::getValue('address_room');
        $rent_house_community_code =Tools::getValue('rent_house_community_code');
        $submitAddrent_house = Tools::getValue('submitAddrent_house');//新增才有差
        $submitAddrent_houseAndStay = Tools::getValue('submitAddrent_houseAndStay');//新增才有差
        $submitEditrent_house = Tools::getValue('submitEditrent_house');//修改才有差
        $submitEditrent_houseAndStay = Tools::getValue('submitEditrent_houseAndStay');//修改才有差



        if(!empty(Tools::getValue('submitAddrent_house')) || !empty(Tools::getValue('submitAddrent_houseAndStay'))){
            $_POST["id_admin"] = $_SESSION["id_admin"];
            $_POST["create_name"] = $_SESSION["name"];
        }

        if(!empty($submitEditrent_house) || !empty($submitAddrent_house) ||  !empty($submitEditrent_houseAndStay) || !empty($submitAddrent_houseAndStay)){//說白了送出才會是"" 因為disabled的關係
            $_POST["update_id_admin"] = $_SESSION["id_admin"];
            $_POST["update_name"] = $_SESSION["name"];

            $sql = "INSERT rent_house_log(id_rent_house,id_admin,first_name)
            VALUES (".GetSQL($id_rent_house,"int").",".GetSQL($_SESSION["id_admin"],"int").",".GetSQL($_SESSION["first_name"],"text").")";
            Db::rowSQL($sql);//記錄誰哪時修改

            if(!empty($rent_house_community_code)){//如果有點選擇社區相關資料則這邊會進行更動
                $community_update = "";//update data
                $rent_house_community_arr = ['community','all_num','id_exterior_wall','household_num','storefront_num','property_management','community_phone','community_fax','builders','redraw_area',
                    'clean_time','time_s','time_e','id_public_utilities','e_school','j_school','park','market','night_market','supermarket','shopping_center','hospital','bus','bus_station','passenger_transport','passenger_transport_station',
                    'train','mrt','mrt_station','high_speed_rail','interchange'];//所需要取出並修改的
                foreach($rent_house_community_arr as $key => $value){
                    $data = "";//每次都先漂白
                    $type = "";//進去洗的類別
                    $data = Tools::getValue($value);//資料~
                    $temlate = "";
                    if(is_array($data)){//確認是否是arr是則進行處理 不是則不處理arr流程
                        $type= 'arr';
                        foreach($data as $k =>$v){
                            if(empty($temlate)){
                                $temlate .= '"'.GetSQL($v,"int").'"';
                            }else{
                                $temlate .= ',"'.GetSQL($v,"int").'"';
                            }
                        }
                        $temlate = "[{$temlate}]";
                    }else{
                        $temlate = $data;
                    }
                    if($type == "arr"){
                        $community_update .= "`{$value}`='".$temlate."',";
                    }else if(is_int($data)){
                        $community_update .= '`'.$value.'`='.GetSQL($data,"int").",";
                    }else{
                        $community_update .= '`'.$value.'`='.GetSQL($data,"text").",";
                    }
                }
                $community_update = substr($community_update,0,-1);
                $community_update = "UPDATE `rent_house_community` SET {$community_update} WHERE rent_house_community_code=".GetSQL($rent_house_community_code,"text");
                Db::rowSQL($community_update);
            }


            //部分地址跟完整地址共用一部分
            $complete_address_arr = array(
                "county_name"=>"",
                "city_name"=>"",
                "village"=>"",
                "neighbor"=>"鄰",
                "road"=>"",
                "segment"=>"段",
                "lane"=>"巷",
                "alley"=>"弄",
                "no"=>"號",
                "no_her"=>"之",
                "floor"=>"樓",
                "floor_her"=>"之",
                "address_room"=>"室");//由於之是放在後面所以會上一個if處理否則一律後面

            $complete_address = '';//重置 雖然不用重置也沒差就是
            $part_address = '';//重置

            $sql = "SELECT * FROM `county` WHERE `id_county` =".GetSQL($id_county,"int");
            $row = Db::rowSQL($sql,true); //主要要取出county_name
            $county_name = $row["county_name"];//縣市名稱
            $sql = "SELECT * FROM `city` WHERE id_city=".GetSQL($id_city,"int");
            $row_city = Db::rowSQL($sql,true); //主要要取出city_name
            $city_name = $row_city["city_name"];//市鄉政
            foreach($complete_address_arr as $k => $i){
                if(!empty($$k)){
                    if($i=="之"){
                        $complete_address .= $i.$$k;
                    }else{
                        $complete_address .= $$k.$i;
                    }
                    if($k=="county_name" || $k=="city_name" || $k=="village" || $k=="neighbor" ||
                        $k=="road" || $k=="segment" || $k=="lane"){ //部分地址會到巷
                        $part_address .= $$k.$i;
                    }
                }
            }
            $_POST['complete_address'] = $complete_address;
            $_POST['part_address'] = $part_address;
            $_POST['update_date'] =  date("Y-m-d H:i:s");
            ///這邊做部分地址的經緯度判斷
            $google_map = "";
            if(!empty($id_rent_house)){//不存在 大概就是新增階段
                $sql = "SELECT part_address,longitude,latitude FROM rent_house WHERE id_rent_house=".GetSQL($id_rent_house,"int");
                $google_map = Db::rowSQL($sql,true); //主要要取出
            }
            if(($part_address != Tools::getValue('complete_address')) || empty($google_map["longitude"]) || empty($google_map["latitude"])){
                //原本的部分地址與現在的不符或是經緯度不存在

                $google_api_location_json = Google_api::get_lat_and_long($part_address);//取得一大包data
                $api_arr = Google_api::get_data_lat_and_long($google_api_location_json);
                //下面由於這邊只有一個所以用0呼叫
                $_POST["longitude"] = $api_arr["0"]["lng"];
                $_POST["latitude"] = $api_arr["0"]["lat"];
            }
        }


//        $sql = "SELECT id_rent_house,rent_house_code FROM rent_house WHERE id_rent_house=".Tools::getValue("id_rent_house");
//        $row_r_h = Db::rowSQL($sql,true); //取出作版並且為後續處理
//
//        $sql = "SELECT * FROM home_appliances WHERE id_rent_house=".GetSQL($row_r_h["id_rent_house"],"int");
//
//        $row = Db::rowSQL($sql,true); //取出作版並且為後續處理
//
//        if(isset($row)){//存在做這道工序
//            foreach($row as $key => $value){
//                if(strstr($value,'[') && strstr($value,']') && strstr($value,'"')){//找到標的物將它變成陣列形式
//                    $row[$key] = Db::antonym_array($row[$key],true);
//                    $row[$key] = explode(",",$row[$key]);
//                }
//            }
//        }

//print_r($row);
        $this->fields['form']['home_appliances'] = [
//            'tab' => 'home_appliances',
            'legend' => [
                'title' => $this->l('家具/家電/設備'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],
            'input'=>[
                'id_rent_house' => [
                    'name' => 'id_rent_house',
                    'type' => 'hidden',
                    'label_col' => 3,
                    'index' => true,
                    'required' => true,
                ],

                'dressing_table_total' => [
                    'name' => 'dressing_table_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l('傢俱類:'),
                    'prefix' => $this->l('梳妝台'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'wardrobe_total' => [
                    'name' => 'wardrobe_total',
                    'type' => 'number',
                    'prefix' => $this->l('衣櫃'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'cupboard_total' => [
                    'name' => 'cupboard_total',
                    'type' => 'number',
                    'form_col'  => 3,
                    'col'       => 12,
                    'prefix' => $this->l('櫥櫃'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'japanese_table_total' => [
                    'name' => 'japanese_table_total',
                    'type' => 'number',
                    'prefix' => $this->l('和式桌'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'tv_cabinet_total' => [
                    'name' => 'tv_cabinet_total',
                    'type' => 'number',
                    'form_col'  => 3,
                    'col'       => 12,
                    'prefix' => $this->l('tv櫃'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'bed_side_cabinet_total' => [
                    'name' => 'bed_side_cabinet_total',
                    'type' => 'number',
                    'prefix' => $this->l('床側櫃'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'fluid_table_total' => [
                    'name' => 'fluid_table_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l(' '),
                    'prefix' => $this->l('流理臺'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'range_hood_total' => [
                    'name' => 'range_hood_total',
                    'type' => 'number',
                    'prefix' => $this->l('油煙機'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'kitchen_cupboard_total' => [
                    'name' => 'kitchen_cupboard_total',
                    'type' => 'number',
                    'form_col'  => 3,
                    'col'       => 12,
                    'prefix' => $this->l('廚房櫥櫃'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'induction_cooker_total' => [
                    'name' => 'induction_cooker_total',
                    'type' => 'number',
                    'prefix' => $this->l('電磁爐'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'gas_cooktop_total' => [
                    'name' => 'gas_cooktop_total',
                    'type' => 'number',
                    'form_col'  => 3,
                    'col'       => 12,
                    'prefix' => $this->l('瓦斯爐'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'curtain_num_total' => [
                    'name' => 'curtain_num_total',
                    'type' => 'number',
                    'prefix' => $this->l('窗簾(幅)'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'single_bed_set_total' => [
                    'name' => 'single_bed_set_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l('床組'),
                    'maxlength' => '20',
                    'prefix' => $this->l('單人'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'double_bed_set_total' => [
                    'name' => 'double_bed_set_total',
                    'type' => 'number',
                    'prefix' => $this->l('雙人'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'desk_total' => [
                    'name' => 'desk_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 3,
                    'col'       => 6,
                    'label' => $this->l('書桌/椅'),
                    'maxlength' => '20',
                    'prefix' => $this->l('書桌'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'desk_chair_total' => [
                    'name' => 'desk_chair_total',
                    'type' => 'number',
                    'prefix' => $this->l('椅'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'sofa_1_total' => [
                    'name' => 'sofa_1_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l('沙發'),
                    'prefix' => $this->l('1人座'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'sofa_2_total' => [
                    'name' => 'sofa_2_total',
                    'type' => 'number',
                    'prefix' => $this->l('2人座'),
                    'is_suffix' => true,
                ],

                'sofa_3_total' => [
                    'name' => 'sofa_3_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'prefix' => $this->l('3人座'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'tea_table_total' => [
                    'name' => 'tea_table_total',
                    'type' => 'number',
                    'prefix' => $this->l('茶几'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'dining_table_total' => [
                    'name' => 'dining_table_total',
                    'type' => 'number',
                    'form_col'  => 12,
                    'label_col' => 3,
                    'col'       => 3,
                    'label' => $this->l('餐桌/椅'),
                    'maxlength' => '20',
                    'prefix' => $this->l('餐桌'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'dining_table_chair_total' => [
                    'name' => 'dining_table_chair_total',
                    'type' => 'number',
                    'prefix' => $this->l('椅'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'furniture_supplement' => [
                    'name' => 'furniture_supplement',
                    'type' => 'text',
                    'form_col'  => 12,
                    'label_col' => 3,
                    'col'       => 9,
                    'label' => $this->l('傢俱補充'),
                    'maxlength' => '64',
                    'col_class'     =>'no_padding_right',
                ],


                'refrigerator_total' => [
                    'name' => 'refrigerator_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l('家電類:'),
                    'prefix' => $this->l('冰箱'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'microwave_oven_total' => [
                    'name' => 'microwave_oven_total',
                    'type' => 'number',
                    'prefix' => $this->l('微波爐'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'oven_total' => [
                    'name' => 'oven_total',
                    'type' => 'number',
                    'form_col'  => 3,
                    'col'       => 12,
                    'prefix' => $this->l('烤箱'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'microwave_oven_stove_total' => [
                    'name' => 'microwave_oven_stove_total',
                    'type' => 'number',
                    'prefix' => $this->l('兩用爐'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'electric_fan_total' => [
                    'name' => 'electric_fan_total',
                    'type' => 'number',
                    'form_col'  => 3,
                    'col'       => 12,
                    'prefix' => $this->l('電風扇'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'dehumidifier_total' => [
                    'name' => 'dehumidifier_total',
                    'type' => 'number',
                    'prefix' => $this->l('除濕機'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'crane_total' => [
                    'name' => 'crane_total',
                    'type' => 'number',
                    'form_col'  => 12,
                    'label_col' => 3,
                    'col'       => 2,
                    'label' => $this->l(' '),
                    'prefix' => $this->l('天車'),
                    'col_class'     =>'no_padding_right',
                ],




                'lcd_total' => [
                    'name' => 'lcd_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l('電視'),
                    'prefix' => $this->l('液晶'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'flat_screen_tv_total' => [
                    'name' => 'flat_screen_tv_total',
                    'type' => 'number',
                    'prefix' => $this->l('平面'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'window_type_total' => [
                    'name' => 'window_type_total',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 3,
                    'col'       => 6,
                    'label' => $this->l('冷氣'),
                    'prefix' => $this->l('窗型'),
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],

                'separate_type_total' => [
                    'name' => 'separate_type_total',
                    'type' => 'number',
                    'prefix' => $this->l('分離式'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'is_gas' => [
                    'name' => 'is_gas',
                    'type' => 'checkbox',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l('瓦斯'),
                    'values' => [
                        [
                            'id' => 'is_gas_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],


                'is_gas_type' => [
                    'name'      => 'is_gas_type',
                    'type'      => 'select',
                    'prefix' => $this->l('類型'),
                    'is_suffix' => true,
                    'options'   => [
                        'default'     => [
                            'text' => '選擇瓦斯類型',
                            'val'  => '',
                        ],
                        //自訂值
                        'val'         => [
                            [
                                'text' => '天然氣',
                                'val'  => '0',
                            ],
                            [
                                'text' => '桶裝',
                                'val'  => '1',
                            ],
                        ],
                    ],
                ],

                'is_geyser' => [
                    'name' => 'is_geyser',
                    'type' => 'checkbox',
                    'form_col'  => 6,
                    'label_col' => 3,
                    'col'       => 6,
                    'label' => $this->l('熱水器'),
                    'values' => [
                        [
                            'id' => 'is_geyser_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],


                'is_geyser_type' => [
                    'name'      => 'is_geyser_type',
                    'type'      => 'select',
                    'prefix' => $this->l('類型'),
                    'is_suffix' => true,
                    'options'   => [
                        'default'     => [
                            'text' => '選擇熱水器類型',
                            'val'  => '',
                        ],
                        //自訂值
                        'val'         => [
                            [
                                'text' => '瓦斯',
                                'val'  => '0',
                            ],
                            [
                                'text' => '電熱',
                                'val'  => '1',
                            ],
                            [
                                'text' => '太陽能',
                                'val'  => '2',
                            ],
                        ],
                    ],
                ],

                'is_private_washer' => [
                    'name' => 'is_private_washer',
                    'type' => 'checkbox',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'label' => $this->l('洗衣機'),
                    'prefix' => $this->l('私人'),
                    'values' => [
                        [
                            'id' => 'is_private_washer_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                    'is_prefix' => true,
                    'col_class'     =>'no_padding_right',
                ],


                'private_washer_type' => [
                    'name'      => 'private_washer_type',
                    'type'      => 'select',
                    'prefix' => $this->l('類型'),
                    'is_suffix' => true,
                    'options'   => [
                        'default'     => [
                            'text' => '選擇私人洗衣機類型',
                            'val'  => '',
                        ],
                        //自訂值
                        'val'         => [
                            [
                                'text' => '洗',
                                'val'  => '0',
                            ],
                            [
                                'text' => '洗烘',
                                'val'  => '1',
                            ],
                        ],
                    ],
                ],


                'public_washer' => [
                    'name' => 'public_washer',
                    'type' => 'number',
                    'form_col'  => 6,
                    'label_col' => 6,
                    'col'       => 6,
                    'prefix' => $this->l('共用'),
                    'is_prefix' => true,
                    'no_label' => true,
                    'col_class'     =>'no_padding',
                ],

                'coin_washer' => [
                    'name' => 'coin_washer',
                    'type' => 'number',
                    'prefix' => $this->l('投幣'),
                    'is_suffix' => true,
                    'col_class'     =>'no_padding',
                ],

                'is_power_supply' => [
                    'name' => 'is_power_supply',
                    'type' => 'checkbox',
                    'label_col' => 3,
                    'col' => 6,
                    'label' => $this->l('動力電'),
                    'values' => [
                        [
                            'id' => 'is_power_supply_l',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                    'is_prefix' => true,
                ],

                'is_power_supply_remark' => [
                    'name' => 'is_power_supply_remark',
                    'type' => 'text',
                    'prefix' => $this->l('動力電備註'),
                    'is_suffix' => true,
                    'maxlength'=>'64',
                ],

                'is_air_conditioning' => [
                    'name' => 'is_air_conditioning',
                    'type' => 'checkbox',
                    'label_col' => 3,
                    'col' => 6,
                    'label' => $this->l('空調'),
                    'values' => [
                        [
                            'id' => 'is_air_conditioning_l',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                    'is_prefix' => true,
                ],

                'is_air_conditioning_remark' => [
                    'name' => 'is_air_conditioning_remark',
                    'type' => 'text',
                    'prefix' => $this->l('空調備註'),
                    'is_suffix' => true,
                    'maxlength'=>'64',
                ],

                'home_appliance_supplement' => [
                    'name' => 'home_appliance_supplement',
                    'type' => 'text',
                    'form_col'  => 12,
                    'label_col' => 3,
                    'col'       => 9,
                    'label' => $this->l('家電補充'),
                    'maxlength' => '64',
                    'col_class'     =>'no_padding_right',
                ],
            ],

            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],
            'reset' => [
                'title' => $this->l('復原'),
            ],
        ];

//        $this->context->smarty->assign([
//            'main_index' =>$this->fields['index'],
//            'tpl_data' => $this->tpl_data,
//            'row'=> $row,
//        ]);

        if(!empty(Tools::getValue("id_rent_house"))){//處理汽機車的
            $park_space = [];//丟進去的
            $id_rent_house = Tools::getValue("id_rent_house");//取得data
//            取得汽車的
            $sql = "SELECT cars_park_position,cars_type,cars_serial,cars_rent,cars_rent_type,cars_remarks FROM rent_house WHERE id_rent_house=".Tools::getValue("id_rent_house");
            $cars_data = Db::rowSQL($sql,true);
            if(isset($cars_data)){//存在做這道工序
                foreach($cars_data as $key => $value){
                    if(strstr($value,'[') && strstr($value,']') && strstr($value,'"')){//找到標的物將它變成陣列形式
                        $cars_data[$key] = Db::antonym_array($cars_data[$key],true);
                        $cars_data[$key] = explode(",",$cars_data[$key]);
                    }
                }
            }

//            取得機車的
            $sql = "SELECT motorcycle_park_position,motorcycle_type,motorcycle_serial,motorcycle_rent,motorcycle_rent_type,motorcycle_remarks FROM rent_house WHERE id_rent_house=".Tools::getValue("id_rent_house");
            $motorcycle_data = Db::rowSQL($sql,true);
            if(isset($cars_data)){//存在做這道工序
                foreach($motorcycle_data as $key => $value){
                    if(strstr($value,'[') && strstr($value,']') && strstr($value,'"')){//找到標的物將它變成陣列形式
                        $motorcycle_data[$key] = Db::antonym_array($motorcycle_data[$key],true);
                        $motorcycle_data[$key] = explode(",",$motorcycle_data[$key]);
                    }
                }
            }

            if(!empty($cars_data)){
                $park_space["cars"] = $cars_data;
            }
            if(!empty($motorcycle_data)){
                $park_space["motorcycle"] = $motorcycle_data;
            }

            $this->context->smarty->assign([
                'park_space' =>$park_space,
            ]);
        }


        //要用的資料
        $this->context->smarty->assign([
            'database_county' =>County::getContext()->database_county(),
            'database_city' =>County::getContext()->database_city(),
            'database_source'=>Member_landlord::get_source(),
//            'database_rent_mange_program'=>Member_landlord::get_rent_mange_program(),
//            'database_intermediary_program'=>Member_landlord::get_intermediary_program(),
            'database_other_conditions' =>Member_landlord::get_other_conditions(),
//            'database_rent_union_type' =>Member_landlord::get_rent_union_type(),
//            'database_pay_type' =>Member_landlord::get_pay_type(),
            'database_data' =>Member_landlord::get_data(Tools::getValue("id_rent_house")),
        ]);

        parent::initProcess();
    }

    public function processAdd(){

        $this->validateRules();
        if($this->max > 0){
            if (($this->num >= $this->max)
                && ($this->max !== null)) {
                $this->_errors[] = sprintf($this->l('最多只能新增 %d 筆資料!'), $this->max);
            }
        }
        if (count($this->_errors))
            return;

        $this->setFormTable();
        $num = $this->FormTable->insertDB();
        //這個是房東
        Member_landlord::Admin_multiple_table($num,$_POST);
        //取自前台系統處理因為這塊問題比較多


//        //這個要塞在這一邊在確認並且新增之後能夠即時判斷並且加入data
//        $sql = "SELECT max(id_rent_house) as add_id FROM rent_house";
//        $row_max_add = Db::rowSQL($sql,true);
//
//        //社區方面
//        $community_insert_key = "";
//        $community_insert_val = "";
//        $community_temlate = "";
//        foreach($this->rent_house_community_arr as $key => $value){
//            $data = "";//每次都先漂白
//            $type = "";//進去洗的類別
//            $data = Tools::getValue($value);//資料~
//            $temlate = "";
//            if(is_array($data)){//確認是否是arr是則進行處理 不是則不處理arr流程
//                $type= 'arr';
//                foreach($data as $k =>$v){
//                    if(empty($temlate)){
//                        $temlate .= '"'.GetSQL($v,"int").'"';
//                    }else{
//                        $temlate .= ',"'.GetSQL($v,"int").'"';
//                    }
//                }
//                $temlate = "[{$temlate}]";
//            }else{
//                $temlate = $data;
//            }
//            $community_insert_key .='`'.$value.'`,';//key的部分
//            if($value=="id_rent_house_community"){//value部分
//                $community_insert_val .= GetSQL($row_max_add["add_id"],"int");
//            }else{
//                if($type == "arr"){
//                    $community_insert_val .= '`'.$temlate.'`,';
//                }else if(is_int($data)){
//                    $community_insert_val .= '`'.GetSQL($temlate,"int").'`,';
//                }else{
//                    $community_insert_val .= '`'.GetSQL($temlate,"text").'`,';
//                }
//            }
//        }
//        //去尾
//        $community_insert_key = substr($community_insert_key,0,-1);
//        $community_insert_val = substr($community_insert_val,0,-1);
//        $sql = "INSERT INTO `rent_house_community` ({$community_insert_key})VALUES({$community_insert_val})";
//        Db::rowSQL($sql);
//
//        $community_update ='';
//        foreach($this->rent_house_community_arr as $key => $value){
//            $data = "";//每次都先漂白
//            $type = "";//進去洗的類別
//            $data = Tools::getValue($value);//資料~
//            $temlate = "";
//            if(is_array($data)){//確認是否是arr是則進行處理 不是則不處理arr流程
//                $type= 'arr';
//                foreach($data as $k =>$v){
//                    if(empty($temlate)){
//                        $temlate .= '"'.GetSQL($v,"int").'"';
//                    }else{
//                        $temlate .= ',"'.GetSQL($v,"int").'"';
//                    }
//                }
//                $temlate = "[{$temlate}]";
//            }else{
//                $temlate = $data;
//            }
//            if($type == "arr"){
//                $community_update .= "`{$value}`='".$temlate."',";
//            }else if(is_int($data)){
//                $community_update .= '`'.$value.'`='.GetSQL($data,"int").",";
//            }else{
//                $community_update .= '`'.$value.'`='.GetSQL($data,"text").",";
//            }
//        }
//        $community_update = substr($community_update,0,-1);
//        $community_update = "UPDATE `rent_house_community` SET {$community_update} WHERE rent_house_community_code=".GetSQL($rent_house_community_code,"text");
//        Db::rowSQL($community_update);


//
//        $insert_key = "";
//        $insert_val = "";
//
//        //這塊是專門insert 因此我就不給hidden了
//        foreach($this->tpl_data as $key => $val){
//            if($val['index']==true) {//跳過因為標頭index會自動產生
//
//            }else if($val['link']==true){//連接點 由於這邊是已經有id_rent_house 因此 直接給質就好
//                $insert_key .= '`'.$key.'`,';
//                $sql = "SELECT max(id_rent_house) as add_id FROM rent_house";
//                $row_max_add = Db::rowSQL($sql,true);
//                $insert_val .= ''.GetSQL($row_max_add["add_id"],"text").',';//改變因為add的時候並不會有檔案 因此要用到max+1才行
//            }else if($val['type']=='hidden'){
//                $insert_key .= '`'.$key.'`,';
//                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
//            }else if($val['type']=='text' || $val['type']=='textarea'){
//                $insert_key .= '`'.$key.'`,';
//                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
//            }else if($val['type']=='number'){
//                $insert_key .= '`'.$key.'`,';
//                $insert_val .= ''.GetSQL($_POST[$key],"int").',';
//            }else if($val['type']=='checkbox'){//由於目前這tpl的checkbox 是單選因此不用額外判斷
//                $insert_key .= '`'.$key.'`,';
//                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
//            }else if(!empty($val['tpl'])){
//                if($val['tpl']==1){
//                    $tpl =["position","num","brand","status"];
//                }else if($val['tpl']==2){
//                    $tpl =["position","num","chair","brand","status"];
//
//                }else if($val['tpl']==3){
//                    $tpl =["position","num","type","brand","status"];
//
//                }else if($val['tpl']==4){
//                    $tpl =["position","num","type","brand","status"];
//
//                }else if($val['tpl']==5){
//                    $tpl =["position","num","status"];
//
//                }else if($val['tpl']==6){
//                    $tpl =["num","status"];
//
//                }else if($val['tpl']==7){
//                    $tpl =["position","num","type","status"];
//
//                }else if($val['tpl']==8){
//                    $tpl =["num","type","status"];
//
//                }
//                foreach($tpl as $tp_k => $tp_v){
//                    $temp = "";
//                    $con_var = "{$key}_{$tp_v}";
//                    foreach($_POST[$con_var] as $k_k => $k_v){
//                        $temp .= '"'.$k_v.'",';
//                    }
//                    $temp = substr($temp,0,-1);//去尾
//                    $insert_key .= '`'.$con_var.'`,';
//                    $insert_val .="'[{$temp}]',";
//                }
//            }
//        }
//        $insert_key = substr($insert_key,0,-1);//去尾
//        $insert_val = substr($insert_val,0,-1);//去尾
//        $sql = "INSERT INTO `home_appliances` ({$insert_key})VALUES({$insert_val})";
//        Db::rowSQL($sql);


//        //家具家電設備的log
//        $sql = "SELECT max(id_home_appliances) max_id FROM home_appliances";
//        $max_row = Db::rowSQL($sql,true);
//
//        $sql = "SELECT * FROM home_appliances WHERE id_home_appliances=".GetSQL($max_row["max_id"],"int");
//        $insert_log = Db::rowSQL($sql,true);
//        $insert_key_log = '';
//        $insert_val_log = '';
//        foreach($insert_log as $k => $v){
//            $insert_key_log .= '`'.$k.'`,';
//            $insert_val_log .= "'{$v}',";
//        }
//        $insert_key_log .='`id_admin`';
//        $insert_val_log .= GetSQL($_SESSION['id_admin'],"int");
//        $sql = "INSERT INTO `home_appliances_log` ({$insert_key_log})VALUES({$insert_val_log})";
//        Db::rowSQL($sql);

        //租汽機車
        $sql = "SELECT max(id_rent_house) as add_id FROM rent_house";
        $row_max_add = Db::rowSQL($sql,true);
        $update = "";
        foreach($this->park_space_arr as $key => $value){
            $temp='';
            foreach($_POST[$value] as $k => $v){
                $temp .='"'.$v.'",';//變成"資料",
            }
            $temp = substr($temp,0,-1);//去尾
            $update .=$value.'='.GetSQL('['.$temp.']',"text").',';//變成key=value,
        }
        $update = substr($update,0,-1);//去尾
        $sql = "UPDATE rent_house SET {$update} WHERE id_rent_house=".GetSQL($row_max_add["add_id"],"text");
        Db::rowSQL($sql);

//尾部上回原先資料處理
//            parent::processAdd();
        if ($num) {
            if (Tools::isSubmit('submitAdd' . $this->table)) {
                $this->back_url = self::$currentIndex . '&conf=1';
            }
            if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
                if (in_array('edit', $this->post_processing)) {
                    $this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
                } else {
                    $this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
                }
            }
            if (Tools::isSubmit('submitAdd' . $this->table . 'AndContinue')) {
                $this->back_url = self::$currentIndex . '&add' . $this->table . '&conf=1';
            }
            Tools::redirectLink($this->back_url);
        } else {
            $this->_errors[] = $this->l('沒有新增任何資料!');
        }
    }

    public function processEdit(){
//        if(!empty($_POST["id_home_appliances"])){//有被建立
//            $update_data = "";
//            $update_where="";
//            //這塊是專門insert 因此我就不給hidden了
//            foreach($this->tpl_data as $key => $val){
//                if($val['index']==true){//跳過因為標頭index會自動產生
//                    $update_where="{$key} =".GetSQL($_POST[$key],"int");
//                }else if($val['type']=='hidden'){
//                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
//                }else if($val['type']=='text' || $val['type']=='textarea'){
//                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
//                }else if($val['type']=='number'){
//                    $update_data .= $key.'='.GetSQL($_POST[$key],"int").',';
//                }else if($val['type']=='checkbox'){//由於目前這tpl的checkbox 是單選因此不用額外判斷
//                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
//                }else if(!empty($val['tpl'])){
//                    if($val['tpl']==1){
//                        $tpl =["position","num","brand","status"];
//                    }else if($val['tpl']==2){
//                        $tpl =["position","num","chair","brand","status"];
//
//                    }else if($val['tpl']==3){
//                        $tpl =["position","num","type","brand","status"];
//
//                    }else if($val['tpl']==4){
//                        $tpl =["position","num","type","brand","status"];
//
//                    }else if($val['tpl']==5){
//                        $tpl =["position","num","status"];
//
//                    }else if($val['tpl']==6){
//                        $tpl =["num","status"];
//
//                    }else if($val['tpl']==7){
//                        $tpl =["position","num","type","status"];
//
//                    }else if($val['tpl']==8){
//                        $tpl =["num","type","status"];
//
//                    }
//                    foreach($tpl as $tp_k => $tp_v){
//                        $temp = "";
//                        $con_var = "{$key}_{$tp_v}";
//                        foreach($_POST[$con_var] as $k_k => $k_v){
//                            $temp .= '"'.$k_v.'",';
//                        }
//                        $temp = substr($temp,0,-1);//去尾
//                        $update_data .="`{$con_var}`='[{$temp}]',";
//                    }
//                }
//            }
//            $update_data = substr($update_data,0,-1);//去尾
//            $sql = "UPDATE `home_appliances` SET {$update_data} WHERE {$update_where}";
//            Db::rowSQL($sql);
//        }else{
//            $insert_key = "";
//            $insert_val = "";
//
//            //這塊是專門insert 因此我就不給hidden了
//            foreach($this->tpl_data as $key => $val){
//                if($val['index']==true) {//跳過因為標頭index會自動產生
//
//                }else if($val['link']==true){//連接點 由於這邊是已經有id_rent_house 因此 直接給質就好
//                    $insert_key .= '`'.$key.'`,';
//                    $insert_val .= ''.GetSQL($_POST["id_rent_house"],"text").',';
//                }else if($val['type']=='hidden'){
//                    $insert_key .= '`'.$key.'`,';
//                    $insert_val .= ''.GetSQL($_POST[$key],"text").',';
//                }else if($val['type']=='text' || $val['type']=='textarea'){
//                    $insert_key .= '`'.$key.'`,';
//                    $insert_val .= ''.GetSQL($_POST[$key],"text").',';
//                }else if($val['type']=='number'){
//                    $insert_key .= '`'.$key.'`,';
//                    $insert_val .= ''.GetSQL($_POST[$key],"int").',';
//                }else if($val['type']=='checkbox'){//由於目前這tpl的checkbox 是單選因此不用額外判斷
//                    $insert_key .= '`'.$key.'`,';
//                    $insert_val .= ''.GetSQL($_POST[$key],"text").',';
//                }else if(!empty($val['tpl'])){
//                    if($val['tpl']==1){
//                        $tpl =["position","num","brand","status"];
//                    }else if($val['tpl']==2){
//                        $tpl =["position","num","chair","brand","status"];
//
//                    }else if($val['tpl']==3){
//                        $tpl =["position","num","type","brand","status"];
//
//                    }else if($val['tpl']==4){
//                        $tpl =["position","num","type","brand","status"];
//
//                    }else if($val['tpl']==5){
//                        $tpl =["position","num","status"];
//
//                    }else if($val['tpl']==6){
//                        $tpl =["num","status"];
//
//                    }else if($val['tpl']==7){
//                        $tpl =["position","num","type","status"];
//
//                    }else if($val['tpl']==8){
//                        $tpl =["num","type","status"];
//
//                    }
//                    foreach($tpl as $tp_k => $tp_v){
//                        $temp = "";
//                        $con_var = "{$key}_{$tp_v}";
//                        foreach($_POST[$con_var] as $k_k => $k_v){
//                            $temp .= '"'.$k_v.'",';
//                        }
//                        $temp = substr($temp,0,-1);//去尾
//                        $insert_key .= '`'.$con_var.'`,';
//                        $insert_val .="'[{$temp}]',";
//                    }
//                }
//            }
//            $insert_key = substr($insert_key,0,-1);//去尾
//            $insert_val = substr($insert_val,0,-1);//去尾
//            $sql = "INSERT INTO `home_appliances` ({$insert_key})VALUES({$insert_val})";
//            Db::rowSQL($sql);
//        }


//        //家具家電設備的log
//        $sql = "SELECT max(id_home_appliances) max_id FROM home_appliances";
//        $max_row = Db::rowSQL($sql,true);
//
//        $sql = "SELECT * FROM home_appliances WHERE id_home_appliances=".GetSQL($max_row["max_id"],"int");
//        $insert_log = Db::rowSQL($sql,true);
//        $insert_key_log = '';
//        $insert_val_log = '';
//        foreach($insert_log as $k => $v){
//            $insert_key_log .= '`'.$k.'`,';
//            $insert_val_log .= "'{$v}',";
//        }
//        $insert_key_log .='`id_admin`';
//        $insert_val_log .= GetSQL($_SESSION['id_admin'],"int");
//        $sql = "INSERT INTO `home_appliances_log` ({$insert_key_log})VALUES({$insert_val_log})";
//        Db::rowSQL($sql);


        //租汽機車
        $id_rent_house = Tools::getValue("id_rent_house");
        $update = "";
        foreach($this->park_space_arr as $key => $value){
            $temp='';
            foreach($_POST[$value] as $k => $v){
                $temp .='"'.$v.'",';//變成"資料",
            }
            $temp = substr($temp,0,-1);//去尾
            $update .=$value.'='.GetSQL('['.$temp.']',"text").',';//變成key=value,
        }
        $update = substr($update,0,-1);//去尾
        $sql = "UPDATE rent_house SET {$update} WHERE id_rent_house=".GetSQL($id_rent_house,"int");
        Db::rowSQL($sql);

        //這個是房東
        Member_landlord::Admin_multiple_table($id_rent_house,$_POST);

        parent::processEdit();
    }

    public function setMedia()
    {
        $this->addJS('/themes/LifeHouse/js/home_appliances.js');
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addJS('/themes/LifeHouse/js/tpl_landlord.js');
        $this->addJS('/themes/LifeHouse/js/pack_space.js');
        $this->addJS('/themes/LifeHouse/js/renthouse.js');
        $this->addJS('/themes/LifeHouse/js/rent_house_key.js');
        $this->addCSS('/themes/LifeHouse/css/renthouse.css');
        $this->addFooterJS('/themes/LifeHouse/js/foot_renthouse.js');
    }

    public function displayAjax(){//不特定多數的ajax
        $action = Tools::getValue('action');
        switch ($action){
            case 'add_landlord'://這邊會包很多東西回去
                $return_data =[
                    'database_county' =>County::getContext()->database_county(),
                    'database_city' =>County::getContext()->database_city(),
                    'database_source'=>Member_landlord::get_source(),
//                    'database_rent_mange_program'=>Member_landlord::get_rent_mange_program(),
//                    'database_intermediary_program'=>Member_landlord::get_intermediary_program(),
                    'database_other_conditions' =>Member_landlord::get_other_conditions(),
//                    'database_rent_union_type' =>Member_landlord::get_rent_union_type(),
//                    'database_pay_type' =>Member_landlord::get_pay_type(),
                ];
                echo json_encode(array("error"=>"","return"=>$return_data));
                break;
            default:

                break;
        }

        exit;
    }

//    public function displayAjaxRentCode(){
//        $action = Tools::getValue('action');    //action = 'Community';
//        $rent_house_code = Tools::getValue('rent_house_code');
//        $id_rent_house = Tools::getValue('id_rent_house');
//        switch ($action){
//            case 'RentCode':
//                $where = "";
//                if(empty($id_rent_house)){
//                    $where = "rent_house_code=".GetSQL($rent_house_code,'text');
//                }else{
//                    $where = "rent_house_code=".GetSQL($rent_house_code,'text').' AND id_rent_house !='.GetSQL($id_rent_house,'int');
//                }
//                $sql = "SELECT count(rent_house_code) as code_count FROM rent_house WHERE ".$where;
//                $row = Db::rowSQL($sql,true);
//
//                echo json_encode(array("error"=>"","return"=>$row["code_count"]));
//                break;
//            default :
//                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
//                break;
//        }
//    }

    public function displayAjaxCity(){
        $action = Tools::getValue('action');
        $id_county =  Tools::getValue('id_county');
        switch ($action) {
            case 'City':
                if(empty($id_county)){
                    echo json_encode(array("error"=>"is not checked county","return"=>""));
                    break;
                }
                $city = County::getContext()->database_city($id_county);
                echo json_encode(array("error"=>"","return"=>$city));
                break;

            default:
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;
    }

    public function displayAjaxDeviceCategory(){
        $action = Tools::getValue('action');    //action = 'DeviceCategory';
        $id_rent_house =   Tools::getValue('id_rent_house');   //id_rent_house
        switch ($action) {
            case 'DeviceCategory':
                if(empty($id_rent_house)){
                    echo json_encode(array("error"=>"is new data","return"=>""));
                    break;
                }
                $sql = "SELECT id_device_category FROM rent_house WHERE id_rent_house = ".GetSQL($id_rent_house,'int');
                $row = Db::rowSQL($sql,true);
                if(!empty($row['id_device_category'])){//先清除 [ ] " (因為我不知道怎處理)
                    $row['id_device_category'] = Db::antonym_array($row['id_device_category'],true);
                    $row['id_device_category'] = explode(",",$row['id_device_category']);//切割成陣列
                }
                echo json_encode(array("error"=>"","return"=>$row));
                break;

            default:
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;
    }

    public function displayAjaxCommunity(){//id_rent_house_community
        $action = Tools::getValue('action');    //action = 'Community';
        $id_rent_house_community =Tools::getValue('id_rent_house_community');
        $community =Tools::getValue('community');
        switch ($action){
            case 'Community':
                $where = "";
                if(!empty($id_rent_house_community)){
                    $where = "id_rent_house_community=".GetSQL($id_rent_house_community,"int");
                }else{
                    $where = "community=".GetSQL($community,"text");
                }

                $sql = "SELECT * FROM `rent_house_community` WHERE ".$where;
                $row = Db::rowSQL($sql,true);//丟回去~

                if(empty($row["id_rent_house_community"])){//整個不存在的意思
                    $row["id_rent_house_community"] = "";//重製掉讓該name變成新的head使用
                }

                if(!empty($row['id_disgust_facility'])){//先清除 [ ] " (因為我不知道怎處理)
                    $row['id_disgust_facility'] = Db::antonym_array($row['id_disgust_facility'],true);
                    $row['id_disgust_facility'] = explode(",",$row['id_disgust_facility']);//切割成陣列
                }

                if(!empty($row['id_public_utilities'])){//先清除 [ ] " (因為我不知道怎處理)
                    $row['id_public_utilities'] = Db::antonym_array($row['id_public_utilities'],true);
                    $row['id_public_utilities'] = explode(",",$row['id_public_utilities']);//切割成陣列
                }
                echo json_encode(array("error"=>"","return"=>$row));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

    public function displayAjaxAG(){
        $action = Tools::getValue('action');    //action = 'Community';
        $ag =Tools::getValue('ag');
        switch ($action){
            case 'AG':
                $sql = "SELECT *,CONCAT(last_name,first_name) as name FROM `admin` WHERE ag =".GetSQL($ag,"int");
                $row = Db::rowSQL($sql,true);//丟回去~
                echo json_encode(array("error"=>"","return"=>$row));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
//        $this->arr_file_type = [
//            '0' => 'img',
//            '1' => 'web_carousel',
//            '2' => 'app_img',
//            '3' => 'app_carousel',
//            '4' => 'building_power',
//            '5' => 'land_power',
//        ];

        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_rent_house = $UpFileVal['id_rent_house'];


        //先判斷操作者是否有啟用浮水印
//        $sql = "SELECT watermark FROM admin WHERE id_admin =".GetSQL($_SESSION["id_admin"],"int");
//        $watermark = Db::rowSQL($sql,true);

        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `rent_house_file` (`id_rent_house`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_rent_house, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int'));
            Db::rowSQL($sql,false,0,'');
//            echo 'id_file:'.$id_file.'<br/>';
//            echo 'sql:'.$sql.'<br/>';
//            echo 'num:'.Db::getContext()->num.'<br/>';

            if (!Db::getContext()->num)
                return false;

            //這邊先敘述做法 1.先取得id_file 的檔案 由上述的$id_file 取得檔案 解url碼 開始做浮水印
            //要上浮水印要這邊反向操作上傳
//            if($watermark["watermark"]=='1'){//有啟用浮水印
//                // 0 1 使用web 2 3 使用app
//                $sql = "SELECT  CONCAT(file_dir,filename) img_url FROM file WHERE id_file=".GetSQL($id_file,"int");
//                $data_img = Db::rowSQL($sql,true);//取得檔案位置
//                $water_type="";//浮水印的搜索類型
//                if($type=='0' || $type=='1'){
//                    $water_type = '0';
//                }else if($type=='2' || $type=='3'){
//                    $water_type = '1';
//                }
//                $sql = "SELECT id_file FROM admin_file WHERE file_type={$water_type} AND id_admin=".GetSQL($_SESSION["id_admin"],"int").' limit 1';
//                $water_before = Db::rowSQL($sql,true);
//                $sql = "SELECT  CONCAT(file_dir,filename) water_url FROM file WHERE id_file=".GetSQL($water_before["id_file"],"int")." ORDER BY id_file DESC ";
//                $water_data = Db::rowSQL($sql,true);
//
//                $after_img = urldecode($data_img["img_url"]);
//                $water_img = urldecode($water_data["water_url"]);
////                echo "{$after_img}   {$water_img}<br/>";
//                Watermark::get_watermark($after_img,$water_img,$after_img);
//            }
            if($type=='0' || $type=='1'){
                $sql = "SELECT  CONCAT(file_dir,filename) img_url FROM file WHERE id_file=".GetSQL($id_file,"int");
                $data_img = Db::rowSQL($sql,true);//取得檔案位置
                $after_img = urldecode($data_img["img_url"]);
                Watermark::get_watermark($after_img,WEB_DIR.DS.'img'.DS.'water'.DS.'water.png',$after_img);

            }
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_rent_house = $UpFileVal['id_rent_house'];

        list($dir, $url) = str_dir_change($id_rent_house);     //切割資料夾
        $dir = WEB_DIR . DS . RENT_HOUSE_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_rent_house = $UpFileVal['id_rent_house'];
        list($dir, $url) = str_dir_change($id_rent_house);
        $url = RENT_HOUSE_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_rent_house)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `rent_house_file`
				WHERE `id_rent_house` = %d',
            GetSQL($id_rent_house, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_rent_house = Tools::getValue('id_rent_house');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_rent_house' => $id_rent_house,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_rent_house);
        $arr_initialPreview = [];
        $arr_initialPreviewConfig =[];
        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }

                    $del_table = "rent_house_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/RentHouse?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];//url後面作補充使她能夠正確刪除資料
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
}
