<?php
class AdminWelcomeController extends AdminController
{
    public function __construct(){
        $this->className = 'AdminWelcomeController';
        parent::__construct();
        $this->tabAccess['add'] = 0;
        $this->tabAccess['edit'] = 0;
        $this->tabAccess['del'] = 0;
        $this->breadcrumb_is_not =1;
    }

    public function checkAccess()	//所有人都有權限
    {
        return true;
    }


    public function initHeader()
    {
        header('Content-Type:text/html; charset=utf-8');
        header('Cache-Control: no-store, no-cache');
        $this->context->smarty->assign([
            'display_admin_menu'=>'1',
            'tab_list'        => Tab::getContext()->getTabList(),
            'web_name'        => WEB_NAME,                //網站名稱
            'NAVIGATION_PIPE' => NAVIGATION_PIPE    //標題間格標示
        ]);
    }


    public function initProcess(){

        $id_diversion = Tools::getValue("id_diversion");
        if(!empty($id_diversion)){
            $_SESSION['id_diversion']=$id_diversion;
            header('Location: https://www.lifegroup.house/manage/ShowNews');
            exit;
        }

        $sql = "SELECT * FROM `diversion` WHERE active=1 ORDER BY position ASC,name ASC";
        $web2_data = Db::rowSQL($sql);


        foreach($web2_data as $key => $value){
            $sql = "SELECT * FROM diversion_file WHERE file_type=0 AND id_diversion=".$value['id_diversion'].' ORDER BY id_diversion_file ASC ';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $web2_data[$key]['img'] = $row['url'];
            //圖片end
        }

        $web2_long = count($web2_data)-1;


        $this->context->smarty->assign([
            'web2_data'=>$web2_data,
            'web2_long'=>$web2_long
        ]);
        parent::initProcess();
    }






}