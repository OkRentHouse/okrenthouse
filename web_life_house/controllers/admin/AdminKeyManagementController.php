<?php

class AdminKeyManagementController extends AdminController{

    public function __construct(){
        $this->className       = 'AdminKeyManagementController';
        $this->table           = 'key_management';
        $this->fields['index'] = 'id_key_management';
        $this->fields['title'] = '鑰匙管理';
        $this->_as             = 'k_m';
        $this->_join           = ' LEFT JOIN key_management_file as k_m_f ON k_m_f.`id_key_management` = k_m.`id_key_management` ';
        $this->fields['order'] = ' ORDER BY k_m.`id_key_management` ASC ';

        $this->fields['list'] = [
            'id_key_management' => [
                'index'  => true,
                'filter_key' => 'k_m!id_key_management',
                'title'  => $this->l('鑰匙管理ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],

            'rent_house_code' => [
                'filter_key' => 'k_m!rent_house_code',
                'title'  => $this->l('物件編號'),
                'class'  => 'text-center',
            ],

            'ag' => [
                'filter_key' => 'k_m!ag',
                'title'  => $this->l('負責的ag'),
                'class'  => 'text-center',
            ],

            'key_use' => [
                'filter_key' => 'k_m!key_use',
                'title' => $this->l('鑰匙用途'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'value' => 0,
                        'title' => $this->l('帶看'),
                    ],
                    [
                        'value' => 1,
                        'title' => $this->l('交屋'),
                    ],
                    [
                        'value' => 2,
                        'title' => $this->l('管理'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'door_key_type' => [
                'filter_key' => 'k_m!door_key_type',
                'title'  => $this->l('大門鑰匙樣式'),
                'class'  => 'text-center',
            ],

            'door_key_num' => [
                'filter_key' => 'k_m!door_key_num',
                'title'  => $this->l('大門鑰匙數量'),
                'class'  => 'text-center',
            ],

            'induction_key_type' => [
                'filter_key' => 'k_m!induction_key_type',
                'title'  => $this->l('磁扣鑰匙樣式'),
                'class'  => 'text-center',
            ],

            'induction_key_num' => [
                'filter_key' => 'k_m!induction_key_num',
                'title'  => $this->l('磁扣鑰匙樣式'),
                'class'  => 'text-center',
            ],

            'lane_key_type' => [
                'filter_key' => 'k_m!lane_key_type',
                'title'  => $this->l('車道鑰匙樣式'),
                'class'  => 'text-center',
            ],

            'lane_key_num' => [
                'filter_key' => 'k_m!lane_key_num',
                'title'  => $this->l('車道鑰匙數量'),
                'class'  => 'text-center',
            ],

            'iron_gate_key_type' => [
                'filter_key' => 'k_m!iron_gate_key_type',
                'title'  => $this->l('鐵門鑰匙樣式'),
                'class'  => 'text-center',
            ],

            'iron_gate_key_num' => [
                'filter_key' => 'k_m!iron_gate_key_num',
                'title'  => $this->l('鐵門鑰匙數量'),
                'class'  => 'text-center',
            ],

            'deliver_time' => [
                'filter_key' => 'k_m!deliver_time',
                'title'  => $this->l('交付時間'),
                'class'  => 'text-center',
            ],

            'retrieve_reason' => [
                'filter_key' => 'k_m!retrieve_reason',
                'title' => $this->l('取回原因'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'retrieve_reason_0',
                        'value' => 0,
                        'title' => $this->l('下架'),
                    ],
                    [
                        'class' => 'retrieve_reason_1',
                        'value' => 1,
                        'title' => $this->l('成交'),
                    ],
                    [
                        'class' => 'retrieve_reason_2',
                        'value' => 2,
                        'title' => $this->l('收回'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'retrieve_time' => [
                'filter_key' => 'k_m!deliver_time',
                'title'  => $this->l('取回時間'),
                'class'  => 'text-center',
            ],

            'img'         => [
                'title'      => $this->l('縮圖'),
                'order'      => true,
                'filter'     => true,
                'filter_sql' => ' IF(k_m_f.`id_key_management_file` > 0, 1, 0) ',
                'class'      => 'text-center', 'values' => [
                    [
                        'class' => 'img_1',
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'img_0 red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],

            'create_name' => [
                'filter_key' => 'k_m!create_name',
                'title'  => $this->l('建立人'),
                'class'  => 'text-center',
            ],

            'create_time' => [
                'filter_key' => 'k_m!create_time',
                'title'  => $this->l('建立時間'),
                'class'  => 'text-center',
            ],

        ];

        $this->arr_file_type = [
            '0' => 'img',
        ];

        $this->fields['form'] = [
            'KeyManagement' => [
                'legend' => [
                    'title' => $this->l('鑰匙管理'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_key_management' => [
                        'name' => 'id_key_management',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'key_use' => [
                        'name'      => 'key_use',
                        'type'      => 'select',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'label' => $this->l('用途'),
                        'options'   => [
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '帶看',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => '交屋',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '管理',
                                    'val'  => '2',
                                ],
                            ],
                        ],
                    ],

                    'ag' => [
                        'name' => 'ag',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('AG'),
                        'required' => true,
                        'maxlength' =>20,
                    ],

                    'rent_house_code' => [
                        'name' => 'rent_house_code',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('物件編號'),
                        'required' => true,
                        'maxlength' =>20,
                    ],

                    'deliver_time' => [
                        'name' => 'deliver_time',
                        'type' => 'date',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('交付時間'),
                        'required' => true,
                        'maxlength' =>20,
                    ],

                    'retrieve_time' => [
                        'name' => 'retrieve_time',
                        'type' => 'date',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('取回時間'),
                        'required' => true,
                        'maxlength' =>20,
                    ],

                    'retrieve_reason' => [
                        'name'      => 'retrieve_reason',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('取回原因'),
                        'options'   => [
                            //自訂值
                            'default'     => [
                                'text' => '請選擇取回原因',
                                'val'  => '',
                            ],
                            'val'         => [
                                [
                                    'text' => '下架',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => '成交',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '收回',
                                    'val'  => '2',
                                ],
                            ],
                        ],
                    ],

                    'borrow' => [
                        'name' => 'borrow',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('另借聯絡'),
                        'maxlength' => '20',
                    ],

                    'door_key_type' => [
                        'name' => 'door_key_type',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('大門鑰匙'),
                        'maxlength' =>20,
                        'suffix' => $this->l('式'),
                        'is_prefix' => true,
                    ],

                    'door_key_num' => [
                        'name' => 'door_key_num',
                        'type' => 'number',
                        'suffix' => $this->l('組'),
                        'is_suffix' => true,
                    ],

                    'induction_key_type' => [
                        'name' => 'induction_key_type',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('磁扣鑰匙'),
                        'maxlength' =>20,
                        'suffix' => $this->l('式'),
                        'is_prefix' => true,
                    ],

                    'induction_key_num' => [
                        'name' => 'induction_key_num',
                        'type' => 'number',
                        'suffix' => $this->l('組'),
                        'is_suffix' => true,
                    ],



                    'lane_key_type' => [
                        'name' => 'lane_key_type',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('車道鑰匙'),
                        'maxlength' =>20,
                        'suffix' => $this->l('式'),
                        'is_prefix' => true,
                    ],

                    'lane_key_num' => [
                        'name' => 'lane_key_num',
                        'type' => 'number',
                        'suffix' => $this->l('組'),
                        'is_suffix' => true,
                    ],

                    'iron_gate_key_type' => [
                        'name' => 'iron_gate_key_type',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('鐵門鑰匙'),
                        'maxlength' =>20,
                        'suffix' => $this->l('式'),
                        'is_prefix' => true,
                    ],

                    'iron_gate_key_num' => [
                        'name' => 'iron_gate_key_num',
                        'type' => 'number',
                        'suffix' => $this->l('組'),
                        'is_suffix' => true,
                    ],

                    'create_name' => [
                        'name' => 'create_name',
                        'type' => 'text',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'label' => $this->l('建立人'),
                        'disabled' =>true,
                    ],

                    'create_time' => [
                        'name' => 'create_time',
                        'type' => 'view',
                        'form_col' =>6,
                        'label_col' => 3,
                        'col'=>6,
                        'auto_datetime' => 'add',
                        'label' => $this->l('建立時間'),
                    ],
                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],

            'img' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('簽章檔'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
//                            'max_width' => 50,
//                            'max_height' => 50,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('上傳簽章檔'),
                    ],
                ],
            ],
        ];
        parent::__construct();
    }

    public function initProcess(){//處理上下架問題
        if(empty(Tools::getValue('create_name'))) $_POST["create_name"] = $_SESSION["name"];
        parent::initProcess();
    }

    public function processDel(){
        $this->_errors[] = $this->l('該資料為紀錄檔無法刪除');
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal){
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_key_management = $UpFileVal['id_key_management'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `key_management_file` (`id_key_management`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_key_management, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );

            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }
    public function iniUpFileDir(){
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_key_management = $UpFileVal['id_key_management'];
        list($dir, $url) = str_dir_change($id_key_management);     //切割資料夾
        $dir = WEB_DIR . DS . KEY_MANAGEMENT_IMG_DIR . DS . $dir;
        return $dir;
    }
    public function iniUpFileUrl(){
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_key_management = $UpFileVal['id_key_management'];
        list($dir, $url) = str_dir_change($id_key_management);
        $url = KEY_MANAGEMENT_IMG_URL . $url;
        return $url;
    }
    public function getUpFile($id_key_management){
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `key_management_file`
				WHERE `id_key_management` = %d',
            GetSQL($id_key_management, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }
    public function getUpFileList(){
        $id_key_management = Tools::getValue('id_key_management');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_key_management' => $id_key_management,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_key_management);
        $arr_initialPreview =[];
        $arr_initialPreviewConfig =[];
        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $del_table = "key_management_file";
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/KeyManagement?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table .  '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('/themes/LifeHouse/css/renthouse.css');
    }
}