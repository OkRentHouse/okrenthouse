<?php

class AdminExteriorWallController extends AdminController
{
	public $no_link = false;

	public function __construct()
	{

		$this->className       = 'AdminExteriorWallController';
		$this->table           = 'exterior_wall';
		$this->fields['index'] = 'id_exterior_wall';
		$this->fields['title'] = '外牆材質';
		$this->actions[]       = 'list';
		$this->fields['list']  = [
			'id_exterior_wall' => [
				'index'  => true,
				'type'   => 'checkbox',
				'select' => '',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'exterior_wall'    => [
				'title'  => $this->l('外牆材質'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'position'  => [
				'title' => $this->l('位置'),
				'order' => true,
				'class' => 'text-center',
			],
		];

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('外牆材質'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_exterior_wall' => [
						'name'     => 'id_exterior_wall',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'exterior_wall'    => [
						'name'        => 'exterior_wall',
						'type'        => 'text',
						'label'       => $this->l('來源名稱'),
						'placeholder' => $this->l('請輸入來源名稱'),
						'maxlength'   => '20',
						'required'    => true,
					],
					'position'  => [
						'type'  => 'number',
						'label' => $this->l('位置'),
						'min'   => 0,
						'name'  => 'position',
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}


	public function processDel()
	{
		$id_exterior_wall = Tools::getValue('id_exterior_wall');
		$sql       = sprintf('SELECT `id_rent_house`
											FROM `' . DB_PREFIX_ . 'rent_house`
											WHERE `id_exterior_wall` = %d
											LIMIT 0, 1',
			GetSQL($id_exterior_wall, 'int'));
		Db::rowSQL($sql);
		if (Db::getContext()->num()) {
			$this->_errors[] = $this->l('無法刪除正在使用的外牆材質');
		}
		if($id_exterior_wall == 1){
			$this->_errors[] = $this->l('無法刪除"其他"項目');
		}
		parent::processDel();
	}
}