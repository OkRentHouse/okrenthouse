<?php
class AdminEproWindowsController extends AdminController
{
    public $tpl_folder;    //樣版資料夾
    public $page = 'epro_cms';
    public $ed;

    public function __construct()
    {
        $this->conn = 'ilifehou_epro';
        $this->className       = 'AdminEproWindowsController';
        $this->fields['title'] = '裝修達人百葉窗';
        $this->table           = 'epro_windows';
        $this->fields['index'] = 'id_epro_windows';
        $this->fields['order'] = ' ORDER BY e_w.`position` ASC, e_w.`id_epro_windows` ASC';
        $this->_as = 'e_w';
        $this->_join = ' LEFT JOIN `epro_windows_file` AS e_w_f ON e_w_f.`id_epro_windows` = e_w.`id_epro_windows` ';

        //網站縮圖 網站輪播 app縮圖 app輪播 建築權狀 土地權狀
        $this->arr_file_type = [
            '0' => 'img',
        ];

        $this->fields['list']  = [
            'id_epro_windows'         => [
                'index'  => true,
                'hidden' => true,
                'type'   => 'checkbox',
                'label_col' => '2',
                'col' => '3',
                'class'  => 'text-center',
                'filter_key' => 'e_w!id_epro_windows',
            ],
            'title'          => [
                'title'  => $this->l('Title'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w!title',
            ],

            'keyword'          => [
                'title'  => $this->l('關鍵字'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w!keyword',
            ],

            'url'          => [
                'title'  => $this->l('url'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w!url',
            ],

            'img' => [
                'title' => $this->l('縮圖'),
                'order' => true,
                'filter' => true,
                'filter_sql' => 'IF(e_w_f.`id_epro_windows_file` > 0, 1, 0)',
                'class' => 'text-center', 'values' => [
                    [
                        'class' => 'img_1',
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'img_0 red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],

            'position' => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w!position',
                'filter' => true,
            ],
            'active'         => [
                'title'  => $this->l('啟用'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w!active',
            ],
        ];

        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('裝修達人百葉窗'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_epro_windows' => [
                        'name'     => 'id_epro_windows',
                        'type'     => 'hidden',
                        'label_col' => '3',
                        'col' => '3',
                        'index'    => true,
                        'required' => true,
                    ],
                    'title'          => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label_col' => '3',
                        'col' => '9',
                        'maxlength' => 20,
                        'label'     => $this->l('title'),
                        'required' => true,
                    ],
                    'url'          => [
                        'name'      => 'url',
                        'type'      => 'text',
                        'label_col' => '3',
                        'col' => '9',
                        'maxlength' => 200,
                        'label'     => $this->l('網址'),
                        'required' => true,
                    ],
                    'keyword'          => [
                        'name'      => 'keyword',
                        'type'      => 'text',
                        'label_col' => '3',
                        'col' => '9',
                        'maxlength' => 255,
                        'label'     => $this->l('關鍵字'),
                        'required' => true,
                    ],
                    'active'         => [
                        'type'     => 'switch',
                        'label_col' => '3',
                        'col' => '3',
                        'label'    => $this->l('啟用狀態'),
                        'name'     => 'active',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    'position'           => [
                        'label_col' => '3',
                        'col' => '3',
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay'  => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('重置'),
                ],
            ],

            'img' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('小圖示'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
                        'p' => $this->l('圖片只可上傳一張'),
                    ],
                ],
            ],
        ];
        parent::__construct();
    }



    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_epro_windows = $UpFileVal['id_epro_windows'];


        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `epro_windows_file` (`id_epro_windows`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_epro_windows, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int'));
            Db::rowSQL($sql,false,0,$this->conn);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_epro_windows = $UpFileVal['id_epro_windows'];

        list($dir, $url) = str_dir_change($id_epro_windows);     //切割資料夾
        $dir = WEB_DIR . DS . EPRO_WINDOWS_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_epro_windows = $UpFileVal['id_epro_windows'];
        list($dir, $url) = str_dir_change($id_epro_windows);
        $url = EPRO_WINDOWS_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_epro_windows)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `epro_windows_file`
				WHERE `id_epro_windows` = %d',
            GetSQL($id_epro_windows, 'int'));
        $arr_row = Db::rowSQL($sql,false,0,$this->conn);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_epro_windows = Tools::getValue('id_epro_windows');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_epro_windows' => $id_epro_windows,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_epro_windows);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }

                    $del_table = "epro_windows_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/EproWindows?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];//url後面作補充使她能夠正確刪除資料
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
}