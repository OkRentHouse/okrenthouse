<?php

class AdminSkillClassificationController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminSkillClassificationController';
        $this->table           = 'skill_classification';
        $this->fields['index'] = 'id_skill_classification';
        $this->fields['title'] = '技能分類';
        $this->fields['order'] = ' ORDER BY s_c_i.`position` ASC';
        $this->_as = 's_c_i';
        $this->_join = ' LEFT JOIN `skill_class` AS s_c ON s_c.`id_skill_class` = s_c_i.`id_skill_class` ';

        $this->fields['list'] = [
            'id_device_category' => [
                'filter_key' => 's_c_i!id_skill_classification',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],

            'title_main'    => [
                'filter_key' => 's_c!title',
                'title'  => $this->l('設備類別'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'title'    => [
                'filter_key' => 's_c_i!title',
                'title'  => $this->l('設備分類'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'active' => [
                'filter_key' => 's_c_i!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'position'     => [
                'filter_key' => 's_c_i!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            'skill_classification' => [
                'legend' => [
                    'title' => $this->l('技能分類'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_device_category' => [
                        'name'     => 'id_skill_classification',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('id'),
                        'required' => true,
                    ],
                    'id_skill_class'          => [
                        'name'      => 'id_skill_class',
                        'type'      => 'select',
                        'label'     => $this->l('設備主類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇設備類別',
                                'val'  => '',
                            ],
                            'table'   => 'skill_class',
                            'text'    => 'title',
                            'value'   => 'id_skill_class',
                            'order'   => ' `position` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'title'    => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('技能分類'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],
                    'active' => [
                        'type' => 'switch',
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],

        ];

        parent::__construct();
    }

    public function processDel(){
        $this->_errors[] = $this->l('無法刪除正在使用的分類!');
        parent::processDel();
    }

    public function setMedia()
    {
        parent::setMedia();
    }

}