<?php

class AdminAnnouncementController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminAnnouncementController';
        $this->table           = 'announcement';
        $this->fields['index'] = 'id_announcement';
        $this->_as             = 'a';
        $this->fields['title'] = '發布消息';
        $this->_join           = ' LEFT JOIN `announcement_file` AS a_f ON a.`id_announcement` = a_f.`id_announcement`';
        $this->fields['order'] = ' ORDER BY `position` ASC, `create_time` DESC, `id_announcement` DESC';

        $this->fields['list_num'] = 50;

        $this->fields['list'] = [
            'id_announcement'      => [
                'filter_key' => 'a!id_announcement',
                'index'      => true,
                'title'      => $this->l('ID'),
                'type'       => 'checkbox',
                'hidden'     => true,
                'class'      => 'text-center',
            ],
            'title'                  => [
                'filter_key' => 'a!title',
                'title'      => $this->l('標題'),
                'order'      => true,
                'filter'     => true,
                'class'      => 'text-center',
            ],
            'content'     => [
                'filter_key' => 'a!content',
                'title'      => $this->l('內容'),
                'order'      => true,
                'filter'     => true,
                'class'      => 'text-center',
                'show'       => false,
            ],
            'img'         => [
                'title'      => $this->l('縮圖'),
                'order'      => true,
                'filter'     => true,
                'filter_sql' => 'IF(a_f.`id_amf` > 0, 1, 0)',
                'class'      => 'text-center', 'values' => [
                    [
                        'class' => 'img_1',
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'img_0 red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],
            'create_time'        => [
                'filter_key' => 'a!create_time',
                'title'      => $this->l('建立時間'),
                'order'      => true,
                'filter'     => true,
                'class'      => 'text-center',
            ],
            'select_option'                => [
                'title'  => $this->l('好康會員店家選取'),
                'order'  => true,
                'filter' => 'switch',
                'class'  => 'text-center',
                'values' => [
                    [
                        'class' => 'select_option_0',
                        'value' => 0,
                        'title' => $this->l('會員'),
                    ],
                    [
                        'class' => 'select_option_1',
                        'value' => 1,
                        'title' => $this->l('店家'),
                    ],
                ],
            ],
            'position'        => [
                'title' => $this->l('位置'),
                'order' => true,
                'class' => 'text-center',
            ],
            'active'      => [
                'filter_key' => 'a!active',
                'title'      => $this->l('啟用狀態'),
                'order'      => true,
                'filter'     => true,
                'class'      => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            'id_announcement' => [
                'tab'    => 'letter',
                'legend' => [
                    'title' => $this->l('發布消息'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_announcement'      => [
                        'name'      => 'id_announcement',
                        'type'      => 'hidden',
                        'label_col' => 3,
                        'index'     => true,
                        'required'  => true,
                    ],

                    'title'                  => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label_col' => 3,
                        'label'     => $this->l('標題'),
                        'maxlength' => '50',
                        'required'  => true,
                    ],

                    'content'            => [
                        'name'      => 'content',
                        'type'      => 'textarea',
                        'label_col' => 3,
                        'maxlength' => 512,
                        'label'     => $this->l('內容'),
                        'col'       => 8,
                        'rows'      => 20,
                        'required'  => true,
                    ],
                    'html_content'                => [
                        'name'      => 'html_content',
                        'type'      => 'textarea',
                        'label_col' => 3,
                        'label'     => $this->l('內頁內容'),
                        'class'     => 'tinymce',
                        'required'  => true,
                        'col'       => 8,
                        'rows'      => 30,
                    ],
                    'create_time'               => [
                        'name'          => 'create_time',
                        'type'          => 'view',
                        'label_col'     => 3,
                        'auto_datetime' => 'add',
                        'label'         => $this->l('建立時間'),
                    ],

                    'select_option'                 => [
                        'type'      => 'switch',
                        'label_col' => 3,
                        'label'     => $this->l('選取發布類別'),
                        'name'      => 'select_option',
                        'val'       => 0,
                        'required'  => true,
                        'values'    => [
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('會員'),
                            ],
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('店家'),
                            ],
                        ],
                    ],

                    [
                        'type'  => 'number',
                        'label' => $this->l('位置'),
                        'min'   => 0,
                        'name'  => 'position',
                    ],

                    'active'                 => [
                        'type'      => 'switch',
                        'label_col' => 3,
                        'label'     => $this->l('啟用狀態'),
                        'name'      => 'active',
                        'val'       => 1,
                        'values'    => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
            'img'            => [
                'tab'    => 'img',
                'legend' => [
                    'title' => $this->l('圖片'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'img' => [
                        'name'      => 'img',
                        'no_label'  => true,
                        'type'      => 'file',
                        'language'  => 'zh-TW',
                        'file'      => [
                            'icon'             => false,
                            'auto_upload'      => true,
                            'max'              => 1,
                            'allowed'          => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width'        => 460,
                            'max_height'       => 350,
                            'resize'           => true,
                            'resizePreference' => 'height',
                        ],
//                        'multiple'  => true,
                        'col'       => 12,
                        'no_action' => true,
                        'p'         => $this->l('圖片建議大小 460*350'),
                    ],
                ],
            ],
        ];

        parent::__construct();
    }

    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
    }


    public function getUpFile($id)
    {
        $sql = sprintf('SELECT `id_file`
				FROM `announcement_file`
				WHERE `id_announcement` = %d',
            GetSQL($id, 'int'));
        $arr_row = Db::rowSQL($sql);
        $arr = [];
        foreach($arr_row as $i => $v){
            $arr[] = $v['id_file'];
        }
        return $arr;
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json              = new JSON();
        $UpFileVal         = (array)$json->decode($UpFileVal);
        $arr_id_file       = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_geely_for_rent = $UpFileVal['id_announcement'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `announcement_file` (`id_announcement`, `id_file`) VALUES (%d, %d)',
                GetSQL($id_geely_for_rent, 'int'),
                GetSQL($id_file, 'int')
            );
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal         = Tools::getValue('UpFileVal');
        $json              = new JSON();
        $UpFileVal         = (array)$json->decode($UpFileVal);
        $id_announcement = $UpFileVal['id_announcement'];
        list($dir, $url) = str_dir_change($id_announcement);
        $dir = WEB_DIR . DS . ANNOUNCEMENT_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal         = Tools::getValue('UpFileVal');
        $json              = new JSON();
        $UpFileVal         = (array)$json->decode($UpFileVal);
        $id_announcement = $UpFileVal['id_announcement'];
        list($dir, $url) = str_dir_change($id_announcement);
        $url = ANNOUNCEMENT_IMG_URL . $url;
        return $url;
    }
}