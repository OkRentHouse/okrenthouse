<?php

class AdminDisgustFacilityClassController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminDisgustFacilityClassController';
        $this->table           = 'disgust_facility_class';
        $this->fields['index'] = 'id_disgust_facility_class';
        $this->fields['title'] = '嫌惡設施類別';
        $this->fields['order'] = ' ORDER BY `position` ASC';

        $this->fields['list'] = [
            'id_disgust_facility_class' => [
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'disgust_facility_class_name'               => [
                'title'  => $this->l('嫌惡設施類別名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'remark'              => [
                'title'  => $this->l('備註'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'            => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'show'                => [
                'title'  => $this->l('篩選顯示'),
                'order'  => true,
                'filter' => 'switch',
                'class'  => 'text-center',
                'values' => [
                    [
                        'class' => 'show_1',
                        'value' => 1,
                        'title' => $this->l('顯示'),
                    ],
                    [
                        'class' => 'show_0',
                        'value' => 0,
                        'title' => $this->l('隱藏'),
                    ],
                ],
            ],
        ];

        $this->fields['form'] = [
            'disgust_facility_class' => [
                'legend' => [
                    'title' => $this->l('嫌惡類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_disgust_facility_class' => [
                        'name'     => 'id_disgust_facility_class',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('類別'),
                        'required' => true,
                    ],
                    'disgust_facility_class_name'  => [
                        'name'      => 'disgust_facility_class_name',
                        'type'      => 'text',
                        'label'     => $this->l('嫌惡設施類別名稱'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],
                    'remark'              => [
                        'name'      => 'remark',
                        'type'      => 'text',
                        'label'     => $this->l('備註'),
                        'maxlength' => '64',
                    ],
                    'position'            => [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                    'show'                => [
                        'type'   => 'switch',
                        'label'  => $this->l('網站篩選'),
                        'name'   => 'show',
                        'val'    => 1,
                        'p'      => $this->l('是否要顯示前台網站篩,隱藏後將不會顯示在前台網站篩選選項內'),
                        'values' => [
                            [
                                'id'    => 'show_1',
                                'value' => 1,
                                'label' => $this->l('顯示'),
                            ],
                            [
                                'id'    => 'show_0',
                                'value' => 0,
                                'label' => $this->l('隱藏'),
                            ],
                        ],
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }

    public function processDel()
    {
//        $id_rent_house_types = sprintf('%d', GetSQL(Tools::getValue('id_disgust_facility_class'), 'int'));
//        $sql                 = 'SELECT `id_disgust_facility_class`
//			FROM `disgust_facility`
//			WHERE `id_object_category` LIKE IN \'%"' . $id_rent_house_types . '"%\'
//			LIMIT 0, 1';
//        $row                 = Db::rowSQL($sql, true);
//        if (count($row))
            $this->_errors[] = $this->l('無法刪除類別!');
//        parent::processDel();
    }
}