<?php



class AdminDailyReturnTypeController extends AdminController
{
    public function __construct()
    {


        $this->className = 'AdminDailyReturnTypeController';
        $this->table = 'daily_return_type';
        $this->fields['index'] = 'id_daily_return_type';
        $this->_as = 'd_r_t';
        $this->fields['title'] = '每日回報類別';

        $this->fields['order'] = ' ORDER BY  d_r_t.`position` ASC ';



//		LEFT 是可以用 一樣有同樣效果，但是取出資料庫迴圈，複雜度會變高，n*n*n 筆搜尋篩選

        $this->fields['list_num'] = 50;

        $this->fields['list'] = [

            'id_daily_return_type' => [
                'filter_key' => 'd_r_t!id_daily_return_type',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],

            'title' => [
                'filter_key' => 'd_r_t!title',
                'title' => $this->l('回報類型'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'remark' => [
                'filter_key' => 'd_r_t!remark',
                'title' => $this->l('備註'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'position' => [
                'filter_key' => 'd_r_t!position',
                'title' => $this->l('順序'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],


        ];


        $this->fields['form'] = [

            'id_daily_return_type' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('回報類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_daily_return_type' => [
                        'name' => 'id_daily_return_type',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'col'       => 9,
                        'index' => true,
                        'required' => true,
                    ],
                    'title' => [
                        'name' => 'title',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '20',
                        'label' => $this->l('回報類型'),
                    ],

                    'remark' => [
                        'name' => 'remark',
                        'type' => 'text',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 9,
                        'maxlength' => '50',
                        'label' => $this->l('備註'),
                    ],

                    'position'           => [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],

                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],

                'cancel' => [
                    'title' => $this->l('取消'),
                ],

                'reset' => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();

    }

}