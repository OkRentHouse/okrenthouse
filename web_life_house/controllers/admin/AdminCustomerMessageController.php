<?php



class AdminCustomerMessageController extends AdminController
{
    public function __construct()
    {
        $this->className = 'AdminCustomerMessageController';
        $this->table = 'customer_message';
        $this->fields['index'] = 'id_customer_message';
//		$this->_as = 'c_m';

        $this->fields['title'] = '留言';

//		$this->_join = ' LEFT JOIN `in_life_activity` AS ila ON ila.`id_in_life_activity` = ilp.`id_in_life_activity`

//                         LEFT JOIN `in_life` AS il ON il.`id_in_life` = ila.`id_in_life`';

//		LEFT 是可以用 一樣有同樣效果，但是取出資料庫迴圈，複雜度會變高，n*n*n 筆搜尋篩選

        $this->fields['list_num'] = 50;

        $this->fields['list'] = [

            'id_customer_message' => [
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],

            'title' => [
                'title' => $this->l('活動標題'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'name' => [
                'title' => $this->l('姓名'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'phone' => [
                'title' => $this->l('手機'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'email' => [
                'title' => $this->l('信箱'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],


            'contact_time' =>[
                'title' => $this->l('聯絡時間'),
                'order' => true,
                'filter'=> 'text',
                'class' => 'text-center', 'values' => [
                    [
                        'class' => 'contact_time_0',
                        'value' => '0',
                        'title' => $this->l('上午'),
                    ],
                    [
                        'class' => 'contact_time_1',
                        'value' => '1',
                        'title' => $this->l('中午'),
                    ],
                    [
                        'class' => 'contact_time_2',
                        'value' => '2',
                        'title' => $this->l('下午'),
                    ],
                    [
                        'class' => 'contact_time_3',
                        'value' => '3',
                        'title' => $this->l('皆可'),
                    ],
                ]
            ],

            'message' => [
                'title' => $this->l('留言'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'schedule' =>[
                'title' => $this->l('進度'),
                'order' => true,
                'filter'=> 'text',
                'class' => 'text-center', 'values' => [
                    [
                        'class' => 'schedule_0',
                        'value' => '0',
                        'title' => $this->l('未處理'),
                    ],
                    [
                        'class' => 'schedule_1',
                        'value' => '1',
                        'title' => $this->l('處理中'),
                    ],
                    [
                        'class' => 'schedule_2',
                        'value' => '2',
                        'title' => $this->l('已結案'),
                    ],
                ]
            ],
        ];



        $this->fields['form'] = [

            'id_customer_message' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('顧客留言'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_customer_message' => [
                        'name' => 'id_customer_message',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                    'title' => [
                        'name' => 'title',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '20',
                        'label' => $this->l('活動標題'),
                        'disabled' => true,
                    ],

                    'phone' => [
                        'name' => 'phone',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '20',
                        'label' => $this->l('手機'),
                        'disabled' => true,
                    ],

                    'tel' => [
                        'name' => 'tel',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '20',
                        'label' => $this->l('市話'),
                        'disabled' => true,
                    ],

                    'email' => [
                        'name' => 'tel',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '128',
                        'label' => $this->l('信箱'),
                        'disabled' => true,
                    ],

                    'sex' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('性別'),
                        'name' => 'sex',
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'sex_0',
                                'value' => 0,
                                'label' => $this->l('女性'),
                            ],
                            [
                                'id' => 'sex_1',
                                'value' => 1,
                                'label' => $this->l('太太'),
                            ],
                            [
                                'id' => 'sex_2',
                                'value' => 2,
                                'label' => $this->l('先生'),
                            ],
                        ],
                        'disabled' => true,
                    ],

                    'contact_time' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('聯絡時間'),
                        'name' => 'contact_time',
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'contact_time_0',
                                'value' => 0,
                                'label' => $this->l('上午'),
                            ],
                            [
                                'id' => 'contact_time_1',
                                'value' => 1,
                                'label' => $this->l('中午'),
                            ],
                            [
                                'id' => 'contact_time_2',
                                'value' => 2,
                                'label' => $this->l('下午'),
                            ],
                            [
                                'id' => 'contact_time_3',
                                'value' => 3,
                                'label' => $this->l('皆可'),
                            ],
                        ],
                        'disabled' => true,
                    ],

                    'message' => [
                        'name' => 'message',
                        'type' => 'textarea',
                        'label' => $this->l('留言'),
                        'rows' =>   '10',
                        'cols'=>'5',
                        'maxlength'=>'100',
                        'disabled' => true,
                    ],

                    'schedule' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('進度'),
                        'name' => 'schedule',
                        'val' => 0,
                        'required' => true,
                        'values' => [
                            [
                                'id' => 'schedule_0',
                                'value' => 0,
                                'label' => $this->l('未處理'),
                            ],
                            [
                                'id' => 'schedule_1',
                                'value' => 1,
                                'label' => $this->l('處理中'),
                            ],
                            [
                                'id' => 'schedule_2',
                                'value' => 2,
                                'label' => $this->l('已結案'),
                            ],
                        ],
                    ],

                    'id_admin'    => [
                        'name'      => 'id_admin',
                        'type'      => 'select',
                        'label_col' => 3,
                        'label'     => $this->l('處理人'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇處理人',
                                'val'  => '',
                            ],

                            'table'   => 'admin',
                            'text'    => 'first_name',
                            'value'   => 'id_admin',
                            'order'   => ' `id_admin` DESC',
                            'where'   => ' AND `active` = 1',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],


                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],

                'cancel' => [
                    'title' => $this->l('取消'),
                ],

                'reset' => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();

    }

}