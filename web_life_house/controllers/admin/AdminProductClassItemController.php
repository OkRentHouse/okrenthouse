<?php

class AdminProductClassItemController extends AdminController
{
    public $no_link = false;

    public function __construct()
    {
        $this->conn = 'ilifehou_life_go';
        $this->className            = 'AdminProductClassItemController';
        $this->table                = 'product_class_item';
        $this->fields['index']      = 'id_product_class_item';
        $this->fields['title']      = '商品類別細項';

        $this->_as = 'p_c_i';


        $this->_join = ' LEFT JOIN `product_class` AS p_c ON p_c.`id_product_class` = p_c_i.`id_product_class`';


//        $this->_group = ' GROUP BY p_c_i.`id_product_class_item`';


        $this->fields['order'] = ' ORDER BY p_c.`position` ASC, p_c_i.`position` ASC';


//        $this->actions[]            = 'list';
        $this->fields['list']       = [
            'id_product_class_item'          => [
                'index'  => true,
                'filter_key' => 'p_c_i!id_product_class_item',
                'title'  => $this->l('商品類別細項ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'product_class_title'    => [
                'filter_key' => 'p_c!title',
                'title'  => $this->l('商品類別名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'title'    => [
                'filter_key' => 'p_c_i!title',
                'title'  => $this->l('商品類別細項名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'     => [
                'filter_key' => 'p_c_i!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'active' => [
                'filter_key' => 'p_c_i!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
        ];



        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('選單'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    [
                        'name'     => 'id_product_class_item',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],

                    'id_county' => [
                        'name'      => 'id_product_class',
                        'type'      => 'select',
                        'label_col' => 3,
                        'label'     => $this->l('選擇商品類別'),
                        'options'   => [
                            'default' => [
                                'text' => '選擇商品類別',
                                'val'  => '',
                            ],
                            'table'   => 'product_class',
                            'text'    => 'title',
                            'value'   => 'id_product_class',
                            'order'   => '`position` ASC',
                        ],
                        'required'  => true,
                    ],
                    [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('商品類別'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],
                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                    [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],

                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],

                        ],
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
        parent::__construct();
    }



    /*
    public function initToolbar(){		//初始化功能按鈕
        if ($this->display == 'add' || $this->display == 'edit') {
            $this->toolbar_btn['save_and_stay'] = array(
                'href' => '#',
                'desc' => $this->l('儲存並繼續編輯')
            );
        }
        parent::initToolbar();
    }
    */
}