<?php
class AdminEproWindowsItemController extends AdminController
{
    public $tpl_folder;    //樣版資料夾
    public $ed;

    public function __construct()
    {
        $this->conn = 'ilifehou_epro';
        $this->className       = 'AdminEproWindowsItemController';
        $this->fields['title'] = '裝修達人百葉窗項目';
        $this->table           = 'epro_windows_item';
        $this->fields['index'] = 'id_epro_windows_item';
        $this->fields['order'] = ' ORDER BY e_w_i.`position` ASC, e_w_i.`id_epro_windows` ASC';
        $this->_as = 'e_w_i';
        $this->_join = ' LEFT JOIN `epro_windows` AS e_w ON e_w.`id_epro_windows` = e_w_i.`id_epro_windows` ';


        $this->fields['list']  = [
            'id_epro_windows_item'         => [
                'index'  => true,
                'hidden' => true,
                'type'   => 'checkbox',
                'label_col' => '2',
                'col' => '3',
                'class'  => 'text-center',
                'filter_key' => 'e_w_i!id_epro_windows_item',
            ],

            'id_epro_windows_title' => [
                'filter_key' => 'e_w_i!id_epro_windows',
                'title' => $this->l('裝修達人百葉窗'),
                'order' => true,
                'filter' => true,
                'multiple' => true,
                'key' => [
                    'table' => 'epro_windows',
                    'key' => 'id_epro_windows',
                    'val' => 'title',
                    'order' => '`position` ASC',
                ],
                'class' => 'text-center',
            ],
            'title'          => [
                'title'  => $this->l('裝修達人百葉窗項目'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w_i!title',
            ],

            'position' => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w!position',
                'filter' => true,
            ],
            'active'         => [
                'title'  => $this->l('啟用'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w!active',
            ],
        ];

        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('裝修達人百葉窗'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_epro_windows_item' => [
                        'name'     => 'id_epro_windows_item',
                        'type'     => 'hidden',
                        'label_col' => '3',
                        'col' => '3',
                        'index'    => true,
                        'required' => true,
                    ],

                    'title'          => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label_col' => '3',
                        'col' => '3',
                        'maxlength' => 32,
                        'label'     => $this->l('title'),
                        'required' => true,
                    ],

                    'id_epro_windows'          => [
                        'name'      => 'id_epro_windows',
                        'type'      => 'select',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'label'     => $this->l('裝修達人百葉窗'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇類型',
                                'val'  => '',
                            ],
                            'table'   => 'epro_windows',
                            'text'    => 'title',
                            'value'   => 'id_epro_windows',
                            'order'   => ' `position` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'active'         => [
                        'type'     => 'switch',
                        'label_col' => '3',
                        'col' => '3',
                        'label'    => $this->l('啟用狀態'),
                        'name'     => 'active',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    'position'           => [
                        'label_col' => '3',
                        'col' => '3',
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay'  => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('重置'),
                ],
            ],

        ];
        parent::__construct();
    }



    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
    }

}