<?php

class AdminLandlordController extends AdminController
{
	public function __construct()
	{
		$this->max = 21;			//最多新增筆數
		$this->className       = 'AdminLandlordController';
		$this->table           = 'landlord';
		$this->fields['index'] = 'id_landlord';
		$this->fields['title'] = '樂租服務 房東篇';
		$this->fields['order'] = ' ORDER BY `position` ASC';

		$this->fields['list_num'] = 50;

		$this->fields['list'] = [
			'id_landlord'      => [
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'title'          => [
				'title'  => $this->l('標題'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'content'        => [
				'title'  => $this->l('內容'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'show'   => false,
			],
			'position'       => [
				'title'  => $this->l('順序'),
				'order'  => true,
				'class'  => 'text-center',
				'filter' => true,
			],
		];

		$this->fields['form'] = [
			'landlord' => [
				'tab'    => 'landlord',
				'legend' => [
					'title' => $this->l('樂租服務 房東篇'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_landlord' => [
						'name'     => 'id_landlord',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'title'     => [
						'name'      => 'title',
						'type'      => 'text',
						'label'     => $this->l('標題'),
						'maxlength' => '50',
						'required'  => true,
						'p' => '若要換行請輸入 "&lt;br&gt;"',
					],
					'content'   => [
						'name'     => 'content',
						'type'     => 'textarea',
						'label'    => $this->l('內容'),
						'class'    => 'tinymce',
						'col'      => 8,
						'rows'     => 20,
					],
					[
						'type'  => 'number',
						'label' => $this->l('位置'),
						'min'   => 0,
						'name'  => 'position',
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function initProcess()
	{
		parent::initProcess();
		if (Tools::isSubmit('submitList' . $this->table) || Tools::isSubmit('submitList' . $this->table . 'AndStay')) {                            //送出新增(執行)
			if ($this->tabAccess['edit']) {
				$this->action  = 'List';
				$this->display = 'list';
			} else {
				$this->_errors[] = $this->l('您沒有修改權限');
			}
		}
		if(($this->display == 'list' || empty($this->display)) && ($this->tabAccess['edit'] && $this->tabAccess['view'])){
			$this->display = 'list';
			$this->fields['form'] = [
				'list_top'     => [
					'tab'    => 'list_top',
					'legend' => [
						'title' => $this->l('樂租服務 房東篇'),
						'icon'  => 'icon-cogs',
						'image' => '',
					],
					'input'  => [
						'Landlord_HTML'    => [
							'name'  => 'Landlord_HTML',
							'type'     => 'textarea',
							'configuration' => true,
							'label' => $this->l('HTML'),
							'label_col' => '12',
							'label_class' => 'text-left',
							'col'      => 12,
							'rows'     => 30,
							'class'    => 'tinymce',
						],
						'Landlord_JS'    => [
							'name'  => 'Landlord_JS',
							'type'     => 'textarea',
							'configuration' => true,
							'label' => $this->l('JS'),
							'label_col' => '12',
							'label_class' => 'text-left',
							'col'      => 12,
							'rows'     => 10,
						],
						'Landlord_CSS'    => [
							'name'  => 'Landlord_CSS',
							'type'     => 'textarea',
							'configuration' => true,
							'label' => $this->l('CSS'),
							'label_col' => '12',
							'label_class' => 'text-left',
							'col'      => 12,
							'rows'     => 10,
						],
					],
					'submit' => [
						[
							'title' => $this->l('儲存'),
						],
					],
					'reset'  => [
						'title' => $this->l('重置'),
					],
				],
			];
			$this->content .= $this->renderForm();    //啟用表單
		}
	}

	public function processList()
	{
		if (count($this->_errors))
			return;
		$this->setFormTable();
		$num = $this->FormTable->updateDB();

		if (Tools::isSubmit('submitList' . $this->table)) {
			$this->back_url = self::$currentIndex . '&conf=2';
		}
		Tools::redirectLink($this->back_url);

		return $num;
	}
}