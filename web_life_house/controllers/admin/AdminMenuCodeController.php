<?php

class AdminMenuCodeController extends AdminController
{
	public $no_link = false;

	public function __construct()
	{

		$this->className       = 'AdminMenuCodeController';
		$this->table           = 'menu_code';
		$this->fields['index'] = 'id_menu_code';
		$this->fields['title'] = '選單Code';
		$this->actions[]       = 'list';
		$where                 = ' AND (`dir` = "web_app" OR `dir` = "web_life_house" OR `dir` = "web_rent")';
		$this->fields['order'] = ' ORDER BY `id_web` ASC, `code` ASC';

		$this->fields['list']  = [
			'id_menu_code' => [
				'index'  => true,
				'type'   => 'checkbox',
				'select' => '',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'id_web'  => [
				'title'  => $this->l('網站'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'key'    => [
					'table' => 'web2',
					'key'   => 'id_web',
					'val'   => 'web',
					'where' => $where,
					'order' => '`web` ASC',
				],
			],
			'code'    => [
				'title'  => $this->l('Code'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['list_num'] = 2;

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('選單'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'     => 'id_menu_code',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'id_web' => [
						'name'     => 'id_web',
						'type'     => 'select',
						'label'    => $this->l('網站'),
						'val'      => $this->id_web,
						'options'  => [
							'default' => [
								'text' => '請選擇網站',
								'val'  => 'web',
							],
							'table'   => 'web2',
							'where'   => $where,
							'text'    => 'web',
							'value'   => 'id_web',
							'order'   => '`position` ASC',
						],
						'required' => true,
					],
					'code'   => [
						'name'      => 'code',
						'type'      => 'text',
						'required'  => true,
						'label'     => $this->l('Code'),
						'maxlength' => '20',
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}


	public function processDel()
	{
		if (Tools::getValue('id_group') == 1)
			$this->_errors[] = $this->l('無法刪除最高管理者群組!');
		parent:: processDel();
	}
	/*
	public function initToolbar(){		//初始化功能按鈕
		if ($this->display == 'add' || $this->display == 'edit') {
			$this->toolbar_btn['save_and_stay'] = array(
				'href' => '#',
				'desc' => $this->l('儲存並繼續編輯')
			);
		}
		parent::initToolbar();
	}
	*/
}