<?php

class AdminOKrentPayController extends AdminController
{
    public function __construct()
    {
        $this->conn = 'ilifehou_okrent';
        $this->className       = 'AdminOKrentPayController';
        $this->table           = 'pay';
        $this->fields['index'] = 'id_pay';
        $this->_as = 'p';
        $this->fields['title'] = '支付方式';
        $this->fields['order'] = ' ORDER BY `position` ASC';
        $this->fields['list'] = [
            'id_pay' => [
                'filter_key' => 'p!id_pay',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'name'    => [
                'filter_key' => 'p!name',
                'title'  => $this->l('名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'active' => [
                'filter_key' => 'p!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'position'     => [
                'filter_key' => 'p!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];


        $this->fields['form'] = [
            'pay' => [
                'legend' => [
                    'title' => $this->l('支付方式'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],

                'input'  => [
                    'id_pay' => [
                        'name'     => 'id_pay',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('型態'),
                        'required' => true,
                    ],

                    'name'    => [
                        'name'      => 'name',
                        'type'      => 'text',
                        'label'     => $this->l('名稱'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],

                    'active' => [
                        'type' => 'switch',
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],

                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],

                    'position'    => [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }


//    public function processAdd()
//    {
//        $this->validateRules();
//        if($this->max > 0){
//            if (($this->num >= $this->max)
//                && ($this->max !== null)) {
//                $this->_errors[] = sprintf($this->l('最多只能新增 %d 筆資料!'), $this->max);
//            }
//        }
//        if (count($this->_errors))
//            return;
//        $this->setFormTable();
//        $num = $this->FormTable->insertDB();
//        if ($num) {
//            if (Tools::isSubmit('submitAdd' . $this->table)) {
//                $this->back_url = self::$currentIndex . '&conf=1';
//            }
//            if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
//                if (in_array('edit', $this->post_processing)) {
//                    $this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
//                } else {
//                    $this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=1&' . $this->index . '=' . $this->FormTable->index_id;
//                }
//            }
//            if (Tools::isSubmit('submitAdd' . $this->table . 'AndContinue')) {
//                $this->back_url = self::$currentIndex . '&add' . $this->table . '&conf=1';
//            }
//            Tools::redirectLink($this->back_url);
//        } else {
//            $this->_errors[] = $this->l('沒有新增任何資料!');
//        }
//    }

    public function processDel(){
        $this->_errors[] = $this->l('無法刪除正在使用的群組類別!');
        parent::processDel();
    }

}