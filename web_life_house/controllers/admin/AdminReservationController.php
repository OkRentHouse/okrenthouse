<?php

class AdminReservationController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminReservationController';
		$this->table           = 'reservation';
		$this->fields['index'] = 'id_reservation';
		$this->fields['title'] = '預約賞屋';
		$this->fields['order'] = ' ORDER BY `add_date` DESC';

		$this->fields['list_num'] = 50;

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				if ($_SESSION['id_web']) {
					$where                 = ' AND (`id_web` = ' . $_SESSION['id_web'] . ' OR `id_web` = 0)';
					$this->fields['where'] = ' AND (`id_web` = ' . $_SESSION['id_web'] . ' OR `id_web` = 0)';
				}
			}
		}

		$this->fields['tabs'] = [
			'reservation' => $this->l('預約留言'),
			'house' => $this->l('物件資料'),
		];

		$this->fields['list'] = [
			'id_reservation' => [
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'id_web'         => [
				'title'  => $this->l('加盟店名稱'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table' => 'web',
					'key'   => 'id_web',
					'val'   => 'web',
					'where' => $where,
					'order' => '`web` ASC',
				],
				'class'  => 'text-center',
			],
			'process'        => [
				'title'  => $this->l('狀態'),
				'order'  => true,
				'filter' => true,
				'values' => [
					[
						'class' => 'process_0',
						'value' => 0,
						'title' => $this->l('未處理'),
					],
					[
						'class' => 'process_1',
						'value' => 1,
						'title' => $this->l('待處理'),
					],
					[
						'class' => 'process_2',
						'value' => 2,
						'title' => $this->l('處理中'),
					],
					[
						'class' => 'process_3',
						'value' => 3,
						'title' => $this->l('已完成'),
					],
				],
				'class'  => 'text-center',
			],
			'name'           => [
				'title'  => $this->l('姓名'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'gender'         => [
				'title'  => $this->l('性別'),
				'order'  => true,
				'filter' => true,
				'values' => [
					[
						'class' => 'gender_1',
						'value' => 1,
						'title' => $this->l('男'),
					],
					[
						'class' => 'gender_0',
						'value' => 0,
						'title' => $this->l('女'),
					],
				],
				'class'  => 'text-center',
			],
			'phone'          => [
				'title'  => $this->l('手機'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'tel'            => [
				'title'  => $this->l('電話'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'email'          => [
				'title'  => $this->l('email'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'time_type'      => [
				'title'  => $this->l('聯絡時段'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'time_type_1',
						'value' => 1,
						'title' => $this->l('上午'),
					],
					[
						'class' => 'time_type_2',
						'value' => 2,
						'title' => $this->l('下午'),
					],
					[
						'class' => 'time_type_3',
						'value' => 3,
						'title' => $this->l('晚上'),
					],
					[
						'class' => 'time_type_4',
						'value' => 4,
						'title' => $this->l('皆可'),
					],
				],
				'show'   => false,
			],
			'message'        => [
				'title'  => $this->l('留言'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'add_date'       => [
				'title'  => $this->l('留言時間'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'r_date'         => [
				'title'  => $this->l('預約時間'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'r_time_type'    => [
				'title'  => $this->l('預約時段'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'time_type_1',
						'value' => 1,
						'title' => $this->l('上午'),
					],
					[
						'class' => 'time_type_2',
						'value' => 2,
						'title' => $this->l('下午'),
					],
					[
						'class' => 'time_type_3',
						'value' => 3,
						'title' => $this->l('晚上'),
					],
					[
						'class' => 'time_type_4',
						'value' => 4,
						'title' => $this->l('皆可'),
					],
				],
			],
			'id_rent_house'  => [
				'title'  => $this->l('物件編號'),
				'order'  => true,
				'filter' => 'text',
				'class'  => 'text-center',
				'key'    => [
					'table' => 'rent_house',
					'key'   => 'id_rent_house',
					'val'   => 'rent_house_code',
				],
			],
			'id_member'      => [
				'title'  => $this->l('留言會員'),
				'order'  => true,
				'filter' => 'text',
				'class'  => 'text-center',
				'key'    => [
					'table' => 'member',
					'key'   => 'id_member',
					'val'   => 'name',
				],
			],
			'id_admin'       => [
				'title'  => $this->l('專員'),
				'order'  => true,
				'filter' => 'text',
				'class'  => 'text-center',
				'show'   => false,
				'key'    => [
					'table' => 'admin',
					'key'   => 'id_admin',
					'val'   => ['last_name', 'first_name'],
				],
			],
			'id_member2'     => [
				'title'  => $this->l('房東'),
				'order'  => true,
				'filter' => 'text',
				'class'  => 'text-center',
				'show'   => false,
				'key'    => [
					'table' => 'member',
					'key'   => 'id_member',
					'val'   => 'name',
				],
			],
		];

		//todo
		$this->fields['form'] = [
			'reservation' => [
				'tab'    => 'reservation',
				'legend' => [
					'title' => $this->l('預約留言'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_reservation' => [
						'name'     => 'id_reservation',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'id_rent_house'  => [
						'name'      => 'id_rent_house',
						'label'     => $this->l('物件編號'),
						'label_col' => 4,
						'form_col'  => 6,
						'col'       => 6,
						'options'   => [
							'table' => 'rent_house',
							'text'  => 'rent_house_code',
							'value' => 'id_rent_house',
						],
					],
					'process'        => [
						'name'      => 'process',
						'type'      => 'checkbox',
						'label'     => $this->l('狀態'),
						'label_col' => 2,
						'form_col'  => 6,
						'col'       => 6,
						'values'    => [
							[
								'id'    => 'process_0',
								'value' => 0,
								'label' => $this->l('未處理'),
							],
							[
								'id'    => 'process_1',
								'value' => 1,
								'label' => $this->l('待處理'),
							],
							[
								'id'    => 'process_2',
								'value' => 2,
								'label' => $this->l('處理中'),
							],
							[
								'id'    => 'process_3',
								'value' => 3,
								'label' => $this->l('已完成'),
							],
						],
					],
					'id_web'         => [
						'name'      => 'id_web',
						'type'      => 'view',
						'label'     => $this->l('加盟店名稱'),
						'label_col' => 2,
						'val'       => $_SESSION['id_web'],
						'options'   => [
							'table' => 'web',
							'text'  => 'web',
							'value' => 'id_web',
							'where' => $where,
							'order' => '`web` ASC',
						],
					],
					'name'           => [
						'name'      => 'name',
						'type'      => 'text',
						'label'     => $this->l('姓名'),
						'label_col' => 2,
						'is_prefix' => true,
					],
					'gender'         => [
						'name'      => 'gender',
						'type'      => 'switch',
						'prefix'    => $this->l('性別'),
						'no_label'  => true,
						'label_col' => 2,
						'values'    => [
							[
								'id'    => 'gender_1',
								'value' => 1,
								'label' => $this->l('男'),
							],
							[
								'id'    => 'gender_0',
								'value' => 0,
								'label' => $this->l('女'),
							],
						],
						'required'  => true,
						'is_suffix' => true,
					],
					'phone'          => [
						'name'      => 'phone',
						'type'      => 'text',
						'label'     => $this->l('手機'),
						'label_col' => 4,
						'form_col'  => 6,
						'col'       => 6,
						'required'  => true,
					],
					'tel'            => [
						'name'      => 'tel',
						'type'      => 'text',
						'label'     => $this->l('電話'),
						'label_col' => 2,
						'form_col'  => 6,
						'col'       => 6,
					],
					'email'          => [
						'name'      => 'tel',
						'type'      => 'text',
						'label'     => $this->l('email'),
						'label_col' => 4,
						'form_col'  => 6,
						'col'       => 6,
					],
					'time_type'      => [
						'name'      => 'time_type',
						'type'      => 'switch',
						'label'     => $this->l('聯絡時段'),
						'label_col' => 2,
						'form_col'  => 6,
						'col'       => 6,
						'values'    => [
							[
								'id'    => 'time_type_1',
								'value' => 1,
								'label' => $this->l('上午'),
							],
							[
								'id'    => 'time_type_2',
								'value' => 2,
								'label' => $this->l('上午'),
							],
							[
								'id'    => 'time_type_3',
								'value' => 3,
								'label' => $this->l('晚上'),
							],
							[
								'id'    => 'time_type_4',
								'value' => 4,
								'label' => $this->l('皆可'),
							],
						],
						'required'  => true,
					],
					'message'        => [
						'name'      => 'message',
						'type'      => 'textarea',
						'label'     => $this->l('留言'),
						'label_col' => 2,
						'maxlength' => 100,
						'required'  => true,
						'col'       => 8,
						'rows'      => 20,
					],
					'add_date'       => [
						'name'          => 'add_date',
						'auto_datetime' => 'add',
						'label'         => $this->l('留言時間'),
						'val'           => today(),
						'label_col'     => 4,
						'form_col'      => 6,
						'col'           => 6,
					],
					'r_date'         => [
						'name'      => 'r_date',
						'label'     => $this->l('預約時間'),
						'label_col' => 2,
						'is_prefix' => true,
					],
					'r_time_type'    => [
						'name'      => 'r_time_type',
						'type'      => 'switch',
						'prefix'    => $this->l('-'),
						'label_col' => 2,
						'values'    => [
							[
								'id'    => 'r_time_type_1',
								'value' => 1,
								'label' => $this->l('上午'),
							],
							[
								'id'    => 'r_time_type_2',
								'value' => 2,
								'label' => $this->l('上午'),
							],
							[
								'id'    => 'r_time_type_3',
								'value' => 3,
								'label' => $this->l('晚上'),
							],
							[
								'id'    => 'r_time_type_4',
								'value' => 4,
								'label' => $this->l('皆可'),
							],
						],
						'required'  => true,
						'is_suffix' => true,
					],
					'id_member'      => [
						'name'      => 'id_member',
						'type'      => 'text',
						'label_col' => 2,
						'label'     => $this->l('留言會員'),
					],
					'id_admin'       => [
						'name'      => 'id_admin',
						'label'     => $this->l('專員'),
						'type'      => 'text',
						'label_col' => 2,
						'options'   => [
							'table' => 'admin',
							'text'  => ['last_name', 'first_name'],
							'value' => 'id_admin',
						],
					],
					'id_member2'     => [
						'name'      => 'id_member2',
						'type'      => 'text',
						'label'     => $this->l('房東'),
						'label_col' => 2,
						'options'   => [
							'table' => 'admin',
							'text'  => 'user',
							'value' => 'id_member',
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
			'house' => [
				'tab'    => 'house',
				'legend' => [
					'title' => $this->l('物件資料'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_rent_house' => [
						'name'     => 'id_rent_house',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],

		];
		parent::__construct();
		foreach ($this->actions as $i => $v) {
			if ($v == 'add') {
				unset($this->actions[$i]);
			}
		}
		foreach ($this->actions as $i => $v) {
			if ($v == 'edit') {
				unset($this->actions[$i]);
			}
		}
		$this->tabAccess['add']  = 0;
		$this->tabAccess['edit'] = false;
	}
}