<?php


class AdminRentHouseCommunityController extends AdminController
{
    public function __construct()
    {

        $this->className = 'AdminRentHouseCommunityController';

        $this->table = 'rent_house_community';

        $this->fields['index'] = 'id_rent_house_community';

        $this->_as = 'r_h_c';

        $this->_join = ' LEFT JOIN `disgust_facility` AS d_f ON d_f.`id_disgust_facility` = r_h_c.`id_disgust_facility`
                         LEFT JOIN `disgust_facility_class` AS d_f_c ON d_f_c.`id_disgust_facility_class` = d_f.`id_disgust_facility_class`
                         LEFT JOIN `public_utilities` AS p_u ON p_u.`id_public_utilities` = r_h_c.`id_public_utilities`
                         LEFT JOIN `county` AS cy ON cy.`id_county` = r_h_c.`id_county`
                         LEFT JOIN `city` AS ci ON ci.`id_city` = r_h_c.`id_city`
                         ';


        $this->fields['title'] = '社區管理';

//        $this->_join = '';

//        $this->_group = ' GROUP BY r_h.`id_rent_house`';

        $this->fields['order'] = ' ORDER BY r_h_c.`position` ASC, r_h_c.`id_rent_house_community` ASC';

        $this->fields['list_num'] = 50;

        $this->fields['list'] = [

            'id_rent_house_community' => [
                'filter_key' => 'r_h_c!id_rent_house_community',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],

            'rent_house_community_code' => [
                'filter_key' => 'r_h_c!rent_house_community_code',
                'title' => $this->l('社區流水號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'community' => [
                'filter_key' => 'r_h_c!community',
                'title' => $this->l('社區名稱'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'property_management' => [
                'filter_key' => 'r_h_c!property_management',
                'title' => $this->l('物管公司'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'phone_fax' => [
                'title' => $this->l('社區電話/傳真'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                'filter_sql'    => 'CONCAT(r_h_c.`community_phone`,"/",r_h_c.`community_fax`)',
            ],

            'builders' => [
                'filter_key' => 'r_h_c!builders',
                'title' => $this->l('建商'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'redraw_area' => [
                'filter_key' => 'r_h_c!redraw_area',
                'title' => $this->l('計劃/重劃區'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

//            'public_utilities' => [
//                'filter_key' => 'r_h_c!public_utilities',
//                'title' => $this->l('公共設施'),
//                'order' => true,
//                'filter' => true,
//                'class' => 'text-center',
//            ],

            'all_num' => [
                'filter_key' => 'r_h_c!all_num',
                'title' => $this->l('總戶數'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'household_num' => [
                'filter_key' => 'r_h_c!household_num',
                'title' => $this->l('住家戶數'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'storefront_num' => [
                'filter_key' => 'r_h_c!storefront_num',
                'title' => $this->l('店面戶數'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'public_utilities_title' => [
                'filter_key' => 'p_u!title',
                'title' => $this->l('公共設施'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'time_s_e' => [
                'title' => $this->l('物管公司時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                'filter_sql'    => 'CONCAT(r_h_c.`time_s`,"~",r_h_c.`time_e`)',
            ],

            'clean_time' => [
                'filter_key' => 'r_h_c!clean_time ',
                'title' => $this->l('清潔隊收取時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'security_system' => [
                'filter_key' => 'r_h_c!security_system',
                'title' => $this->l('保全系統'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'park' => [
                'filter_key' => 'r_h_c!park',
                'title' => $this->l('公園'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'bus' => [
                'filter_key' => 'r_h_c!bus',
                'title' => $this->l('公車'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'bus_station' => [
                'filter_key' => 'r_h_c!bus_station',
                'title' => $this->l('公車站牌'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'passenger_transport' => [
                'filter_key' => 'r_h_c!passenger_transport',
                'title' => $this->l('客運'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'passenger_transport_station' => [
                'filter_key' => 'r_h_c!passenger_transport_station',
                'title' => $this->l('客運站牌'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'train' => [
                'filter_key' => 'r_h_c!train',
                'title' => $this->l('火車'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'mrt' => [
                'filter_key' => 'r_h_c!mrt',
                'title' => $this->l('捷運'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'mrt_station' => [
                'filter_key' => 'r_h_c!mrt_station',
                'title' => $this->l('捷運站牌'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'high_speed_rail' => [
                'filter_key' => 'r_h_c!high_speed_rail',
                'title' => $this->l('高鐵'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'interchange' => [
                'filter_key' => 'r_h_c!interchange',
                'title' => $this->l('交流道'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'e_school' => [
                'filter_key' => 'r_h_c!e_school',
                'title' => $this->l('國小'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'j_school' => [
                'filter_key' => 'r_h_c!j_school',
                'title' => $this->l('國中'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'market' => [
                'filter_key' => 'r_h_c!market',
                'title' => $this->l('傳統市場'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'night_market' => [
                'filter_key' => 'r_h_c!night_market',
                'title' => $this->l('夜市'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'supermarket' => [
                'filter_key' => 'r_h_c!supermarket',
                'title' => $this->l('超商/超市'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'shopping_center' => [
                'filter_key' => 'r_h_c!shopping_center',
                'title' => $this->l('購物中心'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'hospital' => [
                'filter_key' => 'r_h_c!hospital',
                'title' => $this->l('醫院'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'position'        => [
                'filter_key' => 'r_h_c!position',
                'title' => $this->l('位置'),
                'order' => true,
                'class' => 'text-center',
            ],
        ];

        $sql = 'SELECT * FROM disgust_facility as d_f
        LEFT JOIN `disgust_facility_class` AS d_f_c ON d_f_c.`id_disgust_facility_class` = d_f.`id_disgust_facility_class`
        ORDER BY d_f_c.`position`,d_f.`position`,d_f.`id_disgust_facility` ASC ';

        $disgust_facility_arr = Db::rowSQL($sql);
        foreach($disgust_facility_arr as $value){
            $disgust_facility[] = [
                'id' => 'disgust_facility_'.$value['id_disgust_facility'],
                'value' => $value['id_disgust_facility'],
                'label' => $this->l($value['disgust_facility_class_name'].'-'.$value['disgust_facility_name'])
            ];
        }

        $this->fields['tabs'] = [
            'letter' => $this->l('社區管理'),
        ];

        $sql = 'SELECT *,p_u_c.`title` as p_u_c_title,p_u.`title` as title,p_u.`id_public_utilities` as id_public_utilities FROM public_utilities as p_u
                LEFT JOIN `public_utilities_class` as p_u_c ON p_u_c.`id_public_utilities_class` = p_u.`id_public_utilities_class`
                ORDER BY p_u.`position` ASC';
        $public_utilities_arr = Db::rowSQL($sql);

        foreach($public_utilities_arr as $value){
            $public_utilities[] = [
                'id' => 'public_utilities_'.$value['id_public_utilities'],
                'value' => $value['id_public_utilities'],
                'class' =>"public_utilities",
                'label' => $this->l($value['p_u_c_title'].'-'.$value['title'])
            ];
        }

        $this->fields['form'] = [

            'id_rent_house_community' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('社區管理'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [

                    'id_rent_house_community' => [
                        'name' => 'id_rent_house_community',
                        'type' => 'hidden',
                        'label' =>'流水號',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'rent_house_community_code'=>[
                        'name' => 'rent_house_community_code',
                        'type' => 'view',
                        'label_col' => 3,
                        'required' => true,
                    ],

                    'community' => [
                        'name' => 'community',
                        'type' => 'text',
                        'label' => $this->l('社區名稱'),
                        'maxlength' => '20',
                        'required' => true,
                    ],

////                    'building_number' => [
////                        'name' => 'building_number',
////                        'type' => 'text',
////                        'label' => $this->l('區名I棟號'),
////                        'maxlength' => '20',
////                    ],

                    'all_num' => [
                        'name' => 'all_num',
                        'type' => 'text',
                        'maxlength' =>'20',
                        'class' => 'community_data',
                        'label' => $this->l('總戶數'),
                    ],

                    'id_county'          => [
                        'name'      => 'id_county',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label'     => $this->l('縣市'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇縣市',
                                'val'  => '',
                            ],
                            'table'   => 'county',
                            'text'    => 'county_name',
                            'value'   => 'id_county',
                            'order'   => ' `id_county` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'id_city' => [
                        'name'      => 'id_city',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label'     => $this->l('市鄉鎮'),
                        'options'   => [
                            'default'     => [
                                'text' => '請選擇市鄉鎮',
                                'val'  => '',
                            ],
                            //自訂值
                            'parent'      => 'id_county',            //關聯鍵名稱要跟SQL資料表一樣
                            'table'       => 'city',
                            'text'        => 'city_name',
                            'value'       => 'id_city',
                            'order'       => ' `id_city` ASC',
                        ],
                        'required'  => true,
                    ],

                    'road' => [
                        'name' => 'road',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('道/路'),
                        'maxlength' => '20',
                    ],

                    'segment' => [
                        'name' => 'segment',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label' => $this->l('段'),
                        'maxlength' => '20',
                    ],

                    'lane' => [
                        'id'   =>'lane',
                        'name' => 'lane',
                        'type' => 'number',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('剩餘地址資訊'),
                        'suffix' => $this->l('巷'),
                        'is_prefix' => true,
                    ],

                    'alley' => [
                        'id' => 'alley',
                        'name' => 'alley',
                        'type' => 'number',
                        'suffix' => $this->l('弄'),
                        'is_suffix' => true,
                    ],

                    'no' => [
                        'id'   =>'no',
                        'name' => 'no',
                        'type' => 'number',
                        'form_col'  => 3,
                        'no_label'  =>true,
                        'col'       => 12,
                        'suffix' => $this->l('號'),
                        'is_prefix' => true,
                    ],

                    'no_her' => [
                        'id' => 'no_her',
                        'name' => 'no_her',
                        'type' => 'number',
                        'prefix' => $this->l('之'),
                        'is_suffix' => true,
                    ],

                    'floor' => [
                        'id'   =>'floor',
                        'name' => 'floor',
                        'type' => 'number',
                        'form_col'  => 3,
                        'no_label'  =>true,
                        'col'       => 12,
                        'suffix' => $this->l('樓'),
                        'is_prefix' => true,
                    ],

                    'floor_her' => [
                        'id' => 'floor_her',
                        'name' => 'floor_her',
                        'type' => 'number',
                        'prefix' => $this->l('之'),
                        'is_suffix' => true,
                    ],

                    'part_address' => [
                        'name' => 'part_address',
                        'label_col' => 3,
                        'col' => 9,
                        'label' => $this->l('部分地址(由前面欄位組成)'),
                        'type' => 'text',
                        'disabled'=>true,
                    ],

                    'complete_address' => [
                        'name' => 'complete_address',
                        'label_col' => 3,
                        'col' => 9,
                        'label' => $this->l('完整地址(外部匯入或地址組成)'),
                        'type' => 'text',
                        'disabled'=>true,
                    ],


                    'id_exterior_wall'          => [
                        'name'      => 'id_exterior_wall',
                        'type'      => 'select',
//                        'form_col'  => 6,
//                        'label_col' => 6,
//                        'col'       => 6,
                        'label'     => $this->l('外牆材質'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇材質',
                                'val'  => '',
                            ],
                            'table'   => 'exterior_wall',
                            'text'    => 'exterior_wall',
                            'value'   => 'id_exterior_wall',
                            'order'   => ' `position` ASC',
                        ],
//                        'required'  => true,
                        'no_active' => true,
                    ],

                    'household_num' => [
                        'id'   => 'household_num',
                        'name' => 'household_num',
                        'type' => 'number',
                        'class' => 'community_data',
                        'label' => $this->l('戶數'),
                        'prefix' => $this->l('住家'),
                        'is_prefix' => true,
                    ],

                    'storefront_num' => [
                        'id'   => 'storefront_num',
                        'name' => 'storefront_num',
                        'type' => 'number',
                        'class' => 'community_data',
                        'prefix' => $this->l('店面'),
                        'is_suffix' => true,
                    ],

                    'property_management' => [
                        'name' => 'property_management',
                        'type' => 'text',
                        'label' => $this->l('物管公司'),
                        'maxlength' => '20',
                    ],

                    'community_phone' => [
                        'id'   => 'community_phone',
                        'name' => 'community_phone',
                        'type' => 'text',
                        'label' => $this->l('社區電話/傳真'),
                        'prefix' => $this->l('電話'),

                    ],

                    'community_fax' => [
                        'id'   => 'community_fax',
                        'name' => 'community_fax',
                        'type' => 'text',
                        'prefix' => $this->l('傳真'),

                    ],

                    'builders' => [
                        'name' => 'builders',
                        'type' => 'text',
                        'label' => $this->l('建商'),
                        'maxlength'=>'20'
                    ],

                    'redraw_area' => [
                        'label' => $this->l('計劃/重劃區'),
                        'name' => 'redraw_area',
                        'type' => 'text',
                        'maxlongth' =>'20',
                    ],

                    'clean_time'=>[
                        'id'   => 'clean_time',
                        'name' => 'clean_time',
                        'type' => 'text',
                        'label' => $this->l('清潔隊收取時間'),
                        'maxlongth' =>100,
                    ],

                    'time_s' => [
                        'id'   => 'time_s',
                        'name' => 'time_s',
                        'type' => 'date',
                        'label' => $this->l('物管公司時間'),
                        'is_prefix' => true,
                    ],

                    'time_e' => [
                        'id'   =>  'time_e',
                        'name' => 'time_e',
                        'type' => 'date',
                        'prefix' => $this->l('~'),
                        'is_suffix' => true,
                    ],

//                    'public_utilities' =>[
//                        'id' => 'public_utilities',
//                        'name' => 'public_utilities',
//                        'type' => 'textarea',
//                        'label' => $this->l('公共設施'),
//                        'rows' =>   '10',
//                        'cols'=>'5',
//                    ],

                    'id_public_utilities' =>[
                        'type' => 'checkbox',
                        'label' => $this->l('公共設施(複選)'),
                        'name' => 'id_public_utilities',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $public_utilities,
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],


                    'e_school' => [
                        'name' => 'e_school',
                        'type' => 'text',
                        'label'=> $this->l('附近國小'),
                        'maxlength'  => '20',
                    ],

                    'j_school' => [
                        'name' => 'j_school',
                        'type' => 'text',
                        'label'=> $this->l('附近國中'),
                        'maxlength'  => '20',
                    ],

                    'park' => [
                        'name' => 'park',
                        'type' => 'text',
                        'label' => $this->l('公園'),
                        'maxlength' => '20',
                    ],

                    'market' => [
                        'name' => 'market',
                        'type' => 'text',
                        'label' => $this->l('傳統市場'),
                        'maxlength' => '20',
                    ],

                    'night_market' => [
                        'name' => 'night_market',
                        'type' => 'text',
                        'label' => $this->l('夜市'),
                        'maxlength' => '20',
                    ],

                    'supermarket' => [
                        'name' => 'supermarket',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('超商/超市'),
                        'maxlength' => '20',
                    ],

                    'shopping_center' => [
                        'name' => 'shopping_center',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('購物中心/百貨公司'),
                        'maxlength' => '20',
                    ],

                    'hospital' => [
                        'name' => 'hospital',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('醫院'),
                        'maxlength' => '64',
                    ],

                    'bus' => [
                        'name' => 'bus',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('公車'),
                        'maxlength' => '20',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'bus_station' => [
                        'name' => 'bus_station',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('公車站牌'),
                        'maxlength' => '64',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'passenger_transport' => [
                        'name' => 'passenger_transport',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('客運'),
                        'maxlength' => '20',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'passenger_transport_station' => [
                        'name' => 'passenger_transport_station',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('客運站牌'),
                        'maxlength' => '64',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'train' => [
                        'name' => 'train',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('火車'),
                        'maxlength' => '20',
                    ],

                    'mrt' => [
                        'name' => 'mrt',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('捷運'),
                        'maxlength' => '20',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'mrt_station' => [
                        'name' => 'mrt_station',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('捷運站牌'),
                        'maxlength' => '64',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],

                    'high_speed_rail' => [
                        'name' => 'high_speed_rail',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('高鐵'),
                        'maxlength' => '20',
                    ],

                    'interchange' => [
                        'name' => 'interchange',
                        'type' => 'text',
                        'class' => 'community_data',
                        'label' => $this->l('交流道'),
                        'maxlength' => '20',
                    ],

//                    'id_disgust_facility' => [
//                        'type' => 'checkbox',
//                        'label' => $this->l('嫌惡設施(複選)'),
//                        'name' => 'id_disgust_facility',
//                        'in_table' => true,
//                        'multiple' => true,
//                        'values' => $disgust_facility,
//                        'form_col' => 12,
//                        'label_col' => 3,
//                        'col' => 9,
//                    ],

                    [
                        'type'  => 'number',
                        'label' => $this->l('位置'),
                        'min'   => 0,
                        'name'  => 'position',
                    ],
                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset' => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }

    public function initProcess()//處理上下架問題
    {
        $complete_address = Tools::getValue('complete_address');
        $part_address = Tools::getValue('part_address');
        $id_county = Tools::getValue('id_county');
        $id_city = Tools::getValue('id_city');
        $road = Tools::getValue('road');
        $segment = Tools::getValue('segment');
        $lane = Tools::getValue('lane');
        $alley = Tools::getValue('alley');
        $no = Tools::getValue('no');
        $no_her = Tools::getValue('no_her');
        $floor = Tools::getValue('floor');
        $floor_her = Tools::getValue('floor_her');
        $submitAddrent_house_community = Tools::getValue('submitAddrent_house_community');//新增
        $submitEditrent_house_community = Tools::getValue('submitEditrent_house_community');//修改才有差
        $id_rent_house_community = Tools::getValue('id_rent_house_community');
//        $rent_house_community_code = Tools::getValue('rent_house_community_code');    //不會有
        $complete_address_arr = array(
            "county_name"=>"",
            "city_name"=>"",
            "road"=>"",
            "segment"=>"段",
            "lane"=>"巷",
            "alley"=>"弄",
            "no"=>"號",
            "no_her"=>"之",
            "floor"=>"樓",
            "floor_her"=>"之");//由於之是放在後面所以會上一個if處理否則一律後面

        $complete_address = '';//重置 雖然不用重置也沒差就是
        $part_address = '';//重置

        $sql = "SELECT * FROM `county` WHERE `id_county` =".GetSQL($id_county,"int");
        $row = Db::rowSQL($sql,true); //主要要取出county_name
        $county_name = $row["county_name"];//縣市名稱
        $sql = "SELECT * FROM `city` WHERE id_city=".GetSQL($id_city,"int");
        $row_city = Db::rowSQL($sql,true); //主要要取出city_name
        $city_name = $row_city["city_name"];//市鄉政
        foreach($complete_address_arr as $k => $i){
            if(!empty($$k)){
                if($i=="之"){
                    $complete_address .= $i.$$k;
                }else{
                    $complete_address .= $$k.$i;
                }
                if($k=="county_name" || $k=="city_name" || $k=="village" || $k=="neighbor" ||
                    $k=="road" || $k=="segment" || $k=="lane"){ //部分地址會到巷
                    $part_address .= $$k.$i;
                }
            }
        }
        $_POST['complete_address'] = $complete_address;
        $_POST['part_address'] = $part_address;
        ///這邊做部分地址的經緯度判斷
        $google_map = "";
        if(!empty($id_rent_house_community)){//不存在 大概就是新增階段
            $sql = "SELECT part_address,longitude,latitude FROM rent_house_community WHERE id_rent_house=".GetSQL($id_rent_house_community,"int");
            $google_map = Db::rowSQL($sql,true); //主要要取出
        }
        if(($part_address != Tools::getValue('complete_address')) || empty($google_map["longitude"]) || empty($google_map["latitude"])){
            //原本的部分地址與現在的不符或是經緯度不存在

            $google_api_location_json = Google_api::get_lat_and_long($part_address);//取得一大包data
            $api_arr = Google_api::get_data_lat_and_long($google_api_location_json);
            //下面由於這邊只有一個所以用0呼叫
            $_POST["longitude"] = $api_arr["0"]["lng"];
            $_POST["latitude"] = $api_arr["0"]["lat"];
        }



        //這邊要搜尋該id 是否有code沒有則建立有則跳過


        if(!empty($submitAddrent_house_community) || !empty($submitEditrent_house_community)){//新增 //說白了送出才會是"" 因為disabled的關係
//        rent_house_code 內部自編ID 之後要寫
            $sql = "SELECT * FROM rent_house_community WHERE id_rent_house_community".GetSQL($id_rent_house_community,"int");
            $rent_house_community_arr = Db::rowSQL($sql,true);//取得該資料

            if(empty($rent_house_community_arr["rent_house_community_code"])){

            }

            $rent_house_community_code = "";
            while ($rent_house_community_code==""){
                $code = 'C'.date('Ymd').rand(111111,999999);
                $sql = "SELECT count(*) as count FROM rent_house_community WHERE rent_house_community_code=".GetSQL($code,"text");
                $sql_count = Db::rowSQL($sql,true);//避免重複
                if($sql_count["count"] =='0'){//就是該編碼沒有被使用過
                    $rent_house_community_code = $code;
                }
            }
            $_POST['rent_house_community_code'] = $rent_house_community_code;
        }



        parent::initProcess();
    }

    public function setMedia()
    {
        parent::setMedia();
    }
}
