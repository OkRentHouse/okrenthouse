<?php

class AdminRentHouseTypeController extends AdminController
{
	public function __construct()
	{
		$this->className         = 'AdminRentHouseTypeController';
		$this->table             = 'rent_house_type';
		$this->fields['index']   = 'id_rent_house_type';
		$this->fields['title']   = '物件類別';
		$this->fields['order']   = ' ORDER BY `position` ASC';
        $this->_as = 'r_h_t';
        $this->_join = ' LEFT JOIN `rent_house_type_file` AS r_h_t_f ON r_h_t_f.`id_rent_house_type` = r_h_t.`id_rent_house_type` ';
		$this->fields['no_del']  = [1, 2, 3, 4];
//		$this->fields['no_edit'] = [1, 2, 3, 4];
		$this->fields['list']    = [
			'id_rent_house_type' => [
                'filter_key' => 'r_h_t!id_rent_house_type',
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'title'              => [
                'filter_key' => 'r_h_t!title',
				'title'  => $this->l('類別名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'remark'             => [
                'filter_key' => 'r_h_t!remark',
				'title'  => $this->l('備註'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
            'img' => [
                'title' => $this->l('縮圖'),
                'order' => true,
                'filter' => true,
                'filter_sql' => 'IF(r_h_t_f.`id_rhtf` > 0, 1, 0)',
                'class' => 'text-center', 'values' => [
                    [
                        'class' => 'img_1',
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'img_0 red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],
			'position'           => [
                'filter_key' => 'r_h_t!position',
				'title'  => $this->l('順序'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'show'                => [
				'title'  => $this->l('篩選顯示'),
				'order'  => true,
				'filter' => 'r_h_t!switch',
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'show_1',
						'value' => 1,
						'title' => $this->l('顯示'),
					],
					[
						'class' => 'show_0',
						'value' => 0,
						'title' => $this->l('隱藏'),
					],
				],
			],
			'active'             => [
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

        $this->arr_file_type = [
            '0' => 'img'
        ];

		$this->fields['form'] = [
			'rent_house_type' => [
				'legend' => [
					'title' => $this->l('物件類別'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_rent_house_type' => [
						'name'     => 'id_rent_house_type',
						'type'     => 'hidden',
						'index'    => true,
						'label'    => $this->l('類別'),
						'required' => true,
					],
					'rent_house_type'    => [
						'name'      => 'title',
						'type'      => 'text',
						'label'     => $this->l('類別名稱'),
						'maxlength' => '20',
						'required'  => true,
					],
                    'id_main_house_class'   => [
                        'name'      => 'id_main_house_class',
                        'type'      => 'select',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'label'     => $this->l('主要房屋類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇主要類別',
                                'val'  => '',
                            ],
                            'table'   => 'main_house_class',
                            'text'    => 'title',
                            'value'   => 'id_main_house_class',
                            'order'   => ' `id_main_house_class` ASC',
                        ],
//                        'required'  => true,
                        'no_active' => true,
                    ],
					'remark'             => [
						'name'      => 'remark',
						'type'      => 'text',
						'label'     => $this->l('備註'),
						'maxlength' => '50',
					],
					'position'           => [
						'name'  => 'position',
						'type'  => 'number',
						'label' => $this->l('順序'),
					],
					'show'               => [
						'type'   => 'switch',
						'label'  => $this->l('網站篩選'),
						'name'   => 'show',
						'val'    => 1,
						'p'      => $this->l('是否要顯示前台網站篩,隱藏後將不會顯示在前台網站篩選選項內'),
						'values' => [
							[
								'id'    => 'show_1',
								'value' => 1,
								'label' => $this->l('顯示'),
							],
							[
								'id'    => 'show_0',
								'value' => 0,
								'label' => $this->l('隱藏'),
							],
						],
					],
					'active'             => [
						'type'   => 'switch',
						'label'  => $this->l('啟用狀態'),
						'name'   => 'active',
						'val'    => 1,
						'p'      => $this->l('啟用的型態會顯示在網站上'),
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],

            'img' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('小圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 80,
                            'max_height' => 80,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 80*80px'),
                    ],
                ],
            ],
		];

		parent::__construct();
	}

	public function processDel()
	{
		$id_rent_house_type = sprintf('%d', GetSQL(Tools::getValue('id_rent_house_type'), 'int'));
		$sql                = 'SELECT `id_rent_house`
			FROM `rent_house`
			WHERE `object_type` LIKE IN \'%"' . $id_rent_house_type . '"%\'
			LIMIT 0, 1';
		$row                = Db::rowSQL($sql, true);
		if (count($row)){
			$this->_errors[] = $this->l('無法刪除正在使用的型態!');
		}
		parent::processDel();
	}


    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);

        $this->arr_file_type = [
            '0' => 'img',
        ];

        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_rent_house_type = $UpFileVal['id_rent_house_type'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `rent_house_type_file` (`id_rent_house_type`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_rent_house_type, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_rent_house_type = $UpFileVal['id_rent_house_type'];

        list($dir, $url) = str_dir_change($id_rent_house_type);     //切割資料夾
        $dir = WEB_DIR . DS . RENT_HOUSE_TYPE_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_rent_house_type = $UpFileVal['id_rent_house_type'];
        list($dir, $url) = str_dir_change($id_rent_house_type);
        $url = RENT_HOUSE_TYPE_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_rent_house_type)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `rent_house_type_file`
				WHERE `id_rent_house_type` = %d',
            GetSQL($id_rent_house_type, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_rent_house_type = Tools::getValue('id_rent_house_type');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_rent_house_type' => $id_rent_house_type,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_rent_house_type);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $del_table = "rent_house_type_file";
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/RentHouseType?&id_file=' . $v['id_rent_house_type'] . '&type_name=' . $type_name . '&table=' . $del_table .  '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }



}