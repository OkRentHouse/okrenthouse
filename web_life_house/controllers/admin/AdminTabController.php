<?php

class AdminTabController extends AdminController
{
	public $no_link = false;

	public function __construct()
	{
		if (Tools::getValue('id_tab')) {
			$where = sprintf(' AND `id_tab` != %d', getSQL(Tools::getValue('id_tab'), 'int'));
		}

		$this->className            = 'AdminTabController';
		$this->table                = 'tab';
		$this->fields['index']      = 'id_tab';
		$this->fields['title']      = '選單';
		$this->fields['title_key']  = 'title';
		$this->fields['parent_key'] = 'id_parent';
		$this->actions[]            = 'list';
		$this->fields['list']       = [
			'id_tab'          => [
				'index'  => true,
				'type'   => 'checkbox',
				'select' => '',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'icon'            => [
				'title' => $this->l('圖示'),
				'type'  => 'icon',
				'order' => true,
				'class' => 'text-center',
			],
			'title'           => [
				'title'  => $this->l('選單名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'controller_name' => [
				'title'  => $this->l('Controller名稱 / Module名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'module'          => [
				'title'  => $this->l('Controller / Module'),
				'order'  => true,
				'filter' => true,
				'values' => [
					[
						'class' => 'module_0',
						'value' => 0,
						'title' => $this->l('Controller'),
					],
					[
						'class' => 'module_1',
						'value' => 1,
						'title' => $this->l('Module'),
					],
				],
				'class'  => 'text-center',
			],
			'parameter'       => [
				'title'  => $this->l('參數'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'hint'            => [
				'title'  => $this->l('提示'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'active'          => [
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'position'        => [
				'title' => $this->l('位置'),
				'order' => true,
				'class' => 'text-center',
			],
		];

		$this->fields['list_num'] = 2;

        $id_diversion = [];
        $sql = 'SELECT * FROM diversion WHERE active=1 ORDER BY `position` ASC';
        $public_utilities_arr = Db::rowSQL($sql);
        foreach($public_utilities_arr as $value){
            $id_diversion[] = [
                'id' => 'diversion_'.$value['id_diversion'],
                'value' => $value['id_diversion'],
                'label' => $this->l($value['name'])
            ];
        }

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('選單'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'     => 'id_tab',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					[
						'name'    => 'id_parent',
						'type'    => 'select',
						'label'   => $this->l('上層'),
						'options' => [
							'table'   => 'tab',
							'text'    => 'title',
							'value'   => 'id_tab',
							'where'   => ' AND (`id_parent` IS NULL OR`id_parent` = 0)' . $where,
							'order'   => '`position` ASC',
							'default' => [
								'val'  => null,
								'text' => $this->l('主頁'),
							],
						],
					],
					[
						'type'        => 'text',
						'required'    => true,
						'label'       => $this->l('選單名稱'),
						'placeholder' => $this->l('請輸入'),
						'name'        => 'title',
						'maxlength'   => '50',
					],
					[
						'type'        => 'text',
						'required'    => true,
						'label'       => $this->l('Controller名稱 / Module名稱'),
						'placeholder' => $this->l('請輸入提示Controller / Module名稱'),
						'name'        => 'controller_name',
						'maxlength'   => '50',
					],
					[
						'type'   => 'switch',
						'label'  => $this->l('Controller / Module'),
						'name'   => 'module',
						'val'    => 0,
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 0,
								'label' => $this->l('Controller'),
							],
							[
								'id'    => 'active_off',
								'value' => 1,
								'label' => $this->l('Module'),
							],
						],
					],

                    'id_diversion' =>[
                        'type' => 'checkbox',
                        'label' => $this->l('分流'),
                        'name' => 'id_diversion',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $id_diversion,
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],
                    [
						'type'        => 'text',
						'label'       => $this->l('圖示'),
						'placeholder' => $this->l('請輸入圖示'),
						'name'        => 'icon',
						'maxlength'   => '50',
					],
					[
						'type'        => 'text',
						'label'       => $this->l('提示'),
						'placeholder' => $this->l('請輸入提示'),
						'name'        => 'hint',
						'maxlength'   => '100',
					],
					[
						'type'        => 'text',
						'label'       => $this->l('參數'),
						'placeholder' => $this->l('請輸入參數'),
						'hint'        => $this->l('網址後面的參數'),
						'name'        => 'parameter',
						'maxlength'   => '300',
					],
					[
						'type'  => 'number',
						'label' => $this->l('位置'),
						'min'   => 0,
						'name'  => 'position',
					],
					[
						'type'   => 'switch',
						'label'  => $this->l('啟用狀態'),
						'name'   => 'active',
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}


	public function processDel()
	{
		if (Tools::getValue('id_group') == 1)
			$this->_errors[] = $this->l('無法刪除最高管理者群組!');
		parent:: processDel();
	}
	/*
	public function initToolbar(){		//初始化功能按鈕
		if ($this->display == 'add' || $this->display == 'edit') {
			$this->toolbar_btn['save_and_stay'] = array(
				'href' => '#',
				'desc' => $this->l('儲存並繼續編輯')
			);
		}
		parent::initToolbar();
	}
	*/
}