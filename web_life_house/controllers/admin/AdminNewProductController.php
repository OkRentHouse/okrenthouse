<?php

class AdminNewProductController extends AdminController
{
    public $stay = false;
    public $page = 'newproduct';

    public $arr_file_type = [
        '0' => 'img',
        '1' => 'web_carousel',
        '2' => 'app_img',
        '3' => 'app_carousel',
        '4' => 'building_power',
        '5' => 'land_power',
        '6' => 'land_registration',
        '7' => 'shop_product_image',
    ];

    public function __construct()
    {
        // $months = Tools::getValue('rent_houseFilter_submit_date');
        // if($months ==''){
        //     $months = "1";
        // }
        $this->conn = 'ilifehou_shopping';
        $this->className = 'AdminNewProductController';
        $this->fields['title'] = '商品新增';
        $this->table           = 'product_Shopping';
        $this->fields['index'] = 'ProductID';
        $this->fields['order'] = ' ORDER BY p_.`ProductID` ASC';
        $this->_as = 'p_';
        //$this->_join = ' LEFT JOIN `epro_windows_file` AS e_w_f ON e_w_f.`id_epro_windows` = e_w.`id_epro_windows` ';


        $this->fields['list_num'] = 50; //每頁出現筆數

        // if ((Tools::getValue('id_admin') == $_SESSION['id_admin']) && $_SESSION['admin'] != 1 && $_SESSION['id_group'] != 1) {
        // 	$this->stay = true;
        // }

        $this->fields['list']  = [

            'ProductID' => [
                'filter_key' => 'p_!ProductID',
                'index'  => true,
                'title' => $this->l('商品ID'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'Product_Name' => [
                'filter_key' => 'p_!Product_Name',
                'title' => $this->l('商品名稱'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'Product_Main_Category' => [
                'filter_key' => 'p_!Product_Main_Category',
                'title' => $this->l('主類別'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'Product_Price' => [
                'filter_key' => 'p_!Product_Price',
                'title' => $this->l('定價'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

        ];

        $this->fields['form'] = [

            'produce_up' => [
                'legend' => [
                    'title' => $this->l('上架'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'ProductID' => [
                        'name' => 'ProductID',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                    'Product_Ontime' => [
                        'name' => 'Product_Ontime',
                        'label' => $this->l('立即上架'),
                        'type' => 'date',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'required' => true,
                    ],

                    'Product_PreDate' => [
                        'name' => 'Product_PreDate',
                        'label' => $this->l('預約上架'),
                        'type' => 'date',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                        'required' => true,
                    ],

                    'Product_Period' => [
                        'name' => 'Product_Period',
                        'label' => $this->l('刊登期限'),
                        'type' => 'date',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'required' => true,
                    ],
                    'ProductType' => [
                        'name' => 'ProductType',
                        'type' => 'checkbox',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                        'label' => $this->l('精選'),
                        'values' => [
                            [
                                'id' => 'ProductType',
                                'value' => '0',
                                'label' => $this->l('買賣'),
                            ],
                            [
                                'id' => 'ProductType',
                                'value' => '1',
                                'label' => $this->l('租'),
                            ],
                            [
                                'id' => 'ProductType',
                                'value' => '2',
                                'label' => $this->l('交換'),
                            ],
                            [
                                'id' => 'ProductType',
                                'value' => '3',
                                'label' => $this->l('捐贈'),
                            ],
                        ],
                        'required' => true,
                    ],
                    'Product_Main_Category'          => [
                        'name'      => 'Product_Main_Category',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label'     => $this->l('主類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇主類別',
                                'val'  => '',
                            ],
                            'table'   => 'product_class',
                            'text'    => 'title',
                            'value'   => 'id_product_class',
                            'order'   => ' `id_product_class` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],
                    'Product_Sub_Category'          => [
                        'name'      => 'Product_Sub_Category',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label'     => $this->l('子類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇子類別',
                                'val'  => '',
                            ],
                            'table'   => 'product_class_item',
                            'text'    => 'title',
                            'value'   => 'id_product_class_item',
                            'order'   => ' `id_product_class` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],
                    'Product_Number' => [
                        'name' => 'Product_Number',
                        'label' => $this->l('商品代碼'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'required' => true,
                    ],
                    'Product_S_Number' => [
                        'name' => 'Product_S_Number',
                        'label' => $this->l('賣家自訂編號'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                        'required' => true,
                    ],
                    'Product_OrderDay' => [
                        'name' => 'Product_OrderDay',
                        'type' => 'checkbox',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'multiple' => true,
                        'label' => $this->l('備貨時間'),
                        'values' => [
                            [
                                'id' => 'Product_OrderDay_0',
                                'value' => 0,
                                'label' => $this->l('3天內'),
                            ],
                            [
                                'id' => 'Product_OrderDay_1',
                                'value' => 1,
                                'label' => $this->l('3-7天'),
                            ],
                            [
                                'id' => 'Product_OrderDay_2',
                                'value' => 2,
                                'label' => $this->l('7-10天'),
                            ],
                        ],
                        'required' => true,
                    ],
                    'Product_Location'          => [
                        'name'      => 'Product_Location',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'col_class'     =>'no_padding_right width_control',
                        'class'     => 'part_address_add',
                        'label'     => $this->l('地址'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇縣市',
                                'val'  => '',
                            ],
                            'table'   => 'county',
                            'text'    => 'county_name',
                            'value'   => 'id_county',
                            'order'   => ' `id_county` ASC',
                        ],
                    ],
                    'recording'=>[
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/product_size.tpl',
                        'label' => $this->l('規格'),
                        'no_action'=>true,
                        'name' => 'cars',
                        'form_col' =>12,
                        'label_col' =>3,
                        'col'=>9,
                    ],
                    'Product_Name' => [
                        'name' => 'Product_Name',
                        'label' => $this->l('商品名稱'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'required' => true,
                    ],
                    'Product_Label' => [
                        'name' => 'Product_Label',
                        'label' => $this->l('標籤'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                    ],
                    'Product_Brand' => [
                        'name' => 'Product_Brand',
                        'label' => $this->l('品牌'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],
                    'Product_Video' => [
                        'name' => 'Product_Video',
                        'label' => $this->l('影片'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                    ],
                    'Product_Price' => [
                        'name' => 'Product_Price',
                        'label' => $this->l('定價'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],
                    'Product_Discount' => [
                        'name' => 'Product_Discount',
                        'label' => $this->l('優惠價'),
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                    ],
                    'product_discount_tpl'=>[
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/product_discount.tpl',
                        'label' => $this->l('多件優惠'),
                        'no_action'=>true,
                        'name' => 'cars',
                        'form_col' =>12,
                        'label_col' =>3,
                        'col'=>9,
                    ],
                    'Product_Long' => [
                        'id'   =>'Product_Long',
                        'name' => 'Product_Long',
                        'type' => 'text',
                        'col_class'=>'no_padding_right',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col'  => 6,
                        'label' => $this->l('長寬高重量'),
                        'suffix' => $this->l('長'),
                        'is_prefix' => true,
                    ],

                    'Product_Width' => [
                        'id' => 'Product_Width',
                        'name' => 'Product_Width',
                        'type' => 'text',
                        'suffix' => $this->l('寬'),
                        'is_suffix' => true,
                    ],

                    'Product_Height' => [
                        'id' => 'Product_Height',
                        'name' => 'Product_Height',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col' => 12,
                        'suffix' => $this->l('高'),
                        'no_label' => true,
                    ],
                    'Product_Weight' => [
                        'id' => 'Product_Weight',
                        'name' => 'Product_Weight',
                        'type' => 'text',
                        'col_class'=>'no_padding',
                        'form_col' => 2,
                        'col' => 12,
                        'suffix' => $this->l('重量'),
                        'no_label' => true,
                    ],
                    'Product_Payment' => [
                        'name' => 'Product_Payment',
                        'label' => $this->l('收款方式'),
                        'type' => 'checkbox',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                        'values' => [
                            [
                                'id' => 'Product_Payment',
                                'value' => '0',
                                'label' => $this->l('ATM轉帳、Famiport、輕鬆付帳戶餘額'),
                            ],
                            [
                                'id' => 'Product_Payment',
                                'value' => '1',
                                'label' => $this->l('貨到付款'),
                            ],
                            [
                                'id' => 'Product_Payment',
                                'value' => '2',
                                'label' => $this->l('接口支付'),
                            ],
                            [
                                'id' => 'Product_Payment',
                                'value' => '3',
                                'label' => $this->l('信用卡一次付清'),
                            ],
                            // [
                            //     'id' => 'Product_Payment',
                            //     'value' => '4',
                            //     'label' => $this->l('信用卡分期付款'),
                            // ],
                        ],
                        'required' => true,
					],
					'Product_ShipWay' => [
                        'name' => 'Product_ShipWay',
                        'label' => $this->l('運送方式'),
                        'type' => 'checkbox',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                        'values' => [
                            [
                                'id' => 'Product_Payment',
                                'value' => '0',
                                'label' => $this->l('7-11'),
                            ],
                            [
                                'id' => 'Product_Payment',
                                'value' => '1',
                                'label' => $this->l('Family'),
                            ],
                            [
                                'id' => 'Product_Payment',
                                'value' => '2',
                                'label' => $this->l('OK'),
                            ],
                            [
                                'id' => 'Product_Payment',
                                'value' => '3',
                                'label' => $this->l('萊爾富'),
                            ],
                            [
                                'id' => 'Product_Payment',
                                'value' => '4',
                                'label' => $this->l('宅配'),
							],
							[
                                'id' => 'Product_Payment',
                                'value' => '5',
                                'label' => $this->l('貨到付款'),
                            ],
                        ],
                        'required' => true,
                    ],
                    'product_details'                => [
                        'name'      => 'product_details',
                        'type'      => 'textarea',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'     => $this->l('特色'),
                        'class'     => 'tinymce',
                        'required'  => true,
                        'cols'      => 5,
                        'rows'      => 10,
                    ],
                    'shop_product_image' => [
                        'name' => 'shop_product_image',
                        'label_class' => 'text-left',
                        'label' => $this->l('商品圖片'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 5,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 800,
                            'min_width' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],

                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 800*600px 或寬度最少超過600px'),
                    ],
                ],
                
                
            
            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],
            'reset' => [
                'title' => $this->l('復原'),
            ],
        ],
        ];
        


        parent::__construct();
    }

    public function initToolbar()
    {        //初始化功能按鈕
        parent::initToolbar();
        if ($this->stay) {
            unset($this->toolbar_btn['save']);
        }
    }

    public function initProcess()
    {
        //規格 tpl 回傳出來要做的
        $db_link = mysqli_connect("localhost", "ilifehou_ilife", "S^,HO%bh^$&NF3dab", "ilifehou_shopping");
        $ProductID = Tools::getValue('ProductID');
        if ($_POST['upd'] == "2") {
            $sql_delete = "DELETE FROM product_size where productID='".$ProductID."'";
            mysqli_query($db_link, $sql_delete);
            foreach ($_POST["size"] as $i => $val) {
                if ($_POST['size'][$i] != '' || $_POST['options'][$i] !='') {
                    $sql_insert = "INSERT INTO product_size (productID,product_size,product_optine) 
					VALUES
					('".$ProductID."','".$_POST['size'][$i]."','".$_POST['options'][$i]."')";
                    mysqli_query($db_link, $sql_insert);
                }
            }
        } elseif ($_POST['upd'] == "1") {
            foreach ($_POST["size"] as $h => $val11) {
                $sql_update = "UPDATE product_size SET product_size='".$_POST['size'][$h]."', product_optine='".$_POST['options'][$h]."' 
				where productID='".$ProductID."'";
                mysqli_query($db_link, $sql_update);
            }
        }

        $sql_update = "Select * from product_size where productID='".$ProductID ."'";
        $result = mysqli_query($db_link, $sql_update);
        $res = array();
        while ($row = mysqli_fetch_array($result)) {
            array_push($res, array(
                "product_size"=>$row['product_size'],
                "product_optine"=>$row['product_optine'],
                ));
        }

        //多件優惠 tpl 回傳出來要做的
        if ($_POST['upd2'] =="2") {
            $sql_delete = "DELETE FROM product_discount where ProductID='".$ProductID."'";
            mysqli_query($db_link, $sql_delete);
            foreach ($_POST["product_small"] as $i => $val) {
				if($_POST["product_small"][$i] !=""){
					$sql_insert = "INSERT INTO product_discount (ProductID,product_small,product_big,product_each_price) 
					VALUES
					('".$ProductID."','".$_POST['product_small'][$i]."','".$_POST['product_big'][$i]."','".$_POST['product_each_price'][$i]."')";
                mysqli_query($db_link, $sql_insert);
				}
                
            }
        }
        
        $sql_update = "Select * from product_discount where productID='".$ProductID ."'";
        $result = mysqli_query($db_link, $sql_update);
        $res2 = array();
        while ($row = mysqli_fetch_array($result)) {
            array_push($res2, array(
                "product_small"=>$row['product_small'],
                "product_big"=>$row['product_big'],
                "product_each_price"=>$row['product_each_price'],
                ));
        }
        
        
        
        
        
        $this->context->smarty->assign([
            'catch_history' => $res,
            'product_discount1' => $res2,
        ]);

        parent::initProcess();
    }

    public function processEdit()
    {
        echo "processEdit";
        parent::processEdit();
    }

    public function processAdd()
    {
        echo "processAdd";
        parent::processAdd();
        echo "processAdd";
    }

    public function processDel()
    {
        echo "processDel";
        parent::processDel();
    }

    public function processSave()
    {
        echo "processSave";
        parent::processSave();
    }

    public function validateRules()
    {
        parent::validateRules();
    }

    public function processSetMyFieldList()
    {
        parent::processSetMyFieldList();
    }

    public function processdelMyFieldList()
    {
        parent::processdelMyFieldList();
    }
    
    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);


        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_rent_house = $UpFileVal['id_rent_house'];

        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf(
                'INSERT INTO `rent_house_file` (`id_rent_house`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_rent_house, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql, false, 0, '');


            if (!Db::getContext()->num) {
                return false;
            }

            if ($type=='0' || $type=='1') {
                $sql = "SELECT  CONCAT(file_dir,filename) img_url FROM file WHERE id_file=".GetSQL($id_file, "int");
                $data_img = Db::rowSQL($sql, true);//取得檔案位置
                $after_img = urldecode($data_img["img_url"]);
                Watermark::get_watermark($after_img, WEB_DIR.DS.'img'.DS.'water'.DS.'water.png', $after_img);
            }
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_rent_house = $UpFileVal['id_rent_house'];

        list($dir, $url) = str_dir_change($id_rent_house);     //切割資料夾
        $dir = WEB_DIR . DS . RENT_HOUSE_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_rent_house = $UpFileVal['id_rent_house'];
        list($dir, $url) = str_dir_change($id_rent_house);
        $url = RENT_HOUSE_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_rent_house)
    {
        $sql = sprintf(
            'SELECT `id_file`, `file_type`
				FROM `rent_house_file`
				WHERE `id_rent_house` = %d',
            GetSQL($id_rent_house, 'int')
        );
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_rent_house = Tools::getValue('id_rent_house');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_rent_house' => $id_rent_house,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_rent_house);
        $arr_initialPreview = [];
        $arr_initialPreviewConfig =[];
        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }

                    $del_table = "rent_house_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/RentHouse?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];//url後面作補充使她能夠正確刪除資料
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
}
