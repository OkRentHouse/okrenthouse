<?php

class AdminPublicUtilitiesController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminPublicUtilitiesController';
        $this->table           = 'public_utilities';
        $this->fields['index'] = 'id_public_utilities';
        $this->fields['title'] = '公共設備類別';
        $this->fields['order'] = ' ORDER BY p_u.`position` ASC';
        $this->_as = 'p_u';
        $this->_join = ' LEFT JOIN `public_utilities_class` AS p_u_c ON p_u_c.`id_public_utilities_class` = p_u.`id_public_utilities_class` ';

        $this->fields['list'] = [
            'id_public_utilities' => [
                'filter_key' => 'p_u!id_public_utilities',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'title_main'    => [
                'filter_key' => 'p_u_c!title',
                'title'  => $this->l('公共設備主類別'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'title'    => [
                'filter_key' => 'p_u!title',
                'title'  => $this->l('公共設備類別'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'     => [
                'filter_key' => 'p_u!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->arr_file_type = [
            '0' => 'img'
        ];

        $this->fields['form'] = [
            'id_public_utilities_class' => [
                'legend' => [
                    'title' => $this->l('公共設備類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_public_utilities' => [
                        'name'     => 'id_public_utilities',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('公共設備'),
                        'required' => true,
                    ],
                    'id_public_utilities_class'          => [
                        'name'      => 'id_public_utilities_class',
                        'type'      => 'select',
                        'label'     => $this->l('公共設備主類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇公共設備主類別',
                                'val'  => '',
                            ],
                            'table'   => 'public_utilities_class',
                            'text'    => 'title',
                            'value'   => 'id_public_utilities_class',
                            'order'   => ' `position` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],
                    'title'    => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('設備類別'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],
                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }

    public function processDel(){
        $id_public_utilities = Tools::getValue('id_public_utilities');
        $sql = sprintf('SELECT `id_public_utilities`
			FROM `public_utilities`
			WHERE `id_public_utilities` = %d
			LIMIT 0, 1',
            GetSQL($id_public_utilities, 'int'));

        $row = Db::rowSQL($sql, true);
        if(count($row)) $this->_errors[] = $this->l('無法刪除正在使用的類別!');
        parent::processDel();
    }

    public function setMedia()
    {
        parent::setMedia();
    }



}