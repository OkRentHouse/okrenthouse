<?php

class AdminWeb2Controller extends AdminController
{
	public $tpl_folder;    //樣版資料夾

	public function __construct()
	{
		$this->className       = 'AdminWeb2Controller';
		$this->table           = 'web2';
        $this->_as = 'w';
		$this->fields['index'] = 'id_web';
        $this->_join = ' LEFT JOIN  web2_file as w_f ON w_f.`id_web` = w.`id_web`';
		$this->fields['where'] = ' AND w.`id_web` > 0';
		$this->fields['order'] = ' ORDER BY w.`web` ASC';


		$this->fields['title'] = '網站管理';
        $this->arr_file_type = [
            '0' => 'img',
        ];
		$this->fields['list'] = [
			'id_web'      => [
                'filter_key' => 'w!id_web',
				'index'  => true,
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'web'         => [
                'filter_key' => 'w!web',
				'title'  => $this->l('網站名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'domain_name' => [
                'filter_key' => 'w!domain_name',
				'title'  => $this->l('Domain Name'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center mw130',
			],
			'dir'         => [
                'filter_key' => 'w!dir',
				'title'  => $this->l('網站資料夾'),
				'order'  => true,
				'class'  => 'text-center',
				'filter' => true,
			],
			'position' => [
                'filter_key' => 'w!position',
				'title'  => $this->l('驗證順序'),
				'order'  => true,
				'class'  => 'text-center',
				'filter' => true,
			],
			'active'      => [
                'filter_key' => 'w!active',
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['list_num'] = 50;


		$this->fields['form']['Web'] = [
			'tab'    => 'Web',
			'legend' => [
				'title' => $this->l('網站管理'),
				'icon'  => 'icon-cogs',
				'image' => '',
			],
			'input'  => [
				'id_web'      => [
					'name'     => 'id_web',
					'type'     => 'hidden',
					'index'    => true,
					'required' => true,
				],
				'web'         => [
					'name'      => 'web',
					'label'     => $this->l('網站名稱'),
					'type'      => 'text',
					'maxlength' => 20,
					'required'  => true,
				],
				'domain_name' => [
					'name'      => 'domain_name',
					'type'      => 'text',
					'required'  => true,
					'unique'    => true,
					'label'     => $this->l('Domain Name'),
					'maxlength' => 100,
				],
				'dir'         => [
					'name'      => 'dir',
					'label'     => $this->l('網站資料夾'),
					'type'      => 'text',
					'maxlength' => 100,
					'required'  => true,
				],
				'switch'      => [
					'type'     => 'switch',
					'label'    => $this->l('啟用狀態'),
					'p'        => $this->l('社區關閉前幾天請公佈關閉公告，告知使用者關閉訊息！'),
					'name'     => 'active',
					'val'      => 1,
					'values'   => [
						[
							'id'    => 'active_1',
							'value' => 1,
							'label' => $this->l('啟用'),
						],
						[
							'id'    => 'active_0',
							'value' => 0,
							'label' => $this->l('關閉'),
						],
					],
					'required' => true,
				],
			],
			'submit' => [
				[
					'title' => $this->l('儲存'),
				],
			],
			'cancel' => [
				'title' => $this->l('取消'),
			],
			'reset'  => [
				'title' => $this->l('重置'),
			],
		];

        $this->fields['form']['img'] =[
            'tab' => 'file',
            'legend' => [
                'title' => $this->l('縮圖'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],

            'input' => [
                'img' => [
                    'name' => 'img',
                    'label_class' => 'text-left',
                    'label' => $this->l('示意圖'),
                    'type' => 'file',
                    'language' => 'zh-TW',

                    'file' => [
                        'icon' => false,
                        'auto_upload' => true,
                        'max' => 1,
                        'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                        'resize' => true,
                        'resizePreference' => 'height',
                    ],

                    'multiple' => true,
                    'col' => 12,
                    'no_action' => true,
                    'p' => $this->l('上傳一張示意圖'),
//                        'p' => $this->l('圖片建議大小 800*600px'),
                ],
            ],
        ];
		parent::__construct();
	}

	public function processDel()
	{
		$id_web = Tools::getValue('id_web');
		$sql    = sprintf('SELECT `id_web`
									FROM `' . DB_PREFIX_ . 'houses`
									WHERE `id_web` = %d
									LIMIT 0, 1',
			GetSQL($id_web, 'int'));
		Db::rowSQL($sql);
		if (Db::getContext()->num()) {
			$this->_errors[] = $this->l('無法刪除正在使用的建案');
		}
		parent::processDel();
	}


    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_web = $UpFileVal['id_web'];

        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `web2_file` (`id_web`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_web, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int'));
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_web = $UpFileVal['id_web'];

        list($dir, $url) = str_dir_change($id_web);     //切割資料夾
        $dir = WEB_DIR . DS . WEB2_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_web = $UpFileVal['id_web'];
        list($dir, $url) = str_dir_change($id_web);
        $url = WEB2_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_web)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `web2_file`
				WHERE `id_web` = %d',
            GetSQL($id_web, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_web = Tools::getValue('id_web');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_web' => $id_web,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_web);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }

                    $del_table = "web2_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/Web2?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];//url後面作補充使她能夠正確刪除資料
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }

}