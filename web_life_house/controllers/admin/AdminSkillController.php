<?php

class AdminSkillController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminSkillController';
        $this->table           = 'skill';
        $this->fields['index'] = 'id_skill';
        $this->fields['title'] = '技能';
//        $this->fields['order'] = ' ORDER BY s.`position` ASC';
        $this->_as = 's';
        $this->_join = ' LEFT JOIN `skill_classification` AS s_c_i ON s_c_i.`id_skill_classification` = s.`id_skill_classification`
        LEFT JOIN `skill_class` AS s_c ON s_c.`id_skill_class` = s_c_i.`id_skill_class` ';
        $this->fields['list_num'] = 50;

        $this->fields['list'] = [
            'id_skill' => [
                'filter_key' => 's!id_skill',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'id_skill_class' => [
                'filter_key' => 's_c_i!id_skill_class',
                'title' => $this->l('技能分類'),
                'order' => true,
                'filter' => true,
                'multiple' => true,
                'key' => [
                    'table' => 'skill_class',
                    'key' => 'id_skill_class',
                    'val' => 'title',
                    'order' => '`position` ASC',
                ],
                'class' => 'text-center',
            ],
            'id_skill_classification' => [
                'filter_key' => 's!id_skill_classification',
                'title' => $this->l('技能分類'),
                'order' => true,
                'filter' => true,
                'multiple' => true,
                'key' => [
                    'table' => 'skill_classification',
                    'key' => 'id_skill_classification',
                    'val' => 'title',
                    'order' => '`position` ASC',
                ],
                'class' => 'text-center',
            ],
//
//            'title_main'    => [
//                'filter_key' => 's_c_i!title',
//                'title'  => $this->l('技能分類'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//            ],
            'title'    => [
                'filter_key' => 's!title',
                'title'  => $this->l('技能'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'active' => [
                'filter_key' => 's!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
//            'position'     => [
//                'filter_key' => 's!position',
//                'title'  => $this->l('順序'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//            ],
        ];

        $this->fields['form'] = [
            'skill' => [
                'legend' => [
                    'title' => $this->l('技能'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_skill' => [
                        'name'     => 'id_skill',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('id'),
                        'required' => true,
                    ],
                    'id_skill_classification'          => [
                        'name'      => 'id_skill_classification',
                        'type'      => 'select',
                        'label'     => $this->l('技能分類'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇技能分類',
                                'val'  => '',
                            ],
                            'table'   => 'skill_classification',
                            'text'    => 'title',
                            'value'   => 'id_skill_classification',
                            'order'   => ' `position` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'title'    => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('技能分類'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],
                    'active' => [
                        'type' => 'switch',
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
//                    [
//                        'name'  => 'position',
//                        'type'  => 'number',
//                        'label' => $this->l('順序'),
//                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],

        ];

        parent::__construct();
    }


    public function setMedia()
    {
        parent::setMedia();
    }

}