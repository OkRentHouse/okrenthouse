<?php

class AdminManageThemesController extends AdminController
{
	public $page = 'managethemes';

	public function __construct()
	{
		$this->className       = 'AdminManageThemesController';
		$this->fields['title'] = '後台管理樣式';

		$arr_manage_themes  = Themes::getContext()->getAllManageThemes();
		$manage_themes_list = [];
		foreach ($arr_manage_themes as $i => $v) {
			if (ADMIN_DEFAULT_THEME == $i) {
				array_unshift(
					$manage_themes_list,
					[
						'id'    => 'mt_' . $i,
						'value' => $i,
						'label' => $this->l($v['title']),
					]
				); //放在第一個
			} else {
				$manage_themes_list[] = [
					'id'    => 'mt_' . $i,
					'value' => $i,
					'label' => $this->l($v['title']),
				];
			}
		}

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('後台樣式'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => LilyHouse::getContext()->web . '_ManageTheme',
						'type'          => 'switch',
						'col'           => 8,
						'label'         => $this->l('後台樣式'),
						'hint'          => $this->l('可以改變後台管理佈景樣式'),
						'required'      => true,
						'configuration' => true,
						'val'           => ADMIN_DEFAULT_THEME,
						'values'        => $manage_themes_list,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'edit';
		parent::initContent();
	}
}