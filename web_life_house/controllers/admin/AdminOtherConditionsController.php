<?php

class AdminOtherConditionsController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminOtherConditionsController';
		$this->table           = 'other_conditions';
		$this->fields['index'] = 'id_other_conditions';
		$this->fields['title'] = '其他條件';
        $this->_as             = 'o_c';
        $this->_join           = ' LEFT JOIN `other_conditions_file` AS o_c_f ON o_c_f.`id_other_conditions` = o_c.`id_other_conditions` ';
		$this->fields['order'] = ' ORDER BY o_c.`position` ASC';
		$this->fields['list'] = [
			'id_other_conditions' => [
				'index'  => true,
                'filter_key' => 'o_c!id_other_conditions',
				'title'  => $this->l('其他條件ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'title'    => [
                'filter_key' => 'o_c!title',
				'title'  => $this->l('其他條件'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
            'remarks'    => [
                'filter_key' => 'o_c!remarks',
                'title'  => $this->l('備註'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
			'position'     => [
                'filter_key' => 'o_c!position',
				'title'  => $this->l('順序'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
            'show'                => [
                'title'  => $this->l('篩選顯示'),
                'order'  => true,
                'filter' => 'o_c!switch',
                'class'  => 'text-center',
                'values' => [
                    [
                        'class' => 'show_1',
                        'value' => 1,
                        'title' => $this->l('顯示'),
                    ],
                    [
                        'class' => 'show_0',
                        'value' => 0,
                        'title' => $this->l('隱藏'),
                    ],
                ],
            ],
            'img'         => [
                'title'      => $this->l('縮圖'),
                'order'      => true,
                'filter'     => true,
                'filter_sql' => 'IF(o_c_f.`id_ocf` > 0, 1, 0)',
                'class'      => 'text-center', 'values' => [
                    [
                        'class' => 'img_1',
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'img_0 red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],
		];

        $this->arr_file_type = [
            '0' => 'img',
        ];


        $sql = 'SELECT * FROM main_house_class';
        $id_main_house_class_arr = Db::rowSQL($sql);

        foreach($id_main_house_class_arr as $value){
            $id_main_house_class[] = [
                'id' => 'main_house_class'.$value['id_main_house_class'],
                'value' => $value['id_main_house_class'],
                'label' => $this->l($value['title'])
            ];
        }

		$this->fields['form'] = [
			'OtherConditions' => [
				'legend' => [
					'title' => $this->l('其他條件'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_other_conditions' => [
						'name'     => 'id_other_conditions',
						'type'     => 'hidden',
						'index'    => true,
						'label'    => $this->l('其他條件ID'),
						'required' => true,
					],
					'title'    => [
						'name'      => 'title',
						'type'      => 'text',
						'label'     => $this->l('其他條件'),
						'maxlength' => '20',
						'required'  => true,
					],
                    'id_main_house_class' => [
                        'type' => 'checkbox',
                        'label' => $this->l('主要房屋類別(複選)'),
                        'name' => 'id_main_house_class',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $id_main_house_class,
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],
                    'remarks'    => [
                        'name'      => 'remarks',
                        'type'      => 'text',
                        'label'     => $this->l('備註(顯示在前台下行)'),
                        'maxlength' => '20',
//                        'required'  => true,
                    ],
					[
						'name'  => 'position',
						'type'  => 'number',
						'label' => $this->l('順序'),
					],
                    'show'                => [
                        'type'   => 'switch',
                        'label'  => $this->l('網站篩選'),
                        'name'   => 'show',
                        'val'    => 1,
                        'p'      => $this->l('是否要顯示前台網站篩,隱藏後將不會顯示在前台網站篩選選項內'),
                        'values' => [
                            [
                                'id'    => 'show_1',
                                'value' => 1,
                                'label' => $this->l('顯示'),
                            ],
                            [
                                'id'    => 'show_0',
                                'value' => 0,
                                'label' => $this->l('隱藏'),
                            ],
                        ],
                    ],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],

            'img' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('搜索小圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 50,
                            'max_height' => 50,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 50*50px'),
                    ],
                ],
            ],
		];

		parent::__construct();
	}

	public function processDel(){
		$id_other_conditions = Tools::getValue('id_other_conditions');
		$sql = sprintf('SELECT oc.`id_other_conditions`
			FROM `other_conditions` AS oc
			INNER `rent_house_conditions` AS rhc ON rhc.`id_other_conditions` = oc.`id_other_conditions`
			WHERE `id_other_conditions` = %d
			LIMIT 0, 1',
		GetSQL($id_other_conditions, 'int'));
		$row = Db::rowSQL($sql, true);
		if(count($row)) $this->_errors[] = $this->l('無法刪除正在使用的條件!');
		parent::processDel();
	}


    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);

        $this->arr_file_type = [
            '0' => 'img'
        ];

        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_other_conditions = $UpFileVal['id_other_conditions'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `other_conditions_file` (`id_other_conditions`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_other_conditions, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_other_conditions = $UpFileVal['id_other_conditions'];

        list($dir, $url) = str_dir_change($id_other_conditions);     //切割資料夾
        $dir = WEB_DIR . DS . Other_Conditions_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_other_conditions = $UpFileVal['id_other_conditions'];
        list($dir, $url) = str_dir_change($id_other_conditions);
        $url = Other_Conditions_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_other_conditions)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `other_conditions_file`
				WHERE `id_other_conditions` = %d',
            GetSQL($id_other_conditions, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_other_conditions = Tools::getValue('id_other_conditions');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_other_conditions' => $id_other_conditions,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_other_conditions);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $del_table = "other_conditions_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/OtherConditions?&id_file=' . $v['id_other_conditions'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }









}