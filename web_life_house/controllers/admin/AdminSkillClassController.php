<?php

class AdminSkillClassController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminSkillClassController';
        $this->table           = 'skill_class';
        $this->fields['index'] = 'id_skill_class';
        $this->_as = 's_c';
//        $this->_join = '  ';

        $this->fields['title'] = '技能類別';
        $this->fields['order'] = ' ORDER BY `position` ASC';
        $this->fields['list'] = [
            'id_skill_class' => [
                'filter_key' => 's_c!id_skill_class',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'title'    => [
                'filter_key' => 's_c!title',
                'title'  => $this->l('技能類別'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'active' => [
                'filter_key' => 's_c!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'position'     => [
                'filter_key' => 's_c!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->arr_file_type = [
            '0' => 'img'
        ];

        $this->fields['form'] = [
            'skill_class' => [
                'legend' => [
                    'title' => $this->l('技能類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_skill_class' => [
                        'name'     => 'id_skill_class',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('類別'),
                        'required' => true,
                    ],
                    'title'    => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('技能類別'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],
                    'active' => [
                        'type' => 'switch',
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
        parent::__construct();
    }

    public function processDel(){
        $this->_errors[] = $this->l('無法刪除正在使用的主類別!');
        parent::processDel();
    }
}