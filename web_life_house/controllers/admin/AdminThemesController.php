<?php

class AdminThemesController extends AdminController
{
	public $page = 'themes';

	public function __construct()
	{
		$this->className       = 'AdminThemesController';
		$this->fields['title'] = '佈景主題';

		$arr_themes  = Themes::getContext()->getAllThemes();
		$themes_list = [];
		foreach ($arr_themes as $i => $v) {
			if (DEFAULT_THEME == $i) {
				array_unshift(
					$themes_list,
					[
						'id'    => 'mt_' . $i,
						'value' => $i,
						'label' => $this->l($v['title']),
					]
				); //放在第一個
			} else {
				$themes_list[] = [
					'id'    => 'mt_' . $i,
					'value' => $i,
					'label' => $this->l($v['title']),
				];
			}
		}

		foreach (LilyHouse::getContext()->arr_web as $host => $arr) {
			$this->fields['form'][$arr['web_dir']] = [
				'legend' => [
					'title' => sprintf($this->l('%s 佈景主題'), $host),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'Theme' => [
						'name'          => $arr['web_dir'] . '_Theme',
						'type'          => 'switch',
						'label'         => $this->l('網站樣式'),
						'hint'          => $this->l('可以改變網站佈景樣式'),
						'required'      => true,
						'configuration' => true,
						'val'           => DEFAULT_THEME,
						'values'        => $themes_list,
						'col'           => 8,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			];
		}

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'edit';
		parent::initContent();
	}
}