<?php

class AdminInformationController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminInformationController';
		$this->table           = 'geely_for_rent';
		$this->fields['index'] = 'id_geely_for_rent';
		$this->_as             = 'gfr';
		$this->fields['title'] = '租事大吉';
		$this->_join           = ' LEFT JOIN `geely_for_rent_file` AS gfrf ON gfrf.`id_gfr` = gfr.`id_geely_for_rent`';
		$this->fields['order'] = ' ORDER BY `top` DESC, `date` DESC, `add_time` DESC';

		$this->fields['list_num'] = 50;

		$this->fields['list'] = [
			'id_geely_for_rent'      => [
				'filter_key' => 'gfr!id_geely_for_rent',
				'index'      => true,
				'title'      => $this->l('ID'),
				'type'       => 'checkbox',
				'hidden'     => true,
				'class'      => 'text-center',
			],
			'top'                    => [
				'filter_key' => 'gfr!top',
				'title'      => $this->l('置頂'),
				'order'      => true,
				'filter'     => true,
				'values'     => [
					[
						'class' => 'top',
						'value' => 1,
						'title' => $this->l('置頂'),
					],
				],
				'class'      => 'text-center',
			],
			'id_geely_for_rent_type' => [
				'filter_key' => 'gfr!id_geely_for_rent_type',
				'title'      => $this->l('類別'),
				'order'      => true,
				'filter'     => true,
				'key'        => [
					'table' => 'geely_for_rent_type',
					'key'   => 'id_geely_for_rent_type',
					'val'   => 'title',
					'order' => '`active` DESC, `position` ASC',
				],
				'class'      => 'text-center',
			],
			'title'                  => [
				'filter_key' => 'gfr!title',
				'title'      => $this->l('標題'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			'author'                 => [
				'filter_key' => 'gfr!author',
				'title'      => $this->l('作者'),
				'order'      => true,
				'filter'     => true,
				'show'       => false,
				'class'      => 'text-center',
			],
			'identity'               => [
				'filter_key' => 'gfr!identity',
				'title'      => $this->l('身份'),
				'order'      => true,
				'filter'     => true,
				'show'       => false,
				'class'      => 'text-center',
			],
			'source'                 => [
				'filter_key' => 'gfr!source',
				'title'      => $this->l('來源'),
				'order'      => true,
				'filter'     => true,
				'show'       => false,
				'class'      => 'text-center',
			],

			'date'        => [
				'filter_key' => 'gfr!date',
				'title'      => $this->l('發佈日期'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			'content'     => [
				'filter_key' => 'gfr!content',
				'title'      => $this->l('內容'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
				'show'       => false,
			],
			'description' => [
				'filter_key' => 'gfr!description',
				'title'      => $this->l('Description'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
				'show'       => false,
			],
			'keywords'    => [
				'filter_key' => 'gfr!keywords',
				'title'      => $this->l('Keywords'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
				'show'       => false,
			],
			'img'         => [
				'title'      => $this->l('縮圖'),
				'order'      => true,
				'filter'     => true,
				'filter_sql' => 'IF(gfrf.`id_gfrf` > 0, 1, 0)',
				'class'      => 'text-center', 'values' => [
					[
						'class' => 'img_1',
						'value' => '1',
						'title' => $this->l('有'),
					],
					[
						'class' => 'img_0 red',
						'value' => '0',
						'title' => $this->l('無'),
					],
				],
			],
			'add_time'    => [
				'filter_key' => 'gfr!add_time',
				'title'      => $this->l('建立時間'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
				'show'       => false,
			],
			'active'      => [
				'filter_key' => 'gfr!active',
				'title'      => $this->l('啟用狀態'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
		];

		$this->fields['form'] = [
			'geely_for_rent' => [
				'tab'    => 'letter',
				'legend' => [
					'title' => $this->l('租事大吉'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_geely_for_rent'      => [
						'name'      => 'id_geely_for_rent',
						'type'      => 'hidden',
						'label_col' => 3,
						'index'     => true,
						'required'  => true,
					],
					'top'                    => [
						'name'      => 'top',
						'type'      => 'checkbox',
						'label_col' => 3,
						'label'     => $this->l('置頂'),
						'values'    => [
							[
								'id'    => 'top',
								'value' => 1,
								'label' => $this->l('置頂'),
							],
						],
					],
					'id_geely_for_rent_type' => [
						'name'      => 'id_geely_for_rent_type',
						'type'      => 'select',
						'label_col' => 3,
						'label'     => $this->l('類別'),
						'options'   => [
							'default' => [
								'text' => '請選擇類別',
								'val'  => '',
							],
							'table'   => 'geely_for_rent_type',
							'text'    => 'title',
							'value'   => 'id_geely_for_rent_type',
							'order'   => '`position` ASC',
						],
						'required'  => true,
						'p' => $this->l('新增類別'),
						'p' => sprintf($this->l('<a href="/manage/GeelyForRentType">%s</a>'), $this->l('新增類別')),
					],
					'title'                  => [
						'name'      => 'title',
						'type'      => 'text',
						'label_col' => 3,
						'label'     => $this->l('標題'),
						'maxlength' => '50',
						'required'  => true,
					],
					'author'                 => [
						'name'      => 'author',
						'type'      => 'text',
						'label_col' => 3,
						'label'     => $this->l('作者'),
						'maxlength' => '20',
					],
					'identity'               => [
						'name'      => 'identity',
						'type'      => 'text',
						'label_col' => 3,
						'label'     => $this->l('身份'),
						'maxlength' => '20',
					],
					'source'                 => [
						'name'      => 'source',
						'type'      => 'text',
						'label_col' => 3,
						'label'     => $this->l('來源'),
						'maxlength' => '50',
					],
					'date'                   => [
						'name'      => 'date',
						'type'      => 'date',
						'label_col' => 3,
						'label'     => $this->l('發佈日期'),
						'val'       => today(),
						'required'  => true,
					],
					'content'                => [
						'name'      => 'content',
						'type'      => 'textarea',
						'label_col' => 3,
						'label'     => $this->l('內容'),
						'class'     => 'tinymce',
						'required'  => true,
						'col'       => 8,
						'rows'      => 30,
					],
					'description'            => [
						'name'      => 'description',
						'type'      => 'text',
						'label_col' => 3,
						'maxlength' => 300,
						'label'     => $this->l('Description'),
					],
					'keywords'               => [
						'name'      => 'keywords',
						'type'      => 'text',
						'label_col' => 3,
						'maxlength' => 300,
						'data'      => ['role' => 'tagsinput'],
						'label'     => $this->l('Keywords'),
					],
					'add_time'               => [
						'name'          => 'add_time',
						'type'          => 'view',
						'label_col'     => 3,
						'auto_datetime' => 'add',
						'label'         => $this->l('建立時間'),
					],
					'views'                  => [
						'name'      => 'views',
						'label'     => $this->l('瀏覽次數'),
						'val'       => 0,
						'type'      => 'view',
						'label_col' => 3,
					],
					'active'                 => [
						'type'      => 'switch',
						'label_col' => 3,
						'label'     => $this->l('啟用狀態'),
						'name'      => 'active',
						'val'       => 1,
						'values'    => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
			'img'            => [
				'tab'    => 'img',
				'legend' => [
					'title' => $this->l('圖片'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'img' => [
						'name'      => 'img',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'             => false,
							'auto_upload'      => true,
							'max'              => 1,
							'allowed'          => ['jpg', 'png', 'gif', 'jpeg'],
							'max_width'        => 317,
							'max_height'       => 240,
							'resize'           => true,
							'resizePreference' => 'height',
						],
						'multiple'  => true,
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 317*240px'),
					],
				],
			],
		];

		parent::__construct();
	}

	public function setMedia()
	{
		$this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
		parent::setMedia();
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
	}

	public function getUpFile($id)
	{
		return GeelyForRent::getFile($id);
	}

	public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
	{
		$json              = new JSON();
		$UpFileVal         = (array)$json->decode($UpFileVal);
		$arr_id_file       = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
		$id_geely_for_rent = $UpFileVal['id_geely_for_rent'];
		foreach ($arr_id_file as $i => $id_file) {
			//建案檔案
			$sql = sprintf('INSERT INTO `geely_for_rent_file` (`id_gfr`, `id_file`) VALUES (%d, %d)',
				GetSQL($id_geely_for_rent, 'int'),
				GetSQL($id_file, 'int')
			);
			Db::rowSQL($sql);
			if (!Db::getContext()->num)
				return false;
		}

		return true;
	}

	public function iniUpFileDir()
	{
		$UpFileVal         = Tools::getValue('UpFileVal');
		$json              = new JSON();
		$UpFileVal         = (array)$json->decode($UpFileVal);
		$id_geely_for_rent = $UpFileVal['id_geely_for_rent'];
		list($dir, $url) = str_dir_change($id_geely_for_rent);
		$dir = WEB_DIR . DS . GEELY_FOR_RENT_IMG_DIR . DS . $dir;
		return $dir;
	}

	public function iniUpFileUrl()
	{
		$UpFileVal         = Tools::getValue('UpFileVal');
		$json              = new JSON();
		$UpFileVal         = (array)$json->decode($UpFileVal);
		$id_geely_for_rent = $UpFileVal['id_geely_for_rent'];
		list($dir, $url) = str_dir_change($id_geely_for_rent);
		$url = GEELY_FOR_RENT_IMG_URL . $url;
		return $url;
	}
}