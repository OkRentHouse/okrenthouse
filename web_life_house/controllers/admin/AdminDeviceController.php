<?php

class AdminDeviceController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminDeviceController';
		$this->table           = 'device';
		$this->fields['index'] = 'id_device';
		$this->fields['title'] = '設備管理';
		$this->fields['order'] = ' ORDER BY `position` ASC';
		$this->fields['list'] = [
			'id_device' => [
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'type'    => [
				'title'  => $this->l('設備型態'),
				'order'  => true,
				'filter' => true,
				'values'     => [
					[
						'class' => 'type_1',
						'value' => 1,
						'title' => $this->l('傢俱'),
					],
					[
						'class' => 'type_2',
						'value' => 2,
						'title' => $this->l('家電'),
					],
				],
				'class'  => 'text-center',
			],
			'id_device_category'    => [
				'title'  => $this->l('設備類別'),
				'order'  => true,
				'filter' => true,
				'key'        => [
					'table' => 'device_category',
					'key'   => 'id_device_category',
					'val'   => 'id_device_category',
					'order' => '`position` ASC',
				],
				'class'  => 'text-center',
			],
			'device'    => [
				'title'  => $this->l('設備名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'unit'    => [
				'title'  => $this->l('設備單位'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'position'     => [
				'title'  => $this->l('順序'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['form'] = [
			'device' => [
				'legend' => [
					'title' => $this->l('設備管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_device' => [
						'name'     => 'id_device',
						'type'     => 'hidden',
						'index'    => true,
						'label'    => $this->l('設備ID'),
						'required' => true,
					],
					'type'    => [
						'name'      => 'type',
						'type'      => 'select',
						'label'     => $this->l('設備型態'),
						'values'    => [
							[
								'id'    => 'type_1',
								'value' => 1,
								'label' => $this->l('傢俱'),
							],
							[
								'id'    => 'type_2',
								'value' => 2,
								'label' => $this->l('家電'),
							],
						],
						'required'  => true,
					],
					'id_device_category'    => [
						'name'      => 'id_device_category',
						'type'      => 'select',
						'label'     => $this->l('設備類別'),
						'options'   => [
							'table'       => 'device_category',
							'text'        => 'device_category',
							'value'       => 'id_device_category',
							'order'       => '`position` ASC',
							'default'     => [
								'val'  => '',
								'text' => $this->l('請選擇設備類別'),
							],
						],
					],
					'device'    => [
						'name'      => 'device',
						'type'      => 'text',
						'label'     => $this->l('設備名稱'),
						'maxlength' => '20',
						'required'  => true,
					],
					[
						'name'  => 'position',
						'type'  => 'number',
						'label' => $this->l('順序'),
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function processDel(){
		$id_device = Tools::getValue('id_device');
		$sql = sprintf('SELECT d.`id_device`
			FROM `device` AS d
			INSERT JOIN `rent_house_device` AS rhd ON rhd.`id_device` = d.`id_device`
			WHERE d.`id_device` = %d
			LIMIT 0, 1',
		GetSQL($id_device, 'int'));
		$row = Db::rowSQL($sql, true);
		if(count($row)) $this->_errors[] = $this->l('無法刪除正在使用的設備 !');
		parent::processDel();
	}
}