<?php



class AdminRentHouseMessageController extends AdminController
{
    public function __construct()
    {


        $this->className = 'AdminRentHouseMessageController';
        $this->table = 'rent_house_message';
        $this->fields['index'] = 'id_rent_house_message';
		$this->_as = 'r_h_m';
        $this->fields['title'] = '樂租留言';

//        $this->_join = '  LEFT JOIN `rent_house` AS r_h ON r_h.`id_rent_house` = r_h_m.`id_rent_house`
//                          LEFT JOIN `member` AS m ON m.`id_member` = r_h.`id_member`
//                          LEFT JOIN `broker` AS b ON b.`id_member` = m.`id_member` ';
//
//        $this->fields['where'] .= " AND  m.`id_member_additional` not LIKE '%3%' AND m.`id_member_additional` not LIKE '%4%' ";
//        //不顯示房東與房仲
//
//        $this->fields['order'] = ' ORDER BY  r_h_m.`id_rent_house_message` ASC ';



//		LEFT 是可以用 一樣有同樣效果，但是取出資料庫迴圈，複雜度會變高，n*n*n 筆搜尋篩選

        $this->fields['list_num'] = 50;

        $this->fields['list'] = [

            'id_rent_house_message' => [
                'filter_key' => 'r_h_m!id_rent_house_message',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],

            'message_type' =>[
                'filter_key' => 'r_h_m!message_type',
                'title' => $this->l('訊息類型'),
                'order' => true,
                'filter'=> true,
                'class' => 'text-center', 'values' => [
                    [
                        'class' => 'message_type_0',
                        'value' => '0',
                        'title' => $this->l('留言'),
                    ],
                    [
                        'class' => 'message_type_1',
                        'value' => '1',
                        'title' => $this->l('預約賞屋'),
                    ],
                    [
                        'class' => 'message_type_2',
                        'value' => '2',
                        'title' => $this->l('樂購顧問'),
                    ],
                ]
            ],

            'name' => [
                'filter_key' => 'r_h_m!name',
                'title' => $this->l('姓名'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'sex' =>[
                'filter_key' => 'r_h_m!sex',
                'title' => $this->l('性別'),
                'order' => true,
                'filter'=> true,
                'values' => [
                    [
                        'class' => 'sex',
                        'value' => 0,
                        'title' => $this->l('小姐'),
                    ],
                    [
                        'class' => 'sex',
                        'value' => 1,
                        'title' => $this->l('太太'),
                    ],
                    [
                        'class' => 'sex',
                        'value' => 2,
                        'title' => $this->l('先生'),
                    ],
                ],
            ],

            'phone' => [
                'filter_key' => 'r_h_m!phone',
                'title' => $this->l('手機'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'tel' => [
                'filter_key' => 'r_h_m!tel',
                'title' => $this->l('市話'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'email' => [
                'filter_key' => 'r_h_m!email',
                'title' => $this->l('信箱'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'appointment_time' => [
                'filter_key' => 'r_h_m!appointment_time',
                'title' => $this->l('預約時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'appointment_period' =>[
                'filter_key' => 'r_h_m!appointment_period',
                'title' => $this->l('預約時段'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'appointment_period',
                        'value' => 0,
                        'title' => $this->l('上午'),
                    ],
                    [
                        'class' => 'appointment_period',
                        'value' => 1,
                        'title' => $this->l('中午'),
                    ],
                    [
                        'class' => 'appointment_period',
                        'value' => 2,
                        'title' => $this->l('下午'),
                    ],
                    [
                        'class' => 'appointment_period',
                        'value' => 3,
                        'title' => $this->l('皆可'),
                    ],
                ],
            ],

            'contact_time' =>[
                'filter_key' => 'r_h_m!contact_time',
                'title' => $this->l('聯絡時間'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'contact_time',
                        'value' => 0,
                        'title' => $this->l('上午'),
                    ],
                    [
                        'class' => 'contact_time',
                        'value' => 1,
                        'title' => $this->l('中午'),
                    ],
                    [
                        'class' => 'contact_time',
                        'value' => 2,
                        'title' => $this->l('下午'),
                    ],
                    [
                        'class' => 'contact_time',
                        'value' => 3,
                        'title' => $this->l('皆可'),
                    ],
                ],
            ],

            'message' => [
                'filter_key' => 'r_h_m!message',
                'title' => $this->l('留言'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'schedule' => [
                'filter_key' => 'r_h_m!schedule',
                'title' => $this->l('進度'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'schedule',
                        'value' => 0,
                        'title' => $this->l('未處理'),
                    ],
                    [
                        'class' => 'schedule',
                        'value' => 1,
                        'title' => $this->l('處理中'),
                    ],
                    [
                        'class' => 'schedule',
                        'value' => 2,
                        'title' => $this->l('已結案'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'create_time' => [
                'filter_key' => 'r_h_m!create_time',
                'title' => $this->l('建立時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            'rent_house_message' => [
                'legend' => [
                    'title' => $this->l('顧客留言'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_rent_house_message' => [
                        'name' => 'id_rent_house_message',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'message_type' => [
                        'name' => 'message_type',
                        'type' => 'select',
                        'label_col' => 3,
                        'label' => $this->l('留言方式'),
                        'options'   => [
                            'val'         => [
                                [
                                    'text' => '留言',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '預約賞屋',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '樂購顧問',
                                    'val'  => '3',
                                ],
                            ],
                        ],
                        'disabled' => true,
                    ],

                    'name' => [
                        'name' => 'name',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '20',
                        'label' => $this->l('留言者'),
                        'disabled' => true,
                    ],

                    'phone' => [
                        'name' => 'phone',
                        'type' => 'view',
                        'label_col' => 3,
                        'maxlength' => '20',
                        'label' => $this->l('手機'),
                        'disabled' => true,
                    ],

                    'tel' => [
                        'name' => 'tel',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '20',
                        'label' => $this->l('市話'),
                        'disabled' => true,
                    ],

                    'email' => [
                        'name' => 'email',
                        'type' => 'text',
                        'label_col' => 3,
                        'maxlength' => '128',
                        'label' => $this->l('信箱'),
                        'disabled' => true,
                    ],

                    'sex' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('性別'),
                        'name' => 'sex',
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'sex_0',
                                'value' => 0,
                                'label' => $this->l('女性'),
                            ],
                            [
                                'id' => 'sex_1',
                                'value' => 1,
                                'label' => $this->l('先生'),
                            ],
                        ],
                        'disabled' => true,
                    ],

                    'contact_time' => [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('聯絡時間'),
                        'name' => 'contact_time',
                        'val' => 0,
                        'values' => [
                            [
                                'id' => 'contact_time_0',
                                'value' => 0,
                                'label' => $this->l('上午'),
                            ],
                            [
                                'id' => 'contact_time_1',
                                'value' => 1,
                                'label' => $this->l('中午'),
                            ],
                            [
                                'id' => 'contact_time_2',
                                'value' => 2,
                                'label' => $this->l('下午'),
                            ],
                            [
                                'id' => 'contact_time_3',
                                'value' => 3,
                                'label' => $this->l('皆可'),
                            ],
                        ],
                        'disabled' => true,
                    ],

                    'message' => [
                        'name' => 'message',
                        'type' => 'textarea',
                        'label' => $this->l('留言'),
                        'rows' =>   '10',
                        'cols'=>'5',
                        'maxlength'=>'100',
                        'disabled' => true,
                    ],


                    'schedule' => [
                        'type' => 'select',
                        'label_col' => 3,
                        'label' => $this->l('進度'),
                        'name' => 'schedule',
                        'val' => 0,
//                        'required' => true,
//                        'disabled' => true,
                        'options'   => [
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '未處理',
                                    'val'  => 0,
                                ],
                                [
                                    'text' => '處理中',
                                    'val'  => 1,
                                ],
                                [
                                    'text' => '已結案',
                                    'val'  => 2,
                                ],
                            ],
                        ],
                    ],

//                    'id_admin'    => [
//                        'name'      => 'id_admin',
//                        'type'      => 'select',
//                        'label_col' => 3,
//                        'label'     => $this->l('處理人'),
//                        'options'   => [
//                            'default' => [
//                                'text' => '請選擇處理人',
//                                'val'  => '',
//                            ],
//
//                            'table'   => 'admin',
//                            'text'    => 'first_name',
//                            'value'   => 'id_admin',
//                            'order'   => ' `id_admin` DESC',
//                            'where'   => ' AND `active` = 1',
//                        ],
//                        'required'  => true,
//                        'no_active' => true,
//                    ],
                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],

                'cancel' => [
                    'title' => $this->l('取消'),
                ],

                'reset' => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();

    }

}