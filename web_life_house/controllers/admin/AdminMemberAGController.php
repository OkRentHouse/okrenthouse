<?php

class AdminMemberAGController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminMemberAGController';
		$this->table           = 'member_ag';
		$this->_as             = 'm';
		$this->fields['index'] = 'id_member_ag ';
		$this->fields['title'] = '會員AG管理';
		$this->fields['order'] = ' ORDER BY `position` ASC';


		$this->fields['list'] = [
			'id_store_type' => [
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'store_type'    => [
				'title'  => $this->l('類別名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'position'      => [
				'title'  => $this->l('順序'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['form'] = [
			'store_type' => [
				'legend' => [
					'title' => $this->l('店家類別'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_store_type' => [
						'name'     => 'id_store_type',
						'type'     => 'hidden',
						'index'    => true,
						'label'    => $this->l('類別'),
						'required' => true,
					],
					'store_type'    => [
						'name'      => 'store_type',
						'type'      => 'text',
						'label'     => $this->l('類別名稱'),
						'maxlength' => '10',
						'required'  => true,
					],
					[
						'name'  => 'position',
						'type'  => 'number',
						'label' => $this->l('順序'),
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function processDel()
	{
		$id_store_type = Tools::getValue('id_store_type');
		$sql           = 'SELECT `id_store`
			FROM `store`
			WHERE `id_store_type` = ' . $id_store_type . '
			LIMIT 0, 1';
		$row           = Db::rowSQL($sql, true);
		if (count($row))
			$this->_errors[] = $this->l('無法刪除正在使用的類別!');
		parent::processDel();
	}
}