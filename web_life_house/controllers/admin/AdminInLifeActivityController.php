<?php

class AdminInLifeActivityController extends AdminController


{


    public function __construct()
    {

        $this->className = 'AdminInLifeActivityController';

        $this->table = 'in_life_activity';

        $this->fields['index'] = 'id_in_life_activity';

        $this->_as = 'ila';

        $this->fields['title'] = '活動管理';

        $this->_join = ' LEFT JOIN `in_life` AS il ON il.`id_in_life` = ila.`id_in_life`';

        $this->fields['order'] = ' ORDER BY ila.`id_in_life` ASC, ila.`id_in_life_activity`';

        $this->fields['list_num'] = 50;


        $this->fields['list'] = [

            'id_in_life' => [

                'filter_key' => 'ila!id_in_life_activity',

                'index' => true,

                'title' => $this->l('ID'),

                'type' => 'checkbox',

                'hidden' => true,

                'class' => 'text-center',


            ],

            'title' => [


                'filter_key' => 'il!title',


                'title' => $this->l('活動名稱'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],

            'date' => [


                'filter_key' => 'ila!date',


                'title' => $this->l('日期'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],

            'time' => [


                'filter_key' => 'ila!time',


                'title' => $this->l('時間'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],

            'people_limit' => [


                'filter_key' => 'ila!people_limit',


                'title' => $this->l('限制人數'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],

        ];

        $this->fields['tabs'] = [


            'session' => $this->l('場次管理'),


        ];

        $this->fields['form'] = [


            'in_life_activity' =>[

                'tab'    => 'letter',

                'legend' => [
                    'title' => $this->l('活動場次'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [

                    'id_in_life_activity' => [

                        'name' => 'id_in_life_activity',

                        'type' => 'hidden',

                        'label_col' => 3,

                        'index' => true,

                        'required' => true,

                    ],
                    
                    'id_in_life' => [
                        'name'      => 'id_in_life',
                        'type'      => 'select',
                        'label_col' => 3,
                        'label'     => $this->l('選擇活動'),
                        'options'   => [
                            'default' => [
                                'text' => '選擇活動',
                                'val'  => '',
                            ],
                            'table'   => 'in_life',
                            'text'    => 'title',
                            'value'   => 'id_in_life',
                            'order'   => '`date` ASC',
                            'where'   => ' AND active=1 AND url = ""',
                        ],
                        'required'  => true,

                    ],

                    'date' => [


                        'name' => 'date',


                        'type' => 'date',


                        'label_col' => 3,


                        'label' => $this->l('日期'),





                        'required'  => true,

                    ],

                    'time' => [


                        'name' => 'time',


                        'type' => 'time',


                        'label_col' => 3,


                        'label' => $this->l('時間'),


                        'required'  => true,

                    ],

                    'people_limit' => [

                        'name'      => 'people_limit',
                        'type'      => 'text',
                        'label_col' => 3,
                        'label'     => $this->l('人數限制'),
                        'maxlength' => '20',
                    ],


                ],


                'submit' => [

                    [

                        'title' => $this->l('儲存'),

                    ],

                ],


                'cancel' => [

                    'title' => $this->l('取消'),

                ],


                'reset' => [

                    'title' => $this->l('復原'),

                ],

            ],

        ];


        parent::__construct();
    }

}
