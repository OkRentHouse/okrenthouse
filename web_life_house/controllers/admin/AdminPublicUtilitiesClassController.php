<?php

class AdminPublicUtilitiesClassController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminPublicUtilitiesClassController';
        $this->table           = 'public_utilities_class';
        $this->fields['index'] = 'id_public_utilities_class';
        $this->fields['title'] = '公共設施主類別';
        $this->fields['order'] = ' ORDER BY `position` ASC';

        $this->fields['list'] = [
            'id_public_utilities_class' => [
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'title'               => [
                'title'  => $this->l('公共設施主類別名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'            => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            'id_public_utilities_class' => [
                'legend' => [
                    'title' => $this->l('公共設施主類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_public_utilities_class' => [
                        'name'     => 'id_public_utilities_class',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('類別'),
                        'required' => true,
                    ],
                    'title'  => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('公共設施主類別名稱'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],
                    'position'            => [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }

    public function processDel()
    {
        $this->_errors[] = $this->l('無法刪除類別!');
    }
}