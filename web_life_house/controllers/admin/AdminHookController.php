<?php

class AdminHookController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminHookController';
		$this->table           = 'hook';
		$this->fields['index'] = 'id_hook';
		$this->fields['title'] = 'Hook管理';
		$this->fields['list']  = [
			'id_hook'     => [
				'index'  => true,
				'title'  => $this->l('Hook編號'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'name'        => [
				'title'  => $this->l('Hook名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'title'       => [
				'title'  => $this->l('Hook標題'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'description' => [
				'title'  => $this->l('說明'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-left',
			],
			'live_edit'   => [
				'title'  => $this->l('線上編輯'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'live_edit_1',
						'value' => 1,
						'title' => $this->l('是'),
					],
					[
						'class' => 'live_edit_0',
						'value' => 0,
						'title' => $this->l('否'),
					],
				],
			],
		];

		$this->fields['list_num'] = 100;

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('Hook管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'     => 'id_hook',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					[
						'name'      => 'name',
						'type'      => 'text',
						'label'     => $this->l('Hook名稱'),
						'maxlength' => '64',
						'required'  => true,
					],
					[
						'name'      => 'title',
						'type'      => 'text',
						'label'     => $this->l('Hook標題'),
						'maxlength' => '64',
						'required'  => true,
					],
					[
						'name'  => 'description',
						'type'  => 'text',
						'label' => $this->l('說明'),
					],
					[
						'type'   => 'switch',
						'label'  => $this->l('線上編輯'),
						'name'   => 'live_edit',
						'values' => [
							[
								'id'    => 'live_edit_1',
								'value' => 1,
								'label' => $this->l('是'),
							],
							[
								'id'    => 'live_edit_2',
								'value' => 0,
								'label' => $this->l('否'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}
}