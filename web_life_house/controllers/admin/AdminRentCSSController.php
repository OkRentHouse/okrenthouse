<?php

class AdminRentCSSController extends AdminController
{
	public $page = 'rent_css';

	public function __construct()
	{
		$this->className       = 'AdminRentCSSController';
		$this->fields['title'] = '網站設定';


		$this->fields['form'] = [
			'web_set_up' => [
				'tab'    => 'web_set_up',
				'legend' => [
					'title' => $this->l('網站設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'Rent_css_code',
						'type'          => 'textarea',
						'label'         => $this->l('CSS 程式碼'),
						'rows'          => 12,
						'label_col'     => 2,
						'col'           => 10,
						'placeholder'   => '',
						'p'             => $this->l('此CSS會套用在所有"前台"頁面上,設定上請減少錯誤,防止版面跑版!'),
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'options';
		parent::initContent();
	}
}