<?php

class AdminTownshipCodeController extends AdminController
{
    public $no_link = false;

    public function __construct()
    {

        $this->className            = 'AdminTownshipCodeController';
        $this->table                = 'township_code';
        $this->fields['index']      = 'id_township_code';
        $this->fields['title']      = '鄉政市代碼';

        $this->_as = 'tc';


        $this->_join = ' LEFT JOIN `county_code` AS cc ON cc.`id_county_code` = tc.`id_county_code`';


        $this->_group = ' GROUP BY tc.`id_township_code`';


        $this->fields['order'] = ' ORDER BY cc.`id_county_code` ASC, tc.`id_township_code` ASC';



        $this->actions[]            = 'list';
        $this->fields['list']       = [
            'id_township_code'          => [
                'index'  => true,
                'type'   => 'checkbox',
                'select' => '',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'city_name'           => [
                'title'  => $this->l('縣市'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'city_code' => [
                'title'  => $this->l('縣市代碼'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'township_name'          => [
                'title'  => $this->l('鄉鎮'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'township_code'       => [
                'title'  => $this->l('鄉鎮代碼'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];



        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('選單'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    [
                        'name'     => 'id_township_code',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],

                    'id_county_code' => [
                        'name'      => 'id_county_code',
                        'type'      => 'select',
                        'label_col' => 3,
                        'label'     => $this->l('選擇縣市'),
                        'options'   => [
                            'default' => [
                                'text' => '選擇縣市',
                                'val'  => '',
                            ],
                            'table'   => 'county_code',
                            'text'    => 'city_name',
                            'value'   => 'id_county_code',
                            'order'   => '`id_county_code` ASC',
                        ],
                        'required'  => true,
                    ],

                    [
                        'type'        => 'text',
                        'required'    => true,
                        'label'       => $this->l('請輸入鄉鎮名稱'),
                        'placeholder' => $this->l('請輸入鄉鎮名稱'),
                        'name'        => 'township_name',
                        'maxlength'   => '50',
                    ],
                    [
                        'type'        => 'text',
                        'required'    => true,
                        'label'       => $this->l('請輸入鄉鎮代碼'),
                        'placeholder' => $this->l('請輸入鄉鎮代碼'),
                        'name'        => 'township_code',
                        'maxlength'   => '50',
                    ],

                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
        parent::__construct();
    }



    /*
    public function initToolbar(){		//初始化功能按鈕
        if ($this->display == 'add' || $this->display == 'edit') {
            $this->toolbar_btn['save_and_stay'] = array(
                'href' => '#',
                'desc' => $this->l('儲存並繼續編輯')
            );
        }
        parent::initToolbar();
    }
    */
}