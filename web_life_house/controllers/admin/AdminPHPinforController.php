<?php
class AdminPHPinforController extends AdminController
{
    public function __construct(){
        $this->className = 'AdminPHPinforController';
        parent::__construct();
        $this->tabAccess['add'] = 0;
        $this->tabAccess['edit'] = 0;
        $this->tabAccess['del'] = 0;
        $this->breadcrumb_is_not =1;
    }

    public function checkAccess()	//所有人都有權限
    {
        return true;
    }


    public function initHeader()
    {
        header('Content-Type:text/html; charset=utf-8');
        header('Cache-Control: no-store, no-cache');
        $this->context->smarty->assign([
            'display_admin_menu'=>'1',
            'tab_list'        => Tab::getContext()->getTabList(),
            'web_name'        => WEB_NAME,                //網站名稱
            'NAVIGATION_PIPE' => NAVIGATION_PIPE    //標題間格標示
        ]);
    }

}
