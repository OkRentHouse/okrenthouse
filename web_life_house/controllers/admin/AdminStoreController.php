<?php


class AdminStoreController extends AdminController

{

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;
    public $page = 'store';


    public function __construct()

    {

        if ($_SESSION['id_admin'] == 1) {

            Store::getContext()->getCounty();

        }

        $this->className = 'AdminStoreController';

        $this->fields['title'] = '生活好康店家管理';

        $this->table = 'store';

        $this->_as = 's';

        $this->_join = ' LEFT JOIN `store_file` AS sf ON sf.`id_store` = s.`id_store`

LEFT JOIN `admin` AS a ON a.`ag` = s.`ag`';

        $this->fields['order'] = ' ORDER BY  s.`top` DESC, if(s.`sort` > 0,0,1) ASC, s.`sort` ASC, s.`id_store` DESC';


        $this->_group = ' GROUP BY s.`id_store`';


        $this->fields['index'] = 'id_store';

        $this->fields['list'] = [

            'id_store' => [

                'filter_key' => 's!id_store',            //s.`id_store`

                'index' => true,

                'hidden' => true,

                'type' => 'checkbox',

                'class' => 'text-center',

            ],

            'top' => [

                'filter_key' => 's!top',
                'title' => $this->l('置頂'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'top',
                        'value' => 1,
                        'title' => $this->l('置頂'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'sort' => [

                'filter_key' => 's!sort',

                'title' => $this->l('排序'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

                'no_link' => false,

            ],

            'title' => [

                'filter_key' => 's!title',

                'title' => $this->l('店名'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

                'no_link' => false,

            ],

            'website' => [

                'filter_key' => 's!website',

                'title' => $this->l('網站'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            /*'account'        => [

            'filter_key' => 's!account',

            'title'   => $this->l('帳號'),

            'order'   => true,

            'filter'  => true,

            'class'   => 'text-center',

            'no_link' => false,

            ],*/

            'id_store_type' => [

                'filter_key' => 's!id_store_type',

                'title' => $this->l('店家類別'),

                'order' => true,

                'filter' => true,

                'multiple' => true,

                'key' => [

                    'table' => 'store_type',

                    'key' => 'id_store_type',

                    'val' => 'store_type',

                    'order' => '`position` ASC',

                ],

                'class' => 'text-center',

            ],

            'chain' => [

                'filter_key' => 's!chain',

                'title' => $this->l('連鎖類別'),

                'order' => true,

                'filter' => true,

                'values' => [

                    [

                        'class' => 'chain_0',

                        'value' => 0,

                        'title' => $this->l('無'),

                    ],

                    [

                        'class' => 'chain_1',

                        'value' => 1,

                        'title' => $this->l('全國連鎖品牌'),

                    ],

                    [

                        'class' => 'chain_2',

                        'value' => 2,

                        'title' => $this->l('區域連鎖品牌'),

                    ],

                ],

                'class' => 'text-center',

            ],

            //			'information'    => [

            //				'filter_key' => 's!information',

            //				'title'      => $this->l('分店資訊'),

            //				'order'      => true,

            //				'filter'     => true,

            //				'show'       => false,

            //				'class'      => 'text-center',

            //			],

            'county' => [

                'filter_key' => 's!county',

                'title' => $this->l('縣市'),

                'order' => true,

                'filter' => true,

                'multiple' => true,

                'class' => 'text-center',

                'key' => [

                    'table' => 'store',

                    'key' => 'county',

                    'val' => 'county',

                    'order' => '`county` ASC',

                ],

            ],

            'area' => [

                'filter_key' => 's!area',

                'title' => $this->l('行政區'),

                'order' => true,

                'filter' => true,

                'multiple' => true,

                'class' => 'text-center',

                'values' => County::getContext()->form_list_city('county[]'),

            ],

            'address1' => [

                'filter_key' => 's!address1',

                'title' => $this->l('店家地址'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'lat1' => [

                'filter_key' => 's!lat1',

                'title' => $this->l('緯度1'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'lng1' => [

                'filter_key' => 's!lng1',

                'title' => $this->l('經度1'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'tel1' => [

                'filter_key' => 's!tel1',

                'title' => $this->l('店家電話1'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'tel2' => [

                'filter_key' => 's!tel2',

                'title' => $this->l('店家電話2'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'open_time_w' => [

                'filter_key' => 's!open_time_w',

                'title' => $this->l('營業時間'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'close_time' => [

                'filter_key' => 's!close_time',

                'title' => $this->l('公休時間'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'paying' => [

                'filter_key' => 's!paying',

                'title' => $this->l('付款方式'),

                'order' => true,

                'filter' => true,

                'in_table' => true,

                'multiple' => true,

                'class' => 'text-center',

                'values' => [

                    [

                        'class' => 'paying_1',

                        'value' => '1',

                        'title' => $this->l('現金'),

                    ],

                    [

                        'class' => 'paying_2',

                        'value' => '2',

                        'title' => $this->l('線上支付'),

                    ],

                    [

                        'class' => 'paying_3',

                        'value' => '3',

                        'title' => $this->l('刷卡'),

                    ],

                    [

                        'class' => 'paying_4',

                        'value' => '4',

                        'title' => $this->l('刷卡:可分期'),

                    ],

                    [

                        'class' => 'paying_5',

                        'value' => '5',

                        'title' => $this->l('會員制'),

                    ],

                ],

            ],

            'service' => [

                'filter_key' => 's!service',

                'title' => $this->l('產品.服務'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'discount' => [

                'filter_key' => 's!discount',

                'title' => $this->l('好康優惠'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],



            'firm' => [

                'filter_key' => 's!firm',

                'title' => $this->l('商號登記'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'uniform' => [

                'filter_key' => 's!uniform',

                'title' => $this->l('統一編號'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'legal_agent' => [

                'filter_key' => 's!legal_agent',

                'title' => $this->l('法定代理人'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'address' => [

                'filter_key' => 's!address',

                'title' => $this->l('登記地址'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'company_tel' => [

                'filter_key' => 's!company_tel',

                'title' => $this->l('公司電話'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'contact_person' => [

                'filter_key' => 's!contact_person',

                'title' => $this->l('經辦連絡人'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'phone' => [

                'filter_key' => 's!phone',

                'title' => $this->l('連絡手機'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'line_id' => [

                'filter_key' => 's!line_id',

                'title' => $this->l('Line ID'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'email' => [

                'filter_key' => 's!email',

                'title' => $this->l('e-mail'),

                'order' => true,

                'filter' => true,

                'show' => false,

                'class' => 'text-center',

            ],

            'logo' => [

                'title' => $this->l('Logo'),

                'order' => true,

                'filter' => true,

                'filter_sql' => 'IF(sf.`id_store` > 0 AND sf.`id_type` = 0, 1, 0)',

                'class' => 'text-center', 'values' => [

                    [

                        'class' => 'logo_1',

                        'value' => '1',

                        'title' => $this->l('有'),

                    ],

                    [

                        'class' => 'logo_0 red',

                        'value' => '0',

                        'title' => $this->l('無'),

                    ],

                ],

            ],

            'img' => [

                'title' => $this->l('圖片'),

                'order' => true,

                'filter' => true,

                'filter_sql' => 'IF(sf.`id_store` > 0 AND sf.`id_type` = 1, 1, 0)',

                'class' => 'text-center', 'values' => [

                    [

                        'class' => 'img_1',

                        'value' => '1',

                        'title' => $this->l('有'),

                    ],

                    [

                        'class' => 'img_0 red',

                        'value' => '0',

                        'title' => $this->l('無'),

                    ],

                ],

            ],

            'products' => [

                'title' => $this->l('產品圖'),

                'order' => true,

                'filter' => true,

                'filter_sql' => 'IF(sf.`id_store` > 0 AND sf.`id_type` = 2, 1, 0)',

                'class' => 'text-center', 'values' => [

                    [

                        'class' => 'products_1',

                        'value' => '1',

                        'title' => $this->l('有'),

                    ],

                    [

                        'class' => 'products_0 red',

                        'value' => '0',

                        'title' => $this->l('無'),

                    ],

                ],

            ],

            'application' => [

                'title' => $this->l('申請書'),

                'order' => true,

                'filter' => true,

                'filter_sql' => 'IF(sf.`id_store` > 0 AND sf.`id_type` = 1, 1, 0)',

                'class' => 'text-center', 'values' => [

                    [

                        'class' => 'application_1',

                        'value' => '1',

                        'title' => $this->l('有'),

                    ],

                    [

                        'class' => 'application_0 red',

                        'value' => '0',

                        'title' => $this->l('無'),

                    ],

                ],

            ],

            'contract' => [

                'title' => $this->l('契約書'),

                'order' => true,

                'filter' => true,

                'filter_sql' => 'IF(sf.`id_store` > 0 AND sf.`id_type` = 4, 1, 0)',

                'values' => [

                    [

                        'class' => 'contract_1',

                        'value' => '1',

                        'title' => $this->l('有'),

                    ],

                    [

                        'class' => 'contract_0 red',

                        'value' => '0',

                        'title' => $this->l('無'),

                    ],

                ],

                'class' => 'text-center',

            ],

            'ag' => [

                'filter_key' => 'a!ag',

                'title' => $this->l('經辦'),

                'key' => [

                    'table' => 'admin',

                    'key' => 'ag',

                    'text_divide' => '%s - %s%s',

                    'val_divide' => '%s - %s%s',

                    'val' => ['ag', 'last_name', 'first_name'],

                    'where' => ' AND `ag` <> "" AND `id_group` <> 1',

                    'order' => '`active` DESC, `ag` ASC',

                ],

                'order' => true,

                'filter' => true,

                'multiple' => true,

                'show' => false,

                'data' => [

                    'live-search' => 'true',

                ],

                'class' => 'text-center selectpicker',

            ],

            'id_admin' => [

                'filter_key' => 'a!id_admin',

                'title' => $this->l('建檔人'),

                'key' => [

                    'table' => 'id_admin',

                    'key' => 'id_admin',

                    'text_divide' => '%s%s',

                    'val_divide' => '%s%s',

                    'val' => ['last_name', 'first_name'],

                    'order' => '`last_name` DESC, `first_name` ASC',

                ],

                'order' => true,

                'filter' => 'text',

                'multiple' => true,

                'show' => false,

                //				'data'       => [

                //					'live-search' => 'true',

                //				],

                'class' => 'text-center',

            ],

            'submit_date' => [

                'title' => $this->l('建檔日期'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'views' => [

                'title' => $this->l('瀏覽人數'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'popularity' => [

                'filter_sql' => '(SELECT ROUND(SUM(`popularity`)/ COUNT(`id_store`), 1)

FROM `store_popularity` AS sp

WHERE sp.`id_store` = s.`id_store`)',

                'title' => $this->l('評價'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

            'active' => [

                'filter_key' => 's!active',

                'title' => $this->l('啟用狀態'),

                'order' => true,

                'filter' => true,

                'class' => 'text-center',

            ],

        ];


//		$this->fields['excel']['list'] = $this->fields['list'];


        $this->fields['tabs'] = [

            'store' => $this->l('商家資訊'),

            'file' => $this->l('檔案管理'),

        ];


        $this->fields['form'] = [

            'user' => [

                'tab' => 'store',

                'legend' => [

                    'title' => $this->l('商家帳號'),

                    'icon' => 'icon-cogs',

                    'image' => '',

                ],

                'input' => [

                    'id_store' => [

                        'name' => 'id_store',

                        'type' => 'hidden',

                        'index' => true,

                        'required' => true,

                    ],

                    //					'account'         => [

                    //						'name'      => 'account',

                    //						'type'      => 'text',

                    //						'unique'    => true,

                    //						'required'  => true,

                    //						'maxlength' => 20,

                    //						'label'     => $this->l('帳號'),

                    //						'form_col'  => 12,

                    //						'label_col' => 2,

                    //						'col'       => 4,

                    //					],

                    //					'change-password' => [

                    //						'type'       => 'change-password',

                    //						'form_col'   => 12,

                    //						'label_col'  => 2,

                    //						'col'        => 8,

                    //						'label'      => $this->l('修改密碼'),

                    //						'old_passwd' => false,

                    //						'minlength'  => '4',

                    //						'maxlength'  => '20',

                    //						'name'       => 'password',

                    //					],

                    //					'password'        => [

                    //						'type'        => 'new-password',

                    //						'label'       => $this->l('密碼'),

                    //						'placeholder' => $this->l('請輸入密碼'),

                    //						'required'    => true,

                    //						'minlength'   => '4',

                    //						'maxlength'   => '20',

                    //						'name'        => 'password',

                    //						'form_col'    => 12,

                    //						'label_col'   => 2,

                    //						'col'         => 4,

                    //					],

                    //					'password2'       => [

                    //						'type'        => 'confirm-password',

                    //						'label'       => $this->l('確認密碼'),

                    //						'placeholder' => $this->l('再次輸入確認密碼'),

                    //						'required'    => true,

                    //						'minlength'   => '4',

                    //						'maxlength'   => '20',

                    //						'name'        => 'password2',

                    //						'confirm'     => 'password',

                    //						'form_col'    => 12,

                    //						'label_col'   => 2,

                    //						'col'         => 4,

                    //					],

                    'active' => [

                        'type' => 'switch',

                        'label' => $this->l('啟用狀態'),

                        'name' => 'active',

                        'val' => 1,

                        'values' => [

                            [

                                'id' => 'active_on',

                                'value' => 1,

                                'label' => $this->l('啟用'),

                            ],

                            [

                                'id' => 'active_off',

                                'value' => 0,

                                'label' => $this->l('關閉'),

                            ],

                        ],

                        'form_col' => 12,

                        'label_col' => 2,

                        'col' => 3,

                    ],

                    'top' => [
                        'name' => 'top',
                        'type' => 'checkbox',
                        'label_col' => 2,
                        'label' => $this->l('置頂'),
                        'values' => [
                            [
                                'id' => 'top',
                                'value' => 1,
                                'label' => $this->l('置頂'),
                            ],
                        ],
                    ],
                ],

            ],

            'store' => [

                'tab' => 'store',

                'legend' => [

                    'title' => $this->l('商家資訊'),

                    'icon' => 'icon-cogs',

                    'image' => '',

                ],

                'input' => [

                    'id_store' => [

                        'name' => 'id_store',

                        'type' => 'hidden',

                        'index' => true,

                        'required' => true,

                    ],

                    'title' => [

                        'name' => 'title',

                        'type' => 'text',

                        'maxlength' => 50,

                        'label' => $this->l('店名'),

                        'required' => true,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'sort' => [

                        'name' => 'sort',

                        'type' => 'number',

                        'label' => $this->l('排序'),

                        'val' => 0,

                        'min' => 0,

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    'id_store_type' => [

                        'name' => 'id_store_type',

                        'type' => 'select',

                        'options' => [

                            'default' => [

                                'text' => '請選擇類別',

                                'val' => '',

                            ],

                            'table' => 'store_type',

                            'text' => 'store_type',

                            'value' => 'id_store_type',

                            'order' => '`position` ASC',

                        ],

                        'label' => $this->l('店家類別'),

                        'p' => $this->l('<a href="/manage/StoreType?&addstore_type" target="_blank">新增類別</a>'),

                        'required' => true,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'chain' => [

                        'name' => 'chain',

                        'type' => 'switch',

                        'label' => $this->l('連鎖類別'),

                        'val' => 0,

                        'values' => [

                            [

                                'id' => 'chain_0',

                                'value' => 0,

                                'label' => $this->l('無'),

                            ],

                            [

                                'id' => 'chain_1',

                                'value' => 1,

                                'label' => $this->l('全國連鎖品牌'),

                            ],

                            [

                                'id' => 'chain_2',

                                'value' => 2,

                                'label' => $this->l('區域連鎖品牌'),

                            ],

                        ],

                        'required' => true,

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    //					'information'    => [

                    //						'name'      => 'information',

                    //						'type'      => 'textarea',

                    //						'col'       => 8,

                    //						'rows'      => 10,

                    //						'class'     => 'no_enter',

                    //						'maxlength' => 500,

                    //						'label'     => $this->l('分店資訊'),

                    //						'required'  => true,

                    //						'label_col' => 2,

                    //					],

                    'county' => [

                        'name' => 'county',

                        'type' => 'select',

                        'required' => true,

                        'label' => $this->l('縣市'),

                        'options' => [

                            'default' =>

                                [

                                    'val' => '',

                                    'text' => '請選擇縣市',

                                ],

                            'val' => County::getContext()->form_form_county(),

                        ],

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'area' => [

                        'name' => 'area',

                        'type' => 'select',

                        'label' => $this->l('行政區'),

                        'options' => [

                            'default' =>

                                [

                                    'val' => '',

                                    'text' => '請選擇行政區',

                                ],

                            'val' => County::getContext()->form_form_city(),

                        ],

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    'address1' => [

                        'name' => 'address1',

                        'type' => 'text',

                        'maxlength' => 200,

                        'class' => 'auto_address',

                        'data' => [

                            'county' => 'county',

                            'city' => 'area',

                        ],

                        'label' => $this->l('店家地址'),

                        'required' => true,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'lat1' => [

                        'name' => 'lat1',

                        'type' => 'text',

                        'maxlength' => 20,

                        'is_prefix' => true,

                        'prefix' => $this->l('緯度'),

                        'label' => $this->l('經緯度'),

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    'lng1' => [

                        'name' => 'lng1',

                        'type' => 'text',

                        'maxlength' => 20,

                        'is_suffix' => true,

                        'prefix' => $this->l('經度'),

//						'p'         => $this->l('注意:Google MAP網址,左邊是[緯度],右邊是[經度]'),

                    ],

                    'map' => [

                        'name' => 'map',

                        'label' => $this->l(''),

                        'no_action' => true,

                        'type' => 'map',

                        'data' => [

                            'address_key' => 'address1',

                            'lat_key' => 'lat1',

                            'lng_key' => 'lng1',

                            'zoom' => '14',

                        ],

                        'label_col' => 7,

                        'col' => 4,

                    ],

                    //					'address2'       => [

                    //						'name'      => 'address2',

                    //						'type'      => 'text',

                    //						'maxlength' => 200,

                    //						'label'     => $this->l('店家地址2'),

                    //						'form_col'  => 6,

                    //						'label_col' => 4,

                    //						'col'       => 8,

                    //					],

                    //					'lat2'           => [

                    //						'name'      => 'lat2',

                    //						'type'      => 'text',

                    //						'maxlength' => 20,

                    //						'is_prefix' => true,

                    //						'prefix'    => $this->l('緯度'),

                    //						'label'     => $this->l('經緯度2'),

                    //						'form_col'  => 6,

                    //						'label_col' => 2,

                    //						'col'       => 8,

                    //					],

                    //					'lng2'           => [

                    //						'name'      => 'lng2',

                    //						'type'      => 'text',

                    //						'maxlength' => 20,

                    //						'is_suffix' => true,

                    //						'prefix'    => $this->l('經度'),

                    //						'p'         => $this->l('注意:Google MAP網址,左邊是[緯度],右邊是[經度]'),

                    //					],

                    'tel1' => [

                        'name' => 'tel1',

                        'type' => 'tel',

                        'maxlength' => 20,

                        'label' => $this->l('店家電話1'),

                        'required' => true,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'tel2' => [

                        'name' => 'tel2',

                        'type' => 'tel',

                        'maxlength' => 20,

                        'label' => $this->l('店家電話2'),

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    'open_time_w' => [

                        'type' => 'text',

                        'label' => $this->l('營業時間'),

                        'p' => $this->l('按Enter繼續新增'),

//                        'required' => true,

                        'data' => ['role' => 'tagsinput'],

                        'name' => 'open_time_w',

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'close_time' => [

                        'type' => 'text',

                        'label' => $this->l('公休時間'),

                        'p' => $this->l('按Enter繼續新增'),

                        'data' => ['role' => 'tagsinput'],

                        'name' => 'close_time',

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    'paying' => [

                        'type' => 'checkbox',

                        'label' => $this->l('付款方式'),

                        'name' => 'paying',

                        'in_table' => true,

                        'multiple' => true,

                        'values' => [

                            [

                                'id' => 'paying_1',

                                'value' => 1,

                                'label' => $this->l('現金'),

                            ],

                            [

                                'id' => 'paying_2',

                                'value' => 2,

                                'label' => $this->l('線上支付'),

                            ], [

                                'id' => 'paying_3',

                                'value' => 3,

                                'label' => $this->l('刷卡'),

                            ], [

                                'id' => 'paying_4',

                                'value' => 4,

                                'label' => $this->l('刷卡:可分期'),

                            ], [

                                'id' => 'paying_5',

                                'value' => 5,

                                'label' => $this->l('會員制'),

                            ],

                        ],

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'service' => [

                        'type' => 'text',

                        'label' => $this->l('產品.服務'),

                        'name' => 'service',

                        'data' => ['role' => 'tagsinput'],

                        'p' => $this->l('按Enter繼續新增'),

                        'required' => true,

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 10,

                    ],

                    'discount' => [

                        'type' => 'text',

                        'label' => $this->l('好康優惠'),

                        'name' => 'discount',

                        'data' => ['role' => 'tagsinput'],

                        'p' => $this->l('按Enter繼續新增'),

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'website' => [

                        'type' => 'text',

                        'label' => $this->l('網址'),

                        'name' => 'website',

                        'p' => $this->l('按Enter繼續新增'),

                        'label_col' => 2,

                        'col' => 10,

                    ],

                    'introduction'                => [
                        'name'      => 'introduction',
                        'type'      => 'textarea',
                        'label_col' => 2,
                        'label'     => $this->l('店家介紹'),
                        'class'     => 'tinymce',
                        'required'  => true,
                        'col'       => 8,
                        'rows'      => 30,
                    ],

                    'hr' => [

                        'type' => 'hr',

                    ],

                    'firm' => [

                        'type' => 'text',

                        'label' => $this->l('商號登記'),

                        'name' => 'firm',

                        'maxlength' => 20,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'uniform' => [

                        'type' => 'text',

                        'label' => $this->l('統一編號'),

                        //'required'  => true,

                        'name' => 'uniform',

                        'maxlength' => 20,

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    'legal_agent' => [

                        'type' => 'text',

                        'label' => $this->l('法定代理人'),

                        //'required'  => true,

                        'name' => 'legal_agent',

                        'maxlength' => 20,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'legal_agent_sex' => [

                        'type' => 'switch',

                        'name' => 'legal_agent_sex',

                        'values' => [
                            [

                                'id' => 'legal_agent_sex_0',

                                'value' => 0,

                                'label' => $this->l('小姐'),

                            ],

                            [

                                'id' => 'legal_agent_sex_1',

                                'value' => 1,

                                'label' => $this->l('先生'),

                            ],

                        ],

                        'label' => $this->l('法定代理人性別'),


                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],


                    'address' => [

                        'type' => 'text',

                        'label' => $this->l('登記地址'),

                        //'required'  => true,

                        'name' => 'address',

                        'maxlength' => 20,


                        'label_col' => 2,

                        'col' => 9,

                    ],

                    'company_tel' => [

                        'type' => 'text',

                        'label' => $this->l('公司電話'),

                        //'required'  => true,

                        'name' => 'company_tel',

                        'maxlength' => 20,


                        'label_col' => 2,

                        'col' => 4,

                    ],

                    'contact_person' => [

                        'type' => 'text',

                        'label' => $this->l('經辦連絡人'),

                        //'required'  => true,

                        'name' => 'contact_person',

                        'maxlength' => 20,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],


                    'contact_person_sex' => [

                        'type' => 'switch',

                        'name' => 'contact_person_sex',

                        'values' => [
                            [

                                'id' => 'contact_person_sex_0',

                                'value' => 0,

                                'label' => $this->l('小姐'),

                            ],

                            [

                                'id' => 'contact_person_sex_1',

                                'value' => 1,

                                'label' => $this->l('先生'),

                            ],

                        ],

                        'label' => $this->l('連絡人稱謂'),


                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],



                    'phone' => [

                        'type' => 'tel',

                        'label' => $this->l('連絡手機'),

                        //'required'  => true,

                        'name' => 'phone',

                        'data' => ['role' => 'tagsinput'],

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'line_id' => [

                        'type' => 'text',

                        'label' => $this->l('Line ID'),

                        //'required'  => true,

                        'name' => 'line_id',

                        'data' => ['role' => 'tagsinput'],

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                    ],

                    'email' => [

                        'type' => 'email',

                        'label' => $this->l('e-mail'),

                        //'required'  => true,

                        'name' => 'email',

                        'maxlength' => 32,

                        'form_col' => 6,

                        'label_col' => 4,

                        'col' => 8,

                    ],

                    'ag' => [

                        'name' => 'ag',

                        'label' => $this->l('經辦'),

                        'type' => 'select',

                        'options' => [

                            'table' => 'admin',

                            'text' => ['ag', 'last_name', 'first_name'],

                            'text_divide' => '%s - %s%s',

                            'value' => 'ag',

                            'where' => ' AND `ag` <> "" AND `id_group` <> 1',

                            'order' => '`active` DESC, `ag` ASC',

                            'default' => [

                                'val' => '',

                                'text' => $this->l('請選擇AG'),

                            ],

                        ],

                        'form_col' => 6,

                        'label_col' => 2,

                        'col' => 8,

                        'data' => [

                            'live-search' => 'true',

                        ],

                        'class' => 'text-center selectpicker',

                    ],

                    'id_admin' => [
                        'name' => 'id_admin',
                        'label' => $this->l('建檔人'),
                        'type' => 'hidden',        //<input type="XXXXXX">
                        'val' => $_SESSION['id_admin'],
                        'form_col' => 6,
                        'label_col' => 2,
                        'col' => 8,
                    ],
                    'submit_date' => [
                        'name' => 'submit_date',
                        'label' => $this->l('建檔日期'),
                        'type' => 'view',
                        'auto_datetime' => 'add',        //新增的時候加入
//						'auto_datetime' => 'edit',		//修改的時候加入
                        'form_col' => 6,
                        'label_col' => 4,
                        'col' => 8,
                    ],
                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay' => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],

                'reset' => [
                    'title' => $this->l('重置'),
                ],
            ],

            'logo' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('商標LOGO'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [
                    'logo' => [
                        'name' => 'logo',
                        'no_label' => true,
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            //														'max'              => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 640,
                            'max_height' => 330,
                            'resize' => true,
                            //							'resizePreference' => 'height',
                        ],
                        //						'multiple'  => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 640*330px'),
                    ],
                ],
            ],

            'photo' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('店家圖檔'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'photo' => [
                        'name' => 'photo',
                        'no_label' => true,
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            //							'max'         => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 640,
                            'max_height' => 330,
                            'resize' => true,
                            //							'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 640*330px'),
                    ],
                ],
            ],

            'product' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('產品服務介紹圖檔'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'product' => [
                        'name' => 'product',
                        'no_label' => true,
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 860*400px'),
                    ],
                ],
            ],

            'application' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('申請書'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'application' => [
                        'name' => 'application',
                        'no_label' => true,
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                    ],
                ],
            ],
            'contrat' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('契約書'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [
                    'contrat' => [
                        'name' => 'contrat',
                        'no_label' => true,
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                    ],
                ],
            ],
        ];
        parent::__construct();
    }

    public function initProcess()
    {
        parent::initProcess();
        if ($this->display != 'add') {
            $id_store = Tools::getValue("id_store");

            $this->fields['form']['store']['input']['id_admin']['type'] = 'view';
            $this->fields['form']['store']['input']['id_admin']['options'] = [
                'table' => 'admin',
                'text' => ['last_name', 'first_name'],
                'text_divide' => '%s%s',
                'value' => 'id_admin',
            ];

            $arr_popularity_val = Store::getContext()->get_popularity_val($id_store);
            $this->fields['tabs']['popularity'] = $this->l('人氣統計');
//			$this->fields['form']['user']['input']['account']['type'] = 'view';
            //print_r($this->fields['form']['store']['input']['title']);


            $this->fields['form']['popularity'] = [
                'tab' => 'popularity',
                'legend' => [
                    'title' => $this->l('人氣統計'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_store' => [
                        'name' => 'id_store',
                        'type' => 'hidden',
                        'index' => true,
                        'required' => true,
                    ],
                    'views' => [
                        'name' => 'views',
                        'label' => '',
                        'prefix' => $this->l('瀏覽人數'),
                        'suffix' => $this->l('人'),
                        'type' => 'view',
                        'label_col' => 2,
                        'col' => 4,
                    ],

                    'popularity_all' => [
                        'name' => 'popularity_all',
                        'label' => '',
                        'prefix' => $this->l('評價'),
                        'suffix' => $this->l('星'),
                        'type' => 'view',
                        'val' => $arr_popularity_val['all'],
                        'no_action' => true,
                        'label_col' => 2,
                        'col' => 4,
                    ],
                    'popularity_1' => [
                        'name' => 'popularity_1',
                        'label' => '',
                        'prefix' => $this->l('1星'),
                        'suffix' => $this->l('筆'),
                        'type' => 'view',
                        'val' => $arr_popularity_val['1'],
                        'no_action' => true,
                        'label_col' => 2,
                        'col' => 4,
                    ],
                    'popularity_2' => [
                        'name' => 'popularity_2',
                        'label' => '',
                        'prefix' => $this->l('2星'),
                        'suffix' => $this->l('筆'),
                        'type' => 'view',
                        'val' => $arr_popularity_val['2'],
                        'no_action' => true,
                        'label_col' => 2,
                        'col' => 4,
                    ],
                    'popularity_3' => [
                        'name' => 'popularity_3',
                        'label' => '',
                        'prefix' => $this->l('3星'),
                        'suffix' => $this->l('筆'),
                        'type' => 'view',
                        'val' => $arr_popularity_val['3'],
                        'no_action' => true,
                        'label_col' => 2,
                        'col' => 4,
                    ],
                    'popularity_4' => [
                        'name' => 'popularity_4',
                        'label' => '',
                        'prefix' => $this->l('4星'),
                        'suffix' => $this->l('筆'),
                        'type' => 'view',
                        'val' => $arr_popularity_val['4'],
                        'no_action' => true,
                        'label_col' => 2,
                        'col' => 4,
                    ],
                    'popularity_5' => [
                        'name' => 'popularity_5',
                        'label' => '',
                        'prefix' => $this->l('5星'),
                        'suffix' => $this->l('筆'),
                        'type' => 'view',
                        'val' => $arr_popularity_val['5'],
                        'no_action' => true,
                        'label_col' => 2,
                        'col' => 4,
                    ],
                ],
            ];
        }

        if($this->display == 'edit'){
    			$id_admin = Tools::getValue('id_admin');
    			//todo 要轉成模組

    			$admin = Admin::get($id_admin);
    			$html = sprintf('<a href="https://%s/login?user=%s@%s" target="_blank">%s</a>', Configuration::get('cPanel_webemail_url'), $admin['email'], Configuration::get('cPanel_email_domain'), $this->l('前往WebMail'));
    			$this->fields['form']['cPanelEmail'] = [
    				'legend' => [
    					'title' => $this->l('WebMail'),
    					'icon'  => 'icon-cogs',
    					'image' => '',
    				],
    				'input'  => [
    					'html' => [
    						'name'      => 'html',
    						'type'      => 'html',
    						'html'      => $html,
    						'no_action' => true,
    					],
    				],
    			];
    		}

    }


    public function getUpFileList()
    {
        $arr_type = Store::getContext()->arr_type;
        $id_store = Tools::getValue($this->index);
        $json = new JSON();
        $UpFileVal = [];

        foreach ($arr_type as $id_type => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_store' => $id_store,
                    'id_type' => $id_type,
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            $arr_file = Store::getFile($id_store);
            $arr_id_file = [];
            foreach ($arr_file as $id_file => $v) {
                $arr_id_file[] = $id_file;
            }
            $arr_row = FileUpload::get($arr_id_file);
            foreach ($arr_row as $i => $v) {
                if ($_SESSION['id_group'] == 1 || $file_download) {
                    $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                    $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                } else {
                    $url = '';
                }
                $type = $arr_type[$arr_file[$v['id_file']]];
                $del_table = "store_file";
                $arr_initialPreview[$type][] = $url;
                $file_type = est_to_type(ext($v['filename']));
                $arr_initialPreviewConfig[$type][] = [
                    'type' => $file_type,
                    'filetype' => $v['type'],
                    'caption' => $v['filename'],
                    'size' => $v['size'],
                    'url' => '/manage/Store?&viewstore&id_store=' . $id_store . '&id_type=' . $v['id_type'] . '&table=' . $del_table .'&ajax=1&action=DelFile',
                    'downloadUrl' => $url,
                    'key' => $v['id_file'],
                ];
            };
        }


        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
    }


    public function getUpFile($id)
    {
        return GeelyForRent::getFile($id);
    }

    public function iniUpFileDir()
    {
        $input_name = Tools::getValue('input_name');
        $UpFileVal = Tools::getValue('UpFileVal');
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_store = $UpFileVal['id_store'];
        switch ($input_name) {
            case 'logo':            //無加密
            case 'photo':
            case 'product':
                list($dir, $url) = str_dir_change($id_store);
                $dir = WEB_DIR . DS . STORE_IMG_DIR . DS . $dir;
                break;

            case 'application':        //加密

            case 'contrat':
                $dir = WEB_DIR . DS . FILE_DIR . DS . date('Y') . DS . date('m') . DS . date('d');
                break;
        }
        return $dir;
    }


    public function iniUpFileUrl()
    {
        $input_name = Tools::getValue('input_name');
        $UpFileVal = Tools::getValue('UpFileVal');
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_store = $UpFileVal['id_store'];

        switch ($input_name) {
            case 'logo':            //無加密

            case 'photo':

            case 'product':
                list($dir, $url) = str_dir_change($id_store);
                $url = STORE_IMG_URL . $url;
                break;
            case 'application':        //加密

            case 'contrat':
                $url = FILE_URL . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                break;
        }
        return $url;
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_store = $UpFileVal['id_store'];
        $id_type = $UpFileVal['id_type'];
        foreach ($arr_id_file as $i => $id_file) {
//建案檔案
            $sql = sprintf('INSERT INTO `store_file` (`id_store`, `id_type`, `id_file`) VALUES (%d, %d, %d)',
                GetSQL($id_store, 'int'),
                GetSQL($id_type, 'int'),
                GetSQL($id_file, 'int')
            );

            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }
}
