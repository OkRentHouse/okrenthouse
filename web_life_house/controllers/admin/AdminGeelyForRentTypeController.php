<?php

class AdminGeelyForRentTypeController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminGeelyForRentTypeController';
		$this->table           = 'geely_for_rent_type';
		$this->fields['index'] = 'id_geely_for_rent_type';
		$this->fields['title'] = '租事大吉類別';
		$this->fields['order'] = ' ORDER BY `position` ASC';


		$this->fields['list'] = [
			'id_geely_for_rent_type' => [
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'title'    => [
				'title'  => $this->l('類別名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'position'     => [
				'title'  => $this->l('順序'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'active'                 => [
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['form'] = [
			'geely_for_rent_type' => [
				'legend' => [
					'title' => $this->l('租事大吉類別'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_geely_for_rent_type' => [
						'name'     => 'id_geely_for_rent_type',
						'type'     => 'hidden',
						'index'    => true,
						'label'    => $this->l('類別'),
						'required' => true,
					],
					'geely_for_rent_type'    => [
						'name'      => 'title',
						'type'      => 'text',
						'label'     => $this->l('類別名稱'),
						'maxlength' => '10',
						'required'  => true,
					],
					[
						'name'  => 'position',
						'type'  => 'number',
						'label' => $this->l('順序'),
					],
					'active'                 => [
						'type'   => 'switch',
						'label'  => $this->l('啟用狀態'),
						'name'   => 'active',
						'val'    => 1,
						'p'=> $this->l('啟用的類別會顯示在網站上'),
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function processDel(){
		$id_geely_for_rent_type = Tools::getValue('id_geely_for_rent_type');
		$sql = 'SELECT `id_geely_for_rent`
			FROM `geely_for_rent`
			WHERE `id_geely_for_rent_type` = '.$id_geely_for_rent_type.'
			LIMIT 0, 1';
		$row = Db::rowSQL($sql, true);
		if(count($row)) $this->_errors[] = $this->l('無法刪除正在使用的類別!');
		parent::processDel();
	}
}