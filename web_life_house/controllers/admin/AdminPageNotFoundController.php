<?php
class AdminPageNotFoundController extends AdminController
{
	public function __construct()
	{
		$this->bootstrap = true;
		parent::__construct();
	}

	public function checkAccess()
	{
		return true;
	}

	public function viewAccess()
	{
		return true;
	}

	public function initContent()
	{
		$this->meta_title = $this->l('找不到控制項!');
		$this->_errors[] = $this->l('找不到控制項!');
		parent::initContent();
	}
}