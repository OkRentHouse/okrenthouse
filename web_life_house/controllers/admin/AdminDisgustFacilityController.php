<?php

class AdminDisgustFacilityController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminDisgustFacilityController';
        $this->table           = 'disgust_facility';
        $this->fields['index'] = 'id_disgust_facility';
        $this->fields['title'] = '嫌惡設施類別';
        $this->fields['order'] = ' ORDER BY d_f.`position` ASC';

        $this->_as = 'd_f';
        
        $this->_join    =' LEFT JOIN `disgust_facility_class` AS d_f_c ON d_f_c.`id_disgust_facility_class` = d_f.`id_disgust_facility_class` ';

        $this->_group = ' GROUP BY d_f.`id_disgust_facility`';
//
        $this->fields['list_num'] = 50;

        $this->fields['list'] = [
            'id_disgust_facility' => [
                'filter_key' => 'd_f!id_disgust_facility',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'disgust_facility_class_name'               => [
                'filter_key' => 'd_f_c!disgust_facility_class_name',
                'title'  => $this->l('嫌惡設施類別名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'disgust_facility_name'               => [
                'filter_key' => 'd_f!disgust_facility_name',
                'title'  => $this->l('嫌惡設施名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'remark'              => [
                'filter_key' => 'd_f!remark',
                'title'  => $this->l('備註'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'            => [
                'filter_key' => 'd_f!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            'disgust_facility' => [
                'legend' => [
                    'title' => $this->l('嫌惡類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_disgust_facility' => [
                        'name'     => 'id_disgust_facility',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('類別'),
                        'required' => true,
                    ],

                    'id_disgust_facility_class'          => [
                        'name'      => 'id_disgust_facility_class',
                        'type'      => 'select',
                        'label_col' => 3,
                        'label'     => $this->l('嫌惡類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇嫌惡類別',
                                'val'  => '',
                            ],
                            'table'   => 'disgust_facility_class',
                            'text'    => 'disgust_facility_class_name',
                            'value'   => 'id_disgust_facility_class',
                            'order'   => ' `position` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'disgust_facility_name'  => [
                        'name'      => 'disgust_facility_name',
                        'type'      => 'text',
                        'label'     => $this->l('嫌惡設施類別名稱'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],
                    'remark'              => [
                        'name'      => 'remark',
                        'type'      => 'text',
                        'label'     => $this->l('備註'),
                        'maxlength' => '64',
                    ],
                    'position'            => [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }

    public function processDel()
    {
//        $id_rent_house_types = sprintf('%d', GetSQL(Tools::getValue('id_disgust_facility_class'), 'int'));
//        $sql                 = 'SELECT `id_disgust_facility_class`
//			FROM `disgust_facility`
//			WHERE `id_object_category` LIKE IN \'%"' . $id_rent_house_types . '"%\'
//			LIMIT 0, 1';
//        $row                 = Db::rowSQL($sql, true);
//        if (count($row))
        $this->_errors[] = $this->l('無法刪除類別!');
//        parent::processDel();
    }
}