<?php
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;
use \PHPExcel;

class AdminRentHouseExcelUploadController extends AdminController
{
    public $stay = false;
    public $page = 'customerrequest';
    public function __construct()
    {
        $this->className = 'AdminRentHouseExcelUploadController';
        $this->fields['title'] = '商品新增';
        $this->table           = 'CustomerRequest';
        $this->fields['index'] = 'CustomerRequestID';
        $this->fields['order'] = ' ORDER BY p_.`CustomerRequestID` ASC';
        $this->_as = 'p_';
        $this->fields['list_num'] = 50; //每頁出現筆數
        $this->fields['list']  = [

            'CustomerRequestID' => [
                'filter_key' => 'p_!CustomerRequestID',
                'index'  => true,
                'title' => $this->l('客戶需求單ID'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'HouseArea' => [
                'filter_key' => 'p_!HouseArea',
                'title' => $this->l('區域'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'Source' => [
                'filter_key' => 'p_!Source',
                'title' => $this->l('來源'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'CreateDate' => [
                'filter_key' => 'p_!CreateDate',
                'title' => $this->l('日期'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

        ];

        $this->fields['form'] = [

            'produce_up' => [
                'legend' => [
                    'title' => $this->l('ExcelUpload'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'CustomerRequestID' => [
                        'name' => 'CustomerRequestID',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                    'CreateDate' => [
                        'name' => 'CreateDate',
                        'type' => 'date',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'label' => $this->l('建立時間'),
                        'required'  => true,
                        'disabled' =>true,
                    ],
          
                    'set_new_land_details'=>[
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/excelupload.tpl',
                        'label' => $this->l(''),
                        'no_action'=>true,
                        'name' => 'ad_choice',
                        'form_col'=>12,
                        'col'  => 12,
                    ],
                    

                    
                ],
                
                
            
            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],
            'reset' => [
                'title' => $this->l('復原'),
            ],
        ],
        ];
        


        parent::__construct();
    }

    public function initToolbar()
    {        //初始化功能按鈕
        parent::initToolbar();
        if ($this->stay) {
            unset($this->toolbar_btn['save']);
        }
    }


    public function initProcess()
    {
        
        if(isset($_POST['excel'])){
            // $file = $_FILES['excel']['tmp_name'];
            // $obj = PHPExcel_IOFactory::load($_POST['excel']);
            // foreach($obj->getWorksheetIterator() as $sheet){
            //     echo '<pre>';
            //     print_r($sheet);
            // }
            $target_path = "./img/store_img/";
            $getDate= date("Ymdhis");
            $target_path = $target_path . basename( $getDate.$_FILES['uploadedfile']['name']);
            $url = str_replace(WEB_DIR, '', $target_path . $_FILES['uploadedFile']['name']);
            $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
            $file = $_FILES['excel']['tmp_name'];
            $fileName = $_FILES['excel']['name'];

            //$sheetname = 'mysheet'; // 如果要指定工作表
            //$objReader->setLoadSheetsOnly($sheetname);
            
            $type = PHPExcel_IOFactory::identify($file); //自動偵測檔案格式
            //$type = 'Excel2007'; //也可以手動指定
            $objReader = PHPExcel_IOFactory::createReader($type);
            $objPHPExcel = $objReader->load($file); //讀取檔案
            
            echo ' 欄數: ' . $getHighestColumn = $objPHPExcel->setActiveSheetIndex()->getHighestColumn(); // 取得寬有幾格
            echo ' 行數: ' . $getHighestRow = $objPHPExcel->setActiveSheetIndex()->getHighestRow(); // 共有幾行
            
            // 設定資料區間，設定寬度區間為AC(29格)，行數則是取 $getHighestRow，如果不要第一行標題可以把A1改為A2跳過第一行
            $range = "A1:AC".$getHighestRow; 
            
            // 不設區間，把所有資料取出
            //$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true); 
            
            //取出區間資料(陣列)
            $sheetData = $objPHPExcel->getActiveSheet()->rangeToArray($range);
        }
        
        parent::initProcess();
        
       
    }

    public function processEdit()
    {
        //echo "processEdit";
        //parent::processEdit();
        echo "1";
        exit;
    }

    public function processAdd()
    {
        echo "processAdd";
        //parent::processAdd();
    }

    public function processDel()
    {
        echo "processDel";
        parent::processDel();
    }

    public function processSave()
    {
        //echo "processSave";
        //parent::processSave();
        echo "<script>window.location='https://lifegroup.house/manage/RentHouseCustomerRequest';</script>";
    }
}
