<?php


class AdminMemberController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminMemberController';
		$this->table           = 'member';
		$this->_as             = 'm';
		$this->_join           = ' LEFT JOIN `repair_ag` AS ra ON ra.`id_member` = m.`id_member`
								   LEFT JOIN `member_additional` AS m_a ON m_a.`id_member_additional` = m.`id_member_additional`
								    ';
		$this->fields['index'] = 'id_member';
		$this->fields['where'] = " and m.`user` is not null and m.`active` = 1 ";
		$this->fields['group'] = ' GROUP BY m.`id_member` ';
		//$this->fields['order'] = ' ORDER by m.`id_member` DESC';
		$this->fields['title'] = '會員管理';

        $sql = 'SELECT * FROM member_additional ORDER BY `position` ASC';
        $member_additional_arr = Db::rowSQL($sql);

        foreach($member_additional_arr as $value){
            $member_additional[] = [
                'id' => 'member_additional'.$value['id_member_additional'],
                'value' => $value['id_member_additional'],
                'label' => $this->l($value['name'])
            ];
        }
		$this->fields['list_footer_include_tpl'] = 'themes/LifeHouse/member_count.tpl';

		$this->fields['list'] = [
			'id_member' => [
				'filter_key' => 'm!id_member',
				'index'      => true,
				'title'      => $this->l('會員編號'),
				'type'       => 'checkbox',
				'hidden'     => true,
				'class'      => 'text-center',
			],
			'user'      => [
				'filter_key' => 'm!user',
				'title'      => $this->l('帳號'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			'name'      => [
				'filter_key' => 'm!name',
				'title'      => $this->l('姓名'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],

            'nickname'      => [
                'filter_key' => 'm!nickname',
                'title'      => $this->l('暱稱'),
                'order'      => true,
                'filter'     => true,
                'class'      => 'text-center',
            ],

			// 'gender'    => [
			// 	'filter_key' => 'm!gender',
			// 	'title'      => $this->l('性別'),
			// 	'order'      => true,
			// 	'filter'     => true,
			// 	'class'      => 'text-center',
			// 	'values'     => [
			// 		[
			// 			'class' => 'gender1',
			// 			'value' => 1,
			// 			'title' => $this->l('男'),
			// 		],
			// 		[
			// 			'class' => 'gender0',
			// 			'value' => 0,
			// 			'title' => $this->l('女'),
			// 		],
			// 	],
			// ],
			'email'     => [
				'filter_key' => 'm!email',
				'title'      => $this->l('email'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			'point'     => [
				'filter_key' => 'm!point',
				'title'      => $this->l('會員點數'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			// 'tel'       => [
			// 	'filter_key' => 'm!tel',
			// 	'title'      => $this->l('連絡電話'),
			// 	'order'      => true,
			// 	'filter'     => true,
			// 	'class'      => 'text-center',
			// ],
			// 'time'      => [
			// 	'filter_key' => 'm!time',
			// 	'title'      => $this->l('連絡時間'),
			// 	'order'      => true,
			// 	'filter'     => 'select',
			// 	'class'      => 'text-center',
			// ],
			//			'barcode'   => [
			//				'show'   => false,
			//				'title'  => $this->l('會員條碼'),
			//				'order'  => true,
			//				'filter' => true,
			//				'class'  => 'text-center',
			//			],
			// 'ag'        => [
			// 	'filter_key' => 'ra!ag',
			// 	'title'      => $this->l('AG'),
			// 	'key'        => [
			// 		'table'       => 'admin',
			// 		'key'         => 'ag',
			// 		'text_divide' => '%s - %s%s',  //sprintf  (%s - %s%s)
			// 		'val_divide'  => '%s - %s%s',
			// 		'val'         => ['ag', 'last_name', 'first_name'],
			// 		'where'       => ' AND `ag` <> "" AND `id_group` <> 1',
			// 		'order'       => '`active` DESC, `ag` ASC',
			// 	],
			// 	'order'      => true,
			// 	'filter'     => true,
			// 	'multiple'   => true,					//<select data-live-search="true">
			// 	'data'       => [
			// 		'live-search' => 'true',
			// 	],
			// 	'class'      => 'text-center selectpicker',
			// ],
			'active'    => [
				'filter_key' => 'm!active',
				'title'      => $this->l('啟用狀態'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			'id_member_additional'    => [
				'filter_key' => 'm!id_member_additional',
				'title'      => $this->l('會員身分'),
				'order'      => false,
				'filter'     => true,
				'class'      => 'text-center',
				'in_table' => true,
				'multiple' => true,
				'values'  => [
					[
						'value' => '1',
						'like' => '"1"',
						'title' => $this->l('一般權限'),
						'append' => true,
					],
					[
						'value' => '2',
						'like' => '"2"',
						'title' => $this->l('店家身分'),
						'append' => true,
					],
					[
						'value' => '3',
						'like' => '"3"',
						'title' => $this->l('房仲身分'),
						'append' => true,
					],
					[
						'value' => '4',
						'like' => '"4"',
						'title' => $this->l('房東身分'),
						'append' => true,
					],
					[
						'value' => '5',
						'like' => '"5"',
						'title' => $this->l('裝修達人'),
						'append' => true,
					],
					[
						'value' => '6',
						'like' => '"6"',
						'title' => $this->l('代理人'),
						'append' => true,
					],
					[
						'value' => '7',
						'like' => '"7"',
						'title' => $this->l('圈主'),
						'append' => true,
					],
					[
						'value' => '8',
						'like' => '"8"',
						'title' => $this->l('圈友'),
						'append' => true,
					],
				],
			],
			'date_time' => [
				'filter_key' => 'm!date_time',
				'title'      => $this->l('註冊日期'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			'LoginCheck' => [
				'filter_key' => 'm!LoginCheck',
				'title'      => $this->l('登入日期'),
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
			'tpl'=>'themes/LifeHouse/set_nameCard.tpl',
		];

		$this->fields['list_num'] = 100;

		$this->fields['form'] = [
			'member' => [
				'tab'    => 'member',
				'legend' => [
					'title' => $this->l('會員管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_member'        => [
						'name'     => 'id_member',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					//					'barcode_img'      => [
					//						'no_action' => true,
					//						'type'      => 'html',
					//						'html'      => '<img id="barcode_img" src="" title="' . $this->l('複製') . '" class="pointer">',
					//					],
					'user'             => [
						'name'        => 'user',
						'type'        => 'tel',
						'label'       => $this->l('帳號'),
						'placeholder' => $this->l('帳號'),
						'p'           => $this->l('電話'),
						'maxlength'   => '100',
						'required'    => true,
					],
					'change_password'  => [
						'type'        => 'change-password',
						'label'       => $this->l('修改密碼'),
						'placeholder' => $this->l('修改密碼'),
						'col'         => 5,
						'maxlength'   => '20',
						'name'        => 'password',
					],
					'password'         => [
						'type'        => 'new-password',
						'label'       => $this->l('密碼'),
						'placeholder' => $this->l('密碼'),
						'required'    => true,
						'maxlength'   => '20',
						'name'        => 'password',
					],
					'confirm_password' => [
						'type'        => 'confirm-password',
						'label'       => $this->l('確認密碼'),
						'placeholder' => $this->l('確認密碼'),
						'required'    => true,
						'maxlength'   => '20',
						'name'        => 'password2',
						'confirm'     => 'password',
					],
					'name'             => [
						'name'        => 'name',
						'type'        => 'text',
						'label'       => $this->l('姓名'),
						'placeholder' => $this->l('姓名'),
						//						'p'           => $this->l('系統收發信件依據，請填寫正確姓名'),
						//						'hint'        => $this->l('系統收發信件依據，請填寫正確姓名'),
						'maxlength'   => '20',
					],
                    'nickname'             => [
                        'name'        => 'nickname',
                        'type'        => 'text',
                        'label'       => $this->l('暱稱'),
                        'placeholder' => $this->l('暱稱'),
                        'maxlength'   => '20',
                    ],

					// 'gender'           => [
					// 	'name'     => 'gender',
					// 	'type'     => 'switch',
					// 	'label'    => $this->l('性別'),
					// 	'val'      => 1,
					// 	'values'   => [
					// 		[
					// 			'id'    => 'gender1',
					// 			'value' => 1,
					// 			'label' => $this->l('男'),
					// 		],
					// 		[
					// 			'id'    => 'gender0',
					// 			'value' => 0,
					// 			'label' => $this->l('女'),
					// 		],
					// 	],
					// ],
					'email'            => [
						'name'      => 'email',
						'type'      => 'email',
						'label'     => $this->l('email'),
						'maxlength' => '100',
					],
					// 'tel'              => [
					// 	'name'        => 'tel',
					// 	'type'        => 'text',
					// 	'label'       => $this->l('連絡電話'),
					// 	'placeholder' => $this->l('連絡電話'),
					// 	'maxlength'   => '50',
					// ],
					// 'time'             => [
					// 	'name'        => 'time',
					// 	'type'        => 'text',
					// 	'label'       => $this->l('連絡時間'),
					// 	'placeholder' => $this->l('連絡時間'),
					// 	'maxlength'   => '50',
					// ],
                    'id_member_additional' => [
                        'type' => 'checkbox',
                        'label' => $this->l('其他身分(複選)'),
                        'name' => 'id_member_additional',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $member_additional,
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                    ],
					'date_time'        => [
						'name'          => 'date_time',
						'type'          => 'view',
						'auto_datetime' => 'add',
						'label'         => $this->l('註冊日期'),
						'maxlength'     => '50',
					],
					'active'           => [
						'type'   => 'switch',
						'label'  => $this->l('啟用狀態'),
						'name'   => 'active',
						'val'    => 1,
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
					// 'id_member_additional' => [
					// 	'filter_key' => 'm!id_member_additional',
					// 	'title' => $this->l('精選'),
					// 	'order' => true,
					// 	'filter' => true,
					// 	'values' => [
					// 		[
					// 			'class' => 'id_member_additional',
					// 			'value' => 1,
					// 			'title' => $this->l('精選'),
					// 		],
					// 		[
					// 			'class' => 'id_member_additional',
					// 			'value' => '0',
					// 			'title' => $this->l('無'),
					// 		],
					// 	],
					// 	'class' => 'text-center',
					// ],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],

			'ag'     => [
				'table'  => 'repair_ag',
				'tab'    => 'member',
				'legend' => [
					'title' => $this->l('AG'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_member' => [
						'name'     => 'id_member',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'ag'        => [
						'name'    => 'ag',
						'label'   => $this->l('AG'),
						'type'    => 'select',
						'options' => [
							'table'       => 'admin',
							'text'        => ['ag', 'last_name', 'first_name'],
							'text_divide' => '%s - %s%s',
							'value'       => 'ag',
							'where'       => ' AND `ag` <> "" AND `id_group` <> 1',
							'order'       => '`active` DESC, `ag` ASC',
							'default'     => [
								'val'  => '',
								'text' => $this->l('請選擇AG'),
							],
						],
						'data' => [
							'live-search' => 'true',
						],
						'class'   => 'text-center selectpicker',
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		//'SELECT `id_member_additional`,COUNT(`id_member_additional`) FROM `member` GROUP BY `id_member_additional` ASC'
		$sql = sprintf('SELECT TRIM(REPLACE(`id_member_additional`, char(9), "")) as `id_member_additional`,COUNT(`id_member_additional`) as `additional_COUNT` FROM `member` WHERE `id_member_additional` <> "" GROUP BY TRIM(REPLACE(`id_member_additional`, char(9), "")) ASC');
		$row = Db::rowSQL($sql);



		parent::__construct();

		$this->context->smarty->assign([
				'member_additional_row'=>$row,
		]);
	}

	public function initProcess()
	{
		parent::initProcess(); // TODO: Change the autogenerated stub

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				unset($this->fields['list']['action']);
				unset($this->fields['form']['member']['input']['action']);
			}
		}

		if ($this->display == 'edit') {
			unset($this->fields['form']['member']['input']['password']);
			$this->fields['form']['member']['input']['user']['type'] = 'view';
		}

		if ($this->display == 'add') {
			unset($this->fields['form']['member']['input']['barcode_img']);
			unset($this->fields['form']['member']['input']['barcode']);
			unset($this->fields['form']['member']['input']['date_time']);
		}
	}

	public function processAdd()
	{
		$tt = false;
		while (!$tt) {        //產生不重複會員條碼修繕AG
			$barcode = random16number(16);
			$sql     = sprintf('SELECT `id_member` FROM `member` WHERE `barcode` = %s LIMIT 0, 1',
				GetSQL($barcode, 'text'));
			$row     = Db::rowSQL($sql, true);
			if (empty($row['id_member'])) {
				$tt = true;
			}
		}

		$_POST['barcode'] = $barcode;

		$this->fields['form'][0]['input']['barcode'] = [
			'name' => 'barcode',
			'type' => 'text',
		];

		parent::processAdd();
	}

	public function processEdit()
	{
		$ag = Tools::getValue('ag');
		$id = Tools::getValue('id_member');
		$sql = sprintf('INSERT INTO `repair_ag` (`id_member`, `ag`) VALUES (%d, %s)',
			GetSQL($id, 'int'),
			GetSQL($ag, 'text')
		);
		Db::rowSQL($sql);
		return parent::processEdit();
	}

	public function displayAjaxMemberSearch()
	{
		$search     = Tools::getValue('search');
		$num        = 0;
		$arr_member = [];
		if (!empty($search)) {
			$sql = 'SELECT `id_member`, `user`, `name`
				FROM `member`
				WHERE `user` LIKE "' . $search . '%"
					OR `name` LIKE "' . $search . '%"';
			$db  = new db_mysql($sql);
			$num = $db->num();
			if ($db->num() > 0) {
				while ($member = $db->next_row()) {
					if (empty($member['name']))
						$member['name'] = '-';
					$arr_member[] = [$member['id_member'], $member['user'], $member['name']];
				};
			};
		};
		$data = [
			'num'        => $num,
			'arr_member' => $arr_member,
		];
		$json = new Services_JSON();
		echo $json->encode($data);
		exit;
	}

	public function displayAjaxMemberSearchId()
	{
		$id     = Tools::getValue('id');
		$num    = 0;
		$member = [];
		if ($id > 0) {
			$sql = sprintf('SELECT `id_member`, `user`, `name`
				FROM `member`
				WHERE `id_member` = %d
				LIMIT 0, 1', $id);
			$db  = new db_mysql($sql);
			$num = $db->num();
			if ($db->num() > 0) {
				$member = $db->row();
				if (empty($member['name']))
					$member['name'] = '-';
			};
		};
		$data = [
			'num'       => $num,
			'id_member' => $member['id_member'],
			'user'      => $member['user'],
			'name'      => $member['name'],
		];
		$json = new Services_JSON();
		echo $json->encode($data);
		exit;
	}
}
