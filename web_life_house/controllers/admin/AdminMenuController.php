<?php

class AdminMenuController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->tabAccess['add'] = false;
	}

	public function initProcess()
	{
		parent::initProcess();
		$id_menu_code = Tools::getValue('id_menu_code');
		$arr_web      = Menu::getContext()->getMenuCode($id_menu_code);

		$arr_page = [];

		if (!empty($id_menu_code) && count($arr_web)) {
			$dir      = $arr_web[0]['dir'];
			$arr_page = $this->getFrontPage($dir);
		}

		$this->context->smarty->assign([
			'dir'      => $dir,
			'arr_web'  => $arr_web,
			'arr_page' => $arr_page,
		]);
	}

	public function getFrontPage($dir)
	{
		if (empty($dir)) {
			return false;
		}

		$arr_file = File::getContext()->getFileList(WEB_DIR . DS . $dir . DS . 'controllers' . DS . 'front');
		foreach ($arr_file as $i => $file) {
			include_once(WEB_DIR . DS . $dir . DS . 'controllers' . DS . 'front' . DS . $file);
			$controller = str_replace('.php', '', $file);
			$class      = $dir . '\\' . $controller;
			if (class_exists($class, false)) {
				$Controller = new $class();
				if (!empty($Controller->page) && !$Controller->no_page) {
					$val  = $Controller->page;
					$text = $val;
					if (!empty($Controller->meta_title)) {
						$text .= ' (' . $Controller->meta_title . ')';
					}
					$arr_page[] = [
						'val'  => $val,
						'text' => $text,
					];
				}
			}
		}
		return $arr_page;
	}
}