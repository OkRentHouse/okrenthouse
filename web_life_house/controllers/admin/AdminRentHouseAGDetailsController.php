<?php

class AdminRentHouseAGDetailsController extends AdminController
{
	public $stay = false;
	public $page = 'managemodule';


	public function __construct()
	{
        $months = Tools::getValue('rent_houseFilter_submit_date');
        if($months ==''){
            $months = "1";
        }
        $this->className = 'AdminRentHouseAGDetailsController';
        $db_link = mysqli_connect("localhost", "ilifehou_ilife", "S^,HO%bh^$&NF3dab", "ilifehou_ilife_house");
        $sql_split ="SELECT ag FROM rent_house where  date_format(submit_date,'%Y-%m') = date_format(DATE_SUB(curdate(), INTERVAL $months MONTH),'%Y-%m') 
        and ag is not null and ag != '' ";
        $result = mysqli_query($db_link,$sql_split); 
        $i = 0;
        while($row = mysqli_fetch_array($result)){	
            $arr = explode("/",$row['ag']); 
            foreach($arr as $v => $value){ 
                $this->table = 'rent_house';
                $this->fields['index'] = 'id_rent_house';
                $this->_as = 'r';
                $this->fields['title'] = '物件列表';
                $this->fields['where'] = " and date_format(submit_date,'%Y-%m') = date_format(DATE_SUB(curdate(), INTERVAL $months MONTH),'%Y-%m')  
                and r.`ag` like '%".$value[$i]."%' and ag is not null ";
                $this->_group = ' GROUP BY r.`submit_date` ';
                $this->fields['order'] = ' ORDER BY r.`ag` DESC';



                $i++;        
            }
        }
        
    $this->fields['list_num'] = 50; //每頁出現筆數

		if ((Tools::getValue('id_admin') == $_SESSION['id_admin']) && $_SESSION['admin'] != 1 && $_SESSION['id_group'] != 1) {
			$this->stay = true;
		}

		$this->fields['list']  = [

            'ag' => [
                'filter_key' => 'r!ag',
								'index'  => true,
                'title' => $this->l('AG'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],
            
            // 'total' => [
            //     'filter_key' => 'r!total',
            //     'title' => $this->l('123'),
            //     'order' => true,
            //     'filter' => true,
            //     'class' => 'text-center',
            //     ],

                
            'id_rent_house' => [
                'filter_key' => 'r!id_rent_house',
                'title' => $this->l('物件連結'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],

            'case_name' => [
                'filter_key' => 'r!case_name',
                'title' => $this->l('案名'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],

            'submit_date' => [
                'filter_key' => 'r!submit_date',
                'title' => $this->l('搜尋 ex: 搜尋上個月請打1,搜尋兩個月前請打2'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],

            ];

						$this->fields['form'] = [
							[
								'input'  => [
				            'AG' => [
				                'name' => 'AG',
				                'label' => $this->l('AG'),
				                'type' => 'text',
				                'form_col' => 12,
				                'label_col' => 1,
				                'col' => 3,
				                'index' => true,
				                'required' => true,
                                ],
                            // 'total' => [
                            //     'name' => 'total',
                            //     'label' => $this->l('123'),
                            //     'type' => 'text',
                            //     'form_col' => 12,
                            //     'label_col' => 1,
                            //     'col' => 3,
                            //     'required' => true,
                            //     ],

				            'rent_house_code' => [
				                'name' => 'rent_house_code',
				                'label' => $this->l('物件連結'),
				                'type' => 'text',
				                'form_col' => 12,
				                'label_col' => 1,
				                'col' => 3,
				                'required' => true,
				                ],

				            'case_name' => [
				                'name' => 'case_name',
				                'label' => $this->l('案名'),
				                'type' => 'text',
				                'form_col' => 12,
				                'label_col' => 1,
				                'col' => 3,
				                'required' => true,
                                ],
                                'submit_date' => [
                                    'name' => 'submit_date',
                                    'label' => $this->l('案名'),
                                    'type' => 'text',
                                    'form_col' => 12,
                                    'label_col' => 1,
                                    'col' => 3,
                                    'required' => true,
                                    ],
										],




				            'submit' => [
				                   [
				                   'title' => $this->l('儲存'),
				                   ],
				                   [
				                   'title' => $this->l('儲存並繼續編輯'),
				                   'stay' => true,
				                   ],
				                ],
				                'cancel' => [
				                'title' => $this->l('取消'),
				                ],
				                'reset'  => [
				                'title' => $this->l('復原'),
				                ],
												],
				            ];


		parent::__construct();
	}

	public function initToolbar()
	{        //初始化功能按鈕
		parent::initToolbar();
		if ($this->stay)
			unset($this->toolbar_btn['save']);
	}

	public function initProcess()
	{


        $id_admin = Tools::getValue('id_admin');


		parent:: initProcess();

	}

	public function processEdit()
	{
		echo "processEdit-1";
		parent::processEdit();
	}

	public function processAdd()
	{
		echo "processAdd";
		parent::processAdd();
		echo "processAdd";
	}

	public function processDel()
	{
		echo "processDel";
		parent:: processDel();
	}

	public function processSave()
	{
		echo "processSave";
		parent:: processSave();
	}

	public function validateRules()
	{

		parent::validateRules();
	}

	public function processSetMyFieldList()
	{

		parent::processSetMyFieldList();
	}

	public function processdelMyFieldList()
	{

		parent::processdelMyFieldList();
	}

}

