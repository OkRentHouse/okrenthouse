<?php

class AdminMemberAdditionalController extends AdminController
{
    public function __construct()
    {
        $this->className = 'AdminMemberAdditionalController';
        $this->table           = 'member_additional';
        $this->fields['index'] = 'id_member_additional';
        $this->fields['title'] = '會員身分';
        $this->fields['order'] = ' ORDER BY `position` ASC';

        $this->fields['list'] = [
            'id_member_additional' => [
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'name'               => [
                'title'  => $this->l('選項名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'remark'              => [
                'title'  => $this->l('備註'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'            => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],

            'active'              => [
                'title'  => $this->l('啟用狀態'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'create_time' =>[
                'create_time' => $this->l('建立時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ]
        ];

        $this->fields['form'] = [
            'id_member_additional' => [
                'legend' => [
                    'title' => $this->l('會員身分'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_member_additional' => [
                        'name'     => 'id_member_additional',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('型態'),
                        'required' => true,
                    ],
                    'name'    => [
                        'name'      => 'name',
                        'type'      => 'text',
                        'label'     => $this->l('選項名稱'),
                        'maxlength' => '10',
                        'required'  => true,
                    ],
                    'remark'              => [
                        'name'      => 'remark',
                        'type'      => 'text',
                        'label'     => $this->l('備註'),
                        'maxlength' => '64',
                    ],
                    'position'            => [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                    'active'              => [
                        'type'   => 'switch',
                        'label'  => $this->l('啟用狀態'),
                        'name'   => 'active',
                        'val'    => 1,
                        'values' => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }
}