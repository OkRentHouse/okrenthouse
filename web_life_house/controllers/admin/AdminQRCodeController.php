<?php

class AdminQRCodeController extends AdminController
{
	public $page = 'qr_code';

	public function __construct()
	{
		$this->className       = 'AdminQRCodeController';
		$this->fields['title'] = 'QR Code設定';


		$this->fields['form'] = [
			'web_set_up' => [
				'tab'    => 'web_set_up',
				'legend' => [
					'title' => $this->l('QR Code設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'qr_code_link'                               => [
						'name'          => 'qr_code_link',
						'type'          => 'text',
						'label'         => $this->l('QR Code連線網址'),
						'col'           => 6,
						'placeholder'   => 'https://www.xxxxxxxx.com.tw',
						'p'             => $this->l('<a href="https://www.lifegroup.house/QR_Code" target="_blank">https://www.lifegroup.house/QR_Code</a><br>掃描QR_Code後會自動轉到指定網址'),
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'options';
		parent::initContent();
	}
}