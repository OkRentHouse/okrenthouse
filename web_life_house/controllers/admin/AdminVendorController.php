<?php

class AdminVendorController extends AdminController
{
    public $no_link = false;

    public function __construct()
    {
        $this->conn = 'ilifehou_life_go';
        $this->className            = 'AdminVendorController';
        $this->table                = 'vendor';
        $this->fields['index']      = 'id_vendor';
        $this->fields['title']      = '供應商';

        $this->_as = 'v';
//        $this->_group = ' GROUP BY p_c_i.`id_product_class_item`';
        $this->fields['order'] = ' ORDER BY v.`id_vendor` DESC';


//        $this->actions[]            = 'list';
        $this->fields['list']       = [
            'id_product_class_item'          => [
                'index'  => true,
                'filter_key' => 'v!id_vendor',
                'title'  => $this->l('供應商ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'name'    => [
                'filter_key' => 'v!name',
                'title'  => $this->l('供應商名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'address'    => [
                'filter_key' => 'v!address',
                'title'  => $this->l('地址'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'uniform'    => [
                'filter_key' => 'v!uniform',
                'title'  => $this->l('統編'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'legal_agent'    => [
                'filter_key' => 'v!legal_agent',
                'title'  => $this->l('法定代理人'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'legal_agent_tel'    => [
                'filter_key' => 'v!legal_agent_tel',
                'title'  => $this->l('法定代理人連絡電話'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'submit_date'    => [
                'filter_key' => 'v!submit_date',
                'title'  => $this->l('建檔時間'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'active' => [
                'filter_key' => 'v!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

        ];



        $this->fields['form'] = [
            [
                'tab' => 'vendor',

                'legend' => [
                    'title' => $this->l('供應商'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    [
                        'name'     => 'id_vendor',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],
                    [
                        'name'      => 'name',
                        'type'      => 'text',
                        'label'     => $this->l('名稱'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],
                    [
                        'name'      => 'address',
                        'type'      => 'text',
                        'label'     => $this->l('地址'),
                        'maxlength' => '255',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],
                    [
                        'name'      => 'tel',
                        'type'      => 'text',
                        'label'     => $this->l('供應商電話號碼'),
                        'maxlength' => '32',
                    ],
                    [
                        'name'      => 'uniform',
                        'type'      => 'text',
                        'label'     => $this->l('統一編號'),
                        'maxlength' => '32',
                    ],
                    [
                        'name'      => 'legal_agent',
                        'type'      => 'text',
                        'label'     => $this->l('法定代理人'),
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'maxlength' => '32',
                    ],
                    [
                        'name'      => 'legal_agent_tel',
                        'type'      => 'text',
                        'label'     => $this->l('法定代理人電話號碼'),
                        'form_col' => 6,
                        'label_col' => 4,
                        'col' => 6,
                        'maxlength' => '32',
                    ],
                    [
                        'name'      => 'introduction',
                        'type'      => 'textarea',
                        'label'     => $this->l('供應商介紹'),
                        'class'     => 'tinymce',
                        'label_col' => 3,
                        'col'       => 9,
                        'rows'      => 30,
                    ],
                    'id_admin' => [
                        'name' => 'id_admin',
                        'label' => $this->l('建檔人'),
                        'type' => 'hidden',
                        'val' => $_SESSION['id_admin'],
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 3,
                    ],
                    'submit_date' => [
                        'name' => 'submit_date',
                        'label' => $this->l('建檔日期'),
                        'type' => 'view',
                        'auto_datetime' => 'add',        //新增的時候加入
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 3,
                    ],
                    [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],

                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],

                        ],
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
        parent::__construct();
    }



    public function initProcess(){
        parent::initProcess();
    }
}