<?php

class AdminAppBannerController extends AdminController
{
	public $tpl_folder;    //樣版資料夾
	public $arr_file_type = [
		'index_top',
		'index_news',
		'index_service',
		'index_life',
		'news_top',
		'news_find',
		'news_to',
		'news_give',
		'to_top',
		'to_1',
		'to_2',
		'give_top',
		'give_1',
		'give_2',
	];

	public function __construct()
	{
		$this->className       = 'AdminAppBannerController';
		$this->fields['title'] = 'i 生活APP Banner設定';

		$this->fields['tabs'] = [
			'index' => $this->l('首頁'),
			'news'  => $this->l('NEWS好康'),
			'to'    => $this->l('要好康'),
			'give'  => $this->l('給好康'),
		];

		$this->fields['form'] = [
			'index_top'     => [
				'tab'    => 'index',
				'legend' => [
					'title' => $this->l('首頁[上方] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'index_top' => [
						'name'      => 'index_top',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							//														'max'              => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
							//							'resizePreference' => 'height',
						],
						//						'multiple'  => true,
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 400*230px'),
					],
				],
			],
			'index_news'    => [
				'tab'    => 'index',
				'legend' => [
					'title' => $this->l('首頁[News好康] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'index_news' => [
						'name'      => 'index_news',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 3,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
							//							'resizePreference' => 'height',
						],
						//						'multiple'  => true,
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 400*230px, 最多上傳3張圖片'),
					],
				],
			],
			'index_service' => [
				'tab'    => 'index',
				'legend' => [
					'title' => $this->l('首頁[樂租服務] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'index_service' => [
						'name'      => 'index_service',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 3,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 400*230px, 最多上傳3張圖片'),
					],
				],
			],
			'index_life'    => [
				'tab'    => 'index',
				'legend' => [
					'title' => $this->l('首頁[生活家族] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'index_life' => [
						'name'      => 'index_life',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 3,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 400*230px, 最多上傳3張圖片'),
					],
				],
			],
			'news_top'      => [
				'tab'    => 'news',
				'legend' => [
					'title' => $this->l('NEWS好康[上方] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'news_top' => [
						'name'      => 'news_top',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 500*280px'),
					],
				],
			],
			'news_find'     => [
				'tab'    => 'news',
				'legend' => [
					'title' => $this->l('NEWS好康[找好康] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'news_find' => [
						'name'      => 'news_find',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('最多上傳1張圖片'),
					],
				],
			],
			'news_to'       => [
				'tab'    => 'news',
				'legend' => [
					'title' => $this->l('NEWS好康[要好康] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'news_to' => [
						'name'      => 'news_to',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('最多上傳1張圖片'),
					],
				],
			],
			'news_give'     => [
				'tab'    => 'news',
				'legend' => [
					'title' => $this->l('NEWS好康[給好康] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'news_give'     => [
						'name'      => 'news_give',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('最多上傳1張圖片'),
					],
					'news_give_txt' => [
						'name'          => 'news_give_txt',
						'label'         => $this->l('Banner文字'),
						'label_col'=>12,
						'label_class' => 'text-left',
						'type'          => 'text',
						'col'           => 12,
						'configuration' => true,
						'p'             => $this->l('Banner上面的文字'),
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
			'to_top'        => [
				'tab'    => 'to',
				'legend' => [
					'title' => $this->l('要好康[上方] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'to_top' => [
						'name'      => 'to_top',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 500*280px'),
					],
				],
			],
			'to_1'          => [
				'tab'    => 'to',
				'legend' => [
					'title' => $this->l('要好康 [圖1]'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'to_1' => [
						'name'      => 'to_1',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('最多上傳1張圖片'),
					],
				],
			],
			'to_2'          => [
				'tab'    => 'to',
				'legend' => [
					'title' => $this->l('要好康 [圖2]'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'to_2' => [
						'name'      => 'to_2',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('最多上傳1張圖片'),
					],
				],
			],
			'give_top'      => [
				'tab'    => 'give',
				'legend' => [
					'title' => $this->l('給好康[上方] Banner'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'give_top' => [
						'name'      => 'give_top',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('圖片建議大小 500*280px'),
					],
				],
			],
			'give_1'        => [
				'tab'    => 'give',
				'legend' => [
					'title' => $this->l('給好康 [圖1]'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'give_1' => [
						'name'      => 'give_1',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('最多上傳1張圖片'),
					],
				],
			],
			'give_2'        => [
				'tab'    => 'give',
				'legend' => [
					'title' => $this->l('給好康 [圖2]'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'give_2' => [
						'name'      => 'give_2',
						'no_label'  => true,
						'type'      => 'file',
						'language'  => 'zh-TW',
						'file'      => [
							'icon'        => false,
							'auto_upload' => true,
							'max'         => 1,
							'allowed'     => ['jpg', 'png', 'gif', 'jpeg'],
						],
						'col'       => 12,
						'no_action' => true,
						'p'         => $this->l('最多上傳1張圖片'),
					],
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'options';
		parent::initContent();
	}

	public function getUpFileList()
	{
		$this->arr_file_type = [
			'index_top',
			'index_news',
			'index_service',
			'index_life',
			'news_top',
			'news_find',
			'news_to',
			'news_give',
			'to_top',
			'to_1',
			'to_2',
			'give_top',
			'give_1',
			'give_2',
		];

		$json      = new JSON();
		$UpFileVal = [];
		foreach ($this->arr_file_type as $i => $type_neme) {
			$UpFileVal[$type_neme] = '\'' . $json->encode([
					'type' => $type_neme,
				]) . '\'';
		}

		$file_up       = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
		$file_move     = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
		$file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
		$file_del      = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');

		if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
			foreach ($this->arr_file_type as $i_file_type => $type_name) {
				$arr_id_file = Config::get('banner_' . $type_name, true);
//				switch ($type_name) {
//					//幻燈片 N 張
//					case 'index_top' :
//					case 'news_to' :
//					case 'to_top' :
//					case 'give_top' :
//						//幻燈片 限 3 張
//					case 'index_news' :
//					case 'index_service' :
//					case 'index_life' :
//						//只有 1 張圖片
//					case 'news_top' :
//					case 'news_find' :
//					case 'news_give' :
//					case 'to_1' :
//					case 'to_2' :
//					case 'give_1' :
//					case 'give_2' :
//
//					break;
//				}

				$arr_row = FileUpload::get($arr_id_file);
				foreach ($arr_row as $i => $v) {
					if ($_SESSION['id_group'] == 1 || $file_download) {
						$url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
						$url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
					} else {
						$url = '';
					}

					$arr_initialPreview[$type_name][]       = $url;
					$file_type                              = est_to_type(ext($v['filename']));
					$arr_initialPreviewConfig[$type_name][] = [
						'type'        => $file_type,
						'filetype'    => $v['type'],
						'caption'     => $v['filename'],
						'size'        => $v['size'],
						'url'         => '/manage/AppBanner?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&ajax=1&action=DelFile',
						'downloadUrl' => $url,
						'key'         => $v['id_file'],
					];
				}
			}
		}

		foreach ($arr_initialPreview as $i => $v) {
			$arr_initialPreview[$i]       = $json->encode($arr_initialPreview[$i]);
			$arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
		}

		Context::getContext()->smarty->assign([
			'initialPreview'       => $arr_initialPreview,
			'initialPreviewConfig' => $arr_initialPreviewConfig,
			'UpFileVal'            => $UpFileVal,
		]);
	}

	public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
	{
		$json        = new JSON();
		$UpFileVal   = (array)$json->decode($UpFileVal);
		$arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
		$type        = $UpFileVal['type'];

		if (!in_array($type, $this->arr_file_type)) {
			return false;
		}

		$arr_banner_file = Config::get('banner_' . $type);
		$arr_banner_file = (array)$json->decode($arr_banner_file);
		$arr_banner_file = array_merge($arr_banner_file, $arr_id_file);

		Config::set('banner_' . $type, $arr_banner_file);    //array自動轉JSON存入
		return true;
	}

	public function displayAjaxDelFileAction()
	{
		$id_file     = Tools::getValue('id_file');
		$type_name   = Tools::getValue('type_name');
		$json        = new JSON();
		$arr_id_file = (array)$json->decode(Config::get('banner_' . $type_name));
		if (($key = array_search($id_file, $arr_id_file)) !== false) {
			unset($arr_id_file[$key]);
		}
		Config::set('banner_' . $type_name, $json->encode(array_values($arr_id_file)));
		return true;
	}

	public function iniUpFileDir()
	{
		$dir = WEB_DIR . DS . 'img' . DS . 'banner' . DS . date('Y') . DS . date('m') . DS . date('d');
		return $dir;
	}

	public function iniUpFileUrl()
	{
		$url = '/img/banner/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
		return $url;
	}
}