<?php

class AdminPersonnelUnitController extends AdminController{

    public function __construct(){

        $this->className       = 'AdminPersonnelUnitController';
        $this->table           = 'admin';
        $this->fields['index'] = 'id_admin';
        $this->fields['title'] = '人事';
        $this->_as             = 'a';
        $this->fields['list_num'] = 50;
        $this->fields['order'] = ' ORDER BY a.`id_admin` ASC';
        $this->arr_file_type = [
            '0' => 'id_img',
            '1' => 'passbook_img',
            '2' => 'license_img',
            '3' => 'education_img',
            '4' => 'positive_img',
        ];
        $this->personnel_special ='0';//這個是控制會記的部分 1則是會記看的部分 0則是一般身分

        $this->fields['list'] = [
            'id_admin' => [
                'filter_key' => 'a!id_admin',
                'index'  => true,
                'title'  => $this->l('人事ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],

            'email' => [
                'filter_key' => 'a!email',
                'title' => $this->l('帳號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'first_name' => [
                'title'  => $this->l('名'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            'admin' => [
                'legend' => [
                    'title' => $this->l('人事管理'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [
                    'id_admin' => [
                        'name' => 'id_admin',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                ],
                'tpl'=>'themes/LifeHouse/personnel.tpl',

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay' => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],

            ],
        ];

        if($this->personnel_special==1){
            $this->fields['form']['img'] = [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_admin' => [
                        'name' => 'id_admin',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                    'id_img' => [
                        'name' => 'id_img',
                        'label_class' => 'text-left',
                        'label' => $this->l('id上傳'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 800*600px'),
                    ],

                    'passbook_img' => [
                        'name' => 'passbook_img',
                        'label_class' => 'text-left',
                        'label' => $this->l('存摺上傳'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 428,
//                            'max_height' => 285,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
                    ],

                    'license_img' => [
                        'name' => 'license_img',
                        'label_class' => 'text-left',
                        'label' => $this->l('id上傳'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 800*600px'),
                    ],

                    'education_img' => [
                        'name' => 'education_img',
                        'label_class' => 'text-left',
                        'label' => $this->l('id上傳'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 800*600px'),
                    ],

                    'positive_img' => [
                        'name' => 'positive_img',
                        'label_class' => 'text-left',
                        'label' => $this->l('id上傳'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 800*600px'),
                    ],
                ],
            ];
        }

        parent::__construct();
    }


    public function initProcess(){

        $submitEditadmin = Tools::getValue('submitEditadmin');//修改才有差
        $submitEditadminAndStay = Tools::getValue('submitEditadminAndStay');//修改才有差

//       if(!empty($submitEditadmin) || !empty($submitEditadminAndStay)){
//            $database_data = LifeAssets::get_data(Tools::getValue("id_admin"));
//            print_r($database_data);
//        }


        $this->context->smarty->assign([
            'personnel_special'=>$this->personnel_special,//這很重要 這邊設定是如果沒1則不顯示一些重要的資料
            'database_data' => LifeAssets::get_data(Tools::getValue("id_admin")),
            'database_county' =>County::getContext()->database_county(),
            'database_city' =>County::getContext()->database_city(),
            'database_source' => LifeAssets::admin_source(),
            'database_company' =>LifeAssets::company(),
            'database_department' => LifeAssets::department(),//使用預設值生活樂租之後會丟相對應的data現在先新建一個
            'database_adminuser' => LifeAssets::get_admin_ctl_users(), //抓取管理員帳號 id_group=2 OR id_group=14
            'datebase_skill_class' =>Skill::get_skill_class(''),
            'datebase_skill_classification' =>Skill::get_skill_classification(''),
            'datebase_skill'    => Skill::get_skill(''),
        ]);



        parent::initProcess();
    }


    public function displayAjax(){
        $action = Tools::getValue("action");
        switch($action){
            case 'skill':
                $return['skill_class'] = Skill::get_skill_class('');
                $return['skill_classification'] = Skill::get_skill_classification('');
                $return['skill'] = Skill::get_skill('');

                echo json_encode(array("error"=>"","return"=>$return));
                break;
            case 'company':
                $type = Tools::getValue("type");
                $return = LifeAssets::company($type);
                echo json_encode(array("error"=>"","return"=>$return));
                break;

            case 'database_county_city':
                $return['county'] = County::getContext()->database_county();
                $return['city'] = County::getContext()->database_city();
                echo json_encode(array("error"=>"","return"=>$return));
                break;

            case 'database_county':
                $return = County::getContext()->database_county();
                echo json_encode(array("error"=>"","return"=>$return));
                break;
            case 'database_city':
                $return = County::getContext()->database_city();
                echo json_encode(array("error"=>"","return"=>$return));
                break;
            default:
                echo json_encode(array("error"=>"is not work"));
                exit;
                break;
        }
        exit;
    }


    public function processAdd(){
        $this->_errors[] = $this->l('這邊的資料不能新增!');
        //這邊不做新增 直接拋出
    }

    public function processEdit()
    {
        //todo
        if (count($this->fields['no_edit']) && in_array(Tools::getValue($this->fields['index']), $this->fields['no_edit'])) {
            $this->_errors[] = $this->l('無法修改此資料');
        }
        if (count($this->_errors))
            return;
        //丟進去work
        LifeAssets::Admin_multiple_table(Tools::getValue("id_admin"),$_POST,$this->personnel_special);//id 值 是否work保險部分

        //放修改
        $num = Tools::getValue("id_admin");

        if (Tools::isSubmit('submitEdit' . $this->table)) {
            $this->back_url = self::$currentIndex . '&conf=2';
        }
        if (Tools::isSubmit('submitEdit' . $this->table . 'AndStay')) {
            if (in_array('edit', $this->post_processing)) {
                $this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=2&' . $this->index . '=' . $num;
            } else {
                $this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=2&' . $this->index . '=' . $num;
            }
        }
        Tools::redirectLink($this->back_url);
    }


    public function processDel(){
        $this->_errors[] = $this->l('無法刪除正在使用的條件!');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJS('/themes/LifeHouse/js/personnel.js');
        $this->addCSS('/themes/LifeHouse/css/renthouse.css');
    }

    public function displayAjaxCity(){
        $action = Tools::getValue('action');
        $id_county =  Tools::getValue('id_county');
        switch ($action) {
            case 'City':
                if(empty($id_county)){
                    echo json_encode(array("error"=>"is not checked county","return"=>""));
                    break;
                }
                $city = County::getContext()->database_city($id_county);
                echo json_encode(array("error"=>"","return"=>$city));
                break;

            default:
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);

        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_admin = $UpFileVal['id_admin'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `admin_file` (`id_admin`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_admin, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')

            );

            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }


    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_admin = $UpFileVal['id_admin'];

        list($dir, $url) = str_dir_change($id_admin);     //切割資料夾
        $dir = WEB_DIR . DS . ADMIN_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_admin = $UpFileVal['id_admin'];
        list($dir, $url) = str_dir_change($id_admin);
        $url = ADMIN_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_admin)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `admin_file`
				WHERE `id_admin` = %d',
            GetSQL($id_admin, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }

    public function getUpFileList()
    {
        $id_admin = Tools::getValue('id_admin');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_admin' => $id_admin,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_admin);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }

                    $del_table = "admin_flie";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/PersonnelUnit?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];//url後面作補充使她能夠正確刪除資料
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
}
