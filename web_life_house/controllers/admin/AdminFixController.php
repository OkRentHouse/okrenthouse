<?php

class AdminFixController extends AdminController
{
    public function __construct()
    {
        //基本上拿來測試東西用的地方
        $this->className       = 'AdminFixController';
        $this->table = 'fix';
        $this->fields['index'] = 'id_fix';
        $this->_as = 'f';
        $this->fields['title'] = '修繕單';
//        $this->_join =" LEFT JOIN `fix_record` AS f_r ON f_r.`id_fix` = f.`id_fix`  ";

        $this->fields['list'] = [
            'id_home_appliances' => [
                'filter_key' => 'f!id_fix',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],
            'rent_house_code' => [
                'filter_key' => 'f!rent_house_code',
                'title' => $this->l('租屋編號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            'fix_code' => [
                'filter_key' => 'f!fix_code',
                'title' => $this->l('修繕單編號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

        ];

        $this->fix =[
            'id_fix' => [
                'name' => 'id_fix',
                'type' => 'hidden',
                'label_col' => 3,
                'index' => true,
            ],

            'create_time'=>[
                'type'=>'text',
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'label'=>'建立時間',
                'name'=>'create_time',
                'disabled'=>true,
            ],

            'id_admin'=>[
                'type'=>'text',
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'label'=>'建立人',
                'name'=>'id_admin',
                'disabled'=>true,
            ],

            'rent_house_code'=>[
                'type'=>'text',
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'maxlength' =>20,
                'label'=>'租屋物件編號',
                'name'=>'rent_house_code',
                'required' => true,
            ],

            'fix_code'=>[
                'type'=>'text',
                'form_col' => 6,
                'label_col' => 3,
                'col' => 9,
                'maxlength' =>20,
                'label'=>'修繕單號',
                'name'=>'fix_code',
                'required' => true,
            ],

            'schedule'=>[
                'type'=>'select',
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇目前進度',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'建單',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'屋管受理',
                        'id'=>'',
                    ],
                    [
                        'val'=>'2',
                        'text'=>'詢價中',
                        'id'=>'',
                    ],
                    [
                        'val'=>'3',
                        'text'=>'派工中',
                        'id'=>'',
                    ],
                    [
                        'val'=>'4',
                        'text'=>'已派工',
                        'id'=>'',
                    ],
                    [
                        'val'=>'5',
                        'text'=>'待回報屋主',
                        'id'=>'',
                    ],
                    [
                        'val'=>'6',
                        'text'=>'已結案',
                        'id'=>'',
                    ],
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 3,
                'label'=>'進度',
                'name'=>'schedule',
            ],

            'fix_date'=>[
                'type'=>'date',
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'label'=>'報修時間',
                'name'=>'fix_date',
            ],

            'reason'=>[
                'type'=>'text',
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'報修原因',
                'maxlength'=>'64',
                'name'=>'reason',
            ],

            'processing_time'=>[
                'type'=>'date',
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'label'=>'受理時間',
                'name'=>'processing_time',
            ],

            'ag'=>[
                'type'=>'text',
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'報修原因',
                'maxlength'=>'64',
                'name'=>'受理業務',
            ],

            'return_landlord'=>[
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'type'=>'return',
                'label'=>'房東',
                'name'=>'return_landlord',
            ],

            'return_tenant'=>[
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'type'=>'return',
                'label'=>'租客',
                'name'=>'return_tenant',
            ],


            'review'=>[
                'type'=>'text',
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'label'=>'復審',
                'name'=>'review',
                'maxlength'=>'32',
            ],

            'close_date'=>[
                'type'=>'date',
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'label'=>'結案日期',
                'name'=>'close_date',
            ],

        ];

        $this->fix_record =[
            'id_fix_record',
            'id_fix',
            'dispatch_date',
            'dispatch_ag',
            'dispatch_vendor',
            'vendor_return',
            'return_landlord',
            'quoted_price',
            'negotiated_price',
            'net_price',
            'pay',
            'pay_additional_data',
            'reimbursement_date',
            'reimbursement_ag',
            'reimbursement_record'
        ];

        $this->fields['tabs'] = [
            'fix' =>$this->l('修繕單'),
            'fix_record'=>$this->l('修繕紀錄'),
        ];

        parent::__construct();
    }

    public function initProcess()//處理from問題
    {
        $sql = "SELECT *,CONCAT(a.last_name,a.first_name) as id_admin FROM fix as f LEFT JOIN admin as a ON f.id_admin = a.id_admin WHERE id_fix =".GetSQL(Tools::getValue("id_fix"),"int");

        $fix_row = Db::rowSQL($sql,true); //取出作版並且為後續處理

        if(isset($fix_row)){//存在做這道工序
            foreach($fix_row as $key => $value){
                if(strstr($value,'[') && strstr($value,']') && strstr($value,'"')){//找到標的物將它變成陣列形式
                    $fix_row[$key] = Db::antonym_array($fix_row[$key],true);
                    $fix_row[$key] = explode(",",$fix_row[$key]);
                }
            }
        }

        $sql = "SELECT *,CONCAT(a.last_name,a.first_name) as id_admin FROM fix_record as f_r LEFT JOIN admin as a ON f_r.id_admin = a.id_admin WHERE id_fix =".GetSQL(Tools::getValue("id_fix"),"int");
        $fix_record_row = Db::rowSQL($sql);

        $this->fields['form']['id_fix'] = [
                'tab' => 'fix',
                'legend' => [
                    'title' => $this->l('修繕主單'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'tpl'=>'themes/LifeHouse/controllers/fix/fix_main.tpl',

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay' => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset' => [
                    'title' => $this->l('復原'),
                ],

        ];

        $this->fields['form']['fix_record'] = [
            'tab' => 'fix_record',
            'legend' => [
                'title' => $this->l('修繕紀錄'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],
            'tpl'=>'themes/LifeHouse/controllers/fix/fix_record.tpl',

            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],

            'reset' => [
                'title' => $this->l('重置'),
            ],

        ];

        $this->context->smarty->assign([
            'tpl_fix'=>$this->fix,
            'fix_row'=> $fix_row,
            'fix_record_row' => $fix_record_row
        ]);

        parent::initProcess();
    }

    public function processAdd(){
        $insert_key = "";
        $insert_val = "";
        foreach($this->fix as $key => $val) {
            if($val['index']==true) {//跳過因為標頭index會自動產生

            }else if($key=='id_admin'){//跳過新增這邊後面會新增

            }else if($val['type']=='hidden'){
                $insert_key .= '`'.$key.'`,';
                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
            }else if($val['type']=='text' || $val['type']=='textarea' || $val["type"]=='date'){
                $insert_key .= '`'.$key.'`,';
                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
            }else if($val['type']=='number' || $val["type"]=='select'){
                $insert_key .= '`'.$key.'`,';
                $insert_val .= ''.GetSQL($_POST[$key],"int").',';
            }else if($val['type']=='return'){
                $tpl =["ag","date","record"];
                foreach($tpl as $tp_k => $tp_v){
                    $temp = "";
                    $con_var = "{$key}_{$tp_v}";
                    foreach($_POST[$con_var] as $k_k => $k_v){
                        $temp .= '"'.$k_v.'",';
                    }
                    $temp = substr($temp,0,-1);//去尾
                    $insert_key .= '`'.$con_var.'`,';
                    $insert_val .="'[{$temp}]',";
                }
            }
        }
        //新增要加上新增人
        $insert_key .='`id_admin`';
        $insert_val .=GetSQL($_SESSION['id_admin'],'int');
        $sql = "INSERT INTO `fix` ({$insert_key})VALUES({$insert_val})";
        Db::rowSQL($sql);

        $sql = "SELECT max(id_fix) id_fix FROM fix";//取得剛新增的單
        $row = Db::rowSQL($sql,true);

        $insert_record =[];//陣列處理 因為不知道會新增幾個

        foreach($_POST["id_fix_record"] as $p_k => $p_v){//表示幾個也可以用for表示 只是要單純得知次數
            $insert_key_record ="";
            $insert_val_record ="";
            foreach($this->fix_record as $key =>$val){
                if($val=='id_fix_record'){
                    //自動產生所以跳過
                }else if($val=='id_fix'){
                    $insert_key_record .= '`'.$val.'`,';
                    $insert_val_record .= $row["id_fix"].',';//由於是新增所以都是同樣的
                }else if($val=='quoted_price' || $val=='negotiated_price' || $val=='net_price' || $val=='pay'){
                    $insert_key_record .= '`'.$val.'`,';
                    $insert_val_record .= ''.GetSQL($_POST[$val][$p_k],"int").',';
                }else{
                    $insert_key_record .= '`'.$val.'`,';
                    $insert_val_record .= ''.GetSQL($_POST[$val][$p_k],"text").',';
                }
            }
            $insert_key_record .="`id_admin`";
            $insert_val_record .=GetSQL($_SESSION['id_admin'],'int');
            $insert_record[] = "INSERT INTO `fix_record`({$insert_key_record})VALUES({$insert_val_record})";
        }
        foreach($insert_record as $sql){//批次新增檔案
            Db::rowSQL($sql);
        }

        //作原本的儲存轉頁
        if (Tools::isSubmit('submitAdd' . $this->table)) {
            $this->back_url = self::$currentIndex . '&conf=1';
        }
        if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
            if (in_array('edit', $this->post_processing)) {
                $this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=1&' . $this->index . '=' . $row["id_fix"];
            } else {
                $this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=1&' . $this->index . '=' . $row["id_fix"];
            }
        }
        if (Tools::isSubmit('submitAdd' . $this->table . 'AndContinue')) {
            $this->back_url = self::$currentIndex . '&add' . $this->table . '&conf=1';
        }
        Tools::redirectLink($this->back_url);
    }

    public function processEdit(){

        if(!empty($_POST["id_fix"])){
            $update_data = "";
            $update_where="";
            foreach($this->fix as $key => $val){
                if($val['index']==true){    //跳過因為它是跟改的主鍵
                    $update_where="{$key} =".GetSQL($_POST[$key],"int");
                }else if($val["disabled"]==true){//disabled不修改

                }else if($val['type']=='hidden'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
                }else if($val['type']=='text' || $val['type']=='textarea'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
                }else if($val['type']=='number'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"int").',';
                }else if($val['type']=='time' || $val['type']=='date'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
                }else if($val['type']=='select'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"int").',';
                }else if($val['type']=='return'){
                    $tpl =["ag","date","record"];
                    foreach($tpl as $tp_k => $tp_v){
                        $temp = "";
                        $con_var = "{$key}_{$tp_v}";
                        foreach($_POST[$con_var] as $k_k => $k_v){
                            $temp .= '"'.$k_v.'",';
                        }
                        $temp = substr($temp,0,-1);//去尾
                        $update_data .="`{$con_var}`='[{$temp}]',";
                    }
                }
            }
            $update_data = substr($update_data,0,-1);//去尾
            $sql = "UPDATE `fix` SET {$update_data} WHERE {$update_where}";
            Db::rowSQL($sql);

            $insert_update_record =[];//陣列處理 因為不知道會新增幾個

            foreach($_POST["id_fix_record"] as $p_k => $p_v){//表示幾個也可以用for表示 只是要單純得知次數

                if(empty($_POST["id_fix_record"][$p_k])){//不存在id_fix_record則走新增
                    $insert_key_record ="";
                    $insert_val_record ="";
                    foreach($this->fix_record as $key =>$val){
                        if($val=='id_fix_record'){
                            //自動產生所以跳過
                        }else if($val=='id_fix'){
                            $insert_key_record .= '`'.$val.'`,';
                            $insert_val_record .= $_POST["id_fix"].',';//由於是修改因此讀取原有的就行
                        }else if($val=='quoted_price' || $val=='negotiated_price' || $val=='net_price' || $val=='pay'){
                            $insert_key_record .= '`'.$val.'`,';
                            $insert_val_record .= ''.GetSQL($_POST[$val][$p_k],"int").',';
                        }else{
                            $insert_key_record .= '`'.$val.'`,';
                            $insert_val_record .= ''.GetSQL($_POST[$val][$p_k],"text").',';
                        }
                    }
                    $insert_key_record .="`id_admin`";
                    $insert_val_record .=GetSQL($_SESSION['id_admin'],'int');
                    $insert_update_record[] = "INSERT INTO `fix_record`({$insert_key_record})VALUES({$insert_val_record})";
                }else{
                    $update_record_data = "";
                    $update_record_where = "";
                    foreach($this->fix_record as $key =>$val){
                        if($val=='id_fix_record'){
                            $update_record_where = $val.' = '.GetSQL($_POST[$val][$p_k],"int");
                        }else if($val=='id_fix'){//不動因為以經鎖定了

                        }else{
                            $update_record_data .= $val.'='.GetSQL($_POST[$val][$p_k],"text").',';
                        }
                    }
                    $update_record_data = substr($update_record_data,0,-1);//去尾
                    $insert_update_record[] = "UPDATE `fix_record` SET {$update_record_data} WHERE {$update_record_where}";
                }
            }
            foreach($insert_update_record as $sql){//批次新增檔案
                Db::rowSQL($sql);
            }
        }

        if (Tools::isSubmit('submitEdit' . $this->table)) {
            $this->back_url = self::$currentIndex . '&conf=2';
        }
        if (Tools::isSubmit('submitEdit' . $this->table . 'AndStay')) {
            if (in_array('edit', $this->post_processing)) {
                $this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=2&' . $this->index . '=' . $_POST["id_fix"];
            } else {
                $this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=2&' . $this->index . '=' . $_POST["id_fix"];
            }
        }
        Tools::redirectLink($this->back_url);
    }

    public function processDel()
    {
//        $id_fix = Tools::getValue("id_fix");
//        $sql = "DELETE FROM fix_record WHERE id_fix = ".GetSQL($id_fix,"int");
//        Db::rowSQL($sql);   //刪除對應的紀錄(目前暫時保留)
        parent::processDel();
    }

    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('../css/font-awesome.css');
        $this->addJS('/themes/LifeHouse/js/fix.js');
    }
}