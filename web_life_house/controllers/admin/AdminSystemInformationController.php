<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/7/13
 * Time: 下午 04:09
 */

class AdminSystemInformationController extends AdminController
{
	public $page = 'systeminformation';
	public function __construct(){
		$this->className = 'AdminSystemInformationController';
		$this->fields['title'] = '系統資訊';
		parent::__construct();
	}
	
	public function initContent(){
		$this->display = 'view';
		$db = Db::rowSQL('SELECT VERSION()', true);
		$version = $db['VERSION()'];
		$information = array(
			'web' => array(
				'web_name' => WEB_NAME,
				'web_version' => WEB_VERSION,
				'web_dns' => WEB_DNS
			),
			'server' => array(
				'server_information' => function_exists('php_uname') ? php_uname('s').' '.php_uname('v').' '.php_uname('m') : '',
				'server' => $_SERVER['SERVER_SOFTWARE'],
				'php' => phpversion(),
				'memory_limit' => ini_get('memory_limit'),
				'max_execution_time' => ini_get('max_execution_time')
			),
			'data' => array(
				'version' => $version,
				'server' => DB_HOST,
				'name' => DB_DATABASE,
				'user' => DB_USER,
				'tables_prefix' => DB_PREFIX_,
				'engine' => DB_ENGINE,
				'driver' => Db::getClass()
			),
			'user' => array(
				'agent' => $_SERVER['HTTP_USER_AGENT']
			)
		);
		$this->context->smarty->assign(array(
			'information' => $information)
		);
		parent::initContent();
	}

	public function initToolbar(){

	}
}