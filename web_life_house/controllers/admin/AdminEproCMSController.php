<?php
class AdminEproCMSController extends AdminController
{
    public $tpl_folder;    //樣版資料夾
    public $page = 'epro_cms';
    public $ed;

    public function __construct()
    {
        $this->conn = 'ilifehou_epro';
        $this->className       = 'AdminEproCMSController';
        $this->fields['title'] = '裝修達人網頁管理';
        $this->table           = 'epro_cms';
        $this->fields['index'] = 'id_epro_cms';
        $this->fields['order'] = ' ORDER BY c.`id_web` ASC, c.`url` ASC';
        $this->_as = 'e_c';
        $this->_join = ' LEFT JOIN `epro_cms_main` AS e_c_m ON e_c_m.`id_epro_cms_main` = e_c.`id_epro_cms_main` ';

        //網站縮圖 網站輪播 app縮圖 app輪播 建築權狀 土地權狀
        $this->arr_file_type = [
            '0' => 'img',
        ];

        $this->fields['list']  = [
            'id_epro_cms'         => [
                'index'  => true,
                'hidden' => true,
                'type'   => 'checkbox',
                'label_col' => '2',
                'col' => '3',
                'class'  => 'text-center',
                'filter_key' => 'e_c!id_epro_cms',
            ],
            'main_title'          => [
                'title'  => $this->l('主標籤'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c_m!title',
            ],
            'main_url'          => [
                'title'  => $this->l('主url'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c_m!url',
            ],
            'title'          => [
                'title'  => $this->l('Title'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!title',
            ],
            'page_title'     => [
                'title'  => $this->l('頁面標題'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!page_title',
            ],
            'appeal'    => [
                'title'  => $this->l('appeal'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!appeal',
            ],
            'description'    => [
                'title'  => $this->l('Description'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!description',
            ],
            'display_header' => [
                'title'  => $this->l('顯示Header'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!display_header',
                'values' => [
                    [
                        'class' => 'display_h_1',
                        'value' => '1',
                        'title' => $this->l('顯示'),
                    ],
                    [
                        'class' => 'display_h_0',
                        'value' => '0',
                        'title' => $this->l('隱藏'),
                    ],
                ],
            ],
            'display_footer' => [
                'title'  => $this->l('顯示Foote'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!display_footer',
                'values' => [
                    [
                        'class' => 'display_f_1',
                        'value' => '1',
                        'title' => $this->l('顯示'),
                    ],
                    [
                        'class' => 'display_f_0',
                        'value' => '0',
                        'title' => $this->l('隱藏'),
                    ],
                ],
            ],
            'position' => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!position',
                'filter' => true,
            ],
            'active'         => [
                'title'  => $this->l('啟用'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_c!active',
            ],
        ];

        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('裝修達人網頁管理'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_epro_cms' => [
                        'name'     => 'id_epro_cms',
                        'type'     => 'hidden',
                        'label_col' => '2',
                        'col' => '3',
                        'index'    => true,
                        'required' => true,
                    ],
                    'id_epro_cms_main'          => [
                        'name'      => 'id_epro_cms_main',
                        'type'      => 'select',
                        'label_col' => '2',
                        'col' => '3',
                        'label'     => $this->l('主網頁'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇主網頁',
                                'val'  => '',
                            ],
                            'table'   => 'epro_cms_main',
                            'text'    => 'title',
                            'value'   => 'id_epro_cms_main',
                            'order'   => ' `position` ASC',
                        ],
//                        'required'  => true,
                        'no_active' => true,
                        'required' => true,
                    ],
                    'title'          => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label_col' => '2',
                        'col' => '3',
                        'maxlength' => 100,
                        'label'     => $this->l('title(主標)'),
                        'required' => true,
                    ],
                    'page_title'     => [
                        'name'      => 'page_title',
                        'type'      => 'text',
                        'label_col' => '2',
                        'col' => '3',
                        'maxlength' => 50,
                        'label'     => $this->l('頁面標題(副標)'),
                    ],
                    'appeal'     => [
                        'name'      => 'appeal',
                        'type'      => 'text',
                        'label_col' => '2',
                        'col' => '3',
                        'maxlength' => 50,
                        'label'     => $this->l('訴求'),
                    ],
                    'description'    => [
                        'name'      => 'description',
                        'type'      => 'text',
                        'label_col' => '2',
                        'col' => '10',
                        'maxlength' => 300,
                        'label'     => $this->l('Description'),
                    ],
                    'html'           => [
                        'name'     => 'html',
                        'type'     => 'textarea',
                        'label_col' => '2',
                        'col'      => 10,
                        'rows'     => 30,
                        'required' => true,
                        'class'    => 'tinymce',
                        'label'    => $this->l('網頁內容'),
                    ],
                    'css'           => [
                        'name'     => 'css',
                        'type'     => 'textarea',
                        'label_col' => '2',
                        'col'      => 10,
                        'rows'     => 10,
                        'label'    => $this->l('CSS'),
                    ],
                    'js'           => [
                        'name'     => 'js',
                        'type'     => 'textarea',
                        'label_col' => '2',
                        'col'      => 10,
                        'rows'     => 10,
                        'label'    => $this->l('JS'),
                    ],
                    'display_header' => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('顯示Header'),
                        'name'     => 'display_header',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'display_h_1',
                                'value' => 1,
                                'label' => $this->l('顯示'),
                            ],
                            [
                                'id'    => 'display_h_0',
                                'value' => 0,
                                'label' => $this->l('隱藏'),
                            ],
                        ],
                    ],
                    'display_footer' => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('顯示Footer'),
                        'name'     => 'display_footer',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'display_f_1',
                                'value' => 1,
                                'label' => $this->l('顯示'),
                            ],
                            [
                                'id'    => 'display_f_0',
                                'value' => 0,
                                'label' => $this->l('隱藏'),
                            ],
                        ],
                    ],
                    'active'         => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('啟用狀態'),
                        'name'     => 'active',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    'position'           => [
                        'label_col' => '2',
                        'col' => '3',
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay'  => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('重置'),
                ],
            ],

            'img' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('小圖示'),
                        'type' => 'file',
                        'language' => 'zh-TW',
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
                        'p' => $this->l('圖片只可上傳一張'),
                    ],
                ],
            ],
        ];
        parent::__construct();
    }



    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_epro_cms = $UpFileVal['id_epro_cms'];


        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `epro_cms_file` (`id_epro_cms`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_epro_cms, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int'));
            Db::rowSQL($sql,false,0,$this->conn);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_epro_cms = $UpFileVal['id_epro_cms'];

        list($dir, $url) = str_dir_change($id_epro_cms);     //切割資料夾
        $dir = WEB_DIR . DS . EPRO_CMS_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_epro_cms = $UpFileVal['id_epro_cms'];
        list($dir, $url) = str_dir_change($id_epro_cms);
        $url = EPRO_CMS_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_epro_cms)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `epro_cms_file`
				WHERE `id_epro_cms` = %d',
            GetSQL($id_epro_cms, 'int'));
        $arr_row = Db::rowSQL($sql,false,0,$this->conn);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_epro_cms = Tools::getValue('id_epro_cms');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_epro_cms' => $id_epro_cms,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_epro_cms);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }

                    $del_table = "epro_cms_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/EproCMS?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];//url後面作補充使她能夠正確刪除資料
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
}