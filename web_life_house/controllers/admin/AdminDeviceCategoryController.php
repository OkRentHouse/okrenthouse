<?php

class AdminDeviceCategoryController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminDeviceCategoryController';
		$this->table           = 'device_category';
		$this->fields['index'] = 'id_device_category';
		$this->fields['title'] = '設備類別';
		$this->fields['order'] = ' ORDER BY d_c.`position` ASC';
        $this->_as = 'd_c';
        $this->_join = ' LEFT JOIN `device_category_class` AS d_c_c ON d_c_c.`id_device_category_class` = d_c.`id_device_category_class`
                         LEFT JOIN `device_category_file` AS d_c_f ON d_c_f.`id_device_category` =d_c.`id_device_category` ';

		$this->fields['list'] = [
			'id_device_category' => [
                'filter_key' => 'd_c!id_device_category',
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'icon'    => [
                'filter_key' => 'd_c!icon',
				'title'  => $this->l('ICON Class'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
            'title_main'    => [
                'filter_key' => 'd_c_c!title',
                'title'  => $this->l('設備主類別'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
			'title'    => [
                'filter_key' => 'd_c!title',
				'title'  => $this->l('設備類別'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
            'img' => [
                'title' => $this->l('縮圖'),
                'order' => true,
                'filter' => true,
                'filter_sql' => 'IF(d_c_f.`id_dcf` > 0, 1, 0)',
                'class' => 'text-center', 'values' => [
                    [
                        'class' => 'img_1',
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'img_0 red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],
			'position'     => [
                'filter_key' => 'd_c!position',
				'title'  => $this->l('順序'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

        $this->arr_file_type = [
            '0' => 'img'
        ];

        $sql = "SELECT * FROM device_category_group ORDER BY position ASC";
        $id_device_category_group_arr = Db::rowSQL($sql);
        foreach($id_device_category_group_arr as $value){
            $id_device_category_group[] = [
                'id' => 'device_category_group_'.$value['id_device_category_group'],
                'value' => $value['id_device_category_group'],
                'label' => $this->l($value['title'])
            ];
        }

		$this->fields['form'] = [
			'device_category' => [
				'legend' => [
					'title' => $this->l('設備類別'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_device_category' => [
						'name'     => 'id_device_category',
						'type'     => 'hidden',
						'index'    => true,
						'label'    => $this->l('型態'),
						'required' => true,
					],
                    'id_device_category_class'          => [
                        'name'      => 'id_device_category_class',
                        'type'      => 'select',
                        'label'     => $this->l('設備主類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇設備主類別',
                                'val'  => '',
                            ],
                            'table'   => 'device_category_class',
                            'text'    => 'title',
                            'value'   => 'id_device_category_class',
                            'order'   => ' `position` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],
                    'id_device_category_group' => [
                        'type' => 'checkbox',
                        'label' => $this->l('設備群組(複選)'),
                        'name' => 'id_device_category_group',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $id_device_category_group,
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],
					'title'    => [
						'name'      => 'title',
						'type'      => 'text',
						'label'     => $this->l('設備類別'),
						'maxlength' => '20',
						'required'  => true,
					],
					'icon'    => [
						'name'      => 'icon',
						'type'      => 'text',
						'label'     => $this->l('ICON Class'),
						'maxlength' => '20',
					],
					[
						'name'  => 'position',
						'type'  => 'number',
						'label' => $this->l('順序'),
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],


            'img' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('小圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 80,
                            'max_height' => 80,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 80*80px'),
                    ],
                ],
            ],
		];

		parent::__construct();
	}

	public function processDel(){
		$id_device_category = Tools::getValue('id_device_category');
		$sql = sprintf('SELECT `id_device`
			FROM `device`
			WHERE `id_device_category` = %d
			LIMIT 0, 1',
		GetSQL($id_device_category, 'int'));

		$row = Db::rowSQL($sql, true);
		if(count($row)) $this->_errors[] = $this->l('無法刪除正在使用的類別!');
		parent::processDel();
	}

    public function setMedia()
    {
        parent::setMedia();
    }


    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);

        $this->arr_file_type = [
            '0' => 'img',
        ];

        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_device_category = $UpFileVal['id_device_category'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `device_category_file` (`id_device_category`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_device_category, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_device_category = $UpFileVal['id_device_category'];

        list($dir, $url) = str_dir_change($id_device_category);     //切割資料夾
        $dir = WEB_DIR . DS . DEVICE_CATEGORY_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_device_category = $UpFileVal['id_device_category'];
        list($dir, $url) = str_dir_change($id_device_category);
        $url = DEVICE_CATEGORY_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_device_category)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `device_category_file`
				WHERE `id_device_category` = %d',
            GetSQL($id_device_category, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }
    
    public function getUpFileList()
    {
        $id_device_category = Tools::getValue('id_device_category');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_device_category' => $id_device_category,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_device_category);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $del_table= "device_category_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/DeviceCategory?&id_file=' . $v['id_device_category'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
}