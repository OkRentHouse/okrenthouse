<?php
use \PHPMailer;

class AdminRentHouseCustomerRequestController extends AdminController
{
    public $stay = false;
    public $page = 'customerrequest';
    public function __construct()
    {
        $this->className = 'AdminRentHouseCustomerRequestController';
        $this->fields['title'] = '商品新增';
        $this->table           = 'CustomerRequest';
        $this->fields['index'] = 'CustomerRequestID';
        $this->fields['order'] = ' ORDER BY p_.`CustomerRequestID` ASC';
        $this->_as = 'p_';
        $this->fields['list_num'] = 50; //每頁出現筆數
        $this->fields['list']  = [

            'CustomerRequestID' => [
                'filter_key' => 'p_!CustomerRequestID',
                'index'  => true,
                'title' => $this->l('客戶需求單ID'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'HouseArea' => [
                'filter_key' => 'p_!HouseArea',
                'title' => $this->l('區域'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'Source' => [
                'filter_key' => 'p_!Source',
                'title' => $this->l('來源'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'CreateDate' => [
                'filter_key' => 'p_!CreateDate',
                'title' => $this->l('日期'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

        ];

        $this->fields['form'] = [

            'produce_up' => [
                'legend' => [
                    'title' => $this->l('客戶需求單'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'CustomerRequestID' => [
                        'name' => 'CustomerRequestID',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                    'CreateDate' => [
                        'name' => 'CreateDate',
                        'type' => 'date',
                        'form_col' =>6,
                        'label_col' => 6,
                        'col'=>6,
                        'label' => $this->l('建立時間'),
                        'required'  => true,
                    ],
                    // 'ag'          => [
                    //     'name'      => 'ag',
                    //     'type'      => 'select',
                    //     'form_col'  => 6,
                    //     'label_col' => 3,
                    //     'col'       => 6,
                    //     'label'     => $this->l('業務'),
                    //     'options'   => [
                    //         'default' => [
                    //             'text' => '業務',
                    //             'val'  => '',
                    //         ],
                    //         'table'   => 'admin',
                    //         'text'    => 'en_name',
                    //         'value'   => 'id_admin',
                    //         'order'   => ' `id_admin` ASC',
                    //         'where'   => ' AND `id_group` = 9 AND `active` = 1 AND en_name is not null ',
                    //     ],
                    //     'required'  => true,
                    // ],
                    
                        'set_new_land_details'=>[
                            'type'=>'tpl',
                            'file'=>'themes/LifeHouse/customrequest.tpl',
                            'label' => $this->l(''),
                            'no_action'=>true,
                            'name' => 'ad_choice',
                            'form_col'=>12,
                            'col'  => 12,
                        ],
                    

                    
                ],
                
                
            
            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],
            'reset' => [
                'title' => $this->l('復原'),
            ],
        ],
        ];
        


        parent::__construct();
    }

    public function initToolbar()
    {        //初始化功能按鈕
        parent::initToolbar();
        if ($this->stay) {
            unset($this->toolbar_btn['save']);
        }
    }

    // public function orderConfirm($orderID2){ //發送客需單給AG
    //     $mobile = $_POST['user'];
    //     $captcha = Tools::getValue('captcha');
    //     $text_sms = "";
    //     $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
    //   }

    public function initProcess()
    {
        // $this->fields['form']['customerrequest'] = [
        //     //            'tab' => 'home_appliances',
        //                 'legend' => [
        //                     'title' => $this->l('客戶需求單'),
        //                     'icon' => 'icon-cogs',
        //                     'image' => '',
        //                 ],
        //                 'input'=>[
        //                     'set_new_land_details'=>[
        //                         'type'=>'tpl',
        //                         'file'=>'themes/LifeHouse/customrequest.tpl',
        //                         'label' => $this->l(''),
        //                         'no_action'=>true,
        //                         'name' => 'ad_choice',
        //                         'form_col'=>12,
        //                         'col'  => 12,
        //                     ],
        //                 ],
        //                 'submit' => [
        //                     [
        //                         'title' => $this->l('儲存'),
        //                     ],
        //                     [
        //                         'title' => $this->l('儲存並繼續編輯'),
        //                         'stay' => true,
        //                     ],
        //                 ],
        //                 'cancel' => [
        //                     'title' => $this->l('取消'),
        //                 ],
        //                 'reset' => [
        //                     'title' => $this->l('復原'),
        //                 ],
        //             ];            
        
        $db_link = mysqli_connect("localhost", "ilifehou_ilife", "S^,HO%bh^$&NF3dab", "ilifehou_ilife_house");

        $edit_id = Tools::getValue('CustomerRequestID');

        if($edit_id ==""){
            if ($_POST['ss'] =="1") {
                $customerRelation = implode(", ", $_POST['CustomerRelation']);
                $room = implode(", ", $_POST['Room']);
                $pattern = implode(", ", $_POST['Pattern']);
                $houseCook = implode(", ", $_POST['HouseCook']);
                $houseBalcony = implode(", ", $_POST['HouseBalcony']);
                $other = implode(", ", $_POST['Other']);
                $pet = implode(", ", $_POST['Pet']);
                $traffic = implode(", ", $_POST['Traffic']);
                $washMachine = implode(", ", $_POST['WashMachine']);
                $hct = implode(", ", $_POST['HouseCity']);
                $lestime = implode(", ", $_POST['Lestime']);
                $sql_insert = "INSERT INTO CustomerRequest (CustomerName, CustomerTitle, CustomerTel, CustomerPhone,CustomerPhone1, LivePeople1, LivePeople2, CustomerWork, HouseArea, HouseCity, CustomerRelation, HouseRent, Investment, BuyPrice, DownPayment, Loan, Room, Room2, Room2_1, Room3, Room4, Pattern, InsidePi, HouseOld, HouseCook, HouseBalcony, Decorate, Other, CarPlace, Pet, School, ShortRent, Lestime, Traffic, Security, ApplyRent, SocialHouse, HouseLike, HouseDislike, HouseFloor, Period, Looking, All_Options, BedSingle, BedDouble, DeskTable, DressingTable, Wardrobe, Sofa, DinnerTable, TV, Aircondition, WashMachine, Fridge, Source, CreatePeople, ag, CreateDate) 
                VALUES 
                ('".$_POST['CustomerName']."','".$_POST['CustomerTitle']."','".$_POST['CustomerTel']."','".$_POST['CustomerPhone']."','".$_POST['CustomerPhone1']."','".$_POST['LivePeople1']."','".$_POST['LivePeople2']."','".$_POST['CustomerWork']."','".$_POST['HouseArea']."','".$hct."','".$customerRelation."','".$_POST['HouseRent']."','".$_POST['Investment']."','".$_POST['BuyPrice']."','".$_POST['DownPayment']."','".$_POST['Loan']."','".$room."','".$_POST['Room2']."','".$_POST['Room2_1']."','".$_POST['Room3']."','".$_POST['Room4']."','".$pattern."','".$_POST['InsidePi']."','".$_POST['HouseOld']."','".$houseCook."','".$houseBalcony."','".$_POST['Decorate']."','".$other."','".$_POST['CarPlace']."','".$pet."','".$_POST['School']."','".$_POST['ShortRent']."','".$lestime."','".$traffic."','".$_POST['Security']."','".$_POST['ApplyRent']."','".$_POST['SocialHouse']."','".$_POST['HouseLike']."','".$_POST['HouseDislike']."','".$_POST['HouseFloor']."','".$_POST['Period']."','".$_POST['Looking']."','".$_POST['All_Options']."','".$_POST['BedSingle']."','".$_POST['BedDouble']."','".$_POST['DeskTable']."','".$_POST['DressingTable']."','".$_POST['Wardrobe']."','".$_POST['Sofa']."','".$_POST['DinnerTable']."','".$_POST['TV']."','".$_POST['Aircondition']."','".$washMachine."','".$_POST['Fridge']."','".$_POST['Source']."','".$_SESSION["name"]."','".$_POST['newag']."',now())";
                //Db::rowSQL($sql_insert);

                if ($db_link->query($sql_insert) === TRUE) {
                    $last_id = $db_link->insert_id;
                    //echo "New record created successfully. Last inserted ID is: " . $last_id;
                }
                //Db::rowSQL($sql_insert);
                $mail= new PHPMailer();
                $mail->setLanguage('zh', 'language/phpmailer.lang-zh');
                $mail->CharSet='utf-8';
                try {
                    $mail->isSMTP();                                            //Send using SMTP
                    $mail->Host       = 'mail.lifegroup.house';                     //Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                    $mail->Username   = 'ilifehou';                     //SMTP username
                    $mail->Password   = 'ilifE@hOuse318568';                               //SMTP password
                    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for PHPMailer::ENCRYPTION_SMTPS above
                    $mail->From  = 'ok@lifegroup.house';
                    $mail->FromName = 'LifeGroup';
                    $mail->addAddress(''.$_POST['newag'].'', 'Ben');     //Add a recipient
                    $mail->isHTML(true);                                  //Set email format to HTML
                    $mail->Subject = '客戶需求單';
                    //$mail->Body    = '客戶需求單通知<br/>請點以下連結：https://lifegroup.house/manage/RentHouseCustomerRequest?&editCustomerRequest&CustomerRequestID='.$last_id;
                    $mail->Body = '<table width=100% border=1 class=customer><tr><td>登入:<span style=color:red;margin:5px;>'.$_SESSION[name].'</span></td><td align=center><span style=color:red;margin:5px;><h2><b>客戶需求單</b></h2></span></td><td>來源:'.$_POST[Source].'</td><td>派單:'.$_POST[newag].'</td> </tr><tr><td>姓名:'.$_POST[CustomerName].'</td><td>稱謂:'.$_POST[CustomerTitle].'</td><td>手機:'.$_POST[CustomerTel].'市話:'.$_POST[CustomerPhone].'</td><td>居住人數:大x '.$_POST[LivePeople1].'小x '.$_POST[LivePeople2].'</td></tr><tr><td>區域:</td><td>'.$_POST[HouseArea].'<br/>'.$hct.'</td><td>房型:'.$_POST[Room2].'/'.$_POST[Room3].'/'.$_POST[Room4].'</td><td>關係:'.$customerRelation.'</td></tr><tr><td>類別預算</td><td colspan=3>月-月租: '.$_POST[HouseRent].' 買-總價: '.$_POST[BuyPrice].' 自備:'.$_POST[DownPayment].'貸款:'.$_POST[Loan].'</td></tr><tr><td>類別格局</td><td colspan=3>套房:'.$room.' <br/>類別:'.$pattern.'</td></tr><tr><td>需求</td><td colspan=3>市內坪數:'.$_POST[InsidePi].'&nbsp;&nbsp;&nbsp;屋齡('.$_POST[HouseOld].'年↓)&nbsp;&nbsp;&nbsp;可炊:'.$houseCook.'&nbsp;&nbsp;&nbsp;陽台:'.$houseBalcony.'<br/>&nbsp;&nbsp;&nbsp;其他:'.$other.'&nbsp;&nbsp;&nbsp;車位:平面 X '.$_POST[CarPlace].'個&nbsp;&nbsp;&nbsp;可寵:'.$pet.'<br/>近學區:'.$_POST[School].'&nbsp;&nbsp;&nbsp;休閒設施:'.$_POST[Lestime].'&nbsp;&nbsp;&nbsp;交通:'.$traffic.'<br/>申請:包租代管計畫:'.$_POST[ApplyRent].'</td></tr><tr><td>座層</td><td>喜：座:'.$_POST[HouseLike].'</td><td>忌：座'.$_POST[HouseDislike].'</td><td>樓層、數字'.$_POST[HouseFloor].'</td></tr><tr><td>期限</td><td>'.$_POST[Period].'</td><td>帶看時間'.$_POST[Looking].'</td><td>&nbsp;</td></tr><tr><td>家具家電需求</td><td colspan=3>床組:(單人 x '.$_POST[BedSingle].'，雙人 x '.$_POST[BedDouble].')&nbsp;&nbsp;&nbsp;書桌椅x'.$_POST[DeskTable].'&nbsp;&nbsp;&nbsp;衣櫥x'.$_POST[Wardrobe].'&nbsp;&nbsp;&nbsp;冷氣x'.$_POST[Aircondition].'&nbsp;&nbsp;&nbsp;洗衣機:'.$washMachine.'</td></tr></table>';
                    $mail->send();
                } catch (Exception $e) {
                }
            }
        } else {
            if ($_POST['upda'] =="2") {
                $sql_insert2 = "INSERT INTO CustomerRequestAG (CustomerRequestID,AG,LookDate,Sarea,Note,CreateDate) 
                VALUES
                ('".$edit_id."','".$_POST['newag']."','".$_POST['slook']."','".$_POST['sarea']."','".$_POST['note']."',now())";
                Db::rowSQL($sql_insert2);
            } else if ($_POST['upda'] =="1") {
                $sql_update = "UPDATE CustomerRequestAG set AG='".$_POST['newag']."', LookDate='".$_POST['slook']."', Sarea='".$_POST['sarea']."', Note='".$_POST['note']."' where CustomerRequestID='".$edit_id."'"; 
                Db::rowSQL($sql_update);
            }
            if ($_POST['ss'] =="1") {
                $customerRelation = implode(", ", $_POST['CustomerRelation']);
                $room = implode(", ", $_POST['Room']);
                $pattern = implode(", ", $_POST['Pattern']);
                $houseCook = implode(", ", $_POST['HouseCook']);
                $houseBalcony = implode(", ", $_POST['HouseBalcony']);
                $other = implode(", ", $_POST['Other']);
                $pet = implode(", ", $_POST['Pet']);
                $traffic = implode(", ", $_POST['Traffic']);
                $washMachine = implode(", ", $_POST['WashMachine']);
                $lestime = implode(", ", $_POST['Lestime']);
                $sql_update = "UPDATE CustomerRequest Set CustomerName='".$_POST['CustomerName']."',CustomerTitle='".$_POST['CustomerTitle']."',CustomerTel='".$_POST['CustomerTel']."',CustomerPhone='".$_POST['CustomerPhone']."',CustomerPhone1='".$_POST['CustomerPhone1']."',LivePeople1='".$_POST['LivePeople1']."',LivePeople2='".$_POST['LivePeople2']."',CustomerWork='".$_POST['CustomerWork']."',CustomerRelation='".$customerRelation."',HouseRent='".$_POST['HouseRent']."',Investment='".$_POST['Investment']."',BuyPrice='".$_POST['BuyPrice']."',DownPayment='".$_POST['DownPayment']."',Loan='".$_POST['Loan']."',Room='".$room."',Room2='".$_POST['Room2']."',Room2_1='".$_POST['Room2_1']."',Room3='".$_POST['Room3']."',Room4='".$_POST['Room4']."',Pattern='".$pattern."',InsidePi='".$_POST['InsidePi']."',HouseOld='".$_POST['HouseOld']."',HouseCook='".$houseCook."',HouseBalcony='".$houseBalcony."',Decorate='".$_POST['Decorate']."',Other='".$other."',CarPlace='".$_POST['CarPlace']."',Pet='".$pet."',School='".$_POST['School']."',ShortRent='".$_POST['ShortRent']."',Lestime='".$lestime."',Traffic='".$traffic."',Security='".$_POST['Security']."',ApplyRent='".$_POST['ApplyRent']."',SocialHouse='".$_POST['SocialHouse']."',HouseLike='".$_POST['HouseLike']."',HouseDislike='".$_POST['HouseDislike']."',HouseFloor='".$_POST['HouseFloor']."',Period='".$_POST['Period']."',Looking='".$_POST['Looking']."',All_Options='".$_POST['All_Options']."',BedSingle='".$_POST['BedSingle']."',BedDouble='".$_POST['BedDouble']."',DeskTable='".$_POST['DeskTable']."',DressingTable='".$_POST['DressingTable']."',Wardrobe='".$_POST['Wardrobe']."',Sofa='".$_POST['Sofa']."',DinnerTable='".$_POST['DinnerTable']."',TV='".$_POST['TV']."',Aircondition='".$_POST['Aircondition']."',WashMachine='".$washMachine."',Fridge='".$_POST['Fridge']."',Source='".$_POST['Source']."',CreatePeople='".$_SESSION["name"]."', ag ='".$_POST['newag']."' WHERE CustomerRequestID='".$edit_id."'";
                Db::rowSQL($sql_update);
                $mail= new PHPMailer();
                $mail->setLanguage('zh', 'language/phpmailer.lang-zh');
                $mail->CharSet='utf-8';
                try {
                    $mail->isSMTP();                                            //Send using SMTP
                    $mail->Host       = 'mail.lifegroup.house';                     //Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                    $mail->Username   = 'ilifehou';                     //SMTP username
                    $mail->Password   = 'ilifE@hOuse318568';                               //SMTP password
                    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for PHPMailer::ENCRYPTION_SMTPS above
                    $mail->From  = 'ok@lifegroup.house';
                    $mail->FromName = 'LifeGroup';
                    $mail->addAddress(''.$_POST['newag'].'', 'Ben');     //Add a recipient
                    $mail->isHTML(true);                                  //Set email format to HTML
                    $mail->Subject = '客戶需求單';
                    $mail->Body = '<table width=100% border=1 class=customer><tr><td>服務專員:<span style=color:red;margin:5px;>'.$_SESSION[name].'</span></td><td align=center><span style=color:red;margin:5px;><h2><b>客戶需求單</b></h2></span></td><td>來源:'.$_POST[Source].'</td><td>AG:'.$_POST[newag].'</td> </tr><tr><td>姓名:'.$_POST[CustomerName].'</td><td>稱謂:'.$_POST[CustomerTitle].'</td><td>手機:'.$_POST[CustomerTel].'市話:'.$_POST[CustomerPhone].'</td><td>居住人數:大x '.$_POST[LivePeople1].'小x '.$_POST[LivePeople2].'</td></tr><tr><td>區域:</td><td>'.$_POST[HouseArea].'<br/>'.$hct.'</td><td>房型:'.$_POST[Room2].' ~ '.$_POST[Room2_1].'/'.$_POST[Room3].'/'.$_POST[Room4].'</td><td>關係:'.$customerRelation.'</td></tr><tr><td>類別預算</td><td colspan=3>月-月租: '.$_POST[HouseRent].' 買-總價: '.$_POST[BuyPrice].' 自備:'.$_POST[DownPayment].'貸款:'.$_POST[Loan].'</td></tr><tr><td>類別格局</td><td colspan=3>套房:'.$room.' <br/>類別:'.$pattern.'</td></tr><tr><td>需求</td><td colspan=3>市內坪數:'.$_POST[InsidePi].'&nbsp;&nbsp;&nbsp;屋齡('.$_POST[HouseOld].'年↓)&nbsp;&nbsp;&nbsp;可炊:'.$houseCook.'&nbsp;&nbsp;&nbsp;陽台:'.$houseBalcony.'<br/>&nbsp;&nbsp;&nbsp;其他:'.$other.'&nbsp;&nbsp;&nbsp;車位:平面 X '.$_POST[CarPlace].'個&nbsp;&nbsp;&nbsp;可寵:'.$pet.'<br/>近學區:'.$_POST[School].'&nbsp;&nbsp;&nbsp;休閒設施:'.$lestime.'&nbsp;&nbsp;&nbsp;交通:'.$traffic.'<br/>申請:包租代管計畫:'.$_POST[ApplyRent].'</td></tr><tr><td>座層</td><td>喜：座:'.$_POST[HouseLike].'</td><td>忌：座'.$_POST[HouseDislike].'</td><td>樓層、數字'.$_POST[HouseFloor].'</td></tr><tr><td>期限</td><td>'.$_POST[Period].'</td><td>帶看時間'.$_POST[Looking].'</td><td>&nbsp;</td></tr><tr><td>家具家電需求</td><td colspan=3>床組:(單人 x '.$_POST[BedSingle].'，雙人 x '.$_POST[BedDouble].')&nbsp;&nbsp;&nbsp;書桌椅x'.$_POST[DeskTable].'&nbsp;&nbsp;&nbsp;衣櫥x'.$_POST[Wardrobe].'&nbsp;&nbsp;&nbsp;冷氣x'.$_POST[Aircondition].'&nbsp;&nbsp;&nbsp;洗衣機:'.$washMachine.'</td></tr></table>';
                    $mail->send();
                } catch (Exception $e) {
                }
            }
            
        }

        $sql_ag = "select email,en_name from admin where id_group = 9 AND active = 1 AND en_name is not null";
        $newag = Db::rowSQL($sql_ag);
        foreach ($ag as $cas=>$caa) {
            $newag[$cas]['email'] = explode(" ", $newag[$cas]['email'])[0];
            $newag[$cas]['en_name'] = explode(" ", $newag[$cas]['en_name'])[0];
        }
    
        $crid = Tools::getValue("CustomerRequestID");

        $sql_ag = "select * from CustomerRequestAG where CustomerRequestID='".$crid."'";
        $cr = Db::rowSQL($sql_ag, true);
        $AG2 = $cr['AG'];
        $LookDate2 = $cr['LookDate'];
        $Sarea2 = $cr['Sarea'];
        $Note2 = $cr['Note'];


        $sql_cr = "select * from CustomerRequest where CustomerRequestID='".$crid."'";
        $cr = Db::rowSQL($sql_cr, true);
        $CustomerName = $cr['CustomerName'];
        $CustomerTitle = $cr['CustomerTitle'];
        $CustomerTel = $cr['CustomerTel'];
        $CustomerPhone = $cr['CustomerPhone'];
        $LivePeople1 = $cr['LivePeople1'];
        $LivePeople2 = $cr['LivePeople2'];
        $CustomerWork = $cr['CustomerWork'];
        $HouseArea = $cr['HouseArea'];
        $HouseCity = $cr['HouseCity'];
        $CustomerRelation = $cr['CustomerRelation'];
        $HouseRent = $cr['HouseRent'];
        $Investment = $cr['Investment'];
        $BuyPrice = $cr['BuyPrice'];
        $DownPayment = $cr['DownPayment'];
        $Loan = $cr['Loan'];
        $Room = $cr['Room'];
        $Room2 = $cr['Room2'];
        $Room2_1 = $cr['Room2_1'];
        $Room3 = $cr['Room3'];
        $Room4 = $cr['Room4'];
        $Pattern = $cr['Pattern'];
        $InsidePi = $cr['InsidePi'];
        $HouseOld = $cr['HouseOld'];
        $HouseCook = $cr['HouseCook'];
        $HouseBalcony = $cr['HouseBalcony'];
        $Decorate = $cr['Decorate'];
        $Other = $cr['Other'];
        $CarPlace = $cr['CarPlace'];
        $Pet = $cr['Pet'];
        $School = $cr['School'];
        $ShortRent = $cr['ShortRent'];
        $Lestime = $cr['Lestime'];
        $Traffic = $cr['Traffic'];
        $Security = $cr['Security'];
        $ApplyRent = $cr['ApplyRent'];
        $SocialHouse = $cr['SocialHouse'];
        $HouseLike = $cr['HouseLike'];
        $HouseDislike = $cr['HouseDislike'];
        $HouseFloor = $cr['HouseFloor'];
        $Period = $cr['Period'];
        $Looking = $cr['Looking'];
        $All_Options = $cr['All_Options'];
        $BedSingle = $cr['BedSingle'];
        $BedDouble = $cr['BedDouble'];
        $DeskTable = $cr['DeskTable'];
        $DressingTable = $cr['DressingTable'];
        $Wardrobe = $cr['Wardrobe'];
        $Sofa = $cr['Sofa'];
        $DinnerTable = $cr['DinnerTable'];
        $TV = $cr['TV'];
        $Aircondition = $cr['Aircondition'];
        $WashMachine = $cr['WashMachine'];
        $Fridge = $cr['Fridge'];
        $Source = $cr['Source'];
        $CreatePeople = $cr['CreatePeople'];
        $ag = $cr['ag'];
        $CreateDate = $cr['CreateDate'];

        $n1 = substr($Room2, 0,1);
        $n2 = substr($Room2_1, 0,1);

        if($HouseRent != ""){
            $rrr = " and (rent_price_m <=".$HouseRent." or a.ping <= $InsidePi)";
            $aaa = " and a.house_choose='[".'"0"'."]'";
        } 
        if($BuyPrice != ""){
            $rrr = " and (price <=".$BuyPrice." or a.ping <= $InsidePi)";
            $aaa = " and a.house_choose='[".'"1"'."]'";
        }

        $sql_search ="SELECT DISTINCT a.rent_house_code,a.id_rent_house,a.rent_house_code,a.part_address,a.case_name,a.title,a.room,a.hall,a.bathroom,a.ping,c.file_url,c.filename 
        FROM `rent_house` a join rent_house_file b on a.id_rent_house = b.id_rent_house join file c on b.id_file = c.id_file 
        where a.active=1 $aaa  and a.room BETWEEN $n1 and $n2  $rrr  group by a.id_rent_house ORDER BY a.submit_date desc limit 20";
        //echo $sql_search;
        $search = Db::rowSQL($sql_search);
        foreach ($search as $ssc=>$ssv) {
            $search[$ssc]['rent_house_code'] = explode(" ", $search[$ssc]['rent_house_code'])[0];
            $search[$ssc]['file_url'] = explode(" ", $search[$ssc]['file_url'])[0];
            $search[$ssc]['id_rent_house'] = explode(" ", $search[$ssc]['id_rent_house'])[0];
            $search[$ssc]['rent_house_code'] = explode(" ", $search[$ssc]['rent_house_code'])[0];
            $search[$ssc]['part_address'] = explode(" ", $search[$ssc]['part_address'])[0];
            $search[$ssc]['case_name'] = explode(" ", $search[$ssc]['case_name'])[0];
            $search[$ssc]['title'] = explode(" ", $search[$ssc]['title'])[0];
            $search[$ssc]['room'] = explode(" ", $search[$ssc]['room'])[0];
            $search[$ssc]['hall'] = explode(" ", $search[$ssc]['hall'])[0];
            $search[$ssc]['bathroom'] = explode(" ", $search[$ssc]['bathroom'])[0];
            $search[$ssc]['ping'] = explode(" ", $search[$ssc]['ping'])[0];
            $search[$ssc]['filename'] = explode(" ", $search[$ssc]['filename'])[0];
        }
        
        parent::initProcess();
        
        $this->context->smarty->assign([
            'CustomerName' => $CustomerName,
            'CustomerTitle' => $CustomerTitle,
            'CustomerTel' => $CustomerTel,
            'CustomerPhone' => $CustomerPhone,
            'LivePeople1' => $LivePeople1,
            'LivePeople2' => $LivePeople2,
            'CustomerWork' => $CustomerWork,
            'HouseArea' => $HouseArea,
            'HouseCity' => $HouseCity,
            'CustomerRelation' => $CustomerRelation,
            'HouseRent' => $HouseRent,
            'Investment' => $Investment,
            'BuyPrice' => $BuyPrice,
            'DownPayment' => $DownPayment,
            'Loan' => $Loan,
            'Room' => $Room,
            'Room2' => $Room2,
            'Room2_1' => $Room2_1,
            'Room3' => $Room3,
            'Room4' => $Room4,
            'Pattern' => $Pattern,
            'InsidePi' => $InsidePi,
            'HouseOld' => $HouseOld,
            'HouseCook' => $HouseCook,
            'HouseBalcony' => $HouseBalcony,
            'Decorate' => $Decorate,
            'Other' => $Other,
            'CarPlace' => $CarPlace,
            'Pet' => $Pet,
            'School' => $School,
            'ShortRent' => $ShortRent,
            'Lestime' => $Lestime,
            'Traffic' => $Traffic,
            'Security' => $Security,
            'ApplyRent' => $ApplyRent,
            'SocialHouse	' => $SocialHouse,
            'HouseLike' => $HouseLike,
            'HouseDislike' => $HouseDislike,
            'HouseFloor' => $HouseFloor,
            'Period' => $Period,
            'Looking' => $Looking,
            'All_Options' => $All_Options,
            'BedSingle' => $BedSingle,
            'BedDouble' => $BedDouble,
            'DeskTable' => $DeskTable,
            'DressingTable' => $DressingTable,
            'Wardrobe' => $Wardrobe,
            'Sofa' => $Sofa,
            'DinnerTable' => $DinnerTable,
            'TV' => $TV,
            'Aircondition' => $Aircondition,
            'WashMachine' => $WashMachine,
            'Fridge' => $Fridge,
            'Source' => $Source,
            'CreatePeople' => $CreatePeople,
            'ag' => $ag,
            'CreateDate' => $CreateDate,
            'newag' => $newag,
            'search' => $search,
            'AG2' => $AG2,
            'LookDate2' => $LookDate2,
            'Sarea2' => $Sarea2,
            'Note2' => $Note2
        ]);
    }

    public function processEdit()
    {
        //echo "processEdit";
        //parent::processEdit();
        echo "1";
        exit;
    }

    public function processAdd()
    {
        echo "processAdd";
        //parent::processAdd();
    }

    public function processDel()
    {
        echo "processDel";
        parent::processDel();
    }

    public function processSave()
    {
        //echo "processSave";
        //parent::processSave();
        echo "<script>window.location='https://lifegroup.house/manage/RentHouseCustomerRequest';</script>";
    }
}
