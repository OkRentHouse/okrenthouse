<?php

include(TOOL_DIR."PHPExcel.php");
include(TOOL_DIR."PHPExcel/Writer/Excel5.php");
include(TOOL_DIR."PHPExcel/Writer/Excel2007.php");




class AdminRentImportController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminRentImportController';

        $form =<<<hmtl

<html>
<head><meta charset="utf-8"></head>
    <body>
<form action="" method="post" id="form" enctype="multipart/form-data">
    <div class="form-group">
        <label for="exampleInputFile">請選擇租件檔案</label>
        <input type="file" id="import" name="import"  />

    </div>
    <input type="submit"  class="btn btn-warning" value="匯入" name ="import">
</form>
</body>
</html>

hmtl;
    //AIzaSyAU9s5j6uICVXp47T3-AxlV_RZ7cVk7cwg  百合苑
    //AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c   生活樂租
    //function get_lat_and_long($address,$key="AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c"){//取得經緯度 key預設使用生活樂租
    function get_lat_and_long($address,$key="AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs"){//取得經緯度 key預設使用生活樂租
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$key}";//取得經緯度的網址
        return get_curl($url);
    }

    function get_data_lat_and_long($json){//回傳json的經緯值
        $de_json = json_decode($json,true);
        $return_arr = "";
        //print_r($de_json);
        if($de_json["status"]=="OK"){
            foreach($de_json["results"] as $key => $value){
                $return_arr[] = [
                    "lat"=>$de_json["results"][$key]["geometry"]["location"]["lat"],
                    "lng"=>$de_json["results"][$key]["geometry"]["location"]["lng"]
                ];
            }
        }
        return $return_arr;
    }


    function get_curl($url,$type="GET"){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }




        function del_str($str,$del_str){//傳入字串, 要刪除的字眼 用,隔開

            $del_str_arr = explode(",",$del_str);
            foreach($del_str_arr as $key => $value){
                $str = str_replace($value,'',$str);
            }
            return $str;
        }

        $import     = Tools::getValue('import');

        if($import=="匯入" && strpos($_FILES['import']['name'],'.xls')){
            //這邊正式匯入資料
            $file = file_get_contents($_FILES['import']["tmp_name"]);

//            $reader = PHPExcel_IOFactory::createReader('Excel2007'); // 讀取2007 excel 檔案
//            $PHPExcel = $reader->load($_FILES["import"]["tmp_name"]); // 檔案名稱 需已經上傳到主機上
//            $sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從 0 開始)
//            $highestRow = $sheet->getHighestRow(); // 取得總列數

            $reader = PHPExcel_IOFactory::createReader('Excel5'); // 讀取舊版 excel 檔案
            $PHPExcel = $reader->load($_FILES["import"]["tmp_name"]); // 檔案名稱
            $sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從 0 開始)
            $highestRow = $sheet->getHighestRow(); // 取得總列數

            ////基本匯入
            $id_web = '1';
            echo '<meta charset="utf-8">';

            $sql = "SELECT id_rent_house_type,title FROM `rent_house_type`";
            $row = Db::rowSQL($sql);

            foreach ($row as $key => $value){
//              echo 'id_rent_house_type:'.$value['id_rent_house_type'].' title:'.$value['title'].'<br>';
                $rent_house_type_arr[$value['title']]=$value['id_rent_house_type'];//$rent_house_type_arr 陣列
            }

            $sql = "SELECT id_rent_house_types,title FROM `rent_house_types`";
            $row = Db::rowSQL($sql);

            foreach ($row as $key => $value){
                $rent_house_types_arr[$value['title']]=$value['id_rent_house_types'];//$rent_house_types_arr 陣列
            }

            $sql = 'SELECT id_building_materials , name FROM building_materials';
            $row = Db::rowSQL($sql);
            foreach ($row as $key => $value){
                $building_materials_arr[$value['name']]=$value['id_building_materials'];//$building_materials_arr 陣列 主要建材
            }

//            $county_arr = [
//                '台北','新北','桃園','台中','台南','高雄'
//            ];

            $insert_arr = '';
            $update_arr ='';
            $x= '0';

            $sql = "SELECT * FROM county";
            $county_database = Db::rowSQL($sql);
            $sql = "SELECT * FROM city";
            $city_database = Db::rowSQL($sql);

            $num_repeat_code = 0;
            $num_ok = 0;
            $sql = "SELECT rent_house_code FROM rent_house";
            $data_repeat_code = Db::rowSQL($sql);


//            暫時不用
            for ($row = 2; $row <= $highestRow; $row++) {
                $go_break = "false";

                $county=$sheet->getCellByColumnAndRow(0, $row)->getValue();//縣市
                $rent_house_code=$sheet->getCellByColumnAndRow(2, $row)->getValue();//自編
                $case_name=$sheet->getCellByColumnAndRow(3, $row)->getValue();//案名
                $featured=$sheet->getCellByColumnAndRow(4, $row)->getValue();//精選
                $col4=$sheet->getCellByColumnAndRow(5, $row)->getValue(); //分割兩塊 rent_house_types/rent_house_type
                $price=$sheet->getCellByColumnAndRow(6, $row)->getValue();//租金 價格
                $ping=$sheet->getCellByColumnAndRow(7, $row)->getValue();//坪數
                $address=$sheet->getCellByColumnAndRow(8, $row)->getValue();//地址
                $complete_address =$sheet->getCellByColumnAndRow(9, $row)->getValue();//完整地址
//                $active=$sheet->getCellByColumnAndRow(10, $row)->getValue();//上架
                $title=$sheet->getCellByColumnAndRow(10, $row)->getValue();//特色 ==強力訴求
                $room=$sheet->getCellByColumnAndRow(11, $row)->getValue();//房跟室 要拆開
                $hall=$sheet->getCellByColumnAndRow(12, $row)->getValue();//廳
                $bathroom=$sheet->getCellByColumnAndRow(13, $row)->getValue();//衛
                $balcony_seat = $sheet->getCellByColumnAndRow(14, $row)->getValue();//陽台朝向
                $rental_floor=$sheet->getCellByColumnAndRow(15, $row)->getValue();//出租樓層 and 樓高
                $col16=$sheet->getCellByColumnAndRow(16, $row)->getValue();//屋齡 pass 我們只記錄建成時間
                $col17=$sheet->getCellByColumnAndRow(17, $row)->getValue();//車位(換了)
                $road_width=$sheet->getCellByColumnAndRow(18, $row)->getValue();//面前道路
                $width=$sheet->getCellByColumnAndRow(19, $row)->getValue();//面寬
                $depth=$sheet->getCellByColumnAndRow(20, $row)->getValue();//縱深
                $genre=$sheet->getCellByColumnAndRow(21, $row)->getValue();//隔間
                $lease_period=$sheet->getCellByColumnAndRow(22, $row)->getValue();//現況
                $off_date=$sheet->getCellByColumnAndRow(23, $row)->getValue();//下架日期
                $remarks=$sheet->getCellByColumnAndRow(27, $row)->getValue();//備註
                $features=$sheet->getCellByColumnAndRow(31, $row)->getValue();//特色

                foreach($data_repeat_code as $k => $v) {
                    if ($v['rent_house_code'] == $rent_house_code) {
                        $num_repeat_code++;
                        $go_break='true';
                        break;
                    }
                }

                if($go_break=='true'){
                    continue;
                }
                $num_ok++;
//                if(in_array($county,$county_arr) || strstr($county,'市')){
//                    $county = $county.'市';
//                }else if($county=='花蓮'){
//                    $county = $county.'縣';
//                }

                    if ($featured == '有') {
                        $featured = '1';
                    } else {
                        $featured = '0';
                    }

                    $price = str_replace(',', '', $price);

                    //rent_house_types/rent_house_type strart
                    $col4_arr = explode('/', $col4);

                    if (!empty($col4_arr[0])) {
                        $rent_house_types = $col4_arr[0];
                        if (array_key_exists($rent_house_types, $rent_house_types_arr)) {
                            $rent_house_types = $rent_house_types_arr[$rent_house_types];
                        } else {
                            $rent_house_types = '';
                        }
                    } else {
                        $rent_house_types = '';
                    }

                    if (!empty($col4_arr[1])) {
                        $rent_house_type = $col4_arr[1];
                        if (array_key_exists($rent_house_type, $rent_house_type_arr)) {
                            $rent_house_type = $rent_house_type_arr[$rent_house_type];
                        } else {
                            $rent_house_type = '';
                        }
                    } else {
                        $rent_house_type = '';
                    }

                    //rent_house_types/rent_house_type end

//                if($active=='上架'){//上下架
//                    $active = '1';
//                }else{
//                    $active = '0';
//                }


                    if (strstr($room, '+')) {
                        $room_arr = explode('+', $room);
                        $room = $room_arr[0];
                        $muro = $room_arr[1];
                    } else {
                        $muro = '0';
                    }


                    if ($balcony_seat == '' || $balcony_seat == '空白') {//房屋朝向
                        $balcony_seat = '';
                    }


                    //出租樓層
                    $rental_floor = del_str($rental_floor, '樓,共,F');//出租樓層 and 樓高 先去除特定字串

//                $rental_floor = str_replace('樓','',$rental_floor);  //出租樓層 and 樓高 先去除特定字串
//                $rental_floor = str_replace('共','',$rental_floor);
//                $rental_floor = str_replace('F','',$rental_floor);

                    $rental_floor_arr = explode('/', $rental_floor);
                    $rental_floor_s = '';
                    $rental_floor_e = '';


                    if (!empty($rental_floor_arr[0])) {//這邊切割起迄
                        if (strstr($rental_floor_arr[0], '-')) {
                            $rental_floor_arrs = explode('-', $rental_floor_arr[0]);
                            $rental_floor_s = $rental_floor_arrs[0];
                            $rental_floor_e = $rental_floor_arrs[1];

                        } else if (strstr($rental_floor_arr[0], '+')) {
                            $rental_floor_arrs = explode('+', $rental_floor_arr[0]);
                            $rental_floor_s = $rental_floor_arrs[0];
                            $rental_floor_e = $rental_floor_arrs[1];

                        } else if (strstr($rental_floor_arr[0], '~')) {
                            $rental_floor_arrs = explode('~', $rental_floor_arr[0]);
                            $rental_floor_s = $rental_floor_arrs[0];
                            $rental_floor_e = $rental_floor_arrs[1];

                        } else {
                            $rental_floor_s = $rental_floor_arr[0];
                        }
                    } else {
                        $rental_floor_s = '0';
                        $rental_floor_e = '0';
                    }
                    if (empty($rental_floor_e)) {//沒有迄
                        $rental_floor_e = $rental_floor_s;
                    }


                    if (!empty($rental_floor_arr[1])) {
                        $all_floor = $rental_floor_arr[1];
                    } else {
                        $all_floor = '';
                    }


                    if ($lease_period == "出租中") {
                        $lease_period = '1';
                    } else if ($lease_period == "待租") {
                        $lease_period = '0';
                    } else {
                        $lease_period = '';
                    }

                    //隔間
                    if ($genre == "水泥") {
                        $genre = '5';
                    } else if ($genre == "木板") {
                        $genre = '4';
                    } else {
                        $genre = '';
                    }


                    $id_county = $county_database[array_search($$complete_address, $county_database)['id_county']];
                    $id_city = '';
                    foreach ($city_database as $k_c => $v_c) {
                        if ($v_c['id_county'] == $id_county && strstr($county_database, $v_c['city_name'])) {
                            $id_city = $v_c['id_city'];
                        }
                    }

//                $longitude = '';//精度
//                $latitude = '';//緯度
//                //經緯度
//                $part_address = $address;
////                $part_address = "桃園市大有里1鄰興一街65巷91號";
//                $google_api_arr = get_lat_and_long($address);
//                $loation_arr = get_data_lat_and_long($google_api_arr);
//
//                $longitude = '';//精度
//                $latitude = '';//緯度
//
//                if(empty($loation_arr)){
//                    $longitude = '';//精度
//                    $latitude = '';//緯度
//                }else{
//                    $longitude = $loation_arr["0"]["lng"];//精度
//                    $latitude = $loation_arr["0"]["lat"];//緯度
//                }
//
//                echo $part_address;
//                print_r($google_api_arr);
//                print_r($loation_arr);
//                echo "<br/>";
//                echo "longitude:{$longitude}    latitude:{$latitude} <br/>";
//
//
//               $x++;
//                if($x>=10){
//                    exit;
//                }


                    $insert_arr[] = "INSERT INTO `rent_house` (`id_web`,`rent_house_code`,`case_name`,`featured`,`id_rent_house_types`,`id_rent_house_type`,
                `reserve_price_m`,`ping`,`part_address`,`address`,`complete_address`,`active`,`title`,`room`,`muro`,`hall`,`bathroom`,
                `balcony_seat`,`rental_floor_s`,`rental_floor_e`,`all_floor`,
                `road_width`,`width`,`depth`,`lease_period`,`off_date`,
                `remarks`,`genre`,`features`)
                VALUES ('" . GetSQL($id_web, "int") . "'," . GetSQL($rent_house_code, "text") . "," . GetSQL($case_name, "text") . "," . GetSQL($featured, "text") . ",
                '" . GetSQL($rent_house_types, "int") . "','" . GetSQL($rent_house_type, "int") . "',
                '" . GetSQL($price, "double") . "','" . GetSQL($ping, "double") . "',
                " . GetSQL($address, "text") . "," . GetSQL($address, "text") . "," . GetSQL($complete_address, "text") . ",1,
                " . GetSQL($title, "text") . ",'" . GetSQL($room, "int") . "','" . GetSQL($muro, "int") . "',
                '" . GetSQL($hall, "int") . "','" . GetSQL($bathroom, "int") . "'," . GetSQL($balcony_seat, "text") . ",'" . GetSQL($rental_floor_s, "int") . "','" . GetSQL($rental_floor_e, "int") . "',
                '" . GetSQL($all_floor, "int") . "','" . GetSQL($road_width, "double") . "',
                '" . GetSQL($width, "double") . "','" . GetSQL($depth, "double") . "','" . GetSQL($lease_period, "int") . "'," . GetSQL($off_date, "date") . ",
                " . GetSQL($remarks, "int") . ",'" . GetSQL($genre, "int") . "'," . GetSQL($features, "text") . ")";
            }


            echo '匯入數量:'.$highestRow.'<br/>';
            echo '重複匯入:'.$num_repeat_code.'<br/>';
            echo '本次匯入'.$num_ok.'<br/>';

            foreach($insert_arr as $key => $sql){
                Db::rowSQL($sql);
//                echo $sql.'<br/>';
            }
//            print_r($insert_arr);
        }else{
            echo $form;
        }

//        echo $form;
//        print_r($_POST);
//        print_r($_FILES);
    }
}
