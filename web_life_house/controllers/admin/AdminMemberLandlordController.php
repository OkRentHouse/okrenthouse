<?php

class AdminMemberLandlordController extends AdminController
{
    public function __construct()
    {
        $this->className = 'AdminMemberLandlordController';
        $this->table = 'member';
        $this->fields['index'] = 'id_member';
        $this->_as = 'm';
        $this->fields['title'] = '房東管理';
        $this->_join = ' LEFT JOIN `source` AS s ON s.`id_source` = m.`id_source` ';

        $this->fields['order'] = ' ORDER BY m.`id_member` ASC ';
        $this->fields['list_num'] = 50;
        $this->fields['list'] = [
            'member' => [
                'filter_key' => 'm!id_member',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],
            'user' => [
                'filter_key' => 'm!user',
                'title' => $this->l('手機號碼'),
                'class' => 'text-center',
            ],
            'name' => [
                'filter_key' => 'm!name',
                'title' => $this->l('姓名'),
                'class' => 'text-center',
            ],
            'en_name' => [
                'filter_key' => 'm!en_name',
                'title' => $this->l('英文名'),
                'class' => 'text-center',
            ],
            'appellation' => [
                'filter_key' => 'm!appellation',
                'title' => $this->l('稱謂'),
                'class' => 'text-center',
                'values'     => [
                    [
                        'value' => 0,
                        'title' => $this->l('先生'),
                    ],
                    [
                        'value' => 1,
                        'title' => $this->l('小姐'),
                    ],
                    [
                        'value' => 2,
                        'title' => $this->l('太太'),
                    ],
                    [
                        'value' => 3,
                        'title' => $this->l('媽媽'),
                    ],
                    [
                        'value' => 4,
                        'title' => $this->l('伯伯'),
                    ],
                ],
            ],
            'birthday' => [
                'filter_key' => 'm!birthday',
                'title' => $this->l('生日'),
                'class' => 'text-center',
            ],
            'identity' => [
                'filter_key' => 'm!identity',
                'title' => $this->l('身分證'),
                'class' => 'text-center',
            ],



        ];

        $this->fields['tabs'] = [
            'session' => $this->l('房東管理'),
        ];


        $this->fields['form'] = [
            'id_member' =>[
                'tab'    => 'member',
                'legend' => [
                    'title' => $this->l('房東'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [
                    'id_member' => [
                        'name' => 'id_member',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],
                    'user' => [
                        'name' => 'user',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'label'     => $this->l('手機號碼(帳號)'),
                        'maxlength'   => '100',
                        'required' => true,
                    ],
                    'tel' => [
                        'name' => 'tel',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                        'label'     => $this->l('備用手機'),
                        'maxlength'   => '100',
                        'required' => true,
                    ],
                    'name' => [
                        'name' => 'name',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'label'     => $this->l('姓名'),
                        'maxlength'   => '20',
                        'required' => true,
                    ],
                    'appellation' =>[
                        'name'      => 'appellation',
                        'type'      => 'select',
                        'label' => $this->l('稱謂'),
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'options'   => [
                            'default'     => [
                                'text' => '選擇稱謂',
                                'val'  => '',
                            ],
                            //自訂值
                            'val'         => [
                                [
                                    'text' => '先生',
                                    'val'  => '0',
                                ],
                                [
                                    'text' => '小姐',
                                    'val'  => '1',
                                ],
                                [
                                    'text' => '太太',
                                    'val'  => '2',
                                ],
                                [
                                    'text' => '媽媽',
                                    'val'  => '3',
                                ],
                                [
                                    'text' => '伯伯',
                                    'val'  => '4',
                                ],
                            ],
                        ],
                    ],
                    'birthday' => [
                        'name' => 'birthday',
                        'type' => 'date',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'label'     => $this->l('生日'),
                    ],
                    'identity' => [
                        'name' => 'identity',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                        'maxlength'   => '16',
                        'label' => $this->l('身分證字號'),
                    ],
                    'line_id' => [
                        'name' => 'line_id',
                        'type' => 'text',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 3,
                        'maxlength'   => '64',
                        'label' => $this->l('line_id'),
                    ],
                    'ag' => [
                        'name' => 'ag',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'maxlength'   => '32',
                        'label' => $this->l('ag'),
                    ],
                    'id_source'          => [
                        'name'      => 'id_source',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 6,
                        'label'     => $this->l('開發來源'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇開發來源',
                                'val'  => '0',
                            ],
                            'table'   => 'source',
                            'text'    => 'source',
                            'value'   => 'id_source',
                            'order'   => ' `id_source` ASC',
                        ],
                        'no_active' => true,
                    ],
                    'local_tel_1' => [
                        'name' => 'local_tel_1',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col' => 6,
                        'maxlength'   => '32',
                        'label' => $this->l('市話1'),
                    ],
                    'local_tel_2' => [
                        'name' => 'local_tel_2',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                        'maxlength'   => '32',
                        'label' => $this->l('市話2'),
                    ],
                    'company' => [
                        'name' => 'company',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' =>6,
                        'col' => 6,
                        'maxlength'   => '64',
                        'label' => $this->l('公司'),
                    ],
                    'job' => [
                        'name' => 'job',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col' => 6,
                        'maxlength'   => '32',
                        'label' => $this->l('職業'),
                    ],


                    'id_address_domicile'=>[
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'     => $this->l('戶籍地址'),
                        'name'=>'id_address_domicile',
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/address.tpl',
                    ],
                    'id_address_contact'=>[
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'     => $this->l('連絡地址'),
                        'name'=>'id_address_contact',
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/address.tpl',
                    ],
                    'id_address_house'=>[
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'     => $this->l('住家地址'),
                        'name'=>'id_address_house',
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/address.tpl',
                    ],
                    'id_address_company'=>[
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'     => $this->l('公司地址'),
                        'name'=>'id_address_company',
                        'type'=>'tpl',
                        'file'=>'themes/LifeHouse/address.tpl',
                    ],
                    
                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay' => true,
                    ],
                ],

                'cancel' => [
                    'title' => $this->l('取消'),
                ],

                'reset' => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
//        $this->fields['form'] = [
//            'id_broker' =>[
//                'tab'    => 'letter',
//                'legend' => [
//                    'title' => $this->l('房仲'),
//                    'icon'  => 'icon-cogs',
//                    'image' => '',
//                ],
//                'input' => [
//
//                    'id_broker' => [
//                        'name' => 'id_broker',
//                        'type' => 'hidden',
//                        'label_col' => 3,
//                        'index' => true,
//                        'required' => true,
//                    ],
//
//                    'id_in_life' => [
//                        'name'      => 'id_member',
//                        'type'      => 'select',
//                        'label_col' => 3,
//                        'label'     => $this->l('選擇房仲身分id'),
//                        'options'   => [
//                            'default' => [
//                                'text' => '選擇房仲',
//                                'val'  => '',
//                            ],
//                            'table'   => 'member',
//                            'text'    => 'name',
//                            'value'   => 'id_member',
//                            'order'   => '`id_member` ASC',
//                            'where'   => " AND active=1 AND id_member_additional LIKE '%3%'  ",
//                        ],
//                        'required'  => true,
//                    ],
//
//                    'brand' => [
//                        'name'      => 'brand',
//                        'type'      => 'text',
//                        'label_col' => 3,
//                        'label'     => $this->l('品牌'),
//                        'maxlength' => '64',
//                        'required'  => true,
//                    ],
//
//                    'company' => [
//                        'name'      => 'company',
//                        'type'      => 'text',
//                        'label_col' => 3,
//                        'label'     => $this->l('公司'),
//                        'maxlength' => '64',
//                        'required'  => true,
//                    ],
//
//                    'company_tel' => [
//                        'name'      => 'company_tel',
//                        'type'      => 'text',
//                        'label_col' => 3,
//                        'label'     => $this->l('公司電話號碼'),
//                        'maxlength' => '64',
//                        'required'  => true,
//                    ],
//
//                    'company_number' => [
//                        'name'      => 'company_number',
//                        'type'      => 'number',
//                        'label_col' => 3,
//                        'label'     => $this->l('公司統編'),
//                        'maxlength' => '11',
//                        'required'  => true,
//                    ],
//
//                    'company_address' => [
//                        'name'      => 'company_address',
//                        'type'      => 'text',
//                        'label_col' => 3,
//                        'label'     => $this->l('公司地址'),
//                        'maxlength' => '64',
//                        'required'  => true,
//                    ],
//
//                    'update_time' => [
//                        'name' => 'update_time',
//                        'type' => 'time',
//                        'label_col' => 3,
//                        'label' => $this->l('建立時間'),
//                        'disabled' =>true,
//                    ],
//
//                    'create_time' => [
//                        'name' => 'create_time',
//                        'type' => 'time',
//                        'label_col' => 3,
//                        'label' => $this->l('建立時間'),
//                        'disabled' =>true,
//                    ],
//
//                ],
//
//
//                'submit' => [
//                    [
//                        'title' => $this->l('儲存'),
//                    ],
//                ],
//
//                'cancel' => [
//                    'title' => $this->l('取消'),
//                ],
//
//                'reset' => [
//                    'title' => $this->l('復原'),
//                ],
//
//            ],
//
//        ];

        parent::__construct();
    }

    public function displayAjaxCity(){
        $action = Tools::getValue('action');
        $id_county =  Tools::getValue('id_county');
        switch ($action) {
            case 'City':
                if(empty($id_county)){
                    echo json_encode(array("error"=>"is not checked county","return"=>""));
                    break;
                }
                $city = County::getContext()->database_city($id_county);
                echo json_encode(array("error"=>"","return"=>$city));
                break;

            default:
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;
    }


    public function processAdd(){

        print_r($_POST);
    }

    public function processEdit(){

        
        print_r($_POST);
    }

    public function Address(){

    }

    public function initProcess()
    {

        $this->context->smarty->assign([
            'database_county' =>County::getContext()->database_county(),
            'database_city' =>County::getContext()->database_city(),
        ]);

        parent::initProcess(); // TODO: Change the autogenerated stub
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('/themes/LifeHouse/css/renthouse.css');
        $this->addJS('/themes/LifeHouse/js/memberlandlord.js');
    }
}
