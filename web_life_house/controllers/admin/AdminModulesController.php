<?php
class AdminModulesController extends AdminController
{
	public function __construct(){
		$this->className = 'AdminModulesController';
		$this->table = 'module';
		$this->fields['index'] = 'id_module';
		$this->fields['title'] = 'Module管理';
		$this->fields['list'] = array(
			'id_module' => array(
				'index' => true,
				'title' => $this->l('Module編號'),
				'type' => 'checkbox',
				'hidden' => true,
				'class' => 'text-center'
			),
			'name'=> array(
				'title' => $this->l('Module名稱'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'version'=> array(
				'title' => $this->l('版本'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'active'=> array(
				'title' => $this->l('啟用'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center',
				'values' => array(
					array(
						'class' => 'active_1',
						'value' => 1,
						'title' => $this->l('啟用')
					),
					array(
						'class' => 'active_0',
						'value' => 0,
						'title' => $this->l('關閉')
					)
				)
			),
		);

		$this->fields['list_num'] = 100;

		$this->fields['form'] = array(
			array(
				'legend' => array(
					'title' => $this->l('Module管理'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'id_module',
						'type' => 'hidden',
						'index' => true,
						'required' => true),
					array(
						'name' => 'name',
						'type' => 'text',
						'label' => $this->l('Module名稱'),
						'maxlength' => '64',
						'required' => true),
					array(
						'name' => 'version',
						'type' => 'text',
						'label' => $this->l('版本')),
					array(
						'type' => 'switch',
						'label' => $this->l('啟用狀態'),
						'name' => 'active',
						'values' => array(
							array(
								'id' => 'active_1',
								'value' => 1,
								'label' => $this->l('啟用')
							),
							array(
								'id' => 'active_0',
								'value' => 0,
								'label' => $this->l('關閉')
							)
						),
						'required' => true
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					),
				),
				'cancel' => array(
					'title' => $this->l('取消')
				),
				'reset' => array(
					'title' => $this->l('復原')
				)
			)
		);

		parent::__construct();
	}
}