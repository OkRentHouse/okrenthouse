<?php

class AdminDiversionController extends AdminController
{
    public $tpl_folder;    //樣版資料夾

    public function __construct()
    {
        $this->className       = 'AdminDiversionController';
        $this->table           = 'diversion';
        $this->_as = 'd';
        $this->fields['index'] = 'id_diversion';
        $this->_join = ' LEFT JOIN  diversion_file as d_f ON d_f.`id_diversion` = d.`id_diversion`';
        $this->fields['where'] = ' AND d.`active` = 1 ';
        $this->fields['order'] = ' ORDER BY d.`position` ASC';


        $this->fields['title'] = '網站管理';
        $this->arr_file_type = [
            '0' => 'img',
        ];
        $this->fields['list'] = [
            'id_diversion'      => [
                'filter_key' => 'd!id_diversion',
                'index'  => true,
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'name'         => [
                'filter_key' => 'd!name',
                'title'  => $this->l('名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position' => [
                'filter_key' => 'd!position',
                'title'  => $this->l('排列順序'),
                'order'  => true,
                'class'  => 'text-center',
                'filter' => true,
            ],
            'active'      => [
                'filter_key' => 'd!active',
                'title'  => $this->l('啟用狀態'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->fields['list_num'] = 50;


        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('分流管理'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_diversion'      => [
                        'name'     => 'id_diversion',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],
                    'name'         => [
                        'name'      => 'name',
                        'label'     => $this->l('名稱'),
                        'type'      => 'text',
                        'maxlength' => 20,
                        'required'  => true,
                    ],
                    [
                        'type'  => 'number',
                        'label' => $this->l('位置'),
                        'min'   => 0,
                        'name'  => 'position',
                    ],
                    [
                        'type'   => 'switch',
                        'label'  => $this->l('啟用狀態'),
                        'name'   => 'active',
                        'values' => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('重置'),
                ],
            ],
            'img'=>[
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('示意圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
                            'max_width' => 200,
                            'max_height' => 200,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],

                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('上傳一張示意圖'),
//                        'p' => $this->l('圖片建議大小 800*600px'),
                    ],
                ],
            ],
        ];
        parent::__construct();
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal){
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_diversion = $UpFileVal['id_diversion'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `diversion_file` (`id_diversion`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_diversion, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir(){
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_diversion = $UpFileVal['id_diversion'];
        list($dir, $url) = str_dir_change($id_diversion);     //切割資料夾
        $dir = WEB_DIR . DS . DIVERSION_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl(){
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_diversion = $UpFileVal['id_diversion'];
        list($dir, $url) = str_dir_change($id_diversion);
        $url = DIVERSION_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_diversion){
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `diversion_file`
				WHERE `id_diversion` = %d',
            GetSQL($id_diversion, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }

    public function getUpFileList(){
        $id_diversion = Tools::getValue('id_diversion');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_diversion' => $id_diversion,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_diversion);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $del_table = "diversion_file";
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/Diversion?&id_file=' . $v['id_diversion'] . '&type_name=' . $type_name . '&table=' . $del_table .  '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }

}
