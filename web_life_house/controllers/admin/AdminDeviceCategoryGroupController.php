<?php

class AdminDeviceCategoryGroupController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminDeviceCategoryGroupController';
        $this->table           = 'device_category_group';
        $this->fields['index'] = 'id_device_category_group';
        $this->_as = 'd_c_g';

        $this->fields['title'] = '設備群組';
        $this->fields['order'] = ' ORDER BY `position` ASC';
        $this->fields['list'] = [
            'id_device_category_group' => [
                'filter_key' => 'd_c_g!id_device_category_group',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'title'    => [
                'filter_key' => 'd_c_g!title',
                'title'  => $this->l('設備群組類別'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'     => [
                'filter_key' => 'd_c_g!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->arr_file_type = [
            '0' => 'img'
        ];

        $sql = 'SELECT * FROM main_house_class';
        $main_house_class_arr = Db::rowSQL($sql);
        foreach($main_house_class_arr as $value){
            $main_house_class[] = [
                'id' => 'main_house_class_'.$value['id_main_house_class'],
                'value' => $value['id_main_house_class'],
                'label' => $this->l($value['title'])
            ];
        }

        $this->fields['form'] = [
            'device_category_class' => [
                'legend' => [
                    'title' => $this->l('設備主類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_device_category_group' => [
                        'name'     => 'id_device_category_group',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('型態'),
                        'required' => true,
                    ],
                    'title'    => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('設備群組類別'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],

                    'id_main_house_class' => [
                        'type' => 'checkbox',
                        'label' => $this->l('用途(複選)'),
                        'name' => 'id_main_house_class',
                        'in_table' => true,
                        'multiple' => true,
                        'values' => $main_house_class,
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                    ],

                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();
    }

    public function processDel(){
        $this->_errors[] = $this->l('無法刪除正在使用的群組類別!');
        parent::processDel();
    }

}