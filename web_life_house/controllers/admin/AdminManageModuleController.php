<?php

class AdminManageModuleController extends AdminController
{
	public $stay = false;
	public $page = 'managemodule';


	public function __construct()
	{

		$this->className = 'AdminManageModuleController';
		$this->table     = 'rent_house';
        $this->_as             = 'r';
        $this->fields['index'] = 'id_rent_house';
        $this->fields['title'] = '物件列表';
				$this->fields['where'] = ' AND `active` = 1';
		//判別賦予權限

    $this->fields['list_num'] = 50; //每頁出現筆數

		if ((Tools::getValue('id_admin') == $_SESSION['id_admin']) && $_SESSION['admin'] != 1 && $_SESSION['id_group'] != 1) {
			$this->stay = true;
		}

		$this->fields['list']  = [

            'id_rent_house' => [
                'filter_key' => 'r!id_rent_house',
								'index'  => true,
                'title' => $this->l('租屋資料ID'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],

            'rent_house_code' => [
                'filter_key' => 'r!rent_house_code',
                'title' => $this->l('物件編號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],

            'title' => [
                'filter_key' => 'r!title',
                'title' => $this->l('前台顯示標題'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],

            'case_name' => [
                'filter_key' => 'r!case_name',
                'title' => $this->l('案名'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                ],

            ];

						$this->fields['form'] = [
							[
								'input'  => [
				            'id_rent_house' => [
				                'name' => 'id_rent_house',
				                'label' => $this->l('租屋資料ID'),
				                'type' => 'text',
				                'form_col' => 12,
				                'label_col' => 1,
				                'col' => 3,
				                'index' => true,
				                'required' => true,
				                ],

				            'rent_house_code' => [
				                'name' => 'rent_house_code',
				                'label' => $this->l('物件編號'),
				                'type' => 'text',
				                'form_col' => 12,
				                'label_col' => 1,
				                'col' => 3,
				                'required' => true,
				                ],

				            'title' => [
				                'name' => 'title',
				                'label' => $this->l('前台顯示標題'),
				                'type' => 'text',
				                'form_col' => 12,
				                'label_col' => 1,
				                'col' => 3,
				                'required' => true,
				                ],

				            'case_name' => [
				                'name' => 'case_name',
				                'label' => $this->l('案名'),
				                'type' => 'text',
				                'form_col' => 12,
				                'label_col' => 1,
				                'col' => 3,
				                'required' => true,
				                ],
										],




				            'submit' => [
				                   [
				                   'title' => $this->l('儲存'),
				                   ],
				                   [
				                   'title' => $this->l('儲存並繼續編輯'),
				                   'stay' => true,
				                   ],
				                ],
				                'cancel' => [
				                'title' => $this->l('取消'),
				                ],
				                'reset'  => [
				                'title' => $this->l('復原'),
				                ],
												],
				            ];


		parent::__construct();
	}

	public function initToolbar()
	{        //初始化功能按鈕
		parent::initToolbar();
		if ($this->stay)
			unset($this->toolbar_btn['save']);
	}

	public function initProcess()
	{


		$id_admin = Tools::getValue('id_admin');

		parent:: initProcess();

		echo "<p>*</p>";echo "<p>*</p>";echo "<p>*</p>";echo "<p>*</p>";
		echo "action:".$this->action."<br>";
		echo "display:".$this->display."<br>";
		echo $this->tabAccess['add']."<br>";
		echo $this->tabAccess['edit']."<br>";
		echo $this->tabAccess['del']."<br>";
		echo $this->table."<br>";
		echo Tools::isSubmit('submitEdit' . $this->table)."<br>";
		echo Tools::isSubmit('submitEdit' . $this->table . 'AndStay')."<br>";
		echo Tools::isSubmit('submitAdd' . $this->table)."<br>";
		echo Tools::isSubmit('submitAdd' . $this->table . 'AndStay')."<br>";
		echo "<p></p>";

	}

	public function processEdit()
	{
		echo "processEdit-1";
		parent::processEdit();
	}

	public function processAdd()
	{
		echo "processAdd";
		parent::processAdd();
		echo "processAdd";
	}

	public function processDel()
	{
		echo "processDel";
		parent:: processDel();
	}

	public function processSave()
	{
		echo "processSave";
		parent:: processSave();
	}

	public function validateRules()
	{

		parent::validateRules();
	}

	public function processSetMyFieldList()
	{

		parent::processSetMyFieldList();
	}

	public function processdelMyFieldList()
	{

		parent::processdelMyFieldList();
	}

}
