<?php


class AdminInLifeController extends AdminController
{
    public function __construct(){

        $this->className = 'AdminInLifeController';
        $this->table = 'in_life';
        $this->fields['index'] = 'id_in_life';
        $this->_as = 'il';
        $this->fields['title'] = '活動分享';
        $this->_join = ' LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = il.`id_in_life`';
        $this->_group = ' GROUP BY il.`id_in_life`';
        $this->fields['order'] = ' ORDER BY `top` DESC, `date` DESC, `add_time` DESC';
        $this->fields['list_num'] = 50;

        $this->fields['list'] = [
            'id_in_life' => [
                'filter_key' => 'il!id_in_life',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],


            'top' => [
                'filter_key' => 'il!top',
                'title' => $this->l('置頂'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'top',
                        'value' => 1,
                        'title' => $this->l('置頂'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'title' => [
                'filter_key' => 'il!title',
                'title' => $this->l('活動名稱'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'title2' => [
                'filter_key' => 'il!title2',
                'title' => $this->l('副標'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'exp' => [
                'filter_key' => 'il!exp',
                'title' => $this->l('活動介紹'),
                'order' => true,
                'filter' => true,
                'show' => false,
                'class' => 'text-center',
            ],

            'date_s' => [
                'filter_key' => 'il!date_s',
                'title' => $this->l('起始日期'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'date_e' => [
                'filter_key' => 'il!date_e',
                'title' => $this->l('結束日期'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
            
            'weekday' => [


                'filter_key' => 'il!weekday',


                'title' => $this->l('星期'),


                'order' => true,


                'filter' => true,


                'in_table' => true,


                'values' => [


                    [


                        'class' => 'weekday_1',


                        'value' => '1',


                        'title' => $this->l('一'),


                    ],


                    [


                        'class' => 'weekday_2',


                        'value' => '2',


                        'title' => $this->l('二'),


                    ],


                    [


                        'class' => 'weekday_3',


                        'value' => '3',


                        'title' => $this->l('三'),


                    ],


                    [


                        'class' => 'weekday_4',


                        'value' => '4',


                        'title' => $this->l('四'),


                    ],


                    [


                        'class' => 'weekday_5',


                        'value' => '5',


                        'title' => $this->l('五'),


                    ],


                    [


                        'class' => 'weekday_6',


                        'value' => '6',


                        'title' => $this->l('六'),


                    ],


                    [


                        'class' => 'weekday_0',


                        'value' => '0',


                        'title' => $this->l('日'),


                    ],


                ],


                'class' => 'text-center',


            ],


            'time_s' => [


                'filter_key' => 'il!time_s',


                'title' => $this->l('起始時間'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],


            'time_e' => [


                'filter_key' => 'il!time_e',


                'title' => $this->l('結束時間'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],


            'address' => [


                'filter_key' => 'il!address',


                'title' => $this->l('活動地點'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],


            'add_name' => [


                'filter_key' => 'il!add_name',


                'title' => $this->l('地點名稱'),


                'order' => true,


                'filter' => true,


                'show' => false,


                'class' => 'text-center',


            ],

            'cost' => [


                'filter_key' => 'il!cost',


                'title' => $this->l('費用'),


                'order' => true,


                'filter' => true,


                'show' => false,


                'class' => 'text-center',


            ],

            'organizer' => [


                'filter_key' => 'il!organizer',


                'title' => $this->l('主辦單位'),


                'order' => true,


                'filter' => true,


                'show' => false,


                'class' => 'text-center',


            ],

            'co_organizer' => [


                'filter_key' => 'il!co_organizer',


                'title' => $this->l('協辦單位'),


                'order' => true,


                'filter' => true,


                'show' => false,


                'class' => 'text-center',


            ],

            'date' => [


                'filter_key' => 'il!date',


                'title' => $this->l('發佈日期'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],


            'content' => [


                'filter_key' => 'il!content',


                'title' => $this->l('活動說明'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


                'show' => false,


            ],


            'description' => [


                'filter_key' => 'il!description',


                'title' => $this->l('Description'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


                'show' => false,


            ],


            'keywords' => [


                'filter_key' => 'il!keywords',


                'title' => $this->l('Keywords'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


                'show' => false,


            ],


            'img' => [


                'title' => $this->l('縮圖'),


                'order' => true,


                'filter' => true,


                'filter_sql' => 'IF(ilf.`id_ilf` > 0, 1, 0)',


                'class' => 'text-center', 'values' => [


                    [


                        'class' => 'img_1',


                        'value' => '1',


                        'title' => $this->l('有'),


                    ],


                    [


                        'class' => 'img_0 red',


                        'value' => '0',


                        'title' => $this->l('無'),


                    ],


                ],


            ],


            'add_time' => [


                'filter_key' => 'il!add_time',


                'title' => $this->l('建立時間'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


                'show' => false,


            ],


            'active' => [


                'filter_key' => 'il!active',


                'title' => $this->l('啟用狀態'),


                'order' => true,


                'filter' => true,


                'class' => 'text-center',


            ],


        ];


        $this->fields['tabs'] = [

            'letter' => $this->l('活動分享'),

        ];


        $this->arr_file_type = [


            '0' => 'img',


            '1' => 'web_banner',


            '2' => 'web_link_img',


            '3' => 'app_img',


            '4' => 'app_banner',


        ];


        $this->fields['form'] = [


            'in_life' => [


                'tab' => 'letter',


                'legend' => [


                    'title' => $this->l('活動分享'),


                    'icon' => 'icon-cogs',


                    'image' => '',


                ],


                'input' => [


                    'id_in_life' => [


                        'name' => 'id_in_life',


                        'type' => 'hidden',


                        'label_col' => 3,


                        'index' => true,


                        'required' => true,


                    ],


                    'top' => [


                        'name' => 'top',


                        'type' => 'checkbox',


                        'label_col' => 3,


                        'label' => $this->l('置頂'),


                        'values' => [


                            [


                                'id' => 'top',


                                'value' => 1,


                                'label' => $this->l('置頂'),


                            ],


                        ],


                    ],


                    'title' => [


                        'name' => 'title',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('活動名稱'),


                        'maxlength' => '14',


                        'required' => true,


                    ],


                    'title2' => [


                        'name' => 'title2',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('副標'),


                        'maxlength' => '20',


                    ],


                    'exp' => [


                        'name' => 'exp',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('活動介紹'),


                        'maxlength' => '100',


                    ],


                    'date_s' => [


                        'name' => 'date_s',


                        'type' => 'date',


                        'label_col' => 3,


                        'label' => $this->l('時間場次'),


                        'is_prefix' => true,


                    ],


                    'date_e' => [


                        'name' => 'date_e',


                        'type' => 'date',


                        'prefix' => $this->l('~'),


                        'is_suffix' => true,


                    ],


                    'weekday' => [


                        'name' => 'weekday',


                        'type' => 'checkbox',


                        'in_table' => true,


                        'multiple' => true,


                        'label_col' => 3,


                        'col' => 9,


                        'values' => [


                            [


                                'id' => 'weekday_1',


                                'value' => 1,


                                'label' => $this->l('週一'),


                            ],


                            [


                                'id' => 'weekday_2',


                                'value' => 2,


                                'label' => $this->l('週二'),


                            ],


                            [


                                'id' => 'weekday_3',


                                'value' => 3,


                                'label' => $this->l('週三'),


                            ],


                            [


                                'id' => 'weekday_4',


                                'value' => 4,


                                'label' => $this->l('週四'),


                            ],


                            [


                                'id' => 'weekday_5',


                                'value' => 5,


                                'label' => $this->l('週五'),


                            ],


                            [


                                'id' => 'weekday_6',


                                'value' => 6,


                                'label' => $this->l('週六'),


                            ],


                            [


                                'id' => 'weekday_0',


                                'value' => 0,


                                'label' => $this->l('週日'),


                            ],


                        ],


                    ],


                    'time_s' => [


                        'name' => 'time_s',


                        'type' => 'time',


                        'is_prefix' => true,


                    ],


                    'time_e' => [


                        'name' => 'time_e',


                        'type' => 'time',


                        'prefix' => $this->l('~'),


                        'is_suffix' => true,


                    ],


                    'address' => [


                        'name' => 'address',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('活動地點'),


                        'maxlength' => '30',


                    ],


                    'add_name' => [


                        'name' => 'add_name',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('地點名稱'),


                        'maxlength' => '50',


                    ],

                    'cost' => [


                        'name' => 'cost',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('費用'),


                        'maxlength' => '10',


                    ],

                    'organizer' => [


                        'name' => 'organizer',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('主辦單位'),


                        'maxlength' => '50',


                    ],

                    'co_organizer' => [


                        'name' => 'co_organizer',


                        'type' => 'text',


                        'label_col' => 3,


                        'label' => $this->l('協辦單位'),


                        'maxlength' => '64',


                    ],

                    'url' => [

                        'name' => 'url',

                        'type' => 'text',

                        'label_col' => 3,

                        'label' => $this->l('網頁位置(如有網址則轉向)'),

                        'maxlength' => '64',

                    ],

                    'date' => [


                        'name' => 'date',


                        'type' => 'date',


                        'label_col' => 3,


                        'label' => $this->l('發佈日期'),


                        'val' => today(),


                        'required' => true,


                    ],


                    'content' => [


                        'name' => 'content',


                        'type' => 'textarea',


                        'label_col' => 3,


                        'label' => $this->l('活動說明'),


                        'class' => 'tinymce',


                        'required' => true,


                        'col' => 8,


                        'rows' => 30,


                    ],


                    'description' => [


                        'name' => 'description',


                        'type' => 'text',


                        'label_col' => 3,


                        'maxlength' => 300,


                        'label' => $this->l('Description'),


                    ],


                    'keywords' => [


                        'name' => 'keywords',


                        'type' => 'text',


                        'label_col' => 3,


                        'maxlength' => 300,


                        'data' => ['role' => 'tagsinput'],


                        'label' => $this->l('Keywords'),


                    ],


                    'add_time' => [


                        'name' => 'add_time',


                        'type' => 'view',


                        'label_col' => 3,


                        'auto_datetime' => 'add',


                        'label' => $this->l('建立時間'),


                    ],


                    'views' => [


                        'name' => 'views',


                        'label' => $this->l('瀏覽次數'),


                        'val' => 0,


                        'type' => 'view',


                        'label_col' => 3,


                    ],


                    'active' => [


                        'type' => 'switch',


                        'label_col' => 3,


                        'label' => $this->l('啟用狀態'),


                        'name' => 'active',


                        'val' => 1,


                        'values' => [


                            [


                                'id' => 'active_on',


                                'value' => 1,


                                'label' => $this->l('啟用'),


                            ],


                            [


                                'id' => 'active_off',


                                'value' => 0,


                                'label' => $this->l('關閉'),


                            ],


                        ],


                    ],


                ],


                'submit' => [


                    [


                        'title' => $this->l('儲存'),


                    ],


                ],


                'cancel' => [


                    'title' => $this->l('取消'),


                ],


                'reset' => [


                    'title' => $this->l('復原'),


                ],


            ],

            'img' => [


                'tab' => 'letter',


                'legend' => [


                    'title' => $this->l('縮圖'),


                    'icon' => 'icon-cogs',


                    'image' => '',


                ],


                'input' => [


                    'img' => [


                        'name' => 'img',


                        'label_class' => 'text-left',


                        'label' => $this->l('[網站]列表縮圖'),


                        'type' => 'file',


                        'language' => 'zh-TW',


                        'file' => [


                            'icon' => false,


                            'auto_upload' => true,


                            'max' => 1,


                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],


                            'max_width' => 428,


                            'max_height' => 285,


                            'resize' => true,


                            'resizePreference' => 'height',


                        ],


                        'multiple' => true,


                        'col' => 12,


                        'no_action' => true,


                        'p' => $this->l('圖片建議大小 428*285px'),


                    ],


                    'web_banner' => [


                        'name' => 'web_banner',


                        'label_class' => 'text-left',


                        'label' => $this->l('[網站]橫幅圖片 注意:沒有該圖片則顯是預設圖'),


                        'type' => 'file',


                        'language' => 'zh-TW',


                        'file' => [


                            'icon' => false,


                            'auto_upload' => true,


                            'max' => 1,


                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],


                            'max_width' => 1300,


                            'max_height' => 285,


                            'resize' => true,


                            'resizePreference' => 'height',


                        ],


                        'multiple' => true,


                        'col' => 12,


                        'no_action' => true,


                        'p' => $this->l('圖片建議大小 1300*240px'),


                    ],


                    'web_link_img' => [


                        'name' => 'web_link_img',


                        'label_class' => 'text-left',


                        'label' => $this->l('[網站]連結圖片'),


                        'type' => 'file',


                        'language' => 'zh-TW',


                        'file' => [


                            'icon' => false,


                            'auto_upload' => true,


                            'max' => 1,


                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],


                            'max_width' => 428,


                            'max_height' => 285,


                            'resize' => true,


                            'resizePreference' => 'height',


                        ],


                        'multiple' => true,


                        'col' => 12,


                        'no_action' => true,


                        'p' => $this->l('圖片建議大小 317*240px'),


                    ],


                    'app_img' => [


                        'name' => 'app_img',


                        'label_class' => 'text-left',


                        'label' => $this->l('[APP]列表縮圖'),


                        'type' => 'file',


                        'language' => 'zh-TW',


                        'file' => [


                            'icon' => false,


                            'auto_upload' => true,


                            'max' => 1,


                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],


                            'max_width' => 428,


                            'max_height' => 285,


                            'resize' => true,


                            'resizePreference' => 'height',


                        ],


                        'multiple' => true,


                        'col' => 12,


                        'no_action' => true,


                        'p' => $this->l('圖片建議大小 317*240px'),


                    ],


                    'app_banner' => [


                        'name' => 'app_banner',


                        'label_class' => 'text-left',


                        'label' => $this->l('[APP]橫幅圖片'),


                        'type' => 'file',


                        'language' => 'zh-TW',


                        'file' => [


                            'icon' => false,


                            'auto_upload' => true,


                            'max' => 1,


                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],


                            'max_width' => 428,


                            'max_height' => 285,


                            'resize' => true,


                            'resizePreference' => 'width',


                        ],


                        'multiple' => true,


                        'col' => 12,


                        'no_action' => true,


                        'p' => $this->l('圖片建議大小 317*240px'),


                    ],


                ],


            ],


        ];


        parent::__construct();


    }


    public function setMedia()


    {


        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');


        parent::setMedia();


        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');


        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');


        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');


    }


    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)


    {


        $json = new JSON();


        $UpFileVal = (array)$json->decode($UpFileVal);


        $this->arr_file_type = [


            '0' => 'img',


            '1' => 'web_banner',


            '2' => 'web_link_img',


            '3' => 'app_img',


            '4' => 'app_banner',


        ];


        $arr_type_key = array_flip($this->arr_file_type);       //key值調換


        $type = $arr_type_key[$UpFileVal['type']];


        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);


        $id_in_life = $UpFileVal['id_in_life'];


        foreach ($arr_id_file as $i => $id_file) {


            //建案檔案


            $sql = sprintf('INSERT INTO `in_life_file` (`id_in_life`, `id_file`, `file_type`) VALUES (%d, %d, %d)',


                GetSQL($id_in_life, 'int'),


                GetSQL($id_file, 'int'),


                GetSQL($type, 'int')


            );


            Db::rowSQL($sql);


            if (!Db::getContext()->num)


                return false;


        }


        return true;


    }


    public function iniUpFileDir()


    {


        $UpFileVal = Tools::getValue('UpFileVal');


        $type = $UpFileVal['type'];


        $json = new JSON();


        $UpFileVal = (array)$json->decode($UpFileVal);


        $id_in_life = $UpFileVal['id_in_life'];


        list($dir, $url) = str_dir_change($id_in_life);     //切割資料夾


//        switch ($type) {


//            case 'web_banner':


//            case 'web_link_img':


//            case 'app_img':


//            case 'app_banner':


//                $dir = WEB_DIR . DS . IN_LIFE_IMG_DIR . DS . $dir . DS . $type;


//            case 'img':


//            default:


//                $dir = WEB_DIR . DS . IN_LIFE_IMG_DIR . DS . $dir;


//                break;


//        }


        $dir = WEB_DIR . DS . IN_LIFE_IMG_DIR . DS . $dir;


        return $dir;


    }


    public function iniUpFileUrl()


    {


        $UpFileVal = Tools::getValue('UpFileVal');


        $type = $UpFileVal['type'];


        $json = new JSON();


        $UpFileVal = (array)$json->decode($UpFileVal);


        $id_in_life = $UpFileVal['id_in_life'];


        list($dir, $url) = str_dir_change($id_in_life);


//        switch ($type) {


//            case 'web_banner':


//            case 'web_link_img':


//            case 'app_img':


//            case 'app_banner':


//                $url = IN_LIFE_IMG_URL . $url.'/'.$type;


//            case 'img':


//            default:


//                $url = IN_LIFE_IMG_URL . $url;


//                break;


//        }


        $url = IN_LIFE_IMG_URL . $url;


        return $url;


    }


    public function getUpFile($id)


    {


        return InLife::getFile($id);


    }


    public function getUpFileList()


    {


        $id_in_life = Tools::getValue('id_in_life');


        $json = new JSON();


        $UpFileVal = [];


        foreach ($this->arr_file_type as $i => $type_neme) {


            $UpFileVal[$type_neme] = '\'' . $json->encode([


                    'id_in_life' => $id_in_life,


                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型


                ]) . '\'';


        }


        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');


        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');


        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');


        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');


        $arr_type_row = $this->getUpFile($id_in_life);


        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {


            foreach ($arr_type_row as $id => $type) {


                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊


                foreach ($arr_row as $i => $v) {


                    if ($_SESSION['id_group'] == 1 || $file_download) {


                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);


                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);


                    } else {


                        $url = '';


                    }

                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $del_table = "in_life_file";
                    $arr_initialPreviewConfig[$type_name][] = [


                        'type' => $file_type,


                        'filetype' => $v['type'],


                        'caption' => $v['filename'],


                        'size' => $v['size'],


                        'url' => '/manage/InLife?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',


                        'downloadUrl' => $url,


                        'key' => $v['id_file'],


                    ];


                }


            }


        }


        foreach ($arr_initialPreview as $i => $v) {


            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);


            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);


        }


        Context::getContext()->smarty->assign([


            'initialPreview' => $arr_initialPreview,


            'initialPreviewConfig' => $arr_initialPreviewConfig,


            'UpFileVal' => $UpFileVal,


        ]);


    }


}