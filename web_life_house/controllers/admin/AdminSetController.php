<?php

class AdminSetController extends AdminController
{
    public $stay = false;

    public function __construct()
    {

        $this->className = 'AdminSetController';
        $this->fields['index'] = 'id_admin';
        $this->table     = 'admin';
        $this->fields['where'] .= ' AND a.`id_admin`= '.$_SESSION['id_admin'];
        $this->_as             = 'a';
        $this->fields['title'] = '個人帳號';
        $this->fields['list_num'] = 1;
        $this->_group= ' GROUP BY a.id_admin  ';

        $this->fields['list']  = [
            'id_admin'   => [
                'filter_key' => 'a!id_admin',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
                'order'  => true,
            ],
            'id_web'     => [
                'title'  => $this->l('加盟店名稱'),
                'order'  => true,
                'filter' => true,
                'key'    => [
                    'table' => 'web',
                    'key'   => 'id_web',
                    'val'   => 'web',
                    'where' => $this->my_where_web,
                    'order' => '`web` ASC',
                ],
                'class'  => 'text-center',
            ],

            'first_name' => [
                'filter_key' => 'a!first_name',
                'title'  => $this->l('姓名'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'first_name_en' => [
                'filter_key' => 'a!first_name_en',
                'title'  => $this->l('英文。姓名'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'ag'         => [
                'filter_key' => 'a!ag',
                'title'  => $this->l('AG代碼'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'ag_title_zh'         => [
                'filter_key' => 'a!ag_title_zh',
                'title'  => $this->l('中文職稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'ag_title_en'         => [
                'filter_key' => 'a!ag_title_en',
                'title'  => $this->l('英文職稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'phone'         => [
                'filter_key' => 'a!phone',
                'title'  => $this->l('手機號碼'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'email'      => [
                'filter_key' => 'a!email',
                'title'  => $this->l('帳號'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'active'     => [
                'title'      => $this->l('啟用狀態'),
                'filter_key' => 'a!active',
                'order'      => true,
                'filter'     => true,
                'class'      => 'text-center',
            ],

        ];


        if ((Tools::getValue('id_admin') == $_SESSION['id_admin']) && $_SESSION['admin'] != 1 && $_SESSION['id_group'] != 1) {
            $this->stay = true;
        }

        $this->fields['form'] = [
            'admin' => [
                'legend' => [
                    'title' => $this->l('帳號管理'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_admin'        => [
                        'name'     => 'id_admin',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],

                    'last_name'      => [
                        'type'        => 'text',
                        'label'       => $this->l('姓'),
                        'placeholder' => $this->l('姓'),
                        'name'        => 'last_name',
                        'maxlength'   => '20',
                        'required'    => true,
                    ],
                    'first_name'      => [
                        'type'        => 'text',
                        'label'       => $this->l('名'),
                        'placeholder' => $this->l('名'),
                        'name'        => 'first_name',
                        'maxlength'   => '20',
                        'required'    => true,
                    ],
                    'last_name_en'      => [
                        'type'        => 'text',
                        'label'       => $this->l('英文。姓'),
                        'placeholder' => $this->l('英文。姓'),
                        'name'        => 'last_name_en',
                        'maxlength'   => '20',
                        'required'    => false,
                    ],
                    'first_name_en'      => [
                        'type'        => 'text',
                        'label'       => $this->l('英文。名'),
                        'placeholder' => $this->l('英文。名'),
                        'name'        => 'first_name_en',
                        'maxlength'   => '20',
                        'required'    => false,
                    ],
                    'ag'              => [
                        'name'      => 'ag',
                        'type'      => 'view',
                        'label'     => $this->l('AG代碼'),
                        'unique'    => true,
                        'maxlength' => '20',
                        'required'    => true,
                    ],
                    'ag_title_zh'         => [
                        'name'      => 'ag_title_zh',
                        'type'      => 'text',
                        'label'     => $this->l('中文職稱'),
                        'maxlength' => '20',
                        'required'    => false,
                    ],
                    'ag_title_en'         => [
                        'name'      => 'ag_title_en',
                        'type'      => 'text',
                        'label'     => $this->l('英文職稱'),
                        'maxlength' => '20',
                        'required'    => false,
                    ],
                    'email'           => [
                        'type'      => 'view',
                        'required'  => true,
                        'label'     => $this->l('帳號'),
                        'name'      => 'email',
                        'maxlength' => '100',
                        'desc'      => $this->l('請輸入e-mail'),
                    ],
                    'phone'           => [
                        'type'      => 'text',
//                        'required'  => true,
                        'label'     => $this->l('手機號碼'),
                        'name'      => 'phone',
                        'maxlength' => '20',
                        'desc'      => $this->l('請輸入手機號碼業務必填 否則無法接收到簡訊內容'),
                    ],

                    'change-password' => [
                        'type'       => 'change-password',
                        'col'        => 5,
                        'label'      => $this->l('修改密碼'),
                        'old_passwd' => false,
                        'minlength'  => '4',
                        'maxlength'  => '20',
                        'name'       => 'password',
                    ],
                    'password'        => [
                        'type'        => 'new-password',
                        'label'       => $this->l('密碼'),
                        'placeholder' => $this->l('請輸入密碼'),
                        'required'    => true,
                        'minlength'   => '4',
                        'maxlength'   => '20',
                        'name'        => 'password',
                    ],
                    'password2'       => [
                        'type'        => 'confirm-password',
                        'label'       => $this->l('確認密碼'),
                        'placeholder' => $this->l('再次輸入確認密碼'),
                        'required'    => true,
                        'minlength'   => '4',
                        'maxlength'   => '20',
                        'name'        => 'password2',
                        'confirm'     => 'password',
                    ],

                ],
                'tpl'=>'themes/LifeHouse/set_nameCard.tpl',
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],

            ],
        ];



        parent::__construct();

        //echo print_r($this->fields['list']);

    }



    public function initToolbar()
    {        //初始化功能按鈕
//            unset($this->toolbar_btn['save']);
//            unset($this->toolbar_btn['add']);
//        parent::initToolbar();

    }






}
