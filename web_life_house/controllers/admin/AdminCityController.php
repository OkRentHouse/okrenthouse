<?php

class AdminCityController extends AdminController
{
    public $no_link = false;

    public function __construct()
    {

        $this->className            = 'AdminCityController';
        $this->table                = 'city';
        $this->fields['index']      = 'id_city';
        $this->fields['title']      = '鄉政市';
        $this->fields['list_num'] = 50;
        $this->_as = 'c';


        $this->_join = ' LEFT JOIN `county` AS cc ON cc.`id_county` = c.`id_county`';


        $this->_group = ' GROUP BY c.`id_city`';


        $this->fields['order'] = ' ORDER BY cc.`id_county` ASC, c.`id_city` ASC';



        $this->actions[]            = 'list';
        $this->fields['list']       = [
            'id_city'          => [
                'index'  => true,
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'county_name'           => [
                'title'  => $this->l('縣市'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'county_code' => [
                'title'  => $this->l('縣市代碼'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'city_name'          => [
                'title'  => $this->l('鄉鎮'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'city_code'       => [
                'title'  => $this->l('鄉鎮代碼'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];



        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('選單'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    [
                        'name'     => 'id_city',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],

                    'id_county' => [
                        'name'      => 'id_county',
                        'type'      => 'select',
                        'label_col' => 3,
                        'label'     => $this->l('選擇縣市'),
                        'options'   => [
                            'default' => [
                                'text' => '選擇縣市',
                                'val'  => '',
                            ],
                            'table'   => 'county',
                            'text'    => 'county_name',
                            'value'   => 'id_county',
                            'order'   => '`id_county` ASC',
                        ],
                        'required'  => true,
                    ],

                    [
                        'type'        => 'text',
                        'required'    => true,
                        'label'       => $this->l('請輸入鄉鎮名稱'),
                        'placeholder' => $this->l('請輸入鄉鎮名稱'),
                        'name'        => 'city_name',
                        'maxlength'   => '50',
                    ],
                    [
                        'type'        => 'text',
//                        'required'    => true,
                        'label'       => $this->l('請輸入鄉鎮代碼'),
                        'placeholder' => $this->l('請輸入鄉鎮代碼'),
                        'name'        => 'city_code',
                        'maxlength'   => '50',
                    ],

                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
        parent::__construct();
    }



    /*
    public function initToolbar(){		//初始化功能按鈕
        if ($this->display == 'add' || $this->display == 'edit') {
            $this->toolbar_btn['save_and_stay'] = array(
                'href' => '#',
                'desc' => $this->l('儲存並繼續編輯')
            );
        }
        parent::initToolbar();
    }
    */
}