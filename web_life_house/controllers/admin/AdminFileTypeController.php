<?php
class AdminFileTypeController extends AdminController
{
	public function __construct(){
		$this->className = 'AdminFileTypeController';
		$this->table = 'file_type';
		$this->fields['index'] = 'id_file_type';
		$this->fields['title'] = '檔案分類';
		$this->fields['list'] = array(
			'id_file_type' => array(
				'index' => true,
				'title' => $this->l('檔案分類ID'),
				'type' => 'checkbox',
				'hidden' => true,
				'class' => 'text-center'
			),
			'file_type' => array(
				'title' => $this->l('檔案分類'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'position' => array(
				'title' => $this->l('順序'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			)
		);
		
		$this->fields['form'] = array(
			array(
				'legend' => array(
					'title' => $this->l('檔案分類'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'id_file_type',
						'type' => 'hidden',
						'index' => true,
						'required' => true),
					array(
						'name' => 'file_type',
						'type' => 'text',
						'label' => $this->l('檔案分類'),
						'maxlength' => '20',
						'required' => true),
					array(
						'name' => 'position',
						'type' => 'number',
						'label' => $this->l('順序')),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					),
				),
				'cancel' => array(
					'title' => $this->l('取消')),
				'reset' => array(
					'title' => $this->l('復原'))
			)
		);
		
		parent::__construct();
	}
	
}