<?php
class AdminCMSController extends AdminController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'cms';
	public $ed;
//注意CMS相關 連接至classes的CMS.php與 Dispatcher 中的setURL有關
	public function __construct()
	{
		$this->className       = 'AdminCMSController';
		$this->fields['title'] = '網頁管理';
		$this->table           = 'cms';
		$this->fields['index'] = 'id_cms';
		$this->fields['order'] = ' ORDER BY `id_web` ASC, `url` ASC';

        //測試上傳檔案
        $this->arr_file_type = [
            '0' => 'tpl_file',
            '1' => 'js_file',
            '2' => 'css_file',
        ];

		$this->fields['list']  = [
			'id_cms'         => [
				'index'  => true,
				'hidden' => true,
				'type'   => 'checkbox',
				'label_col' => '2',
				'col' => '3',
				'class'  => 'text-center',
			],
			'id_web'         => [
				'title'  => $this->l('網站'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'id_web_1',
						'value' => '1',
						'title' => $this->l('APP'),
					],
					[
						'class' => 'id_web_2',
						'value' => '2',
						'title' => $this->l('生活房屋'),
					],
					[
						'class' => 'id_web_3',
						'value' => '3',
						'title' => $this->l('生活集團'),
					],
					[
						'class' => 'id_web_4',
						'value' => '4',
						'title' => $this->l('生活樂租'),
					],
					[
						'class' => 'id_web_5',
						'value' => '5',
						'title' => $this->l('生活居家修繕'),
					],
					[
						'class' => 'id_web_6',
						'value' => '6',
						'title' => $this->l('生活好科技'),
					],
                    [
                        'class' => 'id_web_7',
                        'value' => '7',
                        'title' => $this->l('共享圈'),
                    ],
                    [
                        'class' => 'id_web_8',
                        'value' => '8',
                        'title' => $this->l('好幫手'),
                    ],
                    [
                        'class' => 'id_web_9',
                        'value' => '9',
                        'title' => $this->l('生活樂購'),
                    ],
                    [
                        'class' => 'id_web_10',
                        'value' => '10',
                        'title' => $this->l('裝修達人'),
                    ],
                    [
                        'class' => 'id_web_11',
                        'value' => '11',
                        'title' => $this->l('生活好康'),
                    ],
                    [
                        'class' => 'id_web_11',
                        'value' => '12',
                        'title' => $this->l('共享圈App'),
                    ],
				],
			],
			'url'            => [
				'title'  => $this->l('URL'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'title'          => [
				'title'  => $this->l('Title'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'page_title'     => [
				'title'  => $this->l('頁面標題'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'description'    => [
				'title'  => $this->l('Description'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'keywords'       => [
				'title'  => $this->l('Keywords'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'display_header' => [
				'title'  => $this->l('顯示Header'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'display_h_1',
						'value' => '1',
						'title' => $this->l('顯示'),
					],
					[
						'class' => 'display_h_0',
						'value' => '0',
						'title' => $this->l('隱藏'),
					],
				],
			],
			'display_footer' => [
				'title'  => $this->l('顯示Foote'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'display_f_1',
						'value' => '1',
						'title' => $this->l('顯示'),
					],
					[
						'class' => 'display_f_0',
						'value' => '0',
						'title' => $this->l('隱藏'),
					],
				],
			],
            'position' => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'class'  => 'text-center',
                'filter' => true,
            ],
            'related_tabs'    => [
                'title'  => $this->l('開啟頁籤(加入:{item_wrap})'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'values' => [
                    [
                        'class' => 'related_tabs_1',
                        'value' => '1',
                        'title' => $this->l('開啟'),
                    ],
                    [
                        'class' => 'related_tabs_0',
                        'value' => '0',
                        'title' => $this->l('關閉'),
                    ],
                ],
            ],
			'index_ation'    => [
				'title'  => $this->l('開啟索引'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'index_a_1',
						'value' => '1',
						'title' => $this->l('開啟'),
					],
					[
						'class' => 'index_a_0',
						'value' => '0',
						'title' => $this->l('關閉'),
					],
				],
			],
			'active'         => [
				'title'  => $this->l('啟用'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'editor_chief'         => [
				'title'  => $this->l('小編'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('網頁管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_cms' => [
						'name'     => 'id_cms',
						'type'     => 'hidden',
						'label_col' => '2',
						'col' => '3',
						'index'    => true,
						'required' => true,
					],
					'id_web' => [
						'name'     => 'id_web',
						'type'     => 'select',
						'label_col' => '2',
						'col' => '3',
						'label' => $this->l('網站'),
						'options'  => [
							'default' =>
								[
									'val'  => '',
									'text' => '請選擇網站',
								],
							'val'     => [
								[
									'val'  => '1',
									'text' => $this->l('APP'),
								],
								[
									'val'  => '2',
									'text' => $this->l('生活房屋'),
								],
								[
									'val'  => '3',
									'text' => $this->l('生活集團'),
								],
								[
									'val'  => '4',
									'text' => $this->l('生活樂租'),
								],
								[
									'val'  => '5',
									'text' => $this->l('生活居家修繕'),
								],
								[
									'val'  => '6',
									'text' => $this->l('生活好科技'),
								],
                                [
                                    'val'  => '7',
                                    'text' => $this->l('共享圈'),
                                ],
                                [
                                    'val'  => '8',
                                    'text' => $this->l('好幫手'),
                                ],
                                [
                                    'val'  => '9',
                                    'text' => $this->l('生活樂購'),
                                ],
                                [
                                    'val'  => '10',
                                    'text' => $this->l('裝修達人'),
                                ],
                                [
                                    'val'  => '11',
                                    'text' => $this->l('生活好康'),
                                ],
                                [
                                    'val'  => '12',
                                    'text' => $this->l('共享圈App'),
                                ],
							],
						],
						'required' => true,
					],
					'url'            => [
						'name'      => 'url',
						'type'      => 'text',
						'label_col' => '2',
						'col' => '3',
						'required'  => true,
						'maxlength' => 200,
						'label'     => $this->l('URL'),
						'hint'      => $this->l('網址符號只能用 _- 字元'),
					],
					'title'          => [
						'name'      => 'title',
						'type'      => 'text',
						'label_col' => '2',
						'col' => '3',
						'maxlength' => 100,
						'label'     => $this->l('Title'),
					],
					'page_title'     => [
						'name'      => 'page_title',
						'type'      => 'text',
						'label_col' => '2',
						'col' => '3',
						'maxlength' => 50,
						'label'     => $this->l('頁面標題'),
					],
					'description'    => [
						'name'      => 'description',
						'type'      => 'text',
						'label_col' => '2',
						'col' => '3',
						'maxlength' => 300,
						'label'     => $this->l('Description'),
					],
					'keywords'       => [
						'name'      => 'keywords',
						'type'      => 'text',
						'label_col' => '2',
						'col' => '3',
						'class'     => 'no_enter',
						'maxlength' => 300,
						'data'      => ['role' => 'tagsinput'],
						'label'     => $this->l('Keywords'),
					],

                    'introduce_id_cms' =>[
                        'name'      => 'introduce_id_cms',
                        'type'      => 'number',
                        'label_col' => '2',
                        'col' => '3',
                        'maxlength' => 11,
                        'label'     => $this->l('引用其他cms的網頁內容,css,js版型'),
                    ],
					'html'           => [
						'name'     => 'html',
						'type'     => 'textarea',
						'label_col' => '2',
						'col'      => 10,
						'rows'     => 30,
						'required' => true,
						'class'    => 'tinymce',
						'label'    => $this->l('網頁內容'),
					],
					'css'           => [
						'name'     => 'css',
						'type'     => 'textarea',
						'label_col' => '2',
						'col'      => 10,
						'rows'     => 10,
						'label'    => $this->l('CSS'),
					],
					'js'           => [
						'name'     => 'js',
						'type'     => 'textarea',
						'label_col' => '2',
						'col'      => 10,
						'rows'     => 10,
						'label'    => $this->l('JS'),
					],

                    'internal_introduce'=>[
                        'name'     => 'internal_introduce',
                        'type'     => 'textarea',
                        'label_col' => '2',
                        'col'      => 10,
                        'rows'     => 3,
                        'label'     => $this->l('引入.css,.js用,分開'),
                    ],

                    'tpl_file' => [
                        'name' => 'tpl_file',
                        'label_class' => 'text-left',
                        'label' => $this->l('tpl版型'),
                        'type' => 'file',
                        'col' => 12,
                        'no_action' => true,
                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['tpl'],
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'p' => $this->l('上傳tpl'),
                    ],
                    'js_file' => [
                        'name' => 'js_file',
                        'label_class' => 'text-left',
                        'label' => $this->l('js版型'),
                        'type' => 'file',
                        'allowed' => ['js'],
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('上傳js'),
                    ],
                    'css_file' => [
                        'name' => 'css_file',
                        'label_class' => 'text-left',
                        'label' => $this->l('css版型'),
                        'type' => 'file',
                        'allowed' => ['css'],
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('上傳css'),
                    ],


                    'customer_message'=>[
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('使用留言(button 需加href="#modal-message" data-toggle="modal")'),
                        'name'     => 'customer_message',
                        'required' => true,
                        'val'      => 0,
                        'values'   => [
                            [
                                'id'    => 'customer_message_1',
                                'value' => 0,
                                'label' => $this->l('不啟用'),
                            ],
                            [
                                'id'    => 'customer_message_0',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                        ],
                    ],
                    'display_header' => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('顯示Header'),
                        'name'     => 'display_header',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'display_h_1',
                                'value' => 1,
                                'label' => $this->l('顯示'),
                            ],
                            [
                                'id'    => 'display_h_0',
                                'value' => 0,
                                'label' => $this->l('隱藏'),
                            ],
                        ],
                    ],
                    'display_footer' => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('顯示Footer'),
                        'name'     => 'display_footer',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'display_f_1',
                                'value' => 1,
                                'label' => $this->l('顯示'),
                            ],
                            [
                                'id'    => 'display_f_0',
                                'value' => 0,
                                'label' => $this->l('隱藏'),
                            ],
                        ],
                    ],
                    'index_ation'    => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('開啟索引'),
                        'name'     => 'index_ation',
                        'required' => true,
                        'val'      => 1,
                        'hint'     => $this->l('若關閉搜尋引擎將無法搜尋到此網頁'),
                        'values'   => [
                            [
                                'id'    => 'index_a_1',
                                'value' => 1,
                                'label' => $this->l('開啟'),
                            ],
                            [
                                'id'    => 'index_a_0',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    'related_tabs'    => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('開啟頁籤'),
                        'name'     => 'related_tabs',
                        'val'      => 0,
                        'hint'     => $this->l('版面上頭增加頁籤(與相關頁籤也會增加)'),
                        'values'   => [
                            [
                                'id'    => 'related_tabs_0',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                            [
                                'id'    => 'related_tabs_1',
                                'value' => 1,
                                'label' => $this->l('開啟'),
                            ],
                        ],
                    ],
                    'active'         => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('啟用狀態'),
                        'name'     => 'active',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
										'editor_chief'         => [
                        'type'     => 'switch',
                        'label_col' => '2',
                        'col' => '3',
                        'label'    => $this->l('小編編輯'),
                        'name'     => 'editor_chief',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'editor_chief_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'editor_chief_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
				],

				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
					[
						'title' => $this->l('儲存並繼續編輯'),
						'stay'  => true,
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
		];
		parent::__construct();
	}

	public function validateRules()
	{
		parent::validateRules();
		$url = Tools::getValue('url');
		if (!isTwUrl($url)) {
			$this->_errors[] = $this->l('網址符號只能用 _- 字元');
		}
		if (isints($url)) {
			$this->_errors[] = $this->l('網址不能只是數字');
		}
	}


	public function setMedia()
	{
		$this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
		parent::setMedia();
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
	}


    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal){
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_cms = $UpFileVal['id_cms'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `cms_file` (`id_cms`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_cms, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );

            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir(){
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_cms = $UpFileVal['id_cms'];
        list($dir, $url) = str_dir_change($id_cms);     //切割資料夾
        $dir = WEB_DIR . DS . CMS_FILE_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl(){
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_cms = $UpFileVal['id_cms'];
        list($dir, $url) = str_dir_change($id_cms);
        $url = CMS_FILE_URL . $url;
        return $url;
    }

    public function getUpFile($id_cms){
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `cms_file`
				WHERE `id_cms` = %d',
            GetSQL($id_cms, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }

    public function getUpFileList(){
        $id_cms = Tools::getValue('id_cms');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_cms' => $id_cms,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_cms);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $del_table = "cms_file";
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/CMS?&id_file=' . $v['id_file'] . '&type_name=' . $type_name . '&table=' . $del_table .  '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }


}
