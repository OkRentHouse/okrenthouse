<?php

class AdminPackageController extends AdminController
{
    public function __construct()
    {
        $this->conn = 'ilifehou_life_go';
        $this->className            = 'AdminPackageController';
        $this->table                = 'package';
        $this->fields['index']      = 'id_package';
        $this->fields['title']      = '大包裝';
        $this->_as = 'p';

        $this->fields['order'] = ' ORDER BY p.`position` ASC';

        $this->fields['list']       = [
            'id_package'          => [
                'index'  => true,
                'filter_key' => 'p!id_package',
                'title'  => $this->l('大包裝id'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'name'    => [
                'filter_key' => 'p!name',
                'title'  => $this->l('名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position'     => [
                'filter_key' => 'p!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('選單'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    [
                        'name'     => 'id_package',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],

                    [
                        'name'      => 'name',
                        'type'      => 'text',
                        'label'     => $this->l('大包裝名稱'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],
                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
        parent::__construct();
    }

}