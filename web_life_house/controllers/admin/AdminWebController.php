<?php

class AdminWebController extends AdminController
{
	public $tpl_folder;    //樣版資料夾

	public function __construct()
	{
		$this->className       = 'AdminWebController';
		$this->table           = 'web';
		$this->fields['index'] = 'id_web';

		$this->fields['where'] = ' AND `id_web` > 0';
		$this->fields['order'] = ' ORDER BY `web` ASC';
		$this->fields['title'] = '加盟店管理';

		$this->fields['list'] = [
			'id_web'   => [
				'index'  => true,
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'web_code' => [
				'title'  => $this->l('加盟店代號'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'web'      => [
				'title'  => $this->l('加盟店名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center mw130',
			],
			'active'   => [
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['list_num'] = 50;

		$this->fields['form']['Web'] = [
			'tab'    => 'Web',
			'legend' => [
				'title' => $this->l('基本資料'),
				'icon'  => 'icon-cogs',
				'image' => '',
			],
			'input'  => [
				[
					'name'     => 'id_web',
					'type'     => 'hidden',
					'index'    => true,
					'required' => true,
				],
				[
					'name'      => 'web',
					'label'     => $this->l('加盟店名稱'),
					'type'      => 'text',
					'maxlength' => 50,
					'required'  => true,
				],
				[
					'name'      => 'web_code',
					'type'      => 'text',
					'required'  => true,
					'unique'    => true,
					'label'     => $this->l('加盟店代號'),
					'maxlength' => '20',
				],
				[
					'type'     => 'switch',
					'label'    => $this->l('啟用狀態'),
					'name'     => 'active',
					'val'      => 1,
					'values'   => [
						[
							'id'    => 'active_1',
							'value' => 1,
							'label' => $this->l('啟用'),
						],
						[
							'id'    => 'active_0',
							'value' => 0,
							'label' => $this->l('關閉'),
						],
					],
					'required' => true,
				],
			],
			'submit' => [
				[
					'title' => $this->l('儲存'),
				],
			],
			'cancel' => [
				'title' => $this->l('取消'),
			],
			'reset'  => [
				'title' => $this->l('重置'),
			],
		];

		$this->fields['form']['web_details'] = [
			'table'  => 'web_details',
			'tab'    => 'Web',
			'legend' => [
				'title' => $this->l('詳細資料'),
				'icon'  => 'icon-cogs',
				'image' => '',
			],
			'input'  => [
				[
					'name'     => 'id_web',
					'type'     => 'hidden',
					'must'     => 'web.id_web',
					'index'    => true,
					'required' => true,
				],
				[
					'name'      => 'address',
					'label'     => $this->l('地址'),
					'type'      => 'text',
					'maxlength' => 500,
				],
				[
					'name'      => 'company',
					'label'     => $this->l('公司名稱'),
					'type'      => 'text',
					'maxlength' => '20',
				],
				[
					'name'      => 'editor',
					'label'     => $this->l('統一編號'),
					'type'      => 'text',
					'maxlength' => '20',
				],
				[
					'name'      => 'tel',
					'label'     => $this->l('電話'),
					'type'      => 'text',
					'maxlength' => '20',
				],
				[
					'name'      => 'fax',
					'label'     => $this->l('傳真'),
					'type'      => 'text',
					'maxlength' => '20',
				],
			],
			'submit' => [
				[
					'title' => $this->l('儲存'),
				],
			],
			'cancel' => [
				'title' => $this->l('取消'),
			],
			'reset'  => [
				'title' => $this->l('重置'),
			],
		];
		parent::__construct();
	}

	public function processDel()
	{
		$id_web = Tools::getValue('id_web');
		$sql    = sprintf('SELECT `id_web`
									FROM `' . DB_PREFIX_ . 'houses`
									WHERE `id_web` = %d
									LIMIT 0, 1',
			GetSQL($id_web, 'int'));
		Db::rowSQL($sql);
		if (Db::getContext()->num()) {
			$this->_errors[] = $this->l('無法刪除正在使用的建案');
		}
		parent::processDel();
	}
}