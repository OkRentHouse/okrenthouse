<?php

class AdminNestedGroupController extends AdminController
{
    public $no_link = false;

    public function __construct()
    {

        $this->className            = 'AdminNestedGroupController';
        $this->table                = 'nested_group';
        $this->fields['index']      = 'id_nested_group';
        $this->fields['title']      = '巢狀集團圖片';
        $this->fields['list_num'] = 50;
        $this->_as = 'n_g';
        $this->_join =' LEFT JOIN nested_group_file as n_g_f ON n_g_f.`id_nested_group` = n_g.`id_nested_group`
                        LEFT JOIN admin as a ON a.`id_admin` = n_g.`update_id_admin` ';

        $this->_group =' GROUP BY n_g.`id_nested_group`';

        $this->fields['list']       = [
            'id_nested_group'          => [
                'index'  => true,
                'filter_key' => 'n_g!id_nested_group',
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'name'           => [
                'title'  => $this->l('標頭'),
                'filter_key' => 'n_g!name',
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'url'           => [
                'title'  => $this->l('網頁'),
                'filter_key' => 'n_g!url',
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position' => [
                'title'  => $this->l('順序'),
                'filter_key' => 'n_g!position',
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'img_0' => [
                'title' => $this->l('縮圖(小圖)'),
                'order' => true,
                'filter' => true,
                'filter_sql' => '(IF((SELECT count(id_ngf) FROM nested_group_file WHERE file_type=0 AND id_nested_group=n_g.`id_nested_group`)>0,1,0))',
                'class' => 'text-center', 'values' => [
                    [
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],
            'img_1' => [
                'title' => $this->l('縮圖(背景大圖)'),
                'order' => true,
                'filter' => true,
                'filter_sql' => '(IF((SELECT count(id_ngf) FROM nested_group_file WHERE file_type=1 AND id_nested_group=n_g.`id_nested_group`)>0,1,0))',
                'class' => 'text-center', 'values' => [
                    [
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],
            'update_name'       => [
                'title'  => $this->l('修改人'),
                'filter_sql' => 'CONCAT(a.`last_name`,a.`first_name`) ',
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->arr_file_type = [
            '0' => 'img',
            '1' => 'img_background',
        ];

        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('巢狀集團'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    [
                        'name'     => 'id_nested_group',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],

                    'name' =>[
                        'type' => 'text',
                        'label' => $this->l('巢狀名'),
                        'name' => 'name',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 3,
                        'maxlength'=>20,
                    ],

                    'update_id_admin' =>[
                        'type' => 'text',
                        'label' => $this->l('更新人'),
                        'name' => 'update_id_admin',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 3,
                        'disabled'=>true,
                    ],

                    'url' =>[
                        'type' => 'text',
                        'label' => $this->l('網頁'),
                        'name' => 'url',
                        'form_col' => 12,
                        'label_col' => 3,
                        'col' => 9,
                        'maxlength'=>128,
                    ],

                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],

                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],

            'img' => [
                'tab' => 'file',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('巢狀小圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],

                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
//                        'p' => $this->l('圖片建議大小 800*600px'),
                    ],

                    'img_background' => [
                        'name' => 'img_background',
                        'label_class' => 'text-left',
                        'label' => $this->l('巢狀背景圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
//                            'max_width' => 800,
//                            'max_height' => 600,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],

                        'col' => 12,
                        'no_action' => true,
//                        'p' => $this->l('圖片建議大小 900*506'),
//                        'p' => $this->l('圖片建議大小 800*600'),
                    ],
                ],
            ],
        ];
        parent::__construct();
    }

    public function processAdd(){
        $_POST['update_id_admin'] = $_SESSION['id_admin'];

        parent::processAdd();
    }

    public function processEdit(){
        $_POST['update_id_admin'] = $_SESSION['id_admin'];

        parent::processEdit();
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);

        $this->arr_file_type = [
            '0' => 'img',
            '1' => 'img_background',
        ];

        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_nested_group = $UpFileVal['id_nested_group'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `nested_group_file` (`id_nested_group`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_nested_group, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_nested_group = $UpFileVal['id_nested_group'];

        list($dir, $url) = str_dir_change($id_nested_group);     //切割資料夾
        $dir = WEB_DIR . DS . NESTED_GROUP_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_nested_group = $UpFileVal['id_nested_group'];
        list($dir, $url) = str_dir_change($id_nested_group);
        $url = NESTED_GROUP_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_nested_group)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `nested_group_file`
				WHERE `id_nested_group` = %d',
            GetSQL($id_nested_group, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }

    public function getUpFileList()
    {
        $id_nested_group = Tools::getValue('id_nested_group');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_nested_group' => $id_nested_group,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_nested_group);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $del_table= "nested_group_file";
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/NestedGroup?&id_file=' . $v['id_nested_group'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }

}