<?php

class AdminRentServiceController extends AdminController
{
	public $page = 'rent_service';

	public function __construct()
	{
		$this->className       = 'AdminRentServiceController';
		$this->fields['title'] = '樂租服務設定';

		$this->fields['tabs'] = [
			'web_set_up' => $this->l(''),
		];

		$this->fields['form'] = [
			'rent_service' => [
				'tab'    => 'rent_service',
				'legend' => [
					'title' => $this->l('樂租服務設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'tenant_title'                              => [
						'name'          => 'tenant_title',
						'type'          => 'text',
						'label'         => $this->l('租客篇標題'),
						'hint'          => $this->l('樂租服務 租客篇上方大標題'),
						'placeholder'   => $this->l('租屋不踩雷 安心沒煩惱'),
						'p' => '若要換行請輸入 "&lt;br&gt;"',
						'configuration' => true,
					],
					'landlord_title'                               => [
						'name'          => 'landlord_title',
						'type'          => 'text',
						'label'         => $this->l('房東篇標題'),
						'hint'          => $this->l('樂租服務 房東篇上方大標題'),
						'placeholder'   => $this->l('獨特服務優勢<br>提供最完善的租管服務'),
						'p' => '若要換行請輸入 "&lt;br&gt;"',
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'options';
		parent::initContent();
	}
}