<?php
class AdminContractIncomeStatementController extends AdminController
{
    public function __construct()
    {
        $this->className = 'AdminContractIncomeStatementController';
        $this->table = 'contract_income_statement';
        $this->fields['index'] = 'id_contract_income_statement';
        $this->_as = 'c_i_s';
        $this->fields['title'] = '合約收支表';
        $this->fields['list_num'] = 50;

        $this->fields['list'] = [
            'id_point_to_pay' => [
                'filter_key' => 'c_i_s!id_contract_income_statement',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
                'order' => true,
                'filter' => true,
            ],

            'rent_house_code' => [
                'filter_key' => 'c_i_s!rent_house_code',
                'title' => $this->l('物件編號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'income_people' => [
                'filter_key' => 'c_i_s!income_people',
                'title' => $this->l('損益人'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'income_people',
                        'value' => 0,
                        'title' => $this->l('屋主/賣方'),
                    ],
                    [
                        'class' => 'income_people',
                        'value' => 1,
                        'title' => $this->l('租客/買方'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'income_type' => [
                'filter_key' => 'c_i_s!income_type',
                'title' => $this->l('損益類型'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'income_type',
                        'value' => 0,
                        'title' => $this->l('支出'),
                    ],
                    [
                        'class' => 'income_type',
                        'value' => 1,
                        'title' => $this->l('收入'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'funds_total' => [
                'filter_key' => 'c_i_s!funds_total',
                'title' => $this->l('金額總計'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
        ];

        $this->fields['tabs'] = [
            'contract_income_statement' => $this->l('合約收支表'),
        ];

        $this->tpl_data_c_i_s =[
            'id_contract_income_statement'=>[
                'name' => 'id_contract_income_statement',
                'type' => 'hidden',
                'label_col' => 3,
                'index' => true,
                'required' => true,
            ],

            'case_name'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'案名',
                'name'=>'case_name',
                'maxlength' => '20',
            ],

            'rent_house_code'=>[
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'type' =>'text',
                'label'=>'租屋物件編號',
                'name'=>'rent_house_code',
                'maxlength' => '20',
            ],

            'income_type' => [
                'name' => 'income_type',
                'type' => 'select',
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'label' => $this->l('損益類型'),
                'values' => [
                    [
                        'text' => '選擇收支',
                        'val' => '',
                    ],
                    [
                        'text' => '支出',
                        'val' => '0',
                    ],
                    [
                        'text' => '收入',
                        'val' => '1',
                    ],
                ],
            ],

             'income_people' => [
                 'name' => 'income_people',
                 'type' => 'select',
                 'form_col' => 6,
                 'label_col' => 3,
                 'col' => 6,
                 'label' => $this->l('損益人'),
                    'values' => [
                        [
                            'text' => '選擇損益人',
                            'val' => '',
                        ],
                        [
                            'text' => '屋主/賣方',
                            'val' => '0',
                        ],
                        [
                            'text' => '租客/買方',
                            'val' => '1',
                        ],
                    ],
             ],


            'item'=>[
                'type' => 'tpl',
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'項目',
                'name' => 'item',
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇收支',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'支出',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'收入',
                        'id'=>'',
                    ],
                ],
                'select'=>[
                    'val'=>',0,1',
                    'text'=>'選擇收支,支出,收入'
                ],
            ],

            'funds_total'=>[
                'form_col' => 12,
                'label_col' => 3,
                'col' => 3,
                'type' =>'number',
                'label'=>'總收支',
                'id'=>'funds_total',
                'name'=>'funds_total',
            ],


            'transfer_in_bank'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入銀行',
                'name'=>'transfer_in_bank',
                'maxlength' => '32',
            ],

            'transfer_in_branch'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入分行',
                'name'=>'transfer_in_branch',
                'maxlength' => '16',
            ],

            'transfer_in_bank_code'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入銀行代碼',
                'name'=>'transfer_in_bank_code',
                'maxlength' => '16',
            ],

            'transfer_in_account'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入帳號',
                'name'=>'transfer_in_account',
                'maxlength' => '20',
            ],

            'transfer_in_account_name'=>[
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入戶名',
                'name'=>'transfer_in_account_name',
                'maxlength' => '20',
            ],

            'transfer_out_bank'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出銀行',
                'name'=>'transfer_out_bank',
                'maxlength' => '32',
            ],

            'transfer_out_branch'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出分行',
                'name'=>'transfer_out_branch',
                'maxlength' => '16',
            ],

            'transfer_out_bank_code'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出銀行代碼',
                'name'=>'transfer_out_bank_code',
                'maxlength' => '16',
            ],

            'transfer_out_account'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出帳號',
                'name'=>'transfer_out_account',
                'maxlength' => '20',
            ],

            'transfer_out_account_name'=>[
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出戶名',
                'name'=>'transfer_out_account_name',
                'maxlength' => '20',
            ],

            'data_mail'=>[
                'form_col' => 12,
                'label_col'=> 3,
                'col' => 9,
                'rows' =>'10',
                'cols'=>'5',
                'type' =>'textarea',
                'label'=>'資料郵寄',
                'name'=>'data_mail',
                'maxlength' => '256',
            ],

            'contract_date'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'期約',
                'id'=>'contract_date',
                'name'=>'contract_date',
            ],


        ];


        parent::__construct();
    }

    public function processAdd(){
        $insert_key = "";
        $insert_val = "";

        //這塊是專門insert 因此我就不給hidden了
        foreach($this->tpl_data_c_i_s as $key => $val){
            if($val['index']==true) {//跳過因為標頭index會自動產生

            }else if($val['type']=='hidden'){
                $insert_key .= '`'.$key.'`,';
                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
            }else if($val['type']=='text' || $val['type']=='textarea' || $val['type']=='select' ||  $val['type']=='date'){
                $insert_key .= '`'.$key.'`,';
                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
            }else if($val['type']=='number'){
                $insert_key .= '`'.$key.'`,';
                $insert_val .= ''.GetSQL($_POST[$key],"int").',';
            }else if($val['type']=='checkbox'){//由於目前這tpl的checkbox 是單選因此不用額外判斷
                $insert_key .= '`'.$key.'`,';
                $insert_val .= ''.GetSQL($_POST[$key],"text").',';
            }else if($val['type']=='tpl'){
                $tpl =["type","detail","price","date"];
                foreach($tpl as $tp_k => $tp_v){
                    $temp = "";
                    $con_var = "{$key}_{$tp_v}";
                    foreach($_POST[$con_var] as $k_k => $k_v){
                        $temp .= '"'.$k_v.'",';
                    }
                    $temp = substr($temp,0,-1);//去尾
                    $insert_key .= '`'.$con_var.'`,';
                    $insert_val .="'[{$temp}]',";
                }
            }
        }
        $insert_key .='`id_admin`';
        $insert_val .= GetSQL($_SESSION['id_admin'],"int");
//        $insert_key = substr($insert_key,0,-1);//去尾
//        $insert_val = substr($insert_val,0,-1);//去尾
        $sql = "INSERT INTO `contract_income_statement` ({$insert_key})VALUES({$insert_val})";
        Db::rowSQL($sql);
        $sql = "SELECT max(id_contract_income_statement) max_id FROM contract_income_statement";//取得剛剛建立的
        $max_row = Db::rowSQL($sql,true);

        if (Tools::isSubmit('submitAdd' . $this->table)) {
            $this->back_url = self::$currentIndex . '&conf=1';
        }
        if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
            if (in_array('edit', $this->post_processing)) {
                $this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=1&' . $this->index . '=' . $max_row["max_id"];
            } else {
                $this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=1&' . $this->index . '=' . $max_row["max_id"];
            }
        }
        if (Tools::isSubmit('submitAdd' . $this->table . 'AndContinue')) {
            $this->back_url = self::$currentIndex . '&add' . $this->table . '&conf=1';
        }
        Tools::redirectLink($this->back_url);

    }

    public function processEdit(){
            $update_data = "";
            $update_where="";
            //這塊是專門insert 因此我就不給hidden了
            foreach($this->tpl_data_c_i_s as $key => $val){
                if($val['index']==true){//跳過因為標頭index會自動產生
                    $update_where="{$key} =".GetSQL($_POST[$key],"int");
                }else if($val['type']=='hidden'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
                }else if($val['type']=='text' || $val['type']=='textarea' || $val['type']=='select' ||  $val['type']=='date'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
                }else if($val['type']=='number'){
                    $update_data .= $key.'='.GetSQL($_POST[$key],"int").',';
                }else if($val['type']=='checkbox'){//由於目前這tpl的checkbox 是單選因此不用額外判斷
                    $update_data .= $key.'='.GetSQL($_POST[$key],"text").',';
                }else if($val['type']=='tpl'){
                    $tpl =["type","detail","price","date"];

                    foreach($tpl as $tp_k => $tp_v){
                        $temp = "";
                        $con_var = "{$key}_{$tp_v}";
                        foreach($_POST[$con_var] as $k_k => $k_v){
                            $temp .= '"'.$k_v.'",';
                        }
                        $temp = substr($temp,0,-1);//去尾
                        $update_data .="`{$con_var}`='[{$temp}]',";
                    }
                }
            }
            $update_data = substr($update_data,0,-1);//去尾
            $sql = "UPDATE {$this->table} SET {$update_data} WHERE {$update_where}";
            Db::rowSQL($sql);


        if (Tools::isSubmit('submitEdit' . $this->table)) {
            $this->back_url = self::$currentIndex . '&conf=2';
        }
        if (Tools::isSubmit('submitEdit' . $this->table . 'AndStay')) {
            if (in_array('edit', $this->post_processing)) {
                $this->back_url = self::$currentIndex . '&view' . $this->table . '&conf=2&' . $this->index . '=' . Tools::getValue($this->fields['index']);
            } else {
                $this->back_url = self::$currentIndex . '&edit' . $this->table . '&conf=2&' . $this->index . '=' . Tools::getValue($this->fields['index']);
            }
        }
        Tools::redirectLink($this->back_url);
    }



    public function initProcess()
    {
        $sql = "SELECT * FROM contract_income_statement WHERE id_contract_income_statement=".GetSQL(Tools::getValue("id_contract_income_statement"),"int");
        $row_c_i_s = Db::rowSQL($sql,true);
        $row_c_i_s = Db::all_antonym_array($row_c_i_s,true);//做處理

        $this->fields['form']['contract_income_statement'] = [
            'tab' => 'contract_income_statement',
            'legend' => [
                'title' => $this->l('合約收支表'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],
            'tpl'=>'themes/LifeHouse/contract_income_statement.tpl',
//             'html'=>'<div> TEST 1234567</div>',

            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],

            'reset' => [
                'title' => $this->l('重置'),
            ],

        ];

        $this->context->smarty->assign([
            'main_index' =>$this->fields['index'],
            'tpl_data_c_i_s' => $this->tpl_data_c_i_s,
            'row_c_i_s'=> $row_c_i_s,
        ]);

        parent::initProcess();
    }

        public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('../media/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.css');
        $this->addJS('../media/bootstrap-datepicker-master/js/bootstrap-datepicker.js');
        $this->addJS('../media/bootstrap-datepicker-master/js/locales/bootstrap-datepicker.zh-TW.js');
        $this->addJS('/themes/LifeHouse/js/contract_income_statement.js');
        $this->addJS('/themes/LifeHouse/js/home_appliances.js');
    }
}





