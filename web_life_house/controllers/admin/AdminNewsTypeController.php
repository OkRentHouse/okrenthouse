<?php

class AdminNewsTypeController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminNewsTypeController';
		$this->table           = 'news_type';
		$this->fields['index'] = 'id_news_type';
		$this->fields['title'] = '公告類別';
		$this->fields['order'] = ' ORDER BY `news_type` ASC';

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				if ($_SESSION['id_web']) {
					$where                 = ' AND `id_web` = ' . $_SESSION['id_web'];
					$this->fields['where'] = ' AND `id_web` = ' . $_SESSION['id_web'];
				}
			}
		}

		$this->fields['list'] = [
			'id_news_type' => [
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'id_web'       => [
				'title'  => $this->l('加盟店名稱'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table' => 'web',
					'key'   => 'id_web',
					'val'   => 'web',
					'where' => $where,
					'order' => '`web` ASC',
				],
				'class'  => 'text-center',
			],
			'news_type'    => [
				'title'  => $this->l('公告類別'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'class'        => [
				'title'  => $this->l('顏色'),
				'order'  => true,
				'filter' => true,
				'values' => [
					[
						'class' => 'p_red',
						'value' => 'p_red',
						'title' => $this->l('紅'),
					],
					[
						'class' => 'p_yellow',
						'value' => 'p_yellow',
						'title' => $this->l('黃'),
					],
					[
						'class' => 'p_blue',
						'value' => 'p_blue',
						'title' => $this->l('藍'),
					],
					[
						'class' => 'p_green',
						'value' => 'p_green',
						'title' => $this->l('綠'),
					],
					[
						'class' => 'p_gray',
						'value' => 'p_gray',
						'title' => $this->l('灰'),
					],
				],
				'class'  => 'text-center',
			],
		];

		$this->fields['form'] = [
			'news_type' => [
				'legend' => [
					'title' => $this->l('公告類別'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_news_type' => [
						'name'     => 'id_news_type',
						'type'     => 'hidden',
						'index'    => true,
						'label'    => $this->l('類別'),
						'required' => true,
					],
					'id_web'       => [
						'name'    => 'id_web',
						'type'    => 'select',
						'label'   => $this->l('加盟店名稱'),
						'options' => [
							'default' => [
								'text' => '所有加盟店共用',
								'val'  => '',
							],
							'table'   => 'web',
							'text'    => 'web',
							'value'   => 'id_web',
							'where'   => $where,
							'order'   => '`web` ASC',
						],
					],
					'news_type'    => [
						'name'      => 'news_type',
						'type'      => 'text',
						'label'     => $this->l('公告類別'),
						'maxlength' => '20',
						'required'  => true,
					],
					'class'        => [
						'name'      => 'class',
						'type'      => 'switch',
						'label'     => $this->l('顏色'),
						'values'    => [
							[
								'id'    => 'p_red',
								'value' => 'p_red',
								'label' => $this->l('紅'),
							],
							[
								'id'    => 'p_yellow',
								'value' => 'p_yellow',
								'label' => $this->l('黃'),
							],
							[
								'id'    => 'p_blue',
								'value' => 'p_blue',
								'label' => $this->l('藍'),
							],
							[
								'id'    => 'p_green',
								'value' => 'p_green',
								'label' => $this->l('綠'),
							],
							[
								'id'    => 'p_gray',
								'value' => 'p_gray',
								'label' => $this->l('灰'),
							],
						],
						'maxlength' => '20',
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				$this->fields['list']['id_web']['hidden']                     = true;
				$this->fields['form']['news_type']['input']['id_web']['type'] = 'hidden';
				unset($this->fields['form']['news_type']['input']['id_web']['options']['default']);
			}
		}
		parent::__construct();
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addCSS('/' . ADMIN_URL . '/themes/' . $this->admin_theme . '/css/news.css');
	}
}