<?php

class AdminNewsController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminNewsController';
		$this->table           = 'news';
		$this->fields['index'] = 'id_news';
		$this->fields['title'] = '公告管理';
		$this->fields['order'] = ' ORDER BY `top` DESC, `date_time` DESC, `add_time` DESC';

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				if ($_SESSION['id_web']) {
					$where                 = ' AND (`id_web` = ' . $_SESSION['id_web'] . ' OR `id_web` = 0)';
					$this->fields['where'] = ' AND (`id_web` = ' . $_SESSION['id_web'] . ' OR `id_web` = 0)';
				}
			}
		}

		$this->fields['list_num'] = 50;

		$this->fields['list'] = [
			'id_news'      => [
				'index'  => true,
				'title'  => $this->l('ID'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'id_web'       => [
				'title'  => $this->l('加盟店名稱'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table' => 'web',
					'key'   => 'id_web',
					'val'   => 'web',
					'where' => $where,
					'order' => '`web` ASC',
				],
				'class'  => 'text-center',
			],
			'id_news_type' => [
				'title'  => $this->l('類別'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table' => 'news_type',
					'key'   => 'id_news_type',
					'class' => 'class',
					'val'   => 'news_type',
					'where' => $where,
					'order' => '`news_type` ASC',
				],
				'class'  => 'text-center',
			],
			'top'          => [
				'title'  => $this->l('置頂'),
				'order'  => true,
				'filter' => true,
				'values' => [
					[
						'class' => 'top',
						'value' => 1,
						'title' => $this->l('置頂'),
					],
				],
				'class'  => 'text-center',
			],
			'title'        => [
				'title'  => $this->l('標題'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'date_time'         => [
				'title'  => $this->l('發佈日期'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'content'      => [
				'title'  => $this->l('內容'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'show'   => false,
			],
			'add_time'  => [
				'title'  => $this->l('建立時間'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'id_add'    => [
				'title'  => $this->l('建立人員'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'key'    => [
					'table' => 'admin',
					'key'   => 'id_admin',
					'val'   => 'first_name',
				],
			],
			'edit_time' => [
				'title'  => $this->l('修改時間'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'id_edit'   => [
				'title'  => $this->l('修改人員'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'key'    => [
					'table' => 'admin',
					'key'   => 'id_admin',
					'val'   => 'first_name',
				],
			],
			'active'       => [
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['form'] = [
			'news' => [
				'tab'    => 'letter',
				'legend' => [
					'title' => $this->l('公告管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_news'      => [
						'name'     => 'id_news',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'id_web'       => [
						'name'    => 'id_web',
						'type'    => 'select',
						'label'   => $this->l('加盟店名稱'),
						'label_col'     => 2,
						'val'     => $_SESSION['id_web'],
						'options' => [
							'default' => [
								'text' => '發佈所有加盟店',
								'val'  => '',
							],
							'table'   => 'web',
							'text'    => 'web',
							'value'   => 'id_web',
							'where'   => $where,
							'order'   => '`web` ASC',
						],
					],
					'id_news_type'       => [
						'name'    => 'id_news_type',
						'type'    => 'select',
						'label'   => $this->l('類別'),
						'label_col'     => 2,
						'options' => [
							'table'   => 'news_type',
							'text'    => 'news_type',
							'value'   => 'id_news_type',
							'where'   => $where,
							'order'   => '`news_type` ASC',
						],
					],
					'top'          => [
						'name'   => 'top',
						'type'   => 'checkbox',
						'label'  => $this->l('置頂'),
						'label_col'     => 2,
						'values' => [
							[
								'id'    => 'top',
								'value' => 1,
								'label' => $this->l('置頂'),
							],
						],
					],
					'title'     => [
						'name'      => 'title',
						'type'      => 'text',
						'label'     => $this->l('標題'),
						'label_col'     => 2,
						'maxlength' => '20',
						'required'  => true,
					],
					'date_time' => [
						'name'     => 'date_time',
						'type'     => 'date',
						'label'    => $this->l('發佈日期'),
						'label_col'     => 2,
						'val'      => today(),
						'required' => true,
					],
					'content'   => [
						'name'     => 'content',
						'type'     => 'textarea',
						'label'    => $this->l('內容'),
						'label_col'     => 2,
						'class'    => 'tinymce',
						'required' => true,
						'col'      => 8,
						'rows'     => 20,
					],
					'add_time'  => [
						'name'          => 'add_time',
						'type'          => 'hidden',
						'auto_datetime' => 'add',
						'label'         => $this->l('建立時間'),
						'val'           => today(),
						'label_col'     => 4,
						'form_col'      => 6,
						'col'           => 6,
					],
					'id_add'    => [
						'name'     => 'id_add',
						'label'    => $this->l('建立人員'),
						'type'     => 'hidden',
						'val'      => $_SESSION['id_admin'],
						'label_col'     => 2,
						'form_col' => 6,
						'col'      => 6,
						'options'  => [
							'table' => 'admin',
							'text'  => 'first_name',
							'value' => 'id_admin',
						],
					],
					'edit_time' => [
						'name'          => 'edit_time',
						'type'          => 'hidden',
						'auto_datetime' => 'edit',
						'label_col'     => 4,
						'form_col'      => 6,
						'col'           => 6,
						'label'         => $this->l('修改時間'),
					],
					'id_edit'   => [
						'name'          => 'id_edit',
						'label'         => $this->l('修改人員'),
						'type'          => 'hidden',
						'val'           => $_SESSION['id_admin'],
						'label_col'     => 2,
						'form_col'      => 6,
						'col'           => 6,
						'auto_datetime' => 'edit',
						'auto_id'       => 'edit',
						'options'       => [
							'table' => 'admin',
							'text'  => 'first_name',
							'value' => 'id_admin',
						],
					],
					'active'    => [
						'type'   => 'switch',
						'label'  => $this->l('啟用狀態'),
						'name'   => 'active',
						'label_col'     => 2,
						'val'    => 1,
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				$this->fields['list']['id_web']['hidden']                = true;
				$this->fields['form']['news']['input']['id_web']['type'] = 'hidden';
				unset($this->fields['form']['news']['input']['id_web']['options']['default']);
			}
		}

		parent::__construct();
	}

	public function initToolbar()
	{        //初始化功能按鈕
		if ($this->display == 'add') {
			$this->toolbar_btn['save_and_stay'] = [
				'href' => '#',
				'desc' => $this->l('儲存並繼續編輯'),
			];
		}
		parent::initToolbar();
	}


	public function validateRules()
	{
		if ($this->action == 'Save' && $this->display = 'edit' && count($this->_errors) == 0) {
			$this->fields['form']['news']['input']['add_time']['type']  = 'view';
			$this->fields['form']['news']['input']['id_add']['type']    = 'view';
			$this->fields['form']['news']['input']['edit_time']['type'] = 'hidden';
			$this->fields['form']['news']['input']['id_edit']['type']   = 'hidden';
			$_POST['edit_time']                                         = now();
			$_POST['id_edit']                                           = $_SESSION['id_admin'];
		}
		if ($this->action == 'Save' && $this->display = 'add' && count($this->_errors) == 0) {
			$this->fields['form']['news']['input']['add_time']['type'] = 'hidden';
			$this->fields['form']['news']['input']['id_add']['type']   = 'hidden';
			$_POST['add_time']                                         = now();
			$_POST['id_add']                                           = $_SESSION['id_admin'];
			unset($_POST['edit_time']);
			unset($_POST['id_edit']);
		}
		parent::validateRules();
	}

	public function initProcess()
	{
		parent::initProcess();

		if ($this->display == 'add') {
			$this->fields['form']['news']['input']['add_time']['type'] = 'hidden';
			$this->fields['form']['news']['input']['id_add']['type']   = 'hidden';
		}
		if ($this->display == 'edit') {
			$this->fields['form']['news']['input']['add_time']['type']  = 'view';
			$this->fields['form']['news']['input']['id_add']['type']    = 'view';
			$this->fields['form']['news']['input']['edit_time']['type'] = 'view';
			$this->fields['form']['news']['input']['id_edit']['type']   = 'view';
		}
	}
}