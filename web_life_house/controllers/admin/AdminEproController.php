<?php

class AdminEproController extends AdminController{
//    public function __construct()
//    {
//        $this->className = 'AdminEproController';
//        $this->table = 'epro';
//        $this->fields['index'] = 'id_epro';
//        $this->_as = 'e';
////        $this->_join = ' LEFT JOIN `epro_file` AS e_f ON e_f.`id_epro` = e.`id_epro` ';
//        $this->fields['title'] = '裝修達人會員';
//        $this->fields['list_num'] = 50;
//
//
//        //基本圖片 證照
//        $this->arr_file_type = [
//            '0' => 'img',
//            '1' => 'license_img',
//        ];
//
//        $this->fields['list']  = [
//            'id_epro'         => [
//                'index'  => true,
//                'hidden' => true,
//                'type'   => 'checkbox',
//                'label_col' => '2',
//                'col' => '3',
//                'class'  => 'text-center',
//                'filter_key' => 'e!id_epro',
//            ],
//            'name'          => [
//                'title'  => $this->l('姓名'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!name',
//            ],
//
//            'nickname'          => [
//                'title'  => $this->l('暱稱'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!nickname',
//            ],
//
//            'fb_share'          => [
//                'title'  => $this->l('fb分享'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!nickname',
//            ],
//
//            'line_share'          => [
//                'title'  => $this->l('line分享'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!line_share',
//            ],
//
//            'honorarium'          => [
//                'title'  => $this->l('酬金(車馬費)'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!honorarium',
//            ],
//
//            'phone_1'          => [
//                'title'  => $this->l('手機1'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!phone_1',
//            ],
//
//            'phone_2'          => [
//                'title'  => $this->l('手機2'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!phone_2',
//            ],
//
//            'active'         => [
//                'title'  => $this->l('啟用'),
//                'order'  => true,
//                'filter' => true,
//                'class'  => 'text-center',
//                'filter_key' => 'e!active',
//            ],
//        ];
//
//        $id_epro_windows =[];
//        $sql = 'SELECT * FROM ilifehou_epro.`epro_windows` WHERE `active`=1 ORDER BY position ASC';
//        $id_epro_windows_arr = Db::rowSQL($sql);
//        foreach($id_epro_windows_arr as $value){
//            $id_epro_windows[] = [
//                'id' => 'ilifehou_epro_'.$value['id_epro_windows'],
//                'value' => $value['id_epro_windows'],
//                'label' => $this->l($value['title'])
//            ];
//        }
//
//        $this->fields['form'] = [
//            [
//                'legend' => [
//                    'title' => $this->l('裝修達人會員'),
//                    'icon'  => 'icon-cogs',
//                    'image' => '',
//                ],
//                'input'  => [
//                    'id_epro' => [
//                        'name'     => 'id_epro',
//                        'type'     => 'hidden',
//                        'label_col' => '3',
//                        'col' => '3',
//                        'index'    => true,
//                        'required' => true,
//                    ],
//                    'name'          => [
//                        'name'      => 'name',
//                        'type'      => 'text',
//                        'form_col'  => 6,
//                        'label_col' => 6,
//                        'col' => 6,
//                        'maxlength' => 20,
//                        'label'     => $this->l('name'),
//                        'required' => true,
//                    ],
//                    'nickname'          => [
//                        'name'      => 'nickname',
//                        'type'      => 'text',
//                        'form_col'  => 6,
//                        'label_col' => 3,
//                        'col' => 6,
//                        'maxlength' => 200,
//                        'label'     => $this->l('暱稱'),
//                        'required' => true,
//                    ],
//                    'fb_share'          => [
//                        'name'      => 'fb_share',
//                        'type'      => 'text',
//                        'form_col'  => 12,
//                        'label_col' => 3,
//                        'col' => 9,
//                        'maxlength' => 128,
//                        'label'     => $this->l('fb分享'),
//                    ],
//                    'line_share'          => [
//                        'name'      => 'line_share',
//                        'type'      => 'text',
//                        'form_col'  => 12,
//                        'label_col' => 3,
//                        'col' => 9,
//                        'maxlength' => 128,
//                        'label'     => $this->l('line分享'),
//                    ],
//                    'id_epro_windows' => [
//                        'type' => 'checkbox',
//                        'label' => $this->l('服務內容(複選)'),
//                        'name' => 'id_epro_windows',
//                        'in_table' => true,
//                        'multiple' => true,
//                        'values' => $id_epro_windows,
//                        'form_col' => 12,
//                        'label_col' => 3,
//                        'col' => 9,
//                    ],
//
//                    'honorarium'          => [
//                        'name'      => 'honorarium',
//                        'type'      => 'number',
//                        'form_col'  => 12,
//                        'label_col' => 3,
//                        'col' => 3,
//                        'label'     => $this->l('車馬費'),
//                    ],
//
//                    'server_cycle' => [
//                        'name'      => 'server_cycle',
//                        'type'      => 'select',
//                        'form_col'  => 6,
//                        'label_col' => 6,
//                        'col' => 6,
//                        'label'     => $this->l('服務周期'),
//                        'options'   => [
//                            'default'     => [
//                                'text' => '選擇服務周期',
//                                'val'  => '',
//                            ],
//                            //自訂值
//                            'val'         => [
//                                [
//                                    'text' => '每月',
//                                    'val'  => '0',
//                                ],
//                                [
//                                    'text' => '每周',
//                                    'val'  => '1',
//                                ],
//                                [
//                                    'text' => '每日',
//                                    'val'  => '2',
//                                ],
//                                [
//                                    'text' => '假日',
//                                    'val'  => '3',
//                                ],
//                            ],
//                        ],
//                    ],
//
//
//                    'active'         => [
//                        'type'     => 'switch',
//                        'label_col' => '3',
//                        'col' => '3',
//                        'label'    => $this->l('啟用狀態'),
//                        'name'     => 'active',
//                        'required' => true,
//                        'val'      => 1,
//                        'values'   => [
//                            [
//                                'id'    => 'active_on',
//                                'value' => 1,
//                                'label' => $this->l('啟用'),
//                            ],
//                            [
//                                'id'    => 'active_off',
//                                'value' => 0,
//                                'label' => $this->l('關閉'),
//                            ],
//                        ],
//                    ],
//                    'position'           => [
//                        'label_col' => '3',
//                        'col' => '3',
//                        'name'  => 'position',
//                        'type'  => 'number',
//                        'label' => $this->l('順序'),
//                    ],
//                ],
//                'submit' => [
//                    [
//                        'title' => $this->l('儲存'),
//                    ],
//                    [
//                        'title' => $this->l('儲存並繼續編輯'),
//                        'stay'  => true,
//                    ],
//                ],
//                'cancel' => [
//                    'title' => $this->l('取消'),
//                ],
//                'reset'  => [
//                    'title' => $this->l('重置'),
//                ],
//            ],
//
//            'img' => [
//                'tab' => 'file',
//                'legend' => [
//                    'title' => $this->l('縮圖'),
//                    'icon' => 'icon-cogs',
//                    'image' => '',
//                ],
//
//                'input' => [
//                    'img' => [
//                        'name' => 'img',
//                        'label_class' => 'text-left',
//                        'label' => $this->l('小圖示'),
//                        'type' => 'file',
//                        'language' => 'zh-TW',
//                        'file' => [
//                            'icon' => false,
//                            'auto_upload' => true,
//                            'max' => 1,
//                            'allowed' => ['jpg', 'png', 'gif', 'jpeg','svg'],
//                            'resize' => true,
//                            'resizePreference' => 'height',
//                        ],
//                        'multiple' => true,
//                        'col' => 12,
//                        'no_action' => true,
//                        'p' => $this->l('圖片只可上傳一張'),
//                    ],
//                ],
//            ],
//        ];
//
//
//
//        parent::__construct();
//    }
}