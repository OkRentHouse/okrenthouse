<?php
class AdminEproWindowsItemDetailsController extends AdminController
{
    public $tpl_folder;    //樣版資料夾
    public $ed;

    public function __construct()
    {
        $this->conn = 'ilifehou_epro';
        $this->className       = 'AdminEproWindowsItemDetailsController';
        $this->fields['title'] = '裝修達人百葉窗項目細項';
        $this->table           = 'epro_windows_item_details';
        $this->fields['index'] = 'id_epro_windows_item_details';
        $this->fields['order'] = ' ORDER BY e_w_i_d.`id_epro_windows_item` ASC,e_w_i_d.`position` ASC';
        $this->_as = 'e_w_i_d';
             $this->_join = ' LEFT JOIN epro_windows_item AS e_w_i ON e_w_i_d.`id_epro_windows_item` = e_w_i.`id_epro_windows_item`
                         LEFT JOIN epro_windows AS e_w ON e_w.`id_epro_windows` = e_w_i.`id_epro_windows`';


        $this->fields['list']  = [
            'id_epro_windows_item_details'         => [
                'index'  => true,
                'hidden' => true,
                'type'   => 'checkbox',
                'label_col' => '2',
                'col' => '3',
                'class'  => 'text-center',
                'filter_key' => 'e_w_i_d!id_epro_windows_item_details',
            ],

            'id_epro_windows_title' => [
                'filter_key' => 'e_w!id_epro_windows',
                'title' => $this->l('裝修達人百葉窗'),
                'order' => true,
                'filter' => true,
                'multiple' => true,
                'key' => [
                    'table' => 'epro_windows',
                    'key' => 'id_epro_windows',
                    'val' => 'title',
                    'order' => '`position` ASC',
                ],
                'class' => 'text-center',
            ],
            'id_epro_windows_item_title' => [
                'filter_key' => 'e_w_i!id_epro_windows_item',
                'title' => $this->l('裝修達人百葉窗項目'),
                'order' => true,
                'filter' => true,
                'multiple' => true,
                'key' => [
                    'table' => 'epro_windows_item',
                    'key' => 'id_epro_windows_item',
                    'val' => 'title',
                    'order' => '`position` ASC',
                ],
                'class' => 'text-center',
            ],
            'title'          => [
                'title'  => $this->l('裝修達人百葉窗項目細項'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w_i_d!title',
            ],

            'position' => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w_i_d!position',
                'filter' => true,
            ],
            'active'         => [
                'title'  => $this->l('啟用'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
                'filter_key' => 'e_w_i_d!active',
            ],
        ];

        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('裝修達人百葉窗項目細項'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'epro_windows_item_details' => [
                        'name'     => 'epro_windows_item_details',
                        'type'     => 'hidden',
                        'label_col' => '3',
                        'col' => '3',
                        'index'    => true,
                        'required' => true,
                    ],

                    'title'          => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label_col' => '3',
                        'col' => '3',
                        'maxlength' => 32,
                        'label'     => $this->l('title'),
                        'required' => true,
                    ],

                    'id_epro_windows_item'          => [
                        'name'      => 'id_epro_windows_item',
                        'type'      => 'select',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'label'     => $this->l('裝修達人百葉窗項目'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇類型',
                                'val'  => '',
                            ],
                            'table'   => 'epro_windows_item',
                            'text'    => 'title',
                            'value'   => 'id_epro_windows_item',
                            'order'   => ' `position` ASC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'active'         => [
                        'type'     => 'switch',
                        'label_col' => '3',
                        'col' => '3',
                        'label'    => $this->l('啟用狀態'),
                        'name'     => 'active',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    'position'           => [
                        'label_col' => '3',
                        'col' => '3',
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay'  => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('重置'),
                ],
            ],

        ];
        parent::__construct();
    }



    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
    }

}