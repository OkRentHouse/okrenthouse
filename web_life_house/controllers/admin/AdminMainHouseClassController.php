<?php

class AdminMainHouseClassController extends AdminController
{
    public function __construct()
    {
        $this->className         = 'AdminMainHouseClassController';
        $this->table             = 'main_house_class';
        $this->fields['index']   = 'id_main_house_class';
        $this->fields['title']   = '主房屋類別';
        $this->fields['order']   = ' ORDER BY m_h_c.`id_main_house_class` ASC';
        $this->_as = 'm_h_c';
//        $this->_join = ' LEFT JOIN `rent_house_type_file` AS r_h_t_f ON r_h_t_f.`id_rent_house_type` = r_h_t.`id_rent_house_type` ';
        $this->fields['list']    = [
            'id_rent_house_type' => [
                'filter_key' => 'm_h_c!id_main_house_class',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'title'              => [
                'filter_key' => 'm_h_c!title',
                'title'  => $this->l('類別名稱'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'remark'             => [
                'filter_key' => 'm_h_c!remark',
                'title'  => $this->l('備註'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];


        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('物件類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_main_house_class' => [
                        'name'     => 'id_main_house_class',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('屋類別'),
                        'required' => true,
                    ],
                    'title'    => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('主房屋類別名稱'),
                        'maxlength' => '32',
                        'required'  => true,
                    ],
                    'remark'             => [
                        'name'      => 'remark',
                        'type'      => 'text',
                        'label'     => $this->l('備註'),
                        'maxlength' => '64',
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                    ],
            ],
        ];

        parent::__construct();
    }
}