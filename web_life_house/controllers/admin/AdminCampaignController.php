<?php

class AdminCampaignController extends AdminController
{
    public $no_link = false;

    public function __construct()
    {
        $this->conn = 'ilifehou_life_go';
        $this->className            = 'AdminCampaignController';
        $this->table                = 'campaign';
        $this->fields['index']      = 'id_campaign';
        $this->fields['title']      = '活動';
        $this->_as = 'c';


        $this->fields['order'] = ' ORDER BY c.`id_campaign` DESC';


//        $this->actions[]            = 'list';
        $this->fields['list']       = [
            'id_product_class_item'          => [
                'index'  => true,
                'filter_key' => 'c!id_campaign',
                'title'  => $this->l('活動ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],

            'title'    => [
                'filter_key' => 'c!title',
                'title'  => $this->l('參加商品的標題'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],

            'active' => [
                'filter_key' => 'c!active',
                'title' => $this->l('啟用狀態'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
        ];



        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('活動'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    [
                        'name'     => 'id_campaign',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],

                    [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('活動名稱'),
                        'maxlength' => '32',
                        'required'  => true,
                    ],

                    'product_number' => [
                        'name' => 'product_number',
                        'type' => 'text',
                        'form_col'=>12,
                        'label_col'=>3,
                        'col'  => 5,
                        'maxlength' => '32',
                        'label' => $this->l('觸發條件'),
                        'prefix' => $this->l('商品編號'),
                        'is_prefix' => true,
                        'required'  => true,
                    ],

                    'num' => [
                        'name' => 'num',
                        'type' => 'number',
                        'prefix' => $this->l('商品數量'),
                        'is_suffix' => true,
                        'required'  => true,
                    ],

                    'get_product_number' => [
                        'name' => 'get_product_number',
                        'type' => 'text',
                        'form_col'=>12,
                        'label_col'=>3,
                        'col'  => 5,
                        'maxlength' => '32',
                        'label' => $this->l('贈送商品'),
                        'prefix' => $this->l('商品編號'),
                        'is_prefix' => true,
                    ],

                    'get_product_num' => [
                        'name' => 'get_product_num',
                        'type' => 'number',
                        'prefix' => $this->l('數量'),
                        'is_suffix' => true,
                    ],

                    'discount_price' => [
                        'name' => 'discount_price',
                        'type' => 'number',
                        'form_col'=>12,
                        'label_col'=>3,
                        'col'  => 2,
                        'label' => $this->l('打折金額'),
                    ],

                    'discount_percent' => [
                        'name' => 'discount_percent',
                        'type' => 'text',
                        'form_col'=>12,
                        'label_col'=>3,
                        'col'  => 2,
                        'label' => $this->l('打折比率'),
                        'suffix' => '%',
                    ],

                    'point' => [
                        'name' => 'point',
                        'type' => 'text',
                        'form_col'=>12,
                        'label_col'=>3,
                        'col'  => 2,
                        'label' => $this->l('贈送點數'),
                    ],

                    'date_s' => [
                        'name' => 'date_s',
                        'type' => 'date',
                        'form_col'=>12,
                        'label_col' => 3,
                        'col'   =>6,
                        'label' => $this->l('持續時間'),
                        'is_prefix' => true,
                    ],

                    'date_e' => [
                        'name' => 'date_e',
                        'type' => 'date',
                        'prefix' => $this->l('~'),
                        'is_suffix' => true,
                    ],


                    [
                        'type' => 'switch',
                        'label_col' => 3,
                        'label' => $this->l('啟用狀態'),
                        'name' => 'active',
                        'val' => 1,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],

                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],

                        ],
                    ],

                    'create_time' => [
                        'name' => 'create_time',
                        'type' => 'view',
                        'auto_datetime' => 'add',
                        'label' => $this->l('建立時間'),
                    ],

                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];
        parent::__construct();
    }



    /*
    public function initToolbar(){		//初始化功能按鈕
        if ($this->display == 'add' || $this->display == 'edit') {
            $this->toolbar_btn['save_and_stay'] = array(
                'href' => '#',
                'desc' => $this->l('儲存並繼續編輯')
            );
        }
        parent::initToolbar();
    }
    */
}