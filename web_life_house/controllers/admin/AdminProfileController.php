<?php

class AdminProfileController extends AdminController{

  public function __construct(){
    $this->className       = 'AdminProfileController';
    $this->table           = 'epro';
    $this->fields['index'] = 'id_epro';
    $this->fields['title'] = '裝修達人->達人資料維護';
    $this->_as             = 'e';
    $this->fields['list_num'] = 50;
    $this->fields['order'] = ' ORDER BY e.`id_epro` ASC';

    $this->fields['list'] = [
      'id_epro' => [
        'filter_key' => 'e!id_epro',
        'index'  => true,
        'title'  => $this->l('達人ID'),
        'type'   => 'checkbox',
        'hidden' => true,
        'class'  => 'text-center',
      ],
    'id_member' => [
        'filter_key' => 'e!id_member',
        'title' => $this->l('達人編號'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
    ],
    'fb_share' => [
        'filter_key' => 'e!fb_share',
        'title' => $this->l('FB'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'line_share' => [
        'filter_key' => 'e!line_share',
        'title' => $this->l('Line'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'honorarium' => [
        'filter_key' => 'e!honorarium',
        'title' => $this->l('車馬費'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'server_features' => [
        'filter_key' => 'e!server_features',
        'title' => $this->l('服務特色'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'server_cycle' => [
        'filter_key' => 'e!server_cycle',
        'title' => $this->l('服務時間'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'server_time_s' => [
        'filter_key' => 'e!server_time_s',
        'title' => $this->l('起始時間'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'server_time_e' => [
        'filter_key' => 'e!server_time_e',
        'title' => $this->l('結束時間'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'server_date_s' => [
        'filter_key' => 'e!server_date_s',
        'title' => $this->l('起始日期'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'server_date_e' => [
        'filter_key' => 'e!server_date_e',
        'title' => $this->l('結束日期'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'phone_1' => [
        'filter_key' => 'e!phone_1',
        'title' => $this->l('聯絡電話1'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'phone_2' => [
        'filter_key' => 'e!phone_2',
        'title' => $this->l('聯絡電話2'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'email' => [
        'filter_key' => 'e!email',
        'title' => $this->l('email'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'line_id' => [
        'filter_key' => 'e!line_id',
        'title' => $this->l('Line ID'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'local_calls' => [
        'filter_key' => 'e!local_calls',
        'title' => $this->l('室內電話'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'address' => [
        'filter_key' => 'e!address',
        'title' => $this->l('地址'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'license' => [
        'filter_key' => 'e!license',
        'title' => $this->l('證照'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'create_time' => [
        'filter_key' => 'e!create_time',
        'title' => $this->l('建立時間'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'update_time' => [
        'filter_key' => 'e!update_time',
        'title' => $this->l('更新時間'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],



    'active' => [
        'filter_key' => 'e!active',
        'title' => $this->l('啟用'),
        'order' => true,
        'filter' => true,
        'class' => 'text-center',
        'class'  => 'text-center',
        ],

    ];

    $_POST['update_user'] = str_replace("'","",GetSQL($_SESSION['name'],"text"));
    $sql = "SELECT `update_log` FROM `epro` WHERE id_epro = ".$_POST['id_epro'];;
    $update_log = Db::rowSQL($sql);
    $this->update_log_str = "";
    foreach ($update_log as $key => $value) { //管理員更新紀錄
      $this->update_log_str .= $value['update_log'] . "<br>";
    }

    $this->fields['form'] = [
      'epro' => [
      'legend' => [
                'title' => $this->l('達人資料維護'),
                'icon'  => 'icon-cogs',
                'image' => '',
                ],

                'legend' => [
                'title' => $this->l('人事管理'),
                'icon'  => 'icon-cogs',
                'image' => '',
                ],

                'input' => [

                  'id_epro' => [
                      'name' => 'id_epro',
                      'label' => $this->l('達人編號'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'index' => true,
                      'required' => true,
                      ],


                  'fb_share' => [
                      'name' => 'fb_share',
                      'label' => $this->l('FB'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => 0,
                      ],


                  'line_share' => [
                      'name' => 'line_share',
                      'label' => $this->l('Line'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => 0,
                      ],


                  'honorarium' => [
                      'name' => 'honorarium',
                      'label' => $this->l('車馬費'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => 0,
                      ],


                  'server_features' => [
                      'name' => 'server_features',
                      'label' => $this->l('服務特色'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => true,
                      ],


                  'server_cycle' => [
                      'name' => 'server_cycle',
                      'label' => $this->l('服務時間'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => true,
                      ],

                  'server_date_s' => [
                      'name' => 'server_date_s',
                      'label' => $this->l('起始 ~ 結束'),
                      'type' => 'date',
                      'form_col' => 3,
                      'label_col' => 4,
                      'col' => 8,
                      'required' => true,
                      ],


                  'server_time_s' => [
                      'name' => 'server_time_s',
                      'type' => 'time',
                      'form_col' => 2,
                      'label_col' => 0,
                      'col' => 12,
                      'no_label' => 1,
                      'required' => true,
                      ],

                  'server_date_e' => [
                      'name' => 'server_date_e',
                      'label' => $this->l(' ~ '),
                      'type' => 'date',
                      'form_col' => 3,
                      'label_col' => 1,
                      'col' => 10,
                      ],

                  'server_time_e' => [
                      'name' => 'server_time_e',
                      'type' => 'time',
                      'form_col' => 2,
                      'label_col' => 0,
                      'col' => 12,
                      'no_label' => 1,
                      'required' => true,
                      ],


                  'phone_1' => [
                      'name' => 'phone_1',
                      'label' => $this->l('聯絡電話1'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => true,
                      ],


                  'phone_2' => [
                      'name' => 'phone_2',
                      'label' => $this->l('聯絡電話2'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => 0,
                      ],


                  'email' => [
                      'name' => 'email',
                      'label' => $this->l('email'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => 0,
                      ],


                  'line_id' => [
                      'name' => 'line_id',
                      'label' => $this->l('Line ID'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => 0,
                      ],


                  'local_calls' => [
                      'name' => 'local_calls',
                      'label' => $this->l('室內電話'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => 0,
                      ],


                  'address' => [
                      'name' => 'address',
                      'label' => $this->l('地址'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => true,
                      ],




                  'create_time' => [
                      'name' => 'create_time',
                      'label' => $this->l('建立時間'),
                      'type' => 'view',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'disabled' => 1,
                      'required' => true,
                      ],


                  'update_time' => [
                      'name' => 'update_time',
                      'label' => $this->l('使用者更新時間'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => true,
                      ],

                  'update_user' => [
                      'name' => 'update_user',
                      'label' => $this->l('更新者'),
                      'type' => 'text',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      'required' => true,
                      ],

                  'update_log' => [
                      'name' => 'update_log',
                      'label' => $this->l('更新紀錄'),
                      'type' => 'html',
                      'data' => $this->update_log_str,
                      'disabled' => 1,
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,
                      ],


                  'active' => [
                      'name' => 'active',
                      'label' => $this->l('啟用'),
                      'type' => 'checkbox',
                      'form_col' => 12,
                      'label_col' => 1,
                      'col' => 3,

                      'required' => true,
                      'values' => [
          							[
          								'id'    => 'active_on',
          								'value' => 1,
          								'label' => $this->l('開放'),
          							],
          							[
          								'id'    => 'active_off',
          								'value' => 0,
          								'label' => $this->l('關閉'),
          							],
          						],
                ],
            ],

            'submit' => [
               [
               'title' => $this->l('儲存'),
               ],
               [
               'title' => $this->l('儲存並繼續編輯'),
               'stay' => true,
               ],
            ],
            'cancel' => [
            'title' => $this->l('取消'),
            ],
            'reset'  => [
            'title' => $this->l('復原'),
            ],


        ],

        'license' => [

            'legend' => [
              'title' => $this->l('證照'),
              'icon' => 'icon-cogs',
              'image' => '',
          ],

          'input' => [

        'license' => [
            'name' => 'license',
            'label_class' => 'text-left',
            'label' => $this->l('證照'),
            'language' => 'zh-TW',
            'type' => 'file',
            'form_col' => 12,
            'label_col' => 1,
            'col' => 3,
            'required' => 0,
            'file' => [
                  'icon' => false,
                  'auto_upload' => true,
                  'max' => 1,
                  'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                  'max_width' => 800,
                  'min_width' => 600,
//                            'max_height' => 600,
                  'resize' => true,
                  'resizePreference' => 'height',
              ],
              'multiple' => true,
              'no_action' => true,
//                        'p' => $this->l('圖片建議大小 428*285px'),
              'p' => $this->l('圖片建議大小 800*600px 或寬度最少超過600px'),
            ],
            ],



            ],
    ];

    // id_member
    // fb_share
    // line_share
    // honorarium
    // server_features
    // server_cycle
    // server_time_s
    // server_time_e
    // server_date_s
    // server_date_e
    // phone_1
    // phone_2
    // email
    // line_id
    // local_calls
    // address
    // license
    // active
    // create_time
    // update_time


    $this->fields['form']['epro']['input']['update_time']['placeholder'] = now();
    // $_POST['update_time'] = now();


    parent::__construct();

  }

  public function initProcess(){
    switch ($this->display) {
			case 'edit':			break;
			case 'add':			break;
			case 'view':			break;
			case 'options':			break;
			case 'excel':			break;
			case 'excelimport':			break;
		}

    parent::initProcess();

    if($this->display == 'edit'){
      $this->fields['form']['epro']['input']['update_time']['type'] = 'text';
      $this->fields['form']['epro']['input']['update_time']['val'] = 'view';
    }
  }

  public function processEdit(){
    echo "processEdit";

    parent::processEdit();
  }

  public function processSave(){
    $sql = "UPDATE `epro` SET `update_log`= '".$this->update_log_str." by ".$_POST['update_user']." at ".now()."' WHERE `id_epro`= ".$_POST['id_epro'];
    Db::rowSQL($sql);//記錄誰哪時修改
  }

}
