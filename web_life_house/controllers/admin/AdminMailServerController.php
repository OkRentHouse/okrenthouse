<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/7/16
 * Time: 上午 10:54
 */

class AdminMailServerController extends AdminController
{
	public $page = 'mailserver';
	public function __construct(){
		$this->className = 'AdminMailServerController';
		$this->fields['title'] = 'Mail 設定';
		$this->fields['form'] = array(
			array(	'tab' => 'mailserver',
				'legend' => array(
					'title' => $this->l('Mail 設定'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'MAIL_DOMAIN_NAME',
						'type' => 'text',
						'label' => $this->l('Mail 網域名稱'),
						'hint' => $this->l('有效的域名 (如果您不知道,請設定空白)'),
						'configuration' => true
					),
					array(
						'name' => 'MAIL_SERVER',
						'type' => 'text',
						'label' => $this->l('SMTP 用戶名'),
						'hint' => $this->l('IP或網域名稱 (例如smtp.mydomain.com)'),
						'configuration' => true
					),
					array(
						'name' => 'SMTP_USER_NAME',
						'type' => 'text',
						'label' => $this->l('SMTP 用戶名'),
						'hint' => $this->l('如果不設定,請留白'),
						'configuration' => true
					),
					array(
						'name' => 'SMTP_PASSWORD',
						'type' => 'password',
						'label' => $this->l('SMTP 密碼'),
						'hint' => $this->l('如果不設定,請留白'),
						'configuration' => true
					),
					array(
						'name' => 'SMTP_ENCRYPTION',
						'label' => $this->l('SMTP 加密'),
						'hint' => $this->l('啟用加密協議'),
						'type' => 'select',
						'val' => 'off',
						'options' => array(
							'val' => array(
								array(
									'val' => 'off',
									'text' => $this->l('none')
								),
								array(
									'val' => 'tls',
									'text' => $this->l('局部')
								),
								array(
									'val' => 'SSL',
									'text' => $this->l('關閉')
								)
							)
						),
						'configuration' => true
					),
					array(
						'name' => 'MAIL_SMTP_PORT',
						'type' => 'text',
						'label' => $this->l('PORT'),
						'hint' => $this->l('使用端口'),
						'configuration' => true,
						'maxlength' => 5
					),
				
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					)
				),
				'reset' => array(
					'title' => $this->l('復原')
				)
			),
			array(	'tab' => 'mailserver',
				'legend' => array(
					'title' => $this->l('Mail 設定'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'TEST_MAIL',
						'type' => 'email',
						'label' => $this->l('測試Mail'),
						'validate' => 'isEmail',
						'configuration' => true,
					),
				
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					)
				),
				'reset' => array(
					'title' => $this->l('復原')
				)
			)
		);
		
		parent::__construct();
	}
	
	public function initContent(){
		$this->display = 'options';
		parent::initContent();
	}
}