<?php

//todo
class AdminSEOController extends AdminController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'seo';
	public $ed;
	public $id_web = null;

	public function __construct()
	{
		$this->className       = 'AdminSEOController';
		$this->fields['title'] = 'SEO';
		$this->table           = 'seo';
		$this->fields['index'] = 'id_seo';

//		$where = ' AND (`dir` = "web_app" OR `dir` = "web_life_house" OR `dir` = "web_rent")';
		if (!empty($id_web)) {
			$where = ' AND `id_web` = ' . $this->id_web;
		}
		$this->fields['list'] = [
			'id_seo'      => [
				'index'  => true,
				'hidden' => true,
				'type'   => 'checkbox',
				'class'  => 'text-center',
			],
			'id_web'      => [
				'title'  => $this->l('網站'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'key'    => [
					'table' => 'web2',
					'key'   => 'id_web',
					'val'   => 'web',
					'where' => $where,
					'order' => '`web` ASC',
				],
			],
			'page'        => [
				'title'  => $this->l('網頁'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'url'         => [
				'title'  => $this->l('URL'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'title'       => [
				'title'  => $this->l('Title'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'description' => [
				'title'  => $this->l('Description'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'keywords'    => [
				'title'  => $this->l('keywords'),
				'order'  => true,
				'filter' => true,
				'show'   => false,
				'class'  => 'text-center',
			],
			'index_ation' => [
				'title'  => $this->l('開啟索引'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'index_a_1',
						'value' => '1',
						'title' => $this->l('開啟'),
					],
					[
						'class' => 'index_a_0',
						'value' => '0',
						'title' => $this->l('關閉'),
					],
				],
			],
		];

		$page_select = [];
		$arr_page    = $this->getFrontPage();
		foreach ($arr_page as $item => $page) {
			$page_select[] = [
				'val'         => $page['val'],
				'text'        => $page['text'],
				'parent_name' => 'id_web',
				'parent'      => $page['parent'],
			];
		}

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('SEO'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_seo'      => [
						'name'     => 'id_seo',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'id_web'      => [
						'name'     => 'id_web',
						'type'     => 'select',
						'label'    => $this->l('網站'),
						'val'      => $this->id_web,
						'options'  => [
							'default' => [
								'text' => '請選擇網站',
								'val'  => 'web',
							],
							'table'   => 'web2',
							'where'   => $where,
							'text'    => 'web',
							'value'   => 'id_web',
							'order'   => '`position` ASC',
						],
						'required' => true,
					],
					'page'        => [
						'name'      => 'page',
						'type'      => 'select',
						'options'   => [
							'val' => $page_select,
						],
						//						'unique'    => true,
//						'required'  => true,
						'maxlength' => 100,
						'label'     => $this->l('Page'),
					],
					'url'         => [
						'name'      => 'url',
						'type'      => 'text',
						'required'  => true,
						'maxlength' => 200,
						'label'     => $this->l('URL'),
					],
					'title'       => [
						'name'      => 'title',
						'type'      => 'text',
						'maxlength' => 100,
						'label'     => $this->l('Title'),
					],
					'description' => [
						'name'      => 'description',
						'type'      => 'text',
						'maxlength' => 300,
						'label'     => $this->l('Description'),
					],
					'keywords'    => [
						'name'      => 'keywords',
						'type'      => 'text',
						'maxlength' => 300,
						'data'      => ['role' => 'tagsinput'],
						'label'     => $this->l('Keywords'),
					],
					'switch'      => [
						'type'     => 'switch',
						'label'    => $this->l('開啟索引'),
						'name'     => 'index_ation',
						'required' => true,
						'val'      => 1,
						'values'   => [
							[
								'id'    => 'index_a_1',
								'value' => 1,
								'label' => $this->l('開啟'),
							],
							[
								'id'    => 'index_a_0',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
					[
						'title' => $this->l('儲存並繼續編輯'),
						'stay'  => true,
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
		];

		parent::__construct();
	}

	public function validateRules()
	{
		parent::validateRules();
		$url = Tools::getValue('url');
		if (!isTwUrl($url)) {
			$this->_errors[] = $this->l('網址符號只能用 _- 字元');
		}
	}



	public function getFrontPage()
	{
		$arr_page   = [];
		$arr_page[] = [
			'val'         => '',
			'text'        => '請選擇 Page',
			'parent_name' => 'id_web',
			'parent'      => '',
		];
		$arr_web    = Web::get($this->id_web);

		foreach ($arr_web as $i => $web) {
			$arr_file = File::getContext()->getFileList(WEB_DIR . DS . $web['dir'] . DS . 'controllers' . DS . 'front');
			foreach ($arr_file as $i => $file) {
			    //原始是用來寫入seo 給值後來不知道為何出問題 有空再修
//			    echo WEB_DIR . DS . $web['dir'] . DS . 'controllers' . DS . 'front' . DS . $file.'<br/>';
//				include_once(WEB_DIR . DS . $web['dir'] . DS . 'controllers' . DS . 'front' . DS . $file);
				$controller = str_replace('.php', '', $file);
				$class      = $web['dir'] . '\\' . $controller;
				if (class_exists($class, false)) {
					$Controller = new $class();
					if (!empty($Controller->page) && !$Controller->no_page) {
						$val  = $Controller->page;
						$text = $val;
						if (!empty($Controller->meta_title)) {
							$text .= ' (' . $Controller->meta_title . ')';
						}
						$arr_page[] = [
							'val'         => $val,
							'text'        => $text,
							'parent_name' => 'id_web',
							'parent'      => $web['id_web'],
						];
					}
				}
			}
		}

		return $arr_page;
	}

	public function setMedia()
	{
		$this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
		parent::setMedia();
		$this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
		$this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
	}
}