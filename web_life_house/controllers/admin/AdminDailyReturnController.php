<?php



class AdminDailyReturnController extends AdminController
{
    public function __construct()
    {
        $this->className = 'AdminDailyReturnController';
        $this->table = 'daily_return';
        $this->fields['index'] = 'id_daily_return';
        $this->_as = 'd_r';
        $this->fields['title'] = '<div class="input-group fixed-width-lg"><span class="input-group-addon">每日回報</span><input type="date"  name="work_date_s" value="'.Tools::getValue(work_date_s).'" class="form-control"  aria-invalid="false"><span class="input-group-addon">~</span><input type="date" name="work_date_e" value="'.Tools::getValue(work_date_e).'" class="form-control"></div>';
        $this->fields['order'] = ' ORDER BY  d_r.`work_date` DESC,d_r.`id_daily_return` DESC ';
        $this->_join = ' LEFT JOIN `daily_return_type` AS d_r_t ON d_r_t.`id_daily_return_type` = d_r.`id_daily_return_type`
                         LEFT JOIN `admin` AS a ON a.`ag` = d_r.`ag` 
                         LEFT JOIN `source` AS s ON s.`id_source` = d_r.`id_source` ';

        if($_SESSION["id_group"]=="9"){
            $this->fields['where'] .=" AND a.ag='{$_SESSION["ag"]}' ";
        }
        //work_date間隔
        $work_date_s = Tools::getValue('work_date_s');
        $work_date_e = Tools::getValue('work_date_e');

        if(!empty($work_date_s) && !empty($work_date_e)){
            $this->fields['where'] .=" AND (d_r.`work_date` >= ".GetSQL($work_date_s,"text")." AND d_r.`work_date` <= ".GetSQL($work_date_e,"text").") ";
        }else if(!empty($work_date_s) && empty($work_date_e)){
            $this->fields['where'] .=" AND d_r.`work_date` >= ".GetSQL($work_date_s,"text")." ";
        }else if(empty($work_date_s) && !empty($work_date_e)){
            $this->fields['where'] .="AND d_r.`work_date` <= ".GetSQL($work_date_e,"text")." ";
        }

        //Tools::getValue($this->fields['index'])


//		LEFT 是可以用 一樣有同樣效果，但是取出資料庫迴圈，複雜度會變高，n*n*n 筆搜尋篩選

        $this->fields['list_num'] = 50;

        $this->fields['list'] = [

            'id_daily_return' => [
                'filter_key' => 'd_r!id_daily_return',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],

            'ag' => [
                'filter_key' => 'd_r!ag',
                'title' => $this->l('ag代碼'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'title' => [
                'filter_key' => 'd_r_t!title',
                'title' => $this->l('回報類型'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'daily_return_class' => [
                'filter_key' => 'd_r!daily_return_class',
                'title' => $this->l('類別(A:屋主租 B:租客 C:屋主售 D:買客)'),
                'order' => true,
                'filter' => true,
                'values' => [
                    [
                        'class' => 'featured',
                        'value' => 'A',
                        'title' => $this->l('屋主租'),
                    ],
                    [
                        'class' => 'featured',
                        'value' => 'B',
                        'title' => $this->l('租客'),
                    ],
                    [
                        'class' => 'featured',
                        'value' => 'C',
                        'title' => $this->l('屋主售'),
                    ],
                    [
                        'class' => 'featured',
                        'value' => 'D',
                        'title' => $this->l('買客'),
                    ],
                ],
                'class' => 'text-center',
            ],

            'customer' => [
                'filter_key' => 'd_r!customer',
                'title' => $this->l('顧客'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'customer_tel' => [
                'filter_key' => 'd_r!customer_tel',
                'title' => $this->l('顧客手機'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'price' => [
                'filter_key' => 'd_r!price',
                'title' => $this->l('金額'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'obj_code' => [
                'filter_key' => 'd_r!obj_code',
                'title' => $this->l('物件編號'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'case_name' => [
                'filter_key' => 'd_r!case_name',
                'title' => $this->l('案名'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'work_date' => [
                'filter_key' => 'd_r!work_date',
                'title' => $this->l('執行時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'pattern' => [
                'title' => $this->l('開始結束-結束時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
                'filter_sql'    => 'CONCAT(d_r.`time_s`,"-",d_r.`time_e`)',
            ],

            'revisit' => [
                'filter_key' => 'd_r!revisit',
                'title' => $this->l('目的'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'purpose' => [
                'filter_key' => 'd_r!purpose',
                'title' => $this->l('目的'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'result' => [
                'filter_key' => 'd_r!result',
                'title' => $this->l('結果'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'countermeasure' => [
                'filter_key' => 'd_r!countermeasure',
                'title' => $this->l('對策'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'remarks' => [
                'filter_key' => 'd_r!remarks',
                'title' => $this->l('備註'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'create_admin' => [
                'filter_key' => 'd_r!create_admin',
                'title' => $this->l('建立者'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'create_time' => [
                'filter_key' => 'd_r!create_time',
                'title' => $this->l('建立時間'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],


        ];


        $this->fields['form'] = [
            'id_daily_return' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('每日回報'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'id_daily_return' => [
                        'name' => 'id_daily_return',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'col'       => 9,
                        'index' => true,
                        'required' => true,
                    ],


                    'ag' => [
                        'name' => 'ag',
                        'type' => 'text',
                        'form_col'  => 12,
                        'label_col' => 3,
                        'col'       => 3,
                        'maxlength' => '20',
                        'label' => $this->l('ag代號'),
                    ],



                    'work_date' => [
                        'name' => 'work_date',
                        'type' => 'date',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label' => $this->l('執行時間'),
                    ],


                    'time_s' => [
                        'name' => 'time_s',
                        'type' => 'time',
                        'form_col'  => 6,
//                        'label_col' => 3,
                        'no_label'  => true,
                        'col'       => 12,
                        'is_prefix' => true,
                    ],

                    'time_e' => [
                        'name' => 'time_e',
                        'type' => 'time',
                        'prefix' => $this->l('~'),
                        'is_suffix' => true,
                    ],



                    'id_daily_return_type'    => [
                        'name'      => 'id_daily_return_type',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label'     => $this->l('每日回報狀態'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇回報',
                                'val'  => '',
                            ],

                            'table'   => 'daily_return_type',
                            'text'    => 'title',
                            'value'   => 'id_daily_return_type',
                            'order'   => ' `id_daily_return_type` DESC',
                        ],
                        'required'  => true,
                        'no_active' => true,
                    ],

                    'id_source'    => [
                        'name'      => 'id_source',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'label'     => $this->l('開發來源'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇開發來源',
                                'val'  => '',
                            ],

                            'table'   => 'source',
                            'text'    => 'source',
                            'value'   => 'id_source',
                            'order'   => ' `position` DESC',
                        ],
//                        'required'  => true,
                        'no_active' => true,
                    ],

                    'daily_return_class'    => [
                        'name'      => 'daily_return_class',
                        'type'      => 'select',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'label'     => $this->l('顧客類別'),
                        'options'   => [
                            'default' => [
                                'text' => '請選擇顧客服務項目',
                                'val'  => '',
                            ],
                            'val' =>[
                                [
                                    'text' => '屋主租',
                                    'val'  => 'A',
                                ],
                                [
                                    'text' => '租客',
                                    'val'  => 'B',
                                ],
                                [
                                    'text' => '屋主售',
                                    'val'  => 'C',
                                ],
                                [
                                    'text' => '買客',
                                    'val'  => 'D',
                                ]
                            ]
                        ],
//                        'required'  => true,
                        'no_active' => true,
                    ],

                    'price' => [
                        'name' => 'price',
                        'type' => 'number',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'label' => $this->l('金額'),
                    ],

                    'obj_code' => [
                        'name' => 'obj_code',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'maxlength' => '20',
                        'label' => $this->l('物件編號'),
                    ],

                    'case_name' => [
                        'name' => 'case_name',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'maxlength' => '128',
                        'label' => $this->l('案名'),
                    ],

                    'customer' => [
                        'name' => 'customer',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 6,
                        'col'       => 6,
                        'maxlength' => '20',
                        'label' => $this->l('顧客姓名'),
                    ],

                    'customer_tel' => [
                        'name' => 'customer_tel',
                        'type' => 'text',
                        'form_col'  => 6,
                        'label_col' => 3,
                        'col'       => 9,
                        'maxlength' => '20',
                        'label' => $this->l('顧客手機'),
                    ],

                    'revisit' => [
                        'name' => 'revisit',
                        'type' => 'textarea',
                        'label_col'=> 3,
                        'col' => 9,
                        'label' => $this->l('復訪'),
                        'rows' =>   '10',
                        'cols'=>'5',
                        'maxlength'=>'128',
                    ],

                    'purpose' => [
                        'name' => 'purpose',
                        'type' => 'textarea',
                        'label_col'=> 3,
                        'col' => 9,
                        'label' => $this->l('目的'),
                        'rows' =>   '10',
                        'cols'=>'5',
                        'maxlength'=>'128',
                    ],

                    'result' => [
                        'name' => 'result',
                        'type' => 'textarea',
                        'label_col'=> 3,
                        'col' => 9,
                        'label' => $this->l('結果'),
                        'rows' =>   '10',
                        'cols'=>'5',
                        'maxlength'=>'128',
                    ],

                    'countermeasure' => [
                        'name' => 'countermeasure',
                        'type' => 'textarea',
                        'label_col'=> 3,
                        'col' => 9,
                        'label' => $this->l('對策'),
                        'rows' =>   '10',
                        'cols'=>'5',
                        'maxlength'=>'128',
                    ],

                    'remarks' => [
                        'name' => 'remarks',
                        'type' => 'textarea',
                        'label_col'=> 3,
                        'col' => 9,
                        'label' => $this->l('備註'),
                        'rows' =>   '10',
                        'cols'=>'5',
                        'maxlength'=>'128',
                    ],


                    'create_admin' => [
                        'name' => 'create_admin',
                        'type' => 'text',
                        'form_col' => 6,
                        'label_col' => 6,
                        'col'=> 6,
                        'disabled' =>true,
                        'label' => $this->l('建立者'),
                    ],

                    'create_time' => [
                        'name' => 'create_time',
                        'type' => 'view',
                        'form_col' => 6,
                        'label_col' => 3,
                        'col'=> 9,
                        'auto_datetime' => 'add',
                        'label' => $this->l('建立時間'),
                    ],
                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],

                'cancel' => [
                    'title' => $this->l('取消'),
                ],

                'reset' => [
                    'title' => $this->l('復原'),
                ],
            ],
        ];

        parent::__construct();

    }

    public function displayAjaxAdd(){
        $action = Tools::getValue('action');    //action = 'Add';
        switch ($action){
            case 'Add':
                $row = $_SESSION;
                echo json_encode(array("error"=>"","return"=>$row));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

    public function initProcess(){
    $submitAdddaily_return = Tools::getValue('submitAdddaily_return');//新增才有差
    $ag = Tools::getValue('ag');//ag

    if(!empty($submitAdddaily_return)){
        $sql = "SELECT * FROM `admin` WHERE id_admin=".GetSQL($_SESSION["id_admin"],"int");
        $row = Db::rowSQL($sql,true);
        $_POST["create_admin"] = $row["last_name"].$row["first_name"];
        $_POST["id_admin"] = $_SESSION["id_admin"];
    }
    if($_SESSION["id_group"]=='9'){//業務
        $_POST['ag'] = $_SESSION["ag"];
    }


        parent::initProcess();
}

    public function setMedia()
    {
        parent::setMedia();
        $this->addJS('/themes/LifeHouse/js/dailyreturn.js');
    }
}