<?php
class AdminEproCMSMainController extends AdminController
{
    public $tpl_folder;    //樣版資料夾
    public $page = 'epro_cms_main';
    public $ed;

    public function __construct()
    {
        $this->conn = 'ilifehou_epro';
        $this->className       = 'AdminEproCMSMainController';
        $this->fields['title'] = '裝修達人網頁管理主列表';
        $this->table           = 'epro_cms_main';
        $this->fields['index'] = 'id_epro_cms_main';
        $this->fields['order'] = ' ORDER BY `position` ASC, `id_epro_cms_main` ASC';
        $this->fields['list']  = [
            'id_epro_cms_main'         => [
                'index'  => true,
                'hidden' => true,
                'type'   => 'checkbox',
                'label_col' => '2',
                'col' => '3',
                'class'  => 'text-center',
            ],
            'title'            => [
                'title'  => $this->l('title'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'url'            => [
                'title'  => $this->l('url'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'position' => [
                'title'  => $this->l('順序'),
                'order'  => true,
                'class'  => 'text-center',
                'filter' => true,
            ],
            'active'         => [
                'title'  => $this->l('啟用'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->fields['form'] = [
            [
                'legend' => [
                    'title' => $this->l('裝修達人網頁管理主列表'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_epro_cms_main' => [
                        'name'     => 'id_epro_cms_main',
                        'type'     => 'hidden',
                        'index'    => true,
                        'required' => true,
                    ],
                    'title'          => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'maxlength' => 50,
                        'label'     => $this->l('Title'),
                    ],
                    'url'          => [
                        'name'      => 'url',
                        'type'      => 'text',
                        'maxlength' => 200,
                        'label'     => $this->l('url'),
                    ],
                    'active'         => [
                        'type'     => 'switch',
                        'label'    => $this->l('啟用狀態'),
                        'name'     => 'active',
                        'required' => true,
                        'val'      => 1,
                        'values'   => [
                            [
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('啟用'),
                            ],
                            [
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('關閉'),
                            ],
                        ],
                    ],
                    'position'           => [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],

                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                    [
                        'title' => $this->l('儲存並繼續編輯'),
                        'stay'  => true,
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('重置'),
                ],
            ],
        ];
        parent::__construct();
    }

    public function validateRules()
    {
        parent::validateRules();
    }

    public function setMedia()
    {
        $this->addJS('/' . MEDIA_URL . '/clipboard/js/clipboard.min.js');
        parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
    }
}