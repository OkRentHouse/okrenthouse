<?php

class AdminDeviceCategoryClassController extends AdminController
{
    public function __construct()
    {
        $this->className       = 'AdminDeviceCategoryClassController';
        $this->table           = 'device_category_class';
        $this->fields['index'] = 'id_device_category_class';
        $this->_as = 'd_c_c';
        $this->_join = ' LEFT JOIN `device_category_class_file` AS d_c_c_f ON d_c_c_f.`id_device_category_class` = d_c_c.`id_device_category_class` ';

        $this->fields['title'] = '設備主類別';
        $this->fields['order'] = ' ORDER BY `position` ASC';
        $this->fields['list'] = [
            'id_device_category_class' => [
                'filter_key' => 'd_c_c!id_device_category_class',
                'index'  => true,
                'title'  => $this->l('ID'),
                'type'   => 'checkbox',
                'hidden' => true,
                'class'  => 'text-center',
            ],
            'title'    => [
                'filter_key' => 'd_c_c!title',
                'title'  => $this->l('設備主類別'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
            'img' => [
                'title' => $this->l('縮圖'),
                'order' => true,
                'filter' => true,
                'filter_sql' => 'IF(d_c_c_f.`id_dccf` > 0, 1, 0)',
                'class' => 'text-center', 'values' => [
                    [
                        'class' => 'img_1',
                        'value' => '1',
                        'title' => $this->l('有'),
                    ],
                    [
                        'class' => 'img_0 red',
                        'value' => '0',
                        'title' => $this->l('無'),
                    ],
                ],
            ],
            'position'     => [
                'filter_key' => 'd_c_c!position',
                'title'  => $this->l('順序'),
                'order'  => true,
                'filter' => true,
                'class'  => 'text-center',
            ],
        ];

        $this->arr_file_type = [
            '0' => 'img'
        ];

        $this->fields['form'] = [
            'device_category_class' => [
                'legend' => [
                    'title' => $this->l('設備主類別'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input'  => [
                    'id_device_category_class' => [
                        'name'     => 'id_device_category_class',
                        'type'     => 'hidden',
                        'index'    => true,
                        'label'    => $this->l('型態'),
                        'required' => true,
                    ],
                    'title'    => [
                        'name'      => 'title',
                        'type'      => 'text',
                        'label'     => $this->l('設備主類別'),
                        'maxlength' => '20',
                        'required'  => true,
                    ],
                    [
                        'name'  => 'position',
                        'type'  => 'number',
                        'label' => $this->l('順序'),
                    ],
                ],
                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],
                'cancel' => [
                    'title' => $this->l('取消'),
                ],
                'reset'  => [
                    'title' => $this->l('復原'),
                ],
            ],
            'img' => [
                'tab' => 'letter',
                'legend' => [
                    'title' => $this->l('縮圖'),
                    'icon' => 'icon-cogs',
                    'image' => '',
                ],

                'input' => [
                    'img' => [
                        'name' => 'img',
                        'label_class' => 'text-left',
                        'label' => $this->l('小圖'),
                        'type' => 'file',
                        'language' => 'zh-TW',

                        'file' => [
                            'icon' => false,
                            'auto_upload' => true,
                            'max' => 1,
                            'allowed' => ['jpg', 'png', 'gif', 'jpeg'],
                            'max_width' => 80,
                            'max_height' => 80,
                            'resize' => true,
                            'resizePreference' => 'height',
                        ],
                        'multiple' => true,
                        'col' => 12,
                        'no_action' => true,
                        'p' => $this->l('圖片建議大小 80*80px'),
                    ],
                ],
            ],
        ];

        parent::__construct();
    }

    public function processDel(){
        $this->_errors[] = $this->l('無法刪除正在使用的主類別!');
        parent::processDel();
    }

    public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal)
    {
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);

        $this->arr_file_type = [
            '0' => 'img',
        ];

        $arr_type_key = array_flip($this->arr_file_type);       //key值調換
        $type = $arr_type_key[$UpFileVal['type']];
        $arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
        $id_device_category_class = $UpFileVal['id_device_category_class'];
        foreach ($arr_id_file as $i => $id_file) {
            //建案檔案
            $sql = sprintf('INSERT INTO `device_category_class_file` (`id_device_category_class`, `id_file`, `file_type`) VALUES (%d, %d, %d)',
                GetSQL($id_device_category_class, 'int'),
                GetSQL($id_file, 'int'),
                GetSQL($type, 'int')
            );
            Db::rowSQL($sql);
            if (!Db::getContext()->num)
                return false;
        }
        return true;
    }

    public function iniUpFileDir()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_device_category_class = $UpFileVal['id_device_category_class'];

        list($dir, $url) = str_dir_change($id_device_category_class);     //切割資料夾
        $dir = WEB_DIR . DS . DEVICE_CATEGORY_CLASS_IMG_DIR . DS . $dir;
        return $dir;
    }

    public function iniUpFileUrl()
    {
        $UpFileVal = Tools::getValue('UpFileVal');
        $type = $UpFileVal['type'];
        $json = new JSON();
        $UpFileVal = (array)$json->decode($UpFileVal);
        $id_device_category_class = $UpFileVal['id_device_category_class'];
        list($dir, $url) = str_dir_change($id_device_category_class);
        $url = DEVICE_CATEGORY_CLASS_IMG_URL . $url;
        return $url;
    }

    public function getUpFile($id_device_category_class)
    {
        $sql = sprintf('SELECT `id_file`, `file_type`
				FROM `device_category_class_file`
				WHERE `id_device_category_class` = %d',
            GetSQL($id_device_category_class, 'int'));
        $arr_row = Db::rowSQL($sql);
        return $arr_row;
    }


    public function getUpFileList()
    {
        $id_device_category_class = Tools::getValue('id_device_category_class');
        $json = new JSON();
        $UpFileVal = [];

        foreach ($this->arr_file_type as $i => $type_neme) {
            $UpFileVal[$type_neme] = '\'' . $json->encode([
                    'id_device_category_class' => $id_device_category_class,
                    'type' => $type_neme,       //自訂意,此為圖片檔案的類型
                ]) . '\'';
        }

        $file_up = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_up');
        $file_move = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_move');
        $file_download = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_download');
        $file_del = GiveGroup::getGroupFunction($_SESSION['id_group'], 'file_del');
        $arr_type_row = $this->getUpFile($id_device_category_class);

        if ($_SESSION['id_group'] == 1 || $file_up || $file_move || $file_download || $file_del) {
            foreach ($arr_type_row as $id => $type) {
                $arr_row = FileUpload::get($type['id_file']);       //取得檔案詳細資訊
                foreach ($arr_row as $i => $v) {
                    if ($_SESSION['id_group'] == 1 || $file_download) {
                        $url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
                        $url = urldecode(Tools::getCurrentUrlProtocolPrefix() . WEB_DNS . $url);
                    } else {
                        $url = '';
                    }
                    $type_name = $this->arr_file_type[$type['file_type']];     //取得檔案type名稱
                    $arr_initialPreview[$type_name][] = $url;
                    $file_type = est_to_type(ext($v['filename']));
                    $del_table = "device_category_class_file";
                    $arr_initialPreviewConfig[$type_name][] = [
                        'type' => $file_type,
                        'filetype' => $v['type'],
                        'caption' => $v['filename'],
                        'size' => $v['size'],
                        'url' => '/manage/DeviceCategoryClass?&id_file=' . $v['id_device_category_class'] . '&type_name=' . $type_name . '&table=' . $del_table . '&ajax=1&action=DelFile',
                        'downloadUrl' => $url,
                        'key' => $v['id_file'],
                    ];
                }
            }
        }

        foreach ($arr_initialPreview as $i => $v) {
            $arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
            $arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
        }

        Context::getContext()->smarty->assign([
            'initialPreview' => $arr_initialPreview,
            'initialPreviewConfig' => $arr_initialPreviewConfig,
            'UpFileVal' => $UpFileVal,
        ]);
    }
}