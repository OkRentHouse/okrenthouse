<?php

class AdminBrokerController extends AdminController


{


    public function __construct()
    {

        $this->className = 'AdminBrokerController';

        $this->table = 'broker';

        $this->fields['index'] = 'id_broker';

        $this->_as = 'b';

        $this->fields['title'] = '房仲管理';

        $this->_join = ' LEFT JOIN `member` AS m ON b.`id_member` = m.`id_member` ';

        $this->fields['order'] = ' ORDER BY b.`id_broker` ASC ';

        $this->fields['list_num'] = 50;

        $this->fields['list'] = [
            'id_broker' => [
                'filter_key' => 'b!id_broker',
                'index' => true,
                'title' => $this->l('ID'),
                'type' => 'checkbox',
                'hidden' => true,
                'class' => 'text-center',
            ],

            'name' => [
                'filter_key' => 'm!name',
                'title' => $this->l('房仲姓名'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'brand' => [
                'filter_key' => 'b!brand',
                'title' => $this->l('品牌'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'company' => [
                'filter_key' => 'b!company',
                'title' => $this->l('公司'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'company_tel' => [
                'filter_key' => 'b!company_tel',
                'title' => $this->l('公司電話'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'company_number' => [
                'filter_key' => 'b!company_number',
                'title' => $this->l('公司統編'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],

            'company_address' => [
                'filter_key' => 'b!company_address',
                'title' => $this->l('公司地址'),
                'order' => true,
                'filter' => true,
                'class' => 'text-center',
            ],
        ];

        $this->fields['tabs'] = [
            'session' => $this->l('房仲管理'),
        ];

        $this->fields['form'] = [
            'id_broker' =>[
                'tab'    => 'letter',
                'legend' => [
                    'title' => $this->l('房仲'),
                    'icon'  => 'icon-cogs',
                    'image' => '',
                ],
                'input' => [

                    'id_broker' => [
                        'name' => 'id_broker',
                        'type' => 'hidden',
                        'label_col' => 3,
                        'index' => true,
                        'required' => true,
                    ],

                    'id_in_life' => [
                        'name'      => 'id_member',
                        'type'      => 'select',
                        'label_col' => 3,
                        'label'     => $this->l('選擇房仲身分id'),
                        'options'   => [
                            'default' => [
                                'text' => '選擇房仲',
                                'val'  => '',
                            ],
                            'table'   => 'member',
                            'text'    => 'name',
                            'value'   => 'id_member',
                            'order'   => '`id_member` ASC',
                            'where'   => " AND active=1 AND id_member_additional LIKE '%3%'  ",
                        ],
                        'required'  => true,
                    ],

                    'brand' => [
                        'name'      => 'brand',
                        'type'      => 'text',
                        'label_col' => 3,
                        'label'     => $this->l('品牌'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],

                    'company' => [
                        'name'      => 'company',
                        'type'      => 'text',
                        'label_col' => 3,
                        'label'     => $this->l('公司'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],

                    'company_tel' => [
                        'name'      => 'company_tel',
                        'type'      => 'text',
                        'label_col' => 3,
                        'label'     => $this->l('公司電話號碼'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],

                    'company_number' => [
                        'name'      => 'company_number',
                        'type'      => 'number',
                        'label_col' => 3,
                        'label'     => $this->l('公司統編'),
                        'maxlength' => '11',
                        'required'  => true,
                    ],

                    'company_address' => [
                        'name'      => 'company_address',
                        'type'      => 'text',
                        'label_col' => 3,
                        'label'     => $this->l('公司地址'),
                        'maxlength' => '64',
                        'required'  => true,
                    ],

                    'update_time' => [
                        'name' => 'update_time',
                        'type' => 'time',
                        'label_col' => 3,
                        'label' => $this->l('建立時間'),
                        'disabled' =>true,
                    ],

                ],


                'submit' => [
                    [
                        'title' => $this->l('儲存'),
                    ],
                ],

                'cancel' => [
                    'title' => $this->l('取消'),
                ],

                'reset' => [
                    'title' => $this->l('復原'),
                ],

            ],

        ];


        parent::__construct();
    }

}
