<?php
class AdminPointToPayController extends AdminController
{
    public function __construct()
    {
        $this->className = 'AdminPointToPayController';
        $this->table = 'point_to_pay';
        $this->fields['index'] = 'id_point_to_pay';
        $this->_as = 'p_t_p';
        $this->fields['title'] = '點交收單';
        $this->_join =' LEFT JOIN home_appliances as h_a ON h_a.`id_point_to_pay` = p_t_p.`id_point_to_pay`';
        $this->fields['list_num'] = 50;

        $this->fields['list'] = [
            'id_point_to_pay' => [
                'filter_key' => 'p_t_p!id_point_to_pay',
                'index' => true,
                'title' => $this->l('ID'),
//                'type' => 'checkbox',
//                'hidden' => true,
                'class' => 'text-center',
                'order' => true,
                'filter' => true,
            ],

        ];

        $this->fields['tabs'] = [
            'home_appliances'=>$this->l('家具/家電/設備/裝潢'),
            'get_data'=>$this->l('交付資料'),
            'change_record'=>$this->l('變更紀錄'),
            'contract_income_statement' =>$this->l('合約損益'),
        ];

        //家具
        //tpl 1 是指 數量,品牌,擺放,狀態
        //2 是 數量,椅,品牌,擺放,狀態
        //3 是 數量,類型,品牌,擺放,狀態
        //4 是 數量,類型(select的),品牌,擺放,狀態
        //5 是 數量,擺放,狀態
        //6 數量,狀態
        //7 數量,類型(select的),擺放,狀態
        //8 數量,類型(select的),狀態
        //這邊規則tpl是代表我這邊所使用的模板方式
        $this->tpl_data = [
            'id_home_appliances' => [
                'name' => 'id_home_appliances',
                'type' => 'hidden',
                'label_col' => 3,
                'index' => true,
                'required' => true,
            ],

            'id_point_to_pay' => [
                'name' => 'id_point_to_pay',
                'type' => 'hidden',
                'label_col' => 3,
                'link' =>true,
            ],

            'single_bed_bottom'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'單人床底',
                'name'=>'single_bed_bottom',
            ],

            'single_bed_mattress'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'單人床墊',
                'name'=>'single_bed_mattress',
            ],

            'single_bed_box'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'單人床頭箱',
                'name'=>'single_bed_box',
            ],

            'single_bed_board'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'單人床頭板',
                'name'=>'single_bed_board',
            ],

            'double_bed_bottom'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'雙人床底',
                'name'=>'double_bed_bottom',
            ],

            'double_bed_mattress'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'雙人床墊',
                'name'=>'double_bed_mattress',
            ],

            'double_bed_box'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'雙人床頭箱',
                'name'=>'double_bed_box',
            ],

            'double_bed_board'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'雙人床頭板',
                'name'=>'double_bed_board',
            ],

            'side_cabinet'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'側櫃',
                'name'=>'side_cabinet',
            ],

            'dresser'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'斗櫃',
                'name'=>'dresser',
            ],

            'dressing_table'=>[
                'tpl'=>2,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'梳妝台',
                'name'=>'dressing_table',
            ],

            'wardrobe'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'衣櫃',
                'name'=>'wardrobe',
            ],

            'desk'=>[
                'tpl'=>2,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'書桌',
                'name'=>'desk',
            ],

            'sofa'=>[
                'tpl'=>4,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇沙發材質',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'布',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'皮',
                        'id'=>'',
                    ],
                    [
                        'val'=>'2',
                        'text'=>'實木',
                        'id'=>'',
                    ],
                    [
                        'val'=>'3',
                        'text'=>'藤',
                        'id'=>'',
                    ]
                ],
                'select'=>[
                    'val'=>',0,1,2,3',
                    'text'=>'選擇沙發材質,布,皮,實木,藤'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'沙發',
                'name'=>'sofa',
            ],

            'pillow'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type'=>'number',
                'label'=>'抱枕',
                'name'=>'pillow',
            ],

            'cushion'=>[
                'form_col' => 3,
                'label_col' => 3,
                'col' => 9,
                'type'=>'number',
                'label'=>'靠墊',
                'name'=>'cushion',
            ],

            'footstool'=>[
                'form_col' => 3,
                'label_col' => 3,
                'col' => 9,
                'type'=>'number',
                'label'=>'腳凳',
                'name'=>'footstool',
            ],

            'tea_table'=>[
                'tpl'=>3,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'茶几',
                'name'=>'tea_table',
            ],

            'tv_cabinet'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'TV櫃',
                'name'=>'tv_cabinet',
            ],

            'cupboard'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'櫥櫃',
                'name'=>'cupboard',
            ],

            'shoebox'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'鞋櫃',
                'name'=>'shoebox',
            ],

            'curtain'=>[
                'tpl'=>8,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇窗簾材質',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'布',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'紗',
                        'id'=>'',
                    ],
                ],
                'select'=>[
                    'val'=>',0,1',
                    'text'=>'選擇窗簾材質,布,紗'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'窗簾',
                'name'=>'curtain',
            ],

            'shower_curtain'=>[
                'tpl'=>6,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'浴簾',
                'name'=>'shower_curtain',
            ],

            'door_curtain'=>[
                'tpl'=>6,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'門簾',
                'name'=>'door_curtain',
            ],

            'carpet'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'地毯',
                'name'=>'carpet',
            ],

            'doily_cloth'=>[
                'tpl'=>6,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'桌巾布',
                'name'=>'doily_cloth',
            ],

            'lcd'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'液晶電視',
                'name'=>'lcd',
            ],

            'flat_screen_tv'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'平面電視',
                'name'=>'flat_screen_tv',
            ],

            'window_type'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'窗型冷氣',
                'name'=>'window_type',
            ],

            'separate_type'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'分離式冷氣',
                'name'=>'separate_type',
            ],

            'refrigerator'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'冰箱',
                'name'=>'refrigerator',
            ],

            'washer'=>[
                'tpl'=>4,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇洗衣機模式',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'洗',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'洗烘',
                        'id'=>'',
                    ]
                ],
                'select'=>[
                    'val'=>',0,1',
                    'text'=>'選擇洗衣機模式,洗,洗烘'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'洗衣機',
                'name'=>'washer',
            ],

            'dryer'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'烘衣機',
                'name'=>'dryer',
            ],

            'geyser'=>[
                'tpl'=>4,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇熱水器運作方式',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'瓦斯',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'電熱',
                        'id'=>'',
                    ],
                    [
                        'val'=>'2',
                        'text'=>'太陽能',
                        'id'=>'',
                    ]
                ],
                'select'=>[
                    'val'=>',0,1,2',
                    'text'=>'選擇熱水器運作方式,瓦斯,電熱,太陽能'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'熱水器',
                'name'=>'geyser',
            ],

            'induction_cooker'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'電磁爐',
                'name'=>'induction_cooker',
            ],

            'gas_cooktop'=>[
                'tpl'=>4,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇瓦斯爐運作方式',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'天然氣',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'桶裝',
                        'id'=>'',
                    ],
                ],
                'select'=>[
                    'val'=>',0,1',
                    'text'=>'選擇瓦斯爐運作方式,天然氣,桶裝'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'瓦斯爐',
                'name'=>'gas_cooktop',
            ],

            'microwave_oven'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'微波爐',
                'name'=>'microwave_oven',
            ],

            'oven'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'烤箱',
                'name'=>'oven',
            ],

            'microwave_oven_stove'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'微波烤箱爐',
                'name'=>'microwave_oven_stove',
            ],

            'range_hood'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'油煙機',
                'name'=>'range_hood',
            ],

            'dish_dryer'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'烘碗機',
                'name'=>'dish_dryer',
            ],

            'dishwasher'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'洗碗機',
                'name'=>'dishwasher',
            ],

            'fluid_table'=>[
                'tpl'=>6,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'流理臺',
                'name'=>'fluid_table',
            ],

            'kitchen_cupboard'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'廚房櫥櫃',
                'name'=>'kitchen_cupboard',
            ],

            'lamp_stand'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'燈座',
                'name'=>'lamp_stand',
            ],

            'bulb'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'燈泡',
                'name'=>'bulb',
            ],

            'cabinet_light'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'櫃燈',
                'name'=>'cabinet_light',
            ],

            'standing_lamp'=>[
                'tpl'=>5,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'立燈',
                'name'=>'standing_lamp',
            ],

            'clotheshorse'=>[
                'tpl'=>7,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇曬衣架運作方式',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'電動',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'手動',
                        'id'=>'',
                    ],
                ],
                'select'=>[
                    'val'=>',0,1',
                    'text'=>'選擇曬衣架運作方式,電動,手動'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'曬衣架',
                'name'=>'clotheshorse',
            ],

            'commode'=>[
                'tpl'=>4,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇馬桶類型',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'免治',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'超級',
                        'id'=>'',
                    ],
                    [
                        'val'=>'2',
                        'text'=>'一般',
                        'id'=>'',
                    ],
                ],
                'select'=>[
                    'val'=>',0,1,2',
                    'text'=>'選擇馬桶類型,免治,超級,一般'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'馬桶',
                'name'=>'commode',
            ],

            'bathtub'=>[
                'tpl'=>4,
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇浴缸類型',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'按摩',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'一般',
                        'id'=>'',
                    ],
                ],
                'select'=>[
                    'val'=>',0,1',
                    'text'=>'選擇浴缸類型,按摩,一般'
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'浴缸',
                'name'=>'bathtub',
            ],

            'oven_room'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'烤箱房',
                'name'=>'oven_room',
            ],

            'steam_room'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'蒸氣室',
                'name'=>'steam_room',
            ],

            'dehumidifier'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'除濕機',
                'name'=>'dehumidifier',
            ],

            'electric_fan'=>[
                'tpl'=>1,
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'電風扇',
                'name'=>'electric_fan',
            ],

            'crane'=>[
                'form_col' => 12,
                'label_col' => 3,
                'col' => 3,
                'type' =>'number',
                'label'=>'天車',
                'name'=>'crane',
            ],

            'power_supply' =>[
                'type' => 'checkbox',
                'label' => '動力電',
                'name' => 'power_supply',
                'values' => [
                    [
                        'val'=>'0',
                        'label'=>'沒有',
                    ],
                    [
                        'val'=>'1',
                        'label'=>'有',
                    ]
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
            ],

            'air_conditioning' =>[
                'type' => 'checkbox',
                'label' => '空調設備',
                'name' => 'air_conditioning',
                'values' => [
                    [
                        'val'=>'0',
                        'label'=>'沒有',
                    ],
                    [
                        'val'=>'1',
                        'label'=>'有',
                    ]
                ],
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
            ],

            'other_bathroom_equipment' => [
                'name' => 'other_bathroom_equipment',
                'type' => 'textarea',
                'form_col' => 12,
                'label_col'=> 3,
                'col' => 9,
                'label' =>'其他衛浴設備',
                'rows' =>'10',
                'cols'=>'5',
            ],

            'other_furniture' => [
                'name' => 'other_furniture',
                'type' => 'textarea',
                'form_col' => 12,
                'label_col'=> 3,
                'col' => 9,
                'label' =>'其他家具',
                'rows' =>'10',
                'cols'=>'5',
            ],

            'other_home_appliance' => [
                'name' => 'other_home_appliance',
                'type' => 'textarea',
                'form_col' => 12,
                'label_col'=> 3,
                'col' => 9,
                'label' =>'其他家電',
                'rows' =>'10',
                'cols'=>'5',
            ],

            'free_article' => [
                'name' => 'free_article',
                'type' => 'textarea',
                'form_col' => 12,
                'label_col'=> 3,
                'col' => 9,
                'label' =>'無償使用',
                'rows' =>'10',
                'cols'=>'5',
            ],

        ];//由於我這塊有各種不同的格是因此要這樣寫

        //收支表
        $this->tpl_data_c_i_s =[
            'id_contract_income_statement'=>[
                'name' => 'id_contract_income_statement',
                'type' => 'hidden',
                'label_col' => 3,
                'index' => true,
                'required' => true,
            ],

            'case_name'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'案名',
                'name'=>'case_name',
                'maxlength' => '20',
            ],

            'rent_house_code'=>[
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'type' =>'text',
                'label'=>'租屋物件編號',
                'name'=>'rent_house_code',
                'maxlength' => '20',
            ],

            'income_type' => [
                'name' => 'income_type',
                'type' => 'select',
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'label' => $this->l('損益類型'),
                'values' => [
                    [
                        'text' => '選擇收支',
                        'val' => '',
                    ],
                    [
                        'text' => '支出',
                        'val' => '0',
                    ],
                    [
                        'text' => '收入',
                        'val' => '1',
                    ],
                ],
            ],

            'income_people' => [
                'name' => 'income_people',
                'type' => 'select',
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'label' => $this->l('損益人'),
                'values' => [
                    [
                        'text' => '選擇損益人',
                        'val' => '',
                    ],
                    [
                        'text' => '屋主/賣方',
                        'val' => '0',
                    ],
                    [
                        'text' => '租客/買方',
                        'val' => '1',
                    ],
                ],
            ],

            'item'=>[
                'type' => 'tpl',
                'form_col' => 12,
                'label_col' => 3,
                'col' => 9,
                'label'=>'項目',
                'name' => 'item',
                'values'=>[
                    [
                        'val'=>'',
                        'text'=>'選擇收支',
                        'id'=>'',
                    ],
                    [
                        'val'=>'0',
                        'text'=>'支出',
                        'id'=>'',
                    ],
                    [
                        'val'=>'1',
                        'text'=>'收入',
                        'id'=>'',
                    ],
                ],
                'select'=>[
                    'val'=>',0,1',
                    'text'=>'選擇收支,支出,收入'
                ],
            ],

            'funds_total'=>[
                'form_col' => 12,
                'label_col' => 3,
                'col' => 3,
                'type' =>'number',
                'label'=>'總收支',
                'id'=>'funds_total',
                'name'=>'funds_total',
            ],

            'transfer_in_bank'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入銀行',
                'name'=>'transfer_in_bank',
                'maxlength' => '32',
            ],

            'transfer_in_branch'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入分行',
                'name'=>'transfer_in_branch',
                'maxlength' => '16',
            ],

            'transfer_in_bank_code'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入銀行代碼',
                'name'=>'transfer_in_bank_code',
                'maxlength' => '16',
            ],

            'transfer_in_account'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入帳號',
                'name'=>'transfer_in_account',
                'maxlength' => '20',
            ],

            'transfer_in_account_name'=>[
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉入戶名',
                'name'=>'transfer_in_account_name',
                'maxlength' => '20',
            ],

            'transfer_out_bank'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出銀行',
                'name'=>'transfer_out_bank',
                'maxlength' => '32',
            ],

            'transfer_out_branch'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出分行',
                'name'=>'transfer_out_branch',
                'maxlength' => '16',
            ],

            'transfer_out_bank_code'=>[
                'form_col' => 3,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出銀行代碼',
                'name'=>'transfer_out_bank_code',
                'maxlength' => '16',
            ],

            'transfer_out_account'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出帳號',
                'name'=>'transfer_out_account',
                'maxlength' => '20',
            ],

            'transfer_out_account_name'=>[
                'form_col' => 6,
                'label_col' => 3,
                'col' => 6,
                'type' =>'text',
                'label'=>'轉出戶名',
                'name'=>'transfer_out_account_name',
                'maxlength' => '20',
            ],

            'data_mail'=>[
                'form_col' => 12,
                'label_col'=> 3,
                'col' => 9,
                'rows' =>'10',
                'cols'=>'5',
                'type' =>'textarea',
                'label'=>'資料郵寄',
                'name'=>'data_mail',
                'maxlength' => '256',
            ],

            'contract_date'=>[
                'form_col' => 6,
                'label_col' => 6,
                'col' => 6,
                'type' =>'text',
                'label'=>'期約',
                'id'=>'contract_date',
                'name'=>'contract_date',
            ],


        ];

        parent::__construct();
    }

    public function initProcess()
    {

        $sql = "SELECT id_point_to_pay FROM point_to_pay WHERE id_point_to_pay=".Tools::getValue("id_point_to_pay");
        $row_p_t_p = Db::rowSQL($sql,true); //取出作版並且為後續處理

        $this->fields['form']['home_appliances'] = [
            'tab' => 'home_appliances',
            'legend' => [
                'title' => $this->l('家具/家電/設備'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],
            'tpl'=>'themes/LifeHouse/home_appliances.tpl',
//             'html'=>'<div> TEST 1234567</div>',

            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],

            'reset' => [
                'title' => $this->l('重置'),
            ],

        ];

        $this->fields['form']['clean'] = [
            'tab' => 'home_appliances',
            'legend' => [
                'title' => $this->l('裝潢清潔'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],
            'input'=>[
                'id_point_to_pay' => [
                    'name' => 'id_point_to_pay',
                    'type' => 'hidden',
                    'label_col' => 3,
                    'index' => true,
                    'required' => true,
                ],

                'wall_status' => [
                    'name' => 'wall_status',
                    'type' => 'switch',
                    'label_col' => 3,
                    'col'=> 9,
                    'label' => $this->l('牆面狀態'),
                    'values' => [
                        [
                            'id' => 'wall_status_0',
                            'value' => 0,
                            'label' => $this->l('完好'),
                        ],
                        [
                            'id' => 'wall_status_1',
                            'value' => 1,
                            'label' => $this->l('尚可'),
                        ],
                        [
                            'id' => 'wall_status_2',
                            'value' => 2,
                            'label' => $this->l('毀損或髒汙'),
                        ],
                    ],
                ],

                'wall_description' => [
                    'name' => 'wall_description',
                    'type' => 'text',
                    'form_col'=>12,
                    'label_col' => 3,
                    'col'=>6,
                    'label' => $this->l('描述'),
                    'maxlength'=>64,
                ],

                'floor_status' => [
                    'name' => 'floor_status',
                    'type' => 'switch',
                    'label_col' => 3,
                    'col'=> 9,
                    'label' => $this->l('地面狀態'),
                    'values' => [
                        [
                            'id' => 'floor_status_0',
                            'value' => 0,
                            'label' => $this->l('完好'),
                        ],
                        [
                            'id' => 'floor_status_1',
                            'value' => 1,
                            'label' => $this->l('尚可'),
                        ],
                        [
                            'id' => 'floor_status_2',
                            'value' => 2,
                            'label' => $this->l('毀損或髒汙'),
                        ],
                    ],
                ],

                'floor_description' => [
                    'name' => 'floor_description',
                    'type' => 'text',
                    'form_col'=>12,
                    'label_col' => 3,
                    'col'=>6,
                    'label' => $this->l('描述'),
                    'maxlength'=>64,
                ],

                'decorate_description' => [
                    'name' => 'decorate_description',
                    'type' => 'text',
                    'form_col'=>12,
                    'label_col' => 3,
                    'col'=>6,
                    'label' => $this->l('裝潢及恢復原狀'),
                    'maxlength'=>64,
                ],

                'house_clean_status' => [
                    'name' => 'house_clean_status',
                    'type' => 'switch',
                    'label_col' => 3,
                    'col'=> 9,
                    'label' => $this->l('地面狀態'),
                    'values' => [
                        [
                            'id' => 'house_clean_status_0',
                            'value' => 0,
                            'label' => $this->l('交屋以清潔'),
                        ],
                        [
                            'id' => 'house_clean_status_1',
                            'value' => 1,
                            'label' => $this->l('未清潔'),
                        ],
                        [
                            'id' => 'house_clean_status_2',
                            'value' => 2,
                            'label' => $this->l('其他'),
                        ],
                    ],
                ],

                'house_clean_description' => [
                    'name' => 'house_clean_description',
                    'type' => 'text',
                    'form_col'=>12,
                    'label_col' => 3,
                    'col'=>6,
                    'label' => $this->l('描述'),
                    'maxlength'=>64,
                ],

            ],
            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],

            'reset' => [
                'title' => $this->l('重置'),
            ],

        ];

        $this->fields['form']['get_data'] = [
            'tab' => 'get_data',
            'legend' => [
                'title' => $this->l('交付資料'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],
            'input'=>[
                'id_point_to_pay' => [
                    'name' => 'id_point_to_pay',
                    'type' => 'hidden',
                    'label_col' => 3,
                    'index' => true,
                    'required' => true,
                ],

                'house_tax_original' => [
                    'name' => 'house_tax_original',
                    'type' => 'switch',
                    'form_col'=>6,
                    'col'=> 6,
                    'label_col' => 6,
                    'label' => $this->l('房屋稅單正本'),
                    'values' => [
                        [
                            'id' => 'house_tax_original_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'house_tax_original_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'house_tax_copy' => [
                    'name' => 'house_tax_copy',
                    'type' => 'switch',
                    'form_col'=>6,
                    'label_col' => 3,
                    'col'=> 6,
                    'label' => $this->l('房屋稅單影本'),
                    'values' => [
                        [
                            'id' => 'house_tax_copy_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'house_tax_copy_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'build_use_consent' => [
                    'name' => 'build_use_consent',
                    'type' => 'switch',
                    'form_col'=>12,
                    'col'=> 3,
                    'label_col' => 3,
                    'label' => $this->l('建築物使用同意書'),
                    'values' => [
                        [
                            'id' => 'build_use_consent_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'build_use_consent_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'water_receipt_original' => [
                    'name' => 'water_receipt_original',
                    'type' => 'switch',
                    'form_col'=>6,
                    'col'=> 6,
                    'label_col' => 6,
                    'label' => $this->l('水費收據正本'),
                    'values' => [
                        [
                            'id' => 'water_receipt_original_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'water_receipt_original_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'water_receipt_copy' => [
                    'name' => 'water_receipt_copy',
                    'type' => 'switch',
                    'form_col'=>6,
                    'label_col' => 3,
                    'col'=> 6,
                    'label' => $this->l('水費收據影本'),
                    'values' => [
                        [
                            'id' => 'water_receipt_copy_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'water_receipt_copy_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'electricity_receipt_original' => [
                    'name' => 'electricity_receipt_original',
                    'type' => 'switch',
                    'form_col'=>6,
                    'col'=> 6,
                    'label_col' => 6,
                    'label' => $this->l('電費收據正本'),
                    'values' => [
                        [
                            'id' => 'electricity_receipt_original_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'electricity_receipt_original_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'electricity_receipt_copy' => [
                    'name' => 'electricity_receipt_copy',
                    'type' => 'switch',
                    'form_col'=>6,
                    'label_col' => 3,
                    'col'=> 6,
                    'label' => $this->l('電費收據影本'),
                    'values' => [
                        [
                            'id' => 'electricity_receipt_copy_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'electricity_receipt_copy_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'home_appliances_manual' => [
                    'name' => 'home_appliances_manual',
                    'type' => 'text',
                    'label' => $this->l('家電設備使用說明書'),
                    'form_col' =>12,
                    'label_col' => 3,
                    'col'=>9,
                    'maxlength'=>128,
                ],

                'garbage_truck_description' => [
                    'name' => 'garbage_truck_description',
                    'type' => 'switch',
                    'form_col'=> 12,
                    'col'=> 3,
                    'label_col' => 3,
                    'label' => $this->l('垃圾車時間說明'),
                    'values' => [
                        [
                            'id' => 'garbage_truck_description_0',
                            'value' => 0,
                            'label' => $this->l('無'),
                        ],
                        [
                            'id' => 'garbage_truck_description_1',
                            'value' => 1,
                            'label' => $this->l('有'),
                        ],
                    ],
                ],

                'garbage_truck_address_time' => [
                    'name' => 'garbage_truck_address_time',
                    'type' => 'text',
                    'label' => $this->l('垃圾車時間與放置地點'),
                    'form_col' =>12,
                    'label_col' => 3,
                    'col'=>9,
                    'maxlength'=>128,
                ],

                'network_install' => [
                    'name' => 'network_install',
                    'type' => 'switch',
                    'form_col'=> 12,
                    'label_col' => 3,
                    'col'=> 9,
                    'label' => $this->l('網路安裝'),
                    'values' => [
                        [
                            'id' => 'network_install_0',
                            'value' => 0,
                            'label' => $this->l('原有'),
                        ],
                        [
                            'id' => 'network_install_1',
                            'value' => 1,
                            'label' => $this->l('租客'),
                        ],
                        [
                            'id' => 'network_install_2',
                            'value' => 2,
                            'label' => $this->l('房東'),
                        ],
                        [
                            'id' => 'network_install_3',
                            'value' => 3,
                            'label' => $this->l('代替租客'),
                        ],
                        [
                            'id' => 'network_install_4',
                            'value' => 4,
                            'label' => $this->l('代替房東'),
                        ],
                    ],
                ],

                'network_operator' => [
                    'name' => 'network_operator',
                    'type' => 'text',
                    'label' => $this->l('網路使用業者'),
                    'form_col' =>12,
                    'label_col' => 3,
                    'col' =>3,
                    'maxlength'=>32,
                ],

                'cable_tv_install' => [
                    'name' => 'cable_tv_install',
                    'type' => 'switch',
                    'form_col'=> 12,
                    'label_col' => 3,
                    'col'=> 9,
                    'label' => $this->l('有線電視安裝'),
                    'values' => [
                        [
                            'id' => 'cable_tv_install_0',
                            'value' => 0,
                            'label' => $this->l('原有'),
                        ],
                        [
                            'id' => 'cable_tv_install_1',
                            'value' => 1,
                            'label' => $this->l('租客'),
                        ],
                        [
                            'id' => 'cable_tv_install_2',
                            'value' => 2,
                            'label' => $this->l('房東'),
                        ],
                        [
                            'id' => 'cable_tv_install_3',
                            'value' => 3,
                            'label' => $this->l('代替租客'),
                        ],
                        [
                            'id' => 'cable_tv_install_4',
                            'value' => 4,
                            'label' => $this->l('代替房東'),
                        ],
                    ],
                ],

                'cable_tv_operator' => [
                    'name' => 'cable_tv_operator',
                    'type' => 'text',
                    'label' => $this->l('第四台使用業者'),
                    'form_col' =>12,
                    'label_col' => 3,
                    'col'=>3,
                    'maxlength'=>32,
                ],


                'network_cost' => [
                    'name' => 'network_cost',
                    'type' => 'switch',
                    'form_col'=> 6,
                    'col'=> 6,
                    'label_col' => 6,
                    'label' => $this->l('網路費用'),
                    'values' => [
                        [
                            'id' => 'network_cost_0',
                            'value' => 0,
                            'label' => $this->l('未付清'),
                        ],
                        [
                            'id' => 'network_cost_1',
                            'value' => 1,
                            'label' => $this->l('以付清'),
                        ],
                    ],
                ],

                'cable_tv_cost' => [
                    'name' => 'cable_tv_cost',
                    'type' => 'switch',
                    'form_col'=> 6,
                    'label_col' => 3,
                    'col'=> 6,
                    'label' => $this->l('第四台費用'),
                    'values' => [
                        [
                            'id' => 'cable_tv_cost_0',
                            'value' => 0,
                            'label' => $this->l('未付清'),
                        ],
                        [
                            'id' => 'cable_tv_cost_1',
                            'value' => 1,
                            'label' => $this->l('以付清'),
                        ],
                    ],
                ],

            ],
            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],

            'reset' => [
                'title' => $this->l('重置'),
            ],

        ];



        $this->fields['form']['contract_income_statement'] = [
            'tab' => 'contract_income_statement',
            'legend' => [
                'title' => $this->l('合約收支紀錄表'),
                'icon' => 'icon-cogs',
                'image' => '',
            ],
            'tpl'=>'themes/LifeHouse/contract_income_statement.tpl',
//             'html'=>'<div> TEST 1234567</div>',

            'submit' => [
                [
                    'title' => $this->l('儲存'),
                ],
                [
                    'title' => $this->l('儲存並繼續編輯'),
                    'stay' => true,
                ],
            ],
            'cancel' => [
                'title' => $this->l('取消'),
            ],

            'reset' => [
                'title' => $this->l('重置'),
            ],

        ];


        $sql = "SELECT * FROM home_appliances WHERE id_point_to_pay=".GetSQL(Tools::getValue("id_point_to_pay"),"int");

        $row = Db::rowSQL($sql,true); //取出作版並且為後續處理
        $row = Db::all_antonym_array($row,true);//做處理

        $this->context->smarty->assign([
            'main_index' =>$this->fields['index'],
            'tpl_data' => $this->tpl_data,
            'row'=> $row,
        ]);

        $sql = "SELECT * FROM contract_income_statement WHERE id_point_to_pay=".GetSQL(Tools::getValue("id_point_to_pay"),"int");
        $row_c_i_s = Db::rowSQL($sql,true);
        $row_c_i_s = Db::all_antonym_array($row_c_i_s,true);//做處理

        $this->context->smarty->assign([
            'main_index' =>$this->fields['index'],
            'tpl_data_c_i_s' => $this->tpl_data_c_i_s,
            'row_c_i_s'=> $row_c_i_s,
        ]);

        parent::initProcess();
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('../media/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.css');
        $this->addJS('../media/bootstrap-datepicker-master/js/bootstrap-datepicker.js');
        $this->addJS('../media/bootstrap-datepicker-master/js/locales/bootstrap-datepicker.zh-TW.js');
        $this->addJS('/themes/LifeHouse/js/contract_income_statement.js');

    }
}