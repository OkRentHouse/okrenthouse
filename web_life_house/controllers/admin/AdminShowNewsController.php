<?php

class AdminShowNewsController extends AdminController
{
	public function __construct()
	{
		$this->page = 'show_news';
		$this->className       = 'AdminShowNewsController';
		$this->table           = 'news';
		$this->fields['index'] = 'id_news';
		$this->fields['title'] = '最新消息';
		$this->fields['order'] = ' ORDER BY `top` DESC, `date_time` DESC, `add_time` DESC';
		$this->fields['where'] .= ' AND `date_time` <= "'.now().'" AND `active` = 1';

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				if ($_SESSION['id_web']) {
					$where                 = ' AND (`id_web` = ' . $_SESSION['id_web'] . ' OR `id_web` = 0)';
					$this->fields['where'] = ' AND (`id_web` = ' . $_SESSION['id_web'] . ' OR `id_web` = 0)';
				}
			}
		}

		$this->fields['list'] = [
			'top'       => [
				'title'  => $this->l('置頂'),
				'order'  => true,
				'filter' => true,
				'values' => [
					[
						'class' => 'top',
						'value' => 1,
						'title' => $this->l('置頂'),
					],
				],
				'class'  => 'text-center',
			],
			'id_news_type' => [
				'title'  => $this->l('類別'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table' => 'news_type',
					'key'   => 'id_news_type',
					'val'   => 'news_type',
					'class' => 'class',
					'where' => $where,
					'order' => '`news_type` ASC',
				],
				'class'  => 'text-center',
			],
			'title'     => [
				'title'  => $this->l('標題'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'date_time' => [
				'title'  => $this->l('發佈日期'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'content'   => [
				'title'  => $this->l('內容'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'add_time'  => [
				'title'  => $this->l('建立時間'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'id_add'    => [
				'title'  => $this->l('建立人員'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'key'    => [
					'table' => 'admin',
					'key'   => 'id_admin',
					'val'   => 'first_name',
				],
			],
			'edit_time' => [
				'title'  => $this->l('修改時間'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'id_edit'   => [
				'title'  => $this->l('修改人員'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'key'    => [
					'table' => 'admin',
					'key'   => 'id_admin',
					'val'   => 'first_name',
				],
			],
			'active'    => [
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];


		parent::__construct();
		$this->tabAccess['add'] = 0;
		$this->tabAccess['edit'] = 0;
		$this->tabAccess['del'] = 0;
		$this->actions = array('view');
	}

	public function initToolbar()
	{        //初始化功能按鈕
	    
	}
}