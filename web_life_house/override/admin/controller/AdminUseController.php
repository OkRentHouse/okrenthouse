<?php

class AdminUseController extends AdminController
{
	public $no_link = false;

	public function __construct()
	{

		$this->className       = 'AdminUseController';
		$this->table           = 'use';
		$this->fields['index'] = 'id_use';
		$this->fields['title'] = '物件用途';
		$this->actions[]       = 'list';
		$this->fields['list']  = [
			'id_use'    => [
				'index'  => true,
				'type'   => 'checkbox',
				'select' => '',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'use'       => [
				'title'  => $this->l('物件用途'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'attribute' => [
				'title'  => $this->l('顯示寬深高'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'attribute_1',
						'value' => 1,
						'title' => $this->l('顯示'),
					],
					[
						'class' => 'attribute_0',
						'value' => 0,
						'title' => $this->l('隱藏'),
					],
				],
			],
			'position'  => [
				'title' => $this->l('位置'),
				'order' => true,
				'class' => 'text-center',
			],
		];

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('物件用途'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_use'    => [
						'name'     => 'id_use',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'use'       => [
						'name'        => 'use',
						'type'        => 'text',
						'label'       => $this->l('用途名稱'),
						'placeholder' => $this->l('請輸入用途名稱'),
						'maxlength'   => '20',
						'required'    => true,
					],
					'attribute' => [
						'name'   => 'attribute',
						'type'   => 'switch',
						'label'  => $this->l('顯示寬深高'),
						'val'    => 0,
						'values' => [
							[
								'id'    => 'position_1',
								'value' => 1,
								'label' => $this->l('顯示'),
							],
							[
								'id'    => 'position_1',
								'value' => 0,
								'label' => $this->l('隱藏'),
							],
						],
					],
					'position'  => [
						'type'  => 'number',
						'label' => $this->l('位置'),
						'min'   => 0,
						'name'  => 'position',
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}


	public function processDel()
	{
		$id_use = Tools::getValue('id_use');
		$sql    = sprintf('SELECT `id_rent_house`
									FROM `' . DB_PREFIX_ . 'rent_house`
									WHERE `id_use` = %d
									LIMIT 0, 1',
			GetSQL($id_use, 'int'));
		Db::rowSQL($sql);
		if (Db::getContext()->num()) {
			$this->_errors[] = $this->l('無法刪除正在使用的開發來源');
		}
		parent::processDel();
	}
}