<?php

class AdminDeedController extends AdminController
{
	public $tpl_folder;    //樣版資料夾

	public function __construct()
	{
		$this->className       = 'AdminDeedController';
		$this->table           = 'deed';
		$this->fields['index'] = 'id_deed';

		$this->fields['where'] .= $this->my_where_web;
		$this->fields['order'] = ' ORDER BY `deed_number` ASC';
		$this->fields['title'] = $this->l('契據管理');

		$this->fields['list'] = [
			'id_deed'          => [
				'index'  => true,
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'id_web'           => [
				'title'  => $this->l('加盟店'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table' => 'web',
					'key'   => 'id_web',
					'val'   => 'web',
					'where' => $this->my_where_web,
					'order' => '`web` ASC',
				],
				'class'  => 'text-center',
			],
			'id_deed_category' => [
				'title'  => $this->l('契據類別'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table'       => 'deed_category',
					'key'         => 'id_deed_category',
					'text'        => ['deed_category', 'category_name'],
					'text_divide' => '%s - %s',
					'val'         =>  ['deed_category', 'category_name'],
					'order'       => '`category_name` ASC',
				],
				'class'  => 'text-center',
			],
			'deed_number'      => [
				'title'  => $this->l('契據編號'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'receipt_date'     => [
				'title'  => $this->l('領用日期'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'id_recipient'     => [
				'title'  => $this->l('領用人'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'use_date'         => [
				'title'  => $this->l('使用日期'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'id_user'          => [
				'title'  => $this->l('使用人'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'status'           => [
				'title'  => $this->l('使用狀況'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'status_0',
						'value' => 0,
						'title' => $this->l('未領取'),
					],
					[
						'class' => 'status_1',
						'value' => 1,
						'title' => $this->l('領用'),
					],
					[
						'class' => 'status_2',
						'value' => 2,
						'title' => $this->l('使用'),
					],
				],
			],
		];

		$this->fields['list_num'] = 50;

		$where = $this->my_where_web;
		if (empty($this->my_where_web)) {
			$where = ' AND FALSE';
		}
		$this->fields['form']['deed'] = [
			'tab'    => 'deed',
			'legend' => [
				'title' => $this->l('契據管理'),
				'icon'  => 'icon-cogs',
				'image' => '',
			],
			'input'  => [
				'id_deed'           => [
					'name'     => 'id_deed',
					'type'     => 'hidden',
					'index'    => true,
					'required' => true,
				],
				'id_web'            => [
					'name'    => 'id_web',
					'label'   => $this->l('加盟店'),
					'type'    => 'select',
					'options' => [
						'default' => [
							'text' => '請選擇加盟店',
							'val'  => '',
						],
						'table'   => 'web',
						'text'    => 'web',
						'where'   => $this->my_where_web,
						'value'   => 'id_web',
						'order'   => '`web` ASC',
					],
				],
				'id_deed_category'  => [
					'name'     => 'id_deed_category',
					'type'     => 'select',
					'label'    => $this->l('契據類別'),
					'options'  => [
						'default'     => [
							'text' => '請選擇契據類別',
							'val'  => '',
						],
						'table'       => 'deed_category',
						'text'        => ['deed_category', 'category_name'],
						'text_divide' => '%s - %s',
						'where'       => $this->my_where_web,
						'value'       => 'id_deed_category',
						'order'       => '`deed_category` ASC',
					],
					'suffix_btn'       => [
						[
							'title' => $this->l('查看編號'),
						],
					],
					'required' => true,
				],
				'deed_number'       => [
					'name'       => 'deed_number',
					'label'      => $this->l('契據編號'),
					'type'       => 'text',
					'maxlength'  => '20',
					'p'          => $this->l('只輸入數字'),
					'required'   => true,
				],
				'deed_number_start' => [
					'name'             => 'deed_number_start',
					'label'            => $this->l('契據編號起始'),
					'type'             => 'text',
					'maxlength'        => '20',
					'no_action'        => true,
					'required'         => true,
					'prefix'           => $this->l('起始'),
				],
				'deed_number_end'   => [
					'name'             => 'deed_number_end',
					'label'            => $this->l('契據編號結束'),
					'type'             => 'text',
					'maxlength'        => '20',
					'no_action'        => true,
					'required'         => true,
					'prefix'           => $this->l('結束'),
					'p'                => $this->l('只輸入數字'),
					'suffix_btn'       => [
						[
							'title' => $this->l('同上'),
						],
					],
				],
				'receipt_date'      => [
					'name'  => 'receipt_date',
					'label' => $this->l('領用日期'),
					'type'  => 'date',
				],
				'id_recipient'      => [
					'name'    => 'id_recipient',
					'label'   => $this->l('領用人'),
					'type'    => 'select',
					'options' => [
						'default'     => [
							'text' => '請選擇領用人',
							'val'  => '',
						],
						'table'       => 'admin',
						'text'        => ['email', 'last_name', 'first_name'],
						'text_divide' => '%s - %s%s',
						'where'       => $where,
						'value'       => 'id_admin',
						'order'       => '`email` ASC',
					],
				],
				'use_date'          => [
					'name'  => 'use_date',
					'label' => $this->l('使用日期'),
					'type'  => 'date',
				],
				'id_user'           => [
					'name'    => 'id_user',
					'label'   => $this->l('使用人'),
					'type'    => 'select',
					'options' => [
						'default'     => [
							'text' => '請選擇使用人',
							'val'  => '',
						],
						'table'       => 'admin',
						'text'        => ['email', 'last_name', 'first_name'],
						'text_divide' => '%s - %s%s',
						'where'       => $where,
						'value'       => 'id_admin',
						'order'       => '`email` ASC',
					],
				],
				[
					'name'     => 'status',
					'type'     => 'switch',
					'label'    => $this->l('使用狀況'),
					'val'      => 0,
					'values'   => [
						[
							'id'    => 'status_0',
							'value' => 0,
							'label' => $this->l('未領取'),
						],
						[
							'id'    => 'status_1',
							'value' => 1,
							'label' => $this->l('領用'),
						],
						[
							'id'    => 'status_2',
							'value' => 2,
							'label' => $this->l('使用'),
						],
					],
					'required' => true,
				],
			],
			'submit' => [
				[
					'title' => $this->l('儲存'),
				],
			],
			'cancel' => [
				'title' => $this->l('取消'),
			],
			'reset'  => [
				'title' => $this->l('重置'),
			],
		];

		parent::__construct();
	}

	public function initProcess()
	{
		parent::initProcess();
		if($this->display == 'add'){
			unset($this->fields['form']['deed']['input']['deed_number']);
		}else{
			unset($this->fields['form']['deed']['input']['deed_number_start']);
			unset($this->fields['form']['deed']['input']['deed_number_end']);
		}
	}

	public function postProcess()
	{
		$deed_number = Tools::getValue('deed_number');

		if (!empty($deed_number)) {
			$this->fields['form']['deed']['input']['deed_number_start']['required'] = false;
			$this->fields['form']['deed']['input']['deed_number_end']['required'] = false;
		}else{
			$this->fields['form']['deed']['input']['deed_number']['required'] = false;
		}
		return parent::postProcess();
	}

	public function validateRules()
	{
		$receipt_date = Tools::getValue('receipt_date');
		$id_recipient = Tools::getValue('id_recipient');
		$use_date = Tools::getValue('use_date');
		$id_user = Tools::getValue('id_user');
		if(!empty($use_date) || !empty($id_user)){
			if(empty($use_date)){
				$this->_errors[] = $this->l('有填寫 使用人 時, 使用日期 必須填寫');
				$this->_errors_name[] = 'use_date';
			}
			if(empty($id_user)){
				$this->_errors[] = $this->l('有填寫 使用日期 時, 使用人 必須填寫');
				$this->_errors_name[] = 'id_user';
			}
		}
		if(!empty($receipt_date) || !empty($id_recipient)){
			if(empty($receipt_date)){
				$this->_errors[] = $this->l('有填寫 領用人 時, 領用日期 必須填寫');
				$this->_errors_name[] = 'receipt_date';
			}
			if(empty($id_recipient)){
				$this->_errors[] = $this->l('有填寫 領用日期 時, 領用人 必須填寫');
				$this->_errors_name[] = 'id_recipient';
			}
		}
		return parent::validateRules();
	}

	public function processAdd(){
		$this->validateRules();
		if(count($this->_errors)) return;
		$this->setFormTable();
		$num = $this->FormTable->insertDB();
		if($num){
			if(Tools::isSubmit('submitAdd'.$this->table)) {
				$this->back_url = self::$currentIndex.'&conf=1';
			}
			if(Tools::isSubmit('submitAdd'.$this->table.'AndStay')) {
				if(in_array('edit', $this->post_processing)){
					$this->back_url = self::$currentIndex.'&view'.$this->table.'&conf=1&'.$this->index.'='.$this->FormTable->index_id;
				}else{
					$this->back_url = self::$currentIndex.'&edit'.$this->table.'&conf=1&'.$this->index.'='.$this->FormTable->index_id;
				}
			}
			if(Tools::isSubmit('submitAdd'.$this->table.'AndContinue')) {
				$this->back_url = self::$currentIndex.'&add'.$this->table.'&conf=1';
			}
			Tools::redirectLink($this->back_url);
		}else{
			$this->_errors[] = $this->l('沒有新增任何資料!');
		}
	}

	public function processDel()
	{
		$sql    = 'SELECT `id_deed`
										FROM `' . DB_PREFIX_ . 'deed`
										WHERE `id_web` IS NOT NULL
										OR `receipt_date` IS NOT NULL
										OR `id_recipient` IS NOT NULL
										OR `use_date` IS NOT NULL 
										OR `id_user` IS NOT NULL
										OR `status` <> 0
										LIMIT 0, 1';
		Db::rowSQL($sql);
		if (Db::getContext()->num()) {
			$this->_errors[] = $this->l('無法刪除正在使用的契約');
		}
		parent::processDel();
	}
}