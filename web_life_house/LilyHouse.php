<?php

class LilyHouse{
	public $version = 1.1;		//版本
	public $type = NULL;		// module:	work:
	public $cache = true;		//啟用快取
	protected $dispatcher = NULL;	//啟用快取
	public $debug = false;
	protected static $instance;
	
	public static function getContext(){
		if (!self::$instance) {
			self::$instance = new LilyHouse();
		};
		return self::$instance;
	}
	
	public function __construct() {
		define('WEB_DIR', (dirname(dirname(__FILE__))));			//網站所在位置資料夾 (實體主機位置)
		//todo spl_autoload_register import_class PROJECT_FOLDER
		//todo spl_autoload_register import_tool
	}
	
	/**
	*	載入系統初始設定檔
	*/
	public function import_int(){
		include_once('config'.DS.'ini_db.php');
		include_once('config'.DS.'ini_setup.php');
		$this->include_cache_ini(CONFIG_DIR,'ini');			//快取載入
		$this->cache = CACHE;		//Config::get(WEB_CACHE);
	}
		
	/**
	*	載入預設modul
	*/
	public function import_modul(){
		$this->include_cache_ini(MODULE_DIR,'modul');			//快取載入
	}

	/*
	*	URL 改寫
	*/
	public function url(){
		$this->dispatcher = Dispatcher::getContext();
		$this->dispatcher->setURL();
		$this->dispatcher->verification_img();
		$this->dispatcher->barcode_img();
		$this->dispatcher->download();
		$this->dispatcher->planned_img();
		
	}
	
	/**
	*	載入工具
	*/
	public function import_tool(){
		$this->include_cache_ini(TOOL_DIR,'tool');			//快取載入
	}
	
	public function import_controllers(){
		$this->include_cache_ini(CONTROLLERS_DIR,'controllers');	//快取載入
	}
	
	/**
	*	載入Class核心
	*/
	public function import_class(){
		//todo 要改成 spl_autoload_register
		$this->include_cache_ini(CLASS_DIR,'class');			//快取載入
	}
	
	/**
	*	載入預設css
	*/
	public function import_css(){
		$this->include_cache_ini(CSS_DIR,'css','.css');			//快取載入
	}
	
	/**
	*	載入預設JavaScript
	*/
	public function import_js(){
		$this->include_cache_ini(JS_DIR,'js','.js');			//快取載入
	}
		
	/**
	*	載入資料夾目錄所有檔案
	*/
	public function include_file($dir, $file_ex = '.php'){
		// 判斷是否為目錄
		if(is_dir($dir)){
			if ($dh = opendir($dir)) {
				$arr_f = array();
				$arr_dir = array();
				while(($file = readdir($dh)) !== false) {
					//只讀過取出php 的檔案
					if ($file != '.' && $file != '..'){
						if (strpos( $file, $file_ex)){	//是檔案
							$arr_f[] = $file;
						}elseif(is_dir($dir.DS.$file)){	//是資料夾
							$arr_dir[] = $file;
						}
					}
				}
				array_multisort($arr_f);
				foreach($arr_f as $f){
					$_include = $dir.DS.$f;
					if($dir != CONTROLLERS_DIR && $file_ex == '.php'){
						include_once($_include);
					}
					
					if($file_ex == '.css'){
						$_include = '/css/'.$f;
					};
					if($file_ex == '.js'){
						$_include = '/js/'.$f;
					};
					$arr_include[str_replace($file_ex,'',$f)] = $_include;
//					$arr_file[] = str_replace($file_ex,'',$f);
				}
				
				foreach ($arr_dir as $d){
					$dir2 = $dir.DS.$d;
					$_include = $dir2.DS.$d.'.php';
					if(is_file($_include)){				//資料夾內同名檔案先引入
						$arr_include[$d.DS.$d] = $_include;
						if($dir != CONTROLLERS_DIR) include_once($_include);
					};
					
					if($dh2 = opendir($dir2)){
						$arr_f = array();
						while(($file2 = readdir($dh2)) !== false) {
							if ($file2 != '.' && $file2 != '..'){
								if (strpos($file2,'.php') && (str_replace('.php','',$file2) != $file)){
									$arr_f[] = $file2;
								}
							}
						}
						array_multisort($arr_f);
						foreach($arr_f as $f){
							$_include = $dir2.DS.$f;
							if($dir != CONTROLLERS_DIR) include_once($_include);
							$arr_include[$d.DS.str_replace('.php','',$f)] = $_include;
						}
					}
				}
				closedir($dh);
			}
		}
		return $arr_include;
	}
	
	/**
	*	載入快取
	*/
	public function include_cache_ini($dir, $file, $file_ex = '.php', $type = true){
		if($file != ''){
			if(is_file(CACHE_DIR.DS.$file.'_index.php')) $_indes = include_once(CACHE_DIR.DS.$file.'_index.php');
			if($this->cache && (count($_indes) > 0) && $type){	//有快取質接載入
				foreach($_indes as $file2){
					if($file_ex == '.php' && is_file($file2) && $file != 'modul'){	//模組不載入
						include_once($file2);
					}elseif($file_ex == '.php'){
						mkdir(CACHE_DIR, '0777', true);				//資料夾權限 改最大
						unlink(CACHE_DIR.DS.$file.'_index.php');		//刪掉檔案重新新建
						$this->include_cache_ini($dir, $file, $file_ex, $type);
					}
				}
			}else{
				$arr_include = $this->include_file($dir, $file_ex);		//載入php檔
				if(count($arr_include) > 0){

					mkdir(CACHE_DIR, '0777', true);
					$fp = fopen(CACHE_DIR.DS.$file.'_index.php', 'w');		//新增、覆蓋檔案
					fwrite($fp, "<?php return [\r\n");
					foreach($arr_include as $i => $v){
						fwrite($fp, "'".$i."' => '".$v."',\r\n");
					};
					fwrite($fp, "]; ");
					fclose($fp);
				}
			}
		}
	}
	
	/**
	*	偵錯
	*/
	public function debug($type = true){
		return false;
	}
		
	/**
	*
	*/
	public function identification($type = false){
		if($type){
			//$_SERVER['HTTP_HOST']
		};
	}
	
	public function template(){
		return 'default';
	}
	/**
	*	清除快取
	*/
	public function re_cache(){
		if(!$this->cache){
			rrmdir(_SMARTY_COMPILE_DIR);
			rrmdir(_SMARTY_CONFIG_DIR);
			rrmdir(CACHE_DIR);
			add_dir(CACHE_DIR);
			add_dir(_SMARTY_CONFIG_DIR);
			add_dir(_SMARTY_COMPILE_DIR);
			header('Cache-Control: max-age=0, must-revalidate');
   			header('Expires: Mon, 06 Jun 1985 06:06:00 GMT+1');
			//Config::updateValue(WEB_CACHE, 0);
			return true;
		}else{

		};
		//Config::updateValue(WEB_CACHE, 1);
		return false;
	}
	
	public function controller(){
		$this->dispatcher->dispatch();
	}

	public function autoload_class(){

	}

	/**
	*	進入大門
	*/
	public function run(){
		//todo
		$this->import_int();

		$this->import_tool();
		$this->import_class();
		$this->autoload_class();
//		$this->import_modul();	//todo
		$this->url();
		$this->import_css();
		$this->import_js();
		$this->re_cache();
		switch($this->type){
			case 'module':	//模組結構模式
				$this->controller();
				//新的開發中
			break;
			case 'work':	//相對資料夾模式
				$self = str_replace('LH_index.php','',$_SERVER['REDIRECT_URL']);
				if(!strpos($self,'.htm') && !strpos($self,'.html') && !strpos($self,'.php'))$self = $self.'index.php';
				if(is_file(WORK_DIR.lin_win_dir($self))) include_once(WORK_DIR.lin_win_dir($self));
				if(is_file(WEB_DIR.lin_win_dir($self))) include_once(WEB_DIR.lin_win_dir($self));
			break;
			default:
				$self = str_replace('LH_index.php','',$_SERVER['REDIRECT_URL']);
				if(!strpos($self,'.htm') && !strpos($self,'.html') && !strpos($self,'.php')) $self = $self.'index.php';
				if(is_file(WORK_DIR.lin_win_dir($self))) include_once(WORK_DIR.lin_win_dir($self));
				if(is_file(WEB_DIR.lin_win_dir($self)))
				{
					include_once(WEB_DIR.lin_win_dir($self));
				}else{
					$this->controller();
				};
			break;
		};
		if($this->type != 'module'){
			if(!is_file(WORK_DIR.lin_win_dir($self)) && !is_file(WEB_DIR.lin_win_dir($self))) include_once(WEB_DIR.DS.'404.php');
		};
		
		if($this->debug) $this->debug();
	}
}
