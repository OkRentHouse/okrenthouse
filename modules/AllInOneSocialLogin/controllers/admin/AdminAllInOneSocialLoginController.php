<?php

class AdminAllInOneSocialLoginController extends AdminModuleController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'allinone';

	public function __construct()
	{
		$this->className       = 'AdminAllInOneSocialLoginController';
		$this->fields['title'] = 'All In One 社群登入';
		$this->fields['tabs']  = [
			'facebook' => $this->l('Facebook'),
			'google'   => $this->l('Google'),
			'advanced' => $this->l('進階'),
		];
		$this->fields['form']  = [
			[
				'tab'    => 'facebook',
				'legend' => [
					'title' => $this->l('facebook'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'AllInOne_FB_app_id',
						'type'          => 'text',
						'label'         => $this->l('應用程式編號'),
						'val'           => 50,
						'configuration' => true,
					],
					[
						'name'          => 'AllInOne_FB_app_secret',
						'type'          => 'password',
						'label'         => $this->l('應用程式密鑰'),
						'configuration' => true,
					],
					[
						'name'          => 'AllInOne_FB_graph_version',
						'type'          => 'text',
						'label'         => $this->l('應用程式版本'),
						'placeholder'   => 'v3.0',
						'val'           => 'v3.0',
						'configuration' => true,
					],
					[
						'name'          => 'AllInOne_FB_active',
						'type'          => 'switch',
						'label'         => $this->l('啟用'),
						'values'        => [
							[
								'id'    => 'AllInOne_FB_active_1`',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'AllInOne_FB_active_0',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
						'val'           => 0,
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
			[
				'tab'    => 'advanced',
				'legend' => [
					'title' => $this->l('Hash'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'AllInOne_Hash',
						'type'          => 'text',
						'minlength'     => 20,
						'label'         => $this->l('Hash'),
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
			[
				'tab'    => 'advanced',
				'legend' => [
					'title' => $this->l('Android'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'AllInOne_AndroidPackageName',
						'type'          => 'text',
						'label'         => $this->l('Android Package Name'),
						'configuration' => true,
					],
					[
						'name'          => 'AllInOne_Android_active',
						'type'          => 'switch',
						'label'         => $this->l('啟用Android登入'),
						'values'        => [
							[
								'id'    => 'AllInOne_Android_active_1`',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'AllInOne_Android_active_0',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
						'val'           => 0,
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
			[
				'tab'    => 'advanced',
				'legend' => [
					'title' => $this->l('IOS'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'AllInOne_IOSPackageName',
						'type'          => 'text',
						'label'         => $this->l('IOS Package Name'),
						'configuration' => true,
					],
					[
						'name'          => 'AllInOne_IOS_active',
						'type'          => 'switch',
						'label'         => $this->l('啟用IOS登入'),
						'values'        => [
							[
								'id'    => 'AllInOne_IOS_active_1`',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'AllInOne_IOS_active_0',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
						'val'           => 0,
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'edit';
		parent::initContent();
	}
}