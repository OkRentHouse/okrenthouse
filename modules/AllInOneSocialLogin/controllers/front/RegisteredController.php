<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/7/25
 * Time: 上午 10:45
 */
class RegisteredController extends FrontController
{
	public $type;
	public $tpl_folder;	//樣版資料夾
	public $page = 'registered2';
	public $definition;
	public $_errors_name;

	public function __construct(){
		$this->tabAccess['add'] = true;
		$this->className = 'RegisteredController';
		$this->table = 'member';
		$this->fields['index'] = 'id_member';
		$this->fields['title'] = '註冊會員';

		$this->definition = array(
			array(
				'validate'	=> 'required',
				'name'	=> 'house_key',
				'val'		=> '1',
				'label'	=> '註冊代碼',
				'type'	=> 'text',
			),
			array(
				'validate'	=> 'maxlength',
				'name'	=> 'house_key',
				'val'		=> '16',
				'label'	=> '註冊代碼',
				'type'	=> 'text',
			),
			array(
				'validate'	=> 'required',
				'name'	=> 'tel',
				'val'		=> '1',
				'label'	=> '連絡電話',
				'type'	=> 'text',
			));
		$this->fields['form']['member'] =
			array(
				'tab_class' => 'ol-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1',
				'tab' => 'member',
				'legend' => array(
					'title' => $this->l('註冊會員'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					'id_member' => array(
						'name' => 'id_member',
						'type' => 'hidden',
						'index' => true,
						'required' => true),
					'id_houses' => array(
						'name' => 'id_houses',
						'type' => 'hidden'),
					'type' => array(
						'name' => 'type',
						'val' => $this->type,
						'type' => 'hidden'
					),
					'house_key' => array(
						'name' => 'house_key',
						'type' => 'text',
						'no_action' => true,
						'label' => $this->l('註冊代碼'),
						'p' => $this->l('請注意大小寫'),
						'placeholder' => $this->l('請輸入註冊代碼'),
						'maxlength' => '16',
						'required' => true
					),
					array(
						'name' => 'tel',
						'type' => 'text',
						'label' => $this->l('聯絡電話'),
						'placeholder' => $this->l('聯絡電話'),
						'maxlength' => '50',
						'required' => true
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存'))
				),
				'cancel' => array(
					'title' => $this->l('取消')),
				'reset' => array(
					'title' => $this->l('重置'))
			);
		parent::__construct();
	}

	public function setFormTable(){
		if(empty($this->FormTable)) $this->FormTable = new FormTable();
		$this->FormTable->back_url = '/loging';
		$this->FormTable->table = $this->table;
		$this->FormTable->_join = $this->_join;
		$this->FormTable->_group = $this->_group;
		$this->FormTable->_as = $this->_as;
		$this->FormTable->list_id = $this->list_id;
		$this->FormTable->actions = $this->actions;
		$this->FormTable->tabAccess = $this->tabAccess;
		$this->FormTable->fields = $this->fields;
		$this->FormTable->display = $this->display;
		$this->FormTable->submit_action = $this->submit_action;
		$this->FormTable->show_cancel_button = $this->show_cancel_button;
		$this->FormTable->bulk_actions = $this->bulk_actions;
		$this->FormTable->display_edit_div = $this->display_edit_div;
	}

	//POST處理
	public function process(){
		if(Tools::isSubmit('submitAddmember')){
			$this->display = 'add';
			$this->action = 'Save';
			$this->processAdd();
		};
	}
	
	public function initContent(){
		$this->fields['form']['member']['input']['type']['val'] = $this->type;
		parent::initContent();
		$this->display = 'add';
		$FormTable = $this->renderForm();	//啟用表單
		$this->context->smarty->assign(array(
			'back_url' => '/loging',
			'FormTable' => $FormTable
		));
		$this->setTemplate(THEME_DIR.'form_table.tpl');
	}
	
	//轉址
	public function redirect(){
		Tools::redirectLink('//'.WEB_DNS.'/member/');
	}
	
	public function processAdd(){
		$this->validateRules();
		if(count($this->_errors)) return;
		switch ($this->type){
			case 'Facebook':
				//待修改
				//取得FB資訊
				include_once(MODULE_DIR . DS . 'AllInOneSocialLogin' . DS . 'classes' . DS . 'AllInOneFacebook.php');
				$AllInOneFacebook = AllInOneFacebook::getContext();
				$fb = $AllInOneFacebook->getFB();
				$oAuth2Client = $fb->getOAuth2Client();
				$tokenMetadata = $oAuth2Client->debugToken($_SESSION['fb_access_token']);
				$data_time = $tokenMetadata->getIssuedAt();
				$arr_data_time = (array)$data_time;
				//註冊時間
				$FB_date = $arr_data_time['date'];
				//註冊時間類型
				$FB_timezone_type = $arr_data_time['timezone_type'];
				//註冊時區
				$FB_timezone = $arr_data_time['timezone'];
				//使用者ID
				$FB_user_id = $tokenMetadata->getField('user_id');
				//使用者類型
				$FB_type = $tokenMetadata->getField('type');
				
				// 取得 id 及 個人資料
				$response = $fb->get('/me?fields=id,name,first_name,last_name,email', $_SESSION['fb_access_token']);
				$FBUserInfo = $response->getGraphUser();
				
				//姓名
				$FB_name = $FBUserInfo['name'];
				//名
				$FB_first_name = $FBUserInfo['first_name'];
				//姓f
				$FB_last_name = $FBUserInfo['last_name'];
				//email
				$FB_email = $FBUserInfo['email'];
				if(empty(AllInOneFacebook::getContext()->is_registered($FB_user_id))){
					$id_houses = Cache::getContext()->get('id_houses');
					$pass = rand_code(20);
					$tel = Tools::getValue('tel');
					$sql = sprintf('INSERT INTO `'.DB_PREFIX_.'member` (`email`, `password`, `id_houses`, `name`, `tel`, `active`) VALUES (%s, %s, %d, %s, %s, 1)',
						GetSQL('FB_'.$FB_user_id, 'text'),
						GetSQL($pass, 'text'),
						GetSQL($id_houses, 'int'),
						GetSQL($FB_name, 'text'),
						GetSQL($tel, 'text'));
					Db::rowSQL($sql);
					$id_member = Db::getContext()->num();
					if($id_member){
						//id_b_c_group = 1 => 住戶
						$sql = sprintf('INSERT INTO `'.DB_PREFIX_.'member_b_c_group` (`id_member`, `id_b_c_group`) VALUES (%d, 1)', $id_member);
						Db::rowSQL($sql);
						
						$AllInOneFacebook->id_member = $id_member;
						$AllInOneFacebook->user_id = $FB_user_id;
						$AllInOneFacebook->type = $FB_type;
						$AllInOneFacebook->name = $FB_name;
						$AllInOneFacebook->date = $FB_date;
						$AllInOneFacebook->timezone_type = $FB_timezone_type;
						$AllInOneFacebook->timezone = $FB_timezone;
						$id_facebook = $AllInOneFacebook->registered();
						if($id_facebook){
							$_SESSION['id_member'] = $id_member;
							$_SESSION['id_houses'] = Cache::getContext()->get('id_houses');
							if(Tools::isSubmit('submitAdd'.$this->table)) {
								$back_url = '/';
							}
							Tools::redirectLink($back_url);
						}else{
							$this->_errors[] = $this->l('此帳號已經註冊過!');
						}
					}else{
						$this->_errors[] = $this->l('此帳號已經註冊過!');
					};
				}
				
				break;
		}
	}
	
	//表單驗證
	public function validateRules(){	//驗證
		if(Member::is_member(Tools::getValue('email'))){
			$this->_errors[] = '此email已注冊過!';
			$this->_errors_name[] = 'email';
		};
		$Number_of_registrations = GetSQL(Configuration::get('Number_of_registrations'), 'int');
		$LIMIT = max($Number_of_registrations, 1);
		$sql = sprintf('SELECT h.`id_houses`, h.`id_build_case`
				FROM `'.DB_PREFIX_.'houses` AS h
				WHERE h.`house_key` = %s
				LIMIT 0, 1',
			GetSQL(Tools::getValue('house_key'), 'text'));
		$row = Db::rowSQL($sql,true);
		if(empty($row['id_houses'])){
			$this->_errors[] = $this->l('註冊代碼錯誤 (請注意大小寫)');
			$this->_errors_name[] = 'house_key';
		}else{
			Cache::getContext()->set('id_houses', $row['id_houses']);
			Cache::getContext()->set('id_build_case', $row['id_build_case']);
			$sql = sprintf('SELECT h.`id_houses`
				FROM `'.DB_PREFIX_.'houses` AS h
				LEFT JOIN `member` AS m ON m.`id_houses` = h.`id_houses`
				GROUP BY m.`id_member`
				LIMIT 0, %d',
				GetSQL(Tools::getValue('house_key'), 'text'),
				GetSQL($LIMIT, 'int'));
			$row = Db::rowSQL($sql);
			
			if(!empty($Number_of_registrations) && $Number_of_registrations <= count($row)){
				$this->_errors[] = $this->l('註冊代碼註冊已超過上限: 最多只能註冊'.$Number_of_registrations.'筆帳號');
				$this->_errors_name[] = 'house_key';
			}
		};
		
		$_POST['active'] = 1;
		$Validate = new Validate();
		$Validate->definition = $this->definition;
		$Validate->validateRules();
		$this->_errors = array_merge($this->_errors, $Validate->_errors);
		$this->_errors_name = array_merge($this->_errors_name, $Validate->_errors_name);
		$this->_errors_name_i = array_merge($this->_errors_name_i, $Validate->_errors_name_i);
	}
}