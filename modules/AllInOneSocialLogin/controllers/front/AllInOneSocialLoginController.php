<?php
class AllInOneSocialLoginController extends FrontModuleController
{
	public $tpl_folder;      //樣版資料夾
	public $page = 'allinonesociallogin';
	public $definition;
	public $_errors_name;
	public $type;
	public $AllInOne_media_hash = null;

	public function postProcess()
	{
		if($this->ajax && Tools::getValue('active') == 'GetMediaHash'){
			$this->displayAjaxGetMediaHash();
			exit;
		}else{
			//todo 20180928改版,舊版的記得要刪除 $_SESSION['AllInOne_media_hash'] 機制 改為 $this->AllInOne_media_hash
			$key = Tools::getValue('key');
			if(!empty($_SESSION['AllInOne_media_hash']) && empty($key)) {    //todo 舊版
				$this->AllInOne_media_hash = $_SESSION['AllInOne_media_hash'];
			}elseif(!empty($key)){											//todo 新版
				$_SESSION['AllInOne_media_hash'] = null;
				$this->AllInOne_media_hash = $this->displayAjaxGetMediaHashByKey($key);
			}
			$arr_loging_type = AllInOneSocialLogin::get_loging_type();
			$AllInOne_Android_active = Configuration::get('AllInOne_Android_active');
			$AllInOne_IOS_active = Configuration::get('AllInOne_IOS_active');
			foreach ($arr_loging_type as $i => $type) {
				//註冊
				if (Tools::isSubmit('submitAddmember') && Tools::getValue('type') == $type) {
					$this->registered($type);
				} else {//登入
					switch ($type) {
						case 'Facebook':
							$FB_accessToken = '';
							$FB_active = Configuration::get('AllInOne_FB_active');
							if ($FB_active) {
								include_once(MODULE_DIR . DS . $this->module . DS . 'classes' . DS . 'AllInOneFacebook.php');
								$AllInOneFacebook = AllInOneFacebook::getContext();
								$fb = $AllInOneFacebook->getFB();
								$FB_app_id = Configuration::get('AllInOne_FB_app_id');

								//啟用Android 手機登入
								if($AllInOne_Android_active || $AllInOne_IOS_active){
									$media_type = null;
									//手機傳入接收的accessToken
									if(Tools::isSubmit('Android') && $AllInOne_Android_active) $media_type = 'Android';
									if(Tools::isSubmit('IOS') && $AllInOne_IOS_active) $media_type = 'IOS';
									$get_accessToken = Tools::getValue('accessToken');
									if(!empty($media_type)){
										$AllInOneFacebook->AllInOne_media_hash = $this->AllInOne_media_hash;
										if($AllInOneFacebook->decode($media_type)){
											$oAuth2Client = $fb->getOAuth2Client();
											$tokenMetadata = $oAuth2Client->debugToken($get_accessToken);
											$FB_user_id = $tokenMetadata->getField('user_id');
											if (count($AllInOneFacebook->is_registered($FB_user_id))) {
												$AllInOneFacebook->login($FB_user_id);
											} else {
												//自訂注冊頁面
												$this->registered($media_type);
											}
										}
										//不管有沒有登入 或 驗證失敗都跳到首頁
										Tools::redirectLink('//' . WEB_DNS);
										exit;
									}
								}
								$helper = $fb->getRedirectLoginHelper();

								try {
									$accessToken = $helper->getAccessToken();
								} catch (Facebook\Exceptions\FacebookResponseException $e) {
									//Graph 返回錯誤
									if (LilyHouse::getContext()->debug) {
										die('Graph returned an error: ' . $e->getMessage());
									}
									continue;
								} catch (Facebook\Exceptions\FacebookSDKException $e) {
									//驗證失敗或其他本地問題
									if (LilyHouse::getContext()->debug) {
										die('Facebook SDK returned an error: ' . $e->getMessage());
										exit;
									}
									continue;
								}
								if(empty((string) $accessToken)){
									continue;
								}
								$FB_accessToken = $accessToken->getValue();
								if (empty($FB_accessToken)) {
									continue;
								}
								//OAuth 2.0客戶端處理程序可幫助我們管理Access Tokens
								$oAuth2Client = $fb->getOAuth2Client();

								$tokenMetadata = $oAuth2Client->debugToken($accessToken);
								$data_time = $tokenMetadata->getIssuedAt();
								$arr_data_time = (array)$data_time;

								//註冊時間
								$FB_date = $arr_data_time['date'];
								//註冊時間類型
								$FB_timezone_type = $arr_data_time['timezone_type'];
								//註冊時區
								$FB_timezone = $arr_data_time['timezone'];
								//使用者ID
								$FB_user_id = $tokenMetadata->getField('user_id');
								//使用者類型
								$FB_type = $tokenMetadata->getField('type');

								//驗證 FB_app_id是否相符
//						if($tokenMetadata->getAppId() != $FB_app_id){
//							if(LilyHouse::getContext()->debug){
//								die($this->l('Facebook 應用程式編號不符'));
//								exit;
//							}
//							continue;
//						}
								//驗證 FB_app_id是否相符
								$tokenMetadata->validateAppId($FB_app_id);

								//果您知道此訪問令牌所屬的用戶ID，則可以在此處進行驗證
								//$tokenMetadata->validateUserId('123');
								$tokenMetadata->validateUserId($FB_user_id);
								$tokenMetadata->validateExpiration();

								//短 accessToken 轉長 accessToken
								if (!$accessToken->isLongLived()) {
									// Exchanges a short-lived access token for a long-lived one
									try {
										$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
									} catch (Facebook\Exceptions\FacebookSDKException $e) {
										if (LilyHouse::getContext()->debug) {
											echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
											exit;
										}
										continue;
									}
									$FB_accessToken = $accessToken->getValue();
									if (empty($FB_accessToken)) {
										continue;
									}
								}
								$_SESSION['fb_access_token'] = (string)$accessToken;

//						// 取得 id 及 個人資料
//						$GraphURL  = 'https://graph.facebook.com/v2.5/me?fields=name,email';
//						$GraphURL .= '&access_token='. $FB_accessToken;
//						$GraphURL .= '&format=json&method=get&pretty=0';
//
//						$ch = curl_init();
//						curl_setopt($ch, CURLOPT_URL, $GraphURL);
//						curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
//						$FBUserInfo = curl_exec($ch);
//						curl_close($ch);

								// 取得 id 及 個人資料
								$response = $fb->get('/me?fields=id,name,first_name,last_name,email', $accessToken);
								$FBUserInfo = $response->getGraphUser();

								//姓名
								$FB_name = $FBUserInfo['name'];
								//名
								$FB_first_name = $FBUserInfo['first_name'];
								//姓
								$FB_last_name = $FBUserInfo['last_name'];
								//email
								$FB_email = $FBUserInfo['email'];

								if (count($AllInOneFacebook->is_registered($FB_user_id))) {
									$AllInOneFacebook->login($FB_user_id);
								} else {
									//自訂注冊頁面
									$this->registered($type);
								}
								Tools::redirectLink('//' . WEB_DNS);
								exit;
							}
							break;
					}
				};
			}
			$_SESSION['AllInOne_media_hash'] = null;
			unset($_SESSION['AllInOne_media_hash']);
			Tools::redirectLink('/');
			exit;
		}
	}

	public function registered($type){
		$this->type = $type;
		include_once(MODULE_DIR.DS.'AllInOneSocialLogin'.DS.'controllers'.DS.'front'.DS.'RegisteredController.php');
		$Registered = new RegisteredController();
		$Registered->type = $this->type;
		$Registered->run();
		exit;
	}

	public function displayAjaxGetMediaHashByKey($key=null){
		if(!empty($key)){
			$sql = sprintf('SELECT `hash` 
				FROM `'.DB_PREFIX_.'allinone__hash`
				WHERE `key` = %s
				AND `time_limit` >= %s
				ORDER BY `id_hash` DESC
				LIMIT 0, 1',
				GetSQL($key, 'text'),
				GetSQL(now(), 'text'));
			$row = Db::rowSQL($sql, true);
			return $row['hash'];
		}
		return false;
	}

	public function displayAjaxGetMediaHash(){
		//刪除過期key
		$now = now();
		$sql = sprintf('DELETE FROM `'.DB_PREFIX_.'allinone__hash` WHERE `time_limit` < %s',
			GetSQL($now, 'text'));
		Db::rowSQL($sql);

		$time_limit = date('Y-m-d H:i:s',time()+300);
		$key = rand_code(20);
		$hash = rand_code(20);
		$sql = sprintf('INSERT INTO `'.DB_PREFIX_.'allinone__hash` (`key`, `hash`, `time_limit`) VALUES (%s, %s, %s)',
			GetSQL($key, 'text'),
			GetSQL($hash, 'text'),
			GetSQL($time_limit, 'text'));
		Db::rowSQL($sql);
		$json = new JSON();
		$arr = array(
			'key' => $key,
			'hash' => $hash
		);
		$str = $json->encode($arr);
		die($str);
	}
}