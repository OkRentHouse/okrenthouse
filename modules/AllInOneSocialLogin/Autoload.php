<?php
spl_autoload_register(function ($class) {
	$prefix = 'AllInOneSocialLogin\\';

	$baseDir = MODULE_DIR. DS. 'AllInOneSocialLogin' .DS;
	
	//比對字串
	$len = strlen($prefix);
	//有一樣不一樣就跳出
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	
	$relativeClass = substr($class, $len);
	$file = rtrim($baseDir, DS) . DS . str_replace('\\', DS, $relativeClass) . '.php';
	
	if (file_exists($file)) {
		require_once($file);
	}
});