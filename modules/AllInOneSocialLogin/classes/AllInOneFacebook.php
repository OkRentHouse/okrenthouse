<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/7/24
 * Time: 下午 04:53
 */
class AllInOneFacebook
{
	protected static $instance;
	protected static $instance_fb = null;
	public $id_member;
	public $user_id;
	public $type;
	public $name;
	public $date;
	public $timezone_type;
	public $timezone;
	public $FB_app_id;
	public $FB_app_secret;
	public $FB_graph_version;
	public $AllInOne_media_hash;

	/**
	 * @return AllInOneFacebook
	 */
	public static function getContext(){
		if (!self::$instance) {
			self::$instance = new AllInOneFacebook();
		};
		return self::$instance;
	}

	public function __construct(){
		$this->FB_app_id = Configuration::get('AllInOne_FB_app_id');
		$this->FB_app_secret = Configuration::get('AllInOne_FB_app_secret');
		$this->FB_graph_version = Configuration::get('AllInOne_FB_graph_version');
		$this->FB_graph_version = ($this->FB_graph_version) ? $this->FB_graph_version : 'v3.0';
	}

	/**
	 * @return \Facebook\Facebook|null
	 * @throws \Facebook\Exceptions\FacebookSDKException
	 */
	public function getFB(){
		if (!self::$instance_fb) {
			self::$instance_fb = new Facebook\Facebook([
				'app_id' => $this->FB_app_id, // Replace {app-id} with your app id
				'app_secret' => $this->FB_app_secret,
				'default_graph_version' => $this->FB_graph_version,
			]);
		};
		return self::$instance_fb;
	}

	/**
	 * 加密
	 */
	public function encode(){

	}

	/**
	 * 解密
	 */
	public function decode($type){
		$get_accessToken = Tools::getValue('accessToken');
		$hash = Tools::getValue('hash');
		$FB_app_id = Configuration::get('AllInOne_FB_app_id');
		$AllInOne_Hash = Configuration::get('AllInOne_Hash');
		if(empty($AllInOne_Hash)) return false;
		switch ($type){
			case 'Android':
				if(Cache::get('AllInOne_Android_active') === null) Cache::set('AllInOne_Android_active', Configuration::get('AllInOne_Android_active'));
				if(Cache::get('AllInOne_Android_active') && Tools::isSubmit('Android')){
					if($hash == md5(sha1($FB_app_id.$get_accessToken.Configuration::get('AllInOne_AndroidPackageName').$this->AllInOne_media_hash.$AllInOne_Hash))){
						$_SESSION['fb_access_token'] = $get_accessToken;
						return true;
					}
					return false;
				}
				break;
			case 'IOS':
				if(Cache::get('AllInOne_IOS_active') === null) Cache::set('AllInOne_IOS_active', Configuration::get('AllInOne_IOS_active'));
				if(Configuration::get('AllInOne_IOS_active') && Tools::isSubmit('IOS')){
					if($hash == md5(sha1($FB_app_id.$get_accessToken.Configuration::get('AllInOne_IOSPackageName').$this->AllInOne_media_hash.$AllInOne_Hash))){
						$_SESSION['fb_access_token'] = $get_accessToken;
						return true;
					}
					return false;
				}
				break;
			default:
				return false;
				break;
		}

	}

	/**
	 * @param $user_id
	 * @return bool|mixed|null
	 */
	public function is_registered($user_id){
		if(!empty($user_id)){
			//是否有快取
			if(empty(Cache::get('AllInOneFB'))){
				//沒有就新增
				$sql = sprintf('SELECT `id_facebook`, `id_member`, `user_id`
					FROM `'.DB_PREFIX_.'allinone__facebook`
					WHERE `user_id` = %s
					LIMIT 0, 1',
					GetSQL($user_id, 'text'));
				$row = Db::rowSQL($sql, true);
				Cache::set('AllInOneFB', $row['id_facebook']);
				return $row['id_facebook'];
			}else{
				//有就直接取出
				return Cache::get('AllInOneFB');
			}
		}
		return false;
	}

	/**
	 * @return int
	 */
	public function registered(){
		$sql = sprintf('INSERT INTO `'.DB_PREFIX_.'allinone__facebook` (`id_member`, `user_id`, `type`, `name`, `date`, `timezone_type`, `timezone`) VALUES (%d, %d, %s, %s, %s, %s, %s)',
			GetSQL($this->id_member, 'int'),
			GetSQL($this->user_id, 'int'),
			GetSQL($this->type, 'text'),
			GetSQL($this->name, 'text'),
			GetSQL($this->date, 'text'),
			GetSQL($this->timezone_type, 'text'),
			GetSQL($this->timezone, 'text'));
		Db::rowSQL($sql);
		return Db::getContext()->num();
	}

	/**
	 * @param $user_id
	 */
	public function login($user_id){
		$sql = sprintf('SELECT a_f.`id_member`, a_f.`user_id`, m.`email`, m.`password`, m.`id_houses`
					FROM `'.DB_PREFIX_.'allinone__facebook` AS a_f
					LEFT JOIN `'.DB_PREFIX_.'member` AS m ON m.`id_member` = a_f.`id_member`
					WHERE a_f.`user_id` = %s
					LIMIT 0, 1',
			GetSQL($user_id, 'text'));
		
		$member = Db::rowSQL($sql, true);
		
		$_SESSION['id_member'] = $member['id_member'];
		$_SESSION['id_houses'] = $member['id_houses'];
		$MemberAutoLoginCookieDate = Configuration::get('MemberAutoLoginCookieDate');
		if($MemberAutoLoginCookieDate > 0){		//記住我
			$cookie_hash = sha1($member['id_member'].$member['email'].$_SERVER['HTTP_USER_AGENT'].date().$member['password']);
			$sql = 'REPLACE INTO `mamber_auto_login` (`id_member`, `effective_time`, `cookie`) VALUES ('.$member['id_member'].', NOW() + INTERVAL '.$MemberAutoLoginCookieDate.' DAY, "'.$cookie_hash.'");';
			$DB = new db_mysql($sql);
			$cookie_time = (3600 * 24 * (int)$MemberAutoLoginCookieDate);
			setcookie('ma_login', '1', time() + $cookie_time, '/');
			setcookie('ma_cookie', $cookie_hash, time() + $cookie_time, '/');
		};
		
	}
}