<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/7/23
 * Time: 下午 05:07
 */

class AllInOneSocialLogin extends Module
{
	public function hookdisplayLoging(){
		$arr_loging_type = AllInOneSocialLogin::get_loging_type();
		$_SESSION['AllInOne_media_hash'] = rand_code();	//隨機亂數 驗證app傳輸用
		foreach($arr_loging_type as $i => $type){
			switch ($type){
				case 'Facebook':
					include_once(MODULE_DIR . DS . 'AllInOneSocialLogin' . DS . 'classes' . DS . 'AllInOneFacebook.php');
					$fb = AllInOneFacebook::getContext()->getFB();
					$helper = $fb->getRedirectLoginHelper();
					
					$permissions = ['email']; // Optional permissions
					$fb_url = Tools::usingSecureMode() ? 'https://' : 'http://';
					$fb_url .= WEB_DNS.'/m/AllInOneSocialLogin';
					$loginUrl = $helper->getLoginUrl($fb_url , $permissions);

					$fb_url = htmlspecialchars($loginUrl);
					
					$this->context = Context::getContext();
					$this->context->smarty->assign('fb_url', $fb_url);
					break;
			}
		}
		if(count($arr_loging_type)){
			$this->context->smarty->assign(array(
				'AllInOne_Android_active' => Configuration::get('AllInOne_Android_active'),
				'AllInOne_IOS_active' => Configuration::get('AllInOne_IOS_active'),
				'AllInOne_media_hash' => $_SESSION['AllInOne_media_hash'],
			));
			return $this->context->smarty->fetch(dirname(__FILE__).DS.'loging.tpl');
		}else{
			return '';
		}
	}
	
	public static function get_loging_type(){
		if(empty(Cache::get('AllInOne_loging_type'))){
			$arr_loging_type = array();
			if(Configuration::get('AllInOne_FB_active')){
				$arr_loging_type[] = 'Facebook';
			};
			Cache::set('AllInOne_loging_type', $arr_loging_type);
			return $arr_loging_type;
		}else{
			//有就直接取出
			return Cache::get('AllInOne_loging_type');
		}
	}

	public function hookLoging_setMedia(){
		$this->context = Context::getContext();
		$this->context->controller->addCSS($this->url.'css/login.css');
	}

}