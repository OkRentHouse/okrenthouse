$(document).ready(function(e) {
	$('.facebook.button', '.all_in_one_social_login').click(function(){
		var event = event || window.event;
		if(typeof(Android) != 'undefined'){
			if(event.preventDefault) event.preventDefault();
			if(event.returnValue) event.returnValue = false;
			Android.FB_loging(media_hash);
			return false;
		};
		if(typeof(window.webkit) != 'undefined'){
			if(event.preventDefault) event.preventDefault();
			if(event.returnValue) event.returnValue = false;
			window.webkit.messageHandlers.FB_loging.postMessage(media_hash);
			return false;
		};
		return true;
	});
});