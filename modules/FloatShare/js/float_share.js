var share_infor_box_height=0;

function share_infor_box_close() {
    $(".share_infor_box .l li").hide(300, function() {
        $(".share_infor_box .r > div").hide(300);
        $(".share_infor_box_bk").animate({opacity: 0},500,function(){ $(".share_infor_box_bk").hide() });
        $(".share_infor_box").hide(300, function() {
          $(".share_infor_box").css("height",share_infor_box_height);
        })
    });
}

$(document).ready(function() {

    $('.fixed_postion a.MAIL').attr('href', 'mailto:?subject=' + EncodedUTF8ToBig5(encodeURIComponent($('title', 'head').html())) + '&body=' + EncodedUTF8ToBig5(encodeURIComponent('分享給您')) + '%0d%0a' + url());

    $('.share_infor_box .l a.MAIL').attr('href', 'mailto:?subject=' + EncodedUTF8ToBig5(encodeURIComponent($('title', 'head').html())) + '&body=' + EncodedUTF8ToBig5(encodeURIComponent('分享給您')) + '%0d%0a' + url());

    $(".menu_right a.icon_block i.fas.fa-share-alt,.menu_right a.icon_block label,.top_R img.share,a.icon_block.share_txt").click(function() {
        if ($(".share_infor_box").css("display") == "none") {
            $(".share_infor_box_bk").show(function() { $(".share_infor_box_bk").animate({opacity: 1},500,function(){
            $(".share_infor_box").show(300, function() {
                $(".share_infor_box .l li").css({"width": "0%"});
                $(this).find(".l li").show(300,function(){
                  $(".share_infor_box .l li").animate({"width": "16%"},{    duration: 500,    specialEasing: {      width: "easeOutElastic"    },complete: function(){}});
                });

                $(this).css({ display: "block" });
                $(this).find(".r span.default").show(200);

                share_infor_box_height=$(".share_infor_box").css("height");



            });}) });

        } else {

            $(".share_infor_box_bk").animate({opacity: 0},500,function(){ $(".share_infor_box_bk").hide() });
            $(".share_infor_box .l li").hide(300, function() {
                $(".share_infor_box").hide(300)
            });
        }

    });

    var now_datatype = '';
    $(".share_infor_box .l").find("[data-type='app']").click(function() {
        infor_box_r('app');
    }).mouseover(function() { infor_box_alt_show('app') });

    $(".share_infor_box .l").find("[data-type='call_us']").click(function() {
        infor_box_r('call_us');
    }).mouseover(function() { infor_box_alt_show('call_us') });

    $(".share_infor_box .l").find("[data-type='qr_code']").click(function() {
        infor_box_r('qr_code');
    }).mouseover(function() { infor_box_alt_show('qr_code') });

    $(".share_infor_box .l").find(".FB").click(function() {
        share('FB');
    }).mouseover(function() { infor_box_alt_show('FB') });

    $(".share_infor_box .l").find(".LINE").click(function() {
        share('LINE');
    }).mouseover(function() { infor_box_alt_show('LINE') });

    $(".share_infor_box .l").find(".MAIL").mouseover(function() { infor_box_alt_show('MAIL') });

    function infor_box_alt_show(datatype) {

        $(".share_infor_box span.alt").hide();
        $(".share_infor_box span.alt." + datatype).stop(true, false);
        $(".share_infor_box span.alt." + datatype).css({ opacity: 1 });
        $(".share_infor_box span.alt." + datatype).show(200, function() { $(this).animate({ opacity: 0 }, 2000) });
        //$(".share_infor_box span.alt." + datatype).hide(5000);
    }


    function infor_box_r(datatype) {

        if (now_datatype != datatype) {
            //$(".share_infor_box .r").html("<span class='default' style='display:none'> 請使用左邊功能按鈕 </span>");
            //$(".share_infor_box .r span.default").show(200);
        }

        if (now_datatype != datatype || $(".share_infor_box .r > div").css("display") == "none" || $(".share_infor_box .r > div").css("display") == undefined) {

            $(".share_infor_box .r").html("<div>" +
                $(".side_content_wrap." + datatype + " div:eq(1)").html() + "</div>");

            $(".share_infor_box .r > div").hide();
            $(".share_infor_box .r > div").addClass(datatype);
            $(".share_infor_box .r > div").show(200, function() {});
            $(".share_infor_box").animate({height: 630},{duration: 500,specialEasing: {width: "easeOutElastic"},complete: function(){}});


        } else {

            $(".share_infor_box .r > div").hide(100);
            //$(".share_infor_box .r").html("<span class='default' style='display:none'> 請使用左邊功能按鈕 </span>");
        }

        now_datatype = datatype;

    };

    $('.sevice_item').hide();

    $('.sevice_wrap_show').show();

    $(".sevice_wrap_show").click(function() {

        $(".fixed_postion .sevice_wrap_hidden i").show();
        $('.sevice_item').show(300);

        $('.sevice_wrap_show').hide();

        $(".fixed_postion .icon_block i").addClass("sevice_wrap_hidden_i_trans");

    });

    $(".sevice_wrap_hidden").click(function() {

        $('.sevice_item').hide(300);
        $(".fixed_postion .sevice_wrap_hidden i").hide();


        $('.sevice_wrap_show').show();

        $(".fixed_postion .icon_block i").removeClass("sevice_wrap_hidden_i_trans");

    });



}).on('click', '.fixed_postion .open', function() {

    var type = $(this).data('type');

    if ($('.side_content_wrap.active.' + type, '.fixed_postion').length > 0) {

        $('.side_content_wrap.active.' + type, '.fixed_postion').removeClass('active');

    } else {

        $('.side_content_wrap', '.fixed_postion').removeClass('active');

        switch (type) {

            case 'app':

                break;

            case 'qr_code':

                break;

        }

        $('.side_content_wrap.' + type, '.fixed_postion').addClass('active');

    };



}).on('click', '.fixed_postion .side_content_wrap .close', function() {

    $(this).parents('.side_content_wrap').removeClass('active');

}).on('click', '.fixed_postion .FB', function() {

    share('FB');

}).on('click', '.fixed_postion .LINE', function() {

    share('LINE');

}).on('click', '.fa-times-circle', function() {

    share_infor_box_close();

});



function share(type) {

    var url_encode = encodeURIComponent(url());

    switch (type) {

        case 'FB':

            window.open('http://www.facebook.com/share.php?u=' + url_encode, '發佈到 Facebook', config = 'height=870,width=750');

            break;

        case 'LINE':

            window.open('https://social-plugins.line.me/lineit/share?url=' + url_encode + '&text=' + url_encode, 'LINE Share', config = 'height=870,width=750');

            break;

    }

};

//

// $(document).ready(function(){

// 	$('.fixed_postion a.MAIL').attr('href', 'mailto:?subject='+EncodedUTF8ToBig5(encodeURIComponent($('title', 'head').html()))+'&body='+EncodedUTF8ToBig5(encodeURIComponent('分享給您'))+'%0d%0a'+url());

// }).on('click', '.fixed_postion .open', function () {

// 	var type = $(this).data('type');

// 	console.log(type);

// 	if ($('.side_content_wrap.active.' + type, '.fixed_postion').length > 0) {

// 		$('.side_content_wrap.active.' + type, '.fixed_postion').removeClass('active');

// 	} else {

// 		$('.side_content_wrap', '.fixed_postion').removeClass('active');

// 		switch (type) {

// 			case 'app':

// 				break;

// 			case 'qr_code':

// 				break;

// 		}

// 		$('.side_content_wrap.' + type, '.fixed_postion').addClass('active');

// 	}

// 	;

// }).on('click', '.fixed_postion .side_content_wrap .close', function () {

// 	$(this).parents('.side_content_wrap').removeClass('active');

// }).on('click', '.fixed_postion .FB', function () {

// 	share('FB');

// }).on('click', '.fixed_postion .LINE', function () {

// 	share('LINE');

// });

//

// function share(type) {

// 	var url_encode = encodeURIComponent(url());

// 	switch (type) {

// 		case 'FB':

// 			window.open('http://www.facebook.com/share.php?u='+url_encode, '發佈到 Facebook', config = 'height=870,width=750');

// 			break;

// 		case 'LINE':

// 			window.open('https://social-plugins.line.me/lineit/share?url='+url_encode+'&text='+url_encode, 'LINE Share', config = 'height=870,width=750');

// 			break;

// 	}

// };

//
