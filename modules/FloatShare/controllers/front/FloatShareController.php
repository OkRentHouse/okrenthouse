<?php
require_once(TOOL_DIR.DS.'phpqrcode'.DS.'phpqrcode.php');
class FloatShareController extends FrontModuleController
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{
		if(Tools::isSubmit('qe_code')){
			$this->getQRCode();
		}else{
			Dispatcher::getContext()->page_not_found();
		}
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new FloatShareController();
		};
		return self::$instance;
	}

	public function getQRCode(){
		$qe_code = Tools::getValue('qe_code');
		$get = '';
		foreach($_GET as $key => $v){
			if($key != 'qe_code' && (strpos($key, 'FloatShare') === false)){
				$get .= '&'.$key.'='.$v;
			}
		}

		$value = $_SERVER['HTTP_HOST'].'/'.$qe_code.'?'.$get;
		$filename = false;  //  有輸入檔名:會生成圖檔, false:不生成圖檔直接印出
		$errorCorrectionLevel = 'L'; // 糾錯級別：L、M、Q、H
		$matrixPointSize = '6'; //生成圖片大小 ：1到10

		QRcode::png($value, $filename, $errorCorrectionLevel, $matrixPointSize,2);
		exit();
	}

}