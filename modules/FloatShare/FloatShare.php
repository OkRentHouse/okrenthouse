<?php


class FloatShare
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new FloatShare();
		};
		return self::$instance;
	}
}