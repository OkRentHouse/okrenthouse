var help_book_ini = {
	//網站介面說明
	'id_1' : {
		'parent' : '',						//父
		'url' : '',						//網址
		'confirm' : '',					//前網頁面確認訊息
		'tabAccess' : {'' : ['view', 'add', 'edit', 'del']},	//權限
		'title' : '網站介面說明',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'.open_help_book',
					'.minbar',
					'a[href="?logout"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.open_help_book',
						'class' : '',
						'fixed' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '開啟說明書按鈕'	//內容１HTML內容
					},
					{
						'dom' : 'a[href="?logout"]',
						'class' : '',
						'fixed' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '登出按鈕'	//內容１HTML內容
					}
				],

			}
		],
		'explanation' : ''	//說明
	},
	//個人資訊
	'id_1_1' : {
		'parent' : 'id_1',						//父
		'url' : '',						//網址
		'confirm' : '',					//前網頁面確認訊息
		'tabAccess' : {'' : ['view', 'add', 'edit', 'del']},	//權限
		'title' : '個人資訊',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'.minbar a[href^="/manage/Management"]',
					'.minbar'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.minbar a[href^="/manage/Management"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '點選可以觀看、修改個人資訊'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//主選單
	'id_1_2' : {
		'parent' : 'id_1',					//父
		'url' : '',			//網址
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'' : ['view', 'add', 'edit', 'del']},	//權限
		'title' : '主選單',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '主選單'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//主要顯示內容
	'id_1_3' : {
		'parent' : 'id_1',					//父
		'url' : '',			//網址
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'' : ['view', 'add', 'edit', 'del']},	//權限
		'title' : '主要顯示內容',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#content'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#content',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '主要顯示內容'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},

	//建案資料
	'id_2' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['view']},	//權限
		'title' : '建案資料',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu li.BuildCase'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu li.BuildCase',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '前往建案資料'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選需要顯示的欄位
	'id_2_0' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['view']},	//權限
		'title' : '篩選需要顯示的欄位',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'div.field_list',
					'div.field_list input[name="all_field_name_val[]"]',
					'div.field_list input[name="field_name_val[]"]',
					'div.field_list button[type="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'div.field_list',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 點擊按鈕開啟要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active input[name="field_name_val[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 送出選取的資料刷新頁面'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_2_1' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'get' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					// '#admin_menu li.BuildCase',
					'#content.BuildCase tr.search th>*',
					'a.media_search',
					'#top_bar_button a.search'
				],	//發亮物件
				'content' : [				//內容
					//內容
					// {
					// 	'dom' : '#admin_menu li.BuildCase',
					// 	'url' : '/manage/BuildCase',
					// 	'class' : '',
					// 	'place' : 'top',	//內容１位置
					// 	'html' : '先點選建案資料',	//內容１HTML內容
					// 	'page' : 'id_2-2'
					// },
					{
						'dom' : 'a.media_search',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(0) 手機模式需先點選搜尋'	//內容１HTML內容
					},
					{
						'dom' : '#content.BuildCase tr.search th>*',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 先在欄位上篩選搜尋的資料'	//內容１HTML內容
					},
					{
						'dom' : '#content.BuildCase tr.search th.edit_div button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下搜尋就可以篩選出搜尋的資料'	//內容１HTML內容
					},
					{
						'dom' : '#top_bar_button a.search',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下搜尋就可以篩選出搜尋的資料'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增建案
	'id_2_2' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					// '#admin_menu li.BuildCase',
					'#content.BuildCase #top_bar_button a.new',
				],	//發亮物件
				'content' : [				//內容
					// {
					// 	'dom' : '#admin_menu li.BuildCase',
					// 	'class' : '',
					// 	'place' : 'top',	//內容１位置
					// 	'html' : '先點選建案資料',	//內容１HTML內容
					// 	'page' : 'id_2_2',
					// },
					{
						'dom' : '#content.BuildCase #top_bar_button a.new',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選"新增"按鈕',	//內容１HTML內容
						'page' : ''
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (頁簽介紹)
	'id_2_2_1' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'tab' : '',
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (頁簽介紹)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.list-group',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.list-group',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '編輯頁簽，選擇修改的建案內容',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (基本資料)
	'id_2_2_2' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_basic_information',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (基本資料)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_basic_information .panel-default',
					'li#tabs_basic_information',
					'.list-group'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_basic_information .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '基本資料區',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'li#tabs_basic_information',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '基本資料',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (房坪統計)
	'id_2_2_3' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_statistics',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (房坪統計)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_statistics .panel-default',
					'li#tabs_statistics',
					'.list-group'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_statistics .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '房坪統計',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'li#tabs_statistics',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '房坪統計',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (產品配比)
	'id_2_2_4' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_proportion',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (產品配比)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_proportion .panel-default',
					'li#tabs_proportion',
					'.list-group'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_proportion .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '產品配比',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'li#tabs_proportion',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '產品配比',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (規劃分析)
	'id_2_2_5' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_analysis',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (規劃分析)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_analysis .panel-default',
					'li#tabs_analysis',
					'.list-group'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_analysis .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '規劃分析',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'li#tabs_analysis',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '規劃分析',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (檔案附件)
	'id_2_2_6' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_file_type',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (檔案附件)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_file_type .panel-default',
					'li#tabs_file_type',
					'.list-group'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_file_type .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '檔案附件<br>若要上傳檔案必須先建立好建案資料後<br>再進入建案資料編輯上傳檔案',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'li#tabs_file_type',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '檔案附件',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (備註)
	'id_2_2_7' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_remarks',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (備註)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_remarks .panel-default',
					'li#tabs_remarks',
					'.list-group'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_remarks .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '備註',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'li#tabs_remarks',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '備註',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (儲存)'
	'id_2_2_8' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_basic_information',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (取消)
	'id_2_2_9' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_basic_information',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/BuildCase?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/BuildCase?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案 (復原、清空)
	'id_2_2_10' : {
		'parent' : 'id_2_2',					//父
		'url' : '/manage/BuildCase?&addbuild_case',			//網址
		'tab' : 'tabs_basic_information',
		'confirm' : '前往新增建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '新增建案 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料<br>(全部清空空白)',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案
	'id_2_3' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'tab' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '修改建案',				//標題
		'step' : [						//步驟
			{	'shine' : [
					// 'table td.pointer',
					'td.edit_div a.view'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.view',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '若有修改權限使用者<br>點選進入檢視後編輯',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案 (點選修改)
	'id_2_3_1' : {
		'parent' : 'id_2_3',					//父
		'url' : '/manage/BuildCase?&viewbuild_case&id_build_case=',			//網址
		'tab' : '',
		'get_id' : 'id_build_case',
		'confirm' : '前往檢視建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '修改建案 (點選修改)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.edit'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.edit',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '若有修改權限使用者<br>點選進入檢視後編輯',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案 (點選修改)
	'id_2_3_2' : {
		'parent' : 'id_2_3',					//父
		'url' : '/manage/BuildCase?editbuild_case&id_build_case=',			//網址
		'tab' : '',
		'get_id' : 'id_build_case',
		'confirm' : '前往修改建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '修改建案 (點選修改)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'div.content'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'div.content',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '在主頁面進行編輯動作',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案 (儲存)
	'id_2_3_3' : {
		'parent' : 'id_2_3',					//父
		'url' : '/manage/BuildCase?editbuild_case&id_build_case=',			//網址
		'tab' : '',
		'get_id' : 'id_build_case',
		'confirm' : '前往修改建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '修改建案 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案 (取消)
	'id_2_3_4' : {
		'parent' : 'id_2_3',					//父
		'url' : '/manage/BuildCase?editbuild_case&id_build_case=',			//網址
		'tab' : '',
		'get_id' : 'id_build_case',
		'confirm' : '前往修改建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '修改建案 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/BuildCase?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/BuildCase?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案 (復原、清空)
	'id_2_3_5' : {
		'parent' : 'id_2_3',					//父
		'url' : '/manage/BuildCase?editbuild_case&id_build_case=',			//網址
		'tab' : '',
		'get_id' : 'id_build_case',
		'confirm' : '前往修改建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '新增建案 (復原)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//刪除建案
	'id_2_4' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['del']},	//權限
		'title' : '刪除建案',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.del'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.del',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選刪除按鈕將會刪除該筆資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯入EXCEL資料
	'id_2_5' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '匯入EXCEL資料',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a[href="/manage/BuildCase?&excelimportbuild_case"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a[href="/manage/BuildCase?&excelimportbuild_case"]',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選匯入EXCEL進入匯入頁面',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯入EXCEL資料 (選擇匯入的縣市、區域)
	'id_2_5_1' : {
		'parent' : 'id_2_5',					//父
		'url' : '/manage/BuildCase?&excelimportbuild_case',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料匯入EXCEL頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '匯入EXCEL資料 (選擇匯入的縣市、區域)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.panel-body #county',
					'.panel-body #district'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.panel-body #county',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '選擇要匯入的縣市<br>(一定要選擇，不然無法匯入)',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-body #district',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '選擇要匯入的區域<br>(一定要選擇，不然無法匯入)',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯入EXCEL資料 (下載空白擋)
	'id_2_5_2' : {
		'parent' : 'id_2_5',					//父
		'url' : '/manage/BuildCase?&excelimportbuild_case',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料匯入EXCEL頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '匯入EXCEL資料 (下載空白擋)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/BuildCase?downloadexcelimportbuild_case"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/BuildCase?downloadexcelimportbuild_case"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊下載EXCEL空白檔',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'a[href="/manage/BuildCase?downloadexcelimportbuild_case"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '下載完檔案後<br>將要匯入網站的建案檔案依序填入EXCEL欄位裡',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯入EXCEL資料 (下載空白擋)
	'id_2_5_3' : {
		'parent' : 'id_2_5',					//父
		'url' : '/manage/BuildCase?&excelimportbuild_case',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料匯入EXCEL頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '匯入EXCEL資料 (下載空白擋)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'div[id!="excel_list"]>.input-group.fixed-width-lg',
					'#copy_import_htm'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#copy_import_htm',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊複製左邊的文字<br>並將要匯入的EXCEL擋案更名相同名稱',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#import_htm',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : 'EXCEL檔案如果名稱不一樣<br>將會無法匯入進去',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯入EXCEL資料 (上傳檔案)
	'id_2_5_4' : {
		'parent' : 'id_2_5',					//父
		'url' : '/manage/BuildCase?&excelimportbuild_case',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料匯入EXCEL頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '匯入EXCEL資料 (上傳檔案)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.file-input-ajax-new',
					'#excel_file'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.file-input-ajax-new',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '將要匯入的EXCEL檔案拖移進去<br>(要按下"上傳"檔案才會上傳上去)',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#excel_file',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '點及選擇要上傳的檔案<br>(要按下"上傳"檔案才會上傳上去)',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯入EXCEL資料 (檢視結果)
	'id_2_5_5' : {
		'parent' : 'id_2_5',					//父
		'url' : '/manage/BuildCase?&excelimportbuild_case',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料匯入EXCEL頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['add']},	//權限
		'title' : '匯入EXCEL資料 (檢視結果)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#excel_list>.input-group.fixed-width-lg',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#excel_list>.input-group.fixed-width-lg',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '如果上傳的檔案錯誤或欄位填寫錯誤<br>系統將會在欄位上標記錯誤<br>使用者需下載檢視錯誤問題點',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯出EXCEL資料
	'id_2_6' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['view']},	//權限
		'title' : '匯出EXCEL資料',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button .excel ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button .excel ',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選匯出EXCEL可將篩選出來的建案匯出',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//設定重劃區
	'id_2_7' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '設定重劃區',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.form_region',
					'.region_btn',
					'input[name="checkbox[]"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.region_btn',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 請先點選設定重劃區按鈕',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'input[name="checkbox[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要設定的建案',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#region_s',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 選則設定的重劃區',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.form_region  button.submit',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(4) 按下確定送出',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//清除重劃區
	'id_2_9' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '清除重劃區',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.form_region',
					'.region_btn',
					'input[name="checkbox[]"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.region_btn',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 請先點選設定重劃區按鈕',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'input[name="checkbox[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要設定的建案',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.form_region  button.clear',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(3) 按下清除',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//設定重劃區
	'id_2_8' : {
		'parent' : 'id_2',					//父
		'url' : '/manage/BuildCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往建案資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'BuildCase' : ['edit']},	//權限
		'title' : '設定區塊',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.form_block',
					'.block_btn',
					'input[name="checkbox[]"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.block_btn',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 請先點選設定區塊按鈕',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'input[name="checkbox[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要設定的建案',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#block_s',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 選則設定的區塊',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : 'form.form_block button.submit',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(4) 按下確定送出',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//推案表
	'id_3' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往推案表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'PushCase' : ['view']},	//權限
		'title' : '推案表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .PushCase',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .PushCase',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '前往推案表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_3_1' : {
		'parent' : 'id_3',					//父
		'url' : '/manage/PushCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往推案表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'PushCase' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.region_div',
					'.region_div button[name="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.region_div',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 選擇篩選條件'	//內容１HTML內容
					},
					{
						'dom' : '.region_div button[name="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 按下送出'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯出推案表
	'id_3_2' : {
		'parent' : 'id_3',					//父
		'url' : '/manage/PushCase?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往推案表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'PushCase' : ['view']},	//權限
		'title' : '匯出推案表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button .excel',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button .excel',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選匯出EXCEL按鈕<br>即可匯出推案表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//市調表
	'id_4' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'CityAdjustment' : ['view']},	//權限
		'title' : '市調表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .CityAdjustment ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .CityAdjustment ',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '前往市調表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_4_1' : {
		'parent' : 'id_4',					//父
		'url' : '/manage/CityAdjustment?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往市調表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'CityAdjustment' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.search>.col-lg-12',
					'.search>.col-lg-12 button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#district',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 選擇篩選條件'	//內容１HTML內容
					},
					{
						'dom' : '.search>.col-lg-12 button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下送出'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯出市調表
	'id_4_2' : {
		'parent' : 'id_4',					//父
		'url' : '/manage/CityAdjustment?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往市調表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'CityAdjustment' : ['view']},	//權限
		'title' : '匯出市調表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button .excel',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button .excel',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選匯出EXCEL按鈕<br>即可匯出市調表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//歷年推案分析
	'id_5' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'PushCaseAnalysis' : ['view']},	//權限
		'title' : '歷年推案分析',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .PushCaseAnalysis ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .PushCaseAnalysis ',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '前往歷年推案分析'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選歷年推案分析
	'id_5_1' : {
		'parent' : 'id_5',					//父
		'url' : '/manage/PushCaseAnalysis',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往歷年推案分析表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'PushCaseAnalysis' : ['view']},	//權限
		'title' : '篩選歷年推案分析',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.region_div',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.region_div',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 選擇篩選條件'	//內容１HTML內容
					},
					{
						'dom' : '.region_div button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 按下送出'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯出歷年推案分析
	'id_5_2' : {
		'parent' : 'id_5',					//父
		'url' : '/manage/PushCaseAnalysis',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往歷年推案分析表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'PushCaseAnalysis' : ['view']},	//權限
		'title' : '匯出歷年推案分析',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button .excel',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button .excel',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選匯出EXCEL按鈕<br>即可匯出歷年推案分析'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//下載圖表
	'id_5_3' : {
		'parent' : 'id_5',					//父
		'url' : '/manage/PushCaseAnalysis',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往歷年推案分析表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Statistical' : ['view']},	//權限
		'title' : '下載圖表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-id="chart1"]',
					'button[data-id="chart2"]',
					'button[data-id="chart3"]',
					'button[data-id="chart4"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-id="chart1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表下載]按鈕<br>即可下載[歷年推案漲幅]圖表'	//內容１HTML內容
					},
					{
						'dom' : 'button[data-id="chart2"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表下載]按鈕<br>即可下載[歷年推案數]圖表'	//內容１HTML內容
					},
					{
						'dom' : 'button[data-id="chart3"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表下載]按鈕<br>即可下載[歷年推案戶數]圖表'	//內容１HTML內容
					},
					{
						'dom' : 'button[data-id="chart4"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表下載]按鈕<br>即可下載[歷年推案總銷金額]圖表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改圖表長寬及行列數
	'id_5_4' : {
		'parent' : 'id_5',					//父
		'url' : '/manage/PushCaseAnalysis',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往歷年推案分析表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Statistical' : ['view']},	//權限
		'title' : '修改圖表長寬及行列數',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-div="edit_cart_1"]',
					'button[data-div="edit_cart_2"]',
					'button[data-div="edit_cart_3"]',
					'button[data-div="edit_cart_4"]',
					'.edit_cart_1',
					'.edit_cart_2',
					'.edit_cart_3',
					'.edit_cart_4'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-div="edit_cart_1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表編輯]按鈕<br>可以編輯[歷年推案漲幅]圖表長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : 'button[data-div="edit_cart_2"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表編輯]按鈕<br>可以編輯[歷年推案數]圖表長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : 'button[data-div="edit_cart_3"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表編輯]按鈕<br>可以編輯[歷年推案戶數]圖表長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : 'button[data-div="edit_cart_4"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表編輯]按鈕<br>可以編輯[歷年推案總銷金額]圖表長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : '.edit_cart_1',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '拖移滑桿(或直接輸入數字)<br>可以即時調整長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : '.edit_cart_2',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '拖移滑桿(或直接輸入數字)<br>可以即時調整長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : '.edit_cart_3',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '拖移滑桿(或直接輸入數字)<br>可以即時調整長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : '.edit_cart_4',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '拖移滑桿(或直接輸入數字)<br>可以即時調整長、寬、行列數'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	// //修改圖表顏色
	// 'id_5_5' : {
	// 	'parent' : 'id_5',					//父
	// 	'url' : '',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'#admin_menu .Other',
	// 				'#admin_menu .Color',
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '#admin_menu .Other ',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '滑鼠移到這顯示顏色設定選單'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '#admin_menu .Other ',
	// 					'class' : '',
	// 					'place' : 'bottom',	//內容１位置
	// 					'html' : '需前往[顏色設定]頁面裡設定'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },
	// //修改圖表顏色
	// 'id_5_5_1' : {
	// 	'parent' : 'id_5_5',					//父
	// 	'url' : '/manage/Color',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定 (選擇要修改的圖表)',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'.list-group',
	// 				'#admin_menu .Color',
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '.list-group',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '選擇要修改的圖表'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },
	// //圖表顏色設定 (設定喜好)
	// 'id_5_5_2' : {
	// 	'parent' : 'id_5_5',					//父
	// 	'url' : '/manage/Color',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定 (設定喜好)',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'.tab-count',
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '.tabs_color_a',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '.tabs_color_b',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '.tabs_color_c',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '.tabs_color_d',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '.tabs_color_e',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '.tabs_color_f',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '.tabs_color_g',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },
	// //圖表顏色設定 (儲存)
	// 'id_5_5_3' : {
	// 	'parent' : 'id_5_5',					//父
	// 	'url' : '/manage/Color',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定 (儲存)',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'#top_bar_button a.save',
	// 				'button[type="submit"]'
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '#top_bar_button a.save',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_a button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_b button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_c button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_d button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_e button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_f button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_g button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },
	// //圖表顏色設定 (儲存)
	// 'id_5_5_4' : {
	// 	'parent' : 'id_5_5',					//父
	// 	'url' : '/manage/Color',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定 (復原)',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'#top_bar_button a.save',
	// 				'button[type="submit"]'
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '#top_bar_button a.save',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_a button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_b button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_c button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_d button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_e button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_f button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_g button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },

	//房坪統計
	'id_6' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Statistical' : ['view']},	//權限
		'title' : '房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Statistical ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Statistical ',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '前往房坪統計'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選房坪統計
	'id_6_1' : {
		'parent' : 'id_6',					//父
		'url' : '/manage/Statistical',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往房坪統計表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Statistical' : ['view']},	//權限
		'title' : '篩選房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.region_div',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.region_div',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 選擇篩選條件'	//內容１HTML內容
					},
					{
						'dom' : '.region_div button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 按下送出'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯出房坪統計
	'id_6_2' : {
		'parent' : 'id_6',					//父
		'url' : '/manage/Statistical',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往房坪統計表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Statistical' : ['view']},	//權限
		'title' : '匯出房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button .excel',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button .excel',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選匯出EXCEL按鈕<br>即可匯出房坪統計表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//下載圖表
	'id_6_3' : {
		'parent' : 'id_6',					//父
		'url' : '/manage/Statistical',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往房坪統計表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Statistical' : ['view']},	//權限
		'title' : '下載圖表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-id="chart1"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-id="chart1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表下載]按鈕<br>即可下載[歷年推案漲幅]圖表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改圖表長寬及行列數
	'id_6_4' : {
		'parent' : 'id_6',					//父
		'url' : '/manage/Statistical',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往房坪統計表頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Statistical' : ['view']},	//權限
		'title' : '修改圖表長寬及行列數',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-div="edit_cart_1"]',
					'.edit_cart_1',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-div="edit_cart_1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表編輯]按鈕<br>可以編輯[房坪統計表]圖表長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : '.edit_cart_1',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '拖移滑桿(或直接輸入數字)<br>可以即時調整長、寬、行列數'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	// //修改圖表顏色
	// 'id_6_5' : {
	// 	'parent' : 'id_6',					//父
	// 	'url' : '',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'#admin_menu .Other',
	// 				'#admin_menu .Color',
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '#admin_menu .Other ',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '滑鼠移到這顯示顏色設定選單'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' : '#admin_menu .Other ',
	// 					'class' : '',
	// 					'place' : 'bottom',	//內容１位置
	// 					'html' : '需前往[顏色設定]頁面裡設定'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },
	// //修改圖表顏色
	// 'id_6_5_1' : {
	// 	'parent' : 'id_6_5',					//父
	// 	'url' : '/manage/Color',			//網址
	// 	'tab' : 'tabs_color_e',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定 (選擇房坪統計)',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'.list-group',
	// 				'#tabs_color_e',
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '#tabs_color_e',
	// 					'next' : true,
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '選擇房坪統計'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },
	// //圖表顏色設定 (設定喜好)
	// 'id_6_5_2' : {
	// 	'parent' : 'id_6_5',					//父
	// 	'url' : '/manage/Color',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定 (設定喜好)',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'.tab-count',
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '.tabs_color_e',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },
	// //圖表顏色設定 (儲存)
	// 'id_6_5_3' : {
	// 	'parent' : 'id_6_5',					//父
	// 	'url' : '/manage/Color',			//網址
	// 	'tab' : '',
	// 	'get_id' : '',
	// 	'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
	// 	'guide' : '',
	// 	'tabAccess' : {'Color' : ['view', 'edit']},	//權限
	// 	'title' : '圖表顏色設定 (儲存)',				//標題
	// 	'step' : [						//步驟
	// 		{	'shine' : [
	// 				'#top_bar_button a.save',
	// 				'button[type="submit"]'
	// 			],	//發亮物件
	// 			'content' : [				//內容
	// 				{
	// 					'dom' : '#top_bar_button a.save',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 				{
	// 					'dom' :'.tabs_color_e button[type="submit"]',
	// 					'class' : '',
	// 					'place' : 'top',	//內容１位置
	// 					'html' : '點擊儲存'	//內容１HTML內容
	// 				},
	// 			],
	// 		}
	// 	],
	// 	'explanation' : ''	//說明
	// },

	//可售戶數房坪統計
	'id_7' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Available' : ['view']},	//權限
		'title' : '可售戶數房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Available ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Available ',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '前往可售戶數房坪統計'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選可售戶數房坪統計
	'id_7_1' : {
		'parent' : 'id_7',					//父
		'url' : '/manage/Available',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往可售戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Available' : ['view']},	//權限
		'title' : '篩選房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.region_div',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.region_div',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 選擇篩選條件'	//內容１HTML內容
					},
					{
						'dom' : '.region_div button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 按下送出'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯出可售戶數房坪統計
	'id_7_2' : {
		'parent' : 'id_7',					//父
		'url' : '/manage/Available',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往可售戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Available' : ['view']},	//權限
		'title' : '匯出房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button .excel',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button .excel',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選匯出EXCEL按鈕<br>即可匯出可售戶數房坪統計'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//下載圖表
	'id_7_3' : {
		'parent' : 'id_7',					//父
		'url' : '/manage/Available',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往可售戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Available' : ['view']},	//權限
		'title' : '下載圖表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-id="chart1"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-id="chart1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表下載]按鈕<br>即可下載[歷年推案漲幅]圖表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改圖表長寬及行列數
	'id_7_4' : {
		'parent' : 'id_7',					//父
		'url' : '/manage/Available',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往可售戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Available' : ['view']},	//權限
		'title' : '修改圖表長寬及行列數',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-div="edit_cart_1"]',
					'.edit_cart_1',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-div="edit_cart_1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表編輯]按鈕<br>可以編輯[可售戶數房坪統計]圖表長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : '.edit_cart_1',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '拖移滑桿(或直接輸入數字)<br>可以即時調整長、寬、行列數'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//餘屋戶數房坪統計
	'id_8' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Remainder' : ['view']},	//權限
		'title' : '餘屋戶數房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Remainder ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Remainder ',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '前往餘屋戶數房坪統計'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選可售戶數房坪統計
	'id_8_1' : {
		'parent' : 'id_8',					//父
		'url' : '/manage/Remainder',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往餘屋戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Remainder' : ['view']},	//權限
		'title' : '篩選房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.region_div',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.region_div',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 選擇篩選條件'	//內容１HTML內容
					},
					{
						'dom' : '.region_div button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 按下送出'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//匯出餘屋戶數房坪統計
	'id_8_2' : {
		'parent' : 'id_8',					//父
		'url' : '/manage/Remainder',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往餘屋戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Remainder' : ['view']},	//權限
		'title' : '匯出房坪統計',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button .excel',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button .excel',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選匯出EXCEL按鈕<br>即可匯出餘屋戶數房坪統計'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//下載圖表
	'id_8_3' : {
		'parent' : 'id_8',					//父
		'url' : '/manage/Remainder',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往餘屋戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Remainder' : ['view']},	//權限
		'title' : '下載圖表',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-id="chart1"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-id="chart1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表下載]按鈕<br>即可下載[歷年推案漲幅]圖表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改圖表長寬及行列數
	'id_8_4' : {
		'parent' : 'id_8',					//父
		'url' : '/manage/Remainder',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往餘屋戶數房坪統計頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Remainder' : ['view']},	//權限
		'title' : '修改圖表長寬及行列數',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[data-div="edit_cart_1"]',
					'.edit_cart_1',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[data-div="edit_cart_1"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選[圖表編輯]按鈕<br>可以編輯[餘屋戶數房坪統計]圖表長、寬、行列數'	//內容１HTML內容
					},
					{
						'dom' : '.edit_cart_1',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '拖移滑桿(或直接輸入數字)<br>可以即時調整長、寬、行列數'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//區塊管理
	'id_9' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['view']},	//權限
		'title' : '區塊管理',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Other  ',
					'#admin_menu .Block   ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Other ',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示區塊管裡選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .Block ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[區塊管理]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//篩選需要顯示的欄位
	'id_9_0' : {
		'parent' : 'id_9',					//父
		'url' : '/manage/Block?p=1',			//網址
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['view']},	//權限
		'title' : '篩選需要顯示的欄位',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'div.field_list',
					'div.field_list input[name="all_field_name_val[]"]',
					'div.field_list input[name="field_name_val[]"]',
					'div.field_list button[type="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'div.field_list',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 點擊按鈕開啟要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active input[name="field_name_val[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 送出選取的資料刷新頁面'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_9_1' : {
		'parent' : 'id_9',					//父
		'url' : '/manage/Block?p=1',			//網址
		'get' : '',
		'confirm' : '前往區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu li.Block',
					'#content.Block tr.search th>*',
					'a.media_search',
					'#top_bar_button a.search'
				],	//發亮物件
				'content' : [				//內容
					//內容
					{
						'dom' : 'a.media_search',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(0) 手機模式需先點選搜尋'	//內容１HTML內容
					},
					{
						'dom' : '#content.Block tr.search th>*',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 先在欄位上篩選搜尋的資料'	//內容１HTML內容
					},
					{
						'dom' : '#content.Block tr.search th.edit_div button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下搜尋就可以篩選出搜尋的資料'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增建案區塊
	'id_9_2' : {
		'parent' : 'id_9',					//父
		'url' : '/manage/Block?p=1',			//網址
		'confirm' : '前往區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['add']},	//權限
		'title' : '新增建案區塊',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu li.Block',
					'#content.Block #top_bar_button a.new',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#content.Block #top_bar_button a.new',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選"新增"按鈕',	//內容１HTML內容
						'page' : ''
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增建案區塊
	'id_9_2_2' : {
		'parent' : 'id_9_2',					//父
		'url' : '/manage/Block?&addblock',			//網址
		'tab' : '',
		'confirm' : '前往新增區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['add']},	//權限
		'title' : '新增建案區塊',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'input[name="block"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'input[name="block"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '輸入要新增的區塊名稱',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案區塊 (儲存)'
	'id_9_2_3' : {
		'parent' : 'id_9_2',					//父
		'url' : '/manage/Block?&addblock',			//網址
		'tab' : '',
		'confirm' : '前往新增區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['add']},	//權限
		'title' : '新增建案區塊 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案區塊 (取消)
	'id_9_2_9' : {
		'parent' : 'id_9_2',					//父
		'url' : '/manage/Block?&addblock',			//網址
		'tab' : '',
		'confirm' : '前往新增區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['add']},	//權限
		'title' : '新增建案區塊 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Block?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Block?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增建案區塊 (復原、清空)
	'id_9_2_10' : {
		'parent' : 'id_9_2',					//父
		'url' : '/manage/Block?&addblock',			//網址
		'tab' : '',
		'confirm' : '前往新增區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['add']},	//權限
		'title' : '新增建案區塊 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料<br>(全部清空空白)',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案區塊
	'id_9_3' : {
		'parent' : 'id_9',					//父
		'url' : '/manage/Block?p=1',			//網址
		'tab' : '',
		'confirm' : '前往區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['edit']},	//權限
		'title' : '修改建案區塊',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.edit'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.edit',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '若有修改權限使用者<br>點選進入編輯',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案區塊 (點選修改)
	'id_9_3_1' : {
		'parent' : 'id_9_3',					//父
		'url' : '/manage/Block?&editblock&id_block=',			//網址
		'tab' : '',
		'get_id' : 'id_block',
		'confirm' : '前往修改區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['edit']},	//權限
		'title' : '修改建案區塊 (點選修改)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'input[name="block"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'input[name="block"]',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '編輯區塊名稱',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案區塊 (儲存)
	'id_9_3_3' : {
		'parent' : 'id_9_3',					//父
		'url' : '/manage/Block?&editblock&id_block=',			//網址
		'tab' : '',
		'get_id' : 'id_block',
		'confirm' : '前往修改區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['edit']},	//權限
		'title' : '修改建案區塊 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案區塊 (取消)
	'id_9_3_4' : {
		'parent' : 'id_9_3',					//父
		'url' : '/manage/Block?&editblock&id_block=',			//網址
		'tab' : '',
		'get_id' : 'id_block',
		'confirm' : '前往修改區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['edit']},	//權限
		'title' : '修改建案區塊 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Block?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Block?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案區塊 (復原、清空)
	'id_9_3_5' : {
		'parent' : 'id_9_3',					//父
		'url' : '/manage/Block?&editblock&id_block=',			//網址
		'tab' : '',
		'get_id' : 'id_block',
		'confirm' : '前往修改區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['edit']},	//權限
		'title' : '新增建案區塊 (復原)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//刪除建案區塊
	'id_9_4' : {
		'parent' : 'id_9',					//父
		'url' : '/manage/Block?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往區塊資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Block' : ['del']},	//權限
		'title' : '刪除建案區塊',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.del'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.del',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選刪除按鈕將會刪除該筆資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//房屋類型
	'id_10' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['view']},	//權限
		'title' : '房屋類型',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Other  ',
					'#admin_menu .Type   ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Other ',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示房屋類型選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .Type ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[房屋類型]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//篩選需要顯示的欄位
	'id_10_0' : {
		'parent' : 'id_10',					//父
		'url' : '/manage/Type?p=1',			//網址
		'confirm' : '前往房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['view']},	//權限
		'title' : '篩選需要顯示的欄位',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'div.field_list',
					'div.field_list input[name="all_field_name_val[]"]',
					'div.field_list input[name="field_name_val[]"]',
					'div.field_list button[type="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'div.field_list',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 點擊按鈕開啟要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active input[name="field_name_val[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 送出選取的資料刷新頁面'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_10_1' : {
		'parent' : 'id_10',					//父
		'url' : '/manage/Type?p=1',			//網址
		'get' : '',
		'confirm' : '前往房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu li.Type',
					'#content.Type tr.search th>*',
					'a.media_search',
					'#top_bar_button a.search'
				],	//發亮物件
				'content' : [				//內容
					//內容
					{
						'dom' : 'a.media_search',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(0) 手機模式需先點選搜尋'	//內容１HTML內容
					},
					{
						'dom' : '#content.Type tr.search th>*',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 先在欄位上篩選搜尋的資料'	//內容１HTML內容
					},
					{
						'dom' : '#content.Type tr.search th.edit_div button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下搜尋就可以篩選出搜尋的資料'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增房屋類型
	'id_10_2' : {
		'parent' : 'id_10',					//父
		'url' : '/manage/Type?p=1',			//網址
		'confirm' : '前往房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['add']},	//權限
		'title' : '新增房屋類型',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu li.Type',
					'#content.Type #top_bar_button a.new',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#content.Type #top_bar_button a.new',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選"新增"按鈕',	//內容１HTML內容
						'page' : ''
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增建案類型
	'id_10_2_2' : {
		'parent' : 'id_10_2',					//父
		'url' : '/manage/Type?&addtype',			//網址
		'tab' : '',
		'confirm' : '前往新增房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['add']},	//權限
		'title' : '新增房屋類型',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'input[name="type"]',
					'#position',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'input[name="type"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '輸入要新增的類型名稱',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#position',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '順序是可以改變表單裡選單的順序',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增房屋類型 (儲存)'
	'id_10_2_3' : {
		'parent' : 'id_10_2',					//父
		'url' : '/manage/Type?&addtype',			//網址
		'tab' : '',
		'confirm' : '前往新增房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['add']},	//權限
		'title' : '新增房屋類型 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增房屋類型 (取消)
	'id_10_2_9' : {
		'parent' : 'id_10_2',					//父
		'url' : '/manage/Type?&addtype',			//網址
		'tab' : '',
		'confirm' : '前往新增房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['add']},	//權限
		'title' : '新增房屋類型 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Type?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Type?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增房屋類型 (復原、清空)
	'id_10_2_10' : {
		'parent' : 'id_10_2',					//父
		'url' : '/manage/Type?&addtype',			//網址
		'tab' : '',
		'confirm' : '前往新增房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['add']},	//權限
		'title' : '新增房屋類型 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料<br>(全部清空空白)',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案類型
	'id_10_3' : {
		'parent' : 'id_10',					//父
		'url' : '/manage/Type?p=1',			//網址
		'tab' : '',
		'confirm' : '前往房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['edit']},	//權限
		'title' : '修改建案類型',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.edit'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.edit',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '若有修改權限使用者<br>點選進入編輯',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案類型 (點選修改)
	'id_10_3_1' : {
		'parent' : 'id_10_3',					//父
		'url' : '/manage/Type?&edittype&id_type=',			//網址
		'tab' : '',
		'get_id' : 'id_type',
		'confirm' : '前往修改類型資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['edit']},	//權限
		'title' : '修改建案類型 (點選修改)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'input[name="type"]',
					'#position'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'input[name="type"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯類型名稱',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#position',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '順序是可以改變表單裡選單的順序',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案類型 (儲存)
	'id_10_3_3' : {
		'parent' : 'id_10_3',					//父
		'url' : '/manage/Type?&edittype&id_type=',			//網址
		'tab' : '',
		'get_id' : 'id_type',
		'confirm' : '前往修改類型資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['edit']},	//權限
		'title' : '修改建案類型 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案類型 (取消)
	'id_10_3_4' : {
		'parent' : 'id_10_3',					//父
		'url' : '/manage/Type?&edittype&id_type=',			//網址
		'tab' : '',
		'get_id' : 'id_type',
		'confirm' : '前往修改類型資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['edit']},	//權限
		'title' : '修改建案類型 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Type?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Type?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改建案類型 (復原、清空)
	'id_10_3_5' : {
		'parent' : 'id_10_3',					//父
		'url' : '/manage/Type?&edittype&id_type=',			//網址
		'tab' : '',
		'get_id' : 'id_type',
		'confirm' : '前往修改類型資料頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['edit']},	//權限
		'title' : '新增建案類型 (復原)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//刪除建案類型
	'id_10_4' : {
		'parent' : 'id_10',					//父
		'url' : '/manage/Type?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['del']},	//權限
		'title' : '刪除建案類型',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.del'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.del',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選刪除按鈕將會刪除該筆資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//改變建案類型顯示順序
	'id_10_5' : {
		'parent' : 'id_10',					//父
		'url' : '/manage/Type?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往房屋類型頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Type' : ['edit']},	//權限
		'title' : '改變建案類型顯示順序',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.glyphicon-move'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.glyphicon-move',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '案住左鍵可以拖移整列上下移動來"及時"改變顯示的順序',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//檔案分類
	'id_11' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['view']},	//權限
		'title' : '檔案分類',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Other  ',
					'#admin_menu .FileType   ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Other ',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示檔案分類選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .FileType ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[檔案分類]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選需要顯示的欄位
	'id_11_0' : {
		'parent' : 'id_11',					//父
		'url' : '/manage/FileType?p=1',			//網址
		'confirm' : '前往檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['view']},	//權限
		'title' : '篩選需要顯示的欄位',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'div.field_list',
					'div.field_list input[name="all_field_name_val[]"]',
					'div.field_list input[name="field_name_val[]"]',
					'div.field_list button[type="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'div.field_list',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 點擊按鈕開啟要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active input[name="field_name_val[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 送出選取的資料刷新頁面'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_11_1' : {
		'parent' : 'id_11',					//父
		'url' : '/manage/FileType?p=1',			//網址
		'get' : '',
		'confirm' : '前往檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu li.FileType',
					'#content.FileType tr.search th>*',
					'a.media_search',
					'#top_bar_button a.search'
				],	//發亮物件
				'content' : [				//內容
					//內容
					{
						'dom' : 'a.media_search',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(0) 手機模式需先點選搜尋'	//內容１HTML內容
					},
					{
						'dom' : '#content.FileType tr.search th>*',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 先在欄位上篩選搜尋的資料'	//內容１HTML內容
					},
					{
						'dom' : '#content.FileType tr.search th.edit_div button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下搜尋就可以篩選出搜尋的資料'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增檔案分類
	'id_11_2' : {
		'parent' : 'id_11',					//父
		'url' : '/manage/FileType?p=1',			//網址
		'confirm' : '前往檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['add']},	//權限
		'title' : '新增檔案分類',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#admin_menu li.FileType',
					'#content.FileType #top_bar_button a.new',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#content.FileType #top_bar_button a.new',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選"新增"按鈕',	//內容１HTML內容
						'page' : ''
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增檔案分類
	'id_11_2_2' : {
		'parent' : 'id_11_2',					//父
		'url' : '/manage/FileType?&addfile_type',			//網址
		'tab' : '',
		'confirm' : '前往新增檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['add']},	//權限
		'title' : '新增檔案分類',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'input[name="file_type"]',
					'#position',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'input[name="file_type"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '輸入要新增的檔案分類',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#position',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '順序是可以改變表單裡選單的順序',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增檔案分類 (儲存)'
	'id_11_2_3' : {
		'parent' : 'id_11_2',					//父
		'url' : '/manage/FileType?&addfile_type',			//網址
		'tab' : '',
		'confirm' : '前往新增檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['add']},	//權限
		'title' : '新增檔案分類 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增檔案分類 (取消)
	'id_11_2_9' : {
		'parent' : 'id_11_2',					//父
		'url' : '/manage/FileType?&addfile_type',			//網址
		'tab' : '',
		'confirm' : '前往新增檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['add']},	//權限
		'title' : '新增檔案分類 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/FileType?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/FileType?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增檔案分類 (復原、清空)
	'id_11_2_10' : {
		'parent' : 'id_11_2',					//父
		'url' : '/manage/FileType?&addfile_type',			//網址
		'tab' : '',
		'confirm' : '前往新增檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['add']},	//權限
		'title' : '新增檔案分類 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料<br>(全部清空空白)',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改檔案分類
	'id_11_3' : {
		'parent' : 'id_11',					//父
		'url' : '/manage/FileType?p=1',			//網址
		'tab' : '',
		'confirm' : '前往檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['edit']},	//權限
		'title' : '修改檔案分類',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.edit'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.edit',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '若有修改權限使用者<br>點選進入編輯',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改檔案分類 (點選修改)
	'id_11_3_1' : {
		'parent' : 'id_11_3',					//父
		'url' : '/manage/FileType?&editfile_type&id_file_type=',			//網址
		'tab' : '',
		'get_id' : 'id_file_type',
		'confirm' : '前往修改檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['edit']},	//權限
		'title' : '修改檔案分類 (點選修改)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'input[name="file_type"]',
					'#position'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'input[name="file_type"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯檔案分類',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '#position',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '順序是可以改變表單裡選單的順序',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改檔案分類 (儲存)
	'id_11_3_3' : {
		'parent' : 'id_11_3',					//父
		'url' : '/manage/FileType?&editfile_type&id_file_type=',			//網址
		'tab' : '',
		'get_id' : 'id_file_type',
		'confirm' : '前往修改檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['edit']},	//權限
		'title' : '修改檔案分類 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改檔案分類 (取消)
	'id_11_3_4' : {
		'parent' : 'id_11_3',					//父
		'url' : '/manage/FileType?&editfile_type&id_file_type=',			//網址
		'tab' : '',
		'get_id' : 'id_file_type',
		'confirm' : '前往修改檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['edit']},	//權限
		'title' : '修改檔案分類 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/FileType?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/FileType?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改檔案分類 (復原、清空)
	'id_11_3_5' : {
		'parent' : 'id_11_3',					//父
		'url' : '/manage/FileType?&editfile_type&id_file_type=',			//網址
		'tab' : '',
		'get_id' : 'id_file_type',
		'confirm' : '前往修改檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['edit']},	//權限
		'title' : '新增檔案分類 (復原)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//刪除檔案分類
	'id_11_4' : {
		'parent' : 'id_11',					//父
		'url' : '/manage/FileType?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['del']},	//權限
		'title' : '刪除檔案分類',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.del'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.del',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選刪除按鈕將會刪除該筆資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//改變檔案分類顯示順序
	'id_11_5' : {
		'parent' : 'id_11',					//父
		'url' : '/manage/FileType?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往檔案分類頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'FileType' : ['edit']},	//權限
		'title' : '改變檔案分類顯示順序',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.glyphicon-move'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.glyphicon-move',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '案住左鍵可以拖移整列上下移動來"及時"改變顯示的順序',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//圖表顏色設定
	'id_12' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Color' : ['view', 'edit']},	//權限
		'title' : '顏色設定',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Other  ',
					'#admin_menu .Color   ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Other ',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示顏色設定選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .Color ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[顏色設定]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改圖表顏色
	'id_12_1' : {
		'parent' : 'id_12',					//父
		'url' : '/manage/Color',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Color' : ['view', 'edit']},	//權限
		'title' : '圖表顏色設定 (選擇要修改的圖表)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.list-group',
					'#admin_menu .Color',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.list-group',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '選擇要修改的圖表'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//圖表顏色設定 (設定喜好)
	'id_12_1_1' : {
		'parent' : 'id_12_1',					//父
		'url' : '/manage/Color',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Color' : ['view', 'edit']},	//權限
		'title' : '圖表顏色設定 (設定喜好)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tab-count',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_color_a',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
					},
					{
						'dom' : '.tabs_color_b',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
					},
					{
						'dom' : '.tabs_color_c',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
					},
					{
						'dom' : '.tabs_color_d',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
					},
					{
						'dom' : '.tabs_color_e',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
					},
					{
						'dom' : '.tabs_color_f',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
					},
					{
						'dom' : '.tabs_color_g',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '根據自己的喜好做顏色及圖表長、寬、行列數調整'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//圖表顏色設定 (儲存)
	'id_12_1_2' : {
		'parent' : 'id_12_1',					//父
		'url' : '/manage/Color',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Color' : ['view', 'edit']},	//權限
		'title' : '圖表顏色設定 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'button[type="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_a button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_b button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_c button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_d button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_e button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_f button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_g button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點擊儲存'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//圖表顏色設定 (復原)
	'id_12_1_3' : {
		'parent' : 'id_12_1',					//父
		'url' : '/manage/Color',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往顏色設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Color' : ['view', 'edit']},	//權限
		'title' : '圖表顏色設定 (復原)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' :'.tabs_color_a button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_b button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_c button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_d button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_e button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_f button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料'	//內容１HTML內容
					},
					{
						'dom' :'.tabs_color_g button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯前的資料'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//Log記錄
	'id_13' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Log' : ['view']},	//權限
		'title' : 'Log記錄',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Management  ',
					'#admin_menu .Log   ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Management ',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示Log記錄選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .Log ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[Log記錄]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選Log記錄
	'id_13_1' : {
		'parent' : 'id_13',					//父
		'url' : '/manage/Log?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往Log記錄頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Log' : ['view']},	//權限
		'title' : '篩選Log記錄',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.search th>*',
					'#admin_menu .Log   ',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.search th>*',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 填寫要搜尋篩選的條件'	//內容１HTML內容
					},
					{
						'dom' : '.edit_div button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 點選搜尋送出篩選條件'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//管理者帳號
	'id_14' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['view']},	//權限
		'title' : '管理者帳號',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu .Management'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu .Management',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示管理者帳號選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .Management ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[管理者帳號]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選需要顯示的欄位
	'id_14_0' : {
		'parent' : 'id_14',					//父
		'url' : '/manage/Management?p=1',			//網址
		'confirm' : '前往管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['view']},	//權限
		'title' : '篩選需要顯示的欄位',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'div.field_list',
					'div.field_list input[name="all_field_name_val[]"]',
					'div.field_list input[name="field_name_val[]"]',
					'div.field_list button[type="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'div.field_list',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 點擊按鈕開啟要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active input[name="field_name_val[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 送出選取的資料刷新頁面'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_14_1' : {
		'parent' : 'id_14',					//父
		'url' : '/manage/Management?p=1',			//網址
		'get' : '',
		'confirm' : '前往管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					// '#admin_menu li.Management',
					'#content.Management tr.search th>*',
					'a.media_search',
					'#top_bar_button a.search'
				],	//發亮物件
				'content' : [				//內容
					//內容
					{
						'dom' : 'a.media_search',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(0) 手機模式需先點選搜尋'	//內容１HTML內容
					},
					{
						'dom' : '#content.Management tr.search th>*',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 先在欄位上篩選搜尋的資料'	//內容１HTML內容
					},
					{
						'dom' : '#content.Management tr.search th.edit_div button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下搜尋就可以篩選出搜尋的資料'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者帳號
	'id_14_2' : {
		'parent' : 'id_14',					//父
		'url' : '/manage/Management?p=1',			//網址
		'confirm' : '前往管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#content.Management #top_bar_button a.new',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#content.Management #top_bar_button a.new',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選"新增"按鈕',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者帳號 (填寫資料)
	'id_14_2_2' : {
		'parent' : 'id_14_2',					//父
		'url' : '/manage/Management?&addadmin',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號 (填寫資料)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_default',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '輸入管理者所屬群組、管理者名稱、帳號、密碼及啟用狀態',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者帳號
	'id_14_2_3' : {
		'parent' : 'id_14_2',					//父
		'url' : '/manage/Management?&addadmin',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號 (選擇群組)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#id_group',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#id_group',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '每一個群組都有不同權限<br>其權限需至<a href="/manage/Competence">權限設定</a>頁面來設定',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//新增管理者帳號 (選擇群組)
	'id_14_2_4' : {
		'parent' : 'id_14_2',					//父
		'url' : '/manage/Management?&addadmin',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號 (選擇群組)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#id_group',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#id_group',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '每一個群組都有不同權限<br>其權限需至<a htref="/manage/Competence">權限設定</a>頁面來設定',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者帳號 (設定啟用狀態)
	'id_14_2_5' : {
		'parent' : 'id_14_2',					//父
		'url' : '/manage/Management?&addadmin',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號 (設定啟用狀態)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'div[data-toggle="buttons"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'label.active',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '啟用狀態設定為啟用使用者才能登入<br>若設定為關閉狀態使用者將無法正常登入',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者帳號 (儲存)'
	'id_14_2_6' : {
		'parent' : 'id_14_2',					//父
		'url' : '/manage/Management?&addadmin',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者帳號 (取消)
	'id_14_2_9' : {
		'parent' : 'id_14_2',					//父
		'url' : '/manage/Management?&addadmin',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Management?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Management?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者帳號 (復原、清空)
	'id_14_2_10' : {
		'parent' : 'id_14_2',					//父
		'url' : '/manage/Management?&addadmin',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['add']},	//權限
		'title' : '新增管理者帳號 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料<br>(全部清空空白)',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號
	'id_14_3' : {
		'parent' : 'id_14',					//父
		'url' : '/manage/Management?p=1',			//網址
		'tab' : '',
		'confirm' : '前往管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.edit'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.edit',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '若有修改權限使用者<br>點選進入編輯',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號 (點選修改)
	'id_14_3_1' : {
		'parent' : 'id_14_3',					//父
		'url' : '/manage/Management?&editadmin&id_admin=',			//網址
		'tab' : '',
		'get_id' : 'id_admin',
		'confirm' : '前往修改管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號 (點選修改)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.tabs_default',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.tabs_default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '輸入管理者所屬群組、管理者名稱、帳號、密碼及啟用狀態',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號
	'id_14_3_2' : {
		'parent' : 'id_14_3',					//父
		'url' : '/manage/Management?&editadmin',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號 (選擇群組)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#id_group',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#id_group',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '每一個群組都有不同權限<br>其權限需至<a href="/manage/Competence">權限設定</a>頁面來設定',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號 (選擇群組)
	'id_14_3_3' : {
		'parent' : 'id_14_3',					//父
		'url' : '/manage/Management?&editadmin',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號 (選擇群組)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#id_group',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#id_group',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '每一個群組都有不同權限<br>其權限需至<a htref="/manage/Competence">權限設定</a>頁面來設定',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號 (設定啟用狀態)
	'id_14_3_4' : {
		'parent' : 'id_14_3',					//父
		'url' : '/manage/Management?&editadmin',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號 (設定啟用狀態)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'div[data-toggle="buttons"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'label.active',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '啟用狀態設定為啟用使用者才能登入<br>若設定為關閉狀態使用者將無法正常登入',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號 (儲存)'
	'id_14_3_5' : {
		'parent' : 'id_14_3',					//父
		'url' : '/manage/Management?&editadmin',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號 (取消)
	'id_14_3_6' : {
		'parent' : 'id_14_3',					//父
		'url' : '/manage/Management?&editadmin',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Management?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Management?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者帳號 (復原、清空)
	'id_14_3_7' : {
		'parent' : 'id_14_3',					//父
		'url' : '/manage/Management?&editadmin',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['edit']},	//權限
		'title' : '修改管理者帳號 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//刪除管理者帳號
	'id_14_4' : {
		'parent' : 'id_14',					//父
		'url' : '/manage/Management?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往管理者帳號頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Management' : ['del']},	//權限
		'title' : '刪除管理者帳號',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.del'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.del',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選刪除按鈕將會刪除該筆資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//管理者群組
	'id_15' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['view']},	//權限
		'title' : '管理者群組',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu>ul>.Management',
					'#admin_menu .Group'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu>ul>.Management',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示管理者群組選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .Group ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[管理者群組]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//篩選需要顯示的欄位
	'id_15_0' : {
		'parent' : 'id_15',					//父
		'url' : '/manage/Group?p=1',			//網址
		'confirm' : '前往管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['view']},	//權限
		'title' : '篩選需要顯示的欄位',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'div.field_list',
					'div.field_list input[name="all_field_name_val[]"]',
					'div.field_list input[name="field_name_val[]"]',
					'div.field_list button[type="submit"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'div.field_list',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 點擊按鈕開啟要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active input[name="field_name_val[]"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(2) 選取要顯示的欄位'	//內容１HTML內容
					},
					{
						'dom' : 'div.field_list.active button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(3) 送出選取的資料刷新頁面'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//搜尋篩選資料
	'id_15_1' : {
		'parent' : 'id_15',					//父
		'url' : '/manage/Group?p=1',			//網址
		'get' : '',
		'confirm' : '前往管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['view']},	//權限
		'title' : '搜尋篩選資料',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					// '#admin_menu li.Group',
					'#content.Group tr.search th>*',
					'a.media_search',
					'#top_bar_button a.search'
				],	//發亮物件
				'content' : [				//內容
					//內容
					{
						'dom' : 'a.media_search',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(0) 手機模式需先點選搜尋'	//內容１HTML內容
					},
					{
						'dom' : '#content.Group tr.search th>*',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '(1) 先在欄位上篩選搜尋的資料'	//內容１HTML內容
					},
					{
						'dom' : '#content.Group tr.search th.edit_div button[type="submit"]',
						'class' : '',
						'place' : 'bottom',	//內容１位置
						'html' : '(2) 按下搜尋就可以篩選出搜尋的資料'	//內容１HTML內容
					},
				],

			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者群組
	'id_15_2' : {
		'parent' : 'id_15',					//父
		'url' : '/manage/Group?p=1',			//網址
		'confirm' : '前往管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['add']},	//權限
		'title' : '新增管理者群組',				//標題
		'step' : [						//步驟
			{
				'shine' : [
					'#content.Group #top_bar_button a.new',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#content.Group #top_bar_button a.new',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '點選"新增"按鈕',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者群組 (填寫資料)
	'id_15_2_3' : {
		'parent' : 'id_15_2',					//父
		'url' : '/manage/Group?&addgroup',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['add']},	//權限
		'title' : '新增管理者群組 (填寫資料)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#name',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#name',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '設定管理者群組名稱<br>每一個群組都有不同權限<br>其權限需至<a htref="/manage/Competence">權限設定</a>頁面來設定',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者群組 (儲存)'
	'id_15_2_4' : {
		'parent' : 'id_15_2',					//父
		'url' : '/manage/Group?&addgroup',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['add']},	//權限
		'title' : '新增管理者群組 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者群組 (取消)
	'id_15_2_5' : {
		'parent' : 'id_15_2',					//父
		'url' : '/manage/Group?&addgroup',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['add']},	//權限
		'title' : '新增管理者群組 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Group?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Group?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//新增管理者群組 (復原、清空)
	'id_15_2_6' : {
		'parent' : 'id_15_2',					//父
		'url' : '/manage/Group?&addgroup',			//網址
		'tab' : '',
		'confirm' : '前往新增管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['add']},	//權限
		'title' : '新增管理者群組 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料<br>(全部清空空白)',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者群組
	'id_15_3' : {
		'parent' : 'id_15',					//父
		'url' : '/manage/Group?p=1',			//網址
		'tab' : '',
		'confirm' : '前往管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['edit']},	//權限
		'title' : '修改管理者群組',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.edit'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.edit',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '若有修改權限使用者<br>點選進入編輯',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者群組 (點選修改)
	'id_15_3_1' : {
		'parent' : 'id_15_3',					//父
		'url' : '/manage/Group?&editgroup&id_group=',			//網址
		'tab' : '',
		'get_id' : 'id_group',
		'confirm' : '前往修改管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['edit']},	//權限
		'title' : '修改管理者群組 (點選修改)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#name',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#name',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '設定管理者群組名稱<br>每一個群組都有不同權限<br>其權限需至<a htref="/manage/Competence">權限設定</a>頁面來設定',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	'id_15_3_2' : {
		'parent' : 'id_15_3',					//父
		'url' : '/manage/Group?&editgroup',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['edit']},	//權限
		'title' : '修改管理者群組 (儲存)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#top_bar_button a.save',
					'.panel-footer button[type="submit"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#top_bar_button a.save',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
					{
						'dom' : '.panel-footer button[type="submit"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '編輯完點選儲存',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者群組 (取消)
	'id_15_3_3' : {
		'parent' : 'id_15_3',					//父
		'url' : '/manage/Group?&editgroup',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['edit']},	//權限
		'title' : '修改管理者群組 (取消)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'a[href="/manage/Group?"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'a[href="/manage/Group?"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若不想編輯可以按取消回主頁',	//內容１HTML內容
						'page' : ''
					},

				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改管理者群組 (復原、清空)
	'id_15_3_4' : {
		'parent' : 'id_15_3',					//父
		'url' : '/manage/Group?&editgroup',			//網址
		'tab' : '',
		'confirm' : '前往修改管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['edit']},	//權限
		'title' : '修改管理者群組 (復原、清空)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'button[type="reset"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'button[type="reset"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '若按下復原將會回到尚未編輯的資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//刪除管理者群組
	'id_15_4' : {
		'parent' : 'id_15',					//父
		'url' : '/manage/Group?p=1',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '前往管理者群組頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Group' : ['del']},	//權限
		'title' : '刪除管理者群組',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'td.edit_div a.del'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'td.edit_div a.del',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '點選刪除按鈕將會刪除該筆資料',	//內容１HTML內容
						'page' : ''
					},
				],
			}
		],
		'explanation' : ''	//說明
	},

	//權限設定
	'id_16' : {
		'parent' : '',					//父
		'url' : '',			//網址
		'tab' : '',
		'get_id' : '',
		'confirm' : '',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '權限設定',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'#admin_menu>ul>.Management',
					'#admin_menu .Competence'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '#admin_menu>ul>.Management',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '滑鼠移到這顯示權限設定選單'	//內容１HTML內容
					},
					{
						'dom' : '#admin_menu .Competence ',
						'class' : '',
						'next' : true,
						'place' : 'bottom',	//內容１位置
						'html' : '前往[權限設定]頁面裡設定'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限
	'id_16_1' : {
		'parent' : 'id_16',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.list-group',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.list-group',
						'class' : '',
						'next' : true,
						'place' : 'top',	//內容１位置
						'html' : '選擇要修改個群組'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (選擇要修改個群組)
	'id_16_1_1' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (選擇要修改個群組)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.competence_tab .panel-default',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.competence_tab .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '設定主要功能表權限'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (主選單功能)
	'id_16_1_2' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (主選單功能)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.competence_tab .panel-default',
					'tr.parent',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'tr.parent',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '灰色底是主選單功能'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (次選單功能)
	'id_16_1_3' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (次選單功能)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.competence_tab .panel-default',
					'tr.child',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'tr.child',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '白色底是次選單功能'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (權限設定)
	'id_16_1_4' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (權限設定)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.competence_tab .panel-default',
					'#tabs_group_2',
					'input.type_view',
					'input.type_add',
					'input.type_edit',
					'input.type_del',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.competence_tab .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '每一列都會對應[顯示]、[新增]、[修改]、[刪除]權限<br>將其打勾該，所對應的"群組"及"功能"將會啟用該[權限]'	//內容１HTML內容
					},
					{
						'dom' : 'input.type_view',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '檢視權限'	//內容１HTML內容
					},
					{
						'dom' : 'input.type_add',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '新增權限'	//內容１HTML內容
					},
					{
						'dom' : 'input.type_edit',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '修改權限'	//內容１HTML內容
					},
					{
						'dom' : 'input.type_del',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '刪除權限'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (賦予帳號管理)
	'id_16_1_5' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (賦予帳號管理)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.for_group_tab .panel-default',
					'tr[data-id_tab="4"]',
					'tr[data-id_tab="12"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.for_group_tab .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '表示該群組如果有[管理者帳號]或[權限設定]權限<br>就可以賦與他管裡的群組<br>即表示他管裡的下屬單位是哪些群組'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (賦予帳號管理)
	'id_16_1_6' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (管理者帳號、權限設定權限位置)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.competence_tab .panel-default',
					'tr[data-id_tab="4"]',
					'tr[data-id_tab="12"]'
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'tr[data-id_tab="4"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '[管理者帳號]權限'	//內容１HTML內容
					},
					{
						'dom' : 'tr[data-id_tab="12"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '[權限設定]權限'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (檔案權限)
	'id_16_1_7' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (檔案權限)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.file_tab .panel-default',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : '.file_tab .panel-default',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '設定群組如果有[建案資料]權限<br>是否啟用檔案[上傳]、[下載]、[移動]、[刪除]權限?'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
	//修改權限 (賦予帳號管理)
	'id_16_1_8' : {
		'parent' : 'id_16_1',					//父
		'url' : '/manage/Competence',			//網址
		'tab' : 'tabs_group_2',
		'get_id' : '',
		'confirm' : '前往權限設定頁面?',			//前網頁面確認訊息
		'guide' : '',
		'tabAccess' : {'Competence' : ['edit']},	//權限
		'title' : '修改權限 (建案資料權限)',				//標題
		'step' : [						//步驟
			{	'shine' : [
					'.competence_tab .panel-default',
					'tr[data-id_tab="4"]',
				],	//發亮物件
				'content' : [				//內容
					{
						'dom' : 'tr[data-id_tab="4"]',
						'class' : '',
						'place' : 'top',	//內容１位置
						'html' : '[建案資料]權限'	//內容１HTML內容
					},
				],
			}
		],
		'explanation' : ''	//說明
	},
};
