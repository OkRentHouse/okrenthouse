(function ($) {
	var HelpBook = {
		'run' : function(){	//執行
			var cc = '';
			var chapter = 0;
			var chapter2 = 0;
			var chapter3 = 0;
			var parent = '';
			var parent_2 = '';
			//選單上編號
			var arr = new Array();
			var com_t = false;
			var arr_competence = new Array();
			for(var id in HelpBook.ini){
				com_t = false;
				var ini = HelpBook.ini[id];
				//檢察官看權限
				if(HelpBook.competence == 'all'){
					com_t = true;
				}else{
					if(ini['tabAccess'][''] != undefined){
						com_t = true;
					}else{
						for(var key in ini.tabAccess) {
							for (var key2 in ini.tabAccess[key]) {
								if(ini.tabAccess[key][key2] != undefined && HelpBook.competence[key] != undefined){
									if(HelpBook.competence[key].indexOf(ini.tabAccess[key][key2]) >= 0){
										com_t = true;
										break;
									};
								};
							};
							if(com_t){
								break;
							};
						};
					};
				};
				if(com_t){
					if(ini.parent != ''){
						if(ini.parent == parent){	//第二層
							chapter3 = 0;
							chapter2++;
							cc = '';
							arr = new Array();
						}else{	//第三層
							arr.push(id);
							this.page_num[ini.parent] = arr;
							chapter3++;
							cc = '-'+chapter3
						};
						$('.help_block_menu ul[help_book_parent="'+ini.parent+'"]').append('<li><a href="#" data-title="'+ini.title+'" data-id="'+id+'"><i class="chapter">'+chapter+'-'+chapter2+cc+'</i>'+ini.title+'</a><ul help_book_parent="'+id+'"></ul></li>');
					}else{		//第一層
						parent = id;
						parent_2 = '';
						chapter++;
						chapter2 = 0;
						chapter3 = 0;
						$('.help_block_menu').append('<li><a href="#" data-title="'+ini.title+'" data-id="'+id+'"><i class="chapter">'+chapter+'</i>'+ini.title+'</a><ul help_book_parent="'+id+'"></ul></li>');
					};
				};
			};
			$(document).on('click', '.open_help_book', function(){
				var $help_block_menu = $('.help_block_menu');
				if($help_block_menu.css('display') == 'none'){
					$help_block_menu.stop(true, false).slideDown();
				}else{
					$help_block_menu.stop(true, false).slideUp();
				};
			}).on('click', '.close_help_block_menu', function(){
				$('.help_block_menu').stop(true, false).slideUp();
			}).on('click', '.close_help_block', function(){
				HelpBook.close();
			}).on('click', '.help_block_menu li a', function(){
				HelpBook.page($(this).data('id'));
			}).on('click', '.help_block_menu a[href="#"]', function(){
				return false;
			}).on('click', '.help_block  a[href="#"]', function(){
				return false;
			}).on('click', '.help_block_previous', function(){
				HelpBook.id;
				HelpBook.step;
			}).on('click', '.help_block_next', function(){
				return HelpBook.next();
			}).on('click', '.help_block_previous', function(){
				return HelpBook.previous();
			}).on('click', '.help_book_shine ', function(){
				return HelpBook.page(HelpBook.id);
			}).on('click', '.help_block_next_button', function(){
				return HelpBook.next($(this));
			});
			$(window).resize(function() {
				HelpBook.page(HelpBook.id);
			});
			var id = url('?helpbook_id');
			if(id != '' && id != undefined){
				HelpBook.page(id);
			};
		},
		'init' : function(help_book_ini){	//設定擋
			if(help_book_ini != undefined) this.ini = help_book_ini
			return this.ini;
		},
		'page' : function(id, $in_this=null){
			var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
			var _return = false;
			$('.help_block_s').removeClass('help_block_s');
			if(id != undefined){
				$('.help_block_menu li a').removeClass('active');
				var $this = $('.help_block_menu li a[data-id="'+id+'"]');
				var $li = $this.parents('.help_block_menu>li');
				var $li2 = $this.parents('.help_block_menu>li>ul>li');
				$this.addClass('active');
				$('>a', $li2).addClass('active');
				$('>a', $li).addClass('active');
				if(id != undefined){
					$('.help_block_menu li ul').hide();
				};
				$('>ul', $li).show();
				$('.help_book_shine').removeClass('help_book_shine');
				var ini = this.ini[id];
				var $hbc = $('.help_block_content');
				var scroll_top = 0;
				var scroll_left = 0;
				if(ini != undefined){
					if(this.id != id){
						this.id = id;
						this.step = 0;
					};
					var tab = ini.tab;
					var get_id = ini.get_id;
					if(ini.url != ''){
						if(url().indexOf(ini.url) < 0){
							if(1){
								if(get_id != '' && get_id != undefined){
									if($in_this != null){
										var _href = $in_this.attr('href');
									}else{
										var _href = $('a[href^="'+ini.url+'"]').eq(0).attr('href');
									};
									if(_href != undefined){
										if(_href.indexOf('#') > 0){
											_href = _href.substr(0, _href.indexOf('#'));
										}
										_href = _href.substr(_href.indexOf(get_id+'='));
										if(_href.indexOf('&') > 0){
											get_id = _href.substr(_href.indexOf('=')+1, _href.indexOf('&'));
										}else{
											get_id = _href.substr(_href.indexOf('=')+1);
										};
									};
								}else{
									get_id = '';
								};
								var t = '?';
								if(ini.url.indexOf('?') > 0){
									t = '&'
								};
								var _url = ini.url+get_id+t+'helpbook_id='+id;
								if(tab != '' && _url != undefined){
									_url += '#'+tab;
								};
								window.location.replace(_url);
								return true;
							}else{
								// $hbc.hide();
								// $('.help_block_tip').hide();
								// exit;
							};
						};
					};
					if(tab != '' && tab != undefined){
						window.history.replaceState(null, null, $(location).attr('pathname')+$(location).attr('search')+'#'+tab);
						$('.count_tabs li').siblings('li').removeClass('active');
						var $edit = $('a.edit.btn', '#top_bar_button');
						if($edit.length > 0){
							var href = $edit.attr('href');
							if(href.match('#') != null){
								href = href.split('#')[0];
							};
							$edit.attr('href', href+'#'+tab);
						};
						var $back = $('a.back.btn', '#top_bar_button');
						if($back.length > 0){
							var href2 = $back.attr('href');
							if(href2.match('#') != null){
								href2 = href2.split('#')[0];
							};
							$back.attr('href', href2+'#'+tab);
						};
						$(this).addClass('active');
						show_tabs();
					};
					var dom_num = 0;
					var ini_step = ini.step[this.step];
					for(var i in ini_step.shine){
						var $this =  $(ini_step.shine[i]);
						if(dom_num == 0){
							dom_num = $this.length;
						};
						$this.addClass('help_book_shine');
						if($this.css('position') != 'fixed' && $this.css('position') != 'absolute'){
							$this.addClass('help_block_s');
						};
					};
					if(dom_num > 0){
						$('.help_block').stop(true, false).fadeIn();
					};
					var arr_htm = '';
					var content = ini_step.content;
					var c_num = content.length;
					var hbc_num = $hbc.length;
					if(c_num > hbc_num){		//說明比說明框多
						var j = c_num - hbc_num;
						for(i=0;i<j;i++){
							$('.help_block_menu').after('<div class="help_block_content"></div>');
							$('.help_block_menu').after('<div class="help_block_tip"></div>');
						};
					}else if(c_num < hbc_num){	//說明比說明框少
						$hbc.eq(c_num).next('.help_block_content').nextAll('.help_block_content').stop(true, false).fadeOut(function(){
							$(this).remove();
						});
						$('.help_block_tip').eq(c_num).next('.help_block_tip').nextAll('.help_block_tip').stop(true, false).fadeOut(function(){
							$(this).remove();
						});
					};
					$('.help_block_content').stop(true, false).fadeOut(function(){
						$(this).html('');
					}).hide();
					$('.help_block_tip').hide();
					var o  = 0;
					var arr_click = {};
					for(var i in content){
						var $dom = $(content[i].dom).eq(0);
						if(content[i].html != '' && content[i].dom != '' && $dom.length){
							if(content[i].url != ''){

							};
							if($dom.css('position') != 'fixed' && $dom.css('position') != 'absolute'){
								$dom.addClass('help_block_s');
							};
							if(content[i].class != ''){
								$dom.addClass(content[i].class);
							};
							if(content[i].next){
								$dom.addClass('help_block_next_button');
							};
							var tip_class = '';
							var tip_left = 0;
							var tip_top = 0;
							var left = '';
							var top = '';
							var $hbc = $('.help_block_content').eq(i);
							$hbc.html(content[i].html);
							var left_r = $dom.offset().left
							var top_r = $dom.offset().top
							if(content[i].fixed){
								left_r = left_r - $body.scrollLeft();
								top_r = top_r - $body.scrollTop();
							};
							var w1 = $dom.width() + parseInt($dom.css('padding-left')) + parseInt($dom.css('padding-right'));
							var h1 = $dom.height() + parseInt($dom.css('padding-top')) + parseInt($dom.css('padding-bottom'));
							var w2 = $hbc.width() + parseInt($hbc.css('padding-left')) + parseInt($hbc.css('padding-right'));
							var h2 = $hbc.height() + parseInt($hbc.css('padding-top')) + parseInt($hbc.css('padding-bottom'));

							switch (content[i].place){
								case 'top':
									tip_class = 'tip_top';
									top = top_r - h2 - this.margin;
									left = left_r + (w1 / 2) - (w2 / 2);
									tip_top = top + h2;
									tip_left = left_r + (w1 / 2);
									break;
								case 'bottom':
									tip_class = 'tip_bottom';
									top = top_r + h1 + this.margin;
									left = left_r + (w1 / 2) - (w2 / 2);
									tip_top = top;
									tip_left = left_r + (w1 / 2);
									break;
								case 'left':
									tip_class = 'tip_left';
									break;
								case 'right':
									tip_class = 'tip_right';
									break;
								case 'top_left':
									break;
								case 'top_right':
									break;
								case 'bottom_left':
									break;
								case 'bottom_right':
									break;
							};
							left = Math.max(15, left);
							if($dom.css('display') == 'none' || ((top_r == 0) && (left_r == 0)) || $dom.is(':hidden')){
								$('.help_block_content').eq(i).hide();
								$('.help_block_tip').eq(i).hide();
							}else{
								if(scroll_top == 0){
									scroll_top = Math.min(top_r, top, tip_top);
								}else{
									scroll_top = Math.min(scroll_top, top_r, top, tip_top);
								};
								scroll_top -= 40;
								if(scroll_left == 0){
									scroll_left = Math.min(left_r, left, tip_left);
								}else{
									scroll_left = Math.min(scroll_left, left_r, left, tip_left);
								};
								scroll_left -= ($(window).width()/4);
								if(content[i].fixed){
									$('.help_block_content').eq(i).css({
										position : 'fixed'
									});
									$('.help_block_tip').eq(i).css({
										position : 'fixed'
									})
								};
								$('.help_block_content').eq(i).css({
									left: left,
									top: top,
									position : 'absolute'
								}).stop(true, false).fadeIn();
								$('.help_block_tip').eq(i).css({		//箭頭
									left: tip_left,
									top: tip_top,
									position : 'absolute'
								}).attr('class', 'help_block_tip '+tip_class).stop(true, false).show();
							};
							o++;
						};
					};
					if(o == 0){
						HelpBook.close();
					}else{
						var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
						$body.stop(true,false).animate({
							scrollTop : scroll_top-this.margin,
							scrollLeft : scroll_left-this.margin
						}, 'slow');
						var id = this.id;
						var index = Object.keys(this.ini).indexOf(id);
						var this_parent = '';
						var _previous = true;
						var _next = true;
						$('.help_block').stop(true, false).fadeIn();
						$('.help_block_previous').stop(true, false).fadeIn();

						if(index >= 0){
							this_parent = this.ini[id].parent;
						};
						var arr_tp = this.page_num[this_parent];
						var arr_id = this.page_num[id];


						if(arr_tp != undefined){
							if(arr_tp[0] == id){
								_previous = false;
							};
							if(arr_tp[arr_tp.length-1] == id){
								_next = false;
							};
						};
						if(arr_id != undefined){
							if(arr_id[0] == id){
								_previous = false;
							};
							if(arr_id[arr_id.length-1] == id){
								_next = false;
							};
						};
						if(arr_tp == undefined && arr_id == undefined){		//有子頁
							_previous = false;
							_next = false;
						};
						if(_previous){
							$('.help_block_previous').stop(true, false).fadeIn();
						}else{
							$('.help_block_previous').hide();
						};
						if(_next){
							$('.help_block_next').stop(true, false).fadeIn();
						}else{
							$('.help_block_next').hide();
						};
						$('.close_help_block').stop(true, false).fadeIn();
					};
					$('h3.title', '.help_block').html(ini.title);
					if(ini.explanation != undefined){

					};
				}else{
					$hbc.stop(true, false).fadeOut(function(){
						$(this).remove();
					});
					$('.help_block_tip').stop(true, false).fadeOut(function(){
						$(this).remove();
					});
				};
			}else{
				HelpBook.close();
			};
			return _return;
		},
		'jump' : function(next, $this=null){
			var id = this.id;
			var $a = $('li>a', '.help_block_menu');
			var num = $a.length;
			var eq = $('li>a[data-id="'+id+'"]', '.help_block_menu').last().index('.help_block_menu li>a');
			if(next > 0){
				if(eq == num-1){
					id = $a.eq(0).data('id');		//返回第一頁
				}else{
					id = $('li>a', '.help_block_menu').eq(eq+(next)).data('id');
				};
			}else{
				if(eq == 0){
					id = $a.eq(num-1).data('id');	//返回最後一頁
				}else{
					id = $('li>a', '.help_block_menu').eq(eq+(next)).data('id');
				};
			};
			this.id = id;
			return this.page(id, $this);
		},
		'next' : function($this){
			return this.jump(1, $this)
		},
		'previous' :function($this){
			return this.jump(-1, $this)
		},
		'close' : function(){
			this.id = null;
			$('.help_block_next_button').removeClass('help_block_next_button');
			$('a', '.help_block_menu').removeClass('active');
			$('.help_block_s').removeClass('help_block_s');
			$('.help_block_previous').stop(true, false).fadeOut();
			$('.help_block_next').stop(true, false).fadeOut();
			$('.close_help_block').stop(true, false).fadeOut();
			$('.help_block').stop(true, false).fadeOut();
			$('.help_block_tip').hide();
			$('.help_block_content').stop(true, false).fadeOut(function(){$(this).html('')});
			$('.help_book_shine').removeClass('help_book_shine');
		},
		'id' : null,
		'step' : null,
		'margin' : 10,
		'page_num' : {},
		'competence' : {},
	};	function win_hight(){
		var h = $(window).height()/3;
		var w = $(window).width()/3;
	};

	$(document).ready(function(){
		HelpBook.ini = help_book_ini;
		HelpBook.competence = help_book_competence;
		HelpBook.run();
	});
})(window.jQuery);