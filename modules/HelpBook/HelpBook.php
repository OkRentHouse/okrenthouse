<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/7/23
 * Time: 下午 05:07
 */

class HelpBook extends Module
{
	public $_languages = WEB_LANGIAGE;		//預設語系
	public $name = 'HelpBook';
	
	protected static $instance;
	
	public static function getContext(){
		if (!self::$instance) {
			self::$instance = new HelpBook();
		};
		return self::$instance;
	}
	
	public function hookdisplayAdminMinbarBefore(){
		if(empty($_SESSION['id_admin'])){
			return false;
		}
		return $this->context->smarty->fetch($this->dir.DS.'minbar_before.tpl');
	}
	
	public function hookdisplayAdminBodyAfter(){
		if(empty($_SESSION['id_admin'])){
			return false;
		}
		return $this->context->smarty->fetch($this->dir.DS.'body_after.tpl');
	}
	
	public function hookdisplayAdminBodyBefore(){
		if(empty($_SESSION['id_admin'])){
			return false;
		}
		return $this->context->smarty->fetch($this->dir.DS.'body_before.tpl');
	}
	
	public function hookdisplayAdminJavaScript(){
		$js = 'var HelpBookCompetence = {};';
		if($_SESSION['id_group'] == 1){
			$js = 'var help_book_competence = "all";';
		}else{
			$sql = 'SELECT c.`id_tab`, c.`view`, c.`add`, c.`edit`, c.`del`, t.`controller_name`
			FROM `competence` AS c
			LEFT JOIN `tab` AS t ON t.`id_tab` = c.`id_tab`
			WHERE `id_group` = '.$_SESSION['id_group'];
			$arr_row = Db::rowSQL($sql);
			$arr = array();
			foreach($arr_row as $i => $v){
				if($v['view']){
					$arr[$v['controller_name']][] = 'view';
				}
				if($v['add']){
					$arr[$v['controller_name']][] = 'add';
				}
				if($v['edit']){
					$arr[$v['controller_name']][] = 'edit';
				}
				if($v['del']){
					$arr[$v['controller_name']][] = 'del';
				}
			}
			$json = JSON::getContext();
			$js = 'var help_book_competence = '.$json->encode($arr).';';
		}
		$js = '<script type="text/javascript" id="HelpBookCompetence_js">'.$js.'document.getElementById("HelpBookCompetence_js");</script>';
		return $js;
	}
	
	public function hookAdmin_setMedia(){
		if(empty($_SESSION['id_admin'])){
			return false;
		}
		HelpBook::getContext();
		$l = '';
		if(false){	//快取
		
		}else{
			if(is_file($this->dir.DS.'js'.DS.'helpbook.ini.'.HelpBook::getContext()->_languages.'.js')){					//模組設定語系
				$l = HelpBook::getContext()->_languages;
			}elseif(!is_file($this->dir.DS.'js'.DS.'helpbook.ini.'.Configuration::get('WEB_LANGIAGE').'.js')){		//網站設定語系
				$l = Configuration::get('WEB_LANGIAGE');
			}elseif(!is_file($this->dir.DS.'js'.DS.'helpbook.ini.'.WEB_LANGIAGE.'.js')){							//網站預設語系
				$l = WEB_LANGIAGE;
			}
			if(!empty($l)){
				$this->context->controller->addJS($this->url.'js/helpbook.ini.'.$l.'.js', false, 1);
			}
		}
		$this->context->controller->addCSS($this->url.'css/helpbook.css');
		$this->context->controller->addJS($this->url.'js/helpbook.js');
	}
}