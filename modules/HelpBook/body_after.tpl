<div class="help_block">
	<h3 class="title"></h3>
	<a href="#" class="open_help_book" title="{l s='說明'}"><i class="glyphicon glyphicon-question-sign"></i></a>
	<div class="help_block_exp"></div>
</div><a href="#" class="help_block_previous">{l s='上一步'}</a><a href="#" class="close_help_block">{l s='關閉'}</a><a href="#" class="help_block_next">{l s='下一步'}</a>
<ul class="help_block_menu">
	<div class="close_help_block_menu btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></div>
	<h3>{l s='網站說明'}</h3>
</ul>