<?php
/**
 * Created by PhpStorm.
 * User: Allen
 * Date: 2018/9/27
 * Time: 下午 02:04
 */

class LoginBlockadeDb
{
	protected static $instance = NULL;        //是否實立
	public $login_blockade_action;
	public $login_error_second;
	public $login_error_num;
	public $login_error_blockade;

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new LoginBlockadeDb();
		};
		return self::$instance;
	}

	public function __construct()
	{
		$this->login_blockade_action = Configuration::get('login_blockade_action');
		$this->login_error_second = Configuration::get('login_error_second');
		$this->login_error_num = Configuration::get('login_error_num');
		$this->login_error_blockade = Configuration::get('login_error_blockade');
	}

	public function setLoginIP($ip = null, $block_time=null, $block_end_time=null)
	{
		if (empty($ip)) {
			$ip = ip();
		}
		if(empty($block_time)){
			$block_time = now();
		}
		if(empty($block_end_time)){
			$block_end_time = date('Y-m-d H:i:s',time() + $this->login_error_blockade);
		}
		$sql = sprintf('INSERT INTO `' . DB_PREFIX_ . 'login_blockade` (`ip`, `block_time`, `block_end_time`) VALUES (%s, %s, %s)',
			GetSQL($ip, 'text'),
			GetSQL($block_time, 'text'),
			GetSQL($block_end_time, 'text')
		);
		Db::rowSQL($sql);
		return Db::getContext()->num();
	}

	public function getLoginIP($ip = null)
	{
		if (empty($ip)) {
			$ip = ip();
		}
		$now = now();
		$sql = sprintf('SELECT `id_lb`
			FROM `' . DB_PREFIX_ . 'login_blockade`
			WHERE `ip` = %s AND (`block_end_time` >= %s OR `block_end_time` IS NULL OR `block_end_time` = 0)
			LIMIT 0, 1',
			GetSQL($ip, 'text'),
			GetSQL($now, 'text')
		);
		$row = Db::rowSQL($sql, true);
		return $row['id_lb'];
	}

	public function getLoginIP_num($ip = null, $time = null)
	{
		if (empty($ip)) {
			$ip = ip();
		}
		if (empty($time)) {
			$time = strtotime('num') - $this->login_error_second;
		}
		$sql = sprintf('SELECT `error_num`, `first_time`
			FROM `' . DB_PREFIX_ . 'login_blockade_num`
			WHERE `ip` = %s AND (`first_time` >= %d)
			ORDER BY `first_time` DESC
			LIMIT 0, 1',
			GetSQL($ip, 'text'),
			GetSQL($time, 'int')
		);
		$row = Db::rowSQL($sql, true);
		return $row;
	}

	public function setLoginIP_num($ip = null, $num = 1)
	{
		if (empty($ip)) {
			$ip = ip();
		}
		$sql = sprintf('SELECT `id_lbn`
			FROM `' . DB_PREFIX_ . 'login_blockade_num`
			WHERE `ip` = %s
			ORDER BY `first_time` DESC
			LIMIT 0, 1',
			GetSQL($ip, 'text')
		);
		$row = Db::rowSQL($sql, true);

		if($row['id_lbn']){		//有資料修改
			$sql = sprintf('UPDATE `' . DB_PREFIX_ . 'login_blockade_num` SET `error_num` = %d
				WHERE `id_lbn` = %d',
				$num,
				$row['id_lbn']);
		}else{					//無資料新增
			$sql = sprintf('INSERT INTO `' . DB_PREFIX_ . 'login_blockade_num` (`ip`, `first_time`, `error_num`) VALUES (%s, %d, %d)',
				GetSQL($ip, 'text'),
				GetSQL(strtotime('now'), 'int'),
				GetSQL($num, 'int'));
		}

		Db::rowSQL($sql);
		return Db::getContext()->num();
	}

	public function delLoginIP_num($time = null){
		if (empty($time)) {
			$time = strtotime('now') - $this->login_error_second;
		}
		$sql = sprintf('DELETE FROM `' . DB_PREFIX_ . 'login_blockade_num`
			WHERE `first_time` < %d',
			GetSQL($time, 'int'));
		Db::rowSQL($sql);
	}

	public function delLoginIP_numByIp($ip = null){
		if (empty($ip)) {
			$ip = ip();
		}
		$sql = sprintf('DELETE FROM `' . DB_PREFIX_ . 'login_blockade_num`
			WHERE `ip` = %s',
			GetSQL($ip, 'text'));
		Db::rowSQL($sql);
	}
}