<?php
/**
 * Created by PhpStorm.
 * User: 資訊組.設計專員
 * Date: 2018/7/23
 * Time: 下午 05:07
 */

include_once(MODULE_DIR . DS . 'LoginBlockade' . DS . 'classes' . DS . 'LoginBlockadeDb.php');
class LoginBlockade extends Module
{
	public	$login_blockade_action;
	public	$login_error_second;
	public	$login_error_num;
	public	$login_error_blockade;
	public	$context;

	public function __construct(){
		$this->context = Context::getContext();
		parent::__construct();
		$this->login_blockade_action = LoginBlockadeDb::getContext()->login_blockade_action;
		$this->login_error_second = LoginBlockadeDb::getContext()->login_error_second;
		$this->login_error_num = LoginBlockadeDb::getContext()->login_error_num;
		$this->login_error_blockade = LoginBlockadeDb::getContext()->login_error_blockade;
	}

	public function hookLoging_initProcess(){
		//查看是否有啟用
		if(LoginBlockadeDb::getContext()->getLoginIP()){
			$this->context->controller->_errors[] = $this->l('您登入失敗次數過多,請稍後再來登入');
		}
	}

	public function hookMemberLogin(){
		//登入成功,刪除IP錯誤紀錄
		LoginBlockadeDb::getContext()->delLoginIP_numByIp();
	}

	public function hookMemberLoginError(){
		$ip = ip();
		if(!empty($this->login_blockade_action) && !empty($this->login_error_num) && !empty($this->login_error_blockade)){
			$now = strtotime('now');
			$time = $now - $this->login_error_second;
			//刪除過期資料
			LoginBlockadeDb::getContext()->delLoginIP_num($time);
			$row = LoginBlockadeDb::getContext()->getLoginIP_num($ip, $time);
			if(count($row) == 0){
				$row['first_time'] = $now;
				$row['error_num'] = 0;
			}
			$le_num = $row['error_num'];			//登入失敗次數
			$le_num++;
			if($le_num >= $this->login_error_num){			//超過失敗次數
				//封鎖IP登入
				$this->context->controller->_errors[] = $this->l('您登入失敗次數過多,請稍後再來登入');
				LoginBlockadeDb::getContext()->setLoginIP($ip);
			}else{		//失敗次數內
				//更新失敗次數
				LoginBlockadeDb::getContext()->setLoginIP_num($ip, $le_num);
			}
		}else{		//登入成功
			LoginBlockadeDb::getContext()->delLoginIP_numByIp($ip);
		}
	}
}