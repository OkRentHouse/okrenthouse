<?php
class AdminLoginBlockadeController extends AdminModuleController
{
	public $tpl_folder;	//樣版資料夾
	public $page = 'LoginBlockade';
	public function __construct(){
		$this->className = 'AdminLoginBlockadeController';
		$this->fields['title'] = '登入錯誤限制設定';

		$this->fields['tabs'] = array(
			'login' => $this->l('登入錯誤限制設定'),
		);

		$this->fields['form'] = array(
			'login' => array(
				'tab' => 'login',
				'legend' => array(
					'title' => $this->l('登入錯誤限制設定'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'login_blockade_action',
						'type' => 'switch',
						'label' => $this->l('登入失敗封鎖'),
						'values' => array(
							array(
								'id' => 'AllInOne_FB_active_1`',
								'value' => 1,
								'label' => $this->l('啟用')
							),
							array(
								'id' => 'AllInOne_FB_active_0',
								'value' => 0,
								'label' => $this->l('關閉')
							)
						),
						'val' => 0,
						'configuration' => true
					),
					array(
						'name' => 'login_error_second',
						'type' => 'number',
						'label' => $this->l('時間範圍'),
						'suffix' => $this->l('秒內錯誤'),
						'min' => 0,
						'p' => $this->l('設定0為不限時間'),
						'configuration' => true
					),
					array(
						'name' => 'login_error_num',
						'type' => 'number',
						'label' => $this->l('登入失敗次數'),
						'suffix' => $this->l('次'),
						'min' => 0,
						'p' => $this->l('設定0(或不填寫)為不封鎖'),
						'configuration' => true
					),
					array(
						'name' => 'login_error_blockade',
						'type' => 'number',
						'label' => $this->l('IP封鎖時間'),
						'suffix' => $this->l('秒'),
						'min' => 0,
						'p' => $this->l('設定0(或不填寫)為不封鎖'),
						'placeholder' => $this->l('預設封鎖5分鐘(300秒)'),
						'configuration' => true
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					)
				),
				'reset' => array(
					'title' => $this->l('重置')
				)
			)
		);
		
		parent::__construct();
	}
	
	public function initContent(){
		$this->display = 'edit';
		parent::initContent();
	}
}