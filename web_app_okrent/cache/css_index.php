<?php return [
'all' => '/css/all.css',
'bootstrap-responsive' => '/css/bootstrap-responsive.css',
'bootstrap-responsive.min' => '/css/bootstrap-responsive.min.css',
'bootstrap-theme.min' => '/css/bootstrap-theme.min.css',
'bootstrap-theme.min.map' => '/css/bootstrap-theme.min.css.map',
'brands' => '/css/brands.css',
'color' => '/css/color.css',
'fa-brands' => '/css/fa-brands.css',
'fa-brands.min' => '/css/fa-brands.min.css',
'fa-regular' => '/css/fa-regular.css',
'fa-regular.min' => '/css/fa-regular.min.css',
'fa-solid' => '/css/fa-solid.css',
'fa-solid.min' => '/css/fa-solid.min.css',
'font-awesome' => '/css/font-awesome.css',
'fontawesome-all' => '/css/fontawesome-all.css',
'fontawesome-all.min' => '/css/fontawesome-all.min.css',
'fontawesome' => '/css/fontawesome.css',
'fontawesome.min' => '/css/fontawesome.min.css',
'jquery-ui' => '/css/jquery-ui.css',
'jquery-ui.min' => '/css/jquery-ui.min.css',
'jquery-ui.structure' => '/css/jquery-ui.structure.css',
'jquery-ui.structure.min' => '/css/jquery-ui.structure.min.css',
'jquery-ui.theme' => '/css/jquery-ui.theme.css',
'jquery-ui.theme.min' => '/css/jquery-ui.theme.min.css',
'jquery.growl' => '/css/jquery.growl.css',
'jquery.jqplot' => '/css/jquery.jqplot.css',
'jquery.jqplot.min' => '/css/jquery.jqplot.min.css',
'jquery.tagify' => '/css/jquery.tagify.css',
'main' => '/css/main.css',
'metro-all' => '/css/metro-all.css',
'metro.min' => '/css/metro.min.css',
'reset' => '/css/reset.css',
'slick-theme' => '/css/slick-theme.css',
'slick' => '/css/slick.css',
]; 