<?php

namespace web_app_okrent;

use \AppOkrentController;
use \CMS;
use \Context;
use \Tools;
use \Db;
use \File;

class CMSController extends AppOkrentController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'cms';
	public $html = '';
	public $js = '';

	public $arr_my_id_web = [0];
	public $arr_my_id_houses = [];

	public function __construct()
	{
		$this->className = 'CMSController';

		$this->fields['page']['cache'] = false;
		$this->display_edit_div        = false;

		parent::__construct();
		$this->no_page = true;
		$cms                             = CMS::getByUrl(12, CMS::getContext()->url);


		if(strpos($cms['html'],'{$tpl}')){
            $sql = "SELECT * FROM cms_file WHERE file_type=0 AND id_cms=".$cms['id_cms'].' ORDER BY id_c_f ASC ';
            $sql_data =  Db::rowSQL($sql, true);
            $row = File::get($sql_data['id_file']);
            $row['file_dir'] = urldecode($row['file_dir'].$row['filename']);//本地位置
            $this->introduce_tpl =  $row['file_dir'];
            $explode_hmtl = explode('{$tpl}',$cms['html']);
            $this->html0 = $explode_hmtl['0'];
            $this->html1 = $explode_hmtl['1'];
        }else{
            $this->html                      = $cms['html'];
        }
        $this->id_cms = $cms['id_cms'];//id值
        $this->internal_introduce = $cms['internal_introduce'];//引入data
		$this->meta_title                = $cms['title'];
		$this->page_header_toolbar_title = $cms['page_title'];
		$this->meta_description          = $cms['description'];
		$this->meta_keywords             = $cms['keywords'];
		$this->nobots                    = !$cms['index_ation'];
		$this->nofollow                  = !$cms['index_ation'];
		$this->display_header            = $cms['display_header'];
		$this->display_footer            = $cms['display_footer'];
		$this->css                       = $cms['css'];
		$this->js                        = $cms['js'];

		$this->no_link = false;
	}

	public function initToolbar()
	{
		parent::initToolbar();
		$this->back_url = '#';
	}


	public function initProcess()
	{
		parent::initProcess();

		$arr = $_SESSION['arr_house'][$_SESSION['house_index']];

		foreach ($_SESSION['arr_house'] as $i => $v) {
			$this->arr_my_id_web[]    = $v['id_web'];
			$this->arr_my_id_houses[] = $v['id_houses'];
		}
		Context::getContext()->smarty->assign([
			'web'        => $arr['web'],
			'arr_house'  => $_SESSION['arr_house'],
			'house_code' => $arr['house_code'],
			'name'       => $_SESSION['name'],
		]);
	}

	public function initContent()
	{
	    $sql = "SELECT `id_cms`,`title` FROM cms WHERE `customer_message`=1 AND url = '".CMS::getContext()->url."'";
        $row     = Db::rowSQL($sql, true);
//        print_r($row);
		parent::initContent();
		$this->context->smarty->assign([
			'cms_url' => CMS::getContext()->url,
			'html'    => $this->html,
            'html_0'    => $this->html0,
            'html_1'    => $this->html1,
            'introduce_tpl'    => $this->introduce_tpl,
			'cms_css'     => $this->css,
			'cms_js'      => $this->js,
            'id_cms'      =>$row['id_cms'],
		]);
		$this->setTemplate('cms.tpl');
	}

    public function setMedia()
    {
        parent::setMedia();
//				$this->addCSS('/css/metro-all.min.css');
//        $this->addJS('/js/metro.min.js');
        if(!empty($this->internal_introduce)){
            $explode_data = explode(',',$this->internal_introduce);
            foreach($explode_data as $value){
                if(strpos($value,'.js')){
                    $this->addJS($value);
                }else if(strpos($value,'.css')){
                    $this->addCSS($value);
                }
            }
        }
        for($i=1;$i<=2;$i++){
            $sql = "SELECT * FROM cms_file WHERE file_type={$i} AND id_cms=".$this->id_cms.' ORDER BY id_c_f ASC ';
            $sql_data =  Db::rowSQL($sql, true);
            if(!empty($sql)){
                $row = File::get($sql_data['id_file']);
                $row['file_dir'] = urldecode($row['file_dir'].$row['filename']);//本地位置
                if(strpos($row['file_dir'],'.js')){
                    $this->addJS($row['file_dir']);
                }else if(strpos($row['file_dir'],'.css')){
                    $this->addCSS($row['file_dir']);
                }
            }
        }
    }
}
