<?php
namespace web_app_okrent;
use \AppOkrentController;
use \Config;

class WelcomeController extends AppOkrentController
{
	public $page = 'welcome';
	
	public $tpl_folder;	//樣版資料夾
	public $definition;
	public $_errors_name;
	
	public function __construct(){
		$this->meta_title = $this->l('歡迎');
		$this->page_header_toolbar_title = $this->l('歡迎');
		$this->fields['page']['class'] = '';
		$this->className = 'WelcomeController';
		$this->no_FormTable              = true;
		$this->display_main_menu = true;
		$this->display_header = true;
		$this->display_footer = true;
		parent::__construct();
	}

	public function initProcess()
	{
		parent::initProcess();

		$app_welcome_scrolling = Config::get('app_welcome_scrolling');
		$this->context->smarty->assign([
			'app_welcome_scrolling'      => $app_welcome_scrolling,            //歡迎頁文字
		]);
	}

}