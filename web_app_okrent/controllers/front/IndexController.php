<?php

namespace web_app_okrent;
use \AppOkrentController;
use \Config;
use \FileUpload;
use \Db;
use \Tools;
use \County;
class IndexController extends \AppOkrentController
{
	public $page = 'index';
	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;
	public function __construct(){

		parent::__construct();
		$this->className                 = 'IndexController';
		$this->meta_title                = $this->l('首頁');
		$this->page_header_toolbar_title = $this->l('首頁');
	}

	public function initProcess(){
		$arr_index_banner   = [];
		$index_ratio = '40:23';
        $list_ratio     = '40:23';
		$n_ratio     = '40:23';
		$h_ratio     = '40:23';
		$r_ratio     = '40:23';

		$arr_id_file = Config::get('banner_index_top', true);
		$arr_index_top = FileUpload::get($arr_id_file);
		foreach($arr_index_top as $i => $v){
			$url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url = urldecode($url);
			$arr_index_banner[] = [
				'href' => '#',
				'img' => $url,
			];
		}

        //list sreach
		$arr_list_banner = [
		    [
                'href' => '/list',
                'img' => '/themes/App/img/1-1.jpg',
            ],
            [
                'href' => '/list?switch=1',
                'img' => '/themes/App/img/1-2.jpg',
            ],
            [
                'href' => '/list',
                'img' => '/themes/App/img/1-3.jpg',
            ]
        ];

//		print_r($arr_index_banner);
		$arr_id_file = Config::get('banner_index_news', true);
		$arr_index_news = FileUpload::get($arr_id_file);
		$arr_index_news_url = ["/coupon","/InLife","#slide_photo_3"];
		foreach($arr_index_news as $i => $v){
			$url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url = urldecode($url);
			$arr_coupon_banner[] = [
				'href' => $arr_index_news_url[$i],
				'img' => $url,
			];
		}

		$arr_id_file = Config::get('banner_index_service', true);
		$arr_index_service = FileUpload::get($arr_id_file);
		$arr_index_service_url = ['/managepage', '/handled', '/tax_benefit'];
		foreach($arr_index_service as $i => $v){
			$url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url = urldecode($url);
			$arr_rent_banner[] = [
				'href' => $arr_index_service_url[$i],
				'img' => $url,
			];
		}

		$arr_id_file = Config::get('banner_index_life', true);
		$arr_index_life = FileUpload::get($arr_id_file);
		$arr_index_life_url = ['/ExpertAdvisor', '/WealthSharing', '/JoinUs'];
		foreach($arr_index_life as $i => $v){
			$url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url = urldecode($url);
			$arr_life_banner[] = [
				'href' => $arr_index_life_url[$i],
				'img' => $url,
			];
		}
		//app work sreach_house

        $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
        $main_house_class = Db::rowSQL($sql);
        $rent_house_types = [];
        $rent_house_type = [];

        foreach($main_house_class as $k => $v){
            $sql = "SELECT * FROM rent_house_types WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%' Order by position ASC";
            $rent_house_types[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
            $sql = "SELECT * FROM rent_house_type WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%' Order by position ASC";
            $rent_house_type[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
        }

        $house_choose = [
            ["value"=>"0","title"=>"租屋"],
            ["value"=>"1","title"=>"售屋"],
        ];

        $json_house_choose = json_encode($house_choose,JSON_UNESCAPED_UNICODE);
        $json_rent_house_types = json_encode($rent_house_types,JSON_UNESCAPED_UNICODE);
        $json_rent_house_type = json_encode($rent_house_type,JSON_UNESCAPED_UNICODE);
        $json_main_house_class = json_encode($main_house_class,JSON_UNESCAPED_UNICODE);
        $json_county = json_encode(County::getContext()->form_list_county(),JSON_UNESCAPED_UNICODE);
        $json_city = json_encode(County::getContext()->form_list_city(),JSON_UNESCAPED_UNICODE);

        $js=<<<js
<script>
    $(".ui-link-house_choose-txt").click(
			function(){
			    var value = $("input[name='house_choose']").val();
				var main_item = "house_choose";
				$(".ui-link-"+main_item).css("display","block");
				if($("[id*='"+main_item+"']").length > 0){
				    list_each(main_item,value);
				}
			});

    $(".ui-link-house_choose").scroll(function (){
		$.each($(".house_chooses"),function(index,c){
		    citys(c,index)
		});
});

    $(".ui-link-main_house_class-txt").click(
			function(){
			    var value = $("input[name='id_main_house_class']").val();
				var main_item = "main_house_class";
				$(".ui-link-"+main_item).css("display","block");
				if($("[id*='"+main_item+"']").length > 0){
				    list_each(main_item,value);
				}
			});
    $(".ui-link-main_house_class").scroll(function (){
		$.each($(".main_house_classs"),function(index,c){
		    citys(c,index)
		});
});

        $(".ui-link-types-txt").click(
			function(){
			        var value = $("input[name='id_types']").val();
                    var main_item = "types";
                    $(".ui-link-"+main_item).css("display","block");
                    if($("[id*='"+main_item+"']").length > 0){
                    list_each(main_item,value);
				}
			});
    $(".ui-link-types").scroll(function (){
		$.each($(".typess"),function(index,c){
		    citys(c,index)
		});
});

    $(".ui-link-type-txt").click(
        function(){
            var value = $("input[name='id_type']").val();
            var main_item = "type";
            $(".ui-link-"+main_item).css("display","block");
            if($("[id*='"+main_item+"']").length > 0){
				list_each(main_item,value);
            }
        });
    $(".ui-link-type").scroll(function (){
		$.each($(".types"),function(index,c){
		    citys(c,index)
		});
});

    $(".ui-link-county-txt").click(
        function(){
            var value = $("input[name='county']").val();
            var main_item = "county";
            $(".ui-link-"+main_item).css("display","block");
            if($("[id*='"+main_item+"']").length > 0){
				list_each(main_item,value);
            }
        });

    $(".ui-link-county").scroll(function (){
		$.each($(".countys"),function(index,c){
		    citys(c,index)
		});
	});

    function city_data(){
        var value = $("input[name='city']").val();
        var level_id_city = {$json_city};//city的data
            //這邊才建立city的data
            create_list("city",level_id_city,value);
            var main_item = "city";
            $(".ui-link-city").css("display","block");
            $(".ui-link-city_b").css("display","block");
            if($("[id*='"+main_item+"']").length > 0){
				list_each(main_item,value);
            }
    }

    $(".ui-link-city").scroll(function (){
		$.each($(".citys"),function(index,c){
		    citys(c,index)
		});
});

    $(".search_btn").click(//轉頁
        function(){
            $("#search_form").submit();
        });

    $(document).ready(function(){
        var level_house_choose = {$json_house_choose};
        var level_id_main_class = {$json_main_house_class};
        var level_id_types = {$json_rent_house_types};
        var level_id_type = {$json_rent_house_type};
        var level_id_county = {$json_county};
        var level_id_city = {$json_city};

        create_list("house_choose",level_house_choose,$("input[name='house_choose']").val());
        create_list("main_house_class",level_id_main_class,$("input[name='id_main_house_class']").val());
        create_list("types",level_id_types,$("input[name='id_types']").val());
        create_list("type",level_id_type,$("input[name='id_type']").val());
        create_list("county",level_id_county,$("input[name='county']").val());
});
</script>
js;


		$this->context->smarty->assign([
			'arr_index_banner' => $arr_index_banner,
            'arr_list_banner' =>$arr_list_banner,
			'arr_coupon_banner' => $arr_coupon_banner,
			'arr_life_banner' => $arr_life_banner,
			'arr_rent_banner' => $arr_rent_banner,
			'index_ratio' => $index_ratio,
			'list_ratio' => $list_ratio,
			'n_ratio'     => $n_ratio,
			'h_ratio'     => $h_ratio,
			'r_ratio'     => $r_ratio,
            'js'          => $js,
		]);
        parent::initProcess();
        //店長要求到相對印的關注
        $menu_buoy = [
            [
                'title'=>'房地專區',
                'active' => ['/index','/','#'],
                'url' =>'#slide_photo_1'
            ],
            [
                'title'=>'樂租管理',
                'active' => [''],
                'url' =>'#slide_photo_2'
            ],
            [
                'title'=>'NEWS好康',
                'active' => [''],
                'url' =>'#slide_photo_3'
            ],
            [
                'title'=>'生活家族',
                'active' => [''],
                'url' =>'#slide_photo_4'
            ]
        ];

        $this->context->smarty->assign([
            'menu_buoy'  =>$menu_buoy,
        ]);
	}

    public function setMedia(){
	    parent::setMedia();
        $this->addCSS(THEME_URL . '/css/search_house.css');
		$this->addJS(THEME_URL . '/js/search_house.js');

//        list
//        $this->addCSS(THEME_URL . '/css/search_list.css');
//        $this->addJS(THEME_URL . '/js/search_list.js');
//        $this->addCSS( '/css/metro-all.css');
//        $this->addJS( '/js/metro.min.js');
    }
}
