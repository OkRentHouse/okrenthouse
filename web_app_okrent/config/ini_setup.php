<?php
session_start();

/** 設定區開始 */

@ini_set('display_errors', 'off' );							//Warning 錯誤警告顯示
@ini_set('expose_php', 'off');									//隱藏php 版本
@ini_set('memory_limit','512M');								//記憶體用量上限
@ini_set('upload_max_filesize','128M');						//上傳檔案大小上限（單一檔案大小）
@ini_set('post_max_size','128M');								//POST 大小上限（所有檔案大小加總）
@ini_set('max_execution_time', 30);							//回應時間
@ini_set('max_input_time', 1000);								//回應時間

error_reporting(E_ALL & ~ E_NOTICE);										//關閉錯誤顯示

/**
 *	安裝時設定
 */
date_default_timezone_set( 'Asia/Taipei' );						//設定地區時間
define('WEB_DNS', $_SERVER['HTTP_HOST']);	//網站DNS (不含http://)

//常數宣告
define('OS', ( strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ) ? 'Windows' : 'Linux');//伺服器作業系統
define('CACHE',			true);														//啟用快取

define('POWERED_BY',			'LilyHouse');										//Powered-by
define('WEB_NAME',				'生活資產管理股份有限公司');						//網站名稱

define('WEB_LANGIAGE',			'zh-tw');											//網站語言
define('SYSTEM_FOLDER',			'web_app');											//系統文件夾
define('WEB_VERSION',			'1.0.0.7');											//系統文件夾
define('NAVIGATION_PIPE',		'│');												//網站標題導航管管
define('PROJECT_FOLDER', 		'IlifeHouse');										//專案資料夾

/**
 * 第二網站資料夾位置
 */
define('WEB_DIR2', DS . 'home' . DS . 'lilyhous' . DS . 'public_html' . DS . 'rps.lilyhouse.com.tw');
define('WEB_DNS2', 'rps.lilyhouse.com.tw');
/** 設定區結束 */
define('ADMIN_URL', 'manage');                                            //系統管理者後台資料夾位
define('MANAGE_URL', 'member2');                                            //後台豋入位置置
define('MODULE_URL', '/modules');                                        //模組資料夾位置
define('MIN_MODULE_URL', 'm');                                                //模組短址
define('FILE_DIR', 'file');                                            //file資料夾位置
define('FILE_URL', '/file');                                            //file網址路徑
define('ADMIN_DEFAULT_THEME', 'default');                                            //樣版資料夾位置
define('MANAGE_DIR', WEB_DIR . DS . MANAGE_URL);                        //一般管理者後台資料夾位置
define('MODULE_DIR', WEB_DIR . DS . 'modules');                            //模組資料夾位置
define('CLASS_DIR', WEB_DIR . DS . 'classes');        //Class資料夾位置
define('TOOL_DIR', WEB_DIR . DS . 'tools');            //工具資料夾位置
define('DEPOT_DIR', WEB_DIR . DS . 'depot');            //depot資料夾位置
define('ADMIN_DIR', WEB_DIR . DS . 'manage');                     //系統管理者後台資料夾位置
define('DEFAULT_THEME', 'default');                                            //選擇的樣版
define('MEDIA_URL', 'media');                                            //

define('CONFIG_DIR', WEB_DIR . DS . SYSTEM_FOLDER . DS . 'config');            //設置資料夾位置
define('WORK_DIR', WEB_DIR . DS . SYSTEM_FOLDER . DS . 'work');            //工作資料夾位置
define('TRANSLATE_DIR', WEB_DIR . DS . SYSTEM_FOLDER . DS . 'translates');        //譯文資料夾位置
define('ADMIN_THEME_DIR', ADMIN_DIR . DS . 'themes' . DS);                        //樣版資料夾位置
define('CACHE_DIR', WEB_DIR . DS . SYSTEM_FOLDER . DS . 'cache');            //快取資料夾位置
define('CONTROLLERS_DIR', WEB_DIR . DS . SYSTEM_FOLDER . DS . 'controllers');    //controllers資料夾位置
define('OVERRIDE_DIR', WEB_DIR . DS . SYSTEM_FOLDER . DS . 'override');        //覆蓋
define('OVERRIDE_THEME_DIR', OVERRIDE_DIR . DS . 'front' . DS . 'templates');            //覆蓋樣版資料夾位置

define('PLANNED_IMG', WEB_DIR . DS . 'img' . DS . 'planned');                    //工程進度圖片位置
define('JS_DIR', WEB_DIR . DS . 'js');                                    //Javascript資料夾位置
define('CSS_DIR', WEB_DIR . DS . 'css');                                //CSS資料夾位置
define('IMG_DIR', WEB_DIR . DS . 'img');                                //image資料夾位置
define('JQUERY_VERSION', '1.11.3');                                            //JQuery
define('URL_VERIFICATION', 'verification');                                    //驗證碼
define('URL_BARCODE', 'barcode');                                            //bar code
define('DEFAULT_PASS', '0000');                                            //預設密碼
define('PAGE_NUM', 10);                                                //分頁顯示數
//分頁顯示數

define('RESERVED_URL', 			serialize(array('word', 'execl')));					//URL保留字

define('URL_DOWNLOAD',			'/download/');										//下載網址
define('WEB_ALLOW_MOBILE_DEVICE', 3);												//1:只有手機 2:只有平版 3:開放手機、平版
define('BEFORE_HASH', 			'X2m7NE3ckW!aZ#y#oKw!');							//前綴雜湊
define('AFTER_HASH', 			'fjo@40fql*0flrp;xkjfwgv');							//後綴雜湊

define('STORE_IMG_DIR', 'img'.DS.'store');                            //商家圖片資料夾位置
define('STORE_IMG_URL', '/img/store/');                          	  //商家圖片網址路徑
