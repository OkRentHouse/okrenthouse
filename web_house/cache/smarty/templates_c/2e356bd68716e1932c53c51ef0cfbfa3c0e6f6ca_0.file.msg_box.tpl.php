<?php
/* Smarty version 3.1.28, created on 2019-10-17 12:03:52
  from "/home/ilifehou/life-house.com.tw/themes/LifeHouse/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5da7e8289c2463_63197025',
  'file_dependency' => 
  array (
    '2e356bd68716e1932c53c51ef0cfbfa3c0e6f6ca' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/LifeHouse/msg_box.tpl',
      1 => 1533802178,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5da7e8289c2463_63197025 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
<div class="alert alert-success <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
<div class="alert alert-danger <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
}
}
