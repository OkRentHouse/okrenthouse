<?php
/* Smarty version 3.1.28, created on 2019-10-17 12:04:19
  from "/home/ilifehou/life-house.com.tw/themes/LifeHouse/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5da7e8431c6674_52587535',
  'file_dependency' => 
  array (
    'dbe50faa9d9cdd893b28a53c2495450ae6ae933e' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/LifeHouse/controllers/Index/content.tpl',
      1 => 1571284473,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5da7e8431c6674_52587535 ($_smarty_tpl) {
?>
<div class="gridContainer">
	<div id="header">
		<div id="header_wrapper">
			<img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/logo.png"/>

		</div>
	</div>
	<div id="NAV"><a href="lifehouse://life-house/open">
			<div>生活資產</div>
		</a><a href="https://okrent.tw">
			<div>生活樂租</div>
		</a><a href="#">
			<div>生活房屋</div>
		</a><a href="#">
			<div>生活裝修設計</div>
		</a><a href="#">
			<div>加盟生活</div>
		</a><a href="#">
			<div>in生活</div>
		</a><a href="#">
			<div>生活家族</div>
		</a>
	</div>
	<div class="body">
		<div class="main_bar">
			<a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/a.png" alt="樂租達人 強力募集 創造被動式永續收入"/></a>
			<a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/b.png" alt="生活好康 LIFE OFFER 特約商店 好康優惠看這裡"/></a>
			<a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/c.png" alt="生活加盟 領導品牌優勢 獨特管理服務 讓您贏得先機"/></a>
		</div>
		<div class="friendly_link">
			<a href="https://www.facebook.com/桃園市租任管理服務業從業人員職業工會-600320820503706" target="_blank"><img
						src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/tyr.png" alt="桃園市租賃管理服務業從業人員職業工會" title="桃園市租賃管理服務業從業人員職業工會"/></a>
			<a href="https://www.cathay-ins.com.tw/insurance" target="_blank"><img
						src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/cathay.png"
						alt="國泰產險" title="國泰產險"/></a>
			<a href="http://www.tycrehda.org.tw" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/leanju.png"
																	  alt="桃園市樂安居住宅發展促進協會" title="桃園市樂安居住宅發展促進協會"/></a>
			<a href="http://www.ctop.tw/" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/ctop.png" alt="僑馥建經"
															   title="僑馥建經"/></a>
			<a href="https://www.hoan.com.tw/ " target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/hoan.png" alt="和安保險"
																	 title="和安保險"/></a>
			<a href="http://www.pcss.com.tw/" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/welcom/pcss.png" alt="保成務業保全"
																   title="保成務業保全"/></a>
		</div>
	</div>
	<div id="footer">
		<p id="Designedby"><strong><a href="/">生活資產管理股份有限公司</a></strong> Life Property Investment Management CO.,LTD.
		</p>
		<p id="Copyright">生活集團版權所有 Copyright© Life Group 2000 All right reservde</p>
	</div>
</div><?php }
}
