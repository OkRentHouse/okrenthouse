<link href="/media/bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css">
<script src="/media/bootstrap-fileinput-master/js/plugins/sortable.min.js"></script>
<script src="/media/bootstrap-fileinput-master/js/plugins/purify.min.js"></script>
<script src="/media/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script src="/media/bootstrap-fileinput-master/js/locales/zh-TW.js"></script>

<label class="control-label">上傳檔案</label>
<input id="input-pa" name="input-pa[]" type="file" multiple class="file-loading" accept="image/*" capture="camera">
<script>
var initialPreview = [];
var initialPreviewConfig = [];
var UpFileVal = [];

{if !empty($initialPreview)}initialPreview = {$initialPreview};{/if}
{if !empty($initialPreviewConfig)}initialPreviewConfig = {$initialPreviewConfig};{/if}
{if !empty($UpFileVal)}UpFileVal = {$UpFileVal};{/if}
{literal}
$('#input-pa').fileinput({
    uploadUrl: document.location.pathname,
    uploadAsync: false,
    autoUpload : true,
    language : 'zh-TW',
    minFileCount: 1,	//最小上傳數量
    maxFileCount: 'auto',	//最大上傳數量
    overwriteInitial: false,
    initialPreview: initialPreview,
    initialPreviewAsData: true, // defaults markup
    initialPreviewFileType: 'image', // 默認type
    initialPreviewConfig: initialPreviewConfig,
    uploadExtraData: {
	  ajax: true,
	  action : 'UpFile',
	  UpFileVal: UpFileVal
    }
}).on('filesorted', function(e, params) {
    console.log('File sorted params', params);
}).on('fileuploaded', function(e, params) {
    console.log('File uploaded params', params);
}).on("filebatchselected", function(e, params) {
    console.log('filebatchselected', params);
});
{/literal}
</script>