<div class="tab-count file_tab tabs_group_{$group['id_group']} col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="{$group.id_group}">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="icon-cogs"></i>{l s="檔案權限"}</div>
		<div class="panel-body">
			<table>
				<tbody>
				<div class="btn-group" data-toggle="buttons">
					<tr class="h6">
						<td>{l s="上傳"}</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function{if $arr_file_up[$group.id_group] == '0'} active{/if}" for="file_up_{$group.id_group}_0"><input type="radio" id="file_up_{$group.id_group}_0" name="file_up_{$group.id_group}" data-fun="file_up" value="0"{if $arr_file_up[$group.id_group] == '0'} checked="checked"{/if}>{l s="關閉"}</label>
								<label class="btn btn btn-default on_ajax_function{if $arr_file_up[$group.id_group] == '1'} active{/if}" for="file_up_{$group.id_group}_1"><input type="radio" id="file_up_{$group.id_group}_1" name="file_up_{$group.id_group}" data-fun="file_up" value="1"{if $arr_file_up[$group.id_group] == '1'} checked="checked"{/if}>{l s="啟用"}</label>
							</div>
						</td>
					</tr>
					<tr class="h6">
						<td>{l s="下載"}</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function{if $arr_file_download[$group.id_group] == '0'} active{/if}" for="file_download_{$group.id_group}_0"><input type="radio" id="file_download_{$group.id_group}_0" name="file_download_{$group.id_group}" data-fun="file_download" value="0"{if $arr_file_move[$group.id_group] == '0'} checked="checked"{/if}>{l s="關閉"}</label>
								<label class="btn btn btn-default on_ajax_function{if $arr_file_download[$group.id_group] == '1'} active{/if}" for="file_download_{$group.id_group}_1"><input type="radio" id="file_download_{$group.id_group}_1" name="file_download_{$group.id_group}" data-fun="file_download" value="1"{if $arr_file_move[$group.id_group] == '1'} checked="checked"{/if}>{l s="啟用"}</label>
							</div>
						</td>
					</tr>
					<tr class="h6">
						<td>{l s="移動"}</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function{if $arr_file_move[$group.id_group] == '0'} active{/if}" for="file_move_{$group.id_group}_0"><input type="radio" id="file_move_{$group.id_group}_0" name="file_move_{$group.id_group}" data-fun="file_move" value="0"{if $arr_file_move[$group.id_group] == '0'} checked="checked"{/if}>{l s="關閉"}</label>
								<label class="btn btn btn-default on_ajax_function{if $arr_file_move[$group.id_group] == '1'} active{/if}" for="file_move_{$group.id_group}_1"><input type="radio" id="file_move_{$group.id_group}_1" name="file_move_{$group.id_group}" data-fun="file_move" value="1"{if $arr_file_move[$group.id_group] == '1'} checked="checked"{/if}>{l s="啟用"}</label>
							</div>
						</td>
					</tr>
					<tr class="h6">
						<td>{l s="刪除"}</td>
						<td>
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn btn-default on_ajax_function{if $arr_file_del[$group.id_group] == '0'} active{/if}" for="file_del_{$group.id_group}_0"><input type="radio" id="file_del_{$group.id_group}_0" name="file_del_{$group.id_group}" data-fun="file_del" value="0"{if $arr_file_del[$group.id_group] == '0'} checked="checked"{/if}>{l s="關閉"}</label>
								<label class="btn btn btn-default on_ajax_function{if $arr_file_del[$group.id_group] == '1'} active{/if}" for="file_del_{$group.id_group}_1"><input type="radio" id="file_del_{$group.id_group}_1" name="file_del_{$group.id_group}" data-fun="file_del" value="1"{if $arr_file_del[$group.id_group] == '1'} checked="checked"{/if}>{l s="啟用"}</label>
							</div>
						</td>
					</tr>
				</div>
				</tbody>
			</table>
		</div>
	</div>
</div>