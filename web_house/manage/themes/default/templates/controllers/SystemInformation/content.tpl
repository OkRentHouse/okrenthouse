<div class="col-lg-6">
	<div>
		<div class="panel panel-default">
			<div class="panel-heading">{l s="伺服器資訊"}</div>
			<div class="panel-body">
				<p><strong>{l s="伺服器資訊:"}</strong>{$information.server.server_information}</p>
				<p><strong>{l s="伺服器軟體版本:"}</strong>{$information.server.server}</p>
				<p><strong>{l s="PHP版本:"}</strong>{$information.server.php}</p>
				<p><strong>{l s="Memory limit:"}</strong>{$information.server.memory_limit}</p>
				<p><strong>{l s="Max execution time:"}</strong>{$information.server.max_execution_time}</p>
			</div>
		</div>
	</div><div>
		<div class="panel panel-default">
			<div class="panel-heading">{l s="資料庫資訊"}</div>
			<div class="panel-body">
				<p><strong>{l s="MySQL 版本:"}</strong>{$information.data.version}</p>
				<p><strong>{l s="MySQL 伺服器:"}</strong>{$information.data.server}</p>
				<p><strong>{l s="MySQL 資料庫名稱:"}</strong>{$information.data.name}</p>
				<p><strong>{l s="MySQL 使用者名稱:"}</strong>{$information.data.user}</p>
				<p><strong>{l s="MySQL Tables prefix:"}</strong>{$information.data.tables_prefix}</p>
				<p><strong>{l s="MySQL 引擎:"}</strong>{$information.data.engine}</p>
				<p><strong>{l s="MySQL 驅動程序:"}</strong>{$information.data.driver}</p>
			</div>
		</div>
	</div><div>
		<div class="panel panel-default">
			<div class="panel-heading">{l s="MAIL 資訊"}</div>
			<div class="panel-body">
				<p><strong>{l s="郵件方法:"}</strong>{$information.mail.driver}</p>
				<p><strong>{l s="SMTP服務器:"}</strong>{$information.mail.driver}</p>
				<p><strong>{l s="SMTP用戶名:"}</strong>{$information.mail.driver}</p>
				<p><strong>{l s="SMTP密碼:"}</strong>{$information.mail.driver}</p>
				<p><strong>{l s="加密:"}</strong>{$information.mail.driver}</p>
				<p><strong>{l s="SMTP端口:"}</strong>{$information.mail.driver}</p>
			</div>
		</div>
	</div>
</div><div class="col-lg-6">
	<div>
		<div class="panel panel-default">
			<div class="panel-heading">{l s="網站資訊"}</div>
			<div class="panel-body">
				<p><strong>{l s="網站名稱:"}</strong>{$information.web.web_name}</p>
				<p><strong>{l s="網站版本:"}</strong>{$information.web.web_version}</p>
				<p><strong>{l s="Dinaub name:"}</strong>{$information.web.web_dns}</p>
			</div>
		</div>
	</div><div>
		<div class="panel panel-default">
			<div class="panel-heading">{l s="您的資訊"}</div>
			<div class="panel-body">
				<p><strong>{l s="您的網路瀏覽器:"}</strong>{$information.user.agent}</p>
			</div>
		</div>
	</div>
</div>