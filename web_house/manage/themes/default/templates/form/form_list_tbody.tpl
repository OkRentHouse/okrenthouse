<tbody>{strip}
{if count($fields.list_val) > 0}
{foreach from=$fields.list_val key=fi item=list}
	<tr id="tr_{$fi}"{if isset($list.tr_class)} class="{$list.tr_class}"{/if}>
      {foreach from=$fields.list key=i item=thead}
		{$i = str_replace('!', '_', $i)}
		{if isset($thead.index)}{$list.index=$list.$i}{/if}
		{if isset($thead.index2)}{$list.index2=$list.$i}{/if}
      	<td class="{$thead.class}{if $thead.hidden} hidden{/if}{if !$thead.no_link && !$no_link && !isset($thead['type'])} pointer{/if}" {if !$thead.no_link && !$no_link && !isset($thead['type']) && ($i != 'position')}onclick="document.location='{$form_action|escape:'html':'UTF-8'}{if in_array('list', $actions)}&amp;list{$table}{elseif in_array('view', $actions) && in_array('view', $post_processing)}&amp;view{$table}{elseif in_array('edit', $actions)}&amp;edit{$table}{elseif in_array('view', $actions)}&amp;view{$table}{/if}&amp;{$fields.index}={$list.index}'"{/if}>
            {if isset($thead['type'])}
			{if $thead['type'] == 'icon'}
				<i class="{$list.icon}"></i>
			{else}
				<input type="{$thead.type}" name="{$i}[]" value="{$list.index}">
			{/if}
            {else}
            	{if isset($thead.values)}
				{$in_arr=false}
				{if isset($thead.in_table) || $thead.in_table}
					{$list.$i}
				{else}
					{foreach $thead.values key=thv_i item=val_v}
						{if $list.$i == $val_v.value}<samp class="{$val_v.class}">{$val_v.title}</samp>{$in_arr=true}{break}{/if}
					{/foreach}
					{if $in_arr == false}<samp>-</samp>{/if}
				{/if}
			{else}
				{if isset($thead.number_format)}{$list.$i|number_format:$thead.number_format:"0":","}{elseif empty($list.$i) || $list.$i == '0000-00-00' || $list.$i == '0000-00-00 00:00:00'}<samp>-</samp>{else}{$list.$i}{/if}
			{/if}
            {/if}
            </td>
	{/foreach}
      {if $has_actions && $display_edit_div}
            <td class="text-center edit_div">{if in_array('preview', $actions)}
 <a class="btn btn-default preview" href="{$form_action}&preview{$table}&{$fields.index}={$list.index}{if isset($fields.index2)}&{$fields.index2}={$list.index2}{/if}">{l s="預覽"}</a>
{/if}{if in_array('edit', $actions) && in_array('view', $actions) && !$list.no_edit}
 <a class="btn btn-default edit" href="{$form_action}&edit{$table}&{$fields.index}={$list.index}{if isset($fields.index2)}&{$fields.index2}={$list.index2}{/if}">{l s="修改"}</a>
{elseif in_array('view', $actions)}
 <a class="btn btn-default view" href="{$form_action}&view{$table}&{$fields.index}={$list.index}{if isset($fields.index2)}&{$fields.index2}={$list.index2}{/if}">{l s="檢視"}</a>
{/if}{if in_array('del', $actions)}
 <a class="btn btn-default del" href="{$form_action}&del{$table}&{$fields.index}={$list.index}{if isset($fields.index2)}&{$fields.index2}={$list.index2}{/if}">{l s="刪除"}</a>
{/if}
            </td>
	{/if}
	</tr>
{/foreach}
{else}
<tr><td class="text-center" colspan="{if $has_actions && $display_edit_div}{count($fields.list)+1}{else}{count($fields.list)}{/if}">{$no_information}</td><tr>
{/if}{/strip}
</tbody>