$(document).ready(function(){
	all_checkbox('.all_view', '.type_view');
	all_checkbox('.all_add', '.type_add');
	all_checkbox('.all_edit', '.type_edit');
	all_checkbox('.all_del', '.type_del');
	all_checkbox('.all_all', '.type_all');

	$('.type_all').change(function(){
		var _all_che_class = $(this);
		var _che_class = $('input[class!="on_ajax type_all"]:enabled', _all_che_class.parents('tr'));
		if(_all_che_class.prop('checked')){
			_che_class.prop('checked',true);
			_che_class.parent('label').addClass('label_checkbox');
		}else{
			_che_class.prop('checked',false);
			_che_class.parent('label').removeClass('label_checkbox');
		};
	});

	$('input[class!="on_ajax type_all"]').change(function(){
		var tr = $(this).parents('tr');
		var _all_che_class = $('.type_all', tr);
		var _che_class = $('input[class!="on_ajax type_all"]', tr);

		if($(this).prop('checked')){
			$(this).parent('label').addClass('label_checkbox');
		}else{
			$(this).parent('label').removeClass('label_checkbox');
		};
		if(_che_class.length == $('input[class!="on_ajax type_all"]:checked', tr).length){
			_all_che_class.prop('checked',true);
		}else{
			_all_che_class.prop('checked',false);
		};
	});

	$('.all_all').change(function(){
		var _all_che_class = $(this);
		var _che_class = $('input[class!="on_ajax all_all"]', _all_che_class.parents('table'));
		if(_all_che_class.prop('checked')){
			_che_class.prop('checked',true);
			_che_class.parent('label').addClass('label_checkbox');
		}else{
			_che_class.prop('checked',false);
			_che_class.parent('label').removeClass('label_checkbox');
		};
	});
}).on('change', '.on_ajax', function(){
	edit_competence($(this));
});
/*
更新權限
 */
function edit_competence($this){
	var id_group = $this.parents('div[data-id_group]').data('id_group');
	var id_tab = $this.parents('tr[data-id_tab]').data('id_tab');
	var id_parent = $this.parents('tr[data-id_parent]').data('id_parent');
	var type = $this.val();
	var enabled = $this.is(':checked') ? 1 : 0;
	$.ajax({
		url : document.location.pathname,
		method : 'get',
		data : {
			'ajax'	: true,
			'action'	: 'EditCompetence',
			'id_group'	: id_group,
			'id_tab'	: id_tab,
			'id_parent'	: id_parent,
			'type'	: type,
			'enabled'	: enabled
		},
		dataType : 'json',
		success: function(msg){
			if(msg.type){
				$.growl.notice({
					title: '',
					message: '更新成功!'
				});
			}else{
				$this.prop('checked', !enabled);
				$.growl.error({
					title: '',
					message: msg.error
				});
			};
		},
		error:function(xhr, ajaxOptions, thrownError){
			$this.prop('checked', !enabled);
			$.growl.error({
				title: '',
				message: '更新失敗!'
			});
		}
	});

};
function on_checkbox(is_group, id_tab, id_parent, type, enabled){

};