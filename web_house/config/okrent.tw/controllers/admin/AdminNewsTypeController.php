<?php
class AdminNewsTypeController extends AdminController
{
	public function __construct(){
		$this->className = 'AdminNewsTypeController';
		$this->table = 'news_type';
		$this->fields['index'] = 'id_news_type';
		$this->fields['title'] = '公告類別';
		$this->fields['order'] = ' ORDER BY `news_type` ASC';
		
		if($_SESSION['id_group'] != 1){
			if($_SESSION['admin'] != 1){		//非系統管理者
				if($_SESSION['id_web']){
					$where = ' AND `id_web` = '.$_SESSION['id_web'];
					$this->fields['where'] = ' AND `id_web` = '.$_SESSION['id_web'];
				}
			}
		}
		
		$this->fields['list'] = array(
			'id_news_type' => array(
				'index' => true,
				'title' => $this->l('ID'),
				'type' => 'checkbox',
				'hidden' => true,
				'class' => 'text-center'
			),
			'id_web' => array(
				'title' => $this->l('社區名稱'),
				'order' => true,
				'filter' => true,
				'key' => array(
					'table' => 'web',
					'key' => 'id_web',
					'val' => 'web',
					'where' => $where,
					'order' => '`web` ASC'
				),
				'class' => 'text-center'
			),
			'news_type' => array(
				'title' => $this->l('公告類別'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'class' => array(
				'title' => $this->l('顏色'),
				'order' => true,
				'filter' => true,
				'values' => array(
					array(
						'class' => 'p_red',
						'value' => 'p_red',
						'title' => $this->l('粉紅')
					),
					array(
						'class' => 'p_yellow',
						'value' => 'p_yellow',
						'title' => $this->l('粉黃')
					),
					array(
						'class' => 'p_blue',
						'value' => 'p_blue',
						'title' => $this->l('粉藍')
					),
					array(
						'class' => 'p_green',
						'value' => 'p_green',
						'title' => $this->l('粉綠')
					),
					array(
						'class' => 'p_gray',
						'value' => 'p_gray',
						'title' => $this->l('灰色')
					),
				),
				'class' => 'text-center'
			),
		);
		
		$this->fields['form'] = array(
			'news_type' => array(
				'legend' => array(
					'title' => $this->l('公告類別'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					'id_news_type' => array(
						'name' => 'id_news_type',
						'type' => 'hidden',
						'index' => true,
						'label' => $this->l('類別'),
						'required' => true
					),
					'id_web' => array(
						'name' => 'id_web',
						'type' => 'select',
						'label' => $this->l('社區名稱'),
						'options' => array(
							'default' => array(
								'text' => '所有社區共用',
								'val' => ''
							),
							'table' => 'web',
							'text' => 'web',
							'value' => 'id_web',
							'where' => $where,
							'order' => '`web` ASC'
						),
					),
					'news_type' => array(
						'name' => 'news_type',
						'type' => 'text',
						'label' => $this->l('公告類別'),
						'maxlength' => '20',
						'required' => true
					),
					'class' => array(
						'name' => 'class',
						'type' => 'switch',
						'label' => $this->l('顏色'),
						'values' => array(
							array(
								'id' => 'p_red',
								'value' => 'p_red',
								'label' => $this->l('粉紅')
							),
							array(
								'id' => 'p_yellow',
								'value' => 'p_yellow',
								'label' => $this->l('粉黃')
							),
							array(
								'id' => 'p_blue',
								'value' => 'p_blue',
								'label' => $this->l('粉藍')
							),
							array(
								'id' => 'p_green',
								'value' => 'p_green',
								'label' => $this->l('粉綠')
							),
							array(
								'id' => 'p_gray',
								'value' => 'p_gray',
								'label' => $this->l('灰色')
							),
						),
						'maxlength' => '20',
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					),
				),
				'cancel' => array(
					'title' => $this->l('取消')),
				'reset' => array(
					'title' => $this->l('復原'))
			)
		);

		if($_SESSION['id_group'] != 1){
			if($_SESSION['admin'] != 1) {        //非系統管理者
				$this->fields['list']['id_web']['hidden'] = true;
				$this->fields['form']['news_type']['input']['id_web']['type'] = 'hidden';
				unset($this->fields['form']['news_type']['input']['id_web']['options']['default']);
			}
		}
		parent::__construct();
	}
	
}