<?php

class AdminMemberController extends AdminController
{
	public function __construct()
	{
		$this->className       = 'AdminMemberController';
		$this->table           = 'member';
		$this->fields['index'] = 'id_member';
		$this->fields['title'] = '會員管理';

		$this->fields['list'] = [
			'id_member' => [
				'index'  => true,
				'title'  => $this->l('會員編號'),
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'email'     => [
				'title'  => $this->l('帳號'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'name'      => [
				'title'  => $this->l('姓名'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'gender'    => [
				'title'  => $this->l('性別'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
				'values' => [
					[
						'class' => 'gender1',
						'value' => 1,
						'title' => $this->l('男'),
					],
					[
						'class' => 'gender0',
						'value' => 0,
						'title' => $this->l('女'),
					],
				],
			],
			'tel'       => [
				'title'  => $this->l('連絡電話'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'time'      => [
				'title'  => $this->l('連絡時間'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'barcode'   => [
				'show'   => false,
				'title'  => $this->l('會員條碼'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'active'    => [
				'title'  => $this->l('啟用狀態'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'date_time' => [
				'title'  => $this->l('註冊日期'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
		];

		$this->fields['list_num'] = 100;

		$this->fields['form'] = [
			'member' => [
				'tab'    => 'member',
				'legend' => [
					'title' => $this->l('會員管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_member'        => [
						'name'     => 'id_member',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'email'            => [
						'name'        => 'email',
						'type'        => 'email',
						'label'       => $this->l('帳號'),
						'placeholder' => $this->l('帳號'),
						'maxlength'   => '100',
						'required'    => true,
					],
					'change_password'  => [
						'type'        => 'change-password',
						'label'       => $this->l('修改密碼'),
						'placeholder' => $this->l('修改密碼'),
						'col'         => 5,
						'minlength'   => '8',
						'maxlength'   => '20',
						'name'        => 'password',
					],
					'password'         => [
						'type'        => 'new-password',
						'label'       => $this->l('密碼'),
						'placeholder' => $this->l('密碼'),
						'required'    => true,
						'minlength'   => '8',
						'maxlength'   => '20',
						'name'        => 'password',
					],
					'confirm_password' => [
						'type'        => 'confirm-password',
						'label'       => $this->l('確認密碼'),
						'placeholder' => $this->l('確認密碼'),
						'required'    => true,
						'minlength'   => '8',
						'maxlength'   => '20',
						'name'        => 'password2',
						'confirm'     => 'password',
					],
					'name'             => [
						'name'        => 'name',
						'type'        => 'text',
						'label'       => $this->l('姓名'),
						'placeholder' => $this->l('姓名'),
						'p'           => $this->l('系統收發信件依據，請填寫正確姓名'),
						'hint'        => $this->l('系統收發信件依據，請填寫正確姓名'),
						'required'    => true,
						'maxlength'   => '20',
					],
					'gender'           => [
						'name'     => 'gender',
						'type'     => 'switch',
						'label'    => $this->l('性別'),
						'required' => true,
						'val'      => 1,
						'values'   => [
							[
								'id'    => 'gender1',
								'value' => 1,
								'label' => $this->l('男'),
							],
							[
								'id'    => 'gender0',
								'value' => 0,
								'label' => $this->l('女'),
							],
						],
					],
					'tel'              => [
						'name'        => 'tel',
						'type'        => 'text',
						'label'       => $this->l('連絡電話'),
						'placeholder' => $this->l('連絡電話'),
						'maxlength'   => '50',
					],
					'time'             => [
						'name'        => 'time',
						'type'        => 'text',
						'label'       => $this->l('連絡時間'),
						'placeholder' => $this->l('連絡時間'),
						'maxlength'   => '50',
					],
					'barcode_img'      => [
						'no_action' => true,
						'type'      => 'html',
						'html'      => '<img id="barcode_img" src="" title="' . $this->l('複製') . '" class="pointer">',
					],
					'barcode'          => [
						'name'      => 'barcode',
						'type'      => 'view',
						'label'     => $this->l('會員條碼'),
						'p'         => $this->l('會員條碼僅供社區註冊用'),
						'maxlength' => '16',
					],
					'date_time'        => [
						'name'          => 'date_time',
						'type'          => 'view',
						'auto_datetime' => 'add',
						'label'         => $this->l('註冊日期'),
						'maxlength'     => '50',
					],
					'active'           => [
						'type'   => 'switch',
						'label'  => $this->l('啟用狀態'),
						'name'   => 'active',
						'val'    => 1,
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}

	public function initProcess()
	{
		parent::initProcess(); // TODO: Change the autogenerated stub

		if ($_SESSION['id_group'] != 1) {
			if ($_SESSION['admin'] != 1) {        //非系統管理者
				unset($this->fields['list']['action']);
				unset($this->fields['form']['member']['input']['action']);
			}
		}
		if ($_SESSION['id_web']) {
			unset($this->fields['form']['member']['input']['id_houses']['options']['default']);
			unset($this->fields['list']['id_web']);
		}

		if ($this->display == 'edit') {
			unset($this->fields['form']['member']['input']['password']);
		}

		if ($this->display == 'add') {
			unset($this->fields['form']['member']['input']['barcode_img']);
			unset($this->fields['form']['member']['input']['barcode']);
		}
	}

	public function processAdd()
	{
		$tt = false;
		while (!$tt) {        //產生不重複會員條碼
			$barcode = random16number(16);
			$sql     = sprintf('SELECT `id_member` FROM `member` WHERE `barcode` = %s LIMIT 0, 1',
				GetSQL($barcode, 'text'));
			$row     = Db::rowSQL($sql, true);
			if (empty($row['id_member'])) {
				$tt = true;
			}
		}

		$_POST['barcode'] = $barcode;

		$this->fields['form'][0]['input']['barcode'] = [
			'name' => 'barcode',
			'type' => 'text',
		];

		parent::processAdd();
	}

	public function displayAjaxMemberSearch()
	{
		$search     = Tools::getValue('search');
		$num        = 0;
		$arr_member = [];
		if (!empty($search)) {
			$sql = 'SELECT `id_member`, `email`, `name`
				FROM `member`
				WHERE `email` LIKE "' . $search . '%"
					OR `name` LIKE "' . $search . '%"';
			$db  = new db_mysql($sql);
			$num = $db->num();
			if ($db->num() > 0) {
				while ($member = $db->next_row()) {
					if (empty($member['name']))
						$member['name'] = '-';
					$arr_member[] = [$member['id_member'], $member['email'], $member['name']];
				};
			};
		};
		$data = [
			'num'        => $num,
			'arr_member' => $arr_member,
		];
		$json = new Services_JSON();
		echo $json->encode($data);
		exit;
	}

	public function displayAjaxMemberSearchId()
	{
		$id     = Tools::getValue('id');
		$num    = 0;
		$member = [];
		if ($id > 0) {
			$sql = sprintf('SELECT `id_member`, `email`, `name`
				FROM `member`
				WHERE `id_member` = %d
				LIMIT 0, 1', $id);
			$db  = new db_mysql($sql);
			$num = $db->num();
			if ($db->num() > 0) {
				$member = $db->row();
				if (empty($member['name']))
					$member['name'] = '-';
			};
		};
		$data = [
			'num'       => $num,
			'id_member' => $member['id_member'],
			'email'     => $member['email'],
			'name'      => $member['name'],
		];
		$json = new Services_JSON();
		echo $json->encode($data);
		exit;
	}
}