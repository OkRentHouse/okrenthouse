<?php

class AdminAPPManagementController extends AdminController
{
	public $page = 'APPManagement';

	public function __construct()
	{
		$this->className       = 'AdminAPPManagementController';
		$this->fields['title'] = 'APP管理';

		$this->fields['tabs'] = [
			'APP' => $this->l('APP'),
		];

		$this->fields['form'] = [
			'APP'     => [
				'tab'    => 'app',
				'legend' => [
					'title' => $this->l('一般設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'app_welcome_scrolling',
						'type'          => 'text',
						'label'         => $this->l('歡迎頁跑馬燈'),
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'options';
		parent::initContent();
	}
}