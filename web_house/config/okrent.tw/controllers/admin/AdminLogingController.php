<?php
class AdminLogingController extends AdminController
{
	public function __construct(){
		$this->meta_title = $this->l('登入系統管理者');
		parent::__construct();
	}

	public function checkAccess()	//所有人都有權限
	{
		return true;
	}

	public function viewAccess()	//所有人都可觀看
	{
		return true;
	}

	public function initContent()
	{
		$this->displayHeader(false);
		$this->context->smarty->assign(array(
			'back' => '//'.WEB_DNS,
		));
		parent::initContent();
	}

	public function postProcess(){				//處理
		if (!empty($_SESSION['id_admin'])) $this->login();      //已登入
		if (isset($_POST['AdminLogin'])) $this->processLogin();                  //登入
		if (isset($_POST['AdminForgot'])) $this->processForgot();            //忘記密碼
		$this->context->smarty->assign('email', $_POST['email']);
	}

	//登入
	public function processLogin(){
		$auto_loging = Tools::getValue('auto_loging');
		if(Tools::isSubmit('flash_barcode')){
			$flash_barcode = Tools::getValue('flash_barcode');
			if(empty($flash_barcode)){
				$this->_errors[] =  $this->l('請掃描登入一維條碼');
			}else{
				if(Admin::flash_login($flash_barcode, $auto_loging)){
					$this->login();
				}else{
					$this->_errors[] = $this->l('條碼掃描錯誤');
				};
			}
		}else{
			$email = Tools::getValue('email');
			$pass = Tools::getValue('password');

			if(empty($email)) $this->_errors[] =  $this->l('請輸入帳號');
			if(empty($pass)) $this->_errors[] = $this->l('請輸入密碼');
			if($_SESSION['verification2'] != Tools::getValue('verification')){
				$this->_errors[] = $this->l('圖形驗證碼輸入錯誤!');
			};
			if(count($this->_errors) == 0){	//todo 群組分社區
				if(Admin::login($email, $pass, $auto_loging)){
					$this->login();
				}else{
					$this->_errors[] = $this->l('帳號密碼錯誤');
				};
			}
		}
	}

	//忘記密碼
	public function processForgot(){

	}

	//已登入
	public function login()
	{
		$page = Tools::getValue('page');
		if(empty($page)){
			Tools::redirectLink('//'.WEB_DNS.'/'.ADMIN_URL);
		}else{
			Tools::redirectLink('//'.WEB_DNS.base64_decode(Tools::getValue('page')));
		}
		exit;
	}
}
