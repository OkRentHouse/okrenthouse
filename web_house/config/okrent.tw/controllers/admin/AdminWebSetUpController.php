<?php

class AdminWebSetUpController extends AdminController
{
	public $page = 'websetup';

	public function __construct()
	{
		$this->className       = 'AdminWebSetUpController';
		$this->fields['title'] = '網站設定';

		$this->fields['tabs'] = [
			'web_set_up' => $this->l('網站設定'),
			'loging'     => $this->l('登入設定'),
		];

		$this->fields['form'] = [
			'web_set_up' => [
				'tab'    => 'web_set_up',
				'legend' => [
					'title' => $this->l('網站設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'GoogleAnalytics',
						'type'          => 'textarea',
						'label'         => $this->l('Google Analytics 追蹤程式碼'),
						'p'             => $this->l('請輸入Analytics (分析) 追蹤程式碼     <a href="https://www.google.com.tw/intl/zh-TW/analytics/" target="_blank">前往 Google Analytics</a>'),
						'col'           => 6,
						'rows'          => 12,
						'placeholder'   => '範例：

<script>
	(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

	ga(\'create\', \'XX-XXXXXXXX-XX\', \'auto\');
	ga(\'send\', \'pageview\');

</script>
',
						'configuration' => true,
					],
					[
						'name'          => 'js_code',
						'type'          => 'textarea',
						'label'         => $this->l('JS 程式碼'),
						'col'           => 6,
						'rows'          => 12,
						'placeholder'   => '',
						'p'             => $this->l('此JS會套用在所有"前台"頁面上,設定上請減少錯誤,防止運算AJAX時出錯!由其在商品運算部分!'),
						'configuration' => true,
					],
					[
						'name'          => 'css_code',
						'type'          => 'textarea',
						'label'         => $this->l('CSS 程式碼'),
						'col'           => 6,
						'rows'          => 12,
						'placeholder'   => '',
						'p'             => $this->l('此CSS會套用在所有"前台"頁面上,設定上請減少錯誤,防止版面跑版!'),
						'configuration' => true,
					],
					[
						'name'          => 'list_Cookie',
						'label'         => $this->l('啟用List Cookie'),
						'hint'          => $this->l('網站會紀錄上一次搜尋的設定'),
						'type'          => 'switch',
						'val'           => 0,
						'values'        => [
							[
								'id'    => 'list_Cookie_2',
								'value' => 2,
								'label' => $this->l('全部'),
							],
							[
								'id'    => 'list_Cookie_1',
								'value' => 1,
								'label' => $this->l('局部'),
							],
							[
								'id'    => 'list_Cookie_0',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
						'configuration' => true,
					],
					[
						'name'          => 'WEB_SSL_ENABLED',
						'label'         => $this->l('HTTPS協定'),
						'type'          => 'switch',
						'val'           => 0,
						'values'        => [
							[
								'id'    => 'http',
								'value' => 0,
								'label' => $this->l('HTTP'),
							],
							[
								'id'    => 'https',
								'value' => 1,
								'label' => $this->l('HTTPS'),
							],
						],
						'configuration' => true,
					],
					[
						'name'          => 'MediaURL',
						'type'          => 'text',
						'label'         => $this->l('手機版網址'),
						'hint'          => $this->l('只會使用到手機版面'),
						'placeholder'   => $this->l('m.DomainName.com.tw'),
						'configuration' => true,
					],
					[
						'name'          => 'APP_URL',
						'type'          => 'text',
						'label'         => $this->l('APP網址'),
						'hint'          => $this->l('只會使用到APP版面'),
						'placeholder'   => $this->l('app.DomainName.com.tw'),
						'configuration' => true,
					],
					[
						'name'          => 'WEB_ALLOW_MOBILE_DEVICE',
						'label'         => $this->l('允許行動裝置'),
						'type'          => 'checkbox',
						'val'           => 0,
						'values'        => [
							[
								'id'    => 'mobile',
								'value' => 1,
								'label' => $this->l('手機'),
							],
							[
								'id'    => 'tablet',
								'value' => 2,
								'label' => $this->l('平版'),
							],
						],
						'configuration' => true,
					],
					[
						'name'          => 'theme-color',
						'type'          => 'color',
						'label'         => $this->l('theme-color'),
						'configuration' => true,
					],
					[
						'name'          => 'msapplication-navbutton-color',
						'type'          => 'color',
						'label'         => $this->l('msapplication-navbutton-color'),
						'configuration' => true,
					],
					[
						'name'          => 'apple-mobile-web-app-status-bar-style',
						'type'          => 'color',
						'label'         => $this->l('apple-mobile-web-app-status-bar-style'),
						'configuration' => true,
					],
					[
						'name'          => 'WEB_LANGIAGE',
						'type'          => 'select',
						'label'         => $this->l('網站語系'),
						'options'       => [
							'val' => [
								['val' => 'en', 'text' => $this->l('en')],
								['val' => 'ar', 'text' => $this->l('ar')],
								['val' => 'ar-sa', 'text' => $this->l('ar-sa')],
								['val' => 'ar-tn', 'text' => $this->l('ar-tn')],
								['val' => 'bg', 'text' => $this->l('bg')],
								['val' => 'ca', 'text' => $this->l('ca')],
								['val' => 'cs', 'text' => $this->l('cs')],
								['val' => 'da', 'text' => $this->l('da')],
								['val' => 'de', 'text' => $this->l('de')],
								['val' => 'de-at', 'text' => $this->l('de-at')],
								['val' => 'el', 'text' => $this->l('el')],
								['val' => 'en-au', 'text' => $this->l('en-au')],
								['val' => 'en-ca', 'text' => $this->l('en-ca')],
								['val' => 'en-gb', 'text' => $this->l('en-gb')],
								['val' => 'en-ie', 'text' => $this->l('en-ie')],
								['val' => 'en-nz', 'text' => $this->l('en-nz')],
								['val' => 'es', 'text' => $this->l('es')],
								['val' => 'es-do', 'text' => $this->l('es-do')],
								['val' => 'eu', 'text' => $this->l('eu')],
								['val' => 'fa', 'text' => $this->l('fa')],
								['val' => 'fi', 'text' => $this->l('fi')],
								['val' => 'fr', 'text' => $this->l('fr')],
								['val' => 'fr-ca', 'text' => $this->l('fr-ca')],
								['val' => 'fr-ch', 'text' => $this->l('fr-ch')],
								['val' => 'gl', 'text' => $this->l('gl')],
								['val' => 'he', 'text' => $this->l('he')],
								['val' => 'hi', 'text' => $this->l('hi')],
								['val' => 'hr', 'text' => $this->l('hr')],
								['val' => 'hu', 'text' => $this->l('hu')],
								['val' => 'id', 'text' => $this->l('id')],
								['val' => 'is', 'text' => $this->l('is')],
								['val' => 'it', 'text' => $this->l('it')],
								['val' => 'ja', 'text' => $this->l('ja')],
								['val' => 'ko', 'text' => $this->l('ko')],
								['val' => 'lb', 'text' => $this->l('lb')],
								['val' => 'lt', 'text' => $this->l('lt')],
								['val' => 'lv', 'text' => $this->l('lv')],
								['val' => 'mk', 'text' => $this->l('mk')],
								['val' => 'ms', 'text' => $this->l('ms')],
								['val' => 'ms-my', 'text' => $this->l('ms-my')],
								['val' => 'nb', 'text' => $this->l('nb')],
								['val' => 'nl', 'text' => $this->l('nl')],
								['val' => 'nn', 'text' => $this->l('nn')],
								['val' => 'pl', 'text' => $this->l('pl')],
								['val' => 'pt', 'text' => $this->l('pt')],
								['val' => 'pt-br', 'text' => $this->l('pt-br')],
								['val' => 'ro', 'text' => $this->l('ro')],
								['val' => 'ru', 'text' => $this->l('ru')],
								['val' => 'sk', 'text' => $this->l('sk')],
								['val' => 'sl', 'text' => $this->l('sl')],
								['val' => 'sr', 'text' => $this->l('sr')],
								['val' => 'sr-cyrl', 'text' => $this->l('sr-cyrl')],
								['val' => 'sv', 'text' => $this->l('sv')],
								['val' => 'th', 'text' => $this->l('th')],
								['val' => 'tr', 'text' => $this->l('tr')],
								['val' => 'uk', 'text' => $this->l('uk')],
								['val' => 'vi', 'text' => $this->l('vi')],
								['val' => 'zh-cn', 'text' => $this->l('zh-cn')],
								['val' => 'zh-tw', 'text' => $this->l('zh-tw 繁體中文')],
							],
						],
						'configuration' => true,
					],
					[
						'name'          => 'FirebaseCloudMessagingKEY',
						'type'          => 'text',
						'label'         => $this->l('Firebase Cloud Messaging 憑證'),
						'hint'          => $this->l('Firebase Cloud Messaging 憑證'),
						'configuration' => true,
					],
					[
						'type' => 'hr',
					],
					[
						'name'          => 'FB_link',
						'type'          => 'text',
						'label'         => $this->l('Facebook 網址'),
						'val'           => '#',
						'placeholder'   => '請輸入Facebook網址',
						'configuration' => true,
					],
					[
						'type'          => 'switch',
						'label'         => $this->l('啟用Enter送出表單'),
						'name'          => 'ENTER_SUBMIT',
						'val'           => 1,
						'configuration' => true,
						'values'        => [
							[
								'id'    => 'ENTER_SUBMIT_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'ENTER_SUBMIT_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
			'loging'     => [
				'tab'    => 'loging',
				'legend' => [
					'title' => $this->l('登入設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'MemberAutoLoginCookieDate',
						'type'          => 'number',
						'label'         => $this->l('會員自動登入時效'),
						'hint'          => $this->l('會員自動登入Cookie時效'),
						'val'           => 30,
						'suffix'        => $this->l('天'),
						'configuration' => true,
					],
					[
						'name'          => 'AdminAutoLoginCookieDate',
						'type'          => 'number',
						'label'         => $this->l('管理者自動登入時效'),
						'hint'          => $this->l('管理者自動登入Cookie時效'),
						'val'           => 30,
						'suffix'        => $this->l('天'),
						'configuration' => true,
					],
					[
						'name'          => 'loging_error_second',
						'type'          => 'number',
						'label'         => $this->l('時間範圍'),
						'suffix'        => $this->l('秒內錯誤'),
						'min'           => 0,
						'p'             => $this->l('設定0(或不填寫)為不封鎖'),
						'configuration' => true,
					],
					[
						'name'          => 'loging_error_num',
						'type'          => 'number',
						'label'         => $this->l('登入失敗次數'),
						'suffix'        => $this->l('次'),
						'min'           => 0,
						'p'             => $this->l('設定0(或不填寫)為不封鎖'),
						'configuration' => true,
					],
					[
						'name'          => 'loging_error_blockade',
						'type'          => 'number',
						'label'         => $this->l('IP封鎖時間'),
						'suffix'        => $this->l('秒'),
						'min'           => 0,
						'placeholder'   => $this->l('預設封鎖5分鐘(300秒)'),
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('重置'),
				],
			],
		];

		parent::__construct();
	}

	public function initContent()
	{
		$this->display = 'options';
		parent::initContent();
	}
}