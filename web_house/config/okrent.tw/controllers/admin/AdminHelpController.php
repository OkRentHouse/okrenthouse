<?php
class AdminHelpController extends AdminController
{
	public $page = 'help';
	public function __construct(){
		$this->className = 'AdminHelpController';
		$this->fields['title'] = '說明書設定';
		parent::__construct();
	}
}