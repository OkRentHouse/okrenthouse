<?php
class AdminThemesController extends AdminController
{
	public $page = 'themes';
	public function __construct(){
		$this->className = 'AdminThemesController';
		$this->fields['title'] = '佈景主題';

		$arr_themes = Themes::getContext()->getAllThemes();
		$themes_list = array();
		foreach($arr_themes as $i => $v){
			if(DEFAULT_THEME == $i){
				array_unshift(
					$themes_list,
					array(
						'id' => 'mt_'.$i,
						'value' => $i,
						'label' => $this->l($v['title'])
					)
				); //放在第一個
			}else{
				$themes_list[] = array(
					'id' => 'mt_'.$i,
					'value' => $i,
					'label' => $this->l($v['title'])
				);
			}
		}

		$this->fields['form'] = array(
			array(
				'legend' => array(
					'title' => $this->l('佈景主題'),
					'icon' => 'icon-cogs',
					'image' => ''
					),
				'input' => array(
					array(
						'name' => LilyHouse::getContext()->web.'_Theme',
						'type' => 'switch',
						'label' => $this->l('網站樣式'),
						'hint' => $this->l('可以改變網站佈景樣式'),
						'required' => true,
						'configuration' => true,
						'val' => DEFAULT_THEME,
						'values' => $themes_list
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					)
				),
				'cancel' => array(
					'title' => $this->l('取消')
				),
				'reset' => array(
					'title' => $this->l('復原')
				)
			)
		);
		
		parent::__construct();
	}
	
	public function initContent(){
		$this->display = 'edit';
		parent::initContent();
	}
}