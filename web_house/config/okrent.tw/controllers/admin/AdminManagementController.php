<?php

class AdminManagementController extends AdminController
{
	public $stay = false;

	public function __construct()
	{
		$this->className = 'AdminManagementController';
		$this->table     = 'admin';
		//判別賦予權限
		if ($_SESSION['id_group'] != 1) {     //非最高系統管理者 隱藏
			$this->fields['where'] = ' AND `id_group` != 1';
			if ($_SESSION['admin'] != 1) {
				$this->fields['where'] .= ' AND `id_group` NOT IN (SELECT `id_group` FROM `group` WHERE `admin` = 1)';
				$this->fields['where'] .= ' AND `id_group` IN (' . implode(', ', GiveGroup::getGroup($_SESSION['id_group'])) . ')';
				$this->fields['where'] .= ' AND `id_web` = ' . $_SESSION['id_web'];
			}
			$where = ' AND `id_group` != 1';
		}
		$this->_as             = 'a';
		$this->fields['index'] = 'id_admin';
		$this->fields['title'] = '管理者帳號';
		$this->fields['list']  = [
			'id_admin'   => [
				'index'  => true,
				'type'   => 'checkbox',
				'select' => '',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'id_web'     => [
				'title'  => $this->l('加盟店名稱'),
				'order'  => true,
				'filter' => true,
				'key'    => [
					'table' => 'web',
					'key'   => 'id_web',
					'val'   => 'web',
					'where' => $this->my_where_web,
					'order' => '`web` ASC',
				],
				'class'  => 'text-center',
			],
			'id_group'   => [
				'title'      => $this->l('群組'),
				'order'      => true,
				'filter'     => true,
				'filter_key' => 'a!id_group',
				'key'        => [
					'table' => 'group',
					'key'   => 'id_group',
					'where' => $where,
					'val'   => 'name',
				],
				'class'      => 'text-center',
			],
			'first_name' => [
				'title'  => $this->l('姓名'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'email'      => [
				'title'  => $this->l('帳號'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'active'     => [
				'title'      => $this->l('啟用狀態'),
				'filter_key' => 'a!active',
				'order'      => true,
				'filter'     => true,
				'class'      => 'text-center',
			],
		];

		$this->fields['list_num'] = 50;

		if ((Tools::getValue('id_admin') == $_SESSION['id_admin']) && $_SESSION['admin'] != 1 && $_SESSION['id_group'] != 1) {
			$this->stay = true;
		}
		$arr_group = GiveGroup::getGroup();

		$this->fields['form'] = [
			'admin' => [
				'legend' => [
					'title' => $this->l('帳號管理'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_admin'        => [
						'name'     => 'id_admin',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'id_web'          => [
						'name'    => 'id_web',
						'type'    => 'select',
						'label'   => $this->l('加盟店名稱'),
						'p'       => $this->l('若不選擇加盟店將可觀看所有社區'),
						'options' => [
							'default' => [
								'text' => '請選擇加盟店',
								'val'  => '',
							],
							'table'   => 'web',
							'text'    => 'web',
							'where'   => $this->my_where_web,
							'value'   => 'id_web',
							'order'   => '`web` ASC',
						],
					],
					'id_group'        => [
						'name'     => 'id_group',
						'type'     => 'select',
						'label'    => $this->l('管理者群組'),
						'required' => true,
						'options'  => [
							'table'   => 'group',
							'text'    => 'name',
							'value'   => 'id_group',
							'where'   => $where . ' AND `id_group` IN (' . implode(', ', $arr_group) . ')',
							'default' => [
								'val'  => '',
								'text' => $this->l('請選擇管理者群組'),
							],
						],
					],
					'last_name'      => [
						'type'        => 'text',
						'label'       => $this->l('姓'),
						'placeholder' => $this->l('姓'),
						'name'        => 'last_name',
						'maxlength'   => '20',
						'required'    => true,
					],
					'first_name'      => [
						'type'        => 'text',
						'label'       => $this->l('名'),
						'placeholder' => $this->l('名'),
						'name'        => 'first_name',
						'maxlength'   => '20',
						'required'    => true,
					],
					'email'           => [
						'type'      => 'view',
						'required'  => true,
						'label'     => $this->l('帳號'),
						'name'      => 'email',
						'maxlength' => '100',
						'desc'      => $this->l('請輸入e-mail'),
					],
					'change-password' => [
						'type'       => 'change-password',
						'col'        => 5,
						'label'      => $this->l('修改密碼'),
						'old_passwd' => false,
						'minlength'  => '4',
						'maxlength'  => '20',
						'name'       => 'password',
					],
					'password'        => [
						'type'        => 'new-password',
						'label'       => $this->l('密碼'),
						'placeholder' => $this->l('請輸入密碼'),
						'required'    => true,
						'minlength'   => '4',
						'maxlength'   => '20',
						'name'        => 'password',
					],
					'password2'       => [
						'type'        => 'confirm-password',
						'label'       => $this->l('確認密碼'),
						'placeholder' => $this->l('再次輸入確認密碼'),
						'required'    => true,
						'minlength'   => '4',
						'maxlength'   => '20',
						'name'        => 'password2',
						'confirm'     => 'password',
					],
					'barcode_img'     => [
						'no_action' => true,
						'type'      => 'html',
						'html'      => '<a href="#" id="barcode_img_file"><img id="barcode_img" src="" title="' . $this->l('複製') . '" class="pointer"></a>',
					],
					'flash_barcode'   => [
						'name'      => 'flash_barcode',
						'type'      => 'hidden',
						'label'     => $this->l('登入條碼'),
						'p'         => $this->l('系統管理者快速登入用'),
						'maxlength' => '16',
					],
					'active'          => [
						'type'   => 'switch',
						'label'  => $this->l('啟用狀態'),
						'name'   => 'active',
						'val'    => 1,
						'values' => [
							[
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用'),
							],
							[
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉'),
							],
						],
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}

	public function initToolbar()
	{        //初始化功能按鈕
		parent::initToolbar();
		if ($this->stay)
			unset($this->toolbar_btn['save']);
	}

	public function initProcess()
	{
		if (Tools::getValue($this->fields['index']) == $_SESSION['id_admin']) {    //本身資料
			if (empty($this->tabAccess['view']) || empty($this->tabAccess['edit'])) {
				$this->submit_action                                = 'submitEditadminAndStay';
				$this->fields['form']['admin']['submit'][0]['stay'] = true;
				unset($this->fields['form']['admin']['input']['id_group']);
				unset($this->fields['form']['admin']['input']['email']);
			}
			$this->tabAccess['view'] = 1;
			$this->tabAccess['edit'] = 1;
		} else {
			if ($_SESSION['id_group'] != 1 && $_SESSION['admin'] != 1) {
				unset($this->fields['form']['admin']['barcode_img']);
				unset($this->fields['form']['admin']['flash_barcode']);
			}
		};

		$id_admin = Tools::getValue('id_admin');
		if (!empty($id_admin) && $_SESSION['id_group'] != 1 && $_SESSION['admin'] != 1 && $id_admin != $_SESSION['id_admin']) {
			//查看編輯的使用者有沒有此賦予權限
			$arr_group = GiveGroup::getGroup();
			$sql       = sprintf('SELECT `id_admin`
					FROM `admin` AS a
					WHERE `id_group` IN (' . implode(', ', $arr_group) . ')
					AND `id_admin` = %d
					LIMIT 0, 1', $id_admin);
			Db::rowSQL($sql);
			if (Db::getContext()->num() == 0) {   //沒有權限 就設禁止讀資料
				$this->fields['where'] .= ' AND FALSE';
				$this->_errors[]       = $this->l('您沒有此群組權限');
			}
		}

		parent:: initProcess();

		Hook::exec('AdminManagement_initProcess');    //cPanel 外掛要加到這
		if($this->display == 'edit'){
			$id_admin = Tools::getValue('id_admin');
			//todo 要轉成模組

			$admin = Admin::get($id_admin);
			$html = sprintf('<a href="https://%s/login?user=%s@%s" target="_blank">%s</a>', Configuration::get('cPanel_webemail_url'), $admin['email'], Configuration::get('cPanel_email_domain'), $this->l('前往WebMail'));
			$this->fields['form']['cPanelEmail'] = [
				'legend' => [
					'title' => $this->l('WebMail'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'html' => [
						'name'      => 'html',
						'type'      => 'html',
						'html'      => $html,
						'no_action' => true,
					],
				],
			];
		}

		if ($this->display == 'add') {
			$this->fields['form']['admin']['input']['email']['type'] = 'text';
			$this->fields['form']['admin']['input']['email']['p']    = $this->l('帳號新增後將無法更改');
			unset($this->fields['form']['admin']['input']['barcode_img']);
			unset($this->fields['form']['admin']['input']['flash_barcode']);
		}
	}


	public function processAdd()
	{
		$tt = false;
		while (!$tt) {        //產生不重複會員條碼
			$flash_barcode = rand_code(16);
			$sql           = sprintf('SELECT `id_admin` FROM `admin` WHERE `flash_barcode` = %s LIMIT 0, 1',
				GetSQL($flash_barcode, 'text'));
			$row           = Db::rowSQL($sql, true);
			if (empty($row['id_member'])) {
				$tt = true;
			}
		}

		$_POST['flash_barcode'] = $flash_barcode;

		$this->fields['form']['admin']['input']['flash_barcode'] = [
			'name' => 'flash_barcode',
			'type' => 'text',
		];

		parent::processAdd();
	}

	public function processDel()
	{
		$index_id = Tools::getValue('id_admin');
		if (is_array($index_id)) {
			$WHERE = ' NOT IN (' . implode(', ', $index_id) . ')';
		} else {
			$WHERE = ' != ' . $index_id;
		};
		$sql = 'SELECT `id_admin`
			FROM `admin`
			WHERE `id_admin`' . $WHERE;
		$db  = new db_mysql($sql);
		if ($db->num() == 0)
			$this->_errors[] = $this->l('[系統管理者帳號]必須要有一個!');

		$sql = 'SELECT `id_admin`
			FROM `admin`
			WHERE `id_group` = 1 AND `id_admin`' . $WHERE;
		$db  = new db_mysql($sql);
		if ($db->num() == 0)
			$this->_errors[] = $this->l('[最高管理者帳號]必須要有一個!');

		if (count($this->_errors))
			Configuration::del('MyManageTheme_' . $_SESSION['id_admin']);
		parent:: processDel();
	}

	public function validateRules()
	{
		if ($this->action == 'Save') {
			$id_admin = Tools::getValue('id_admin');
			$id_group = Tools::getValue('id_group');
			/*
			 * 不是選擇最高系統管理者
			 */
			if ($id_group != 1 && $this->display != 'add') {
				$sql = 'SELECT `id_admin`
					FROM `admin`
					WHERE `id_group` = 1 AND `id_admin` != ' . $id_admin . '
					LIMIT 0, 1';
				$row = Db::rowSQL($sql, true);
				if (!count($row))
					$this->_errors[] = $this->l('[最高系統管理者]帳號必須要有一個!');
			}

			/*
			 * 系統管理者 列表
			 */
			$sql          = 'SELECT `id_group`
				FROM `group`
				WHERE `admin` = 1
				AND `id_group` != 1';
			$arr_row      = Db::rowSQL($sql, false);
			$arr_id_group = [];
			foreach ($arr_row as $row) {
				$arr_id_group[] = $row['id_group'];
				if (count($arr_id_group) == 0)
					$this->_errors[] = $this->l('[系統管理者]群組必須要有一個!請先至[群組管裡]新增一個[系統管理者]群組，將[管理者]勾[是]');
			}
			if (!in_array($id_group, $arr_id_group) && count($arr_id_group)) {
				if (!empty($id_admin))
					$WHERE = ' AND `id_admin` != ' . $id_admin;
				$sql = 'SELECT `id_admin`
					FROM `admin`
					WHERE `id_group` IN (' . implode(', ', $arr_id_group) . ')
					AND `id_group` != 1 ' . $WHERE . '
					LIMIT 0, 1';
				$row = Db::rowSQL($sql, true);
				if (!count($row))
					$this->_errors[] = $this->l('[系統管理者]帳號必須要有一個!請先新增一個[系統管理者]群組的帳號!');
			}
		}
		parent::validateRules();
	}

}