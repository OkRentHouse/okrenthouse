<?php
class AdminLogController extends AdminController
{
	public function __construct(){
		$this->className = 'AdminLogController';
		$this->table = 'log';
		$this->_as = 'l';
		$this->fields['index'] = 'id_log';
		$this->fields['title'] = 'Log記錄';
		$this->fields['order'] = ' ORDER BY `time` DESC';
		
		if($_SESSION['id_group'] != 1){
			$this->_join .= ' LEFT JOIN `admin` AS a ON a.`id_admin` = l.`id_admin`';            //小訂
			$this->fields['where'] .= ' AND a.`id_group` <> 1';
			$this->fields['where'] .= ' AND a.`id_admin` <> 1408';
		}
		if($_SESSION['admin'] != 1){
			$this->fields['where'] .= ' AND l.`id_admin` = '.$_SESSION['id_admin'];
		}

		$this->fields['list'] = array(
			'id_log' => array(
				'index' => true,
				'title' => $this->l('Log ID'),
				'type' => 'checkbox',
				'hidden' => true,
				'class' => 'text-center'
			),
			'id_admin' => array(
				'title' => $this->l('管理者ID'),
				'filter_key' => 'l!id_admin',
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'email' => array(
				'title' => $this->l('管理者帳號'),
				'filter_key' => 'l!email',
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'type' => array(
				'title' => $this->l('Log類型'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'detailed' => array(
				'title' => $this->l('詳細說明'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'ip' => array(
				'title' => $this->l('IP'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'time' => array(
				'title' => $this->l('時間'),
				'order' => true,
				'filter' => 'datetime_range',
				'class' => 'text-center'
			),
		);
		
		$this->fields['form'] = array(
			array(
				'legend' => array(
					'title' => $this->l('Log 管理'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'id_log',
						'type' => 'hidden',
						'index' => true,
						'required' => true
					),
					array(
						'name' => 'id_log',
						'type' => 'text',
						'label' => $this->l('管理者ID'),
					),
					array(
						'name' => 'email',
						'type' => 'text',
						'label' => $this->l('管理者帳號'),
					),
					array(
						'name' => 'type',
						'type' => 'text',
						'label' => $this->l('Log類型'),
					),
					array(
						'name' => 'detailed',
						'type' => 'text',
						'label' => $this->l('詳細說明'),
					),
					array(
						'name' => 'ip',
						'type' => 'text',
						'label' => $this->l('IP'),
					),
				),
				'cancel' => array(
					'title' => $this->l('取消')
				)
			)
		);
		
		parent::__construct();
		$this->tabAccess['add'] = 0;
		$this->tabAccess['edit'] = 0;
		$this->tabAccess['del'] = 0;
		$this->actions = array('view');
	}
	
}