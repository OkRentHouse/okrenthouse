<?php
class IndexController extends FrontController
{
	public $page = 'index';
	
	public $tpl_folder;	//樣版資料夾
	public $definition;
	public $_errors_name;
	
	public function __construct(){
		$this->meta_title = $this->l('首頁');
		$this->page_header_toolbar_title = $this->l('首頁');
		$this->className = 'IndexController';
		$this->display_main_menu = false;
		$this->display_header = false;
		$this->display_footer = false;
		parent::__construct();
	}
	
}