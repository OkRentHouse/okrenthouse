<?php
class CMSController extends FrontController
{
	public $tpl_folder;	//樣版資料夾
	public $page = 'cms';
	public $html = '';


	public function __construct(){
		$this->className = 'CMSController';
		parent::__construct();
		$cms = CMS::getByUrl(CMS::getContext()->url);
		$this->meta_title					= $cms['title'];
		$this->page_header_toolbar_title	= $cms['page_title'];
		$this->meta_description				= $cms['description'];
		$this->meta_keywords				= $cms['keywords'];
		$this->nobots						= !$cms['index_ation'];
		$this->nofollow						= !$cms['index_ation'];
		$this->display_header				= $cms['display_header'];
		$this->display_footer				= $cms['display_footer'];
		$this->html							= $cms['html'];

	}

	public function initToolbar(){
		parent::initToolbar();
		$this->back_url = 'history.back();';
	}
	public function initContent(){
		parent::initContent();
		$this->context->smarty->assign(array(
			'html'		=> $this->html
		));
		$this->setTemplate('cms.tpl');
	}
	
}