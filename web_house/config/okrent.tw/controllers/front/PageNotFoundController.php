<?php
class PageNotFoundController extends FrontController
{
	public $page = 'page-not-found';

	public function __construct() {
		$this->meta_title = $this->l('404 找不到網頁');
		$this->page_header_toolbar_title = $this->l('404 找不到網頁');
		parent::__construct();
	}

	public function initToolbar()
	{
		parent::initToolbar();
		unset($this->back_url);
	}

	public function initContent(){
		parent::initContent();
		$this->setTemplate(THEME_DIR.'page-not-found.tpl');
	}
	
	public function displayAjax(){
		$this->ajax = false;
		$this->display_header = true;
		$this->display_footer = true;
		$this->setTemplate(THEME_DIR.'page-not-found.tpl');
		parent::run();
	}
}