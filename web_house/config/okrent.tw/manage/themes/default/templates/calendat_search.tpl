<form class="calendat_search">
	<div class="col-lg-2 col-md-3 col-sm-4 col-xs-7 no_left">
		<div class=" input-group">
			<select id="id_repair_class" name="id_repair_class[]" size="0" class="form-control selectpicker form-control" multiple="multiple" data-selected-text-format="count > 2">
				{foreach $arr_repair_class as $i => $option}
				<option value="{$i}"
					  {if !empty($post_repair_class)}
					  {foreach $post_repair_class as $post}
					  {if $post == $i} selected="selected"{/if}
				{/foreach}
				{elseif !empty($cookie_repair_class) > 0}
				{foreach $cookie_repair_class as $cookie}
				{if $cookie == $i} selected="selected"{/if}
				{/foreach}
				{/if}>{$option}</option>
				{/foreach}
			</select>
			<span class="input-group-btn"><button type="submit" name="submit" class="view print btn btn-default search" title="搜尋"><span class="glyphicon glyphicon-search"></span> 搜尋</button></span>
		</div>
	</div>
	<input type="hidden" name="calendarschedule">
</form>