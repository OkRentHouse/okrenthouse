<nav id="admin_menu">
	{*<div class="web_title"><a>{$fields['title']}</a></div>*}
	<ul>
		<div class="logo"></div>
		{foreach from=$tab_list key=k item=tab}
			{if !empty($k)}
			<li class="{$tab['controller_name']}{if $page == $tab['controller_name']} active{/if}"><a{if isset($tab['list'])} href="#" role="button"{else} href="/{$ADMIN_URL}/{$tab['controller_name']}{$tab['parameter']}"href="/{$ADMIN_URL}/{$tab['controller_name']}{$tab['parameter']}"{/if}><i class="{$tab['icon']}"></i>{$tab['title']|escape:'UTF-8'}{if isset($tab['list'])}{/if}</a>
				{if isset($tab['list'])}{strip}
					<ul class="submenu">
					{foreach from=$tab['list'] key=k2 item=tab2}{if !empty($k2)}
					<li class="{$tab2['controller_name']}{if $page == $k2} active{/if}"><a href="/{$ADMIN_URL}/{$tab2['controller_name']}{$tab2['parameter']}">{$tab2['title']|escape:'UTF-8'}</a></li>{/if}{/foreach}
					</ul>{/strip}
				{/if}
				</li>{/if}
		{/foreach}
	</ul>
	<span class="menu-collapse"><i class="glyphicon glyphicon-menu-hamburger rotate-90"></i></span>
</nav>