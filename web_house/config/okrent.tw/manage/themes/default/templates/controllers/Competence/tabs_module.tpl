<div class="tab-count module_tab tabs_group_{$group['id_group']} col-lg-6 col-md-12 col-sm-12 col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="icon-cogs"></i>{l s="模組"}</div>
		<div class="panel-body">
			<table>
				<thead>
				<tr class="h6" data-id_tab="all" data-id_parent="all">
					<td></td>{foreach from=$type key=i item=type_v}<td><label><input type="checkbox" class="on_ajax" value="{$type_v['code']}" data-id_group="{$group['id_group']}"{if false} checked="checked"{/if}>{$type_v.text}</label></td>{/foreach}<td><label><input type="checkbox" class="on_ajax" data-id_group="{$group['id_group']}" value="all"{if false} checked="checked"{/if}>全部</label></td>
				</tr>
				</thead>
				<tbody>
				{foreach from=$arr_module key=j item=module}
					<tr class="h6{if $module.id_parent == 0} parent{else} child{/if}" data-id_tab="{$module.id_tab}" data-id_parent="{$module.id_parent}">
						<td>{$tab.title}</td>{foreach from=$type key=i item=type_v}<td><label><input type="checkbox" class="on_ajax" value="{$type_v['code']}"{if $arr_competence[$group.id_group][$module['id_module']][$type_v['code']]} checked="checked"{/if}></label></td>{/foreach}<td><label><input type="checkbox" class="on_ajax" value="all"{if false} checked="checked"{/if}>{$arr_competence[$group.id_group][$type_v]}</label></td>
					</tr>
				{/foreach}
				</tbody>
			</table>
		</div>
	</div>
</div>