function show_footer(){
	if($('html').height() - $(window).height() - $(window).scrollTop() <= 0){
		$('#footer').removeClass('hide');
	}else{
		$('#footer').addClass('hide');
	};
};
function minbar_shadow(){
	if($(window).scrollTop() > 0){
		$('.minbar').addClass('shadow');
	}else{
		$('.minbar').removeClass('shadow');
	};
};
function table_th_title(){
	var $th = $('th:not(.hidden)', '.list table thead tr:first-child');
	if($th.length > 0){
		var w_top = $(window).scrollTop();
		var zero_top = $th.eq(0).offset().top;
		var bor_top = $th.eq(0).css('border-top-width').replace(/px/, '');
		var m_height = $('.minbar').height();
		$('.list table thead tr').each(function(){
			var $this = $(this);
			var top = $('th:not(.hidden)', $this).eq(0).offset().top;
			var now_t = w_top + m_height;
			if(now_t >= zero_top){
				$('th .th_title', $this).css('top', now_t -top + (top - zero_top - bor_top - 1)).addClass('show');
			}else{
				$('th .th_title', $this).css('top', 0).removeClass('show');
			};
		});
	};
};
function show_tabs(){
	if($('.count_tabs li.active').length == 0){
		var _hash = $(location).attr('hash');
		if(_hash == '' || _hash == '#_=_'){
			$('.count_tabs li').eq(0).addClass('active');
		}else{
			$('.count_tabs li'+_hash).addClass('active');
			var $edit = $('a.edit.btn', '#top_bar_button');
			if($edit.length > 0){
				var href = $edit.attr('href');
				if(href.match('#') != null){
					href = href.split('#')[0];
				};
				$edit.attr('href', href+_hash);
			};
			// var $back = $('a.back.btn', '#top_bar_button');
			// if($back.length > 0){
			// 	var href2 = $back.attr('href');
			// 	if(href2.match('#') != null){
			// 		href2 = href2.split('#')[0];
			// 	};
			// 	//$back.attr('href', href2+_hash);
			// };
		};
	};
	if($('.count_tabs li').length) $('.tab-count').hide();
	$('.'+$('.count_tabs li.active').attr('id')).show();
};
//搜尋會員ID
var ajax_member_search;
var ajax_member_keyup;
function member_search_to_id(id){
	if(id > 0){
		ajax_member_search = $.ajax({
			url : '/manage/Member',
			method : 'POST',
			data : {
				'ajax'	: true,
				'action'	: 'MemberSearchId',
				'id'	: id},
			dataType : 'json',
			success: function(msg){
				if(msg.num > 0){
					var html = '<a href="/manage/Member?&editmember&id_member='+msg.id_member+'" target="_blank" class="form-control">'+msg.email+' '+msg.name+'</a><span class="input-group-btn"><button type="button" class="btn btn-default search"><span class="glyphicon glyphicon-refresh"></span>重新搜尋</span></button><button type="button" class="btn btn-default cancel hide"><span class="glyphicon glyphicon-remove"></span>取消</button>></div>';
					$('.member_search_div').addClass('hide');
					$('#show_member').html(html);
				}else{
					$('#no_member').removeClass('hide');
				};
			},
			beforeSend:function(){
				$('#no_member').addClass('hide');
				$('.member_search_div').addClass('hide');
				$('#member_search_loding').removeClass('hide');
			},
			complete:function(){
				$('#member_search_loding').addClass('hide');
			},
			error:function(xhr, ajaxOptions, thrownError){
				$('.member_search_div').removeClass('hide');
				alert('資料搜尋錯誤!');
			}
		});
	};
};
//搜尋會員ID
function member_search(str){
	if(str.length > 0){
		ajax_member_search = $.ajax({
			url : '/manage/Member',
			method : 'POST',
			data : {
				'ajax'	: true,
				'action'	: 'MemberSearch',
				'search'	: str},
			dataType : 'json',
			success: function(msg){
				if(msg.num > 0){
					html = '搜尋到 '+msg.num+' 筆<table class="table"><thead><tr><th class="text-center">e-mail</th><th class="text-center">姓名</th><th></th></tr></thead><tbody>';
					for(var i in msg.arr_member){
						if(msg.arr_member[i][2] == null) msg.arr_member[i][2] = '';
						html += '<tr><td>'+msg.arr_member[i][1]+'</td><td>'+msg.arr_member[i][2]+'</td><td class="text-center"><button type="button" class="add" data-id="'+msg.arr_member[i][0]+'" data-email="'+msg.arr_member[i][1]+'" data-name="'+msg.arr_member[i][2]+'">加入</button</td></div>';
					};
					html += '</tbody></table>';
					$('#show_member').html(html);
				}else{
					$('#no_member').removeClass('hide');
				};
			},
			beforeSend:function(){
				$('#no_member').addClass('hide');
				$('#member_search_loding').removeClass('hide');
			},
			complete:function(){
				$('#member_search_loding').addClass('hide');
			},
			error:function(xhr, ajaxOptions, thrownError){
				alert('資料搜尋錯誤!');
			}
		});
	}else{
		$('#show_member').html('');
	};
};
function td_checkbox(i){
	var $checkbox = $('tr#tr_'+i+' input[type="checkbox"]', '.list tbody');
	if($checkbox.prop('checked') == true){
		$checkbox.prop('checked', false);
		$('tr#tr_'+i, '.list tbody').removeClass('active');
	}else{
		$checkbox.prop('checked', true);
		$('tr#tr_'+i, '.list tbody').addClass('active');
	};
};
$(document).ready(function(e) {
	$('input[type="text"].password').attr('type', 'password');
	if(display == 'add' || display == 'edit'){
		var isChange = false;
		$(function () {
			$('input, textarea, select').change(function () {
				isChange = true;
				$(this).addClass('editing');
			});
			$(window).bind('beforeunload', function (e) {
				if (isChange || $('.editing').get().length > 0) {
					return '資料尚未儲存,您確定要離開?';
				};
			});
		});
	};
	$('form').submit(function(){
		isChange = false;
		$('.editing').removeClass('editing');
	});
	$('.edit_cart').draggable();
	$('.close_edit_cart').click(function(){
		$(this).parents('.edit_cart').stop(true, false).fadeOut();
	});
	$('.edit_cart_on').click(function(){
		var div = $(this).data('div');
		$('div.'+div).stop(true, false).fadeIn();
	});
	if($(window).width() < 992){
		$('body').addClass('media');
	}else{
		$('body').removeClass('media');
	};
	$('#admin_menu>ul>li').click(function(e){
		if($(this).children('ul').length == 0) return true;
		if(e.target.nodeName == 'IMG') return true;
		var type = false;
		if($(window).width() < 992) {
			var $this = $(this);
			var $ul = $('ul', $this);
			if($ul.css('display') == 'none' || $ul.css('display') == 'block'){
				$ul.stop(true, false).slideDown();
			}else{
				$ul.stop(true, false).slideUp();
			}
			$('ul', $this.siblings('li')).stop(true, false).slideUp();
			$('.submenu li a').each(function(){
				if($(this)[0] == e.target){
					type = true;
				};
			});
			return type;
		};
	});
	$('.submenu a').click(function(e){
		return true;
	});
	//主選單變色
	$('#admin_menu li.active').parents('#admin_menu li').addClass('active');
	$('[data-toggle="tooltip"]').tooltip();
	$('input[type="checkbox"][name^="all_"]').each(function(index, element) {
		var name = $(this).attr('name');
		all_checkbox('input[name="'+name+'"]', 'input[name="'+name.substring(4, name.length)+'"]');
      });
	//footerv顯示
	show_footer();
	minbar_shadow();
	$(window).resize(function(){
		if($(window).width() < 992){
			if($('body.media').index() == -1) $('.list .table thead').css({"display":"none"});
			$('body').addClass('media');
		}else{
			$('body').removeClass('media');
			$('.list .table thead').css({"display":"table-header-group"});
		};
		show_footer();
		minbar_shadow();
	});
	$(window).scroll(function(){
		show_footer();
		minbar_shadow();
		table_th_title();
	});
	//內選單tab
	$('.count_tabs li').click(function(){
		var id = $(this).attr('id');
		window.history.replaceState(null, null, $(location).attr('pathname')+$(location).attr('search')+'#'+id);
		$('.count_tabs li').siblings('li').removeClass('active');
		var $edit = $('a.edit.btn', '#top_bar_button');
		if($edit.length > 0){
			var href = $edit.attr('href');
			if(href.match('#') != null){
				href = href.split('#')[0];
			};
			$edit.attr('href', href+'#'+id);
		};
		// var $back = $('a.back.btn', '#top_bar_button');
		// if($back.length > 0){
		// 	var href2 = $back.attr('href');
		// 	if(href2.match('#') != null){
		// 		href2 = href2.split('#')[0];
		// 	};
		// 	//$back.attr('href', href2+'#'+id);
		// };
		$(this).addClass('active');
		show_tabs();
	});
	//選單位置
	$drag_tbody = $('.drag_group').parents('tbody');
	$drag_tbody.tableDnD({
		onDragClass: 'myDragClass',
		onDrop: function(table, row) {
			var arr_id = new Array();
			$('.drag_group', $drag_tbody).each(function(index, element) {
				arr_id.push($(this).data('id'));
			});
			$.ajax({
				url : document.location.pathname,
				method : 'POST',
				data : {
					'ajax'	: true,
					'action'	: 'Position',
					'id'		: arr_id
				},
				dataType : 'json',
				success: function(msg){
					if(msg.error == undefined){
						$.growl.notice({
							title: '',
							message: msg.msg
						});
					}else{
						$.growl.error({
							title: '',
							message: msg.error
						});
					}
					$('.drag_group', $drag_tbody).each(function(index, element) {
						$('.positions', this).html((index+1));
					});
				},
				error:function(xhr, ajaxOptions, thrownError){
					$.growl.error({
						title: '',
						message: '更新失敗!'
					});
				}
			});
		},
		dragHandle: 'td .drag_group'
	});
	show_tabs();
	change_option();
}).on('click', 'a.del', function(){
	if(confirm('是否要刪除?')){
		return true;
	};
	return false;
});
$(document).ready(function(e) {
	member_search_to_id($('#id_member').val());
}).on('keypress', '#member_search', function(e){
	code = e.keyCode ? e.keyCode : e.which;
	if(code == 13)  e.preventDefault();	//取消ENTER送出表單
}).on('keypress', '.no_enter', function(e){
	code = e.keyCode ? e.keyCode : e.which;
	if(code == 13)  e.preventDefault();	//取消ENTER送出表單
}).on('keyup', '#member_search', function(e){
	if(ajax_member_search != undefined) ajax_member_search.abort();
	clearTimeout(ajax_member_keyup);
	ajax_member_keyup = setTimeout('member_search("'+$(this).val()+'")', 1000);
}).on('click', '.member_search_div .search', function(e){
	if(ajax_member_search != undefined) ajax_member_search.abort();
	clearTimeout(ajax_member_keyup);
	ajax_member_keyup = setTimeout('member_search("'+$('#member_search').val()+'")', 1);
}).on('click', '#show_member .add', function(e){
	var html = '<a href="/manage/Member?&editmember&id_member='+$(this).data('id')+'" target="_blank" class="form-control">'+$(this).data('email')+' '+$(this).data('name')+'</a><span class="input-group-btn"><button type="button" class="btn btn-default search"><span class="glyphicon glyphicon-refresh"></span>重新搜尋</span></button><button type="button" class="btn btn-default cancel hide"><span class="glyphicon glyphicon-remove"></span>取消</button>></div>';
	$('.member_search_div').addClass('hide');
	$('#show_member').html(html);
	$('#id_member').val($(this).data('id'));
}).on('click', '#show_member .search', function(e){
	$('.member_search_div').removeClass('hide')
	$('#show_member .cancel').removeClass('hide')
	$(this).addClass('hide');
}).on('click', '#show_member .cancel', function(e){
	$('#show_member .search').removeClass('hide')
	$(this).addClass('hide');
	$('.member_search_div').addClass('hide')
}).on('click', '.has-error .btn', function(e){
	$(this).parents('.form-group').removeClass('has-error')
}).on('click', '.hamburg', function(){
	if($(window).width() < 992){
		$('#admin_menu').addClass('action');
	};
}).on('click', '#admin_menu ul', function(event){
	if($(window).width() < 992 && event.target.nodeName == 'UL'){
		$('#admin_menu').removeClass('action');
	}
}).on('click', '#admin_menu .x img', function(){
	$('#admin_menu').removeClass('action');
	$('.submenu').hide();
}).on('click', '.media a.media_search', function(){
	$thead = $('.list .table thead');
	if($thead.css('display') == 'none'){
		$thead.css({"display":"block","opacity":"0"});
		$thead.stop(true, false).animate({"display":"block","opacity":"1"});
	}else{
		$thead.stop(true, false).fadeOut();
	}
}).on('click', '.field_list .title', function(){
	if($('.field_list').hasClass('active')){
		$('.field_list').removeClass('active');
		$('.fas', '.field_list .up').addClass('fa-angle-double-up').removeClass('fa-angle-double-down');
		$('.field', '.field_list').stop(true, false).slideUp();
	}else{
		$('.field_list').addClass('active');
		$('.fas', '.field_list .up').addClass('fa-angle-double-down').removeClass('fa-angle-double-up');
		$('.field', '.field_list').stop(true, false).slideDown();
	};
}).on('click', '.field_list .up', function(){
	if($('.field_list').hasClass('active')){
		$('.field_list').removeClass('active');
		$('.fas', '.field_list .up').addClass('fa-angle-double-up').removeClass('fa-angle-double-down');
		$('.field', '.field_list').stop(true, false).slideUp();
	}else{
		$('.field_list').addClass('active');
		$('.fas', '.field_list .up').addClass('fa-angle-double-down').removeClass('fa-angle-double-up');
		$('.field', '.field_list').stop(true, false).slideDown();
	};
}).on('click', 'input[type="number"][value="0"]', function(){
	$(this).attr('value', '');
	$(this).val('');
}).on('change', 'input[type="range"]', function(){
	var $this = $(this);
	var name = $this.data('to');
	if(name != undefined){
		$('input[name="'+name+'"]').val($this.val());
	};
}).on('change', 'input', function(){
	var $this = $(this);
	var name = $this.attr('name');
	if(name != undefined){
		$('input[data-to="'+name+'"]').val($this.val());
	};
});
//所見即所得 todo
tinymce.init({
	language:'zh_TW',
	selector:'textarea.tinymce',
	plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern"
  ],
  toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontsizeselect",
  toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
  menubar: false,
  toolbar_items_size: 'small',
  content_css: [
	  '//'+THEME_URL+'/css/reset.css',
	  '//'+THEME_URL+'/css/mani.css',
  ]
});