<?php
/**
 * Created by PhpStorm.
 * User: Allen
 * Date: 2019/9/16
 * Time: 上午 09:51
 */

class AdminCPanelController extends AdminController
{
	public $page = 'cPanel';
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{
		$this->className       = 'AdminCPanelController';
		$this->fields['title'] = 'cPanel設定';
		$this->display         = 'options';

		$this->fields['tabs'] = [
			'cPanel' => $this->l('cPanel設定'),
			'Email'  => $this->l('Email設定'),
		];

		$this->fields['form'] = [
			'cPanel' => [
				'tab'    => 'cPanel',
				'legend' => [
					'title' => $this->l('cPanel設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'cPanel_server_url',
						'type'          => 'text',
						'label'         => $this->l('Server URL'),
						'placeholder'   => '',
						'p'             => $this->l('cPanel登入網址'),
						'configuration' => true,
					],
					[
						'name'          => 'cPanel_ssl_enbled',
						'label'         => $this->l('HTTPS協定'),
						'type'          => 'switch',
						'val'           => 0,
						'values'        => [
							[
								'id'    => 'http',
								'value' => 0,
								'label' => $this->l('HTTP'),
							],
							[
								'id'    => 'https',
								'value' => 1,
								'label' => $this->l('HTTPS'),
							],
						],
						'configuration' => true,
					],
					[
						'name'          => 'cPanel_port',
						'type'          => 'text',
						'label'         => $this->l('port'),
						'placeholder'   => '2083',
						'p'             => $this->l('<div class="help-block">2082 - 作為特定cPanel帳戶的不安全調用</div>
						<div class="help-block">2083 - 將呼叫作為特定的cPanel帳戶進行安全保護</div>
						<div class="help-block">2095 - 通過Webmail會話進行不安全的呼叫</div>
						<div class="help-block">2096 - 通過Webmail會話安全呼叫</div>'),
						'configuration' => true,
					],
					[
						'name'          => 'cPanel_user',
						'type'          => 'text',
						'label'         => $this->l('管理者帳號'),
						'p'             => 'cPanel 管理者帳號',
						'configuration' => true,
					],
					[
						'name'          => 'cPanel_password',
						'type'          => 'text',
						'class'         => 'password',
						'label'         => $this->l('管理者密碼'),
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
			'Email'  => [
				'tab'    => 'Email',
				'legend' => [
					'title' => $this->l('Email 設定'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					[
						'name'          => 'cPanel_email_domain',
						'type'          => 'text',
						'label'         => $this->l('信箱domain'),
						'configuration' => true,
					],
					[
						'name'          => 'cPanel_webemail_url',
						'type'          => 'text',
						'label'         => $this->l('WebMail URL'),
						'configuration' => true,
					],
					[
						'name'          => 'cPanel_email_quota',
						'type'          => 'number',
						'label'         => $this->l('信箱配額'),
						'suffix'        => 'MB',
						'p'             => $this->l('預設配額500M,設定0為無限制'),
						'configuration' => true,
					],
					[
						'name'          => 'cPanel_skip_update_db',
						'label'         => $this->l('是否跳過電子郵件帳戶數據庫緩存的更新'),
						'p'             => $this->l('預設執行更新'),
						'type'          => 'switch',
						'val'           => 1,
						'values'        => [
							[
								'id'    => 'http',
								'value' => 0,
								'label' => $this->l('執行更新'),
							],
							[
								'id'    => 'https',
								'value' => 1,
								'label' => $this->l('跳過更新'),
							],
						],
						'configuration' => true,
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}
}