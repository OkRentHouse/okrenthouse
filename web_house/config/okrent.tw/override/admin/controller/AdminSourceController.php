<?php

class AdminSourceController extends AdminController
{
	public $no_link = false;

	public function __construct()
	{

		$this->className       = 'AdminSourceController';
		$this->table           = 'source';
		$this->fields['index'] = 'id_source';
		$this->fields['title'] = '開發來源';
		$this->actions[]       = 'list';
		$this->fields['list']  = [
			'id_source' => [
				'index'  => true,
				'type'   => 'checkbox',
				'select' => '',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'source'    => [
				'title'  => $this->l('開發來源'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'position'  => [
				'title' => $this->l('位置'),
				'order' => true,
				'class' => 'text-center',
			],
		];

		$this->fields['form'] = [
			[
				'legend' => [
					'title' => $this->l('開發來源'),
					'icon'  => 'icon-cogs',
					'image' => '',
				],
				'input'  => [
					'id_source' => [
						'name'     => 'id_source',
						'type'     => 'hidden',
						'index'    => true,
						'required' => true,
					],
					'source'    => [
						'name'        => 'source',
						'type'        => 'text',
						'label'       => $this->l('來源名稱'),
						'placeholder' => $this->l('請輸入來源名稱'),
						'maxlength'   => '20',
						'required'    => true,
					],
					'position'  => [
						'type'  => 'number',
						'label' => $this->l('位置'),
						'min'   => 0,
						'name'  => 'position',
					],
				],
				'submit' => [
					[
						'title' => $this->l('儲存'),
					],
				],
				'cancel' => [
					'title' => $this->l('取消'),
				],
				'reset'  => [
					'title' => $this->l('復原'),
				],
			],
		];
		parent::__construct();
	}


	public function processDel()
	{
		$id_source = Tools::getValue('id_source');
		$sql       = sprintf('SELECT `id_rent_house`
									FROM `' . DB_PREFIX_ . 'rent_house`
									WHERE `id_source` = %d
									LIMIT 0, 1',
			GetSQL($id_source, 'int'));
		Db::rowSQL($sql);
		if (Db::getContext()->num()) {
			$this->_errors[] = $this->l('無法刪除正在使用的開發來源');
		}
		parent::processDel();
	}
}