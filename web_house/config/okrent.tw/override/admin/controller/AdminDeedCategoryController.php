<?php

class AdminDeedCategoryController extends AdminController
{
	public $tpl_folder;    //樣版資料夾

	public function __construct()
	{
		$this->className       = 'AdminDeedCategoryController';
		$this->table           = 'deed_category';
		$this->fields['index'] = 'id_deed_category';

		$this->fields['order'] = ' ORDER BY `deed_category` ASC';
		$this->fields['title'] = '契據類別';

		$this->fields['list'] = [
			'id_deed_category'   => [
				'index'  => true,
				'type'   => 'checkbox',
				'hidden' => true,
				'class'  => 'text-center',
			],
			'deed_category' => [
				'title'  => $this->l('契據類別'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center',
			],
			'category_name'      => [
				'title'  => $this->l('類別名稱'),
				'order'  => true,
				'filter' => true,
				'class'  => 'text-center mw130',
			],
		];

		$this->fields['list_num'] = 50;

		$this->fields['form']['deed_category'] = [
			'tab'    => 'deed_category',
			'legend' => [
				'title' => $this->l('契據類別'),
				'icon'  => 'icon-cogs',
				'image' => '',
			],
			'input'  => [
				[
					'name'     => 'id_deed_category',
					'type'     => 'hidden',
					'index'    => true,
					'required' => true,
				],
				[
					'name'      => 'deed_category',
					'type'      => 'text',
					'label'     => $this->l('契據類別'),
					'maxlength' => 5,
					'required'  => true,
					'unique'    => true,
				],
				[
					'name'      => 'category_name',
					'type'      => 'text',
					'label'     => $this->l('類別名稱'),
					'maxlength' => 20,
					'required'  => true,
					'unique'    => true,
				],
			],
			'submit' => [
				[
					'title' => $this->l('儲存'),
				],
			],
			'cancel' => [
				'title' => $this->l('取消'),
			],
			'reset'  => [
				'title' => $this->l('重置'),
			],
		];

		parent::__construct();
	}

	public function processDel()
	{
		$id_web = Tools::getValue('id_web');
		$sql    = sprintf('SELECT `id_deed`
									FROM `' . DB_PREFIX_ . 'deed`
									WHERE `id_deed_category` = %d
									LIMIT 0, 1',
			GetSQL($id_web, 'int'));
		Db::rowSQL($sql);
		if (Db::getContext()->num()) {
			$this->_errors[] = $this->l('無法刪除正在使用的契據類別');
		}
		parent::processDel();
	}
}