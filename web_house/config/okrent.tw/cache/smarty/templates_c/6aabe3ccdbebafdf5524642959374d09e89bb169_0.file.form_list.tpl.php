<?php
/* Smarty version 3.1.28, created on 2019-09-26 09:56:16
  from "/home/ilifehou/life-house.com.tw/themes/default/form/form_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5d8c1ac0defe00_41432899',
  'file_dependency' => 
  array (
    '6aabe3ccdbebafdf5524642959374d09e89bb169' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/default/form/form_list.tpl',
      1 => 1567156682,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d8c1ac0defe00_41432899 ($_smarty_tpl) {
?>
<form action="" method="get" class="list" accept-charset="utf-8">
<div class="panel panel-default">
	<?php if ($_smarty_tpl->tpl_vars['fields']->value['list_heading_display']) {?><div class="panel-heading"><?php echo $_smarty_tpl->tpl_vars['fields']->value['title'];?>
 <samp class="badge label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="<?php echo l(array('s'=>"搜尋筆數"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['table_num']->value;?>
</samp><?php if (isset($_smarty_tpl->tpl_vars['fields']->value['show_list_num']) && !empty($_smarty_tpl->tpl_vars['fields']->value['show_list_num'])) {?><div class="pull-right input-group w180 pieces"><span class="input-group-addon"><?php echo l(array('s'=>"顯示"),$_smarty_tpl);?>
</span><input type="number" min="0" id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_m" name="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
Filter_m" class="form-control input-sm" value="<?php echo $_smarty_tpl->tpl_vars['fields']->value['list_num'];?>
" title="<?php echo l(array('s'=>"顯示幾筆資料"),$_smarty_tpl);?>
" placeholder="<?php echo l(array('s'=>"50"),$_smarty_tpl);?>
"><span class="input-group-addon"><?php echo l(array('s'=>"筆資料"),$_smarty_tpl);?>
</div><?php }?></div><?php }?>
      <?php if ($_smarty_tpl->tpl_vars['fields']->value['list_body_display']) {?>
	<div class="panel-body">
      <table class="table table-bordered"><?php echo $_smarty_tpl->tpl_vars['form_list_thead']->value;
echo $_smarty_tpl->tpl_vars['form_list_tbody']->value;
echo $_smarty_tpl->tpl_vars['form_list_tfoot']->value;?>
</table>
	</div>
	<?php }?>
      <?php if ($_smarty_tpl->tpl_vars['fields']->value['list_footer_display']) {?><div class="panel-footer"><?php echo $_smarty_tpl->tpl_vars['fields']->value['list_footer'];?>
</div><?php }?>
</div>
<?php echo $_smarty_tpl->tpl_vars['form_list_pagination']->value;?>

<input type="hidden" name="p" value="<?php echo $_GET['p'];?>
" />
<input type="hidden" name="m" value="<?php echo $_GET['m'];?>
" />
</form><?php }
}
