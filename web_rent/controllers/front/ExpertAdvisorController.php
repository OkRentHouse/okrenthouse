<?php
namespace web_rent;
use \IifeHouseController;
use \Tools;
use \Db;
use \File;
use \db_mysql;
use \Bank;
use \County;
use \Life_Master;

class ExpertAdvisorController extends IifeHouseController{
    public $page = 'ExpertAdvisor';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct(){
        $this->className = 'ExpertAdvisorController';
        $this->meta_title                = $this->l('加入生活');
        $this->page_header_toolbar_title = $this->l('加入生活');
        $this->no_FormTable              = true;

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('加入生活'),
            ],
            [
                'href' => '/ExpertAdvisor',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('生活家族'),
            ],
            //這塊?　對
        ];
        parent::__construct();
    }

    //初始化麵包屑        //參考

    public function initBreadcrumbs($tab_id = null){

        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }

        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }



    public function postProcess(){
        if($_POST['agree']=='on'){
            $validation = Life_Master::validation($_POST,$_FILES);
            if(!empty($validation['error'])){
                $this->_errors = $validation['error'];//錯誤訊息
//                print_r($_POST);
            }else{
                Life_Master::insert_data($_POST,$_FILES,0);
                $master_message_js =<<<js
            <script>
            $(document).ready(function(){
                alert("您已經申請完成請靜候通知!");
            });
            </srcript>
js;

                $this->context->smarty->assign([
                   'master_message_js'=>$master_message_js
                ]);
            }
            unset($_POST);
            unset($_FILES);
        }
    }

    public function initProcess(){
        $this->context->smarty->assign([
            'county'=>County::getContext()->database_county(),
            'bank'=>Bank::getContext()->bank,
        ]);

        parent::initProcess();
    }

    public function displayAjaxCity()
    {
        $action = Tools::getValue('action');    //action = 'City';
        $id_county = Tools::getValue('id_county');    //選定國家
        switch($action){
            case 'City':
                if($id_county==null){
                    echo json_encode(array("error"=>"","return"=>"請重新選擇縣市"));
                    break;
                }
                echo json_encode(array("error"=>"","return"=>County::getContext()->database_city($id_county)));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;//保險ajax能夠完整結束
    }

    public function setMedia(){
        parent::setMedia();
        $this->addCSS(THEME_URL."/css/joinmaster_form2.css");
        $this->addCSS(THEME_URL . '/css/joinmaster.css');
        $this->addCSS(THEME_URL . '/css/joinmaster_rwd.css');
    }
}
