<?php
namespace web_rent;

use \IifeHouseController;
use \Tools;
use \Db;
use \File;
use \db_mysql;
use \County;
use \Bank;
use \Life_Master;

class JoinMasterController extends IifeHouseController
{

    public $page = 'JoinMaster';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct(){

        $this->className = 'JoinMasterController';
        $this->meta_title                = $this->l('加入生活');
        $this->page_header_toolbar_title = $this->l('加入生活');
        $this->no_FormTable              = true;

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('加入生活'),
            ],
            //這塊?　對
        ];


        parent::__construct();
    }

    //初始化麵包屑        //參考

    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }

        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
            'url_p' => Tools::getValue('p'),
        ]);
    }


    public function initProcess(){
        $county = County::getContext()->database_county();
        $this->context->smarty->assign([
            'county'=>$county,
            'bank'=>Bank::getContext()->bank,
        ]);


        parent::initProcess();
    }

    public function postProcess(){
//        Life_Master::insert_data($_POST,$_FILES);
//        print_r($_POST);
//        print_r($_FILES)

//        unset($_POST);
//        unset($_FILES);

        parent::postProcess();
    }

    public function displayAjaxCity()
    {
        $action = Tools::getValue('action');    //action = 'City';
        $id_county = Tools::getValue('id_county');    //選定國家
        switch($action){
            case 'City':
                if($id_county==null){
                    echo json_encode(array("error"=>"","return"=>"請重新選擇縣市"));
                    break;
                }
                echo json_encode(array("error"=>"","return"=>County::getContext()->database_city($id_county)));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;//保險ajax能夠完整結束
    }

    public function setMedia()
    {
        parent::setMedia();
        if(!Tools::getValue('p')){$this->addCSS(THEME_URL . '/css/joinmaster_rwd.css');};
        $this->addCSS(THEME_URL . '/css/joinmaster_form2.css');
    }
}
