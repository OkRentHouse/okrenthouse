<?php

namespace web_rent;

use \IifeHouseController;
use \Tools;

class AboutController extends IifeHouseController

{

    public $page = 'About';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()

    {

        $this->meta_title = $this->l('關於樂租');

        $this->page_header_toolbar_title = $this->l('關於樂租');

        $this->className = 'WealthSharingController';

        $this->no_FormTable = true;

//        註解  CTRL + /

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/about',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('關於樂租'),
            ],
            //這塊?　對
        ];

        parent::__construct();
    }


    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function initProcess()
    {
     


        $js=<<<js
        AOS.init({ 
				once: false,
				mirror: true, 
				duration:1000,
			});
js;


        $this->context->smarty->assign([
           'js'=>$js
        ]);
        parent::initProcess();
    }


    public function setMedia()
    {
        $this->addJS('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js');
        $this->addJS('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js');
        $this->addJS('https://unpkg.com/aos@2.3.1/dist/aos.js');
        $this->addCSS('https://unpkg.com/aos@2.3.1/dist/aos.css');
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/about-animate.css');
        $this->addCSS(THEME_URL . '/css/about-normalize.css');
    }

}