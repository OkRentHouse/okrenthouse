<?php

namespace web_rent;

use \IifeHouseController;
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;

class LifeBuyController extends IifeHouseController
{
    public $page = 'LifeBuy';

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->className = 'LifeBuyController';

        $this->meta_title                = $this->l('生活樂購');
        $this->page_header_toolbar_title = $this->l('生活樂購');
        $this->no_FormTable              = true;

        //print_r($_SERVER);

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life_index',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('in 生活'),
            ],
            [
                'href' => '/Store',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('生活好康'),
            ],
            [
                'href' => '/LifeBuy',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('生活樂購'),
            ],
            //這塊?　對
        ];

        parent::__construct();
    }

    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

//    public function getStoreType($type = 'all')
//    {
//        $sql = 'SELECT * FROM `store_type` ORDER BY `position` ASC';
//        $arr = Db::rowSQL($sql);
//        return $arr;
//    }

    public function initProcess()
    {
        $id_product_class = Tools::getValue("id_product_class");
        $id_product_class_item = Tools::getValue("id_product_class_item");

//        $arr_store = Store::getContext()->get(null, 1, 30,$country,$area,$type,'head',$title);
//
//
//
//        $pagination = Db::getContext()->getJumpPage(5);
//        foreach ($arr_store as $i => $v) {
//            $arr_img                         = Store::getContext()->get_store_file_url($v['id_store'],0);
//            $arr_store[$i]['img']            = $arr_img[0];
//            $arr_store[$i]['discount']       = array_filter(explode(',', $v['discount']));
//            $arr_store[$i]['service']        = array_filter(explode(',', $v['service']));
//        }

        $sql = "SELECT * FROM product_class ORDER BY position ASC";
        $product_class_arr = Db::rowSQL($sql);

        if (!empty($id_product_class)) {
            $sql = "SELECT title FROM product_class WHERE id_product_class=".$id_product_class;
            $product_class_title = Db::rowSQL($sql, true);

            $sql = "SELECT *,p_c_i.title as item_title FROM product_class_item AS p_c_i 
            LEFT JOIN product_class AS p_c ON p_c_i.`id_product_class` = p_c.`id_product_class` 
            WHERE p_c_i.`active`=1 AND p_c.`active`=1 AND p_c_i.`id_product_class`=".$id_product_class;
            $product_class_item_arr = Db::rowSQL($sql);

//            print_r($product_class_item_arr);
        }

        $ip = $_SERVER['REQUEST_URI'];//頁籤active判斷

        $js =<<<js
<script>
    $(".regular").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        slidesToScroll: 3,
    });
</script>
js;




        $this->context->smarty->assign([
           'ip' => $ip,
           'product_class_arr' => $product_class_arr,
           'product_class_item_arr' => $product_class_item_arr,//暫時不用
           'id_product_class'  => $id_product_class,
           'id_product_class_item' => $id_product_class_item,
           'product_class_title' => $product_class_title,
           'js' => $js,
        ]);

//        $sql = 'SELECT li.`id_in_life`,li.`title`, ilf.`id_file` FROM `in_life` li
//                            LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
//                            WHERE ilf.`file_type`=2 AND li.`active`=1 ORDER BY li.`top` DESC ,li.id_in_life ASC';//旁邊顯示各種活動與連結
//
//        $row_all = Db::rowSQL($sql);
//
//        foreach ($row_all as $i => $v) {
//            $row = File::get($v['id_file']);
//            $row_all[$i]['img'] = $row['url'];
//        }
//        $this->context->smarty->assign([
//            'row_all' => $row_all,
//        ]);

        parent::initProcess();
    }


    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/good_buy_tab.css');
        $this->addCSS('/css/font-awesome.css');
//        $this->addCSS('/css/fontawesome-all.css');
        $this->addJS('/js/slick.min.js');
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');
    }
}
