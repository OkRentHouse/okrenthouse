<?php
namespace web_rent;
use \FrontController;
class WelcomController extends FrontController
{
	public $page = 'welcom';
	
	public $tpl_folder;	//樣版資料夾
	public $definition;
	public $_errors_name;
	
	public function __construct(){
		$this->meta_title = $this->l('歡迎頁');
		$this->page_header_toolbar_title = $this->l('歡迎頁');
		$this->className = 'WelcomController';
		$this->display_main_menu = false;
		$this->display_header = false;
		$this->display_footer = false;
		parent::__construct();
	}
	
}