<?php

namespace web_rent;

use \IifeHouseController;

use \Tools;

class WealthSharingController extends IifeHouseController

{

    public $page = 'WealthSharing';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()

    {

        $this->meta_title = $this->l('創富分享');

        $this->page_header_toolbar_title = $this->l('創富分享');

        $this->className = 'WealthSharingController';

        $this->no_FormTable = true;

//        註解  CTRL + /

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life_index',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('in 生活'),
            ],
            [
                'href' => '/WealthSharing',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('創富分享'),
            ],
            //這塊?　對
        ];

        parent::__construct();
    }


    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function initProcess()
    {
        $submenu = [
            [
                'text' => '活動分享',
                'href' => 'in_life',
            ],
            [
                'text' => '創富分享',
                'href' => 'WealthSharing',
                'active' => true,
            ],
            [
                'text' => '生活好康',
                'href' => 'Store',
            ],
            [
                'text' => '生活樂購',
                'href' => '#',
            ],
        ];


        $this->context->smarty->assign([
            'submenu' => $submenu,
        ]);

        parent::initProcess();
    }


    public function setMedia()

    {
        parent::setMedia();
    }

}