<?php

namespace web_rent;
use \Tools;
use \Db;
use \Member;
use \SMS;
use \IifeHouseController;

class RegisterController extends IifeHouseController
{
	public $page = 'registered';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('註冊會員');
		$this->page_header_toolbar_title = $this->l('註冊會員');
//		$this->fields['page']['class']   = 'orange_page';
		$this->className                 = 'RegisterController';
		$this->table                     = 'member';
//		$this->fields['index'] = 'id_member';
//		$this->no_FormTable    = false;
		parent::__construct();
	}
	public function initProcess()
	{
        $this->context->smarty->assign([
            'table' => $this->table
        ]);

		$this->tabAccess['add'] = true;
		$this->display          = 'add';
        parent::initProcess();
	}

	public function validateRules(){    //驗證基本資料的輸入
        $user    = Tools::getValue('user');
        $password = Tools::getValue('password');
        $check_password = Tools::getValue('check_password');
        $name = Tools::getValue('name');
        $nickname = Tools::getValue('nickname');
        $gender = Tools::getValue('gender');
		$agree = Tools::getValue('agree');
        $captcha = Tools::getValue('captcha');

        $sql = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($user, 'text');
        $row    = Db::rowSQL($sql, true);
        $captcha_log = $row['captcha']; //驗證碼



        if (empty($agree)) {
			$this->_errors[] = $this->l('您必須同意[服務條款]與[隱私權政策]');
		}
        if(strlen($name) < '2' || strlen($name) > '20'){
            $this->_errors[] = $this->l('未輸入姓名或是姓名長度不符');
        }
        if(strlen($nickname) < '2' || strlen($nickname) > '20'){
            $this->_errors[] = $this->l('未輸入暱稱或是暱稱長度不符');
        }
        if($gender !='0' && $gender !='1'){
            $this->_errors[] = $this->l('請選擇性別');
        }
        if(empty($user)){
            $this->_errors[] = $this->l('您必須填寫帳號');
        }else if(strlen($user)<'6' || strlen($user)>20){
            $this->_errors[] = $this->l('您的帳號不符合長度');
        }else if(!preg_match("/09[0-9]{8}/",$user)){
            $this->_errors[] = $this->l('請輸入手機號碼');
        }
        if (empty($captcha)) {
            $this->_errors[] = $this->l('您必須輸入驗證碼');
        }else if($captcha != $captcha_log){
            $this->_errors[] = $this->l('輸入驗證碼錯誤');
        }
        if(empty($password)){
            $this->_errors[] = $this->l('您必須填寫密碼');
        }else if(strlen($password)<'6' || strlen($password)>20){
            $this->_errors[] = $this->l('您的密碼不符合長度');
        }
        if(empty($check_password)){
            $this->_errors[] = $this->l('您必須填寫密碼');
        }else if($check_password != $password){
            $this->_errors[] = $this->l('再次輸入密碼必須與密碼相同');
        }

		parent::validateRules();
	}

    public function displayAjaxTel()//發送簡訊給
    {
        $action = Tools::getValue('action');    //action = 'Tel';
        $mobile = Tools::getValue('tel_code');    //手機號碼
        switch($action){
            case 'Tel':

                $sql = 'SELECT count(*) coun FROM member WHERE user='.GetSQL($mobile, 'text');
                $row     = Db::rowSQL($sql, true);
                $count =  $row['coun'];//該手機是否有申請

                $sql = "select count(*) coun from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($mobile, 'text');
                //避免10分鐘內重複撥打同支手機

                $row_session     = Db::rowSQL($sql, true);
                $session_count = $row_session["coun"];

                if(!preg_match("/09[0-9]{8}/",$mobile)){
                    echo json_encode(array("error"=>"tel error","return"=>"請輸入手機號碼 如:09XXXXXXXX123"));
                    break;
                }else if($count > '0'){
                    echo json_encode(array("error"=>"Repeat registration","return"=>"該帳號已註冊"));
                    break;
                }else if($session_count > '0'){
                    echo json_encode(array("error"=>"tel wait","return"=>"請等待10分鐘後再輸入手機號碼"));
                    break;
                }

                $user_ip = $_SERVER['REMOTE_ADDR'];//如果有異常的發送則能夠透過log得知
                $text = rand(1000,9999);//生成隨機的4碼
                $text_sms = '您的驗證碼為:'.$text;
                $user_id = '';
                $sms_id= '';
                $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
                //自己輸入否則會連 您的驗證碼為:都有影響輸入
                $sql = "INSERT INTO sms_log (`session_id`, `tel`, `captcha`, `ip`, `msgid`, `statuscode`,
                            `return_status`, `accountPoint`)
                            VALUES (".GetSQL(session_id(), 'text').",".GetSQL($mobile, 'text').",
                            ".GetSQL($text, 'text').",".GetSQL($user_ip, 'text').",".GetSQL($sms_return['msgid'],
                        'text').",".GetSQL($sms_return['statuscode'], 'text').",
                            ".GetSQL(SMS::sms_status_text($sms_return['statuscode'],"mitake"), 'text').",".GetSQL($sms_return['AccountPoint'], 'int').")";
                Db::rowSQL($sql);//存入資料庫

                if($sms_return['statuscode'] =='0' ||$sms_return['statuscode'] =='1' || $sms_return['statuscode'] =='2'|| $sms_return['statuscode'] =='4'){
                    echo json_encode(array("error"=>"","return"=>"請等候簡訊發送"));
                    break;
                }else{
                    echo json_encode(array("error"=>"","return"=>"簡訊發送有誤 請洽管理人員"));
                    break;
                }
               break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

    public function postProcess()
	{
		if (Tools::isSubmit('submitAdd' . $this->table)) {
			$user    = Tools::getValue('user');
			$password = Tools::getValue('password');
            $name = Tools::getValue('name');
            $nickname = Tools::getValue('nickname');
            $gender = Tools::getValue('gender');

			$this->validateRules();
			if (count($this->_errors))
				return;
			$tt = false;
			while (!$tt) {        //產生不重複會員條碼
				$barcode = random16number(16);
				$sql     = sprintf('SELECT `id_member` FROM `member` WHERE `barcode` = %s LIMIT 0, 1',
					GetSQL($barcode, 'text'));
				$row     = Db::rowSQL($sql, true);
				if (empty($row['id_member'])) {
					$tt = true;
				}
			}

			$_POST['barcode'] = $barcode;
			$this->fields['form'][0]['input']['barcode'] = [
				'name' => 'barcode',
				'type' => 'text',
			];

            $sql = 'SELECT count(user) as num,user FROM `'.$this->table.'` WHERE  `user`= '.GetSQL($user, 'text').'AND active=1 ';
                    //有user 代表有註冊過
            $row = Db::rowSQL($sql,true);

            if($row['num']<'1'){
                //測試帳號暫時先不啟用 等api串接完成方可註冊完畢後啟用
                $sql = 'INSERT INTO `'.$this->table.'` (`user`, `password`,`name`,`nickname`,`gender`,`barcode`,`active`,`id_member_additional`)
                    VALUES('.GetSQL($user,"text").', '.GetSQL($password,"text").',
                    '.GetSQL($name,"text").','.GetSQL($nickname,"text").',
                    '.GetSQL($gender,"int").','.GetSQL($barcode,"int").',
                    1,'.GetSQL('["1"]',"text").')';
                Db::rowSQL($sql);
                $_GET["success"] = "ok";
//                header('Location: https://www.okrent.house/login');//轉址
//                exit;
            }else{
                $this->_errors[] = $this->l('此手機已註冊過!');
            }

//            $this->setFormTable();
//			$num = $this->FormTable->insertDB();
//			if ($num) {
//				Member::login($user, $password);
//				Tools::redirectLink('/home');
//				exit;
//			} else {
//				$this->_errors[] = $this->l('此信箱已註冊過!');
//			};
		}
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/jquery.validate.min.js');
		$this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/localization/messages_zh_TW.js');
        $this->addCSS('/css/font-awesome.css');
	}
}