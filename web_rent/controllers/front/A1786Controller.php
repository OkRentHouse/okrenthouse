<?php

namespace web_rent;

use \IifeHouseController;
use \Tools;
use \ListUse;

class A1786Controller extends IifeHouseController

{

    public $page = 'A1786';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()

    {

        $this->meta_title = $this->l('關於樂租');

        $this->page_header_toolbar_title = $this->l('關於樂租');

        $this->className = 'WealthSharingController';

        $this->no_FormTable = true;

//        註解  CTRL + /

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/about',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('關於樂租'),
            ],
            //這塊?　對
        ];

				$this->display_header = false;

				$this->display_footer = false;

        parent::__construct();
    }


    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function initProcess()
    {


        parent::initProcess();
				$commodity_order_by = " ORDER by r_h.`featured` DESC,r_h.`id_rent_house` DESC";//精選優先 之後則是其他條件

		//精選的物件
		$commodity_sreach = " AND r_h.`featured`=1 ";
//        $commodity_sreach = " AND r_h.`rent_house_code` IN('HR-4A33','HR-2F32','AR-OA4','HR-3B292') ";
		$commodity_data = ListUse::commodity_data(null,1,999999,$commodity_sreach,$commodity_order_by);



js;
				//輪播
				        $js =<<<hmtl
				        <script>
				        $(".commodity_data").slick({
				        dots: false,
				        infinite: true,
				        autoplay: true,
				        autoplaySpeed: 3000,
				        slidesToShow: 5,
				        slidesToScroll: 5,

				    });


						    $(".slider_center").slick({
						        dots: false,
						        infinite: true,
						        centerMode: true,
						        slidesToShow: 5,
						        slidesToScroll: 9
						    });


				</script>
hmtl;

js;
				//輪播
				        $js1786bg =<<<hmtl
				        <script>
                $(window).scroll(function(){

                    $(".bg").css({backgroundPositionY:($(this).scrollTop()*-0.4)})
                })


				</script>
hmtl;

		$this->context->smarty->assign([
			'A1786_HTML' => $A1786_HTML,
		 'A1786_JS'   => $A1786_JS,
		 'A1786_CSS'  => $A1786_CSS,
		 'commodity_data' => $commodity_data,
		 'js'               => $js,
     'js1786bg'               => $js1786bg,
		]);

    }

		public function initContent()
		{
		  parent::initContent();
			$this->context->smarty->assign([
				'display_header'=>false,
			]);

		}


    public function setMedia()
    {
        $this->addJS('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js');
        $this->addJS('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js');
        $this->addJS('https://unpkg.com/aos@2.3.1/dist/aos.js');
        $this->addCSS('https://unpkg.com/aos@2.3.1/dist/aos.css');
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/about-animate.css');
        $this->addCSS(THEME_URL . '/css/about-normalize.css');
    }

}
