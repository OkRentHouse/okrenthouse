<?php

namespace web_rent;
use \IifeHouseController;
use \Tools;
use \Db;
use \Member;
use \SMS;

class ForgotController extends IifeHouseController
{
	public $page = 'Forgot';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('會員登入');
		$this->page_header_toolbar_title = $this->l('會員登入');
//		$this->fields['page']['class']   = 'orange_page';
		$this->className                 = 'ForgotController';
		parent::__construct();
//		$this->fields['form'] = [
//			'forgot' => [
//				'input' => [
//					'user' => [
//						'name'        => 'email',
//						'type'        => 'text',
//						'label'       => $this->l('email'),
//						'placeholder' => $this->l('email'),
//						'maxlength'   => '100',
//						'required'    => true,
//					],
//				],
//			],
//		];
	}

    public function displayAjaxTel()//發送簡訊給
    {
        $action = Tools::getValue('action');    //action = 'Tel';
        $mobile = Tools::getValue('tel_code');    //手機號碼
        switch($action){
            case 'Tel':

                $sql = 'SELECT count(*) coun FROM member WHERE user='.GetSQL($mobile, 'text');
                $row     = Db::rowSQL($sql, true);
                $count =  $row['coun'];//該手機是否有申請

                $sql = "select count(*) coun from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($mobile, 'text');
                //避免10分鐘內重複撥打同支手機

                $row_session     = Db::rowSQL($sql, true);
                $session_count = $row_session["coun"];

                if(!preg_match("/09[0-9]{8}/",$mobile)){
                    echo json_encode(array("error"=>"tel error","return"=>"請輸入手機號碼 如:09XXXXXXXX123"));
                    break;
                }else if(empty($count)){
                    echo json_encode(array("error"=>"No registration","return"=>"該帳號並不存在"));
                    break;
                }else if($session_count > '0'){
                    echo json_encode(array("error"=>"tel wait","return"=>"請等待10分鐘後再輸入手機號碼"));
                    break;
                }
                $sql = "SELECT password FROM `member` WHERE user=".GetSQL($mobile,"text");
                $tel_row     = Db::rowSQL($sql, true);
                $password =  $tel_row['password'];//該手機是否有申請

                $user_ip = $_SERVER['REMOTE_ADDR'];//如果有異常的發送則能夠透過log得知
                $text_sms = '您所遺忘的密碼為:'.$password;
                $user_id = '';
                $sms_id= '';
                $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
                //自己輸入否則會連 您的驗證碼為:都有影響輸入
                $sql = "INSERT INTO sms_log (`session_id`, `tel`, `sms_text`, `ip`, `msgid`, `statuscode`,
                            `return_status`, `accountPoint`)
                            VALUES (".GetSQL(session_id(), 'text').",".GetSQL($mobile, 'text').",
                            ".GetSQL($text_sms, 'text').",".GetSQL($user_ip, 'text').",".GetSQL($sms_return['msgid'],
                        'text').",".GetSQL($sms_return['statuscode'], 'text').",
                            ".GetSQL(SMS::sms_status_text($sms_return['statuscode'],"mitake"), 'text').",".GetSQL($sms_return['AccountPoint'], 'int').")";
                Db::rowSQL($sql);//存入資料庫

                if($sms_return['statuscode'] =='0' ||$sms_return['statuscode'] =='1' || $sms_return['statuscode'] =='2'|| $sms_return['statuscode'] =='4'){
                    echo json_encode(array("error"=>"","return"=>"請等候簡訊送達 您的密碼已經發送"));
                    break;
                }else{
                    echo json_encode(array("error"=>"","return"=>"簡訊發送有誤 請洽管理人員"));
                    break;
                }
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

	public function postProcess()
	{
//		$is_forgot = Tools::isSubmit('is_forgot');
//		$email     = Tools::getValue('email');
//		if (!empty($forgot)) {
//			$this->validateRules();
//			if (count($this->_errors) == 0) {
//				Member::forgot($email);
//				$this->_errors = array_merge($this->_errors, Member::getContext()->_errors);
//			}
//		}
	}

    public function setMedia()
    {
        parent::setMedia();
        $this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/jquery.validate.min.js');
        $this->addJS('/' . MEDIA_URL . '/jquery-validation-1.19.1/localization/messages_zh_TW.js');
        $this->addCSS('/css/font-awesome.css');
    }
}