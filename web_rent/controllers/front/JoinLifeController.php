<?php

namespace web_rent;

use \IifeHouseController;
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;

class JoinLifeController extends IifeHouseController
{
    public $page = 'JoinLife';

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->className = 'JoinLifeController';

        $this->meta_title                = $this->l('加入生活');
        $this->page_header_toolbar_title = $this->l('加入生活');
        $this->no_FormTable              = true;

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('加入生活'),
            ],
            //這塊?　對
        ];

        parent::__construct();
    }

    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }


    public function initProcess()
    {

        $this->context->smarty->assign([
        ]);

        parent::initProcess();
    }


    public function setMedia()
    {
        parent::setMedia();
    }
}