<?php

namespace web_rent;

use \IifeHouseController;
use \CMS;
use \Context;
use \Tools;
use \Db;

class CMSController extends IifeHouseController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'cms';
	public $html = '';
//	public padding-right: 70px;$css = '';
	public $js = '';

	public $arr_my_id_web = [0];
	public $arr_my_id_houses = [];

	public function __construct()
	{
		$this->className = 'CMSController';

		$this->fields['page']['cache'] = false;
		$this->display_edit_div        = false;

		parent::__construct();
		$this->no_page = true;
		//1:APP, 2:house, 3: lifegroup, 4:rent, 5:repair
		$cms                             = CMS::getByUrl(4, CMS::getContext()->url);

		if($cms['related_tabs']=='1'){//有選中則做此動作
            $sql = "SELECT title,url,id_cms,page_title FROM `cms` WHERE `related_tabs`='1' AND id_cms !=24 AND active=1 ORDER BY position ASC";
            $item_wrap = Db::rowSQL($sql);
            $item_wrap_body ='';
            foreach($item_wrap as $k => $v){
                if($item_wrap[$k]['id_cms'] !='12') {
                    if ($cms['page_title'] == $item_wrap[$k]['page_title']) {
                        $item_wrap_body .= '<td><a class="active"  href="' . $item_wrap[$k]['url'] . '">' . $item_wrap[$k]['page_title'] . '</td></a>';

                    } else {
                        $item_wrap_body .= '<td><a  href="' . $item_wrap[$k]['url'] . '">' . $item_wrap[$k]['page_title'] . '</td></a>';

                    }
                }
            }
            $item_wrap_html =<<<hmtl
            <div class="item_wrap">
                <div class="title">租事順利</div>
                <div class="item_list"><table><tbody><tr>{$item_wrap_body}</tr></tbody></table></div>
            </div>
hmtl;




            $cms['html'] = str_replace("{item_wrap}",$item_wrap_html,$cms['html']);
            $cms['css'] = <<<css
            {$cms['css']}
            /* 頁籤 start */
.credit_card_wrap .item_wrap {
 display: flex;
 align-items: center;
 justify-content: space-between;
 margin: 0px auto 40px;
}

.credit_card_wrap .item_wrap .title {
 width: 20%;
 color: #;
 font-size: 2em;
 font-weight: bold;
 letter-spacing: 1px;

}

.credit_card_wrap .item_wrap .item_list {
 display: flex;
 justify-content: space-between;
 align-items: flex-end;
 width: 80%;
}

.credit_card_wrap .item_wrap .item_list a {
 position: relative;
 color: var(--text_light_color);
 font-size: 1.1em;
 font-weight: bold;
 letter-spacing: 1px;
}

.credit_card_wrap .item_wrap .item_list a:hover {
 color: var(--main_color1);
 /* font-size: 1.3em; */
}

.credit_card_wrap .item_wrap .item_list a.active {
 color: var(--text_normal_color);
 font-size: 1.3em;
}

.credit_card_wrap .item_wrap .item_list a:after {
 content: "|";
 color: var(--text_light_color);
 position: absolute;
 top: -2px;
 right: -1.6vw;
 height: 20px;
 padding: 0px;
 margin: 0px;
 font-size: 1em;
 font-weight: normal;
}

.credit_card_wrap .item_wrap .item_list a:last-of-type:after {
 content: "";
}


/* 頁籤 end */
css;

        }
		$this->meta_title                = $cms['title'];
		$this->page_header_toolbar_title = $cms['page_title'];
		$this->meta_description          = $cms['description'];
		$this->meta_keywords             = $cms['keywords'];
		$this->nobots                    = !$cms['index_ation'];
		$this->nofollow                  = !$cms['index_ation'];
		$this->display_header            = $cms['display_header'];
		$this->display_footer            = $cms['display_footer'];
		$this->html                      = $cms['html'];
		$this->css                       = $cms['css'];
		$this->js                        = $cms['js'];

		$this->no_link = false;
	}

	public function initToolbar()
	{
		parent::initToolbar();
		$this->back_url = '#';
	}

    public function displayAjax()
    {
        $action = Tools::getValue('action');

        switch($action){
            case 'CustomerMessage':
                echo CMS::model_ajax();
                break;
            default :
                echo json_encode("no");
                break;
        }
    }

	public function initProcess()
	{
		parent::initProcess();

		$arr = $_SESSION['arr_house'][$_SESSION['house_index']];

		foreach ($_SESSION['arr_house'] as $i => $v) {
			$this->arr_my_id_web[]    = $v['id_web'];
			$this->arr_my_id_houses[] = $v['id_houses'];
		}
		Context::getContext()->smarty->assign([
			'web'        => $arr['web'],
			'arr_house'  => $_SESSION['arr_house'],
			'house_code' => $arr['house_code'],
			'name'       => $_SESSION['name'],
		]);
	}

	public function initContent()
	{
	    $sql = "SELECT `id_cms`,`title` FROM cms WHERE `customer_message`=1 AND url = '".CMS::getContext()->url."'";
        $row     = Db::rowSQL($sql, true);

	    //model 相關資料
        $model = CMS::get_model($row);

//       $model_css=<<<html
//html;

		parent::initContent();
		$this->context->smarty->assign([
			'cms_url' => CMS::getContext()->url,
			'html'    => $this->html,
			'cms_css'     => $this->css,
			'cms_js'      => $this->js,
            'id_cms'      =>$row['id_cms'],
            'model_html'  =>$model['model_html'],
            'model_js'    =>$model['model_js'],
//            'model_css'    =>$model_css,
		]);
		$this->setTemplate('cms.tpl');
	}

    public function setMedia()
    {
        parent::setMedia();

//        $this->addCSS(THEME_URL . '/css/cms.css');
        $sql = "SELECT count(*) count_row FROM cms WHERE `customer_message`=1 AND url = '".CMS::getContext()->url."'";
        $row     = Db::rowSQL($sql, true);

        if(!empty($row["count_row"])){
            $this->addCSS('/css/font-awesome.css');
        }
    }
}
