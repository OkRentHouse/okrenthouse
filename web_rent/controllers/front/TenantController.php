<?php

namespace web_rent;

use \IifeHouseController;
use \Db;
use \Config;
use \ListUse;

class TenantController extends IifeHouseController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'tenant';

	public function __construct()
	{
		$this->meta_title                = $this->l('樂租服務-租客篇');
		$this->page_header_toolbar_title = $this->l('樂租服務-租客篇');
		$this->className                 = 'TenantController';
		$this->no_FormTable              = true;

		$this->breadcrumbs = [
			[
				'href'  => '/',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('首頁'),
			],
			[
				'href'  => '/service',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('樂租服務'),
			],
			[
				'href'  => '',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('租客篇'),
			],
		];



		parent::__construct();
	}

	public function initProcess()
	{


		parent::initProcess();
		$commodity_order_by = " ORDER by r_h.`featured` DESC,r_h.`id_rent_house` DESC";//精選優先 之後則是其他條件

		//精選的物件
		$commodity_sreach = " AND r_h.`featured`=1 ";
//        $commodity_sreach = " AND r_h.`rent_house_code` IN('HR-4A33','HR-2F32','AR-OA4','HR-3B292') ";
		$commodity_data = ListUse::commodity_data(null,1,999999,$commodity_sreach,$commodity_order_by);

		$sql         = 'SELECT * FROM `tenant`';
		$row         = Db::rowSQL($sql);
		$Tenant_HTML = Config::get('Tenant_HTML');
		$Tenant_JS   = Config::get('Tenant_JS');
		$Tenant_CSS  = Config::get('Tenant_CSS');

js;
				//輪播
				        $js =<<<hmtl
				        <script>
				        $(".commodity_data").slick({
				        dots: false,
				        infinite: true,
				        autoplay: true,
				        autoplaySpeed: 3000,
				        slidesToShow: 5,
				        slidesToScroll: 5,

				    });


						    $(".slider_center").slick({
						        dots: false,
						        infinite: true,
						        centerMode: true,
						        slidesToShow: 5,
						        slidesToScroll: 9
						    });


				</script>
hmtl;

		$this->context->smarty->assign([
			'row'         => $row,
			'Tenant_HTML' => $Tenant_HTML,
			'Tenant_JS'   => $Tenant_JS,
			'Tenant_CSS'  => $Tenant_CSS,
			'commodity_data' => $commodity_data,
			'js'               => $js,
		]);
	}

	public function setMedia()
	{
		parent::setMedia();
		
		$this->addJS(THEME_URL . '/js/rent_service.js');
        $this->addCSS(THEME_URL . '/css/rent_service.css');
	}
}
