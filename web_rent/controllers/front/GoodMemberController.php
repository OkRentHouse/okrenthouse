<?php

namespace web_rent;

use \Tools;
use \IifeHouseController;
use \Db;
use \File;

class GoodMemberController extends IifeHouseController
{

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->className = 'GoodMemberController';

        $this->meta_title                = $this->l('好康會員');
        $this->page_header_toolbar_title = $this->l('好康會員');
        $this->no_FormTable              = true;
        $this->compartment = '3';//間格
        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life_index',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('in 生活'),
            ],
            [
                'href' => '/Store',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('生活好康'),
            ],
            [
                'href' => '/GoodMember',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('好康會員'),
            ],
            //這塊?　對
        ];

        parent::__construct();
    }

    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function displayAjaxAddhtml(){
        $action = Tools::getValue('action');    //action = 'Addhtml';
        switch ($action){
            case 'Addhtml':
                $count = Tools::getValue('count');
                $count_now = Tools::getValue('count_now');
                $count_add = $count_now+$this->compartment;//間格多少

                if($count_add>=$count){//防止超過
                    $count_add = $count;
                }


                $sql = 'SELECT * FROM `announcement` AS a
                LEFT JOIN `announcement_file` AS a_f ON a.`id_announcement` = a_f.`id_announcement`
 WHERE a.`active`=1 AND a.select_option=0 ORDER BY a.`position` ASC , a.`id_announcement` DESC limit '.$count_now.','.$count_add;

                $row_all = Db::rowSQL($sql);
                foreach ($row_all as $i => $v) {
                    $row = File::get($v['id_file']);
                    $row_all[$i]['img'] = $row['url'];
                    $row_all[$i]['create_time'] = substr($row_all[$i]['create_time'],0,11);
                }

                $return = '';

                foreach($row_all as $k=> $v){
                    $return['addhtml'] .='<li class="list"><div class="image_wrap"><img src="'.$v['img'].'"></div>
                    <div class="text_wrap"><div class="caption_wrap"><div class="caption">'.$v[title].'</div>
                    <div class="date">'.$v['create_time'].'</div></div>'.$v['content'].'<span>
                    <a href="/GoodMember?id_announcement='.$v['id_announcement'].'">more</a></span></div></li>';
                }
                echo json_encode(array("error"=>"","return"=>$return));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }

    public function initProcess()
    {
        $id_announcement      = Tools::getValue('id_announcement');

        $sql = "SELECT count(*) count_log  FROM `announcement` AS a
                LEFT JOIN `announcement_file` AS a_f ON a.`id_announcement` = a_f.`id_announcement`
 WHERE a.`active`=1 AND a.select_option=0 ORDER BY a.`position` ASC , a.`id_announcement` DESC ";

        $row_sql = Db::rowSQL($sql,true);//長度 用於ajax使用

        $sql = 'SELECT * FROM `announcement` AS a
                LEFT JOIN `announcement_file` AS a_f ON a.`id_announcement` = a_f.`id_announcement`
 WHERE a.`active`=1 AND a.select_option=0 ORDER BY a.`position` ASC , a.`id_announcement` DESC LIMIT 0,'.$this->compartment;



        $ip = $_SERVER['REQUEST_URI'];//頁籤active判斷


//        echo $sql;

        $row_all = Db::rowSQL($sql);

        foreach ($row_all as $i => $v) {
            $row = File::get($v['id_file']);
            $row_all[$i]['img'] = $row['url'];
            $row_all[$i]['create_time'] = substr($row_all[$i]['create_time'],0,11);
        }


        $sql = "SELECT * FROM announcement WHERE `active`=1 AND select_option=0 AND id_announcement=".$id_announcement;

        $announcement_arr = Db::rowSQL($sql,true);

        $this->context->smarty->assign([
           'row_all' => $row_all,
            'ip' => $ip,
            'row_sql' => $row_sql,
            'compartment' =>$this->compartment,
            'announcement_arr' => $announcement_arr,
        ]);

        parent::initProcess();
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/good_tab.css');
    }
}