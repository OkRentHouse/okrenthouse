<?php

namespace web_rent;
use \IifeHouseController;
use \Tools;
use \SearchHouse;
use \RentHouse;
use \Db;
use \db_mysql;
use \File;
use \ListUse;

class ListController extends IifeHouseController

{
    //File::get($row_id['id_file']);
    public $page = 'list';
	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;
	public function __construct()
	{
		$type                            = Tools::getValue('type');
		$this->meta_title                = $this->l('吉屋快搜');
		$this->page_header_toolbar_title = $this->l('吉屋快搜');
		$this->className                 = 'ListController';
    $this->display_footer = true;
        $this->table = 'rent_house';//主資料表
        $this->no_FormTable              = true;
        $this->THEME_URL = "/themes/Rent";
        $transfer_in = Tools::getValue("transfer_in");
        if($transfer_in==1){
            $this->breadcrumbs               = [
                [
                    'href'  => '/',
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l('首頁'),
                ],
                [
                    'href'  => '/list',
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l('吉屋快搜'),
                ],
            ];



            if(!empty(Tools::getValue("county"))){
                $this->breadcrumbs[] =
                [
                    'href'  => '/list?county='.Tools::getValue("county"),
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l(Tools::getValue("county")),
                ];
            }


            if(!empty(Tools::getValue("city"))){
                $city_s = "";
                foreach(Tools::getValue("city") as $key => $value){
                    $city_s = empty($city_s)?$value:$city_s.','.$value;
                }

                $this->breadcrumbs[] =
                    [
                        'href'  => '/list?city_s='.$city_s,
                        'hint'  => $this->l(''),
                        'icon'  => '',
                        'title' => $this->l(Tools::getValue("county").' '.$city_s),
                    ];
            }

            if(!empty(Tools::getValue("id_main_house_class"))){
                $id_main_house_class_s = "";
                foreach(Tools::getValue("id_main_house_class") as $key => $value){
                    $id_main_house_class_s = empty($id_main_house_class_s)?$value:$id_main_house_class_s.','.$value;
                }

                
                $title = "";
                $sql = "SELECT title FROM main_house_class WHERE id_main_house_class IN(".$id_main_house_class_s.") ORDER BY id_main_house_class ASC";
                $get_data = Db::rowSQL($sql);
                foreach($get_data as $i=>$v){
                    $title = empty($title)?$v["title"]:$title.','.$v["title"];
                }

                $this->breadcrumbs[] =
                    [
                        'href'  => '/list?id_main_house_class_s ='.Tools::getValue("id_main_house_class_s"),
                        'hint'  => $this->l(''),
                        'icon'  => '',
                        'title' => $this->l($title),
                    ];
                unset($get_data);//避免被重複使用
                unset($title);//避免被重複使用
            }

            if(!empty(Tools::getValue("id_types"))){
                $id_types_s = "";
                foreach(Tools::getValue("id_types") as $key => $value){
                    $id_types_s = empty($id_types_s)?$value:$id_types_s.','.$value;
                }

                $title = "";
                $sql = "SELECT title FROM rent_house_types WHERE id_rent_house_types IN(".$id_types_s.") ORDER BY id_main_house_class ASC";
                $get_data = Db::rowSQL($sql);
                foreach($get_data as $i=>$v){
                    $title = empty($title)?$v["title"]:$title.','.$v["title"];
                }

                $this->breadcrumbs[] =
                    [
                        'href'  => '/list?id_types_s ='.Tools::getValue("id_types_s"),
                        'hint'  => $this->l(''),
                        'icon'  => '',
                        'title' => $this->l($title),
                    ];
                unset($get_data);//避免被重複使用
                unset($title);//避免被重複使用
            }

            if(!empty(Tools::getValue("id_type"))){
                $id_type_s = "";
                foreach(Tools::getValue("id_type") as $key => $value){
                    $id_type_s = empty($id_type_s)?$value:$id_type_s.','.$value;
                }

                $title = "";
                $sql = "SELECT title FROM rent_house_type WHERE id_rent_house_type IN(".$id_type_s.") ORDER BY id_main_house_class ASC";
                $get_data = Db::rowSQL($sql);
                foreach($get_data as $i=>$v){
                    $title = empty($title)?$v["title"]:$title.','.$v["title"];
                }

                $this->breadcrumbs[] =
                    [
                        'href'  => '/list?id_type_s ='.Tools::getValue("id_type_s"),
                        'hint'  => $this->l(''),
                        'icon'  => '',
                        'title' => $this->l($title),
                    ];
                unset($get_data);//避免被重複使用
                unset($title);//避免被重複使用
            }

            if(empty(Tools::getValue("county")) && empty(Tools::getValue("city_s")) && empty(Tools::getValue("id_main_house_class")) && empty(Tools::getValue("id_types")) && empty(Tools::getValue("id_type"))){
                $this->breadcrumbs[] =
                    [
                        'href'  => '/list',
                        'hint'  => $this->l(''),
                        'icon'  => '',
                        'title' => $this->l("全部"),
                    ];
            }


        }else{
            $this->breadcrumbs               = [
                [
                    'href'  => '/',
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l('首頁'),
                ],
                [
                    'href'  => '/list',
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l('吉屋快搜'),
                ],
                [
                    'href'  => '/list',
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l('條件搜尋'),
                ],
            ];
        }
		parent::__construct();
	}

	public function initProcess()
	{
        // 店長說不要出現預設搜尋 06/04/2021
        if($_GET['search'] ==''){
                // $_GET['min_room'] = '0';
                // $_GET['max_room'] = '6';
                // $_GET['min_price'] = '10000';
                // $_GET['max_price'] = '100000';
                // $_GET['min_ping'] = '10';
                // $_GET['max_ping'] = '100';
        } else {
                // $_GET['min_room'] = '0';
                // $_GET['max_room'] = '6';
                // $_GET['min_price'] = '10000';
                // $_GET['max_price'] = '100000';
                // $_GET['min_ping'] = '10';
                // $_GET['max_ping'] = '100';
                $_POST['search'] = $search;
        }


		SearchHouse::getContext()->getGet();
//        Tools::getValue("county");
        //print_r($_GET);
        $county = Tools::getValue("county");//縣直轄市
        $city = Tools::getValue("city");//鄉鎮市的陣列
        $id_type = Tools::getValue("id_type");//型態的陣列
        $id_types = Tools::getValue("id_types");//類別的陣列
        $id_device = Tools::getValue("id_device");//設備的陣列
        $id_other = Tools::getValue("id_other");//其他條件的陣列
        $max_price = Tools::getValue("max_price");//最高金額
        $min_price = Tools::getValue("min_price");//最低金額
        $max_ping = Tools::getValue("max_ping");//最高坪數
        $min_ping = Tools::getValue("min_ping");//最低坪數
        $max_room = Tools::getValue("max_room");//最多房數
        $min_room = Tools::getValue("min_room");//最低房數
        $id_device_class = Tools::getValue("id_device_class");//設備主類別的陣列
        $search = Tools::getValue("search");//鄉鎮市的陣列
        $id_member = $_SESSION["id_member"];//favorite要用的
        $session_id = session_id();//favorite要用的
        $favorite_data = "";//favorite要用的
        $active = Tools::getValue("active"); //參值

        //print_r($search);




//        $arr_house = $row = Db::rowSQL($sql, false, $num);
        $arr_house = ListUse::getContext()->get(null,1,20,$county,$city,
            $id_type,$id_types,$id_device,$id_device_class,$id_other,$max_price,$min_price,$max_ping,$min_ping,
            $max_room,$min_room,$search);

        $pagination = Db::getContext()->getJumpPage(10);

        if(!empty($id_member)){//有
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND is_member=".GetSQL($id_member,"int");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個is_member
        }else{//只有session_id
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND session_id=".GetSQL($session_id,"text");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個session_id
        }

        foreach ($arr_house as $i => $v) {
            //物件型態
            $sql = "SELECT * FROM rent_house_types WHERE id_rent_house_types IN (".Db::antonym_array($arr_house[$i]['id_rent_house_types']).")";
            $sql_types = Db::rowSQL($sql, false);
            $arr_house[$i]['sql_types'] = $sql_types;//整批搜索到的丟進去

            //物件類別
            $sql = "SELECT * FROM rent_house_type WHERE id_rent_house_type IN (".Db::antonym_array($arr_house[$i]['id_rent_house_type']).")";
            $sql_type = Db::rowSQL($sql, false);
            $arr_house[$i]['sql_type'] = $sql_type;//整批搜索到的丟進去

            //我的最愛start
            foreach($favorite_data as $key => $value){
                if($arr_house[$i]["id_rent_house"] == $favorite_data[$key]["table_id"]){//上面搜索了該table與本次使用者的資訊 這邊濾同id
                    $arr_house[$i]["favorite"] = $favorite_data[$key]["active"];
                }
            }
            //end

            //這邊要做處理圖片 LIST 只處理版面 所以讀版面
            $sql = "SELECT * FROM rent_house_file WHERE file_type=0 AND id_rent_house=".$v['id_rent_house'].' ORDER BY id_rhf ASC ';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $arr_house[$i]['img'] = $row['url'];
            //圖片end
            if($arr_house[$i]['id_web'] =='1'){//改名
                $arr_house[$i]['w_title'] = "藝文直營店";//改名
            }
            //處理 rent_house_types的資料
            $sql = "SELECT * FROM rent_house_types WHERE id_rent_house_types IN (".Db::antonym_array($arr_house[$i]['id_rent_house_types']).")";
//            echo $sql.'</br>';
            $sql_types = Db::rowSQL($sql, false);
            $arr_house[$i]['sql_types'] = $sql_types;//整批搜索到的丟進去

            //tag start ---
            $sql = "SELECT * FROM other_conditions WHERE id_other_conditions IN (".Db::antonym_array($arr_house[$i]['id_other_conditions']).")
            ORDER BY position ASC";

            $sql_tag = Db::rowSQL($sql, false);
            $tag_body = "";
            foreach($sql_tag as $key => $value){
                if($sql_tag[$key]['svg_url']!=""){
                $tag_body .=<<<html
            <i>
				<img src="{$sql_tag[$key]['svg_url']}" alt="{$sql_tag[$key]['title']}" title="{$sql_tag[$key]['title']}">
				<p>{$sql_tag[$key]['title']}</p>
			</i>
html;
                }
            }
            //這邊是作tagh
            $arr_house[$i]['tag'] =<<<hmtl
            <tag>
                {$tag_body}
                <div class="clear"></div>
            </tag>
hmtl;
            //tag end ---
//            print_r($sql_types);

        //右側相關資料start


            $sql = "SELECT *,(IF(m.`gender`=1,'先生','小姐')) as member_gender FROM member as m
        LEFT JOIN `broker` as b ON b.`id_member` = m.`id_member`
        WHERE m.`id_member`=".GetSQL($arr_house[$i]["id_member"],"int");
            $member_data = Db::rowSQL($sql,true);

//          tag的金額
            $management_fee = $arr_house[$i]['s1']==1?'含管理費':'';
            $price = '<div class="price"><price>'.$arr_house[$i]['rent_cash'].'<unit>元/月</unit>
<div class="cost">'.$management_fee.'</div></price></div>';


//        print_r($member_data);
        $consultant = "";
            if(strstr($member_data["id_member_additional"],"3")){//房仲
                $arr_house[$i]["user_html"]=<<<hmtl
        <div class="data">
            {$price}
            <div class="title">
                <span>{$member_data["name"]}</span> {$member_data["member_gender"]}收取服務費
            </div>
            <div class="number"><img src="{$this->THEME_URL}/img/icon/house_search_phone_01.svg" alt="">{$member_data["user"]}</div>
            <div class="number"><img src="{$this->THEME_URL}/img/icon/house_search_phone_02.svg" alt="">{$member_data["company_tel"]}</div>
            <div class="btn_wrap">
                <a class="" href="#modal-container1" role="button" data-toggle="modal" data-code="{$arr_house[$i]["rent_house_code"]}"  data-id="{$arr_house[$i]["id_rent_house"]}" data-case_name="{$arr_house[$i]["case_name"]}" data-type="1" onclick="message_get(this);">留言</a>
                <i></i>
                <a class="" href="#modal-container2" role="button" data-toggle="modal" data-code="{$arr_house[$i]["rent_house_code"]}"  data-id="{$arr_house[$i]["id_rent_house"]}" data-case_name="{$arr_house[$i]["case_name"]}" data-type="2" onclick="message_get(this);">預約賞屋</a>
            </div>
	    </div>
hmtl;
            }else if(strstr($member_data["id_member_additional"],"4")){//房東自上
                $arr_house[$i]["user_html"]=<<<hmtl

        <div class="data">
            {$price}
            <div class="title">
                <span>{$member_data["name"]}</span> {$member_data["member_gender"]}
            </div>
            <div class="number"><img src="{$this->THEME_URL}/img/icon/house_search_phone_01.svg" alt="">{$member_data["user"]}</div>
            <div class="number"><img src="{$this->THEME_URL}/img/icon/house_search_phone_02.svg" alt="">{$member_data["tel"]}</div>
            <div class="btn_wrap">
                <a class="" href="#modal-container1" role="button" data-toggle="modal" data-code="{$arr_house[$i]["rent_house_code"]}"  data-id="{$arr_house[$i]["id_rent_house"]}" data-case_name="{$arr_house[$i]["case_name"]}" data-type="1" onclick="message_get(this);">留言</a>
                <i></i>
                <a class="" href="#modal-container2" role="button" data-toggle="modal" data-code="{$arr_house[$i]["rent_house_code"]}"  data-id="{$arr_house[$i]["id_rent_house"]}" data-case_name="{$arr_house[$i]["case_name"]}" data-type="2" onclick="message_get(this);">預約賞屋</a>
            </div>
	    </div>
hmtl;
            }else{//我們這邊
                $consultant = "1";
                if($arr_house[$i]["w_title"]=="桃園總店"){
                    $arr_house[$i]["w_title"] = "藝文直營店";
                }
                if(empty($arr_house[$i]["ag_phone"])){
                    $arr_house[$i]["ag_phone"] = "0939-688089";
                }
                if(empty($arr_house[$i]["develop_ag"])){
                    $arr_house[$i]["develop_ag"] = "房似錦";
                }
                if(empty($member_data["member_gender"])){
                    $member_data["member_gender"] = "小姐";
                }

                $arr_house[$i]["user_html"]=<<<hmtl

        <div class="data">
            {$price}
            <div class="title">
                <span>{$arr_house[$i]["develop_ag"]}</span> 顧問<i></i>收取服務費
            </div>
            <div class="number"><img src="{$this->THEME_URL}/img/icon/house_search_phone_01.svg" alt="">{$arr_house[$i]["ag_phone"]}</div>
            <div class="number"><img src="{$this->THEME_URL}/img/icon/house_search_phone_02.svg" alt="">03-358-1098</div>
            <div class="btn_wrap">
                <a class="" href="#modal-container1" role="button" data-toggle="modal" data-code="{$arr_house[$i]["rent_house_code"]}"  data-id="{$arr_house[$i]["id_rent_house"]}" data-case_name="{$arr_house[$i]["case_name"]}" data-type="1" onclick="message_get(this);">留言</a>
                <i></i>
                <a class="" href="#modal-container2" role="button" data-toggle="modal" data-code="{$arr_house[$i]["rent_house_code"]}"  data-id="{$arr_house[$i]["id_rent_house"]}" data-case_name="{$arr_house[$i]["case_name"]}" data-type="2" onclick="message_get(this);">預約賞屋</a>
                <i></i>
                <a class="" href="#modal-container3" role="button" data-toggle="modal" data-code="{$arr_house[$i]["rent_house_code"]}"  data-id="{$arr_house[$i]["id_rent_house"]}" data-case_name="{$arr_house[$i]["case_name"]}" data-type="3" onclick="message_get(this);">呼叫樂租顧問</a>
            </div>
	    </div>
hmtl;
            }

//    print_r($_GET);

        //右側相關資料end　
        }

//        print_r($arr_house);
        $hidden_open_search = ($active==1||!empty($county))&&!empty($arr_house)?'更多搜尋<i class="far fa-plus-square" aria-hidden="true"></i>':'收和搜尋<i class="far fa-minus-square"></i>';
        $open_status =    ($active==1||!empty($county))&&!empty($arr_house)?'1':'0';

        $search_hidden = '';
        if(empty($arr_house)){
            $search_hidden = '$(".process_bar").css("display","none");
					$(".no_search_data").css("margin-top","-45px");';
        }else{
            $search_hidden = '$("article .breadcrumb").css("padding-bottom", "4em");';
        }
        $index_hidden=(Tools::getValue("transfer_in")==1&&!empty($arr_house))?'$("#open_search").hide(); $("#search_box_big").hide();':'';//10-13 因index有搜索到值要隱藏顧這樣寫


//        因為店長要做成連動by 2020/10/19

//這邊要做js變更id_main_house_class的child部分
        $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
        $main_house_class = Db::rowSQL($sql);
        $rent_house_types = [];
        $rent_house_type = [];

        foreach($main_house_class as $k => $v){
            $sql = "SELECT * FROM rent_house_types WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%'
            Order by position ASC";
            $rent_house_types[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
            $sql = "SELECT * FROM rent_house_type WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%'
            Order by position ASC";
            // print_r($sql);
            $rent_house_type[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
        }



        $json_rent_house_types = json_encode($rent_house_types,JSON_UNESCAPED_UNICODE);
        $json_rent_house_type = json_encode($rent_house_type,JSON_UNESCAPED_UNICODE);


        $county = Tools::getValue("county");//縣直轄市
        $city = Tools::getValue("city");//鄉鎮市的陣列
        $id_type = Tools::getValue("id_type");//型態的陣列
        $id_types = Tools::getValue("id_types");//類別的陣列
        $id_device = Tools::getValue("id_device");//設備的陣列
        $id_other = Tools::getValue("id_other");//其他條件的陣列
        $max_price = Tools::getValue("max_price");//最高金額
        $min_price = Tools::getValue("min_price");//最低金額
        $max_ping = Tools::getValue("max_ping");//最高坪數
        $min_ping = Tools::getValue("min_ping");//最低坪數
        $max_room = Tools::getValue("max_room");//最多房數
        $min_room = Tools::getValue("min_room");//最低房數
        $id_device_class = Tools::getValue("id_device_class");//設備主類別的陣列
        //$search_house = Tools::getValue("search_house");//搜尋關鍵字
        $search = Tools::getValue("search");//搜尋關鍵字
        //$search = Tools::getValue("search")

        //print_r($search_house);
        //print_r($search);

        $tag_status = "0";
        foreach($_GET as $key =>$value){
            if(($key=='search' && $value==$search)
                ) continue;
                if(!empty($value)){
                    $tag_status = '1';
                }
        }

        if($tag_status==0){
            $tag_status = '$(".tag_div").hide()';
        }else{
            $tag_status = '$(".tag_div").show()';
        }


//        print_R($_GET);

//        echo $tag_status;



        $js =<<<js
    function message_get(o){
        var code = $(o).data("code");
        var id = $(o).data("id");
        var case_name = $(o).data("case_name");
        var type = $(o).data("type");
        $("#modal-container"+type+" [name='id_rent_house']").val(id);
        $("#modal-container"+type+" [name='case_name']").val(case_name);
        $("#modal-container"+type+" [name='rent_house_code']").val(code);
    }

    function call_reset_main_data(){//處理點選住家那區塊後的動作
        var level_id_types = {$json_rent_house_types};
        var level_id_type = {$json_rent_house_type};
        var id_main_house_class = '';
        $.each($("input[name='id_main_house_class[]']"),function(n,value){
            if(value.checked){
                if(id_main_house_class==''){
                    id_main_house_class = $(value).val();
                }else{
                    id_main_house_class = id_main_house_class+','+$(value).val();
                }
            }
         });

        //先用each隱藏 型態 類別 並且拔掉選擇
        $.each($("input[name='id_types[]']"),function(n,value){
            $(value).attr("checked",false);
            $(value).next().hide();
         });
        $.each($("input[name='id_type[]']"),function(n,value){
            $(value).attr("checked",false);
            $(value).next().hide();
         });

        $.each($("input[name='id_other[]']"),function(n,value){
            $(value).attr("checked",false);
            $(value).next().hide();
         });

        //顯示被選擇到的
        if(id_main_house_class==''){
            id_main_house_class = '1,2,3';
        }

        id_main_house_class_arr = id_main_house_class.split(",");
        $.each(id_main_house_class_arr,function(n,value){
             $.each(level_id_types,function(types_n,types_value){
                  if(value !=types_n) return;
                  $.each(types_value,function(types_value_n,types_value_value){
                      if(types_value_value["id_main_house_class"].search(value) =="-1" || ( (value=='2' && id_main_house_class.search("1")!='-1') &&  (types_value_value["id_rent_house_types"]==18 || types_value_value["id_rent_house_types"]==5 || types_value_value["id_rent_house_types"]==1))) return; //不包含並解奘態為2但有1 排除電梯大樓 公寓 透天
                           console.log(types_value_value);
                           console.log(types_value_value["id_rent_house_types"]);
                           $("#id_types_"+types_value_value["id_rent_house_types"]).next().show();
                    });
                });
              $.each(level_id_type,function(types_n,type_value){
                  $.each(type_value,function(type_value_n,type_value_value){
                        if(value.search(type_value_value["id_main_house_class"])) return;
                            console.log(type_value_value);
                            console.log(type_value_value["id_rent_house_type"]);
                           $("#id_type_"+type_value_value["id_rent_house_type"]).next().show();
                    });
                });

             $.each($("input[name='id_other[]']"),function(main_n,main_value){
                    var main_n_data = $(main_value).data("id_main_house_class");
                    main_n_data = String(main_n_data);

                    if(main_n_data.search(value) !='-1'){
                        $(main_value).next().show();
                    }
              });
         });
    }

        $(document).ready(function(){
            // //執行首頁的顯示隱藏
            // call_reset_main_data();
           {$tag_status}

             var open_status = '{$open_status}';
            $(".open_search").html('{$hidden_open_search}');
            if(open_status=='1'){
                $(".process_bar_data_1").hide();
                $(".process_bar_data_2").hide();
                $("#search_box_big").hide();
                $("#open_search").val("0");
            }else{
                $(".process_bar_data_1").show();
                $(".process_bar_data_2").show();
                $("#search_box_big").show();
                $("#open_search").val("1");
            }

        $("#open_search").click(function(){
            if($("#open_search").val()==1){
                $(".open_search").html('展開搜尋<i class="far fa-plus-square" aria-hidden="true"></i>');
                $(".process_bar_data_1").hide();
                $(".process_bar_data_2").hide();
                $("#search_box_big").hide();
                $("#open_search").val("0");
            }else if($("#open_search").val()==0){
                $(".open_search").html('收和搜尋<i class="far fa-minus-square"></i>');
                $(".process_bar_data_1").show();
                $(".process_bar_data_2").show();
                $("#search_box_big").show();
                $("#open_search").val("1");
            }
        });
        {$search_hidden}
        {$index_hidden}

       $("input[name='id_main_house_class[]']").click(function(){
          call_reset_main_data();
        });

    });
js;
        //店長說不要 search_process by 10/12

		$default_price_step = 1000;
		$default_ping_step  = 0;
        $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
        $main_house_class = Db::rowSQL($sql);

        //print_r($search);

		$this->context->smarty->assign([
            'js'  =>$js,
			'select_county'           => RentHouse::getContext()->getCounty('none'),
			'arr_city'                => RentHouse::getContext()->getCity('none'),
			//			'select_use'    => RentHouse::getContext()->getUse('none'),	//20200213 用途拿掉
			'select_type'             => RentHouse::getContext()->getType(),
			'select_types'            => RentHouse::getContext()->getTypes(),
            'select_device_category_class' => RentHouse::getContext()->getDeviceCategoryClass(),
			'select_device_category'  => RentHouse::getContext()->getDeviceCategory(),
			'select_other_conditions' => RentHouse::getContext()->getOtherConditionsy(),
      'select_public_utilities' => RentHouse::getContext()->getPublicUtilities(),
            'main_house_class'      => $main_house_class,
			'min_price'               => 0,
			'max_price'               => 100000,
			'min_ping'                => 0,
			'max_ping'                => 100,
			'price_step'              => $default_price_step,
            'ping_step'               => $default_ping_step,
            'search'                  => $search,
            // 'search'                  => $search,
            'arr_house' => $arr_house,
		]);

		parent::initProcess();
	}

	public function setMedia()
	{
		parent::setMedia();

		$this->addCSS(THEME_URL . '/css/search_house.css');
    $this->addCSS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
    $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput.js');
    $this->addCSS('/css/font-awesome.css');
    $this->addCSS(THEME_URL . '/css/rent_house_modal.css');//modal css
    $this->addCSS(THEME_URL . '/css/list042721.css');
    $this->addCSS('/css/metro-all.css');
    $this->addJS('/js/metro.js');
    $this->addCSS(THEME_URL . '/css/footer_0428.css');
    $this->addJS(THEME_URL . '/js/search_house_index.js');


//        $this->addJS('/' . MEDIA_URL . '/typeahead.js');
//        $this->addJS('/' . MEDIA_URL . '/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
	}

}
