<?php
namespace web_rent;
use \IifeHouseController;
use \RentHouse;
use \ListUse;
use \Db;
class LandController extends IifeHouseController
{
	public $page = 'land';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
//		$this->meta_title                = $this->l('首頁');
		$this->WEB_NAME                  = "生活房屋租事順利|代租‧代管‧包租‧租管服務‧房屋信用認證‧安心租履約保證‧安心租保險‧房屋投資收益憑證‧生活房屋加盟體系|租屋管理領導品牌|租屋就是快又好|";
		$this->page_header_toolbar_title = $this->l('生活房屋-首頁');
		$this->className                 = 'LandController';
		$this->no_FormTable              = true;
//        $_GET["county"] = "桃園市";
//        $_GET["city_s"] = "桃園區";

//		$this->display_main_menu = false;
//		$this->display_header = false;
		$this->display_footer = true;
		parent::__construct();
	}


	public function initProcess()
	{
	    //這三個分別對應三種不同的標頭精選,住宅,商用

        $commodity_order_by = " ORDER by r_h.`featured` DESC,r_h.`id_rent_house` DESC";//精選優先 之後則是其他條件

        //精選的物件
        $commodity_sreach = " AND r_h.`active`=1 AND r_h.`featured`=1 AND False";
//        $commodity_sreach = " AND r_h.`rent_house_code` IN('HR-4A33','HR-2F32','AR-OA4','HR-3B292') ";
        $commodity_data = ListUse::commodity_data(null,1,999999,$commodity_sreach,$commodity_order_by);
				$commodity_sreach = " AND r_h.`active`=1 AND r_h.`featured`=1 AND true";
				$commodity_data_0312 = ListUse::commodity_data(null,1,999999,$commodity_sreach,$commodity_order_by);

				$commodity_sreach = " AND r_h.`active`=1 AND r_h.`room`>=2 AND r_h.`room`<=3 AND False";
				$commodity_data_a = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$commodity_sreach = " AND r_h.`active`=1 AND r_h.`id_rent_house_type`LIKE '%\"3\"%' AND False";
				$commodity_data_b = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$commodity_sreach = " AND r_h.`active`=1 AND r_h.`room`>=4 AND r_h.`room`<=6 AND False";
				$commodity_data_c = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$commodity_sreach = " AND r_h.`active`=1 AND r_h.`id_rent_house_types`=2 AND False";
				$commodity_data_d = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$commodity_sreach = " AND r_h.`active`=1 AND (r_h.`id_rent_house_type` LIKE '%\"17\"%' OR  r_h.`id_rent_house_type` LIKE '%\"15\"%') AND False";
				$commodity_data_e = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$commodity_sreach = " AND r_h.`active`=1 AND (r_h.`id_rent_house_type` LIKE '%\"18\"%' OR  r_h.`id_rent_house_type` LIKE '%\"19\"%') AND False";
				$commodity_data_f = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$commodity_sreach = " AND r_h.`active`=1 AND r_h.`id_rent_house_type`LIKE '%\"16\"%' AND False";
				$commodity_data_g = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$d=0;$commodity_sreach = " AND r_h.`active`=1 AND r_h.`featured`=1 AND true";
				$commodity_data_0414[$d] = ListUse::commodity_data(null,1,25,$commodity_sreach,$commodity_order_by);

				$d=$d+1;$commodity_sreach = " AND r_h.`active`=1 AND r_h.`id_other_conditions` NOT LIKE '%\"35\"%' AND r_h.`id_other_conditions` NOT LIKE '%\"36\"%'";
				$commodity_data_0414[$d] = ListUse::commodity_data(null,1,25,$commodity_sreach," ORDER by RAND()");

				$d=$d+1;$commodity_sreach = " AND r_h.`active`=1 AND r_h.`id_other_conditions` NOT LIKE '%\"5\"%'";
				$commodity_data_0414[$d] = ListUse::commodity_data(null,1,25,$commodity_sreach," ORDER by RAND()");

				$d=$d+1;$commodity_sreach = " AND r_h.`active`=1 AND r_h.`id_other_conditions` NOT LIKE '%\"13\"%'";
				$commodity_data_0414[$d] = ListUse::commodity_data(null,1,25,$commodity_sreach," ORDER by RAND()");

				$d=$d+1;$commodity_sreach = " AND r_h.`active`=1 AND (r_h.`id_rent_house_type` LIKE '%\"17\"%' OR  r_h.`id_rent_house_type` LIKE '%\"15\"%')";
				$commodity_data_0414[$d] = ListUse::commodity_data(null,1,25,$commodity_sreach," ORDER by RAND()");

				$d=$d+1;$commodity_sreach = " AND r_h.`active`=1 AND r_h.`id_rent_house_type`LIKE '%\"16\"%'";
				$commodity_data_0414[$d] = ListUse::commodity_data(null,1,25,$commodity_sreach," ORDER by RAND()");

				$d=$d+1;$commodity_sreach = " AND r_h.`active`=1 AND (r_h.`id_rent_house_type` LIKE '%\"18\"%' OR  r_h.`id_rent_house_type` LIKE '%\"19\"%')";
				$commodity_data_0414[$d] = ListUse::commodity_data(null,1,25,$commodity_sreach," ORDER by RAND()");

        $show_array_data = ["1","2"];//住宅 商用

        $commodity_data2 =[];
        $commodity_data3 =[];

        foreach($show_array_data as $value){
//            $i = $value+1;
            //型態
            $sql = "SELECT * FROM rent_house_type WHERE id_main_house_class=".GetSQL($value,"int")."
             AND `show`=1 AND active=1 ORDER BY position ASC";
            $arr_rent_house_type = Db::rowSQL($sql);

            //類別
            $sql = 'SELECT * FROM rent_house_types WHERE id_main_house_class LIKE "%'.$value.'%"
             AND `show`=1 AND active=1 ORDER BY position ASC';
            $arr_rent_house_types = Db::rowSQL($sql);
            //sql link 的搜索

            $sql_rent_house_type = '';
            $sql_rent_house_types ='';

//            print_r($arr_rent_house_type);

            foreach($arr_rent_house_type as $k => $v){

                if(empty($sql_rent_house_type)){
                    $sql_rent_house_type .= "r_h.`id_rent_house_type` LIKE ".GetSQL('%"'.$arr_rent_house_type[$k]["id_rent_house_type"].'"%',"text");
                }else{
                    $sql_rent_house_type .= " || r_h.`id_rent_house_type` LIKE ".GetSQL('%"'.$arr_rent_house_type[$k]["id_rent_house_type"].'"%',"text");
                }
            }
//            echo $sql_rent_house_type.'<br/>';
            foreach($arr_rent_house_types as $k => $v){
                $stop_data = Db::antonym_array($arr_rent_house_types[$k]["id_rent_house_types"],true);
                if(empty($sql_rent_house_types)){
                    $sql_rent_house_types .=$stop_data;
                }else{
                    $sql_rent_house_types .=','.$stop_data;
                }
            }

            $data = ListUse::commodity_data(null,1,12," AND r_h.`active`=1 AND ({$sql_rent_house_type}) AND r_h.`id_rent_house_types` IN ($sql_rent_house_types) ",$commodity_order_by);

            if($value==1){
                $commodity_data2 = $data;
            }else if($value==2){
                $commodity_data3 = $data;
            }
        }



//
//
//        //家用的定義為 公寓 華廈 大樓 透天 別墅
//        $commodity_data2 = "";//3輪~的data
//        $commodity2_order_by=' ORDER by r_h.`featured` DESC,r_h.`id_rent_house` DESC ';
//        $commodity2_order_by_arr= [
//            ' AND r_h.`id_rent_house_type`=2 ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"5"%',"text").' OR r_h.`id_rent_house_types`=5) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"6"%',"text").' OR r_h.`id_rent_house_types`=6) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"23"%',"text").' OR r_h.`id_rent_house_types`=23) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"1"%',"text").' OR r_h.`id_rent_house_types`=1) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"2"%',"text").' OR r_h.`id_rent_house_types`=2) '
//        ];
//
//        foreach($commodity2_order_by_arr as $key => $value){
//            $commodity2_{$key} =  ListUse::commodity_data(null,1,3,$value,$commodity2_order_by);
//        }
//
//       for($i=1;$i<=3;$i++){//跑三輪~
//           foreach($commodity2_order_by_arr as $key => $value){
//               if(!empty($commodity2_{$key}[$i])){
//                   $commodity_data2[] =  $commodity2_{$key}[$i];
//               }
//           }
//       }
//
//
//        //商用的定義為 辦公室 廠房 店面 土地 頂讓 昌庫
//        $commodity_data3 = "";//3輪~的data
//        $commodity3_order_by=' ORDER by r_h.`featured` DESC,r_h.`id_rent_house` DESC ';
//        $commodity3_order_by_arr= [
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"12"%',"text").' OR r_h.`id_rent_house_types`=12) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"22"%',"text").' OR r_h.`id_rent_house_types`=22) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"20"%',"text").' OR r_h.`id_rent_house_types`=20) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"16"%',"text").' OR r_h.`id_rent_house_types`=16) ',
//            ' AND (r_h.`id_rent_house_types` LIKE '.GetSQL('%"13"%',"text").' OR r_h.`id_rent_house_types`=13) ',
//            ' AND r_h.`id_rent_house_type`=10 ',
//        ];
//
//        foreach($commodity3_order_by_arr as $key => $value){
//            $commodity3_{$key} =  ListUse::commodity_data(null,1,3,$value,$commodity3_order_by);
//        }
//
//        for($i=1;$i<=3;$i++){//跑三輪~
//            foreach($commodity3_order_by_arr as $key => $value){
//                if(!empty($commodity3_{$key}[$i])){
//                    $commodity_data3[] =  $commodity3_{$key}[$i];
//                }
//            }
//        }
//


        $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
        $main_house_class = Db::rowSQL($sql);
        $rent_house_types = [];
        $rent_house_type = [];

        foreach($main_house_class as $k => $v){
            $sql = "SELECT * FROM rent_house_types WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%'
            Order by position ASC";
            $rent_house_types[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
            $sql = "SELECT * FROM rent_house_type WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%'
            Order by position ASC";
            $rent_house_type[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
        }


		$arr_index_banner = [
			[
				'href' => '#index',
				'img'  => '/img/okrent/banner/1/a.jpg?v=' . WEB_VERSION,
			],
			[
				'href' => '#index',
				'img'  => '/img/okrent/banner/2/b.jpg?v=' . WEB_VERSION,
			],
			[
				'href' => '#index',
				'img'  => '/img/okrent/banner/3/c.jpg?v=' . WEB_VERSION,
			],
			[
				'href' => '#index',
				'img'  => '/img/okrent/banner/4/d.jpg?v=' . WEB_VERSION,
			],
		];

		$json_rent_house_types = json_encode($rent_house_types,JSON_UNESCAPED_UNICODE);
        $json_rent_house_type = json_encode($rent_house_type,JSON_UNESCAPED_UNICODE);

        $add_js=<<<js
    	$(".class_select").click(function () {
    	$("#search_box_big").stop(true,true);
		$("#search_box_big").animate({
			height: ($(".category").css("height").replace("px","")*1)+150,
		  }, 1000, function() {});
				$(".search_list_wrap").addClass("auto");console.log("IndexPhp-191");
        $(".category").show();

        $({deg: 0}).animate({deg: 180}, {
            duration: 200,
            step: function(now) {
                $("i.class_select").css({
                    transform: 'rotate(' + now + 'deg)'
                });
            }
        });


	});

        $(".houses_select").click(function () {
            $("#search_box_big").stop(true,true);
            $("#search_box_big").animate({
                height: ($(".houses").css("height").replace("px","")*1)+150,
              }, 1000, function() {});
            $(".houses").show();
						$(".search_list_wrap").addClass("auto");console.log("IndexPhp-212");


            $({deg: 0}).animate({deg: 180}, {
                duration: 200,
                step: function(now) {
                    $("i.houses_select").css({
                        transform: 'rotate(' + now + 'deg)'
                    });
                }
            });



        });

		$(document).click(function (e) {
		if (!$(e.target).hasClass("class_select") && !$(e.target).hasClass("category")
			&& $(e.target).parents(".category").length === 0) {
            $(".category").hide(500)
            $({deg: 0}).animate({deg: 0}, {
                duration: 200,
                step: function(now) {
                    $("i.class_select").css({
                        transform: 'rotate(' + now + 'deg)'
                    });
                },
                complete: function() {
                    $("i.class_select").css({transform: ''});
                 }
            });

if($("#search_box_big").css("height") != "78px" && $(".city_div").css("display") == "none" &&
    $(".room_display").css("display") == "none" && $(".patterns").css("display") == "none" &&
	$(".houses").css("display") == "none" && $(".category").css("display") == "none" &&
	$(".county_div").css("display") == "none"){
	        $("#search_box_big").animate({
			        height: 60,
		        }, 1000, function() {
	                $("#search_box_big").css("height","");
		        });
            }
		}
	});

	$(".type_select").click(function () {
	    $("#search_box_big").stop(true,true);
	    $("#search_box_big").animate({
			height: ($(".patterns").css("height").replace("px","")*1)+150,
		  }, 1000, function() {});
        $(".patterns").show();
				$(".search_list_wrap").addClass("auto");console.log("IndexPhp-263");
        $({deg: 0}).animate({deg: 180}, {
            duration: 200,
            step: function(now) {
                $("i.type_select").css({
                    transform: 'rotate(' + now + 'deg)'
                });
            }
        });
	});

	$(document).click(function (e) {
		if (!$(e.target).hasClass("type_select") && !$(e.target).hasClass("patterns")
			&& $(e.target).parents(".patterns").length === 0) {
            $(".patterns").hide(500)

            $({deg: 0}).animate({deg: 0}, {
                duration: 200,
                step: function(now) {
                    $("i.type_select").css({
                        transform: 'rotate(' + now + 'deg)'
                    });
                },
                complete: function() {
                    $("i.type_select").css({transform: ''});
                    console.log("houses_select complete");
                 }
            });

		}
	});

	$(document).click(function (e) {
		if (!$(e.target).hasClass("houses_select") && !$(e.target).hasClass("houses")
			&& $(e.target).parents(".houses").length === 0) {
            $(".houses").hide(500)


            $({deg: 0}).animate({deg: 0}, {
                duration: 200,
                step: function(now) {
                    $("i.houses_select").css({
                        transform: 'rotate(' + now + 'deg)'
                    });
                },
                complete: function() {
                    $("i.houses_select").css({transform: ''});
                    console.log("houses_select complete");
                 }
            });


		}
	});

	$("#room_name").click(function () {
		$("#search_box_big").stop(true,true);
		$("#search_box_big").animate({
			height: ($(".room_display").css("height").replace("px","")*1)+150,
		  }, 1000, function() {});

		$(".room_display").show();
	});


		$(document).click(function (e) {
		if (!$(e.target).hasClass("room_name_head") && !$(e.target).hasClass("room_display")
			&& $(e.target).parents(".room_display").length === 0) {
			$(".room_display").hide(500);
		}
	});

	//住家的管理
		$('input[name="id_main_house_class[]"]').click(function(){
	    var id_main_house_class_text='';
	    var level_data = [];

        for (var i=0;i<document.getElementsByName('id_main_house_class[]').length;i++){
            if(document.getElementsByName('id_main_house_class[]')[i].checked){
                var id_object_val = document.getElementsByName('id_main_house_class[]')[i].value;
                level_data[i] = document.getElementsByName('id_main_house_class[]')[i].value;//取的check到的i
                if(id_main_house_class_text==''){
                    id_main_house_class_text = document.getElementById('main_house_class_name_'+id_object_val).innerHTML;
                }else{
                    id_main_house_class_text = id_main_house_class_text+','+document.getElementById('main_house_class_name_'+id_object_val).innerHTML;
                }
            }
         }

        id_main_house_class_work(level_data);

        id_main_house_class_text = id_main_house_class_text.replace(/<.*?>/g, "");

        $("#main_house_class_all_title").text(id_main_house_class_text);

        if(id_main_house_class_text==''){//全部都消失
            all_id_main_house_class = {1:'1',2:'2',3:'3'};
            id_main_house_class_work(all_id_main_house_class);
             $("#main_house_class_all_title").text("全部");
        }

        $("#types_all_name").html("全部");
        $("#type_all_name").html("全部");
});
		$('#id_main_house_class_all').click(function(){
		     all_id_main_house_class = {1:'1',2:'2',3:'3'};
            id_main_house_class_work(all_id_main_house_class);
            $("#main_house_class_all_title").text('全部');
            $("#types_all_name").text('全部');
            $("#type_all_name").text('全部');
});

	function reset_child(o){
	    var data_id = o.getAttribute("id");
	    var id = $("#"+data_id).data("id");
	    $("#"+id).html("全部");
	}

	function get_data(o){
	    var data_hmtl = '';
	    var name = o.getAttribute("name");
	    var id_head = "";
	    var html_id = "";
	    if(name=="id_types[]"){
	        id_head = "types_name_";
	        html_id = "types_all_name";
	    }else if(name=="id_type[]"){
	        id_head = "type_name_";
	        html_id = "type_all_name";
	    }

	    $.each($("input[name='"+name+"']"),function(n,value){
	        if(document.getElementsByName(name)[n].checked){
	           if(data_hmtl==""){
	               data_hmtl = $("#"+id_head+$(value).val()).html();
	           }else{
	               data_hmtl = data_hmtl+','+$("#"+id_head+$(value).val()).html();
	           }
	        }
        });
	    $("#"+html_id).html(data_hmtl);
	}

	function id_main_house_class_work(level_data){//處理點選住家那區塊後的動作
        var level_id_types = {$json_rent_house_types};
        var level_id_type = {$json_rent_house_type};

        new_id_types_html='<input type="radio" id="id_types_all" value="" onclick="reset_child(this);" data-id="types_all_name"><label for="id_types_all" id="id_types" >全部</label>';//寫入用 id_types
        new_id_type_html ='<input type="radio" id="type_all" value="" onclick="reset_child(this);" data-id="type_all_name"><label for="id_type_all" id="id_type" >全部</label>';//寫入用 id_type
        copy_id_types_html = new_id_types_html;
        copy_id_type_html = new_id_type_html;
        //下面兩條用來紀錄是否有重複資料 例如:型態在用途上有重疊
        new_id_types_data_str = '';
        new_id_type_data_str ='';

        for(var level_data_k in level_data){
            //型態
               $.each(level_id_types,function(n,value){
                   if(level_data[level_data_k]!=n) return;
                $.each(value,function(n,value_value){
                     if((new_id_types_data_str.indexOf(value_value["id_rent_house_types"]+','))==-1){
                         new_id_types_html +='<input type="checkbox" name="id_types[]" id="id_types_'+value_value["id_rent_house_types"]+'" value="'+value_value["id_rent_house_types"]+'"  onclick="get_data(this);"><label for="id_types_'+value_value["id_rent_house_types"]+'" id="types_name_'+value_value["id_rent_house_types"]+'">'+value_value["title"]+'</label>';
                     }
                     new_id_types_data_str += value_value["id_rent_house_types"]+',';
                });
            });

            $.each(level_id_type,function(n,value){
                   if(level_data[level_data_k]!=n) return;
                $.each(value,function(n,value_value){
                     console.log(value_value["title"]);
                     if((new_id_type_data_str.indexOf(value_value["id_rent_house_type"]+','))==-1){
                         new_id_type_html +='<input type="checkbox" name="id_type[]" id="id_type_'+value_value["id_rent_house_type"]+'" value="'+value_value["id_rent_house_type"]+'" onclick="get_data(this);"><label for="id_type_'+value_value["id_rent_house_type"]+'" id="type_name_'+value_value["id_rent_house_type"]+'">'+value_value["title"]+'</label>';
                     }
                     new_id_type_data_str += value_value["id_rent_house_type"]+',';
                });
            });
        }

        //這邊判斷是否無資料如果沒有新的則隱藏
        if(copy_id_types_html == new_id_types_html){
            $(".category").empty();
             $("#types_all_name").parent().hide();
        }else{
            $("#types_all_name").parent().show();
            $(".category").html(new_id_types_html);
        }


        if(copy_id_type_html == new_id_type_html){
            $(".houses").empty();
            $("#type_all_name").parent().hide();
        }else{
            $("#type_all_name").parent().show();
            $(".houses").html(new_id_type_html);
        }

        // $(".category").html(new_id_types_html);
        // $(".houses").html(new_id_type_html);
	}




//	$('input[name="id_types[]"]').click(function(){
//	    console.log("work2");
//	    var id_object_text='';
//        for (var i=0;i<document.getElementsByName('id_types[]').length;i++){
//            if(document.getElementsByName('id_types[]')[i].checked){
//                var id_object_val = document.getElementsByName('id_types[]')[i].value;
//                if(id_object_text==''){
//                    id_object_text = document.getElementById('types_name_'+id_object_val).innerHTML;
//                }else{
//                    id_object_text = id_object_text+','+document.getElementById('types_name_'+id_object_val).innerHTML;
//                }
//            }
//         }
//
/*        id_object_text = id_object_text.replace(/<.*?>/g, ""); */
//
//        $("#types_all_name").text(id_object_text);
//
//        if(id_object_text==''){//全部都消失
//             $("#types_all_name").text("全部");
//        }
//});
//		$('#id_types').click(function(){
//            $("#types_all_name").text('全部');
//});
//
//    $('input[name="id_type[]"]').click(function(){
//	    var id_type='';
//        for (var i=0;i<document.getElementsByName('id_type[]').length;i++){
//            if(document.getElementsByName('id_type[]')[i].checked){
//                var id_type_val = document.getElementsByName('id_type[]')[i].value;
//                if(id_type==''){
//                    id_type = document.getElementById('type_name_'+id_type_val).innerHTML;
//                }else{
//                    id_type = id_type+','+document.getElementById('type_name_'+id_type_val).innerHTML;
//                }
//            }
//         }
//
//        if($("#id_type_3").prop('checked')){
//            min_room_v=0;
//            max_room_v=1;
//	        room_range(min_room_v, max_room_v);
//	        span_txt(min_room_v, max_room_v);
//        }else if($("#id_type_2").prop('checked')){
//            min_room_v=0;
//            max_room_v=0;
//             room_range(min_room_v, max_room_v);
//	         span_txt(min_room_v, max_room_v);
//	   }
//
/*        id_type = id_type.replace(/<.*?>/g, ""); */
//
//        $("#type_all_name").text(id_type);
//         if(id_type==''){//全部都消失
//             $("#type_all_name").text("全部");
//        }
//});
//	    $('#type_name').click(function(){
//	        $("#type_all_name").text('全部');
//});

js;
//輪播
        $js =<<<hmtl
        <script>
        $(".commodity_data").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 4,
        slidesToScroll: 4,

    });
        $(".commodity_data2").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 4,
        slidesToScroll: 4,

    });
        $(".commodity_data3").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 4,
        slidesToScroll: 4,
    });
</script>
hmtl;


		$default_price_step = 1000;
		$default_ping_step = 0;
		$this->context->smarty->assign([
			'select_county'          => RentHouse::getContext()->getCounty('none'),
			'arr_city'               => RentHouse::getContext()->getCity('none'),
			//			'select_use'    => RentHouse::getContext()->getUse('none'),	//20200213 用途拿掉
			'select_type'            => RentHouse::getContext()->getType(),
			'select_types'           => RentHouse::getContext()->getTypes(),
			'select_device_category' => RentHouse::getContext()->getDeviceCategory(),
			'select_other_conditions' => RentHouse::getContext()->getOtherConditionsy(),
			'min_price'  => 0,
			'max_price'  => 100000000,
			'min_ping'   => 0,
			'max_ping'   => 10000,
			'price_step' => $default_price_step,
			'ping_step'  => $default_ping_step,
            'add_js'     => $add_js,
		]);
		$index_ratio      = '1903:553';
		//$index_ratio      = '1423:710';		//*$index_ratio      = '1423:415';
		$this->context->smarty->assign([
			'commodity_data'        => $commodity_data,
			'commodity_data_0312'   => $commodity_data_0312,
			'commodity_data_0414' => $commodity_data_0414,
			'commodity_data_a'        => $commodity_data_a, //溫馨居家〜2-3房
			'commodity_data_b'        => $commodity_data_b, //單身美學〜套房
			'commodity_data_c'        => $commodity_data_c, //大器美宅〜4+房
			'commodity_data_d'        => $commodity_data_d, //獨享天地〜透天/別墅
			'commodity_data_e'        => $commodity_data_e, //店面
			'commodity_data_e_1'        => $commodity_data_e_1, //店辦
			'commodity_data_f'        => $commodity_data_f, //廠房倉庫
			'commodity_data_g'        => $commodity_data_g, //廠房倉庫
			'commodity_data2'       => $commodity_data2,
			'commodity_data3'       => $commodity_data3,
			'js'               => $js,
			'arr_index_banner' => $arr_index_banner,
			'index_ratio'      => $index_ratio,
            'group_link' => '0',
            'main_house_class'      => $main_house_class,
            'rent_house_types'  => $rent_house_types,
            'rent_house_type'   => $rent_house_type,
		]);
		parent::initProcess();
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addCSS(THEME_URL . '/css/index.css');
		$this->addCSS(THEME_URL . '/css/search_house_index.css');
		$this->addJS(THEME_URL . '/js/search_house_index.js');
        $this->addJS('/js/slick.min.js');
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');

				$this->addCSS(THEME_URL . '/css/house6_index.css');
				$this->addCSS(THEME_URL . '/css/index040121.css');
				$this->addJS(THEME_URL . '/js/index040121.js');
	}
}
