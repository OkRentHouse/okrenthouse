<?php

namespace web_rent;
use \IifeHouseController;
use \Tools;
use \SearchHouse;
use \RentHouse;
use \Db;
use \db_mysql;
use \File;
use \ListUse;
use \QRcode;

class RentHouseController extends IifeHouseController
{

    //File::get($row_id['id_file']);
    public $page = 'RentHouse';

    public $tpl_folder;    //樣版資料夾

    public $definition;

    public $_errors_name;

    public function __construct()
    {
        $this->table = 'rent_house';//主資料表
        $this->no_FormTable              = true;
        $this->google_key = 'AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c';//api key
        $this->google_key = 'AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs';//api key
        $id_rent_house = Tools::getValue("id_rent_house");
        $type                            = Tools::getValue('type');

        $sql = "SELECT r_h.`case_name`,r_h.`title`,
            (CASE r_h.`part_address` WHEN '' THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
            IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,'鄰'),''),
            r_h.`road`,r_h.`segment`,
             IF(r_h.`lane`,CONCAT(r_h.`lane`,'巷'),'')  )
             ELSE r_h.`part_address`
             END) rent_address FROM rent_house as r_h
             LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                         LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
             WHERE r_h.`active`=1 AND r_h.`id_rent_house`=".GetSQL(Tools::getValue("id_rent_house"),"int");
        $row = Db::rowSQL($sql,true);

        //圖片處理start
        $sql = "SELECT * FROM rent_house_file WHERE file_type=0 AND id_rent_house =".GetSQL(Tools::getValue("id_rent_house"),"int")." ORDER BY id_rhf ASC ";//物件輪播圖
        $house_file = Db::rowSQL($sql);
        $img_seo = array();//放入圖片的url
        foreach($house_file as $k => $v){
            $img = File::get($house_file[$k]['id_file']);
            $img_seo[] = $img["url"];//塞進去圖片
        }
        //圖片處理end

        $sql = "SELECT * FROM seo WHERE index_ation=1 AND id_seo=3";
        $seo = Db::rowSQL($sql,true);
        $this->meta_title                = $this->l('生活樂租 吉屋快搜 '.$row["case_name"].' '.$row["title"].' '.$row["rent_address"]);
        $this->meta_description          = $this->l('生活樂租 吉屋快搜 '.$row["case_name"].' '.$row["title"].' '.$row["rent_address"].'('.$seo["description"].')');
        $this->meta_keywords             = $row["case_name"];
        $this->meta_img = $img_seo;

        $this->page_header_toolbar_title = $this->l('吉屋快搜');
        $this->className                 = 'RentHouseController';


        $sql = "SELECT count(*) as rent_count FROM rent_house WHERE active=1 AND id_rent_house=".GetSQL(Tools::getValue("id_rent_house"),"int");
        $test_is_good = Db::rowSQL($sql,true);

        if($test_is_good["rent_count"]=='0' && empty(Tools::getValue('action'))){//防止下架被讀取
            header('Location: https://www.okrent.house/list');
        }

        $this->breadcrumbs               = [
            [
                'href'  => '/',
                'hint'  => $this->l(''),
                'icon'  => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href'  => '/list' . $type,
                'hint'  => $this->l(''),
                'icon'  => '',
                'title' => $this->l('吉屋快搜'),
            ],
            [
                'href'  => '/list',
                'hint'  => $this->l(''),
                'icon'  => '',
                'title' => $this->l('條件搜尋'),
            ],
        ];

//        if(!empty($id_rent_house)){//這邊在於瀏覽人數+1
//            $sql = "SELECT `views` FROM `{$this->table}` WHERE `id_rent_house`=".GetSQL($id_rent_house,"int");
//            $row = Db::rowSQL($sql, true);
//            $views = $row["views"]+1;
//            $SQL = "UPDATE `{$this->table}` set `views`=".GetSQL($views,"int")." WHERE `id_rent_house`=".GetSQL($id_rent_house,"int");
//            Db::rowSQL($sql);
//        }

        if(!empty($id_rent_house)){
            //瀏覽人數+1記錄 主要用於昨日觀看 start
            $sql = "INSERT INTO `rent_house_view`(`id_rent_house`, `table`, `platform`)
                VALUES(".GetSQL($id_rent_house,"int").",".GetSQL($this->table,"text").",0)";
            Db::rowSQL($sql);
            //瀏覽人數+1記錄 end

        }

        //麵包屑 status
        $sql = "SELECT cy.`county_name` as county,ci.`city_name` as city_s,r_h_ts.`title` as title,r_h.`id_rent_house_type` as id_type,r_h.`id_rent_house_types` as id_types
                FROM ".$this->table." AS r_h
                LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                LEFT JOIN `rent_house_types` AS r_h_ts ON r_h_ts.`id_rent_house_types` = r_h.`id_rent_house_types`
                WHERE r_h.`id_rent_house`=".$id_rent_house;

        $row = Db::rowSQL($sql,true);

        $get_data = "/list?";//搜索東西

//        print_r($row);

        foreach($row as $k => $v){
            if(empty($v)) continue;
            if($k!='title' && $k !='id_type' && $k !='id_types'){
                $get_data .="&".$k."=".$v;
                $this->breadcrumbs[] = [
                    'href'  => $get_data,
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l($v),
                ];
            }else if($k =="title"){
                $get_data .="&id_types_s=".$row["id_types"];
                $this->breadcrumbs[] = [
                    'href'  => $get_data,
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l($v),
                ];
            }else if($k =="id_type"){
                $sql = "SELECT title,id_rent_house_type FROM rent_house_type WHERE id_rent_house_type IN(".Db::antonym_array($v,true).")";
                $row_type = Db::rowSQL($sql);
                $type_title = '';
                $type_value = '';

                foreach($row_type as $t_k => $t_v){
                    $type_title = empty($type_title)?$t_v["title"]:$type_title.','.$t_v["title"];
                    $type_value = empty($type_value)?$t_v["id_rent_house_type"]:$type_value.','.$t_v["id_rent_house_type"];
                }
                $get_data .="&id_type_s=".$type_value;
                $this->breadcrumbs[] = [
                    'href'  => $get_data,
                    'hint'  => $this->l(''),
                    'icon'  => '',
                    'title' => $this->l($type_title),
                ];

            }
        }
    //麵包屑 end

        parent::__construct();
    }

    public function initProcess()
    {
        $id_rent_house = Tools::getValue("id_rent_house");
        ListUse::setViews($id_rent_house);
        $js=<<<js

js;
        $sql = "SELECT r_h.`id_rent_house`,r_h.`rent_house_code`,r_h.`title` as rent_house_title,r_h.`dompletion_date` as dompletion_date,r_h.`price` as rent_house_price,r_h.`kitchen` as kitchen,
                r_h.`id_main_house_class` as id_main_house_class,m_h_c.`title` as m_h_c_title,r_h.`dompletion_date_y` as dompletion_date_y,r_h.`dompletion_date_m` as dompletion_date_m,r_h.`dompletion_date_d` as dompletion_date_d,
                r_h.`case_name`,r_h.`cleaning_fee`,m.`name` as memeber_name,w.`id_web` as id_web,r_h.`lighting_num` as lighting_num,r_h.`id_member` as id_member,
                m.`user` as member_phone,m.`tel` as member_tel,r_h.`id_other_conditions` as id_other_conditions, r_h.`households` as households,r_h.`sideroom_num` as sideroom_num,r_h.`is_sideroom` as is_sideroom,
                r_h.`id_rent_house_types` as id_rent_house_types,r_h.`id_rent_house_type` as id_rent_house_type,
                r_h.`id_device_category` as id_device_category,r_h.`features` as features,
                r_h.`indoor_ping` as indoor_ping,r_h.`ping` as ping,r_h.`update_date` as update_date,
                IF(r_h.`rent_price_m` ,r_h.`rent_price_m`,r_h.`reserve_price_m`) as rent_cash,
                r_h.`m_s` as m_s,r_h.`m_e` as m_e,
                r_h.`id_rent_house_community` as id_rent_house_community,r_h.`rent_house_community_code` as rent_house_community_code, r_h.`id_public_utilities` as id_public_utilities, r_h.`all_num`as all_num, r_h.`household_num` as household_num,
                r_h.`storefront_num` as storefront_num, r_h.`builders` as builders,r_h.`e_school` as e_school,r_h.`j_school` as j_school,r_h.`park` as park,r_h.`clean_time` as clean_time,
                r_h.`market` as market,r_h.`night_market` as night_market,r_h.`supermarket` as supermarket,r_h.`shopping_center` as shopping_center,r_h.`hospital` as hospital,
                r_h.`bus` as bus,r_h.`bus_station` as bus_station,r_h.`passenger_transport` as passenger_transport,r_h.`passenger_transport_station` as passenger_transport_station,
                r_h.`train` as train,r_h.`mrt` as mrt,r_h.`mrt_station` as mrt_station,r_h.`high_speed_rail` as high_speed_rail,r_h.`interchange` as interchange,
                r_h.`housing_time_s` as housing_time_s,r_h.`housing_time_e` as housing_time_e,
                r_h.`age` as age, r_h.`year_old` as year_old,
                cy.`county_name` as county,ci.`city_name` as city_s,r_h.`id_county` as id_county,r_h.`id_city` as id_city,
                r_h.`park_fee_type` as park_fee_type,r_h.`car_space` as car_space,
                r_h.`house_door_seat` as  house_door_seat,r_h.`limit_industry` as limit_industry,r_h.`contract_time_e` as contract_time_e,
                IFNULL((CASE a.`phone` WHEN '' THEN r_h.`ag_phone`
                                    WHEN null THEN r_h.`ag_phone`
                                    ELSE a.`phone`
                                    END ),'0939-688089')as ag_phone,
                    IFNULL((CASE a.`first_name` WHEN ''   THEN r_h.`develop_ag`
                                         WHEN null THEN r_h.`develop_ag`
                                         ELSE CONCAT(a.`last_name`,a.`first_name`)
                                        END ),'房似錦')  as develop_ag,
        (CASE r_h.`cleaning_fee_cycle` WHEN '1' THEN '月' WHEN '2' THEN '季' WHEN '3' THEN '半年' WHEN '4' THEN '年' WHEN '' THEN '' END) as cleaning_fee_cycle,
        (CASE r_h.`cleaning_fee_unit` WHEN  '1' THEN '坪' WHEN  '2' THEN '戶' ELSE '' END)  as cleaning_fee_unit,r_h.`deposit` as deposit,
        (CASE r_h.`genre` WHEN '0' THEN '頂加' WHEN '1' THEN '樓中樓' WHEN '2' THEN '挑高' WHEN '3' THEN '夾層' WHEN '4' THEN '木板' WHEN '5' THEN '水泥'  WHEN '' THEN '無' END)  as genre,
        (CASE m.`gender` WHEN '0' THEN '小姐' WHEN '1' THEN '先生' END) as member_gender,
        (CASE r_h.`part_address` WHEN '' THEN CONCAT(cy.`county_name`,ci.`city_name`,r_h.`village`,
            IF(r_h.`neighbor`,CONCAT(r_h.`neighbor`,'鄰'),''),
            r_h.`road`,r_h.`segment`,
             IF(r_h.`lane`,CONCAT(r_h.`lane`,'巷'),'')  )
             ELSE r_h.`part_address`
             END) rent_address,
             r_h.`cleaning_fee_type` as s1,
             r_h.`room` as t3, r_h.`hall` as t4, r_h.`bathroom` as t5, r_h.`muro` as t6,  r_h.`balcony_front` t7,
             r_h.`balcony_back` t8, r_h.`room_balcony` t9,
             r_h.`ping` as ping,
             IF(((r_h.`rental_floor_s` = r_h.`all_floor`) &&
             (r_h.`rental_floor_e` = r_h.`underground` ||
                ((r_h.`rental_floor_e`='' || r_h.`rental_floor_e`='0' || r_h.`rental_floor_e`='1')
                && (r_h.`underground`='' || r_h.`underground`='0'))
             )),1,0) whole_building,
            (CASE r_h.`rental_floor_e`  WHEN r_h.`rental_floor_s` THEN r_h.`rental_floor_s`
                                    WHEN ''                   THEN r_h.`rental_floor_s`
                                    ELSE CONCAT(r_h.`rental_floor_s`,'~',r_h.`rental_floor_e`) END) rental_floor,
            r_h.`all_floor` as floor, o_c.`title`, w.`web` AS w_title
                         FROM rent_house AS r_h
                         LEFT JOIN `web` AS w ON w.`id_web` = r_h.`id_web`
                         LEFT JOIN `member` AS m ON m.`id_member` = r_h.`id_member`
                         LEFT JOIN `county` AS cy ON cy.`id_county` = r_h.`id_county`
                         LEFT JOIN `city` AS ci ON ci.`id_city` = r_h.`id_city`
                         LEFT JOIN `device_category` AS d_c ON d_c.`id_device_category` = r_h.`id_device_category`
                         LEFT JOIN `device_category_class` AS d_c_c ON d_c.`id_device_category_class` = d_c_c.`id_device_category_class`
                         LEFT JOIN `disgust_facility` AS d_f ON d_f.`id_disgust_facility` = r_h.`id_disgust_facility`
                         LEFT JOIN `disgust_facility_class` AS d_f_c ON d_f_c.`id_disgust_facility_class` = d_f.`id_disgust_facility_class`
                         LEFT JOIN `other_conditions` AS o_c ON o_c.`id_other_conditions`  = r_h.`id_other_conditions`
                         LEFT JOIN admin as a ON a.ag = r_h.`ag`
                         LEFT JOIN main_house_class as m_h_c ON m_h_c.`id_main_house_class` = r_h.`id_main_house_class`
                         WHERE r_h.`active`=1 AND r_h.`id_rent_house` =".$id_rent_house;

//        echo $sql;
        // print_r($sql);
        $arr_house = Db::rowSQL($sql, true);

        $arr_house["rent_cash"] = number_format($arr_house["rent_cash"]);

        //由於有些跟社區有連結 因此這邊會先判斷是否有選取社區 有得話走下面這項並且覆蓋掉相關參數(我懶的一個一個改了 感覺也會卡SQL速度)
        if(!empty($arr_house["id_rent_house_community"])){
            $sql = "SELECT * FROM rent_house_community WHERE id_rent_house_community ='{$arr_house["id_rent_house_community"]}'";
            $rent_house_community_data = Db::rowSQL($sql, true);//取得資料
            foreach($rent_house_community_data as $k => $v){//將有的全部塞回主陣列
                $arr_house[$k] = $rent_house_community_data[$k];
            }
        }

        //圖片處理start
        $sql = "SELECT * FROM rent_house_file WHERE file_type=1 AND id_rent_house =".GetSQL($id_rent_house,"int")." ORDER BY id_rhf ASC ";//物件輪播圖
        $house_file = Db::rowSQL($sql);
        $img_arr = array();//放入圖片的url
        foreach($house_file as $k => $v){
            $img = File::get($house_file[$k]['id_file']);
            $img_arr[] = $img["url"];//塞進去圖片
        }
        //圖片處理end

//        物件類別
        $sql = "SELECT * FROM rent_house_types WHERE id_rent_house_types IN (".Db::antonym_array($arr_house['id_rent_house_types']).")";
        $sql_types = Db::rowSQL($sql, false);
        $arr_house['sql_types'] = $sql_types;//整批搜索到的丟進去

//        物件型態
        $sql = "SELECT * FROM rent_house_type WHERE id_rent_house_type IN (".Db::antonym_array($arr_house['id_rent_house_type']).")";
        $sql_type = Db::rowSQL($sql, false);
        $arr_house['sql_type'] = $sql_type;//整批搜索到的丟進去

            //產權登記的start
        $sql = "SELECT count(*) as house_power FROM rent_house_file WHERE file_type=4 OR file_type=5 AND id_rent_house=".GetSQL($id_rent_house,"int");
        $house_power = Db::rowSQL($sql,true);
        if(!empty($house_power['house_power'])){
            $arr_house["house_power"] = '1';
        }
        //產權登記的end

        //屋齡 start
        if(!empty($arr_house["dompletion_date"])){
            $dompletion_date = $arr_house["dompletion_date"];
            $date = date("Y-m-d");
            $house_old = round((strtotime($date)-strtotime($dompletion_date))/(60*60*24*364),1);
            $arr_house["house_old"] = $house_old;
        }else if(!empty($arr_house["dompletion_date_y"])){//後面修改的關係
            $dompletion_date = (int)$arr_house["dompletion_date_y"]+1911;
            $dompletion_date = $dompletion_date.'-'.$arr_house["dompletion_date_m"].'-'.$arr_house["dompletion_date_d"];
            $date = date("Y-m-d");
            $house_old = round((strtotime($date)-strtotime($dompletion_date))/(60*60*24*364),1);
            $arr_house["house_old"] = $house_old;
        }else{
            $arr_house["house_old"] = "";
        }
        //屋齡 end
        //更新時間start

        if(!empty($arr_house["update_date"])){
            $update_date = $arr_house["update_date"];
            $date = date("Y-m-d H:i:s");
            $date_gap = strtotime($date)-strtotime($update_date);//時間差

            if($date_gap<'60'){
                $update_date = "在1分鐘之內";//1分鐘之內
            }else if($date_gap <(60*60)){//分(1小時以下)
                $update_date = "約".round(($date_gap)/(60))."分鐘";
            }else if($date_gap <(60*60*24)){//小時 (1天以下)
                $update_date = "約".round(($date_gap)/(60*60))."小時";
            }else if($date_gap <(60*60*24*30)){//天(一個月以下)
                $update_date = "約".round(($date_gap)/(60*60*24))."天";
            }else if($date_gap <(60*60*24*364)){//月(1年以下)
                $update_date = "約".round(($date_gap)/(60*60*24*30))."月";
            }else{//年
                $update_date = "約".round(($date_gap)/(60*60*24*364),1)."年";
            }
            $arr_house["update_date"] = $update_date;
        }else{
            $arr_house["update_date"] = "";
        }
        //更新時間end

        //其他條件 start
        //首先先將["X","2",""Z] 轉換成我們一般在IN() 使用的
        $id_other_conditions_arr = Db::antonym_array($arr_house["id_other_conditions"],true);//其他條件

        $sql = "SELECT `id_other_conditions` as id,`title` FROM `other_conditions` WHERE `id_other_conditions` IN(".$id_other_conditions_arr.")";
        $other_conditions_data = Db::rowSQL($sql);

        foreach($other_conditions_data as $k => $v){//這邊要把 $arr_house["other_conditions"] 的主key修改成 id的 讓我後面比較好處理
//            echo $other_conditions_data[$k]["id"]."<br>";
            $arr_house["other_conditions"][$other_conditions_data[$k]["id"]] = $other_conditions_data[$k]["title"];
        }
        //會把原先的 array([0]=>array([id]=>12,[title]=>test)); ==> array([12]=>"test");//的動作會比較好使用
        //其他條件end

        //目前還沒開始寫社區連接
        //公設相關start
        $id_public_utilities_arr = Db::antonym_array($arr_house["id_public_utilities"],true);//去除[ ] "
        $sql = "SELECT p_u.`id_public_utilities` as id,p_u.`title` as title, p_u.`id_public_utilities_class` as id_p_u_c,
                p_u_c.`title` as p_u_c_title
                FROM public_utilities as p_u
                LEFT JOIN  public_utilities_class AS p_u_c ON p_u_c.`id_public_utilities_class` = p_u.`id_public_utilities_class`
                WHERE p_u.`id_public_utilities` IN({$id_public_utilities_arr}) ORDER BY p_u.`position` ASC ";
        $public_utilities_data = Db::rowSQL($sql);

        $public_utilities_arr = array();//暫存用來看狀態
        foreach($public_utilities_data as $k => $v){
            if(!empty($public_utilities_arr[$public_utilities_data[$k]["id_p_u_c"]])){//我要將它做成主類別 array("title"=>"work,xxx,1234");
                $public_utilities_arr[$public_utilities_data[$k]["id_p_u_c"]] .= "‧".$public_utilities_data[$k]["title"];
            }else{//不存在
                $public_utilities_arr[$public_utilities_data[$k]["id_p_u_c"]] = $public_utilities_data[$k]["title"];
            }
        }
        $arr_house["public_utilities"] = $public_utilities_arr;//已經處理完的資料

        //公設相關end

        //設備相關start
        $list_html = '';
        $temp = '';
        $id_device_category_arr = Db::antonym_array($arr_house["id_device_category"]);
        $id_device_category_arr =  str_replace('"','',$id_device_category_arr);
        $id_device_category_arr = explode(',',$id_device_category_arr);

        $sql = "SELECT * FROM device_category_group  ORDER BY `position` ASC";
        $device_category_group = Db::rowSQL($sql); //群組
        $sql = "SELECT * FROM device_category ORDER BY `position` ASC";
        $device_category_data = Db::rowSQL($sql); //2020.10.21 列出所有


        foreach($device_category_group as $key => $value){
            if(empty($arr_house['id_main_house_class'])) $arr_house['id_main_house_class']=1;//防止沒有參數 預設為1(住家)
            if(!strpos($value['id_main_house_class'],$arr_house['id_main_house_class'])) continue;//不存在彈出
            $num =0;//8個要換行
            $total = 0;//目前有幾項

            foreach($device_category_data as $k => $v){//必須先取得有幾項
                if(!strpos($v['id_device_category_group'],$value['id_device_category_group'])) continue;//配對group的
                $total++;
            }

            foreach($device_category_data as $k => $v){
                if(!strpos($v['id_device_category_group'],$value['id_device_category_group'])) continue;//配對group的
                if($num%6==0) $temp .='<tr class="list_wrap">';
                if($num==0) $temp .='<td class="title" rowspan="2">'.$value['title'].'</td>';
                if($num%6==0 && $num!=0) $temp .='<td class="title" style="background: #fff;border: 0px;"></td>';

                $available = in_array($v['id_device_category'],$id_device_category_arr)?'available':'';//直接判斷陣列是否存在
                $temp .='<td class="list"><div class="item '.$available.'">'.$v['title'].'</div></td>';
                $num ++;
                if($total==$num || $num%6==0) $temp .='</tr>';
            }
        }

        $list_html = '<table style="width: 100%;">'.$temp.'</table>';




//        $list_html = '';
//        $temp_left = '';
//        $temp_right = '';
//
//
//        $id_device_category_arr = Db::antonym_array($arr_house["id_device_category"]);
//        $id_device_category_arr =  str_replace('"','',$id_device_category_arr);
//        $id_device_category_arr = explode(',',$id_device_category_arr);
//
//        $sql = "SELECT * FROM device_category_group  ORDER BY `position` ASC";
//        $device_category_group = Db::rowSQL($sql); //群組
//        $sql = "SELECT * FROM device_category ORDER BY `position` ASC";
//        $device_category_data = Db::rowSQL($sql); //2020.10.21 列出所有
//
//
//
//        foreach($device_category_group as $key => $value){
//            if(empty($arr_house['id_main_house_class'])) $arr_house['id_main_house_class']=1;//防止沒有參數 預設為1(住家)
//            if(!strpos($value['id_main_house_class'],$arr_house['id_main_house_class'])) continue;//不存在彈出
//            $num =0;//8個要換行
//            $total = 0;//目前有幾項
//
//            foreach($device_category_data as $k => $v){//必須先取得有幾項
//                if(!strpos($v['id_device_category_group'],$value['id_device_category_group'])) continue;//配對group的
//                $total++;
//            }
//
//            foreach($device_category_data as $k => $v){
//
//                if(!strpos($v['id_device_category_group'],$value['id_device_category_group'])) continue;//配對group的
//                if($num%7==0) $temp_right .='<ul class="list_wrap">';
//                if($num==0) $temp_left .='<div class="title">'.$value['title'].'設備</div>';
//                if($num%7==0 && $num!=0) $temp_left .='<div class="title"></div>';
//                $available = array_search($v['id_device_category'],$id_device_category_arr)?'available':'';//直接判斷陣列是否存在
//                $temp_right .='<li class="list"><div class="item '.$available.'">'.$v['title'].'</div></li>';
//                $num ++;
//                if($total==$num || $num%7==0) $temp_right .='</ul>';
//            }
//        }
//        $list_html = '<div style="display: flex;"><div style="width: 20%;">'.$temp_left.'</div><div style="width: 80%;">'.$temp_right.'</div></div>';

        //設備相關end

//汽機車start
        $park_space = [];//丟進去的
        $id_rent_house = Tools::getValue("id_rent_house");//取得data
        //是否有車位
        $arr_house["cars_have"]='0';
        //車位是否含在租金內
        $arr_house["pfy"]='';
        $arr_house["motorcycle_have"]='0';
//            取得汽車的
        $sql = "SELECT cars_park_position,cars_type,cars_serial,cars_rent,cars_rent_type,cars_remarks FROM rent_house WHERE id_rent_house=".GetSQL($id_rent_house, "int");
        $cars_data = Db::rowSQL($sql,true);
        if(isset($cars_data)){//存在做這道工序
            foreach($cars_data as $key => $value){
                if(strstr($value,'[') && strstr($value,']') && strstr($value,'"')){//找到標的物將它變成陣列形式
                    $cars_data[$key] = Db::antonym_array($cars_data[$key],true);
                    $cars_data[$key] = explode(",",$cars_data[$key]);
                }
            }
        }

//            取得機車的
        $sql = "SELECT motorcycle_park_position,motorcycle_type,motorcycle_serial,motorcycle_rent,motorcycle_rent_type,motorcycle_remarks FROM rent_house WHERE id_rent_house=".GetSQL($id_rent_house, "int");
        $motorcycle_data = Db::rowSQL($sql,true);
        if(isset($cars_data)){//存在做這道工序
            foreach($motorcycle_data as $key => $value){
                if(strstr($value,'[') && strstr($value,']') && strstr($value,'"')){//找到標的物將它變成陣列形式
                    $motorcycle_data[$key] = Db::antonym_array($motorcycle_data[$key],true);
                    $motorcycle_data[$key] = explode(",",$motorcycle_data[$key]);
                }
            }
        }
        if(!empty($cars_data)){
            $arr_house["cars"] = $cars_data;
            //$arr_house["pfy"]=$arr_house["park_fee_type"];
            if($arr_house["park_fee_type"]=='0') $arr_house["pfy"]='(費用另計)';
            foreach($arr_house["cars"]["cars_park_position"] as $key => $value){
                switch($arr_house["cars"]["cars_rent_type"][$key]){
                    case '0': $arr_house["cars"]["cars_rent_type"][$key]='月'; break;
                    case '1': $arr_house["cars"]["cars_rent_type"][$key]='季'; break;
                    case '2': $arr_house["cars"]["cars_rent_type"][$key]='半年'; break;
                    case '3': $arr_house["cars"]["cars_rent_type"][$key]='年'; break;
                    case '4': $arr_house["cars"]["cars_rent_type"][$key]='含管理費'; break;
                }
                switch($arr_house["cars"]["cars_type"][$key]){
                    case '0': $arr_house["cars"]["cars_type"][$key]='坡道平面'; break;
                    case '1': $arr_house["cars"]["cars_type"][$key]='坡道機械'; break;
                    case '2': $arr_house["cars"]["cars_type"][$key]='升降平面'; break;
                    case '3': $arr_house["cars"]["cars_type"][$key]='升降機械'; break;
                    case '4': $arr_house["cars"]["cars_type"][$key]='一樓平面'; break;
                }
                if(!empty($arr_house["cars"]["cars_serial"][$key]) || !empty($arr_house["cars"]["cars_rent"][$key]) || !empty($arr_house["cars"]["cars_remarks"][$key]) || $arr_house["cars"]["cars_rent_type"][$key]=="含管理費") $arr_house["cars_have"]='1';

            }
        }
        if(!empty($motorcycle_data)){
            $arr_house["motorcycle"] = $motorcycle_data;
            foreach($arr_house["motorcycle"]["motorcycle_park_position"] as $key => $value){
                switch($arr_house["motorcycle"]["motorcycle_rent_type"][$key]){
                    case '0': $arr_house["motorcycle"]["motorcycle_rent_type"][$key]='月'; break;
                    case '1': $arr_house["motorcycle"]["motorcycle_rent_type"][$key]='季'; break;
                    case '2': $arr_house["motorcycle"]["motorcycle_rent_type"][$key]='半年'; break;
                    case '3': $arr_house["motorcycle"]["motorcycle_rent_type"][$key]='年'; break;
                    case '4': $arr_house["motorcycle"]["motorcycle_rent_type"][$key]='含管理費'; break;
                }
                switch($arr_house["motorcycle"]["motorcycle_type"][$key]){
                    case '0': $arr_house["motorcycle"]["motorcycle_type"][$key]='坡道平面'; break;
                    case '1': $arr_house["motorcycle"]["motorcycle_type"][$key]='坡道機械'; break;
                    case '2': $arr_house["motorcycle"]["motorcycle_type"][$key]='升降平面'; break;
                    case '3': $arr_house["motorcycle"]["motorcycle_type"][$key]='升降機械'; break;
                    case '4': $arr_house["motorcycle"]["motorcycle_type"][$key]='一樓平面'; break;
                }
                if(!empty($arr_house["motorcycle"]["motorcycle_serial"][$key]) || !empty($arr_house["motorcycle"]["motorcycle_rent"][$key]) || !empty($arr_house["motorcycle"]["motorcycle_remarks"][$key]  || $arr_house["motorcycle"]["motorcycle_rent_type"][$key]=="含管理費")) $arr_house["motorcycle_have"]='1';
            }
        }
        //汽機車end

        //瀏覽人數start
        $date = date("Y-m-d")." 00:00:00";
        $date_before = date("Y-m-d",strtotime("-1 day"))." 00:00:00";
        //PC平台
        $sql = "SELECT count(*) as count FROM `rent_house_view` WHERE `create_time` >='".$date_before."' AND `create_time` <'".$date."' AND platform=0 AND id_rent_house=".GetSQL($id_rent_house,"int");
        $date_count_pc = Db::rowSQL($sql,true);

//        print_r($date_count_pc);

        //phone平台
        $sql = "SELECT count(*) as count FROM `rent_house_view` WHERE `create_time` >='".$date_before."' AND `create_time` <'".$date."' AND platform=1  AND id_rent_house=".GetSQL($id_rent_house,"int");
        $date_count_phone = Db::rowSQL($sql,true);




        $browse = ["pc"=>$date_count_pc["count"],"phone"=>$date_count_phone["count"],"total"=>$date_count_pc["count"]+$date_count_phone["count"]];
        //瀏覽人數end

        //身分相關start

        $sql = "SELECT *,(IF(m.`gender`=1,'先生','小姐')) as member_gender FROM member as m
        LEFT JOIN `broker` as b ON b.`id_member` = m.`id_member`
        WHERE m.`id_member`=".GetSQL($arr_house["id_member"],"int");
        $member_data = Db::rowSQL($sql,true);

//        print_r($member_data);

        if(strstr($member_data["id_member_additional"],"3")){//房仲
            $arr_house["user_html"]=<<<hmtl
            <div class="user_info">
                 <!--<img src="/themes/Rent/img/renthouse/1091007-1.png" alt="" />-->
                 <div class="text">
                     <div class="">
                         <p><span>{$member_data["name"]}</span> {$member_data["member_gender"]} <br>收取服務費</p>

                     </div>
                     <!-- 109.10.07 moved-->
                 </div>
             </div>

             <div class="category_wrap">
                 <div class="category">
                     <p>{$member_data["job"]}</p>
                     <p>{$member_data["occupation"]}</p>
                 </div>
                 <div class="box_wrap">
                     <div class="box">
                     <p>{$member_data["brand"]}</p>
                     <p>{$member_data["company"]}</p></div>
                 </div>
             </div>

            <div class="phone_info">
                 <div class="number"><img src="/themes/Rent/img/renthouse/house_search_phone_01.svg" alt="" />{$member_data["user"]}</div>
                 <hr>
                 <div class="number"><img src="/themes/Rent/img/renthouse/house_search_phone_02.svg" alt="" />{$member_data["company_tel"]}</div>
             </div>
hmtl;
        }else if(strstr($member_data["id_member_additional"],"4")){//房東自上
            $arr_house["user_html"]=<<<hmtl
            <div class="user_info">
                 <!--<img src="/themes/Rent/img/renthouse/1091007-1.png" alt="" />-->
                 <div class="text">
                     <div class="">
                         <p><span>{$member_data["name"]}</span> {$member_data["member_gender"]} </p>
                     </div>
                     房東
                 </div>
             </div>
            <div class="phone_info">
                 <div class="number"><img src="/themes/Rent/img/renthouse/house_search_phone_01.svg" alt="" />{$member_data["user"]}</div>
                 <hr>
                 <div class="number"><img src="/themes/Rent/img/renthouse/house_search_phone_02.svg" alt="" />{$member_data["tel"]}</div>
             </div>
hmtl;
        }else{//我們這邊
            if($arr_house["w_title"]=="桃園總店"){
                $arr_house["w_title"] = "藝文直營店";
            }
            if(empty($arr_house["ag_phone"])){
                $arr_house["ag_phone"] = "0939-688089";
            }
            if(empty($arr_house["develop_ag"])){
                $arr_house["develop_ag"] = "房似錦";
            }

            $arr_house["consultant"] = "1";//與呼叫樂租顧問相關
            $arr_house["user_html"]=<<<hmtl
            <div class="user_info">
                 <!--<img src="/themes/Rent/img/renthouse/1091007-1.png" alt="" />-->
                 <div class="text">
                     <div class="">
                         <p><span><label>顧問</label>{$arr_house["develop_ag"]}</span><span class="e_phone">
                         <svg class="Path_20819" viewBox="3.024 0 10.512 20.952">
                         			<path id="Path_20819" d="M 11.66399955749512 20.95199966430664 L 11.66399955749512 20.95199966430664 L 11.73600006103516 20.95199966430664 L 11.80799961090088 20.95199966430664 L 11.88000011444092 20.95199966430664 L 11.95199966430664 20.95199966430664 L 12.02400016784668 20.95199966430664 L 12.0959997177124 20.8799991607666 L 12.16800022125244 20.8799991607666 L 12.23999977111816 20.8799991607666 L 12.3120002746582 20.8799991607666 L 12.3120002746582 20.8080005645752 L 12.38399982452393 20.8080005645752 L 12.45600032806396 20.8080005645752 L 12.52799987792969 20.73600006103516 L 12.60000038146973 20.73600006103516 L 12.67199993133545 20.73600006103516 L 12.67199993133545 20.66399955749512 L 12.74400043487549 20.66399955749512 L 12.74400043487549 20.59199905395508 L 12.81599998474121 20.59199905395508 L 12.88799953460693 20.59199905395508 L 12.88799953460693 20.52000045776367 L 12.96000003814697 20.52000045776367 L 12.96000003814697 20.44799995422363 L 13.0319995880127 20.44799995422363 L 13.10400009155273 20.37599945068359 L 13.17599964141846 20.30400085449219 L 13.17599964141846 20.23200035095215 L 13.2480001449585 20.23200035095215 L 13.2480001449585 20.15999984741211 L 13.31999969482422 20.08799934387207 L 13.31999969482422 20.01600074768066 L 13.39200019836426 19.94400024414063 L 13.39200019836426 19.87199974060059 L 13.46399974822998 19.79999923706055 L 13.46399974822998 19.72800064086914 L 13.46399974822998 19.6560001373291 L 13.46399974822998 19.58399963378906 L 13.53600025177002 19.51199913024902 L 13.53600025177002 19.44000053405762 L 13.53600025177002 19.36800003051758 L 13.53600025177002 19.29599952697754 L 13.53600025177002 19.22400093078613 L 13.53600025177002 19.15200042724609 L 13.53600025177002 19.07999992370605 L 13.53600025177002 1.871999979019165 L 13.53600025177002 1.799999952316284 L 13.53600025177002 1.728000044822693 L 13.53600025177002 1.656000018119812 L 13.53600025177002 1.51199996471405 L 13.53600025177002 1.440000057220459 L 13.53600025177002 1.368000030517578 L 13.46399974822998 1.296000003814697 L 13.46399974822998 1.223999977111816 L 13.46399974822998 1.151999950408936 L 13.39200019836426 1.080000042915344 L 13.39200019836426 1.008000016212463 L 13.31999969482422 0.9359999895095825 L 13.31999969482422 0.8640000224113464 L 13.31999969482422 0.7919999957084656 L 13.2480001449585 0.7919999957084656 L 13.2480001449585 0.7200000286102295 L 13.17599964141846 0.6480000019073486 L 13.10400009155273 0.5759999752044678 L 13.0319995880127 0.5040000081062317 L 12.96000003814697 0.5040000081062317 L 12.96000003814697 0.4320000112056732 L 12.88799953460693 0.4320000112056732 L 12.88799953460693 0.3600000143051147 L 12.81599998474121 0.3600000143051147 L 12.74400043487549 0.2879999876022339 L 12.67199993133545 0.2879999876022339 L 12.67199993133545 0.2160000056028366 L 12.60000038146973 0.2160000056028366 L 12.52799987792969 0.2160000056028366 L 12.52799987792969 0.1439999938011169 L 12.45600032806396 0.1439999938011169 L 12.38399982452393 0.1439999938011169 L 12.3120002746582 0.07199999690055847 L 12.23999977111816 0.07199999690055847 L 12.16800022125244 0.07199999690055847 L 12.0959997177124 0 L 12.02400016784668 0 L 11.95199966430664 0 L 11.88000011444092 0 L 11.80799961090088 0 L 11.73600006103516 0 L 11.66399955749512 0 L 8.279999732971191 0 L 8.279999732971191 0.9359999895095825 L 9.576000213623047 0.9359999895095825 L 9.64799976348877 0.9359999895095825 L 9.720000267028809 1.008000016212463 L 9.720000267028809 1.080000042915344 L 9.720000267028809 1.151999950408936 L 9.720000267028809 1.223999977111816 L 9.720000267028809 1.296000003814697 L 9.720000267028809 1.368000030517578 L 9.64799976348877 1.368000030517578 L 9.576000213623047 1.368000030517578 L 8.279999732971191 1.368000030517578 L 8.279999732971191 2.375999927520752 L 12.60000038146973 2.375999927520752 L 12.60000038146973 17.56800079345703 L 8.279999732971191 17.56800079345703 L 8.279999732971191 18.28800010681152 L 8.35200023651123 18.28800010681152 L 8.423999786376953 18.28800010681152 L 8.496000289916992 18.28800010681152 L 8.567999839782715 18.28800010681152 L 8.640000343322754 18.28800010681152 L 8.640000343322754 18.36000061035156 L 8.711999893188477 18.36000061035156 L 8.784000396728516 18.36000061035156 L 8.855999946594238 18.43199920654297 L 8.928000450134277 18.50399971008301 L 9 18.50399971008301 L 9 18.57600021362305 L 9.071999549865723 18.64800071716309 L 9.144000053405762 18.71999931335449 L 9.144000053405762 18.79199981689453 L 9.144000053405762 18.86400032043457 L 9.215999603271484 18.86400032043457 L 9.215999603271484 18.93600082397461 L 9.215999603271484 19.00799942016602 L 9.215999603271484 19.07999992370605 L 9.215999603271484 19.15200042724609 L 9.288000106811523 19.22400093078613 L 9.215999603271484 19.22400093078613 L 9.215999603271484 19.29599952697754 L 9.215999603271484 19.36800003051758 L 9.215999603271484 19.44000053405762 L 9.215999603271484 19.51199913024902 L 9.144000053405762 19.58399963378906 L 9.144000053405762 19.6560001373291 L 9.144000053405762 19.72800064086914 L 9.071999549865723 19.72800064086914 L 9.071999549865723 19.79999923706055 L 9 19.79999923706055 L 9 19.87199974060059 L 8.928000450134277 19.94400024414063 L 8.855999946594238 19.94400024414063 L 8.855999946594238 20.01600074768066 L 8.784000396728516 20.01600074768066 L 8.711999893188477 20.08799934387207 L 8.640000343322754 20.08799934387207 L 8.567999839782715 20.08799934387207 L 8.567999839782715 20.15999984741211 L 8.496000289916992 20.15999984741211 L 8.423999786376953 20.15999984741211 L 8.35200023651123 20.15999984741211 L 8.279999732971191 20.15999984741211 L 8.279999732971191 20.95199966430664 L 11.66399955749512 20.95199966430664 Z M 8.279999732971191 0 L 4.967999935150146 0 L 4.895999908447266 0 L 4.823999881744385 0 L 4.751999855041504 0 L 4.679999828338623 0 L 4.607999801635742 0 L 4.535999774932861 0 L 4.464000225067139 0.07199999690055847 L 4.320000171661377 0.07199999690055847 L 4.248000144958496 0.07199999690055847 L 4.248000144958496 0.1439999938011169 L 4.176000118255615 0.1439999938011169 L 4.104000091552734 0.1439999938011169 L 4.032000064849854 0.2160000056028366 L 3.960000038146973 0.2160000056028366 L 3.888000011444092 0.2879999876022339 L 3.815999984741211 0.2879999876022339 L 3.74399995803833 0.3600000143051147 L 3.671999931335449 0.4320000112056732 L 3.599999904632568 0.5040000081062317 L 3.528000116348267 0.5040000081062317 L 3.528000116348267 0.5759999752044678 L 3.456000089645386 0.5759999752044678 L 3.456000089645386 0.6480000019073486 L 3.384000062942505 0.6480000019073486 L 3.384000062942505 0.7200000286102295 L 3.312000036239624 0.7919999957084656 L 3.240000009536743 0.8640000224113464 L 3.240000009536743 0.9359999895095825 L 3.240000009536743 1.008000016212463 L 3.167999982833862 1.080000042915344 L 3.167999982833862 1.151999950408936 L 3.095999956130981 1.223999977111816 L 3.095999956130981 1.296000003814697 L 3.095999956130981 1.368000030517578 L 3.095999956130981 1.440000057220459 L 3.095999956130981 1.51199996471405 L 3.023999929428101 1.656000018119812 L 3.023999929428101 1.728000044822693 L 3.023999929428101 1.799999952316284 L 3.023999929428101 1.871999979019165 L 3.023999929428101 19.07999992370605 L 3.023999929428101 19.15200042724609 L 3.023999929428101 19.22400093078613 L 3.023999929428101 19.29599952697754 L 3.095999956130981 19.36800003051758 L 3.095999956130981 19.44000053405762 L 3.095999956130981 19.51199913024902 L 3.095999956130981 19.58399963378906 L 3.095999956130981 19.6560001373291 L 3.167999982833862 19.72800064086914 L 3.167999982833862 19.79999923706055 L 3.167999982833862 19.87199974060059 L 3.240000009536743 19.94400024414063 L 3.240000009536743 20.01600074768066 L 3.312000036239624 20.08799934387207 L 3.312000036239624 20.15999984741211 L 3.384000062942505 20.23200035095215 L 3.456000089645386 20.30400085449219 L 3.456000089645386 20.37599945068359 L 3.528000116348267 20.37599945068359 L 3.528000116348267 20.44799995422363 L 3.599999904632568 20.44799995422363 L 3.671999931335449 20.52000045776367 L 3.74399995803833 20.59199905395508 L 3.815999984741211 20.59199905395508 L 3.888000011444092 20.66399955749512 L 3.960000038146973 20.73600006103516 L 4.032000064849854 20.73600006103516 L 4.104000091552734 20.73600006103516 L 4.104000091552734 20.8080005645752 L 4.176000118255615 20.8080005645752 L 4.248000144958496 20.8080005645752 L 4.320000171661377 20.8799991607666 L 4.464000225067139 20.8799991607666 L 4.535999774932861 20.8799991607666 L 4.607999801635742 20.95199966430664 L 4.679999828338623 20.95199966430664 L 4.751999855041504 20.95199966430664 L 4.823999881744385 20.95199966430664 L 4.895999908447266 20.95199966430664 L 4.967999935150146 20.95199966430664 L 8.279999732971191 20.95199966430664 L 8.279999732971191 20.15999984741211 L 8.208000183105469 20.15999984741211 L 8.13599967956543 20.15999984741211 L 8.064000129699707 20.15999984741211 L 7.992000102996826 20.08799934387207 L 7.920000076293945 20.08799934387207 L 7.848000049591064 20.08799934387207 L 7.848000049591064 20.01600074768066 L 7.776000022888184 20.01600074768066 L 7.703999996185303 19.94400024414063 L 7.631999969482422 19.94400024414063 L 7.631999969482422 19.87199974060059 L 7.559999942779541 19.87199974060059 L 7.559999942779541 19.79999923706055 L 7.48799991607666 19.72800064086914 L 7.48799991607666 19.6560001373291 L 7.415999889373779 19.6560001373291 L 7.415999889373779 19.58399963378906 L 7.415999889373779 19.51199913024902 L 7.343999862670898 19.44000053405762 L 7.343999862670898 19.36800003051758 L 7.343999862670898 19.29599952697754 L 7.343999862670898 19.22400093078613 L 7.343999862670898 19.15200042724609 L 7.343999862670898 19.07999992370605 L 7.343999862670898 19.00799942016602 L 7.343999862670898 18.93600082397461 L 7.415999889373779 18.93600082397461 L 7.415999889373779 18.86400032043457 L 7.415999889373779 18.79199981689453 L 7.48799991607666 18.71999931335449 L 7.48799991607666 18.64800071716309 L 7.559999942779541 18.64800071716309 L 7.559999942779541 18.57600021362305 L 7.631999969482422 18.50399971008301 L 7.703999996185303 18.50399971008301 L 7.703999996185303 18.43199920654297 L 7.776000022888184 18.43199920654297 L 7.776000022888184 18.36000061035156 L 7.848000049591064 18.36000061035156 L 7.920000076293945 18.36000061035156 L 7.992000102996826 18.28800010681152 L 8.064000129699707 18.28800010681152 L 8.13599967956543 18.28800010681152 L 8.208000183105469 18.28800010681152 L 8.279999732971191 18.28800010681152 L 8.279999732971191 17.56800079345703 L 4.032000064849854 17.56800079345703 L 4.032000064849854 2.375999927520752 L 8.279999732971191 2.375999927520752 L 8.279999732971191 1.368000030517578 L 6.984000205993652 1.368000030517578 L 6.912000179290771 1.368000030517578 L 6.912000179290771 1.296000003814697 L 6.840000152587891 1.296000003814697 L 6.840000152587891 1.223999977111816 L 6.840000152587891 1.151999950408936 L 6.840000152587891 1.080000042915344 L 6.912000179290771 1.008000016212463 L 6.912000179290771 0.9359999895095825 L 6.984000205993652 0.9359999895095825 L 8.279999732971191 0.9359999895095825 L 8.279999732971191 0 Z">
                         			</path>
                         		</svg><label class="number">03-2525899#299</label>
                         </span></p>

                     </div>
                     <!-- 109.10.07 moved-->
                 </div>
             </div>

             <div class="category_wrap">
             <div class="category">
                 <p>{$arr_house["w_title"]}</p>
                 <p><img src="/themes/Rent/img/renthouse/house_search_phone_02g.svg" alt="">
                 03-358-1098</p>
             </div>
                 <div class="box_wrap">
                     <div class="box">
                     <p><label>仲介業</label><label>租管業</label><label>收取服務費</label></p>
                     <p>生活資產管理股份有限公司</p></div>
                 </div>
             </div>

            <!--<div class="phone_info">
                 <div class="number"><img src="/themes/Rent/img/renthouse/house_search_phone_01.svg" alt="" />{$arr_house["ag_phone"]}</div>
                 <hr>
                 <div class="number"><img src="/themes/Rent/img/renthouse/house_search_phone_02.svg" alt="" />03-358-1098</div>
             </div>-->
hmtl;
        }

        //身分相關end


        ///這邊做下面推薦物件

        $commodity_data_where = "";
        $commodity_data_order_by = " ORDER BY r_h.`featured` DESC,r_h.`id_rent_house`";
        $arr_house["type_title"];//對應住辦
        $arr_house["county"];//對應城市(直轄 縣)
        $arr_house["city_s"];//對應城市(區)

        $commodity_arr = [
            "id_county",
            "id_city",
            "id_rent_house_type",
        ];
        //因為台灣的地址都是三個字所以這樣玩
        $commodity_address_arr =[//做別的處理有/做分割再丟入
            "巷",
            "段",
            "路/街",
            "鄰",
            "里/村",
            "6",
            "3",
        ];

        for($i=(count($commodity_arr)-1);$i>=0;$i--){
            foreach($commodity_arr as $key =>$value){
                if($i>=$key){
                    $commodity_data_where .=" AND {$value}=".GetSQL($arr_house[$value],"int");
                }
            }

            $sql = 'SELECT count(*) count_num FROM rent_house WHERE active=1 '.$commodity_data_where.' AND r_h.`id_rent_house` !='.GetSQL(Tools::getValue("id_rent_house"),"int");
            $commodity_count = Db::rowSQL($sql,true);//求這原因是為了要處理搜索相關事宜
            if($commodity_count["count_num"]>='3'){
                break;//這個break是為了確定該搜索法是我要的 因為我要填滿三個欄位
            }
            $commodity_data_where = '';//在後面重置 如果真的所有都搜索不到則可以用別種方式處理
        }
        //由於某些匯入值的關係必須要用到其他作法

        if($commodity_data_where==''){//上一個搜索沒有取得where值
            for($i=0;$i<count($commodity_address_arr);$i++) {
                if(is_numeric($commodity_address_arr[$i])){//判斷數字最前面兩個 3,6要用
                    $commodity_data_where =" AND r_h.`part_address` LIKE ".GetSQL("%".mb_substr($arr_house["rent_address"],0,($commodity_address_arr[$i]))."%","text");
                    //上面手段是取得 例如: 桃園市 桃園區 等的做法
                }else{
                    $value_arr = explode("/",$commodity_address_arr[$i]);
                    foreach($value_arr as $kk =>$vv){//切割後執行
                        $data_num = mb_strpos($arr_house["rent_address"],$vv);
                        $data_text = mb_substr($arr_house["rent_address"],0,$data_num);
                        if($data_text==""){continue;}
                        $commodity_data_where =" AND r_h.`part_address` LIKE ".GetSQL("%".$data_text.$vv."%","text");
//                        echo $commodity_data_where.'<br/>';
                    }
                }

                $sql = "SELECT count(*) count_num FROM rent_house as r_h WHERE active=1 ".$commodity_data_where.' AND r_h.`id_rent_house` !='.GetSQL(Tools::getValue("id_rent_house"),"int");
                $commodity_count = Db::rowSQL($sql,true);//求這原因是為了要處理搜索相關事宜
                if($commodity_count["count_num"]>='3'){
                    break;//這個break是為了確定該搜索法是我要的 因為我要填滿三個欄位
                }
            }
        }

        $commodity_data_where .=' AND r_h.`id_rent_house` !='.GetSQL(Tools::getValue("id_rent_house"),"int");
        //最後加一行否則會重新秀出該id的物件

        $commodity_data = ListUse::commodity_data(null,1,3,$commodity_data_where,$commodity_data_order_by);
//        print_r($commodity_data);


        //取得是否為住家
        $sql = "SELECT count(*) as count_num FROM rent_house as r_h WHERE active=1 AND (r_h.`id_rent_house_types` LIKE '%1%' OR  r_h.`id_rent_house_types` LIKE '%2%' OR
              r_h.`id_rent_house_types` LIKE '%5%' OR r_h.`id_rent_house_types` LIKE '%6%' OR
              r_h.`id_rent_house_types` LIKE '%23%' OR r_h.`id_rent_house_types`=23 OR
              r_h.`id_rent_house_types`=1 OR r_h.`id_rent_house_types`=2 OR
              r_h.`id_rent_house_types`=3 OR r_h.`id_rent_house_types`=4) AND r_h.`id_rent_house`=".GetSQL(Tools::getValue("id_rent_house"),"int");
        $my_house_count = Db::rowSQL($sql,true);//求是否住家
        $arr_house["my_house"] = $my_house_count["count_num"];

//        echo $sql;
//        echo $arr_house["my_house"];
        // print_r($arr_house);
        $this->context->smarty->assign([
            'js' => $js,
            'arr_house' => $arr_house,
            'img' => $img_arr,
            'list_html' => $list_html,
            'browse' => $browse,
            'commodity_data' => $commodity_data
        ]);
        parent::initProcess();
    }

    public function setMedia()
    {
//        $this->addJS('https://maps.googleapis.com/maps/api/js?key=' . $this->google_key . '&libraries=geometry&sensor=false');
//        $this->addJS('https://maps.googleapis.com/maps/api/js?key='.$this->google_key);
        $this->addJS('https://maps.googleapis.com/maps/api/js?key='.$this->google_key.'&callback=initMap');
        parent::setMedia();
        // $this->addJS('/js/base.js');
        $this->addJS(THEME_URL . '/js/base.js');
        $this->addJS('/js/hc-sticky.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS(THEME_URL . '/css/vertical.css');
        $this->addCSS(THEME_URL . '/css/rent_house_modal.css');//modal css
    }



    public function displayAjaxMessageVerificationSMSRentHouse()//發送簡訊驗證碼 已存在則跳出
    {
        $action = Tools::getValue('action');    //action = 'Tel';
        $mobile = Tools::getValue('phone');    //手機號碼
        switch($action){
            case 'MessageVerificationSMSRentHouse':

                $sql = "select count(*) coun from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($mobile, 'text');
                //避免10分鐘內重複撥打同支手機
                $row_session     = Db::rowSQL($sql, true);
                $session_count = $row_session["coun"];

                $sql = "SELECT count(*) as m_count  FROM `member` WHERE active=1 AND user=".GetSQL($mobile,'text');
                $row_count     = Db::rowSQL($sql, true);

                if(!preg_match("/09[0-9]{8}/",$mobile)){
                    echo json_encode(array("error"=>"tel error","return"=>"請輸入手機號碼 如:09XXXXXXXX123"));
                    break;
                }else if($session_count > '0'){
                    echo json_encode(array("error"=>"tel wait","return"=>"請等待10分鐘後再輸入手機號碼"));
                    break;
                }else if($row_count>=1){
                    echo json_encode(array("error"=>"user exist","return"=>"此帳號已存在 請登入在留言"));
                    break;
                }

                $user_ip = $_SERVER['REMOTE_ADDR'];//如果有異常的發送則能夠透過log得知
                $text = rand(1000,9999);//生成隨機的4碼
                $text_sms = '您的驗證碼為:'.$text;
                $user_id = '';
                $sms_id= '';
                $sms_return =  SMS::send_sms($mobile,$text_sms,$user_id,$sms_id,$vendor='mitake',"off");
                //自己輸入否則會連 您的驗證碼為:都有影響輸入
                $sql = "INSERT INTO sms_log (`session_id`, `tel`, `captcha`, `ip`, `msgid`, `statuscode`,
                            `return_status`, `accountPoint`)
                            VALUES (".GetSQL(session_id(), 'text').",".GetSQL($mobile, 'text').",
                            ".GetSQL($text, 'text').",".GetSQL($user_ip, 'text').",".GetSQL($sms_return['msgid'],
                        'text').",".GetSQL($sms_return['statuscode'], 'text').",
                            ".GetSQL(SMS::sms_status_text($sms_return['statuscode'],"mitake"), 'text').",".GetSQL($sms_return['AccountPoint'], 'int').")";
                Db::rowSQL($sql);//存入資料庫

                if($sms_return['statuscode'] =='0' ||$sms_return['statuscode'] =='1' || $sms_return['statuscode'] =='2'|| $sms_return['statuscode'] =='4'){
                    echo json_encode(array("error"=>"","return"=>"請等候簡訊發送"));
                    break;
                }else{
                    echo json_encode(array("error"=>"","return"=>"簡訊發送有誤 請洽管理人員"));
                    break;
                }
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
    }
}
