<?php
namespace web_rent;
use \IifeHouseController;
use \Tools;
use \SearchHouse;
use \RentHouse;
use \JSON;
use \ListUse;
use \File;
use \Db;
use \Google_api;
class HouseMapController extends IifeHouseController
{
	public $page = 'house_map';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->google_key = 'AIzaSyAU9s5j6uICVXp47T3-AxlV_RZ7cVk7cwg';        //百合苑
		$this->google_key = 'AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs';        //2021 update
//		$this->$google_key = 'AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c';		//生活樂租
		$type                            = Tools::getValue('type');
		$this->meta_title                = $this->l('地圖搜尋');
		$this->page_header_toolbar_title = $this->l('地圖搜尋');
		$this->className                 = 'HouseMapController';
        $this->THEME_URL = "/themes/Rent";
		$this->no_FormTable              = true;
        $this->table = 'rent_house';
		$this->breadcrumbs               = [
			[
				'href'  => '/',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('首頁'),
			],
			[
				'href'  => '/house_map?type=' . $type,
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('吉屋快搜'),
			],
			[
				'href'  => '',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('租屋'),
			],
		];

        $default_county = '桃園市';

        $default_city   = '桃園區';

        if (empty($_GET['county'])) {

            $_GET['county'] = $default_county;

            $_GET['city_s']   = $default_city;

        }

		parent::__construct();
	}

	public function setMedia()
	{
////		$this->addJS('https://maps.googleapis.com/maps/api/js?key=' . $this->google_key . '&libraries=geometry&sensor=false');
//		$this->addJS('/' . MEDIA_URL . '/MarkerClusterer-1.0.1/MarkerClusterer.js');
//		$this->addJS('/' . MEDIA_URL . '/markerwithlabel-1.1.8/markerwithlabel.js');
//		$this->addJS('/' . MEDIA_URL . '/google-map/js/google_map.js');
		$this->addCSS('/' . MEDIA_URL . '/google-map/css/google_map.css');
		parent::setMedia();
        $this->addCSS(THEME_URL . '/css/search_house.css');
        $this->addJS(THEME_URL . '/js/search_house.js');

	}

	public function initProcess()
	{
        SearchHouse::getContext()->getGet();
//        Tools::getValue("county");
//        print_r($_GET);
        $county = Tools::getValue("county");//縣直轄市
        $city = Tools::getValue("city");//鄉鎮市的陣列
        $id_type = Tools::getValue("id_type");//型態的陣列
        $id_types = Tools::getValue("id_types");//類別的陣列
        $id_device = Tools::getValue("id_device");//設備的陣列
        $id_other = Tools::getValue("id_other");//其他條件的陣列
        $max_price = Tools::getValue("max_price");//最高金額
        $min_price = Tools::getValue("min_price");//最低金額
        $max_ping = Tools::getValue("max_ping");//最高坪數
        $min_ping = Tools::getValue("min_ping");//最低坪數
        $max_room = Tools::getValue("max_room");//最多房數
        $min_room = Tools::getValue("min_room");//最低房數
        $id_device_class = Tools::getValue("id_device_class");//設備主類別的陣列
        $search = Tools::getValue("search");//鄉鎮市的陣列
        $id_member = $_SESSION["id_member"];//favorite要用的
        $session_id = session_id();//favorite要用的
        $favorite_data = "";//favorite要用的

        $arr_house = ListUse::getContext()->get(null,1,10,$county,$city,
            $id_type,$id_types,$id_device,$id_device_class,$id_other,$max_price,$min_price,$max_ping,$min_ping,
            $max_room,$min_room,$search);


        if(!empty($id_member)){//有
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND is_member=".GetSQL($id_member,"int");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個is_member
        }else{//只有session_id
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND session_id=".GetSQL($session_id,"text");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個session_id
        }

        $info_config = [];
        $marker_config = '';
        $info_config_html =[];

        foreach ($arr_house as $i => $v) {
            if(empty($arr_house[$i]['latitude']) || empty($arr_house[$i]['longitude'])
              || $arr_house[$i]['latitude']=='NULL' || $arr_house[$i]['longitude']=='NULL'){//避免出現因為沒有座標而導致無法運行
//                echo  $arr_house[$i]['id_rent_house']."is jump <br/>";
                continue;
            }//因為如果無法正確的取得經緯度會導致錯誤 不如直接跳過

            //物件型態
            $sql = "SELECT * FROM rent_house_types WHERE id_rent_house_types IN (".Db::antonym_array($arr_house[$i]['id_rent_house_types']).")";
            $sql_types = Db::rowSQL($sql, false);
            $arr_house[$i]['sql_types'] = $sql_types;//整批搜索到的丟進去

            //物件類別
            $sql = "SELECT * FROM rent_house_type WHERE id_rent_house_type IN (".Db::antonym_array($arr_house[$i]['id_rent_house_type']).")";
            $sql_type = Db::rowSQL($sql, false);
            $arr_house[$i]['sql_type'] = $sql_type;//整批搜索到的丟進去

            //我的最愛start
            foreach($favorite_data as $key => $value){
                if($arr_house[$i]["id_rent_house"] == $favorite_data[$key]["table_id"]){//上面搜索了該table與本次使用者的資訊 這邊濾同id
                    $arr_house[$i]["favorite"] = $favorite_data[$key]["active"];
                }
            }
            //end

            //這邊要做處理圖片 LIST 只處理版面 所以讀版面
            $sql = "SELECT * FROM rent_house_file WHERE file_type=0 AND id_rent_house=".$v['id_rent_house'].' ORDER BY id_rhf ASC ';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $arr_house[$i]['img'] = $row['url'];
            //圖片end

            //這邊是要弄google輸出檔案所使用



            if(!empty($marker_config)){
                $marker_config .=",{
                    position :{lat : {$arr_house[$i]['latitude']}, lng :{$arr_house[$i]['longitude']}},
                    map : map,
                    title : '{$arr_house[$i]["case_name"]}'
                }";
            }else{
                $marker_config .="{
                    position :{lat : {$arr_house[$i]['latitude']}, lng :{$arr_house[$i]['longitude']}},
                    map : map,
                    title : '{$arr_house[$i]["case_name"]}'
                }";
            }



//************************ 這邊是準備策是直接丟入html材料用的******************//

            //tag start ---
            $sql = "SELECT * FROM other_conditions WHERE id_other_conditions IN (".Db::antonym_array($arr_house[$i]['id_other_conditions']).")
            ORDER BY position ASC";

            $sql_tag = Db::rowSQL($sql, false);
            $tag_body = "";
            foreach($sql_tag as $key => $value){
                if($sql_tag[$key]['svg_url']!=""){
                    $tag_body .= '<i><img src="'.$sql_tag[$key]['svg_url'].'" alt="'.$sql_tag[$key]['title'].'" title="'.$sql_tag[$key]['title'].'"><p>'.$sql_tag[$key]['title'].'</p></i>';
                }
            }
            //這邊是作tag

            $tag ='<tag>'.$tag_body.'</tag>';
            //tag end ---

            //各種聯絡人 等..的狀態資料
            $sql = "SELECT *,(IF(m.`gender`=1,'先生','小姐')) as member_gender FROM member as m
                    LEFT JOIN `broker` as b ON b.`id_member` = m.`id_member`
                    WHERE m.`id_member`=".GetSQL($arr_house[$i]["id_member"],"int");
            $member_data = Db::rowSQL($sql,true);
            $favorite = '';

            if($arr_house[$i]["favorite"]){
                $favorite ='<collect data-id="'.$arr_house[$i]["id_rent_house"].'"data-type="'.$this->table.'" class="favorite on"><img src="/themes/Rent/img/icon/heart_on.svg"></collect>';
            }else{
                $favorite ='<collect data-id="'.$arr_house[$i]["id_rent_house"].'"data-type="'.$this->table.'" class="favorite"><img src="/themes/Rent/img/icon/heart.svg"></collect>';
            }
            $phone = '';//主要是ag的關係
            $name = '';
            if(strstr($member_data["id_member_additional"],"3")){//房仲
                $phone = $member_data["user"];
                $name = $member_data["name"];
            }else if(strstr($member_data["id_member_additional"],"4")) {//房東自上
                $phone = $member_data["user"];
                $name = $member_data["name"];
            }else{//我們這邊
                if(empty($arr_house[$i]["ag_phone"])){
                    $arr_house[$i]["ag_phone"] = "0939-688089";
                }
                if(empty($arr_house[$i]["develop_ag"])){
                    $arr_house[$i]["develop_ag"] = "Joy";
                }
                $phone = $arr_house[$i]["ag_phone"];
                $name = $arr_house[$i]["develop_ag"];
            }

            $floor = "";
            if($arr_house[$i]['whole_building']=='1'){
                $floor = "整棟建築";
            }else if((empty($arr_house[$i]["rental_floor"]) && empty($arr_house[$i]["floor"])) ||
                    ($arr_house[$i]["rental_floor"]='NULL' && $arr_house[$i]["floor"]=='NULL')){
            }else{
                $floor = "{$arr_house[$i]["rental_floor"]}/{$arr_house[$i]["floor"]}F";
            }
            $info_config_html[] = <<<html
                <div class="map_marker">
                    <div class="photo_row">
                        <img src="{$arr_house[$i]['img']}" class="img" alt="{$arr_house[$i]['rent_house_title']}">
                        {$favorite}
                    </div>
                    <div class="data">
                        <h3 class="title">{$arr_house[$i]['case_name']}</h3>
                        <div>{$arr_house[$i]["type_title"]}<i></i>{$arr_house[$i]["ping"]}坪<i></i>{$floor}</div>
                        <div class="address">{$arr_house[$i]["rent_address"]}</div>
                        <price>{$arr_house[$i]["rent_cash"]}<unit>元/月</unit></price>
                        <br>
                        {$tag}
                        <a class="phone" href="tel:{$phone}">
                            <img src="{$this->THEME_URL}/img/icon/house_search_phone_01.svg">{$phone}
                        </a>
                        <div class="name">{$name}</div>
                    </div>
                </div>
html;

            //************************ 這邊是準備策是直接丟入html材料用的 end ******************//

            //備案用
            $info_config[] = [
                "id_rent_house" => $arr_house[$i]["id_rent_house"],
                "case_name" => $arr_house[$i]["case_name"],
                "rent_house_title" => $arr_house[$i]["rent_house_title"],
                "img" => $arr_house[$i]["img"],
                "favorite" => $arr_house[$i]["favorite"],
                "type_title" => $arr_house[$i]["type_title"],
                "ping"=>$arr_house[$i]["ping"],
                "part_address"=> $arr_house[$i]["rent_address"],
                "rent_cash" =>$arr_house[$i]["rent_cash"],
                "favorite_html" =>$favorite,
                "tag"=>$tag,
                "phone" => $phone,
                "floor" => $floor,
                "name"=>$name
            ];
        }

        $marker_config = "[{$marker_config}]";//轉成她的通用格式
        $info_config = json_encode($info_config);
        $info_config_html = json_encode($info_config_html);

        //公司座標
        $lat        = 25.0210944;
        $lng        = 121.2982983;
//        $lat = "25.0147685";
//        $lng = "121.3160043";

        if(!empty($search)){//拿搜索的來當作標的
            $get_google_api = Google_api::get_lat_and_long($search);
            $json_de = json_decode($get_google_api,true);
            if($json_de["status"]=="OK"){
                $get_google_api = Google_api::get_data_lat_and_long($get_google_api);
                if($get_google_api!=''){
                    $lat = $get_google_api["0"]["lat"];
                    $lng = $get_google_api["0"]["lng"];
                }
            }
        }


        $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
        $main_house_class = Db::rowSQL($sql);

		$zoom       = 13;
		$minZoom    = 10;
		$maxZoom    = 19;
//		$google_key = 'AIzaSyAU9s5j6uICVXp47T3-AxlV_RZ7cVk7cwg';        //百合苑
		$google_key = 'AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c';		//生活樂租
		$google_key = 'AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs';        // 2021 update

		$default_price_step = 1000;
		$default_ping_step = 0;
		$this->context->smarty->assign([
			'select_county'          => RentHouse::getContext()->getCounty('none'),
			'arr_city'               => RentHouse::getContext()->getCity('none'),
			//			'select_use'    => RentHouse::getContext()->getUse('none'),	//20200213 用途拿掉
			'select_type'            => RentHouse::getContext()->getType(),
			'select_types'           => RentHouse::getContext()->getTypes(),
			'select_device_category' => RentHouse::getContext()->getDeviceCategory(),
			'select_other_conditions' => RentHouse::getContext()->getOtherConditionsy(),
            'main_house_class'      => $main_house_class,
			'min_price'  => 0,
			'max_price'  => 100000000,
			'min_ping'   => 0,
			'max_ping'   => 10000,
			'price_step' => $default_price_step,
			'ping_step'  => $default_ping_step,
		]);

		$this->context->smarty->assign([
			'google_key'    => $google_key,
			'google_map'    => [
				'lat'     => $lat,
				'lng'     => $lng,
				'zoom'    => $zoom,
				'minZoom' => $minZoom,
				'maxZoom' => $maxZoom,
				'url'     => '/HouseMap',
			],
			'arr_house'    => $arr_house,
            'marker_config' => $marker_config,
            'info_config'  => $info_config,
            'info_config_html' => $info_config_html
		]);

		parent::initProcess();
	}

	public function ajaxProcessGetGoogleMarker()
	{
		return JSON::getContext()->encode();
	}
}
