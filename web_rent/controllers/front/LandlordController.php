<?php

namespace web_rent;

use \IifeHouseController;
use \Db;
use \Config;
use \ListUse;

class LandlordController extends IifeHouseController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'landlord';

	public function __construct()
	{
		$this->meta_title                = $this->l('樂租服務-房東篇');
		$this->page_header_toolbar_title = $this->l('樂租服務-房東篇');
		$this->className                 = 'LandlordController';
		$this->no_FormTable              = true;

		$this->breadcrumbs = [
			[
				'href'  => '/',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('首頁'),
			],
			[
				'href'  => '/service',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('樂租服務'),
			],
			[
				'href'  => '',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('房東篇'),
			],
		];
		parent::__construct();
	}

	public function initProcess()
	{
		parent::initProcess();
		$commodity_order_by = " ORDER by r_h.`featured` DESC,r_h.`id_rent_house` DESC";//精選優先 之後則是其他條件

		//精選的物件
		$commodity_sreach = " AND r_h.`featured`=1 ";
//        $commodity_sreach = " AND r_h.`rent_house_code` IN('HR-4A33','HR-2F32','AR-OA4','HR-3B292') ";
		$commodity_data = ListUse::commodity_data(null,1,999999,$commodity_sreach,$commodity_order_by);

		$sql         = 'SELECT * FROM `tenant`';
		$row         = Db::rowSQL($sql);
		$Tenant_HTML = Config::get('Tenant_HTML');
		$Tenant_JS   = Config::get('Tenant_JS');
		$Tenant_CSS  = Config::get('Tenant_CSS');

js;
				//輪播
				        $js =<<<hmtl
				        <script>
				        $(".commodity_data").slick({
				        dots: false,
				        infinite: true,
				        autoplay: true,
				        autoplaySpeed: 3000,
				        slidesToShow: 5,
				        slidesToScroll: 5,

				    });


						    $(".slider_center").slick({
						        dots: false,
						        infinite: true,
						        centerMode: true,
						        slidesToShow: 5,
						        slidesToScroll: 9
						    });


				</script>
hmtl;

		$sql           = 'SELECT * FROM `landlord`';
		$row           = Db::rowSQL($sql);
		$Landlord_HTML = Config::get('Landlord_HTML');
		$Landlord_JS   = Config::get('Landlord_JS');
		$Landlord_CSS  = Config::get('Landlord_CSS');

		$this->context->smarty->assign([
			'row'            => $row,
			'Landlord_HTML' => $Landlord_HTML,
			'Landlord_JS'   => $Landlord_JS,
			'Landlord_CSS'  => $Landlord_CSS,
			'commodity_data' => $commodity_data,
			'js'               => $js,
		]);
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addCSS(THEME_URL . '/css/tenant.css');
		$this->addJS(THEME_URL . '/js/tenant.js');
        $this->addJS(THEME_URL . '/js/rent_service.js');
        $this->addCSS(THEME_URL . '/css/rent_service.css');
	}
}
