<?php

namespace web_rent;
use \IifeHouseController;
use \Tools;
use \Member;

class LogoutController extends IifeHouseController
{

    public $page = 'logout';

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {

//        $this->meta_title                = $this->l('會員登出');
//        $this->page_header_toolbar_title = $this->l('會員登出');
//        $this->fields['page']['class']   = 'orange_page';
//        $this->className                 = 'LogoutController';
        parent::__construct();

        $_GET['logout'] = 'yes';//刻意給的
        $logout = new Member;
        $logout->logout();
        exit;
    }
}

