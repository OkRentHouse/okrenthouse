<?php

namespace web_rent;
use \IifeHouseController;
use \Tools;
use \Member;

class LoginController extends IifeHouseController
{

	public $page = 'login';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('會員登入');
		$this->page_header_toolbar_title = $this->l('會員登入');
		$this->fields['page']['class']   = 'orange_page';
		$this->className                 = 'LoginController';
		parent::__construct();
		$this->fields['form'] = [
			'member' => [
				'input' => [
					'user'     => [
						'name'        => 'user',
						'type'        => 'text',
						'label'       => $this->l('帳號'),
						'placeholder' => $this->l('帳號'),
						'maxlength'   => '100',
						'required'    => true,
					],
					'password' => [
						'type'        => 'new-password',
						'label'       => $this->l('密碼'),
						'placeholder' => $this->l('密碼'),
						'required'    => true,
						'minlength'   => '6',
						'maxlength'   => '20',
						'name'        => 'password',
					],
				],
			],
		];
	}

	public function postProcess()
	{
		$is_login    = Tools::isSubmit('is_login');
		$user        = Tools::getValue('user');
		$password    = Tools::getValue('password');
		$auto_loging = Tools::getValue('auto_login');
		if (!empty($is_login)) {
			$this->validateRules();
			if (count($this->_errors) == 0) {
				if (!Member::login($user, $password, $auto_loging)) {
					$this->_errors[] = $this->l('帳號密碼錯誤!');
				} else {
					$this->login();
					exit;
				}
			}
		}
	}

	//已登入
	public function login()
	{
		$page = Tools::getValue('page');
		if (empty($page)) {
			Tools::redirectLink('//' . WEB_DNS);
		} else {
			Tools::redirectLink('//' . WEB_DNS . base64_decode(Tools::getValue('page')));
		}
		exit;
	}


    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/inlife.css');
        $this->addCSS('/css/font-awesome.css');
    }





}





