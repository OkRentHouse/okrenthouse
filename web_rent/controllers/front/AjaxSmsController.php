<?php

namespace web_rent;
use \Tools;
use \Db;
use \IifeHouseController;


class AjaxSmsController extends IifeHouseController
{
    public $page = 'ajax_sms';

    public function __construct()
    {
        $this->className                 = 'AjaxSmsController';
        $verification_code =   empty($_POST['verification_code'])?"":$_POST['verification_code'];//驗證碼
        $user = empty($_POST['user'])?"":$_POST['user'];//電話
        $table = 'member';

        if(!empty($verification_code) && !empty($user)) {
            $sql = 'SELECT num(*) as num FROM `' . $table . '`  WHER `user`=' . GetSQL($user, "text") . ' AND  `verification_code`=' . GetSQL($verification_code, "text");
            //驗證手機與sms驗證碼是否相同
            $row = Db::rowSQL($sql);
//        echo json_encode($row['num']);
            echo $row['num'];
        }else{
            echo "0";
        }
    }
}
exit;//避免有問題