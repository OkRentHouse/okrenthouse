<?php

namespace web_rent;

use \IifeHouseController;
use \Tools;
use \Db;
use \File;

class CollectDividendController extends IifeHouseController
{
    public $page = 'collectdividend';

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->className = 'CollectDividendController';

        $this->meta_title                = $this->l('大集大利');
        $this->page_header_toolbar_title = $this->l('大集大利');

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life_index',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('in 生活'),
            ],
            [
                'href' => '/collectdividend',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('大集大利'),
            ],
            //這塊?　對
        ];

        parent::__construct();
    }

    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }


    public function initProcess()
    {
        //輪播js外掛
        $js =<<<js
        $(".regular").slick({
            dots: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow: 4,
            slidesToScroll: 2,
        });
js;

        $sql = 'SELECT li.`id_in_life`,li.`title`, ilf.`id_file` FROM `in_life` li
                            LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
                            WHERE ilf.`file_type`=2 AND li.`active`=1 ORDER BY li.`top` DESC ,li.id_in_life ASC';//旁邊顯示各種活動與連結

        $row_all = Db::rowSQL($sql);

        foreach ($row_all as $i => $v) {
            $row = File::get($v['id_file']);
            $row_all[$i]['img'] = $row['url'];
        }
        $this->context->smarty->assign([
            'row_all' => $row_all,
            'js' => $js,
        ]);

        parent::initProcess();
    }


    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('css/slick.css');
        $this->addCSS('css/slick-theme.css');
        $this->addJS('js/slick.min.js');
    }
}