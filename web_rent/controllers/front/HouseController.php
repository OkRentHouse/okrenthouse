<?php

namespace web_rent;

use \IifeHouseController;

use \Tools;

class HouseController extends IifeHouseController

{

	public $page = 'house';



	public $tpl_folder;    //樣版資料夾

	public $definition;

	public $_errors_name;



	public function __construct()

	{

		$type                            = Tools::getValue('type');

		$this->meta_title                = $this->l('中悅一品|裝潢雅緻屋況優|桃園市桃園區中正路');

		$this->page_header_toolbar_title = $this->l('中悅一品|裝潢雅緻屋況優|桃園市桃園區中正路');

		$this->className                 = 'HouseController';

		$this->tabAccess['view'] = true;

		$this->breadcrumbs               = [

			[

				'href'  => '/',

				'hint'  => $this->l(''),

				'icon'  => '',

				'title' => $this->l('首頁'),

			],

			[

				'href'  => '/list' . $type,

				'hint'  => $this->l(''),

				'icon'  => '',

				'title' => $this->l('吉屋快搜'),

			],

			[

				'href'  => '',

				'hint'  => $this->l('/list?&county=桃園市'),

				'icon'  => '',

				'title' => $this->l('桃園市'),

			],

			[

				'href'  => '',

				'hint'  => $this->l('/list?&county=桃園市&city_s=桃園區'),

				'icon'  => '',

				'title' => $this->l('桃園區'),

			],

			[

				'href'  => '',

				'hint'  => $this->l(''),

				'icon'  => '',

				'title' => $this->l('整層住家'),

			],

		];

		parent::__construct();

	}



	public function initProcess()

	{

		parent::initProcess();



		$arr_house  = [

			[

				'id'       => 1,

				'title'    => '早安藝文',

				'subtitle' => '中正藝文特區 大器方正格局',

				'img'      => '/img/house/1/home_a.png',

				'price'    => '16800',

				'collect'  => false,

				'unit'     => $this->l('元'),

				't1'       => '住家',

				't2'       => '華廈',

				't3'       => '桃園市桃園區新埔八街',

				't4'       => '3房',

				't5'       => '1廳',

				't6'       => '2衛',

				'ping'     => '38.52',

				'floor'    => '5/16樓',

			],

			[

				'id'       => 2,

				'title'    => '中山天廈',

				'subtitle' => '捷運中山站旁 摩登時尚美宅',

				'img'      => '/img/house/2/home_b.png',

				'price'    => '8800',

				'collect'  => false,

				'unit'     => $this->l('元'),

				't1'       => '套房',

				't2'       => '華夏',

				't3'       => '台北市中山區中山北路',

				't4'       => '2房',

				't5'       => '1廳',

				't6'       => '2衛',

				'ping'     => '28.52坪',

				'floor'    => '3/12樓',

			],

			[

				'id'       => 3,

				'title'    => '得安御品',

				'subtitle' => '鄰近得安公園 坐擁無敵景觀',

				'img'      => '/img/house/3/home_c.png',

				'price'    => '12800',

				'collect'  => false,

				'unit'     => $this->l('元'),

				't1'       => '整層住家',

				't2'       => '華廈',

				't3'       => '花蓮市德安路',

				't4'       => '3房',

				't5'       => '2廳',

				't6'       => '2衛',

				'ping'     => '42.52坪',

				'floor'    => '5/18樓',

			],

		];



		$this->context->smarty->assign([

			'arr_house'        => $arr_house,

		]);

	}



	public function setMedia()

	{

		parent::setMedia();

		$this->addJS($this->context->js_list['base']);

		$this->addJS($this->context->js_list['hc-sticky']);

	}



}