<?php
namespace web_rent;
use \IifeHouseController;
class ScheduleController extends IifeHouseController
{
    public $page = 'service';

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->meta_title                = $this->l('樂租服務');
        $this->page_header_toolbar_title = $this->l('樂租服務');
        $this->className                 = 'ScheduleController';
        $this->no_FormTable              = true;

        $this->breadcrumbs = [
            [
                'href'  => '/',
                'hint'  => $this->l(''),
                'icon'  => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href'  => '/service' ,
                'hint'  => $this->l(''),
                'icon'  => '',
                'title' => $this->l('樂租服務'),
            ],
            [
                'href'  => '',
                'hint'  => $this->l(''),
                'icon'  => '',
                'title' => $this->l('租事大吉'),
            ],
        ];
        parent::__construct();
    }
}