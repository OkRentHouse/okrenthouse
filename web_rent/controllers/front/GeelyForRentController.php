<?php
namespace web_rent;
use \IifeHouseController;
use \Tools;
use \GeelyForRent;
use \File;
use \Db;
class GeelyForRentController extends IifeHouseController
{
	public $page = 'geely_for_rent';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$type                            = Tools::getValue('type');
		$this->meta_title                = $this->l('新聞資訊');
		$this->page_header_toolbar_title = $this->l('新聞資訊');
		$this->className                 = 'GeelyForRentController';
		$this->no_FormTable              = true;

		$this->breadcrumbs = [
			[
				'href'  => '/',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('首頁'),
			],
			[
				'href'  => '/geely_for_rent' . $type,
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('新聞資訊'),
			],
		];
		parent::__construct();
	}

	public function initProcess()
	{
		$id      = Tools::getValue('id');
		$id_type = Tools::getValue('id_type');

		GeelyForRent::getContext()->setViews($id);
		$Rent_type = GeelyForRent::getContext()->getType(true);

		$date       = today();
		$arr        = GeelyForRent::getContext()->get($id_type, $id, 1, $date, 10, true);
		$pagination = Db::getContext()->getJumpPage(5);
		foreach ($arr as $i => $v) {
			$row            = File::get($v['id_file']);
			$arr[$i]['img'] = $row['url'];
		}
		if (!empty($id)) {
			$id_type = $arr[0]['id_geely_for_rent_type'];
		}

		if (!empty($id_type)) {
			foreach ($Rent_type as $i => $v) {
				if ($v['id_geely_for_rent_type'] == $id_type) {
					$href = '';
					if (!empty($id)) {
						$href = '/geely_for_rent?id_type=' . $id_type;
					}
					$this->breadcrumbs[] = [
						'href'  => $href,
						'hint'  => $this->l(''),
						'icon'  => '',
						'title' => $v['title'],
					];
					break;
				}
			}
		}
		if (!empty($id) && !empty($id_type)) {

			//20200213 業主要求刪除
//			$this->breadcrumbs[] = [
//				'href'  => '',
//				'hint'  => $this->l(''),
//				'icon'  => '',
//				'title' => $arr[0]['title'],
//			];
		} elseif (empty($id_type)) {
			$this->breadcrumbs[] = [
				'href'  => '',
				'hint'  => $this->l(''),
				'icon'  => '',
				'title' => $this->l('總覽'),
			];
		}

//		print_r($arr);

		$this->context->smarty->assign([
			'geely_rent' => $arr,
			'rent_type'  => $Rent_type,
			'id_type'    => $id_type,
		]);
		if (!empty($id)) {
			$this->setTemplate('page.tpl');
		} else {
			$this->setTemplate('list.tpl');
		}
		parent::initProcess();
	}

	public function displayAjax()
	{
		die('Ajax');
		if ($this->json) {
			$this->context->smarty->assign([
				'json'   => true,
				'status' => $this->status,
			]);
		};
		$this->layout                    = 'layout-ajax.tpl';
		$this->display_header            = false;
		$this->display_footer            = false;
		$this->display_header_javascript = false;
		$this->display_footer_javascript = false;
		return $this->display();
	}
    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/rent_type.css');
    }
}