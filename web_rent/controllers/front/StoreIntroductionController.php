<?php

namespace web_rent;

use \IifeHouseController;
use \Tools;
use \Db;
use \File;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \RentHouse;


class StoreIntroductionController extends IifeHouseController
{

    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->className = 'StoreIntroductionController';

        $this->meta_title                = $this->l('生活好康');
        $this->page_header_toolbar_title = $this->l('生活好康');
        $this->no_FormTable              = true;

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life_index',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('in 生活'),
            ],
            [
                'href' => '/Store',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('生活好康'),
            ],
            [
                'href' => '/Store',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('特約商店'),
            ],

            //這塊?　對
        ];

        $id =Tools::getValue("id");




        if(empty($id)){//如果沒有id就遣返/Store
            echo " <script   language ='javascript'  type ='text/javascript'> window.location.href = '/Store' </script> ";
        }else{
            $sql = "SELECT count(*) num FROM store WHERE id_store=".GetSQL($id,"int")." AND active=1";
            $store_data = Db::rowSQL($sql,true);
            if(empty($store_data["num"])){//指如果他不啟用則跳出去
                echo " <script   language ='javascript'  type ='text/javascript'> window.location.href = '/Store' </script> ";
            }
        }

//        print_r($_SESSION);

        //瀏覽人數start
        $sql = "SELECT views FROM store WHERE id_store=".GetSQL($id,"int");
        $views = Db::rowSQL($sql,true);
        $views_update = $views["views"]+1;
        $sql = "UPDATE store set views=".$views_update." WHERE id_store=".GetSQL($id,"int");
        Db::rowSQL($sql,true);
        //瀏覽人數end

        //麵包屑start
        $sql = "SELECT s.county as county,s.area as city_s,s.id_store_type as id_store_type_s,s_t.store_type as store_type FROM store  as s 
                LEFT JOIN store_type as s_t ON s_t.id_store_type = s.id_store_type
                WHERE  s.id_store=".GetSQL($id,"int");

        $breadcrumbs_data = Db::rowSQL($sql,true);
        $get_data = "/Store?";//搜索東西
       foreach($breadcrumbs_data as $k => $v){
           if($k !='id_store_type_s' && $k !='store_type'){
               $get_data .="&".$k."=".$v;
               $this->breadcrumbs[] = [
                   'href' => $get_data,
                   'hint' => $this->l(''),
                   'icon' => '',
                   'title' => $this->l($v),
               ];
           }else if($k =='id_store_type_s'){
               $this->breadcrumbs[] = [
                   'href' => '/Store?id_store_type_s='.$v,
                   'hint' => $this->l(''),
                   'icon' => '',
                   'title' => $this->l($breadcrumbs_data["store_type"]),
               ];
           }
       }
        //麵包屑end

        parent::__construct();
    }

    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function initProcess()
    {

        $id =Tools::getValue("id");

        $sql = "SELECT *FROM store  as s 
                LEFT JOIN store_type as s_t ON s_t.id_store_type = s.id_store_type
                WHERE  s.id_store=".GetSQL($id,"int");
        $store_data = Db::rowSQL($sql,true);

        $file_type = ['0','1','2'];//(0:商標LOGO, 1:店家圖檔, 2:產品服務介紹圖檔) 各取一個
        foreach($file_type as $k => $v){
            $sql =  "SELECT id_file FROM store_file WHERE id_type={$v} AND id_store = ".GetSQL($id,"int")."
             ORDER BY id_store_logo ASC LIMIT 1";
            $photo_data = Db::rowSQL($sql,true);
            if(!empty($photo_data["id_file"])){
                $file_img = File::get($photo_data["id_file"]);
                $img = $file_img["url"];
            }else{
                $img = "/themes/Rent/img/storeintroduction/default.jpg";
            }
            $store_data["img"][$v] = $img;
        }


        $arr_popularity      = Store::getContext()->get_popularity_val($store_data['id_store']);

        $store_data['popularity'] = Popularity::getPopularityImg($store_data['id_store'], max(0, $arr_popularity['all']));
//        $row               = Store::getContext()->get_popularity($store_data['id_store'], $_SESSION['id_member']);

        $sql = "SELECT * FROM `store_popularity` WHERE id_store=".GetSQL($id,"int")." AND id_member=".GetSQL($_SESSION["id_member"],"int");
        $store_popularity_data = Db::rowSQL($sql,true);

        $my_star           = $store_popularity_data["popularity"];
        $store_data["star"] = $my_star;
        $store_data['star_img'] = Popularity::getPopularityImg($store_data['id_store'], max(0, $my_star));


        $this->context->smarty->assign([
            'store' => $store_data,
        ]);

        parent::initProcess();
    }

    public function setMedia()
    {
        parent::setMedia();
    }
}