<?php

namespace web_rent;

use \IifeHouseController;

use \Tools;

use \InLife;

use \Db;

use \File;

class InLifeIndexController extends IifeHouseController

{

    public $page = 'in_life_index';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()

    {

        $this->meta_title = $this->l('活動分享');

        $this->page_header_toolbar_title = $this->l('活動分享');

        $this->className = 'InLifeIndexController';

        $this->table = 'participation';//參與

        $this->no_FormTable = true;

//        註解  CTRL + /

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life_index',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('in 生活'),
            ],
            //這塊?　對
        ];

        parent::__construct();
    }

    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function initProcess()

    {
        $submenu = [
            [
                'text' => '活動分享',
                'href' => '/in_life',
            ],
            [
                'text' => '創富分享',
                'href' => '/WealthSharing',
            ],
            [
                'text' => '生活好康',
                'href' => '/Store',
            ],
            [
                'text' => '生活樂購',
                'href' => '/LifeBuy',

            ],
        ];

        $carousel_js =<<<js
<script>
    $('#news_frame').newsTicker({
        row_height : 60,
        max_rows: 5,
        duration: 3000,
    });
</script>
js;

        $sql = "SELECT * FROM `in_life` WHERE active=1 ORDER BY top DESC,id_in_life DESC";
        $row = $arr = Db::rowSQL($sql);

        $carousel_body = "";

        foreach ($row as $k => $v){
            $title = "";
            $url ="";
            $title = str_replace("『","",$row[$k]["title"]);
            $title = str_replace("』","",$title);
            if($row[$k]["url"]){//存在直接給位子
                $url = "/{$row[$k]["url"]}";
            }else{//作組合
                $url = "/in_life?id={$row[$k]["id_in_life"]}";
            }
            $carousel_body .=<<<hmtl
                <li>
                    <a href="{$url}">
                        <img src="/themes/Rent/img/inlifeindex/icon.svg" alt="" class="">
                            {$title}
                    </a>
                </li>
hmtl;
        }


        if(!empty($carousel_body)){//有東西
            $carousel =<<<hmtl
            <div id="news_wrap">
               <ul id="news_frame"> 
                {$carousel_body}
               </ul>
            </div>
hmtl;

        }else{
            $carousel='';
        }
        
        $this->context->smarty->assign([
            'submenu' => $submenu,
            'carousel_js' => $carousel_js,
            'carousel' => $carousel,
        ]);

        parent::initProcess();
    }


    public function setMedia()

    {
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/in_life_type.css');
        $this->addJS('/js/jquery.newsTicker.js');
    }

}