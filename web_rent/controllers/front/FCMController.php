<?php
namespace web_rent;
use \FrontController;
use \Tools;
use \JSON;
use \HouseMessage;
use \Db;
class FCMController extends FrontController
{
	public $no_page = true;
	public function displayAjaxFCM(){
		$reg_id = Tools::getValue('reg_id');
		$type = Tools::getValue('type');
		/*
		 * 新增 reg_id
		 */
		if(!empty($reg_id) && !empty($_SESSION['id_member']) && !empty($type)){
			$id_member = $_SESSION['id_member'];
			if(!empty($id_member)){
				$sql = sprintf('INSERT INTO `fcm_reg_id` (`reg_id`, `id_member`, `type`, `model`)
									VALUES (%s, %d, %s, %s) ON
									DUPLICATE KEY
									UPDATE `reg_id` = %s,`id_member` = %d, `type` = %s, `model` = %s',
					GetSQL($reg_id, 'text'),
					$id_member,
					GetSQL($type, 'text'),
					GetSQL(get_mobile(), 'text'),
					GetSQL($reg_id, 'text'),
					$id_member,
					GetSQL($type, 'text'),
					GetSQL(get_mobile(), 'text'));
				Db::rowSQL($sql);
			}
			$json = new JSON();
			$num = HouseMessage::getMemberNoReadNum($_SESSION['id_member']);		//所有未讀數字
			$time = time();
			echo $json->encode(array(
				'reg_id' => $reg_id,
				'id_member' => $id_member,
				'msg_num' => $num,
				'msg_time' => $time
			));
			exit;
		}
	}
}