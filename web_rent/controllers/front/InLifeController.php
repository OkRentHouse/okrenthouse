<?php

namespace web_rent;

use \IifeHouseController;

use \Tools;

use \InLife;

use \Db;

use \File;

class InLifeController extends IifeHouseController

{

    public $page = 'in_life';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()

    {

        $this->meta_title = $this->l('活動分享');

        $this->page_header_toolbar_title = $this->l('活動分享');

        $this->className = 'InLifeController';

        $this->table = 'participation';//參與

        $this->no_FormTable = true;

//        註解  CTRL + /

        $this->breadcrumbs = [
            [
                'href' => '/',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('首頁'),
            ],
            [
                'href' => '/in_life_index',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('in 生活'),
            ],
            [

                'href' => '/in_life',
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l('活動分享'),
            ],
            //這塊?　對
        ];
        $id = Tools::getValue('id');

        if (!empty($id)) {
            $sql = 'SELECT * FROM `in_life` li
                            LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
                            WHERE li.`id_in_life`=' . $id;

            $row_id = Db::rowSQL($sql, true);       //單一資料要加true　　會自動加LIMIT 0, 1
            $this->breadcrumbs[] = [                    //直接醬寫
                'href' => '#',     //最後一個就是本身就不用給他LINK
                'hint' => $this->l(''),
                'icon' => '',
                'title' => $this->l($row_id['title']),
            ];
        }
        parent::__construct();

    }

    public function validateRules()
    {    //驗證基本資料的輸入
        $id = Tools::getValue('id');//該活動id
        $phone    = Tools::getValue('phone');//手機
        $people    = Tools::getValue('people');//人數
        $id_in_life_activity = Tools::getValue('id_in_life_activity');//活動場次
        $verification = Tools::getValue('verification');//驗證碼
        //這邊要驗證 該活動場次是否符合這個並且時段是否存在 人數填寫　且不超越現在所剩下的報名人數 驗證碼正確 手機是否為合法　

        if(empty($phone)){  //手機
            $this->_errors[] = $this->l('您必須填寫手機');
        }else if(strlen($phone)<'6' || strlen($phone)>20){
            $this->_errors[] = $this->l('您的手機不符合長度');
        }else if(!preg_match("/09[0-9]{8}/",$phone)){
            $this->_errors[] = $this->l('請輸入手機號碼');
        }

//        $sql = "SELECT count(*) FROM participation WHERE pay=1 AND ";


        parent::validateRules();
    }
    //初始化麵包屑        //參考
    public function initBreadcrumbs($tab_id = null)
    {
        foreach ($this->breadcrumbs as $i => $v) {
            $this->breadcrumbs_txt[] = $this->l($v['title']);
        }
        $this->context->smarty->assign([
            'breadcrumbs' => $this->breadcrumbs,
            'breadcrumbs_txt' => implode(NAVIGATION_PIPE, $this->breadcrumbs_txt),
        ]);
    }

    public function initProcess()

    {

        $id = Tools::getValue('id');       //這裡

        $in_life = InLife::getContext()->getType(true);
        $submenu = [
            [
                'text' => '活動分享',
                'href' => '/in_life',
                'active' => true,
            ],
            [
                'text' => '創富分享',
                'href' => '/WealthSharing',
            ],
            [
                'text' => '生活好康',
                'href' => '/Store',
            ],
            [
                'text' => '生活樂購',
                'href' => 'https://www.lifegroup.house/LifeGo',

            ],
        ];


        InLife::getContext()->setViews($id);


        $date = today();

        $arr = InLife::getContext()->get($id, 1, $date, 10, 0,true);

        foreach ($arr as $i => $v) {
            $row = File::get($v['id_file']);
            $arr[$i]['img'] = $row['url'];
        }


        if (!empty($id)) {
            //這段是為了要處理圖片預設問題
            $sql = 'SELECT count(*) as count FROM in_life_file WHERE `file_type`=1 AND `id_in_life`='.$id;
            $row_count = Db::rowSQL($sql, true);
            $row_count  = $row_count['count'];
            if($row_count !='0'){
                $sql_add_where =' ilf.`file_type`=1 AND ';
            }

            $sql = 'SELECT * FROM `in_life` li
                            LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
                            WHERE '.$sql_add_where.' li.`id_in_life`=' . $id;//活動頁面的登入

            $row_id = Db::rowSQL($sql, true);       //單一資料要加true　　會自動加LIMIT 0, 1
            if($row_count !='0'){
                $row_id['id_file'] = File::get($row_id['id_file']);
            }else{
                $row_id['id_file']['url'] ='';//無該圖片則給再tpl給預設圖片
            }

            $sql = 'SELECT * FROM `in_life_activity` 
                    WHERE `id_in_life`=' . $id;

            $row_all = Db::rowSQL($sql);

            $option ='<option value="">---請選擇報名場次---</option>';//基本值

            foreach($row_all as $k => $v){
                $option .='<option value="'.$v['id_in_life_activity'].'">'.$v['date'].' '.$v['time'].'</option>';//run的值
            }

            $sql = 'SELECT li.`id_in_life`,li.`title`, ilf.`id_file` FROM `in_life` li
                            LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
                            WHERE ilf.`file_type`=2 AND li.`active`=1 ORDER BY li.`top` DESC ,li.id_in_life ASC';//旁邊顯示各種活動與連結

            $row_all = Db::rowSQL($sql);

            foreach ($row_all as $i => $v) {

                $row = File::get($v['id_file']);

                $row_all[$i]['img'] = $row['url'];

            }
        }

        $this->context->smarty->assign([

            'id' =>  $id,

            'row_id' => $row_id,

            'in_life' => $arr,

            'submenu' => $submenu,

            'row_all' => $row_all,

            'option'  => $option,

            'table'   =>  $this->table,
        ]);

        if (!empty($id)) {
            //內頁
            $this->setTemplate('page.tpl');

        } else {
            //List頁
            $this->setTemplate('list.tpl');

        }

        parent::initProcess();

    }



    public function postProcess(){
        $submit = Tools::getValue('submit'.$this->table);
        if(!empty($submit)){//按下去
            $this->validateRules();
            if (count($this->_errors))
                return;//有問題跳出問題


        }
    }


//
//    public function postProcess()
//    {
//        $is_login    = Tools::isSubmit('is_login');
//        $user        = Tools::getValue('user');
//        $password    = Tools::getValue('password');
//        $auto_loging = Tools::getValue('auto_login');
//
//
//
//
//        if (!empty($is_login)) {
//            $this->validateRules();
//            if (count($this->_errors) == 0) {
//                if (!Member::login($user, $password, $auto_loging)) {
//                    $this->_errors[] = $this->l('帳號密碼錯誤!');
//                } else {
//                    $this->login();
//                    exit;
//                }
//            }
//        }
//    }




    public function setMedia()

    {
        parent::setMedia();
        $this->addCSS(THEME_URL . '/css/in_life_type.css');
//        $this->addCSS(THEME_URL . '/css/login.css');
    }

}