<?php
/* Smarty version 3.1.28, created on 2020-12-14 04:00:54
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/InLife/page.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd672f63f0447_09841166',
  'file_dependency' => 
  array (
    '30e52f3a94879c51a59d76420de0eec38f3b2a20' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/InLife/page.tpl',
      1 => 1607678603,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd672f63f0447_09841166 ($_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['row_id']->value['title'])) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

    <div class="activity_wrap">
        <div class="banner">
            <img class="" src="<?php if (!empty($_smarty_tpl->tpl_vars['row_id']->value['id_file']['url'])) {
echo $_smarty_tpl->tpl_vars['row_id']->value['id_file']['url'];
} else { ?>https://okrent.house/img/in_life_banner_default.jpg<?php }?>" width="100%">
        </div>
        <div class="title_wrap">
            <?php echo $_smarty_tpl->tpl_vars['row_id']->value['content'];?>

            <div class="info_wrap">
                <div class="left">
                    <div class="info_list">
                        <div class="caption">時間場次
                        </div>
                        <div class="text">
                            <?php echo $_smarty_tpl->tpl_vars['row_id']->value['date_s'];?>
.<?php echo $_smarty_tpl->tpl_vars['row_id']->value['time_s'];?>
 ~ <?php if (!empty($_smarty_tpl->tpl_vars['row_id']->value[$_smarty_tpl->tpl_vars['row_id']->value['date_e']])) {
echo $_smarty_tpl->tpl_vars['row_id']->value['date_e'];
} else {
echo $_smarty_tpl->tpl_vars['row_id']->value['date_s'];
}?>.<?php echo $_smarty_tpl->tpl_vars['row_id']->value['time_e'];?>

                        </div>
                    </div>
                    <div class="info_list">
                        <div class="caption">活動費用
                        </div>
                        <div class="text">
                            <?php echo $_smarty_tpl->tpl_vars['row_id']->value['cost'];?>

                        </div>
                    </div>
                    <div class="info_list">
                        <div class="caption">主辦單位
                        </div>
                        <div class="text">
                            <?php echo $_smarty_tpl->tpl_vars['row_id']->value['organizer'];?>

                        </div>
                    </div>
                    <div class="info_list">
                        <div class="caption">協辦單位
                        </div>
                        <div class="text">
                            <?php echo $_smarty_tpl->tpl_vars['row_id']->value['co_organizer'];?>

                        </div>
                    </div>
                    <form action="" method="post"
                          enctype="application/x-www-form-urlencoded" class="form_wrap">
                        <div class="sign_up_wrap">
                        <div class="title">
                            活動報名
                        </div>



















                        <div class="form-group">
                            <div class="floating">
                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/phone_01.svg">
                                <input class="floating__input" type="text" name="phone" id="phone" placeholder="手 機" required />
                                <label for="input__text" class="floating__label" data-content="手 機">
                                </label>
                            </div>






                        </div>








                        <div class="form-group">
                            <div class="floating">
                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/sessions.svg">
                                <!-- <label class="control-label" for="">報名場次</label> -->
                                <select class="form-control search-form" name="id_in_life_activity" id="id_in_life_activity">
                                    <?php echo $_smarty_tpl->tpl_vars['option']->value;?>

                                </select>
                            </div>
                            <div class="floating">
                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/people.svg">
                                <input class="floating__input" type="number" placeholder="人數" id="people" name="people" required />
                                <label for="input__text" class="floating__label" data-content="人數">
                                </label>
                            </div>
                        </div>





                        <div class="verification">
                            <div class="caption">驗證碼</div>
                            <img class="icon" src="http://lifegroup.house/verification">
                            <div class="floating ">

                                <input type="text" id="verification" class="form-control" name="verification" maxlength="20" required autocomplete="off">
                                <label for="input__text" class="floating__label" data-content="">
                                </label>
                            </div>
                        </div>
                        <div class="btn_wrap">
                            <button class="btn" type="submit"  id="submit<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
" name="submit<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
" value="1">送 出</button>



                        </div>
                    </div>
                    </form>


                </div>
                <div class="right">
                    <div class="other_activity">
                        <?php
$_from = $_smarty_tpl->tpl_vars['row_all']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                            <a href="https://www.okrent.house/in_life?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id_in_life'];?>
">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['v']->value['img'];?>
">
                            </a>
                        <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } else { ?>
    找不到相關資料
<?php }
}
}
