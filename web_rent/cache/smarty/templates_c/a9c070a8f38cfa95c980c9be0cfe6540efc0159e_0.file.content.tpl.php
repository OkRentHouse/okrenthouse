<?php
/* Smarty version 3.1.28, created on 2021-04-29 13:32:13
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Landlord/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_608a44dd698833_66358384',
  'file_dependency' => 
  array (
    'a9c070a8f38cfa95c980c9be0cfe6540efc0159e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Landlord/content.tpl',
      1 => 1619668682,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../rent_service/menu.tpl' => 1,
  ),
),false)) {
function content_608a44dd698833_66358384 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<div class="rent_service">
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../rent_service/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="frame_wrap">
		<div class="title text-center"><?php echo Config::get('landlord_title');?>
</div>
		<div class="honeycomb">
			<ul class="honeycomb_list_wrap1"> 
                <?php $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'j', 0);?>
                <?php
$_from = $_smarty_tpl->tpl_vars['row']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
                <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'k', 0);?>
                <?php if (in_array($_smarty_tpl->tpl_vars['i']->value,array(5,11,16,21))) {?>
                    <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'k', 0);?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['j']->value != ($_smarty_tpl->tpl_vars['j']->value+$_smarty_tpl->tpl_vars['k']->value)) {?>
                <?php $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable($_smarty_tpl->tpl_vars['j']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'j', 0);?>
			</ul><ul class="honeycomb_list_wrap<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
">
                <?php }?>
				<li class="honeycomb_list">
					<a class="hexagontent"<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['content'])) {?> data-show="service_content<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" href="#service_content"<?php } else { ?> href="javascript:void()"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
				</li>
                <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_item_0_saved_key;
}
?>
			</ul>
		</div>
		<div class="service_content_wrap" id="service_content">
            <?php
$_from = $_smarty_tpl->tpl_vars['row']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_1_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_1_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
				<div id="service_content<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="service_content collapse"><?php echo $_smarty_tpl->tpl_vars['item']->value['content'];?>
</div>
            <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_local_item;
}
}
if ($__foreach_item_1_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_item;
}
if ($__foreach_item_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_item_1_saved_key;
}
?>
		</div>
	</div>
</div>
<?php echo $_smarty_tpl->tpl_vars['Landlord_HTML']->value;?>

<?php if (!empty($_smarty_tpl->tpl_vars['Landlord_CSS']->value)) {?>
	<style>
		<?php echo $_smarty_tpl->tpl_vars['Landlord_CSS']->value;?>

	</style>
<?php }
if (!empty($_smarty_tpl->tpl_vars['Landlord_JS']->value)) {?>
	<?php echo '<script'; ?>
 type="text/javascript">
        <?php echo $_smarty_tpl->tpl_vars['Landlord_JS']->value;?>

	<?php echo '</script'; ?>
>
<?php }
}
}
