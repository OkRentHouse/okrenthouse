<?php
/* Smarty version 3.1.28, created on 2021-02-25 20:14:04
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/StoreIntroduction/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6037948c1fd7a3_83714108',
  'file_dependency' => 
  array (
    'ad0449d1b1f66188ef107387c47aee7cbe488b09' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/StoreIntroduction/content.tpl',
      1 => 1614253789,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6037948c1fd7a3_83714108 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<div class="store_name">
    <div class="title"><?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
</div>
    <i></i>
    <div class="category">
        <p class="" href=""><?php echo $_smarty_tpl->tpl_vars['store']->value['store_type'];?>
</p>
    </div>
</div>
<div class="special_store_main_wrap">
    <hr>
    <br>
    <div class="row">
        <div class="col-sm-4 text-left">
            <div class="img_frame">
                <img src="<?php echo $_smarty_tpl->tpl_vars['store']->value['img']["1"];?>
">


            </div>
        </div>
        <div class="col-sm-4  text-center">
            <div class="store_content">
                <!-- <div class="title"></div>
                <div class="career"></div> -->
                <div class="store_info">
                    <img src="/themes/Rent/img/storeintroduction/icon_star.svg" alt="" class="">
                    店家資訊
                </div>
                <div class="list add">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/map_tag.svg">
                        ADD
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['address1'];?>

                    </div>
                </div>

                <div class="list tel">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/phone.svg">
                        TEL 1
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['tel1'];?>


                    </div>
                </div>

                <div class="list tel">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/phone.svg">
                        TEL 2
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['tel2'];?>

                    </div>
                </div>

                <div class="list time">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/time.svg">
                        TIME
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['open_time_w'];?>

                    </div>
                </div>

                <div class="list time">
                    <label>

                        END
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['close_time'];?>

                    </div>
                </div>

                <div class="list hot">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/hot.svg">
                        HOT
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['service'];?>

                        <br><?php echo $_smarty_tpl->tpl_vars['store']->value['service'];?>

                    </div>
                </div>

                <div class="list offer">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/offer.svg">
                        OFFER
                        <i></i>
                    </label>
                    <div class="content"><?php echo $_smarty_tpl->tpl_vars['store']->value['discount'];?>

                    </div>
                </div>
                <div class="list offer">
                    <label>

                        WEBSITE
                        <i></i>
                    </label>
                    <div class="content"><a href="<?php echo $_smarty_tpl->tpl_vars['store']->value['website'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['website'];?>
</a>
                    </div>
                </div>
                <div class="popularity"><label><img class="icon" src="/themes/Rent/img/storeintroduction/star.svg">人氣星等</label><div class="div_popularity" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['popularity'];?>
</div></div>
                <?php if (!empty($_SESSION['id_member'])) {?>
                <div class="star"><label><img class="icon" src="/themes/Rent/img/storeintroduction/star.svg"></i>給星星</label><div class="div_star give_star" data-title="<?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
" data-star="<?php echo $_smarty_tpl->tpl_vars['store']->value['star'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['star_img'];?>
</div></div>
                <?php }?>




























            </div>
        </div>
        <div class="col-sm-4 text-right">
            <div class="img_frame">
                <img src="<?php echo $_smarty_tpl->tpl_vars['store']->value['img']["2"];?>
">

                <!-- <img src="./生活好康-特約商店_files/default.jpg" alt="" class=""> -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center">
            <div class="main_img img_frame">
                <img src="<?php echo $_smarty_tpl->tpl_vars['store']->value['img']["0"];?>
">

            </div>
        </div>
    </div>

    <div class="introduction_wrap">
        <div class="caption">
            <img src="/themes/Rent/img/storeintroduction/icon_star.svg" alt="" class="">
            店家介紹
        </div>
       <?php echo $_smarty_tpl->tpl_vars['store']->value['introduction'];?>

    </div>


</div>
<?php }
}
