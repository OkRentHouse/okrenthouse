<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:03:08
  from "/home/ilifehou/life-house.com.tw/themes/Rent/breadcrumbs.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5a45c40f443_31355874',
  'file_dependency' => 
  array (
    'b66eb290c3fa0284d2330a9bf44f89112fea65f2' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/breadcrumbs.tpl',
      1 => 1572011903,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fc5a45c40f443_31355874 ($_smarty_tpl) {
?>
<ol class="breadcrumb">
	<?php
$_from = $_smarty_tpl->tpl_vars['breadcrumbs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_breadcrumbs_val_0_saved_item = isset($_smarty_tpl->tpl_vars['breadcrumbs_val']) ? $_smarty_tpl->tpl_vars['breadcrumbs_val'] : false;
$__foreach_breadcrumbs_val_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['breadcrumbs_val'] = new Smarty_Variable();
$__foreach_breadcrumbs_val_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_breadcrumbs_val_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['breadcrumbs_val']->value) {
$__foreach_breadcrumbs_val_0_saved_local_item = $_smarty_tpl->tpl_vars['breadcrumbs_val'];
?><li class="breadcrumb-item<?php if (count($_smarty_tpl->tpl_vars['breadcrumbs']->value) == $_smarty_tpl->tpl_vars['i']->value+1) {?> active<?php }?>"><a<?php if (!empty($_smarty_tpl->tpl_vars['breadcrumbs_val']->value['href'])) {?> href="<?php echo $_smarty_tpl->tpl_vars['breadcrumbs_val']->value['href'];?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['breadcrumbs_val']->value['title'];?>
</a></li><?php
$_smarty_tpl->tpl_vars['breadcrumbs_val'] = $__foreach_breadcrumbs_val_0_saved_local_item;
}
}
if ($__foreach_breadcrumbs_val_0_saved_item) {
$_smarty_tpl->tpl_vars['breadcrumbs_val'] = $__foreach_breadcrumbs_val_0_saved_item;
}
if ($__foreach_breadcrumbs_val_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_breadcrumbs_val_0_saved_key;
}
?>
</ol><?php }
}
