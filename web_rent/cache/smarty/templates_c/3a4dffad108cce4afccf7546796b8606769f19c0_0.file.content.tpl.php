<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:04:34
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/RentHouse/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5a4b2c61120_24301304',
  'file_dependency' => 
  array (
    '3a4dffad108cce4afccf7546796b8606769f19c0' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/RentHouse/content.tpl',
      1 => 1604467850,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../house/house_item_list.tpl' => 1,
  ),
),false)) {
function content_5fc5a4b2c61120_24301304 ($_smarty_tpl) {
?>
 <!-- 麵包屑 -->
 <?php echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

    <div class="house_search_view_postion">
        <div class="house_search_view">
            昨日瀏覽 <span class="view_number"><?php echo $_smarty_tpl->tpl_vars['browse']->value["total"];?>
</span> ( <img src="/themes/Rent/img/renthouse/house_search_pc_01.svg"> <?php echo $_smarty_tpl->tpl_vars['browse']->value["pc"];?>
 | <img
                    src="/themes/Rent/img/renthouse/house_search_pc_02.svg"> <?php echo $_smarty_tpl->tpl_vars['browse']->value["phone"];?>
 )
        </div>
    </div>
    <div class="clear"></div>
 <!-- <div class="main_caption">
     <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
</h3>
 </div>
 <div class="main_caption">
     <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
</h3>
 </div> -->
 <div class="house_search">
     <div id="content" class="data_wrap">
         <div class="object_view">


             <div id="preview" class="spec-preview"> <span class="jqzoom"><img jqimg="<?php echo $_smarty_tpl->tpl_vars['img']->value[0];?>
" src="<?php echo $_smarty_tpl->tpl_vars['img']->value[0];?>
" style="width: 100%;height: 600px;" /></span> </div>
             <!--縮圖開始-->
             <input type="hidden" id="jqimg_s" value="2">
             <div class="spec-scroll"> <a class="prev">&lt;</a> <a class="next">&gt;</a>
                 <div class="items">
                     <ul>
                         <?php
$_from = $_smarty_tpl->tpl_vars['img']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                             <li><img alt="" bimg="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" src="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" onmousemove="preview(this);">
                             </li>
                         <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
                     </ul>
                 </div>
             </div>
             <!--縮圖結束-->
             <div class="clear"></div>
         </div>

         <!-- 基本資料 -->
         <?php $_smarty_tpl->tpl_vars['Break_count'] = new Smarty_Variable(3, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'Break_count', 0);?>
         <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['Break_count']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
         <?php $_smarty_tpl->tpl_vars['Break_code'] = new Smarty_Variable('</ul><ul class="list_wrap">', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'Break_code', 0);?>
         <section class="base_data">
             <h3 class="caption">基本資料</h3>
             <ul class="list_wrap">
                  <li class="list">
                      <div class="title">型態</div>
                      <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_types'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_1_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_1_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_1_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_1_saved_local_item = $_smarty_tpl->tpl_vars['types'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['types']->value['title'];?>

                          <?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_1_saved_local_item;
}
}
if ($__foreach_types_1_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_1_saved_item;
}
if ($__foreach_types_1_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_types_1_saved_key;
}
?></div>
                  </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                  <li class="list">
                      <div class="title">類別</div>
                      <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_type'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_2_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_2_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_2_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_2_saved_local_item = $_smarty_tpl->tpl_vars['type'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['type']->value['title'];?>

                          <?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_2_saved_local_item;
}
}
if ($__foreach_type_2_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_2_saved_item;
}
if ($__foreach_type_2_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_type_2_saved_key;
}
?></div>
                  </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">屋齡</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['house_old'] != '') {?>約<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['house_old'];?>
年<?php } else { ?>暫不提供<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">權狀坪數</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['ping'];?>
坪</div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                    <div class="title">室內坪數</div>
                    <div class="info">0.00坪</div>
                </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">產權登記</div>
                     <!-- <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['house_power']) {?>有<?php } else { ?>無<?php }?></div> -->
                     <div class="info">有</div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">隔間材質</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['genre']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['genre'];
} else { ?>未隔間<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">邊間</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['is_sideroom'] == 1) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['is_sideroom'];?>
有<?php } else { ?>無<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">採光</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['lighting_num']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['lighting_num'];?>
面<?php } else { ?>無採光<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">戶數</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['households']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['households'];?>
戶<?php } else { ?>未提供<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">座向</div>

                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['house_door_seat'];?>
</div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">汽車位</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['cars_have'] == 1) {?>有<?php } else { ?>無<?php }?>



                    </div>




                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">管理費</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['house']->value['s1'] == 1) {?>(包含在租金)<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee'];?>
元/<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_cycle'];
if ($_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_unit']) {?>(單位:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_unit'];?>
)<?php } else {
}
}?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">警衛管理</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
         </section>
         <div class="clear"></div>
         <hr>
         <!-- 設備提供 -->
         <section class="devices base_data equipment_provided">
             <h3 class="caption">設備提供</h3>
             <?php echo $_smarty_tpl->tpl_vars['list_html']->value;?>

         </section>
         <div class="clear"></div>
         <hr>
         <!-- 出租條件 -->
         <section class="base_data rental_conditions">
             <h3 class="caption">出租條件</h3>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">性別</div>
                     <div class="info"><?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["26"]) && empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["27"])) {?>
                     不限<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["26"];?>
 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["27"];
}?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">身份</div>
                     <div class="info"><?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["28"]) && empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["29"]) && empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["41"])) {?>
                         不限<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["28"];?>
 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["29"];?>
 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["41"];
}?></div>

                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">短租</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['m_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_s'];?>
個月~<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['m_e'];?>
個月
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_s'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_s'];?>
個月
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_e'];?>
個月
                         <?php } else { ?>不可<?php }?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">押金</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['deposit'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['deposit'];?>
個月押金<?php } else { ?>無押金<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">神龕</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["8"]) {?>可設置<?php } else { ?>不可<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">公證</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["24"]) {?>是<?php } else { ?>否<?php }?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">寵物</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["5"]) {?>可<?php } else { ?>不可<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">與房東同住</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["25"]) {?>是<?php } else { ?>否<?php }?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">入住時間</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s'];?>
~<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'];?>

                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s'];?>
之後
                         <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'];?>
之前
                         <?php } else { ?>隨時<?php }?></div>
                 </li>
             </ul>

             <?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value['my_house'])) {?>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">年齡限制</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['age']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['year_old'])) {?>拒<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
歲以下幼童‧<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
以上長者
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['age']) && empty($_smarty_tpl->tpl_vars['arr_house']->value['year_old'])) {?>拒<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
以上長者
                         <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['age']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['year_old'])) {?>拒<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
歲以下幼童
                         <?php } else { ?>不拘年齡<?php }?></div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">行業限制</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value["limit_industry"]) {
echo $_smarty_tpl->tpl_vars['arr_house']->value["limit_industry"];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">營業及工廠登記</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["10"]) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["11"])) {?>皆可<?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["10"])) {?>可營業登記<?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["11"])) {?>可工廠登記<?php } else { ?>否<?php }?></div>
                 </li>
             </ul>
             <?php }?>
         </section>
         <div class="clear"></div>
         <hr>
         <!-- 生活機能 -->
         <section class="life">
             <h3 class="caption">生活機能</h3>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">學區</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['j_school'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['e_school'];?>
‧<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>

                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>

                         <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>

                         <?php } else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">公園</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['park']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['park'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">購物</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['supermarket']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['supermarket'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">醫療</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['hospital']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['hospital'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
         </section>
         <div class="clear"></div>
         <hr>
         <!-- 交通 -->
         <section class="traffic">
             <h3 class="caption">交通</h3>
             <table class="table table-bordered">
                 <thead>
                     <tr>
                         <th>類別</th>
                         <th>站名</th>
                         <th>路線</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td>公車</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['bus'];?>
</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['bus_station'];?>
</td>
                     </tr>
                     <tr class="table-active">
                         <td>客運</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['passenger_transport'];?>
</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['passenger_transport_station'];?>
</td>
                     </tr>
                     <tr class="table-success">
                         <td>火車</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['train'];?>
</td>
                         <td></td>
                     </tr>
                     <tr class="table-warning">
                         <td>捷運</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['mrt'];?>
</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['mrt_station'];?>
</td>
                     </tr>
                     <tr class="table-danger">
                         <td>高鐵</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['high_speed_rail'];?>
</td>
                         <td></td>
                     </tr>
                 </tbody>
             </table>
         </section>
         <div class="clear"></div>
         <hr>
         <!-- 社區介紹 -->
         <section class="community">
             <h3 class="caption">社區介紹</h3>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">戶 數</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['all_num']) {?>總戶數:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['all_num'];?>
戶 <?php }
if ($_smarty_tpl->tpl_vars['arr_house']->value['household_num']) {?>住家:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['household_num'];?>
戶 <?php }
if ($_smarty_tpl->tpl_vars['arr_house']->value['storefront_num']) {?>店家:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['storefront_num'];?>
戶 <?php }?></div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">建 商</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['builders']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['builders'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list full">
                     <div class="title">休閒公設</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list full">
                     <div class="title title2">垃圾處理</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['clean_time'];?>
</div>


                 </li>
             </ul>
         </section>
         <div class="clear"></div>
         <hr>
         <!-- 特色說明 -->
         <section class="characteristic">
                <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['features'];?>

         </section>
         <div class="clear"></div>
     </div>
     <!-- 右側資訊欄 - start-->
     <div id="element" class="user_info_wrap">
         <div class="postion_for_qrcode">
             <!-- QRcode -->
             <div class="qr_code">
                 <img src="/themes/Rent/img/renthouse/qr_code.jpg" alt="" />
             </div>
             <div class="main_caption">
                 <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
<span>(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
)</span></h3>
             </div>
             <div class="main_caption">
                 <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['title'];?>
</h3>
             </div>
             <div class="caption"><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_cash'];?>
</span> 元/月 <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['s1'] == 1) {?> 含管理費<?php } else {
}?></div>
             <!-- <div class="number_wrap">
                 <li class="number_list">
                     <div class="title">案號</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
</div>
                 </li>
             </div> -->

             <ul class="list_wrap">
                 <li class="list_full">
                     <!-- <div class="title">地址</div> -->
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
</div>
                 </li>
             </ul>

             <!-- <ul class="list_wrap">
                 <li class="list">
                     <div class="title">型態</div>
                     <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_types'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_3_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_3_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_3_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_3_saved_local_item = $_smarty_tpl->tpl_vars['types'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['types']->value['title'];?>

                         <?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_local_item;
}
}
if ($__foreach_types_3_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_item;
}
if ($__foreach_types_3_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_types_3_saved_key;
}
?></div>
                 </li>

                 <li class="list clear">
                     <div class="title">類別</div>
                     <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_type'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_4_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_4_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_4_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_4_saved_local_item = $_smarty_tpl->tpl_vars['type'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['type']->value['title'];?>

                         <?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_local_item;
}
}
if ($__foreach_type_4_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_item;
}
if ($__foreach_type_4_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_type_4_saved_key;
}
?></div>
                 </li>
             </ul> -->
             <ul class="list_wrap">
                 <li class="list_full">
                     <!-- <div class="title">樓層</div> -->
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['whole_building'] == '1') {?>整棟建築<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['rental_floor'];?>
 / <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['floor'];?>
 F<?php }?></div>
                 </li>
                 <li class="list_full">
                     <!-- <div class="title">格局</div> -->
                     <div class="info" style="margin-left: -25%;">
                         <?php $_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);?>
                         <?php if (($_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '0') && ($_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '0')) {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);?>
                             開放空間
                         <?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t3'];?>
<span  class="unit">房</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t4'];?>
<span  class="unit">廳</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t5'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t5'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t5'];?>
<span  class="unit">衛</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t6'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t6'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t6'];?>
<span  class="unit">室</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t7'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t7'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t7'];?>
<span  class="unit">前陽台</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t8'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t8'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t8'];?>
<span  class="unit">後陽台</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t9'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t9'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t9'];?>
<span  class="unit">房間陽台</span><?php }?>
                     </div>
                 </li>
             </ul>

             <!-- <ul class="list_wrap house_pattern">

             </ul> -->

             <!-- <ul class="list_wrap">
                 <li class="list_full">
                     <div class="title">權狀坪數</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['ping'];?>
坪</div>
                 </li>
                 <li class="list_full">
                     <div class="title">室內坪數</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['indoor_ping'];?>
坪</div>
                 </li>
             </ul> -->
             <ul class="list_wrap">
                 <!-- <li class="list_full">
                     <div class="title">更新時間</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['update_date'];?>
</div>
                 </li> -->





             </ul>
<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['user_html'];?>

             <!-- 彈窗 - strat -->
             <div class="modal_btn text-center">
                 <div class="btn_wrap">
                     <a class="" href="#modal-container1" role="button" data-toggle="modal">
                         留言
                     </a>
                     <a class="" href="#modal-container2" role="button" data-toggle="modal">
                         預約賞屋
                     </a>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['consultant']) {?>
                        <a class="" href="#modal-container3" role="button" data-toggle="modal">
                            呼叫樂租顧問
                        </a>
                    <?php }?>
                 </div>
                 <div class="clear"></div>
             </div>
         </div>
     </div>
     <!-- 右側資訊欄 - end-->

     <div class="clear"></div>
     <div class="m_b_30"></div>
     <!-- 底部滿版區塊 -->
     <div class="full_block ">
         <div class="clear"></div>
         <hr>
         <!-- 地圖 -->
         <section class="map">
             <h3 class="caption">生活機能 / 地圖</h3>



             <iframe class="google_map" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c&q=<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
&language=zh-TW" frameborder="0" style="border:0;" allowfullscreen="">
             </iframe>



         </section>
         <section class="commodity_list">
         <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_5_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_5_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_local_item;
}
} else {
}
if ($__foreach_v_5_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_item;
}
if ($__foreach_v_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_5_saved_key;
}
?>
         </section>
     </div>
 </div>

 <!-- 彈窗 - start -->
 <!-- 留言 -->
 <div class="modal fade" id="modal-container1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>
                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
">
                 </div>
             </div>
             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name" name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_1" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>
                                         <div class="radio">
                                             <input  type="radio" name="sex_1" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel"  type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_1" type="radio" value="0">早上
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="1">下午
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>
                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <input  name="verification" type="text" value=""  placeholder="已登入則免驗證"><button type="button" onclick="MessageVerification('1')">進行驗證</button>
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
,'1');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>
 <!-- 預約賞屋 -->

 <div class="modal fade" id="modal-container2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>

                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
">
                 </div>
             </div>

             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_2" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>
                                         <div class="radio">
                                             <input  type="radio" name="sex_2" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel" type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_2" type="radio" value="0">早上
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="1">下午
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="order_time_wrap">
                                 <div class="caption">預約賞屋時間 | <span>(限一週內)</span></div>
                                 <div class="radio_wrap">
                                     <input class="form-control" name="year" type="text" placeholder="年">
                                     <input class="form-control" name="mom" type="text" placeholder="月">
                                     <input class="form-control" name="day" type="text" placeholder="日">
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="0" checked>上午
                                     </div>

                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="1">下午
                                     </div>
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="2">晚上
                                     </div>
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="3">皆可
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>

                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
,'2');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>

 <!-- 呼叫樂租顧問 -->

 <div class="modal fade" id="modal-container3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>
                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
">
                 </div>
             </div>

             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_3" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>

                                         <div class="radio">
                                             <input  type="radio" name="sex_3" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel"　type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_3" type="radio" value="0">早上
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="1">下午
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>
                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
,'3');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>
 <!-- 彈窗 - end -->

 <?php echo '<script'; ?>
>
     $(document).ready(function () {
         $('.nav .dropdown').hover(function () {
             $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(200);
         }, function () {
             $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(200);
         });
     });

     var $sticky = $('#element');
     $sticky.hcSticky({
         stickTo: '#content',
         stickyClass : 'content_class'
     });
 <?php echo '</script'; ?>
>
<?php }
}
