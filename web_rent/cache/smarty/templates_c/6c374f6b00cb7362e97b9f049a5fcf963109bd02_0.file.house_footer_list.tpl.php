<?php
/* Smarty version 3.1.28, created on 2021-04-13 16:27:17
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/house_footer_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_607555e56bc904_09614205',
  'file_dependency' => 
  array (
    '6c374f6b00cb7362e97b9f049a5fcf963109bd02' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/house_footer_list.tpl',
      1 => 1618302433,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_607555e56bc904_09614205 ($_smarty_tpl) {
?>
<style>
.commodity_footers.slick-initialized .slick-slide {
    border: 0px solid rgba(201,202,202,1);
}
<?php
$_smarty_tpl->tpl_vars['m_top'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['m_top']->value = 2;
if ($_smarty_tpl->tpl_vars['m_top']->value <= 2) {
for ($_foo=true;$_smarty_tpl->tpl_vars['m_top']->value <= 2; $_smarty_tpl->tpl_vars['m_top']->value=$_smarty_tpl->tpl_vars['m_top']->value+2) {
?>
.commodity_footers.slick-initialized .slick-slide:nth-child(<?php echo $_smarty_tpl->tpl_vars['m_top']->value;?>
) {
    margin-top: 32px;
}
<?php }
}
?>


.commodity_footers.slick-initialized .slick-slide.mar_top {
	margin-top: 32px;
}

.commodity_footers .slick-next {
    position: absolute;
    right: 1rem;
    top: -1rem;
}

.commodity_footers .slick-prev {
	position: absolute;
	right: 0px;
	top: -1rem;
	left: auto;
}

.commodity_footers button.slick-arrow.slick-next:before {
    content: url(/img/svg/b_s_next.svg);
}

.commodity_footers button.slick-arrow.slick-prev:before {
    content: url(/img/svg/b_s_prev.svg);
}
</style>
<?php $_smarty_tpl->tpl_vars['footers_link'] = new Smarty_Variable(array('',"#","#","#","#","#","#","#","#","#"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footers_link', 0);
$_smarty_tpl->tpl_vars['footers_title'] = new Smarty_Variable(array('',"#","#","#","#","#","#","#","#","#"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footers_title', 0);?>
<div class="commodity_list commodity_footers">
	<?php
$_smarty_tpl->tpl_vars['footers'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['footers']->value = 1;
if ($_smarty_tpl->tpl_vars['footers']->value <= 9) {
for ($_foo=true;$_smarty_tpl->tpl_vars['footers']->value <= 9; $_smarty_tpl->tpl_vars['footers']->value++) {
?>
		<div class="commodity<?php if ($_smarty_tpl->tpl_vars['footers']->value%2 == 0) {?> mar_top<?php }?>">
			<div class="footers_row">
				<a href="<?php echo $_smarty_tpl->tpl_vars['footers_link']->value[$_smarty_tpl->tpl_vars['footers']->value];?>
">
					<img src="/img/footer<?php echo $_smarty_tpl->tpl_vars['footers']->value;?>
.svg" class="img" alt="<?php echo $_smarty_tpl->tpl_vars['footers_title']->value[$_smarty_tpl->tpl_vars['footers']->value];?>
">
				</a>
			</div>
		</div>
	<?php }
}
?>

</div>

<?php echo '<script'; ?>
>
	$(".commodity_footers").slick({
	dots: false,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 3000,
	slidesToShow: 4,
	slidesToScroll: 4,
	});
<?php echo '</script'; ?>
>
<?php }
}
