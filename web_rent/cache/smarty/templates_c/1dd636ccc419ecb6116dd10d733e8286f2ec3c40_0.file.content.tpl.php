<?php
/* Smarty version 3.1.28, created on 2020-12-16 20:05:02
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/GoodMember/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd9f7ee1496b9_84934082',
  'file_dependency' => 
  array (
    '1dd636ccc419ecb6116dd10d733e8286f2ec3c40' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/GoodMember/content.tpl',
      1 => 1607678602,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd9f7ee1496b9_84934082 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./good_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php if ($_GET['id_announcement']) {?>
    <div class="margin_box">
        <div class="title">
            <h1><?php echo $_smarty_tpl->tpl_vars['announcement_arr']->value['title'];?>
</h1>
            <div class="good_member">
               <div>發佈時間 <?php echo $_smarty_tpl->tpl_vars['announcement_arr']->value['create_time'];?>
</div>
            </div>
        </div>
        <div class="good_member_warp">
            <?php echo $_smarty_tpl->tpl_vars['announcement_arr']->value['html_content'];?>

        </div>
    </div>
<?php } else { ?>
    <div class="goodies_member_wrap">
        <div class="banner">
            <img class="" src="/themes/Rent/img/goodmember/banner.jpg">
        </div>

        <div class="text-center">
            <h2 class="caption"><span><img src="/themes/Rent/img/goodmember/star.svg" alt="" class=""></span>我要享好康</h2>
        </div>

        <div class="step_main_wrap">
            <div class="step_img step_one">
                <img src="/themes/Rent/img/goodmember/download.svg" alt="">
            </div>

            <div class="step_wrap">
                <div class="list_frame">
                    <p class="">到
                        <a target="_blank"
                           href="https://play.google.com/store/apps/details?id=com.ilife_house.life_house&hl=zh_TW"
                           class=""><img src="/themes/Rent/img/goodmember/google_play.png" alt="google_play" class=""></a>
                        下載生活樂租APP或到生活集團體系網站線上完成會員註冊。
                    </p>
                    <ul class="flex">
                        <li class=""><img src="/themes/Rent/img/goodmember/01.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/02.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/03.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/04.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/05.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/06.svg" alt="" class=""></li>
                    </ul>

                    <div class="text-center">
                        <a href="" class="btn">立馬註冊</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="step_main_wrap">
            <div class="step_img step_two">
                <img src="/themes/Rent/img/goodmember/mobile.svg" alt="">
            </div>

            <div class="step_wrap">
                <div class="list_frame">
                    <p class="">
                        以有效手機號碼綁定VIP卡，接收認證簡訊，立馬就享好康。
                    </p>
                </div>
            </div>
        </div>

        <div class="step_main_wrap">
            <div class="step_img step_three">
                <img src="/themes/Rent/img/goodmember/gift.svg" alt="">
            </div>

            <div class="step_wrap">
                <div class="list_frame">
                    <p class="">
                        成為會員享有會員專屬生日精美禮品及紅利積點換好康。
                    </p>
                </div>
            </div>
        </div>

        <div style="margin-bottom: 130px;"></div>

        <div class="text-center">
            <img src="/themes/Rent/img/goodmember/click.jpg" alt="" class="text-center">
        </div>

        <div style="margin-bottom: 50px;"></div>

        <div class="text-center">
            <h2 class="caption"><span><img src="/themes/Rent/img/goodmember/star.svg" alt="" class=""></span>NEWS</h2>
        </div>

        <?php if (!empty($_smarty_tpl->tpl_vars['row_all']->value)) {?>
            <input type="hidden" id="count" value="<?php echo $_smarty_tpl->tpl_vars['row_sql']->value['count_log'];?>
">
            <input type="hidden" id="count_now" value="<?php echo $_smarty_tpl->tpl_vars['compartment']->value;?>
">
            <input type="hidden" id="compartment" value="<?php echo $_smarty_tpl->tpl_vars['compartment']->value;?>
">
            <ul class="list_wrap" id="add_wrap">
                <?php
$_from = $_smarty_tpl->tpl_vars['row_all']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                    <li class="list">
                        <div class="image_wrap">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['v']->value['img'];?>
">
                        </div>
                        <div class="text_wrap">
                            <div class="caption_wrap">
                                <div class="caption">
                                    <?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>

                                </div>
                                <div class="date"><?php echo $_smarty_tpl->tpl_vars['v']->value['create_time'];?>
</div>
                            </div>
                            <?php echo $_smarty_tpl->tpl_vars['v']->value['content'];?>
<span><a href="/GoodMember?id_announcement=<?php echo $_smarty_tpl->tpl_vars['v']->value['id_announcement'];?>
">more</a></span>
                        </div>
                    </li>
                <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
            </ul>
        <?php }?>
    </div>
<?php }?>


<?php }
}
