<?php
/* Smarty version 3.1.28, created on 2021-02-06 17:07:15
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/HouseMap/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_601e5c43cb0db1_67418206',
  'file_dependency' => 
  array (
    '1103f912009cc865ea36eb99bf125d8700adb5e3' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/HouseMap/content.tpl',
      1 => 1612598633,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../house/search_house3.tpl' => 1,
  ),
),false)) {
function content_601e5c43cb0db1_67418206 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<div class="big_box">
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/search_house3.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div id="map" class="house_map ajax_map" data-lat="<?php echo $_smarty_tpl->tpl_vars['google_map']->value['lat'];?>
" data-lng="<?php echo $_smarty_tpl->tpl_vars['google_map']->value['lng'];?>
" data-zoom="<?php echo $_smarty_tpl->tpl_vars['google_map']->value['zoom'];?>
" data-minZoom="<?php echo $_smarty_tpl->tpl_vars['google_map']->value['minZoom'];?>
" data-maxZoom="<?php echo $_smarty_tpl->tpl_vars['google_map']->value['maxZoom'];?>
" data-url="<?php echo $_smarty_tpl->tpl_vars['google_map']->value['url'];?>
"></div>
</div>
	<?php echo '<script'; ?>
>
		var map;

		function initMap() {
			var data_window = [];
			var myLatLng = {
					lat:<?php echo $_smarty_tpl->tpl_vars['google_map']->value['lat'];?>
,
					lng: <?php echo $_smarty_tpl->tpl_vars['google_map']->value['lng'];?>

					};

			var map = new google.maps.Map(document.getElementById('map'), {
				center: myLatLng,
				zoom : <?php echo $_smarty_tpl->tpl_vars['google_map']->value['zoom'];?>
,
			});
			var info_config = <?php echo $_smarty_tpl->tpl_vars['info_config']->value;?>
;
			var marker_config = <?php echo $_smarty_tpl->tpl_vars['marker_config']->value;?>
;
			var info_config_html = <?php echo $_smarty_tpl->tpl_vars['info_config_html']->value;?>
;

			for (var x= 0; info_config.length >x ; x++) {
				var contentString ='';
				contentString = '<div class="map_marker">' +
						'<div class="photo_row">' +
						'<img src="'+info_config[x]["img"]+'" class="img" alt="'+info_config[x]["rent_house_title"]+'">' +
						 info_config[x]["favorite_html"] +
						'</div>' +
						'<div class="data">' +
						'<h3 class="title">'+info_config[x]["case_name"]+'</h3>' +
						'<div>'+info_config[x]["type_title"]+'<i></i>'+info_config[x]["ping"]+'坪<i></i>'+info_config[x]["floor"]+'</div>' +
						'<div class="address">'+info_config[x]["part_address"]+'</div>' +
						'<price>'+info_config[x]["rent_cash"]+'<unit>元/月</unit></price>' +
						'<br>' +
						 info_config[x]["tag"]+
						'<a class="phone" href="tel:'+info_config[x]["phone"]+'">' +
						'<img src="/themes/Rent/img/icon/house_search_phone_01.svg">'+info_config[x]["phone"] +
						'</a>' +
						'<div class="name">'+info_config[x]["name"]+'</div>' +
						'</div>' +
						'</div>';


				// if(info_config[x]["favorite"]){
				// 	contentString ='<h2><a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+info_config[x]["case_name"]+'</a></h2>'+
				// 	'<span>'+info_config[x]["rent_house_title"]+'</span></br>'+
				// 	'<div class="photo_row">'+
				// 			'<a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+
				// 				'<img src="'+info_config[x]["img"]+'" class="img" alt="'+info_config[x]["rent_house_title"]+'">'+
				// 			'</a>'+
				// 			'<collect data-id="'+info_config[x]["id_rent_house"]+'" data-type="rent_house" class="favorite on">'+
				// 				'<img src="/themes/Rent/img/icon/heart_on.svg">'+
				// 			'</collect>'+
				// 	'<div>';
				// }else{
				// 	contentString ='<h2><a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+info_config[x]["case_name"]+'</a></h2>'+
				// 			'<span>'+info_config[x]["rent_house_title"]+'</span></br>'+
				// 				'<div class="photo_row">'+
				// 					'<a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+
				// 						'<img src="'+info_config[x]["img"]+'" class="img" alt="'+info_config[x]["rent_house_title"]+'">'+
				// 					'</a>'+
				// 					'<collect data-id="'+info_config[x]["id_rent_house"]+'" data-type="rent_house" class="favorite">'+
				// 						'<img src="/themes/Rent/img/icon/heart.svg">'+
				// 					'</collect>'+
				// 				'<div>';
				// }
				data_window.push(contentString);
			}

			marker_config.forEach(function (e,i) {

				var infowindow = new google.maps.InfoWindow({
					content: data_window[i]
					// content: info_config_html[i]
				});

				var marker = new google.maps.Marker(e);

				marker.addListener('click', function() {
					infowindow.open(map, marker);
				});

			})
		}




	<?php echo '</script'; ?>
>


    <?php echo '<script'; ?>
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&callback=initMap" async="" defer=""><?php echo '</script'; ?>
>
	<?php echo '</script'; ?>
>
<?php }
}
