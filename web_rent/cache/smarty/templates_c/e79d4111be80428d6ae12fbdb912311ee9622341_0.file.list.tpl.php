<?php
/* Smarty version 3.1.28, created on 2020-12-11 19:27:25
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/in_life/list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd3579decd8c8_53541050',
  'file_dependency' => 
  array (
    'e79d4111be80428d6ae12fbdb912311ee9622341' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/in_life/list.tpl',
      1 => 1607678511,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../pagination.tpl' => 1,
  ),
),false)) {
function content_5fd3579decd8c8_53541050 ($_smarty_tpl) {
?>
<ul class="list_wrap">
	<?php
$_from = $_smarty_tpl->tpl_vars['in_life']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?><li class="list"><a href="/<?php if (empty($_smarty_tpl->tpl_vars['item']->value['url'])) {?>in_life?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id_in_life'];
} else {
echo $_smarty_tpl->tpl_vars['item']->value['url'];
}?>"><div class="img">
				<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['img'])) {?><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['img'];?>
"><?php } else { ?><div class="no_img"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/logo.svg"></div><?php }?>
			</div><div class="content">
				<h3 class="caption1"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</h3>
				<div class="caption2"><?php echo $_smarty_tpl->tpl_vars['item']->value['title2'];?>
</div>
				<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['exp'])) {?><div class="text"><?php echo $_smarty_tpl->tpl_vars['item']->value['exp'];?>
</div><?php }?>
			</div><?php if (!empty($_smarty_tpl->tpl_vars['item']->value['date_t']) || !empty($_smarty_tpl->tpl_vars['item']->value['weekday_t']) || !empty($_smarty_tpl->tpl_vars['item']->value['time_t']) || !empty($_smarty_tpl->tpl_vars['item']->value['address']) || !empty($_smarty_tpl->tpl_vars['item']->value['add_name'])) {?><ul class="info">
			<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['date_t']) || !empty($_smarty_tpl->tpl_vars['item']->value['weekday_t']) || !empty($_smarty_tpl->tpl_vars['item']->value['time_t'])) {?><li class="time"><span>時間</span><?php if (!empty($_smarty_tpl->tpl_vars['item']->value['date_t'])) {?><div><?php echo $_smarty_tpl->tpl_vars['item']->value['date_t'];?>
</div><?php }
if (!empty($_smarty_tpl->tpl_vars['item']->value['weekday_t'])) {?><div class="weekday"><?php echo $_smarty_tpl->tpl_vars['item']->value['weekday_t'];?>
</div><?php }
if (!empty($_smarty_tpl->tpl_vars['item']->value['time_t'])) {?><div class="time"><?php echo $_smarty_tpl->tpl_vars['item']->value['time_t'];?>
</div><?php }?></li><?php }?>
			<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['address']) || !empty($_smarty_tpl->tpl_vars['item']->value['add_name'])) {?><li class="postion"><span>地點</span><?php if (!empty($_smarty_tpl->tpl_vars['item']->value['address'])) {?><div><?php echo $_smarty_tpl->tpl_vars['item']->value['address'];?>
</div><?php }
if (!empty($_smarty_tpl->tpl_vars['item']->value['add_name'])) {?><div><?php echo $_smarty_tpl->tpl_vars['item']->value['add_name'];?>
</div><?php }?></li><?php }?>
			</ul><?php }?></a></li><?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
} else {
?><li class="no_data"><div class="title">
			<h3 class="caption1"><?php echo l(array('s'=>"找不到相關資料"),$_smarty_tpl);?>
</h3>
		</div></li><?php
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_item_0_saved_key;
}
?>
</ul><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
