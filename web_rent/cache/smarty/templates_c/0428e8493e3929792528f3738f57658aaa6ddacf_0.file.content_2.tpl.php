<?php
/* Smarty version 3.1.28, created on 2020-12-25 14:07:44
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/JoinMaster/content_2.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fe581b04e2458_63682990',
  'file_dependency' => 
  array (
    '0428e8493e3929792528f3738f57658aaa6ddacf' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/JoinMaster/content_2.tpl',
      1 => 1607678603,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fe581b04e2458_63682990 ($_smarty_tpl) {
?>

<div class="join_life_wrap step2" style="margin: 0 auto;">
    <div class="item_wrap">
	    <div class="title">加入生活</div>
	    <div class="item_list">
		    <a class="" href="/in_life" style="font-weight: bold;">生活家族</a>
            <i></i>
            <a class="" href="/WealthSharing">加盟體系</a>
            <i></i>
            <a class="" href="/Store">菁英招募</a>
	    </div>
    </div>
    <form action="/JoinMaster"  enctype="multipart/form-data" method="post" id="sumbit_data" >
        <input type="hidden" name="put_sumbit" value="1">
        <img src="/themes/Rent/img/joinmaster/banner_01.svg" class="">
        <!-- <img src="/themes/Rent/img/joinmaster/step_02.svg" class=""> -->
        <div class="step_02">Step1.加入申請 <img src="/themes/Rent/img/joinmaster/banner_02_arrow_an.svg"> <span style="color:#3f0d81">Step2.上傳資料</span></div>
        <div class="applyform">
          <div class="row_134">
          <div class="row_1row_1_1">
            <div class="row_1">
                <div class="form_item">
                  <span class="must">*</span>
                  <span class="title">身分證字號</span>
                  <input class="inp" type="text" name="identity" maxlength="10" placeholder="" required="required">
                </div>

            </div>
            <div class="row_1_1">
              <div class="attachment">
                  <div class="row_1_1_1">
                    <input class="file_1_1_1" type="file" style="display:none">
                    <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file"><span class="get_file">上傳身分證正面</span>
                    <div class="view">僅供申請生活達人之用，不得移為他用</div>
                  </div>
                  <div class="row_1_1_2">
                    <input class="file_1_1_2" type="file" style="display:none">
                    <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file"><span class="get_file">上傳身分證反面</span>
                    <div class="view">僅供申請生活達人之用，不得移為他用</div>
                  </div>
              </div>
            </div>
          </div><div class="row_3row_4" style="display:none"><div class="row_3">
                <div class="form_item"><span class="must">*</span>
                    <span class="title"><img src="/themes/Rent/img/Jointalent/telephone.svg" class="svg">手機</span>
                    <input class="inp" type="text" name="phone" value="<?php echo $_SESSION['user'];?>
" placeholder="0912345678" maxlength="20" required="required">
                </div>
                <div class="form_item">
                    <span class="must">*</span>
                    <span class="title">
                        <img src="/themes/Rent/img/Jointalent/line.svg" class="svg">LinelD
                    </span>
                    <input class="inp" type="text" name="line_id" maxlength="32" placeholder="" required="required">
                </div>

            </div>
            <div class="row_4">
                <div class="form_item"><span class="must">*</span>
                    <span class="title">電郵</span>
                    <input class="inp" type="email" name="email" maxlength="128" placeholder="　　@　　　　　　　　"  required="required"></div>
            </div>
            </div></div><div class="row_2">
                <div class="form_item">
                    <div class="post_ID">
                        <input type="hidden" name="postal" maxlength="6">
                        <span class="must">*</span><span class="title">獎金撥人帳戶(限本人帳戶)</span>

                    </div>
                </div>
            </div>



        <div class="row_6 form2">
            <div class="form_item">
              <div class="bank_tb">
              <table class="bank_tb">
                      <tr>
                          <!-- <td rowspan="2" class="left_title">帳號</td> -->
                          <td class="row_5_td">
                          <div class="row_5_1">
                              <span class="title">
                                  <select style="width: 10rem!important;" class="inp no_border" name="bank" id="bank">
                                          <option>中國信託</option>
                                      <?php
$_from = $_smarty_tpl->tpl_vars['bank']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                                          <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
" data-code="<?php echo $_smarty_tpl->tpl_vars['v']->value['code'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
</option>
                                      <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
                                  </select>銀行</span>
                                  <span class="title">
                                      <input class="inp" type="text" name="branch" placeholder="" required="required" style="border: 1px solid;"><span class="title">分行<a target="_blank" href="/JoinMaster?p=list"><i class="fas fa-external-link-alt"></i></a>
                                  </span>
                                  <span class="title">銀行代碼：
                                      <input class="inp" type="text" name="bank_code" placeholder="822" maxlength="4" required="required">
                                  </span>
                                  <span class="title"></span>
                              </div>
                              <br>
                              <span class="bank_txt_0">金融機構存款帳號(分行別、科目、編號、檢查號碼)</span>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <div class="bank_ID">
                                  <table class="bank_ID_tb">
                                      <input type="hidden" name="bank_account" maxlength="32">
                                      <tr>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_1" type="text" maxlength="1" onkeyup="this.value=this.value.toUpperCase();document.getElementsByClassName('bank_ID_n')[1].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_2" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[2].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_3" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[3].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_4" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[4].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_5" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[5].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_6" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[6].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_7" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[7].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_8" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[8].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_9" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[9].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_10" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[10].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_11" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[11].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_12" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[12].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_13" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[13].focus()" onfocus="this.value=''">
                                          </td>
                                          <td>
                                              <input class="bank_ID_n" name="bank_account_14" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[14].focus()" onfocus="this.value=''">
                                          </td>
                                      </tr>
                                  </table>
                              </div>
                          </td>
                      </tr>
                  </table></div>
                  <!-- <span class="bank_txt_1">※限用中國信託、國泰世華帳戶非上述2家銀行者須自行負擔每次轉帳之手續費15元</span> -->

            </div>
        </div>
        <div class="row_6_1">
          <div class="attachment">
              <div class="row_1_1_1">
                <input class="file_1_1_2" type="file" style="display:none">
                <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file"><span class="get_file">上傳存摺正面</span>
                <div class="view">僅供申請生活達人之用，不得移為他用</div>
              </div>
              <div class="row_1_1_2">
                <input class="file_1_1_2" type="file" style="display:none">
                <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file"><span class="get_file">上傳存摺反面</span>
                <div class="view">僅供申請生活達人之用，不得移為他用</div>
              </div>
          </div>
        </div>
        <div class="row_6_1_1">
            <div class="form_item">
                <div class="post_ID">
                    <input type="hidden" name="postal" maxlength="6">
                    <input type="checkbox" name="i_agree"><span class="title">我已閲讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</span>

                </div>
            </div>
        </div>
        <div class="row_7">
          <div class="form_item" style="display: flex;">
            <!-- <div class="SMS submit"> 發送驗證碼 </div><input class="inp no_border" type="text" placeholder="請輸入手機驗證碼"> -->
            <div class="submit" onclick="sub_per()">
                上一步
            </div>
          </div>
        <p>&nbsp</p>
            <div class="form_item" style="display: flex;">
              <!-- <div class="SMS submit"> 發送驗證碼 </div><input class="inp" type="text" placeholder="請輸入手機驗證碼"> -->
              <div class="submit" onclick="submit_data()" class="submit_img">
                  送出
              </div>
            </div>
        </div>
        <!-- <div class="submit">
            <img src="\themes\App\mobile\img\expertAdvisor\button_empty_180t.svg" class="svg" class="submit_img" onclick="submit_data()">
        </div> -->
        </div>
    </form>
</div>
<?php }
}
