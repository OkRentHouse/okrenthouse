<?php
/* Smarty version 3.1.28, created on 2020-12-01 11:12:34
  from "/home/ilifehou/life-house.com.tw/themes/Rent/house/house_list_item2.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5b4a23d7901_64939144',
  'file_dependency' => 
  array (
    '55173b1f786f2834e9cd0fd44389ccdd070d0592' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/house/house_list_item2.tpl',
      1 => 1599731372,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fc5b4a23d7901_64939144 ($_smarty_tpl) {
?>
<div class="commodity">
	<div class="photo_row"><a href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['house']->value['id_rent_house'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['house']->value['img'];?>
" class="img" alt="<?php echo $_smarty_tpl->tpl_vars['house']->value['title'];?>
"></a>
		<collect data-id="<?php echo $_smarty_tpl->tpl_vars['house']->value['id_rent_house'];?>
" data-type="rent_house" class="favorite <?php if ($_smarty_tpl->tpl_vars['house']->value['favorite']) {?>on<?php }?>"><?php if ($_smarty_tpl->tpl_vars['house']->value['favorite']) {?>
				<img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg"><?php }?>
		</collect>
	</div>
	<div class="object">
		<div class="title_wrap">
			<div class="number"><?php echo $_smarty_tpl->tpl_vars['house']->value['rent_house_code'];?>
</div>
			<div class="address"><?php echo $_smarty_tpl->tpl_vars['house']->value['rent_address'];?>
</div>
		</div>
		<h3 class="title big"><a href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['house']->value['id_rent_house'];?>
"><?php echo $_smarty_tpl->tpl_vars['house']->value['case_name'];?>
</a></h3>
		<div class="title"><a href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['house']->value['id_rent_house'];?>
"><?php echo $_smarty_tpl->tpl_vars['house']->value['rent_house_title'];?>
</a></div>
		<div>
			<?php $_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);?>
			<?php if (($_smarty_tpl->tpl_vars['house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['house']->value['t3'] == '0') && ($_smarty_tpl->tpl_vars['house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['house']->value['t4'] == '0')) {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);?>
				開放空間　
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['house']->value['t3'] == '0') {
} else {
if ($_smarty_tpl->tpl_vars['ti']->value == 1) {?>　<?php } else {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);
}?><span class="get_me_power"><?php echo $_smarty_tpl->tpl_vars['house']->value['t3'];?>
</span>房<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['house']->value['t4'] == '0') {
} else {
if ($_smarty_tpl->tpl_vars['ti']->value == 1) {?>　<?php } else {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);
}?><span class="get_me_power"><?php echo $_smarty_tpl->tpl_vars['house']->value['t4'];?>
</span>廳<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['house']->value['t5'] == '' || $_smarty_tpl->tpl_vars['house']->value['t5'] == '0') {
} else {
if ($_smarty_tpl->tpl_vars['ti']->value == 1) {?>　<?php } else {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);
}?><span class="get_me_power"><?php echo $_smarty_tpl->tpl_vars['house']->value['t5'];?>
</span>衛<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['house']->value['t6'] == '' || $_smarty_tpl->tpl_vars['house']->value['t6'] == '0') {
} else {
if ($_smarty_tpl->tpl_vars['ti']->value == 1) {?>　<?php } else {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);
}?><span class="get_me_power"><?php echo $_smarty_tpl->tpl_vars['house']->value['t6'];?>
</span>室<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['house']->value['t7'] == '' || $_smarty_tpl->tpl_vars['house']->value['t7'] == '0') {
} else {
if ($_smarty_tpl->tpl_vars['ti']->value == 1) {?>　<?php } else {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);
}?><span class="get_me_power"><?php echo $_smarty_tpl->tpl_vars['house']->value['t7'];?>
</span>前陽台<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['house']->value['t8'] == '' || $_smarty_tpl->tpl_vars['house']->value['t8'] == '0') {
} else {
if ($_smarty_tpl->tpl_vars['ti']->value == 1) {?>　<?php } else {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);
}?><span class="get_me_power"><?php echo $_smarty_tpl->tpl_vars['house']->value['t8'];?>
</span>後陽台<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['house']->value['t9'] == '' || $_smarty_tpl->tpl_vars['house']->value['t9'] == '0') {
} else {
if ($_smarty_tpl->tpl_vars['ti']->value == 1) {?>　<?php } else {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);
}?><span class="get_me_power"><?php echo $_smarty_tpl->tpl_vars['house']->value['t9'];?>
</span>房間陽台<?php }?>
		</div>
		<div class="balcony">
			
			
			
		</div>
		<div><?php echo $_smarty_tpl->tpl_vars['house']->value['ping'];?>
 坪&nbsp;&nbsp;&nbsp;&nbsp;<?php
$_from = $_smarty_tpl->tpl_vars['house']->value['sql_types'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_0_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_0_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_0_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_0_saved_local_item = $_smarty_tpl->tpl_vars['types'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['types']->value['title'];?>

			<?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_0_saved_local_item;
}
}
if ($__foreach_types_0_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_0_saved_item;
}
if ($__foreach_types_0_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_types_0_saved_key;
}
if ($_smarty_tpl->tpl_vars['house']->value['id_rent_house_type']) {?>&nbsp;&nbsp;&nbsp;&nbsp;<?php
$_from = $_smarty_tpl->tpl_vars['house']->value['sql_type'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_1_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_1_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_1_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_1_saved_local_item = $_smarty_tpl->tpl_vars['type'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['type']->value['title'];?>

			<?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_1_saved_local_item;
}
}
if ($__foreach_type_1_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_1_saved_item;
}
if ($__foreach_type_1_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_type_1_saved_key;
}
}?>&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($_smarty_tpl->tpl_vars['house']->value['whole_building'] == '1') {?>整棟建築<?php } else {
echo $_smarty_tpl->tpl_vars['house']->value['rental_floor'];?>
 ／ <?php echo $_smarty_tpl->tpl_vars['house']->value['floor'];?>
 F<?php }?></div>
		<?php echo $_smarty_tpl->tpl_vars['house']->value['tag'];?>

	</div>












































	<?php echo $_smarty_tpl->tpl_vars['house']->value['user_html'];?>

</div>



























































<?php }
}
