<?php
/* Smarty version 3.1.28, created on 2021-01-25 14:16:18
  from "/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_600e623208d4f8_37704336',
  'file_dependency' => 
  array (
    '386603f0bbc509f6c97a32e50b20af843f96ffa4' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/header.tpl',
      1 => 1611552667,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_600e623208d4f8_37704336 ($_smarty_tpl) {
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content=" user-scalable=yes">
	<?php if (!empty($_smarty_tpl->tpl_vars['GOOGLE_SITE_VERIFICATION']->value)) {?><meta name="google-site-verification" content="<?php echo $_smarty_tpl->tpl_vars['GOOGLE_SITE_VERIFICATION']->value;?>
" /><?php }?>
<title><?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
</title>
<?php if ($_smarty_tpl->tpl_vars['meta_description']->value != '') {?><meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
"><?php }
if ($_smarty_tpl->tpl_vars['meta_keywords']->value != '') {?><meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['meta_keywords']->value;?>
"><?php }
if ($_smarty_tpl->tpl_vars['meta_img']->value != '') {?><meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['meta_img']->value[0];?>
"><?php }?>
<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
<?php if (!empty($_smarty_tpl->tpl_vars['t_color']->value)) {?><meta name="theme-color" content="<?php echo $_smarty_tpl->tpl_vars['t_color']->value;?>
"><?php }
if (!empty($_smarty_tpl->tpl_vars['mn_color']->value)) {?><meta name="msapplication-navbutton-color" content="<?php echo $_smarty_tpl->tpl_vars['mn_color']->value;?>
"><?php }
if (!empty($_smarty_tpl->tpl_vars['amwasb_style']->value)) {?><meta name="apple-mobile-web-app-status-bar-style" content="<?php echo $_smarty_tpl->tpl_vars['amwasb_style']->value;?>
"><?php }?>
<!--[if lt IE 9]>
<?php echo '<script'; ?>
 src="//html5shiv.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
<![endif]-->
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./table_title.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php
$_from = $_smarty_tpl->tpl_vars['css_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_0_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_0_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>
	<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css"/>
<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_local_item;
}
}
if ($__foreach_css_uri_0_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_item;
}
if ($__foreach_css_uri_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_0_saved_key;
}
?>
<style type="text/css">
	<?php echo Configuration::get('Rent_css_code');?>

</style>
<?php if (!isset($_smarty_tpl->tpl_vars['display_header_javascript']->value) || $_smarty_tpl->tpl_vars['display_header_javascript']->value) {?>
	<?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=G-M3BZDSGV51"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">

<?php echo '</script'; ?>
>
<?php
$_from = $_smarty_tpl->tpl_vars['js_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_1_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_1_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>
	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>
<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_local_item;
}
}
if ($__foreach_js_uri_1_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_item;
}
if ($__foreach_js_uri_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_1_saved_key;
}
if (count($_smarty_tpl->tpl_vars['_errors_name']->value) > 0) {
echo '<script'; ?>
 type="text/javascript">
$(document).ready(function(e) {
<?php
$_from = $_smarty_tpl->tpl_vars['_errors_name']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_err_name_2_saved_item = isset($_smarty_tpl->tpl_vars['err_name']) ? $_smarty_tpl->tpl_vars['err_name'] : false;
$_smarty_tpl->tpl_vars['err_name'] = new Smarty_Variable();
$__foreach_err_name_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_err_name_2_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['err_name']->value) {
$__foreach_err_name_2_saved_local_item = $_smarty_tpl->tpl_vars['err_name'];
?>
	<?php if (count($_smarty_tpl->tpl_vars['_errors_name_i']->value[$_smarty_tpl->tpl_vars['err_name']->value]) > 0) {?>
		<?php
$_from = $_smarty_tpl->tpl_vars['_errors_name_i']->value[$_smarty_tpl->tpl_vars['err_name']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ei_3_saved_item = isset($_smarty_tpl->tpl_vars['ei']) ? $_smarty_tpl->tpl_vars['ei'] : false;
$_smarty_tpl->tpl_vars['ei'] = new Smarty_Variable();
$__foreach_ei_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_ei_3_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['ei']->value) {
$__foreach_ei_3_saved_local_item = $_smarty_tpl->tpl_vars['ei'];
?>
			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parents('.form-group').addClass('has-error');
			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parent('td').addClass('has-error');
			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').focus(function(){
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parents('.form-group').removeClass('has-error');
				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parent('td').removeClass('has-error');
			});
		<?php
$_smarty_tpl->tpl_vars['ei'] = $__foreach_ei_3_saved_local_item;
}
}
if ($__foreach_ei_3_saved_item) {
$_smarty_tpl->tpl_vars['ei'] = $__foreach_ei_3_saved_item;
}
?>
	<?php } else { ?>
		$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parents('.form-group').addClass('has-error');
		$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parent('td').addClass('has-error');
		$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').focus(function(){
			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parents('.form-group').removeClass('has-error');
			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parent('td').removeClass('has-error');
		});
	<?php }
$_smarty_tpl->tpl_vars['err_name'] = $__foreach_err_name_2_saved_local_item;
}
}
if ($__foreach_err_name_2_saved_item) {
$_smarty_tpl->tpl_vars['err_name'] = $__foreach_err_name_2_saved_item;
}
?>
});
<?php echo '</script'; ?>
>
<?php }
}?>
</head>
<body <?php if (empty($_SESSION['id_member'])) {?>onContextMenu="return false"<?php }?>>
<div class="display_content">
	<?php if ($_smarty_tpl->tpl_vars['display_header']->value) {?>
		<header class="minbar">
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./main_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

		</header>
    <?php }?>
    <article class="<?php echo $_smarty_tpl->tpl_vars['mamber_url']->value;?>
">
    <?php echo $_smarty_tpl->tpl_vars['msg_box']->value;?>

<?php }
}
