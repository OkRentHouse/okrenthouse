<?php
/* Smarty version 3.1.28, created on 2021-04-20 19:20:20
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/RentHouse/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_607eb8f46fc677_05069379',
  'file_dependency' => 
  array (
    'c9a2f0d77be4ed153c158f32efc6c750376e98f5' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/RentHouse/content.tpl',
      1 => 1618917611,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../house/house_item_list.tpl' => 1,
  ),
),false)) {
function content_607eb8f46fc677_05069379 ($_smarty_tpl) {
?>
 <!-- 麵包屑 -->
 <?php echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

    <div class="house_search_view_postion">
        <div class="house_search_view">
          <!-- <svg style="cursor: pointer;" onclick="share('FB')" class="svg-inline--fa fa-share-alt fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="share-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M352 320c-22.608 0-43.387 7.819-59.79 20.895l-102.486-64.054a96.551 96.551 0 0 0 0-41.683l102.486-64.054C308.613 184.181 329.392 192 352 192c53.019 0 96-42.981 96-96S405.019 0 352 0s-96 42.981-96 96c0 7.158.79 14.13 2.276 20.841L155.79 180.895C139.387 167.819 118.608 160 96 160c-53.019 0-96 42.981-96 96s42.981 96 96 96c22.608 0 43.387-7.819 59.79-20.895l102.486 64.054A96.301 96.301 0 0 0 256 416c0 53.019 42.981 96 96 96s96-42.981 96-96-42.981-96-96-96z"></path></svg> -->


          <svg onmouseover="$('.qr_code').show()" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="qrcode" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-qrcode fa-w-14 fa-2x" style="
    width: 12pt;
    vertical-align: top;
    color: #aaa;
    margin-right: 10px;
">
            <path fill="currentColor" d="M0 224h192V32H0v192zM64 96h64v64H64V96zm192-64v192h192V32H256zm128 128h-64V96h64v64zM0 480h192V288H0v192zm64-128h64v64H64v-64zm352-64h32v128h-96v-32h-32v96h-64V288h96v32h64v-32zm0 160h32v32h-32v-32zm-64 0h32v32h-32v-32z" class=""></path></svg>
          <div class="qr_code" style="
    position: absolute;
    top: -30px;
    width: 100px;
    left: -110px;
    display: none;
">
              <img style="
    width: 100%;
" src="/themes/Rent/img/renthouse/qr_code.jpg" alt="" />
          </div>
            瀏覽數 <span class="view_number"><?php echo $_smarty_tpl->tpl_vars['browse']->value["total"];?>
</span> ( <img src="/themes/Rent/img/renthouse/house_search_pc_01.svg"> <?php echo $_smarty_tpl->tpl_vars['browse']->value["pc"];?>
 | <img
                    src="/themes/Rent/img/renthouse/house_search_pc_02.svg"> <?php echo $_smarty_tpl->tpl_vars['browse']->value["phone"];?>
 )
        </div>
    </div>
    <div class="clear"></div>
 <!-- <div class="main_caption">
     <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
</h3>
 </div>
 <div class="main_caption">
     <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
</h3>
 </div> -->
 <span class="top_title">

   <label><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
</label><span class="sec_title"><label><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_title'];?>
</label></span><label style="display:none"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
</label></span>
 <div class="house_search">
     <div id="content" class="data_wrap">
         <div class="object_view">


             <div id="preview" class="spec-preview"> <span class="jqzoom"><img jqimg="<?php echo $_smarty_tpl->tpl_vars['img']->value[0];?>
" src="<?php echo $_smarty_tpl->tpl_vars['img']->value[0];?>
" style="width: 100%;height: 600px;" /></span> </div>
             <!--縮圖開始-->
             <input type="hidden" id="jqimg_s" value="2">
             <div class="spec-scroll"> <a class="prev">&lt;</a> <a class="next">&gt;</a>
                 <div class="items">
                     <ul>
                         <?php
$_from = $_smarty_tpl->tpl_vars['img']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                             <li><img alt="" bimg="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" src="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" onmousemove="preview(this);">
                             </li>
                         <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
                     </ul>
                 </div>
             </div>
             <!--縮圖結束-->
             <div class="clear"></div>
         </div>

         <section class="characteristic">
                <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['features'];?>

         </section>
         <section class="devices base_data equipment_provided">
             <h3 class="caption"><span>設備提供</span></h3>
             <?php echo $_smarty_tpl->tpl_vars['list_html']->value;?>

         </section>
         <!-- 出租條件 -->
         <section class="base_data rental_conditions">
             <h3 class="caption"><span>出租條件</span></h3>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">性別</div>
                     <div class="info"><?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["26"]) && empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["27"])) {?>
                     不限<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["26"];?>
 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["27"];
}?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">身份</div>
                     <div class="info"><?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["28"]) && empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["29"]) && empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["41"])) {?>
                         不限<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["28"];?>
 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["29"];?>
 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["41"];
}?></div>

                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">短租</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['m_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_s'];?>
個月~<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['m_e'];?>
個月
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_s'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_s'];?>
個月
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_e'];?>
個月
                         <?php } else { ?>不可<?php }?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">押金</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['deposit'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['deposit'];?>
個月押金<?php } else { ?>無押金<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">神龕</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["8"]) {?>可設置<?php } else { ?>不可<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">公證</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["24"]) {?>是<?php } else { ?>否<?php }?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">寵物</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["5"]) {?>可<?php } else { ?>不可<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">與房東同住</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["25"]) {?>是<?php } else { ?>否<?php }?></div>
                 </li>
                 <li class="list clear">
                     <div class="title">入住時間</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s'];?>
~<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'];?>

                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s'];?>
之後
                         <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'];?>
之前
                         <?php } else { ?>隨時<?php }?></div>
                 </li>
             </ul>

             <?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value['my_house'])) {?>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">年齡限制</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['age']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['year_old'])) {?>拒<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
歲以下幼童‧<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
以上長者
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['age']) && empty($_smarty_tpl->tpl_vars['arr_house']->value['year_old'])) {?>拒<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
以上長者
                         <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['age']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['year_old'])) {?>拒<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['age'];?>
歲以下幼童
                         <?php } else { ?>不拘年齡<?php }?></div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">行業限制</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value["limit_industry"]) {
echo $_smarty_tpl->tpl_vars['arr_house']->value["limit_industry"];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">營業及工廠登記</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["10"]) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["11"])) {?>皆可<?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["10"])) {?>可營業登記<?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["11"])) {?>可工廠登記<?php } else { ?>否<?php }?></div>
                 </li>
             </ul>
             <?php }?>
         </section>

         <!-- 社區介紹 -->
         <section class="community">
             <h3 class="caption"><span>社區介紹</span></h3>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">戶 數</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['all_num']) {?>總戶數:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['all_num'];?>
戶 <?php }
if ($_smarty_tpl->tpl_vars['arr_house']->value['household_num']) {?>住家:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['household_num'];?>
戶 <?php }
if ($_smarty_tpl->tpl_vars['arr_house']->value['storefront_num']) {?>店家:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['storefront_num'];?>
戶 <?php }?></div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">建 商</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['builders']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['builders'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list full">
                     <div class="title">休閒公設</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list full">
                     <div class="title title2">垃圾處理</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['clean_time'];?>
</div>


                 </li>
             </ul>
         </section>

         <!-- 生活機能 -->
         <section class="life">
             <h3 class="caption"><span>生活機能</span><div class="labels"><label>位置</label><label>學區</label><label>公園</label><label>購物</label><label>醫療</label><label>餐飲</label><label>交通</label></div></h3>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">學區</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['j_school'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['e_school'];?>
‧<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>

                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>

                         <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>

                         <?php } else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">公園</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['park']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['park'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">購物</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['supermarket']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['supermarket'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">醫療</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['hospital']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['hospital'];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
             <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['rent_address']) {?>
             <iframe class="google_map" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&q=<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
&language=zh-TW" frameborder="0" style="border:0;width: 100%;height: 600px;" allowfullscreen="">
             </iframe>
             <?php } else { ?>
             <div class="google_map"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="map-marker-alt-slash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-map-marker-alt-slash fa-w-20 fa-3x"><path fill="currentColor" d="M300.8 502.4c9.6 12.8 28.8 12.8 38.4 0 18.6-26.69 35.23-50.32 50.14-71.47L131.47 231.62c10.71 52.55 50.15 99.78 169.33 270.78zm333.02-44.3L462.41 325.62C502.09 265.52 512 238.3 512 192 512 86.4 425.6 0 320 0c-68.2 0-128.24 36.13-162.3 90.12L45.47 3.37C38.49-2.05 28.43-.8 23.01 6.18L3.37 31.45C-2.05 38.42-.8 48.47 6.18 53.9l588.35 454.73c6.98 5.43 17.03 4.17 22.46-2.81l19.64-25.27c5.42-6.97 4.17-17.02-2.81-22.45zm-263.7-203.81L247 159.13c12.34-27.96 40.01-47.13 73-47.13 44.8 0 80 35.2 80 80 0 25.57-11.71 47.74-29.88 62.29z" class=""></path></svg><br>Opss ! 本物件沒有提供詳細位置 ... <br>歡迎聯繫 <label><a href="tel:+886033581098">03-358-1098</a></label> 由專人為您服務<br>來電請告知物件編號：<label><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
</label></div>
             <?php }?>
         </section>

         <!-- 地圖 -->
         <section class="map">
             <h3 class="caption"><span>生活機能 / 地圖</h3>



             <iframe class="google_map" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&q=<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
&language=zh-TW" frameborder="0" style="border:0;" allowfullscreen="">
             </iframe>
             <div id="map"></div>
    <div id="coords"></div>



         </section>

         <!-- 交通 -->
         <section class="traffic">
             <h3 class="caption"><span>大眾運輸</span></h3>
             <div class="stops">
               <div class="stop">
                 <ul>
                   <li class="type">公車</li>
                   <li class="list">
                     <div class="stop_name">同德十一街口</div>
                     <ul>
                       <?php $_smarty_tpl->tpl_vars['bus'] = new Smarty_Variable(preg_split("/[.]/",$_smarty_tpl->tpl_vars['arr_house']->value['bus']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bus', 0);?>
                       <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                       <li><?php echo $_smarty_tpl->tpl_vars['bus']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</li>
                       <?php }
}
?>

                     </ul>
                   </li>
                 </ul>


                 <ul>
                   <li class="type">客運</li>
                   <li class="list">
                     <div class="stop_name">中正藝文特區</div>
                     <ul>
                       <?php $_smarty_tpl->tpl_vars['bus'] = new Smarty_Variable(preg_split("/[.]/",$_smarty_tpl->tpl_vars['arr_house']->value['passenger_transport']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bus', 0);?>
                       <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                       <li><?php echo $_smarty_tpl->tpl_vars['bus']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</li>
                       <?php }
}
?>

                     </ul>
                   </li>
                 </ul>


                   <ul>
                     <li class="type">火車</li>
                     <li class="list">
                       <div class="stop_name">桃園站</div>
                       <ul>
                         <?php $_smarty_tpl->tpl_vars['bus'] = new Smarty_Variable(preg_split("/[.]/",$_smarty_tpl->tpl_vars['arr_house']->value['train']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bus', 0);?>
                         <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                         <li><?php echo $_smarty_tpl->tpl_vars['bus']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</li>
                         <?php }
}
?>

                       </ul>
                     </li>
                   </ul>

                 <ul>
                   <li class="type">捷運</li>
                   <li class="list">
                     <div class="stop_name">A9站</div>
                     <ul>
                       <?php $_smarty_tpl->tpl_vars['bus'] = new Smarty_Variable(preg_split("/[.]/",$_smarty_tpl->tpl_vars['arr_house']->value['mrt']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bus', 0);?>
                       <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                       <li><?php echo $_smarty_tpl->tpl_vars['bus']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</li>
                       <?php }
}
?>

                     </ul>
                   </li>
                 </ul>
                 <ul>
                   <li class="type">高鐵</li>
                   <li class="list">
                     <div class="stop_name">桃園站</div>
                     <ul>
                       <?php $_smarty_tpl->tpl_vars['bus'] = new Smarty_Variable(preg_split("/[.]/",$_smarty_tpl->tpl_vars['arr_house']->value['high_speed_rail']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bus', 0);?>
                       <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['bus']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                       <li><?php echo $_smarty_tpl->tpl_vars['bus']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</li>
                       <?php }
}
?>

                     </ul>
                   </li>
                 </ul>

               </div>
             </div>
             <table class="table table-bordered">
                 <thead>
                     <tr>
                         <th>類別</th>
                         <th>站名</th>
                         <th>路線</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td>公車</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['bus'];?>
</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['bus_station'];?>
</td>
                     </tr>
                     <tr class="table-active">
                         <td>客運</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['passenger_transport'];?>
</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['passenger_transport_station'];?>
</td>
                     </tr>
                     <tr class="table-success">
                         <td>火車</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['train'];?>
</td>
                         <td></td>
                     </tr>
                     <tr class="table-warning">
                         <td>捷運</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['mrt'];?>
</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['mrt_station'];?>
</td>
                     </tr>
                     <tr class="table-danger">
                         <td>高鐵</td>
                         <td><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['high_speed_rail'];?>
</td>
                         <td></td>
                     </tr>
                 </tbody>
             </table>
         </section>

         <!-- 基本資料 -->
         <?php $_smarty_tpl->tpl_vars['Break_count'] = new Smarty_Variable(3, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'Break_count', 0);?>
         <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['Break_count']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
         <?php $_smarty_tpl->tpl_vars['Break_code'] = new Smarty_Variable('</ul><ul class="list_wrap">', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'Break_code', 0);?>
         <section class="base_data">
             <h3 class="caption"><span>基本資料</span></h3>
             <ul class="list_wrap">
                  <li class="list">
                      <div class="title">型態</div>
                      <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_types'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_1_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_1_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_1_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_1_saved_local_item = $_smarty_tpl->tpl_vars['types'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['types']->value['title'];?>

                          <?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_1_saved_local_item;
}
}
if ($__foreach_types_1_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_1_saved_item;
}
if ($__foreach_types_1_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_types_1_saved_key;
}
?></div>
                  </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                  <li class="list">
                      <div class="title">類別</div>
                      <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_type'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_2_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_2_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_2_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_2_saved_local_item = $_smarty_tpl->tpl_vars['type'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['type']->value['title'];?>

                          <?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_2_saved_local_item;
}
}
if ($__foreach_type_2_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_2_saved_item;
}
if ($__foreach_type_2_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_type_2_saved_key;
}
?></div>
                  </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">屋齡</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['house_old'] != '') {?>約<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['house_old'];?>
年<?php } else { ?>暫不提供<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">權狀坪數</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['ping'];?>
坪</div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                    <div class="title">室內坪數</div>
                    <div class="info">0.00坪</div>
                </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">產權登記</div>
                     <!-- <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['house_power']) {?>有<?php } else { ?>無<?php }?></div> -->
                     <div class="info">有</div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">隔間材質</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['genre']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['genre'];
} else { ?>未隔間<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">邊間</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['is_sideroom'] == 1) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['is_sideroom'];?>
有<?php } else { ?>無<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">採光</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['lighting_num']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['lighting_num'];?>
面<?php } else { ?>無採光<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">戶數</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['households']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['households'];?>
戶<?php } else { ?>未提供<?php }?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">座向</div>

                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['house_door_seat'];?>
</div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">汽車位</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['cars_have'] == 1) {?>有<?php } else { ?>無<?php }
echo $_smarty_tpl->tpl_vars['arr_house']->value['pfy'];?>




                    </div>




                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">管理費</div>
                     <div class="info"><?php if ($_smarty_tpl->tpl_vars['house']->value['s1'] == 1) {?>(包含在租金)<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee'];?>
元/<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_cycle'];
if ($_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_unit']) {?>(單位:<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_unit'];?>
)<?php } else {
}
}?></div>
                 </li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if ($_smarty_tpl->tpl_vars['i']->value%$_smarty_tpl->tpl_vars['Break_count']->value == 0) {
echo $_smarty_tpl->tpl_vars['Break_code']->value;
}?>
                 <li class="list">
                     <div class="title">警衛管理</div>
                     <div class="info"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"];
} else { ?>無<?php }?></div>
                 </li>
             </ul>
         </section>
         <div class="clear"></div>
         <!--<hr>-->
         <!-- 設備提供 -->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->
         <!-- 特色說明 -->

         <div class="clear"></div>
     </div>
     <!-- 右側資訊欄 - start-->
     <div id="element" class="user_info_wrap">
         <div class="postion_for_qrcode">
             <!-- QRcode -->

             <div class="main_caption btn_a">
                 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>

             </div>
             <!-- <div class="main_caption">
                  <div class="title" style="display:none">主標</div>
                 <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
</h3>
             </div> -->
             <div class="main_caption">
                 <h3><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['title'];?>
</h3>
             </div>
             <div class="caption"><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_cash'];?>
</span><span style="color: #000;font-size: xx-small;"> 元/月 </span><span style="color: #999;font-size: xx-small;"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['s1'] == 1 || $_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][0] == '含管理費') {?>(含<?php if ($_smarty_tpl->tpl_vars['arr_house']->value['s1'] == 1) {?>管理費<?php }
if ($_smarty_tpl->tpl_vars['arr_house']->value['cars_have'] == 1 && $_smarty_tpl->tpl_vars['arr_house']->value['s1'] == 1 && $_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][0] == '含管理費') {?>、<?php }
if ($_smarty_tpl->tpl_vars['arr_house']->value['cars_have'] == 1 && $_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][0] == '含管理費') {?>車位<?php }?>)<?php }?></span></div>
             <!-- <div class="number_wrap">
                 <li class="number_list">
                     <div class="title">案號</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
</div>
                 </li>
             </div> -->

             <ul class="list_wrap">
                 <li class="list_full">
                     <!-- <div class="title">地址</div> -->
                     <div class="info address"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
</div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     
                     <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_types'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_3_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_3_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_3_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_3_saved_local_item = $_smarty_tpl->tpl_vars['types'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['types']->value['title'];?>

                         <?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_local_item;
}
}
if ($__foreach_types_3_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_item;
}
if ($__foreach_types_3_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_types_3_saved_key;
}
?></div>
                 </li>

                 <li class="list clear">
                     
                     <div class="info"><?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['sql_type'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_4_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_4_saved_key = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_4_total) {
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_4_saved_local_item = $_smarty_tpl->tpl_vars['type'];
if ($_smarty_tpl->tpl_vars['t']->value != '0') {?>,<?php }
echo $_smarty_tpl->tpl_vars['type']->value['title'];?>

                         <?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_local_item;
}
}
if ($__foreach_type_4_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_item;
}
if ($__foreach_type_4_saved_key) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_type_4_saved_key;
}
?></div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <!-- <li class="list_full"> -->
                     <!-- <div class="title">樓層</div> -->
                     <!-- <div class="info"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['whole_building'] == '1') {?>整棟建築<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['rental_floor'];?>
 / <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['floor'];?>
 F<?php }?></div> -->
                 <!-- </li> -->
                 <li class="list_full">
                     <!-- <div class="title">格局</div> -->
                     <div class="info" style="margin-left: -15px;">
                         <?php $_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);?>
                         <?php if (($_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '0') && ($_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '0')) {
$_smarty_tpl->tpl_vars['ti'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ti', 0);?>
                             開放空間
                         <?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t3'];?>
<span  class="unit">房</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t4'];?>
<span  class="unit">廳</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t5'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t5'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t5'];?>
<span  class="unit">衛</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t6'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t6'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t6'];?>
<span  class="unit">室</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['kitchen'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['kitchen'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['kitchen'];?>
<span  class="unit">廚</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t7'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t7'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t7'];?>
<span  class="unit">前陽台</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t8'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t8'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t8'];?>
<span  class="unit">後陽台</span><?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t9'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t9'] == '0') {
} else { ?>&ensp;<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t9'];?>
<span  class="unit">房間陽台</span><?php }?>
                     </div>
                 </li>
             </ul>
<style>

.info.address {
    font-size: 8pt !important;
    margin-top: -10px;
    font-weight: bold;
}

li.info_50w > div {
    width: 50%;
    margin-top: 10px;
}

li.info_50w {
    display: flex;
}

li.info_50w span.left_title {
  color: #999;
  font-weight: bold;
  text-align: end;
  display: inline-table;
  letter-spacing: 2px;
  font-size: 16px;
  padding-right: 5px;
}

li.info_50w span.right_title {
    font-weight: bold;
    font-size: 16px;
    vertical-align: middle;
}

li.info_50w > div span {
    width: 50%;
    display: -webkit-inline-box;
}

span.e_phone {
    color: var(--main_color1);
    vertical-align: bottom;
}

span.e_phone svg {
    width: 10px;
    fill: var(--main_color1);
    vertical-align: middle;
    margin: 0 10px;
}
span.e_phone .number {
    font-size: 15pt !important;
    letter-spacing: 1px !important;
}
.house_search .user_info_wrap .user_info .text span:nth-child(1) {
    display: inline-grid;
}
.house_search .user_info_wrap .user_info .text span label {
    line-height: 5pt;
    font-size: xx-small;
}
.category_wrap {
    background: yellow;
    border-radius: 0 20px;
}
.category p:nth-child(1) {
    text-align: center;
    font-size: 12pt !important;
}
.category p:nth-child(2) {
    font-size: 11pt !important;
    letter-spacing: -1px;
    font-weight: bold;
    color: var(--main_color1);
}
.category p:nth-child(2) img {
    width: 15px;
}
.house_search .user_info_wrap .box_wrap {
    width: 60%;
}

.house_search .user_info_wrap .box label {
    border-right: 0px solid #999;
    font-size: 10pt;
    padding: 0 5px;
    margin: 5px 0;
    position: relative;
}

.house_search .user_info_wrap .box label:last-child{
    border: 0px;
}

.house_search .user_info_wrap .box p:last-child{
    font-size: 11pt !important;
    margin-top: -4px;
    margin-bottom: 10px;
}

.category p, .box_wrap .box p {
    margin: 4px 0;
}

.category_wrap .category{
    margin-right: 10px;

}

.house_search .user_info_wrap .box {
    min-width: 100%;
}

.house_search .user_info_wrap .box label:first-child {
    padding-left: 0px;
}

.house_search .user_info_wrap .box label:last-child {
    padding-right: 0px;
}

.house_search .user_info_wrap .box label:after {
  border-right: 2px solid #bbb;
    content: " ";
    position: absolute;
    top: 0px;
    right: 0px;
    height: 7pt;
    top: 8px;
}

.house_search .user_info_wrap .box label:last-child:after {
    border-right: 0px solid #999;
    content: " ";

}

.house_search .user_info_wrap .box p {
  text-align: center;
}

.house_search .user_info_wrap .qr_code {
    top: -10px;
    right: -10px;
}

.category_wrap .category:after{
  border: 0px;
}

.modal_btn .btn_wrap {
    background: unset;
    border: 0px;
}

.modal_btn a:after {
    border: 0px;
}

.modal_btn a {
    background: #fff;
    margin: 0px 10px;
    border-radius: 0 10px;
    border: 2px solid #aaa;
    line-height: 2em;
}
</style>
             <?php $_smarty_tpl->tpl_vars['info'] = new Smarty_Variable(array("坪數","屋齡","樓層","當層戶數","採光/邊間","座向","汽車",'',"機車"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'info', 0);?>
             <?php $_smarty_tpl->tpl_vars['info_name'] = new Smarty_Variable(array("ping","house_old","floor","rental_floor","lighting_num","house_door_seat","car",'',"motorcycle"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'info_name', 0);?>

             <?php $_smarty_tpl->tpl_vars['info'] = new Smarty_Variable(array("坪數","屋齡","樓層","總樓層","當層戶數","座向","採光","邊間","汽車",'',"機車"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'info', 0);?>
             <?php $_smarty_tpl->tpl_vars['info_name'] = new Smarty_Variable(array("ping","house_old","floor","tt_floor","rental_floor","house_door_seat","lighting_num_1","lighting_num_2","car",'',"motorcycle"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'info_name', 0);?>

             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["ping"] = $_smarty_tpl->tpl_vars['arr_house']->value['ping'];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["house_old"] = $_smarty_tpl->tpl_vars['arr_house']->value['house_old'];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["floor"] = ((($_smarty_tpl->tpl_vars['arr_house']->value['rental_floor']).(" / ")).($_smarty_tpl->tpl_vars['arr_house']->value['floor'])).("F");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["floor"] = ($_smarty_tpl->tpl_vars['arr_house']->value['rental_floor']).("F");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["tt_floor"] = ($_smarty_tpl->tpl_vars['arr_house']->value['floor']).("F");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["rental_floor"] = ($_smarty_tpl->tpl_vars['arr_house']->value['households']).("戶");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["lighting_num_1"] = ($_smarty_tpl->tpl_vars['arr_house']->value['lighting_num']).("面");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["lighting_num"] = ($_smarty_tpl->tpl_vars['arr_house']->value['lighting_num']).(" / ");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['is_sideroom'] == 0) {?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["lighting_num"] = ($_smarty_tpl->tpl_vars['_house']->value["lighting_num"]).("無");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["lighting_num_2"] = "無";
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php } else { ?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["lighting_num"] = ($_smarty_tpl->tpl_vars['_house']->value["lighting_num"]).("有");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["lighting_num_2"] = "有";
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php }?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["house_door_seat"] = $_smarty_tpl->tpl_vars['arr_house']->value['house_door_seat'];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["car"] = "無";
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>

             <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['cars_have'] == 1) {?>
             <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][0] == '含管理費') {?>
              <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["car"] = (($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_type'][0]).("，")).($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][0]);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php } else { ?>
             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["car"] = (((($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_type'][0]).("，另計")).($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent'][0])).("/")).($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][0]);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php }?>
             <?php }?>

             <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["motorcycle"] = "無";
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
             <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle_have'] == 1) {?>
               <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle']['motorcycle_rent_type'][0] == '含管理費') {?>
                <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["motorcycle"] = (($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle']['motorcycle_type'][0]).("，")).($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle']['motorcycle_rent_type'][0]);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
               <?php } else { ?>
               <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, '_house', null);
$_smarty_tpl->tpl_vars['_house']->value["motorcycle"] = (((($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle']['motorcycle_type'][0]).("，另計")).($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle']['motorcycle_rent'][0])).("/")).($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle']['motorcycle_rent_type'][0]);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, '_house', 0);?>
               <?php }?>
             <?php }?>
             <ul class="info" style="    width: 100%;">
               <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['info']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['info']->value); $_smarty_tpl->tpl_vars['i']->value=$_smarty_tpl->tpl_vars['i']->value+2) {
?>
                <li class="info_50w"><div><span class="left_title"><?php echo $_smarty_tpl->tpl_vars['info']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</span><span class="right_title"<?php if (!$_smarty_tpl->tpl_vars['info']->value[$_smarty_tpl->tpl_vars['i']->value+1]) {?> style="position: absolute; margin-top: 3px;"<?php }?>><?php echo $_smarty_tpl->tpl_vars['_house']->value[$_smarty_tpl->tpl_vars['info_name']->value[$_smarty_tpl->tpl_vars['i']->value]];?>
</span></div>
                  <?php if ($_smarty_tpl->tpl_vars['info']->value[$_smarty_tpl->tpl_vars['i']->value+1]) {?>
                  <div><span class="left_title"><?php echo $_smarty_tpl->tpl_vars['info']->value[$_smarty_tpl->tpl_vars['i']->value+1];?>
</span><span class="right_title"><?php echo $_smarty_tpl->tpl_vars['_house']->value[$_smarty_tpl->tpl_vars['info_name']->value[$_smarty_tpl->tpl_vars['i']->value+1]];?>
</span></div></li>
                  <?php }?>
               <?php }
}
?>

             </ul>

             <!-- <ul class="list_wrap house_pattern">

             </ul> -->

             <!-- <ul class="list_wrap">
                 <li class="list_full">
                     <div class="title">權狀坪數</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['ping'];?>
坪</div>
                 </li>
                 <li class="list_full">
                     <div class="title">室內坪數</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['indoor_ping'];?>
坪</div>
                 </li>
             </ul> -->
             <ul class="list_wrap">
                 <!-- <li class="list_full">
                     <div class="title">更新時間</div>
                     <div class="info"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['update_date'];?>
</div>
                 </li> -->





             </ul>
<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['user_html'];?>

             <!-- 彈窗 - strat -->
             <div class="modal_btn text-center">
                 <div class="btn_wrap">
                     <a class="" href="#modal-container1" role="button" data-toggle="modal">
                         留言
                     </a>
                     <a class="" href="#modal-container2" role="button" data-toggle="modal">
                         預約賞屋
                     </a>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['consultant']) {?>
                        <a class="" href="#modal-container3" role="button" data-toggle="modal">
                            呼叫顧問
                        </a>
                    <?php }?>
                 </div>
                 <div class="clear"></div>
             </div>
         </div>
     </div>
     <!-- 右側資訊欄 - end-->

     <div class="clear"></div>
     <div class="m_b_30"></div>
     <!-- 底部滿版區塊 -->
     <div class="full_block ">
         <div class="clear"></div>
         <!--<hr>-->



         <div class="house_search"><div id="content" class="data_wrap_bot">
         <h3 class="caption"><span>其它人也看了下列週邊物件推薦</span></h3></div></div>
         <section class="commodity_list">

         <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_5_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_5_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_local_item;
}
} else {
}
if ($__foreach_v_5_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_item;
}
if ($__foreach_v_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_5_saved_key;
}
?>
         </section>
     </div>
 </div>

 <!-- 彈窗 - start -->
 <!-- 留言 -->
 <div class="modal fade" id="modal-container1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>
                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
">
                 </div>
             </div>
             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name" name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_1" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>
                                         <div class="radio">
                                             <input  type="radio" name="sex_1" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel"  type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_1" type="radio" value="0">早上
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="1">下午
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>
                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <input  name="verification" type="text" value=""  placeholder="已登入則免驗證"><button type="button" onclick="MessageVerification('1')">進行驗證</button>
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
,'1');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>
 <!-- 預約賞屋 -->

 <div class="modal fade" id="modal-container2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>

                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
">
                 </div>
             </div>

             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_2" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>
                                         <div class="radio">
                                             <input  type="radio" name="sex_2" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel" type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_2" type="radio" value="0">早上
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="1">下午
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="order_time_wrap">
                                 <div class="caption">預約賞屋時間 | <span>(限一週內)</span></div>
                                 <div class="radio_wrap">
                                     <input class="form-control" name="year" type="text" placeholder="年">
                                     <input class="form-control" name="mom" type="text" placeholder="月">
                                     <input class="form-control" name="day" type="text" placeholder="日">
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="0" checked>上午
                                     </div>

                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="1">下午
                                     </div>
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="2">晚上
                                     </div>
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="3">皆可
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>

                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
,'2');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>

 <!-- 呼叫樂租顧問 -->

 <div class="modal fade" id="modal-container3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>
                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
">
                 </div>
             </div>

             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_3" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>

                                         <div class="radio">
                                             <input  type="radio" name="sex_3" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel"　type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_3" type="radio" value="0">早上
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="1">下午
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>
                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
,'3');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>
 <!-- 彈窗 - end -->

 <?php echo '<script'; ?>
>
     $(document).ready(function () {
         $('.nav .dropdown').hover(function () {
             $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(200);
         }, function () {
             $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(200);
         });
     });
     $($sticky).css("width","30%");
     stickywidth=$($sticky).css("width");

     var $sticky = $('#element');
     $sticky.hcSticky({
         stickTo: '#content',
         stickyClass : 'content_class'

     });
 <?php echo '</script'; ?>
>
<?php }
}
