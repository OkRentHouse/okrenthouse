<?php
/* Smarty version 3.1.28, created on 2020-12-17 14:07:49
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Forgot/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fdaf5b59b5660_48274230',
  'file_dependency' => 
  array (
    '32a41276b6c538d188b25ebed809b5c80c3ce1cd' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Forgot/content.tpl',
      1 => 1607678602,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fdaf5b59b5660_48274230 ($_smarty_tpl) {
?>
<div class="bg">
	<div class="home_web_register">
		<form action="https://www.okrent.house/Register" method="post"
			  enctype="application/x-www-form-urlencoded" class="form_wrap">
			<div class="row list">
				<div class="col-md-3">
				</div>
				<div class="col-md-6">
					<div class="floating">
						<i class="fas fa-mobile-alt"></i>
						<input id="user" data-role="none" class="floating_input" name="user" type="text"
							   placeholder="手機號碼" value="<?php echo $_POST['user'];?>
" minlength="6" maxlength="20" required >
					</div>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			<div class="row list">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<div class="floating text-center" id="tel_submit">
						<button type="button" id="forgot_pwd" name="forgot_pwd" class="btn ">發送密碼</button>
					</div>
				</div>
				<div class="col-md-3">
					<div class="floating text-center">
						<button type="button" onclick="location.href='/login'" class="btn ">返回登入</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<?php }
}
