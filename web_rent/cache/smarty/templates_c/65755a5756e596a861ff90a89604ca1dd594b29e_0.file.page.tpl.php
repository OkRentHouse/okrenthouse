<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:26:54
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/GeelyForRent/page.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5a9ee9ff1c3_21568834',
  'file_dependency' => 
  array (
    '65755a5756e596a861ff90a89604ca1dd594b29e' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/GeelyForRent/page.tpl',
      1 => 1581607935,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../geely_for_rent/rent_type.tpl' => 1,
  ),
),false)) {
function content_5fc5a9ee9ff1c3_21568834 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../geely_for_rent/rent_type.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="margin_box">
	<h1><?php echo $_smarty_tpl->tpl_vars['geely_rent']->value[0]['title'];?>
</h1>
	<div class="information">
		<div><div>發佈時間</div><div class="time"><?php echo $_smarty_tpl->tpl_vars['geely_rent']->value[0]['date'];?>
</div></div>
		<div><div>類别</div><div class="type"><?php echo $_smarty_tpl->tpl_vars['geely_rent']->value[0]['type'];?>
</div></div>
		<div><div>瀏覧次數</div><div class="views"><?php echo number_format($_smarty_tpl->tpl_vars['geely_rent']->value[0]['views'],0,'.',',');?>
</div></div>
	</div>
	<div class="geely_rent">
        <?php echo $_smarty_tpl->tpl_vars['geely_rent']->value[0]['content'];?>

        <?php if (!empty($_smarty_tpl->tpl_vars['geely_rent']->value[0]['author'])) {?>

		<?php }?>
	</div>
    <?php if ($_smarty_tpl->tpl_vars['geely_rent']->value[0]['author']) {?>
		<div class="information no_border_b">
			<div><div>作者</div><div class="author"><?php echo $_smarty_tpl->tpl_vars['geely_rent']->value[0]['author'];?>
</div><?php if ($_smarty_tpl->tpl_vars['geely_rent']->value[0]['identity']) {?> (<?php echo $_smarty_tpl->tpl_vars['geely_rent']->value[0]['identity'];?>
)<?php }?></div>
		</div>
    <?php }?>
</div><?php }
}
