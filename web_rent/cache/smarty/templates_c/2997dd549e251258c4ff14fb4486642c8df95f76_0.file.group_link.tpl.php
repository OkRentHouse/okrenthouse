<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:16:53
  from "/home/ilifehou/life-house.com.tw/themes/Rent/group_link.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5a795f359f7_14012924',
  'file_dependency' => 
  array (
    '2997dd549e251258c4ff14fb4486642c8df95f76' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/group_link.tpl',
      1 => 1606714690,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fc5a795f359f7_14012924 ($_smarty_tpl) {
?>
<section class="slider_center">
    <div>
        <a href="https://www.lifegroup.house/"><img alt="生活集團" src="/themes/Rent/img/index/logo-b-01.png"></a>
    </div>
    <div>
        <a href="https://www.okdeal.house/"><img alt="生活房屋" src="/themes/Rent/img/index/logo-b-02.png"></a>
    </div>
    <div>
        <a href="https://www.okmaster.life/ "><img alt="生活好科技" src="/themes/Rent/img/index/logo-b-12.png "></a>
    </div>
    <div>
        <a href="/Store?"><img alt="生活好康" src="/themes/Rent/img/index/logo-b-04.png "></a>
    </div>
    <div>
        <a href=""><img src="/themes/Rent/img/index/logo-b-05.png "></a>
    </div>
    <div>
        <a href="https://www.lifegroup.house/LifeGo"><img alt="生活樂購" src="/themes/Rent/img/index/logo-b-06.png "></a>
    </div>
    <div>
        <a href=" https://www.okpt.life"><img alt="好幫手" src="/themes/Rent/img/index/logo-b-07.png "></a>
    </div>
    <div>
        <a href=" https://www.okrent.tw"><img alt="共享圈" src="/themes/Rent/img/index/logo-b-08.png "></a>
    </div>
    <div>
        <a href="https://www.epro.house  "> <img alt="裝修達人" src="/themes/Rent/img/index/logo-b-09.png "></a>
    </div>
</section>

<?php echo '<script'; ?>
>
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 9,
        slidesToScroll: 9
    });
<?php echo '</script'; ?>
>
<?php }
}
