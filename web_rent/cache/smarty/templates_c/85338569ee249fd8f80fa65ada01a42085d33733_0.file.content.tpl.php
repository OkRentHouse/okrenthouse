<?php
/* Smarty version 3.1.28, created on 2021-04-28 17:34:09
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60892c1102c659_10669507',
  'file_dependency' => 
  array (
    '85338569ee249fd8f80fa65ada01a42085d33733' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Index/content.tpl',
      1 => 1619602445,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../house/search_house7_index.tpl' => 1,
    'file:../../house/house_item_list2021Apr.tpl' => 1,
    'file:../../house/house_footer_list.tpl' => 1,
    'file:../../house/house_item_list.tpl' => 9,
    'file:../../footer_0428.tpl' => 1,
  ),
),false)) {
function content_60892c1102c659_10669507 ($_smarty_tpl) {
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/search_house7_index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="line-height-90">
</div>

	<div class="banner_1223">
		<!-- <svg class="_18">
			<ellipse id="_18" rx="30vw" ry="30vw" cx="30vw" cy="30vw">
			</ellipse>
		</svg> -->
		<img src="\themes\Rent\img\index\0401\banner.jpg">
	</div>

<div>
	<?php $_smarty_tpl->tpl_vars['icons'] = new Smarty_Variable(array(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'icons', 0);?>
	<ul>
		<li>

		</li>
	</ul>
</div>
<div class="main">
<div class="right_icons top_R">

		<img class="icons_history" src="\themes\Rent\img\index\0401\history.svg">
		<img class="icons_favorite" src="\themes\Rent\img\index\0401\favorite.svg">
		<img class="icons_download" src="\themes\Rent\img\index\0401\download.svg">
		<img class="icons_massage" src="\themes\Rent\img\index\0401\massage.svg">
		<img class="icons_focus" src="\themes\Rent\img\index\0401\focus.svg">
		<img class="icons_share share" src="\themes\Rent\img\index\0401\share.svg">
		<img class="icons_top" src="\themes\Rent\img\index\0401\top.svg">

</div>
	<div class="relative" style="    height: 100px;margin-top: 5em;">
		<div id="_bm">
			<span>精選主題房源</span>
		</div>
		<div id="_bm0414">
			<span>特色房源</span>
		</div>
		<div class="relative" style="">
			<?php $_smarty_tpl->tpl_vars['img_12_banner1_053610630'] = new Smarty_Variable(array("精選推薦","老幼安居","寵物友善","津貼補助","店面","辦公室","廠房"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'img_12_banner1_053610630', 0);?>
			<div id="img_12_banner1_053610630">
				<header class="minbar2">
		      <nav class="main_menu2 not_select">
		      <div class="content_width">
		        <nav class="menu">
							<?php
$_from = $_smarty_tpl->tpl_vars['img_12_banner1_053610630']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
							<a href="#" id="data_list_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="<?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?> active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</a><?php if (sizeof($_smarty_tpl->tpl_vars['img_12_banner1_053610630']->value) != $_smarty_tpl->tpl_vars['i']->value+1) {
}?>
							<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>
		        </nav>
		      </div>
		      </nav>
		    </header>
			</div>
		</div>
	</div>

	<?php
$_from = $_smarty_tpl->tpl_vars['img_12_banner1_053610630']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
	<div class="relative data_list_<?php echo $_smarty_tpl->tpl_vars['i']->value;
if ($_smarty_tpl->tpl_vars['i']->value != 0) {?> hidden<?php }?>" style="margin-top: 0em;overflow: hidden;">
		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_0414']->value[$_smarty_tpl->tpl_vars['i']->value])) {?>
		<div class="house_row">
			<div class="commodity_list commodity_data commodity_data_0414[$i]" style="display: inline-block;">
                <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_0414']->value[$_smarty_tpl->tpl_vars['i']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list2021Apr.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
} else {
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_2_saved_key;
}
?>
			</div>
		</div>
    <?php }?>
	</div>
	<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_1_saved_key;
}
?>
	</div>
	<div class="banner_1223 banner2 relative" style="/* height: 762px; */margin-top: 5em;">

			<img src="\themes\Rent\img\index\0401\banner2@2x.png" style="    width: 100%;">

	</div>
	<div class="main">
	<div class="relative" style="margin-top: 8em !important;">

		<img src="\themes\Rent\img\index\0401\banner3_0424.svg" style="    width: 100%;">
	</div>

	<div id="_ma" style="margin: 0em;margin-top: -4rem;">
			<span>租事順利</span>
	</div>

<div class="relative footer_img hidden" style="    margin-top: 5em;">
	<?php $_smarty_tpl->tpl_vars['footer_img'] = new Smarty_Variable(array("footer1.svg","footer2.svg","footer3.svg","footer4.jpg","footer5.svg","footer6.svg","footer7.svg","footer8.svg","footer9.svg"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footer_img', 0);?>
	<?php
$_from = $_smarty_tpl->tpl_vars['footer_img']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_3_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_3_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
	<img src="/img/<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
">
	<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_local_item;
}
}
if ($__foreach_v_3_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_item;
}
if ($__foreach_v_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_3_saved_key;
}
?>
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_footer_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</div>
<div class="b_padding title_banner hidden">
	<!-- <h2>吉屋快搜</h2> -->

    <?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>精選推薦</h2>
			</div>
			<div class="commodity_list commodity_data">
                <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_4_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_4_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_4_saved_local_item;
}
} else {
}
if ($__foreach_v_4_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_4_saved_item;
}
if ($__foreach_v_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_4_saved_key;
}
?>
			</div>
		</div>
    <?php }?>

		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_a']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>溫馨居家〜2-3房</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_a']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_5_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_5_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_local_item;
}
} else {
}
if ($__foreach_v_5_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_item;
}
if ($__foreach_v_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_5_saved_key;
}
?>
			</div>
		</div>
		<?php }?>

		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_b']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>單身美學〜套房</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_b']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_6_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_6_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_6_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_6_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_6_saved_local_item;
}
} else {
}
if ($__foreach_v_6_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_6_saved_item;
}
if ($__foreach_v_6_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_6_saved_key;
}
?>
			</div>
		</div>
		<?php }?>

		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_c']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>大器美宅〜4+房</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_c']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_7_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_7_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_7_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_7_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_7_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_7_saved_local_item;
}
} else {
}
if ($__foreach_v_7_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_7_saved_item;
}
if ($__foreach_v_7_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_7_saved_key;
}
?>
			</div>
		</div>
		<?php }?>

		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_d']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>獨享天地〜透天/別墅</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_d']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_8_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_8_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_8_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_8_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_8_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_8_saved_local_item;
}
} else {
}
if ($__foreach_v_8_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_8_saved_item;
}
if ($__foreach_v_8_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_8_saved_key;
}
?>
			</div>
		</div>
		<?php }?>

		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_e']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>店面店辦</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_e']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_9_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_9_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_9_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_9_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_9_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_9_saved_local_item;
}
} else {
}
if ($__foreach_v_9_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_9_saved_item;
}
if ($__foreach_v_9_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_9_saved_key;
}
?>
			</div>
		</div>
		<?php }?>

		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_e_1']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>店辦</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_e_1']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_10_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_10_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_10_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_10_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_10_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_10_saved_local_item;
}
} else {
}
if ($__foreach_v_10_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_10_saved_item;
}
if ($__foreach_v_10_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_10_saved_key;
}
?>
			</div>
		</div>
		<?php }?>


		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_g']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>辦公室</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_g']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_11_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_11_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_11_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_11_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_11_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_11_saved_local_item;
}
} else {
}
if ($__foreach_v_11_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_11_saved_item;
}
if ($__foreach_v_11_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_11_saved_key;
}
?>
			</div>
		</div>
		<?php }?>

		<?php if (count($_smarty_tpl->tpl_vars['commodity_data_f']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>廠房倉庫</h2>
			</div>
			<div class="commodity_list commodity_data">
								<?php
$_from = $_smarty_tpl->tpl_vars['commodity_data_f']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_12_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_12_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_12_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_12_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_12_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_12_saved_local_item;
}
} else {
}
if ($__foreach_v_12_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_12_saved_item;
}
if ($__foreach_v_12_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_12_saved_key;
}
?>
			</div>
		</div>
		<?php }?>





</div>


<img class="hidden" src="/themes/Rent/img/index/ad_banner_1.jpg" style="width: 100%;">
<div class="ad hidden">

	<img src="/themes/Rent/img/index/ad3 (4).JPG">
	<img src="/themes/Rent/img/index/ad3 (1).JPG">
	<img src="/themes/Rent/img/index/ad3 (3).JPG">

</div>
<img class="hidden" src="/themes/Rent/img/index/ad_banner_2.jpg" style="width: 100%;">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../footer_0428.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php }
}
