<?php
/* Smarty version 3.1.28, created on 2021-01-04 03:29:11
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/search_house5_index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5ff21b073cc935_62973969',
  'file_dependency' => 
  array (
    'b851c3374e27b85443ce519311a8c318dd6c3c16' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/search_house5_index.tpl',
      1 => 1609701873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ff21b073cc935_62973969 ($_smarty_tpl) {
?>
<form action="https://okrent.house/list" method="get" id="search_box_big">

  <div class="logo_main col-sm-3"><img src="/themes/Rent/img/logo_main.svg"><a href="/" class="index"></a></div>
    <input type="hidden" name="transfer_in" value="1">
    <div class="search_list_wrap col-sm-6">
        <div class="search_div2">

            <div class="city_select">
                <spam class="county_city search search_1">
                    區域
                </spam>
            </div><!--<img class="county_city search search_1 class_select_img class_select_img_2" src="themes/Rent/img/select_D.png">-->
            <i class="county_city search search_1 class_select_img class_select_img_2 fas fa-angle-down"></i>
            <div class="county_div" style="display: none;">
                <?php
$_from = $_smarty_tpl->tpl_vars['select_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_county_0_saved_item = isset($_smarty_tpl->tpl_vars['county']) ? $_smarty_tpl->tpl_vars['county'] : false;
$__foreach_county_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['county'] = new Smarty_Variable();
$__foreach_county_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_county_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['county']->value) {
$__foreach_county_0_saved_local_item = $_smarty_tpl->tpl_vars['county'];
?><spam class="county"><input type="radio" name="county" id="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['county']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['county']->value['value'] == $_GET['county']) {?>checked<?php }?>><label for="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['county']->value['title'];?>
</label></spam><?php
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_local_item;
}
}
if ($__foreach_county_0_saved_item) {
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_item;
}
if ($__foreach_county_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_county_0_saved_key;
}
?>
            </div>
                <div class="city_div"><?php
$_from = $_smarty_tpl->tpl_vars['arr_city']->value[$_GET['county']];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_city_1_saved_item = isset($_smarty_tpl->tpl_vars['city']) ? $_smarty_tpl->tpl_vars['city'] : false;
$__foreach_city_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['city'] = new Smarty_Variable();
$__foreach_city_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_city_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['city']->value) {
$__foreach_city_1_saved_local_item = $_smarty_tpl->tpl_vars['city'];
?><spam class="city"><input type="checkbox" name="city[]" id="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['city']->value;?>
" <?php if (in_array($_smarty_tpl->tpl_vars['city']->value,$_GET['city'])) {?>checked<?php }?>><label for="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['city']->value;?>
</label></spam><?php
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_local_item;
}
}
if ($__foreach_city_1_saved_item) {
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_item;
}
if ($__foreach_city_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_city_1_saved_key;
}
?></div>
        </div>

        <div class="search_div2">
            <div class="type_select search_1" id="main_house_class_all_title">
                住宅
            </div><!--<img class="type_select class_select_img class_select_img_2" src="themes/Rent/img/select_D.png">--><i class="type_select class_select_img class_select_img_2 fas fa-angle-down"></i>
            <div class="patterns none">
                <input type="radio" id="id_main_house_class_all" value=""<?php if (count($_GET['id_main_house_class']) == 0) {?> checked<?php }?>>
                <label for="id_main_house_class_all" id="id_main_house_class_name">全部</label>
                <?php
$_from = $_smarty_tpl->tpl_vars['main_house_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                    <input type="checkbox" name="id_main_house_class[]" id="id_main_house_class_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
"
                           <?php if (in_array($_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'],$_GET['id_main_house_class'])) {?>checked<?php }?>>
                            <label for="id_main_house_class_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
" id="main_house_class_name_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
"><?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</label>
                <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_2_saved_key;
}
?>
            </div>
        </div>

        <div class="search_div2">
            <div class="class_select" id="types_all_name">
                電梯大樓
            </div><!--<img class="class_select class_select_img class_select_img_2" src="themes/Rent/img/select_D.png" >--><i class="class_select class_select_img class_select_img_2 fas fa-angle-down"></i>
            <div class="category none">
                <input type="radio" id="id_types_all" value=""<?php if (count($_GET['id_types']) == 0) {?> checked<?php }?>>
                <label for="id_types_all" id="id_types">全部</label>
                <?php
$_from = $_smarty_tpl->tpl_vars['select_types']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_3_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_3_saved_local_item = $_smarty_tpl->tpl_vars['types'];
?>
                    <input type="checkbox" name="id_types[]" id="id_types_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
"
                           <?php if (in_array($_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'],$_GET['id_types'])) {?>checked<?php }?>>
                            <label for="id_types_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
" id="types_name_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
"><?php echo $_smarty_tpl->tpl_vars['types']->value['title'];?>
</label>
                <?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_local_item;
}
}
if ($__foreach_types_3_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_item;
}
if ($__foreach_types_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_types_3_saved_key;
}
?>
            </div>
        </div>

        <div class="search_div2">
            <div class="houses_select" id="type_all_name">
                整層住家
            </div><!--<img class="houses_select class_select_img class_select_img_2" src="themes/Rent/img/select_D.png">--><i class="houses_select class_select_img class_select_img_2 fas fa-angle-down"></i>

            <div class="houses none">
                <input type="radio" id="type_all" value=""<?php if (count($_GET['id_type']) == 0) {?> checked<?php }?>><label for="type_all" id="type_name">全部</label><?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_4_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_4_saved_local_item = $_smarty_tpl->tpl_vars['type'];
?><input type="checkbox" name="id_type[]" id="id_type_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'],$_GET['id_type'])) {?>checked<?php }?>><label for="id_type_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" id="type_name_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
"><?php echo $_smarty_tpl->tpl_vars['type']->value['title'];?>
</label><?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_local_item;
}
}
if ($__foreach_type_4_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_item;
}
if ($__foreach_type_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_type_4_saved_key;
}
?>
            </div>
        </div>
        <div class="search_input"><input type="text" class="field w50p2" name="search" id="search"
                                         value="" placeholder="">
        </div>





        <button class="search_icon" type="submit"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/pic_m.svg"></button>
        <!-- <button class="search_icon search_icon_map" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/map_m.svg"></a></button> -->
    </div>

    <header class="minbar2 col-sm-3">
                <nav class="main_menu2 not_select">

      <div class="content_width">



        <nav class="menu">
          <div id="menu_right_2" class="menu_right_2 menu_right_no_top menu_right_2_no_top" style="cursor: pointer; height: 70%; z-index: 1; width: 60px; left: 90% !important; display: none;"> <img src="/themes/Rent/img/rows.svg" style="height: 100%;">
            <div id="menu_right_2_div" style="display: none;" class="dropdown-menu"> <a href="https://www.okrent.house/#">刊登廣告</a><a href="https://www.okrent.house/Register">註冊</a><a href="https://www.okrent.house/login">登入</a><a class="no_border" href="https://www.okrent.house/#">加盟店</a> </div>
          </div>

          <div class="menu_right" style="display: block;">
            <a class="icon_block share_txt not_select"><label class="share_lab not_select">分享樂租</label><svg class="svg-inline--fa fa-share-alt fa-w-14 not_select" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="share-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M352 320c-22.608 0-43.387 7.819-59.79 20.895l-102.486-64.054a96.551 96.551 0 0 0 0-41.683l102.486-64.054C308.613 184.181 329.392 192 352 192c53.019 0 96-42.981 96-96S405.019 0 352 0s-96 42.981-96 96c0 7.158.79 14.13 2.276 20.841L155.79 180.895C139.387 167.819 118.608 160 96 160c-53.019 0-96 42.981-96 96s42.981 96 96 96c22.608 0 43.387-7.819 59.79-20.895l102.486 64.054A96.301 96.301 0 0 0 256 416c0 53.019 42.981 96 96 96s96-42.981 96-96-42.981-96-96-96z"></path></svg><!-- <i class="fas fa-share-alt"></i> Font Awesome fontawesome.com --></a>
            <a href="https://www.okrent.house/#"><span class="free" style="position: absolute;font: small-caption;color: red;text-align: start;left: 0px;">免費</span>刊登廣告</a>
            <a href="https://www.okrent.house/Register">註冊</a>
            <a href="https://www.okrent.house/login">登入</a>
            <a class="no_border" href="https://www.okrent.house/#">加盟店</a>
            <a class="no_border not_select" href="#">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="hb_line" class="hb_line not_select" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve">
<g>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,19.312h104.659c2.536,0,4.612,2.074,4.612,4.84l0,0   c0,2.767-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.075-4.61-4.842l0,0C1.56,21.386,3.634,19.312,6.17,19.312z"/>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,87.777h104.659c2.536,0,4.612,2.306,4.612,4.842l0,0   c0,2.766-2.076,5.071-4.612,5.071H6.17c-2.536,0-4.61-2.306-4.61-5.071l0,0C1.56,90.083,3.634,87.777,6.17,87.777z"/>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,53.428h104.659c2.536,0,4.612,2.306,4.612,5.072l0,0   c0,2.536-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.306-4.61-4.842l0,0C1.56,55.734,3.634,53.428,6.17,53.428z"/>
</g>
<g>
	<defs>
		<path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"/>
	</defs>
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_51_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"/>
	</defs>
	<clipPath id="SVGID_4_">
		<use xlink:href="#SVGID_81_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"/>
	</defs>
	<clipPath id="SVGID_6_">
		<use xlink:href="#SVGID_105_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"/>
	</defs>
	<clipPath id="SVGID_8_">
		<use xlink:href="#SVGID_129_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"/>
	</defs>
	<clipPath id="SVGID_10_">
		<use xlink:href="#SVGID_155_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"/>
	</defs>
	<clipPath id="SVGID_12_">
		<use xlink:href="#SVGID_183_" overflow="visible"/>
	</clipPath>
</g>
<image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
</image>
</svg></a>


          </div>

          </nav>

      </div>

    </nav>

    		</header>

</form>
<div class="col-sm-12 row3">

  <header class="minbar_3">
                <!-- <nav class="main_menu">

      <ul class="menu_left"><li><a href="/list" class="">吉屋快搜</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/list" class="">條件搜尋</a></li><li><a href="/HouseMap" class="">地圖搜尋</a></li></ul></li><li><a href="/tenant" class="">樂租服務</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/tenant" class="">租客</a></li><li><a href="/landlord" class="">房東</a></li></ul></li><li><a href="/tax_benefit" class="">租事順利</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/tax_benefit" class="">租事順利</a></li><li><a href="/authenticate_main" class="">信用認證</a></li><li><a href="/credit_card_main" class="">履約保證</a></li><li><a href="/rent_insurance_main" class="">安心保險</a></li><li><a href="/deposit_storage_main" class="">租管服務</a></li><li><a href="/smart_lock" class="">智能門鎖</a></li><li><a href="/house_check_main" class="">健康好宅</a></li><li><a href="/fix_adept_main" class="">裝修達人</a></li><li><a href="/agents_misson_main" class="">好幫手</a></li><li><a href="/rent_lend_main" class="">共享圈</a></li></ul></li><li><a href="/geely_for_rent" class="">新聞資訊</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/geely_for_rent?id_type=1" class="">資訊</a></li><li><a href="/geely_for_rent?id_type=3" class="">新聞</a></li><li><a href="/geely_for_rent?id_type=4" class="">政策法令</a></li><li><a href="/geely_for_rent?id_type=2" class="">租法觀點</a></li><li><a href="/geely_for_rent?id_type=5" class="">租賃稅費</a></li><li><a href="/geely_for_rent?id_type=6" class="">趨勢</a></li><li><a href="/geely_for_rent?id_type=7" class="">爆料租舍</a></li></ul></li><li><a href="/in_life_index" class="">in生活</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/in_life" class="">活動分享</a></li><li><a href="/WealthSharing" class="">創富分享</a></li><li><a href="/Store" class="">生活好康</a></li><li><a href="https://www.lifegroup.house/LifeGo" class="">生活樂購</a></li></ul></li><li><a href="/JoinLife" class="">加入生活</a><ul class="dropdown-menu" role="menu"><li><a href="/ExpertAdvisor" class="">生活家族</a></li><li><a href="/Joinrenthouse" class="">菁英招募</a></li><li><a href="#" class="">加盟樂租</a></li></ul></li><li><a href="/about" class="">關於生活</a></li>


        </ul>

    </nav> -->


    <nav class="main_menu">




        <nav class="main_menu">




          <ul class="menu_left"><?php
$_from = $_smarty_tpl->tpl_vars['main_menu']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_5_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_5_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?><li <?php if ($_smarty_tpl->tpl_vars['v']->value['color']) {?> class="nav_active_color" <?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['v']->value['href'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['v']->value['active']) {?>active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
</a><?php if (count($_smarty_tpl->tpl_vars['v']->value['submenu'])) {?><ul class="dropdown-menu" role="menu"><?php
$_from = $_smarty_tpl->tpl_vars['v']->value['submenu'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_w_6_saved_item = isset($_smarty_tpl->tpl_vars['w']) ? $_smarty_tpl->tpl_vars['w'] : false;
$__foreach_w_6_saved_key = isset($_smarty_tpl->tpl_vars['j']) ? $_smarty_tpl->tpl_vars['j'] : false;
$_smarty_tpl->tpl_vars['w'] = new Smarty_Variable();
$__foreach_w_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_w_6_total) {
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['w']->value) {
$__foreach_w_6_saved_local_item = $_smarty_tpl->tpl_vars['w'];
?><li ><a href="<?php echo $_smarty_tpl->tpl_vars['w']->value['href'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['w']->value['active']) {?>active<?php }?> <?php echo $_smarty_tpl->tpl_vars['w']->value['class'];?>
"><?php echo $_smarty_tpl->tpl_vars['w']->value['text'];?>
</a></li><?php
$_smarty_tpl->tpl_vars['w'] = $__foreach_w_6_saved_local_item;
}
}
if ($__foreach_w_6_saved_item) {
$_smarty_tpl->tpl_vars['w'] = $__foreach_w_6_saved_item;
}
if ($__foreach_w_6_saved_key) {
$_smarty_tpl->tpl_vars['j'] = $__foreach_w_6_saved_key;
}
?></ul><?php }?></li><?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_local_item;
}
}
if ($__foreach_v_5_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_item;
}
if ($__foreach_v_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_5_saved_key;
}
?>

            

        </nav>


    </nav>


    		</header>





</div>
<?php echo '<script'; ?>
>
    arr_city = <?php echo json_encode($_smarty_tpl->tpl_vars['arr_city']->value,1);?>
;
    <?php echo $_smarty_tpl->tpl_vars['add_js']->value;?>

<?php echo '</script'; ?>
>
<?php }
}
