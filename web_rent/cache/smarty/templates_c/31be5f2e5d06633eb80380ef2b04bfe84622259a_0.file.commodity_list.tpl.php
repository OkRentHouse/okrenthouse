<?php
/* Smarty version 3.1.28, created on 2020-12-25 20:38:46
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Tenant/commodity_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fe5dd5612f561_67962232',
  'file_dependency' => 
  array (
    '31be5f2e5d06633eb80380ef2b04bfe84622259a' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Tenant/commodity_list.tpl',
      1 => 1608899921,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../house/house_item_list.tpl' => 1,
  ),
),false)) {
function content_5fe5dd5612f561_67962232 ($_smarty_tpl) {
?>
<div class="b_padding title_banner ">
	<!-- <h2>吉屋快搜</h2> -->

    <?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>精選推薦</h2>
			</div>
			<div class="commodity_list commodity_data">
                <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
} else {
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>
			</div>
		</div>
    <?php }?>



</div>
<?php }
}
