<?php
/* Smarty version 3.1.28, created on 2020-12-10 13:11:59
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Store/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd1ae1f602ec0_47651094',
  'file_dependency' => 
  array (
    '412caa69d3293483f5edd5fe1803c0c8fe157b04' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Store/content.tpl',
      1 => 1607577105,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../pagination.tpl' => 1,
  ),
),false)) {
function content_5fd1ae1f602ec0_47651094 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./good_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <div class="special_store_wrap" style="margin: auto;">
        <div class="banner" style="width: 90%;margin: auto;">
            <img class="" src="/themes/Rent/img/store/banner_1210.png">
        </div>

        <div class="roof" style="width: 90%;">

          <div class="b_padding title_banner" style="width: 90%;margin: auto;">
              <form action="#" method="get" id="search_box_big">
                  <div class="search_list_wrap">
                      <div class="search_div">
                          
                          <div class="city_select">
                              <spam class="county_city search">區域<i class="fas fa-chevron-down"></i></spam>
                          </div>
                          <div class="county_div" style="display: none;">
                              <?php
$_from = $_smarty_tpl->tpl_vars['select_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_county_0_saved_item = isset($_smarty_tpl->tpl_vars['county']) ? $_smarty_tpl->tpl_vars['county'] : false;
$__foreach_county_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['county'] = new Smarty_Variable();
$__foreach_county_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_county_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['county']->value) {
$__foreach_county_0_saved_local_item = $_smarty_tpl->tpl_vars['county'];
?><spam class="county"><input type="radio" name="county" id="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['county']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['county']->value['value'] == $_GET['county']) {?>checked<?php }?>><label for="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['county']->value['title'];?>
</label></spam><?php
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_local_item;
}
}
if ($__foreach_county_0_saved_item) {
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_item;
}
if ($__foreach_county_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_county_0_saved_key;
}
?>
                          </div>
                          <div class="city_div" style="display: none;"><?php
$_from = $_smarty_tpl->tpl_vars['arr_city']->value[$_GET['county']];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_city_1_saved_item = isset($_smarty_tpl->tpl_vars['city']) ? $_smarty_tpl->tpl_vars['city'] : false;
$__foreach_city_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['city'] = new Smarty_Variable();
$__foreach_city_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_city_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['city']->value) {
$__foreach_city_1_saved_local_item = $_smarty_tpl->tpl_vars['city'];
?><spam class="city"><input type="checkbox" name="city[]" id="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['city']->value;?>
" <?php if (in_array($_smarty_tpl->tpl_vars['city']->value,$_GET['city'])) {?>checked<?php }?>><label for="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['city']->value;?>
</label></spam><?php
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_local_item;
}
}
if ($__foreach_city_1_saved_item) {
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_item;
}
if ($__foreach_city_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_city_1_saved_key;
}
?></div>
                      </div>

                      <div class="search_div">
                          <div class="class_select" id="id_store_type_name">類別<i class="fas fa-chevron-down"></i></div>
                          <div class="category none">
                              <input type="radio" id="id_store_type_all" value=""<?php if (count($_GET['id_store_type']) == 0) {?> checked<?php }?>><label for="id_store_type_all" id="id_store_type">全部</label><?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_2_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_2_saved_local_item = $_smarty_tpl->tpl_vars['types'];
?><input type="checkbox" name="id_store_type[]" id="id_store_type_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['types']->value['id_store_type'],$_GET['id_store_type'])) {?>checked<?php }?>><label for="id_store_type_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
" id="store_type_name_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
"><?php echo $_smarty_tpl->tpl_vars['types']->value['store_type'];?>
</label><?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_2_saved_local_item;
}
}
if ($__foreach_types_2_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_2_saved_item;
}
if ($__foreach_types_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_types_2_saved_key;
}
?>
                          </div>
                      </div>


                      <div class="search_input"><input type="text" class="field" name="search" id="search" value="<?php echo $_GET['search'];?>
" placeholder="可輸入關鍵字">
                      </div>
  
  
  
                      <button class="search_icon" type="submit"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/pic_m.svg"></button>
                      <button class="search_icon" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/map_m.svg"></a></button>
                  </div>
              </form>
              <?php echo '<script'; ?>
>
                  arr_city = <?php echo json_encode($_smarty_tpl->tpl_vars['arr_city']->value,1);?>
;
                  <?php echo $_smarty_tpl->tpl_vars['js']->value;?>

              <?php echo '</script'; ?>
>

          </div>

            <img src="/themes/Rent/img/store/roof.jpg" alt="" class="">
        </div>
        <div class="store_list_wrap" style="width: 90%;margin: auto;">
            <ul class="">
                <?php
$_from = $_smarty_tpl->tpl_vars['arr_store']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_store_3_saved_item = isset($_smarty_tpl->tpl_vars['store']) ? $_smarty_tpl->tpl_vars['store'] : false;
$__foreach_store_3_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['store'] = new Smarty_Variable();
$__foreach_store_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_store_3_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['store']->value) {
$__foreach_store_3_saved_local_item = $_smarty_tpl->tpl_vars['store'];
?>
                    <li class="">
                        <div class="image_frame">
                            <div class="img"><a href="/StoreIntroduction?id=<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><img src="<?php if (!empty($_smarty_tpl->tpl_vars['store']->value['img'])) {
echo $_smarty_tpl->tpl_vars['store']->value['img'];
} else {
echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/default.jpg<?php }?>"></a><div class="favorite <?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?>on<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
" data-type="store"><?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg"><?php }?></div></div>
                        </div>
                        <div class="content_wrap">
                            <div class="caption"><?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
<i></i><span class=""><?php echo $_smarty_tpl->tpl_vars['storestore_type']->value;?>
</span></div>
                            <div class="list">
                                <div class="hot">Hot</div>
                                <i></i>
                                <div class="hot_list"><?php
$_from = $_smarty_tpl->tpl_vars['store']->value['service'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_service_4_saved_item = isset($_smarty_tpl->tpl_vars['service']) ? $_smarty_tpl->tpl_vars['service'] : false;
$__foreach_service_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['service'] = new Smarty_Variable();
$__foreach_service_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_service_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['service']->value) {
$__foreach_service_4_saved_local_item = $_smarty_tpl->tpl_vars['service'];
?><samp><?php echo $_smarty_tpl->tpl_vars['service']->value;?>
</samp><?php
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_4_saved_local_item;
}
}
if ($__foreach_service_4_saved_item) {
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_4_saved_item;
}
if ($__foreach_service_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_service_4_saved_key;
}
?></div>
                            </div>
                            <div class="list">
                                <div class="offer">Offer</div>
                                <i></i>
                                <?php
$_from = $_smarty_tpl->tpl_vars['store']->value['discount'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_discount_5_saved_item = isset($_smarty_tpl->tpl_vars['discount']) ? $_smarty_tpl->tpl_vars['discount'] : false;
$__foreach_discount_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['discount'] = new Smarty_Variable();
$__foreach_discount_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_discount_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['discount']->value) {
$__foreach_discount_5_saved_local_item = $_smarty_tpl->tpl_vars['discount'];
?><a href="/StoreIntroduction?id=<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</a><?php
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_5_saved_local_item;
}
}
if ($__foreach_discount_5_saved_item) {
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_5_saved_item;
}
if ($__foreach_discount_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_discount_5_saved_key;
}
?>
                            </div>
                        </div>
                    </li>
                <?php
$_smarty_tpl->tpl_vars['store'] = $__foreach_store_3_saved_local_item;
}
} else {
?>
                <li class="">
                    查不到相關店家
                </li>
                <?php
}
if ($__foreach_store_3_saved_item) {
$_smarty_tpl->tpl_vars['store'] = $__foreach_store_3_saved_item;
}
if ($__foreach_store_3_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_store_3_saved_key;
}
?>
            </ul>
        </div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
<?php }
}
