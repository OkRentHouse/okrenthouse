<?php
/* Smarty version 3.1.28, created on 2021-01-25 13:55:56
  from "/opt/lampp/htdocs/life-house.com.tw/themes/1786/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_600e5d6ccac108_55424312',
  'file_dependency' => 
  array (
    '613ca6e8e70a95c9fb78c384aa4ab108bf054a32' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/1786/controllers/Index/content.tpl',
      1 => 1611554153,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:house_item_list.tpl' => 1,
    'file:house_salesitem_list.tpl' => 1,
  ),
),false)) {
function content_600e5d6ccac108_55424312 ($_smarty_tpl) {
?>
<div class="bg">
</div>
<div class="introduction">
    <div class="col-sm-6 l">
      <ul>
      <li class="nostyle"><label class="H_light">租客</label> 省時省力</li>
      <li>房源眾多 找屋快速</li>
      <li>以條件搜尋屋源 找屋好輕鬆</li>
      <li>需求登錄留言 系統自動配對回報</li>
      <li>雲總機保護您 免除干擾或被迫接收</li>
      <li class="nostyle">推銷訊息而疲勞轟炸</li>
      </ul>
    </div>
    <div class="col-sm-6 r">
      <ul>
      <li class="nostyle"><label class="H_light">房東</label> 省心樂租</li>
      <li>客源超廣 出租快速</li>
      <li>不止刊登還主動找租客</li>
      <li>系統自動媒合租客。主動出擊</li>
      <li>預約看屋及租客提問 能掌握市況反應</li>
      <li>雲總機保護您 免除干擾或被迫接收</li>
      <li class="nostyle">推銷訊息而疲勞轟炸</li>
      </ul>
    </div>
</div>
<div class="bg" style="height: 400px;"></div>
<div class="text_box">
  <label class="imitate_btn">樂租推薦</label>
</div>
<div class="type_class">
<label class="b_right_line"><a class="active">我要租屋</a></label>
<label class="b_right_line"><a>補助津貼</a></label>
<label class="b_right_line"><a>老幼安居</a></label>
<label class="b_right_line"><a>寵物友善</a></label>
<label class="b_right_line"><a>社會住宅</a></label>
<label class="b_right_line"><a>短期季租</a></label>
<label class="b_right_line"><a>共享商辦</a></label>
<label><a>盤出頂讓</a></label>
</div>

<?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
<div class="A1786_row">
  <div class="commodity_list commodity_data">
            <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
} else {
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>
  </div>
</div>
<?php }?>
<div class="text_box">
  <label class="imitate_btn">好買賣精選</label>
</div>
<div class="type_class">
  <label class="b_right_line"><a class="active">我要買屋</a></label>
  <label class="b_right_line"><a>電梯華廈</a></label>
  <label class="b_right_line"><a>透天別墅</a></label>
  <label class="b_right_line"><a>店 面</a></label>
  <label class="b_right_line"><a>廠 房</a></label>
  <label class="b_right_line"><a>商 辦</a></label>
  <label class="b_right_line"><a>土 地</a></label>
  <label><a>新建案</a></label>
</div>
<?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
<div class="A1786_row">
  <div class="commodity_list commodity_data">
            <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:house_salesitem_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
} else {
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_1_saved_key;
}
?>
  </div>
</div>
<?php }?>
<img src="/themes/Rent/img/1786/1100120-3.JPG">
<img src="/themes/Rent/img/1786/1100120-4.JPG">
<img src="/themes/Rent/img/1786/1100120-5.JPG">
<img src="/themes/Rent/img/1786/1100120-6.JPG">
<div class="link_group">
  <ul>
    <li><img src="/themes/Rent/img/1786/footer_icon1.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon2.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon3.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon4.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon5.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon6.png"></li>
  </ul>
</div>
<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php echo $_smarty_tpl->tpl_vars['js1786bg']->value;?>

<?php }
}
