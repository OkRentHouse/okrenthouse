<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:16:57
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Register/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5a79981ea99_74150128',
  'file_dependency' => 
  array (
    '10d92bb9477ec2ca7d7cad5a177c80d05d4f7c3e' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Register/content.tpl',
      1 => 1604540883,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fc5a79981ea99_74150128 ($_smarty_tpl) {
?>
<div class="bg">
    <div class="home_web_register">
        <form action="https://www.okrent.house/Register" method="post"
              enctype="application/x-www-form-urlencoded" class="form_wrap">
            <div class="row list">
                <div class="col-md-6 name_frame">
                    <div class="floating">
                        <i class="fas fa-user"></i>
                        <input id="name" data-role="none" class="floating_input name " name="name"
                               type="text" placeholder="姓名" value="<?php echo $_POST['name'];?>
" minlength="2" maxlength="20" required="">
                    </div>

                    <div class="radio_wrap">
                        <div class="radio">
                            <input type="radio" name="gender" value="0" checked="">
                            <img src="/themes/Rent/img/register/woman.svg" alt="小 姐" title="小 姐" class="">

                        </div>
                        <div class="radio">
                            <input type="radio" name="gender" value="1">
                            <img src="/themes/Rent/img/register/men.svg" alt="先 生" title="先 生" class="">

                        </div>
                    </div>

                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">

                    <div class="floating">
                        <i class="fas fa-heart"></i>
                        <input id="nickname" data-role="none" class="floating_input nickname "
                               name="nickname" type="text" placeholder="暱稱" value="<?php echo $_POST['nickname'];?>
" minlength="2"
                               maxlength="20" required="">
                    </div>


                </div>
            </div>

            <div class="row list">
                <div class="col-md-6">
                    <div class="floating">
                        <i class="fas fa-mobile-alt"></i>
                        <input id="user" data-role="none" class="floating_input" name="user" type="text"
                               placeholder="手機號碼" value="<?php echo $_POST['user'];?>
" minlength="6" maxlength="20" required >
                    </div>
                </div>
            </div>

            <div class="row list">
                <div class="col-md-6">
                    <div class="floating">
                        <i class="fas fa-unlock-alt"></i>
                        <input id="password" data-role="none" class="floating_input " name="password"
                               type="password" placeholder="密 碼 - 數字或英文最少6個字" value="<?php echo $_POST['password'];?>
" minlength="6"
                               maxlength="20" required="">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="floating">
                        <i class="fas fa-unlock-alt"></i>
                        <input id="new_password" data-role="none" class="floating_input "
                               name="check_password" type="password" placeholder="確認密碼" value="<?php echo $_POST['check_password'];?>
" minlength="6"
                               maxlength="20" required >
                    </div>
                </div>

            </div>

            <div class="row list">
                <div class="col-md-3">
                    <div class="floating text-center" id="tel_submit">
                        <button type="button" id="tel_code_submit" class="btn ">取得驗證碼</button>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="floating" id="captcha_hidden">
                        <input id="captcha" data-role="none" class="floating_input" name="captcha"
                               type="text" placeholder="驗證碼" maxlength="16" required="">
                    </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class=" content text-center">
                        <button class="btn_reg" type="submit"  id="submitAdd<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
" name="submitAdd<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
" value="1">註 冊</button>
                    </div>
                </div>
            </div>

            <div class="row agree">
                <div class="col-md-12 flex">
                    <div><input id="agree" name="agree" type="checkbox" value="1" checked="true"></div>
                    <label for="agree">
                        <a href="https://okrent.house/statement" target="_blank">
                            我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</a>
                    </label>
                </div>
            </div>

        </form>
    </div>
</div>













































































<?php if ($_GET['success'] == "ok") {
echo '<script'; ?>
>
        $(document).ready(function (e){
            alert("您已註冊成功! 三秒後跳轉頁面到登入畫面!");
        setTimeout(wait_location,3000);
        });

        function wait_location() {
            window.location = 'https://www.okrent.house/login';
        }
<?php echo '</script'; ?>
>
<?php }
}
}
