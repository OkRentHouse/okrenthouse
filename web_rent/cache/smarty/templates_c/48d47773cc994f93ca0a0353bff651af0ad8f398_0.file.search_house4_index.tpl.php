<?php
/* Smarty version 3.1.28, created on 2020-12-11 17:48:45
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/search_house4_index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd3407d196fe6_63065789',
  'file_dependency' => 
  array (
    '48d47773cc994f93ca0a0353bff651af0ad8f398' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/search_house4_index.tpl',
      1 => 1607678511,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd3407d196fe6_63065789 ($_smarty_tpl) {
?>
<form action="https://okrent.house/list" method="get" id="search_box_big">
    <input type="hidden" name="transfer_in" value="1">
    <div class="search_list_wrap">
        <div class="search_div">

            <div class="city_select">
                <spam class="county_city search search_1">
                    區域
                </spam>
            </div><!--<img class="county_city search search_1 class_select_img class_select_img_2" src="themes/Rent/img/select_D.png">-->
            <i class="county_city search search_1 class_select_img class_select_img_2 fas fa-angle-down"></i>
            <div class="county_div" style="display: none;">
                <?php
$_from = $_smarty_tpl->tpl_vars['select_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_county_0_saved_item = isset($_smarty_tpl->tpl_vars['county']) ? $_smarty_tpl->tpl_vars['county'] : false;
$__foreach_county_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['county'] = new Smarty_Variable();
$__foreach_county_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_county_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['county']->value) {
$__foreach_county_0_saved_local_item = $_smarty_tpl->tpl_vars['county'];
?><spam class="county"><input type="radio" name="county" id="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['county']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['county']->value['value'] == $_GET['county']) {?>checked<?php }?>><label for="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['county']->value['title'];?>
</label></spam><?php
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_local_item;
}
}
if ($__foreach_county_0_saved_item) {
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_item;
}
if ($__foreach_county_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_county_0_saved_key;
}
?>
            </div>
                <div class="city_div"><?php
$_from = $_smarty_tpl->tpl_vars['arr_city']->value[$_GET['county']];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_city_1_saved_item = isset($_smarty_tpl->tpl_vars['city']) ? $_smarty_tpl->tpl_vars['city'] : false;
$__foreach_city_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['city'] = new Smarty_Variable();
$__foreach_city_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_city_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['city']->value) {
$__foreach_city_1_saved_local_item = $_smarty_tpl->tpl_vars['city'];
?><spam class="city"><input type="checkbox" name="city[]" id="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['city']->value;?>
" <?php if (in_array($_smarty_tpl->tpl_vars['city']->value,$_GET['city'])) {?>checked<?php }?>><label for="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['city']->value;?>
</label></spam><?php
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_local_item;
}
}
if ($__foreach_city_1_saved_item) {
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_item;
}
if ($__foreach_city_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_city_1_saved_key;
}
?></div>
        </div>

        <div class="search_div">
            <div class="type_select search_1" id="main_house_class_all_title">
                住宅
            </div><!--<img class="type_select class_select_img class_select_img_2" src="themes/Rent/img/select_D.png">--><i class="type_select class_select_img class_select_img_2 fas fa-angle-down"></i>
            <div class="patterns none">
                <input type="radio" id="id_main_house_class_all" value=""<?php if (count($_GET['id_main_house_class']) == 0) {?> checked<?php }?>>
                <label for="id_main_house_class_all" id="id_main_house_class_name">全部</label>
                <?php
$_from = $_smarty_tpl->tpl_vars['main_house_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                    <input type="checkbox" name="id_main_house_class[]" id="id_main_house_class_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
"
                           <?php if (in_array($_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'],$_GET['id_main_house_class'])) {?>checked<?php }?>>
                            <label for="id_main_house_class_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
" id="main_house_class_name_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
"><?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</label>
                <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_2_saved_key;
}
?>
            </div>
        </div>

        <div class="search_div">
            <div class="class_select" id="types_all_name">
                電梯大樓
            </div><!--<img class="class_select class_select_img class_select_img_2" src="themes/Rent/img/select_D.png" >--><i class="class_select class_select_img class_select_img_2 fas fa-angle-down"></i>
            <div class="category none">
                <input type="radio" id="id_types_all" value=""<?php if (count($_GET['id_types']) == 0) {?> checked<?php }?>>
                <label for="id_types_all" id="id_types">全部</label>
                <?php
$_from = $_smarty_tpl->tpl_vars['select_types']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_3_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_3_saved_local_item = $_smarty_tpl->tpl_vars['types'];
?>
                    <input type="checkbox" name="id_types[]" id="id_types_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
"
                           <?php if (in_array($_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'],$_GET['id_types'])) {?>checked<?php }?>>
                            <label for="id_types_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
" id="types_name_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
"><?php echo $_smarty_tpl->tpl_vars['types']->value['title'];?>
</label>
                <?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_local_item;
}
}
if ($__foreach_types_3_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_item;
}
if ($__foreach_types_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_types_3_saved_key;
}
?>
            </div>
        </div>

        <div class="search_div">
            <div class="houses_select" id="type_all_name">
                整層住家
            </div><!--<img class="houses_select class_select_img class_select_img_2" src="themes/Rent/img/select_D.png">--><i class="houses_select class_select_img class_select_img_2 fas fa-angle-down"></i>

            <div class="houses none">
                <input type="radio" id="type_all" value=""<?php if (count($_GET['id_type']) == 0) {?> checked<?php }?>><label for="type_all" id="type_name">全部</label><?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_4_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_4_saved_local_item = $_smarty_tpl->tpl_vars['type'];
?><input type="checkbox" name="id_type[]" id="id_type_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'],$_GET['id_type'])) {?>checked<?php }?>><label for="id_type_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" id="type_name_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
"><?php echo $_smarty_tpl->tpl_vars['type']->value['title'];?>
</label><?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_local_item;
}
}
if ($__foreach_type_4_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_item;
}
if ($__foreach_type_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_type_4_saved_key;
}
?>
            </div>
        </div>
        <div class="search_input"><input type="text" class="field w50p2" name="search" id="search"
                                         value="" placeholder="可輸入關鍵字">
        </div>





        <button class="search_icon" type="submit"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/pic_m.svg"></button>
        <button class="search_icon search_icon_map" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/map_m.svg"></a></button>
    </div>
</form>
<?php echo '<script'; ?>
>
    arr_city = <?php echo json_encode($_smarty_tpl->tpl_vars['arr_city']->value,1);?>
;
    <?php echo $_smarty_tpl->tpl_vars['add_js']->value;?>

<?php echo '</script'; ?>
><?php }
}
