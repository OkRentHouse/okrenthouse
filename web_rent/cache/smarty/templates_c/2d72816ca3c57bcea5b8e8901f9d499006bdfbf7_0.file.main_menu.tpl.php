<?php
/* Smarty version 3.1.28, created on 2021-04-08 08:56:49
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/main_menu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_606e54d1288e95_56380432',
  'file_dependency' => 
  array (
    '2d72816ca3c57bcea5b8e8901f9d499006bdfbf7' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/main_menu.tpl',
      1 => 1617843408,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606e54d1288e95_56380432 ($_smarty_tpl) {
?>
<nav class="main_menu">

  <div class="content_width">

    <div class="logo_main"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/logo_main.svg"><a href="/" class="index"></a></div>

    <nav class="menu">
      

      <div id="menu_right_2" class="menu_right_2 menu_right_no_top" style="cursor: pointer; height: 70%; z-index: 1; width: 60px; left: 90% !important; display: none;"> <img src="/themes/Rent/img/rows.svg" style="height: 100%;">
        <div id="menu_right_2_div" style="display: none;" class="dropdown-menu"> <a href="https://www.okrent.house/#">刊登廣告</a><?php if (!empty($_SESSION['id_member'])) {?><a href="https://www.okrent.house/member"><?php echo $_SESSION['nickname'];?>
</a> <a
            href="https://www.okrent.house/member">會員中心</a> <a href="https://www.okrent.house/logout">登出</a><?php } else { ?><a href="https://www.okrent.house/Register">註冊</a><a href="https://www.okrent.house/login">登入</a><?php }?><a class="no_border"
            href="https://www.okrent.house/#">加盟店</a> </div>
      </div>

      

      <div class="menu_right">
        <a class="icon_block"><label>分享樂租</label><i class="fas fa-share-alt"></i></a>
        <?php if (!empty($_SESSION['id_member'])) {?><a href="https://www.okrent.house/member"><?php echo $_SESSION['nickname'];?>
</a> <a href="https://www.okrent.house/member">會員中心</a> <a
          href="https://www.okrent.house/logout">登出</a><?php } else { ?><a href="https://www.okrent.house/Register">註冊</a><a href="https://www.okrent.house/login">登入</a><?php }?><a class="no_border" href="https://www.okrent.house/#">加盟店</a>

      </div>

      <ul class="menu_left"><?php
$_from = $_smarty_tpl->tpl_vars['main_menu']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?><li <?php if ($_smarty_tpl->tpl_vars['v']->value['color']) {?> class="nav_active_color" <?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['v']->value['href'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['v']->value['active']) {?>active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
</a><?php if (count($_smarty_tpl->tpl_vars['v']->value['submenu'])) {?><ul class="dropdown-menu" role="menu"><?php
$_from = $_smarty_tpl->tpl_vars['v']->value['submenu'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_w_1_saved_item = isset($_smarty_tpl->tpl_vars['w']) ? $_smarty_tpl->tpl_vars['w'] : false;
$__foreach_w_1_saved_key = isset($_smarty_tpl->tpl_vars['j']) ? $_smarty_tpl->tpl_vars['j'] : false;
$_smarty_tpl->tpl_vars['w'] = new Smarty_Variable();
$__foreach_w_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_w_1_total) {
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['w']->value) {
$__foreach_w_1_saved_local_item = $_smarty_tpl->tpl_vars['w'];
?><li ><a href="<?php echo $_smarty_tpl->tpl_vars['w']->value['href'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['w']->value['active']) {?>active<?php }?> <?php echo $_smarty_tpl->tpl_vars['w']->value['class'];?>
"><?php echo $_smarty_tpl->tpl_vars['w']->value['text'];?>
</a></li><?php
$_smarty_tpl->tpl_vars['w'] = $__foreach_w_1_saved_local_item;
}
}
if ($__foreach_w_1_saved_item) {
$_smarty_tpl->tpl_vars['w'] = $__foreach_w_1_saved_item;
}
if ($__foreach_w_1_saved_key) {
$_smarty_tpl->tpl_vars['j'] = $__foreach_w_1_saved_key;
}
?></ul><?php }?></li><?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>

        

    </nav>

  </div>

</nav>
<?php }
}
