<?php
/* Smarty version 3.1.28, created on 2020-12-07 17:33:25
  from "/home/ilifehou/life-house.com.tw/themes/Rent/css/joinmaster_form2.css" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fcdf6e58a1110_36190749',
  'file_dependency' => 
  array (
    '1ed55ba262740882ba2302eb1c9e01740ebc1cb8' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/css/joinmaster_form2.css',
      1 => 1607329332,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fcdf6e58a1110_36190749 ($_smarty_tpl) {
?>

div.bank_tb {
    /* border: 1px solid; */
    border-radius: 20px;
    z-index: 999;
    width: 100%;
    background: #fff;
}

table.bank_ID_tb td::after {
    border-right: 1px solid;
    content: " ";
    height: 3em;
    position: absolute;
    right: 0px;
    border-bottom: 1px solid #00000000;
}

table.bank_ID_tb td:last-child::after {
    border-right: 0px solid;
    content: " ";
    height: 2em;
    position: absolute;
    right: 0px;

}

table.bank_ID_tb td::before {
    border-top: 1px solid;
    content: " ";
    width: 100%;
    position: absolute;
    right: 0px;
    z-index:9;
}

table.bank_tb td {
    border: 0px solid;
    height: 3em;
    position: relative;
}

td.left_title {
    width: 2rem;
    text-align: center;
    vertical-align: middle;
    padding: 1rem;
}

table.bank_ID_tb {
    width: 100%;
}

table.bank_tb {
  width: 100%;
  margin: 0%;
  text-align: center;
}

.bank_ID {
    margin: 0px !important;
}

table.bank_tb select.inp, table.bank_tb input.inp {
    border: 0px;
    border-bottom: 1px solid;
    margin-right: 1em;
    font-size: 16pt;
    font-weight: bold;
}

table.bank_tb input.inp::placeholder{
    font-size: 16pt;
    text-align: left;
}


table.bank_tb select.inp {
    width: 15rem !important;
    text-align: center;
  text-align-last: center;

}



.attachment {
    display: inline-flex;
    margin: auto;
    text-align: center;
    width: 100%;
    display: flex !important;
}

.attachment > div {
    width: 50%;
    text-align: -webkit-center;
}

.attachment > div > img {
    margin-bottom: 20px;
    vertical-align: top;

}

.attachment > div > .get_file {
    cursor: pointer;
}

.attachment .row_1_1_1 {
    margin-left: 20%;
}

.attachment .row_1_1_2 {
    margin-right: 20%;
}

.attachment .view {
    /* border: 1px solid; */
    width: 227pt;
border-radius: 10px;
vertical-align: middle;
display: table-cell;
padding: 20px;
height: 135pt;
font-size: 1vw;
background: #fff;
}

.attachment > div .view {
}

.row_6_1 .attachment > div {
    width: 50% !important;
    margin: 0px;
    padding: 4%;
}

.row_6_1 .attachment .view{
    width: inherit;
    font-size: 1.5vw;
    height: 235pt;
}

.row_1 input.inp {
    /* margin-left: 8px; */
    width: 10em !important;
    border: 0px;
}

.row_2 {
    margin-bottom: 0px;
}

.row_6 {
    margin-top: 0px;
}

.row_6.form2 .form_item {
    display: flex !important;
}
.row_6 .form_item i.fas {
    margin: 0px 5px;
    color: #a7a7a7;
}
.step_02 {
    text-align: center;
    font-size: 3.5vh;
    font-weight: bold;
    /* background: url(/themes/Rent/img/joinmaster/banner_02_arrow.svg);
    background-size: cover;
    background-position-y: -9px;
    background-position-x: -30px;*/
}

.step_02 img {
    width: inherit !important;
}


/* for bankbranchlist.tpl */

table.list td:first-child {
    /* border: 1px solid; */
    border-right: 1px solid #000;
}

table.list td {
    padding: 10px;
    border-bottom: 1px solid;
}

.list_div {
    border: 2px solid #000;
    width: max-content;
    border-radius: 20px;
    margin: auto;
    margin: 2em auto;
}

tr.td_head {
    background: orange;
    color: #fff;
    font-size: 16pt;
    font-weight: bold;
}

.list_div.search input[type="text"] {
    border-radius: 10px;
    font-size: 16pt;
}

.list_div.search input[type="text"]::placeholder{
    font-size: 16pt;
}

.list_div.search {
    padding: 15px 45px;
    border: 0px;
}
.search_ing span {
    vertical-align: middle;
    top: 40%;
    position: absolute;
    left: 0px;
    width: 100%;
}
.search_ing {
    position: fixed;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background: #999999d1;
    color: #fff;
    font-size: 72pt;
    text-align: center;
    display: none;
}

input.search_inp_but {
    margin: 0 10px;
    background: #fff;
    border: 2px solid #53268a;
    border-radius: 5px;
    vertical-align: text-bottom;
    color: #53268a;
}

input.search_inp_but:hover {
    color: #fff;
    border: 2px solid #fff;
    background: #53268a;
}
<?php }
}
