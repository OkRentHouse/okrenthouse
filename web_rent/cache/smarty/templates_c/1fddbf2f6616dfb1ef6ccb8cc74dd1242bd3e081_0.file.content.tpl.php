<?php
/* Smarty version 3.1.28, created on 2020-12-08 20:28:59
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Joinrenthouse/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fcf718b619e84_92327716',
  'file_dependency' => 
  array (
    '1fddbf2f6616dfb1ef6ccb8cc74dd1242bd3e081' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Joinrenthouse/content.tpl',
      1 => 1607430455,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fcf718b619e84_92327716 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

    <div class="Joinrent_house_wrap" style="margin: 0 auto;">
      <div class="item_wrap">
  	    <div class="title">加盟體系</div>
  	    <div class="item_list">
  		    <a class="" href="/in_life">生活家族</a>
              <i></i>
              <a class="" href="/WealthSharing" style="font-weight: bold;">加盟體系</a>
              <i></i>
              <a class="" href="/Store">菁英招募</a>
  	    </div>
      </div>
      <img src="/themes/Rent/img/joinrenthouse/banner.svg" class="">
      <img src="/themes/Rent/img/joinrenthouse/slogan.svg" class="">
      <div class="row_button">
        <div class="button _a1">無店鋪加盟</div>
        <div class="button _b1">門店式加盟</div>
      </div>
    </div>

<style>
article.Joinrenthouse img {
    width: 100%;
}
.Joinrent_house_wrap {
    width: 90%;
    margin: auto;
}
.row_button {
    display: flex;
    width: 50%;
    margin: auto;
}

.row_button {}

.row_button {}

.button {
    border: 0px solid;
    padding: .5em 1em;
    font-size: 2em;
    border-radius: 0 20px;
    margin: auto;
    cursor: pointer;
}

.button._a1 {
    background: #3e0c80;
    color: #fff;
}

.button._b1 {
    background: #fffe00;
    color: #3e0c80;
    font-weight: bold;
}

.item_wrap {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    position: relative;
    margin: 0px auto 40px;
    padding: 0px 70px 0px 15px;
}

.item_wrap:after{
    content: "";
    flex: auto;
    width: 70%;
    height: 1px;
    position: absolute;
    background: #aaa;
}

.item_wrap .title{
    width: unset;
    padding-right: 15px;
    background: #fff;
    z-index: 1;
    color: var(--main_color1);
    font-size: 2em;
    font-weight: bold;
    letter-spacing: 1px;
}

.item_wrap .item_list {
    width: unset;
    padding-left: 15px;
    align-items: unset;
    justify-content: unset;
    margin: unset;
    background: #fff;
    z-index: 1;
    display: flex;
}

.item_wrap .item_list a{
    text-align: center;
    width: 120px;
    position: relative;
    color: var(--text_light_color);
    font-size: 1.1em;
    /* font-weight: bold; */
    letter-spacing: 1px;
}

.item_wrap .item_list a:after{
    display: none;
}

.item_wrap .item_list i{
    width: 1px;
    height: 25px;
    background: #888;
}

.item_wrap .item_list a.active {
    color: #555;
    font-size: 1.2em;
    line-height: 1.2em;
}

.item_wrap .item_list a:hover{
    color: var(--main_color1);
}
/* 頁籤 - end */
</style>
<?php }
}
