<?php
/* Smarty version 3.1.28, created on 2021-05-10 11:41:43
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/search_house5.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6098ab77235b23_59888972',
  'file_dependency' => 
  array (
    '3175ad357a3cfb6376cd53e7d7bdd5a694c29cb2' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/search_house5.tpl',
      1 => 1620618101,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6098ab77235b23_59888972 ($_smarty_tpl) {
?>


<?php echo '<script'; ?>
>
	function clean(){
		document.getElementById("search").value = '';
		document.getElementById("county").selectedIndex  = '';
		document.getElementById("id_main_house_class").selectedIndex  = '';
		document.getElementById("id_type").selectedIndex  = '';
		document.getElementById("id_types").selectedIndex  = '';
	}
	<?php echo '</script'; ?>
>
<form action="https://okrent.house/list" method="get" id="search_box_big2">
	<input type="hidden" name="active" value="1">
	<input type="hidden" id="p" name="p" value="<?php echo $_GET['p'];?>
">
	<div class="tag_div"></div>
	<div class="search_forms_wrap">

		<div id="search_forms" class="searcounty_sch_content">
			<div class="div_rage rage1">

				
				<?php $_smarty_tpl->tpl_vars['search_tt_class'] = new Smarty_Variable(array("county_city search dropdown-toggle bont","bont","類別","型態","輸入關鍵字"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'search_tt_class', 0);?>
				<?php $_smarty_tpl->tpl_vars['search_tt'] = new Smarty_Variable(array("區域","用途","型態","類別","輸入關鍵字"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'search_tt', 0);?>
				
				<?php
$_from = $_smarty_tpl->tpl_vars['search_tt']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
				<?php if ($_smarty_tpl->tpl_vars['st']->value == array_key_last($_smarty_tpl->tpl_vars['search_tt']->value)) {?>
				<div class="search_tt_div"><input class="search_tt" type="text" id="search" name="search" placeholder="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" value="<?php echo $_GET['search'];?>
"><button class="search_icon" type="submit"><img class="pic_m" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/pic_m.svg"></button></div>
				<?php } else { ?>
					<?php if ($_smarty_tpl->tpl_vars['v']->value == "區域") {?>
					<select class="search_tt" id="county" name="county">
					<option value="">區域</option>
					<?php
$_from = $_smarty_tpl->tpl_vars['select_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_county_1_saved_item = isset($_smarty_tpl->tpl_vars['county']) ? $_smarty_tpl->tpl_vars['county'] : false;
$__foreach_county_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['county'] = new Smarty_Variable();
$__foreach_county_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_county_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['county']->value) {
$__foreach_county_1_saved_local_item = $_smarty_tpl->tpl_vars['county'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['county']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['county']->value['value'] == $_GET['county']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['county']->value['value'];?>
</option><?php
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_1_saved_local_item;
}
}
if ($__foreach_county_1_saved_item) {
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_1_saved_item;
}
if ($__foreach_county_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_county_1_saved_key;
}
?>
					</select>
					<?php } elseif ($_smarty_tpl->tpl_vars['v']->value == "用途") {?>		
					<select class="search_tt" id="id_main_house_class" name="id_main_house_class">
					<option value="">用途</option>
					<?php
$_from = $_smarty_tpl->tpl_vars['main_house_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_2_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_2_saved_local_item = $_smarty_tpl->tpl_vars['type'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['type']->value['title'];?>
" <?php if ($_smarty_tpl->tpl_vars['type']->value['title'] == $_GET['id_main_house_class']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['type']->value['title'];?>
</option><?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_2_saved_local_item;
}
}
if ($__foreach_type_2_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_2_saved_item;
}
if ($__foreach_type_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_type_2_saved_key;
}
?>
					</select>
					<?php } elseif ($_smarty_tpl->tpl_vars['v']->value == "型態") {?>		
					<select class="search_tt" id="id_types" name="id_types">
					<option value="">型態</option>
					<?php
$_from = $_smarty_tpl->tpl_vars['select_types']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_3_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_3_saved_local_item = $_smarty_tpl->tpl_vars['types'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
" <?php if ($_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'] == $_GET['id_types']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['types']->value['title'];?>
</option><?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_local_item;
}
}
if ($__foreach_types_3_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_3_saved_item;
}
if ($__foreach_types_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_types_3_saved_key;
}
?>
					</select>	
					<?php } elseif ($_smarty_tpl->tpl_vars['v']->value == "類別") {?>		
					<select class="search_tt" id="id_type" name="id_type[]">
					<option value="">類別</option>
					<?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_4_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_4_saved_local_item = $_smarty_tpl->tpl_vars['type'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'],$_GET['id_type'])) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['type']->value['title'];?>
</option><?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_local_item;
}
}
if ($__foreach_type_4_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_4_saved_item;
}
if ($__foreach_type_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_type_4_saved_key;
}
?>
					</select>		
					<?php } else { ?>
					<select class="search_tt"><option><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</option></select>
					<?php }?>
					
				<?php }?>
				<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_0_saved_key;
}
?>


			</div>
		</div>

	</div>

	

	<div class="search_forms_wrap_II">

		<div class="div_rage rage2">
			<img class="pic_m selectCondition" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/list/Group 310_l.svg" onclick="selectCondition()">
			<img class="pic_m mapview" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/list/Group 361.svg">
		</div>
		<img class="Union_6" onclick="clean()" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/list/Union 6.svg">


	</div>

	<div class="search_forms_wrap_III">
		<?php $_smarty_tpl->tpl_vars['search_tt'] = new Smarty_Variable(array("房數","衛浴數","屋齡","室內坪數","預算","居住人數","租期","起租日"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'search_tt', 0);?>
		<?php $_smarty_tpl->tpl_vars['search_tt_unit'] = new Smarty_Variable(array("間","間","年","坪","元","人","年","日"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'search_tt_unit', 0);?>
		<div class="container rage3">
			<?php
$_from = $_smarty_tpl->tpl_vars['search_tt']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_5_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_5_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_5_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_5_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
			<?php if (($_smarty_tpl->tpl_vars['st']->value+1)%2 == 1 || $_smarty_tpl->tpl_vars['st']->value == 0) {?>
			<div class="row">
			<?php }?>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-2 searchform_title"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</div>
						<div class="col-lg-9">
							<?php if ($_smarty_tpl->tpl_vars['st']->value == array_key_last($_smarty_tpl->tpl_vars['search_tt']->value)) {?>
							<img class="linear" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/list/linear color.svg"><input type="date" class="linear">
							<?php } else { ?>
							<input data-role="doubleslider" data-hint="true" data-hint-always="true" data-hint-mask-min="$1 <?php echo $_smarty_tpl->tpl_vars['search_tt_unit']->value[$_smarty_tpl->tpl_vars['st']->value];?>
" data-hint-mask-max="$1 <?php echo $_smarty_tpl->tpl_vars['search_tt_unit']->value[$_smarty_tpl->tpl_vars['st']->value];?>
">
							<?php }?>
						</div>
						<div class="col-lg-1"></div>
					</div>
				</div>
			<?php if (($_smarty_tpl->tpl_vars['st']->value+1)%2 == 0) {?>
			</div>
			<?php }?>
			<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_local_item;
}
}
if ($__foreach_v_5_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_item;
}
if ($__foreach_v_5_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_5_saved_key;
}
?>
		</div>
		<div class="rage_hr rage4">
			<span class="searchform_title">設備</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>


		<div class="rage4 contect">
		<?php $_smarty_tpl->tpl_vars['rage4_c'] = new Smarty_Variable(array("床組","梳妝台組","衣櫃","沙發茶几組","餐桌椅","書桌椅","天然氣","冷氣","電視","冰箱","洗衣機","烘衣機","第四臺","網路","熱水器:",":電熱水器","廚房炊具:",":瓦斯爐"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rage4_c', 0);?>
		<?php $_smarty_tpl->tpl_vars['rage4_c'] = new Smarty_Variable(array(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rage4_c', 0);?>
		<div class="hidden">
		<?php
$_from = $_smarty_tpl->tpl_vars['select_device_category']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_6_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_6_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_6_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_6_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
			<?php echo array_push($_smarty_tpl->tpl_vars['rage4_c']->value,$_smarty_tpl->tpl_vars['v']->value['title']);?>

		<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_6_saved_local_item;
}
}
if ($__foreach_v_6_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_6_saved_item;
}
if ($__foreach_v_6_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_6_saved_key;
}
?>
		</div>
		<?php
$_from = $_smarty_tpl->tpl_vars['rage4_c']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_7_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_7_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_7_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_7_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_7_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>

			<?php if (!preg_match("/:/i",$_smarty_tpl->tpl_vars['v']->value)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
			</div>
			<?php } else { ?>
				<?php if (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == (mb_strlen($_smarty_tpl->tpl_vars['v']->value)-1)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
				<?php } elseif (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == 0) {?>
				<select class="form-select" aria-label="Default select example">
				  <option selected><?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
</option>
				</select>
			</div>
				<?php }?>
			<?php }?>

		<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_7_saved_local_item;
}
}
if ($__foreach_v_7_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_7_saved_item;
}
if ($__foreach_v_7_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_7_saved_key;
}
?>
		</div>

		<div class="rage_hr rage5">
			<span class="searchform_title">需求</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>

		<?php $_smarty_tpl->tpl_vars['rage5_c'] = new Smarty_Variable(array(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rage5_c', 0);?>
		<div class="hidden">
		<?php
$_from = $_smarty_tpl->tpl_vars['select_other_conditions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_8_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_8_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_8_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_8_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_8_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
			<?php echo array_push($_smarty_tpl->tpl_vars['rage5_c']->value,$_smarty_tpl->tpl_vars['v']->value['title']);?>

		<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_8_saved_local_item;
}
}
if ($__foreach_v_8_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_8_saved_item;
}
if ($__foreach_v_8_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_8_saved_key;
}
?>
		</div>

		<div class="rage5 contect">
			<?php
$_from = $_smarty_tpl->tpl_vars['rage5_c']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_9_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_9_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_9_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_9_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_9_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>

			<?php if (!preg_match("/:/i",$_smarty_tpl->tpl_vars['v']->value)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
			</div>
			<?php } else { ?>
				<?php if (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == (mb_strlen($_smarty_tpl->tpl_vars['v']->value)-1)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
				<?php } elseif (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == 0) {?>
				<select class="form-select" aria-label="Default select example">
				  <option selected><?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
</option>
				</select>
			</div>
				<?php }?>
			<?php }?>

		<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_9_saved_local_item;
}
}
if ($__foreach_v_9_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_9_saved_item;
}
if ($__foreach_v_9_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_9_saved_key;
}
?>
		</div>

		<div class="rage_hr rage6">
			<span class="searchform_title">機能</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>

		<?php $_smarty_tpl->tpl_vars['rage6_c'] = new Smarty_Variable(array("近商場賣場","近傳統市場","近交流道","近綠地公園","近捷運"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rage6_c', 0);?>


		<div class="rage6 contect">
			<?php
$_from = $_smarty_tpl->tpl_vars['rage6_c']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_10_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_10_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_10_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_10_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_10_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>

			<?php if (!preg_match("/:/i",$_smarty_tpl->tpl_vars['v']->value)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
			</div>
			<?php } else { ?>
				<?php if (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == (mb_strlen($_smarty_tpl->tpl_vars['v']->value)-1)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
				<?php } elseif (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == 0) {?>
				<select class="form-select" aria-label="Default select example">
				  <option selected><?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
</option>
				</select>
			</div>
				<?php }?>
			<?php }?>

		<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_10_saved_local_item;
}
}
if ($__foreach_v_10_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_10_saved_item;
}
if ($__foreach_v_10_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_10_saved_key;
}
?>
		</div>

		<div class="rage_hr rage7">
			<span class="searchform_title">社區休閒公設</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>

		<?php $_smarty_tpl->tpl_vars['rage7_c'] = new Smarty_Variable(array(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rage7_c', 0);?>
		<div class="hidden">
		<?php
$_from = $_smarty_tpl->tpl_vars['select_public_utilities']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_11_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_11_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_11_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_11_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_11_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
			<?php echo array_push($_smarty_tpl->tpl_vars['rage7_c']->value,$_smarty_tpl->tpl_vars['v']->value['title']);?>

		<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_11_saved_local_item;
}
}
if ($__foreach_v_11_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_11_saved_item;
}
if ($__foreach_v_11_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_11_saved_key;
}
?>
		</div>

		<div class="rage7 contect">
			<?php
$_from = $_smarty_tpl->tpl_vars['rage7_c']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_12_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_12_saved_key = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_12_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_12_total) {
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_12_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>

			<?php if (!preg_match("/:/i",$_smarty_tpl->tpl_vars['v']->value)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
			</div>
			<?php } else { ?>
				<?php if (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == (mb_strlen($_smarty_tpl->tpl_vars['v']->value)-1)) {?>
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
" data-caption="<?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
">
				<?php } elseif (mb_stripos($_smarty_tpl->tpl_vars['v']->value,':') == 0) {?>
				<select class="form-select" aria-label="Default select example">
				  <option selected><?php echo str_replace(':','',$_smarty_tpl->tpl_vars['v']->value);?>
</option>
				</select>
			</div>
				<?php }?>
			<?php }?>

		<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_12_saved_local_item;
}
}
if ($__foreach_v_12_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_12_saved_item;
}
if ($__foreach_v_12_saved_key) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_v_12_saved_key;
}
?>
		</div>

	</div>


</form>

<?php echo '<script'; ?>
>

	arr_city = <?php echo json_encode($_smarty_tpl->tpl_vars['arr_city']->value,1);?>
;

    <?php if ($_smarty_tpl->tpl_vars['min_price']->value > 0) {?>
	min_price = <?php echo $_smarty_tpl->tpl_vars['min_price']->value;?>
;
    <?php } else { ?>
	min_price = 0;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['max_price']->value > 0) {?>
	max_price = <?php echo $_smarty_tpl->tpl_vars['max_price']->value;?>
;
    <?php } else { ?>
	max_price = 50000;//100000000;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['price_step']->value > 0) {?>
	price_step = <?php echo $_smarty_tpl->tpl_vars['price_step']->value;?>
;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['min_ping']->value > 0) {?>
	min_ping = <?php echo $_smarty_tpl->tpl_vars['min_ping']->value;?>
;
    <?php } else { ?>
	min_ping = 0;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['max_ping']->value > 0) {?>
	max_ping = <?php echo $_smarty_tpl->tpl_vars['max_ping']->value;?>
;
    <?php } else { ?>
	max_ping = 1000;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['ping_step']->value > 0) {?>
	ping_step = <?php echo $_smarty_tpl->tpl_vars['ping_step']->value;?>
;
    <?php }
echo '</script'; ?>
>
<?php }
}
