<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:58:53
  from "/home/ilifehou/life-house.com.tw/themes/Rent/house/search_house3.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5b16d751553_43715518',
  'file_dependency' => 
  array (
    '69e874d83e08d74706a496d4a4c7657821efda41' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/house/search_house3.tpl',
      1 => 1606276273,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fc5b16d751553_43715518 ($_smarty_tpl) {
?>
<form action="<?php if ($_smarty_tpl->tpl_vars['page']->value == 'index') {?>/list<?php }?>" method="get" id="search_box_big">
	<input type="hidden" name="active" value="1">
	<input type="hidden" id="p" name="p" value="<?php echo $_GET['p'];?>
">
	<div class="tag_div"></div>

	<div class="search_forms_wrap">

		<div id="search_forms" class="searcounty_sch_content">
			<div class="div_rage">
				<div class="search_div search_row_div L">

						<strong class="bont">用途<label class="border">請選擇</label></strong>
						<div>
							<div class="items">
							<input type="radio" id="id_main_house_class_all" value=""<?php if (count($_GET['id_main_house_class']) == 0) {?> checked<?php }?>>
							<?php
$_from = $_smarty_tpl->tpl_vars['main_house_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
								<input type="checkbox" name="id_main_house_class[]" id="id_main_house_class_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
"
									   <?php if (in_array($_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'],$_GET['id_main_house_class'])) {?>checked<?php }?>>
								<label for="id_main_house_class_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
" id="main_house_class_name_<?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['id_main_house_class'];?>
"><?php echo $_smarty_tpl->tpl_vars['main_house_class']->value[$_smarty_tpl->tpl_vars['i']->value]['title'];?>
</label>
							<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>
							</div>
						</div>

				</div>
				<div class="search_div search_row_div R">
					<div class="search_div">
					<strong class="county_city search dropdown-toggle bont">區域<label class="border">請選擇</label></strong>
					<div><spam class="county_city search" style="display:none"><?php if (empty($_GET['county'])) {?>所有縣市<?php } else {
echo $_GET['county'];
}?></spam></div>

					<div class="county_div">
	                    <?php
$_from = $_smarty_tpl->tpl_vars['select_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_county_1_saved_item = isset($_smarty_tpl->tpl_vars['county']) ? $_smarty_tpl->tpl_vars['county'] : false;
$__foreach_county_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['county'] = new Smarty_Variable();
$__foreach_county_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_county_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['county']->value) {
$__foreach_county_1_saved_local_item = $_smarty_tpl->tpl_vars['county'];
?><spam class="county"><input type="radio" name="county" id="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['county']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['county']->value['value'] == $_GET['county']) {?>checked<?php }?>><label for="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['county']->value['title'];?>
</label></spam><?php
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_1_saved_local_item;
}
}
if ($__foreach_county_1_saved_item) {
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_1_saved_item;
}
if ($__foreach_county_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_county_1_saved_key;
}
?>
					</div>

					<div class="city_div"><?php
$_from = $_smarty_tpl->tpl_vars['arr_city']->value[$_GET['county']];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_city_2_saved_item = isset($_smarty_tpl->tpl_vars['city']) ? $_smarty_tpl->tpl_vars['city'] : false;
$__foreach_city_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['city'] = new Smarty_Variable();
$__foreach_city_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_city_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['city']->value) {
$__foreach_city_2_saved_local_item = $_smarty_tpl->tpl_vars['city'];
?><spam class="city"><input type="checkbox" name="city[]" id="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['city']->value;?>
" <?php if (in_array($_smarty_tpl->tpl_vars['city']->value,$_GET['city'])) {?>checked<?php }?>><label for="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['city']->value;?>
</label></spam><?php
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_2_saved_local_item;
}
}
if ($__foreach_city_2_saved_item) {
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_2_saved_item;
}
if ($__foreach_city_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_city_2_saved_key;
}
?></div>

				</div>
				</div>
			</div>

			<div class="div_rage sec_class" style="display: none">

			<div class="search_div search_row_div L">
			<strong class="bont">類別<label class="border">請選擇</label></strong>
				<div class="type_class items">
					<input type="radio" id="type_all" value=""<?php if (count($_GET['id_type']) == 0) {?> checked<?php }?>>
					<?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_type_3_saved_item = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$__foreach_type_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$__foreach_type_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_type_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['type']->value) {
$__foreach_type_3_saved_local_item = $_smarty_tpl->tpl_vars['type'];
?><input type="checkbox" name="id_type[]" id="id_type_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'],$_GET['id_type'])) {?>checked<?php }?>><label for="id_type_<?php echo $_smarty_tpl->tpl_vars['type']->value['id_rent_house_type'];?>
"><?php echo $_smarty_tpl->tpl_vars['type']->value['title'];?>
</label>
				<?php
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_3_saved_local_item;
}
}
if ($__foreach_type_3_saved_item) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_type_3_saved_item;
}
if ($__foreach_type_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_type_3_saved_key;
}
?>
				</div>
			</div>

			<div class="search_div search_row_div R">
				<strong class="bont">型態<label class="border">請選擇</label></strong>
					<div class="items">
						<input type="radio" id="id_types_all" value=""<?php if (count($_GET['id_types']) == 0) {?> checked<?php }?>><?php
$_from = $_smarty_tpl->tpl_vars['select_types']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_4_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_4_saved_local_item = $_smarty_tpl->tpl_vars['types'];
?><input type="checkbox" name="id_types[]" id="id_types_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'],$_GET['id_types'])) {?>checked<?php }?>><label for="id_types_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_rent_house_types'];?>
"><?php echo $_smarty_tpl->tpl_vars['types']->value['title'];?>
</label><?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_4_saved_local_item;
}
}
if ($__foreach_types_4_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_4_saved_item;
}
if ($__foreach_types_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_types_4_saved_key;
}
?>
					</div>

			</div>


			</div>

			<div class="div_rage">

				<div class="search_div rang search_row_div"><strong>房數</strong>
					<div class="search_table">
						<div class="w290" id="house_w290"><div style="display:none" class="room"></div>
						<div class="reng_view">
					<input type="text" class="input_no_border" id="room_0" value="OPEN">
						<input type="number" class="input_no_border" name="min_room" id="min_room" value="<?php echo sprintf("%d",$_GET['min_room']);?>
" min="0" max="6" style="display:none"> ~
						<input type="number" class="input_no_border" name="max_room" id="max_room" value="<?php echo sprintf("%d",$_GET['max_room']);?>
" min="0" max="6"> <unit class="room_unit">房</unit> </div>
						</div><p> </p><p> </p>
						<div><div id="room_range"></div><div class="room_Scale_line">
							<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 7) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 7; $_smarty_tpl->tpl_vars['i']->value++) {
?>
							<div class="Scale"><!--|--></div>
							<?php }
}
?>

						</div>
						<div class="room_Scale">
							<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 7) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 7; $_smarty_tpl->tpl_vars['i']->value++) {
?>
							<div class="number <?php if ($_smarty_tpl->tpl_vars['i']->value%2 == 0) {?>m-5<?php }?>" ><span><?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>OPEN<?php } else {
if ($_smarty_tpl->tpl_vars['i']->value > 5) {
echo $_smarty_tpl->tpl_vars['i']->value-1;?>
+<?php } else {
echo $_smarty_tpl->tpl_vars['i']->value;
}
}?></span></div>
							<?php }
}
?>

						</div>
					</div>
					</div>
				</div>

			</div><div class="div_rage">

				<div class="search_div rang search_row_div"><strong>租金</strong>
					<div class="search_table">
						<div class="w290">
							<div class="reng_view">
							<input type="number" class="input_no_border" name="min_price" id="min_price" value="<?php echo sprintf("%d",$_GET['min_price']);?>
" step="<?php echo $_smarty_tpl->tpl_vars['price_step']->value;?>
" min="<?php echo $_smarty_tpl->tpl_vars['min_price']->value;?>
" max="<?php echo $_smarty_tpl->tpl_vars['max_price']->value;?>
"><i> ～ </i><input type="number" class="input_no_border" name="max_price" id="max_price" value="<?php echo sprintf("%d",$_GET['max_price']);?>
"step="<?php echo $_smarty_tpl->tpl_vars['price_step']->value;?>
" min="<?php echo $_smarty_tpl->tpl_vars['min_price']->value;?>
"  max="<?php echo $_smarty_tpl->tpl_vars['max_price']->value;?>
"><unit>元</unit></div>
							</div><p> </p><p> </p>
						<div><div id="price_range"></div><div class="price_Scale_line">
							<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
							<div class="Scale"><!--|--></div>
							<?php }
}
?>

						</div>
						<div class="price_Scale">
							<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value < 12) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 12; $_smarty_tpl->tpl_vars['i']->value++) {
?>
							<div class="number m-5<?php if ($_smarty_tpl->tpl_vars['i']->value%2 == 0) {
}?>" ><span><?php if ($_smarty_tpl->tpl_vars['i']->value > 10) {
echo $_smarty_tpl->tpl_vars['max_price']->value*(($_smarty_tpl->tpl_vars['i']->value-1)/10)/10000;?>
萬+<?php } else {
echo $_smarty_tpl->tpl_vars['max_price']->value*($_smarty_tpl->tpl_vars['i']->value/10)/10000;?>
萬<?php }?></span></div>
							<?php }
}
?>

						</div></div>
					</div>
				</div>

			</div><div class="div_rage">

				<div class="search_div rang search_row_div"><strong>坪數</strong>
					<div class="search_table">
						<div class="w290">
							<div class="reng_view">
							<input type="number" class="input_no_border" name="min_ping" id="min_ping" value="<?php echo sprintf("%d",$_GET['min_ping']);?>
" step="<?php echo $_smarty_tpl->tpl_vars['ping_step']->value;?>
" min="<?php echo $_smarty_tpl->tpl_vars['min_ping']->value;?>
" max="<?php echo $_smarty_tpl->tpl_vars['max_ping']->value;?>
"><i> ～ </i><input type="number" class="input_no_border" name="max_ping" id="max_ping" value="<?php echo sprintf("%d",$_GET['max_ping']);?>
" step="<?php echo $_smarty_tpl->tpl_vars['ping_step']->value;?>
" min="<?php echo $_smarty_tpl->tpl_vars['min_ping']->value;?>
" max="<?php echo $_smarty_tpl->tpl_vars['max_ping']->value;?>
"><unit>坪</unit></div>
							</div><p> </p><p> </p>
						<div><div id="ping_range"></div>
						<div class="ping_Scale_line">
							<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
							<div class="Scale"><!--|--></div>
							<?php }
}
?>

						</div>
						<div class="ping_Scale">
							<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value < 12) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 12; $_smarty_tpl->tpl_vars['i']->value++) {
?>
							<div class="number m-5<?php if ($_smarty_tpl->tpl_vars['i']->value%2 == 0) {
}?>" ><span><?php if ($_smarty_tpl->tpl_vars['i']->value > 10) {
echo $_smarty_tpl->tpl_vars['max_ping']->value*(($_smarty_tpl->tpl_vars['i']->value-1)/50*5);?>
+<?php } else {
echo $_smarty_tpl->tpl_vars['max_ping']->value*($_smarty_tpl->tpl_vars['i']->value/50*5);
}?></span></div>
							<?php }
}
?>

						</div>
					</div>
					</div>
				</div>



			</div>

			<div class="div_rage">

				<div class="search_div xsearch_row_div"><strong class="bont">需求<label class="border">請選擇</label></strong>
					<div class="items">
						<input type="radio" id="id_other_all" value=""<?php if (count($_GET['id_other']) == 0) {?> checked<?php }?>><?php
$_from = $_smarty_tpl->tpl_vars['select_other_conditions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_other_5_saved_item = isset($_smarty_tpl->tpl_vars['other']) ? $_smarty_tpl->tpl_vars['other'] : false;
$__foreach_other_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['other'] = new Smarty_Variable();
$__foreach_other_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_other_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['other']->value) {
$__foreach_other_5_saved_local_item = $_smarty_tpl->tpl_vars['other'];
?><input type="checkbox"  name="id_other[]" id="id_other_<?php echo $_smarty_tpl->tpl_vars['other']->value['id_other_conditions'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['other']->value['id_other_conditions'];?>
" data-id_main_house_class="<?php echo $_smarty_tpl->tpl_vars['other']->value['id_main_house_class'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['other']->value['id_other_conditions'],$_GET['id_other'])) {?>checked<?php }?>><label for="id_other_<?php echo $_smarty_tpl->tpl_vars['other']->value['id_other_conditions'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['other']->value['remarks']) {?>other_remarks<?php }?>"><?php echo $_smarty_tpl->tpl_vars['other']->value['title'];
if ($_smarty_tpl->tpl_vars['other']->value['remarks']) {?><br><span class="fv">(<?php echo $_smarty_tpl->tpl_vars['other']->value['remarks'];?>
)</span><?php }?></label><?php
$_smarty_tpl->tpl_vars['other'] = $__foreach_other_5_saved_local_item;
}
}
if ($__foreach_other_5_saved_item) {
$_smarty_tpl->tpl_vars['other'] = $__foreach_other_5_saved_item;
}
if ($__foreach_other_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_other_5_saved_key;
}
?>
					</div>
				</div>

				<div class="L">



			</div>
				<div class="R">



				</div>
				</div>

				<div class="submit_b">

						<input type="text" class="field w50p2" name="search" id="search" value="" placeholder="請輸入街道名稱或社區名稱等關鍵字" style="border: 1px solid #727171; width: 400px !important;height: 30pt;vertical-align: revert;border-radius: 10px;">
						<button type="submit" class="submit">送出</button>


				</div>





			<label class="search_remid"> <i class="far fa-hand-point-left" style="margin-right: 10px;    font-size: 14pt;" aria-hidden="true"></i>點放大鏡開始搜尋喔<i class="fas fa-exclamation-circle" aria-hidden="true"></i></label>
			<div class="input_txt search_div search_txt" style="display:none">
				<!-- <div class="search_input"><input type="text" class="field w50p2" name="search" id="search" value="<?php echo $_GET['search'];?>
" placeholder="請輸入關鍵字"><button type="submit"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/pic_m.svg"></button></div> -->
			</div>
		</div>
	</div>
</form>

<?php echo '<script'; ?>
>

	arr_city = <?php echo json_encode($_smarty_tpl->tpl_vars['arr_city']->value,1);?>
;

    <?php if ($_smarty_tpl->tpl_vars['min_price']->value > 0) {?>
	min_price = <?php echo $_smarty_tpl->tpl_vars['min_price']->value;?>
;
    <?php } else { ?>
	min_price = 0;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['max_price']->value > 0) {?>
	max_price = <?php echo $_smarty_tpl->tpl_vars['max_price']->value;?>
;
    <?php } else { ?>
	max_price = 50000;//100000000;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['price_step']->value > 0) {?>
	price_step = <?php echo $_smarty_tpl->tpl_vars['price_step']->value;?>
;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['min_ping']->value > 0) {?>
	min_ping = <?php echo $_smarty_tpl->tpl_vars['min_ping']->value;?>
;
    <?php } else { ?>
	min_ping = 0;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['max_ping']->value > 0) {?>
	max_ping = <?php echo $_smarty_tpl->tpl_vars['max_ping']->value;?>
;
    <?php } else { ?>
	max_ping = 1000;
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['ping_step']->value > 0) {?>
	ping_step = <?php echo $_smarty_tpl->tpl_vars['ping_step']->value;?>
;
    <?php }
echo '</script'; ?>
>
<?php }
}
