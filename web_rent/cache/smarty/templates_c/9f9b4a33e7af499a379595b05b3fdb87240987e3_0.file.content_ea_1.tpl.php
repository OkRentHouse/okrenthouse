<?php
/* Smarty version 3.1.28, created on 2020-12-08 11:00:38
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/JoinMaster/content_ea_1.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fceec5679f429_31601725',
  'file_dependency' => 
  array (
    '9f9b4a33e7af499a379595b05b3fdb87240987e3' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/JoinMaster/content_ea_1.tpl',
      1 => 1607396053,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fceec5679f429_31601725 ($_smarty_tpl) {
?>
<hr>
<style>
.join_life_wrap.step1{
  opacity:0;
  display: none;
}
</style>

    <div class="join_life_wrap step1" style="margin: 0 auto;">
        <div class="step_01">
            <span style="color:#3f0d81">Step1.加入申請</span>
            <img src="/themes/Rent/img/joinmaster/banner_01_arrow_an.svg"> Step2.上傳資料
        </div>
        <div class="applyform">
            <div class="row_134">
                <div class="row_1row_1_1">
                    <div class="row_1">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">
                                <img src="/themes/Rent/img/Jointalent/head.svg" class="svg">姓名
                            </span>
                            <input class="inp no_border" type="text" name="name"  maxlength="20" value="<?php echo $_SESSION['name'];?>
" maxlength="20" placeholder="" required="required">
                        </div>
                        <div class="form_item">
                            <span class="title">暱稱</span>
                            <input class="inp no_border" type="text" name="nickname" value="<?php echo $_SESSION['nickname'];?>
" maxlength="20" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="row_1_1">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">生日</span>
                            <input class="inp no_border" type="number" name="birthday_y" maxlength="4" placeholder="" required="required">年
                            <input class="inp no_border" type="number" name="birthday_m" maxlength="2" placeholder="" required="required">月
                            <input class="inp no_border" type="number" name="birthday_d" maxlength="2" placeholder="" required="required">日
                        </div>
                    </div>
                </div>
                <div class="row_3row_4">
                    <div class="row_3">
                        <div class="form_item"><span class="must">*</span>
                            <span class="title"><img src="/themes/Rent/img/Jointalent/telephone.svg" class="svg">手機</span>
                            <input class="inp no_border" type="text" name="phone" value="" placeholder="0912345678" maxlength="20" required="required">
                        </div>
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">
                                <img src="/themes/Rent/img/Jointalent/line.svg" class="svg">Line lD
                            </span>
                            <input class="inp no_border" type="text" name="line_id" maxlength="32" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="row_4">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">電郵</span>
                            <input class="inp no_border" type="email" name="email" maxlength="128" placeholder="　　@　　　　　　　　" required="required">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_2">
                <div class="form_item">
                    <div class="post_ID">
                        <input type="hidden" name="postal" maxlength="6">
                        <span class="must">*</span><span class="title">通訊地址</span>
                        <input class="post_ID_n no_border" name="postal_1" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[1].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_2" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[2].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_3" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[3].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_4" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[4].focus()" onfocus="this.value=''">
                        <input class="post_ID_n no_border" name="postal_5" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[5].focus()" onfocus="this.value=''">
                        <input type="hidden" name="address" maxlength="255">
                        <select class="inp no_border" name="id_county" id="id_county">
                            <option value="">桃園市</option>
                            <?php
$_from = $_smarty_tpl->tpl_vars['county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_county'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['county_name'];?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
                        </select>
                        <select class="inp no_border" name="id_city" id="id_city">
                            <option>桃園區</option>
                        </select>
                        <input type="text" class="inp _10em no_border" name="address" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row_6">
                <div class="form_item">
                    <span>
                        <img src="/themes/Rent/img/Jointalent/speaker.svg" class="svg">訊息來源
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="0" checked="">友人分享
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="1">其他網站APP<input type="text" class="inp_bottom_line no_border" name="address_room" placeholder="">
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="2">人力銀行<input type="text" class="inp_bottom_line no_border" name="address_room" placeholder="">
                    </span>
                    <span>
                        <input type="radio" name="message_source" value="3">其他<input type="text" class="inp_bottom_line no_border" name="address_room" placeholder="">
                    </span>
                </div>
            </div>
            <div class="row_6_1" style="display:none">
                <span class="must">*</span>
                <span>
                      舉薦顧問
                    <input class="inp no_border" type="text" name="recommend_id_member" maxlength="20" placeholder="請填入分享代碼">
                      ※填入本欄資訊您將獲得紅利點數200點的獎勵喔!
                </span>
            </div>
            <div class="row_7">
                <div class="form_item" style="display: flex;">
                    <div class="submit" onclick="sub_next()">
                          下一步
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="join_life_wrap step2" style="margin: 0 auto;">
    <div id="step_02" class="step_02">Step1.加入申請
        <img src="/themes/Rent/img/joinmaster/banner_02_arrow_an.svg">
        <span style="color:#3f0d81">Step2.上傳資料</span>
    </div>
    <div class="applyform">
        <div class="row_134">
            <div class="row_1row_1_1">
                <div class="row_1">
                    <div class="form_item">
                        <span class="must">*</span>
                        <span class="title">身分證字號</span>
                        <input class="inp" type="text" name="identity" maxlength="10" placeholder="" required="required">
                    </div>
                </div>
                <div class="row_1_1">
                    <div class="attachment">
                        <div class="row_1_1_1">
                            <input class="file_1_1_1" type="file" style="display:none">
                            <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">
                            <span class="get_file">上傳身分證正面</span>
                            <div class="view">僅供申請生活達人之用，不得移為他用</div>
                        </div>
                        <div class="row_1_1_2">
                            <input class="file_1_1_2" type="file" style="display:none">
                            <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">
                            <span class="get_file">上傳身分證反面</span>
                            <div class="view">僅供申請生活達人之用，不得移為他用</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_3row_4" style="display:none">
                <div class="row_3">
                    <div class="form_item">
                        <span class="must">*</span>
                        <span class="title"><img src="/themes/Rent/img/Jointalent/telephone.svg" class="svg">手機</span>
                        <input class="inp" type="text" name="phone" value="<?php echo $_SESSION['user'];?>
" placeholder="0912345678" maxlength="20" required="required">
                    </div>
                    <div class="form_item">
                        <span class="must">*</span>
                        <span class="title">
                                <img src="/themes/Rent/img/Jointalent/line.svg" class="svg">LinelD
                            </span>
                        <input class="inp" type="text" name="line_id" maxlength="32" placeholder="" required="required">
                    </div>
                </div>
                <div class="row_4">
                    <div class="form_item">
                        <span class="must">*</span>
                        <span class="title">電郵</span>
                        <input class="inp" type="email" name="email" maxlength="128" placeholder="　　@　　　　　　　　"  required="required">
                    </div>
                </div>
            </div>
        </div>
        <div class="row_2">
            <div class="form_item">
                <div class="post_ID">
                    <input type="hidden" name="postal" maxlength="6">
                    <span class="must">*</span>
                    <span class="title">獎金撥人帳戶(限本人帳戶)</span>
                </div>
            </div>
        </div>
        <div class="row_6 form2">
            <div class="form_item">
                <div class="bank_tb">
                    <table class="bank_tb">
                        <tr>
                            <td class="row_5_td">
                                <div class="row_5_1">
                                    <span class="title">
                                        <select style="width: 10rem!important;" class="inp no_border" name="bank" id="bank">
                                            <option>中國信託</option>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['bank']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
" data-code="<?php echo $_smarty_tpl->tpl_vars['v']->value['code'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_1_saved_key;
}
?>
                                        </select>銀行
                                    </span>
                                    <span class="title">
                                        <input class="inp" type="text" name="branch" placeholder="" required="required" style="border: 1px solid;"><span class="title">分行<a target="_blank" href="/JoinMaster?p=list"><i class="fas fa-external-link-alt"></i></a>
                                    </span>
                                    <span class="title">銀行代碼：
                                        <input class="inp" type="text" name="bank_code" placeholder="822" maxlength="4" required="required">
                                    </span>
                                    <span class="title"></span>
                                </div>
                                <br>
                                <span class="bank_txt_0">金融機構存款帳號(分行別、科目、編號、檢查號碼)</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="bank_ID">
                                    <table class="bank_ID_tb">
                                        <input type="hidden" name="bank_account" maxlength="32">
                                        <tr>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_1" type="text" maxlength="1" onkeyup="this.value=this.value.toUpperCase();document.getElementsByClassName('bank_ID_n')[1].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_2" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[2].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_3" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[3].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_4" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[4].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_5" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[5].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_6" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[6].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_7" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[7].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_8" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[8].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_9" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[9].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_10" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[10].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_11" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[11].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_12" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[12].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_13" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[13].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_14" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[14].focus()" onfocus="this.value=''">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row_6_1">
            <div class="attachment">
                <div class="row_1_1_1">
                    <input class="file_1_1_2" type="file" style="display:none">
                    <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">
                    <span class="get_file">上傳存摺正面</span>
                    <div class="view">僅供申請生活達人之用，不得移為他用</div>
                </div>
                <div class="row_1_1_2">
                    <input class="file_1_1_2" type="file" style="display:none">
                    <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file"><span class="get_file">上傳存摺反面</span>
                    <div class="view">僅供申請生活達人之用，不得移為他用</div>
                </div>
            </div>
        </div>
        <div class="row_6_1_1">
            <div class="form_item">
                <div class="post_ID">
                    <input type="hidden" name="postal" maxlength="6">
                    <input type="checkbox" name="i_agree"><span class="title">我已閲讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</span>
                </div>
            </div>
        </div>
        <div class="row_7">
            <div class="form_item" style="display: flex;">
                <div class="submit" onclick="sub_per()">
                    上一步
                </div>
            </div>
            <p>&nbsp</p>
            <div class="form_item" style="display: flex;">
                <div class="submit" onclick="submit_data()" class="submit_img">
                    送出
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
