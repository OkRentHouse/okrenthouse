<?php
/* Smarty version 3.1.28, created on 2020-12-01 11:02:12
  from "/home/ilifehou/life-house.com.tw/themes/Rent/geely_for_rent/list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5b234336429_40995085',
  'file_dependency' => 
  array (
    '63f48462a2cdda882a8262220b0228c26fe6f489' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/geely_for_rent/list.tpl',
      1 => 1585298025,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../pagination.tpl' => 1,
  ),
),false)) {
function content_5fc5b234336429_40995085 ($_smarty_tpl) {
?>
<div class="rent_list">
    <?php
$_from = $_smarty_tpl->tpl_vars['geely_rent']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?><li><a href="/geely_for_rent?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id_geely_for_rent'];?>
"><div class="img"><?php if (!empty($_smarty_tpl->tpl_vars['item']->value['img'])) {?><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['img'];?>
"><?php } else { ?><div class="no_img"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/logo.svg"></div><?php }?></div><div class="title"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</div><div class="time text-center"><div class="month"><?php echo substr($_smarty_tpl->tpl_vars['item']->value['date'],5,5);?>
</div><div class="year"><?php echo substr($_smarty_tpl->tpl_vars['item']->value['date'],0,4);?>
</div></div></a></li><?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
} else {
?>
		<li class="no_data"><div class="title"><?php echo l(array('s'=>"找不到相關資料"),$_smarty_tpl);?>
</div></li>
    <?php
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_item_0_saved_key;
}
?>
</div><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 <?php }
}
