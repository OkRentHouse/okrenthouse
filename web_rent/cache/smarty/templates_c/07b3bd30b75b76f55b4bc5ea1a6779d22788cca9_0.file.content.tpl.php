<?php
/* Smarty version 3.1.28, created on 2020-12-02 11:57:55
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Forgot/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc710c372e8b9_81519987',
  'file_dependency' => 
  array (
    '07b3bd30b75b76f55b4bc5ea1a6779d22788cca9' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Forgot/content.tpl',
      1 => 1593414198,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fc710c372e8b9_81519987 ($_smarty_tpl) {
?>
<div class="bg">
	<div class="home_web_register">
		<form action="https://www.okrent.house/Register" method="post"
			  enctype="application/x-www-form-urlencoded" class="form_wrap">
			<div class="row list">
				<div class="col-md-3">
				</div>
				<div class="col-md-6">
					<div class="floating">
						<i class="fas fa-mobile-alt"></i>
						<input id="user" data-role="none" class="floating_input" name="user" type="text"
							   placeholder="手機號碼" value="<?php echo $_POST['user'];?>
" minlength="6" maxlength="20" required >
					</div>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			<div class="row list">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<div class="floating text-center" id="tel_submit">
						<button type="button" id="forgot_pwd" name="forgot_pwd" class="btn ">發送密碼</button>
					</div>
				</div>
				<div class="col-md-3">
					<div class="floating text-center">
						<button type="button" onclick="location.href='/login'" class="btn ">返回登入</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<?php }
}
