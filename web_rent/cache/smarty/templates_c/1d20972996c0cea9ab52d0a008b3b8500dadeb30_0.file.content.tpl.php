<?php
/* Smarty version 3.1.28, created on 2021-01-26 15:49:26
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Tenant/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_600fc98655ea28_89693683',
  'file_dependency' => 
  array (
    '1d20972996c0cea9ab52d0a008b3b8500dadeb30' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Tenant/content.tpl',
      1 => 1611647365,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../rent_service/menu.tpl' => 1,
    'file:../../house/house_item_list.tpl' => 1,
    'file:../../../../../../tpl/group_link.tpl' => 1,
  ),
),false)) {
function content_600fc98655ea28_89693683 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<div class="rent_service">
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../rent_service/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="frame_wrap">
		<div class="title text-center"><?php echo Config::get('tenant_title');?>
</div>
		<div class="honeycomb">
			<ul class="honeycomb_list_wrap1">
                <?php $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'j', 0);?>
                <?php
$_from = $_smarty_tpl->tpl_vars['row']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
                <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'k', 0);?>
                <?php if (in_array($_smarty_tpl->tpl_vars['i']->value,array(5,11,16,22))) {?>
                    <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'k', 0);?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['j']->value != ($_smarty_tpl->tpl_vars['j']->value+$_smarty_tpl->tpl_vars['k']->value)) {?>
                <?php $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable($_smarty_tpl->tpl_vars['j']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'j', 0);?>
			</ul><ul class="honeycomb_list_wrap<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
">
                <?php }?>
				<li class="honeycomb_list">
					<a class="hexagontent"<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['content'])) {?> data-show="service_content<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" href="#service_content"<?php } else { ?> href="javascript:void()"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
				</li>
                <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_item_0_saved_key;
}
?>
			</ul>
		</div>
		<div class="service_content_wrap" id="service_content">
            <?php
$_from = $_smarty_tpl->tpl_vars['row']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_1_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$__foreach_item_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_item_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['item']->value) {
$__foreach_item_1_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
				<div id="service_content<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="service_content collapse"><?php echo $_smarty_tpl->tpl_vars['item']->value['content'];?>
</div>
            <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_local_item;
}
}
if ($__foreach_item_1_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_item;
}
if ($__foreach_item_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_item_1_saved_key;
}
?>
		</div>
	</div>
</div>
<?php echo $_smarty_tpl->tpl_vars['Tenant_HTML']->value;?>

<?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
<div class="house_row">
  <div class="commodity_list commodity_data">
            <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
} else {
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_2_saved_key;
}
?>
  </div>
</div>
<?php }?>

<div class="banner2" style="margin: 0 auto;">
<ul class="list_wrap">
<li>&nbsp;</li>
</ul>
</div>
<div class="banner" style="margin: 0 auto;">
<h2 class="caption"><span class="title_h">管理查詢</span></h2>
<ul class="list_wrap">
<li class="list1"><a>管理查詢</a></li>
<li class="list2"><a>紅利點數</a></li>
<li class="list3"><a>租約</a></li>
<li class="list4"><a>屋況報修</a></li>
<li class="list5"><a>租期</a></li>
<li class="list6"><a>房屋點交</a></li>
<li class="list7"><a>費用查詢</a></li>
</ul>
</div>
<div class="banner4" style="margin: 0 auto;">
<ul class="list_wrap">
<li>&nbsp;</li>
</ul>
</div>

<?php if (!empty($_smarty_tpl->tpl_vars['Tenant_CSS']->value)) {?>
<style>
    <?php echo $_smarty_tpl->tpl_vars['Tenant_CSS']->value;?>

</style>
<?php }
if (!empty($_smarty_tpl->tpl_vars['Tenant_JS']->value)) {
echo '<script'; ?>
 type="text/javascript">
    <?php echo $_smarty_tpl->tpl_vars['Tenant_JS']->value;?>

<?php echo '</script'; ?>
>
<?php }?>

<section class="slider_center footers">

    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../../../../../tpl/group_link.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</section>


<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php }
}
