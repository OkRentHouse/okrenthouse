<?php
/* Smarty version 3.1.28, created on 2021-03-18 09:47:57
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/A1786/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6052b14d9d4c74_20887348',
  'file_dependency' => 
  array (
    '9b4438d944a501bb46ae46fa168bc32649d0f66f' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/A1786/content.tpl',
      1 => 1611896704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:house_item_list.tpl' => 1,
    'file:house_salesitem_list.tpl' => 1,
  ),
),false)) {
function content_6052b14d9d4c74_20887348 ($_smarty_tpl) {
?>


<style>



.rent_cash {
    color: red;
    font-weight: bold;
    transform: scale(1.5);
    margin-right: 1em;
}

.commodity_list .commodity .title.big {
    text-align: center;
}

.commodity h3.title.big>a {
    font-size: 12pt;
    letter-spacing: 0px;
}

.commodity h3.title.big_2>a {
    font-size: 10pt;
}

footer .A1786_footer {
    height: 64px;
    background-color: #f0047f;
    padding: 3px 0 0px;
    padding-top: 6px;
}

footer table {
    height: 100%;
    vertical-align: middle;
}

footer table td {
    vertical-align: middle;
}

.footer img {
    height: 12pt;
    margin-right: 5pt;
}

.b_right_line {
    border-right: 1px solid #777;
}

.no_width {
    width: max-content !important;
}

.head_div label a {
    color: #777;
}

.head_div .r label {
    width: 5em;
    text-align: center;
    padding: 0 .5em;
    line-height: 1em;
}

.head_div .l img {
    width: 30%;
    margin: 0em auto;
}

.head_div {
    position: fixed;
    width: 100%;
    z-index: 99;
}

.head_div .l, .head_div .m {
    text-align: center;
}


.head_div div {
    vertical-align: middle;
    line-height: 6em;
    background: #fff;
}

.head_div .m label {
    line-height: 1em;
}

.head_div .m .row {
    height: 3em;
    line-height: 3em;
    position: relative;
}

.head_div .m select {
    border: 0px;
}

.head_div .search select {
    position: absolute;
    left: 12%;
    top: 13%;
}

.head_div .search input[type="text"] {
    width: 80%;
    height: 2em;
    border-radius: 20px;
    border: 1px solid #777;
    padding-left: 5em;
}

.head_div .search .fa-search {
    position: absolute;
    right: 12%;
    width: 20px;
    top: 14%;
    height: auto;
}

.head_div .tab label {
    /* width: 1em; */
    margin: 0 7pt;
}

.search {
    line-height: 2em !important;
}


.type_class {
    text-align: center;
    margin: 2em 0px;
}

.type_class label {
    width: 7em;
    font-weight: bold;
}

.type_class label a {
    color: #000;
}

.type_class label a:hover {
    color: #b55283;
}

.type_class label a.active {
    color: #e662a3;
}

.introduction div.r li {
    margin-left: 60%;
}

.introduction div li {
    width: 20em;
    text-align: start;
    margin: auto;
    line-height: 2.5em;
    list-style: disc;
    text-shadow: #ffffff 0.1em 0.1em 0.2em, #ffffff -0.1em -0.1em 0.2em, #ffffff -0.1em 0.1em 0.2em, #ffffff 0.1em -0.1em 0.2em;
    margin-left: 15%;
}


.introduction div ul {
  margin: 2em 0;
}

.introduction .l .H_light {
    color: #d9167d;
    font-size: 22pt;
}

.introduction .r .H_light {
    color: #9a762c;
    font-size: 22pt;
}

.introduction .H_light {
    margin-right: .5em;
    text-shadow: #f1ebeb 0.1em 0.1em 0.2em;
}

.introduction div.r li:first-child {
    margin-left: 55%;
}

.introduction div li:first-child {
    font-weight: bold;
    margin-left: 10%;
}


.introduction div li.nostyle {
    list-style: none;
}

.imitate_btn {
  background: #d9167d;color: #fff;
  border-radius: 0 10px;
  padding: 5px 20px;
}

.text_box {
    text-align: center;
    margin: 3em;
}
.A1786_row {
  width: 90%;
  margin: auto;
  margin-bottom: 6em;
}

.A1786_row .slick-slide {
    margin: .5em;
}

button.slick-arrow {
    top: 35%;
}

button.slick-arrow:before {
    color: #d9167d;
}

.A1786_row .commodity .photo_row{
    height: auto;
}

.link_group ul {
    display: flex;
    justify-content: center;
    margin: 2em 0;
    margin-top: 5em;
}

.link_group li {
    width: 10%;
    margin: 0 3%;
}

.commodity_list .commodity {
    border: 0px !important;
  }

  .photo_row .img {
      min-height: 171px;
  }
  .bg {
    height: 1550px;
    background: url(/themes/Rent/img/1786/bg.jpg);
      background-attachment: fixed;
      background-position: inherit;
      background-repeat: no-repeat;
      background-size: cover;
  }
</style>
<div class="head_div">
  <div class="col-sm-3 l"><img src="/themes/Rent/img/1786/logo.png"></div>
  <div class="col-sm-5 m">
<div class="row tab">
    <select><option>出租</option><option>出售</option></select>
    <label><a>中古屋</a></label>
    <label><a>新建案</a></label>
    <label><a>店辦</a></label>
    <label><a>辦公室</a></label>
    <label><a>廠房</a></label>
    <label><a>倉庫</a></label>
    <label><a>土地</a></label>
    <label><a>新聞資訊</a></label></div>
    <div class="row search">
      <select class="city" onchange="cell_window_append_cityurl(this.value)">
                <option disabled="" selected="">桃園市</option>
              <option data-content="台北市" class="county_0" value="county_0">台北市</option><option data-content="基隆市" class="county_1" value="county_1">基隆市</option><option data-content="新北市" class="county_2" value="county_2">新北市</option><option data-content="連江縣" class="county_3" value="county_3">連江縣</option><option data-content="宜蘭縣" class="county_4" value="county_4">宜蘭縣</option><option data-content="新竹市" class="county_5" value="county_5">新竹市</option><option data-content="新竹縣" class="county_6" value="county_6">新竹縣</option><option data-content="桃園市" class="county_7" value="county_7">桃園市</option><option data-content="苗栗縣" class="county_8" value="county_8">苗栗縣</option><option data-content="台中市" class="county_9" value="county_9">台中市</option><option data-content="彰化縣" class="county_10" value="county_10">彰化縣</option><option data-content="南投縣" class="county_11" value="county_11">南投縣</option><option data-content="嘉義市" class="county_12" value="county_12">嘉義市</option><option data-content="嘉義縣" class="county_13" value="county_13">嘉義縣</option><option data-content="雲林縣" class="county_14" value="county_14">雲林縣</option><option data-content="台南市" class="county_15" value="county_15">台南市</option><option data-content="高雄市" class="county_16" value="county_16">高雄市</option><option data-content="澎湖縣" class="county_17" value="county_17">澎湖縣</option><option data-content="金門縣" class="county_18" value="county_18">金門縣</option><option data-content="屏東縣" class="county_19" value="county_19">屏東縣</option><option data-content="台東縣" class="county_20" value="county_20">台東縣</option><option data-content="花蓮縣" class="county_21" value="county_21">花蓮縣</option></select>
      <input type="text" placeholder="請輸入社區或街道名稱">
      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16 fa-3x"><path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" class=""></path></svg>
    </div>
  </div>
  <div class="col-sm-4 r">
    <label class="b_right_line"><a>登入</a></label>
    <label class="b_right_line"><a>註冊</a></label>
    <label><a>免費刊登</a></label>
    <label class="no_width"><a><img style="width: 18pt;" class="share" src="/img/share-alt-solid-777.svg"></a></label>
    <label class="no_width"><a><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve" style="fill: #777;width: 42pt;">
                    <g>
                    <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,21.992h97.498c2.363,0,4.296,1.933,4.296,4.509l0,0c0,2.578-1.934,4.512-4.296,4.512H9.75c-2.363,0-4.295-1.934-4.295-4.512   l0,0C5.455,23.925,7.387,21.992,9.75,21.992z"></path>
                    <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,85.773h97.498c2.363,0,4.296,2.147,4.296,4.511l0,0c0,2.576-1.934,4.725-4.296,4.725H9.75c-2.363,0-4.295-2.149-4.295-4.725   l0,0C5.455,87.921,7.387,85.773,9.75,85.773z"></path>
                    <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,53.774h97.498c2.363,0,4.296,2.15,4.296,4.726l0,0c0,2.362-1.934,4.51-4.296,4.51H9.75c-2.363,0-4.295-2.147-4.295-4.51l0,0   C5.455,55.924,7.387,53.774,9.75,53.774z"></path>
                    </g>
                    <g>
                    <defs>
                    <path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"></path>
                    </defs>
                    <clipPath id="SVGID_2_">
                    <use xlink:href="#SVGID_51_" overflow="visible"></use>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"></rect>
                    </defs>
                    <clipPath id="SVGID_4_">
                    <use xlink:href="#SVGID_81_" overflow="visible"></use>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"></rect>
                    </defs>
                    <clipPath id="SVGID_6_">
                    <use xlink:href="#SVGID_105_" overflow="visible"></use>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"></rect>
                    </defs>
                    <clipPath id="SVGID_8_">
                    <use xlink:href="#SVGID_129_" overflow="visible"></use>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"></rect>
                    </defs>
                    <clipPath id="SVGID_10_">
                    <use xlink:href="#SVGID_155_" overflow="visible"></use>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"></rect>
                    </defs>
                    <clipPath id="SVGID_12_">
                    <use xlink:href="#SVGID_183_" overflow="visible"></use>
                    </clipPath>
                    </g>
                    <image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
                    </image>
                    </svg></a></label>
  </div>
</div>
<div class="bg">
<!-- <img src="/themes/Rent/img/1786/1100120-1.JPG">
<img src="/themes/Rent/img/1786/1100120-2.JPG"> -->
</div>
<div class="introduction">
    <div class="col-sm-6 l">
      <ul>
      <li class="nostyle"><label class="H_light">租客</label> 省時省力</li>
      <li>房源眾多 找屋快速</li>
      <li>以條件搜尋屋源 找屋好輕鬆</li>
      <li>需求登錄留言 系統自動配對回報</li>
      <li>雲總機保護您 免除干擾或被迫接收</li>
      <li class="nostyle">推銷訊息而疲勞轟炸</li>
      </ul>
    </div>
    <div class="col-sm-6 r">
      <ul>
      <li class="nostyle"><label class="H_light">房東</label> 省心樂租</li>
      <li>客源超廣 出租快速</li>
      <li>不止刊登還主動找租客</li>
      <li>系統自動媒合租客。主動出擊</li>
      <li>預約看屋及租客提問 能掌握市況反應</li>
      <li>雲總機保護您 免除干擾或被迫接收</li>
      <li class="nostyle">推銷訊息而疲勞轟炸</li>
      </ul>
    </div>
</div>
<div class="bg" style="height: 400px;"></div>
<div class="text_box">
  <label class="imitate_btn">樂租推薦</label>
</div>
<div class="type_class">
<label class="b_right_line"><a class="active">我要租屋</a></label>
<label class="b_right_line"><a>補助津貼</a></label>
<label class="b_right_line"><a>老幼安居</a></label>
<label class="b_right_line"><a>寵物友善</a></label>
<label class="b_right_line"><a>社會住宅</a></label>
<label class="b_right_line"><a>短期季租</a></label>
<label class="b_right_line"><a>共享商辦</a></label>
<label><a>盤出頂讓</a></label>
</div>


<?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
<div class="A1786_row">
  <div class="commodity_list commodity_data">
            <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
} else {
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>
  </div>
</div>
<?php }?>

<div class="text_box">
  <label class="imitate_btn">好買賣精選</label>
</div>

<div class="type_class">
  <label class="b_right_line"><a class="active">我要買屋</a></label>
  <label class="b_right_line"><a>電梯華廈</a></label>
  <label class="b_right_line"><a>透天別墅</a></label>
  <label class="b_right_line"><a>店 面</a></label>
  <label class="b_right_line"><a>廠 房</a></label>
  <label class="b_right_line"><a>商 辦</a></label>
  <label class="b_right_line"><a>土 地</a></label>
  <label><a>新建案</a></label>
</div>

<?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
<div class="A1786_row">
  <div class="commodity_list commodity_data">
            <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:house_salesitem_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
} else {
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_1_saved_key;
}
?>
  </div>
</div>
<?php }?>

<img src="/themes/Rent/img/1786/1100120-3.JPG">
<img src="/themes/Rent/img/1786/1100120-4.JPG">
<img src="/themes/Rent/img/1786/1100120-5.JPG">
<img src="/themes/Rent/img/1786/1100120-6.JPG">
<div class="link_group">
  <ul>
    <li><img src="/themes/Rent/img/1786/footer_icon1.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon2.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon3.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon4.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon5.png"></li>
    <li><img src="/themes/Rent/img/1786/footer_icon6.png"></li>
  </ul>
</div>
<footer>

                  <div class="footer A1786_footer">

                        <table>

                              <tbody>

                                    <tr>

                                          <td>


                                                <div class="footer_txt2"><a href="/Aboutokmaster"><img src="/themes/Okmaster/img/index/footer_logo.png"></a>生活好科技有限公司 <span class="spacing"></span> copyright ©Life Group 2000 ALL right reserde <span class="spacing"></span> <!--<a style='margin-left: 20%;' href='https://www.okmaster.life/Aboutokmaster'>合作提案</a> |--> <a href="/State">相關聲明</a></div>

                                          </td>

                                    </tr>

                              </tbody>

                        </table>

                  </div>

            </footer>
<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php echo $_smarty_tpl->tpl_vars['js1786bg']->value;?>

<?php }
}
