<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:58:53
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/List/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5b16d5adc81_68197365',
  'file_dependency' => 
  array (
    'f0175b038b78d0e9d6e16714d0047dbdb49f0894' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/List/content.tpl',
      1 => 1604461717,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../house/search_process_bar.tpl' => 1,
    'file:../../house/search_house3.tpl' => 1,
    'file:../../house/house_list_item2.tpl' => 1,
    'file:../../pagination.tpl' => 1,
  ),
),false)) {
function content_5fc5b16d5adc81_68197365 ($_smarty_tpl) {
?>
<div class="List_d"> <?php echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>
 <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/search_process_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
<?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value)) {?>
		<div class="no_search_data"><div><!--<i class="fas fa-exclamation-triangle"></i>--></div>
            <div><span class="no_search_span_1">目前未有符合您搜尋條件的物件</span>
            <br>
            <span class="no_search_span_2">建議您重新設定條件搜索適當的物件</span><br>
		<span class="no_search_span_3">
        <!--<i class="far fa-hand-point-down"></i>請於下面設定條件後開始搜尋-->
        </span></div>
        </div>
<?php }?>
<div class="big_box">
    <div class="open_search" id="open_search" value="0">更多搜尋<i class="far fa-plus-square"></i>
    </div>
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/search_house3.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value)) {?>
    <div class="list">
            <?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_house_0_saved_item = isset($_smarty_tpl->tpl_vars['house']) ? $_smarty_tpl->tpl_vars['house'] : false;
$__foreach_house_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['house'] = new Smarty_Variable();
$__foreach_house_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_house_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['house']->value) {
$__foreach_house_0_saved_local_item = $_smarty_tpl->tpl_vars['house'];
?> <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_list_item2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
 <?php
$_smarty_tpl->tpl_vars['house'] = $__foreach_house_0_saved_local_item;
}
} else {
?> <?php
}
if ($__foreach_house_0_saved_item) {
$_smarty_tpl->tpl_vars['house'] = $__foreach_house_0_saved_item;
}
if ($__foreach_house_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_house_0_saved_key;
}
?>
    </div>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } else { ?>
        <!--<div class="no_search_data" style="text-align: center;"><div><i class="fas fa-exclamation-triangle"></i></div>
            <div><span class="no_search_span_1">很抱歉,目前未有符合相關的物件</span>
            <br>
            <span class="no_search_span_2">建議您重新設定條件搜索適當的物件</span><br>
		<span class="no_search_span_3">
        <i class="far fa-hand-point-left"></i>請按左邊 展開搜尋
        </span></div>
        </div>-->
    <?php }?>
</div>
<!-- 留言 -->
<div class="modal fade" id="modal-container1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <input type="hidden" name="id_rent_house">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal_title1" id="myModalLabel">
                    留言 | 回電
                </div>
                <div class="modal_title2">
                    <label for="">案 名</label>
                    <input type="text" name="case_name">
                    <label2 for="">案 號</label2>
                    <input type="text" name="rent_house_code">
                </div>
            </div>
            <div class="modal-body">
                <div class="content_wrap">

                    <div class="content">

                        <div class="form">
                            <div class="row list">
                              <div class="col-md-5 name_frame">

                                <div class="floating">
                                    <i class="fas fa-user"></i>
                                    <input data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                </div>


                              </div>

                                <div class="col-md-1">

                                  <div class="radio_wrap">
                                    <!--<i class="fas fa-user-friends"></i>-->
                                      <div class="radio">
                                          <input type="radio" name="sex_1" value="0" checked="checked">
                                          <label class="caption" for="miss">
                                              小 姐
                                          </label>
                                      </div>
                                      <div class="radio">
                                          <input type="radio" name="sex_1" value="1">
                                          <label class="caption" for="mister">
                                              先 生
                                          </label>
                                      </div>
                                  </div>

                                </div>
                                <div class="col-md-5">

                                  <div class="floating">
                                      <i class="far fa-envelope"></i>
                                      <input data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                  </div>

                                </div>
                            </div>

                            <div class="row list">
                                <div class="col-md-5">
                                  <div class="floating">
                                      <i class="fas fa-mobile-alt"></i>
                                      <input data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                  </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-phone-volume"></i>
                                        <input data-role="none" class="floating_input" name="tel" type="text" placeholder="市 話" value="" maxlength="20" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="message">
                                <div class="caption">
                                    <img class="" src="/themes/Rent/img/msg.svg" alt=""> 留言
                                    <div class="contact_time_wrap">
                                        <i class="far fa-clock"></i>
                                        <div class="caption">方便聯絡時間</div>
                                        <div class="radio">
                                            <input checked="checked" name="contact_time_1" type="radio" value="0">早上
                                        </div>
                                        <div class="radio"><input name="contact_time_1" type="radio" value="1">下午
                                        </div>
                                        <div class="radio"><input name="contact_time_1" type="radio" value="2">晚上
                                        </div>
                                        <div class="radio"><input name="contact_time_1" type="radio" value="3">其他
                                        </div>
                                    </div>
                                  </div>


                                <textarea class="form-control" aria-label="With textarea" id="message" name="message" placeholder="( 限100個字內 )"></textarea>
                            </div>

                            <div class="agree">
                                <label><input type="checkbox" name="agree" value="1">
                                    我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="modal-footer">
              <button type="button" class="btn btn_wait" data-dismiss="modal" onclick="push_message('0');">
                  取消
              </button>
                <input  name="verification" type="text" value=""  placeholder="已登入則免驗證"><button type="button" onclick="MessageVerification('1')">進行驗證</button>
                <button type="button" class="btn" data-dismiss="modal" onclick="push_message('1');">
                    確定送出
                </button>
            </div>
        </div>
    </div>
</div>

<!-- 預約賞屋 -->
<div class="modal fade" id="modal-container2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <input type="hidden" name="id_rent_house">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal_title1" id="myModalLabel">
                    留言 | 回電
                </div>
                <div class="modal_title2">
                    <label for="">案 名</label>
                    <input type="text" name="case_name">
                    <label2 for="">案 號</label2>
                    <input type="text" name="rent_house_code">
                </div>
            </div>
            <div class="modal-body">
                <div class="content_wrap">

                    <div class="content">

                        <div class="form">
                            <div class="row list">
                                <div class="col-md-5 name_frame">
                                    <div class="floating">
                                        <i class="fas fa-user"></i>
                                        <input data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                    </div>
                                </div>

                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="radio_wrap">

                                        <div class="radio">
                                            <input type="radio" name="sex_2" value="0" checked="checked">
                                            <label class="caption" for="miss">
                                                小 姐
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="sex_2" value="1">
                                            <label class="caption" for="mister">
                                                先 生
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-mobile-alt"></i>
                                        <input data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-phone-volume"></i>
                                        <input data-role="none" class="floating_input" name="tel" type="text" placeholder="市 話" value="" maxlength="20" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="far fa-envelope"></i>
                                        <input data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="contact_time_wrap">
                                        <div class="caption">方便聯絡時間</div>
                                        <div class="radio">
                                            <input checked="checked" name="contact_time_2" type="radio" value="0">早上
                                        </div>
                                        <div class="radio"><input name="contact_time_2" type="radio" value="1">下午
                                        </div>
                                        <div class="radio"><input name="contact_time_2" type="radio" value="2">晚上
                                        </div>
                                        <div class="radio"><input name="contact_time_2" type="radio" value="3">其他
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="order_time_wrap">
                                <div class="caption">預約賞屋時間 | <span>(限一週內)</span></div>
                                <div class="radio_wrap">
                                    <input class="form-control" name="year" type="text" placeholder="年">
                                    <input class="form-control" name="mom" type="text" placeholder="月">
                                    <input class="form-control" name="day" type="text" placeholder="日">
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="0" checked>上午
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="1">下午

                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="2">晚上
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="3">皆可
                                    </div>
                                </div>
                            </div>
							<div class="row list">
                                <div class="col-md-5 name_frame" style="display: none;">
                                    <input  name="verification" type="text" value="" style="width: 50%;"><button onclick="MessageVerification('2')">進行驗證</button>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                </div>
                            </div>

                            <div class="message">
                                <div class="caption">
                                    <img class="" src="/themes/Rent/img/msg.svg" alt=""> 留言 ( 限100個字內 )</div>
                                <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                            </div>

                            <div class="agree">
                                <label><input type="checkbox" name="agree" value="1">
                                    我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" onclick="push_message('2');">
                    確定送出
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-container3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <input type="hidden" name="id_rent_house">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal_title1" id="myModalLabel">
                    留言 | 回電
                </div>
                <div class="modal_title2">
                    <label for="">案 名</label>
                    <input type="text" name="case_name">
                    <label2 for="">案 號</label2>
                    <input type="text" name="rent_house_code">
                </div>
            </div>
            <div class="modal-body">
                <div class="content_wrap">
                    <div class="content">
                        <div class="form">
                            <div class="row list">
                                <div class="col-md-5 name_frame">
                                    <div class="floating">
                                        <i class="fas fa-user"></i>
                                        <input data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="radio_wrap">
                                        <div class="radio">
                                            <input type="radio" name="sex_3" value="0" checked="checked">
                                            <label class="caption" for="miss">
                                                小 姐
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="sex_3" value="1">
                                            <label class="caption" for="mister">
                                                先 生
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-mobile-alt"></i>
                                        <input data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-phone-volume"></i>
                                        <input data-role="none" class="floating_input" name="tel" type="text" placeholder="市 話" value="" maxlength="20" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="far fa-envelope"></i>
                                        <input data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="contact_time_wrap">
                                        <div class="caption">方便聯絡時間</div>
                                        <div class="radio">
                                            <input checked="checked" name="contact_time_3" type="radio" value="0">早上
                                        </div>
                                        <div class="radio"><input name="contact_time_3" type="radio" value="1">下午
                                        </div>
                                        <div class="radio"><input name="contact_time_3" type="radio" value="2">晚上
                                        </div>
                                        <div class="radio"><input name="contact_time_3" type="radio" value="3">其他
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="row list">
							    <div class="col-md-5 name_frame" style="display: none;">
                                    <input  name="verification" type="text" value="" style="width: 50%;"><button onclick="MessageVerification('3')">進行驗證</button>
								</div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
		                        </div>
					        </div>
                            <div class="message">
                                <div class="caption">
                                    <img class="" src="/themes/Rent/img/msg.svg" alt=""> 留言 ( 限100個字內 )</div>
                                <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                            </div>
                            <div class="agree">
                                <label><input type="checkbox" name="agree" value="1">
                                    我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" onclick="push_message('3');">
                    確定送出
                </button>
            </div>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    <?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php echo '</script'; ?>
>
<?php }
}
