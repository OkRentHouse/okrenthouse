<?php
/* Smarty version 3.1.28, created on 2021-04-29 17:42:46
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/land_house_index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_608a7f964c50d1_90175633',
  'file_dependency' => 
  array (
    'e21a5516c768c11c80697e51e5c08beab95eb48e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/land_house_index.tpl',
      1 => 1619689362,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608a7f964c50d1_90175633 ($_smarty_tpl) {
?>
<form action="https://okrent.house/list" method="get" id="search_box_big">
  <div class="minbar2_zone">
    <header class="minbar2 col-sm-4 l">
      <nav class="main_menu2 not_select">
      <div class="content_width">
        <nav class="menu" style="text-align: left;">
          <a href="/list">購買</a>
          <a href="/landlord">出售</a>          
        </nav>
      </div>
      </nav>
    </header>

    <input type="hidden" name="transfer_in" value="1">
    <div class="search_list_wrap col-sm-4">
      <!-- <img style="    margin: auto;" src="/themes/Rent/img/logo_main.svg"> -->
      <!-- <img id="logo" style="    margin: auto;" class="bnfy-enter" alt="Rectangle 56" width="243" height="45" src="/themes/Rent/img/index/fig/rectangle-56_122330384.png"> -->
      <div id="logo">
			<img src="/themes/Rent/img/land/logo.svg">
		</div>
    </div>

    <header class="minbar2 col-sm-4 r">
                <nav class="main_menu2 not_select">

      <div class="content_width">



        <nav class="menu">


          <div class="menu_right" style="display: block;">


            <a href="https://www.okrent.house/Register" class="border">註冊</a>
            <a class="noborder" href="https://www.okrent.house/login">登入</a>
            <a class="noborder" class="icon_block share_txt not_select"><svg class="svg-inline--fa fa-share-alt fa-w-14 not_select" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="share-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M352 320c-22.608 0-43.387 7.819-59.79 20.895l-102.486-64.054a96.551 96.551 0 0 0 0-41.683l102.486-64.054C308.613 184.181 329.392 192 352 192c53.019 0 96-42.981 96-96S405.019 0 352 0s-96 42.981-96 96c0 7.158.79 14.13 2.276 20.841L155.79 180.895C139.387 167.819 118.608 160 96 160c-53.019 0-96 42.981-96 96s42.981 96 96 96c22.608 0 43.387-7.819 59.79-20.895l102.486 64.054A96.301 96.301 0 0 0 256 416c0 53.019 42.981 96 96 96s96-42.981 96-96-42.981-96-96-96z"></path></svg><!-- <i class="fas fa-share-alt"></i> Font Awesome fontawesome.com --></a>
            <a class="no_border not_select" href="#">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="hb_line" class="hb_line not_select" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve">
<g>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,19.312h104.659c2.536,0,4.612,2.074,4.612,4.84l0,0   c0,2.767-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.075-4.61-4.842l0,0C1.56,21.386,3.634,19.312,6.17,19.312z"/>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,87.777h104.659c2.536,0,4.612,2.306,4.612,4.842l0,0   c0,2.766-2.076,5.071-4.612,5.071H6.17c-2.536,0-4.61-2.306-4.61-5.071l0,0C1.56,90.083,3.634,87.777,6.17,87.777z"/>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="#522686" d="M6.17,53.428h104.659c2.536,0,4.612,2.306,4.612,5.072l0,0   c0,2.536-2.076,4.842-4.612,4.842H6.17c-2.536,0-4.61-2.306-4.61-4.842l0,0C1.56,55.734,3.634,53.428,6.17,53.428z"/>
</g>
<g>
	<defs>
		<path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"/>
	</defs>
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_51_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"/>
	</defs>
	<clipPath id="SVGID_4_">
		<use xlink:href="#SVGID_81_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"/>
	</defs>
	<clipPath id="SVGID_6_">
		<use xlink:href="#SVGID_105_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"/>
	</defs>
	<clipPath id="SVGID_8_">
		<use xlink:href="#SVGID_129_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"/>
	</defs>
	<clipPath id="SVGID_10_">
		<use xlink:href="#SVGID_155_" overflow="visible"/>
	</clipPath>
</g>
<g>
	<defs>
		<rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"/>
	</defs>
	<clipPath id="SVGID_12_">
		<use xlink:href="#SVGID_183_" overflow="visible"/>
	</clipPath>
</g>
<image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
</image>
</svg></a>


          </div>

          </nav>

      </div>

    </nav>

    		</header>
    </div>
</form>
<div class="col-sm-12 row3">

  <header class="minbar_3">
                <!-- <nav class="main_menu">

      <ul class="menu_left"><li><a href="/list" class="">吉屋快搜</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/list" class="">條件搜尋</a></li><li><a href="/HouseMap" class="">地圖搜尋</a></li></ul></li><li><a href="/tenant" class="">樂租服務</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/tenant" class="">租客</a></li><li><a href="/landlord" class="">房東</a></li></ul></li><li><a href="/tax_benefit" class="">租事順利</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/tax_benefit" class="">租事順利</a></li><li><a href="/authenticate_main" class="">信用認證</a></li><li><a href="/credit_card_main" class="">履約保證</a></li><li><a href="/rent_insurance_main" class="">安心保險</a></li><li><a href="/deposit_storage_main" class="">租管服務</a></li><li><a href="/smart_lock" class="">智能門鎖</a></li><li><a href="/house_check_main" class="">健康好宅</a></li><li><a href="/fix_adept_main" class="">裝修達人</a></li><li><a href="/agents_misson_main" class="">好幫手</a></li><li><a href="/rent_lend_main" class="">共享圈</a></li></ul></li><li><a href="/geely_for_rent" class="">新聞資訊</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/geely_for_rent?id_type=1" class="">資訊</a></li><li><a href="/geely_for_rent?id_type=3" class="">新聞</a></li><li><a href="/geely_for_rent?id_type=4" class="">政策法令</a></li><li><a href="/geely_for_rent?id_type=2" class="">租法觀點</a></li><li><a href="/geely_for_rent?id_type=5" class="">租賃稅費</a></li><li><a href="/geely_for_rent?id_type=6" class="">趨勢</a></li><li><a href="/geely_for_rent?id_type=7" class="">爆料租舍</a></li></ul></li><li><a href="/in_life_index" class="">in生活</a><ul class="dropdown-menu" role="menu" style="display: none;"><li><a href="/in_life" class="">活動分享</a></li><li><a href="/WealthSharing" class="">創富分享</a></li><li><a href="/Store" class="">生活好康</a></li><li><a href="https://www.lifegroup.house/LifeGo" class="">生活樂購</a></li></ul></li><li><a href="/JoinLife" class="">加入生活</a><ul class="dropdown-menu" role="menu"><li><a href="/ExpertAdvisor" class="">生活家族</a></li><li><a href="/Joinrenthouse" class="">菁英招募</a></li><li><a href="#" class="">加盟樂租</a></li></ul></li><li><a href="/about" class="">關於生活</a></li>


        </ul>

    </nav> -->


    <nav class="main_menu">




        <nav class="main_menu">




          <ul class="menu_left"><?php
$_from = $_smarty_tpl->tpl_vars['main_menu']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?><li <?php if ($_smarty_tpl->tpl_vars['v']->value['color']) {?> class="nav_active_color" <?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['v']->value['href'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['v']->value['active']) {?>active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['v']->value['text'];?>
</a><?php if (count($_smarty_tpl->tpl_vars['v']->value['submenu'])) {?><ul class="dropdown-menu" role="menu"><svg class="_350" viewBox="0 0 15 15">
					<path id="_350" d="M 0 0 L 15 1.165734175856414e-14 L 1.165734175856414e-14 15 L 0 0 Z">
					</path>
				</svg><?php
$_from = $_smarty_tpl->tpl_vars['v']->value['submenu'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_w_1_saved_item = isset($_smarty_tpl->tpl_vars['w']) ? $_smarty_tpl->tpl_vars['w'] : false;
$__foreach_w_1_saved_key = isset($_smarty_tpl->tpl_vars['j']) ? $_smarty_tpl->tpl_vars['j'] : false;
$_smarty_tpl->tpl_vars['w'] = new Smarty_Variable();
$__foreach_w_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_w_1_total) {
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['w']->value) {
$__foreach_w_1_saved_local_item = $_smarty_tpl->tpl_vars['w'];
?><li ><a href="<?php echo $_smarty_tpl->tpl_vars['w']->value['href'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['w']->value['active']) {?>active<?php }?> <?php echo $_smarty_tpl->tpl_vars['w']->value['class'];?>
"><?php echo $_smarty_tpl->tpl_vars['w']->value['text'];?>
</a></li><?php
$_smarty_tpl->tpl_vars['w'] = $__foreach_w_1_saved_local_item;
}
}
if ($__foreach_w_1_saved_item) {
$_smarty_tpl->tpl_vars['w'] = $__foreach_w_1_saved_item;
}
if ($__foreach_w_1_saved_key) {
$_smarty_tpl->tpl_vars['j'] = $__foreach_w_1_saved_key;
}
?></ul><?php }?></li><?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>



        </nav>


    </nav>


    		</header>





</div>
<?php echo '<script'; ?>
>
    arr_city = <?php echo json_encode($_smarty_tpl->tpl_vars['arr_city']->value,1);?>
;
    <?php echo $_smarty_tpl->tpl_vars['add_js']->value;?>

<?php echo '</script'; ?>
>
<?php }
}
