<?php
/* Smarty version 3.1.28, created on 2021-02-19 03:54:22
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/About/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_602ec5ee9a1874_56039547',
  'file_dependency' => 
  array (
    '972793f9d38ee437bbe5a9ab34188050eef114c0' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/About/content.tpl',
      1 => 1613650326,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_602ec5ee9a1874_56039547 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>


<style>
<style>
        /* 消除container-fluid左右邊距的方法，在container-fluid後面加[px-0]，然後在row後面加[no-gutters] */

        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }

        .no-gutters>.col,
        .no-gutters>[class*="col-"] {
            padding-right: 0;
            padding-left: 0;
        }

        @charset "UTF-8";
        @-webkit-keyframes swal2-show {
            0% {
                -webkit-transform: scale(.7);
                transform: scale(.7)
            }
            45% {
                -webkit-transform: scale(1.05);
                transform: scale(1.05)
            }
            80% {
                -webkit-transform: scale(.95);
                transform: scale(.95)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @keyframes swal2-show {
            0% {
                -webkit-transform: scale(.7);
                transform: scale(.7)
            }
            45% {
                -webkit-transform: scale(1.05);
                transform: scale(1.05)
            }
            80% {
                -webkit-transform: scale(.95);
                transform: scale(.95)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @-webkit-keyframes swal2-hide {
            0% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 1
            }
            100% {
                -webkit-transform: scale(.5);
                transform: scale(.5);
                opacity: 0
            }
        }

        @keyframes swal2-hide {
            0% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 1
            }
            100% {
                -webkit-transform: scale(.5);
                transform: scale(.5);
                opacity: 0
            }
        }

        @-webkit-keyframes swal2-animate-success-line-tip {
            0% {
                top: 1.1875em;
                left: .0625em;
                width: 0
            }
            54% {
                top: 1.0625em;
                left: .125em;
                width: 0
            }
            70% {
                top: 2.1875em;
                left: -.375em;
                width: 3.125em
            }
            84% {
                top: 3em;
                left: 1.3125em;
                width: 1.0625em
            }
            100% {
                top: 2.8125em;
                left: .875em;
                width: 1.5625em
            }
        }

        @keyframes swal2-animate-success-line-tip {
            0% {
                top: 1.1875em;
                left: .0625em;
                width: 0
            }
            54% {
                top: 1.0625em;
                left: .125em;
                width: 0
            }
            70% {
                top: 2.1875em;
                left: -.375em;
                width: 3.125em
            }
            84% {
                top: 3em;
                left: 1.3125em;
                width: 1.0625em
            }
            100% {
                top: 2.8125em;
                left: .875em;
                width: 1.5625em
            }
        }

        @-webkit-keyframes swal2-animate-success-line-long {
            0% {
                top: 3.375em;
                right: 2.875em;
                width: 0
            }
            65% {
                top: 3.375em;
                right: 2.875em;
                width: 0
            }
            84% {
                top: 2.1875em;
                right: 0;
                width: 3.4375em
            }
            100% {
                top: 2.375em;
                right: .5em;
                width: 2.9375em
            }
        }

        @keyframes swal2-animate-success-line-long {
            0% {
                top: 3.375em;
                right: 2.875em;
                width: 0
            }
            65% {
                top: 3.375em;
                right: 2.875em;
                width: 0
            }
            84% {
                top: 2.1875em;
                right: 0;
                width: 3.4375em
            }
            100% {
                top: 2.375em;
                right: .5em;
                width: 2.9375em
            }
        }

        @-webkit-keyframes swal2-rotate-success-circular-line {
            0% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }
            5% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }
            12% {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }
            100% {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }
        }

        @keyframes swal2-rotate-success-circular-line {
            0% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }
            5% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }
            12% {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }
            100% {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }
        }

        @-webkit-keyframes swal2-animate-error-x-mark {
            0% {
                margin-top: 1.625em;
                -webkit-transform: scale(.4);
                transform: scale(.4);
                opacity: 0
            }
            50% {
                margin-top: 1.625em;
                -webkit-transform: scale(.4);
                transform: scale(.4);
                opacity: 0
            }
            80% {
                margin-top: -.375em;
                -webkit-transform: scale(1.15);
                transform: scale(1.15)
            }
            100% {
                margin-top: 0;
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 1
            }
        }

        @keyframes swal2-animate-error-x-mark {
            0% {
                margin-top: 1.625em;
                -webkit-transform: scale(.4);
                transform: scale(.4);
                opacity: 0
            }
            50% {
                margin-top: 1.625em;
                -webkit-transform: scale(.4);
                transform: scale(.4);
                opacity: 0
            }
            80% {
                margin-top: -.375em;
                -webkit-transform: scale(1.15);
                transform: scale(1.15)
            }
            100% {
                margin-top: 0;
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 1
            }
        }

        @-webkit-keyframes swal2-animate-error-icon {
            0% {
                -webkit-transform: rotateX(100deg);
                transform: rotateX(100deg);
                opacity: 0
            }
            100% {
                -webkit-transform: rotateX(0);
                transform: rotateX(0);
                opacity: 1
            }
        }

        @keyframes swal2-animate-error-icon {
            0% {
                -webkit-transform: rotateX(100deg);
                transform: rotateX(100deg);
                opacity: 0
            }
            100% {
                -webkit-transform: rotateX(0);
                transform: rotateX(0);
                opacity: 1
            }
        }

        body.swal2-toast-shown .swal2-container {
            background-color: transparent
        }

        body.swal2-toast-shown .swal2-container.swal2-shown {
            background-color: transparent
        }

        body.swal2-toast-shown .swal2-container.swal2-top {
            top: 0;
            right: auto;
            bottom: auto;
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.swal2-toast-shown .swal2-container.swal2-top-end,
        body.swal2-toast-shown .swal2-container.swal2-top-right {
            top: 0;
            right: 0;
            bottom: auto;
            left: auto
        }

        body.swal2-toast-shown .swal2-container.swal2-top-left,
        body.swal2-toast-shown .swal2-container.swal2-top-start {
            top: 0;
            right: auto;
            bottom: auto;
            left: 0
        }

        body.swal2-toast-shown .swal2-container.swal2-center-left,
        body.swal2-toast-shown .swal2-container.swal2-center-start {
            top: 50%;
            right: auto;
            bottom: auto;
            left: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%)
        }

        body.swal2-toast-shown .swal2-container.swal2-center {
            top: 50%;
            right: auto;
            bottom: auto;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%)
        }

        body.swal2-toast-shown .swal2-container.swal2-center-end,
        body.swal2-toast-shown .swal2-container.swal2-center-right {
            top: 50%;
            right: 0;
            bottom: auto;
            left: auto;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%)
        }

        body.swal2-toast-shown .swal2-container.swal2-bottom-left,
        body.swal2-toast-shown .swal2-container.swal2-bottom-start {
            top: auto;
            right: auto;
            bottom: 0;
            left: 0
        }

        body.swal2-toast-shown .swal2-container.swal2-bottom {
            top: auto;
            right: auto;
            bottom: 0;
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.swal2-toast-shown .swal2-container.swal2-bottom-end,
        body.swal2-toast-shown .swal2-container.swal2-bottom-right {
            top: auto;
            right: 0;
            bottom: 0;
            left: auto
        }

        body.swal2-toast-column .swal2-toast {
            flex-direction: column;
            align-items: stretch
        }

        body.swal2-toast-column .swal2-toast .swal2-actions {
            flex: 1;
            align-self: stretch;
            height: 2.2em;
            margin-top: .3125em
        }

        body.swal2-toast-column .swal2-toast .swal2-loading {
            justify-content: center
        }

        body.swal2-toast-column .swal2-toast .swal2-input {
            height: 2em;
            margin: .3125em auto;
            font-size: 1em
        }

        body.swal2-toast-column .swal2-toast .swal2-validation-message {
            font-size: 1em
        }

        .swal2-popup.swal2-toast {
            flex-direction: row;
            align-items: center;
            width: auto;
            padding: .625em;
            overflow-y: hidden;
            box-shadow: 0 0 .625em #d9d9d9
        }

        .swal2-popup.swal2-toast .swal2-header {
            flex-direction: row
        }

        .swal2-popup.swal2-toast .swal2-title {
            flex-grow: 1;
            justify-content: flex-start;
            margin: 0 .6em;
            font-size: 1em
        }

        .swal2-popup.swal2-toast .swal2-footer {
            margin: .5em 0 0;
            padding: .5em 0 0;
            font-size: .8em
        }

        .swal2-popup.swal2-toast .swal2-close {
            position: initial;
            width: .8em;
            height: .8em;
            line-height: .8
        }

        .swal2-popup.swal2-toast .swal2-content {
            justify-content: flex-start;
            font-size: 1em
        }

        .swal2-popup.swal2-toast .swal2-icon {
            width: 2em;
            min-width: 2em;
            height: 2em;
            margin: 0
        }

        .swal2-popup.swal2-toast .swal2-icon::before {
            display: flex;
            align-items: center;
            font-size: 2em;
            font-weight: 700
        }

        @media all and (-ms-high-contrast:none),
        (-ms-high-contrast:active) {
            .swal2-popup.swal2-toast .swal2-icon::before {
                font-size: .25em
            }
        }

        .swal2-popup.swal2-toast .swal2-icon.swal2-success .swal2-success-ring {
            width: 2em;
            height: 2em
        }

        .swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line] {
            top: .875em;
            width: 1.375em
        }

        .swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left] {
            left: .3125em
        }

        .swal2-popup.swal2-toast .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right] {
            right: .3125em
        }

        .swal2-popup.swal2-toast .swal2-actions {
            height: auto;
            margin: 0 .3125em
        }

        .swal2-popup.swal2-toast .swal2-styled {
            margin: 0 .3125em;
            padding: .3125em .625em;
            font-size: 1em
        }

        .swal2-popup.swal2-toast .swal2-styled:focus {
            box-shadow: 0 0 0 .0625em #fff, 0 0 0 .125em rgba(50, 100, 150, .4)
        }

        .swal2-popup.swal2-toast .swal2-success {
            border-color: #a5dc86
        }

        .swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line] {
            position: absolute;
            width: 2em;
            height: 2.8125em;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
            border-radius: 50%
        }

        .swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=left] {
            top: -.25em;
            left: -.9375em;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-transform-origin: 2em 2em;
            transform-origin: 2em 2em;
            border-radius: 4em 0 0 4em
        }

        .swal2-popup.swal2-toast .swal2-success [class^=swal2-success-circular-line][class$=right] {
            top: -.25em;
            left: .9375em;
            -webkit-transform-origin: 0 2em;
            transform-origin: 0 2em;
            border-radius: 0 4em 4em 0
        }

        .swal2-popup.swal2-toast .swal2-success .swal2-success-ring {
            width: 2em;
            height: 2em
        }

        .swal2-popup.swal2-toast .swal2-success .swal2-success-fix {
            top: 0;
            left: .4375em;
            width: .4375em;
            height: 2.6875em
        }

        .swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line] {
            height: .3125em
        }

        .swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=tip] {
            top: 1.125em;
            left: .1875em;
            width: .75em
        }

        .swal2-popup.swal2-toast .swal2-success [class^=swal2-success-line][class$=long] {
            top: .9375em;
            right: .1875em;
            width: 1.375em
        }

        .swal2-popup.swal2-toast.swal2-show {
            -webkit-animation: showSweetToast .5s;
            animation: showSweetToast .5s
        }

        .swal2-popup.swal2-toast.swal2-hide {
            -webkit-animation: hideSweetToast .2s forwards;
            animation: hideSweetToast .2s forwards
        }

        .swal2-popup.swal2-toast .swal2-animate-success-icon .swal2-success-line-tip {
            -webkit-animation: animate-toast-success-tip .75s;
            animation: animate-toast-success-tip .75s
        }

        .swal2-popup.swal2-toast .swal2-animate-success-icon .swal2-success-line-long {
            -webkit-animation: animate-toast-success-long .75s;
            animation: animate-toast-success-long .75s
        }

        @-webkit-keyframes showSweetToast {
            0% {
                -webkit-transform: translateY(-.625em) rotateZ(2deg);
                transform: translateY(-.625em) rotateZ(2deg);
                opacity: 0
            }
            33% {
                -webkit-transform: translateY(0) rotateZ(-2deg);
                transform: translateY(0) rotateZ(-2deg);
                opacity: .5
            }
            66% {
                -webkit-transform: translateY(.3125em) rotateZ(2deg);
                transform: translateY(.3125em) rotateZ(2deg);
                opacity: .7
            }
            100% {
                -webkit-transform: translateY(0) rotateZ(0);
                transform: translateY(0) rotateZ(0);
                opacity: 1
            }
        }

        @keyframes showSweetToast {
            0% {
                -webkit-transform: translateY(-.625em) rotateZ(2deg);
                transform: translateY(-.625em) rotateZ(2deg);
                opacity: 0
            }
            33% {
                -webkit-transform: translateY(0) rotateZ(-2deg);
                transform: translateY(0) rotateZ(-2deg);
                opacity: .5
            }
            66% {
                -webkit-transform: translateY(.3125em) rotateZ(2deg);
                transform: translateY(.3125em) rotateZ(2deg);
                opacity: .7
            }
            100% {
                -webkit-transform: translateY(0) rotateZ(0);
                transform: translateY(0) rotateZ(0);
                opacity: 1
            }
        }

        @-webkit-keyframes hideSweetToast {
            0% {
                opacity: 1
            }
            33% {
                opacity: .5
            }
            100% {
                -webkit-transform: rotateZ(1deg);
                transform: rotateZ(1deg);
                opacity: 0
            }
        }

        @keyframes hideSweetToast {
            0% {
                opacity: 1
            }
            33% {
                opacity: .5
            }
            100% {
                -webkit-transform: rotateZ(1deg);
                transform: rotateZ(1deg);
                opacity: 0
            }
        }

        @-webkit-keyframes animate-toast-success-tip {
            0% {
                top: .5625em;
                left: .0625em;
                width: 0
            }
            54% {
                top: .125em;
                left: .125em;
                width: 0
            }
            70% {
                top: .625em;
                left: -.25em;
                width: 1.625em
            }
            84% {
                top: 1.0625em;
                left: .75em;
                width: .5em
            }
            100% {
                top: 1.125em;
                left: .1875em;
                width: .75em
            }
        }

        @keyframes animate-toast-success-tip {
            0% {
                top: .5625em;
                left: .0625em;
                width: 0
            }
            54% {
                top: .125em;
                left: .125em;
                width: 0
            }
            70% {
                top: .625em;
                left: -.25em;
                width: 1.625em
            }
            84% {
                top: 1.0625em;
                left: .75em;
                width: .5em
            }
            100% {
                top: 1.125em;
                left: .1875em;
                width: .75em
            }
        }

        @-webkit-keyframes animate-toast-success-long {
            0% {
                top: 1.625em;
                right: 1.375em;
                width: 0
            }
            65% {
                top: 1.25em;
                right: .9375em;
                width: 0
            }
            84% {
                top: .9375em;
                right: 0;
                width: 1.125em
            }
            100% {
                top: .9375em;
                right: .1875em;
                width: 1.375em
            }
        }

        @keyframes animate-toast-success-long {
            0% {
                top: 1.625em;
                right: 1.375em;
                width: 0
            }
            65% {
                top: 1.25em;
                right: .9375em;
                width: 0
            }
            84% {
                top: .9375em;
                right: 0;
                width: 1.125em
            }
            100% {
                top: .9375em;
                right: .1875em;
                width: 1.375em
            }
        }

        body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) {
            overflow: hidden
        }

        body.swal2-height-auto {
            height: auto!important
        }

        body.swal2-no-backdrop .swal2-shown {
            top: auto;
            right: auto;
            bottom: auto;
            left: auto;
            max-width: calc(100% - .625em * 2);
            background-color: transparent
        }

        body.swal2-no-backdrop .swal2-shown>.swal2-modal {
            box-shadow: 0 0 10px rgba(0, 0, 0, .4)
        }

        body.swal2-no-backdrop .swal2-shown.swal2-top {
            top: 0;
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.swal2-no-backdrop .swal2-shown.swal2-top-left,
        body.swal2-no-backdrop .swal2-shown.swal2-top-start {
            top: 0;
            left: 0
        }

        body.swal2-no-backdrop .swal2-shown.swal2-top-end,
        body.swal2-no-backdrop .swal2-shown.swal2-top-right {
            top: 0;
            right: 0
        }

        body.swal2-no-backdrop .swal2-shown.swal2-center {
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%)
        }

        body.swal2-no-backdrop .swal2-shown.swal2-center-left,
        body.swal2-no-backdrop .swal2-shown.swal2-center-start {
            top: 50%;
            left: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%)
        }

        body.swal2-no-backdrop .swal2-shown.swal2-center-end,
        body.swal2-no-backdrop .swal2-shown.swal2-center-right {
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%)
        }

        body.swal2-no-backdrop .swal2-shown.swal2-bottom {
            bottom: 0;
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%)
        }

        body.swal2-no-backdrop .swal2-shown.swal2-bottom-left,
        body.swal2-no-backdrop .swal2-shown.swal2-bottom-start {
            bottom: 0;
            left: 0
        }

        body.swal2-no-backdrop .swal2-shown.swal2-bottom-end,
        body.swal2-no-backdrop .swal2-shown.swal2-bottom-right {
            right: 0;
            bottom: 0
        }

        .swal2-container {
            display: flex;
            position: fixed;
            z-index: 1060;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            padding: .625em;
            overflow-x: hidden;
            background-color: transparent;
            -webkit-overflow-scrolling: touch
        }

        .swal2-container.swal2-top {
            align-items: flex-start
        }

        .swal2-container.swal2-top-left,
        .swal2-container.swal2-top-start {
            align-items: flex-start;
            justify-content: flex-start
        }

        .swal2-container.swal2-top-end,
        .swal2-container.swal2-top-right {
            align-items: flex-start;
            justify-content: flex-end
        }

        .swal2-container.swal2-center {
            align-items: center
        }

        .swal2-container.swal2-center-left,
        .swal2-container.swal2-center-start {
            align-items: center;
            justify-content: flex-start
        }

        .swal2-container.swal2-center-end,
        .swal2-container.swal2-center-right {
            align-items: center;
            justify-content: flex-end
        }

        .swal2-container.swal2-bottom {
            align-items: flex-end
        }

        .swal2-container.swal2-bottom-left,
        .swal2-container.swal2-bottom-start {
            align-items: flex-end;
            justify-content: flex-start
        }

        .swal2-container.swal2-bottom-end,
        .swal2-container.swal2-bottom-right {
            align-items: flex-end;
            justify-content: flex-end
        }

        .swal2-container.swal2-bottom-end>:first-child,
        .swal2-container.swal2-bottom-left>:first-child,
        .swal2-container.swal2-bottom-right>:first-child,
        .swal2-container.swal2-bottom-start>:first-child,
        .swal2-container.swal2-bottom>:first-child {
            margin-top: auto
        }

        .swal2-container.swal2-grow-fullscreen>.swal2-modal {
            display: flex!important;
            flex: 1;
            align-self: stretch;
            justify-content: center
        }

        .swal2-container.swal2-grow-row>.swal2-modal {
            display: flex!important;
            flex: 1;
            align-content: center;
            justify-content: center
        }

        .swal2-container.swal2-grow-column {
            flex: 1;
            flex-direction: column
        }

        .swal2-container.swal2-grow-column.swal2-bottom,
        .swal2-container.swal2-grow-column.swal2-center,
        .swal2-container.swal2-grow-column.swal2-top {
            align-items: center
        }

        .swal2-container.swal2-grow-column.swal2-bottom-left,
        .swal2-container.swal2-grow-column.swal2-bottom-start,
        .swal2-container.swal2-grow-column.swal2-center-left,
        .swal2-container.swal2-grow-column.swal2-center-start,
        .swal2-container.swal2-grow-column.swal2-top-left,
        .swal2-container.swal2-grow-column.swal2-top-start {
            align-items: flex-start
        }

        .swal2-container.swal2-grow-column.swal2-bottom-end,
        .swal2-container.swal2-grow-column.swal2-bottom-right,
        .swal2-container.swal2-grow-column.swal2-center-end,
        .swal2-container.swal2-grow-column.swal2-center-right,
        .swal2-container.swal2-grow-column.swal2-top-end,
        .swal2-container.swal2-grow-column.swal2-top-right {
            align-items: flex-end
        }

        .swal2-container.swal2-grow-column>.swal2-modal {
            display: flex!important;
            flex: 1;
            align-content: center;
            justify-content: center
        }

        .swal2-container:not(.swal2-top):not(.swal2-top-start):not(.swal2-top-end):not(.swal2-top-left):not(.swal2-top-right):not(.swal2-center-start):not(.swal2-center-end):not(.swal2-center-left):not(.swal2-center-right):not(.swal2-bottom):not(.swal2-bottom-start):not(.swal2-bottom-end):not(.swal2-bottom-left):not(.swal2-bottom-right):not(.swal2-grow-fullscreen)>.swal2-modal {
            margin: auto
        }

        @media all and (-ms-high-contrast:none),
        (-ms-high-contrast:active) {
            .swal2-container .swal2-modal {
                margin: 0!important
            }
        }

        .swal2-container.swal2-fade {
            transition: background-color .1s
        }

        .swal2-container.swal2-shown {
            background-color: rgba(0, 0, 0, .4)
        }

        .swal2-popup {
            display: none;
            position: relative;
            box-sizing: border-box;
            flex-direction: column;
            justify-content: center;
            width: 32em;
            max-width: 100%;
            padding: 1.25em;
            border-radius: .3125em;
            background: #fff;
            font-family: inherit;
            font-size: 1rem
        }

        .swal2-popup:focus {
            outline: 0
        }

        .swal2-popup.swal2-loading {
            overflow-y: hidden
        }

        .swal2-header {
            display: flex;
            flex-direction: column;
            align-items: center
        }

        .swal2-title {
            position: relative;
            max-width: 100%;
            margin: 0 0 .4em;
            padding: 0;
            color: #595959;
            font-size: 1.875em;
            font-weight: 600;
            text-align: center;
            text-transform: none;
            word-wrap: break-word
        }

        .swal2-actions {
            z-index: 1;
            flex-wrap: wrap;
            align-items: center;
            justify-content: center;
            margin: 1.25em auto 0
        }

        .swal2-actions:not(.swal2-loading) .swal2-styled[disabled] {
            opacity: .4
        }

        .swal2-actions:not(.swal2-loading) .swal2-styled:hover {
            background-image: linear-gradient(rgba(0, 0, 0, .1), rgba(0, 0, 0, .1))
        }

        .swal2-actions:not(.swal2-loading) .swal2-styled:active {
            background-image: linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .2))
        }

        .swal2-actions.swal2-loading .swal2-styled.swal2-confirm {
            box-sizing: border-box;
            width: 2.5em;
            height: 2.5em;
            margin: .46875em;
            padding: 0;
            -webkit-animation: swal2-rotate-loading 1.5s linear 0s infinite normal;
            animation: swal2-rotate-loading 1.5s linear 0s infinite normal;
            border: .25em solid transparent;
            border-radius: 100%;
            border-color: transparent;
            background-color: transparent!important;
            color: transparent;
            cursor: default;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none
        }

        .swal2-actions.swal2-loading .swal2-styled.swal2-cancel {
            margin-right: 30px;
            margin-left: 30px
        }

        .swal2-actions.swal2-loading :not(.swal2-styled).swal2-confirm::after {
            content: '';
            display: inline-block;
            width: 15px;
            height: 15px;
            margin-left: 5px;
            -webkit-animation: swal2-rotate-loading 1.5s linear 0s infinite normal;
            animation: swal2-rotate-loading 1.5s linear 0s infinite normal;
            border: 3px solid #999;
            border-radius: 50%;
            border-right-color: transparent;
            box-shadow: 1px 1px 1px #fff
        }

        .swal2-styled {
            margin: .3125em;
            padding: .625em 2em;
            box-shadow: none;
            font-weight: 500
        }

        .swal2-styled:not([disabled]) {
            cursor: pointer
        }

        .swal2-styled.swal2-confirm {
            border: 0;
            border-radius: .25em;
            background: initial;
            background-color: #3085d6;
            color: #fff;
            font-size: 1.0625em
        }

        .swal2-styled.swal2-cancel {
            border: 0;
            border-radius: .25em;
            background: initial;
            background-color: #aaa;
            color: #fff;
            font-size: 1.0625em
        }

        .swal2-styled:focus {
            outline: 0;
            box-shadow: 0 0 0 2px #fff, 0 0 0 4px rgba(50, 100, 150, .4)
        }

        .swal2-styled::-moz-focus-inner {
            border: 0
        }

        .swal2-footer {
            justify-content: center;
            margin: 1.25em 0 0;
            padding: 1em 0 0;
            border-top: 1px solid #eee;
            color: #545454;
            font-size: 1em
        }

        .swal2-image {
            max-width: 100%;
            margin: 1.25em auto
        }

        .swal2-close {
            position: absolute;
            top: 0;
            right: 0;
            justify-content: center;
            width: 1.2em;
            height: 1.2em;
            padding: 0;
            overflow: hidden;
            transition: color .1s ease-out;
            border: none;
            border-radius: 0;
            outline: initial;
            background: 0 0;
            color: #ccc;
            font-family: serif;
            font-size: 2.5em;
            line-height: 1.2;
            cursor: pointer
        }

        .swal2-close:hover {
            -webkit-transform: none;
            transform: none;
            color: #f27474
        }

        .swal2-content {
            z-index: 1;
            justify-content: center;
            margin: 0;
            padding: 0;
            color: #545454;
            font-size: 1.125em;
            font-weight: 300;
            line-height: normal;
            word-wrap: break-word
        }

        #swal2-content {
            text-align: center
        }

        .swal2-checkbox,
        .swal2-file,
        .swal2-input,
        .swal2-radio,
        .swal2-select,
        .swal2-textarea {
            margin: 1em auto
        }

        .swal2-file,
        .swal2-input,
        .swal2-textarea {
            box-sizing: border-box;
            width: 100%;
            transition: border-color .3s, box-shadow .3s;
            border: 1px solid #d9d9d9;
            border-radius: .1875em;
            background: inherit;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .06);
            font-size: 1.125em
        }

        .swal2-file.swal2-inputerror,
        .swal2-input.swal2-inputerror,
        .swal2-textarea.swal2-inputerror {
            border-color: #f27474!important;
            box-shadow: 0 0 2px #f27474!important
        }

        .swal2-file:focus,
        .swal2-input:focus,
        .swal2-textarea:focus {
            border: 1px solid #b4dbed;
            outline: 0;
            box-shadow: 0 0 3px #c4e6f5
        }

        .swal2-file::-webkit-input-placeholder,
        .swal2-input::-webkit-input-placeholder,
        .swal2-textarea::-webkit-input-placeholder {
            color: #ccc
        }

        .swal2-file:-ms-input-placeholder,
        .swal2-input:-ms-input-placeholder,
        .swal2-textarea:-ms-input-placeholder {
            color: #ccc
        }

        .swal2-file::-ms-input-placeholder,
        .swal2-input::-ms-input-placeholder,
        .swal2-textarea::-ms-input-placeholder {
            color: #ccc
        }

        .swal2-file::placeholder,
        .swal2-input::placeholder,
        .swal2-textarea::placeholder {
            color: #ccc
        }

        .swal2-range {
            margin: 1em auto;
            background: inherit
        }

        .swal2-range input {
            width: 80%
        }

        .swal2-range output {
            width: 20%;
            font-weight: 600;
            text-align: center
        }

        .swal2-range input,
        .swal2-range output {
            height: 2.625em;
            padding: 0;
            font-size: 1.125em;
            line-height: 2.625em
        }

        .swal2-input {
            height: 2.625em;
            padding: 0 .75em
        }

        .swal2-input[type=number] {
            max-width: 10em
        }

        .swal2-file {
            background: inherit;
            font-size: 1.125em
        }

        .swal2-textarea {
            height: 6.75em;
            padding: .75em
        }

        .swal2-select {
            min-width: 50%;
            max-width: 100%;
            padding: .375em .625em;
            background: inherit;
            color: #545454;
            font-size: 1.125em
        }

        .swal2-checkbox,
        .swal2-radio {
            align-items: center;
            justify-content: center;
            background: inherit
        }

        .swal2-checkbox label,
        .swal2-radio label {
            margin: 0 .6em;
            font-size: 1.125em
        }

        .swal2-checkbox input,
        .swal2-radio input {
            margin: 0 .4em
        }

        .swal2-validation-message {
            display: none;
            align-items: center;
            justify-content: center;
            padding: .625em;
            overflow: hidden;
            background: #f0f0f0;
            color: #666;
            font-size: 1em;
            font-weight: 300
        }

        .swal2-validation-message::before {
            content: '!';
            display: inline-block;
            width: 1.5em;
            min-width: 1.5em;
            height: 1.5em;
            margin: 0 .625em;
            zoom: normal;
            border-radius: 50%;
            background-color: #f27474;
            color: #fff;
            font-weight: 600;
            line-height: 1.5em;
            text-align: center
        }

        @supports (-ms-accelerator:true) {
            .swal2-range input {
                width: 100%!important
            }
            .swal2-range output {
                display: none
            }
        }

        @media all and (-ms-high-contrast:none),
        (-ms-high-contrast:active) {
            .swal2-range input {
                width: 100%!important
            }
            .swal2-range output {
                display: none
            }
        }

        @-moz-document url-prefix() {
            .swal2-close:focus {
                outline: 2px solid rgba(50, 100, 150, .4)
            }
        }

        .swal2-icon {
            position: relative;
            box-sizing: content-box;
            justify-content: center;
            width: 5em;
            height: 5em;
            margin: 1.25em auto 1.875em;
            zoom: normal;
            border: .25em solid transparent;
            border-radius: 50%;
            line-height: 5em;
            cursor: default;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none
        }

        .swal2-icon::before {
            display: flex;
            align-items: center;
            height: 92%;
            font-size: 3.75em
        }

        .swal2-icon.swal2-error {
            border-color: #f27474
        }

        .swal2-icon.swal2-error .swal2-x-mark {
            position: relative;
            flex-grow: 1
        }

        .swal2-icon.swal2-error [class^=swal2-x-mark-line] {
            display: block;
            position: absolute;
            top: 2.3125em;
            width: 2.9375em;
            height: .3125em;
            border-radius: .125em;
            background-color: #f27474
        }

        .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=left] {
            left: 1.0625em;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .swal2-icon.swal2-error [class^=swal2-x-mark-line][class$=right] {
            right: 1em;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .swal2-icon.swal2-warning {
            border-color: #facea8;
            color: #f8bb86
        }

        .swal2-icon.swal2-warning::before {
            content: '!'
        }

        .swal2-icon.swal2-info {
            border-color: #9de0f6;
            color: #3fc3ee
        }

        .swal2-icon.swal2-info::before {
            content: 'i'
        }

        .swal2-icon.swal2-question {
            border-color: #c9dae1;
            color: #87adbd
        }

        .swal2-icon.swal2-question::before {
            content: '?'
        }

        .swal2-icon.swal2-question.swal2-arabic-question-mark::before {
            content: '؟'
        }

        .swal2-icon.swal2-success {
            border-color: #a5dc86
        }

        .swal2-icon.swal2-success [class^=swal2-success-circular-line] {
            position: absolute;
            width: 3.75em;
            height: 7.5em;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
            border-radius: 50%
        }

        .swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=left] {
            top: -.4375em;
            left: -2.0635em;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-transform-origin: 3.75em 3.75em;
            transform-origin: 3.75em 3.75em;
            border-radius: 7.5em 0 0 7.5em
        }

        .swal2-icon.swal2-success [class^=swal2-success-circular-line][class$=right] {
            top: -.6875em;
            left: 1.875em;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-transform-origin: 0 3.75em;
            transform-origin: 0 3.75em;
            border-radius: 0 7.5em 7.5em 0
        }

        .swal2-icon.swal2-success .swal2-success-ring {
            position: absolute;
            z-index: 2;
            top: -.25em;
            left: -.25em;
            box-sizing: content-box;
            width: 100%;
            height: 100%;
            border: .25em solid rgba(165, 220, 134, .3);
            border-radius: 50%
        }

        .swal2-icon.swal2-success .swal2-success-fix {
            position: absolute;
            z-index: 1;
            top: .5em;
            left: 1.625em;
            width: .4375em;
            height: 5.625em;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .swal2-icon.swal2-success [class^=swal2-success-line] {
            display: block;
            position: absolute;
            z-index: 2;
            height: .3125em;
            border-radius: .125em;
            background-color: #a5dc86
        }

        .swal2-icon.swal2-success [class^=swal2-success-line][class$=tip] {
            top: 2.875em;
            left: .875em;
            width: 1.5625em;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .swal2-icon.swal2-success [class^=swal2-success-line][class$=long] {
            top: 2.375em;
            right: .5em;
            width: 2.9375em;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .swal2-progress-steps {
            align-items: center;
            margin: 0 0 1.25em;
            padding: 0;
            background: inherit;
            font-weight: 600
        }

        .swal2-progress-steps li {
            display: inline-block;
            position: relative
        }

        .swal2-progress-steps .swal2-progress-step {
            z-index: 20;
            width: 2em;
            height: 2em;
            border-radius: 2em;
            background: #3085d6;
            color: #fff;
            line-height: 2em;
            text-align: center
        }

        .swal2-progress-steps .swal2-progress-step.swal2-active-progress-step {
            background: #3085d6
        }

        .swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step {
            background: #add8e6;
            color: #fff
        }

        .swal2-progress-steps .swal2-progress-step.swal2-active-progress-step~.swal2-progress-step-line {
            background: #add8e6
        }

        .swal2-progress-steps .swal2-progress-step-line {
            z-index: 10;
            width: 2.5em;
            height: .4em;
            margin: 0 -1px;
            background: #3085d6
        }

        [class^=swal2] {
            -webkit-tap-highlight-color: transparent
        }

        .swal2-show {
            -webkit-animation: swal2-show .3s;
            animation: swal2-show .3s
        }

        .swal2-show.swal2-noanimation {
            -webkit-animation: none;
            animation: none
        }

        .swal2-hide {
            -webkit-animation: swal2-hide .15s forwards;
            animation: swal2-hide .15s forwards
        }

        .swal2-hide.swal2-noanimation {
            -webkit-animation: none;
            animation: none
        }

        .swal2-rtl .swal2-close {
            right: auto;
            left: 0
        }

        .swal2-animate-success-icon .swal2-success-line-tip {
            -webkit-animation: swal2-animate-success-line-tip .75s;
            animation: swal2-animate-success-line-tip .75s
        }

        .swal2-animate-success-icon .swal2-success-line-long {
            -webkit-animation: swal2-animate-success-line-long .75s;
            animation: swal2-animate-success-line-long .75s
        }

        .swal2-animate-success-icon .swal2-success-circular-line-right {
            -webkit-animation: swal2-rotate-success-circular-line 4.25s ease-in;
            animation: swal2-rotate-success-circular-line 4.25s ease-in
        }

        .swal2-animate-error-icon {
            -webkit-animation: swal2-animate-error-icon .5s;
            animation: swal2-animate-error-icon .5s
        }

        .swal2-animate-error-icon .swal2-x-mark {
            -webkit-animation: swal2-animate-error-x-mark .5s;
            animation: swal2-animate-error-x-mark .5s
        }

        @-webkit-keyframes swal2-rotate-loading {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        @keyframes swal2-rotate-loading {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        @media print {
            body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) {
                overflow-y: scroll!important
            }
            body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown)>[aria-hidden=true] {
                display: none
            }
            body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) .swal2-container {
                position: initial!important
            }
        }

        html {
            box-sizing: border-box;
            scroll-behavior: smooth;
            /* 只加這一行就可以有 smooth-scroll 的效果，甚至不需要jQuery */
        }
    </style>
</style>
        <div class="l">
            <ul>
                <li class="sevice_item" style="display: none;">

                    <a class="icon_block open" data-type="app"><img src="/themes/Rent/img/about/2021204/rent/app.svg"></a>

                    <span class="text">APP下載</span>

                </li>
                <li class="sevice_item" style="display: none;">

                    <a class="icon_block open" data-type="qr_code"><img src="/themes/Rent/img/about/2021204/rent/qr_code.svg"></a>

                    <span class="text">QR Code</span>

                </li>


                <li class="sevice_item" style="display: none;">

                    <a class="icon_block FB"><i class="fab fa-facebook-square sevice_wrap_hidden_i_trans"></i>
    <!--<img src="/modules/FloatShare/img/icon/facebook.svg">--></a>

                    <span class="text">FB分享</span>

                </li>

                <li class="sevice_item" style="display: none;">

                    <a class="icon_block LINE"><i class="fab fa-line sevice_wrap_hidden_i_trans"></i>
    <!--<img src="/modules/FloatShare/img/icon/line.svg">--></a>

                    <span class="text">Line分享</span>

                </li>

                <li class="sevice_item" style="display: none;">

                    <a class="icon_block MAIL" href="mailto:?subject=%c3%f6%a9%f3%bc%d6%af%b2%a2x%a5%cd%ac%a1%bc%d6%af%b2&amp;body=%a4%c0%a8%c9%b5%b9%b1z%0d%0ahttps://www.okrent.house/about"><img src="/themes/Rent/img/about/2021204/rent/share.svg"></a>

                    <span class="text">寄給朋友</span>

                </li>

                <li class="sevice_item" style="display: none;">

                    <hr style="border: 1px solid #99999973;width: 28pt;">

                </li>

                <li class="sevice_item" style="display: none;">

                    <a class="icon_block open" data-type="call_us">
                        <img src="/themes/Rent/img/about/2021204/rent/contact_us.svg"></a>

                    <span class="text">聯絡我們</span>

                </li>


            </ul>
        </div>


        <div class="item_wrap">
        <div class="title">關於生活</div>
        <div class="item_list">
              <i></i>
              <a href="#1">品牌故事</a>
              <i></i>
              <a href="#2">核心價值</a>
              <i></i>
              <a href="#3">願景展望</a>
              <i></i>
              <a href="#4">歷史軌跡</a>
              <i></i>
              <a href="#5">合作提案</a>
              <i></i>
              <a href="#6">共好夥伴</a>
              <i></i>
              <a href="#7">聯絡我們</a>
              <i></i>
        </div>
      </div>

        <!-- 主要內容 -->

        <!-- 公司名抬頭 -->
        <section id="portfolio" class="bg-light-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bg-2">

                        <div class="logo-txt">生活資產管理股份有限公司</div>
                        <div class="logo-txt-s">Life Property Investment Management CO.,LTD.</div>
                    </div>
                </div>

                <div class="title-3"></div>

        </div></section>
        <!-- 品牌故事 -->
        <section id="portfolio" class="bg-light-gray center" style="padding:60px;">
            <div class="center">
                <img src="/themes/Rent/img/about/2021204/group/p1.png" alt="">
            </div>
        </section>
        <div id="2"></div>
        <!-- 核心價值 -->
        <section id="portfolio" class="bg-light-gray bg-3" style="padding:0px 60px;">
            <div class="center">
                <img src="/themes/Rent/img/about/2021204/group/p4.png" alt="">
            </div>

        </section>
        <!-- 願景展望 -->
        <section class="m60" id="3">
            <div class="center">
                <img src="/themes/Rent/img/about/2021204/group/p2.png" alt="" style="background-color: #e1e1e1;">
            </div>

        </section>

        <!-- 歷史軌跡 -->
        <section id="4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="timeline">
                            <!-- 2020 -->
                            <li>
                                <div class="timeline-image year-1">2020</div>
                                <!-- <img class="img-circle img-responsive" src="/themes/Rent/img/about/2021204/about/1.jpg" alt=""> -->
                                <div class="timeline-panel">
                                    <div class="timeline-body">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2020.png" style="margin-left:-70px;margin-top:-75px;" alt="">
                                        </p>
                                    </div>
                                </div>

                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="margin-top:60px;">
                                        <h4 class="subheading">生活樂租 APP上線</h4>
                                    </div>
                                </div>

                            </li>

                            <!-- 2019 -->
                            <li class="timeline-inverted">
                                <div class="timeline-image year-1">2019</div>
                                <div class="timeline-panel">
                                    <div class="timeline-body">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2019.png" alt="">
                                        </p>
                                    </div>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-body" style="text-align: right;margin-right:120px; margin-top:-10px;">
                                        <h4 class="subheading">首創『樂租租賃信用認證』</h4>
                                        <p class="text-muted">- 全國首創『樂租租賃信用認證』中心</p>
                                        <p class="text-muted">- 提升租賃交易安全</p>
                                        <p class="text-muted">- 提供『安心租履約保證』方案</p>
                                    </div>
                                </div>
                            </li>

                            <!-- 2018 -->
                            <li>
                                <div class="timeline-image year-1">2018</div>
                                <!-- <img class="img-circle img-responsive" src="/themes/Rent/img/about/2021204/about/1.jpg" alt=""> -->
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="margin-top:-40px;margin-right:80px;">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2018.png" alt="">
                                        </p>
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2018-2.png" alt="">
                                        </p>

                                    </div>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-body" style="margin-top:-40px;">
                                        <p class="text-muted">與『中國信託』合作 提供多元金流服務方案</p>
                                        <p class="text-muted">全國首創刷卡 | 超商及郵局代收服務</p>
                                        <p class="text-muted">攜手『僑馥建金』提供租賃履約保證</p>
                                        <p class="text-muted">與『國泰產物保險』及『和安保代』合作</p>
                                        <p class="text-muted">首創安心租保險</p>



                                    </div>
                                </div>
                            </li>

                            <!-- 2017 -->
                            <li class="timeline-inverted">
                                <div class="timeline-image year-1">2017</div>
                                <div class="timeline-panel">
                                    <div class="timeline-body">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2017.png" alt="" style="margin-top:10px;">
                                        </p>
                                    </div>
                                </div>
                                <div class="timeline-panel" style="text-align: right;">
                                    <div class="timeline-heading" style="margin-top:45px;margin-right:120px;">
                                        <p class="text-muted txt-40">獲選為桃園市政府<br>『民間住宅包租代管計畫執行廠商』
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <!-- 2013 -->
                            <li>
                                <div class="timeline-image year-1">2013</div>
                                <div class="timeline-panel">
                                    <div class="timeline-body" style="margin-right:80px;">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2013.png" alt="" style="margin-top:20px;">
                                        </p>
                                    </div>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="margin-top:30px;">
                                        <h4 class="subheading">創立生活樂租</h4>
                                        <h4 class="subheading">及加盟事業體系</h4>
                                    </div>

                                    <div class="timeline-body">
                                        <h4 class="subheading"><br></h4>
                                    </div>

                                </div>
                            </li>

                            <!-- 2010 -->
                            <li>
                                <div class="timeline-image year-1">2010</div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="text-align: right; margin-top:60px;margin-right:80px;">
                                        <h4 class="subheading">發展包租業務</h4>
                                    </div>

                                    <div class="timeline-body">
                                        <h4 class="subheading"><br></h4>
                                    </div>

                                </div>
                                <div class="timeline-panel">
                                </div>
                            </li>

                            <!-- 2007 -->
                            <li>
                                <div class="timeline-image year-1">2007</div>
                                <div class="timeline-panel">
                                    <div class="timeline-body" style="margin-right:80px;">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2007.jpg" alt="" style="margin-top:20px;">
                                        </p>
                                    </div>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="margin-top:50px;">
                                        <h4 class="subheading">成立生活房屋</h4>
                                    </div>

                                    <div class="timeline-body">
                                        <h4 class="subheading"><br></h4>
                                    </div>

                                </div>
                            </li>

                            <!-- 2005 -->
                            <li>
                                <div class="timeline-image year-1">2005</div>
                                <!-- <img class="img-circle img-responsive" src="/themes/Rent/img/about/2021204/about/1.jpg" alt=""> -->
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="text-align: right;margin-right:80px; margin-top:60px;">
                                        <h4 class="subheading">委託專業廠商訂製租管專用智能鎖</h4>
                                    </div>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-body">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2005.png" alt="">
                                        </p>
                                    </div>
                                </div>

                            </li>

                            <!-- 2002 -->
                            <li>
                                <div class="timeline-image year-1">2002</div>
                                <!-- <img class="img-circle img-responsive" src="/themes/Rent/img/about/2021204/about/1.jpg" alt=""> -->
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="text-align: right; margin-top:-20px;margin-right:70px;">
                                        <img src="/themes/Rent/img/about/2021204/rent/year2002.png" alt="" style="margin-top:20px;">                                        </div>
                                </div>

                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="margin-top:40px;">
                                        <h4 class="subheading">提供迷你倉儲服務</h4>
                                        <h4 class="subheading">便利房東與租客</h4>
                                    </div>
                                </div>
                            </li>

                            <!-- 2000 -->
                            <li>
                                <div class="timeline-image year-1">2000</div>
                                <!-- <img class="img-circle img-responsive" src="/themes/Rent/img/about/2021204/about/1.jpg" alt=""> -->
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="text-align: right; margin-top:40px;margin-right:80px;">
                                        <h4 class="subheading">提供二手傢俱電租借服務</h4>
                                        <h4 class="subheading">解決房東與租客的煩惱</h4>
                                    </div>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-body">
                                        <p>
                                            <img src="/themes/Rent/img/about/2021204/rent/year2000.png" alt="">
                                        </p>
                                    </div>
                                </div>

                            </li>

                            <!-- 1998 -->
                            <li>
                                <div class="timeline-image year-1">1998</div>
                                <!-- <img class="img-circle img-responsive" src="/themes/Rent/img/about/2021204/about/1.jpg" alt=""> -->
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="text-align: right; margin-top:-20px;margin-right:70px;">
                                        <img src="/themes/Rent/img/about/2021204/rent/year1998.png" alt="" style="margin-top:20px;">                                        </div>
                                </div>

                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="margin-top:40px;">
                                        <h4 class="subheading">首創制定租屋點交作業規範</h4>
                                        <h4 class="subheading">保障雙方權益</h4>
                                    </div>
                                </div>
                            </li>

                            <!-- 1997 -->
                            <li>
                                <div class="timeline-image year-1">1997</div>
                                <!-- <img class="img-circle img-responsive" src="/themes/Rent/img/about/2021204/about/1.jpg" alt=""> -->
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="text-align: right;margin-right:80px; margin-top:40px;margin-bottom:120px;">
                                        <h4 class="subheading">生活租屋</h4>
                                        <h4 class="subheading">專責租屋及管理</h4>
                                    </div>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading" style="text-align: right; margin-top:60px;margin-right:80px;">
                                        <h4 class="subheading"></h4>
                                    </div>
                                </div>

                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="container-fluid" id="5">
                <row class="no-gutters">
                    <a href="#">
                        <img src="/themes/Rent/img/about/2021204/group/p3.png" alt="" width="100%" style="border-radius: 0px;">
                    </a>
                </row>
            </div>

            <section class="center m60" id="6">

                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td>
                                <a href="https://www.facebook.com/桃園市租任管理服務業從業人員職業工會-600320820503706 " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_01.jpg " alt="桃園市租任管理服務業從業人員職業工會 ">
                                </a>
                            </td>
                            <td>
                                <a href="https://www.cathay-ins.com.tw/insurance " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_02.jpg " alt="國泰產險 ">
                                </a>
                            </td>
                            <td>
                                <a href="http://www.tycrehda.org.tw/ " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_03.jpg " alt="桃園市樂安居住宅發展促進協會 ">
                                </a>
                            </td>
                            <td>
                                <a href="http://www.pcss.com.tw/ " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_04.jpg " alt="保成務業保全 ">
                                </a>
                            </td>
                            <td>
                                <a href="https://www.hoan.com.tw/ " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_05.jpg " alt="和安保險 ">
                                </a>
                            </td>
                            <td>
                                <a href="# " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_11.png " alt="台灣樂安居 ">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.hoan.com.tw/ " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_07.jpg " alt="僑馥建經 ">
                                </a>
                            </td>
                            <td>
                                <a href="http://www.tbm.com.tw/ " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_08.png " alt="桃園市公寓大廈管理維護商業公會 ">
                                </a>
                            </td>
                            <td>
                                <a href="http://lawyeratticus.com/ " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_09.png " alt="鄭三川律師 ">
                                </a>
                            </td>
                            <td>
                                <a href="https://www.facebook.com/cesst24650448/" target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_12.png " alt="誠鷹物業 ">
                                </a>
                            </td>
                            <td>
                                <a href="http://www.da-i.com.tw/ " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_06.jpg " alt="大愛搬家 ">
                                </a>
                            </td>
                            <td>
                                <a href="https://www.twincn.com/item.aspx?no=42800193 " target="_blank ">
                                    <img src="/themes/Rent/img/about/2021204/logos/welcome_company_13.png " alt="翔鷹物業">
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </section>

            <footer>
            <!-- 聯絡我們 -->
            <section class="contact " id="7">
                <div class="container ">
                    <div class="row ">
                        <div class="col-lg-6 text-center ">
                            <h2 class="section-heading ">聯絡我們</h2>
                            <h3 class="section-subheading text-muted ">免付費專線|0800-010-568</h3>
                            <!-- <h3 class="section-subheading text-muted ">行動|0987-010-880</h3> -->
                            <h3 class="section-subheading text-muted ">電話|(03)358-1095</h3>
                            <h3 class="section-subheading text-muted ">傳真|(03)358-1095</h3>
                            <h3 class="section-subheading text-muted ">電郵|okrent@lifegroup.house</h3>
                            <h3 class="section-subheading text-muted ">地址|330-71桃園市桃園區新埔八街113號</h3>
                            <br><br>
                        </div>
                        <div class="col-lg-6 ">

                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.3819474167626!2d121.29609951467222!3d25.021108944962823!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34681fb738d97c5b%3A0xf6a4b6cfb3b1cdcd!2zMzMw5qGD5ZyS5biC5qGD5ZyS5Y2A5paw5Z-U5YWr6KGXMTEz6Jmf!5e0!3m2!1szh-TW!2stw!4v1606716224043!5m2!1szh-TW!2stw
                        " width="320px " height="260px " frameborder="1 " allowfullscreen=" " aria-hidden="false " tabindex="0 " style="border-top-left-radius: 50px;border-bottom-right-radius: 50px;"></iframe>


                        </div>

                    </div>
                </div>
            </section>


        </footer>
        <div class="gototop" style="display: block;">TOP</div>

<?php echo '<script'; ?>
>
    <?php echo $_smarty_tpl->tpl_vars['js']->value;?>

    <?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
            $(function() {

                $(window).scroll(function() {
                    //var $(window).scrollTop(); 為 scroll
                    var scroll = $(window).scrollTop();

                    if (scroll >= 70) {
                        // 當卷軸超過70px,.gototop就淡入，如果小於就淡出
                        $(".gototop").fadeIn();

                    } else {

                        $(".gototop").fadeOut();

                    }
                });
                // 當我按下.gototop時，添加動畫讓卷軸跑道最上面
                $('.gototop').click(function() {
                    $('html,body').animate({
                        scrollTop: $('html').offset().top
                    })

                    return false;
                });

            })
        <?php echo '</script'; ?>
>
<?php }
}
