<?php
/* Smarty version 3.1.28, created on 2021-01-25 14:16:18
  from "/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/main_menu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_600e62320946a9_05404899',
  'file_dependency' => 
  array (
    'f02f2f4d8d00b4f60794e4ff0136b46dd2319496' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/NumberHouse/main_menu.tpl',
      1 => 1611552677,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_600e62320946a9_05404899 ($_smarty_tpl) {
?>
<div class="head_div">
  <div class="col-sm-3 l"><img src="/themes/Rent/img/1786/logo.png"></div>
  <div class="col-sm-5 m">
    <div class="row tab">
      <select><option>出租</option><option>出售</option></select>
      <label><a>中古屋</a></label>
      <label><a>新建案</a></label>
      <label><a>店辦</a></label>
      <label><a>廠房|倉庫 </a></label>
      <label><a>土地</a></label>
      <label><a>新聞資訊</a></label></div>
    <div class="row search">
      <select class="city" onchange="cell_window_append_cityurl(this.value)">
        <option disabled="" selected="">桃園市</option>
        <option data-content="台北市" class="county_0" value="county_0">台北市</option><option data-content="基隆市" class="county_1" value="county_1">基隆市</option><option data-content="新北市" class="county_2" value="county_2">新北市</option><option data-content="連江縣" class="county_3" value="county_3">連江縣</option><option data-content="宜蘭縣" class="county_4" value="county_4">宜蘭縣</option><option data-content="新竹市" class="county_5" value="county_5">新竹市</option><option data-content="新竹縣" class="county_6" value="county_6">新竹縣</option><option data-content="桃園市" class="county_7" value="county_7">桃園市</option><option data-content="苗栗縣" class="county_8" value="county_8">苗栗縣</option><option data-content="台中市" class="county_9" value="county_9">台中市</option><option data-content="彰化縣" class="county_10" value="county_10">彰化縣</option><option data-content="南投縣" class="county_11" value="county_11">南投縣</option><option data-content="嘉義市" class="county_12" value="county_12">嘉義市</option><option data-content="嘉義縣" class="county_13" value="county_13">嘉義縣</option><option data-content="雲林縣" class="county_14" value="county_14">雲林縣</option><option data-content="台南市" class="county_15" value="county_15">台南市</option><option data-content="高雄市" class="county_16" value="county_16">高雄市</option><option data-content="澎湖縣" class="county_17" value="county_17">澎湖縣</option><option data-content="金門縣" class="county_18" value="county_18">金門縣</option><option data-content="屏東縣" class="county_19" value="county_19">屏東縣</option><option data-content="台東縣" class="county_20" value="county_20">台東縣</option><option data-content="花蓮縣" class="county_21" value="county_21">花蓮縣</option></select>
      <input type="text" placeholder="請輸入社區或街道名稱">
      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16 fa-3x"><path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" class=""></path></svg>
    </div>
  </div>
  <div class="col-sm-4 r">
    <label class="b_right_line"><a>登入</a></label>
    <label class="b_right_line"><a>註冊</a></label>
    <label><a>免費刊登</a></label>
    <label class="no_width"><a><img style="width: 18pt;" class="share" src="/img/share-alt-solid-777.svg"></a></label>
    <label class="no_width"><a><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve" style="fill: #777;width: 42pt;">
                    <g>
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,21.992h97.498c2.363,0,4.296,1.933,4.296,4.509l0,0c0,2.578-1.934,4.512-4.296,4.512H9.75c-2.363,0-4.295-1.934-4.295-4.512   l0,0C5.455,23.925,7.387,21.992,9.75,21.992z"></path>
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,85.773h97.498c2.363,0,4.296,2.147,4.296,4.511l0,0c0,2.576-1.934,4.725-4.296,4.725H9.75c-2.363,0-4.295-2.149-4.295-4.725   l0,0C5.455,87.921,7.387,85.773,9.75,85.773z"></path>
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,53.774h97.498c2.363,0,4.296,2.15,4.296,4.726l0,0c0,2.362-1.934,4.51-4.296,4.51H9.75c-2.363,0-4.295-2.147-4.295-4.51l0,0   C5.455,55.924,7.387,53.774,9.75,53.774z"></path>
                    </g>
          <g>
            <defs>
              <path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"></path>
            </defs>
            <clipPath id="SVGID_2_">
              <use xlink:href="#SVGID_51_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"></rect>
            </defs>
            <clipPath id="SVGID_4_">
              <use xlink:href="#SVGID_81_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"></rect>
            </defs>
            <clipPath id="SVGID_6_">
              <use xlink:href="#SVGID_105_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"></rect>
            </defs>
            <clipPath id="SVGID_8_">
              <use xlink:href="#SVGID_129_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"></rect>
            </defs>
            <clipPath id="SVGID_10_">
              <use xlink:href="#SVGID_155_" overflow="visible"></use>
            </clipPath>
          </g>
          <g>
            <defs>
              <rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"></rect>
            </defs>
            <clipPath id="SVGID_12_">
              <use xlink:href="#SVGID_183_" overflow="visible"></use>
            </clipPath>
          </g>
          <image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
          </image>
                    </svg></a></label>
  </div>
</div>
<?php }
}
