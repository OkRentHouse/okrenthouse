<?php
/* Smarty version 3.1.28, created on 2021-01-21 15:36:59
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/house_item_list_a.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60092f1ba4d722_89324841',
  'file_dependency' => 
  array (
    '1af890e00efa49f6a5fd37e38ccc98ece66039c2' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/house/house_item_list_a.tpl',
      1 => 1611214617,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60092f1ba4d722_89324841 ($_smarty_tpl) {
?>
<!-- 溫馨居家〜2-3房 -->
<div class="commodity">
	<div class="photo_row">
		<a href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['v']->value['id_rent_house'];?>
">
			<img src="<?php echo $_smarty_tpl->tpl_vars['v']->value['img'];?>
" class="img" alt="">
			<collect data-id="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_rent_house'];?>
" data-type="rent_house" class="favorite <?php if ($_smarty_tpl->tpl_vars['v']->value['favorite']) {?>on<?php }?>"><?php if ($_smarty_tpl->tpl_vars['v']->value['favorite']) {?>
					<img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg"><?php }?>
		</a>
		<price><?php echo $_smarty_tpl->tpl_vars['v']->value['rent_cash'];?>

			<unit>元</unit>
		</price>
	</div><data>
		<h3 class="title big"><a href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['v']->value['id_rent_house'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['case_name'];?>
</a></h3>
		<h3 class="title big_2"><a href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['v']->value['id_rent_house'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</a></h3>
		<div class="title_2"><img src="/themes/Rent/img/icon/icons8-address-50.png"><?php echo $_smarty_tpl->tpl_vars['v']->value['rent_address'];?>
</div>
		<div class="title_4">
		<?php if (($_smarty_tpl->tpl_vars['v']->value['room'] == '' || $_smarty_tpl->tpl_vars['v']->value['room'] == '0') && ($_smarty_tpl->tpl_vars['v']->value['hall'] == '' || $_smarty_tpl->tpl_vars['v']->value['hall'] == '0')) {?>開放空間
		<?php } else { ?>
			<?php if ($_smarty_tpl->tpl_vars['v']->value['room'] == '' || $_smarty_tpl->tpl_vars['v']->value['room'] == '0') {
} else {
echo $_smarty_tpl->tpl_vars['v']->value['room'];?>
房<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['v']->value['hall'] == '' || $_smarty_tpl->tpl_vars['v']->value['hall'] == '0') {
} else {
echo $_smarty_tpl->tpl_vars['v']->value['hall'];?>
廳<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['v']->value['muro'] == '' || $_smarty_tpl->tpl_vars['v']->value['muro'] == '0') {
} else {
echo $_smarty_tpl->tpl_vars['v']->value['muro'];?>
室<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['v']->value['kitchen'] == '' || $_smarty_tpl->tpl_vars['v']->value['kitchen'] == '0') {
} else {
echo $_smarty_tpl->tpl_vars['v']->value['kitchen'];?>
廚<?php }?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value['bathroom'] == '' || $_smarty_tpl->tpl_vars['v']->value['bathroom'] == '0') {
} else {
echo $_smarty_tpl->tpl_vars['v']->value['bathroom'];?>
衛<?php }?>
			<!--<i></i>--> &nbsp &nbsp &nbsp <span class="title_4_pin"><?php echo $_smarty_tpl->tpl_vars['v']->value['ping'];?>
坪</span></div>
	</data>
</div>
<?php }
}
