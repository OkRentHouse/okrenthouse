<?php
/* Smarty version 3.1.28, created on 2021-02-01 13:35:33
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Store/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_601793253dc0f1_48237452',
  'file_dependency' => 
  array (
    '624faf776938176919b652c566fd70a61acb514f' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/Store/content.tpl',
      1 => 1612157719,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../pagination.tpl' => 1,
  ),
),false)) {
function content_601793253dc0f1_48237452 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./good_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <div class="special_store_wrap" style="margin: auto;">
        <div class="banner" style="width: 90%;margin: auto;text-align: center;">
            <img class="" src="/themes/Rent/img/store/banner_1210.png">
        </div>

        <div class="roof" style="width: 90%;text-align: center;">

          <div class="b_padding title_banner" style="width: 100%;margin: auto;">
              <form action="#" method="get" id="search_box_big">
                  <div class="search_list_wrap">
                      <div class="search_div">
                          
                          <div class="city_select">
                              <spam class="county_city search">區域<i class="fas fa-chevron-down"></i></spam>
                          </div>
                          <div class="county_div" style="display: none;">
                              <?php
$_from = $_smarty_tpl->tpl_vars['select_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_county_0_saved_item = isset($_smarty_tpl->tpl_vars['county']) ? $_smarty_tpl->tpl_vars['county'] : false;
$__foreach_county_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['county'] = new Smarty_Variable();
$__foreach_county_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_county_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['county']->value) {
$__foreach_county_0_saved_local_item = $_smarty_tpl->tpl_vars['county'];
?><spam class="county"><input type="radio" name="county" id="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['county']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['county']->value['value'] == $_GET['county']) {?>checked<?php }?>><label for="county_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['county']->value['title'];?>
</label></spam><?php
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_local_item;
}
}
if ($__foreach_county_0_saved_item) {
$_smarty_tpl->tpl_vars['county'] = $__foreach_county_0_saved_item;
}
if ($__foreach_county_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_county_0_saved_key;
}
?>
                          </div>
                          <div class="city_div" style="display: none;"><?php
$_from = $_smarty_tpl->tpl_vars['arr_city']->value[$_GET['county']];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_city_1_saved_item = isset($_smarty_tpl->tpl_vars['city']) ? $_smarty_tpl->tpl_vars['city'] : false;
$__foreach_city_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['city'] = new Smarty_Variable();
$__foreach_city_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_city_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['city']->value) {
$__foreach_city_1_saved_local_item = $_smarty_tpl->tpl_vars['city'];
?><spam class="city"><input type="checkbox" name="city[]" id="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['city']->value;?>
" <?php if (in_array($_smarty_tpl->tpl_vars['city']->value,$_GET['city'])) {?>checked<?php }?>><label for="city_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['city']->value;?>
</label></spam><?php
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_local_item;
}
}
if ($__foreach_city_1_saved_item) {
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_1_saved_item;
}
if ($__foreach_city_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_city_1_saved_key;
}
?></div>
                      </div>

                      <div class="search_div">
                          <div class="class_select" id="id_store_type_name">類別<i class="fas fa-chevron-down"></i></div>
                          <div class="category none">
                              <input type="radio" id="id_store_type_all" value=""<?php if (count($_GET['id_store_type']) == 0) {?> checked<?php }?>><label for="id_store_type_all" id="id_store_type">全部</label><?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_types_2_saved_item = isset($_smarty_tpl->tpl_vars['types']) ? $_smarty_tpl->tpl_vars['types'] : false;
$__foreach_types_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['types'] = new Smarty_Variable();
$__foreach_types_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_types_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['types']->value) {
$__foreach_types_2_saved_local_item = $_smarty_tpl->tpl_vars['types'];
?><input type="checkbox" name="id_store_type[]" id="id_store_type_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['types']->value['id_store_type'],$_GET['id_store_type'])) {?>checked<?php }?>><label for="id_store_type_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
" id="store_type_name_<?php echo $_smarty_tpl->tpl_vars['types']->value['id_store_type'];?>
"><?php echo $_smarty_tpl->tpl_vars['types']->value['store_type'];?>
</label><?php
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_2_saved_local_item;
}
}
if ($__foreach_types_2_saved_item) {
$_smarty_tpl->tpl_vars['types'] = $__foreach_types_2_saved_item;
}
if ($__foreach_types_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_types_2_saved_key;
}
?>
                          </div>
                      </div>


                      <div class="search_input"><input type="text" class="field" name="search" id="search" value="<?php echo $_GET['search'];?>
" placeholder="可輸入關鍵字">
                      </div>
  
  
  
                      

                      <button class="search_icon" type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">

                        <path style="fill: #666;" class="st0" d="M20.71,18.8l-6.01-6.01c-0.25-0.16-0.58-0.16-0.82,0.08l-0.17,0.17l-1.97-1.92c2.02-2.56,1.87-6.25-0.49-8.61  C9.93,1.27,8.29,0.62,6.64,0.62c-1.73,0-3.37,0.66-4.61,1.97c-2.55,2.55-2.55,6.66,0,9.21C3.27,13.04,5,13.7,6.64,13.7  c1.39,0,2.83-0.5,3.99-1.44l1.94,1.94l-0.16,0.16c-0.25,0.25-0.25,0.58-0.08,0.82l6.09,6.01c0.16,0.16,0.58,0.16,0.82-0.08l1.4-1.48  C20.96,19.37,20.96,19.04,20.71,18.8z M10.02,10.49c-0.9,0.99-2.14,1.4-3.37,1.4c-1.23,0-2.47-0.41-3.37-1.32  c-1.89-1.89-1.89-4.94,0-6.75c0.9-0.99,2.14-1.4,3.37-1.4c1.23,0,2.39,0.41,3.37,1.32C11.83,5.63,11.83,8.68,10.02,10.49z"/>
                        </svg>
                      </button>

                      

                      <button class="search_icon" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" viewBox="0 0 25 22" style="enable-background:new 0 0 25 22;" xml:space="preserve">

                      <g>
                      	<polygon class="st0" points="0.5,1.8 8.5,5 8.5,13.08 0.5,9.88  "/>
                      	<polygon class="st0" points="0.5,9.8 8.5,13 8.5,21.07 0.5,17.87  "/>
                      	<polygon class="st0" points="16.5,1.85 8.5,5.03 8.5,13.03 16.5,9.85  "/>
                      	<polygon class="st0" points="16.5,9.85 8.5,13.02 8.5,21.02 16.5,17.85  "/>
                      	<polygon class="st0" points="16.5,1.85 24.5,5.03 24.5,13.03 16.5,9.85  "/>
                      	<polygon class="st0" points="16.5,9.85 24.5,13.02 24.5,21.02 16.5,17.85  "/>
                      	<path class="st0" d="M16.6,7.55c1.3,0,2.38,1.08,2.38,2.45c0,1.3-1.08,2.38-2.38,2.38c-1.37,0-2.45-1.08-2.45-2.38   C14.15,8.63,15.23,7.55,16.6,7.55z"/>
                      	<path class="st1" d="M16.6,8.56c0.79,0,1.37,0.65,1.37,1.44s-0.58,1.37-1.37,1.37c-0.79,0-1.44-0.58-1.44-1.37   S15.81,8.56,16.6,8.56z"/>
                      </g>
                      </svg>
                      </a></button>

                  </div>
              </form>
              <?php echo '<script'; ?>
>
                  arr_city = <?php echo json_encode($_smarty_tpl->tpl_vars['arr_city']->value,1);?>
;
                  <?php echo $_smarty_tpl->tpl_vars['js']->value;?>

              <?php echo '</script'; ?>
>

          </div>

            <img src="/themes/Rent/img/store/roof.jpg" alt="" class="">
        </div>
        <div class="store_list_wrap" style="width: 90%;margin: auto;">
            <ul class="">
                <?php
$_from = $_smarty_tpl->tpl_vars['arr_store']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_store_3_saved_item = isset($_smarty_tpl->tpl_vars['store']) ? $_smarty_tpl->tpl_vars['store'] : false;
$__foreach_store_3_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['store'] = new Smarty_Variable();
$__foreach_store_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_store_3_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['store']->value) {
$__foreach_store_3_saved_local_item = $_smarty_tpl->tpl_vars['store'];
?>
                    <li class="">
                        <div class="image_frame">
                            <div class="img"><a href="/StoreIntroduction?id=<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><img src="<?php if (!empty($_smarty_tpl->tpl_vars['store']->value['img'])) {
echo $_smarty_tpl->tpl_vars['store']->value['img'];
} else {
echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/default.jpg<?php }?>"></a><div class="favorite <?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?>on<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
" data-type="store"><?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg"><?php }?></div></div>
                        </div>
                        <div class="content_wrap">
                            <div class="caption"><?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
<i></i><span class=""><?php echo $_smarty_tpl->tpl_vars['storestore_type']->value;?>
</span></div>
                            <div class="list">
                                <div class="hot">Hot</div>
                                <i></i>
                                <div class="hot_list"><?php
$_from = $_smarty_tpl->tpl_vars['store']->value['service'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_service_4_saved_item = isset($_smarty_tpl->tpl_vars['service']) ? $_smarty_tpl->tpl_vars['service'] : false;
$__foreach_service_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['service'] = new Smarty_Variable();
$__foreach_service_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_service_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['service']->value) {
$__foreach_service_4_saved_local_item = $_smarty_tpl->tpl_vars['service'];
?><samp><?php echo $_smarty_tpl->tpl_vars['service']->value;?>
</samp><?php
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_4_saved_local_item;
}
}
if ($__foreach_service_4_saved_item) {
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_4_saved_item;
}
if ($__foreach_service_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_service_4_saved_key;
}
?></div>
                            </div>
                            <div class="list">
                                <div class="offer">Offer</div>
                                <i></i>
                                <?php
$_from = $_smarty_tpl->tpl_vars['store']->value['discount'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_discount_5_saved_item = isset($_smarty_tpl->tpl_vars['discount']) ? $_smarty_tpl->tpl_vars['discount'] : false;
$__foreach_discount_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['discount'] = new Smarty_Variable();
$__foreach_discount_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_discount_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['discount']->value) {
$__foreach_discount_5_saved_local_item = $_smarty_tpl->tpl_vars['discount'];
?><a href="/StoreIntroduction?id=<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</a><?php
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_5_saved_local_item;
}
}
if ($__foreach_discount_5_saved_item) {
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_5_saved_item;
}
if ($__foreach_discount_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_discount_5_saved_key;
}
?>
                            </div>
                        </div>
                    </li>
                <?php
$_smarty_tpl->tpl_vars['store'] = $__foreach_store_3_saved_local_item;
}
} else {
?>
                <li class="">
                    查不到相關店家
                </li>
                <?php
}
if ($__foreach_store_3_saved_item) {
$_smarty_tpl->tpl_vars['store'] = $__foreach_store_3_saved_item;
}
if ($__foreach_store_3_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_store_3_saved_key;
}
?>
            </ul>
        </div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
<?php }
}
