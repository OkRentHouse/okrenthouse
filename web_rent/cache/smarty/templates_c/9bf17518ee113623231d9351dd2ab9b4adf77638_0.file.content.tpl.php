<?php
/* Smarty version 3.1.28, created on 2020-12-01 10:16:53
  from "/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fc5a795e538a0_69159078',
  'file_dependency' => 
  array (
    '9bf17518ee113623231d9351dd2ab9b4adf77638' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Rent/controllers/Index/content.tpl',
      1 => 1606598106,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../house/search_house4_index.tpl' => 1,
    'file:../../house/house_item_list.tpl' => 3,
  ),
),false)) {
function content_5fc5a795e538a0_69159078 ($_smarty_tpl) {
if (count($_smarty_tpl->tpl_vars['arr_index_banner']->value)) {?>
	<div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: <?php echo $_smarty_tpl->tpl_vars['index_ratio']->value;?>
">
		<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
			<ul class="uk-slideshow-items">
                <?php
$_from = $_smarty_tpl->tpl_vars['arr_index_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_0_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_0_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img style="width:100%" src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a></li>
                <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_local_item;
}
}
if ($__foreach_banner_0_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_item;
}
if ($__foreach_banner_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_0_saved_key;
}
?>
			</ul>

			<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
				  uk-slideshow-item="previous"></samp>
			<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
				  uk-slideshow-item="next"></samp>
		</div>
		<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
	</div>
<?php }?>

<div class="b_padding title_banner ">
	<h2>吉屋快搜</h2>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/search_house4_index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php if (count($_smarty_tpl->tpl_vars['commodity_data']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>精選推薦</h2>
			</div>
			<div class="commodity_list commodity_data">
                <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
} else {
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_1_saved_key;
}
?>
			</div>
		</div>
    <?php }?>

    <?php if (count($_smarty_tpl->tpl_vars['commodity_data2']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>住宅最新</h2>
			</div>
			<div class="commodity_list commodity_data2">
                <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data2']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
} else {
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_2_saved_key;
}
?>
			</div>
		</div>
    <?php }?>
    <?php if (count($_smarty_tpl->tpl_vars['commodity_data3']->value)) {?>
		<div class="house_row">
			<div class="title_banner">
				<h2>商用最新</h2>
			</div>
			<div class="commodity_list commodity_data3">
                <?php
$_from = $_smarty_tpl->tpl_vars['commodity_data3']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_3_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_3_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../house/house_item_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_local_item;
}
} else {
}
if ($__foreach_v_3_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_item;
}
if ($__foreach_v_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_3_saved_key;
}
?>
			</div>
		</div>
    <?php }?>
</div>
<?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php }
}
