<?php
/* Smarty version 3.1.28, created on 2021-04-28 17:22:26
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60892952051127_92755112',
  'file_dependency' => 
  array (
    '3af74af3a5badd132fd4ef6813615fd4b682d248' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/footer.tpl',
      1 => 1619601740,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:group_link.tpl' => 1,
    'file:../../modules/FloatShare/float_share.tpl' => 1,
    'file:../../../../modules/DesigningFooter/designingfooter.tpl' => 1,
  ),
),false)) {
function content_60892952051127_92755112 ($_smarty_tpl) {
?>
</article>



<?php if ($_smarty_tpl->tpl_vars['group_link']->value == '1') {?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:group_link.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../modules/FloatShare/float_share.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php if ($_smarty_tpl->tpl_vars['display_footer']->value) {?><footer>

    

        

        

    <div class="footer">

        <table>

            <tr>

                <td>

                    <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt1']->value)) {?><div class="footer_txt1"><?php echo $_smarty_tpl->tpl_vars['footer_txt1']->value;?>
</div><?php }?>

                    <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt2']->value)) {?><div class="footer_txt2"><?php echo $_smarty_tpl->tpl_vars['footer_txt2']->value;?>
</div><?php }?>

                    <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt3']->value)) {?><div class="footer_txt3"><?php echo $_smarty_tpl->tpl_vars['footer_txt3']->value;?>
</div><?php }?>

                    <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt0428']->value)) {?><div class="footer_txt0428"><?php echo $_smarty_tpl->tpl_vars['footer_txt0428']->value;?>
</div><?php }?>

                    <div colspan="0" rowspan="0" style="vertical-align: middle;color: #fff;position: absolute;right: 23%;top: 28%;">
                        <a href="/statement" style="color: #fff;font-size: 8pt;">相關聲明</a>
                    </div>

                </td>

                <?php if (!empty($_smarty_tpl->tpl_vars['qrcod_img']->value)) {?>

                <td class="qr_code_td">

                    <a href="<?php if (!empty($_smarty_tpl->tpl_vars['footer_qrcode']->value)) {
echo $_smarty_tpl->tpl_vars['footer_qrcode']->value;
} else {
echo $_smarty_tpl->tpl_vars['qrcod_img']->value;
}?>" target="_blank"
                        class="qrcode"><img src="<?php echo $_smarty_tpl->tpl_vars['qrcod_img']->value;?>
"></a>

                </td><?php }?>

            </tr>

        </table>

    </div>

    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../../../modules/DesigningFooter/designingfooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</footer><?php }?>

<?php
$_from = $_smarty_tpl->tpl_vars['css_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_0_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_0_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>

<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css" />

<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_local_item;
}
}
if ($__foreach_css_uri_0_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_item;
}
if ($__foreach_css_uri_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_0_saved_key;
}
?>

<?php
$_from = $_smarty_tpl->tpl_vars['js_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_1_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_1_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>

<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>

<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_local_item;
}
}
if ($__foreach_js_uri_1_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_item;
}
if ($__foreach_js_uri_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_1_saved_key;
}
?>

</div>

</body>

</html>
<?php }
}
