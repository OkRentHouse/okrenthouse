<?php
/* Smarty version 3.1.28, created on 2020-12-11 19:28:28
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/WealthSharing/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd357dcb87d25_91824551',
  'file_dependency' => 
  array (
    '4d12aa1f71a2fecb5c29f63fd670fdebf61514fd' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Rent/controllers/WealthSharing/content.tpl',
      1 => 1607678604,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd357dcb87d25_91824551 ($_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['breadcrumbs_htm']->value;?>

<div class="wealth_wrap" style="margin: 0 auto;">
    <div class="banner" style="width: 90%;">
        <img class="" src="themes/Rent/img/wealthsharing/banner.jpg">
    </div>
    <div class="content_block_wrap" style="width: 90%;">
        <div class="text_wrap">
            <h1 class="caption">
                <span class="title_h_sec">隨時隨地透過分享 為自己開創多元收入</span>
            </h1>
            <img src="themes/Rent/img/wealthsharing/img_1.png" alt="" class="share">
        </div>
        <div class="image_wrap">
            <img src="themes/Rent/img/wealthsharing/img_1.jpg" alt="" class="">
        </div>
    </div>
    <div class="content_block_wrap " style="width: 90%;">
        <div class="image_wrap">
            <img src="themes/Rent/img/wealthsharing/img_2.jpg" alt="" class="">
        </div>
        <div class="text_wrap">
            <h1 class="caption"><span class="title_h_sec">創造被動式收入的同時 多了淨空的空間</span></h1>			<span class="title_c">
            <p class="text">一時衝動花了大把鈔票買的露營設備…</p>
            <p class="text">只用了一次就束之高閣的健身器材…</p>
            <p class="text">還有寶貝已用不到的嬰兒推車…</p>
            <p class="text">快來把這些佔空間又招灰塵的物品 物盡其用</p>
            <p class="text">可多出空間 還能出租賺取租金</p>
            <p class="text">一舉數得</p>			</span>
        </div>
    </div>
    <div class="content_block_wrap" style="width: 90%;">
        <div class="text_wrap">
            <h1 class="caption">
                <span class="title_h_sec">提供各類裝修服務平台</span>
            </h1>			<span class="title_c">
            <p class="text">只要分享平台訊息 就能為自己創造永續收入</p>
            <p class="text">歡迎多才多藝的你變身達人發揮所長</p>
            <p class="text">增加多元收入 </p>			</span>
        </div>
        <div class="image_wrap">
            <img src="themes/Rent/img/wealthsharing/img_3.jpg" alt="" class="" style="width: 100%;">
        </div>
    </div>
    <div class="content_block_wrap " style="width: 90%;">
        <div class="image_wrap">
            <img src="themes/Rent/img/wealthsharing/img_4.jpg" alt="" class="" style="width: 100%;">
        </div>
        <div class="text_wrap">
            <h1 class="caption"><span class="title_h_sec">各類兼職工作媒合服務平台</span></h1>			<span class="title_c">
            <p class="text">只要分享平台訊息 </p>
            <p class="text">就能為自己創造永續收入</p>
            <p class="text">歡迎加入特工行列 展現多采職涯</p>
            <p class="text">拓展人脈 豐富多元收入</p>			</span>
        </div>
    </div>
    <div class="content_block_wrap" style="width: 90%;">
        <div class="text_wrap">
            <h1 class="caption">
                <span class="title_h_sec">生活樂租 創富加盟事業</span>
            </h1>			<span class="title_c">
            <p class="text">無需店鋪 個人即可經營加盟事業</p>
            <p class="text">品牌優勢與獨特服務</p>
            <p class="text">成就您掌握</p>
            <p class="text red bold">５０００億租賃錢潮</p>			</span>
        </div>
        <div class="image_wrap">
            <img src="themes/Rent/img/wealthsharing/img_5.jpg" alt="" class="" style="width: 100%;">
        </div>
    </div>
    <div class="content_block_wrap " style="width: 90%;">
        <div class="image_wrap">
            <img src="themes/Rent/img/wealthsharing/img_6.jpg" alt="" class="" style="width: 100%;">
        </div>
        <div class="text_wrap">
            <h1 class="caption"><span class="title_h_sec">樂租房東投資收益</span></h1>			<span class="title_c">
            <p class="text">創造每月穩定被動收入</p>
            <p class="text">穩收租金</p>
            <p class="text">累積你的人生多桶金</p>			</span>
        </div>
    </div>
</div>
<?php }
}
