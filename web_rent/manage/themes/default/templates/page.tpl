{if isset($tab_breadcrumbs) && count($tab_breadcrumbs) > 0}
<ul>
{foreach from=$tab_breadcrumbs item=tab_bread}
<li class="{$tab_bread.class}">{if isset($tab_bread.href)}<a href="{$tab_bread.href}">{/if}{$tab_bread.title}{if isset($tab_bread.href)}</a>{/if}</li>
{/foreach}
</ul>
{/if}