<div class="tab-count for_group_tab tabs_group_{$group['id_group']} col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="{$group.id_group}">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="icon-cogs"></i>{l s="賦予帳號管理"}</div>
		<div class="panel-body">
			{l s="(管理帳號時可以管理的群組)"}
			<table>
				<thead>
				<tr class="h5" data-id_tab="all" data-id_parent="all">
					<td></td><td><label>{*<input type="checkbox" class="on_ajax all_give" data-id_group="{$group['id_group']}" value="all"{if false} checked="checked"{/if}>全部*}</label></td>
				</tr>
				</thead>
				<tbody>
				{foreach from=$arr_group key=j item=group2}
					{if $group2.id_group != 1}
						<tr class="h6" data-give="{$group2.id_group}">
							<td>{$group2.name}</td><td><label><input type="checkbox" class="on_ajax2 type_give" value="give"{if in_array($group2.id_group, $arr_give_grupe[$group.id_group])} checked="checked"{/if}></label></td>
						</tr>
					{/if}
				{/foreach}
				</tbody>
			</table>
		</div>
	</div>
</div>