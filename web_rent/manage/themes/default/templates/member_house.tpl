<form method="post" name="member" id="member" class="defaultForm form-horizontal" novalidate="novalidate">
<div class="panel panel-default tab-count tabs_houses_warranty">
<div class="panel-heading">
<i class="icon-cogs">
</i>房屋資訊</div> <div class="panel-body">
<div class="form-wrapper col-lg-12">
<div class=" form-group hide">
	<input type="hidden" name="id_member" id="id_member" value="">
</div>
<div class=" form-group hide">
	<input type="hidden" name="id_houses" id="id_houses" value="">
</div>
<div class=" form-group">
	<label class="control-label col-lg-3" for="id_company">公司名稱</label>
	<div class="col-lg-3">
		<select name="id_company" class=" form-control" id="id_company">
			<option value="">請選擇公司名稱</option>
		</select>
	</div>
</div>
<div class=" form-group">
	<label class="control-label col-lg-3" for="id_build_case">建案名稱</label>
	<div class="col-lg-3">
		<select name="id_build_case" class=" form-control" id="id_build_case">
			<option value="">請選擇建案名稱</option>
			<option value="1" data-parent="2" data-parent_name="id_company">大和經典</option>
		</select>
	</div>
</div>
<div class=" form-group">
<label class="control-label col-lg-3" for="house_code">戶別代號</label>
<div class="col-lg-3">
<input type="text" id="house_code" name="house_code" class="form-control " value="" maxlength="10">
</div>
</div>
</div>
</div>
	<div class="panel-footer">
		<button type="submit" value="1" id="member_form_submit_btn" name="submitAddhouses_warranty" class="btn btn-default pull-right"> <i class="icon glyphicon glyphicon-floppy-disk"></i>儲存</button>
		<a href="/manage/Member?" class="btn btn-default"><i class="icon glyphicon glyphicon-remove"></i>取消</a><button type="reset" id="member_form_reset_btn" class="btn btn-default">
		<i class="icon glyphicon glyphicon-refresh"></i>復原</button>
	</div>
</div>
<input type="hidden" name="submitAddmamber_houses" value="1">
</form>