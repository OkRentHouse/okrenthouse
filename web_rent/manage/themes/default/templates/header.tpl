<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	{if !empty($t_color)}<meta name="theme-color" content="{$t_color}">{/if}
	{if !empty($mn_color)}<meta name="msapplication-navbutton-color" content="{$mn_color}">{/if}
	{if !empty($amwasb_style)}<meta name="apple-mobile-web-app-status-bar-style" content="{$amwasb_style}">{/if}
	{if !empty($meta_viewport)}<meta name="viewport" content="{$meta_viewport}">{/if}
	<title>{if isset($meta_title)}{$meta_title}{$NAVIGATION_PIPE}{/if}{$web_name}</title>
	<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
	<!--[if lt IE 9]>
	<script src="/css/html5shiv.min.js"></script>
	<![endif]-->
	{$GoogleAnalytics}
	{include file="$tpl_dir./table_title.tpl"}
	{foreach from=$css_files key=key item=css_uri}
		<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
	{/foreach}
	{if !isset($display_header_javascript) || $display_header_javascript}
		<script type="text/javascript">
			var display  = '{$display}';
			var THEME_URL = '{$THEME_URL}';
			var ADMIN_THEME_URL = '{$ADMIN_THEME_URL}';
		</script>
		{hook h='displayAdminJavaScript'}
	{foreach from=$js_files key=key item=js_uri}
		<script type="text/javascript" src="{$js_uri}"></script>
	{/foreach}
	{if !$ENTER_SUBMIT}
		<script type="text/javascript">
			$(document).keypress(function(e) {
				if(e.which == 13) {
					return false;
				}
			});
		</script>
	{/if}
	{if count($_errors_name) > 0}
		<script type="text/javascript">
			$(document).ready(function(e) {
				{foreach from=$_errors_name item=err_name}
				{if count($_errors_name_i.$err_name) > 0}
				{foreach from=$_errors_name_i.$err_name item=ei}
				$('[name="{$err_name}"]').eq({$ei}).parents('.form-group').addClass('has-error');
				$('[name="{$err_name}"]').eq({$ei}).parent('td').addClass('has-error');
				$('[name="{$err_name}"][type="checkbox"]').eq({$ei}).parent('label').addClass('has-error');
				$('[name="{$err_name}"][type="checkbox"]').eq({$ei}).focus(function(){
					$(this).parent('label').removeClass('has-error');
				});
				$('[name="{$err_name}"]').focus(function(){
					$('[name="{$err_name}"]').eq({$ei}).parents('.form-group').removeClass('has-error');
					$('[name="{$err_name}"]').eq({$ei}).parent('td').removeClass('has-error');
				});
				{/foreach}
				{else}
				$('[name="{$err_name}"]').parents('.form-group').addClass('has-error');
				$('[name="{$err_name}"]').parent('td').addClass('has-error');
				$('[name="{$err_name}"][type="checkbox"]').parent('label').addClass('has-error');
				$('[name="{$err_name}"]').focus(function(){
					$('[name="{$err_name}"]').parents('.form-group').removeClass('has-error');
					$('[name="{$err_name}"]').parent('td').removeClass('has-error');
					$('[name="{$err_name}"][type="checkbox"]').parent('label').removeClass('has-error');
				});
				{/if}
				{/foreach}
			});
		</script>
	{/if}
		<script type="text/javascript">
			$(document).ready(function(e) {
				$('#{if isset($fields.form.id)}{$fields.form.id}{elseif isset($fields.form.name)}{$fields.form.name}{else}{$table}{/if}').validate({
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block'
				});
			});
		</script>
	{/if}
</head>
{if $display_header}{* ↓有 header 的版型↓ *}
<body class="{if $menu_close}menu_close {$menu_close}{/if}">
{hook h='displayAdminBodyBefore'}
	{$minbar}
	<div id="main">
		{$admin_menu}
		<div id="content" class="{$admin_url}">
			{if isset($page_header_toolbar) && $display_page_header_toolbar}{$page_header_toolbar}{/if}
			<div class="content no-floa row">
				{$msg_box}
				{* ↑有 header 的版型↑ *}{else}{* ↓沒 header 的版型↓ *}
				<body>
					<div class="minbar col-lg-12"></div>
					<div id="main">
						<div id="content" class="row {$admin_url}">
							{$msg_box}
{* ↑沒 header 的版型↑ *}{/if}