<?php

namespace web_okrent;
use \FrontController;
use \OkrentController;
use \Db;

class List32Controller extends OkrentController
{

    public $page = 'index';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->meta_title                = $this->l('首頁');
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'List32Controller';
        $this->no_FormTable              = true;
//        $this->display_footer            = false;
        parent::__construct();
    }

    public function initProcess()
    {
        $ohter_proudct = [
            [
                'name'=>'掃地機器人',
                'title'=>'組裝簡易內裝組裝簡易內裝組內 組裝簡易內裝組裝簡易內裝組內',
                'img'=>'/themes/Okrent/img/index/1090918_13.png',
            ],
            [
                'name'=>'掃地機器人',
                'title'=>'組裝簡易內裝組裝簡易內裝組內 組裝簡易內裝組裝簡易內裝組內',
                'img'=>'/themes/Okrent/img/index/1090918_13.png',
            ],
        ];


        $product = [
            [
                'name' =>'掃地機器人',
                'title'=>'遠百週年慶滿額送贈品',
                'img'=>'',
                'ID'=>'NO.122544',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'Iphone',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'掃地機器人',
                'title'=>'遠百週年慶滿額送贈品',
                'img'=>'',
                'ID'=>'NO.122544',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'Iphone',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'掃地機器人',
                'title'=>'遠百週年慶滿額送贈品',
                'img'=>'',
                'ID'=>'NO.122544',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'Iphone',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'掃地機器人',
                'title'=>'遠百週年慶滿額送贈品',
                'img'=>'',
                'ID'=>'NO.122544',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'Iphone',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'掃地機器人',
                'title'=>'遠百週年慶滿額送贈品',
                'img'=>'',
                'ID'=>'NO.122544',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'Iphone',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
        ];


        $js =<<<js
<script>
    $(".regular").slick({
        dots: true,
        autoplay : true,
        infinite: true,
        autoplaySpeed: 3000,
        slidesToShow: 6,
        slidesToScroll: 6,
    });
</script>


js;

        $this->context->smarty->assign([
            'ohter_proudct'=>$ohter_proudct,
            'product'=>$product,
            'js'=>$js,
        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
    }
}
