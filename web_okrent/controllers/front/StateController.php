<?php

namespace web_okrent;
use \FrontController;
use \OkrentController;
use \Db;

class StateController extends OkrentController
{
    public $page = 'State';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;
//      public $fb_app_id = '1319773841697579';
//    public $fb_api_version = 'v9.0';


    public function __construct()
    {
        $this->page_header_toolbar_title = $this->l('智慧財產權、隱私權、服務聲明');
        $this->className                 = 'StateController';
        $this->no_FormTable              = true;

//        $this->display_footer            = false;
        parent::__construct();
    }



	public function initProcess()
	{




    $css =<<<js

    <style>

    p {
      width: 100%;
    }

    </style>

js;


		$this->context->smarty->assign([
            'fb_app_id'=>$this->fb_app_id,
            'fb_api_version'=>$this->fb_api_version,
            'css'=>$css,
		]);

		parent::initProcess();

	}





	public function setMedia(){
        $this->removeJS(THEME_URL .'/css/page_left.css');
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
		parent::setMedia();
	}
}
