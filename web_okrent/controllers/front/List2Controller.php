<?php
namespace web_okrent;
use \FrontController;
use \OkrentController;
use \Db;

class List2Controller extends OkrentController
{
    public $page = 'index';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'List2Controller';
        $this->no_FormTable              = true;
//        $this->display_footer            = false;
        parent::__construct();
    }

    public function initProcess()
    {
        $caption_title1 ='樂租物品';
        $caption_data1 = [
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',

            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
        ];

        $caption_title2 ='交換物品';

        $caption_data2 = [
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
            [
                'item_number'=>'L1234567',
                'main_title'=>'健康御守 二氧化氯清淨錠套組',
                'title'=>'消毒殺菌防疫最威殺菌率達99.9%',
                'price_original'=>'2,100',
                'price'=>'1,350',
                'quantity'=>'123',
                'favorite'=>'123',
                'item_id'=>'1',
            ],
        ];

        $js =<<<js

<script>
    $(".regular").slick({
        dots: true,
        autoplay : true,
        infinite: true,
        autoplaySpeed: 3000,
        slidesToShow: 6,
        slidesToScroll: 6,
    });
</script>

js;

        $this->context->smarty->assign([
            'caption_data1' => $caption_data1,
            'caption_title1'=>$caption_title1,
            'caption_data2' => $caption_data2,
            'caption_title2'=>$caption_title2,
            'js'=>$js,
        ]);
        parent::initProcess();
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
    }
}
