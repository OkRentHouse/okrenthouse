<?php
namespace web_okrent;
use \FrontController;
use \OkrentNewController;
use \Db;

class IndexNewController extends OkrentNewController
{
    public $page = 'indexNew';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'IndexNewController';
        $this->no_FormTable              = true;
        $this->display_footer            = false;
        parent::__construct();
        $this->display_header            = false;//首頁不用共用表頭
        $this->display_header_javascript = false;
        $this->display_main_menu = false;
    }

	public function initProcess()
	{
        $js = "";

		$this->context->smarty->assign([
            'caption_data1' => $caption_data1,
		    'caption_title0'=>$caption_title0,
        'caption_title1'=>$caption_title1,
            'caption_data2' => $caption_data2,
            'caption_title2'=>$caption_title2,
            'js'=>$js,
		]);
		parent::initProcess();
	}


	public function setMedia(){
        $this->removeJS(THEME_URL .'/css/all.css');
        $this->removeJS('/css/all.css');
        $this->removeJS('css/all.css');
        $this->removeJS('all.css');
		parent::setMedia();
	}
    /**
	public function setMedia(){

        parent::setMedia();

        $this->removeJS(THEME_URL .'/css/all.css');

        //parent::setMedia();

        $this->removeJS(THEME_URL .'/css/page_left.css');
        $this->removeJS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->removeJS(THEME_URL .'/css/main.css');
        $this->removeJS(THEME_URL .'/css//not_mobile_font.css');
		
        //$this->controller_name = '';
	}
    **/
}
