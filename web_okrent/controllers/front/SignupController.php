<?php

namespace web_okrent;

use \FrontController;
use \OkrentController;
use \Db;
use\LineApi;
use \Tools;
class SignupController extends OkrentController
{
    public $page = 'Signup';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;
//      public $fb_app_id = '1319773841697579';
//    public $fb_api_version = 'v9.0';


    public function __construct()
    {
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'SignupController';
        $this->no_FormTable              = true;
//        $this->display_footer            = false;
        parent::__construct();
        $this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
    }



	public function initProcess()
	{
        if(!empty($_GET['code']) && !empty($_GET['state'])){


        }



		$this->context->smarty->assign([
            'fb_app_id'=>$this->fb_app_id,
            'fb_api_version'=>$this->fb_api_version,
		]);

		// parent::initProcess();

	}

    public function displayAjax()
    {
        $action = Tools::getValue('action');

        switch($action){
            case 'line_registered':
                $line_get_authorize = [
                    'response_type'=>'code',
                    'client_id'=>'1655314652',
                    'redirect_uri'=>'https://shar.help//Signup',
                ];
                $line_get_authorize['state'] = 'test';//自己產生 之後要處理
                $line_get_authorize['nonce']= 'signup';//加在後面
                echo json_encode(LineApi::Authorization($line_get_authorize));
                break;
            default :
                echo json_encode("no");
                break;
        }
        exit;
    }




	public function setMedia(){
        $this->removeJS(THEME_URL .'/css/page_left.css');
		parent::setMedia();
	}
}

