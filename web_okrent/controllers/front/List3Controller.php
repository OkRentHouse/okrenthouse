<?php
namespace web_okrent;
use \FrontController;
use \OkrentController;
use \Db;
use \Tools;

class List3Controller extends OkrentController
{
    public $page = 'index';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'List3Controller';
        $this->no_FormTable              = true;
//        $this->display_footer            = false;
        $this->conn = 'ilifehou_okrent';
        parent::__construct();
    }

    public function initProcess()
    {
        $id_main_class = Tools::getValue("id_main_class");
        $county = Tools::getValue("county");
        $city = Tools::getValue("city");
        $id_product_class = Tools::getValue("id_product_class");
        $id_product_class_item = Tools::getValue("id_product_class_item");
        $keyword = Tools::getValue("keyword");

        if(!empty($id_product_class_item)){

            if($id_main_class=='2'){
                $data_go = "";//為了轉址動作
                foreach($_GET as $key => $value){
                    $data_go .= '&'.$key.'='.$value;
                }
                header('Location: /list32?'.$data_go);
                exit;
            }else{
              $data_go = "";//為了轉址動作
              foreach($_GET as $key => $value){
                  $data_go .= '&'.$key.'='.$value;
              }
              header('Location: /list2?'.$data_go);
              exit;
            }
        }

        $area_1 = [
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
        ];

        $area_2 = [
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_13.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_13.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_13.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'全功能切割機',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_13.png'
            ],
        ];

        $area_3 = [
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'智慧型測量儀',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'智慧型測量儀',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'智慧型測量儀',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
            [
                'product_number' =>'A123456',
                'county' =>'桃園市',
                'city' =>'桃園區',
                'name'=>'智慧型測量儀',
                'subtitle'=>'用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!',
                'img'=>'/themes/Okrent/img/index/1090918_12.png'
            ],
        ];


        if($id_main_class=='1' ||  $id_main_class=='2' || $id_main_class=='3'){
            $area_title_1 = 'New';
            $area_title_2 = 'Hot';
            $area_title_3 = 'Wish';
            $area_title_4 = '';
        }else if($id_main_class=='4'){
            $area_title_1 = '想要租';
            $area_title_2 = '想要換';
            $area_title_3 = '待援助';
            $area_title_4 = '最新願望';
        }



        $this->context->smarty->assign([
            'area_title_1'=>$area_title_1,
            'area_title_2'=>$area_title_2,
            'area_title_3'=>$area_title_3,
            'area_1'=>$area_1,
            'area_2'=>$area_2,
            'area_3'=>$area_3,
            'area_title_4'=>$area_title_4,
        ]);
        parent::initProcess();
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
    }
}
