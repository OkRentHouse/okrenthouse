<?php

namespace web_okrent;
use \FrontController;
use \OkrentController;
use \Db;

class List31Controller extends OkrentController
{

    public $page = 'index';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        $this->meta_title                = $this->l('首頁');
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'List31Controller';
        $this->no_FormTable              = true;
//        $this->display_footer            = false;
        parent::__construct();
    }

    public function initProcess()
    {
        $ohter_proudct = [
            [
                'name'=>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組內 組裝簡易內裝組裝簡易內裝組內',
                'img'=>'/themes/Okrent/img/index/1090918_13.png',
            ],
            [
                'name'=>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組內 組裝簡易內裝組裝簡易內裝組內',
                'img'=>'/themes/Okrent/img/index/1090918_13.png',
            ],
        ];


        $product = [
            [
                'name' =>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'img'=>'',
                'unit'=>'三人',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'戶外立式鐵板烤肉架',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'img'=>'',
                'unit'=>'三人',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'戶外立式鐵板烤肉架',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'img'=>'',
                'unit'=>'三人',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'戶外立式鐵板烤肉架',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'img'=>'',
                'unit'=>'三人',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'戶外立式鐵板烤肉架',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
            [
                'name' =>'露營帳篷',
                'title'=>'組裝簡易內裝組裝簡易內裝組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'img'=>'',
                'unit'=>'三人',
                'country'=>'桃園市',
                'member_name'=>'白白屋',
                'like'=>'38',
                'great'=>'5.0',
                'exchange_name'=>'戶外立式鐵板烤肉架',
                'exchange_title'=>'組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝 組裝簡易內裝組裝簡易內裝',
                'exchange_img'=>'',
            ],
        ];


        $js =<<<js
<script>
    $(".regular").slick({
        dots: true,
        autoplay : true,
        infinite: true,
        autoplaySpeed: 3000,
        slidesToShow: 6,
        slidesToScroll: 6,
    });
</script>


js;

        $this->context->smarty->assign([
            'ohter_proudct'=>$ohter_proudct,
            'product'=>$product,
            'js'=>$js,
        ]);
        parent::initProcess();
    }



    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
    }
}
