<?php

namespace web_okrent;

use \FrontController;
use \OkrentController;
use \Db;
use \File;

class ItemController extends OkrentController{

    public $page = 'item';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;

    public function __construct(){

        $this->meta_title                = $this->l('首頁');
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'ItemController';
        $this->no_FormTable              = true;
        $this->conn = 'ilifehou_okrent';
//        $this->display_footer            = false;




        parent::__construct();
    }

    public function initProcess(){
        //假設的資料
        $data = [
            'id_product'=>'1',
            'product_number'=>'HR-123456',
            'id_main_class'=>'1',
            'main_class'=>[
                'id_main_class'=>'2',
                'title'=>'交換趣',
                'position'=>'2',
                'active'=>'1',
            ],
            'id_product_class'=>'6',
            'id_product_class_item'=>'93',
            'id_pay'=>'["1","4","6","7","8","9","10"]',
            'id_delivery'=>'["1","2","3","4"]',
            'name'=>'3人露營帳篷',
            'title'=>'3人露營帳篷',
            'subtitle'=>'輕鋼架材質輕巧易組裝輕鋼架材質輕巧易組裝輕鋼架材質輕巧易組裝輕鋼架',
            'content'=>'',
            'rent_five'=>'800',
            'rent_ten'=>'900',
            'rent_fifteenth'=>'1000',
            'rent_thirty'=>'1200',
            'deposit'=>'1600',
            'unit'=>'頂',
            'id_county'=>'7',
            'id_city'=>'3',
            'address'=>'桃園市桃園區新埔八街113號',
            'status'=>'1',
            'create_time'=>'2020-12-01',
            'update_time'=>'2020-12-03',
            'id_member'=>'2',
            'link_product_number'=>'',
            'url'=>[
                '0'=>'/themes/Okmaster/demo/1090922_01.png',
                '1'=>'/themes/Okmaster/demo/1090922_02.png',
                '2'=>'/themes/Okmaster/demo/1090922_03.png',
                '3'=>'/themes/Okmaster/demo/1090922_06.png',
            ],
        ];
        $data_number_format = array('rent_five','rent_ten','rent_fifteenth','rent_thirty','deposit');
        foreach($data_number_format as $v){
            $data[$v] = number_format($data[$v]);//千位數加入,
        }


//$this->conn
        //這邊先暫用到時候有資料會搬去class一併做處理
        $sql = "SELECT * FROM {$this->conn}.product_class WHERE active=1 AND id_product_class=".GetSQL($data["id_product_class"],"int");
        $product_class = Db::rowSQL($sql,true);
        $data['product_class'] = $product_class;

        $sql = "SELECT * FROM {$this->conn}.product_class_item WHERE active=1 AND id_product_class_item=".GetSQL($data["id_product_class_item"],"int");

        $product_class_item = Db::rowSQL($sql,true);
        $data['product_class_item'] = $product_class_item;

        $sql = "SELECT * FROM county WHERE id_county=".GetSQL($data["id_county"],"int");
        $county = Db::rowSQL($sql,true);
        $data['county'] = $county;

        $sql = "SELECT * FROM city WHERE id_city=".GetSQL($data["id_city"],"int");
        $city = Db::rowSQL($sql,true);
        $data['city'] = $city;

        $sql = "SELECT * FROM {$this->conn}.pay WHERE active=1 AND id_pay IN(".Db::antonym_array($data['id_pay']).") ORDER BY position ASC";
        $pay = Db::rowSQL($sql);
        $data['pay'] = $pay;

        $sql = "SELECT * FROM {$this->conn}.delivery as d
        LEFT JOIN {$this->conn}.delivery_file as d_f ON d.`id_delivery` = d_f.`id_delivery`
        WHERE d.`active`=1 AND d.`id_delivery` IN(".Db::antonym_array($data['id_delivery']).")  ORDER BY d.`position` ASC";
        $delivery = Db::rowSQL($sql);
        foreach($delivery as $k=>$v){
            if(!empty($v['id_file'])){//取得file url
                $img = File::get($delivery[$k]['id_file']);
                $delivery[$k]['url'] = $img["url"];//塞進去圖片
            }
        }
        $data['delivery'] = $delivery;



        $this->context->smarty->assign([
            'data'=>$data,
        ]);

        parent::initProcess();

    }



    public function setMedia(){
        parent::setMedia();
        $this->addCSS('/css/metro-all.min.css');
        $this->addJS('/js/metro.min.js');
        $this->addCSS(THEME_URL. '/css/item1211.css');


    }
}
