<?php

namespace web_okrent;

use \FrontController;
use \OkrentController;
use \Db;

class LoginController extends OkrentController{
    public $page = 'Login';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;
    public function __construct(){

        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'LoginController';
        $this->no_FormTable              = true;
        $this->display_footer            = false;
//        $this->display_footer            = false;
        parent::__construct();
        $this->display_header            = false;//首頁不用共用表頭

    }



	public function initProcess(){



		$this->context->smarty->assign([

		]);

		// parent::initProcess();
	}



	public function setMedia(){

		parent::setMedia();
	}
}

