<?php
/* Smarty version 3.1.28, created on 2020-12-11 15:35:54
  from "/home/ilifehou/life-house.com.tw/themes/Okrent/controllers/Item/content_1.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd3215a70c5f2_10678011',
  'file_dependency' => 
  array (
    '3e145f5048e920279d917e2ebb3b779f42d69ee3' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Okrent/controllers/Item/content_1.tpl',
      1 => 1607672146,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd3215a70c5f2_10678011 ($_smarty_tpl) {
?>

<!-- <div class="top_more"><span class="more">挖寶趣 | 交換趣 | 環保捐贈 | 願望清單</span></div> -->

<div class="containerx"> 
  <div class="row item">
    <div class="photo">
      <div class="row">
        <div class="picture border"></div>
        <img class="heart" src="/themes/LifeHouse/img/lifego/heart.svg" alt="">
      </div>
      <div class="row minipictures">
        <div class="minipicture border"></div>
        <div class="minipicture border"></div>
        <div class="minipicture border"></div>
        <div class="minipicture border"></div>
      </div>
    </div>
    <div class="info">
      <span class="title"><?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
</span><span style="display: flex;margin-left: -2rem;" class="heart"><div class="heart_in"></div><img src="/themes/LifeHouse/img/lifego/heart.svg" alt="">388</span>
      <span class="title_vice">
        <?php echo $_smarty_tpl->tpl_vars['data']->value['subtitle'];?>

      </span>
      <span class="class_name"><?php echo $_smarty_tpl->tpl_vars['data']->value['product_class']['title'];?>
</span>
      <span class="price">$<span class="HighLight rent_price">800</span>元<div class="rent_day"><img src="\themes\Okrent\img\list\clock.svg"><span class="rent_time">5日</span>|<span class="rent_time">10日</span>|<span class="rent_time">15日</span>|<span class="rent_time">30日</span></div></span>
      <span class="price_o">$<?php echo $_smarty_tpl->tpl_vars['data']->value['deposit'];?>
元</span>
      <span class="payment"><?php
$_from = $_smarty_tpl->tpl_vars['data']->value['pay'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
if ($_smarty_tpl->tpl_vars['k']->value != 0) {?>・<?php }
echo $_smarty_tpl->tpl_vars['v']->value['name'];
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?></span>
      <span class="delivery"><?php
$_from = $_smarty_tpl->tpl_vars['data']->value['delivery'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
if ($_smarty_tpl->tpl_vars['v']->value['url']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['v']->value['url'];?>
"><?php } elseif ($_smarty_tpl->tpl_vars['v']->value['icon']) {?><i class="<?php echo $_smarty_tpl->tpl_vars['v']->value['icon'];?>
"></i><?php } else {
}
echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
 <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_1_saved_key;
}
?></span>
      <div class="type_icon">
        <img src="\themes\Okrent\img\list\ch_icon.png">
        <img src="\themes\Okrent\img\list\calendar.png">
      </div>
      <span class="user_info"><div class="photo"><img src="\themes\Okrent\img\list\head.svg"></div>白白屋 5.8<img class="thumbsup" src="\themes\Okrent\img\list\thumbsup.svg"> <?php echo $_smarty_tpl->tpl_vars['data']->value['county']['county_name'];
echo $_smarty_tpl->tpl_vars['data']->value['city']['city_name'];?>
</span>
    </div>
  </div>
  <div class="border item_introduce row">
  </div>
  <div class="row message_title">
    <span>留言板</span>
  </div>
  <div class="row arranged border">
    <div class="user">小凱</div><label class="line"></label><div class="command">有附睡袋嗎?</div>
  </div>
</div>
<?php }
}
