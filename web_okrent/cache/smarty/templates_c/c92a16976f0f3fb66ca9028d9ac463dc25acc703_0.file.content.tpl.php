<?php
/* Smarty version 3.1.28, created on 2020-12-10 13:07:25
  from "/home/ilifehou/life-house.com.tw/themes/Okrent/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd1ad0da43666_74184812',
  'file_dependency' => 
  array (
    'c92a16976f0f3fb66ca9028d9ac463dc25acc703' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Okrent/controllers/Index/content.tpl',
      1 => 1607576842,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd1ad0da43666_74184812 ($_smarty_tpl) {
?>
<div class="t_container top">
    <div class="row index">
        <div class="fn_bar">
          <div class="l">
            <a href="/Login">登入</a> | <a href="/Signup">註冊</a>
          </div><div class="R">
            <!-- <img class="share" src="/themes/Okrent/img/index/share.png"> -->

            <img class="share" src="/img/share-alt-solid-w.svg">
            <img class="hb" src="/themes/Okrent/img/index/hb.png">
            <!--<i class="fas fa-share-alt"></i>-->

          </div>
        </div>

        <?php $_smarty_tpl->tpl_vars['arr_index_banner'] = new Smarty_Variable(array("1091204_1_banner_1.JPG","1091030_1_banner_2.JPG","1091210_1_banner_2.JPG"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'arr_index_banner', 0);?>
        <div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: 1903:1070">
      		<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
      			<ul class="uk-slideshow-items">
                      <?php
$_from = $_smarty_tpl->tpl_vars['arr_index_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_0_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_0_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
      					<li><a href="####"><img src="/themes/Okrent/img/index/<?php echo $_smarty_tpl->tpl_vars['banner']->value;?>
"></a></li>
                      <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_local_item;
}
}
if ($__foreach_banner_0_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_item;
}
if ($__foreach_banner_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_0_saved_key;
}
?>
      			</ul>

      			<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
      				  uk-slideshow-item="previous"></samp>
      			<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
      				  uk-slideshow-item="next"></samp>
      		</div>
      		<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
      	</div>


    </div>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./top_fn.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</div>
<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
$_smarty_tpl->tpl_vars['array_imgs'] = new Smarty_Variable(array("Subtraction_1.png","Subtraction_2.png","Subtraction_1_v.png","Subtraction_2_w.png","Subtraction_1_x.png","Subtraction_2_y.png"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'array_imgs', 0);
$_smarty_tpl->tpl_vars['array_icons'] = new Smarty_Variable(array("<img class='icons' src='/themes/Okrent/img/index/Group_14.png'>",'',"<div id='exchange_icon'><div id='Group_53'><svg class='Ellipse_46'><ellipse id='Ellipse_46' rx='7.645463466644287' ry='7.645463466644287' cx='7.645463466644287' cy='7.645463466644287'></ellipse></svg><svg class='Path_652' viewBox='-273.656 -1510.64 51.428 15.316'><path id='Path_652' d='M -222.2285461425781 -1496.489990234375 C -222.2285461425781 -1496.489990234375 -224.5710144042969 -1510.414306640625 -249.1690368652344 -1510.637939453125 C -273.7671813964844 -1510.862060546875 -273.6562805175781 -1495.324462890625 -273.6562805175781 -1495.324462890625'></path></svg></div><div id='Group_54'><svg class='Ellipse_46_bp'><ellipse id='Ellipse_46_bp' rx='7.645463466644287' ry='7.645463466644287' cx='7.645463466644287' cy='7.645463466644287'></ellipse></svg><svg class='Path_652_bq' viewBox='-273.656 -1510.64 51.428 15.316'><path id='Path_652_bq' d='M -273.6562805175781 -1509.474853515625 C -273.6562805175781 -1509.474853515625 -271.3138732910156 -1495.550537109375 -246.7157897949219 -1495.326904296875 C -222.1177062988281 -1495.102783203125 -222.2285461425781 -1510.640380859375 -222.2285461425781 -1510.640380859375'></path></svg></div></div>","<img class='icons' src='/themes/Okrent/img/index/Group_221.png'>","<img class='icons' src='/themes/Okrent/img/index/Group_72.png'>","<img class='icons_1' src='/themes/Okrent/img/index/Group_615.png'><img class='icons_2' src='/themes/Okrent/img/index/Group_615.png'>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'array_icons', 0);
$_smarty_tpl->tpl_vars['array_txt'] = new Smarty_Variable(array("<span>租用</span><br> <div class='content'>何必買 租就好<br> <b>以租代買</b> 分享經濟效益最大化<br></div>","<div class='content' style='margin-bottom: -25px;'>出租閒置物品<br><span>輕鬆創造被動式收入</span><br> 還能多了淨空空間<br> </div>","<span>交換趣</span><br>  <div class='content'>享受舊愛變新歡的新奇樂趣^以物易物 多種效益</div>","<span>物友圈</span>^<div class='content'>以物會友 拓展社交圈^志趣相投 豐富心視野</div>","<span>公益捐贈</span><br> <div class='content'>扶助弱勢做公益^^將用不到的物資 發揮愛心^送給需要扶助的社福團體|勢族群</div>","<span>物盡其用</span><br> <div class='content'>愛護地球做環保^^用不到的 放著佔地方又積灰塵 丟了可惜! ^何不給需要它的人?</div>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'array_txt', 0);?>

<div class="t_container mid">
    <div class="process_1">
        <img src="/themes/Okrent/img/index/1090918_03.png">
        <div class="process_2">
            <ul>
              <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                <div class="r0 txt l"><?php echo str_replace('^','<br>',$_smarty_tpl->tpl_vars['array_txt']->value[$_smarty_tpl->tpl_vars['i']->value]);?>
</div><div class="r0 r"><img src="/themes/Okrent/img/index/<?php echo $_smarty_tpl->tpl_vars['array_imgs']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['array_icons']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</div></li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>

                <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                  <div class="r1 l"><img src="/themes/Okrent/img/index/<?php echo $_smarty_tpl->tpl_vars['array_imgs']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['array_icons']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</div><div class="r1 txt r"><?php echo str_replace('^','<br>',$_smarty_tpl->tpl_vars['array_txt']->value[$_smarty_tpl->tpl_vars['i']->value]);?>

                        </div></li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>

                <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                  <div class="r2 txt l"><?php echo str_replace('^','<br>',$_smarty_tpl->tpl_vars['array_txt']->value[$_smarty_tpl->tpl_vars['i']->value]);?>
</div><div class="r2 r"><img src="/themes/Okrent/img/index/<?php echo $_smarty_tpl->tpl_vars['array_imgs']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['array_icons']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</div></li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>

                <li class="open_url" onclick="window.open('/list3?id_main_class=2','_blank ')">
                  <div class="r3 l"><img src="/themes/Okrent/img/index/<?php echo $_smarty_tpl->tpl_vars['array_imgs']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['array_icons']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</div><div class="r3 txt r"><?php echo str_replace('^','<br>',$_smarty_tpl->tpl_vars['array_txt']->value[$_smarty_tpl->tpl_vars['i']->value]);?>
</div></li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>

                <li class="open_url" onclick="window.open('/list3?id_main_class=3','_blank ')">
                  <div class="r4 txt l"><?php echo str_replace('^','<br>',$_smarty_tpl->tpl_vars['array_txt']->value[$_smarty_tpl->tpl_vars['i']->value]);?>
</div><div class="r4 r"><img src="/themes/Okrent/img/index/<?php echo $_smarty_tpl->tpl_vars['array_imgs']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['array_icons']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</div></li><?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>

                <li><div class="r5 l"><img src="/themes/Okrent/img/index/<?php echo $_smarty_tpl->tpl_vars['array_imgs']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['array_icons']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</div><div class="r5 txt r"><?php echo str_replace('^','<br>',$_smarty_tpl->tpl_vars['array_txt']->value[$_smarty_tpl->tpl_vars['i']->value]);?>
</div></li>

                </ul>
        </div>
    </div>

    <div class="process_3">
        <div class="title_txt">租用三部曲</div>
        <div class="content">
            <img src="/themes/Okrent/img/index/1091027-2.png">
            <div class="txt c_1"><label>挑選 | 預約</label><br>超強搜尋功能協助找到<br>最適合你的<br>預約使用期間</div>
            <div class="txt c_2"><label>簽約 | 點收</label><br>完成租賃單<br>面交點收租賃物<br>即能開始享用</div>
            <div class="txt c_3"><label>歸還 | 退押</label><br>享用後準時歸還<br>退還押金<br>讓大家都能共享</div>
        </div>
    </div>
</div>

<div class="t_container bot">
    <div class="rows_1">
        <div class="content">
            <div class="title_txt">
                <img src="/themes/Okrent/img/index/1090918_10_01.png">
                <span><?php echo $_smarty_tpl->tpl_vars['caption_title0']->value;?>
</span>
            </div>

            <div class="news_row">
                <img src="/themes/Okrent/img/index/1090918_10_02.png">



                <?php $_smarty_tpl->tpl_vars['l_titel'] = new Smarty_Variable(array("<h5><b>露營帳篷</b></h5>","品牌<label></label>","物主<label></label>","租金<label></label>","租期<label></label>","可租日<label></label>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'l_titel', 0);?>
                <?php $_smarty_tpl->tpl_vars['l_c'] = new Smarty_Variable(array('','',"白白屋",'',"1日租金|7日租金 15日租金|30日租金","2020-11-10"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'l_c', 0);?>
                <?php $_smarty_tpl->tpl_vars['r_titel'] = new Smarty_Variable(array("瀏覽次數<label></label>","規格型號<label></label>","<i class='far fa-thumbs-up'></i> 5.0","位置<label></label>",'',''), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'r_titel', 0);?>
                <?php $_smarty_tpl->tpl_vars['r_c'] = new Smarty_Variable(array("38",'','',"桃園市",'',''), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'r_c', 0);?>
                <?php $_smarty_tpl->tpl_vars['l_c_s'] = new Smarty_Variable(array('','','','',"position: absolute;margin-top: -15px;","position: absolute;margin-top: -15px;"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'l_c_s', 0);?>

                <?php $_smarty_tpl->tpl_vars['l_titel'] = new Smarty_Variable(array("<h4><b>露營帳篷</b></h4>","<b>品牌</b><label></label>","<b>租金</b><label></label>",'','',''), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'l_titel', 0);?>
                <?php $_smarty_tpl->tpl_vars['l_c'] = new Smarty_Variable(array('','','','','',''), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'l_c', 0);?>
                <?php $_smarty_tpl->tpl_vars['r_titel'] = new Smarty_Variable(array('',"<b>規格型號</b><label></label>","<b>位置</b><label></label>",'','',''), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'r_titel', 0);?>
                <?php $_smarty_tpl->tpl_vars['r_c'] = new Smarty_Variable(array('','',"桃園市",'','',''), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'r_c', 0);?>
                <?php $_smarty_tpl->tpl_vars['l_c_s'] = new Smarty_Variable(array('','','','','',''), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'l_c_s', 0);?>

            <section class="slider regular">
                <?php
$_from = $_smarty_tpl->tpl_vars['caption_data1']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_1_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_1_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                    <div class="list">
                        <div class="img_frame">
                          <div class="heart">
                                    <a><i class="far fa-heart"></i></a>
                                </div>
                          <a href="List?item_id=<?php echo $_smarty_tpl->tpl_vars['value']->value['item_id'];?>
"><img src="/themes/Okrent/img/index/1090918_12.png"></a></div>
                        <div class="content_frame">

                          <div class="infor">

                            <ul>


                  <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['l_titel']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['l_titel']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>

                              <li>
                              <ul class="row row_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
 p_name">
                                <li class="l"><span style="<?php echo $_smarty_tpl->tpl_vars['l_titel_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"></span></li>
                                <li class="c_l"><span style="<?php echo $_smarty_tpl->tpl_vars['l_c_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['l_titel']->value[$_smarty_tpl->tpl_vars['i']->value];
echo $_smarty_tpl->tpl_vars['l_c']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</span></li>
                                <li class="r"><span style="<?php echo $_smarty_tpl->tpl_vars['r_titel_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"></span></li>
                                <li class="c_r"><span style="<?php echo $_smarty_tpl->tpl_vars['r_c_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['r_titel']->value[$_smarty_tpl->tpl_vars['i']->value];
echo $_smarty_tpl->tpl_vars['r_c']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</span></li>
                              </ul>
                              </li>
                   <?php }
}
?>


                            </ul>

                          </div>

                            <!--<div style="" class="heart_div">
                                <div class="number"><?php echo $_smarty_tpl->tpl_vars['value']->value['item_number'];?>
</div>
                                <div class="heart">
                                    <img src="/themes/LifeHouse/img/lifego/heart.svg" alt="" style="width: 20px;">
                                    <div class="quantity"><?php echo $_smarty_tpl->tpl_vars['value']->value['favorite'];?>
</div>
                                </div>
                            </div>
                            <div class="caption"><a href="List"><?php echo $_smarty_tpl->tpl_vars['value']->value['main_title'];?>
</a></div>
                            <div class="caption2"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</div>
                            <div class="heart_wrap">
                                <div class="price_original">原價 <span class="outer"><span class="inner"><?php echo $_smarty_tpl->tpl_vars['value']->value['price_original'];?>
日/</span></span>元</div>
                                <div class="text-center price">
                                  <div class="text-center"><span>$ <?php echo $_smarty_tpl->tpl_vars['value']->value['price'];?>
</span>/日</div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_local_item;
}
}
if ($__foreach_value_1_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_item;
}
if ($__foreach_value_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_1_saved_key;
}
?>
            </section>
            <span> <a href="####"><</a> <a href="####">1</a> &nbsp <a href="####">2</a> &nbsp <a href="####">3</a> <a href="####">></a> </span>
            </div>
        </div>
    </div>

    <div class="rows_2">
            <div class="content">
                <div class="title_txt">
                    <img src="/themes/Okrent/img/index/1090918_10_01.png">
                    <span><?php echo $_smarty_tpl->tpl_vars['caption_title1']->value;?>
</span></div>
                <div class="news_row">
                    <img src="/themes/Okrent/img/index/1090918_10_02.png">
                    <section class="slider regular">
                        <?php
$_from = $_smarty_tpl->tpl_vars['caption_data1']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_2_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_2_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_2_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                            <div class="list">
                                <div class="img_frame">
                                  <div class="heart">
                                            <a><i class="far fa-heart"></i></a>
                                        </div>
                                  <a href="List?item_id=<?php echo $_smarty_tpl->tpl_vars['value']->value['item_id'];?>
"><img src="/themes/Okrent/img/index/1090918_13.png"></a></div>
                                <div class="content_frame">

                                  <div class="infor">

                                    <ul>


                          <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['l_titel']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < sizeof($_smarty_tpl->tpl_vars['l_titel']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>

                                      <li>
                                      <ul class="row row_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
 p_name">
                                        <li class="l"><span style="<?php echo $_smarty_tpl->tpl_vars['l_titel_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"></span></li>
                                        <li class="c_l"><span style="<?php echo $_smarty_tpl->tpl_vars['l_c_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['l_titel']->value[$_smarty_tpl->tpl_vars['i']->value];
echo $_smarty_tpl->tpl_vars['l_c']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</span></li>
                                        <li class="r"><span style="<?php echo $_smarty_tpl->tpl_vars['r_titel_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"></span></li>
                                        <li class="c_r"><span style="<?php echo $_smarty_tpl->tpl_vars['r_c_s']->value[$_smarty_tpl->tpl_vars['i']->value];?>
"><?php echo $_smarty_tpl->tpl_vars['r_titel']->value[$_smarty_tpl->tpl_vars['i']->value];
echo $_smarty_tpl->tpl_vars['r_c']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</span></li>
                                      </ul>
                                      </li>
                           <?php }
}
?>


                                    </ul>

                                  </div>

                                    <!--<div style="" class="heart_div">
                                        <div class="number"><?php echo $_smarty_tpl->tpl_vars['value']->value['item_number'];?>
</div>
                                        <div class="heart">
                                            <img src="/themes/LifeHouse/img/lifego/heart.svg" alt="" style="width: 20px;">
                                            <div class="quantity"><?php echo $_smarty_tpl->tpl_vars['value']->value['favorite'];?>
</div>
                                        </div>
                                    </div>
                                    <div class="caption"><a href="List"><?php echo $_smarty_tpl->tpl_vars['value']->value['main_title'];?>
</a></div>
                                    <div class="caption2"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</div>
                                    <div class="heart_wrap">
                                      <div class="price_original">原價 <span class="outer"><span class="inner"><?php echo $_smarty_tpl->tpl_vars['value']->value['price_original'];?>
日/</span></span>元</div>
                                      <div class="text-center price">
                                        <div class="text-center"><span>$ <?php echo $_smarty_tpl->tpl_vars['value']->value['price'];?>
</span>/日</div>
                                      </div>
                                    </div>-->
                                </div>
                            </div>
                        <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_local_item;
}
}
if ($__foreach_value_2_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_item;
}
if ($__foreach_value_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_2_saved_key;
}
?>
                    </section>
                    <span> <a href="####"><</a> <a href="####">1</a> &nbsp <a href="####">2</a> &nbsp <a href="####">3</a> <a href="####">></a> </span>
                </div>
            </div>
        </div>

    <div class="rows_3">
      <i class="fas fa-chevron-right"></i>
      <i class="fas fa-chevron-left"></i>
        <div class="content">
            <div class="title_txt">
                <span>使用分享</span>
            </div>
            <div class="news_row">
                <ul class="content_ul">
                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_14.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>
                    <li class="content_li">
                        <ul class="content_2">
                            <li><img src="/themes/Okrent/img/index/1090918_15.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="rows_4">
        <div class="content">
            <div class="title_txt">
                <span>最新消息</span>
            </div>
            <div class="news_row">
                <ul class="content_ul">
                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_16.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>

                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_17.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>

                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_18.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>

                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_19.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
    <?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php }
}
