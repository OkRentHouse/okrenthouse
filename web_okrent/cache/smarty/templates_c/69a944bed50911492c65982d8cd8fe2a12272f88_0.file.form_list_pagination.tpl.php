<?php
/* Smarty version 3.1.28, created on 2021-03-12 14:31:14
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okrent/form/form_list_pagination.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_604b0ab26483b4_24646432',
  'file_dependency' => 
  array (
    '69a944bed50911492c65982d8cd8fe2a12272f88' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okrent/form/form_list_pagination.tpl',
      1 => 1607678506,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_604b0ab26483b4_24646432 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['form_list_pagination_max']->value > 1) {?>
<div class="text-center">
<nav class="visible-lg-block">
<ul class="pagination pagination-lg">
  <li<?php if ($_GET['p'] <= 1) {?> class="disabled"<?php }?>><a href="?p=<?php echo max($_GET['p']-1,1);?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
  <?php
$_smarty_tpl->tpl_vars['pagination_p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['pagination_p']->step = 1;$_smarty_tpl->tpl_vars['pagination_p']->total = (int) min(ceil(($_smarty_tpl->tpl_vars['pagination_p']->step > 0 ? $_smarty_tpl->tpl_vars['form_list_pagination_max']->value+1 - ($_smarty_tpl->tpl_vars['form_list_pagination_min']->value) : $_smarty_tpl->tpl_vars['form_list_pagination_min']->value-($_smarty_tpl->tpl_vars['form_list_pagination_max']->value)+1)/abs($_smarty_tpl->tpl_vars['pagination_p']->step)),$_smarty_tpl->tpl_vars['form_list_pagination_num']->value);
if ($_smarty_tpl->tpl_vars['pagination_p']->total > 0) {
for ($_smarty_tpl->tpl_vars['pagination_p']->value = $_smarty_tpl->tpl_vars['form_list_pagination_min']->value, $_smarty_tpl->tpl_vars['pagination_p']->iteration = 1;$_smarty_tpl->tpl_vars['pagination_p']->iteration <= $_smarty_tpl->tpl_vars['pagination_p']->total;$_smarty_tpl->tpl_vars['pagination_p']->value += $_smarty_tpl->tpl_vars['pagination_p']->step, $_smarty_tpl->tpl_vars['pagination_p']->iteration++) {
$_smarty_tpl->tpl_vars['pagination_p']->first = $_smarty_tpl->tpl_vars['pagination_p']->iteration == 1;$_smarty_tpl->tpl_vars['pagination_p']->last = $_smarty_tpl->tpl_vars['pagination_p']->iteration == $_smarty_tpl->tpl_vars['pagination_p']->total;?>
  <li<?php if (($_GET['p'] == $_smarty_tpl->tpl_vars['pagination_p']->value) || (($_smarty_tpl->tpl_vars['pagination_p']->value == 1) && ($_GET['p'] <= 1)) || (($_smarty_tpl->tpl_vars['pagination_p']->value == $_smarty_tpl->tpl_vars['form_list_pagination_max']->value) && ($_GET['p'] >= $_smarty_tpl->tpl_vars['form_list_pagination_max']->value))) {?> class="active"<?php }?>><a href="?p=<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
<span class="sr-only">(current)</span></a></li>
  <?php }
}
?>

  <li<?php if (($_GET['p'] >= $_smarty_tpl->tpl_vars['form_list_pagination_max']->value) || ($_GET['p'] == $_smarty_tpl->tpl_vars['form_list_pagination_max']->value)) {?> class="disabled"<?php }?>><a href="?p=<?php echo min($_GET['p']+1,$_smarty_tpl->tpl_vars['form_list_pagination_max']->value);?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
</ul>
</nav>
<nav class="visible-md-block">
<ul class="pagination">
  <li<?php if ($_GET['p'] <= 1) {?> class="disabled"<?php }?>><a href="?p=<?php echo max($_GET['p']-1,1);?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
  <?php
$_smarty_tpl->tpl_vars['pagination_p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['pagination_p']->step = 1;$_smarty_tpl->tpl_vars['pagination_p']->total = (int) min(ceil(($_smarty_tpl->tpl_vars['pagination_p']->step > 0 ? $_smarty_tpl->tpl_vars['form_list_pagination_max']->value+1 - ($_smarty_tpl->tpl_vars['form_list_pagination_min']->value) : $_smarty_tpl->tpl_vars['form_list_pagination_min']->value-($_smarty_tpl->tpl_vars['form_list_pagination_max']->value)+1)/abs($_smarty_tpl->tpl_vars['pagination_p']->step)),$_smarty_tpl->tpl_vars['form_list_pagination_num']->value);
if ($_smarty_tpl->tpl_vars['pagination_p']->total > 0) {
for ($_smarty_tpl->tpl_vars['pagination_p']->value = $_smarty_tpl->tpl_vars['form_list_pagination_min']->value, $_smarty_tpl->tpl_vars['pagination_p']->iteration = 1;$_smarty_tpl->tpl_vars['pagination_p']->iteration <= $_smarty_tpl->tpl_vars['pagination_p']->total;$_smarty_tpl->tpl_vars['pagination_p']->value += $_smarty_tpl->tpl_vars['pagination_p']->step, $_smarty_tpl->tpl_vars['pagination_p']->iteration++) {
$_smarty_tpl->tpl_vars['pagination_p']->first = $_smarty_tpl->tpl_vars['pagination_p']->iteration == 1;$_smarty_tpl->tpl_vars['pagination_p']->last = $_smarty_tpl->tpl_vars['pagination_p']->iteration == $_smarty_tpl->tpl_vars['pagination_p']->total;?>
  <li<?php if (($_GET['p'] == $_smarty_tpl->tpl_vars['pagination_p']->value) || (($_smarty_tpl->tpl_vars['pagination_p']->value == 1) && ($_GET['p'] <= 1)) || (($_smarty_tpl->tpl_vars['pagination_p']->value == $_smarty_tpl->tpl_vars['form_list_pagination_max']->value) && ($_GET['p'] >= $_smarty_tpl->tpl_vars['form_list_pagination_max']->value))) {?> class="active"<?php }?>><a href="?p=<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
<span class="sr-only">(current)</span></a></li>
  <?php }
}
?>

  <li<?php if (($_GET['p'] >= $_smarty_tpl->tpl_vars['form_list_pagination_max']->value) || ($_GET['p'] == $_smarty_tpl->tpl_vars['form_list_pagination_max']->value)) {?> class="disabled"<?php }?>><a href="?p=<?php echo min($_GET['p']+1,$_smarty_tpl->tpl_vars['form_list_pagination_max']->value);?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
</ul>
</nav>
<nav class="visible-sm-block visible-xs-block">
<ul class="pagination pagination-sm">
  <li<?php if ($_GET['p'] <= 1) {?> class="disabled"<?php }?>><a href="?p=<?php echo max($_GET['p']-1,1);?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
  <?php
$_smarty_tpl->tpl_vars['pagination_p'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['pagination_p']->step = 1;$_smarty_tpl->tpl_vars['pagination_p']->total = (int) min(ceil(($_smarty_tpl->tpl_vars['pagination_p']->step > 0 ? $_smarty_tpl->tpl_vars['form_list_pagination_max']->value+1 - ($_smarty_tpl->tpl_vars['form_list_pagination_min']->value) : $_smarty_tpl->tpl_vars['form_list_pagination_min']->value-($_smarty_tpl->tpl_vars['form_list_pagination_max']->value)+1)/abs($_smarty_tpl->tpl_vars['pagination_p']->step)),$_smarty_tpl->tpl_vars['form_list_pagination_num']->value);
if ($_smarty_tpl->tpl_vars['pagination_p']->total > 0) {
for ($_smarty_tpl->tpl_vars['pagination_p']->value = $_smarty_tpl->tpl_vars['form_list_pagination_min']->value, $_smarty_tpl->tpl_vars['pagination_p']->iteration = 1;$_smarty_tpl->tpl_vars['pagination_p']->iteration <= $_smarty_tpl->tpl_vars['pagination_p']->total;$_smarty_tpl->tpl_vars['pagination_p']->value += $_smarty_tpl->tpl_vars['pagination_p']->step, $_smarty_tpl->tpl_vars['pagination_p']->iteration++) {
$_smarty_tpl->tpl_vars['pagination_p']->first = $_smarty_tpl->tpl_vars['pagination_p']->iteration == 1;$_smarty_tpl->tpl_vars['pagination_p']->last = $_smarty_tpl->tpl_vars['pagination_p']->iteration == $_smarty_tpl->tpl_vars['pagination_p']->total;?>
  <li<?php if (($_GET['p'] == $_smarty_tpl->tpl_vars['pagination_p']->value) || (($_smarty_tpl->tpl_vars['pagination_p']->value == 1) && ($_GET['p'] <= 1)) || (($_smarty_tpl->tpl_vars['pagination_p']->value == $_smarty_tpl->tpl_vars['form_list_pagination_max']->value) && ($_GET['p'] >= $_smarty_tpl->tpl_vars['form_list_pagination_max']->value))) {?> class="active"<?php }?>><a href="?p=<?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['pagination_p']->value;?>
<span class="sr-only">(current)</span></a></li>
  <?php }
}
?>

  <li<?php if (($_GET['p'] >= $_smarty_tpl->tpl_vars['form_list_pagination_max']->value) || ($_GET['p'] == $_smarty_tpl->tpl_vars['form_list_pagination_max']->value)) {?> class="disabled"<?php }?>><a href="?p=<?php echo min($_GET['p']+1,$_smarty_tpl->tpl_vars['form_list_pagination_max']->value);?>
&<?php echo $_smarty_tpl->tpl_vars['form_list_pagination_url']->value;?>
"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
</ul>
</nav>
</div>
<?php }
}
}
