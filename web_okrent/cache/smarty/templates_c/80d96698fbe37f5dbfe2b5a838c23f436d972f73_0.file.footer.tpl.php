<?php
/* Smarty version 3.1.28, created on 2020-12-30 11:37:53
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Okrent/footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5febf611aec3f6_08444890',
  'file_dependency' => 
  array (
    '80d96698fbe37f5dbfe2b5a838c23f436d972f73' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Okrent/footer.tpl',
      1 => 1609299472,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:group_link.tpl' => 1,
  ),
),false)) {
function content_5febf611aec3f6_08444890 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['group_link']->value == '1') {?>
      <?php if (preg_match("/list/i",FILENAME) == 0 && preg_match("/item/i",FILENAME) == 0) {?>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:group_link.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php }
}?>
</article>
<?php if ($_smarty_tpl->tpl_vars['display_footer']->value) {?>
<footer>
      <div class="footer_0"></div>
      <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt']->value)) {?><div class="footer"><?php echo $_smarty_tpl->tpl_vars['footer_txt']->value;?>
</div><?php }?>
      <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt1']->value)) {?><div class="footer_1"><?php echo $_smarty_tpl->tpl_vars['footer_txt1']->value;?>
</div><?php }?>
      <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt2']->value)) {?><div class="footer_2"><?php echo $_smarty_tpl->tpl_vars['footer_txt2']->value;?>
</div><?php }?>
      <?php if (!empty($_smarty_tpl->tpl_vars['footer_txt3']->value)) {?><div class="footer_3"><?php echo $_smarty_tpl->tpl_vars['footer_txt3']->value;?>
</div><?php }?>
      
</footer>
<?php }?>
      <?php
$_from = $_smarty_tpl->tpl_vars['css_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_0_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_0_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>
            <link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css"/>
      <?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_local_item;
}
}
if ($__foreach_css_uri_0_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_item;
}
if ($__foreach_css_uri_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_0_saved_key;
}
?>
      <?php
$_from = $_smarty_tpl->tpl_vars['js_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_1_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_1_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>
            <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>
      <?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_local_item;
}
}
if ($__foreach_js_uri_1_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_item;
}
if ($__foreach_js_uri_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_1_saved_key;
}
?>
      </div>
      <input type="hidden" value="\themes\Okrent\footer.tpl">
</body>
</html>
<?php }
}
