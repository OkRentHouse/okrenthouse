<?php
/* Smarty version 3.1.28, created on 2020-09-25 19:05:11
  from "/home/ilifehou/life-house.com.tw/themes/Okrent/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f6dcee7010666_43976472',
  'file_dependency' => 
  array (
    'c9e4aa26d3dc2231293d30356cb810ad6c5bca6d' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Okrent/msg_box.tpl',
      1 => 1598257918,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f6dcee7010666_43976472 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
<div class="alert alert-success <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
<div class="alert alert-danger <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
}
}
