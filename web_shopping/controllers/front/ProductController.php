<?php
namespace web_shopping;
use \ShoppingController;
use \FrontController;
use \County;
use \Db;
use \File;
use \Tools;

class ProductController extends ShoppingController
{
    public $page = 'product';


//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('176.shopping');
		$this->page_header_toolbar_title = $this->l('首頁176.shopping');
    $this->meta_description          = '176歡樂購|聰明消費精采生活享紅利回饋,免費刊登,超有感的購物商城千萬別錯過!商品新奇豐富輕鬆選,商城實惠好康享不完,比計本讓你花得少買得好,安心購物的交易全機制,隨時隨地享受樂趣盡在176歡樂購';
    $this->meta_keywords             = '176歡樂購|聰明消費精采生活享紅利回饋,免費刊登,超有感的購物商城千萬別錯過!商品新奇豐富輕鬆選,商城實惠好康享不完,比計本讓你花得少買得好,安心購物的交易全機制,隨時隨地享受樂趣盡在176歡樂購';
		$this->className                 = 'ProductController';
		$this->no_FormTable              = true;

    if(!empty(Tools::getValue("city"))){
        $city_s = "";
        foreach(Tools::getValue("city") as $key => $value){
            $city_s = empty($city_s)?$value:$city_s.','.$value;
        }

        $this->breadcrumbs[] =
            [
                'href'  => '/list?city_s='.$city_s,
                'hint'  => $this->l(''),
                'icon'  => '',
                'title' => $this->l(Tools::getValue("county").' '.$city_s),
            ];
    }

		parent::__construct();

    $this->display_header            = true;//首頁不用共用表頭
	}

	public function initProcess()
	{

    $js =<<<hmtl
    <script>
    $(".commodity_product.commodity_data").slick({
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 4,
    slidesToScroll: 4,

});
</script>
hmtl;

    $commodity_product_photo = array("1","2","2","2","2","2","2","2");
		$this->context->smarty->assign([
            'group_link'    => 1,
            'get_listclass'    => Tools::getValue("class"),
            'form_list_county' => County::getContext()->form_list_county(),
            'commodity_product_photo' => $commodity_product_photo,
            'js'               => $js,
		]);
		parent::initProcess();
	}

	public function setMedia()
	{
		parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS('/js/slick.min.js');
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');
        $this->addCSS('/css/metro-all.css');
		$this->addCSS('/themes/Shopping/css/rwd-index.css');
        $this->addJS('/js/metro.min.js');
        $this->addJS(THEME_URL . '/js/group_link.js');
        $this->addJS(THEME_URL . '/js/search_bar.js');
        $this->addJS(THEME_URL . '/js/hb.js');
        $this->addCSS(THEME_URL . '/css/search_bar.css');
        $this->addJS('/js/lazysizes.min.js');
        $this->addCSS(THEME_URL . '/css/page_all.css');
        $this->addCSS(THEME_URL . '/css/minbar.css');
        $this->addJS(THEME_URL . '/js/head.js');
        $this->addCSS(THEME_URL . '/css/footer.css');
        $this->addCSS(THEME_URL . '/css/index.css');
        $this->addCSS(THEME_URL . '/css/searchitem.css');

	}
}
