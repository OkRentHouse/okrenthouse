<?php
namespace web_shopping;
use \ShoppingController;
use \FrontController;
use \Db;
use \File;

class IndexController extends ShoppingController
{
    public $page = 'index';


//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('176.shopping');
		$this->page_header_toolbar_title = $this->l('首頁176.shopping');
    $this->meta_description          = '176歡樂購|聰明消費精采生活享紅利回饋,免費刊登,超有感的購物商城千萬別錯過!商品新奇豐富輕鬆選,商城實惠好康享不完,比計本讓你花得少買得好,安心購物的交易全機制,隨時隨地享受樂趣盡在176歡樂購';
    $this->meta_keywords             = '176歡樂購|聰明消費精采生活享紅利回饋,免費刊登,超有感的購物商城千萬別錯過!商品新奇豐富輕鬆選,商城實惠好康享不完,比計本讓你花得少買得好,安心購物的交易全機制,隨時隨地享受樂趣盡在176歡樂購';
		$this->className                 = 'IndexController';
		$this->no_FormTable              = true;

		parent::__construct();

    $this->display_header            = true;//首頁不用共用表頭
	}

	public function initProcess()
	{



		$this->context->smarty->assign([
            'group_link'    => 1,
		]);
		parent::initProcess();
	}

	public function setMedia() 
	{
		parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS('/js/slick.min.js');
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');
        $this->addCSS('/css/metro-all.css');
		$this->addCSS('/themes/Shopping/css/rwd-index.css');
        $this->addJS('/js/metro.min.js');
        $this->addJS(THEME_URL . '/js/group_link.js');
        $this->addJS(THEME_URL . '/js/search_bar.js');
        $this->addJS(THEME_URL . '/js/hb.js');
        $this->addCSS(THEME_URL . '/css/search_bar.css');
        $this->addJS('/js/lazysizes.min.js');
        $this->addCSS(THEME_URL . '/css/page_all.css');
        $this->addCSS(THEME_URL . '/css/minbar.css');
        $this->addJS(THEME_URL . '/js/head.js');
        $this->addCSS(THEME_URL . '/css/footer.css');
	}
}
