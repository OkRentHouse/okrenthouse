<?php
/* Smarty version 3.1.28, created on 2021-04-24 14:54:28
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/controllers/Product/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6083c0a47dd869_51348579',
  'file_dependency' => 
  array (
    '3df9b745f0e995ab48b155f7710c4aa256840741' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/controllers/Product/content.tpl',
      1 => 1619247266,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../head_ad_bar.tpl' => 1,
    'file:themes/Shopping/commodity.tpl' => 1,
    'file:tpl/footer_list.tpl' => 1,
  ),
),false)) {
function content_6083c0a47dd869_51348579 ($_smarty_tpl) {
$_smarty_tpl->tpl_vars['brack_title'] = new Smarty_Variable(array("<label>開賣啦</label>","<label>最精選</label>","<label>好新掀</label>","<label>海外代購</label>","<label>推薦</label>","<label>分享讚</label>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'brack_title', 0);
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);
$_smarty_tpl->tpl_vars['page_tab'] = new Smarty_Variable("<ul><li><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='chevron-left' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-chevron-left fa-w-10 fa-3x'><path fill='currentColor' d='M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z' class=''></path></svg></li><li class='action'>1</li><li>2</li><li>3</li><li><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='chevron-right' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-chevron-right fa-w-10 fa-3x'><path fill='currentColor' d='M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z' class=''></path></svg></li></ul>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'page_tab', 0);?>
	<div class="body">

		<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../head_ad_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


		<div class="row p_top">
			<div class="cell-1"></div>
		  <div class="cell-4 p_l">
				<label class="p_title">清淨錠 每盒10錠</label><br>
				<label class="p_sec_title">防疫第一選擇 消毒 殺菌 除臭</label>
			</div>
		  <div class="cell-6" style="border-bottom: 1px solid #000;">
				<div class="row p_r">

					<div class="cell-4 p_number">
						L1234567
					</div>
				  <div class="cell-8 s_stauts">
						<label>5.0 <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 5; $_smarty_tpl->tpl_vars['i']->value++) {
?>
							<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg>
							<?php }
}
?>

						</label>
						<label><span class="p_number">熱銷</span>102</label>
						<label><span class="p_number">庫存</span>38</label>
						<label><span class="p_line">LINE</span></label>
					</div>

				</div>
			</div>
			<div class="cell-1"></div>
		</div>
		<div class="row">
		  <div class="cell-1"></div>
			<div class="cell-4">
				<img src="/themes/Shopping/img/p_main.png">

				<div class="commodity_list commodity_data commodity_product" style="display: inline-block;width: 100%;">
                <?php
$_from = $_smarty_tpl->tpl_vars['commodity_product_photo']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:themes/Shopping/commodity.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
} else {
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_v_0_saved_key;
}
?>
				</div>
				<?php echo $_smarty_tpl->tpl_vars['js']->value;?>


			</div>
		  <div class="cell-6">
				<?php $_smarty_tpl->tpl_vars['item_infor'] = new Smarty_Variable(array("關鍵字","促銷","價錢","數量"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor', 0);?>
				<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["關鍵字"] = array("<a class='keyword'>#消毒殺菌</a>","<a class='keyword'>#清潔</a>");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
				<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["促銷"] = array("<label class='discount'>滿5送1</label>","<label class='discount'>紅利點數5點</label>");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
				<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["價錢"] = array("<label class='sale'>"."$"."1,350</label>","<label class='p_number sale_del'>"."$"."2,100</label>");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
				<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["數量"] = array("<input type='number' class='type_number' data-validate='number'><svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='plus' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 384 512' class='svg-inline--fa fa-plus fa-w-12 fa-3x'><path fill='currentColor' d='M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z' class=''></path></svg><svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='minus' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 384 512' class='svg-inline--fa fa-minus fa-w-12 fa-3x'><path fill='currentColor' d='M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z' class=''></path></svg>");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
				<?php
$_from = $_smarty_tpl->tpl_vars['item_infor']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['ii']) ? $_smarty_tpl->tpl_vars['ii'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['ii'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['ii']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
					<div class="row">
						<div class="cell-2" style="    align-items: center;    display: flex;"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</div>
						<div class="cell-10"><?php
$_from = $_smarty_tpl->tpl_vars['item_infor_h']->value[$_smarty_tpl->tpl_vars['v']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v__2_saved_item = isset($_smarty_tpl->tpl_vars['v_']) ? $_smarty_tpl->tpl_vars['v_'] : false;
$__foreach_v__2_saved_key = isset($_smarty_tpl->tpl_vars['ii_']) ? $_smarty_tpl->tpl_vars['ii_'] : false;
$_smarty_tpl->tpl_vars['v_'] = new Smarty_Variable();
$__foreach_v__2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v__2_total) {
$_smarty_tpl->tpl_vars['ii_'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['ii_']->value => $_smarty_tpl->tpl_vars['v_']->value) {
$__foreach_v__2_saved_local_item = $_smarty_tpl->tpl_vars['v_'];
echo $_smarty_tpl->tpl_vars['v_']->value;
$_smarty_tpl->tpl_vars['v_'] = $__foreach_v__2_saved_local_item;
}
}
if ($__foreach_v__2_saved_item) {
$_smarty_tpl->tpl_vars['v_'] = $__foreach_v__2_saved_item;
}
if ($__foreach_v__2_saved_key) {
$_smarty_tpl->tpl_vars['ii_'] = $__foreach_v__2_saved_key;
}
?></div>
					</div>
				<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['ii'] = $__foreach_v_1_saved_key;
}
?>
				<?php $_smarty_tpl->tpl_vars['item_infor'] = new Smarty_Variable(array("<a class='shop_car_push'>放入購物車</a>","<a class='shop_car_buy'>直接購買</a>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor', 0);?>

					<div class="row shop_car">

						<div class="col-lg-offset-2">
							<?php
$_from = $_smarty_tpl->tpl_vars['item_infor']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_3_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_3_saved_key = isset($_smarty_tpl->tpl_vars['ii']) ? $_smarty_tpl->tpl_vars['ii'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_3_total) {
$_smarty_tpl->tpl_vars['ii'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['ii']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_3_saved_local_item = $_smarty_tpl->tpl_vars['v'];
echo $_smarty_tpl->tpl_vars['v']->value;
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_local_item;
}
}
if ($__foreach_v_3_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_item;
}
if ($__foreach_v_3_saved_key) {
$_smarty_tpl->tpl_vars['ii'] = $__foreach_v_3_saved_key;
}
?>
						</div>
					</div>

				<hr>

				<?php $_smarty_tpl->tpl_vars['item_infor'] = new Smarty_Variable(array("所在地區","付款","運費"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor', 0);?>
				<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["所在地區"] = array('');
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
				<?php
$_from = $_smarty_tpl->tpl_vars['form_list_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_4_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_4_saved_key = isset($_smarty_tpl->tpl_vars['f_l_c']) ? $_smarty_tpl->tpl_vars['f_l_c'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_4_total) {
$_smarty_tpl->tpl_vars['f_l_c'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['f_l_c']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_4_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
					<?php if ($_smarty_tpl->tpl_vars['f_l_c']->value == 0) {?>
						<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["所在地區"][$_smarty_tpl->tpl_vars['f_l_c']->value] = (('<select class="city_sel"><option>').($_smarty_tpl->tpl_vars['v']->value['value'])).('</option>');
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['f_l_c']->value == sizeof($_smarty_tpl->tpl_vars['form_list_county']->value)-1) {?>
						<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["所在地區"][$_smarty_tpl->tpl_vars['f_l_c']->value] = (('<option>').($_smarty_tpl->tpl_vars['v']->value['value'])).('</option></select>');
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["所在地區"][$_smarty_tpl->tpl_vars['f_l_c']->value] = (('<option>').($_smarty_tpl->tpl_vars['v']->value['value'])).('</option>');
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
					<?php }?>
				<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_4_saved_local_item;
}
}
if ($__foreach_v_4_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_4_saved_item;
}
if ($__foreach_v_4_saved_key) {
$_smarty_tpl->tpl_vars['f_l_c'] = $__foreach_v_4_saved_key;
}
?>
				<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["付款"] = array("ATM，","信用卡，","LINE Pay，","Google Pay，","Apple Pay，","貨到付款，","超商");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
				<?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'item_infor_h', null);
$_smarty_tpl->tpl_vars['item_infor_h']->value["運費"] = array("<img id='_bak' src='/themes/Shopping/img/_bak.png' srcset='/themes/Shopping/img/_bak.png 1x, /themes/Shopping/img/_bak@2x.png 2x'>宅配 單件 60元<p>","<img id='_bai' src='/themes/Shopping/img/_bai.png' srcset='/themes/Shopping/img/_bai.png 1x, /themes/Shopping/img/_bai@2x.png 2x'>郵寄 單件 60元<p>","<img id='_16_v' src='/themes/Shopping/img/_16_v.png' srcset='/themes/Shopping/img/_16_v.png 1x, /themes/Shopping/img/_16_v@2x.png 2x'>超商取貨 7-ELEVEN-"."$"."70元 全家-"."$"."70元 萊爾富-"."$"."65");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'item_infor_h', 0);?>
				<?php
$_from = $_smarty_tpl->tpl_vars['item_infor']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_5_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_5_saved_key = isset($_smarty_tpl->tpl_vars['ii']) ? $_smarty_tpl->tpl_vars['ii'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_5_total) {
$_smarty_tpl->tpl_vars['ii'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['ii']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_5_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
					<div class="row">
						<div class="cell-2" style="    align-items: center;    display: flex;"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</div>
						<div class="cell-10"><?php
$_from = $_smarty_tpl->tpl_vars['item_infor_h']->value[$_smarty_tpl->tpl_vars['v']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v__6_saved_item = isset($_smarty_tpl->tpl_vars['v_']) ? $_smarty_tpl->tpl_vars['v_'] : false;
$__foreach_v__6_saved_key = isset($_smarty_tpl->tpl_vars['ii_']) ? $_smarty_tpl->tpl_vars['ii_'] : false;
$_smarty_tpl->tpl_vars['v_'] = new Smarty_Variable();
$__foreach_v__6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v__6_total) {
$_smarty_tpl->tpl_vars['ii_'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['ii_']->value => $_smarty_tpl->tpl_vars['v_']->value) {
$__foreach_v__6_saved_local_item = $_smarty_tpl->tpl_vars['v_'];
echo $_smarty_tpl->tpl_vars['v_']->value;
$_smarty_tpl->tpl_vars['v_'] = $__foreach_v__6_saved_local_item;
}
}
if ($__foreach_v__6_saved_item) {
$_smarty_tpl->tpl_vars['v_'] = $__foreach_v__6_saved_item;
}
if ($__foreach_v__6_saved_key) {
$_smarty_tpl->tpl_vars['ii_'] = $__foreach_v__6_saved_key;
}
?></div>
					</div>
				<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_local_item;
}
}
if ($__foreach_v_5_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_5_saved_item;
}
if ($__foreach_v_5_saved_key) {
$_smarty_tpl->tpl_vars['ii'] = $__foreach_v_5_saved_key;
}
?>
			</div>
			<div class="cell-1"></div>
		</div>

		<div class="row">
			<div class="cell-1"></div>
			<div class="cell-5"><div id="_49_">
				<img id="_7__8_w" src="/themes/Shopping/img/_7__8_w.png" srcset="/themes/Shopping/img/_7__8_w.png 1x, /themes/Shopping/img/_7__8_w@2x.png 2x">


				<img id="_7__9_x" src="/themes/Shopping/img/_7__9_x.png" srcset="/themes/Shopping/img/_7__9_x.png 1x, /themes/Shopping/img/_7__9_x@2x.png 2x">


				<svg class="_7__10_y" viewBox="0 0 600 2">
					<path id="_7__10_y" d="M 600 0 L 0 0">
					</path>
				</svg>
				<svg class="_7__13_z" viewBox="0 0 12 2">
					<path id="_7__13_z" d="M 0 0 L 12 0">
					</path>
				</svg>
				<div id="__a">
					<span> 商品資訊</span>
				</div>
				<div id="_bbk">
					<span>留言問答</span>
				</div>
			</div></div>
			<div class="cell-3 offset-2">

				<div class="hot_push_title">
					<label>人氣推薦</label>
				</div>
				<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 3) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 3; $_smarty_tpl->tpl_vars['i']->value++) {
?>
				<div class="hot_push_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
					<div class="hot_push_photo"><img src="/themes/Shopping/img/Clip_a.png"></div>
					<div class="hot_push_contect">
						<span>選手牌 蛙鏡及#36蛙鞋</span>
					</div>
				</div>
				<?php }
}
?>

			</div>
			<div class="cell-1"></div>
		</div>


		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 list_6">
				<div class="row">
					<div class="cell-12"><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tpl/footer_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
			</div>
			<div class="cell-1"></div>
		</div>

	</div>

	</div>
<?php }
}
