<?php
/* Smarty version 3.1.28, created on 2021-04-15 18:12:58
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_607811aab690a4_96573818',
  'file_dependency' => 
  array (
    'a9ad5254d82ec2d9d68f4a7539c81348ddfde06e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/footer.tpl',
      1 => 1618481576,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_607811aab690a4_96573818 ($_smarty_tpl) {
?>
      </article>

      <?php if ($_smarty_tpl->tpl_vars['display_footer']->value) {?><footer>



        <div id="footer" class="row">

          <?php $_smarty_tpl->tpl_vars['footer_title'] = new Smarty_Variable(array("買家","賣家","幫助中心","關於我們"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footer_title', 0);?>
          <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'footer_row', null);
$_smarty_tpl->tpl_vars['footer_row']->value["買家"] = array("訂單查詢","商品問答","互動訊息","喜愛商品 ","關注賣家帳戶管理","紅利點數","評價紀錄");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footer_row', 0);?>
          <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'footer_row', null);
$_smarty_tpl->tpl_vars['footer_row']->value["賣家"] = array("商品管理","商品問答","互動訊息","訂管管理","帳務管理","物流管理","廣告行銷","紅利點數","評價紀錄");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footer_row', 0);?>
          <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'footer_row', null);
$_smarty_tpl->tpl_vars['footer_row']->value["幫助中心"] = array("系統公告","訂單付款","退貨退款","刊登廣告","帳戶帳務","貨運物流","紅利點數");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footer_row', 0);?>
          <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'footer_row', null);
$_smarty_tpl->tpl_vars['footer_row']->value["關於我們"] = array("問題建議","服務條款","免責聲明","隱私權聲明","合作提案");
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'footer_row', 0);?>
          <!-- <div class="cell-12"><img src="/themes/Shopping/img/0401_20.jpg"></div> -->
          <div class="cell-9">
            <ul>
              <?php
$_from = $_smarty_tpl->tpl_vars['footer_title']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
              <li class="footer_title"><span><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</span>
                <ul>
                  <?php
$_from = $_smarty_tpl->tpl_vars['footer_row']->value[$_smarty_tpl->tpl_vars['v']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_row_1_saved_item = isset($_smarty_tpl->tpl_vars['v_row']) ? $_smarty_tpl->tpl_vars['v_row'] : false;
$__foreach_v_row_1_saved_key = isset($_smarty_tpl->tpl_vars['k_row']) ? $_smarty_tpl->tpl_vars['k_row'] : false;
$_smarty_tpl->tpl_vars['v_row'] = new Smarty_Variable();
$__foreach_v_row_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_row_1_total) {
$_smarty_tpl->tpl_vars['k_row'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k_row']->value => $_smarty_tpl->tpl_vars['v_row']->value) {
$__foreach_v_row_1_saved_local_item = $_smarty_tpl->tpl_vars['v_row'];
?>
                  <li><span><?php echo $_smarty_tpl->tpl_vars['v_row']->value;?>
</span></li>
                  <?php
$_smarty_tpl->tpl_vars['v_row'] = $__foreach_v_row_1_saved_local_item;
}
}
if ($__foreach_v_row_1_saved_item) {
$_smarty_tpl->tpl_vars['v_row'] = $__foreach_v_row_1_saved_item;
}
if ($__foreach_v_row_1_saved_key) {
$_smarty_tpl->tpl_vars['k_row'] = $__foreach_v_row_1_saved_key;
}
?>
                </ul>
              </li>
              <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>
            </ul>
          </div>
          <div class="cell-3">
            <ul class="footer_contectus">
              <li>
                若對平台使用上有任何建議與疑問
              </li>
              <li>
                <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-envelope fa-w-16 fa-5x"><path fill="currentColor" d="M464 64H48C21.5 64 0 85.5 0 112v288c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h416c8.8 0 16 7.2 16 16v41.4c-21.9 18.5-53.2 44-150.6 121.3-16.9 13.4-50.2 45.7-73.4 45.3-23.2.4-56.6-31.9-73.4-45.3C85.2 197.4 53.9 171.9 32 153.4V112c0-8.8 7.2-16 16-16zm416 320H48c-8.8 0-16-7.2-16-16V195c22.8 18.7 58.8 47.6 130.7 104.7 20.5 16.4 56.7 52.5 93.3 52.3 36.4.3 72.3-35.5 93.3-52.3 71.9-57.1 107.9-86 130.7-104.7v205c0 8.8-7.2 16-16 16z" class=""></path></svg>176@lifegroup.house
              </li>
              <li>
                <div class="download_app row">
                  <div class="cell-6">下載176歡樂購</div>
                  <div class="cell-6">關注</div>
                </div>
              </li>
              <li>
                <div class="row">
                  <div class="cell-6"><img src="/themes/Shopping/img/Layer_1535.png"></div>
                  <div class="cell-6 links icons">
                    <img src="/themes/Okmaster/img/login/fb.png">
                    <img src="/themes/Okmaster/img/login/Google.png">
                    <img src="/themes/Okmaster/img/login/Line_logo.png">
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>


            </footer><?php }?>

      </div>

</body>

</html>
<?php }
}
