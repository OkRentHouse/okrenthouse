<?php
/* Smarty version 3.1.28, created on 2021-04-22 20:28:09
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/controllers/List/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_60816bd92d7765_59603355',
  'file_dependency' => 
  array (
    '5c6fc528cf7770d8b8cfd2d07fd0d5f82d227e1d' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/controllers/List/content.tpl',
      1 => 1618996819,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../head_ad_bar.tpl' => 1,
    'file:../../head_search_bar.tpl' => 1,
    'file:tpl/footer_list.tpl' => 2,
  ),
),false)) {
function content_60816bd92d7765_59603355 ($_smarty_tpl) {
$_smarty_tpl->tpl_vars['brack_title'] = new Smarty_Variable(array("<label>開賣啦</label>","<label>最精選</label>","<label>好新掀</label>","<label>海外代購</label>","<label>推薦</label>","<label>分享讚</label>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'brack_title', 0);
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);
$_smarty_tpl->tpl_vars['page_tab'] = new Smarty_Variable("<ul><li><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='chevron-left' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-chevron-left fa-w-10 fa-3x'><path fill='currentColor' d='M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z' class=''></path></svg></li><li class='action'>1</li><li>2</li><li>3</li><li><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='chevron-right' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-chevron-right fa-w-10 fa-3x'><path fill='currentColor' d='M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z' class=''></path></svg></li></ul>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'page_tab', 0);?>
	<div class="body">

		<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../head_ad_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../head_search_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


		<?php if (!$_smarty_tpl->tpl_vars['get_listclass']->value) {?>
		<?php $_smarty_tpl->tpl_vars['get_listclass'] = new Smarty_Variable(rand(1,sizeof($_smarty_tpl->tpl_vars['brack_title']->value)), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'get_listclass', 0);?>
		<?php }?>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value+1) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 zone_1"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php echo $_smarty_tpl->tpl_vars['brack_title']->value[$_smarty_tpl->tpl_vars['bt']->value];
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable($_smarty_tpl->tpl_vars['bt']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);?><div class="page_tab"><?php echo $_smarty_tpl->tpl_vars['page_tab']->value;?>
</div></div>
			<!-- <div class="cell-5"></div> -->
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 item_list list_1">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>
				<div class="row">
					<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
					<div class="items item_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
 cell-2"><div class="contect">
						<iframe src="https://www.youtube.com/embed/_U0DG0jhOQI?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						<span>阿美來了</span><br><span class="second_title">神奇廚房好用小物<br>廚房好用小物</span>

						</div>
					</div>
					<?php }
}
?>

				</div>
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>
			</div>
			<div class="cell-1"></div>
		</div>


		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value+1) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 zone_2"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php echo $_smarty_tpl->tpl_vars['brack_title']->value[$_smarty_tpl->tpl_vars['bt']->value];
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable($_smarty_tpl->tpl_vars['bt']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);?><div class="page_tab"><?php echo $_smarty_tpl->tpl_vars['page_tab']->value;?>
</div></div>
			<!-- <div class="cell-5"></div> -->
			<div class="cell-1"></div>
		</div>


		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 item_list list_2">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>
				<div class="row">
					<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
					<div class="items item_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
 cell-2"><div class="contect">
						<img src="\img\default.png" ></img>
						<span>超立淨 高滅菌錠</span><br><span class="second_title">神奇廚房好用小物<br>廚房好用小物</span>
						<div class="name_star"><span>折 $30元</span><span  class="light_count"><img src="/themes/Shopping/img/light.svg">678</span><?php
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['j']->value = 0;
if ($_smarty_tpl->tpl_vars['j']->value < 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['j']->value < 5; $_smarty_tpl->tpl_vars['j']->value++) {
?><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php }
}
?>
</div>
						<div class="prices"><span class="s">$1350</span><span class="o">$2100</span></div></div>
					</div>
					<?php }
}
?>

				</div>
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>
			</div>
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value+1) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 zone_3"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php echo $_smarty_tpl->tpl_vars['brack_title']->value[$_smarty_tpl->tpl_vars['bt']->value];
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable($_smarty_tpl->tpl_vars['bt']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);?><div class="page_tab"><?php echo $_smarty_tpl->tpl_vars['page_tab']->value;?>
</div></div>
			<!-- <div class="cell-5"></div> -->
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 item_list list_3">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>
				<div class="row">
					<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
					<div class="items item_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
 cell-2"><div class="contect">
						<img src="\img\default.png" ></img>
						<span>超立淨 高滅菌錠</span><br><span class="second_title">神奇廚房好用小物<br>廚房好用小物</span>
						<div class="name_star"><span>折 $30元</span><span  class="light_count"><img src="/themes/Shopping/img/light.svg">678</span><?php
$_smarty_tpl->tpl_vars['ii'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['ii']->value = 0;
if ($_smarty_tpl->tpl_vars['ii']->value < 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['ii']->value < 5; $_smarty_tpl->tpl_vars['ii']->value++) {
?><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php }
}
?>
</div>
						<div class="prices"><span class="s">$1350</span><span class="o">$2100</span></div></div>
					</div>
					<?php }
}
?>

				</div>
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>
			</div>
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value+1) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 zone_4"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php echo $_smarty_tpl->tpl_vars['brack_title']->value[$_smarty_tpl->tpl_vars['bt']->value];
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable($_smarty_tpl->tpl_vars['bt']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);?><div class="page_tab"><?php echo $_smarty_tpl->tpl_vars['page_tab']->value;?>
</div></div>
			<!-- <div class="cell-5"></div> -->
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 item_list list_4">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>
				<div class="row">
					<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
					<div class="items item_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
 cell-2"><div class="contect">
						<img src="\img\default.png" ></img>
						<span>超立淨 高滅菌錠</span><br><span class="second_title">神奇廚房好用小物<br>廚房好用小物</span>
						<div class="name_star"><span>折 $30元</span><span  class="light_count"><img src="/themes/Shopping/img/light.svg">678</span><?php
$_smarty_tpl->tpl_vars['ii'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['ii']->value = 0;
if ($_smarty_tpl->tpl_vars['ii']->value < 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['ii']->value < 5; $_smarty_tpl->tpl_vars['ii']->value++) {
?><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php }
}
?>
</div>
						<div class="prices"><span class="s">$1350</span><span class="o">$2100</span></div></div>
					</div>
					<?php }
}
?>

				</div>
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>
			</div>
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value+1) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 zone_5"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php echo $_smarty_tpl->tpl_vars['brack_title']->value[$_smarty_tpl->tpl_vars['bt']->value];
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable($_smarty_tpl->tpl_vars['bt']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);?><div class="page_tab"><?php echo $_smarty_tpl->tpl_vars['page_tab']->value;?>
</div></div>
			<!-- <div class="cell-5"></div> -->
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 item_list list_5">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>
				<div class="row">
					<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
					<div class="items item_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
 cell-2"><div class="contect">
						<img src="\img\default.png" ></img>
						<span>超立淨 高滅菌錠</span><br><span class="second_title">神奇廚房好用小物<br>廚房好用小物</span>
						<div class="name_star"><span>折 $30元</span><span  class="light_count"><img src="/themes/Shopping/img/light.svg">678</span><?php
$_smarty_tpl->tpl_vars['ii'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['ii']->value = 0;
if ($_smarty_tpl->tpl_vars['ii']->value < 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['ii']->value < 5; $_smarty_tpl->tpl_vars['ii']->value++) {
?><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php }
}
?>
</div>
						<div class="prices"><span class="s">$1350</span><span class="o">$2100</span></div></div>
					</div>
					<?php }
}
?>

				</div>
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>
			</div>
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value+1) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 zone_6"><?php echo $_smarty_tpl->tpl_vars['brack_title']->value[$_smarty_tpl->tpl_vars['bt']->value];
$_smarty_tpl->tpl_vars['bt'] = new Smarty_Variable($_smarty_tpl->tpl_vars['bt']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'bt', 0);?><div class="page_tab"><?php echo $_smarty_tpl->tpl_vars['page_tab']->value;?>
</div></div>
			<div class="cell-1"></div>
		</div>

		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 list_6">
				<div class="row">
					<div class="cell-12"><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tpl/footer_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
			 </div></div>
			<div class="cell-1"></div>
		</div>

		<div class="row">
		  <div class="cell-1"></div>
		  <div class="items_tabs cell-10">
				<label class="items_tab action">全部(9999)</label>
				<label class="items_tab">二手</label>
				<label class="items_tab">免運</label>
				<label class="items_tab">優惠</label>
				<label class="items_tab"></label>
			</div>
		  <div class="cell-1"></div>
		</div>

		<?php
$_smarty_tpl->tpl_vars['il'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['il']->value = 0;
if ($_smarty_tpl->tpl_vars['il']->value < 3) {
for ($_foo=true;$_smarty_tpl->tpl_vars['il']->value < 3; $_smarty_tpl->tpl_vars['il']->value++) {
?>
		<div class="row">
			<div class="cell-1"></div>
			<div class="cell-10 tab_item_list item_list">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>
				<div class="row">
					<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 5; $_smarty_tpl->tpl_vars['i']->value++) {
?>
					<div class="items item_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" style="width:<?php echo 100/5;?>
%"><div class="contect">
						<img src="\img\default.png" ></img>
						<span>超立淨 高滅菌錠</span><br><span class="second_title">神奇廚房好用小物<br>廚房好用小物</span>
						<div class="name_star"><span>折 $30元</span><span  class="light_count"><img src="/themes/Shopping/img/light.svg">678</span><?php
$_smarty_tpl->tpl_vars['ii'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['ii']->value = 0;
if ($_smarty_tpl->tpl_vars['ii']->value < 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['ii']->value < 5; $_smarty_tpl->tpl_vars['ii']->value++) {
?><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php }
}
?>
</div>
						<div class="prices"><span class="s">$1350</span><span class="o">$2100</span></div></div>
					</div>
					<?php }
}
?>

				</div>
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>
			</div>
			<div class="cell-1"></div>
		</div>
		<?php }
}
?>


		<div class="row<?php if ($_smarty_tpl->tpl_vars['get_listclass']->value == $_smarty_tpl->tpl_vars['bt']->value) {?> active<?php } else { ?> hidden<?php }?>">
			<div class="cell-1"></div>
			<div class="cell-10 list_6">
				<div class="row">
					<div class="cell-12"><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tpl/footer_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
</div>
			</div>
			<div class="cell-1"></div>
		</div>

	</div>

	</div>
<?php }
}
