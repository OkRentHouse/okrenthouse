<?php
/* Smarty version 3.1.28, created on 2021-04-13 17:37:41
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6075666507f417_58015121',
  'file_dependency' => 
  array (
    '3a3264028a5f9abd19e5ad4e6bb7e4acd620fa88' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Shopping/header.tpl',
      1 => 1618306657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6075666507f417_58015121 ($_smarty_tpl) {
?>
<!doctype html>

<html>

<head>

<head><link rel="icon" href="<?php echo @constant('THEME_URL');?>
/img/favicon.png" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<meta name="apple-mobile-web-app-capable" content="yes">

	<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<?php if (!empty($_smarty_tpl->tpl_vars['GOOGLE_SITE_VERIFICATION']->value)) {?><meta name="google-site-verification" content="<?php echo $_smarty_tpl->tpl_vars['GOOGLE_SITE_VERIFICATION']->value;?>
" /><?php }?>

<title><?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
</title>

<?php if ($_smarty_tpl->tpl_vars['meta_description']->value != '') {?><meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
"><?php }?>

<?php if ($_smarty_tpl->tpl_vars['meta_keywords']->value != '') {?><meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['meta_keywords']->value;?>
"><?php }?>

<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">

<meta property="og:image" content="<?php echo @constant('THEME_URL');?>
/img/favicon.png" />
<meta property=og:url content="176.shopping">
<?php if ($_smarty_tpl->tpl_vars['meta_description']->value != '') {?><meta property=og:title content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
"><?php }?>
<meta property="og:image:width" content="664" />
<meta property="og:image:height" content="523" />
<meta property="twitter:image" content="<?php echo @constant('THEME_URL');?>
/img/favicon.png" />
<meta property="line:image" content="<?php echo @constant('THEME_URL');?>
/img/favicon.png" />
<?php if (!empty($_smarty_tpl->tpl_vars['t_color']->value)) {?><meta name="theme-color" content="<?php echo $_smarty_tpl->tpl_vars['t_color']->value;?>
"><?php }?>

<?php if (!empty($_smarty_tpl->tpl_vars['mn_color']->value)) {?><meta name="msapplication-navbutton-color" content="<?php echo $_smarty_tpl->tpl_vars['mn_color']->value;?>
"><?php }?>

<?php if (!empty($_smarty_tpl->tpl_vars['amwasb_style']->value)) {?><meta name="apple-mobile-web-app-status-bar-style" content="<?php echo $_smarty_tpl->tpl_vars['amwasb_style']->value;?>
"><?php }?>

<!--[if lt IE 9]>

<?php echo '<script'; ?>
 src="//html5shiv.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>

<![endif]-->

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./table_title.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<?php
$_from = $_smarty_tpl->tpl_vars['css_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_0_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_0_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>

	<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css"/>

<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_local_item;
}
}
if ($__foreach_css_uri_0_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_item;
}
if ($__foreach_css_uri_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_0_saved_key;
}
?>

<?php if (!isset($_smarty_tpl->tpl_vars['display_header_javascript']->value) || $_smarty_tpl->tpl_vars['display_header_javascript']->value) {?>

<?php echo '<script'; ?>
 type="text/javascript">

var THEME_URL = '<?php echo @constant('THEME_URL');?>
';

<?php echo '</script'; ?>
>

<?php
$_from = $_smarty_tpl->tpl_vars['js_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_1_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_1_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>

	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>

<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_local_item;
}
}
if ($__foreach_js_uri_1_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_item;
}
if ($__foreach_js_uri_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_1_saved_key;
}
?>

<?php if (count($_smarty_tpl->tpl_vars['_errors_name']->value) > 0) {?>

<?php echo '<script'; ?>
 type="text/javascript">

$(document).ready(function(e) {

<?php
$_from = $_smarty_tpl->tpl_vars['_errors_name']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_err_name_2_saved_item = isset($_smarty_tpl->tpl_vars['err_name']) ? $_smarty_tpl->tpl_vars['err_name'] : false;
$_smarty_tpl->tpl_vars['err_name'] = new Smarty_Variable();
$__foreach_err_name_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_err_name_2_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['err_name']->value) {
$__foreach_err_name_2_saved_local_item = $_smarty_tpl->tpl_vars['err_name'];
?>

	<?php if (count($_smarty_tpl->tpl_vars['_errors_name_i']->value[$_smarty_tpl->tpl_vars['err_name']->value]) > 0) {?>

		<?php
$_from = $_smarty_tpl->tpl_vars['_errors_name_i']->value[$_smarty_tpl->tpl_vars['err_name']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ei_3_saved_item = isset($_smarty_tpl->tpl_vars['ei']) ? $_smarty_tpl->tpl_vars['ei'] : false;
$_smarty_tpl->tpl_vars['ei'] = new Smarty_Variable();
$__foreach_ei_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_ei_3_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['ei']->value) {
$__foreach_ei_3_saved_local_item = $_smarty_tpl->tpl_vars['ei'];
?>

			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parents('.form-group').addClass('has-error');

			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parent('td').addClass('has-error');

			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').focus(function(){

				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parents('.form-group').removeClass('has-error');

				$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').eq(<?php echo $_smarty_tpl->tpl_vars['ei']->value;?>
).parent('td').removeClass('has-error');

			});

		<?php
$_smarty_tpl->tpl_vars['ei'] = $__foreach_ei_3_saved_local_item;
}
}
if ($__foreach_ei_3_saved_item) {
$_smarty_tpl->tpl_vars['ei'] = $__foreach_ei_3_saved_item;
}
?>

	<?php } else { ?>

		$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parents('.form-group').addClass('has-error');

		$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parent('td').addClass('has-error');

		$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').focus(function(){

			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parents('.form-group').removeClass('has-error');

			$('[name="<?php echo $_smarty_tpl->tpl_vars['err_name']->value;?>
"]').parent('td').removeClass('has-error');

		});

	<?php }?>

<?php
$_smarty_tpl->tpl_vars['err_name'] = $__foreach_err_name_2_saved_local_item;
}
}
if ($__foreach_err_name_2_saved_item) {
$_smarty_tpl->tpl_vars['err_name'] = $__foreach_err_name_2_saved_item;
}
?>

});

<?php echo '</script'; ?>
>

<?php }?>

<?php }?>

</head>

<body>

<div class="display_content">

	<?php if ($_smarty_tpl->tpl_vars['display_header']->value) {?>

    <header class="minbar">

			<div class="top_link_bar">
				<div class="cell-1"></div>
				<div class="links icons cell-5">
					<!-- <img src="/themes/Okmaster/img/login/Line_logo.png">
					<img src="/themes/Okmaster/img/login/Google.png">
					<img src="/themes/Okmaster/img/login/fb.png"> -->
					<span class="right_border">關注我們</span><label class="borderline"></label><span class="right_border">下載</span><label class="borderline"></label><span>客服</span></div>
				<div class="cell-5 links r"><span class="right_border">買家</span><label class="borderline"></label><span class="right_border">賣家</span><label class="borderline"></label><span>註冊</span></div>
				<div class="cell-1"></div>
			</div>
			<div class="top_bar">
				<div class="icons_">
					<?php
$_smarty_tpl->tpl_vars['icons_'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['icons_']->value = 1;
if ($_smarty_tpl->tpl_vars['icons_']->value <= 25) {
for ($_foo=true;$_smarty_tpl->tpl_vars['icons_']->value <= 25; $_smarty_tpl->tpl_vars['icons_']->value++) {
?>
					<?php if ($_smarty_tpl->tpl_vars['icons_']->value != 5) {?>
					<div><img src="/themes/Shopping/img/icons_<?php echo $_smarty_tpl->tpl_vars['icons_']->value;?>
.png"></div>
					<?php }?>
					<?php }
}
?>

				</div>
				<div class="cell-1"></div>
				<div class="links logo cell-3 l">
					<span class="right_border"><img src="/themes/Shopping/img/menu_03.png" alt="分類" class="classify"></span>


					<span class="right_border logo"><img src="/themes/Shopping/img/menu_07.png" alt="logo" class="classify"></span>
				</div>

				<div class="cell-4 m search_key_w">
					<div class="search_input"><input type="text" placeholder="請輸入關鍵字"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16 fa-2x"><path fill="#fff" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z" class=""></path></svg></div>
					<span>超立淨</span><span class="action">外套</span><span>包包</span><span>長褲</span><span>防疫口罩</span><span>公背包</span><span>行動電源</span></div>
				<div class="links cell-3 r">
					<span class="right_border head_photo"><img src="/themes/Shopping/img/menu_05.png"></span>
					<span class="right_border"><img src="/themes/Shopping/img/menu_11.png" alt="購物中心"> </span>
					<span class="right_border"><img src="/themes/Shopping/img/menu_09.png" alt="互動訊息"></span>
					<span><img src="/themes/Shopping/img/menu_13.png" alt="幫助中心"></span></div>
					<div class="cell-1"></div>
			</div>
    </header>
	<?php }?>

    <article class="<?php echo $_smarty_tpl->tpl_vars['mamber_url']->value;?>
">

    <?php echo $_smarty_tpl->tpl_vars['msg_box']->value;?>

<?php }
}
