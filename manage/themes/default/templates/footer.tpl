{if $display_footer}{* ↓有 footer 的版型↓ *}
			</div>
		</div>
		<div id="footer" class="hide"><span class="execution_time"><span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{l s="載入時間"}"><span class="glyphicon glyphicon-time"></span>{$execution_time}</span></span></div>
		</div>
		{* ↑有 footer 的版型↑ *}{else}{* ↓沒 footer 的版型↓ *}
			</div>
		</div>
	</div>
	{* ↑沒 footer 的版型↑ *}{/if}
</div>
{hook h='displayAdminBodyAfter'}
</body>
</html>