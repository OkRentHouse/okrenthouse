{$admin_icon='<svg style="vertical-align: inherit;width: 15pt;margin-right: 4px;color: #999;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="users-cog" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-users-cog fa-w-20 fa-3x"><path fill="currentColor" d="M610.5 341.3c2.6-14.1 2.6-28.5 0-42.6l25.8-14.9c3-1.7 4.3-5.2 3.3-8.5-6.7-21.6-18.2-41.2-33.2-57.4-2.3-2.5-6-3.1-9-1.4l-25.8 14.9c-10.9-9.3-23.4-16.5-36.9-21.3v-29.8c0-3.4-2.4-6.4-5.7-7.1-22.3-5-45-4.8-66.2 0-3.3.7-5.7 3.7-5.7 7.1v29.8c-13.5 4.8-26 12-36.9 21.3l-25.8-14.9c-2.9-1.7-6.7-1.1-9 1.4-15 16.2-26.5 35.8-33.2 57.4-1 3.3.4 6.8 3.3 8.5l25.8 14.9c-2.6 14.1-2.6 28.5 0 42.6l-25.8 14.9c-3 1.7-4.3 5.2-3.3 8.5 6.7 21.6 18.2 41.1 33.2 57.4 2.3 2.5 6 3.1 9 1.4l25.8-14.9c10.9 9.3 23.4 16.5 36.9 21.3v29.8c0 3.4 2.4 6.4 5.7 7.1 22.3 5 45 4.8 66.2 0 3.3-.7 5.7-3.7 5.7-7.1v-29.8c13.5-4.8 26-12 36.9-21.3l25.8 14.9c2.9 1.7 6.7 1.1 9-1.4 15-16.2 26.5-35.8 33.2-57.4 1-3.3-.4-6.8-3.3-8.5l-25.8-14.9zM496 368.5c-26.8 0-48.5-21.8-48.5-48.5s21.8-48.5 48.5-48.5 48.5 21.8 48.5 48.5-21.7 48.5-48.5 48.5zM96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm224 32c1.9 0 3.7-.5 5.6-.6 8.3-21.7 20.5-42.1 36.3-59.2 7.4-8 17.9-12.6 28.9-12.6 6.9 0 13.7 1.8 19.6 5.3l7.9 4.6c.8-.5 1.6-.9 2.4-1.4 7-14.6 11.2-30.8 11.2-48 0-61.9-50.1-112-112-112S208 82.1 208 144c0 61.9 50.1 112 112 112zm105.2 194.5c-2.3-1.2-4.6-2.6-6.8-3.9-8.2 4.8-15.3 9.8-27.5 9.8-10.9 0-21.4-4.6-28.9-12.6-18.3-19.8-32.3-43.9-40.2-69.6-10.7-34.5 24.9-49.7 25.8-50.3-.1-2.6-.1-5.2 0-7.8l-7.9-4.6c-3.8-2.2-7-5-9.8-8.1-3.3.2-6.5.6-9.8.6-24.6 0-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h255.4c-3.7-6-6.2-12.8-6.2-20.3v-9.2zM173.1 274.6C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z" class=""></path></svg>'}
{$user_mail_icon='<svg style="vertical-align: inherit;width: 15pt;margin-right: 4px;color: #FFF;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-envelope-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zM178.117 262.104C87.429 196.287 88.353 196.121 64 177.167V152c0-13.255 10.745-24 24-24h272c13.255 0 24 10.745 24 24v25.167c-24.371 18.969-23.434 19.124-114.117 84.938-10.5 7.655-31.392 26.12-45.883 25.894-14.503.218-35.367-18.227-45.883-25.895zM384 217.775V360c0 13.255-10.745 24-24 24H88c-13.255 0-24-10.745-24-24V217.775c13.958 10.794 33.329 25.236 95.303 70.214 14.162 10.341 37.975 32.145 64.694 32.01 26.887.134 51.037-22.041 64.72-32.025 61.958-44.965 81.325-59.406 95.283-70.199z" class=""></path></svg>'}
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top">
  <img src="\img\admin\control_panel\admin_panel_0_2021.png" style="width: 8%;/*height: 100%;*/">
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
{foreach $web2_data as $key =>$value}
    {if $key%5==0}<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{/if}
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.lifegroup.house/manage/Welcome?id_diversion={$value.id_diversion}">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><img alt="{$value.name}" src="{$value.img}" style="width: 100%;/*height: 100%;*/"></div>
                <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><span>{$value.name}</span></div>-->
            </a>
        </div>
    {if ($key%5==4) || $key==$web2_long}</div>{/if}
{/foreach}
    <div class="Welcome admin_tools">
      <svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="line-height" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="fas svg-inline--fa fa-line-height fa-w-20 fa-5x"><g class="fa-group"><path fill="currentColor" d="M626.29 224H269.71c-7.57 0-13.71 7.16-13.71 16v32c0 8.84 6.14 16 13.71 16h356.58c7.57 0 13.71-7.16 13.71-16v-32c0-8.84-6.14-16-13.71-16zm0 160H269.71c-7.57 0-13.71 7.16-13.71 16v32c0 8.84 6.14 16 13.71 16h356.58c7.57 0 13.71-7.16 13.71-16v-32c0-8.84-6.14-16-13.71-16zm0-320H269.71C262.14 64 256 71.16 256 80v32c0 8.84 6.14 16 13.71 16h356.58c7.57 0 13.71-7.16 13.71-16V80c0-8.84-6.14-16-13.71-16z" class="fa-secondary"></path></g></svg>
    {if $smarty.session.id_group==2 || $smarty.session.id_admin==1}

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{$admin_icon}<a href="https://www.lifegroup.house/manage/Welcome?id_diversion=admin">管理者全體顯現用</a><label>總管理員權限者可見所有管理項目</label></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{$admin_icon}<a href="http://104.199.245.59/KRsEGdEKCKPW6HfgEYySqXt8wbzNqKzUhD3m5yet5aDwpEghuRM3trVqUUCBuPUc/" target="_blank">Myphpadmin <i class="fas fa-external-link-alt"></i></a><label>GCP 資料庫管理介面</label></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{$admin_icon}<a href="https://twnoc.net/whmcs/clientarea.php" target="_blank"> 遠振資訊 <i class="fas fa-external-link-alt"></i></a><label>遠振資訊有限公司(email空間)</label></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{$admin_icon}<a href="https://cp27.g-dns.com:2083/cpsess3606593881/frontend/paper_lantern/email_accounts/index.html" target="_blank"> mail帳號管理 <i class="fas fa-external-link-alt"></i></a><label>查閱目前所有 email 帳戶(需先登入遠振資訊)</label></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{$admin_icon}<a href="https://www.lifegroup.house/manage/Welcome?id_diversion=17"> 176 生活樂購 <i class="fas fa-external-link-alt"></i></a><label>176 生活樂購</label></div>
    {/if}
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{$user_mail_icon}<a href="https://cp27.g-dns.com:2096/" target="_blank">eMail入口</a><label>員工信箱入口</label></div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer">
  <img src="\img\admin\control_panel\copyright.png">
</div>
<input type='hidden' value='{gc_maxlifetime}'>
<input type='hidden' value='{cookie_lifetime}'>
