<div class="list col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				網站選單
			</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<thead>
					<tr><th class="text-center">網站名稱<div class="th_title">網站名稱</div></th><th class="text-center">Menu Code<div class="th_title">Menu Code</div></th></tr>
					</thead>
					<tbody>
					{foreach $arr_web as $i => $web}
						<tr id="tr_{$web.id_menu_code}" onclick="document.location='/manage/Menu?&id_menu_code={$web.id_menu_code}'"><td class="text-center">{$web.web}</td><td class="text-center">{$web.code}</td></tr>
					{/foreach}
					</tbody>
					<tfoot></tfoot>
				</table>
			</div>
		</div>
</div>