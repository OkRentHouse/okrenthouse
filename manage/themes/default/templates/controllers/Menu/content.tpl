{if !empty($dir)}
	<ul class="col-lg-12 draggable_div">
        {foreach $arr_page as $v => $page}<li class="ui-state-default page">
			<span class="ui-icon ui-icon-arrowthick-2-n-s move"></span>
			<span class="portlet-header">{$page.text}</span>
			</li>{/foreach}
	</ul>
	<hr>
	<ul id="sortable" class="col-lg-12">
		<li class="ui-state-default item-depth-0" data-depth="0"><span class="ui-icon ui-icon-arrowthick-2-n-s move"></span><span>Item 0</span></li>
		<li class="ui-state-default item-depth-1" data-depth="1"><span class="ui-icon ui-icon-arrowthick-2-n-s move"></span><span>Item 1</span></li>
		<li class="ui-state-default item-depth-2" data-depth="2"><span class="ui-icon ui-icon-arrowthick-2-n-s move"></span><span>Item 2</span></li>
		<li class="ui-state-default item-depth-3" data-depth="3"><span class="ui-icon ui-icon-arrowthick-2-n-s move"></span><span>Item 3</span></li>
		<li class="ui-state-default item-depth-1" data-depth="1"><span class="ui-icon ui-icon-arrowthick-2-n-s move"></span><span>Item 4</span></li>
	</ul>
{else}
	<div class="col-lg-12">找不到 Menu Code</div>
{/if}