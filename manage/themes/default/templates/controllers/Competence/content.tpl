<form method="post" class="defaultForm form-horizontal col-lg-10" novalidate="novalidate">
{foreach from=$arr_group key=i item=group}
	{if $group.id_group == $smarty.session.id_group}
		<div class="col-lg-12 panel-body tab-count tabs_group_{$group.id_group}">{l s="無法更改本身的群組權限!"}</div>
	{elseif $group.id_group != 1}
	<div class="tab-count competence_tab tabs_group_{$group['id_group']} col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="{$group.id_group}">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-cogs"></i>{l s="功能表"}</div>
			<div class="panel-body">
				<table>
					<thead>
					<tr class="h5" data-id_tab="all" data-id_parent="all">
						<td></td>{foreach from=$type key=i item=type_v}<td><label><input type="checkbox" class="on_ajax all_{$type_v['code']}" value="{$type_v['code']}" data-id_group="{$group['id_group']}"{if false} checked="checked"{/if}>{$type_v.text}</label></td>{/foreach}<td><label><input type="checkbox" class="on_ajax all_all" data-id_group="{$group['id_group']}" value="all"{if false} checked="checked"{/if}>全部</label></td>
					</tr>
					</thead>
					<tbody>
					{foreach from=$arr_tab key=j item=tab}
						{if !in_array($tab.id_tab, $arr_no_tab)}
							<tr class="h6{if $tab.id_parent == 0} parent{else} child{/if}" data-id_tab="{$tab.id_tab}" data-id_parent="{$tab.id_parent}">
								<td>{$tab.title}</td>{foreach from=$type key=i item=type_v}<td>{if ($arr_competence[$group.id_group][$tab['id_tab']][$type_v['code']] != 2) && ($arr_competence[$smarty.session.id_group][$tab['id_tab']][$type_v['code']] != 2)}<label><input type="checkbox" class="on_ajax type_{$type_v['code']}" value="{$type_v['code']}"{if $arr_competence[$group.id_group][$tab['id_tab']][$type_v['code']]} checked="checked"{/if}></label>{/if}</td>{/foreach}<td><label><input type="checkbox" class="on_ajax type_all" value="all"{if $arr_competence[$group.id_group][$tab['id_tab']]['all']} checked="checked"{/if}>{$arr_competence[$group.id_group][$type_v]}</label></td>
							</tr>
						{/if}
					{/foreach}
					</tbody>
				</table>
			</div>
		</div>
		</div>{if count($arr_features) > 0}<div class="tab-count features_tab tabs_group_{$group['id_group']} col-lg-6 col-md-12 col-sm-12 col-xs-12" data-id_group="{$group.id_group}">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-cogs"></i>{l s="功能"}</div>
			<div class="panel-body">
				<table>
					<thead>
					<tr class="h5" data-id_tab="all" data-id_parent="all">
						<td></td><td><label>{*<input type="checkbox" class="on_ajax all_give" data-id_group="{$group['id_group']}" value="all"{if false} checked="checked"{/if}>全部*}</label></td>
					</tr>
					</thead>
					<tbody>
					{foreach from=$arr_features key=j item=features}
						{if $features.id_features != 1}
							<tr class="h6" data-id_tab="{$features.features}" data-id_parent="{$features.features}">
								<td>{$features.features}</td><td><label><input type="checkbox" class="on_ajax type_give" value="give" checked="checked"></label></td>
							</tr>
						{/if}
					{/foreach}
					</tbody>
				</table>
			</div>
		</div>
		</div>{/if}{include file="$tpl_dir./tabs_group.tpl"}{include file="$tpl_dir./tabs_file.tpl"}{if count($arr_module)}{include file="$tpl_dir./tabs_module.tpl"}{/if}
	{else}
		<div class="col-lg-12 panel-body tab-count competence_tab tabs_group_{$group.id_group}">{l s="最高系統管理者 權限無法更改!"}</div>
	{/if}
{/foreach}
	{hook h='displayCompetenceTabs'}
</form>