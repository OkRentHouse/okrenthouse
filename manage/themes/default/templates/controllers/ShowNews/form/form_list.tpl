{$form_list_pagination}
{$form_list_field_list}{if count($fields.list) > 0}<form action="" method="get" class="list col-lg-12" accept-charset="utf-8">{/if}
<div class="panel panel-default">
	{if $fields.list_heading_display}
	{/if}
      {if $fields.list_body_display}
	<div class="panel-body">
      {strip}
		  {$form_list_tbody}
      {/strip}
	</div>
	{/if}
      {if $fields.list_footer_display}<div class="panel-footer">{$fields.list_footer}</div>{/if}
</div>
{$form_list_pagination}
<input type="hidden" name="p" value="{$smarty.get.p}" />
<input type="hidden" name="m" value="{$smarty.get.m}" />
{if count($fields.list) > 0}</form>{/if}