{if count($fields.list_val) > 0}
{foreach from=$fields.list_val key=fi item=list}
	<div class="news_div news_{$list.id_news} {if $list.top}top{/if}">
			{if !empty({$list.id_news_type})}<samp class="type">{$list.id_news_type}</samp>{/if}
			<samp class="time">發佈時間：{$list.date_time}</samp>
			{if $list.top}<i class="glyphicon glyphicon-bookmark top" title="置頂" alt="置頂"></i>{/if}
			<h1 class="title">{$list.title}</h1>
			<div class="content">{$list.content}</div>
			<samp class="release">發佈：{$list.id_add}{if $list.id_edit != '-'}　最後修改：{$list.id_edit} {$list.edit_time}{/if}</samp>
	</div>
{/foreach}
{else}
<div class="text-center" colspan="{if $has_actions && $display_edit_div}{count($fields.list)+1}{else}{count($fields.list)}{/if}">{l s="沒有最新消息!"}</div>
{/if}{/strip}