{if count($arr_reservation) > 0}
	{foreach from=$arr_reservation key=i item=reservation}
		<div>訂單編號：{$reservation.order_number}</div>
		<table class="table table-bordered">
			<body>
			<tr>
				<th class="text-center">成交日</th>
				<th class="text-center">戶別</th>
				<th class="text-center">成交金額</th>
				<th class="text-center">底價</th>
				<th class="text-center">溢價</th>
				<th class="text-center">訂金</th>
				<th class="text-center" colspan="3">銷售人員</th>
				<th class="text-center">成交狀態</th>
				<th class="text-center">交屋狀態</th>
				<th class="text-center edit_div">管理</th>
			</tr>
			<tr>
				<td class="text-center">{$reservation.deal_days}</td>
				<td class="text-center">{$reservation.account}</td>
				<td class="text-center">{$reservation.deal_money|number_format:0:'.':','}</td>
				<td class="text-center">{$reservation.reserve_price|number_format:0:'.':','}</td>
				<td class="text-center">{$reservation.overflow|number_format:0:'.':','}</td>
				<td class="text-center">{$reservation.deposit|number_format:0:'.':','}{if !empty($reservation.payment_method_d)}<div>({$reservation.payment_method_d})</div>{/if}</td>
				<td class="text-center" colspan="3">
						{$reservation.id_assistant}
						{if !empty($reservation.id_assistant) && !empty($reservation.id_assistant2)}、{/if}
						{$reservation.id_assistant2}
						{if !empty($reservation.id_assistant) || !empty($reservation.id_assistant2)}/{/if}
						{$reservation.id_commissioner}
						{if !empty($reservation.id_commissioner) && !empty($reservation.id_commissioner2)}、{/if}
						{$reservation.id_commissioner2}
						{if empty($reservation.id_commissioner) && empty($reservation.id_commissioner2)}自來客{/if}
				</td>
				<td class="text-center">{if $reservation.transaction_status}成交{else}<span class="text-red">退戶<div>{$reservation.refund_date}</div></span>{/if}</td>
				<td class="text-center">{if $reservation.house_status}已交屋{else}<span class="text-red">未交屋</span>{/if}</td>
				<td class="text-center edit_div" rowspan="3">{if $tabAccess.ti.edit || $tabAccess.ti.view}<a class="btn btn-default edit" href="{$back_url}&amp;{if $tabAccess.ti.edit}edit{elseif $tabAccess.ti.view}view{/if}clients&amp;id_reservation={$reservation.id_reservation}&amp;id_clients={$id_clients}">{if $tabAccess.ti.edit}修改{elseif $tabAccess.ti.view}檢視{/if}</a>{/if}{if $tabAccess.ti.del}<a class="btn btn-default del" href="{$back_url}&amp;delclients&amp;id_reservation={$reservation.id_reservation}&amp;id_clients={$id_clients}">刪除</a>{/if}</td>
			</tr>
			<tr>
				<th class="text-center">簽約日</th>
				<th class="text-center">簽約金</th>
				<th class="text-center">工程款一</th>
				<th class="text-center">工程款二</th>
				<th class="text-center">工程款三</th>
				<th class="text-center">工程款四</th>
				<th class="text-center">使照取得</th>
				<th class="text-center">暫收款</th>
				<th class="text-center">貸款</th>
				<th class="text-center">交屋款</th>
				<th class="text-center">總價</th>
			</tr>
				<td class="text-center">{$reservation.contract_day}</td>
				<td class="text-center">{$reservation.contract|number_format:0:'.':','}{if !empty($reservation.payment_method_c)}<div>({$reservation.payment_method_c})</div>{/if}</td>
				<td class="text-center">{if !empty($reservation.engineering_section1)}{$reservation.engineering_section1|number_format:0:'.':','}{if !empty($reservation.payment_method_1)}<div>({$reservation.payment_method_1})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{if !empty($reservation.engineering_section2)}{$reservation.engineering_section2|number_format:0:'.':','}{if !empty($reservation.payment_method_2)}<div>({$reservation.payment_method_2})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{if !empty($reservation.engineering_section3)}{$reservation.engineering_section3|number_format:0:'.':','}{if !empty($reservation.payment_method_3)}<div>({$reservation.payment_method_3})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{if !empty($reservation.engineering_section4)}{$reservation.engineering_section4|number_format:0:'.':','}{if !empty($reservation.payment_method_4)}<div>({$reservation.payment_method_4})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{if !empty($reservation.license_obtain)}{$reservation.license_obtain|number_format:0:'.':','}{if !empty($reservation.payment_method_l)}<div>({$reservation.payment_method_l})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{if !empty($reservation.temporary_payment)}{$reservation.temporary_payment|number_format:0:'.':','}{if !empty($reservation.payment_method_t)}<div>({$reservation.payment_method_t})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{if !empty($reservation.ioan)}{$reservation.ioan|number_format:0:'.':','}{if !empty($reservation.payment_method_i)}<div>({$reservation.payment_method_i})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{if !empty($reservation.house_paragraph)}{$reservation.house_paragraph|number_format:0:'.':','}{if !empty($reservation.payment_method_h)}<div>({$reservation.payment_method_h})</div>{/if}{else}-{/if}</td>
				<td class="text-center">{($reservation.deposit+$reservation.contract+$reservation.engineering_section1+$reservation.engineering_section2+$reservation.engineering_section3+$reservation.engineering_section4+$reservation.license_obtain+$reservation.ioan+$reservation.house_paragraph)|number_format:0:'.':','}</td>
			</tr>
			<body>
		</table>
	{/foreach}
{/if}