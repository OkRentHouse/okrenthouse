{block name="defaultTab"}{strip}{if isset($fields.tabs)}<div class="count_tabs col-lg-2{if isset($fields.tabs.class)}{$fields.tabs.class}{/if}"><ul class="list-group">{foreach from=$fields.tabs key=link_id item=tab_title}{if $link_id != 'class'}<li id="tabs_{$link_id}" class="list-group-item">{$tab_title}</li>{/if}{/foreach}</ul></div>{/if}{/strip}{/block}{block name="defaultForm"}{if count($fields.form) > 0}<form method="post"
      name="{if isset($fields.form.name)}{$fields.form.name}{elseif isset($fields.form.id)}{$fields.form.id}{else}{$table}{/if}"
      id="{if isset($fields.form.id)}{$fields.form.id}{elseif isset($fields.form.name)}{$fields.form.name}{else}{$table}{/if}"
      class="defaultForm form-horizontal {if isset($fields.tabs)}row{/if} {if $view} view{/if}{if isset($fields.tabs)} col-lg-10{/if}{if isset($fields.form.class)} {$fields.form.class}{/if}"
      novalidate>{/if}{foreach from=$fields.form key=form_tab_id item=form}{if $key == 'name'}{continue}{/if}{if $form_tab_id !== 'class'}{strip}<div{if $form.tab_id} id="{$form.tab_id}"{/if} class="tab-count tabs_{$form.tab}{if $form.tab_class} {$form.tab_class}{/if}{if $form.tab_col} col-lg-{$form.tab_col}{else} col-lg-12{/if}"{if isset($fields.tabs)} style="display:none"{/if}
	     enctype="multipart/form-data"><div class="panel panel-default{if isset($form.class)} {$form.class}{/if}">
			{assign var=body_display value=false}
			{foreach from=$form key=key item=form_v}
			{if $key == 'html'}
			{$form_v}
			{elseif $key == 'tpl'}
			{include file=$form_v}
			{elseif $key == 'legend'}
			{block name="legend"}
			{assign var=body_display value=true}
			{if $form.heading_display}
				<div class="panel-heading">
					{if isset($form_v.image) && isset($field.title)}<img src="{$form_v.image}" alt="{$form_v.title|escape:'html':'UTF-8'}" />{/if}
					{if isset($form_v.icon)}<i class="{$form_v.icon}"></i>{/if}
					{$form_v.title}
				</div>
			{/if}
			<div class="panel-body">
				{/block}
				{elseif $key == 'description' && $form_v}
				<div class="alert alert-info">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'warning' && $form_v}
				<div class="alert alert-warning">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'success' && $form_v}
				<div class="alert alert-success">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'error' && $form_v}
				<div class="alert alert-danger">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'input'}
				<div class="form-wrapper">
					{foreach $form_v as $input}
					{block name="input_row"}
					{if ($input.is_prefix && !$input.is_suffix) || (!$input.is_prefix && !$input.is_suffix)}
					<div class="form-group{if isset($input.form_col)} col-lg-{$input.form_col}{else} col-lg-12{/if}{if isset($input.form_group_class)} {$input.form_group_class}{/if}{if $input.type == 'hidden'} hide{/if}"{if isset($tabs) && isset($input.tab)} data-tab-id="{$input.tab}"{/if}>{/if}
						{if $input.type == 'hr'}
						<hr{if isset($input.id)} id="{$input.id}"{/if}{if isset($input.class)} class="{$input.class}"{/if}>
						{elseif $input.type == 'hidden'}
						<input type="hidden" name="{$input.name}" id="{if !empty($input.id)}{$input.id}{else}{$input.name}{/if}"
						       class="{if isset($input.class)}{$input.class}{/if}"
						       value="{if isset($input.string_format)}{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}{else}{$input.val|escape:'html':'UTF-8'}{/if}"/>
						{else}
						{block name="label"}
							{if isset($input.label) && !$input.no_label}
								<label class="main_t control-label col-lg-{if isset($input.label_col)}{$input.label_col}{else}3{/if}{if isset($input.label_class) && $input.label_class} {$input.label_class}{/if}{if isset($input.required) && $input.required && $input.type != 'radio'} required{/if}"
								       for="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}">
									{if isset($input.hint)}
									<span class="main_t label-tooltip" data-toggle="tooltip" data-html="true"
									      title="{if is_array($input.hint)}
                                                {foreach $input.hint as $hint}
                                                      {if is_array($hint)}
                                                            {$hint.text|escape:'quotes'}
                                                      {else}
                                                            {$hint|escape:'quotes'}
                                                      {/if}
                                                {/foreach}
                                          {else}
                                                {$input.hint|escape:'quotes'}
                                          {/if}">
                                    {/if}
										{$input.label}
										{if isset($input.hint)}</span>{/if}
								</label>
							{/if}
						{/block}
						{block name="field"}
						{if ($input.is_prefix && !$input.is_suffix) || (!$input.is_prefix && !$input.is_suffix)}
						<div class="col-lg-{if isset($input.col)}{$input.col|intval}{else}3{/if}{if isset($input.div_class)} {$input.div_class}{/if}{if !isset($input.label) && !$input.no_label} col-lg-offset-3{/if} {$input.col_class}">{/if}
							{if ((isset($input.prefix) || isset($input.suffix) || isset($input.prefix_btn) || isset($input.suffix_btn)) && (!$input.is_prefix && !$input.is_suffix)) || ($input.is_prefix && !$input.is_suffix)}
							<div class="input-group fixed-width-lg">{/if}{if isset($input.prefix)}<span class="main_t_2 input-group-addon{if isset($input.prefix_class)} {$input.prefix_class}{/if}">{$input.prefix}</span>{/if}{if isset($input.prefix_btn)}<div class="input-group-btn">
								{foreach $input.prefix_btn as $p_btn}
								<a class="btn btn-default {$p_btn.class}" href="{if $p_btn.href}{$p_btn.href}{else}javascript:void(0);{/if}"><span class="{$p_btn.icon}"></span>{$p_btn.title}</a>
								{/foreach}
								</div>{/if}
								{if $input.type == 'tpl'}
								{include file=$input.file}
								{elseif $input.type == 'html'}
								{$input.data}
								{elseif $input.type == 'text' || $input.type == 'number' || $input.type == 'color' || $input.type == 'email' || $input.type == 'tel' || $input.type == 'url' || $input.type == 'date' || $input.type == 'datetime' || $input.type == 'datetime-local' || $input.type == 'time'}
								<input type="{$input.type}"
								       id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
								       name="{$input.name}{if isset($input.multiple) && $input.multiple}[]{/if}"
								       class="form-control {if isset($input.class)}{$input.class}{/if}"
								       value="{if isset($input.string_format)}
											{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}
										{else}
											{$input.val|escape:'html':'UTF-8'}
										{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.min) && ($input.min || $input.min == 0)} min="{$input.min}"{/if}
									  {if isset($input.max)} max="{$input.max}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.minlength) && $input.minlength} minlength="{$input.minlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
									  {if isset($input.required) && $input.required} required="required" {/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.autocorrect) && $input.autocorrect} autocorrect="{$input.autocorrect}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
									  {if isset($input.step)} step="{$input.step}"{/if}
								/>{if $input.button}{$input.button}{/if}
								{elseif $input.type == 'select'}
									<select name="{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}"
									        class="{if isset($input.class)}{$input.class|escape:'html':'utf-8'}{/if}{if $input.live_search} selectpicker{/if} form-control"
									        id="{if isset($input.id)}{$input.id|escape:'html':'utf-8'}{else}{$input.name|escape:'html':'utf-8'}{/if}"
										  {if isset($input.multiple) && $input.multiple} multiple="multiple"{/if}
										  {if isset($input.size)} size="{$input.size|escape:'html':'utf-8'}"{/if}
										  {if isset($input.onchange)} onchange="{$input.onchange|escape:'html':'utf-8'}"{/if}
										  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
										  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
										  {if isset($input.js) && $input.js} onchange="{$input.js}"{/if}
										  {if isset($input.required) && $input.required} required="required"{/if}
										  {if isset($input.live_search) && $input.live_search} data-live-search="true"{/if}
										  {if isset($input.data)}
										{foreach from=$input.data key=k item=$input_data}
											data-{$k}="{$input_data}"
										{/foreach}
										  {/if}>
										{if isset($input.options.default)}
											<option value="{$input.options.default.val|escape:'html':'utf-8'}"
												  {if isset($input.string_format)}
														{if isset($input.multiple)}
															{foreach $input.string_format as $input_string_format}
																{if $input_string_format == $input.options.default.val} selected="selected"{/if}
															{/foreach}
														{else}
															{if $input.string_format == $input.options.default.val} selected="selected"{/if}
														{/if}
													{elseif isset($input.val)}
														{if isset($input.multiple)}
															{foreach $input.val as $input_val}
																{if $input_val == $input.options.default.val} selected="selected"{/if}
															{/foreach}
														{else}
															{if $input.val == $input.options.default.val} selected="selected"{/if}
														{/if}
												  {/if}>{$input.options.default.text|escape:'html':'utf-8'}</option>
										{/if}
										{if isset($input.options.val)}
											{foreach $input.options.val as $option}
												<option value="{$option.val|escape:'html':'utf-8'}"{if isset($option.parent)} data-parent="{$option.parent}"{/if}{if isset($option.parent_name)} data-parent_name="{$option.parent_name}"{/if}
													  {if isset($option.data)}
														  {foreach from=$option.data key=k item=option_data}
															  data-{$k}="{$option_data}"
														  {/foreach}
													  {/if}
													  {if isset($input.string_format)}
													{if isset($input.multiple)}
														{foreach $input.string_format as $input_string_format}
															{if $input_string_format == $option.val} selected="selected"{/if}
														{/foreach}
													{else}
														{if $input.string_format == $option.val} selected="selected"{/if}
													{/if}
													  {elseif isset($input.val)}
													{if isset($input.multiple)}
														{foreach $input.val as $input_val}
															{if $input_val == $option.val} selected="selected"{/if}
														{/foreach}
													{else}
														{if $input.val == $option.val} selected="selected"{/if}
													{/if}
													  {/if}>{$option.text|escape:'html':'utf-8'}</option>
											{/foreach}
										{/if}
									</select>
								{elseif $input.type == 'change-password'}
								<div class="row">
									<div class="col-lg-12">
										<button type="button" id="{$input.name}-btn-change"
										        class="btn btn-default">
											<i class="icon-lock"></i>
											{l s='更改密碼'}
										</button>
										<div id="{$input.name}-change-container"
										     class="form-password-change well hide">
											{*<div class="form-group">*}
												{*<label for="old_passwd"*}
												       {*class="main_t control-label col-lg-2 required">*}
													{*{l s='當前密碼'}*}
												{*</label>*}
												{*<div class="col-lg-10">*}
													{*<div class="input-group fixed-width-lg">*}
                                                                                    {*<span class="main_t_2 input-group-addon">*}
                                                                                          {*<i class="glyphicon glyphicon-lock"></i>*}
                                                                                    {*</span>*}
														{*<input type="password"*}
														       {*id="old_passwd"*}
														       {*name="old_passwd"*}
														       {*class="form-control" required*}
														       {*autocomplete="off"*}
														       {*placeholder="{l s='現在使用的密碼'}">*}
													{*</div>*}
												{*</div>*}
											{*</div>*}
											{*<hr/>*}
											<div class="form-group">
												<label for="{$input.name}"
												       class="main_t required control-label col-lg-2">
                                                                              <span class="main_t label-tooltip"
                                                                                    data-toggle="tooltip"
                                                                                    data-html="true" title=""
                                                                                    data-original-title="{l s='密碼最少要8個字元'}">
                                                                                    {l s='新密碼'}
                                                                              </span>
												</label>
												<div class="col-lg-6">
													<div class="input-group fixed-width-lg">
                                                                                    <span class="main_t_2 input-group-addon">
                                                                                          <i class="glyphicon glyphicon-lock"></i>
                                                                                    </span>
														<input type="password"
														       id="{if !empty($input.id)}{$input.id}{else}{$input.name}{/if}"
														       name="{$input.name}"
														       class="form-control{if isset($input.class)} {$input.class}{/if}"
														       required autocomplete="off"
														       placeholder="{l s='新密碼'}"/>
													</div>
													<span id="{$input.name}-output"></span>
												</div>
											</div>
											<div class="form-group">
												<label for="{$input.name}2"
												       class="main_t required control-label col-lg-2">
													{l s='確認密碼'}
												</label>
												<div class="col-lg-6">
													<div class="input-group fixed-width-lg">
                                                                                    <span class="main_t_2 input-group-addon">
                                                                                          <i class="glyphicon glyphicon-lock"></i>
                                                                                    </span>
														<input type="password"
														       id="{$input.name}2"
														       name="{$input.name}2"
														       class="form-control{if isset($input.class)} {$input.class}{/if}"
														       value="" autocomplete="off"
														       placeholder="{l s='確認密碼'}"/>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<button type="button"
													        id="{$input.name}-cancel-btn"
													        class="btn btn-default">
														<i class="icon-remove"></i>
														{l s='取消'}
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>{/strip}
								<script type="text/javascript">
									$(function () {
										var $oldPwd = $('#old_passwd');
										var $passwordField = $('#{$input.name}');
										var $output = $('#{$input.name}-output');
										var $generateBtn = $('#{$input.name}-generate-btn');
										var $generateField = $('#{$input.name}-generate-field');
										var $cancelBtn = $('#{$input.name}-cancel-btn');
										var $container = $('#{$input.name}-change-container');
										var $changeBtn = $('#{$input.name}-btn-change');
										var $confirmPwd = $('#{$input.name}2');
										$changeBtn.on('click', function () {
											$container.removeClass('hide');
											$changeBtn.addClass('hide');
										});
										$cancelBtn.on('click', function () {
											$container.find("input").val("");
											$container.addClass('hide');
											$changeBtn.removeClass('hide');
										});
										var email_type = true;
										if ($('input[name="email"]').attr('type') != 'email') email_type = false;
										$('#{if isset($form.id)}{$form.id}{elseif isset($form.name)}{$form.name}{else}{$table}{/if}').validate({
											rules: {
												"email": {
													email: email_type
												},
												"{$input.name}": {
													minlength: 8
												},
												"{$input.name}2": {
													password_same: true
												},
												"old_passwd": {},
											},
											// override jquery validate plugin defaults for bootstrap 3
											highlight: function (element) {
												$(element).closest('.form-group').addClass('has-error');
											},
											unhighlight: function (element) {
												$(element).closest('.form-group').removeClass('has-error');
											},
											errorElement: 'span',
											errorClass: 'help-block',
											errorPlacement: function (error, element) {
												if (element.parent('.input-group').length) {
													error.insertAfter(element.parent());
												} else {
													error.insertAfter(element);
												};
											}
										});
									});
								</script>{strip}
								{elseif $input.type == 'password' || $input.type == 'new-password'}

								<input type="password"
								       id="{if !empty($input.id)}{$input.id}{else}{$input.name}{/if}"
								       name="{$input.name}"
								       class="form-control {if isset($input.class)}{$input.class}{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.minlength) && $input.minlength} minlength="{$input.minlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
                                                       autocomplete="off"/>

								{elseif $input.type == 'confirm-password'}

								<input type="password"
								       id="{if !empty($input.id)}{$input.id}{else}{$input.name}{/if}"
								       name="{$input.name}"
								       class="form-control {if isset($input.class)}{$input.class}{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.minlength) && $input.minlength} minlength="{$input.minlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
                                                       autocomplete="off"/>

								{elseif $input.type == 'switch'}
								{if $input.is_prefix || $input.is_suffix}
								<div class="input-group-btn">{/if}
									<div class="btn-group{if isset($input.btn_group)} btn-group-{$input.btn_group}{/if}" data-toggle="buttons">
										{foreach $input.values as $value}
											<label class="btn btn-default{if isset($input.string_format)}
												{foreach $input.string_format as $string_format_v}
													{if $string_format_v == $value.value} active{/if}
												{/foreach}
											{else}
												{foreach $input.val as $value_value_v}
													{if $value_value_v == $value.value} active{/if}
												{/foreach}
											{/if}{if isset($value.label_class)} {$value.label_class}{/if}"
											       for="{$value.id}">
												<input type="radio" autocomplete="off"
												       name="{$input.name}" id="{$value.id}"
												       value="{$value.value}"
													  {if isset($input.string_format)}
														  {foreach $input.string_format as $string_format_v}
															  {if $string_format_v == $value.value} checked="checked"{/if}
														  {/foreach}
													  {else}
														  {foreach $input.val as $value_value_v}
															  {if $value_value_v == $value.value} checked="checked"{/if}
														  {/foreach}
													  {/if}
													  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
													/>{$value.label}
											</label>
										{/foreach}
									</div>
									{if $input.is_prefix || $input.is_suffix}</div>{/if}
								{elseif $input.type == 'checkbox'}
								{if $input.is_prefix || $input.is_suffix}
								<div class="input-group-btn">{/if}
								<div class="btn-group{if isset($input.btn_group)} btn-group-{$input.btn_group}{/if}{if isset($input.class)} {$input.class}{/if}"{if !is_null($input.data.toggle)} data-toggle="{$input.data.toggle}"{else} data-toggle="buttons"{/if}>
									{foreach $input.values as $value}
										<label class="{if !isset($input.checkbox.class)}btn btn-default checkbox{else}{$input.checkbox.class}{/if}{if isset($input.string_format)}
                                                            	{foreach $input.string_format as $string_format_v}
                                                            	{if $string_format_v == $value.value} active{/if}
                                                                  {/foreach}
                                                            {else}
											{foreach $input.val as $value_value_v}
                                                            	{if $value_value_v == $value.value} active{/if}
                                                                  {/foreach}
                                                            {/if}{if isset($value.label_class)} {$value.label_class}{/if}"
										       for="{$value.id}">
											<input type="checkbox" autocomplete="off"
											       name="{$input.name}{if (isset($input.multiple) && $input.multiple) || (count($input.val) > 1)}[]{/if}" id="{$value.id}" data-class_id="{$value.class_id}"
											       value="{$value.value}"
												  {if isset($input.string_format)}
													  {foreach $input.string_format as $string_format_v}
														  {if $string_format_v == $value.value} checked="checked"{/if}
													  {/foreach}
												  {else}
													  {foreach $input.val as $value_value_v}
														  {if $value_value_v == $value.value} checked="checked"{/if}
													  {/foreach}
												  {/if}
												  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}/>{$value.label}
										</label>
									{/foreach}
								</div>
								{if $input.is_prefix || $input.is_suffix}</div>{/if}
								{elseif $input.type == 'textarea'}
								<textarea type="text"
								          id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
								          name="{$input.name}{if isset($input.multiple) && $input.multiple}[]{/if}"
								          class="form-control {if isset($input.class)}{$input.class}{/if}{if $input.type == 'tags'} tagify{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.minlength) && $input.minlength} minlength="{$input.minlength|intval}"{/if}
									  {if isset($input.rows)} rows="{$input.rows|intval}"{/if}
									  {if isset($input.cols)} cols="{$input.cols|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
									  {if isset($input.required) && $input.required} required="required" {/if}
									  {if isset($input.autocorrect) && $input.autocorrect} autocorrect="autocorrect" {/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.data)}
									{foreach from=$input.data key=k item=$input_data}
										data-{$k}="{$input_data}"
									{/foreach}
									  {/if}>
									{if isset($input.string_format)}{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}{else}{$input.val|escape:'html':'UTF-8'}{/if}</textarea>
								{elseif $input.type == 'signature'}
								<div id="signature_{$input.name}_div" class="signature_div">
									<div id="signature_{$input.name}" class="img_div" style="max-width:{$input.signature.width+2}px;max-height:{$input.signature.height+2}px;height:{$input.signature.height+2}px;{*if $arr_signature_data[$input.name].data == ''}display:none;{/if*}">{if $arr_signature_data[$input.name].data != ''}<img src="data:{if isset($input.string_format)}{$input.string_format}{else}{$arr_signature_data[$input.name].data}{/if}">{/if}</div>
									{if $display != 'view' && $input.key.type != 'view' && empty($arr_signature_data[$input.name].data) && empty($arr_signature_data[$input.name].data)}<input type="hidden" id="{$input.name}" name="{$input.name}" value="{if isset($input.string_format)}{$input.string_format}{else}{$arr_signature_data[$input.name].data}{/if}">
									<div class="signature_btn">
										<button type="button" class="open_signature btn btn-default" title="{l s="簽名"}"><i class="glyphicon glyphicon-pencil"></i></button>
										<button type="button" class="close_signature btn btn-default" style="display:none;" title="{l s="關閉"}"><i class="glyphicon glyphicon-remove"></i></button>
										<button type="button" class="reset_signature btn btn-default" style="display:none;" title="{l s="清除"}"><i class="glyphicon glyphicon-refresh"></i></button>
										<button type="button" class="ok_signature btn btn-default pull-right" style="display:none;" title="{l s="確定"}"><i class="glyphicon glyphicon-ok"></i></button>
									</div>{else}
									{/if}
								</div>
								{if $display != 'view'}<script type="text/javascript">
								var $div = '#signature_{$input.name}_div';
								function close_signature_{$input.name}(){
									$('#signature_{$input.name}', $div).jSignature('destroy');
									if(screenfull.enabled){
										screenfull.exit();
									};
									$('.reset_signature', $div).hide();
									$('.close_signature', $div).hide();
									$('.ok_signature', $div).hide();
									$('#signature_{$input.name}').removeClass('full');
									$('.open_signature', $div).show();
									$('.signature_btn', $div).removeClass('full');
								};
								$('.open_signature', $div).click(function(){
									if(screenfull.enabled && {$input.signature.screen_full}) {
										screenfull.request();
										setTimeout(function(){
											$('#signature_{$input.name}', $div).jSignature({ lineWidth: Math.max(1, $(window).height() / 180), width: $(window).width(), height: $(window).height() });
											$('#signature_{$input.name}').show().addClass('full');
											$('.signature_btn', $div).addClass('full');
										}, 500);

									}else{
										$('#signature_{$input.name}', $div).jSignature({ lineWidth: {$input.signature.lineWidth}, width: Math.min({$input.signature.width}, $(window).width()), height: Math.min({$input.signature.height}, $(window).height()) });
										$(this).hide();
										$('#signature_{$input.name}').show();
									};
									$(this).hide();
									$('#signature_{$input.name} img', $div).addClass('hidden');
									$('.reset_signature', $div).show();
									$('.close_signature', $div).show();
									$('.ok_signature', $div).show();
								});
								$('.reset_signature', $div).click(function(){
									$('#signature_{$input.name}', $div).jSignature('clear');
								});
								$('.close_signature', $div).click(function(){
									close_signature_{$input.name}();
									$('#signature_{$input.name} img', $div).removeClass('hidden');
									if($('#signature_{$input.name} img', $div).length == 0){
										$('#signature_{$input.name}').hide();
									};
								});
								$('.ok_signature', $div).click(function(){
									var img_data = $('#signature_{$input.name}', $div).jSignature('getData', 'image');
									$('#signature_{$input.name} img', $div).remove();
									if (typeof img_data === 'string'){
										$('#{$input.name}').val(img_data);
										$('#signature_{$input.name}', $div).html('<img src="data:'+img_data+'">');
									} else if($.isArray(img_data) && img_data.length === 2){
										$('#{$input.name}').val(img_data.join(','));
									$('#signature_{$input.name}', $div).html('<img src="data:'+img_data.join(',')+'">');
									} else {
										try {
											$('#{$input.name}').val(JSON.stringify(img_data));
										} catch (ex) {
											$('#{$input.name}').val('Not sure how to stringify this, likely binary, format.');
										};
									};
									close_signature_{$input.name}();
								});
								</script>{/if}
								{elseif $input.type == 'take_photo'}
								<div id="take_photo_{$input.name|escape:'html':'utf-8'}_div" class="take_photo_div">
									<div class="img_list">
									{foreach from=$arr_take_photo_data[$input.name] key=k item=$img_data}<div class="img_div file-preview-frame krajee-default kv-preview-thumb">
										<img class="img" src="{$img_data.data}"/>
										{if $img_data.type != 'view'}<input type="hidden" name="{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}" value="">
										<input type="hidden" name="id_img_{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}" value="{$img_data.index}">
										{if $input.key.type != 'view'}
										<div class="file-actions"><div class="file-footer-buttons">
										<bottom type="button" class="kv-file-remove btn btn-kv btn-default btn-outline-secondary del" title="{l s="刪除"}"><i class="glyphicon glyphicon-trash"></i></bottom>
										</div>
										</div>{/if}{/if}
										</div>{/foreach}
									</div>{if $display != 'view' && $input.key.type != 'view'}<div class="img_div file-preview-frame krajee-default kv-preview-thumb">
										<div id="take_photo_{$input.name|escape:'html':'utf-8'}"></div>
										<div class="file-footer-buttons">
											<button type="button" class="open_take btn" title="{$input.take_photo.open_txt}" style="width:{$input.take_photo.width}px;height:{$input.take_photo.height}px;"><i class="glyphicon glyphicon-facetime-video" style="font-size:{$input.take_photo.font_size};"></i></button>
											<button type="button" class="take btn btn-default" style="display:none;" title="{$input.take_photo.take_txt}"><i class="glyphicon glyphicon-camera"></i></button>
											<button type="button" class="close_take btn btn-default" style="display:none;" title="{$input.take_photo.close_txt}"><i class="glyphicon glyphicon-off"></i></button>
										</div>
									</div>{/if}
								</div>
								<script type="text/javascript">
									var $take_photo_{$input.name} = $('#take_photo_{$input.name}_div');
									function open_take_photo_{$input.name|escape:'html':'utf-8'}(){
										Webcam.reset();
										Webcam.set({
											width: {$input.take_photo.width},
											height: {$input.take_photo.height},
											image_format: 'png',
											jpeg_quality: 100
										});
										Webcam.attach('#take_photo_{$input.name}');
										$('#take_photo_{$input.name|escape:'html':'utf-8'}').stop(true, false).fadeIn();
										$('.take', $take_photo_{$input.name}).stop(true, false).fadeIn();
										$('.close_take', $take_photo_{$input.name}).stop(true, false).fadeIn();
										$('.open_take', $take_photo_{$input.name}).hide();
									};
									function close_take_photo_{$input.name|escape:'html':'utf-8'}(){
										$('#take_photo_{$input.name|escape:'html':'utf-8'}').hide();
										$('.take', $take_photo_{$input.name}).hide();
										$('.close_take', $take_photo_{$input.name}).hide();
										$('.open_take', $take_photo_{$input.name}).stop(true, false).fadeIn();
										Webcam.reset();
									};
									function take_photo_{$input.name|escape:'html':'utf-8'}(){
										shutter.play();
										Webcam.snap( function(data_uri) {
											$('.img_list', $take_photo_{$input.name}).append('<div class="img_div file-preview-frame krajee-default  kv-preview-thumb"><img class="img" src="' + data_uri + '"/><input type="hidden" name="{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}" value="' + data_uri + '"><input type="hidden" name="id_img_{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}" value=""><div class="file-actions"><div class="file-footer-buttons"><button type="button" class="kv-file-remove btn btn-kv btn-default btn-outline-secondary del" title="{l s="刪除"}"><i class="glyphicon glyphicon-trash"></i></button></div></div></div>');
										});
									};
									$('.open_take', $take_photo_{$input.name}).click(function(){
										open_take_photo_{$input.name|escape:'html':'utf-8'}();
										$('#take_photo_{$input.name}').addClass('pointer').attr('title', '{$input.take_photo.take_txt}');
									});
									$('.close_take', $take_photo_{$input.name}).click(function(){
										close_take_photo_{$input.name|escape:'html':'utf-8'}();
										$('#take_photo_{$input.name}').removeClass('pointer').attr('title', '');
									});
									$('.take', $take_photo_{$input.name}).click(function(){
										{if isset($input.multiple) && $input.multiple}
										take_photo_{$input.name}();
										{else}
										$('.img_list .img_div', $take_photo_{$input.name}).remove();
										take_photo_{$input.name}();
										{/if}
									});
									$(document).on('click', '#take_photo_{$input.name}_div .del', function(){
										if(confirm('{$input.take_photo.confirm}')){
											$(this).parents('.img_div').remove();
										};
									});
									$(document).on('click', '#take_photo_{$input.name}_div video', function(){
										{if isset($input.multiple) && $input.multiple}
											take_photo_{$input.name}();
										{else}
											$('.img_list .img_div', $take_photo_{$input.name}).remove();
											take_photo_{$input.name}();
										{/if}
									});
								</script>
								{elseif $input.type == 'map'}
								<div id="map" data-draggable="{if $display == 'edit' || $display == 'add'}true{else}false{/if}"{if isset($input.data)} class="google_map {if isset($input.class)}{$input.class}{/if}"
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}></div>
								{elseif $input.type == 'file'}
								<input type="file"
								       id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
								       name="{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}"
								       class="form-control {if isset($input.class)}{$input.class}{/if}"
								       value="{if isset($input.string_format)}{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}{else}{$input.val|escape:'html':'UTF-8'}{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.min) && ($input.min || $input.min == 0)} min="{$input.min}"{/if}
									  {if isset($input.max)} max="{$input.max}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.minlength) && $input.minlength} minlength="{$input.minlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
									  {if isset($input.required) && $input.required} required="required" {/if}
									  {if isset($input.multiple) && $input.multiple} multiple="multiple"{/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.autocorrect) && $input.autocorrect} autocorrect="{$input.autocorrect}"{/if}
									  {if isset($input.file.type)} data-allowed-file-extensions="{$input.file.type}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
								/>
								{/strip}
								<script type="text/javascript">
									{if isset($input.file.advanced) && $input.file.advanced}

									{else}
									var initialPreview_{$input.name} = [];
									var initialPreviewConfig_{$input.name} = [];
									var UpFileVal_{$input.name} = [];
									{if !empty($initialPreview[$input.name])}initialPreview_{$input.name} = {$initialPreview[$input.name]};{/if}
									{if !empty($initialPreviewConfig[$input.name])}initialPreviewConfig_{$input.name} = {$initialPreviewConfig[$input.name]};{/if}
									{if !empty($UpFileVal[$input.name])}UpFileVal_{$input.name} = {$UpFileVal[$input.name]};{/if}
									$('#{if isset($input.id)}{$input.id}{else}{$input.name}{/if}').fileinput({
										{if !isset($input.file.ajax)}
										uploadUrl: document.location.pathname,
										{else}
										{if $input.file.ajax}uploadUrl: document.location.pathname,{/if}
										{/if}
										initialPreviewAsData: true,
										autoUpload: true,
										validateInitialCount: true,
										{if $input.file.auto_upload}
									   		showUpload: false,
									    	showRemove: false,
										{/if}
										{if $input.file.size > 0}maxFileSize:{$input.file.size},{/if}
										{if !empty($input.file.class)}mainClass: '{$input.file.class}',{/if}
										language: '{if isset($input.file.language)}{$input.file.language}{else}zh-TW{/if}',
										minFileCount: {if isset($input.file.min)}{$input.file.min}{else}'auto'{/if},
										maxFileCount: {if isset($input.file.max)}{$input.file.max}{else}'auto'{/if},
										{if !empty($input.file.class)}mainClass: '{$input.file.class}',{/if}
										{if !empty($input.file.resize) && $input.file.resize}resizeImage: true,{/if}
										{if !empty($input.file.resizePreference)}resizePreference: '{$input.file.resizePreference}',{/if}
										{if !empty($input.file.min_width)}minImageWidth: '{$input.file.min_width}',{/if}
										{if !empty($input.file.min_hight)}minImageHeight: '{$input.file.min_hight}',{/if}
										{if !empty($input.file.max_width)}maxImageWidth: '{$input.file.max_width}',{/if}
										{if !empty($input.file.max_height)}maxImageHeight: '{$input.file.max_height}',{/if}
										initialPreview: initialPreview_{$input.name},
										overwriteInitial: {if isset($input.file.overwrite)}{$input.file.overwrite}{else}false{/if},
										initialPreviewFileType: 'image',
										initialPreviewConfig: initialPreviewConfig_{$input.name},
										{if isset($input.file.allowed)}allowedFileExtensions:{$input.file.allowed|@json_encode},{/if}
										uploadExtraData: function(){
										return {
											ajax: true,
											action: '{if isset($input.file.action)}{$input.file.action}{else}UpFile{/if}',
											UpFileVal: UpFileVal_{$input.name},
											input_name:'{$input.name}',
											data:{if $input.file.data != ''}{$input.file.data}{else}''{/if}
										};},
										{if !isset($input.file.icon) || $input.file.icon}
										preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
									    previewFileIconSettings: { // configure your icon file extensions
										  'doc': '<i class="fa fa-file-word text-primary"></i>',
										  'xls': '<i class="fa fa-file-excel text-success"></i>',
										  'ppt': '<i class="fa fa-file-powerpoint text-danger"></i>',
										  'pdf': '<i class="fa fa-file-pdf text-danger"></i>',
										  'zip': '<i class="fa fa-file-archive text-muted"></i>',
										  'htm': '<i class="fa fa-file-code text-info"></i>',
										  'txt': '<i class="fa fa-file-text text-info"></i>',
										  'mov': '<i class="fa fa-file-video text-warning"></i>',
										  'mp3': '<i class="fa fa-file-audio text-warning"></i>',
										  // note for these file types below no extension determination logic
										  // has been configured (the keys itself will be used as extensions)
										  // 'jpg': '<i class="fa fa-file-image text-danger"></i>',
										  // 'gif': '<i class="fa fa-file-image text-muted"></i>',
										  // 'png': '<i class="fa fa-file-image text-primary"></i>'
									    },
									    previewFileExtSettings: { // configure the logic for determining icon file extensions
										  'jpg': function(ext) {
											return ext.match(/(jpg|jpeg)$/i);
										  },
										  'doc': function(ext) {
											return ext.match(/(doc|docx)$/i);
										  },
										  'xls': function(ext) {
											return ext.match(/(xls|xlsx)$/i);
										  },
										  'ppt': function(ext) {
											return ext.match(/(ppt|pptx)$/i);
										  },
										  'zip': function(ext) {
											return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
										  },
										  'htm': function(ext) {
											return ext.match(/(htm|html)$/i);
										  },
										  'txt': function(ext) {
											return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
										  },
										  'mov': function(ext) {
											return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
										  },
										  'mp3': function(ext) {
											return ext.match(/(mp3|wav)$/i);
										  },
									    }
										{/if}
									}).on('filesorted', function (event, data, previewId, index) {
										{if $display != 'view'}
										var arr_id = new Array();
										for(var i in data.stack){
											arr_id.push(data.stack[i].key);
										};
										$.ajax({
											url : document.location.pathname,
											method : 'POST',
											data : {
												'ajax' : true,
												'action' : 'MoveFile',
												'arr_id' : arr_id,
											},
											dataType : 'json',
											success: function(data){
												if(data.error == '' || data.error == undefined){
													$.growl.notice({
														title: '',
														message: data.msg,
													});
												}else{
													$.growl.error({
														title: '',
														message: data.error,
													});
												};
											},
											error:function(xhr, ajaxOptions, thrownError){
											},
										});
										{/if}
									}).on('fileuploaded', function (event, data) {
										{$input.file.loaded}
									}).on("filebatchselected", function(event, data) {
										{if $input.file.auto_upload}
									 	$(this).fileinput('upload');
									 	{/if}
									});
									{/if}
									$('button.kv-file-zoom', '.file-footer-buttons').removeAttr('disabled').attr('disabled', false);
									{if $display == 'view'}
									$('button.kv-file-remove', '.file-footer-buttons').remove();
									$('.file-drag-handle', '.file-thumbnail-footer').remove();
									$('.input-group.file-caption-main', '.file-input').remove();
									{/if}
								</script>
								{strip}
								{elseif $input.type == 'view'}
								{if $input.multiple && isset($input.options)}
									<textarea type="text"
									          id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
									          class="view"
									          disabled style="width:100%;" rows="8"
										  {if isset($input.data)}
										{foreach from=$input.data key=k item=$input_data}

											data-{$k}="{$input_data}"
										{/foreach}
										  {/if}>{if !empty($input.val) || $input.val === 0 || $input.val === '0'}{$input.val}{/if}</textarea>
								{else}
									<samp class="form-control view {if isset($input.class)}{$input.class}{/if}"{if isset($input.rows) && $input.rows > 0} style="min-height:{$input.rows * 1.5}em;overflow:auto;"{/if}
									      id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
									      {if isset($input.data)}{foreach from=$input.data key=k item=$input_data} data-{$k}="{$input_data}"{/foreach}{/if}
										>{if ((!empty($input.val) || $input.val === 0 || $input.val === '0') && !in_array($input.validate, array('isdate', 'isdatetime'))) || ($input.validate == 'isdate' && $input.val != '0000-00-00') || ($input.validate == 'isdatetime' && $input.val != '0000-00-00 00:00:00')}{if stristr($input.class, 'tinymce') === false}{$input.val|nl2br}{else}{$input.val}{/if}{/if}</samp>
								{/if}
								{/if}
								{if isset($input.suffix_btn)}<div class="input-group-btn">
								{foreach $input.suffix_btn as $s_btn}
								<a class="btn btn-default {$s_btn.class}" href="{if $s_btn.href}{$s_btn.href}{else}javascript:void(0);{/if}"><span class="{$s_btn.icon}"></span>{$s_btn.title}</a>
								{/foreach}
								</div>{/if}{if isset($input.suffix)}<span class="main_t_2 input-group-addon{if isset($input.suffix_class)} {$input.suffix_class}{/if}">{$input.suffix}</span>{/if}{if ((isset($input.prefix) || isset($input.suffix) || isset($input.prefix_btn) || isset($input.suffix_btn)) && (!$input.is_prefix && !$input.is_suffix)) || (!$input.is_prefix && $input.is_suffix)}</div>{/if}
							{if isset($input.p) && $input.p}<p
								class="help-block{if isset($input.p_class) && ($input.p_class != "")} {$input.p_class}{/if}">{$input.p}</p>{/if}
							{if isset($input.html) && $input.html}{$input.html}{/if}
							{if (!$input.is_prefix && $input.is_suffix) || (!$input.is_prefix && !$input.is_suffix)}
						</div>{/if}
						{/block}
						{/if}
						{if (!$input.is_prefix && $input.is_suffix) || (!$input.is_prefix && !$input.is_suffix)}
					</div>{/if}
					{/block}
					{/foreach}
				</div>
				{/if}
				{/strip}{/foreach}
				{if $body_display}</div>{/if}
			{if $form.footer_display}
				{block name="footer"}{strip}
					<div class="panel-footer">
					{if isset($form.submit) && !empty($form.submit)}
						{foreach from=$form.submit item=sub}
            {if $id_group!=1 && $id_group!=2 && $id_group!=3 && isset($sub.keepsave) && $sub.keepsave}
            <!-- 除了管理員 不開放 keepsave-->
            {else}
							<button type="submit" value="1"
							        id="{if isset($sub.id)}{$sub.id}{else}{$table}_form_submit_btn{/if}"
							        name="{if isset($sub.name)}{$sub.name}{else}submit{$submit_display}{if !empty($form.table)}{$form.table}{else}{$form.tab}{/if}{/if}{if isset($sub.stay) && $sub.stay}AndStay{/if}{if isset($sub.keepsave) && $sub.keepsave}AndKeepsave{/if}"
							        class="{if isset($sub.class)}{$sub.class}{else}btn btn-default pull-right{/if}">
								<i class="{if isset($sub.icon)}{$sub.icon}{else}icon glyphicon glyphicon-floppy-disk{/if}"></i>{$sub.title}
							</button>
              {/if}
						{/foreach}
					{/if}

					{if isset($form.cancel) && $form.cancel}
						<a href="{$back_url|escape:'html':'UTF-8'}" class="btn btn-default"><i
								  class="{if isset($form.submit.icon)}{$form.submit.icon}{else}icon glyphicon glyphicon-remove{/if}"></i> {l s='取消'}
						</a>
					{/if}

					{if isset($form.reset)}
						<button type="reset"  id="{if isset($form.reset.id)}{$form.reset.id}{else}{$table}_form_reset_btn{/if}"  class="{if isset($form.reset.class)}{$form.reset.class}{else}btn btn-default{/if}">
							<i class="{if isset($form.submit.icon)}{$form.submit.icon}{else}icon glyphicon glyphicon-share-alt{/if}"></i> {$form.reset.title}
						</button>
					{/if}

					{if isset($form.buttons)}
						{foreach from=$form.buttons key=k item=btn}
							{if isset($btn.href) && trim($btn.href) != ''}
								<a href="{$btn.href}" {if isset($btn.id)}id="{$btn.id}"{/if}
								   class="btn btn-default{if isset($btn.class)} {$btn.class}{/if}" {if isset($btn.js) && $btn.js} onclick="{$btn.js}"{/if}>{if isset($btn.icon)}
										<i class="{$btn.icon}"></i> {/if}{$btn.title}</a>
							{else}
								<button type="{if isset($btn.type)}{$btn.type}{else}button{/if}"
								        {if isset($btn.id)}id="{$btn.id}"{/if}
								        class="btn btn-default{if isset($btn.class)} {$btn.class}{/if}"
								        name="{if isset($btn.name)}{$btn.name}{else}submitOptions{$table}{/if}"{if isset($btn.js) && $btn.js} onclick="{$btn.js}"{/if}>{if isset($btn.icon)}
										<i class="{$btn.icon}"></i> {/if}{$btn.title}</button>
							{/if}
						{/foreach}{/if}</div>{/strip}{/block}{/if}</div>
	</div>{if !empty($submit_action)}<input type="hidden" name="{$submit_action}" value="1"/>{/if}{/if}{/foreach}{if count($fields.form) > 0}<input type="hidden" name="post_code" value="{$post_code}"></form>{/if}{/block}
