$(document).ready(function() {
    var id_rent_house_community = $("#id_rent_house_community").val();
    if (id_rent_house_community != '') { //代表有被選擇
        ajax_work('first'); //有選擇則套入該規則
    }

    function ajax_work(str = '') { //ajax 自動帶入社區相關資料
        var id_rent_house_community = '';
        if (str == 'first') { //只有第一次
            id_rent_house_community = $("#id_rent_house_community").val();
        }

        var community_arr = ['id_rent_house_community', 'rent_house_community_code', 'community', 'all_num', 'id_exterior_wall', 'household_num', 'storefront_num', 'property_management', 'community_phone', 'community_fax', 'builders', 'redraw_area',
            'clean_time', 'time_s', 'time_e', 'id_public_utilities', 'e_school', 'j_school', 'park', 'market', 'night_market', 'supermarket', 'shopping_center', 'hospital', 'bus', 'bus_station', 'passenger_transport', 'passenger_transport_station',
            'train', 'mrt', 'mrt_station', 'high_speed_rail', 'interchange'
        ];

        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'Community',
                'id_rent_house_community': id_rent_house_community,
                'community': $("#community").val()

            },
            dataType: 'json',
            success: function(data) {
                if (data["error"] != "") {
                    alert(data["error"]);
                } else {
                    for (i = 0; i < community_arr.length; i++) {
                        if (community_arr[i] == 'id_exterior_wall') { //他是用select
                            $("#id_exterior_wall").val(data["return"][community_arr[i]]);
                        } else if (community_arr[i] != 'id_disgust_facility' && community_arr[i] != 'id_public_utilities') {
                            $("input[name='" + community_arr[i] + "']").val(data["return"][community_arr[i]]);
                        } else if (community_arr[i] == 'id_disgust_facility') { //針對 id_disgust_facility 因為它是多選
                            var id_disgust_facility_arr = data["return"]["id_disgust_facility"];
                            $("input[name='id_disgust_facility[]']").parent().removeClass('active');
                            $("input[name='id_disgust_facility[]']").prop('checked', false);
                            for (j = 0; j < document.getElementsByName('id_disgust_facility[]').length; j++) {
                                for (k = 0; k < id_disgust_facility_arr.length; k++) {
                                    // console.log(id_disgust_facility_arr[k]);
                                    // console.log(document.getElementsByName('id_disgust_facility[]')[j].value);
                                    if (document.getElementsByName('id_disgust_facility[]')[j].value == id_disgust_facility_arr[k]) {
                                        document.getElementsByName('id_disgust_facility[]')[j].checked = true;
                                        document.getElementsByName('id_disgust_facility[]')[j].parentNode.className += ' active';
                                    }
                                }
                            }
                        } else if (community_arr[i] == 'id_public_utilities') {
                            var id_public_utilities_arr = data["return"]["id_public_utilities"];
                            $("input[name='id_public_utilities[]']").parent().removeClass('active');
                            $("input[name='id_public_utilities[]']").prop('checked', false);
                            for (j = 0; j < document.getElementsByName('id_public_utilities[]').length; j++) {
                                for (k = 0; k < id_public_utilities_arr.length; k++) {
                                    if (document.getElementsByName('id_public_utilities[]')[j].value == id_public_utilities_arr[k]) {
                                        document.getElementsByName('id_public_utilities[]')[j].checked = true;
                                        document.getElementsByName('id_public_utilities[]')[j].parentNode.className += ' active';
                                    }
                                }
                            }
                        }
                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });
    }
});