$(function () {
	var max_depth = 5;	//選單層數
	$('.draggable_div .ui-state-default').draggable({
		connectToSortable: '#sortable',
		helper: 'clone',
		revert: 'invalid'
	});
	$('#sortable').sortable({
		handle: '.move',
		cancel: '.portlet-toggle',
		placeholder: 'placeholder',
		animation: 200,
		sort: function(event, ui) {
			sortable_depth(ui, max_depth);
		},
		stop: function(event, ui) {
			console.log('e');
		},
	});
});

/**
 *
 * @param ui
 * @param max_depth	選單層數
 */
function sortable_depth(ui, max_depth=5){
	for(var i=0;i<=max_depth;i++){
		var $placeholder = $('#sortable .placeholder');

		for(var j=0;j<=max_depth;j++){
			$placeholder.removeClass('item-depth-'+j);
		}

		if($placeholder.length > 0){
			if($placeholder.index('#sortable li') > 0){
				var depth = $placeholder.prev('li').data('depth');
				max_depth = Math.min($placeholder.prev('li').prev('li').data('depth') + 1, max_depth);

				var this_depth = Math.max(depth + parseInt(ui.position.left / 50), 0);
				this_depth = Math.min(this_depth, max_depth);
				console.log(this_depth);
				if(ui.position.left < -50 || 50 < ui.position.left){
					// console.log('depth = '+depth);
					// console.log('this_depth = '+this_depth);
					$placeholder.addClass('item-depth-' + this_depth);
				}else{
					$placeholder.addClass('item-depth-' + depth);
				}
			}else{
				$placeholder.addClass('item-depth-0');
			}
		};
	};
};