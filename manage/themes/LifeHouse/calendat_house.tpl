<div id="house_list">
	<p class="ui-widget-header"><button class="pull-right"><i class="glyphicon glyphicon-minus"></i></button></p>
<div class="list">
{foreach $house_list as $house}
	<div style="background-color: {$house.color};" data-id="{$house.id_schedule}">{$house.schedule_data} {$house.id_company}-[{$house.id_build_case}] {$house.status} {$house.id_repair_class[0]}</div>
{/foreach}
</div>
</div>
<style type="text/css">
	#house_list{
		background-color: #fff;
		position: absolute;
		z-index: 300;
		right:15px;
		font-size: 14px;
		width: 450px;
		border: 1px solid #1b6d85;
	}
	#house_list p{
		height:30px;
		margin: 0px;
		background-color: #27547a;
		width: 100%;
	}
	#house_list .list{
		padding: 0.5em;
		color: #fff;
	}
	#house_list .list div{
		padding:0.2em;
	}
	.fc-event-container .active{
		background-color: #efff00 !important;
		border-color: #f00 !important;
	}
</style>
<script>
	$('#house_list').draggable({
		handle: 'p'
	});
	function show_house(id){
		$('a', '.fc-widget-content').removeClass('active');
		$('a[href^="/manage/Schedule?&editschedule&id_schedule='+id+'"]', '.fc-widget-content').addClass('active');
	};
	function house_d(){
		var $list = $('.list', '#house_list');
		if($list.css('display') == 'none'){
			$('button i', '#house_list p').addClass('glyphicon-minus').removeClass('glyphicon-plus');
			$list.stop(true, false).slideDown();
		}else{
			$('button i', '#house_list p').removeClass('glyphicon-minus').addClass('glyphicon-plus');
			$list.stop(true, false).slideUp();
		};
	};
	$('button', '#house_list p').click(function(){
		house_d();
	});
	$('div', '#house_list').hover(function(){
		show_house($(this).data('id'));
	});
</script>