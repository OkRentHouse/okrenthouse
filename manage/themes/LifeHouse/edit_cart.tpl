<div class="edit_cart_{$sv} w50 edit_cart">
	<div class="close_edit_cart btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></div>
	<div><label for="chart{$sv}_w" class="pull-left">寬度</label><input type="range" min="0" max="50000" id="chart{$sv}_w" data-chart="{$sv}" class="custom-range" data-to="chart{$sv}_w" value="{$sv_w}"><div class="input-group"><input type="number" class="form-control" name="chart{$sv}_w" data-chart="{$sv}" min="0" max="50000" value="{$sv_w}"><span class="input-group-addon">px</span></div></div>
	<hr>
	<div><label for="chart{$sv}_h" class="pull-left">高度</label><input type="range" class="custom-range" min="0" max="50000" data-chart="{$sv}" id="chart{$sv}_h" data-to="chart{$sv}_h" value="{$sv_h}"><div class="input-group"><input type="number" class="form-control" data-chart="{$sv}" name="chart{$sv}_h" min="0" max="50000" value="{$sv_h}"><span class="input-group-addon">px</span></div></div>
	<hr>
	<div><label for="chart{$sv}_t" class="pull-left">刻度</label><input type="number" id="chart{$sv}_t" class="form-control" min="0" max="50000" name="chart{$sv}_t" data-chart="{$sv}" value="{$sv_t}"></div>
	<hr>
	{*<div><button type="button" class="btn btn-default" date-save="{$sv}">儲存設定</button></div>*}
</div>
<div class="text-center"><button type="button" class="dow_chart btn btn-default" data-id="chart{$sv}" data-title="{$county}{$district}{$region}{$arr_block.$block}{$chart_title}">圖表下載</button><button type="button" class="btn btn-default edit_cart_on" data-div="edit_cart_{$sv}">圖表編輯</button></div>