{$form_list_pagination}
{$form_list_field_list}{if count($fields.list) > 0}<form action="" method="get" class="list col-lg-12" accept-charset="utf-8">{/if}
<div class="panel panel-default">
	{if $fields.list_heading_display}
		{if isset($fields.show_list_num) && !empty($fields.show_list_num)}<div class="pull-right input-group pieces"><span class="">{l s="顯示 | "}</span><input type="number" min="0" id="{$list_id}Filter_m" name="{$list_id}Filter_m" class="input-sm" value="{$fields.list_num}" title="{l s="顯示幾筆資料"}" placeholder="{l s="50"}"><span class="">{l s="筆資料"}{l s=",共"}<samp>{$table_num}</samp>{l s="筆"}</div>{/if}
		<div class="panel-heading">
			{$fields.title}
		</div>
	{/if}
      {if $fields.list_body_display}
	<div class="panel-body">
      <table class="table table-bordered">{strip}
            {$form_list_thead}
            {$form_list_tbody}
            {$form_list_tfoot}
      {/strip}</table>
	</div>
	{/if}
      {if $fields.list_footer_display}<div class="panel-footer">{$fields.list_footer}</div>{/if}
</div>
{$form_list_pagination}
<input type="hidden" name="p" value="{$smarty.get.p}" />
<input type="hidden" name="m" value="{$smarty.get.m}" />
{if count($fields.list) > 0}</form>{/if}