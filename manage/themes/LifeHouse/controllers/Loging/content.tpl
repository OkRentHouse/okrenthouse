<form action="{if !empty($smarty.get.page)}?page={$smarty.get.page}{/if}" method="post" class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
      {if isset($smarty.get.flash)}
            <fieldset class="panel panel-default">
                  <div class="panel-heading">快速登入</div>
                  <div class="panel-body form-group"><label for="flash_barcode" class="col-lg-3 text-right"><samp class="required"></samp>條碼掃描</label><div class="col-lg-9"><input type="password" id="flash_barcode" class="form-control" name="flash_barcode" maxlength="16" required></div></div>
                  <div class="panel-body form-group"><label for="auto_loging" class="col-lg-3 text-right"><samp class="required"></samp>自動登入</label><div class="col-lg-9"><input type="checkbox" id="auto_loging" name="auto_loging" value="1"><a href="/manage/Loging" class="float-right">一般登入</a></div></div>
                <input type="hidden" name="AdminLogin">
            </fieldset>
      {else}
            <fieldset class="panel panel-default">
                  <div class="panel-heading">登入</div>
                  <div class="panel-body form-group"><label for="email" class="col-lg-3 text-right"><samp class="required">*</samp>帳號</label><div class="col-lg-9"><input type="text" id="email" class="form-control" name="email" maxlength="100" required  value="{$email}"></div></div>
                  <div class="panel-body form-group"><label for="password" class="col-lg-3 text-right"><samp class="required">*</samp>密碼</label><div class="col-lg-9"><input type="password" id="password" class="form-control" name="password" maxlength="20" required></div></div>
                  <div class="panel-body form-group"><label for="verification" class="col-lg-3 text-right"><samp class="required">*</samp>圖型驗證</label><div class="col-lg-9">
                              <div class="input-group">
                              <span class="input-group-addon">
                                    <img src="/verification">
                              </span>
                                    <input type="text" id="verification" class="form-control" name="verification" maxlength="20" required autocomplete="off">
                              </div>
                        </div>
                  </div>
                  <div class="panel-body form-group"><label for="auto_loging" class="col-lg-3 text-right"><samp class="required"></samp>自動登入</label><div class="col-lg-9"><input type="checkbox" id="auto_loging" name="auto_loging" value="1">{*<a href="/manage/Loging?flash" class="float-right">快速登入</a>*}</div></div>
                  <div class="panel-footer">
                        <button type="submit" id="loging" name="AdminLogin" class="submit btn btn-default col-lg-12">登入</button>
                  </div>
            </fieldset>
      {/if}
</form>