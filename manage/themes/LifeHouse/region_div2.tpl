<div class="region_div input-group search">
	<select name="county" class="form-control">
		<option value="">{l s="全部縣市"}</option>
		{foreach from=$arr_county key="i" item="countys"}
			<option value="{$countys}"{if $county==$countys} selected="selected"{/if}>{$countys}</option>
		{/foreach}
	</select>
	<select name="district" class="form-control">
		<option value="">顯示全部</option>
		{foreach from=$arr_district key="i" item="districts"}
			{foreach from=$districts key="j" item="d"}
				<option value="{$d}" data-parent="{$i}" data-parent_name="county"{if $district==$d} selected="selected"{/if}>{$d}</option>
			{/foreach}
		{/foreach}
	</select>
	<select name="region" class="form-control">
		<option value="">顯示全部重劃區</option>
		{foreach from=$arr_region key="r" item="r_parent"}
			<option value="{$r}" data-parent="{$r_parent}" data-parent_name="county"{if $region==$r} selected="selected"{/if}>{$r}</option>
		{/foreach}
	</select>
	<select name="block" class="form-control">
		<option value="">顯示全部區塊</option>
		{foreach from=$arr_block key="i" item="r"}
			<option value="{$i}" {if $block==$i} selected="selected"{/if}>{$r}</option>
		{/foreach}
	</select>
	<select name="s_y" class="form-control">
		{for $foo=$year.min_y to $year.max_y}
			<option value="{$foo}"{if $s_y==$foo} selected="selected"{/if}>{$foo-1911}</option>
		{/for}
	</select><span class="input-group-addon">~</span>
	<select name="e_y" class="form-control">
		{for $foo=$year.min_y to $year.max_y}
			<option value="{$foo}"{if $e_y==$foo} selected="selected"{/if}>{$foo-1911}</option>
		{/for}
	</select><span class="input-group-addon">年</span>
	<div class="input-group-btn"><button type="submit" name="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>搜尋</button></div>
</div>