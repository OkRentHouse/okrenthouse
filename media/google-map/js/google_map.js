var map, markerCluster;
var markers = [];
var markers_have = [];
var arr_markers = [];
var infoWindows = [];
var activeInfoWindow;
var ajaxMap = null;

//todo
function click_map_marker(marker) {
	console.log(marker);
	console.log(marker.data.id);
	// alert(marker.data.id);
}
function editGoogleMap() {
	$('.google_map').each(function () {
		var id = $(this).attr('id');
		var draggable = $(this).data('draggable');
		var lat_t = $(this).data('lat_key');
		var lng_t = $(this).data('lng_key');
		var address_t = $(this).data('address_key');
		var zoom = $(this).data('zoom');

		if (display == 'view') {
			var lat = $('#' + lat_t).html();
			var lng = $('#' + lng_t).html();
		} else {
			var lat = $('#' + lat_t).val();
			var lng = $('#' + lng_t).val();
		}
		if (lat === undefined) {
			lat = 0;
		}
		if (lng === undefined) {
			lng = 0;
		}
		if (zoom === undefined) {
			zoom = 1;
		}

		if (lat == 0 && lng == 0) {
			map_zoom = zoom;
			map_lat = 25.042227979033655;
			map_lng = 121.53051797724606;
		} else {
			map_zoom = 17;
			map_lat = parseFloat(lat);
			map_lng = parseFloat(lng);
		}
		map = new google.maps.Map(document.getElementById(id),
			{
				center: {
					lat: map_lat,
					lng: map_lng,
				},
				zoom: map_zoom,
				minZoom: 1,
				maxZoom: 21,
				streetViewControl: false,	//顯示街景
				fullscreenControl: true,		//全螢幕地圖
				zoomControl: false,			//放大縮小地圖
				mapTypeControl: false,		//地圖與衛星類型
				scrollwheel: true,
			});

		markers[id] = new google.maps.Marker({
			map: map,
			draggable: draggable,
			position: {lat: lat, lng: lng},
		});

		if ((lat_t !== undefined) && (lat_t !== '') && (lng_t !== undefined) && (lng_t !== '')) {
			let $lat = $('#' + lat_t);
			let $lng = $('#' + lng_t);
			//拖移錨點
			if (display == 'view') {
				var lat = parseFloat($lat.html());
				var lng = parseFloat($lng.html());
			} else {
				var lat = parseFloat($lat.val());
				var lng = parseFloat($lng.val());
				$(document).on('change', $lat, function () {
						lat = parseFloat($lat.val());
						lng = parseFloat($lng.val());
						var latlng = new google.maps.LatLng(lat, lng);
						map.setCenter(latlng);
						console.log(lat +","+ lng);
						setMarkerPosition(markers[id], lat, lng);

				}).on('change', '#' + lng_t, function () {

					lat = parseFloat($lat.val());
					lng = parseFloat($lng.val());
					var latlng = new google.maps.LatLng(lat, lng);
					map.setCenter(latlng);
					console.log(lat +","+ lng);
					setMarkerPosition(markers[id], lat, lng);

				});
				google.maps.event.addListener(markers[id], 'dragend', function () {
					setMarkerInput(markers[id].getPosition(), $lat, $lng);
				});
			}

			if (isNaN(lat) || isNaN(lng)) {
				setMarkerPosition(markers[id], map_lat, map_lng);
			} else {
				setMarkerPosition(markers[id], lat, lng);
			}
			lat = null;
			lng = null;
		}

		if (address_t !== undefined) {
			let $address = $('#' + address_t);
			$(document).on('change', $address, function () {

				var address = $address.val();
				var geocoder = new google.maps.Geocoder();

				geocoder.geocode({'address': address}, function (results, status) {
					if($("#complete_address").val()!=""){

					if (status == google.maps.GeocoderStatus.OK) {
						map.setCenter(results[0].geometry.location);
						map.setZoom(zoom);
						markers[id].setPosition(results[0].geometry.location);
						if ((lat_t !== undefined) && (lat_t !== '') && (lng_t !== undefined) && (lng_t !== '')) {
							$('#' + lat_t).val(markers[id].getPosition().lat());
							$('#' + lng_t).val(markers[id].getPosition().lng());
						}
					} else {
						console.log('Geocode was not successful for the following reason: ' + status);
					}
					}
				});

				address = null;
				geocoder = null;

			});
		}
	});
	id = null;
	draggable = null;
	lat_t = null;
	lng_t = null;
	address_t = null;
	zoom = null;
}

function setMarkerPosition(marker, lat, lng) {
	var latlng = new google.maps.LatLng(lat, lng);
	marker.setPosition(latlng);
	latlng = null;
};

function setMarkerInput(pos, $lat, $lng) {
	if (pos.lat() !== undefined && $lat !== undefined) {
		$lat.val(pos.lat());
	}
	if (pos.lng() !== undefined && $lng !== undefined) {
		$lng.val(pos.lng());
	}
};

function deleteEditMarkers(id) {
	if(markers[id] != null){
		markers[id].forEach(function (e) {
			e.setMap(null);
		});
	}
};

//清除地圖標記
function deleteMarkers() {
	console.log(markers);
	markers.forEach(function (e) {
		e.setMap(null);
	});
	markers = [];
	markers_have = [];
};

class MapAip {
	//map 標籤
	getTitle(item) {
		return item.title;
	}

	getHtml(item){
		return item.html;
	}
}

MapAip = new MapAip();

function initialize() {
	// 必須使用 .ajax_map 才會連資料
	$('.ajax_map').each(function () {
		var id = $(this).attr('id');
		// var markerCenter = new google.maps.LatLng(25.0625938, 121.5308613);
		// var mapOptions = {
		// 	zoom: 14,
		// 	center: {
		// 		lat: google_map.lat,
		// 		lng: google_map.lng,
		// 	},
		// 	disableDefaultUI: true,
		// };

		var lat = parseFloat($(this).data('lat'));
		var lng = parseFloat($(this).data('lng'));
		var zoom = parseFloat($(this).data('zoom'));
		var minZoom = parseFloat($(this).data('minzoom'));
		var maxZoom = parseFloat($(this).data('maxzoom'));
		map = new google.maps.Map(document.getElementById(id),
			{
				center: {
					lat: lat,
					lng: lng,
				},
				zoom: zoom,
				minZoom: minZoom,
				maxZoom: maxZoom,
				streetViewControl: false,	//顯示街景
				fullscreenControl: true,	//全螢幕地圖
				zoomControl: false,			//放大縮小地圖
				mapTypeControl: false,		//地圖與衛星類型
				scrollwheel: true,
			});

		//移動地圖更新
		google.maps.event.addListener(map, 'idle', function () {
			var bounds = map.getBounds();
			var sw = bounds.getSouthWest();		//南西
			var ne = bounds.getNorthEast();		//北東
			sw = [sw.lat(), sw.lng()];
			ne = [ne.lat(), ne.lng()];
			addMarker(id, sw, ne);
			bounds = null;
			sw = null;
			ne = null;
		});
		id = null;
		lat = null;
		lng = null;
		zoom = null;
		minZoom = null;
		maxZoom = null;
	});
	// $('.gm-style-pbc', $(this)).click(function () {
	// 	if(activeInfoWindow !== undefined){
	// 		activeInfoWindow.close();
	// 	};
	// });
}

//加入標記
function addMarker(id, south_west, north_east) {
	//建立地圖 marker 的集合

	var marker_list = [
		// {
		// 	id: 100,
		// 	lat: 25.0031,
		// 	lng: 121.5623212,
		// 	title: '總統府',
		// 	price: '15,000',
		// 	labelclass: 'labels',
		// 	w: 40,
		// 	h: 10,
		// },
		// {
		// 	id: 3,
		// 	lat: 25.0339031,
		// 	lng: 121.5623212,
		// 	title: '8,120,154',
		// 	price: '8,120,154',
		// 	labelclass: 'labels',
		// 	w: 40,
		// 	h: 10,
		// },
	];

	// for(var i=0;i<=10;i++) {
	// 	var lat_v = Math.random() * 4 + 21;
	// 	var lng_v = Math.random() * 2 + 120;
	// 	var id_v = Math.round(Math.random() *  10000000);
	// 	marker_list.push({
	// 		id: id_v,
	// 		lat: lat_v,
	// 		lng: lng_v,
	// 		title: 'test',
	// 		price: 'test',
	// 		labelclass: 'labels',
	// 		w: 40,
	// 		h: 10,
	// 	});
	// };


	if(ajaxMap !== null){
		ajaxMap.abort();
	}

	ajaxMap = $.ajax({
		url: $('#' + id).data('url'),
		type: 'GET',
		data: {
			'action': 'GetGoogleMarker',
			'sw': south_west,
			'ne': north_east,
		},
		success: function(data){

		},
		beforeSend:function(){

		},
		complete:function(){
			ajaxMap = null;
		},
		error:function(xhr, ajaxOptions, thrownError){
			console.log('資料錯誤!');
		},
	});

	marker_list.map(function (e, i) {
		var item = marker_list[i];
		if(markers_have[item.id] != true){
			var infowindow = new google.maps.InfoWindow({
				disableAutoPan : true,
			});
			//單一標籤item
			var marker = new MarkerWithLabel({
				draggable: false,
				raiseOnDrag: false,
				map: map[id],
				icon: {
					url: '',
					size: new google.maps.Size(30, 30),
					origin: new google.maps.Point(20, 0),
				},
				labelContent: MapAip.getTitle(item),
				labelAnchor: new google.maps.Point(item.w / 2, item.h),
				labelClass: item.labelclass,
				labelStyle: {
					opacity: 1,
				},
				position: {
					lat: item.lat,
					lng: item.lng,
				},
				labelInBackground: false,
				data:{
					'id' : item.id,
				},
			});

			markers[item.id] = marker;
			// //點開小視窗
			markers[item.id].addListener('click', function () {
				//一次開一個視窗
				if(activeInfoWindow !== undefined){
					activeInfoWindow.close();
				};

				if($('.gm-style-iw').length) {
					$('.gm-style-iw').parent().remove();
				}

				click_map_marker(markers[item.id]);
				infowindow.setContent(MapAip.getHtml(item));
				infowindow.open(map, markers[item.id]);
				activeInfoWindow = infowindow;
			});
			arr_markers[i] = marker;
			markers_have[item.id] = true;
		}
	});

	//集合
	markerCluster = new MarkerClusterer(map, arr_markers,
		{
			imagePath: THEME_URL + '/img/map_m/m',
			// styles : [{
			// 	fontWeight:"400",
			// 	textColor:'#3b1382',
			// 	height: 52,
			// 	width: 53,
			// }]
		});
	// markerCluster  = null;
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, 'load', editGoogleMap);
