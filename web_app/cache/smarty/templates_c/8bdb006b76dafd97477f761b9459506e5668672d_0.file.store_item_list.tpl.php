<?php
/* Smarty version 3.1.28, created on 2020-12-14 12:49:08
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/store/store_item_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd6eec4684e87_67041665',
  'file_dependency' => 
  array (
    '8bdb006b76dafd97477f761b9459506e5668672d' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/store/store_item_list.tpl',
      1 => 1607678517,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd6eec4684e87_67041665 ($_smarty_tpl) {
?>
<div class="store store_list">
	<?php
$_from = $_smarty_tpl->tpl_vars['arr_store']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_store_0_saved_item = isset($_smarty_tpl->tpl_vars['store']) ? $_smarty_tpl->tpl_vars['store'] : false;
$__foreach_store_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['store'] = new Smarty_Variable();
$__foreach_store_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_store_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['store']->value) {
$__foreach_store_0_saved_local_item = $_smarty_tpl->tpl_vars['store'];
?>
		<div class="item" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
">
			<div class="img"><a href="/Store?id=<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><img src="<?php if ($_smarty_tpl->tpl_vars['store']->value['img']) {
echo $_smarty_tpl->tpl_vars['store']->value['img'];
} else {
echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/default.jpg<?php }?>"></a><div class="favorite <?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?>on<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
" data-type="store"><?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg"><?php }?></div></div>
			<div class="main_title"><div class="title"><?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
</div><div class="career"><?php echo $_smarty_tpl->tpl_vars['store']->value['store_type'];?>
</div><div class="popularity div_popularity give_star" data-title="<?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['popularity_img'];?>
</div></div>
			<?php if (count($_smarty_tpl->tpl_vars['store']->value['service']) > 0) {?><div class="hot"><div class="subtitle"><i><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/hot.svg"></i>HOT</div><div><?php
$_from = $_smarty_tpl->tpl_vars['store']->value['service'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_service_1_saved_item = isset($_smarty_tpl->tpl_vars['service']) ? $_smarty_tpl->tpl_vars['service'] : false;
$__foreach_service_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['service'] = new Smarty_Variable();
$__foreach_service_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_service_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['service']->value) {
$__foreach_service_1_saved_local_item = $_smarty_tpl->tpl_vars['service'];
?><samp><?php echo $_smarty_tpl->tpl_vars['service']->value;?>
</samp><?php
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_1_saved_local_item;
}
}
if ($__foreach_service_1_saved_item) {
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_1_saved_item;
}
if ($__foreach_service_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_service_1_saved_key;
}
?></div></div><?php }?>
			<?php if (count($_smarty_tpl->tpl_vars['store']->value['discount']) > 0) {?><div class="offer"><div class="subtitle">OFFER</div><?php
$_from = $_smarty_tpl->tpl_vars['store']->value['discount'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_discount_2_saved_item = isset($_smarty_tpl->tpl_vars['discount']) ? $_smarty_tpl->tpl_vars['discount'] : false;
$__foreach_discount_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['discount'] = new Smarty_Variable();
$__foreach_discount_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_discount_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['discount']->value) {
$__foreach_discount_2_saved_local_item = $_smarty_tpl->tpl_vars['discount'];
?><a href="/Store?id=<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</a><?php
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_2_saved_local_item;
}
}
if ($__foreach_discount_2_saved_item) {
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_2_saved_item;
}
if ($__foreach_discount_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_discount_2_saved_key;
}
?></div><?php }?>
		</div>
	<?php
$_smarty_tpl->tpl_vars['store'] = $__foreach_store_0_saved_local_item;
}
} else {
?>
		<div class="item text-center">
			查不到相關店家
		</div>
	<?php
}
if ($__foreach_store_0_saved_item) {
$_smarty_tpl->tpl_vars['store'] = $__foreach_store_0_saved_item;
}
if ($__foreach_store_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_store_0_saved_key;
}
?>
</div>
<div class="store_list_loading text-center"><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/lds.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
</div><?php }
}
