<?php
/* Smarty version 3.1.28, created on 2020-12-14 11:23:59
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/house/search_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd6dacf21e082_03049253',
  'file_dependency' => 
  array (
    '0b31acabe0bcd8351fb8cdcf174d55331ea4133b' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/house/search_list.tpl',
      1 => 1607678514,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd6dacf21e082_03049253 ($_smarty_tpl) {
?>
<form id="search_form" name="search_form" method="get" action="/list">
    <input type="hidden" id="active" name="active" value="<?php echo $_GET['active'];?>
">
    <input type="hidden" name="house_choose" value="<?php echo $_GET['house_choose'];?>
">
    <input type="hidden" name="id_main_house_class" value="<?php echo $_GET['id_main_house_class'];?>
">
    <input type="hidden" name="id_types" value="<?php echo $_GET['id_types'];?>
">
    <input type="hidden" name="id_type" value="<?php echo $_GET['id_type'];?>
">
    <input type="hidden" name="county" value="<?php echo $_GET['county'];?>
">
    <input type="hidden" name="city" value="<?php echo $_GET['city'];?>
">
    <input type="hidden" name="max_price" value="<?php echo $_GET['max_price'];?>
">
    <input type="hidden" name="min_price" value="<?php echo $_GET['min_price'];?>
">
    <input type="hidden" name="max_room" value="<?php echo $_GET['max_room'];?>
">
    <input type="hidden" name="min_room" value="<?php echo $_GET['min_room'];?>
">
    <input type="hidden" name="id_other" value="<?php echo $_GET['id_other'];?>
">
    <input type="hidden" name="special" value="<?php echo $_GET['special'];?>
">

    <div class="contect_0">
        <div class="conditions">
            <ul class="head">
                <li>
                    租售
                </li>
            </ul>

            <div class="b_div">
              <div class="b2_div">
                <label id="label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                    <?php
$_from = $_smarty_tpl->tpl_vars['house_choose']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_0_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_0_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                    <li name="house_choose_a[]" id="house_choose_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['value'];?>
" onclick="click_li(this);"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</li>
                    <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_local_item;
}
}
if ($__foreach_value_0_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_item;
}
if ($__foreach_value_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_0_saved_key;
}
?>
                </ul><div class="close_window">OK</div><div class="close_window_x">OK</div>
            </div></div>

            <?php $_smarty_tpl->tpl_vars['rck'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rck', 0);?>
            <!--<div data-role="fieldcontain">
                <fieldset data-role="controlgroup">
                <?php
$_from = $_smarty_tpl->tpl_vars['house_choose']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_1_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_1_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                        <input type="radio" name="house_choose_a[]" id="radio-choice-<?php echo $_smarty_tpl->tpl_vars['rck']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value == 0) {?>checked="checked"<?php }?> />
                        <label for="radio-choice-<?php echo $_smarty_tpl->tpl_vars['rck']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</label>
                        <?php $_smarty_tpl->tpl_vars['rck'] = new Smarty_Variable($_smarty_tpl->tpl_vars['rck']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rck', 0);?>
                <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_local_item;
}
}
if ($__foreach_value_1_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_item;
}
if ($__foreach_value_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_1_saved_key;
}
?>
                </fieldset>
            </div>-->
        </div>
    </div>

    <div class="contect_1">
        <div class="conditions">
            <ul class="head" onclick="get_county();">
                <li>區域</li>
            </ul>
            <div class="b_div">

                <ul class="buttons_c">
                    <li>
                        <a id="citys_txt" href="#" class="dropdown-toggle"></a>
                        <ul id="citys" class="d-menu" data-role="dropdown">
                        </ul>
                    </li>
                    <li>
                    </li>
                </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="contect_1_1" style="display:none">
        <div class="conditions">

            <ul class="head">
            </ul>
            <div class="select_box_bk"></div>
            <div class="select_box">
            <div class="b_div"></div><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
        </div>
    </div>

    <div class="contect_1_2" style="display:none">
        <div class="conditions">

            <ul class="head">
            </ul>
            <div class="select_box_bk"></div>
            <div class="select_box">
            <div class="b_div"></div><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
        </div>
    </div>

    <div class="contect_2">
        <div class="conditions">
            <ul class="head" onclick="check_label_types(this);">
                <li>
                    用途
                </li>
            </ul>
            <div class="b_div">
              <div class="b2_div">
                <label id="label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                    <?php
$_from = $_smarty_tpl->tpl_vars['id_main_house_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_2_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_2_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_2_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                    <li name="id_main_house_class_a[]" id="id_main_house_class_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id_main_house_class'];?>
" onclick="click_li(this);"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</li>
                    <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_local_item;
}
}
if ($__foreach_value_2_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_item;
}
if ($__foreach_value_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_2_saved_key;
}
?>
                </ul>
                <div class="close_window">OK</div><div class="close_window_x">OK</div>
            </div></div>


            <!--<div data-role="fieldcontain">
                <fieldset data-role="controlgroup">
                <?php
$_from = $_smarty_tpl->tpl_vars['id_main_house_class']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_3_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_3_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_3_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_3_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                        <input type="radio" name="id_main_house_class_a[]" id="radio-choice-<?php echo $_smarty_tpl->tpl_vars['rck']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id_main_house_class'];?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value == 0) {?>checked="checked"<?php }?> />
                        <label for="radio-choice-<?php echo $_smarty_tpl->tpl_vars['rck']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</label>
                        <?php $_smarty_tpl->tpl_vars['rck'] = new Smarty_Variable($_smarty_tpl->tpl_vars['rck']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rck', 0);?>
                <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_3_saved_local_item;
}
}
if ($__foreach_value_3_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_3_saved_item;
}
if ($__foreach_value_3_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_3_saved_key;
}
?>
                </fieldset>
            </div>-->
        </div>
    </div>

    <div class="contect_3">
        <div class="conditions">
            <ul class="head" onclick="check_label_types(this);">
                <li>
                    型態<br><!--<i class="type fas fa-caret-down"></i>-->
                </li>
            </ul>
            <div class="b_div b_div_id_types">
                <label id="types_label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                <?php
$_from = $_smarty_tpl->tpl_vars['select_types']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_4_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_4_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_4_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_4_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?> <?php if ($_smarty_tpl->tpl_vars['key']->value%4 == 0) {?> <?php if ($_smarty_tpl->tpl_vars['key']->value != 0) {?><!--</ul>--><?php }?>
                    <!--<ul class="buttons">--><?php }?>
                    <li name="id_types_a[]" id="id_types_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id_rent_house_types'];?>
" onclick="click_li(this);"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</li>
                    <?php if (count($_smarty_tpl->tpl_vars['select_types']->value) == $_smarty_tpl->tpl_vars['key']->value) {?><!--</ul>--><?php }?> <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_4_saved_local_item;
}
}
if ($__foreach_value_4_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_4_saved_item;
}
if ($__foreach_value_4_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_4_saved_key;
}
?>
                </ul><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
            </div>
        </div>
    </div>

    <div class="contect_4">
        <div class="conditions">
            <ul class="head" onclick="check_label_type(this);">
                <li>
                    類別<br><!--<i class="type fas fa-caret-down"></i>-->
                </li>
            </ul>
            <div class="b_div b_div_id_type">
                <label id="type_label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                <?php
$_from = $_smarty_tpl->tpl_vars['select_type']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_5_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_5_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_5_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_5_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?> <?php if ($_smarty_tpl->tpl_vars['key']->value%4 == 0) {?> <?php if ($_smarty_tpl->tpl_vars['key']->value != 0) {?><!--</ul>--><?php }?>
                    <!--<ul class="buttons">--><?php }?>
                    <li name="id_type_a[]" id="id_type_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id_rent_house_type'];?>
" onclick="click_li(this);"><?php echo str_replace(" ",'',$_smarty_tpl->tpl_vars['value']->value['title']);?>
</li>
                    <?php if (count($_smarty_tpl->tpl_vars['select_type']->value) == $_smarty_tpl->tpl_vars['key']->value) {?><!--</ul>--><?php }?> <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_5_saved_local_item;
}
}
if ($__foreach_value_5_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_5_saved_item;
}
if ($__foreach_value_5_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_5_saved_key;
}
?>
                </ul><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
            </div>
        </div>
    </div>

    <div class="contect_5">
        <div class="conditions">
            <ul class="head">
                <li>房數</li>
            </ul>
            <div class="b_div">
              <div class="b_div_editer"> <input type="text" id="room_min_editer"> <span>房 ~ </span> <input type="text" id="room_max_editer"> <span>房</span> </div>
                <input id="room" data-hint-position-min="top" data-hint-position-max="top" data-role="doubleslider" data-min="<?php echo $_smarty_tpl->tpl_vars['min_room']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['max_room']->value;?>
" data-hint-always="false" data-hint="true" data-value-max="1" class="ultra-thin cycle-marker" data-on-change-value="slider_get(this);slider_transfor_room(this)">
                <div class="Sroom_txt">
                    <div style="width: 5%;padding-right: 14px;">Open</div>
                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= 5) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= 5; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                    <div><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</div>
                    <?php }
}
?>

                    <div style="padding-right: 0px;width: 60px;">5+</div>
                </div>
            </div>
        </div>
    </div>

    <div class="contect_5_5">
        <div class="conditions">
            <ul class="head">
                <li>坪數</li>
            </ul>
            <div class="b_div">
              <div class="b_div_editer"> <input type="text" id="ping_min_editer"> <span>坪 ~ </span> <input type="text" id="ping_max_editer"> <span>坪</span> </div>
                <input id="ping" data-hint-position-min="top" data-hint-position-max="top" data-role="doubleslider" data-min="<?php echo $_smarty_tpl->tpl_vars['min_ping']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['max_ping']->value;?>
" data-hint-always="false" data-hint="true" data-value-max="1" class="ultra-thin cycle-marker" data-on-change-value="slider_get(this);slider_transfor_ping(this)">
                <div class="Sroom_txt">
                    <div style="width: 5%;padding-right: 14px;">10</div>
                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 2;
if ($_smarty_tpl->tpl_vars['i']->value < 6) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 6; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                    <div><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
0</div>
                    <?php }
}
?>

                    <!-- <div style="padding-right: 0px;width: 60px;">50+</div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="contect_6">
        <div class="conditions">
            <ul class="head">
                <li>預算</li>
            </ul>
            <div class="b_div">
                <div class="b_div_editer"> <input type="text" id="min_editer"> <span>元 ~ </span> <input type="text" id="max_editer"> <span>元</span> </div>
                <input id="price" data-role="doubleslider" data-accuracy="1000" data-min="<?php echo $_smarty_tpl->tpl_vars['min_price']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['max_price']->value;?>
" data-value-max="10000"
                    class="ultra-thin cycle-marker" data-on-change-value="slider_get(this);slider_transfor(this)" data-hint="true">
                <div class="Scale_txt">
                    <div style="width: 2em;">1萬</div>
                    <!-- <div>1萬</div> -->
                    <div>2萬</div>
                    <div>3萬</div>
                    <div>4萬</div>
                    <div>5萬</div>
                    <div style="width: 27px;margin-right: -10px;">+</div>
                </div>
                <div class="Scale">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="contect_7">
        <div class="conditions under">
            <ul class="head">
                <li>
                    條件<br><!--<i class="type fas fa-caret-down"></i>-->
                </li>
            </ul>
            <div class="b_div">
              <label style="display:none">請選擇</label>
              <div class="select_box_bk"></div>
              <div class="select_box"><ul class="buttons">
                <?php
$_from = $_smarty_tpl->tpl_vars['select_other_conditions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_6_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_6_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_6_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_6_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?> <?php if ($_smarty_tpl->tpl_vars['key']->value%3 == 0) {?> <?php if ($_smarty_tpl->tpl_vars['key']->value != 0) {?><!--</ul>--><?php }?>
                <!-- <ul class="buttons fixshow"> -->
                  <?php }?>
                    <li name="id_other_a[]" id="id_other_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id_other_conditions'];?>
" onclick="click_li(this);"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</li>
                    <?php if (count($_smarty_tpl->tpl_vars['select_other_conditions']->value) == $_smarty_tpl->tpl_vars['key']->value) {?><!--</ul>--><?php }?> <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_6_saved_local_item;
}
}
if ($__foreach_value_6_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_6_saved_item;
}
if ($__foreach_value_6_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_6_saved_key;
}
?>
                    </ul><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
            </div>
        </div>
    </div>



    <div class="contect_8" style="
"><button type="button" class="search_btn">　搜尋</button></div>


        <?php $_smarty_tpl->tpl_vars['tt_i'] = new Smarty_Variable(19, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'tt_i', 0);?>
        <?php $_smarty_tpl->tpl_vars['f_style'] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'f_style', 0);?>

        <style><?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['tt_i']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['tt_i']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
        <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2) == $_smarty_tpl->tpl_vars['i']->value) {?>
.morex svg:nth-child(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
) {
    font-size: 22pt;
}
        <?php }?>
        <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2)+1 == $_smarty_tpl->tpl_vars['i']->value || round($_smarty_tpl->tpl_vars['tt_i']->value/2)-1 == $_smarty_tpl->tpl_vars['i']->value) {?>
.morex svg:nth-child(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
) {
    font-size: 22pt;
}
        <?php }?>
        <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2)+2 == $_smarty_tpl->tpl_vars['i']->value || round($_smarty_tpl->tpl_vars['tt_i']->value/2)-2 == $_smarty_tpl->tpl_vars['i']->value) {?>
.morex svg:nth-child(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
) {
    font-size: 20pt;
}
        <?php }?>
        <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2)+2 == $_smarty_tpl->tpl_vars['i']->value || round($_smarty_tpl->tpl_vars['tt_i']->value/2)-2 == $_smarty_tpl->tpl_vars['i']->value) {?>
.morex svg:nth-child(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
) {
    font-size: 16pt;
    <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2)+2 == $_smarty_tpl->tpl_vars['i']->value) {?>
    margin-right: 20px;
    <?php }?>
    <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2)-2 == $_smarty_tpl->tpl_vars['i']->value) {?>
    margin-left: 20px;
    <?php }?>
}
        <?php }?>
        <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2)+6 < $_smarty_tpl->tpl_vars['i']->value || round($_smarty_tpl->tpl_vars['tt_i']->value/2)-6 > $_smarty_tpl->tpl_vars['i']->value) {?>
.morex svg:nth-child(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
) {
    font-size: 8pt;
}
        <?php }?>
        <?php }
}
?>

.morex svg {
    font-size: 8pt;
}
</style>


    <div class="more">
      <div class="content">
      <!--<i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i>-->
      <!--<span>更多篩選條件</span>-->
      <!--<i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i>-->
      </div>
      <div id="search_box">
          <div id="search_text">
              <input name="search" id="search" type="text" class="field" data-role="none" placeholder="請輸入關鍵字" size="40" value="<?php echo $_GET['search'];?>
">
          </div>
          <div id="search_btn" class="search_btn">
              <img src="/themes/App/mobile/img/icon/pic_m.svg" class="sunmit icon">
          </div>
      </div>
      <div>

        <?php $_smarty_tpl->tpl_vars['f_style'] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'f_style', 0);?>

        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['tt_i']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['tt_i']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
        <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2)+2 < $_smarty_tpl->tpl_vars['i']->value || round($_smarty_tpl->tpl_vars['tt_i']->value/2)-2 > $_smarty_tpl->tpl_vars['i']->value) {?>
        <!--<i class="fas fa-ellipsis-v fa-xs"></i>-->
        <?php } else { ?>
        <?php if (round($_smarty_tpl->tpl_vars['tt_i']->value/2) == $_smarty_tpl->tpl_vars['i']->value) {?>
        <!--<i class="fas fa-long-arrow-alt-down"></i>-->
        <?php } else { ?>
        <!--<i class="fas fa-long-arrow-alt-down"></i>--><?php }
}
}
}
?>

    </div></div>
</form>
<?php echo '<script'; ?>
>
    <?php echo $_smarty_tpl->tpl_vars['js']->value;?>

<?php echo '</script'; ?>
>
<?php }
}
