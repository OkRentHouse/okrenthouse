<?php
/* Smarty version 3.1.28, created on 2020-09-30 09:59:06
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/store/search_store.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f73e66ac10097_46264282',
  'file_dependency' => 
  array (
    '4c6df68119addd9cf41beb2908a8725792254af9' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/store/search_store.tpl',
      1 => 1601361206,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f73e66ac10097_46264282 ($_smarty_tpl) {
?>
<form action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" method="get" class="search_content store_search">
	<div class="search_wrap none_buoy">
		<select class="button county active" data-role="none" data-v="<?php echo $_GET['county'];?>
" name="county">
			<option value=""><?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
</option>
            <?php
$_from = $_smarty_tpl->tpl_vars['arr_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_arr_0_saved_item = isset($_smarty_tpl->tpl_vars['arr']) ? $_smarty_tpl->tpl_vars['arr'] : false;
$__foreach_arr_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable();
$__foreach_arr_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_arr_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['arr']->value) {
$__foreach_arr_0_saved_local_item = $_smarty_tpl->tpl_vars['arr'];
?>
                <?php if (isset($_GET['county'])) {?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
"<?php if ($_GET['county'] == $_smarty_tpl->tpl_vars['arr']->value['value']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['arr']->value['title'];?>
</option>
                <?php } else { ?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
"<?php if ($_smarty_tpl->tpl_vars['default_county']->value == $_smarty_tpl->tpl_vars['arr']->value['value']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['arr']->value['title'];?>
</option>
                <?php }?>
            <?php
$_smarty_tpl->tpl_vars['arr'] = $__foreach_arr_0_saved_local_item;
}
}
if ($__foreach_arr_0_saved_item) {
$_smarty_tpl->tpl_vars['arr'] = $__foreach_arr_0_saved_item;
}
if ($__foreach_arr_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_arr_0_saved_key;
}
?>
		</select><select class="button area" data-role="none" data-v="<?php echo $_GET['parent'];?>
" name="area">
			<option value=""><?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
</option>
            <?php
$_from = $_smarty_tpl->tpl_vars['arr_area']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_arr_1_saved_item = isset($_smarty_tpl->tpl_vars['arr']) ? $_smarty_tpl->tpl_vars['arr'] : false;
$__foreach_arr_1_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable();
$__foreach_arr_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_arr_1_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['arr']->value) {
$__foreach_arr_1_saved_local_item = $_smarty_tpl->tpl_vars['arr'];
?>
                <?php if (isset($_GET['area'])) {?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
" data-parent="<?php echo $_smarty_tpl->tpl_vars['arr']->value['parent'];?>
" data-parent_name="<?php echo $_smarty_tpl->tpl_vars['arr']->value['parent_name'];?>
" style="display: none;"<?php if ($_GET['area'] == $_smarty_tpl->tpl_vars['arr']->value['value']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['arr']->value['title'];?>
</option>
                <?php } else { ?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
" data-parent="<?php echo $_smarty_tpl->tpl_vars['arr']->value['parent'];?>
" data-parent_name="<?php echo $_smarty_tpl->tpl_vars['arr']->value['parent_name'];?>
" style="display: none;"<?php if ($_smarty_tpl->tpl_vars['default_area']->value == $_smarty_tpl->tpl_vars['arr']->value['value']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['arr']->value['title'];?>
</option>
                <?php }?>
            <?php
$_smarty_tpl->tpl_vars['arr'] = $__foreach_arr_1_saved_local_item;
}
}
if ($__foreach_arr_1_saved_item) {
$_smarty_tpl->tpl_vars['arr'] = $__foreach_arr_1_saved_item;
}
if ($__foreach_arr_1_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_arr_1_saved_key;
}
?>
		</select><select class="button career" data-role="none" data-v="<?php echo $_GET['career'];?>
" name="career">
			<option value=""><?php echo l(array('s'=>"全部"),$_smarty_tpl);?>
</option>
            <?php
$_from = $_smarty_tpl->tpl_vars['arr_career']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_arr_2_saved_item = isset($_smarty_tpl->tpl_vars['arr']) ? $_smarty_tpl->tpl_vars['arr'] : false;
$__foreach_arr_2_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable();
$__foreach_arr_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_arr_2_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['arr']->value) {
$__foreach_arr_2_saved_local_item = $_smarty_tpl->tpl_vars['arr'];
?>
                <?php if (isset($_GET['career'])) {?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
"<?php if ($_GET['career'] == $_smarty_tpl->tpl_vars['arr']->value['value']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['arr']->value['title'];?>
</option>
                <?php } else { ?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
"<?php if ($_smarty_tpl->tpl_vars['default_career']->value == $_smarty_tpl->tpl_vars['arr']->value['value']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['arr']->value['title'];?>
</option>
                <?php }?>
            <?php
$_smarty_tpl->tpl_vars['arr'] = $__foreach_arr_2_saved_local_item;
}
}
if ($__foreach_arr_2_saved_item) {
$_smarty_tpl->tpl_vars['arr'] = $__foreach_arr_2_saved_item;
}
if ($__foreach_arr_2_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_arr_2_saved_key;
}
?>
		</select>
		<buoy></buoy>
	</div>
	<div id="search_box">
		<div id="search_text">
			<input name="title" id="title" type="text" class="field" value="<?php echo $_GET['title'];?>
" data-role="none" placeholder="請輸入關鍵字(店家名稱等)"
				   size="40"/>
		</div>
		<div class="search_btn">
			<button type="button" class="button pic_btn" data-role="none"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/pic_m.svg"></button>
		</div>
		<div class="">
			<button type="button" class="button map_btn" data-role="none"><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/map_m.svg"></button>
		</div>
	</div>
</form><?php }
}
