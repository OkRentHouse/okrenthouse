<?php
/* Smarty version 3.1.28, created on 2020-12-14 12:47:51
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/Coupon/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd6ee77c57515_96524465',
  'file_dependency' => 
  array (
    'ab2c71d60953db581b6b306df57f6b67e235e91e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/Coupon/content.tpl',
      1 => 1607678620,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd6ee77c57515_96524465 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/coupon/min_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="document">
    <?php if (count($_smarty_tpl->tpl_vars['arr_content_banner']->value)) {?>
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true;ratio: <?php echo $_smarty_tpl->tpl_vars['content_ratio']->value;?>
">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_content_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_0_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_0_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a></li>
                    <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_local_item;
}
}
if ($__foreach_banner_0_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_item;
}
if ($__foreach_banner_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_0_saved_key;
}
?>
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
    <?php }?>
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">特約商店</div>
	</div>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/store/search_store.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

	<div class="item_wrap_box store_box">
		<div class="banner">
			<a class="ui-link" href="/SearchStore"><?php if (!empty($_smarty_tpl->tpl_vars['coupon_img']->value)) {?><img class="" src="<?php echo $_smarty_tpl->tpl_vars['coupon_img']->value;?>
"><?php }?></a>
		</div>
		<a class="ui-link new_store_wrap" href="/SearchStore/?order=news"></a>
	</div>
    
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">好康會員</div>

	</div>
	<div class="want">
		<a href="/WantCoupon"><?php if (!empty($_smarty_tpl->tpl_vars['want_img']->value)) {?><img src="<?php echo $_smarty_tpl->tpl_vars['want_img']->value;?>
"><?php }?></a>
        <?php if (count($_smarty_tpl->tpl_vars['arr_want']->value)) {?>
			<div class="text">
                <?php
$_from = $_smarty_tpl->tpl_vars['arr_want']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_want_1_saved_item = isset($_smarty_tpl->tpl_vars['want']) ? $_smarty_tpl->tpl_vars['want'] : false;
$__foreach_want_1_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['want'] = new Smarty_Variable();
$__foreach_want_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_want_1_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['want']->value) {
$__foreach_want_1_saved_local_item = $_smarty_tpl->tpl_vars['want'];
?>
					<div>
						<a href="<?php echo $_smarty_tpl->tpl_vars['want']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['want']->value['text'];?>
</a>
					</div>
                <?php
$_smarty_tpl->tpl_vars['want'] = $__foreach_want_1_saved_local_item;
}
}
if ($__foreach_want_1_saved_item) {
$_smarty_tpl->tpl_vars['want'] = $__foreach_want_1_saved_item;
}
if ($__foreach_want_1_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_want_1_saved_key;
}
?>
			</div>
        <?php }?>
	</div>
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">好康店家</div>

	</div>
	<div class="give">
		<a href="/GiveCoupon"><?php if (!empty($_smarty_tpl->tpl_vars['give_img']->value)) {?><img src="<?php echo $_smarty_tpl->tpl_vars['give_img']->value;?>
"><?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['give_text']->value)) {?>
				<div class="text text-center">
					<?php echo $_smarty_tpl->tpl_vars['give_text']->value;?>

				</div>
            <?php }?>
		</a>
	</div>
</div><?php }
}
