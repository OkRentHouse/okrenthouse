<?php
/* Smarty version 3.1.28, created on 2020-12-11 17:40:19
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd33e835934b9_43360835',
  'file_dependency' => 
  array (
    'a43add978a7c98c005697551a5146fe4d4b23a12' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/msg_box.tpl',
      1 => 1607678489,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd33e835934b9_43360835 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'success',
			html: '<?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'error',
			html: '<?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
}
}
