<?php
/* Smarty version 3.1.28, created on 2020-11-18 14:41:59
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/List/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fb4c237eb0e28_17787179',
  'file_dependency' => 
  array (
    '50fecf113e09a0800e6aa4e8f48fd842fb210b33' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/List/content.tpl',
      1 => 1605681327,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:switch.tpl' => 1,
  ),
),false)) {
function content_5fb4c237eb0e28_17787179 ($_smarty_tpl) {
if ($_GET['switch'] != 1) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/menu_buoy.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
if ($_GET['active'] == 1) {?>
<div class="breadcrumb"><?php echo $_smarty_tpl->tpl_vars['breadcrumb']->value;?>
</div>
<div class="document" style="padding: 0 15px;">
    <?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_house_0_saved_item = isset($_smarty_tpl->tpl_vars['house']) ? $_smarty_tpl->tpl_vars['house'] : false;
$__foreach_house_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['house'] = new Smarty_Variable();
$__foreach_house_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_house_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['house']->value) {
$__foreach_house_0_saved_local_item = $_smarty_tpl->tpl_vars['house'];
?>
    <div class="commodity slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1"
        style="width: 100%;margin: 0px 0px 5px 0px;overflow: hidden;height: 200px;border-radius: 5px;">
        <div class="photo_row" style="border-radius: 0;">
            <a href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['house']->value['id_rent_house'];?>
" class="ui-link" target="_top">
                <img src="<?php if ($_smarty_tpl->tpl_vars['house']->value['img']) {
echo $_smarty_tpl->tpl_vars['house']->value['img'];
} else { ?>https://www.lifegroup.house/uploads/comm_soon.jpg<?php }?>"
                    class="img" alt="" />
                <collect data-id="<?php echo $_smarty_tpl->tpl_vars['house']->value['id_rent_house'];?>
" data-type="rent_house"
                    class="favorite <?php if ($_smarty_tpl->tpl_vars['house']->value['favorite']) {?>on<?php }?>">
                    <?php if ($_smarty_tpl->tpl_vars['house']->value['favorite']) {?>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg">
                    <?php }?>
                </collect>
            </a>
            <div>
                <data class="rows_content">
                    <div style="position: absolute; padding-left: 5px;">
                        <h3 class="title big_2" style="text-align: left;"><a
                                href="/RentHouse?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['house']->value['id_rent_house'];?>
" tabindex="-1"
                                class="ui-link"><?php echo $_smarty_tpl->tpl_vars['house']->value['case_name'];?>
</a></h3>
                        <div class="title_2" style="display: flex;"><a
                                href="https://www.google.com.tw/maps/place/<?php echo $_smarty_tpl->tpl_vars['house']->value['rent_address'];?>
" target="_blank">
                                <!--<img
                                    src="/themes/Rent/img/icon/icons8-address-50.png" />--><i
                                    class="fas fa-map-marked-alt"></i>&nbsp<?php echo $_smarty_tpl->tpl_vars['house']->value['rent_address'];?>
</a>
                        </div>
                        <div class="title_4">
                            <h3 class="title big"><a href="RentHouse.php?id_rent_house=15543" tabindex="-1"
                                    class="ui-link"><?php echo $_smarty_tpl->tpl_vars['house']->value['rent_house_title'];?>
</a></h3>
                        </div>
                    </div>
                    <div style="position: absolute; padding-right: 5px; width: 50%; text-align: end; right: 0px;">
                        <ul style="display: inline-flex; list-style-type: none;">
                            <li style="margin-right: 30px; margin-left: -30px;"><?php if ($_smarty_tpl->tpl_vars['house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['house']->value['t3'] == '0') {?>0房<?php } else {
echo $_smarty_tpl->tpl_vars['house']->value['t3'];?>
房<?php }
if ($_smarty_tpl->tpl_vars['house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['house']->value['t4'] == '0') {?>0廳<?php } else {
echo $_smarty_tpl->tpl_vars['house']->value['t4'];?>
廳<?php }
if ($_smarty_tpl->tpl_vars['house']->value['t5'] == '' || $_smarty_tpl->tpl_vars['house']->value['t5'] == '0') {?>0衛<?php } else {
echo $_smarty_tpl->tpl_vars['house']->value['t5'];?>
衛<?php }?></li>
                            <li><?php echo $_smarty_tpl->tpl_vars['house']->value['ping'];?>
坪</li>
                        </ul>
                        <price><?php echo $_smarty_tpl->tpl_vars['house']->value['rent_cash'];?>

                            <unit>元</unit>
                        </price>
                    </div>
                </data>
            </div>
        </div>
    </div>
    <?php
$_smarty_tpl->tpl_vars['house'] = $__foreach_house_0_saved_local_item;
}
}
if ($__foreach_house_0_saved_item) {
$_smarty_tpl->tpl_vars['house'] = $__foreach_house_0_saved_item;
}
if ($__foreach_house_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_house_0_saved_key;
}
?>
</div>
<?php } elseif ($_GET['switch'] == 1) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:switch.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } else {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/house/search_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
}
