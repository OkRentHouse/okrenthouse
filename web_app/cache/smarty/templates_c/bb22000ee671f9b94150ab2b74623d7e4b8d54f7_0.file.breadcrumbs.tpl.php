<?php
/* Smarty version 3.1.28, created on 2020-12-11 17:40:19
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/breadcrumbs.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd33e8355dbf8_15644991',
  'file_dependency' => 
  array (
    'bb22000ee671f9b94150ab2b74623d7e4b8d54f7' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/breadcrumbs.tpl',
      1 => 1607678489,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd33e8355dbf8_15644991 ($_smarty_tpl) {
?>



<ol class="breadcrumb">
    <?php
$_from = $_smarty_tpl->tpl_vars['breadcrumbs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_breadcrumbs_val_0_saved_item = isset($_smarty_tpl->tpl_vars['breadcrumbs_val']) ? $_smarty_tpl->tpl_vars['breadcrumbs_val'] : false;
$__foreach_breadcrumbs_val_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['breadcrumbs_val'] = new Smarty_Variable();
$__foreach_breadcrumbs_val_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_breadcrumbs_val_0_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['breadcrumbs_val']->value) {
$__foreach_breadcrumbs_val_0_saved_local_item = $_smarty_tpl->tpl_vars['breadcrumbs_val'];
?><li class="breadcrumb-item<?php if (count($_smarty_tpl->tpl_vars['breadcrumbs']->value) == $_smarty_tpl->tpl_vars['i']->value+1) {?> active<?php }?>"><a<?php if (!empty($_smarty_tpl->tpl_vars['breadcrumbs_val']->value['href'])) {?> href="<?php echo $_smarty_tpl->tpl_vars['breadcrumbs_val']->value['href'];?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['breadcrumbs_val']->value['title'];?>
</a></li><?php
$_smarty_tpl->tpl_vars['breadcrumbs_val'] = $__foreach_breadcrumbs_val_0_saved_local_item;
}
}
if ($__foreach_breadcrumbs_val_0_saved_item) {
$_smarty_tpl->tpl_vars['breadcrumbs_val'] = $__foreach_breadcrumbs_val_0_saved_item;
}
if ($__foreach_breadcrumbs_val_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_breadcrumbs_val_0_saved_key;
}
?>
</ol><?php }
}
