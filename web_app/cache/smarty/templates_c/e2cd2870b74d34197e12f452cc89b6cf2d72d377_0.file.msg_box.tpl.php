<?php
/* Smarty version 3.1.28, created on 2020-12-12 15:57:29
  from "/opt/lampp/htdocs/life-house.com.tw/themes/AppOkrent/mobile/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd477e94854b0_50148298',
  'file_dependency' => 
  array (
    'e2cd2870b74d34197e12f452cc89b6cf2d72d377' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/AppOkrent/mobile/msg_box.tpl',
      1 => 1607678490,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd477e94854b0_50148298 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'success',
			html: '<?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'error',
			html: '<?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
}
}
