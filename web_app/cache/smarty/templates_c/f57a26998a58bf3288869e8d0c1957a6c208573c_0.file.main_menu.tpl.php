<?php
/* Smarty version 3.1.28, created on 2020-10-23 13:45:24
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/main_menu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f926df4e4dab5_17079883',
  'file_dependency' => 
  array (
    'f57a26998a58bf3288869e8d0c1957a6c208573c' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/main_menu.tpl',
      1 => 1603431901,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f926df4e4dab5_17079883 ($_smarty_tpl) {
?>
<div class="menu_shadow"></div>

<div class="side_menu">
	<div class="close_btn glyphicon glyphicon-remove"></div>
	<div class="title">
		<div class="logo text-center">
			<a href="/" data-role="none"><img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/logo.svg"></a>
		</div>
	</div>
	<div class="main_menu">
		<div class="nav_body">
			<div class="login_div text-center">
				<a class="login ui-link" href="login">會員登入</a>
			</div>
			<ul class="">
				<li class="list">
					<a class="ui-link underline" href="#">會員中心</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/star-03.svg"><a class="ui-link" href="#">我的</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/flow-02.svg"><a class="ui-link" href="#">好康VIP</a>
				</li>
				<li class="list">
					<img class="" ><a class="ui-link underline" href="#">租事順利</a>
				</li>
<li class="list">
					<img class="" ><a class="ui-link underline" href="#">房地專區</a>
				</li>
				<li class="list">
					<i class="fas fa-search"></i><a class="ui-link" href="/list">吉屋快搜</a>
				</li>
				<li class="list">
					<i class="far fa-thumbs-up"></i><a class="ui-link" href="/list?switch=1" >推薦精選</a>
				</li>
				<li class="list">
					<i class="fas fa-ad"></i><a class="ui-link" href="#">刊登廣告</a>
				</li>


				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/house.svg"><a class="ui-link" href="#">租客專區</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/Inquire.svg"><a class="ui-link" href="#">房東專區</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/mail-04.svg"><a class="ui-link" href="#">樂租信用認證</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_d.svg"><a class="ui-link" href="#">安心租履約</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/credit.svg"><a class="ui-link" href="#">樂租信用認證</a>
				</li>
				<li class="list">
					<img class="" ><a class="ui-link underline" href="/Inlife">好康分享</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/gift.svg"><a class="ui-link" href="/coupon">生活好康</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/share1.svg"><a class="ui-link" href="/Inlife">活動分享</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/share2.svg"><a class="ui-link" href="/WealthSharing">創富分享</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/fix.svg"><a class="ui-link" href="#">裝修達人</a>
				</li>
				<li class="list">
					<img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/work.svg"><a class="ui-link" href="#">好幫手</a>
				</li>
				</li>
				<li class="list">
					<img class="" ><a class="ui-link underline" href="#">客服中心</a>
				</li>
			</ul>

			<div class="close">
				<div class="close_btn glyphicon glyphicon-remove"></div>
			</div>
		</div>
	</div>
</div><?php }
}
