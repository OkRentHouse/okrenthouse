<?php
/* Smarty version 3.1.28, created on 2020-12-12 15:57:29
  from "/opt/lampp/htdocs/life-house.com.tw/themes/AppOkrent/mobile/layout.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd477e9475759_25626643',
  'file_dependency' => 
  array (
    '023a5ce36929fbb66b0bb1072d3e20c35ac0886c' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/AppOkrent/mobile/layout.tpl',
      1 => 1607678490,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd477e9475759_25626643 ($_smarty_tpl) {
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php if (!empty($_smarty_tpl->tpl_vars['GOOGLE_SITE_VERIFICATION']->value)) {?><meta name="google-site-verification" content="<?php echo $_smarty_tpl->tpl_vars['GOOGLE_SITE_VERIFICATION']->value;?>
" /><?php }?>
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	<?php if ($_smarty_tpl->tpl_vars['meta_description']->value != '') {?><meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
"><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['meta_keywords']->value != '') {?><meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['meta_keywords']->value;?>
"><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['meta_img']->value != '') {?><meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['meta_img']->value[0];?>
"><?php }?>
	<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
	<?php if (!empty($_smarty_tpl->tpl_vars['t_color']->value)) {?><meta name="theme-color" content="<?php echo $_smarty_tpl->tpl_vars['t_color']->value;?>
"><?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['mn_color']->value)) {?><meta name="msapplication-navbutton-color" content="<?php echo $_smarty_tpl->tpl_vars['mn_color']->value;?>
"><?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['amwasb_style']->value)) {?><meta name="apple-mobile-web-app-status-bar-style" content="<?php echo $_smarty_tpl->tpl_vars['amwasb_style']->value;?>
"><?php }?>
	<?php
$_from = $_smarty_tpl->tpl_vars['css_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_0_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_0_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css"/>
	<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_local_item;
}
}
if ($__foreach_css_uri_0_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_0_saved_item;
}
if ($__foreach_css_uri_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_0_saved_key;
}
?>
	<?php if (!isset($_smarty_tpl->tpl_vars['display_header_javascript']->value) || $_smarty_tpl->tpl_vars['display_header_javascript']->value) {?>
		<?php echo '<script'; ?>
 type="text/javascript">
			var THEME_URL = '<?php echo @constant('THEME_URL');?>
';
		<?php echo '</script'; ?>
>
	<?php
$_from = $_smarty_tpl->tpl_vars['js_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_1_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_1_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>
		<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>
	<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_local_item;
}
}
if ($__foreach_js_uri_1_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_1_saved_item;
}
if ($__foreach_js_uri_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_1_saved_key;
}
?>
	<?php }?>
</head>
<body>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/main_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div data-role="<?php echo $_smarty_tpl->tpl_vars['role']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
" class="<?php echo $_smarty_tpl->tpl_vars['role']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['fields']->value['page']['class'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['fields']->value['page']['cache'])) {?> data-dom-cache="<?php if ($_smarty_tpl->tpl_vars['fields']->value['page']['cache']) {?>true<?php } else { ?>false<?php }?>"<?php }?>>
	<?php
$_from = $_smarty_tpl->tpl_vars['css_header_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_2_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_2_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_2_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css"/>
	<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_2_saved_local_item;
}
}
if ($__foreach_css_uri_2_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_2_saved_item;
}
if ($__foreach_css_uri_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_2_saved_key;
}
?>
	<?php
$_from = $_smarty_tpl->tpl_vars['js_header_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_3_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_3_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_3_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_3_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>
	<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_3_saved_local_item;
}
}
if ($__foreach_js_uri_3_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_3_saved_item;
}
if ($__foreach_js_uri_3_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_3_saved_key;
}
?>
	<?php if ($_smarty_tpl->tpl_vars['display_header']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}?>
	<div data-role="content" id="<?php echo $_smarty_tpl->tpl_vars['mamber_url']->value;?>
">
		<?php if (!empty($_smarty_tpl->tpl_vars['content']->value)) {
echo $_smarty_tpl->tpl_vars['content']->value;
}?>
		<?php if (!empty($_smarty_tpl->tpl_vars['template']->value)) {
echo $_smarty_tpl->tpl_vars['template']->value;
}?>
		<?php if ($_smarty_tpl->tpl_vars['role']->value == 'dialog') {?>
		<a href="#" class="btn btn-default ui-btn ui-shadow ui-corner-all" data-rel="back"><?php echo l(array('s'=>"關閉"),$_smarty_tpl);?>
</a>
		<?php }?>
	</div>
	<?php
$_from = $_smarty_tpl->tpl_vars['css_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_css_uri_4_saved_item = isset($_smarty_tpl->tpl_vars['css_uri']) ? $_smarty_tpl->tpl_vars['css_uri'] : false;
$__foreach_css_uri_4_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable();
$__foreach_css_uri_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_css_uri_4_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['css_uri']->value) {
$__foreach_css_uri_4_saved_local_item = $_smarty_tpl->tpl_vars['css_uri'];
?>
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['css_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="stylesheet" type="text/css"/>
	<?php
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_4_saved_local_item;
}
}
if ($__foreach_css_uri_4_saved_item) {
$_smarty_tpl->tpl_vars['css_uri'] = $__foreach_css_uri_4_saved_item;
}
if ($__foreach_css_uri_4_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_css_uri_4_saved_key;
}
?>
	<?php
$_from = $_smarty_tpl->tpl_vars['js_footer_files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_js_uri_5_saved_item = isset($_smarty_tpl->tpl_vars['js_uri']) ? $_smarty_tpl->tpl_vars['js_uri'] : false;
$__foreach_js_uri_5_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable();
$__foreach_js_uri_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_js_uri_5_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['js_uri']->value) {
$__foreach_js_uri_5_saved_local_item = $_smarty_tpl->tpl_vars['js_uri'];
?>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"><?php echo '</script'; ?>
>
	<?php
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_5_saved_local_item;
}
}
if ($__foreach_js_uri_5_saved_item) {
$_smarty_tpl->tpl_vars['js_uri'] = $__foreach_js_uri_5_saved_item;
}
if ($__foreach_js_uri_5_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_js_uri_5_saved_key;
}
?>
	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/msg_box.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

	<?php if ($_smarty_tpl->tpl_vars['display_main_menu']->value) {?>
	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/display_main_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

	<?php }?>
</div>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/house/flash_search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php if ($_smarty_tpl->tpl_vars['display_footer']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}?>
</body>
</html><?php }
}
