<?php
/* Smarty version 3.1.28, created on 2020-12-11 17:40:19
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/house/search_house.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd33e835732f8_35746683',
  'file_dependency' => 
  array (
    '6e736c1e1a7131338ee5af3a90166773e3570b70' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/house/search_house.tpl',
      1 => 1607678514,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd33e835732f8_35746683 ($_smarty_tpl) {
?>

<form id="search_form" name="search_form" method="get" action="/list">
	<input type="hidden" name="active" value="1">
	<input type="hidden" name="house_choose" value="<?php echo $_GET['house_choose'];?>
">
	<input type="hidden" name="id_main_house_class" value="<?php echo $_GET['id_main_house_class'];?>
">
	<input type="hidden" name="id_types" value="<?php echo $_GET['id_types'];?>
">
	<input type="hidden" name="id_type" value="<?php echo $_GET['id_type'];?>
">
	<input type="hidden" name="county" value="<?php echo $_GET['county'];?>
">
	<input type="hidden" name="city" value="<?php echo $_GET['city'];?>
">
	<div style="display: flex">
		<div class="search_select">
			<span><a href="#" data-transition="slide" class="ui-link ui-link-house_choose-txt">租屋</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-county-txt">區域</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-main_house_class-txt">用途</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-types-txt">型態</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-type-txt">類別</a></span>
		</div>

		<div id="search_box">
			<div id="search_btn" class="search_btn">
				<img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/pic_m.svg" class="sunmit icon">
			</div>
		</div>
	</div>
</form>
<?php echo $_smarty_tpl->tpl_vars['js']->value;
}
}
