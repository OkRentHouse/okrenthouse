<?php
/* Smarty version 3.1.28, created on 2020-10-28 14:06:21
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/Register/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f990a5dcd03e1_38054224',
  'file_dependency' => 
  array (
    '37567842c2beb578df92492e5cf2e8ddbac27253' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/Register/content.tpl',
      1 => 1603865177,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f990a5dcd03e1_38054224 ($_smarty_tpl) {
?>
<div class="home_sign_in">
    <div class="form_wrap">
        <form action="/register" method="post" enctype="application/x-www-form-urlencoded">
          <div class="floating">
              <input id="name" name="name" data-role="none" class="floating__input" type="text"
                     placeholder="姓名" value="<?php echo $_POST['name'];?>
"><label></label>
                     <img src="/themes/App/mobile/img/register/member.png">
              <!-- <label for="password" class="floating__label" data-content="密碼"></label> -->
          </div>
          <div class="floating sexSel">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="gender" id="gender1" value="1">
              <label class="form-check-label" for="gender1"></label>
              <img src="/themes/App/mobile/img/register/B.png">
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="gender" id="gender0" value="0">
              <label class="form-check-label" for="gender0"></label>
              <img src="/themes/App/mobile/img/register/G.png">
            </div>
          </div>
          <div class="floating" style="opacity: 0;margin: 0px">
            ***
          </div>


          <div class="floating">
              <input id="nickname" name="nickname" data-role="none" class="floating__input" type="text"
                     placeholder="暱稱" value="<?php echo $_POST['nickname'];?>
"><label></label>
                     <img src="/themes/App/mobile/img/register/k.png">
              <!-- <label for="password" class="floating__label" data-content="密碼"></label> -->
          </div>
          <div class="floating">
              <input id="user" name="user" data-role="none" class="floating__input" type="text"
                     placeholder="手機號碼" value="<?php echo $_POST['user'];?>
"><label></label>
                     <img src="/themes/App/mobile/img/register/id.png">
              <!-- <label for="password" class="floating__label" data-content="密碼"></label> -->
          </div>
          <div class="floating code">
            <div class="L">
              <button class="post_code" type="button" id="tel_submit" >取得驗證碼</button>
              </div>
              <div class="R">

              <input id="captcha" name="captcha" data-role="none" class="floating__input" type="text"
                     placeholder="請輸入驗證碼" value="<?php echo $_POST['captcha'];?>
">
              </div>

          </div>
            <div class="floating">
                <input id="password" name="password" data-role="none" class="floating__input"  type="password"
                       placeholder="密碼" value="<?php echo $_POST['password'];?>
"><label></label>
                       <img src="/themes/App/mobile/img/register/psw.png">
                <!-- <label for="user" class="floating__label" data-content="手機號碼"></label> -->
            </div>
            <div class="floating">
                <input id="check_password" name="check_password" data-role="none" class="floating__input" type="password"
                       placeholder="確認密碼" value="<?php echo $_POST['check_password'];?>
"><label></label>
                       <img src="/themes/App/mobile/img/register/psw.png">
                <!-- <label for="password" class="floating__label" data-content="密碼"></label> -->
            </div>

            <div class="floating consent">


              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="agree" name="agree">
                <div class="context">我已閱讀並同意生活樂租的【服務條款】<br> 與【隱私權聲明】及會員同意條款</div>
                <label class="form-check-label" for="agree">

                </label>
              </div>



            </div>

            <div class="content">

                <button class="btn_sign_in" type="submit"  id="submitAdd<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
" name="submitAdd<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
" value="1">註 冊</button>
                <button class="btn_cancel" type="button"  onclick="get_to_index();">取 消</button>
            </div>
        </form>

    </div>
</div>
<?php }
}
