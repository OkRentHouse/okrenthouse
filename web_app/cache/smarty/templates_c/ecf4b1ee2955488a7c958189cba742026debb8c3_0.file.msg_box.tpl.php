<?php
/* Smarty version 3.1.28, created on 2020-09-29 14:33:38
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f72d542bc0c01_73889394',
  'file_dependency' => 
  array (
    'ecf4b1ee2955488a7c958189cba742026debb8c3' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/msg_box.tpl',
      1 => 1601361206,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f72d542bc0c01_73889394 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'success',
			html: '<?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'error',
			html: '<?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
}
}
