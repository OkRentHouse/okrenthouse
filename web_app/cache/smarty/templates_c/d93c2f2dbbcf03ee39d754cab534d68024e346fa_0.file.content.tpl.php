<?php
/* Smarty version 3.1.28, created on 2020-09-29 18:54:06
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/RentHouse/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f73124ecafb62_96061515',
  'file_dependency' => 
  array (
    'd93c2f2dbbcf03ee39d754cab534d68024e346fa' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/RentHouse/content.tpl',
      1 => 1601361224,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f73124ecafb62_96061515 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/menu_buoy.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/breadcrumbs.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="data_wrap">
    <div class="result"></div>
    <div class="commodity slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1" >
        <div class="photo_row">
            <a href="#" class="ui-link">
                <img src="<?php if ($_smarty_tpl->tpl_vars['img']->value[0]) {
echo $_smarty_tpl->tpl_vars['img']->value[0];
} else { ?>https://www.lifegroup.house/uploads/comm_soon.jpg<?php }?>" class="img" alt="" />
                <collect data-id="<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
" data-type="rent_house" class="favorite<?php if ($_smarty_tpl->tpl_vars['arr_house']->value['favorite']) {?> on<?php }?>"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['favorite']) {?>
                        <img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg"><?php }?>
                </collect>
            </a>

            <div>
                <data class="rows_content">
                    <div class="rows_content_1">
                        <h3 class="title big_2"><a href="RentHouse.php?id_rent_house=<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['id_rent_house'];?>
" tabindex="-1" class="ui-link"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_title'];?>
</a></h3>
                        <div class="title_2"><img src="https://www.okrent.house/themes/Rent/img/icon/icons8-address-50.png" /><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_address'];?>
</div>
                        <div class="title_4">
                            <ul>
                                <?php if (($_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '0') && ($_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '0')) {?><li>開放空間</li><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t3'] == '0') {
} else { ?><li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t3'];?>
房</li><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t4'] == '0') {
} else { ?><li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t4'];?>
廳</li><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t5'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t5'] == '0') {
} else { ?><li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t5'];?>
衛</li><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t6'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t6'] == '0') {
} else { ?><li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t6'];?>
室</li><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t7'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t7'] == '0') {
} else { ?><li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t7'];?>
前陽台</li><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t8'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t8'] == '0') {
} else { ?><li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t8'];?>
後陽台</li><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['t9'] == '' || $_smarty_tpl->tpl_vars['arr_house']->value['t9'] == '0') {
} else { ?><li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['t9'];?>
房間陽台</li><?php }?>
                                <li><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['ping'];?>
坪</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rows_content_2">
                        <h3 class="title big"><a href="RentHouse.php?id_rent_house=15543" tabindex="-1" class="ui-link"><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['case_name'];?>
</a></h3>

                        <price style="position: absolute; top: 44px; right: 0px; padding-right: 5px;">
                            10000
                            <unit>元</unit>
                        </price>
                    </div>
                </data>
            </div>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F">
            <ul class="Fr">
                <li>租金含</li>
            </ul>
            <ul class="La">
                <li>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_type'] == 1) {?><span>管理費</span><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['park_fee_type'] == 1) {?><span>停車費</span><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['pay_tv'] == 2) {?><span>有線電視</span><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['water_fee_type'] == 1) {?><span>水費</span><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['electricity_fee_type'] == 1) {?><span>電費</span><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['gas_fee_type'] == 1) {?><span>瓦斯</span><?php }?>
                </li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>房屋資訊</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <li>案號</li>
                <li>樓層</li>
                <li>汽車位</li>
                <li>座向</li>
                <li>坪數</li>
                <li> &nbsp; </li>
　
            </ul>
            <ul class="La">
                <li><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rent_house_code'];?>
</span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['whole_building'] == '1') {?>整棟建築<?php } else { ?>樓層 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['rental_floor'];?>
 F/ <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['floor'];?>
 F<?php }?></span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle_have'] == 1) {?>有<?php } else { ?>無<?php }?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_park_position'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                            <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent'][$_smarty_tpl->tpl_vars['k']->value] != '') {
if ($_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][$_smarty_tpl->tpl_vars['k']->value] == '含管理費') {?>(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][$_smarty_tpl->tpl_vars['k']->value];?>
)<?php } else { ?>(每<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent_type'][$_smarty_tpl->tpl_vars['k']->value];
echo $_smarty_tpl->tpl_vars['arr_house']->value['cars']['cars_rent'][$_smarty_tpl->tpl_vars['k']->value];?>
元)<?php }
}?>
                        <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?></span></li>
                <li><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['house_door_seat'];?>
</span></li>
                <li><span>權狀 <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['ping']) && $_smarty_tpl->tpl_vars['arr_house']->value['ping'] != '0.00') {
echo $_smarty_tpl->tpl_vars['arr_house']->value['ping'];?>
坪<?php } else { ?>暫不提供<?php }?></span></li>
                <li class="pos"><span>主建 <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['main_construction_ping']) && $_smarty_tpl->tpl_vars['arr_house']->value['main_construction_ping'] != '0.00') {
echo $_smarty_tpl->tpl_vars['arr_house']->value['main_construction_ping'];?>
坪<?php } else { ?>暫不提供<?php }?></span>
                    <span>附屬 <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['accessory_ping']) && $_smarty_tpl->tpl_vars['arr_house']->value['accessory_ping'] != '0.00') {
echo $_smarty_tpl->tpl_vars['arr_house']->value['accessory_ping'];?>
坪<?php } else { ?>暫不提供<?php }?></span>
                    <span>公設 <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['axiom_ping']) && $_smarty_tpl->tpl_vars['arr_house']->value['axiom_ping'] != '0.00') {
echo $_smarty_tpl->tpl_vars['arr_house']->value['axiom_ping'];?>
坪<?php } else { ?>暫不提供<?php }?></span></li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr">
                <li>屋齡</li>
                <li>車位</li>
                <li>隔間</li>
                <li>機車位</li>
                <li>位置</li>
                <li> </li>
                <li> </li>
            </ul>
            <ul class="La">
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['house_old']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['house_old'];?>
年<?php } else { ?>暫不提供<?php }?></span></li>
                <li><span><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['car_space_ping']) && $_smarty_tpl->tpl_vars['arr_house']->value['car_space_ping'] != '0.00') {
echo $_smarty_tpl->tpl_vars['arr_house']->value['car_space_ping'];?>
坪<?php } else { ?>暫不提供<?php }?></span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['genre']) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['genre'];
} else { ?>未隔間<?php }?></span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['motorcycle_have'] == 1) {?>有<?php } else { ?>無<?php }?></span></li>
                <li><img src="/themes/App/mobile/img/RentHouse/go_map.png"></li>
                <li> </li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>社區公設</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"]) {?>
                <?php $_smarty_tpl->tpl_vars['count_arr'] = new Smarty_Variable(ceil(count($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"])/4)-1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_arr', 0);?>
                    <?php
$_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['count_arr']->value+1 - (0) : 0-($_smarty_tpl->tpl_vars['count_arr']->value)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 0, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
                        <?php if ($_smarty_tpl->tpl_vars['foo']->value == 0) {?><li>管理</li><?php } else { ?><li> &nbsp; </li><?php }?>
                    <?php }
}
?>

                 <?php } else { ?>
                    <li>管理</li>
                <?php }?>

                <li>管理費</li>
                <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["1"]) {?>
                <?php $_smarty_tpl->tpl_vars['count_arr'] = new Smarty_Variable(ceil(count($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["1"])/4)-1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_arr', 0);?>
                    <?php
$_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['count_arr']->value+1 - (0) : 0-($_smarty_tpl->tpl_vars['count_arr']->value)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 0, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
                        <?php if ($_smarty_tpl->tpl_vars['foo']->value == 0) {?><li>休閒公設</li><?php } else { ?><li> &nbsp; </li><?php }?>
                    <?php }
}
?>

                <?php } else { ?>
                    <li>休閒公設</li>
                <?php }?>
            </ul>
            <ul class="La">
                <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"])) {?>
                    <?php $_smarty_tpl->tpl_vars['count_arr'] = new Smarty_Variable(count($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"])-1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_arr', 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["3"];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value%4 == 0) {?><li class="pos"><?php }?>
                            <span><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</span>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value%4 == 3 || $_smarty_tpl->tpl_vars['k']->value == $_smarty_tpl->tpl_vars['count_arr']->value) {?></li><li> &nbsp; </li><?php }?>
                    <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_1_saved_key;
}
?>
                <?php } else { ?>
                    <li><span>無</span></li>
                <?php }?>
                <li><span><?php if ('s1' == 1) {?>含管理費<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee'];?>
/<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_cycle'];
if ($_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_unit']) {?>(<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['cleaning_fee_unit'];?>
)<?php }
}?></span></li>
                <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["1"])) {?>
                    <?php $_smarty_tpl->tpl_vars['count_arr'] = new Smarty_Variable(count($_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["1"])-1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_arr', 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['public_utilities']["1"];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_2_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_2_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value%4 == 0) {?><li class="pos"><?php }?>
                        <span><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</span>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value%4 == 3 || $_smarty_tpl->tpl_vars['k']->value == $_smarty_tpl->tpl_vars['count_arr']->value) {?></li><li> &nbsp; </li><?php }?>
                    <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
if ($__foreach_v_2_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_2_saved_key;
}
?>
                <?php } else { ?>
                    <li><span>無</span></li>
                <?php }?>
            </ul>





















        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>設備提供</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['device_category_data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_3_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_3_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_3_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_3_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                    <?php $_smarty_tpl->tpl_vars['count_arr'] = new Smarty_Variable(ceil(count($_smarty_tpl->tpl_vars['v']->value['device_category_data'])/4)-1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_arr', 0);?>
                        <?php
$_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['count_arr']->value+1 - (0) : 0-($_smarty_tpl->tpl_vars['count_arr']->value)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 0, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
                            <?php if ($_smarty_tpl->tpl_vars['foo']->value == 0) {?><li><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</li><?php } else { ?><li> &nbsp; </li><?php }?>
                        <?php }
}
?>

                <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_local_item;
}
}
if ($__foreach_v_3_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_3_saved_item;
}
if ($__foreach_v_3_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_3_saved_key;
}
?>
                <li></li>
            </ul>
            <ul class="La">
                <?php
$_from = $_smarty_tpl->tpl_vars['arr_house']->value['device_category_data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_4_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_4_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_4_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_4_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                    <?php $_smarty_tpl->tpl_vars['count_arr'] = new Smarty_Variable(ceil(count($_smarty_tpl->tpl_vars['v']->value['device_category_data']))-1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'count_arr', 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['v']->value['device_category_data'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_ch_5_saved_item = isset($_smarty_tpl->tpl_vars['v_ch']) ? $_smarty_tpl->tpl_vars['v_ch'] : false;
$__foreach_v_ch_5_saved_key = isset($_smarty_tpl->tpl_vars['k_ch']) ? $_smarty_tpl->tpl_vars['k_ch'] : false;
$_smarty_tpl->tpl_vars['v_ch'] = new Smarty_Variable();
$__foreach_v_ch_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_ch_5_total) {
$_smarty_tpl->tpl_vars['k_ch'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k_ch']->value => $_smarty_tpl->tpl_vars['v_ch']->value) {
$__foreach_v_ch_5_saved_local_item = $_smarty_tpl->tpl_vars['v_ch'];
?>
                        <?php if ($_smarty_tpl->tpl_vars['k_ch']->value%4 == 0) {?><li class="pos"><?php }?>
                        <span><?php echo $_smarty_tpl->tpl_vars['v_ch']->value['title'];?>
</span>
                        <?php if ($_smarty_tpl->tpl_vars['k_ch']->value%4 == 3 || $_smarty_tpl->tpl_vars['k_ch']->value == $_smarty_tpl->tpl_vars['count_arr']->value) {?></li><li> &nbsp; </li><?php }?>
                    <?php
$_smarty_tpl->tpl_vars['v_ch'] = $__foreach_v_ch_5_saved_local_item;
}
}
if ($__foreach_v_ch_5_saved_item) {
$_smarty_tpl->tpl_vars['v_ch'] = $__foreach_v_ch_5_saved_item;
}
if ($__foreach_v_ch_5_saved_key) {
$_smarty_tpl->tpl_vars['k_ch'] = $__foreach_v_ch_5_saved_key;
}
?>
                <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_4_saved_local_item;
}
}
if ($__foreach_v_4_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_4_saved_item;
}
if ($__foreach_v_4_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_4_saved_key;
}
?>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>出租條件</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <li>押金</li>
                <li>性别要求</li>
                <li>最短租期</li>
                <li>飼養寵物</li>
                <li>身分限制</li>
            </ul>
            <ul class="La">
                <li><span><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['deposit'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['deposit'];?>
個月押金<?php } else { ?>無押金<?php }?></span></li>
                <li><span><?php if (empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["26"]) && empty($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["27"])) {?>
                     不限<?php } else {
echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["26"];?>
 <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["27"];
}?></span></li>

                <li><span><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['m_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_s'];?>
個月~<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['m_e'];?>
個月
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_s'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_s'];?>
個月
                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['m_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['m_e'];?>
個月
                         <?php } else { ?>不可<?php }?></span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["5"]) {?>可<?php } else { ?>不可<?php }?></span></li>
                <li class="pos"><span>限家庭/夫妻 </span><span> 拒合租/幼童</span></li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr">
                <li>入住時間</li>
                <li>與房東同住</li>
                <li>租約公證</li>
                <li>神龕拜拜</li>
            </ul>
            <ul class="La">
                <li><span><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s'];?>
~<?php echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'];?>

                         <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s'];?>
之後
                         <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_s']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'])) {
echo $_smarty_tpl->tpl_vars['arr_house']->value['housing_time_e'];?>
之前
                         <?php } else { ?>隨時<?php }?></span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["25"]) {?>是<?php } else { ?>否<?php }?></span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["24"]) {?>是<?php } else { ?>否<?php }?></span></li>
                <li><span><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['other_conditions']["8"]) {?>可設置<?php } else { ?>不可<?php }?></span></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>生活機能</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <li>學區</li>
                <li>購物</li>
                <li>交通</li>
                <li>醫院</li>
            </ul>
            <ul class="La">
                <li class="pos"><?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school']) && !empty($_smarty_tpl->tpl_vars['arr_house']->value['j_school'])) {?><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['e_school'];?>
</span><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>
</span>
                        <?php } elseif (!empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school'])) {?><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>
</span>
                        <?php } elseif (empty($_smarty_tpl->tpl_vars['arr_house']->value['e_school'])) {?><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['j_school'];?>
</span>
                        <?php } else { ?><span>無</span><?php }?></li>
                <li> &nbsp; </li>
                <li class="pos">
                    <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['supermarket']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['shopping_center']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['market']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['night_market'])) {?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['supermarket']) {?><span>超商</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['shopping_center']) {?><span>購物中心/百貨公司</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['market']) {?><span>傳統市場</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['night_market']) {?><span>夜市</span><?php }?>
                    <?php } else { ?>未提供<?php }?></li>
                <li> &nbsp; </li>
                <li class="pos">
                    <?php if (!empty($_smarty_tpl->tpl_vars['arr_house']->value['bus']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['passenger_transport']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['train']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['mrt']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['high_speed_rail']) || !empty($_smarty_tpl->tpl_vars['arr_house']->value['interchange'])) {?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['bus']) {?><span>公車</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['passenger_transport']) {?><span>客運</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['night_market']) {?><span>夜市</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['mrt']) {?><span>捷運</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['high_speed_rail']) {?><span>高鐵</span><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['arr_house']->value['interchange']) {?><span>交流道</span><?php }?>
                    <?php } else { ?>未提供<?php }?></li>
                <li> &nbsp; </li>
                <li class="pos"><?php if ($_smarty_tpl->tpl_vars['arr_house']->value['hospital']) {?><span><?php echo $_smarty_tpl->tpl_vars['arr_house']->value['hospital'];?>
</span><?php }?></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>特色介紹</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F">
            <ul class="Fr">
                <li></li>
            </ul>
            <ul class="La">
                <?php echo $_smarty_tpl->tpl_vars['arr_house']->value['features'];?>
　
            </ul>
        </div>
    </div>
    <hr>
    <div class="contect_map_txt">
        <div class="contect_0_L" style="">
            <ul class="Fr" style="">
                <li>聯絡我</li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr" style="">
                <li>留言</li>
            </ul>
        </div>
        <div class="contect_0_L" style="">
            <ul class="Fr" style="">
                <li>預約賞屋</li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr" style="">
                <li>呼叫顧問</li>
            </ul>
        </div>
    </div>
</div><?php }
}
