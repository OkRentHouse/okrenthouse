<?php
/* Smarty version 3.1.28, created on 2020-12-11 17:40:19
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/menu_buoy.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd33e8356f8b7_29990776',
  'file_dependency' => 
  array (
    'c84710ae6186d072635f41e7bd5e707506fc6fed' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/menu_buoy.tpl',
      1 => 1607678489,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd33e8356f8b7_29990776 ($_smarty_tpl) {
?>
<nav class="menu_buoy">
    <div>
        <?php
$_from = $_smarty_tpl->tpl_vars['menu_buoy']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_0_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_0_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
            <?php if ($_smarty_tpl->tpl_vars['key']->value != 0) {?><b></b><?php }?>
            <a <?php if (!empty($_smarty_tpl->tpl_vars['value']->value['onclick'])) {?> href="javascript:void(0)" onclick="<?php echo $_smarty_tpl->tpl_vars['value']->value['onclick'];?>
"
                 <?php } else { ?>
                     href="<?php echo $_smarty_tpl->tpl_vars['value']->value['url'];?>
"
                 <?php }?>
               <?php if (in_array($_smarty_tpl->tpl_vars['buoy_url']->value,$_smarty_tpl->tpl_vars['value']->value['active'])) {?>class="active"<?php }?> target="_self" tabindex="-1">
                <?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>

            </a>
        <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_local_item;
}
}
if ($__foreach_value_0_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_item;
}
if ($__foreach_value_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_0_saved_key;
}
?>
        <buoy></buoy>
    </div>
</nav><?php }
}
