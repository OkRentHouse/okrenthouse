<?php
/* Smarty version 3.1.28, created on 2020-09-30 23:43:53
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/CollectPoints/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f74a7b9aaa0c7_63110670',
  'file_dependency' => 
  array (
    '1e30548e1a3b6fd58f9e21a547f015be569d900a' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/CollectPoints/content.tpl',
      1 => 1601361225,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f74a7b9aaa0c7_63110670 ($_smarty_tpl) {
?>
<div data-role="content" id="Coupon" class="ui-content" role="main">
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/coupon/min_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <?php if (empty($_GET['id'])) {?>
        <div class="document">
        <div class="take_goodies_wrap">
            <div class="banner">
                <div class="postion">
                    <div class="caption">大集大利拿好康</div>
                    <div class="text">快來兌換你的專屬好康</div>
                </div>
                <img src="themes/App/mobile/img/collectpoints/banner.jpg" alt="">
            </div>
            <div class="commodity_wrap">
                <ul>
                    <li class="commodity">
                        <div class="frame_wrap">
                            <a href="CollectPoints?id=1" class="link">
                                <img src="themes/App/mobile/img/collectpoints/01.jpg" alt="">
                            </a>
                            <div class="info_content">
                                <div class="name">DASHIANG保溫杯12345678901234567890</div>
                                <div class="point">1,000<span> 兌點</span></div>
                                <div class="amount">剩餘數量 <span>20</span> 個</div>
                            </div>
                        </div>
                    </li>
                    <li class="commodity">
                        <div class="frame_wrap">
                            <a href="" class="link">
                                <img src="themes/App/mobile/img/collectpoints/02.jpg" alt="">
                            </a>
                            <div class="info_content">
                                <div class="name">健康御守滅菌王漱口錠</div>
                                <div class="point">2,000 <span> 兌點</span></div>
                                <div class="amount">剩餘數量 <span>5</span> 組</div>
                            </div>
                        </div>
                    </li>
                    <li class="commodity">
                        <div class="frame_wrap">
                            <a href="CollectPoints?id=3" class="link">
                                <img src="themes/App/mobile/img/collectpoints/03.jpg" alt="">
                            </a>
                            <div class="info_content">
                                <div class="name">室內空氣健康指數檢測服務</div>
                                <div class="point">5,000<span> 兌點</span></div>
                                <div class="amount">剩餘數量 <span>30</span> 次</div>
                            </div>
                        </div>
                    </li>
                    <li class="commodity">
                        <div class="frame_wrap">
                            <a href="" class="link">
                                <img src="themes/App/mobile/img/collectpoints/04.jpg" alt="">
                            </a>
                            <div class="info_content">
                                <div class="name">健康御守-滅菌王隨身寶禮盒組</div>
                                <div class="point">3,000<span> 兌點</span></div>
                                <div class="amount">剩餘數量 <span>20</span> 組</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php } else { ?>
        <div class="document">
            <div class="take_goodies_main_wrap">
                <div class="banner">
                    <img src="themes/App/mobile/img/collectpoints//banner_in.jpg" alt="">
                </div>

                <div class="commodity_wrap">
                    <div class="frame text-center">
                        <div class="caption">室內空氣健康指數檢測服務</div>
                    </div>

                    <div class="frame">
                        <div class="content">
                            透過精密高端的檢測儀器 守護居住環境
                            經由室內環境的健康認證 居住更安心
                            可精確檢測　PM2.5｜甲醛｜黴菌｜揮發物質
                            二氧化碳｜室內綜合指數　等
                            為您的健康把關
                        </div>
                    </div>
                    <hr>
                    <div class="frame">
                        <div class="flex m_b_10">
                            <div class="list">
                                <div class="caption">兌換點數</div>
                                <div class="value">5,000</div>
                            </div>
                            <div class="list">
                                <div class="caption">剩餘數量</div>
                                <div class="value">30</div>
                            </div>
                        </div>
                        <div class="flex m_b_10">
                            <div class="list">
                                <div class="caption">我的紅利點數</div>
                                <div class="value">15,000</div>
                            </div>
                            <div class="list">
                                <div class="caption">兌換數量</div>
                                <div class="value">
                                    <input data-role="none" type="number" value="1">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-center">
                            <a href="" class="exchange">
                                我要兌換
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    <?php }?>
</div><?php }
}
