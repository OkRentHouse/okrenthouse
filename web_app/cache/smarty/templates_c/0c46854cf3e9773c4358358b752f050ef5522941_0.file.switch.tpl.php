<?php
/* Smarty version 3.1.28, created on 2020-10-29 16:57:56
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/List/switch.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f9a8414c93e51_84110139',
  'file_dependency' => 
  array (
    '0c46854cf3e9773c4358358b752f050ef5522941' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/List/switch.tpl',
      1 => 1603961875,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f9a8414c93e51_84110139 ($_smarty_tpl) {
?>
<!--租or售-->
<div class="ui-link-house_choose_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-house_choose">
        <div class="ui-link-house_choose-selter">

        </div>
    </div>
</div>

<!--型態-->
<div class="ui-link-types_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-types">
        <div class="ui-link-types-selter">

        </div>
    </div>
</div>

<!--城市-->
<div class="ui-link-county_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-county">
        <div class="ui-link-county-selter">

        </div>
    </div>
</div>

<!--鄉鎮-->
<div class="ui-link-city_b" style="background: #15191f;    width: 100%;    height: 100%; display:none">
    <div class="ui-link-city">
        <div class="ui-link-city-selter">

        </div>
    </div>
</div>

<table class="menu_buoy_main">

  <tr>
  <td class="c_1"><span class="txt">精選推薦</span></td>
  </tr>
  <tr>
  <td class="c_2"><nav class="menu_buoy">
    <div>
            <?php
$_from = $_smarty_tpl->tpl_vars['menu_buoy_switch']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_0_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_0_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_0_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['v']->value['a'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['v']->value['active']) {?>active<?php }?> ui-link" <?php if ($_smarty_tpl->tpl_vars['v']->value['onclick']) {?>onclick="<?php echo $_smarty_tpl->tpl_vars['v']->value['onclick'];?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['v']->value['data_a']) {?>data-a="<?php echo $_smarty_tpl->tpl_vars['v']->value['data_a'];?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['v']->value['data_class']) {?>data-class="<?php echo $_smarty_tpl->tpl_vars['v']->value['data_class'];?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</a>
            <?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_local_item;
}
}
if ($__foreach_v_0_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_0_saved_item;
}
if ($__foreach_v_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_0_saved_key;
}
?>

            <buoy style="left: 0px; right: 318px;"></buoy>
    </div>
  </nav>
  </td></tr>
</table>

<!-- 搜尋ber -->
	<input type="hidden" name="active" value="1">
	<input type="hidden" name="house_choose" value="">
	<input type="hidden" name="id_types" value="">
	<input type="hidden" name="county" value="">
	<input type="hidden" name="city" value="">
	<div style="display: flex;width: 70%;margin: auto;">
		<div class="search_select">
			<span><a href="#" data-transition="slide" class="ui-link ui-link-house_choose-txt">租屋</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-county-txt">區域</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-types-txt">型態</a></span>
		</div>





	</div>
  <div class="notify" style="width: 100%;text-align: center;">
    <!--<label>選擇 <span>租屋</span> <span>區域</span> 或 <span>型態</span> 篩選後點圖片</label>-->
  </div>
<?php
$_from = $_smarty_tpl->tpl_vars['img_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$__foreach_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_v_1_total) {
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
    <div class="zone zone_1 <?php echo $_smarty_tpl->tpl_vars['v']->value['class'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['hidden'] == true) {?> style="display: none"<?php }?>>
        <div class="item">
            <div class="top_bar"><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</div>
            <div class="photo">
                <!--<div class="search"><span class="notify">點我看更多</span> <i class="fas fa-angle-double-right"></i>  <i class="fas fa-search"></i> </div>-->
                <a href="<?php echo $_smarty_tpl->tpl_vars['v']->value['a'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['data_a']) {?>data-a="<?php echo $_smarty_tpl->tpl_vars['v']->value['data_a'];?>
"<?php }?>>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['v']->value['img'];?>
">
                </a>
            </div>
        </div>
    </div>
<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_1_saved_key;
}
echo $_smarty_tpl->tpl_vars['js']->value;?>

<!-- 搜尋ber END-->
<?php }
}
