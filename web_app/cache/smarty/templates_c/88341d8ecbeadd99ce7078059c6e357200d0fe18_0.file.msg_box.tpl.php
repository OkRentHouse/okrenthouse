<?php
/* Smarty version 3.1.28, created on 2020-11-23 14:00:24
  from "/home/ilifehou/life-house.com.tw/themes/AppOkrent/mobile/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fbb4ff83c40b1_54269161',
  'file_dependency' => 
  array (
    '88341d8ecbeadd99ce7078059c6e357200d0fe18' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/AppOkrent/mobile/msg_box.tpl',
      1 => 1601361206,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbb4ff83c40b1_54269161 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'success',
			html: '<?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
	<?php echo '<script'; ?>
>
		Swal.fire({
			type: 'error',
			html: '<?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
'
		});
	<?php echo '</script'; ?>
>
<?php }
}
}
