<?php
/* Smarty version 3.1.28, created on 2020-12-19 00:16:32
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/WantCoupon/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fdcd5e0aceaf8_86256159',
  'file_dependency' => 
  array (
    '703667e1707ff1016796c0b52564aef34a9ba7e4' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/WantCoupon/content.tpl',
      1 => 1607678621,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fdcd5e0aceaf8_86256159 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/coupon/min_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="document">
    <?php if (count($_smarty_tpl->tpl_vars['arr_content_benner']->value)) {?>
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true;ratio: <?php echo $_smarty_tpl->tpl_vars['content_ratio']->value;?>
">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_content_benner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_0_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_0_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a></li>
                    <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_local_item;
}
}
if ($__foreach_banner_0_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_item;
}
if ($__foreach_banner_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_0_saved_key;
}
?>
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
    <?php }?>
</div>
<div class="title_banner s">
	<div class="separate"></div>
	<div class="slide_title">要好康</div>

</div>
<div class="want1">
	<a class="ui-link" href="#">
        <?php if (count($_smarty_tpl->tpl_vars['to_1_img']->value)) {?><img src="<?php echo $_smarty_tpl->tpl_vars['to_1_img']->value;?>
"><?php }?>
		<a class="want_btn ui-link" href="#">我要好康</a>
	</a>
</div>
<div class="title_banner s">
	<div class="separate"></div>
</div>
<div class="want2">
	<a class="ui-link" href="#">
        <?php if (count($_smarty_tpl->tpl_vars['to_2_img']->value)) {?><img src="<?php echo $_smarty_tpl->tpl_vars['to_2_img']->value;?>
"><?php }?>
		<a class="link ui-link" href="#">每天都有新掀貨 快來逛逛</a>
	</a>
</div><?php }
}
