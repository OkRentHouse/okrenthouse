<?php
/* Smarty version 3.1.28, created on 2020-12-15 15:31:20
  from "/opt/lampp/htdocs/life-house.com.tw/themes/default/maintenance.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd86648d8ab77_55564896',
  'file_dependency' => 
  array (
    '724e222f6d017e4b8c11832e10412debd10249a4' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/default/maintenance.tpl',
      1 => 1607678483,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd86648d8ab77_55564896 ($_smarty_tpl) {
?>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>{}</title>
	<link href="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
/css/reset.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
/css/maintenance.css" rel="stylesheet">
	</head>
<body>
<header>
	<div class="nav-bar">
		<nav class="nav_left">
			<img class="logo" src="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
/images/logo.png" title="" alt="">
		</nav>
	</div>
</header>
<article>
	<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
/images/maintenance/Planet.png" class="planet">
	<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
/images/maintenance/UFO.png" class="ufo">
	<?php if (Configuration::get('MAINTENANCE_TEXT') != '') {?><div class="text"><?php echo Configuration::get('MAINTENANCE_TEXT');?>
</div><?php }?>
	<?php if (Configuration::get('MAINTENANCE_TEXT') != '' && Configuration::get('MAINTENANCE_EMAIL') != '') {?>
	<div class="link">
		<?php if (Configuration::get('MAINTENANCE_FB') != '') {?><a href="<?php echo Configuration::get('MAINTENANCE_FB');?>
" class="fb"><img src="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
/images/maintenance/UFO_fb.png" class="fb" title="facebook" alt="facebook"></a><?php }?>
		<?php if (Configuration::get('MAINTENANCE_EMAIL') != '') {?><a href="mailto:<?php echo Configuration::get('MAINTENANCE_EMAIL');?>
" class="email"><img src="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
/images/maintenance/UFO_email.png" class="email" title="email" alt="email"></a><?php }?>
	</div>
	<?php }?>
</article>
</body>
</html><?php }
}
