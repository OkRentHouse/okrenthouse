<?php
/* Smarty version 3.1.28, created on 2020-11-23 14:00:24
  from "/home/ilifehou/life-house.com.tw/themes/AppOkrent/mobile/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fbb4ff83b2351_00706643',
  'file_dependency' => 
  array (
    '53e17195acd6c7b6e488099296da0f1f7849623c' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/AppOkrent/mobile/header.tpl',
      1 => 1603073011,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fbb4ff83b2351_00706643 ($_smarty_tpl) {
?>
<div data-role="header" class="color_page" data-id="index" data-position="fixed">
	<?php if ($_smarty_tpl->tpl_vars['role']->value != 'dialog') {?>
		<?php if (!empty($_smarty_tpl->tpl_vars['back_url']->value)) {?><a href="<?php echo $_smarty_tpl->tpl_vars['back_url']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['back_url']->value == '#') {?> class="close_btn" data-rel="back"<?php }?> data-transition="slide" data-direction="reverse" data-role="none"><img class="" src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/return.svg"></a><?php }?>

		<div class="open_btn glyphicon glyphicon-menu-hamburger"></div><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['page_header_toolbar_img']->value != '') {?>
		<h2 class="ui-title"><a href="/" data-role="none"><img src="<?php echo $_smarty_tpl->tpl_vars['page_header_toolbar_img']->value;?>
"></a></h2>
	<?php } else { ?>
		<h1 class="ui-title"><?php echo $_smarty_tpl->tpl_vars['page_header_toolbar_title']->value;?>
</h1>
	<?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['right_menu']->value) && !empty($_smarty_tpl->tpl_vars['right_menu']->value)) {?><div class="right_manu"><?php echo $_smarty_tpl->tpl_vars['right_menu']->value;?>
</div><?php }?>
</div><?php }
}
