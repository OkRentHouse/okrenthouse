<?php
/* Smarty version 3.1.28, created on 2021-01-04 11:31:00
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5ff28bf410a916_68311161',
  'file_dependency' => 
  array (
    '965738e196607e13237499eb5aeeb9a3d4a3cb7e' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/Index/content.tpl',
      1 => 1609731057,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ff28bf410a916_68311161 ($_smarty_tpl) {
?>
<!--租or售-->
<div class="ui-link-house_choose_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-house_choose">
        <div class="ui-link-house_choose-selter">

        </div>
    </div>
</div>

<!--用途-->
<div class="ui-link-main_house_class_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-main_house_class">
        <div class="ui-link-main_house_class-selter">

        </div>
    </div>
</div>

<!--型態-->
<div class="ui-link-types_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-types">
        <div class="ui-link-types-selter">

        </div>
    </div>
</div>

<!--類別-->
<div class="ui-link-type_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-type">
        <div class="ui-link-type-selter">

        </div>
    </div>
</div>

<!--城市-->
<div class="ui-link-county_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-county">
        <div class="ui-link-county-selter">

        </div>
    </div>
</div>

<!--鄉鎮-->
<div class="ui-link-city_b" style="background: #15191f;    width: 100%;    height: 100%; display:none">
    <div class="ui-link-city">
        <div class="ui-link-city-selter">

        </div>
    </div>
</div>

<?php if (count($_smarty_tpl->tpl_vars['arr_index_banner']->value)) {?>
<div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: <?php echo $_smarty_tpl->tpl_vars['index_ratio']->value;?>
">
    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
        <ul class="uk-slideshow-items">
            <?php
$_from = $_smarty_tpl->tpl_vars['arr_index_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_0_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_0_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
            <li>
                <a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a>
            </li>
            <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_local_item;
}
}
if ($__foreach_banner_0_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_item;
}
if ($__foreach_banner_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_0_saved_key;
}
?>
        </ul>
        <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
        <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
    </div>
    <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>

</div>
<?php }?> <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/menu_buoy.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="document">
    <!--<div class="title_banner">
		<div class="slide_title no_border_r">吉屋快搜</div>
	</div>-->

    <div class="slide_area">
          

        <div class="slide_photo" uk-slideshow="animation: push;ratio: <?php echo $_smarty_tpl->tpl_vars['list_ratio']->value;?>
" id="slide_photo_1" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">房地專區</div>
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="https://app.life-house.com.tw/list" class="" >吉屋快搜</a><a href="https://app.life-house.com.tw/list?switch=1" >推薦精選</a><a href="https://app.life-house.com.tw/list"   style="position: relative;" >刊登廣告<span class="free" style="">免費</span></a></nav>
                </div>
            </div>
            <?php if (count($_smarty_tpl->tpl_vars['arr_list_banner']->value)) {?>
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_list_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_1_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_1_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_1_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_1_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
                    <li>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a>
                    </li>
                    <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_1_saved_local_item;
}
}
if ($__foreach_banner_1_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_1_saved_item;
}
if ($__foreach_banner_1_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_1_saved_key;
}
?>
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-nexttitle_banner uk-slideshow-item="next"></samp>
            </div>
            <?php }?> 
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/house/search_house.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

        </div>

        <div class="slide_photo" uk-slideshow="animation: push;ratio: <?php echo $_smarty_tpl->tpl_vars['r_ratio']->value;?>
" id="slide_photo_2" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">租管服務</div>
                <!-- <div class="slide_title border_r">樂租管理</div> -->
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="#" uk-slideshow-item="0" class="active">管理查詢</a><a href="/handled" uk-slideshow-item="1">代租代管</a><a href="#" uk-slideshow-item="2">租事順利</a></nav>
                </div>
            </div>
            <?php if (count($_smarty_tpl->tpl_vars['arr_rent_banner']->value)) {?>
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_rent_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_2_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_2_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_2_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_2_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
                    <li>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a>
                    </li>
                    <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_2_saved_local_item;
}
}
if ($__foreach_banner_2_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_2_saved_item;
}
if ($__foreach_banner_2_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_2_saved_key;
}
?>
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
            </div>
            <?php }?> 
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>

        <div class="slide_photo" uk-slideshow="animation: push;ratio: <?php echo $_smarty_tpl->tpl_vars['n_ratio']->value;?>
" id="slide_photo_3" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">News好康</div>
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="https://app.life-house.com.tw/coupon" class="" >生活好康</a><a href="https://app.life-house.com.tw/InLife" >活動分享</a><a href="https://app.life-house.com.tw/LifeGo"
                             >生活樂購</a>
                    </nav>
                </div>
            </div>
            <?php if (count($_smarty_tpl->tpl_vars['arr_coupon_banner']->value)) {?>
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_coupon_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_3_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_3_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_3_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_3_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
                    <li>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a>
                    </li>
                    <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_3_saved_local_item;
}
}
if ($__foreach_banner_3_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_3_saved_item;
}
if ($__foreach_banner_3_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_3_saved_key;
}
?>
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-nexttitle_banner uk-slideshow-item="next"></samp>
            </div>
            <?php }?> 
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>

        <div class="slide_photo" uk-slideshow="animation: push;ratio: <?php echo $_smarty_tpl->tpl_vars['h_ratio']->value;?>
" id="slide_photo_4" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">生活家族</div>
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="https://app.life-house.com.tw/ExpertAdvisor"  class="active">達人顧問</a><a href="https://app.life-house.com.tw/Patner"  >共好夥伴</a><a href="https://app.life-house.com.tw/JoinUs" >加盟體系</a></nav>
                </div>
            </div>
            <?php if (count($_smarty_tpl->tpl_vars['arr_life_banner']->value)) {?>
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    <?php
$_from = $_smarty_tpl->tpl_vars['arr_life_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_4_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_4_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_4_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_4_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
                    <li>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a>
                    </li>
                    <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_4_saved_local_item;
}
}
if ($__foreach_banner_4_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_4_saved_item;
}
if ($__foreach_banner_4_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_4_saved_key;
}
?>
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
            </div>
            <?php }?> 
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>
    </div>
</div>
<?php }
}
