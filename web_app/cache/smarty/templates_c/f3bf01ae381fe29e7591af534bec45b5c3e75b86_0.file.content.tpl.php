<?php
/* Smarty version 3.1.28, created on 2020-10-27 10:18:13
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/StoreMap/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f9783655379f5_64488231',
  'file_dependency' => 
  array (
    'f3bf01ae381fe29e7591af534bec45b5c3e75b86' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/StoreMap/content.tpl',
      1 => 1601361224,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f9783655379f5_64488231 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./mobile/coupon/min_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="document">
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">找好康</div>
	</div>
    <?php echo $_smarty_tpl->tpl_vars['search_store']->value;?>

	<div id="map"></div>
	<?php echo '<script'; ?>
 type="text/javascript">
		var map;
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
				center: {
					lat: <?php echo $_smarty_tpl->tpl_vars['google_map']->value['lat'];?>
,
					lng: <?php echo $_smarty_tpl->tpl_vars['google_map']->value['lng'];?>
,
				},
				zoom: <?php echo $_smarty_tpl->tpl_vars['google_map']->value['zoom'];?>
,
				minZoom:<?php echo $_smarty_tpl->tpl_vars['google_map']->value['minZoom'];?>
,
				maxZoom:<?php echo $_smarty_tpl->tpl_vars['google_map']->value['maxZoom'];?>
,
				streetViewControl:false,	//顯示街景
				fullscreenControl:true,		//全螢幕地圖
				zoomControl:false,			//放大縮小地圖
				mapTypeControl:false		//地圖與衛星類型
			});
		}
	<?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $_smarty_tpl->tpl_vars['google_key']->value;?>
&callback=initMap&libraries=geometry&sensor=false">
	<?php echo '</script'; ?>
>
</div><?php }
}
