<?php
/* Smarty version 3.1.28, created on 2020-12-29 09:26:52
  from "/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/Store/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fea85dc4052b5_23692339',
  'file_dependency' => 
  array (
    '24b3f23724a9c5cc4b9b251df7b44b97709f394d' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/App/mobile/controllers/Store/content.tpl',
      1 => 1607678621,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fea85dc4052b5_23692339 ($_smarty_tpl) {
?>
<div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: <?php echo $_smarty_tpl->tpl_vars['store']->value['ratio'];?>
">
	<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
		<ul class="uk-slideshow-items">
            <?php
$_from = $_smarty_tpl->tpl_vars['store']->value['img'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_img_0_saved_item = isset($_smarty_tpl->tpl_vars['img']) ? $_smarty_tpl->tpl_vars['img'] : false;
$__foreach_img_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['img'] = new Smarty_Variable();
$__foreach_img_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_img_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['img']->value) {
$__foreach_img_0_saved_local_item = $_smarty_tpl->tpl_vars['img'];
?>
				<li><img src="<?php echo $_smarty_tpl->tpl_vars['img']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</li>
			<?php
$_smarty_tpl->tpl_vars['img'] = $__foreach_img_0_saved_local_item;
}
} else {
?>
				<li><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/default.jpg"></li>
            <?php
}
if ($__foreach_img_0_saved_item) {
$_smarty_tpl->tpl_vars['img'] = $__foreach_img_0_saved_item;
}
if ($__foreach_img_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_img_0_saved_key;
}
?>
		</ul>
		<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
			  uk-slideshow-item="previous"></samp>
		<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
			  uk-slideshow-item="next"></samp>
	</div>
	<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
	<div class="favorite <?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?>on<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
" data-type="store"><?php if ($_smarty_tpl->tpl_vars['store']->value['favorite']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart_on.svg"><?php } else { ?><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/icon/heart.svg"><?php }?></div>

</div>
<div class="store_content">
	<div class="title"><?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
</div>
	<div class="career"><?php echo $_smarty_tpl->tpl_vars['store']->value['store_type'];?>
</div>
    <?php
$_from = $_smarty_tpl->tpl_vars['store']->value['address1'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_address_1_saved_item = isset($_smarty_tpl->tpl_vars['address']) ? $_smarty_tpl->tpl_vars['address'] : false;
$__foreach_address_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['address'] = new Smarty_Variable();
$__foreach_address_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_address_1_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['address']->value) {
$__foreach_address_1_saved_local_item = $_smarty_tpl->tpl_vars['address'];
?>
	<div class="add"><label><?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?><i><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/map_tag.svg"></i>ADD<?php } else { ?>　<?php }?></label>
		<div><?php echo $_smarty_tpl->tpl_vars['address']->value;?>
</div>
	</div>
    <?php
$_smarty_tpl->tpl_vars['address'] = $__foreach_address_1_saved_local_item;
}
}
if ($__foreach_address_1_saved_item) {
$_smarty_tpl->tpl_vars['address'] = $__foreach_address_1_saved_item;
}
if ($__foreach_address_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_address_1_saved_key;
}
?>
    <?php
$_from = $_smarty_tpl->tpl_vars['store']->value['tel'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tel_2_saved_item = isset($_smarty_tpl->tpl_vars['tel']) ? $_smarty_tpl->tpl_vars['tel'] : false;
$__foreach_tel_2_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['tel'] = new Smarty_Variable();
$__foreach_tel_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_tel_2_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['tel']->value) {
$__foreach_tel_2_saved_local_item = $_smarty_tpl->tpl_vars['tel'];
?>
		<div class="tel"><label><?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?><i><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/phone.svg"></i>TEL<?php } else { ?>　<?php }?></label>
			<div><?php echo $_smarty_tpl->tpl_vars['tel']->value;?>
</div>
		</div>
	<?php
$_smarty_tpl->tpl_vars['tel'] = $__foreach_tel_2_saved_local_item;
}
}
if ($__foreach_tel_2_saved_item) {
$_smarty_tpl->tpl_vars['tel'] = $__foreach_tel_2_saved_item;
}
if ($__foreach_tel_2_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_tel_2_saved_key;
}
?>
    <?php
$_from = $_smarty_tpl->tpl_vars['store']->value['time'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_time_3_saved_item = isset($_smarty_tpl->tpl_vars['time']) ? $_smarty_tpl->tpl_vars['time'] : false;
$__foreach_time_3_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['time'] = new Smarty_Variable();
$__foreach_time_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_time_3_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['time']->value) {
$__foreach_time_3_saved_local_item = $_smarty_tpl->tpl_vars['time'];
?>
	<div class="time"><label><?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?><i><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/time.svg"></i>TIME<?php } else { ?>　<?php }?></label>

		<div><div><?php echo $_smarty_tpl->tpl_vars['time']->value;?>
</div></div>
	</div>
    <?php
$_smarty_tpl->tpl_vars['time'] = $__foreach_time_3_saved_local_item;
}
}
if ($__foreach_time_3_saved_item) {
$_smarty_tpl->tpl_vars['time'] = $__foreach_time_3_saved_item;
}
if ($__foreach_time_3_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_time_3_saved_key;
}
?>
	<div class="hot"><label><i><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/hot.svg"></i>HOT</label>
		<div><?php
$_from = $_smarty_tpl->tpl_vars['store']->value['service'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_service_4_saved_item = isset($_smarty_tpl->tpl_vars['service']) ? $_smarty_tpl->tpl_vars['service'] : false;
$__foreach_service_4_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['service'] = new Smarty_Variable();
$__foreach_service_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_service_4_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['service']->value) {
$__foreach_service_4_saved_local_item = $_smarty_tpl->tpl_vars['service'];
?><samp><?php echo $_smarty_tpl->tpl_vars['service']->value;?>
</samp><samp><?php echo $_smarty_tpl->tpl_vars['service']->value;?>
</samp><?php
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_4_saved_local_item;
}
}
if ($__foreach_service_4_saved_item) {
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_4_saved_item;
}
if ($__foreach_service_4_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_service_4_saved_key;
}
?></div>
	</div>
    <?php
$_from = $_smarty_tpl->tpl_vars['store']->value['discount'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_discount_5_saved_item = isset($_smarty_tpl->tpl_vars['discount']) ? $_smarty_tpl->tpl_vars['discount'] : false;
$__foreach_discount_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['discount'] = new Smarty_Variable();
$__foreach_discount_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_discount_5_total) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['discount']->value) {
$__foreach_discount_5_saved_local_item = $_smarty_tpl->tpl_vars['discount'];
?>
	<div class="offer"><label><?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>OFFER<?php } else { ?>　<?php }?></label>
		<div><a><?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</a></div>
	</div>
    <?php
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_5_saved_local_item;
}
}
if ($__foreach_discount_5_saved_item) {
$_smarty_tpl->tpl_vars['discount'] = $__foreach_discount_5_saved_item;
}
if ($__foreach_discount_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_discount_5_saved_key;
}
?>
	<div class="popularity"><label>人氣星等</label><div class="div_popularity" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['popularity'];?>
</div></div>
	<?php if (!empty($_SESSION['id_member'])) {?>
	<div class="star"><label><i><img src="<?php echo $_smarty_tpl->tpl_vars['THEME_URL']->value;?>
/img/star.svg"></i>給星星</label><div class="div_star give_star" data-title="<?php echo $_smarty_tpl->tpl_vars['store']->value['title'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['store']->value['id_store'];?>
" data-star="<?php echo $_smarty_tpl->tpl_vars['store']->value['star'];?>
"><?php echo $_smarty_tpl->tpl_vars['store']->value['star_img'];?>
</div></div>
	<?php }?>
</div><?php }
}
