<?php
/* Smarty version 3.1.28, created on 2020-10-23 14:27:45
  from "/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/LoginStore/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5f9277e13ea7c4_50625184',
  'file_dependency' => 
  array (
    '649701c4837015cc856e3967c0a65be9693b4faf' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/App/mobile/controllers/LoginStore/content.tpl',
      1 => 1601361224,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f9277e13ea7c4_50625184 ($_smarty_tpl) {
?>
<div class="login_wrap">
	<form role="form">
		<div class="input_div">
			<label class="label" for="account">帳 號</label><input type="text" id="account" name="account" class="input_frame" data-role="none" value="">
		</div>
		<div class="input_div">
			<label class="label" for="password">密 碼</label><input type="password" id="password" name="password" class="input_frame" data-role="none" value="">
		</div>
		<div class="btn_div">
			<div class="text-center">
				<a href="#" class="confirm_btn ui-btn ui-shadow ui-corner-all">註 冊</a>
				<a class="forget text-center ui-link" href="#">忘記密碼 ?</a>
			</div><div class="text-center">
				<button type="submit" class="confirm_btn">登 入</button>
				<div class="checkbox_wrap">
					<input type="checkbox" name="auto_login" data-role="none" value=""><span class="forget">自動登入</span>
				</div>
			</div>
		</div>
	</form>
	<div class="text-center">
		<a class="fb_login ui-link"><img src="./註冊_files/imgs/fb.svg"></a>
	</div>
</div><?php }
}
