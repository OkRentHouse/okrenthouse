<?php
class AdminNewsController extends AdminController
{
	public function __construct(){
		$this->className = 'AdminNewsController';
		$this->table = 'news';
		$this->fields['index'] = 'id_news';
		$this->fields['title'] = '公告管理';
		$this->fields['order'] = ' ORDER BY `top` DESC, `date` DESC, `add_time` DESC';
		
		if($_SESSION['id_group'] != 1){
			if($_SESSION['admin'] != 1){		//非系統管理者
				if($_SESSION['id_web']){
					$where = ' AND (`id_web` = '.$_SESSION['id_web'].' OR `id_web` = 0)';
					$this->fields['where'] = ' AND (`id_web` = '.$_SESSION['id_web'].' OR `id_web` = 0)';
				}
			}
		}

		$this->fields['list_num'] = 50;

		$this->fields['list'] = array(
			'id_news' => array(
				'index' => true,
				'title' => $this->l('ID'),
				'type' => 'checkbox',
				'hidden' => true,
				'class' => 'text-center'
			),
			'id_web' => array(
				'title' => $this->l('社區名稱'),
				'order' => true,
				'filter' => true,
				'key' => array(
					'table' => 'web',
					'key' => 'id_web',
					'val' => 'web',
					'where' => $where,
					'order' => '`web` ASC'
				),
				'class' => 'text-center'
			),
			'top' => array(
				'title' => $this->l('置頂'),
				'order' => true,
				'filter' => true,
				'values' => array(
					array(
						'class' => 'top',
						'value' => 1,
						'title' => $this->l('置頂')
					),
				),
				'class' => 'text-center'
			),
			'id_news_type' => array(
				'title' => $this->l('類別'),
				'order' => true,
				'filter' => true,
				'key' => array(
					'table' => 'news_type',
					'key' => 'id_news_type',
					'val' => 'news_type',
					'where' => $where,
					'order' => '`news_type` ASC'
				),
				'class' => 'text-center'
			),
			'title' => array(
				'title' => $this->l('標題'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'date' => array(
				'title' => $this->l('發佈日期'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'content' => array(
				'title' => $this->l('內容'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center',
				'show' => false
			),
			'end_date' => array(
				'title' => $this->l('結束日期'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'add_time' => array(
				'title' => $this->l('建立時間'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'active' => array(
				'title' => $this->l('啟用狀態'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
		);

		$this->fields['form'] = array(
			'news' => array(
				'tab' => 'letter',
				'legend' => array(
					'title' => $this->l('公告管理'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					'id_news' => array(
						'name' => 'id_news',
						'type' => 'hidden',
						'index' => true,
						'required' => true
					),
					'id_web' => array(
						'name' => 'id_web',
						'type' => 'select',
						'label' => $this->l('社區名稱'),
						'val' => $_SESSION['id_web'],
						'options' => array(
							'default' => array(
								'text' => '發佈所有社區',
								'val' => ''
							),
							'table' => 'web',
							'text' => 'web',
							'value' => 'id_web',
							'where' => $where,
							'order' => '`web` ASC'
						),
					),
					'top' => array(
						'name' => 'top',
						'type' => 'checkbox',
						'label' => $this->l('置頂'),
						'values' => array(
							array(
								'id' => 'top',
								'value' => 1,
								'label' => $this->l('置頂')
							),
						)
					),
					'id_news_type' => array(
						'name' => 'id_news_type',
						'type' => 'select',
						'label' => $this->l('類別'),
						'options' => array(
							'default' => array(
								'text' => '請選擇類別',
								'val' => ''
							),
							'table' => 'news_type',
							'where' => $where,
							'parent_name' => 'id_web',
							'parent' => 'id_web',
							'text' => 'news_type',
							'value' => 'id_news_type',
							'order' => '`news_type` ASC'
						),
						'required' => true
					),
					'title' => array(
						'name' => 'title',
						'type' => 'text',
						'label' => $this->l('標題'),
						'maxlength' => '20',
						'required' => true
					),
					'date' => array(
						'name' => 'date',
						'type' => 'date',
						'label' => $this->l('發佈日期'),
						'val' => today(),
						'required' => true
					),
					'end_date' => array(
						'name' => 'end_date',
						'type' => 'date',
						'label' => $this->l('結束日期'),
					),
					'content' => array(
						'name' => 'content',
						'type' => 'textarea',
						'label' => $this->l('內容'),
						'class' => 'tinymce',
						'required' => true,
						'col' => 8,
						'rows' => 20,
					),
					'add_time' => array(
						'name' => 'add_time',
						'type' => 'view',
						'auto_datetime' => 'add',
						'label' => $this->l('建立時間'),
					),
					'active' => array(
						'type' => 'switch',
						'label' => $this->l('啟用狀態'),
						'name' => 'active',
						'val' => 1,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('啟用')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('關閉')
							)
						)
					)
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					),
				),
				'cancel' => array(
					'title' => $this->l('取消')
				),
				'reset' => array(
					'title' => $this->l('復原')
				)
			),
			'file' => array(
				'tab' => 'file',
				'legend' => array(
					'title' => $this->l('附件'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					'annex' => array(
						'no_label' => true,
						'name' => 'annex',
						'type' => 'file',
						'language' => 'zh-TW',
						'file' => array(
							'icon' => false,
						),
						'multiple' => true,
						'col' => 12,
						'no_action' => true
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					),
				),
				'cancel' => array(
					'title' => $this->l('取消')),
				'reset' => array(
					'title' => $this->l('復原')
				)
			),
		);

		if($_SESSION['id_group'] != 1){
			if($_SESSION['admin'] != 1) {        //非系統管理者
				$this->fields['list']['id_web']['hidden'] = true;
				$this->fields['form']['news']['input']['id_web']['type'] = 'hidden';
				unset($this->fields['form']['news']['input']['id_web']['options']['default']);
			}
		}

		parent::__construct();
	}

	public function initToolbar(){		//初始化功能按鈕
		if ($this->display == 'add') {
				$this->toolbar_btn['save_and_stay'] = array(
					'href' => '#',
				'desc' => $this->l('儲存並繼續編輯')
			);
		}
		parent::initToolbar();
	}

	public function initProcess()
	{
		parent::initProcess();
		if($this->display == 'add'){
			unset($this->fields['form']['news']['input']['add_time']);
		}
		
		//todo :  上傳檔案
		$json = new JSON();
		$id_news = Tools::getValue('id_news');
		if(!empty($id_news)){
			$arr_id_file = News::getFile($id_news);
		}else{
			if(isset($_COOKIE['news_file'])){
				$arr_id_file = unserialize($_COOKIE['news_file']);
			}else{
				$arr_id_file = array();
			}
		}
		$arr_row = FileUpload::get($arr_id_file);	//取得檔案資訊
		foreach ($arr_row as $i => $v) {
			$url = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url = urldecode('//'.WEB_DNS.$url);
			$arr_initialPreview['annex'][] = $url;
			$file_type = est_to_type(ext($v['filename']));
			$arr_initialPreviewConfig['annex'][] = array(
				'type' => $file_type,
				'filetype' =>  $v['type'],
				'caption' => $v['filename'],
				'size' => $v['size'],
				'url' => '/manage/News?&viewnews&id_news=' . $id_news . '&id_file_type=' . $v['id_file_type'] . '&ajax=1&action=DelFile',
				'downloadUrl' => $url,
				'key' => $v['id_file'],
			);
		};
		
		foreach($arr_initialPreview as $i => $v){
			$arr_initialPreview[$i] = $json->encode($arr_initialPreview[$i]);
			$arr_initialPreviewConfig[$i] = $json->encode($arr_initialPreviewConfig[$i]);
		}

		$UpFileVal['annex'] = '\'' . $json->encode(array(
			'id_news' => $id_news,
		)) . '\'';

		$this->context->smarty->assign(array(
			'initialPreview' => $arr_initialPreview,
			'initialPreviewConfig' => $arr_initialPreviewConfig,
			'UpFileVal' => $UpFileVal,
		));
	}

	public function postProcess()
	{
		return parent::postProcess(); // TODO: Change the autogenerated stub
	}

	public function processAdd(){
		$this->validateRules();
		if(count($this->_errors)) return;
		$this->setFormTable();
		$num = $this->FormTable->insertDB();
		if($num){
			if(isset($_COOKIE['news_file'])){
				$news_file = unserialize($_COOKIE['news_file']);
				foreach($news_file as $key => $id_file){
					$sql = sprintf('INSERT INTO `news_file` (`id_news`, `id_file`) VALUES (%d, %d)',
						GetSQL($num, 'int'),
						GetSQL($id_file, 'int')
					);
					Db::rowSQL($sql);
				}
				//清空暫存
				Context::getContext()->cookie['news_file'] = null;
				setcookie('news_file', '', time() - 3600, '/');
			}

			if(Tools::isSubmit('submitAdd'.$this->table)) {
				$this->back_url = self::$currentIndex.'&conf=1';
			};
			if(Tools::isSubmit('submitAdd'.$this->table.'AndStay')) {
				if(in_array('edit', $this->post_processing)){
					$this->back_url = self::$currentIndex.'&view'.$this->table.'&conf=1&'.$this->index.'='.$this->FormTable->index_id;
				}else{
					$this->back_url = self::$currentIndex.'&edit'.$this->table.'&conf=1&'.$this->index.'='.$this->FormTable->index_id;
				}
			};
			if(Tools::isSubmit('submitAdd'.$this->table.'AndContinue')) {
				$this->back_url = self::$currentIndex.'&add'.$this->table.'&conf=1';
			};
			Tools::redirectLink($this->back_url);
		}else{
			$this->_errors[] = $this->l('沒有新增任何資料!');
		};
	}
	
	public function displayAjaxMoveFileAction(){	//檔案搬移
		return true;
	}
	
	public function displayAjaxDelFileAction(){	//檔案刪除
		
		return true;
	}
	
	public function displayAjaxUpFileAction($arr, $dir, $url, $UpFileVal){
		$json = new JSON();
		$UpFileVal = (array) $json->decode($UpFileVal);
		$arr_id_file = FileUpload::set($arr, $dir, $url, 0, $_SESSION['id_admin']);
		$id_news = $UpFileVal['id_news'];
		if(!empty($id_news)){		//"修改"上傳
			foreach($arr_id_file as $i => $id_file){
				//建案檔案
				$sql = sprintf('INSERT INTO `news_file` (`id_news`, `id_file`) VALUES (%d, %d)',
					GetSQL($id_news, 'int'),
					GetSQL($id_file, 'int')
				);
				Db::rowSQL($sql);
				if(!Db::getContext()->num) return false;
			}
		}else{					//"新增"上傳
			if(isset($_COOKIE['news_file'])){
				Context::getContext()->cookie['news_file'] = unserialize($_COOKIE['news_file']);
			}else{
				Context::getContext()->cookie['news_file'] = array();
			}
			foreach($arr_id_file as $i => $id_file){
				Context::getContext()->cookie['news_file'][] = $id_file;
			}
			setcookie('news_file', serialize(Context::getContext()->cookie['news_file']), time() + 3600, '/');
		}
		return true;
	}

}