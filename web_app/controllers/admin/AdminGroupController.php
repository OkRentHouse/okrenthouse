<?php
class AdminGroupController extends AdminController
{
	public function __construct(){
		$this->className = 'AdminGroupController';
		$this->table = 'group';
		if($_SESSION['id_group'] != 1) $this->fields['where'] = ' AND `id_group` != 1';
		
		if($_SESSION['id_group'] != 1){     //非最高系統管理者 隱藏
			$this->fields['where'] = ' AND `id_group` != 1';
			if($_SESSION['admin'] != 1){
				$this->fields['where'] .= ' AND `id_group` IN ('.implode(', ', GiveGroup::getGroup($_SESSION['id_group'])).')';
				$where = ' AND `id_web` = '.$_SESSION['id_web'];
			}
			$where = ' AND `id_group` != 1';
		}
		
		$this->fields['index'] = 'id_group';
		$this->fields['title'] = '管理者群組';
		$this->fields['list'] = array(
			'id_group' => array(
				'index' => true,
				'type' => 'checkbox',
				'select' => '',
				'hidden' => true,
				'class' => 'text-center'
			),
			'id_web' => array(
				'title' => $this->l('社區名稱'),
				'order' => true,
				'filter' => true,
				'key' => array(
					'table' => 'web',
					'key' => 'id_web',
					'val' => 'web',
					'where' => $where,	//todo
					'order' => '`web` ASC'
				),
				'class' => 'text-center'
			),
			'name'=> array(
				'title' => $this->l('群組名稱'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'));
		
		if($_SESSION['id_group'] == 1){
			$this->fields['list']['admin'] = array(
				'title' => $this->l('管理者'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center',
				'values' => array(
					array(
						'class' => 'yes',
						'value' => 1,
						'title' => $this->l('是')
					),
					array(
						'class' => 'no',
						'value' => 0,
						'title' => $this->l('否')
					)
				
				)
			);
		}
		
		$this->fields['list_num'] = 50;
		
		$type = 'text';
		if($_SESSION['id_group'] != 1 && !empty($id_group)){
			$sql = 'SELECT `id_group`
				FROM `group`
				WHERE `id_group` = '.$id_group.'
				AND `admin` != 1
				LIMIT 0, 1';
			$row = Db::rowSQL($sql, true);
			if(count($row)){
				$type = 'view';
			}
		}
		$this->fields['form'] = array(
			array(
				'legend' => array(
					'title' => $this->l('系統管理者'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'id_group',
						'type' => 'hidden',
						'index' => true,
						'required' => true
					),
					'id_web' => array(
						'name' => 'id_web',
						'type' => 'select',
						'label' => $this->l('社區名稱'),
						'options' => array(
							'default' => array(
								'text' => '公用',
								'val' => ''
							),
							'table' => 'web',
							'text' => 'web',
							'where' => $where,
							'value' => 'id_web',
							'order' => '`web` ASC'
						),
					),
					array(
						'type' => $type,
						'required' => true,
						'label' => $this->l('群組名稱'),
						'name' => 'name',
						'maxlength' => '20'
					)
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					)
				),
				'cancel' => array(
					'title' => $this->l('取消')
				),
				'reset' => array(
					'title' => $this->l('復原')
				)
			)
		);
		if($_SESSION['id_group'] == 1){
			$this->fields['form'][0]['input'][] = array(
				'type' => 'switch',
				'no_action' => false,
				'label' => $this->l('管理者'),
				'name' => 'admin',
				'val' => 0,
				'values' => array(
					array(
						'id' => 'yes',
						'label_class' => 'yes ',
						'value' => 1,
						'label' => $this->l('是')
					),
					array(
						'id' => 'no',
						'label_class' => 'no',
						'value' => 0,
						'label' => $this->l('否')
					)
				)
			);
		}
		parent::__construct();
	}
	
	public function processDel(){
		$id_group = Tools::getValue('id_group');
		if($id_group == 1) $this->_errors[] = $this->l('無法刪除最高管理者群組!');
		$sql = 'SELECT `id_admin`
			FROM `admin`
			WHERE `id_group` = '.$id_group.'
			LIMIT 0, 1';
		$row = Db::rowSQL($sql, true);
		if(count($row)) $this->_errors[] = $this->l('無法刪除正在使用的群組!');
		
		if($_SESSION['id_group'] != 1 && $_SESSION['admin']){
			$arr_id_group = GiveGroup::getGroup($_SESSION['id_group']);
			if(!in_array($id_group, $arr_id_group)) $this->_errors[] = $this->l('沒有刪除任何資料!');
		}
		parent::processDel();
	}
	/*
	public function initToolbar(){		//初始化功能按鈕
		if ($this->display == 'add' || $this->display == 'edit') {
			$this->toolbar_btn['save_and_stay'] = array(
				'href' => '#',
				'desc' => $this->l('儲存並繼續編輯')
			);
		}
		parent::initToolbar();
	}
	*/
}