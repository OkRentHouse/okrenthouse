<?php
class AdminWebController extends AdminController
{
	public $tpl_folder;	//樣版資料夾
	public function __construct(){
		$this->className = 'AdminWebController';
		$this->table = 'web';
		$this->fields['index'] = 'id_web';

		$this->fields['where'] = ' AND `id_web` > 0';
		$this->fields['order'] = ' ORDER BY `web` ASC';
		$this->fields['title'] = '社區管理';

		$this->fields['list'] = array(
			'id_web' => array(
				'index' => true,
				'type' => 'checkbox',
				'hidden' => true,
				'class' => 'text-center'
			),
			'build_case_code'=> array(
				'title' => $this->l('建案代號'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'web' => array(
				'title' => $this->l('社區名稱'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center mw130'
			),
			'building_type' => array(
				'title' => $this->l('大樓型態'),
				'order' => true,
				'class' => 'text-center',
				'multiple' => true,
				'values' => array(
					array(
						'class' => 'b_type_0',
						'value' => 0,
						'title' => $this->l('大樓')),
					array(
						'class' => 'b_type_1',
						'value' => 1,
						'title' => $this->l('透天')
					)
				),
				'filter' => true
			),
			'address' => array(
				'title' => $this->l('地址'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center mw130'
			),
			'active' => array(
				'title' => $this->l('啟用狀態'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			)
		);

		$this->fields['list_num'] = 50;

		$this->fields['form']['Web'] = array(
			'tab' => 'Web',
			'legend' => array(
				'title' => $this->l('社區管理'),
				'icon' => 'icon-cogs',
				'image' => ''
			),
			'input' => array(
				array(
					'name' => 'id_web',
					'type' => 'hidden',
					'index' => true,
					'required' => true
				),
				array(
					'name' => 'web',
					'label' => $this->l('社區名稱'),
					'type' => 'text',
					'maxlength' => 50,
					'required' => true
				),
				array(
					'name' => 'build_case_code',
					'type' => 'text',
					'required' => true,
					'unique' => true,
					'label' => $this->l('建案代號'),
					'hint' => $this->l('若更改建案代號,舊有流水編號不會變動,新的流水編號將以新的代號重新計算'),
					'maxlength' => '5'
				),
				array(
					'name' => 'building_type',
					'label' => $this->l('大樓型態'),
					'type' => 'switch',
					'values' => array(
						array(
							'id' => 'b_type_0',
							'label_class' => 'b_type_0',
							'value' => 0,
							'label' => $this->l('大樓')
						),
						array(
							'id' => 'b_type_1',
							'label_class' => 'b_type_1',
							'value' => 1,
							'label' => $this->l('透天')
						)
					),
					'required' => true
				),
				array(
					'name' => 'address',
					'label' => $this->l('地址'),
					'p' => $this->l('門牌號碼、樓層不必填寫'),
					'type' => 'text',
					'maxlength' => 20
				),
				array(
					'label' => $this->l('建案樓層'),
					'p' => $this->l('Ctrl+左鍵 可複選'),
					'name' => 'build_floor',
					'type' => 'select',
					'col' => '3',
					'size' => 10,
					'multiple' => true,
					'required' => true,
					'options' => array(
						'val' => fields_floor(Configuration::get('floor_f'), Configuration::get('floor_b'), Configuration::get('floor_rf')),
						'default' => array(
							'val' => 'XF',
							'text' => $this->l('XF')
						)
					)
				),
				array(
					'type' => 'switch',
					'label' => $this->l('啟用狀態'),
					'p' => $this->l('社區關閉前幾天請公佈關閉公告，告知使用者關閉訊息！'),
					'name' => 'active',
					'val' => 1,
					'values' => array(
						array(
							'id' => 'active_1',
							'value' => 1,
							'label' => $this->l('啟用')
						),
						array(
							'id' => 'active_0',
							'value' => 0,
							'label' => $this->l('關閉')
						)
					),
					'required' => true
				),
			),
			'submit' => array(
				array(
					'title' => $this->l('儲存')
				)
			),
			'cancel' => array(
				'title' => $this->l('取消')
			),
			'reset' => array(
				'title' => $this->l('重置')
			)
		);
		parent::__construct();
	}

	public function processDel(){
		$id_web = Tools::getValue('id_web');
		$sql = sprintf('SELECT `id_web`
									FROM `'.DB_PREFIX_.'houses`
									WHERE `id_web` = %d
									LIMIT 0, 1',
			GetSQL($id_web, 'int'));
		Db::rowSQL($sql);
		if(Db::getContext()->num()){
			$this->_errors[] = $this->l('無法刪除正在使用的建案');
		}
		parent::processDel();
	}
}