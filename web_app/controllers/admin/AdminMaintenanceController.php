<?php
class AdminMaintenanceController extends AdminController
{
	public $tpl_folder;	//樣版資料夾
	public $page = 'maintenance';
	public function __construct(){
		$this->className = 'AdminMaintenanceController';
		$this->fields['title'] = '網站維護';

		$this->fields['form'] = array(
			array(
				'tab' => 'test',
				'legend' => array(
					'title' => $this->l('網站維護'),
					'icon' => 'icon-cogs',
					'image' => ''
					),
				'input' => array(
					array(
						'name' => 'WEB_ENABLE',
						'type' => 'switch',
						'values' => array(
							array(
								'id' => 'web_enable_1',
								'value' => 1,
								'label' => $this->l('啟用')
							),
							array(
								'id' => 'web_enable_0',
								'value' => 0,
								'label' => $this->l('關閉')
							)
						),
						'hint' => $this->l('若關閉，網站將導入維修中頁面!'),
						'label' => $this->l('啟用網站'),
						'p' => $this->l('若關閉，網站將導入維修中頁面!'),
						'configuration' => true
					),
					array(
						'name' => 'MAINTENANCE_TEXT',
						'type' => 'text',
						'hint' => $this->l('若關閉，網站將導入維修中頁面!'),
						'label' => $this->l('維護頁面文字敘述'),
						'p' => $this->l('顯示在維護頁面中央的文字'),
						'configuration' => true
					),
					array(
						'name' => 'MAINTENANCE_FB',
						'type' => 'text',
						'label' => $this->l('維護頁面Facebook連結'),
						'configuration' => true
					),
					array(
						'name' => 'MAINTENANCE_EMAIL',
						'type' => 'text',
						'label' => $this->l('維護頁面Email連結'),
						'configuration' => true
					)
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存'))
					),
				'cancel' => array(
					'title' => $this->l('取消')),
				'reset' => array(
					'title' => $this->l('復原')
				)
			)
		);
		
		parent::__construct();
	}
	
	public function initContent(){
		$this->display = 'edit';
		parent::initContent();
	}
}