<?php
class AdminSEOController extends AdminController
{
	public $tpl_folder;	//樣版資料夾
	public $page = 'seo';
	public $ed;
	public function __construct(){
		$this->className = 'AdminSEOController';
		$this->fields['title'] = 'SEO';
		$this->table = 'seo';
		$this->fields['index'] = 'id_seo';
		$this->fields['list'] = array(
			'id_seo' => array(
				'index' => true,
				'hidden' => true,
				'type' => 'checkbox',
				'class' => 'text-center'
			),
			'page'=> array(
				'title' => $this->l('Page'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'url'=> array(
				'title' => $this->l('URL'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
			'title'=> array(
				'title' => $this->l('Title'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center'
			),
//			'description'=> array(
//				'title' => $this->l('Description'),
//				'order' => true,
//				'filter' => true,
//				'class' => 'text-center'
//			),
//			'keywords'=> array(
//				'title' => $this->l('Keywords'),
//				'order' => true,
//				'filter' => true,
//				'class' => 'text-center'
//			),
			'index_ation'=> array(
				'title' => $this->l('開啟索引'),
				'order' => true,
				'filter' => true,
				'class' => 'text-center',
				'values' => array(
					array(
						'class' => 'index_a_1',
						'value' => '1',
						'title' => $this->l('開啟')
					),
					array(
						'class' => 'index_a_0',
						'value' => '0',
						'title' => $this->l('關閉')
					)
				),
			),
		);
		
		$page_select = array();
		$arr_page = $this->getFrontPage();
		foreach ($arr_page as $item => $page) {
			$page_select[] = array(
				'val' => $page,
				'text' => $page
			);
		}
		$this->fields['form'] = array(
			array(
				'legend' => array(
					'title' => $this->l('SEO'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'id_seo',
						'type' => 'hidden',
						'index' => true,
						'required' => true
					),
					array(
						'name' => 'page',
						'type' => 'select',
						'options' => array(
							'val' => $page_select
						),
						'unique' => true,
						'required' => true,
						'maxlength' => 100,
						'label' => $this->l('Page'),
					),
					array(
						'name' => 'url',
						'type' => 'text',
						'required' => true,
						'maxlength' => 200,
						'label' => $this->l('URL'),
					),
					array(
						'name' => 'title',
						'type' => 'text',
						'maxlength' => 100,
						'label' => $this->l('Title'),
					),
					array(
						'name' => 'description',
						'type' => 'text',
						'maxlength' => 300,
						'label' => $this->l('Description'),
					),
					array(
						'name' => 'keywords',
						'type' => 'text',
						'maxlength' => 300,
						'data' => array('role' => 'tagsinput'),
						'label' => $this->l('Keywords'),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('開啟索引'),
						'name' => 'index_ation',
						'required' => true,
						'val' => 1,
						'values' => array(
							array(
								'id' => 'index_a_1',
								'value' => 1,
								'label' => $this->l('開啟')
							),
							array(
								'id' => 'index_a_0',
								'value' => 0,
								'label' => $this->l('關閉')
							)
						)
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					),
					array(
						'title' => $this->l('儲存並繼續編輯'),
						'stay' => true
					)
				),
				'cancel' => array(
					'title' => $this->l('取消')),
				'reset' => array(
					'title' => $this->l('重置')
				)
			)
		);
		
		parent::__construct();
	}
	
	public function validateRules(){
		parent::validateRules();
		$url = Tools::getValue('url');
		if(!isTwUrl($url)){
			$this->_errors[] = $this->l('網址符號只能用 _- 字元');
		}
	}
	
	public function getFrontPage(){
		$arr_page = array();
		$arr_file = File::getContext()->getFileList(CONTROLLER_DIR.DS.'front');
		foreach ($arr_file as $i => $file) {
			include_once(CONTROLLER_DIR.DS.'front'.DS.$file);
			$controller_name = str_replace('.php', '', $file);
			if(class_exists($controller_name, false)){
				$Controller = new $controller_name();
				if(!empty($Controller->page)){
					$page = $Controller->page;
					if(!empty($Controller->meta_title)){
						$page .= ' ('.$Controller->meta_title.')';
					}
					$arr_page[] =  $page;
				}
			}
		}
		return $arr_page;
	}
	
	public function setMedia(){
		$this->addJS('/'.MEDIA_URL.'/clipboard/js/clipboard.min.js');
		parent::setMedia();
		$this->addCSS('/'.MEDIA_URL.'/bootstrap-tagsinput/bootstrap-tagsinput.css');
		$this->addJS('/'.MEDIA_URL.'/bootstrap-tagsinput/bootstrap-tagsinput.js');
		$this->addJS('/'.MEDIA_URL.'/bootstrap-tagsinput/bootstrap-tagsinput-angular.js');
	}
}