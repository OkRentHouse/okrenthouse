<?php
class AdminFileCompetenceController extends AdminController
{
	public $page = 'websetup';
	public function __construct(){
		$this->className = 'AdminFileCompetenceController';
		$this->fields['title'] = '檔案下載權限';

		$this->fields['tabs'] = array(
			'file_competence' => $this->l('檔案下載權限'),
		);

		$this->fields['form'] = array(
			'file_competence' => array(
				'tab' => 'file_competence',
				'legend' => array(
					'title' => $this->l('檔案下載權限'),
					'icon' => 'icon-cogs',
					'image' => ''
				),
				'input' => array(
					array(
						'name' => 'file_competence',
						'label' => $this->l('檔案下載權限'),
						'type' => 'checkbox',
						'val' => 1,
						'values' => array(
							array(
								'id' => 'file_competence_1',
								'value' => 1,
								'label' => $this->l('訪客')
							),
							array(
								'id' => 'file_competence_2',
								'value' => 2,
								'label' => $this->l('會員')
							),
							array(
								'id' => 'file_competence_3',
								'value' => 3,
								'label' => $this->l('WEB')
							)
						),
						'configuration' => true
					),
				),
				'submit' => array(
					array(
						'title' => $this->l('儲存')
					)
				),
				'reset' => array(
					'title' => $this->l('復原')
				)
			),
		);

		parent::__construct();
	}

	public function initContent(){
		$this->display = 'options';
		parent::initContent();
	}
}