<?php


namespace web_app;

use \AppController;
use \Db;
use \File;
use \Tools;
class CollectPointsController extends AppController

{

    protected static $instance;

    public $_errors = [];

    public $_msgs = [];

    public $msg = '';

    public $page = 'CollectPointsController';


    public function __construct()

    {

        parent::__construct();

        $this->no_page = true;

        $this->className = 'CollectPointsController';

        $this->table = 'admin';

        $this->meta_title = $this->l('大集大利');

        $this->page_header_toolbar_title = $this->l('大集大利');

//        $this->fields['page']['cache'] = true;


    }


    public function initProcess()
    {
        parent::initProcess();

        $id = Tools::getValue('id');


        $this->context->smarty->assign([
            'id'   => $id,
        ]);


//        $sql = 'SELECT li.`id_in_life`, li.`title`, li.`title2`, li.`date_s`, li.`date_e`, li.`time_s`, li.`time_e`,
//                 li.`weekday`, li.`content`,ilf.`id_file`
//                        FROM `in_life` li
//                        LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
//                        WHERE ilf.`file_type` = 3
//                        AND active = 1
//                        ORDER BY top DESC,date DESC ';
//
//        $row = Db::rowSQL($sql);
//
//        $as_arr = [];
//        foreach($row as $i => $v){
//            $file = File::get($v['id_file']);
//            $as_arr[] = [
//                'id_in_life'=>$v['id_in_life'],
//                'title1' => $v['title'],
//                'title2' => $v['title2'],
//                'img' => $file['url'],
//            ];
//        }
//
//        $this->context->smarty->assign([
//            'as_arr' => $as_arr,
//        ]);


    }

    public function setMedia()
    {
        $id = Tools::getValue('id');
        if(!empty($id)){
            $this->addFooterCSS(THEME_URL . '/css/collectpoints2.css');
            $this->removeJS(THEME_URL . '/css/collectpoints.css');
        }
        parent::setMedia();

    }

}