<?php
namespace web_app;
use \Tools;
use \Context;
use \Member;
use \AppController;

class LoginController extends AppController
{
	public $page = 'login';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title              = $this->l('會員登入');
		$this->className               = 'LoginController';
		$this->display_main_menu       = false;
		parent::__construct();
        $this->display_header = false;
        $this->fields['form'] = [
            'member' => [
                'input' => [
                    'user'     => [
                        'name'        => 'user',
                        'type'        => 'text',
                        'label'       => $this->l('帳號'),
                        'placeholder' => $this->l('帳號'),
                        'maxlength'   => '100',
                        'required'    => true,
                    ],
                    'password' => [
                        'type'        => 'new-password',
                        'label'       => $this->l('密碼'),
                        'placeholder' => $this->l('密碼'),
                        'required'    => true,
                        'minlength'   => '6',
                        'maxlength'   => '20',
                        'name'        => 'password',
                    ],
                ],
            ],
        ];
	}

    public function postProcess()
    {
        $is_login    = Tools::isSubmit('is_login');
        $user        = Tools::getValue('user');
        $password    = Tools::getValue('password');
        $auto_loging = Tools::getValue('auto_login');
        if (!empty($is_login)) {
            $this->validateRules();
            if (count($this->_errors) == 0) {
                if (!Member::login($user, $password, $auto_loging)) {
                    $this->_errors[] = $this->l('帳號密碼錯誤!');
                } else {
                    $this->login();
                    exit;
                }
            }
        }
    }

//	public function initProcess()
//	{
//		$this->fields['page']['class'] = 'bg_store';
//		$Context              = Context::getContext();
//		$Context->smarty->assign([
//			'fields'   => $this->fields,
//		]);
//		parent::initProcess();
//	}

    public function login()
    {
        $page = Tools::getValue('page');
        if (empty($page)) {
            Tools::redirectLink('//' . WEB_DNS);
        } else {
            Tools::redirectLink('//' . WEB_DNS . base64_decode(Tools::getValue('page')));
        }
        exit;
    }
}