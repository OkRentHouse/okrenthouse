<?php

namespace web_app;
use \AppController;
use \Config;
use \Db;
use \FileUpload;
use \Tools;
use \County;
use \File;
use \ListUse;
use \RentHouse;

class ListController extends AppController
{

    public $page = 'List';
    public $tpl_folder;    //樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct()
    {
        parent::__construct();
        $this->className = 'ListController';
        $this->meta_title = $this->l('進階搜索');
        $this->page_header_toolbar_title = $this->l('進階搜索');
        $this->table = 'rent_house';//主資料表
    }

    public function initProcess()
    {
        $active = Tools::getValue("active");//active 1等於執行搜索
        $special = Tools::getValue("special");//1是住宅精選 2是商用精選
        $house_choose = Tools::getValue("house_choose");//0是租 1是售
        $id_main_house_class = Tools::getValue("id_main_house_class");//住宅商用其他
        $county = Tools::getValue("county");//縣直轄市
        $city = Tools::getValue("city");//鄉鎮市的陣列
        $id_type = Tools::getValue("id_type");//型態
        $id_types = Tools::getValue("id_types");//類別
        $id_device = Tools::getValue("id_device");//設備的陣列
        $id_other = Tools::getValue("id_other");//其他條件的陣列
        $max_price = Tools::getValue("max_price");//最高金額
        $min_price = Tools::getValue("min_price");//最低金額
        $max_ping = Tools::getValue("max_ping");//最高坪數
        $min_ping = Tools::getValue("min_ping");//最低坪數
        $max_room = Tools::getValue("max_room");//最多房數
        $min_room = Tools::getValue("min_room");//最低房數
        $id_device_class = Tools::getValue("id_device_class");//設備主類別的陣列
        $search = Tools::getValue("search");//鄉鎮市的陣列

        $id_member = $_SESSION["id_member"];//favorite要用的
        $session_id = session_id();//favorite要用的
        $favorite_data = "";//favorite要用的

        if ($active == 1) {//搜尋後
            $limit = "0,5";
            $city_arr = $city != '' ? explode(',', $city) : null;
            $id_type_arr = $city != '' ? explode(',', $id_type) : null;
            $id_types_arr = $city != '' ? explode(',', $id_types) : null;
            $id_other_arr = $city != '' ? explode(',', $id_other) : null;
            //搜索
            $arr_house = ListUse::getContext()->get(null, 1, 0, $county, $city_arr,
                $id_type_arr, $id_types_arr, $id_device, $id_device_class, $id_other_arr, $max_price, $min_price, $max_ping, $min_ping,
                $max_room, $min_room, $search, $house_choose, $special, $limit);

            if (!empty($id_member)) {//有
                $sql = "SELECT * FROM `favorite` WHERE table_name=" . GetSQL($this->table, "text") . " AND is_member=" . GetSQL($id_member, "int");
                $favorite_data = Db::rowSQL($sql);//求出裡面有幾個is_member
            } else {//只有session_id
                $sql = "SELECT * FROM `favorite` WHERE table_name=" . GetSQL($this->table, "text") . " AND session_id=" . GetSQL($session_id, "text");
                $favorite_data = Db::rowSQL($sql);//求出裡面有幾個session_id
            }

            foreach ($arr_house as $i => $v) {
                //物件型態
                $sql = "SELECT * FROM rent_house_types WHERE id_rent_house_types IN (" . Db::antonym_array($arr_house[$i]['id_rent_house_types']) . ")";
                $sql_types = Db::rowSQL($sql, false);
                $arr_house[$i]['sql_types'] = $sql_types;//整批搜索到的丟進去

                //物件類別
                $sql = "SELECT * FROM rent_house_type WHERE id_rent_house_type IN (" . Db::antonym_array($arr_house[$i]['id_rent_house_type']) . ")";
                $sql_type = Db::rowSQL($sql, false);
                $arr_house[$i]['sql_type'] = $sql_type;//整批搜索到的丟進去

                //我的最愛start
                foreach ($favorite_data as $key => $value) {
                    if ($arr_house[$i]["id_rent_house"] == $favorite_data[$key]["table_id"]) {//上面搜索了該table與本次使用者的資訊 這邊濾同id
                        $arr_house[$i]["favorite"] = $favorite_data[$key]["active"];
                    }
                }
                //end

                //這邊要做處理圖片 LIST 只處理版面 所以讀版面
                $sql = "SELECT * FROM rent_house_file WHERE file_type=0 AND id_rent_house=" . $v['id_rent_house'] . ' ORDER BY id_rhf ASC ';
                $sql_url = Db::rowSQL($sql, true);
                $row = File::get($sql_url['id_file']);
                $arr_house[$i]['img'] = $row['url'];
                //圖片end

                if ($arr_house[$i]['id_web'] == '1') {//改名
                    $arr_house[$i]['w_title'] = "藝文直營店";//改名
                }

                //處理 rent_house_types的資料
                $sql = "SELECT * FROM rent_house_types WHERE id_rent_house_types IN (" . Db::antonym_array($arr_house[$i]['id_rent_house_types']) . ")";
//            echo $sql.'</br>';
                $sql_types = Db::rowSQL($sql, false);
                $arr_house[$i]['sql_types'] = $sql_types;//整批搜索到的丟進去

            }
            $breadcrumb = '';//頭的部分
//        print_r($arr_house);
        }

//        下面這段是搜尋的
        $house_choose = [
            [
                'title' => '租屋',
                'value' => '0'
            ],
            [
                'title' => '售屋',
                'value' => '1'
            ]
        ];


        $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
        $id_main_house_class = Db::rowSQL($sql);

        $json_county = json_encode(County::getContext()->form_list_county(), JSON_UNESCAPED_UNICODE);
        $json_city = json_encode(County::getContext()->form_list_city(), JSON_UNESCAPED_UNICODE);
        $select_types = RentHouse::getContext()->getTypes();//要處理一下資料

        foreach ($select_types as $key => $value) {
            $select_types[$key]['id_main_house_class'] = Db::antonym_array($value['id_main_house_class'], true);
        }

        //這邊要做js變更id_main_house_class的child部分
        $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
        $main_house_class = Db::rowSQL($sql);
        $rent_house_types = [];
        $rent_house_type = [];

        foreach($main_house_class as $k => $v){
            $sql = "SELECT * FROM rent_house_types WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%'
            Order by position ASC";
            $rent_house_types[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
            $sql = "SELECT * FROM rent_house_type WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%".GetSQL($main_house_class[$k]["id_main_house_class"],"int")."%'
            Order by position ASC";
            $rent_house_type[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
        }

        $json_rent_house_types = json_encode($rent_house_types,JSON_UNESCAPED_UNICODE);
        $json_rent_house_type = json_encode($rent_house_type,JSON_UNESCAPED_UNICODE);



        $js = <<<js
    $( document ).ready(function() {
        $("input[name='id_types']").val("18");
        $("input[name='id_type']").val("1");
        $("input[name='id_main_house_class']").val("1");
        $("#id_main_house_class_0").addClass("buttons_checkes");
});



function call_reset_main_data(){//處理點選住家那區塊後的動作
        var level_id_types = {$json_rent_house_types};
        var level_id_type = {$json_rent_house_type};
        var id_main_house_class = $("input[name='id_main_house_class']").val();
        var id_main_house_class_arr ='';

        new_id_types_html='<label id="types_label" onclick="check_label_types();"><!-- 請選擇<i class="fas fa-plus-square"></i> --></label>';//寫入用 id_types
        new_id_type_html ='<label id="type_label" onclick="check_label_type();"><!-- 請選擇<i class="fas fa-plus-square"></i> --></label>';//寫入用 id_type
        copy_id_types_html = new_id_types_html+'<div class="select_box_bk"></div><div class="select_box">';
        copy_id_type_html = new_id_type_html+'<div class="select_box_bk"></div><div class="select_box">';
        //下面兩條用來紀錄是否有重複資料 例如:型態在用途上有重疊
        id_types_data_num = 0;
        id_type_data_num = 0;
        id_types_data_num_new = 0;
        id_type_data_num_new = 0;
        //下面跑兩次一次確認數量一次(因為要弄ul的問題在)正式給值

            if(id_main_house_class == '' || id_main_house_class=="undefined"){//沒選擇==全選
                id_main_house_class = "1,2,3";
            }
                id_main_house_class_arr = id_main_house_class.split(",");
            //型態

        $.each(id_main_house_class_arr,function(n,value){
              $.each(level_id_types,function(types_n,types_value){
                  if(value !=types_n) return;
                  $.each(types_value,function(types_value_n,types_value_value){
                      if(types_value_value["id_main_house_class"].search(value) =="-1" || ( (value=='2' && id_main_house_class.search("1")!='-1') &&  (types_value_value["id_rent_house_types"]==18 || types_value_value["id_rent_house_types"]==5 || types_value_value["id_rent_house_types"]==1))) return; //不包含並解奘態為2但有1 排除電梯大樓 公寓 透天
                        id_types_data_num = id_types_data_num+1;
                    });
                });

              $.each(level_id_type,function(type_n,type_value){
                  $.each(type_value,function(type_value_n,type_value_value){
                    if(value.search(type_value_value["id_main_house_class"])) return;
                        id_type_data_num = id_type_data_num+1;
                    });
                });
         });
         copy_id_types_html= copy_id_types_html+'<ul class="buttons" style="display: none;">';
         copy_id_type_html= copy_id_type_html+'<ul class="buttons" style="display: none;">';

         $.each(id_main_house_class_arr,function(n,value){
             $.each(level_id_types,function(types_n,types_value){
                  if(value !=types_n) return;
                  $.each(types_value,function(types_value_n,types_value_value){
                      if(types_value_value["id_main_house_class"].search(value) =="-1" || ( (value=='2' && id_main_house_class.search("1")!='-1') &&  (types_value_value["id_rent_house_types"]==18 || types_value_value["id_rent_house_types"]==5 || types_value_value["id_rent_house_types"]==1))) return; //不包含並解奘態為2但有1 排除電梯大樓 公寓 透天
                        //if(id_types_data_num_new==0 || id_types_data_num_new%4==0) copy_id_types_html= copy_id_types_html+'<ul class="buttons" style="display: none;">';
                            copy_id_types_html= copy_id_types_html+'<li name="id_types_a[]" id="id_types_'+id_types_data_num_new+'" value="'+types_value_value["id_rent_house_types"]+'" onclick="click_li(this);" >'+types_value_value["title"]+'</li>';
                            id_types_data_num_new = id_types_data_num_new+1;
                        //if(id_types_data_num_new %4==0 || id_types_data_num_new==id_types_data_num) copy_id_types_html= copy_id_types_html+'</ul>';
                    });
                });
              $.each(level_id_type,function(types_n,type_value){
                  $.each(type_value,function(type_value_n,type_value_value){
                        if(value.search(type_value_value["id_main_house_class"])) return;
                        //if(id_type_data_num_new==0 || id_type_data_num_new%4==0) copy_id_type_html= copy_id_type_html+'<ul class="buttons" style="display: none;">';
                            copy_id_type_html= copy_id_type_html+'<li name="id_type_a[]" id="id_type_'+id_type_data_num_new+'" value="'+type_value_value["id_rent_house_type"]+'" onclick="click_li(this);" >'+type_value_value["title"]+'</li>';
                            id_type_data_num_new = id_type_data_num_new+1;
                        //if(id_type_data_num_new %4==0 || id_type_data_num_new==id_type_data_num) copy_id_type_html= copy_id_type_html+'</ul>';
                    });
                });
         });

         copy_id_types_html= copy_id_types_html+'</ul>';
         copy_id_type_html= copy_id_type_html+'</ul>';

         $(".b_div_id_types").empty();
         $(".b_div_id_types").append(copy_id_types_html+'<div class="close_window">OK</div><div class="close_window_x">OK</div></div>');

         $(".b_div_id_type").empty();
         $(".b_div_id_type").append(copy_id_type_html+'<div class="close_window">OK</div><div class="close_window_x">OK</div></div>');
            // $.each(level_id_type,function(n,value){
            //        if(level_data[level_data_k]!=n) return;
            //     $.each(value,function(n,value_value){
            //
            //     });
            // });
	}

    function get_county(){
    types_label_c(this);
    var level_id_county = {$json_county};
    e = $(".contect_1_1");
    e_2=$(".contect_1_2");

    // if($(e).css("display") == "none"){
    //     $(e).show(300);
    // }else{
    //     $(e).hide(500);
    //     $(e_2).hide(500);
    // }

   if($(".contect_1_1 .b_div").html().trim()==''){
       var county_data = '';
       $.each(level_id_county,function(n,value){
           if(n%4==0){
               if(n !='0'){
                  county_data += '</ul>';
               }
               county_data += '<ul class="buttons">';
           }
           county_data += '<li name="county_a[]" id="county_'+value["value"]+'" value="'+value["value"]+'" onclick="get_city(this);">'+value["title"]+'</li>';
           if((n-1)==level_id_county.length){
                county_data += '</ul>';
           }
        });
       $(".contect_1_1 .b_div").append(county_data);
   }
}

    function get_city(data){
        var level_id_city = {$json_city};
        var data_name = data.getAttribute("name");
        var data_value = data.getAttribute("value");
        var input_from = data_name.slice(0,-4);
        var data_class = data.getAttribute("class");
        var temp_input_val = $("input[name='"+input_from+"']").val();//取得佔存點為了如果重複點選需要做判斷

        $("li[name='"+data_name+"']").removeClass("buttons_checkes");
        $(data).addClass("buttons_checkes");
        $("input[name='"+input_from+"']").val(data_value);
        if(temp_input_val!=data_value){//點選同樣的則不動作否則
            $(".contect_1_2 .b_div").empty();//清空
            $("input[name='city']").val("");//重置
            var city_data = '';
            var ul_after = 0;
            var citys_txt ='';
            $.each(level_id_city,function(n,value){
                if(value["parent"]==data_value){
                    citys_txt = data_value;
                    if(ul_after==0) city_data += '<ul class="buttons">';
                    if(ul_after!=0 && ul_after%4==0) city_data += '<ul class="buttons">';
                    city_data += '<li name="city_a[]" data-parent="'+value["parent"]+'" id="city_'+value["value"]+'" value="'+value["value"]+'" onclick="click_li(this);">'+value["title"]+'</li>';
                    ul_after++;
                    if(ul_after!=0 && ul_after%4==0) city_data += '</ul>';
                }
            });
            $("#citys_txt").html(citys_txt);
            if(city_data.slice(-5)!='</ul>') city_data += '</ul>';
            $(".contect_1_2 .b_div").append(city_data);//存入資料
        }

        types_label_c(data);
        close_window_select_box_bk(data);
        $(".contect_1_2").show();
    }

js;

        if (Tools::getValue("switch") == 1) {
            $sql = "SELECT * FROM main_house_class";//求得主main的住家 商用 其他
            $main_house_class = Db::rowSQL($sql);
            $rent_house_types = [];
            foreach ($main_house_class as $k => $v) {
                $sql = "SELECT * FROM rent_house_types WHERE `active`=1 AND `show`=1 AND `id_main_house_class` LIKE '%" . GetSQL($main_house_class[$k]["id_main_house_class"], "int") . "%' Order by position ASC";
                $rent_house_types[$main_house_class[$k]["id_main_house_class"]] = Db::rowSQL($sql);
            }
            $json_house_choose = json_encode($house_choose, JSON_UNESCAPED_UNICODE);
            $json_rent_house_types = json_encode($rent_house_types, JSON_UNESCAPED_UNICODE);
            $json_county = json_encode(County::getContext()->form_list_county(), JSON_UNESCAPED_UNICODE);
            $json_city = json_encode(County::getContext()->form_list_city(), JSON_UNESCAPED_UNICODE);
            //頭的
            $menu_buoy_switch = [
                [
                    'title' => '房屋',
                    'a' => '##',
                    'data_a' => '/list?active=1&id_main_house_class=1',
                    'data_class' => '1',
                    'onclick' => 'change_img(this);',
                    'active' => '1',
                ],
                [
                    'title' => '商用',
                    'a' => '##',
                    'data_a' => '/list?active=1&id_main_house_class=2',
                    'data_class' => '2',
                    'onclick' => 'change_img(this);',
                ],
                [
                    'title' => '土地',
                    'a' => '/list?active=1&id_types=13',
                    'data_a' => '/list?active=1&id_types=13',
                    'data_class' => '3',
                ],
                [
                    'title' => '頂讓',
                    'a' => '/list?active=1&id_types=25',
                    'data_a' => '/list?active=1&id_types=25',
                    'data_class' => '4',
                ],
            ];

            $img_data = [
                [
                    'title' => '溫馨居家〜2-3房',
                    'img' => '\themes\App\mobile\img\item-1.jpg',
                    'class' => 'img_w img_w_1',
                    'data_a' => '&min_room=2&max_room=3',
                    'a' => '/list?active=1&id_main_house_class=1&min_room=2&max_room=3',
                    'name' => 'change_name',
                ],
                [
                    'title' => '單身美學〜套房',
                    'img' => '\themes\App\mobile\img\item-2.jpg',
                    'class' => 'img_w img_w_1',
                    'data_a' => '&id_type=3',
                    'a' => '/list?active=1&id_main_house_class=1&id_type=2,3',
                    'name' => 'change_name',
                ],
                [
                    'title' => '大器美宅〜4+房',
                    'img' => '\themes\App\mobile\img\item-3.jpg',
                    'class' => 'img_w img_w_1',
                    'data_a' => '&min_room=4&max_room=6',
                    'a' => '/list?active=1&id_main_house_class=1&min_room=4&max_room=6',
                    'name' => 'change_name',
                ],
                [
                    'title' => '獨享天地〜透天/別墅',
                    'img' => '\themes\App\mobile\img\item-4.jpg',
                    'class' => 'img_w img_w_1',
                    'data_a' => '&min_room=4&max_room=6',
                    'a' => '/list?active=1&id_main_house_class=1&min_room=4&max_room=6',
                    'name' => 'change_name',
                ],
                [
                    'title' => '店面店辦',
                    'img' => '\themes\App\mobile\img\item-4.jpg',
                    'class' => 'img_w img_w_2',
                    'data_a' => '&id_type=17,15',
                    'a' => '/list?active=1&id_main_house_class=2&id_type=17,15',
                    'hidden' => 'true',
                    'name' => 'change_name',
                ],
                [
                    'title' => '廠房倉庫',
                    'img' => '\themes\App\mobile\img\item-5.jpg',
                    'class' => 'img_w img_w_2',
                    'data_a' => '&id_type=18,19',
                    'a' => '/list?active=1&id_main_house_class=2&id_type=18,19',
                    'hidden' => 'true',
                    'name' => 'change_name',
                ],
                [
                    'title' => '辦公室',
                    'img' => '\themes\App\mobile\img\item-6.jpg',
                    'class' => 'img_w img_w_2',
                    'data_a' => '&id_type=16',
                    'a' => '/list?active=1&id_main_house_class=2&id_type=16',
                    'hidden' => 'true',
                    'name' => 'change_name',
                ],
            ];

            $this->context->smarty->assign([
                'menu_buoy_switch' => $menu_buoy_switch,
                'img_data' => $img_data,
            ]);

            $js = <<<js
<script>
    $(".ui-link-house_choose-txt").click(
			function(){
			    var value = $("input[name='house_choose']").val();
				var main_item = "house_choose";
				$(".ui-link-"+main_item).css("display","block");
				if($("[id*='"+main_item+"']").length > 0){
				    list_each(main_item,value);
				}
			});

    $(".ui-link-house_choose").scroll(function (){
		$.each($(".house_chooses"),function(index,c){
		    citys(c,index)
		});
});

        $(".ui-link-types-txt").click(
			function(){
			        var value = $("input[name='id_types']").val();
                    var main_item = "types";
                    $(".ui-link-"+main_item).css("display","block");
                    if($("[id*='"+main_item+"']").length > 0){
                    list_each(main_item,value);
				}
			});

    $(".ui-link-types").scroll(function (){
		$.each($(".typess"),function(index,c){
		    citys(c,index)
		});
});

    $(".ui-link-county-txt").click(
        function(){
            var value = $("input[name='county']").val();
            var main_item = "county";
            $(".ui-link-"+main_item).css("display","block");
            if($("[id*='"+main_item+"']").length > 0){
				list_each(main_item,value);
            }
        });

    $(".ui-link-county").scroll(function (){
		$.each($(".countys"),function(index,c){
		    citys(c,index)
		});
	});

        $(document).ready(function(){
        var level_house_choose = {$json_house_choose};
        var level_id_types = {$json_rent_house_types};
        var level_id_county = {$json_county};
        var level_id_city = {$json_city};

        create_list("house_choose",level_house_choose,$("input[name='house_choose']").val());
        create_list("types",level_id_types,$("input[name='id_types']").val());
        create_list("county",level_id_county,$("input[name='county']").val());

});
    function city_data(){
        var value = $("input[name='city']").val();
        var level_id_city = {$json_city};//city的data
            //這邊才建立city的data
            create_list("city",level_id_city,value);
            var main_item = "city";
            $(".ui-link-city").css("display","block");
            $(".ui-link-city_b").css("display","block");
            if($("[id*='"+main_item+"']").length > 0){
				list_each(main_item,value);
            }
    }

    $(".ui-link-city").scroll(function (){
		$.each($(".citys"),function(index,c){
		    citys(c,index)
		});
});



</script>

js;
        }


        $this->context->smarty->assign([
            'house_choose' => $house_choose,
            'id_main_house_class' => $id_main_house_class,
            'select_type' => RentHouse::getContext()->getType(),
            'select_types' => $select_types,
            'select_device_category_class' => RentHouse::getContext()->getDeviceCategoryClass(),
            'select_other_conditions' => RentHouse::getContext()->getOtherConditionsy(),
            'min_price' => 10000,
            'max_price' => 51000,
            'min_room' => 0,
            'max_room' => 6,
            'min_ping' => 10,
            'max_ping' => 50,
            'js' => $js,
            'arr_house' => $arr_house,
        ]);
        parent::initProcess();
    }

    public function displayAjaxGetData(){
        $action = Tools::getValue('action');
        $num = Tools::getValue('num');

        switch($action){
            case 'GetData':

                echo json_encode(array("error"=>"","return"=>""));
            break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
            break;
        }

        exit;
}

    public function setMedia(){
//        $active = Tools::getValue("active");//active 1等於執行搜索
        parent::setMedia();
        if(Tools::getValue("switch")==1){
            $this->addCSS(THEME_URL . '/css/search_house.css');
            $this->addCSS( THEME_URL .'/css/switch.css');
            $this->addJS( THEME_URL .'/js/switch.js');
        }else{
            $this->addCSS(THEME_URL . '/css/search_list.css');
            $this->addJS(THEME_URL . '/js/search_list.js');
            $this->addCSS( '/css/metro-all.css');
            $this->addCSS( '/css/fontawesome.min.css');
            $this->addJS( '/js/metro.min.js');
            $this->addJS( '/js/fontawesome.min.js');
            $this->addJS( '/js/solid.min.js');
            $this->addJS( '/js/regular.min.js');

        }
    }
}
