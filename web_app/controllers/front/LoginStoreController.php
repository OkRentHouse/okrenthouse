<?php
namespace web_app;
use \Tools;
use \Context;
use \Member;
use \AppController;

class LoginStoreController extends AppController
{
	public $page = 'login_store';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title              = $this->l('會員登入');
		$this->className               = 'LoginStoreController';
		$this->display_main_menu       = false;
		parent::__construct();
		$this->fields['form'] = [
			'member' => [
				'input' => [
					'email'    => [
						'name'        => 'email',
						'type'        => 'email',
						'label'       => $this->l('帳號'),
						'placeholder' => $this->l('帳號'),
						'maxlength'   => '100',
						'required'    => true,
					],
					'password' => [
						'type'        => 'new-password',
						'label'       => $this->l('密碼'),
						'placeholder' => $this->l('密碼'),
						'required'    => true,
						'minlength'   => '8',
						'maxlength'   => '20',
						'name'        => 'password',
					],
				],
			],
		];
	}

	public function postProcess()
	{
		$login       = Tools::getValue('login');
		$email       = Tools::getValue('email');
		$password    = Tools::getValue('password');
		$auto_loging = Tools::getValue('auto_loging');
		if (!empty($login)) {
			$this->validateRules();
			if (count($this->_errors) == 0) {
				if (!Member::login($email, $password, $auto_loging)) {
					$this->_errors[] = $this->l('帳號密碼錯誤!');
				} else {
					Tools::redirectLink('/home');
					exit;
				}
			}
		}
	}

	public function initProcess()
	{
		$this->fields['page']['class'] = 'bg_store';
		$Context              = Context::getContext();
		$Context->smarty->assign([
			'fields'   => $this->fields,
		]);
		parent::initProcess();
	}
}