<?php
namespace web_app;

class StoreLoginController
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';

	public function __construct()
	{

	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new StoreLoginController();
		};
		return self::$instance;
	}

}