<?php

namespace web_app;
use \Store;
use \Tools;
use \Popularity;
use \AppController;
use \Db;

class StoreController extends AppController
{
	public $page = 'store';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		parent::__construct();
		$this->className                 = 'StoreController';
		$this->meta_title                = $this->l('商家');
		$this->page_header_toolbar_title = $this->l('商家');
        $this->table = "store";
	}

	public function initProcess()
	{
		parent::initProcess();

		$id = Tools::getValue('id');
        $id_member = $_SESSION["id_member"];//favorite要用的
        $session_id = session_id();//favorite要用的
        $favorite_data = [];//favorite要用的

		Store::getContext()->addView($id);


		$store                = Store::getContext()->get($id, 1, 1);



        $store = $store["0"];//因為這種搜索指會搜索一次
        //最愛start
        if(!empty($id_member)){//有
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND is_member=".GetSQL($id_member,"int");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個is_member
        }else{//只有session_id
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND session_id=".GetSQL($session_id,"text");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個session_id
        }
        foreach ($favorite_data as $key => $value) {
            if ($store["id_store"] == $favorite_data[$key]["table_id"]) {//上面搜索了該table與本次使用者的資訊 這邊濾同id
                $store["favorite"] = $favorite_data[$key]["active"];
            }
        }
        //end

		$store['tel']         = array_filter([
			$store['tel1'],
			$store['tel2'],
		]);
		$store['address1']    = [$store['address1']];
		$store['open_time_w'] = array_filter(explode(',', $store['open_time_w']));
		$store['close_time']  = array_filter(explode(',', $store['close_time']));
		$store['time']        = array_merge($store['open_time_w'], $store['close_time']);
		$store['service']     = array_filter(explode(',', $store['service']));
		$store['discount']    = array_filter(explode(',', $store['discount']));

        $arr_popularity      = Store::getContext()->get_popularity_val($id);
        $store['popularity'] = Popularity::getPopularityImg($store['id_store'], max(0, $arr_popularity['all']));

//		$arr_popularity      = Store::getContext()->get_popularity_val($store['id_store']);
//		$store['popularity'] = Popularity::getPopularityImg($store['id_store'], max(0, $arr_popularity['all']));

//		$row               = Store::getContext()->get_popularity($store['id_store'], $_SESSION['id_member']);

//		$my_star           = max($row[0]['popularity']);
//		$store['star_img'] = Popularity::getPopularityImg($store['id_store'], max(0, $my_star));






//        $sql = "SELECT * FROM `store_popularity` WHERE id_store=".GetSQL($id,"int")." AND id_member=".GetSQL($_SESSION["id_member"],"int");
//        $store_popularity_data = Db::rowSQL($sql,true);
//
//        $my_star           = $store_popularity_data["popularity"];
//        $store["star"] = $my_star;
//        $store['star_img'] = Popularity::getPopularityImg($store['id_store'], max(0, $my_star));
//

		$arr_img = array_merge(Store::getContext()->get_store_file_url($store['id_store'], 0), Store::getContext()->get_store_file_url($store['id_store']));

		$store['ratio'] = '64:33';

		$store['img'] = $arr_img;
		$this->context->smarty->assign([
			'store' => $store,
		]);

	}
}