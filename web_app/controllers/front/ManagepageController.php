<?php
namespace web_app;
use \AppController;

class ManagepageController extends AppController
{
    public $page = 'managepage';

    public $tpl_folder;	//樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct(){
        parent::__construct();
        $this->className = 'ManagepageController';
        $this->meta_title = $this->l('管理查詢');
        $this->page_header_toolbar_title = $this->l('管理查詢');
    }

    public function initProcess()
    {
        parent::initProcess();

        $menu_buoy = [
            [
                'title'=>'管理查詢',
                'active' => ['/managepage'],
                'url' =>'/managepage'
            ],
            [
                'title'=>'託租代管',
                'active' => ['/handled'],
                'url' =>'/handled'
            ],
            [
                'title'=>'租事順利',
                'active' => ['/tax_benefit'],
                'url' =>'/tax_benefit'
            ],
        ];

        $this->context->smarty->assign([
            'menu_buoy'       => $menu_buoy,
        ]);
    }

    public function setMedia(){

        parent::setMedia();
            $this->addCSS( '/css/metro-all.css');
            $this->addCSS( '/css/fontawesome.min.css');
            $this->addJS( '/js/metro.min.js');
            $this->addJS( '/js/fontawesome.min.js');
            $this->addJS( '/js/solid.min.js');
            $this->addJS( '/js/regular.min.js');
            $this->addCSS( THEME_URL .'/css/list.css');
    }
}