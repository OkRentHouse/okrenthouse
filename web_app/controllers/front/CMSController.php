<?php
namespace web_app;
use \CMS;
use \Context;
use \AppController;
class CMSController extends AppController
{
	public $tpl_folder;    //樣版資料夾
	public $page = 'cms';
	public $html = '';
	public $css = '';
	public $js = '';

	public $arr_my_id_web = [0];
	public $arr_my_id_houses = [];

	public function __construct()
	{
		$this->className = 'CMSController';

		$this->fields['page']['cache'] = false;
		$this->display_edit_div        = false;

		parent::__construct();
		//1:APP, 2:house, 3: lifegroup, 4:rent, 5:repair
		$cms                             = CMS::getByUrl(1, CMS::getContext()->url);
		$this->meta_title                = $cms['title'];
		$this->page_header_toolbar_title = $cms['page_title'];
		$this->meta_description          = $cms['description'];
		$this->meta_keywords             = $cms['keywords'];
		$this->nobots                    = !$cms['index_ation'];
		$this->nofollow                  = !$cms['index_ation'];
		$this->display_header            = $cms['display_header'];
		$this->display_footer            = $cms['display_footer'];
		$this->html                      = $cms['html'];
		$this->css                       = $cms['css'];
		$this->js                        = $cms['js'];

		$this->no_link = false;
	}

	public function initToolbar()
	{
		parent::initToolbar();
		$this->back_url = '#';
	}

	public function initProcess()
	{
		parent::initProcess();

		$arr = $_SESSION['arr_house'][$_SESSION['house_index']];

		foreach ($_SESSION['arr_house'] as $i => $v) {
			$this->arr_my_id_web[]    = $v['id_web'];
			$this->arr_my_id_houses[] = $v['id_houses'];
		}

		Context::getContext()->smarty->assign([
			'web'        => $arr['web'],
			'arr_house'  => $_SESSION['arr_house'],
			'house_code' => $arr['house_code'],
			'name'       => $_SESSION['name'],
		]);
	}

	public function initContent()
	{
		parent::initContent();
		$this->context->smarty->assign([
			'cms_url' => CMS::getContext()->url,
			'html'    => $this->html,
			'cms_css'     => $this->css,
			'cms_js'      => $this->js,
		]);
		$this->setTemplate('cms.tpl');
	}

}