<?php
namespace web_app;
use \Tools;
use \Store;
use \Popularity;
use \JSON;
use \db_mysql;
use \AppController;
use \Db;

class SearchStoreController extends AppController
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';
	public $page = 'search_store';
	public $no_page = false;

	public function __construct()
	{
		parent::__construct();
		$this->className                 = 'SearchStoreController';
		$this->meta_title                = $this->l('找好康');
		$this->page_header_toolbar_title = $this->l('找好康');
//		$this->fields['page']['cache'] = true;
        $this->table = "store";
	}

	public function initProcess()
	{
		parent::initProcess();

		$content_ratio = '36:20';
		
		$arr_county = Store::getContext()->getCounty(1);        //縣市

		$arr_area = Store::getContext()->get_city(1);        //行政區

		$arr_career = Store::getContext()->getStoreType(1);    //類別

		$county = Tools::getValue('county');
		$area   = Tools::getValue('area');
		$career = Tools::getValue('career');
        $title = Tools::getValue('title');
		$order = Tools::getValue('order');
        $id_member = $_SESSION["id_member"];//favorite要用的
        $session_id = session_id();//favorite要用的
        $favorite_data = [];//favorite要用的

		$arr_store = Store::getContext()->get(null, 1, 10, $county, $area, $career, $order,$title);



        if(!empty($id_member)){//有
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND is_member=".GetSQL($id_member,"int");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個is_member
        }else{//只有session_id
            $sql = "SELECT * FROM `favorite` WHERE table_name=".GetSQL($this->table,"text")." AND session_id=".GetSQL($session_id,"text");
            $favorite_data = Db::rowSQL($sql);//求出裡面有幾個session_id
        }
        
		foreach ($arr_store as $i => $v) {
            //我的最愛start
            foreach ($favorite_data as $key => $value) {
                if ($arr_store[$i]["id_store"] == $favorite_data[$key]["table_id"]) {//上面搜索了該table與本次使用者的資訊 這邊濾同id
                    $arr_store[$i]["favorite"] = $favorite_data[$key]["active"];
                }
            }

            //end

			$num                             = Store::getContext()->get_popularity_val($v['id_store']);
			$arr_store[$i]['popularity_img'] = Popularity::getPopularityImg($num['all'],$num);
			$arr_img                         = Store::getContext()->get_store_file_url($v['id_store'],0);
			$arr_store[$i]['img']            = $arr_img[0];
			$arr_store[$i]['discount']       = array_filter(explode(',', $v['discount']));
			$arr_store[$i]['service']        = array_filter(explode(',', $v['service']));
		}

		$this->context->smarty->assign([
			'arr_county' => $arr_county,
			'arr_career' => $arr_career,
			'arr_area'   => $arr_area,
			'arr_store'  => $arr_store,
			'action'     => '/SearchStore',
		]);

		//2019-12-16 寫在tpl
//		$this->context->smarty->assign([
//			'coupon_min_menu' => $this->context->smarty->fetch('coupon' . DS . 'min_menu.tpl'),
//			'search_store'    => $this->context->smarty->fetch('store' . DS . 'search_store.tpl'),
//			'store_item_list' => $this->context->smarty->fetch('store' . DS . 'store_item_list.tpl'),
//		]);
	}



	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new SearchStoreController();
		};
		return self::$instance;
	}

	public function ajaxProcessTEST(){

	}
	
	public function ajaxProcessShowList()
	{
		$p = Tools::getValue('p');
		$m = Tools::getValue('m');

		$county = Tools::getValue('county');
		$area   = Tools::getValue('area');
		$career = Tools::getValue('career');
        $title = Tools::getValue('title');

		$WHERE = '';
		if (!empty($county)) {
			$WHERE .= sprintf(' AND s.`county` = %s', GetSQL($county, 'text'));
		}
		if (!empty($area)) {
			$WHERE .= sprintf(' AND s.`area` = %s', GetSQL($area, 'text'));
		}
		if (!empty($career)) {
			$WHERE .= sprintf(' AND s.`career` = %d', GetSQL($career, 'int'));
		}

        if (!empty($title)) {   //新增的
            $title = '%'.$title.'%';
            $WHERE .= sprintf(' AND s.`title` LIKE %s', GetSQL($title, 'text'));
        }

		if (!empty($_SESSION['id_member'])) {
			$WHERE .= ' AND sfa.`id_member` = ' . $_SESSION['id_member'];
		}

		$sql = 'SELECT s.`id_store`, s.`title`, s.`service`, s.`discount`,
					st.`store_type`,
					f.`file_url`, f.`filename`,
					IF(sfa.`id_member` > 0, "on", "") AS favorite
					FROM `store` AS s
					LEFT JOIN `store_type` AS st ON st.`id_store_type` = s.`id_store_type`
					LEFT JOIN `store_favorite` AS sfa ON sfa.`id_store` = s.`id_store`
					LEFT JOIN `store_file` AS sf ON sf.`id_store` = s.`id_store`
					LEFT JOIN `file` AS f ON f.`id_file` = sf.`id_file`
					WHERE TRUE' . $WHERE.'
					GROUP BY s.`id_store`
					ORDER BY if(s.`sort` > 0, 0, 1) ASC, s.`sort` ASC, s.`id_store` DESC';

		$db = new db_mysql($sql, $m, '', 'p', 'm');
		if ($db->num() > 0) {
			while ($d = $db->next_row()) {
				if (empty($d['filename'])) {
					$d['img'] = THEME_URL . '/img/default.jpg';
				}else{
					$d['img'] = urldecode($d['file_url'].$d['filename']);
				}
				$num                 = Store::getContext()->get_popularity_val($d['id_store']);
				$d['popularity_img'] = Popularity::getPopularityImg($num['all'],$num);
				$d['discount']       = array_filter(explode(',', $d['discount']));
				$d['service']        = array_filter(explode(',', $d['service']));
				$arr_d[]             = $d;
			};
		};
		$data = [
			'p'    => ($p) + 1,
			'max'  => $db->total_page(),
			'data' => $arr_d,
		];
//		echo JSON::getContext()->encode($data);
//		exit;
	}

    public function setMedia()
    {

        parent::setMedia();

        $this->addCSS(THEME_URL . '/css/search_store.css');

    }
}