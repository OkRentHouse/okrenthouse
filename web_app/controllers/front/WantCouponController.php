<?php
namespace web_app;
use \AppController;
use \Config;
use \FileUpload;
class WantCouponController extends AppController
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';
	public $page = 'want_coupon';

	public function __construct()
	{
		parent::__construct();
		$this->className                 = 'WantCouponController';
		$this->meta_title                = $this->l('要好康');
		$this->page_header_toolbar_title = $this->l('要好康');
//		$this->fields['page']['cache'] = true;
	}

	public function initProcess()
	{
		parent::initProcess();

		$content_ratio = '50:28';

		$arr_id_file = Config::get('banner_to_top', true);

		$arr_to_top = FileUpload::get($arr_id_file);
		foreach ($arr_to_top as $i => $v) {
			$url                  = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url                  = urldecode($url);
			$arr_content_benner[] = [
				'href' => '#',
				'img'  => $url,
			];
		}

		$arr_id_file = Config::get('banner_to_1', true);

		$arr_to_1 = FileUpload::get($arr_id_file);
		$v             = $arr_to_1[0];
		$url           = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url           = urldecode($url);
		$to_1_img    = $url;

		$arr_id_file = Config::get('banner_to_2', true);

		$arr_to_2 = FileUpload::get($arr_id_file);
		$v             = $arr_to_2[0];
		$url           = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url           = urldecode($url);
		$to_2_img    = $url;


		$this->context->smarty->assign([
			'content_ratio'      => $content_ratio,			//要好康幻燈片圖片比例
			'arr_content_benner' => $arr_content_benner,	//要好康幻燈片
			'to_1_img' => $to_1_img,
			'to_2_img' => $to_2_img,
		]);
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new WantCouponController();
		};
		return self::$instance;
	}
}