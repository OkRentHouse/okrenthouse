<?php
namespace web_app;
use \Store;
use \AppController;

class StoreMapController extends AppController
{
	public $page = 'store_map';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		parent::__construct();
		$this->className                 = 'StoreMapController';
		$this->meta_title                = $this->l('商家地圖');
		$this->page_header_toolbar_title = $this->l('商家地圖');
	}

	public function initProcess()
	{
		parent::initProcess();

		$arr_county = [
			'1'  => [
				'code' => 'A',
				'text' => '臺北市',
			],
			'2'  => [
				'code' => 'G',
				'text' => '基隆市',
			],
			'3'  => [
				'code' => 'B',
				'text' => '新北市',
			],
			'4'  => [
				'code' => 'R',
				'text' => '宜蘭縣',
			],
			'5'  => [
				'code' => 'I',
				'text' => '新竹市',
			],
			'6'  => [
				'code' => 'K',
				'text' => '新竹縣',
			],
			'7'  => [
				'code' => 'H',
				'text' => '桃園市',
			],
			'8'  => [
				'code' => 'L',
				'text' => '苗栗縣',
			],
			'9'  => [
				'code' => 'C',
				'text' => '臺中市',
			],
			'10' => [
				'code' => 'M',
				'text' => '彰化縣',
			],
			'11' => [
				'code' => 'N',
				'text' => '南投縣',
			],
			'12' => [
				'code' => 'J',
				'text' => '嘉義市',
			],
			'13' => [
				'code' => 'P',
				'text' => '嘉義縣',
			],
			'14' => [
				'code' => 'O',
				'text' => '雲林縣',
			],
			'15' => [
				'code' => 'E',
				'text' => '臺南市',
			],
			'16' => [
				'code' => 'F',
				'text' => '高雄市',
			],
			'17' => [
				'code' => 'Q',
				'text' => '屏東縣',
			],
			'18' => [
				'code' => 'T',
				'text' => '臺東縣',
			],
			'19' => [
				'code' => 'S',
				'text' => '花蓮縣',
			],
			'20' => [
				'code' => 'U',
				'text' => '金門縣',
			],
			'21' => [
				'code' => 'U',
				'text' => '連江縣',
			],
			'22' => [
				'code' => 'U',
				'text' => '澎湖縣',
			],
		];

		$arr_county = Store::getContext()->getCounty(1);		//縣市

		$arr_area = Store::getContext()->get_city(1);		//行政區

		$arr_career = Store::getContext()->getStoreType(1);	//類別

		$lat        = '25.0210604';
		$lng        = '121.2985558';
		$zoom       = '15';
		$minZoom	= 10;
		$maxZoom	= 19;
		$google_key = 'AIzaSyAU9s5j6uICVXp47T3-AxlV_RZ7cVk7cwg';        //百合苑
//		$google_key = 'AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c';		//生活樂租
		$this->context->smarty->assign([
			'arr_county' => $arr_county,
			'arr_career' => $arr_career,
			'arr_area'   => $arr_area,
			'google_key' => $google_key,
			'action'     => '/StoreMap',
			'google_map' => [
				'lat'        => $lat,
				'lng'        => $lng,
				'zoom'       => $zoom,
				'minZoom'	=> $minZoom,
				'maxZoom'	=> $maxZoom,
			],
		]);

		$this->context->smarty->assign([
			'search_store'    => $this->context->smarty->fetch('store' . DS . 'search_store.tpl'),
		]);

	}
}