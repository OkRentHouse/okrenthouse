<?php

namespace web_app;

use \AppController;
use \Db;
use \File;
use \Tools;

class LifeGoController extends AppController
{
	public $page = 'LifeGo';

	public $tpl_folder;    //樣版資料夾
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		parent::__construct();
		$this->className                 = 'LifeGoController';
		$this->meta_title                = $this->l('活動分享');
		$this->page_header_toolbar_title = $this->l('活動分享');
	}



    public function initProcess()
    {
        parent::initProcess();

        $sql = 'SELECT li.`id_in_life`, li.`title`, li.`title2`, li.`date_s`, li.`date_e`, li.`time_s`, li.`time_e`,
                 li.`weekday`, li.`content`,ilf.`id_file`
                        FROM `in_life` li
                        LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
                        WHERE ilf.`file_type` = 3
                        AND active = 1
                        ORDER BY top DESC,date DESC ';

        $row = Db::rowSQL($sql);

        $as_arr = [];
        foreach($row as $i => $v){
            $file = File::get($v['id_file']);
            $as_arr[] = [
                'id_in_life'=>$v['id_in_life'],
                'title1' => $v['title'],
                'title2' => $v['title2'],
                'img' => $file['url'],
            ];
        }


        $id = Tools::getValue('id');


        $this->context->smarty->assign([
            'as_arr' => $as_arr,
        ]);

//		if (!empty($id)) {
//			$this->setTemplate('page.tpl');
//		} else {
//			$this->setTemplate('list.tpl');
//		}

        $this->setTemplate('list.tpl');

    }


    public function setMedia()
    {
        parent::setMedia();
        $this->addFooterCSS(THEME_URL . '/css/LifeGo.css');
    }
}
