<?php

namespace web_app;

use Tools;
use Db;
use Message;
use \AppController;
class FCMController extends AppController
{
	public $no_page = true;
	public function displayAjaxFCM()
	{
		$reg_id = Tools::getValue('reg_id');
		$type   = Tools::getValue('type');
		/*
		 * 新增 reg_id
		 */
		if (!empty($reg_id) && !empty($_SESSION['id_member']) && !empty($type)) {
			$id_member = $_SESSION['id_member'];
			if (!empty($id_member)) {
				$sql = sprintf('INSERT INTO `fcm_reg_id` (`reg_id`, `id_member`, `type`, `model`)
									VALUES (%s, %d, %s, %s) ON
									DUPLICATE KEY
									UPDATE `reg_id` = %s,`id_member` = %d, `type` = %s, `model` = %s',
					GetSQL($reg_id, 'text'),
					$id_member,
					GetSQL($type, 'text'),
					GetSQL(get_mobile(), 'text'),
					GetSQL($reg_id, 'text'),
					$id_member,
					GetSQL($type, 'text'),
					GetSQL(get_mobile(), 'text'));
				Db::rowSQL($sql);
			}
			$json = new JSON();
			$num  = 1;//Message::getMemberNum($_SESSION['id_member']);		//所有未讀數字
			$time = time();
			echo $json->encode([
				'reg_id'    => $reg_id,
				'id_member' => $id_member,
				'msg_num'   => $num,
				'msg_time'  => $time,
			]);
			exit;
		}
		$json = new JSON();
		$time = time();
		echo $json->encode([
			'reg_id'    => $reg_id,
			'id_member' => 1,
			'msg_num'   => 1,
			'msg_time'  => $time,
		]);
		exit;
	}
}