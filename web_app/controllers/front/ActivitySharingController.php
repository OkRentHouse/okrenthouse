<?php


namespace web_app;

use \AppController;
use \Db;
use \File;
class ActivitySharingController extends AppController
{
    protected static $instance;
    public $_errors = [];
    public $_msgs = [];
    public $msg = '';
    public $page = 'ActivitySharing';

    public function __construct()
    {
        parent::__construct();
        $this->no_page = true;
        $this->className = 'ActivitySharingController';
        $this->table = 'in_life';
        $this->meta_title = $this->l('生活好康');
        $this->page_header_toolbar_title = $this->l('生活好康');
//        $this->fields['page']['cache'] = true;
    }

    public function initProcess()
    {
        parent::initProcess();

        $sql = 'SELECT li.`id_in_life`, li.`title`, li.`title2`, li.`date_s`, li.`date_e`, li.`time_s`, li.`time_e`, 
                 li.`weekday`, li.`content`,ilf.`id_file`
                        FROM `in_life` li
                        LEFT JOIN `in_life_file` AS ilf ON ilf.`id_in_life` = li.`id_in_life`
                        WHERE ilf.`file_type` = 3
                        AND active = 1
                        ORDER BY top DESC,date DESC ';

        $row = Db::rowSQL($sql);

        $as_arr = [];
        foreach($row as $i => $v){
            $file = File::get($v['id_file']);
            $as_arr[] = [
                'id_in_life'=>$v['id_in_life'],
                'title1' => $v['title'],
                'title2' => $v['title2'],
                'img' => $file['url'],
            ];
        }
        $this->context->smarty->assign([
            'as_arr' => $as_arr,
        ]);
        //2019-12-16 寫在tpl裡
//		$this->context->smarty->assign([
//			'coupon_min_menu' => $this->context->smarty->fetch('coupon' . DS . 'min_menu.tpl'),
//			'search_store'    => $this->context->smarty->fetch('store' . DS . 'search_store.tpl'),
//			'store_item_list' => $this->context->smarty->fetch('store' . DS . 'store_item_list.tpl'),
//		]);
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addFooterCSS(THEME_URL . '/css/activity.css');
    }
}