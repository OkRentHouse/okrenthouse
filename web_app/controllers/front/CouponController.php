<?php

namespace web_app;

use \Store;
use \AppController;
use \Config;
use \FileUpload;
use \Db;

class CouponController extends AppController
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';
	public $page = 'coupon';

	public function __construct()
	{
		parent::__construct();
		$this->no_page                   = true;
		$this->className                 = 'CouponController';
		$this->meta_title                = $this->l('生活好康');
		$this->page_header_toolbar_title = $this->l('生活好康');
//		$this->fields['page']['cache']   = true;
	}

	public function initProcess()
	{
		parent::initProcess();


		$arr_county = Store::getContext()->getCounty(1);        //縣市

		$arr_area = Store::getContext()->get_city(1);        //行政區

		$arr_career = Store::getContext()->getStoreType(1);    //類別

		$content_ratio = '50:28';


		$arr_id_file = Config::get('banner_news_top', true);




		$arr_news_top = FileUpload::get($arr_id_file);
		foreach ($arr_news_top as $i => $v) {
			$url                  = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url                  = urldecode($url);
			$arr_content_banner[] = [
				'href' => '#',
				'img'  => $url,
			];
		}

//        $sql = 'SELECT * FROM `store` WHERE active=1';
//
//        $arr_store = Db::rowSQL($sql);
//
//        print_r($arr_store);



		$arr_id_file = Config::get('banner_news_find', true);

		$arr_news_find = FileUpload::get($arr_id_file);
		$v             = $arr_news_find[0];
		$url           = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url           = urldecode($url);
		$coupon_img    = $url;

		$arr_id_file = Config::get('banner_news_find', true);

		$arr_news_find = FileUpload::get($arr_id_file);
		$v             = $arr_news_find[0];
		$url           = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url           = urldecode($url);
		$coupon_img    = $url;

		$arr_id_file = Config::get('banner_news_to', true);

		$arr_news_to = FileUpload::get($arr_id_file);
		$v           = $arr_news_to[0];
		$url         = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url         = urldecode($url);
		$want_img    = $url;

		$arr_id_file = Config::get('banner_news_give', true);

		$arr_news_give = FileUpload::get($arr_id_file);
		$v             = $arr_news_give[0];
		$url           = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url           = urldecode($url);
		$give_img      = $url;

		$give_text = Config::get('news_give_txt');







		$this->context->smarty->assign([
			'content_ratio'      => $content_ratio,            //要好康幻燈片圖片比例
			'arr_content_banner' => $arr_content_banner,    //要好康幻燈片
			'arr_county'         => $arr_county,
			'arr_career'         => $arr_career,
			'arr_area'           => $arr_area,
			//			'arr_store'          => $arr_store,
			'action'             => '/SearchStore',
			'coupon_img'         => $coupon_img,
			'want_img'           => $want_img,
			'give_img'           => $give_img,
			'give_text'          => $give_text,
		]);

		//2019-12-16 寫在tpl裡
//		$this->context->smarty->assign([
//			'coupon_min_menu' => $this->context->smarty->fetch('coupon' . DS . 'min_menu.tpl'),
//			'search_store'    => $this->context->smarty->fetch('store' . DS . 'search_store.tpl'),
//			'store_item_list' => $this->context->smarty->fetch('store' . DS . 'store_item_list.tpl'),
//		]);
	}


	public function setMedia()
	{
		parent::setMedia();
		$this->addJS(THEME_URL . '/js/search_store.js');
        $this->addCSS( THEME_URL . '/css/search_store.css');
	}



}