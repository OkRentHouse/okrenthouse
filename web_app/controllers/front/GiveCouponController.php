<?php

namespace web_app;

use \AppController;
use \Config;
use \FileUpload;

class GiveCouponController extends AppController
{
	protected static $instance;
	public $_errors = [];
	public $_msgs = [];
	public $msg = '';
	public $page = 'give_coupon';

	public function __construct()
	{
		parent::__construct();
		$this->no_page                   = true;
		$this->className                 = 'GiveCouponController';
		$this->meta_title                = $this->l('給好康');
		$this->page_header_toolbar_title = $this->l('給好康');
//		$this->fields['page']['cache']   = true;
	}

	public function initProcess()
	{
		parent::initProcess();

		$content_ratio      = '36:20';
		$arr_id_file = Config::get('banner_give_top', true);

		$arr_give_top = FileUpload::get($arr_id_file);
		foreach ($arr_give_top as $i => $v) {
			$url                  = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
			$url                  = urldecode($url);
			$arr_content_benner[] = [
				'href' => '#',
				'img'  => $url,
			];
		}

		$arr_id_file = Config::get('banner_give_1', true);

		$arr_give_1 = FileUpload::get($arr_id_file);
		$v          = $arr_give_1[0];
		$url        = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url        = urldecode($url);
		$give_1_img = $url;

		$arr_id_file = Config::get('banner_give_2', true);

		$arr_give_2 = FileUpload::get($arr_id_file);
		$v          = $arr_give_2[0];
		$url        = str_replace(WEB_DIR, '', $v['file_url'] . $v['filename']);
		$url        = urldecode($url);
		$give_2_img = $url;

		$this->context->smarty->assign([
			'content_ratio'      => $content_ratio,            //給好康幻燈片圖片比例
			'arr_content_benner' => $arr_content_benner,    //給好康幻燈片
			'give_1_img'         => $give_1_img,
			'give_2_img'         => $give_2_img,
		]);
	}

	public static function getContext()
	{
		if (!self::$instance) {
			self::$instance = new GiveCouponController();
		};
		return self::$instance;
	}
}