<?php

namespace web_app;

use \AppController;
use \Config;
use \FileUpload;
use \Db;
class AddStoreController extends AppController
{
    protected static $instance;
    public $_errors = [];
    public $_msgs = [];
    public $msg = '';
    public $page = 'add_store';



    public function __construct()
    {
        parent::__construct();
        $this->no_page                   = true;
        $this->className                 = 'AddStoreController';
        $this->meta_title                = $this->l('加入特約店家');
        $this->page_header_toolbar_title = $this->l('加入特約店家');
//        $this->fields['page']['cache']   = true;
    }



    public function initProcess()
    {
        $sql = "SELECT * FROM `store_type` ORDER BY `position` ASC";
        $row = Db::rowSQL($sql);

////        print_r($row);
//
//        foreach($row as $key => $value){
//            echo  $row[$key]['store_type'].'<br>';
//        }


        parent::initProcess();



        $this->context->smarty->assign([
            'row' => $row,
        ]);
    }


    public function postProcess()
    {

    }

}