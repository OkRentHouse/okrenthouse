<?php

namespace web_app;

use \AppController;
use \Config;

class WealthSharingController extends AppController
{
    protected static $instance;
    public $_errors = [];
    public $_msgs = [];
    public $msg = '';
    public $page = 'WealthSharing';

    public function __construct()
    {
        parent::__construct();
        $this->no_page                   = true;
        $this->className                 = 'WealthSharingController';
        $this->meta_title                = $this->l('創富生活');
        $this->page_header_toolbar_title = $this->l('創富生活');
//        $this->fields['page']['cache']   = true;
    }

    public function initProcess()
    {
        parent::initProcess();

        $menu_buoy = [
            [
                'title'=>'達人顧問',
                'active' => ['/ExpertAdvisor'],
                'url' =>'/ExpertAdvisor'
            ],
            [
                'title'=>'創富分享',
                'active' => ['/WealthSharing'],
                'url' =>'/WealthSharing'
            ],
            [
                'title'=>'加盟體系',
                'active' => ['/JoinUs'],
                'url' =>'/JoinUs'
            ],
        ];


        $this->context->smarty->assign([
            'menu_buoy'       => $menu_buoy,
        ]);
    }


    public function setMedia()
    {
        parent::setMedia();

    }



}
