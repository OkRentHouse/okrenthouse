<?php
namespace web_app;
use \AppController;

class PatnerController extends AppController
{
    public $page = 'Patner';

    public $tpl_folder;	//樣版資料夾
    public $definition;
    public $_errors_name;

    public function __construct(){
        parent::__construct();
        $this->className = 'PatnerController';
        $this->meta_title = $this->l('共好夥伴');
        $this->page_header_toolbar_title = $this->l('共好夥伴');
    }

    public function initProcess()
    {
        parent::initProcess();

        $menu_buoy = [
            [
                'title'=>'達人顧問',
                'active' => ['/ExpertAdvisor'],
                'url' =>'/ExpertAdvisor'
            ],
            [
                'title'=>'共好夥伴',
                'active' => ['/Patner'],
                'url' =>'/Patner'
            ],
            [
                'title'=>'加盟體系',
                'active' => ['/JoinUs'],
                'url' =>'/JoinUs'
            ],
        ];

        $this->context->smarty->assign([
            'menu_buoy'       => $menu_buoy,
        ]);
    }

    public function setMedia(){

        parent::setMedia();
            $this->addCSS( '/css/metro-all.css');
            $this->addCSS( '/css/fontawesome.min.css');
            $this->addJS( '/js/metro.min.js');
            $this->addJS( '/js/fontawesome.min.js');
            $this->addJS( '/js/solid.min.js');
            $this->addJS( '/js/regular.min.js');
            $this->addCSS( THEME_URL .'/css/list.css');
            $this->addCSS( THEME_URL .'/css/Patner.css');
    }
}
