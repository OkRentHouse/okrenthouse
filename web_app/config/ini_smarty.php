<?php
define('_SMARTY_DIR',			TOOL_DIR.DS.'smarty-3.1.28');
define('_SMARTY_CACHE_DIR',		CACHE_DIR.DS.'smarty'.DS.'cache');			//快取路徑
define('_SNARTY_TEMPLATE_DIR',	CACHE_DIR.DS.'smarty'.DS.'templates');
define('_SMARTY_COMPILE_DIR',	CACHE_DIR.DS.'smarty'.DS.'templates_c');			//Smarty 編譯
define('_SMARTY_CONFIG_DIR',	CONFIG_DIR.DS.'smarty');							//配置檔案路徑
define('_SMARTY_CACHING', 		false);												//Smarty 快取