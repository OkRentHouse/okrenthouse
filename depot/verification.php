<?php
 $img_height = 32;  // 圖形高度
 $img_width = 100;   // 圖形寬度
 $num = '';             // 數字
 $mass = 400;        // 雜點的數量，數字愈大愈不容易辨識
 $num_max = 6;      // 產生6個驗證碼
 for( $i=0; $i<$num_max; $i++ )  //驗證產生6碼
 {
     $num .= rand(0,9);
 }
 if(!session_id()){
	session_start();
 }
 $_SESSION['verification2']=$num;
 // 創造圖片，定義圖形和文字顏色
 header("Content-type: image/PNG");
 srand((double)microtime()*1000000);
 $im = imagecreate($img_width,$img_height);
 $black = ImageColorAllocate($im, 80,80,80);     // 底色
 $white = ImageColorAllocate($im, 250,250,250); //文字顏色 
 imagefill($im,0,0,$black);

 // 在圖形產上黑點，起干擾作用;
 for( $i=0; $i<$mass; $i++ )
 {
 imagesetpixel($im, rand(0,$img_width), rand(0,$img_height), $white);
 }

 // 將數字隨機顯示在圖形上,文字的位置都按一定波動範圍隨機生成
 $strx=rand(0,8);
 for( $i=0; $i<$num_max; $i++ )
 {
      $strpos=rand(1,8);
      imagestring($im,5,$strx,$strpos, substr($num,$i,1), $white);
      $strx+=rand(18,10);
 }
 ImagePNG($im);
 ImageDestroy($im);
 exit;
?>

