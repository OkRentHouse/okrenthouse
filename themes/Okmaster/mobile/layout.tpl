<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	{if !empty($GOOGLE_SITE_VERIFICATION)}<meta name="google-site-verification" content="{$GOOGLE_SITE_VERIFICATION}" />{/if}
	<title>{$title}</title>
	{if $meta_description != ''}<meta name="description" content="{$meta_description}">{/if}
	{if $meta_keywords != ''}<meta name="keywords" content="{$meta_keywords}">{/if}
	<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
	{if !empty($t_color)}<meta name="theme-color" content="{$t_color}">{/if}
	{if !empty($mn_color)}<meta name="msapplication-navbutton-color" content="{$mn_color}">{/if}
	{if !empty($amwasb_style)}<meta name="apple-mobile-web-app-status-bar-style" content="{$amwasb_style}">{/if}
	{foreach from=$css_files key=key item=css_uri}
		<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
	{/foreach}
	{if !isset($display_header_javascript) || $display_header_javascript}
		<script type="text/javascript">
			var THEME_URL = '{$smarty.const.THEME_URL}';
		</script>
	{foreach from=$js_files key=key item=js_uri}
		<script type="text/javascript" src="{$js_uri}"></script>
	{/foreach}
	{/if}
</head>
<body>
{include file="$tpl_dir./mobile/main_menu.tpl"}
<div data-role="{$role}" id="{$page}" class="{$role} {$fields.page.class}"{if isset($fields.page.cache)} data-dom-cache="{if $fields.page.cache}true{else}false{/if}"{/if}>
	{foreach from=$css_header_files key=key item=css_uri}
		<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
	{/foreach}
	{foreach from=$js_header_files key=key item=js_uri}
		<script src="{$js_uri}"></script>
	{/foreach}
	{if $display_header}{include file="$tpl_dir./mobile/header.tpl"}{/if}
	<div data-role="content" id="{$mamber_url}">
		{if !empty($content)}{$content}{/if}
		{if !empty($template)}{$template}{/if}
		{if $role == 'dialog'}
		<a href="#" class="btn btn-default ui-btn ui-shadow ui-corner-all" data-rel="back">{l s="關閉"}</a>
		{/if}
	</div>
	{foreach from=$css_footer_files key=key item=css_uri}
		<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
	{/foreach}
	{foreach from=$js_footer_files key=key item=js_uri}
		<script src="{$js_uri}"></script>
	{/foreach}
	{include file="$tpl_dir./mobile/msg_box.tpl"}
	{if $display_main_menu}
	{include file="$tpl_dir./mobile/display_main_menu.tpl"}
	{/if}
</div>
{if $display_footer}{include file="$tpl_dir./mobile/footer.tpl"}{/if}
</body>
</html>