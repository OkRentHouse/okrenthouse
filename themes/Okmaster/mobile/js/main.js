function alert(msg){
	Swal.fire(msg);
};
function open_side_menu(){
	$('.menu_shadow').stop(true, false).fadeIn('fast');
	$('.side_menu').animate({left:'0px'}, 'fast');
};
function close_side_menu(){
	$('.menu_shadow').stop(true, false).fadeOut('fast');
	$('.side_menu').animate({left:'-80vw'}, 'fast',function(){
		// $('.subs', $side_menu).stop(true, false).slideUp('fast');
		// $('.side_menu').scrollTop(0);
	});
};
//恢復開關
function flipswitch($this, type){
	if(type){
		$this.find('option[value="on"]').prop('selected', true);
		$this.parent('.ui-flipswitch').addClass('ui-flipswitch-active');
	}else{
		$this.find('option[value="off"]').prop('selected', true);
		$this.parent('.ui-flipswitch').removeClass('ui-flipswitch-active');
	};
};
function set_personally($type){		//本人領取
	$.ajax({
		url : '/information',
		method : 'POST',
		data : {
			'ajax'      : true,
			'action'    : 'SetPersonally',
			'type' : $type,
		},
		dataType : 'json',
		success: function(msg){
			if(!msg){
				$.growl.error({
					title: '',
					message: '資料錯誤'
				});
			};
		},
		beforeSend:function(){

		},
		complete:function(){

		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '資料錯誤'
			});
		}
	});
};
function setFCM($type){			//APP推播通知
	$.ajax({
		url : '/information',
		method : 'POST',
		data : {
			'ajax'      : true,
			'action'    : 'SetFCM',
			'type' : $type,
		},
		dataType : 'json',
		success: function(msg){
			if(!msg){
				$.growl.error({
					title: '',
					message: '資料錯誤'
				});
				flipswitch($('#information #fms'), !$type);
			};
		},
		beforeSend:function(){

		},
		complete:function(){

		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '資料錯誤'
			});
			flipswitch($('#information #fms'), !$type);
		}
	});
};
function setEmailOn($type){		//Email通知
	$.ajax({
		url : '/information',
		method : 'POST',
		data : {
			'ajax'      : true,
			'action'    : 'SetEmail',
			'type' : $type,
		},
		dataType : 'json',
		success: function(msg){
			if(!msg){
				$.growl.error({
					title: '',
					message: '資料錯誤'
				});
				flipswitch($('#information #email_notification'), !$type);
			};
		},
		beforeSend:function(){

		},
		complete:function(){

		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '資料錯誤'
			});
			flipswitch($('#information #email_notification'), !$type);
		}
	});
};
function personally(){
	let $personally = $('#personally', '#information');
	if($personally.val() == 'off'){
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-primary',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false,
		});
		swalWithBootstrapButtons.fire({
			title: '不限本人領取?',
			html: '您將取消"限本人裡取"設定<br>取消後先前信件、包裹領件紀錄同住戶將檢示的到!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: '確定',
			cancelButtonText: '取消',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				swalWithBootstrapButtons.fire(
					'不限本人領取',
					'同住戶將可代領您的信件、包裹;<br>檢視您的先前信件、包裹領件紀錄',
					'success'
				);
				set_personally(false);
			} else{
				flipswitch($personally, true);
				set_personally(true);
			};
		});
	}else{
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-primary',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false,
		});
		swalWithBootstrapButtons.fire({
			title: '僅限本人領取?',
			html: '您將設定"限本人裡取"<br>設定後先前信件、包裹領件紀錄同住戶將無法檢示!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: '確定',
			cancelButtonText: '取消',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				swalWithBootstrapButtons.fire(
					'僅限本人領取',
					'同住戶無法代領您的信件、包裹;<br>無法檢視您的先前信件、包裹領件紀錄',
					'success'
				);
				set_personally(true);
			} else{
				flipswitch($personally, false);
				set_personally(false);
			};
		});
	};
};
function ServiceMessage(){
	$.ajax({
		url : document.location.pathname,
		method : 'POST',
		data : {
			'ajax'		: true,
			'action'		: 'Message',
			'id_service'	: $('#id_service').val(),
			'id_message'	: $('#id_message').val(),
			'message'		: $('#message').val()
		},
		dataType : 'JSON',
		success: function(msg){
			if(msg.error == ''){
				$('#message').val('');
				$('#id_message').val(msg.id_message);
				msg.message.forEach(function(v){
					var class_txt = '';
					if(v.id_admin > 0){
						class_txt = 'admin_msg';
					}else{
						class_txt = 'member_msg text-right';
					};
					var htm = `<div class="` + class_txt + `">
						<div class="time">` + v.time + `</div>
						<div class="message_div">` + v.message + `</div>
					</div>`;
					$('.message_list2').append(htm).scrollTop(99999999999);
				});
			}else{
				$.growl.error({
					title: '',
					message: msg.error
				})
			};
		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '留言失敗!'
			})
		}
	});
};
//螢幕變亮
if(typeof(Android) != 'undefined'){				//Android
	function screen(on){
		try{
			Android.screen(on);
		}catch (e){

		};
	};
}else if(typeof(window.webkit) != 'undefined'){	//IOS
	function screen(on){
		try{
			window.webkit.messageHandlers.screen.postMessage(on);
		}catch (e){

		};
	};
}else{
	function screen(on){
		return false;
	};
};
function logout(){
	if(typeof(Android) != 'undefined'){
		try{
			Android.set_msg_num(0, re_time);
			Android.logout();
		}catch (e){

		};
	};
	if(typeof(window.webkit) != 'undefined'){
		try{
			window.webkit.messageHandlers.logout.postMessage();
		}catch (e){

		};
	};
};
function set_msg_num(msg_num, msg_time){
	if(typeof(Android) != 'undefined' && typeof(msg_num) != 'undefined' && typeof(msg_time) != 'undefined'){
		if(msg_time > 0){
			Android.set_msg_num(msg_num, msg_time);
		};
	};
	if(typeof(window.webkit) != 'undefined' && typeof(msg_num) != 'undefined' && typeof(msg_time) != 'undefined'){
		if(msg_time > 0){
			window.webkit.messageHandlers.set_msg_num.postMessage({'msg_num':msg_num, 'msg_time':msg_time});
		};
	};
};
function login(reg_id, type = 'Android'){
	$.ajax({
		url : '/FCM',
		method : 'POST',
		data : {
			'ajax'	: true,
			'action': 'FCM',
			'reg_id': reg_id,
			'type'	: type
		},
		dataType : 'json',
		success: function(msg){
			set_msg_num(parseInt(msg.msg_num), parseInt(msg.msg_time));
		},
		beforeSend:function(){
		},
		complete:function(){
		},
		error:function(xhr, ajaxOptions, thrownError){
		}
	});
	return true;
};
function welcome_close(){
};
var st;
function countdown(mini_second, fun, t, url){
	if(t == 0){
		clearTimeout(st);
	};
	$('.cd_m', '.countdown').css('transform', 'rotate(' + t / mini_second * 360 + 'deg)');
	if(t >= (mini_second/2)){
		$('.cd_t', '.countdown').hide();
		$('.cd_c', '.countdown').show();
	};
	if(t >= mini_second){
		eval(fun);
		$('.cd_t', '.countdown').show();
		$('.cd_c', '.countdown').hide();
		t = 0;
	};
	t += 0.5;
	if(location.pathname == url){
		st = setTimeout('countdown(' + mini_second + ', \'' + fun + '\', ' + t + ', \'' + url + '\')', 500);
	}else{
		clearTimeout(st);
	};
};
if(typeof(st) == "undefined"){
	var st = null;
};

$(document).ready(function(){
	const $side_menu = $('.side_menu');
	const $menu_shadow = $('.menu_shadow');
	$('.close_btn', $side_menu).click(function(){
		close_side_menu();
	});
	$('.menu_shadow').click(function(){
		close_side_menu();
	});
	$('a[href!="javascript:void(0);"]', $side_menu).click(function(){
		$('.action', $side_menu).removeClass('action');
		$(this).addClass('action');
		close_side_menu();
	});
	$('a[href="javascript:void(0);"]', $side_menu).click(function(){
		var $this = $(this);
		var $div = $this.parent('div');
		var $subs = $this.next('.subs');
		$('a', $div.siblings('div')).each(function(){
			$(this).removeClass('active');
			$(this).next('.subs').stop(true, false).slideUp('fast');
		});
		if($this.hasClass('active')){
			$this.removeClass('active');
			$subs.stop(true, false).slideUp('fast');
		}else{
			$this.addClass('active');
			$subs.stop(true, false).slideDown('fast');
		};
	});
	change_option();
}).on('click', '.open_btn', function(){
	open_side_menu();
}).on('change','#information #personally', function(){	//Email通知
	personally();
}).on('change','#information #fms', function(){			//APP推播ON
	setFCM(($(this).val() == 'on'));
}).on('change','#information #email_notification', function(){
	setEmailOn(($(this).val() == 'on'));
}).on('change', '#screen', function () {					//亮度調整
	screen($(this).val() == 'on');
}).on('click', 'input[type="text"],textarea', function () {
	var target = this;
	setTimeout(function(){
		target.scrollIntoViewIfNeeded();
	},400);
}).on('click', 'a[href="?logout"]', function(){
	logout();
}).on('click', '#del_member', function(){
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-primary',
			// cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false,
	});
	swalWithBootstrapButtons.fire({
		title: '解除開通',
		html: '<div class="orange_title ">'+$(this).data('web')+'</div>'+$(this).data('dex')+'<br><br>'+'您確定要解除您的帳戶?',
		type: 'warning',
		showCancelButton: false,
		confirmButtonText: '確定',
		// cancelButtonText: '取消',
		reverseButtons: true
	}).then((result) => {
		if (result.value) {
			location.href = '/information?del_member&id='+$(this).data('id_houses');
		};
	});
});