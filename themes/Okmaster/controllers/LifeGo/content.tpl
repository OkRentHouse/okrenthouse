 {include file="$tpl_dir./life_go_tab.tpl"}
     <div class="happy_shopping_wrap">
         <div class="flex">
             <div class="marquee_wrap">
                 <marquee direction="left" scrollamount="10" behavior="scroll">
                     <img src="/themes/Okmaster/img/lifego/star.svg" alt="" class="">歡慶聖誕佳節　全館限時優惠
                 </marquee>
             </div>
         </div>
         <div class="main_area_wrap">
             <div class="left_area_wrap">
                         {if $smarty.get.id_product_class}
                             <ul class="sub_menu">
                                 <li class="item {if $smarty.get.id_product_class_item==''} active {/if}">
                                     <a href="/LifeGo?id_product_class={$smarty.get.id_product_class}" >
                                         <p class="text-center">{$product_class_title.title}</p>
                                     </a>
                                 </li>
                             {foreach from=$product_class_item_arr key=k item=v}
                                     <li class="item {if $smarty.get.id_product_class_item==$v.id_product_class_item} active {/if}">
                                         <a href="/LifeGo?id_product_class={$v.id_product_class}&id_product_class_item={$v.id_product_class_item}" >
                                             <p class="text-center">{$v.item_title}</p>
                                         </a>
                                     </li>
                             {/foreach}
                             </ul>
                         {/if}				<div class="title_txt"><span>其他人也看了</span></div>
                 <div class="category_wrap">				 				 
                    <ul class="menu_wrap">
                             <li class="menu_list">
                                 <a href="" class="">
                                     <img src="/img/default.png" alt="">
                                 </a>
                             </li>
                             <li class="menu_list">
                                 <a href="" class="">
                                     <img src="/img/default.png" alt="">
                                 </a>
                             </li>
                             <li class="menu_list">
                                 <a href="" class="">
                                     <img src="/img/default.png" alt="">
                                 </a>
                             </li>
                             <li class="menu_list">
                                 <a href="" class="">
                                     <img src="/img/default.png" alt="">
                                 </a>
                             </li>
                             <li class="menu_list">
                                 <a href="" class="">
                                     <img src="/img/default.png" alt="">
                                 </a>
                             </li>
                             <li class="menu_list">
                                 <a href="" class="">
                                     <img src="/img/default.png" alt="">
                                 </a>
                             </li>
                     </ul>
                 </div>
             </div>


             <div class="right_area_wrap">

                 <div class="text-center">
                     <h2 class="caption hot"><span><img src="/themes/Okmaster/img/lifego/star_pink.svg" alt=""
                                                        class=""></span>HOT 熱銷榜
                     </h2>
                 </div>
                 <div style="margin-bottom: 20px;"></div>

                 <section class="slider hot regular">
                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption"><a href="/LifeGoProduct">健康御守 二氧化氯清淨錠套組</a></div>
                             <!-- <ul class="list_wrap">
                                 <li class="list">個人</li>
                                 <i></i>
                                 <li class="list">物品</li>
                                 <i></i>
                                 <li class="list">食品</li>
                                 <i></i>
                                 <li class="list">環境</li>
                                 <i></i>
                                 <li class="list">水質</li>
                                 <i></i>
                                 <li class="list">養生</li>
                             </ul> -->

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                             <!-- <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 滅菌率達99.99%
                             </div>
                             <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 最環保安全
                             </div> -->
                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234568</div>
                             <div class="caption"><a href="/LifeGoProduct">無痕美肌修復凝膠</a></div>

                             <div class="caption2">可撫平細紋疤痕 肌膚亮采透白</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>1,650</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">88</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 999
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption"><a href="/LifeGoProduct">健康御守 二氧化氯清淨錠套組</a></div>
                             <!-- <ul class="list_wrap">
                                 <li class="list">個人</li>
                                 <i></i>
                                 <li class="list">物品</li>
                                 <i></i>
                                 <li class="list">食品</li>
                                 <i></i>
                                 <li class="list">環境</li>
                                 <i></i>
                                 <li class="list">水質</li>
                                 <i></i>
                                 <li class="list">養生</li>
                             </ul> -->

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                             <!-- <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 滅菌率達99.99%
                             </div>
                             <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 最環保安全
                             </div> -->
                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234568</div>
                             <div class="caption">無痕美肌修復凝膠</div>

                             <div class="caption2">可撫平細紋疤痕 肌膚亮采透白</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>1,650</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">88</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 999
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                         </div>
                     </div>
                 </section>

                 <div style="margin-bottom: 80px;"></div>
                 <div class="text-center">
                     <h2 class="caption offer"><span><img src="/themes/Okmaster/img/lifego/star_blue.svg" alt=""
                                                          class=""></span>Offer
                         好康到
                     </h2>
                 </div>

                 <div style="margin-bottom: 20px;"></div>

                 <section class="slider offer regular">
                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>
                             <!-- <ul class="list_wrap">
                                 <li class="list">個人</li>
                                 <i></i>
                                 <li class="list">物品</li>
                                 <i></i>
                                 <li class="list">食品</li>
                                 <i></i>
                                 <li class="list">環境</li>
                                 <i></i>
                                 <li class="list">水質</li>
                                 <i></i>
                                 <li class="list">養生</li>
                             </ul> -->

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                             <!-- <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 滅菌率達99.99%
                             </div>
                             <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 最環保安全
                             </div> -->
                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234568</div>
                             <div class="caption">無痕美肌修復凝膠</div>

                             <div class="caption2">可撫平細紋疤痕 肌膚亮采透白</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>1,650</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">88</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 999
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>
                             <!-- <ul class="list_wrap">
                                 <li class="list">個人</li>
                                 <i></i>
                                 <li class="list">物品</li>
                                 <i></i>
                                 <li class="list">食品</li>
                                 <i></i>
                                 <li class="list">環境</li>
                                 <i></i>
                                 <li class="list">水質</li>
                                 <i></i>
                                 <li class="list">養生</li>
                             </ul> -->

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                             <!-- <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 滅菌率達99.99%
                             </div>
                             <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 最環保安全
                             </div> -->
                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234568</div>
                             <div class="caption">無痕美肌修復凝膠</div>

                             <div class="caption2">可撫平細紋疤痕 肌膚亮采透白</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>1,650</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">88</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 999
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                         </div>
                     </div>
                 </section>

                 <div style="margin-bottom: 80px;"></div>
                 <div class="text-center">
                     <h2 class="caption new"><span><img src="/themes/Okmaster/img/lifego/star_green.svg" alt=""
                                                        class=""></span>New
                         新貨色
                     </h2>
                 </div>
                 <div style="margin-bottom: 20px;"></div>

                 <section class="slider new regular">
                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>
                             <!-- <ul class="list_wrap">
                                 <li class="list">個人</li>
                                 <i></i>
                                 <li class="list">物品</li>
                                 <i></i>
                                 <li class="list">食品</li>
                                 <i></i>
                                 <li class="list">環境</li>
                                 <i></i>
                                 <li class="list">水質</li>
                                 <i></i>
                                 <li class="list">養生</li>
                             </ul> -->

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                             <!-- <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 滅菌率達99.99%
                             </div>
                             <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 最環保安全
                             </div> -->
                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234568</div>
                             <div class="caption">無痕美肌修復凝膠</div>

                             <div class="caption2">可撫平細紋疤痕 肌膚亮采透白</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>1,650</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">88</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 999
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>
                             <!-- <ul class="list_wrap">
                                 <li class="list">個人</li>
                                 <i></i>
                                 <li class="list">物品</li>
                                 <i></i>
                                 <li class="list">食品</li>
                                 <i></i>
                                 <li class="list">環境</li>
                                 <i></i>
                                 <li class="list">水質</li>
                                 <i></i>
                                 <li class="list">養生</li>
                             </ul> -->

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                             <!-- <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 滅菌率達99.99%
                             </div>
                             <div class="directions">
                                 <img src="./生活樂購_files/star_pink.svg" alt="" class="">
                                 最環保安全
                             </div> -->
                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234568</div>
                             <div class="caption">無痕美肌修復凝膠</div>

                             <div class="caption2">可撫平細紋疤痕 肌膚亮采透白</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>1,650</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">88</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 999
                             </div>

                         </div>
                     </div>

                     <div class="list">
                         <div class="img_frame"><a href="/LifeGoProduct"></a></div>
                         <div class="content_frame">
                             <div class="number">L1234567</div>
                             <div class="caption">健康御守 二氧化氯清淨錠套組</div>

                             <div class="caption2">消毒殺菌防疫最威殺菌率達99.9%</div>
                             <div class="heart_wrap">
                                 <div class="price_original">原價 <span>2,100</span> 元</div>
                                 <div class="heart">
                                     <img src="/themes/Okmaster/img/lifego/heart.svg" alt="">
                                     <div class="quantity">123</div>
                                 </div>
                             </div>

                             <div class="text-center price">
                                 $ 1,350
                             </div>

                         </div>
                     </div>
                 </section>
             </div>
         </div>
     </div>
 {include file="$tpl_dir./LifeGo_footer.tpl"}
    {$js}