<div class="main">
    {$Breadcrumb}
    {include file="$tpl_dir./page_head.tpl"}
<div class="store_name">
    <div class="title">{$store.title}</div>
    <i></i>
    <div class="category">
        <p class="" href="">{$store.store_type}</p>
    </div>
</div>
<div class="special_store_main_wrap">
    <hr>
    <br>
    <div class="row">
        <div class="col-sm-4 text-left">
            <div class="img_frame">
              {if sizeof($store1.img)==1}
                <img src="{$store.img["1"]}">
              {else}
                    <div id="myCarousel" class="carousel slide">
                      <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                          {$store_i=0}
                          {foreach $store1.img as $key => $value}
                            <li data-target="#myCarousel" data-slide-to="{$key}" {if $store_i==0}class="active"{/if}></li>
                          {$store_i=$store_i+1}
                          {/foreach}
                        </ol>
                        <!-- 轮播（Carousel）项目 -->
                        <div class="carousel-inner">
                          {$store_i=0}
                          {foreach $store1.img as $key => $value}
                          <div class="item{if $store_i==0} active{/if}">
                            <img src="{$value}" alt="First slide">
                          </div>
                          {$store_i=$store_i+1}
                          {/foreach}
                        </div>
                        <!-- 轮播（Carousel）导航 -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>
            {/if}
{*                <img src="/themes/Rent/img/storeintroduction/img_01.jpg" alt="" class="">*}
{*               <img src="/themes/Rent/img/storeintroduction/default.jpg" alt="" class=""> *}
            </div>
        </div>
        <div class="col-sm-4  text-center">
            <div class="store_content">
                <!-- <div class="title"></div>
                <div class="career"></div> -->
                <div class="store_info">
                    <img src="/themes/Rent/img/storeintroduction/icon_star.svg" alt="" class="">
                    店家資訊
                </div>
                <div class="list add">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/map_tag.svg">
                        ADD
                        <i></i>
                    </label>
                    <div class="content">{$store.address1}
                    </div>
                </div>

                <div class="list tel">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/phone.svg">
                        TEL
                        <i></i>
                    </label>
                    <div class="content">{$store.tel1}
                        {if $store.tel2}<br>{$store.tel2}{/if}
                    </div>
                </div>

                <div class="list time">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/time.svg">
                        TIME
                        <i></i>
                    </label>
                    <div class="content">{$store.open_time_w}
                    </div>
                </div>

                <div class="list hot">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/hot.svg">
                        HOT
                        <i></i>
                    </label>
                    <div class="content">{$store.service}
                        <br>{$store.service}
                    </div>
                </div>

                <div class="list offer">
                    <label>
                        <img class="icon" src="/themes/Rent/img/storeintroduction/offer.svg">
                        OFFER
                        <i></i>
                    </label>
                    <div class="content">{$store.discount}
                    </div>
                </div>
                <div class="popularity"><label><img class="icon" src="/themes/Rent/img/storeintroduction/star.svg">人氣星等</label><div class="div_popularity" data-id="{$store.id_store}">{$store.popularity}</div></div>
                {if !empty($smarty.session.id_member)}
                <div class="star"><label><img class="icon" src="/themes/Rent/img/storeintroduction/star.svg"></i>給星星</label><div class="div_star give_star" data-title="{$store.title}" data-id="{$store.id_store}" data-star="{$store.star}">{$store.star_img}</div></div>
                {/if}
{*                <div class="list popularity">*}
{*                    <label>*}
{*                        <img class="icon" src="/themes/Rent/img/storeintroduction/star.svg">*}
{*                        人氣星等*}
{*                        <i></i>*}
{*                    </label>*}
{*                    <div class="div_popularity" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2_gray.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2_gray.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2_gray.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2_gray.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2_gray.svg" data-id="">*}
{*                    </div>*}
{*                </div>*}
{*                <div class="list star">*}
{*                    <label>*}
{*                        <img class="icon" src="/themes/Rent/img/storeintroduction/star.svg">*}
{*                        給星星*}
{*                        <i></i>*}
{*                    </label>*}
{*                    <div class="div_star give_star" data-title="" data-id="" data-star="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2_gray.svg" data-id="">*}
{*                        <img src="/themes/Rent/img/storeintroduction/popularity2_gray.svg" data-id="">*}
{*                    </div>*}
{*                </div>*}
            </div>
        </div>
        <div class="col-sm-4 text-right">
            <div class="img_frame">
              {if sizeof($store2.img)==1}
                <img src="{$store.img["2"]}">
              {else}
              <div id="myCarousel" class="carousel slide">
                <!-- 轮播（Carousel）指标 -->
                  <ol class="carousel-indicators">
                    {$store_i=0}
                    {foreach $store2.img as $key => $value}
                    <li data-target="#myCarousel" data-slide-to="{$key}" {if $store_i==1}class="active"{/if}></li>
                    {/foreach}
                  </ol>
                  <!-- 轮播（Carousel）项目 -->
                  <div class="carousel-inner">
                    {$store_i=0}
                    {foreach $store2.img as $key => $value}
                    <div class="item{if $store_i==0} active{/if}">
                      <img src="{$value}" alt="First slide {$key}">
                    </div>
                    {$store_i=$store_i+1}
                    {/foreach}
                  </div>
                  <!-- 轮播（Carousel）导航 -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                  </a>
              </div>
              {/if}
{*                <img src="/themes/Rent/img/storeintroduction/img_02.jpg" alt="" class="">*}
                <!-- <img src="./生活好康-特約商店_files/default.jpg" alt="" class=""> -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center">
            <div class="main_img img_frame">
                <img src="{$store.img["0"]}">
{*                <img src="/themes/Rent/img/storeintroduction/img_03.jpg" alt="" class="">*}
            </div>
        </div>
    </div>

    <div class="introduction_wrap">
        <div class="caption">
            <img src="/themes/Rent/img/storeintroduction/icon_star.svg" alt="" class="">
            店家介紹
        </div>
       {$store.introduction}
    </div>
</div>
</div>
