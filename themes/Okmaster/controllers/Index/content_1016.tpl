
    <div class="top">
        <div class="top_L"><a href="####">登入</a> | <a href="####">註冊</a></div>
        <div class="top_R">

          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
          <g>
          	<g>
          		<path d="M501.333,96H10.667C4.779,96,0,100.779,0,106.667s4.779,10.667,10.667,10.667h490.667c5.888,0,10.667-4.779,10.667-10.667    S507.221,96,501.333,96z"/>
          	</g>
          </g>
          <g>
          	<g>
          		<path d="M501.333,245.333H10.667C4.779,245.333,0,250.112,0,256s4.779,10.667,10.667,10.667h490.667    c5.888,0,10.667-4.779,10.667-10.667S507.221,245.333,501.333,245.333z"/>
          	</g>
          </g>
          <g>
          	<g>
          		<path d="M501.333,394.667H10.667C4.779,394.667,0,399.445,0,405.333C0,411.221,4.779,416,10.667,416h490.667    c5.888,0,10.667-4.779,10.667-10.667C512,399.445,507.221,394.667,501.333,394.667z"/>
          	</g>
          </g>

          </svg>

        </div>
    </div>

    <div class="head_div">
        <div class="head_row_1">
    <div class="l"><div class="img"><img src="/themes/Okmaster/img/index/1090925_03.jpg"></div></div><div class="m">
      <div class="m_1"><div class="img"><img src="/themes/Okmaster/img/index/1090925_05.jpg"></div></div>
      <div class="m_2"><div class="img"><img src="/themes/Okmaster/img/index/1090925_10.jpg"></div></div></div>
      <div class="r"><div class="img"><img src="/themes/Okmaster/img/index/1090925_07.jpg"></div></div></div>
      <div class="head_row_2">
    <div class="l_b"><div class="img"><a href="https://www.okmaster.life/LifeGo" target="_blank"><img src="/themes/Okmaster/img/index/1090925_16.jpg"></a></div></div>
    <div class="r_b"><div class="img"><img src="/themes/Okmaster/img/index/1090925_18.jpg"></div></div></div>
    </div>

<!--
    <table class="head_tb" style="height: 197px;" width="303">
        <tbody>
        <tr>
            <td class="td_1 td_L" rowspan="2">
                <div><img src="/themes/Okmaster/img/index/1090917_03.png"></div>
            </td>
            <td class="td_2 td_T" colspan="2">
                <div><img src="/themes/Okmaster/img/index/1090917_09.png"></div>
            </td>
        </tr>
        <tr>
            <td class="td_1 td_M">
                <div><img src="/themes/Okmaster/img/index/1090917_16.png"></div>
            </td>
            <td class="td_1 td_R" rowspan="2">
                <div><img src="/themes/Okmaster/img/index/1090917_18.png"></div>
            </td>
        </tr>
        <tr>
            <td class="td_2 td_B" colspan="2">
                <div><img src="/themes/Okmaster/img/index/1090917_21.png"></div>
            </td>
        </tr>
        </tbody>
    </table>
  -->
    {$txt=array("健康宅檢測","樂租 智能門鎖","租吧借 物品 租 | 換 平台","好幫手 生活服務","生活達人")}
    {$txt_color=array("88BB23","755D3F","E60012","E60012","18B0C6")}
   {$img_name=array("1090923_01","1090923_03","1090923_04","1090923_05","1090923_06")}
  {$img_title=array("","","","","")}

    <ul>
      {for $i=0;$i<5;$i++ }
        <li>
            {if $img_title[$i]!=""}<img class="title" src="/themes/Okmaster/img/index/{$img_title[$i]}.png">{/if}<span style="color:#{$txt_color[$i]}">{$txt[$i]}</span>
            <img alt="{$txt[$i]}" src="/themes/Okmaster/img/index/{$img_name[$i]}.png">
        </li>
       {/for}
    </ul>

    <div class="news_row_title"><span>NEWS</span></div>
    <div class="news_row">

        <div class="news_row">
          {$img_arr=array("1090917_01","1090917_02","1090917_03","1090917_04","1090917_05")}
          {for $i=0;$i<5;$i++ }
            <div class="news_cols">
              <div class="items">
                <div class="photo"><img src="/themes/Okmaster/img/index/product/{$img_arr[$i]}.png"></div>
                <div class="content"><span class="news_cols title">無痕美肌修復凝勝</span>
                    <span class="news_cols context">可撫平細紋疤痕肌膚亮采透自</span></div>
                  </div>
            </div>
          {/for}

        </div>
    </div>

