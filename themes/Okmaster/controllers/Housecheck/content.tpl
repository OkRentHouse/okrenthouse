
<div class="main">

  {include file="$tpl_dir./page_head.tpl"}

<!-- <ol class="breadcrumb">
<li class="breadcrumb-item"><a href="https://okrent.house/">首頁</a></li>
<li class="breadcrumb-item active"><a href="../tax_benefit">租事順利</a></li>
<li class="breadcrumb-item active"><a>健康好宅</a></li> -->
</ol>
<!-- 健康宅檢測框架 -->
<div class="house_check_wrap">
<div class="banner"><a href="https://www.okmaster.life/">
  <img style="display: block; margin-left: auto; margin-right: auto;" src="/themes/Okmaster/img/page/Housecheck-3-banner.png" alt="" /></a></div>
<div class="content_block_wrap text-center">
<div class="text_wrap">
<h2 class="caption"><span class="title_h">全國第一家提供</span></h2>
<h2 class="caption"><span class="title_h">室內健康指數檢測服務</span></h2>

<p><span class="title_c">透過精密高端的檢測儀器 把關居住環境健康指數</span></p>
<p><span class="title_c">經由室内環境的健康認證 居住更安心</span></p>
<p>　</p><p>　</p>
<img src="../uploads/%E7%A7%9F%E4%BA%8B%E9%A0%86%E5%88%A9/%E5%81%A5%E5%BA%B7%E5%AE%85%E6%AA%A2%E6%B8%AC/house_check_main-1091106.jpg" alt="" /> <br /><br />

</div>
</div>
</div>
<div class="bottom message">
  <form action="/Housecheck" method="post" enctype="application/x-www-form-urlencoded" id="form_data">
    <input type="hidden" name="put_data" value="1">
    <input type="hidden" name="type" value="1">
    <table>
      <tr class="under_line"><td colspan="3" class="col-3"><label>留言</label></td><td colspan="1" class="col-1"></td></tr>
      <tr>
        <td colspan="1" class="col-1 right_line T_txt">姓名</td><!-- <label>*</label> -->
        <td colspan="2" class="col-2  left_line"><input type="text" name="name" placeholder="" class="under_line"></td>
        <td colspan="1" class="col-1">
        <input type="radio" name="sex" value="0" checked="true">女士 &nbsp&nbsp&nbsp
        <input type="radio" name="sex" value="1">先生</td></tr>
      <tr>
        <td colspan="1" class="col-1 right_line T_txt">手機</td><!-- <label>*</label> -->
        <td colspan="2" class="col-2 left_line"><input type="text" name="tel" placeholder="" class="under_line"></td>
        <td colspan="1" class="col-1">&nbsp&nbsp&nbsp</td></tr>
      <tr><td colspan="2" class="col-2"></td><td colspan="2" class="col-2"></td></tr>
      <!-- <tr><td class="col-1 right_line">方便連絡時間</td><td colspan="3" class="col-3 left_line">
        <input type="radio" name="time" value="0" checked="true">上午 &nbsp&nbsp&nbsp
        <input type="radio" name="time" value="1">下午 &nbsp&nbsp&nbsp
        <input type="radio" name="time" value="2">晚上 &nbsp&nbsp&nbsp
        <input type="radio" name="time" value="3">皆可
      </td></tr> -->
      <tr><td colspan="1" class="right_line T_txt">留言</td>
        <td colspan="3" class="col-3 left_line"><label class="nomral">(限100個字以內)</label></td></tr>
      <tr><td colspan="4" class="col-4"><textarea maxlength="100" name="message"></textarea></td></tr>
      <tr><td colspan="4" class="col-4 readme"><input type="checkbox" name="agree" value="1">我已閲讀並同意生活好科技的【服務條款】與【隱私權聲明】及【會員同意條款】</td></tr>
      <tr><td colspan="4" class="col-4" style="    text-align: center;">
        <button type="button" class="submit"  data-type="3">取消</button>
        <button type="button" class="submit" id="tel_submit"  data-type="2">取得驗證碼</button>
        <input type="text" name="captcha"  placeholder="" class="under_line check_code" style="border-bottom: 1px solid #5dabbc !important;">
        <button type="button" class="submit" id="put_data"  data-type="1">送出</button>
      </tr>
    </table>
  </form>

</div>
{include file="$tpl_dir./page_footer.tpl"}

</div>

{$js}
