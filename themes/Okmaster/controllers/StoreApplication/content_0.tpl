<form action="?application_page=1" method="post">
  <div class="row form_row">
    <div class="cell-12">
      <div class="row form_title_row">
        <div class="cell-1"></div>
        <div class="cell-11 form_title"><span>生活好康特約商店註冊申請書</span></div>
      </div>
      
      {$input=array("店家名稱","店址","電話","營業時間","店休日","商品服務","好康優惠","店家介紹","官網")}
      {foreach $input as $n => $x}
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          {if $x=="店家名稱"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input"><input type="text" value="{$smarty.session.store_name_new}" id="store_name_new" name="store_name_new"  required ></div>
          </div>
          {else if $x=="店址"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input_list">
            <input type="text" id="store_address_postcode" name="store_address_postcode" maxlength="6" value="{$smarty.session.store_address_postcode}" placeholder="郵遞區號" onkeyup="value=value.replace(/[^\d]/g,'')">
            <input type="text" id="store_address_city" name="store_address_city" value="{$smarty.session.store_address_city}" placeholder="縣市">
            <input type="text" id="store_address_sub" name="store_address_sub" value="{$smarty.session.store_address_sub}" placeholder="鄉鎮市區"></div>
          </div>
          <div class="row">
            <div class="cell-4 title"></div>
            <div class="cell-8 input"><input type="text" id="store_address_details" name="store_address_details" value="{$smarty.session.store_address_details}" placeholder="詳細地址"></div>
          </div>
          {else if $x=="電話"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input"><input type="text" id="store_phone" name="store_phone" maxlength="10" value="{$smarty.session.store_phone}" placeholder="手機號碼或市話" onkeyup="value=value.replace(/[^\d]/g,'')">
              {* <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="mutileplus svg-inline--fa fa-plus fa-w-14 fa-3x"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z" class=""></path></svg> *}
            </div>
          </div>
          {else if $x=="營業時間"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input_list"><input type="time" id="store_open_time" name="store_open_time" value="{$smarty.session.store_open_time}"> <svg style="width: 1rem;" aria-hidden="true" focusable="false" data-prefix="far" data-icon="wave-sine" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-wave-sine fa-w-20 fa-3x"><path fill="currentColor" d="M628.41 261.07L613 256.63a15.88 15.88 0 0 0-19.55 10.16C572.85 329.76 511.64 432 464 432c-52.09 0-87.41-93.77-121.53-184.45C302.56 141.58 261.31 32 176 32 87.15 32 17.77 178.46.78 230.69a16 16 0 0 0 10.81 20.23L27 255.36a15.87 15.87 0 0 0 19.55-10.15C67.15 182.24 128.36 80 176 80c52.09 0 87.41 93.77 121.53 184.45C337.44 370.42 378.69 480 464 480c88.85 0 158.23-146.46 175.22-198.7a16 16 0 0 0-10.81-20.23z" class=""></path></svg>
              <input type="time" id="store_close_time" name="store_close_time" value="{$smarty.session.store_close_time}"></div>
          </div>
          {else if $x=="店休日"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input"><input type="text" id="store_closed" name="store_closed" value="{$smarty.session.store_closed}"></div>
          </div>
          {else if $x=="商品服務"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input"><input type="text" id="store_product" name="store_product" value="{$smarty.session.store_product}"></div>
            <div class="cell-8" style="margin-left: 33.7%;padding: 0px;margin-bottom: 10px;margin-top: -6px;"><span style="color:#c50606;">範例:超立淨環境消毒服務,營業場所殺菌,二氧化氯滅菌產品</span></div>
          </div>
          {else if $x=="好康優惠"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input"><input type="text" id="store_coupons" name="store_coupons" placeholder="持生活好康電子卡可享：" value="{$smarty.session.store_coupons}"></div>
          </div>
          {else if $x=="店家介紹"}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input textarea"><textarea id="store_details" name="store_details" value="">{$smarty.session.store_details}</textarea>
            </div>
          </div>
          {else}
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input"><input type="text" id="store_website" name="store_website" value="{$smarty.session.store_website}"></div>
          </div>
          {/if}
        </span></div>
        <div class="cell-3"></div>
      </div>
      {/foreach}
      <div class="row form_submit">
        <div class="cell-12">
        <input type="hidden" id="t1" name="t1" value="1">
        <button class="carousel" type="submit"> Next <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg></button>
        </div>
      </div>
      <div class="row form_end">
        <div class="cell-12"></div>
      </div>
      
    </div>
    
  </div>

</div>
</form>
    {include file="$tpl_dir./controllers/StoreApplication/process.tpl"}
