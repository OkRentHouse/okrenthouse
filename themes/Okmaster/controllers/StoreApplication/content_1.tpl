<style>
.carousel {
  margin: auto 2rem;
}

</style>
<form action="?application_page=2" method="post">
  <div class="row form_row">
    <div class="cell-12">
      <div class="row form_title_row">
        <div class="cell-1"></div>
        <div class="cell-11 form_title"><span>店家資訊</span></div>
      </div>

      {$input=array("連鎖類別","商號登記","法定代理人","登記地址","總公司電話","經辦聯絡人","聯絡手機","請輸入驗證碼","Email")}
      {$input2=array("store_category","store_business_number","store_law_rep","store_regi_address","store_tel","touch_people","store_mobile","","store_email")}
      {$input3=array({$smarty.session.store_category},{$smarty.session.store_business_number},{$smarty.session.store_law_rep},{$smarty.session.store_regi_address},{$smarty.session.store_tel},{$smarty.session.touch_people},{$smarty.session.store_mobile},"",{$smarty.session.store_email})}
      {foreach $input as $n => $x}
      {if $x=="聯絡手機"}
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8">
                <div class="row" style="margin-left: -5px;">
                  <div class="cell-8 input"> <input id="user" data-role="none" class="floating_input" name="user" type="text"
                               placeholder="手機號碼" value="{$smarty.post.user}" minlength="6" maxlength="10" onkeyup="value=value.replace(/[^\d]/g,'') " required > </div>
                  <div class="cell-4" style="padding: 0 1rem;"><button type="button" id="tel_code_submit" class="btn ">取得驗證碼</button></div>
                </div>
              </div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      {else if $x=="請輸入驗證碼"}
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8">
                <div class="row" style="margin-left: -5px;">
                  <div class="cell-8 input"> <input id="captcha" data-role="none" class="floating_input" name="captcha" type="text"
                               placeholder="手機號碼" value="{$smarty.post.user}" minlength="6" maxlength="10" onkeyup="value=value.replace(/[^\d]/g,'') " required > </div>
                  <div class="cell-4" style="padding: 0 1rem;"></div>
                </div>
              </div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      {else if $x=="連鎖類別"}
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input">
              <select class="input" id="{$input2[$n]}" name="{$input2[$n]}" value="{$input3[$n]}">
                {if $select_type}
                {foreach $select_type as $i => $v}
                <option value="{$i}">
                  {$v.store_type}
                </option>
                {/foreach}
                {/if}
              </select></div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      {else}
      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label>{$x}</label></div>
            <div class="cell-8 input"><input type="text" id="{$input2[$n]}" name="{$input2[$n]}" value="{$input3[$n]}"></div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>
      {/if}
      {/foreach}


      <div class="row form_body">
        <div class="cell-3"></div>
        <div class="cell-6"><span>
          <div class="row">
            <div class="cell-4 title"><label>密碼</label></div>
            <div class="cell-8 input"><input type="text" id="password" name="password"></div>
            <div class="cell-4 title"></div>
            <div><span style="color:red">＊生活好康商店管理登入之用途</span></div>
          </div>
        </span></div>
        <div class="cell-3"></div>
      </div>


      <div class="row form_submit">
      <input type="hidden" id="t2" name="t2" value="2">
        <div class="cell-12"><button class="carousel"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg> <a href="?application_page=0&s=0" target="_self">Previous</a> </button>
          <button class="carousel" type="submit"> Next <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg></button></div>
      </div>
      <div class="row form_end">
        <div class="cell-12"></div>
      </div>
    </div>
  </div>
</form>
</div>
    {include file="$tpl_dir./controllers/StoreApplication/process.tpl"}
