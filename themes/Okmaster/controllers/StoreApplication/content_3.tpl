
<style>
.carousel {
  margin: auto 2rem;
}

.form_submit {
  display: none;
}

</style>
<form action="?application_page=4" method="post">
  <div class="row form_row">
    <div class="cell-12">


      <div class="row form_body">
        <div class="cell-0"></div>
        <div class="cell-12 agree">
          <img style="width: 100%;" src="\themes\Okmaster\img\storeapplication\page_3.jpg">
          <input type="checkbox" id="i_agree"  class="i_agree"> 我已詳細閱讀並同意上述內容
        </div>
        <div class="cell-0"></div>
      </div>

      <div class="row form_submit">
      <input type="hidden" id="t4" name="t4" value="4">
        <div class="cell-12"><button class="carousel"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-3x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>
          <a href="?application_page=2&s=0" target="_self">Previous</a> </button>
          <button class="carousel">
            <a href="?application_page=4" target="_self">Next</a> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-3x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg></button></div>
      </div>
      <div class="row form_end">
        <div class="cell-12"></div>
      </div>
    </div>
  </div>
</form>
</div>
    {include file="$tpl_dir./controllers/StoreApplication/process.tpl"}


<script>
$("#i_agree").on("click",function(){
    if($("#i_agree").prop("checked")){
        $(".form_submit").show(500,function(){
            window.scrollTop(9999)
        })
    }else{
        $(".form_submit").hide(100)
    }

})
</script>
