
<div class="form_footer" style="">
  <div class="row">
    <div class="cell-2"></div>
    <div class="cell-8"><span>
      <div class="row">
        <div class="cell-3 title"><label>填表進度</label></div>
        <div class="cell-9 Circle">
          <div class="row">
          <label class="cell-1 circle {if $application_process>=1}pass{/if}"></label><label class="cell-4 line{if $application_process>=1} pass{/if}"></label>
          <label class="cell-1 circle {if $application_process>=2}pass {/if}number">50</label><label class="cell-4 line{if $application_process>=2} pass{/if}"></label>
          <label class="cell-1 circle {if $application_process>=3}pass {/if}number">100</label>
          </div>
        </div>
      </div>
    </span></div>
    <div class="cell-2"></div>
  </div>
</div>
