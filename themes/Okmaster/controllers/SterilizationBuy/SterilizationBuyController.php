<?php

namespace web_okmaster;
use \OkmasterController;
use \FrontController;
use \Db;
use \Tools;
use \SMS;
use \FileUpload;
use \Context;

class SterilizationBuyController extends OkmasterController
{
    public $page = 'SterilizationBuy';

//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->page_header_toolbar_title = $this->l('首頁');
		$this->className                 = 'SterilizationBuyController';
//		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
        //$this->conn ='ilifehou_okmaster';
        $this->conn ='ilifehou_ilife_house';
        $this->table = 'SterilizationBuy_Product';
        
		parent::__construct();
  }
  
  
  public function validateRules(){    //驗證基本資料的輸入

    $captcha = Tools::getValue('captcha');

    $sql = "select captcha from sms_log where now() < SUBDATE(create_time,interval -10 minute) AND tel=".GetSQL($user, 'text');
    $row    = Db::rowSQL($sql, true);
    $captcha_log = $row['captcha']; //驗證碼


    if (empty($captcha)) {
        //$this->_errors[] = $this->l('您必須輸入驗證碼');
        echo "<script>window.alert('您必須輸入驗證碼');</script>";
    }else if($captcha != $captcha_log){
        //$this->_errors[] = $this->l('輸入驗證碼錯誤');
        echo "<script>window.alert('輸入驗證碼錯誤');javascript:location.reload();</script>";

    }
    

    parent::validateRules();
    
}

	public function initProcess()
	{

    $sql = "SELECT * FROM SterilizationBuy_Product ORDER BY ProductID ASC";
    $product = Db::rowSQL($sql);

    //print_r(array_values($product));
    
    $productID = $product[0]["ProductNumber"];
    $productNumber = $product[0]["ProductNumber"];
    $productTitle = $product[0]["ProductTitle"];
    $productInfo = $product[0]["ProductInfo"];
    $productDescription = $product[0]["ProductDescription"];
    $productPrice = $product[0]["ProductPrice"];
    $productBonus = $product[0]["ProductBonus"];

    


    


        $js =<<<js
<script>
$(document).ready(function(){
  $("#_da").click(function(){
    $(".dialogbox").show(100);
    $(".dialogboxbk").show(100);
  });
  $("#cancel").click(function(){
    $(".dialogbox").hide(300);
    $(".dialogboxbk").hide(300);
  });
  $(".upload").click(function(){
      $(".fileupload").click();
  });
})
</script>
js;
        $Breadcrumb                = "";//'首頁 > 生活好科技 > 清淨對策 > 關於我們';
        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/Aboutokmaster/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data='<div class="footer"><img src="/themes/Okmaster/img/Aboutokmaster/footer_logo.png">生活好科技有限公司 &nbsp; LIFE  MASTER  TECHNOLOGY  COMPANY &nbsp; 生活好科技·科技好生活</div>';
        $footer_data='';

    $order="";
    $order_price="";
    if($_POST['pr_q']){
      $order=$_POST['pr_q'];
      $order_price=$_POST['pr_price']; // 對應 index.pl 裡面的 pr_price

    }

    function checkId($id) {
      // 去空白&轉大寫
      $id = strtoupper(trim($id));

      // 英文字母與數值對照表
      $alphabetTable = [
          'A' => 10, 'B' => 11, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15, 'G' => 16,
          'H' => 17, 'I' => 34, 'J' => 18, 'K' => 19, 'L' => 20, 'M' => 21, 'N' => 22,
          'O' => 35, 'P' => 23, 'Q' => 24, 'R' => 25, 'S' => 26, 'T' => 27, 'U' => 28,
          'V' => 29, 'X' => 30, 'Y' => 31, 'Z' => 33
      ];

      // 檢查身份證字號格式
      // ps. 第二碼的例外條件ABCD，在這裡未實作，僅提供需要的人參考，實作方式是A對應10，只取個位數0去加權即可
      // 臺灣地區無戶籍國民、大陸地區人民、港澳居民：
      // 男性使用A、女性使用B
      // 外國人：
      // 男性使用C、女性使用D
      if (!preg_match("/^[A-Z]{1}[12ABCD]{1}[0-9]{8}$/", $id)){
          // ^ 是開始符號
          // $ 是結束符號
          // [] 中括號內是正則條件
          // {} 是要重複執行幾次
          throw new Exception('格式、長度錯誤'); 
      }

      // 切開字串
      $idArray = str_split($id);

      // 英文字母加權數值
      $alphabet = $alphabetTable[$idArray[0]];
      $point = substr($alphabet, 0, 1) * 1 + substr($alphabet, 1, 1) * 9;

      // 數字部分加權數值
      for ($i = 1; $i <= 8; $i++) {
          $point += $idArray[$i] * (9 - $i);
      }
      $point = $point + $idArray[9];

      return $point % 10 == 0 ? true : false;
  }


    $getDate= date("Ymd"); //訂單編號 OrderID2
    $randnew = rand(1000,9999);//生成隨機的4碼 OrderID2 用 $getDate + $randnew 串起來

    $paymethod = $_POST['pay'];

    $db_link = mysqli_connect("localhost", "ilifehou_ilife", "S^,HO%bh^$&NF3dab", "ilifehou_ilife_house");

    $captcha = Tools::getValue('captcha');

        



    if($_POST['checkorder'] == "1"){

      $this->validateRules();

      if($_POST['payway'] == "1"){
        $insert_sql = "INSERT INTO 
        SterilizationBuy_Order (`OrderID2`,`ProductID`,`OrderUserName`,`OrderUserEmail`,`OrderUserPhone`,`OrderUserAddress`,`OrderUserIdentity`,`OrderEachPrice`,`OrderEachAmount`,`OrderTotalPrice`,`OrderMessage`,`OrderTotalAmount`,`OrderMuiltPrice`,`CreditCard`,`PCYM`,`CVS`,`PaymentATMPeriod`,`PaymentATMMoney`,`PaymentType`,`PaymentFlag`,`PickUpStore`,`PickUpName`,`PickUpPhone`,`PickUpRecipt`,`PickupReciptName`,`PickupReciptNum`) 
        VALUES 
        ('$getDate$randnew','$_POST[productID]','$_POST[username]','$_POST[email]','$_POST[user]','$_POST[address]','$_POST[Identity]','$_POST[productPrice]','$_POST[order]','$_POST[totalPrice]','$_POST[message]','$_POST[order_item_q]','$_POST[order_price_total]','$_POST[cd1]-$_POST[cd2]-$_POST[cd3]-$_POST[cd4]','$_POST[yearmonth]','$_POST[cvs]','0','0','$_POST[payway]','0','$_POST[delway]','$_POST[pickusername]','$_POST[pickphone]','$_POST[receipt]','$_POST[receiptname]','$_POST[receiptname]')";
      } else if($_POST['payway'] == "2" || $_POST['payway'] == "3" || $_POST['payway'] == "5"){
        $insert_sql = "INSERT INTO 
        SterilizationBuy_Order (`OrderID2`,`ProductID`,`OrderUserName`,`OrderUserEmail`,`OrderUserPhone`,`OrderUserAddress`,`OrderUserIdentity`,`OrderEachPrice`,`OrderEachAmount`,`OrderTotalPrice`,`OrderMessage`,`OrderTotalAmount`,`OrderMuiltPrice`,`CreditCard`,`PCYM`,`CVS`,`PaymentATMPeriod`,`PaymentATMMoney`,`PaymentType`,`PaymentFlag`,`PickUpStore`,`PickUpName`,`PickUpPhone`,`PickUpRecipt`,`PickupReciptName`,`PickupReciptNum`) 
        VALUES 
        ('$getDate$randnew','$_POST[productID]','$_POST[username]','$_POST[email]','$_POST[user]','$_POST[address]','$_POST[Identity]','$_POST[productPrice]','$_POST[order]','$_POST[totalPrice]','$_POST[message]','$_POST[order_item_q]','$_POST[order_price_total]','0','0','0','0','0','$_POST[payway]','0','$_POST[delway]','$_POST[pickusername]','$_POST[pickphone]','$_POST[receipt]','$_POST[receiptname]','$_POST[receiptname]')";
       
      }  else if($_POST['payway'] == "4") {
        $insert_sql = "INSERT INTO 
        SterilizationBuy_Order (`OrderID2`,`ProductID`,`OrderUserName`,`OrderUserEmail`,`OrderUserPhone`,`OrderUserAddress`,`OrderUserIdentity`,`OrderEachPrice`,`OrderEachAmount`,`OrderTotalPrice`,`OrderMessage`,`OrderTotalAmount`,`OrderMuiltPrice`,`CreditCard`,`PCYM`,`CVS`,`PaymentATMPeriod`,`PaymentATMMoney`,`PaymentType`,`PaymentFlag`,`PickUpStore`,`PickUpName`,`PickUpPhone`,`PickUpRecipt`,`PickupReciptName`,`PickupReciptNum`) 
        VALUES 
        ('$getDate$randnew','$_POST[productID]','$_POST[username]','$_POST[email]','$_POST[user]','$_POST[address]','$_POST[Identity]','$_POST[productPrice]','$_POST[order]','$_POST[totalPrice]','$_POST[message]','$_POST[order_item_q]','$_POST[order_price_total]','0','0','0','$_POST[ped]','0','$_POST[payway]','0','$_POST[delway]','$_POST[pickusername]','$_POST[pickphone]','$_POST[receipt]','$_POST[receiptname]','$_POST[receiptname]')";
       
      } 

      //if(isset($_POST['submit'])){
        $to = $_POST['email']; // this is your Email address
        $from = $_POST['email2']; // this is the sender's Email address
        $first_name = "123";
        $last_name = "456";
        $subject = "Form submission";
        $subject2 = "Copy of your form submission";
        $message = $first_name . " " . $last_name . " wrote the following:" . "\n\n" . $_POST['message'];
        $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $_POST['message'];
    
        $headers = "From:" . $from;
        $headers2 = "From:" . $to;
        mail($to,$subject,$message,$headers);
        //mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
        //echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
        // You can also use header('Location: thank_you.php'); to redirect to another page.
        //}

      // $eemail = $_POST['email'];
      // $UserName = $_POST['username'];
      // $headers = "From: ok@lifegroup.house"; //寄件者
      // $title = "您的訂單已完成 \n\n";
      // $title .= "這封信來自生活好科技 \n\n";
      // $title .= "您所購買的 超立淨 隨身寶禮盒體驗組 目前已完成訂單  \n\n";
      // $title .= "---------------------------------------------------------------------- \n\n";
      // $title .= "                            重要！ \n\n";
      // $title .= "---------------------------------------------------------------------- \n\n";
      // $title .= " \n\n";
      // $title .= "---------------------------------------------------------------------- \n\n";
      // $title .= "                 \n\n";
      // $title .= "---------------------------------------------------------------------- \n\n";
      // $title .= "訂單查詢 \n\n";
      // $title .= "https://www.okmaster.life/SterilizationBuy?order=search \n\n";
      // $title .= "生活好科技管理群組 \n\n";
      // mail("ben@lifegroup.com","您的訂單已完成 ",$title, "$headers");
      // if(mail("$eemail","您的訂單已完成 ",$title, "$headers")){
      //   echo "信件已經發送成功。";//寄信成功就會顯示的提示訊息
      // }else{
      //   echo "信件發送失敗！";//寄信失敗顯示的錯誤訊息
      // }

    

      $db_link->query($insert_sql);
      $order_last_id = mysqli_insert_id($db_link);

      $order_confirm = "order_confirm";

      $sql_lastid = "SELECT OrderID2 FROM SterilizationBuy_Order where OrderID = '".$order_last_id."'";
      $order_last = Db::rowSQL($sql_lastid);
      
      $orderID2 = $order_last[0]["OrderID2"];

        $this->context->smarty->assign([
          'order_confirm' => $order_confirm,
          'orderID2' => $orderID2,
        ]);


    } else {

    }

    $order_search = Tools::getValue('order');
    if($order_search == "search"){
      if($_POST['oocheck'] == '1'){

        $searchphone = $_POST['searchphone'];
        $searchorderid = $_POST['searchorderid'];

        $sql_order_search = "SELECT * FROM SterilizationBuy_Order where OrderUserPhone='".$searchphone."' and OrderID2 ='".$searchorderid."' and PaymentFlag <> '2'";
        $search_details = Db::rowSQL($sql_order_search);
        //print_r($sql_order_search);

        
        $detail_orderID2 = $search_details[0]["OrderID2"];

        if($detail_orderID2 == ''){
          echo "<script>window.alert('未找尋到此訂單');</script>";
          $this->context->smarty->assign([
            'order_search' => $order_search,
        ]);
        }
        $search_productid = $search_details[0]["ProductID"];
        $detail_OrderCreateTime = $search_details[0]["OrderCreateTime"];
        $detail_OrderUserName = $search_details[0]["OrderUserName"];
        $detail_OrderUserPhone = $search_details[0]["OrderUserPhone"];
        $detail_PaymentFlag = $search_details[0]["PaymentFlag"];
        $detail_OrderTotalAmount = $search_details[0]["OrderTotalAmount"];
        $detail_OrderTotalPrice = $search_details[0]["OrderTotalPrice"];
        

        if ($detail_PaymentFlag == "0"){
          $detail_PaymentFlag = "未付款";
        } else {
          $detail_PaymentFlag = "已付款";
        }
        $detail_PickUpStore = $search_details[0]["PickUpStore"];
        if ($detail_PickUpStore == "1"){
          $detail_PickUpStore = "宅配";
        } else if ($detail_PickUpStore == "2"){
          $detail_PickUpStore = "郵寄";
        } else if ($detail_PickUpStore == "3"){
          $detail_PickUpStore = "7-11";
        } else if ($detail_PickUpStore == "4"){
          $detail_PickUpStore = "全家";
        } else if ($detail_PickUpStore == "5"){
          $detail_PickUpStore = "萊爾富";
        } else if ($detail_PickUpStore == "6"){
          $detail_PickUpStore = "OK 超商";
        } else if ($detail_PickUpStore == "7"){
          $detail_PickUpStore = "超商取貨";
        } else {
          $detail_PickUpStore = "未定義";
        }
        $detail_OrderMessage = $search_details[0]["OrderMessage"];


        $sql_product_detail = "SELECT * FROM SterilizationBuy_Product where ProductNumber='".$search_productid."'";
        $produce_details = Db::rowSQL($sql_product_detail);
        //print_r($sql_product_detail);
        
        $product_title = $produce_details[0]["ProductTitle"];
        $product_info = $produce_details[0]["ProductInfo"];
        $product_price = $produce_details[0]["ProductPrice"];

        $order_search_confire = "order_search_confire";
          $this->context->smarty->assign([
            'order_search_confire' => $order_search_confire,
            'detail_orderID2' => $detail_orderID2,
            'product_title' => $product_title,
            'product_info' => $product_info,
            'product_price' => $product_price,
            'detail_OrderTotalPrice' => $detail_OrderTotalPrice,
            'detail_OrderTotalAmount' => $detail_OrderTotalAmount,
            'detail_OrderCreateTime' => $detail_OrderCreateTime,
            'detail_OrderUserName' => $detail_OrderUserName,
            'detail_OrderUserPhone' => $detail_OrderUserPhone,
            'detail_PaymentFlag' => $detail_PaymentFlag,
            'detail_PickUpStore' => $detail_PickUpStore,
            'detail_OrderMessage' => $detail_OrderMessage,
        ]);
      } else {
        $this->context->smarty->assign([
          'order_search' => $order_search,
      ]);
      }
    }    

    if($_POST['ccancel'] == "OK"){
      $sql_order_delete = "Update SterilizationBuy_Order SET PaymentFlag ='2' where OrderID2 ='".$_POST['cancel']."'";
      $order_delete = Db::rowSQL($sql_order_delete);
      //print_r($sql_order_delete);
      //echo "<script>window.alert('訂單已取消');</script>";
      $order_cancel = "order_cancel";
      $this->context->smarty->assign([
        'order_cancel' => $order_cancel,
    ]);
    
    }


    
    


		$this->context->smarty->assign([
            'slick_data'    =>['class'=>'slider_center','slidesToShow'=>'7','slidesToScroll'=>'7',],
            'footer_data'   =>$footer_data,
            'PGWD'   => $_SERVER['QUERY_STRING'],
            'Breadcrumb'   => $Breadcrumb,
            'js' =>$js,
            'order' => $order,
            'productID' => $productID,
            'productNumber' => $productNumber,
            'productTitle' => $productTitle,
            'productInfo' => $productInfo,
            'productBonus' => $productBonus,
            'productDescription' => $productDescription,
            'productPrice' => $productPrice,
            'order_price' => $order_price,
            'order_item_q' => sizeof($_POST['pr_q']),
            'order_price_total' => $order_price*$order,
    ]);
    


		parent::initProcess();
	}

	public function setMedia()
	{
		parent::setMedia();
        $WD = substr(strtok(strchr($_SERVER['QUERY_STRING'],"&"),"="),1);
        $PGWD = $_SERVER['QUERY_STRING'];

        $this->addJS('/js/metro.min.js');
        $this->addCSS('/css/font-awesome.css');
        $this->addCSS('/css/fontawesome.css');
        $this->addCSS('/css/all.css');
        $this->addCSS( THEME_URL .'/css/'.$PGWD.'.css');
        $this->addJS(THEME_URL .'/message/message.js');
        $this->addCSS( THEME_URL .'/css/all_page.css');
        $this->addCSS( THEME_URL .'/css/SterilizationBuy.css');

        $this->addCSS( '/themes/LifeHouse/css/life_go_tab.css');
        $this->addCSS( '/themes/LifeHouse/css/life_go_main.css');
        $this->addCSS( '/themes/LifeHouse/css/lifegoproduct.css');
        $this->addCSS( '/themes/LifeHouse/css/main.css');
        $this->addCSS('/css/metro-all.min.css');



	}
}
