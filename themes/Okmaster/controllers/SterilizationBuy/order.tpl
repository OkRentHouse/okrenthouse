<style>

.border,
.file, .input, .select, .spinner, .tag-input, .textarea, input[type=datetime-local], input[type=email], input[type=file], input[type=month], input[type=number], input[type=password], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week], select, textarea,
.row_1 img {
    border: 2px solid #dfdfdf;
}

label.h4 {
    margin-top: 2em;
    color: #66c3b7;
}

.shopping {
    margin: 5em auto !important;
}

.shopping * {
    border-radius: 10px;
    margin: 0 1em;
    height: 26pt;
}

.shopping input, .shopping button {
    border: 3px solid #66C3B7;
    /* background: #fff; */
    padding: .3em;
    line-height: initial;
    width: 10em;
}

.shopping {
    display: flex;
    line-height: 28pt;
    max-width: fit-content;
    margin: auto;
}

.shopping input {
    padding: .5em;
}

.shopping button, .row_3 [type=button], .row_4 [type=button] {
    border: 3px solid #66C3B7;
    background: #66C3B7;
    color: #FFF;
}

.yellow[type=button] {
    border: 3px solid yellow;
    background: yellow;
    color: #333;
}

.shopping button:hover, .row_3 [type=button]:hover, .row_4 [type=button]:hover {
    background: #FFF;
    color: #66C3B7;
}

.yellow[type=button]:hover {
  background: #333;
  color: yellow;
}

.col-10.main {
    margin-top: 2em;
}

.row_1_div {
    width: 10%;
    text-align: center;
}

.row_1 *, .row_2 input, .row_3 input, .row_4 input, .row_5 input {
    border-radius: 8px;
}

.row_1 span, .row_2 span  {
    font-weight: bolder;
}

.row_1 {
  padding: 5%;
}

.row_1 .col-6.gap-24:first-child {
    padding-left: 0px !important;
}

.col-6.gap-24:first-child input[type="text"] {
    width: 90%;
}

.row_2 {
    padding: 1em;
}

.row_1 img {
    border: 1px solid #ccc;
    width: 100px;
    height: 100px;
}

.p_name {
  width: 30%;
}

.p_name input::placeholder {

}

.p_name label {
    color: orange;
    position: absolute;
    top: 1.9em;
}

.p_name .disabled {
    background: white !important;
}

.row_b {
    display: flex;
}

.row.b div {
    display: -webkit-box;
    line-height: 28pt;
}
.row_3 span {
    line-height: 38px;
}

.row_3 .col-6 > div, .row_3 span {
    padding: 0px;
}

.row_3 [type=button], .row_4 [type=button] {
    margin: 0px 1em !important;
    height: fit-content;
}

.row_3 input[type="radio"], .row_4 input[type="radio"], .row_5 input[type="radio"] {
    margin: 0px .3em !important;

}

.row_4 img {
  height: 32px;
  width: auto;
}

.row_4 span {
  height: 32px;
  line-height: 21px;
}

.row_4 .row .col-10 input[type="radio"], .row_4 .row .col-10 input[type="checkbox"], .row_5 .row .col-10 input[type="radio"] {
  height: 25px;
  margin-right: 1em;
  margin-left: .3em;
}

.row_4.border .row .col-10, .row_5.border .row .col-10 {
    padding: 0px;
}

.row_4.border .row input.col-sm, .row_5.border .row input.col-sm {
    max-height: 1.5em;
    margin: 0 .5em;
}

.row.b div input[type="text"],.row_2 input[type="text"],.row_3 input[type="text"] {
    width: 70%;
    margin: 0 .5em;
}


.row_2 > .col-6.gap-24 {
    padding: .3em 1em !important;
    padding-left: 2em !important;
}

</style>

<script>
function check(){

  const nameElement = document.getElementById("Identity");
  const IdentityID = nameElement.value;
  //alert(IdentityID);

  if (!checkID( document.form.Identity.value))
      window.alert( "身份證字號錯誤!" );
  

    if(form.payway.value==""){
      alert('請選擇付款方式');
      return false;
    }
    if(form.payway.value=="1"){
        if(form.cd1.value== ""){
          alert("請填寫信用卡號碼");
          return false;
        } else if(form.cd2.value== ""){
          alert("請填寫信用卡號碼");
          return false;
        } else if(form.cd3.value== ""){
          alert("請填寫信用卡號碼");
          return false;
        } else if(form.cd4.value== ""){
          alert("請填寫信用卡號碼");
          return false;
        } else if(form.yearmonth.value== ""){
          alert("請填寫信用卡有效日期");
          return false;
        } else if(form.cvs.value== ""){
          alert("請填寫信用卡 CVS 號碼");
          return false;
        }
      return true;
    }
    //if(form.payway.value=="3"){
      //if(form.ped.value== ""){
          //alert("請選擇要分期的期數");
          //return false;
        //} 
        //return true;
    //}

    if(form.payway.value=="4"){
      //if(form.delway.value== "7" ){
          alert("請選擇要取貨的方式");
          return false;
      //} 
    } 

    if(form.delway.value== "7"){
        if(form.delwaystore.value== ""){
          alert("請選擇超商店家");
          return false;
        }
        return true;
    }

    //delwaystore

    if(form.receipt.value==""){
      alert('請選擇所取發票資料方式');
      return false;
    } 

    //if(form.receipt.value == "2"){
        //if(form.receiptnum.value== ""){
          //alert("請填寫載具");
          //return false;
        //}
    //}
    if(form.receipt.value =="3"){
      if(form.receiptname1.value== ""){
          alert("請填寫公司名稱");
          return false;
        } else if(form.receiptnum1.value== ""){
          alert("請填寫統一編號");
          return false;
        }
      return true;
    }
    
}

function checkID(Identity) {
   tab = "ABCDEFGHJKLMNPQRSTUVWXYZIO"
   A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
   A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
   Mx = new Array (9,8,7,6,5,4,3,2,1,1);

   if ( Identity.length != 10 ) return false;
   i = tab.indexOf( Identity.charAt(0) );
   if ( i == -1 ) return false;
   sum = A1[i] + A2[i]*9;

   for ( i=1; i<10; i++ ) {
      v = parseInt( Identity.charAt(i) );
      if ( isNaN(v) ) return false;
      sum = sum + v * Mx[i];
   }
   if ( sum % 10 != 0 ) return false;
   return true;
}
</script>



<div class="main">

  {include file="$tpl_dir./page_head.tpl"}

<div class="house_check_wrap">

  <div id="Web_1280__2" style="transform-origin: 0px 0px;">
    {$Breadcrumb}
  <form action="" method="post" name="form" onsubmit="return check()">
    <div class="t_container main">
    
        <div class="row">
          <div class="t_col_1 col-1 left">

          </div>
            <div class="t_col_10 col-10 main">

              <label class="h4">訂單詳情 | 結帳</label>
              <div class="row_1 border">
                <div class="row a">
                    <img src="/themes/LifeHouse/img/lifegoproduct/product_photo_1_022321-02.svg" alt="#">
                    <div class="center-block row_1_div p_name">
                      <span>商品名稱</span>
                      <input type="hidden" value="{$productID}" name="productID">
                      <input type="text" name="productTitle" value={$productTitle} placeholder="{$productTitle}" class="disabled">
                      <label>{$productInfo}</label>
                    </div>
                    <div class="center-block row_1_div">
                      <span>單價</span>
                      <input type="text" name="productPrice" value="{$productPrice}" readonly>
                    </div>
                    <div class="center-block row_1_div">
                      <span>數量</span>
                      <input type="text" name="order" value="{$order}" readonly>
                    </div>
                    <div class="center-block row_1_div">
                      <span>總價</span>
                      <input type="text" name="totalPrice" value="{$productPrice*$order}" readonly>
                    </div>
                    <div class="center-block row_1_div">
                      <span>運費</span>
                      <input type="text" name="shipprice" value="{$deliveryfee}" readonly>
                    </div>
                </div>
                <div class="row b">
                  <div class="col-6 gap-24"><span>留言給</span><input type="text" value="{$message}" name="message"></div>
                  <div class="col-3 gap-24"><span>訂單總額</span><input type="text" name="order_item_q" value="{$order_item_q}" readonly>件</div>
                  <div class="col-3 gap-24"><span>商品</span><input type="text" name="order_price_total" value="{$order_price_total}" readonly>元 </div>

                </div>
            </div>

              <label class="h4">訂購人資料</label>
              <div class="row_2 border">
                <div class="col-6 gap-24"><div class="row"><span class="col-3">姓名</span><input class="col-6" type="text" name="username" value="{$username}" placeholder="請輸入姓名" required ></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">Email</span><input class="col-6" type="email" name="email" value="{$email}" placeholder="請輸入 Email" required ></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">手機號碼</span><input id="user" data-role="none" class="floating_input" name="user" type="text"
                               placeholder="手機號碼" value="{$smarty.post.user}" minlength="6" maxlength="10" onkeyup="value=value.replace(/[^\d]/g,'') " required ><button type="button" id="tel_code_submit" class="btn ">取得驗證碼</button>
                  <!-- <span>發送驗證碼</span> -->
                </div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">請輸入驗證碼</span>
                        <input id="captcha" data-role="none" class="floating_input" name="captcha"
                               type="text" placeholder="驗證碼" maxlength="4" onkeyup="value=value.replace(/[^\d]/g,'') " required="">
                    </div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">連絡電話</span><input maxlength="10" class="col-6" type="text" name="tel" value="{$tel}" onkeyup="value=value.replace(/[^\d]/g,'') " placeholder="請輸入連絡電話" required ></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">地址</span><input class="col-6" type="text" name="address" value="{$address}" placeholder="請輸入地址" required ></div></div>
                <div class="col-6 gap-24"><div class="row"><span class="col-3">身分證字號</span><input class="col-6" type="text" id="Identity" name="Identity" value="{$id}" placeholder="請輸入身分證字號" maxlength="10" required ></div></div>
                {* <div class="col-6 gap-24"><div class="row">*若非會員身分,資料填寫完送出,將自動註冊會員資料。</div></div> *}
            </div>

            {* <label class="h4">信用卡測試</label>
              <div class="row_2 border">
                132
            </div> *}

             <label class="h4">付款方式</label>
              <div class="row_3 border">
              <div class="col-12 gap-24"><div class="row">
              {if $paymethod == "1"}
                <span class="col-2"><input class="" type="radio" id="payway" name="payway" value="1" checked>信用卡</span>
              {else}
                <span class="col-2"><input class="" type="radio" id="payway" name="payway" value="1">信用卡</span>
              {/if}
                <div class="col-6">
                  <div class="row">
                  <span class="col-sm text-right" >信用卡號</span>
                  <input class="col-sm" type="text" maxlength="4" id="cd1" name="cd1" size=4 value="{$cd1}" onKeyUp="setBlur(this,'cd2');">
                  <input class="col-sm" type="text" maxlength="4" id="cd2" name="cd2" size=4 value="{$cd2}" onKeyUp="setBlur(this,'cd3');">
                  <input class="col-sm" type="text" maxlength="4" id="cd3" name="cd3" size=4 value="{$cd3}" onKeyUp="setBlur(this,'cd4');">
                  <input class="col-sm" type="text" maxlength="4" id="cd4" name="cd4" size=4 value="{$cd4}">
                  </div>
                </div>
                           
                 <span class="col-sm text-right" >有效日期</span><input class="col-1" id="yearmonth" name="yearmonth" value="{$yearmonth}" type="date" onchange="console.log(this.value)" >
                 <span class="col-sm text-right" >CVS</span><input class="col-1" id="cvs" name="cvs" value="{$cvs}" type="text" maxlength="3"></div></div>
              <div class="col-12 gap-24"><div class="row">
              {if $paymethod =="2"}
                <span class="col-2"><input class="" type="radio" id="payway" name="payway" value="2" checked>貨到付款</span></div>
              {else}
                <span class="col-2"><input class="" type="radio" id="payway" name="payway" value="2">貨到付款</span></div>
              {/if}
              </div>
              <div class="col-12 gap-24"><div class="row">
              {if $paymethod =="5"}
              <span class="col-2"><input class="" type="radio" id="payway" name="payway" value="5" checked>LINE Pay</span><span class="col-2"> (請於4小時内付款)</span></div>
              {else}
              <span class="col-2"><input class="" type="radio" id="payway" name="payway" value="5">LINE Pay</span><span class="col-2"> (請於4小時内付款)</span></div>
              {/if}
              </div>
              <div class="col-12 gap-24"><div class="row"><span class="col-2">
              {if $paymethod =="3"}
              <input class="" type="radio" id="payway" name="payway" value="3" checked>ATM</span>
              {else}
              <input class="" type="radio" id="payway" name="payway" value="3">ATM</span>
              {/if}
              {* <div class="col-12 gap-24"><div class="row"><span class="col-2"><input class="" type="radio" id="payway" name="payway" value="3">ATM無卡分期</span> <span>申請人同訂購人</span><span style="margin-right: 1em;color:red">信用卡</span> <span>選擇無卡分期期數</span> *}
                {* <button type="button" class="btn btn-primary btn-sm" id="ped" name="ped" value="3">3期 X $ {round($order_price_total/3)}</button>
                <button type="button" class="btn btn-primary btn-sm" id="ped" name="ped" value="6">6期 X $ {round($order_price_total/6)}</button>
                <button type="button" class="btn btn-primary btn-sm" id="ped" name="ped" value="9">9期 X $ {round($order_price_total/9)}</button>
                <button type="button" class="btn btn-primary btn-sm" id="ped" name="ped" value="10">10期 X $ {round($order_price_total/10)}</button> *}
              </div></div>
              <div class="col-12 gap-24"><div class="row">
              {if $paymethod =="4"}
              <span class="col-2"><input id='radiostore' type="radio" id="payway" name="payway" value="4" checked/>超商付款</span>
              {else}
              <span class="col-2"><input id='radiostore' type="radio" id="payway" name="payway" value="4" />超商付款</span>
              {/if}
              </div></div>
              </div>

              <script>
              window.onload = function() {
                /* you can assign a listener like this:

                        var radio = document.getElementById('radio');    
                        radio.onclick = myFunction;
                        
                or like this: */
                          document.getElementById("radiostore").addEventListener("click", myFunction);
                }
                function myFunction() {
                  var check_pick = document.getElementById("check_pick");
                  var check_pick1 = document.getElementById("check_pick1");
                  if (check_pick.style.display === "none") {
                    check_pick.style.display = "block";
                  } else {
                    check_pick.style.display = "none";
                  }
                  if (check_pick1.style.display === "none") {
                    check_pick1.style.display = "block";
                  } else {
                    check_pick1.style.display = "none";
                  } 
                }
              </script>


              <label class="h4" name="check_pick" id="check_pick" style="display: none;">取貨方式</label>
              <div class="row_4 border" name="check_pick1" id="check_pick1" style="display: none;">
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" id="delway" name="delway" value="7">超商取貨</span>
                  <div class="col-10">
                    <div class="row">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_3-07.svg"><input class="" type="radio" id="delwaystore" name="delwaystore" value="3">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_4-07.svg"><input class="" type="radio" id="delwaystore" name="delwaystore" value="4">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_5-07.svg"><input class="" type="radio" id="delwaystore" name="delwaystore" value="5">
                    <img src="/themes/LifeHouse/img/lifegoproduct/dleveily_6-07.svg"><input class="" type="radio" id="delwaystore" name="delwaystore" value="6">
                    <span class="col-2 text-right-sm">門市名稱</span><button type="button" class="btn btn-primary btn-sm yellow">查詢門市名稱</button>
                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" id="delway" name="delway" value="1">宅配</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>收件人姓名:</span><input class="col-sm" type="text" id="pickusername" name="pickusername" value={$pickusername}></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>手機號碼:</span><input class="col-sm" type="text" id="pickphone" name="pickphone" value="{$pickphone}" onkeyup="value=value.replace(/[^\d]/g,'') "></div></span>
                      <input class="" type="checkbox" name="asrow_4"><span class="col-sm-2">同訂購人資料</span>
                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" name="delway" value="2">郵寄</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>收件人姓名:</span><input class="col-sm" type="text" id="pickusername1" name="pickusername" value={$pickusername}></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>手機號碼:</span><input class="col-sm" type="text" id="pickphone1" name="pickphone" value="{$pickphone}"></div></span>
                      <input class="" type="checkbox" name="asrow_4"><span class="col-sm-2">同訂購人資料</span>
                    </div>
                  </div>
                </div>
              </div></div>

              <label class="h4">發票資料</label>
              <div class="row_5 border">
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" id="receipt" name="receipt" value="1">公益捐贈</span>
                  <div class="col-10">
                    <div class="row">

                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" id="receipt" name="receipt" value="2">電子發票</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>Email:</span><input class="col-sm" type="text" id="receiptname" name="receiptname" value="{$receiptname}"></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>載具:</span><input class="col-sm" type="text" id="receiptnum" name="receiptnum" value="{$receiptnum}"></div></span>

                    </div>
                  </div>
                </div></div>
                <div class="col-12 gap-24"><div class="row">
                  <span class="col-2"><input class="" type="radio" id="receipt" name="receipt" value="3">公司發票</span>
                  <div class="col-10">
                    <div class="row">
                      <span class="col-sm-4 text-left"><div class="row"><span>公司名稱:</span><input class="col-sm" type="text" id="receiptname1" name="receiptname" value="{$receiptname}"></div></span>
                      <span class="col-sm-4 text-left"><div class="row"><span>統編:</span><input class="col-sm" id="receiptnum1" type="text" name="receiptnum" value="{$receiptnum}"></div></span>

                    </div>
                  </div>
                </div></div>
              </div>

              <div class="shopping">
                本次訂單金額: <input type="text" value="{$order_price_total+$deliveryfee}" readonly></input>
                <button type="submit" name="checkorder" value="1">確認結帳</button>
              </div>

              {* <label class="h4" name="check_pick" id="check_pick" style="color:red" >退換貨須知</label>
              <div class="row_4 border" name="check_pick1" id="check_pick1" >
                <ul>
                  <li>提醒您，申請退貨時，請務必將您所訂購之完整商品、配件、贈品及包裝置入於原外包裝箱內，以利後續退換貨作業。如您已收到發票，請依發票背面之銷退折讓單上說明，簽章後寄回生活好科技，以便儘速為您辦理退款。若您是以其他方式(電話/語音/型錄)訂購則恕不提供本項服務。請注意，退貨的商品必須為全新狀態且完整包裝(包含主機、附件、內外包裝、隨機文件、贈品等)。
                    此外，下列情形可能影響您的退貨權限：</li>
                  <li>(1)隨商品已附上相同之試用品，或在收到影音光碟及軟體前已提供您試聽、試用機會。</li>
                  <li>(2)在不影響您檢查商品情形下，您將商品包裝毀損、封條移除、吊牌拆除、貼膠移除或標籤拆除等情形。</li>
                  <li>(3)在您收到商品之前，已提供您檢查商品之機會。</li>
                  <li>(4)其他逾越檢查之必要或可歸責於您之事由，致商品有毀損、滅失或變更者。</li>
                </ul>
                <ul>
                  <li>退款方式：<br/>
(1)付款方式為貨到付款、超商取貨付款、IBON付款、ATM付款之訂單，確認退貨後將款項以支票方式掛號寄送訂購人。<br/>
(2)付款方式為信用卡者，確認退貨後將款項退至原信用卡帳戶中，如一筆訂單中包含多項商品，辦理部份商品退貨者，待退回物品確認回數將再向原發卡行申請支付剩餘款項，並進行全額款項刷退作業。</li>
                </ul>
              </div> *}

            </div>
            <div class="t_col_1 col-1 right">

            </div>
        </div>
        </form>
  
</div>


{include file="$tpl_dir./page_footer.tpl"}
</div>
{$js}
{$js_return}
