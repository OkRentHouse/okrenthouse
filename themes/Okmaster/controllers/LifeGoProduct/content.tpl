{include file="$tpl_dir./life_go_tab.tpl"}
<div class="main">
    <div class="flex">
        <div class="marquee_wrap">
            <marquee direction="left" scrollamount="10" behavior="scroll">
                <img src="/themes/Okmaster/img/lifego/star.svg" alt="" class="">慶祝母親節 全館優惠中到5/10止
                <img src="/themes/Okmaster/img/lifego/star.svg" alt="" class="">慶祝母親節 全館優惠中到5/10止
            </marquee>
        </div>
    </div>
    <div class="container">
        <div class="main__top">
            <div class="main__top-left">
                <div class="main__top-left-img">
                    <div class="main__top-left-img-inner"></div>
                </div>
                <div class="main__top-left-nav">
                    <div class="main__top-left-nav-img"></div>
                    <div class="main__top-left-nav-img"></div>
                    <div class="main__top-left-nav-img"></div>
                    <div class="main__top-left-nav-img"></div>
                </div>
            </div>
            <div class="main__top-right">
                <div class="main__top-right-inner">
                    <div class="main__title">健康御守 除菌消味錠</div>
                    <div class="main__slogan">
                        消毒殺菌 防疫第一選擇 消毒 殺菌 除臭
                    </div>
                    <div class="detail-box">
                        <div class="detail-box-left">
                            <div class="star"><span class="icon"><i class="far fa-star"></i></span>5.0</div>
                            <div class="buy"><span class="icon"><i class="fas fa-shopping-bag"></i></span>36</div>
                            <div>滿5送1</div>
                            <div>紅利點數<span class="accent">5</span>點</div>
                        </div>
                        <div class="detail-box-right">
                            <div class="more-btn">
                                更多活動<span class="icon-down"><i class="fas fa-angle-down"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="price">
                        <div class="price__origin">
                            <div class="price__origin-label">原價</div>
                            <div class="price__origin-price">2,100</div>
                            <div class="price__origin-unit">元</div>
                        </div>
                        <div class="price__special">
                            <div class="price__special-label">好康價</div>
                            <div class="price__special-price">1,350</div>
                            <div class="price__special-unit">元</div>
                        </div>
                    </div>
                    <div class="detail-box">
                        <div class="detail-box-left">
                            <div>信用卡．貨到付款．LINE Pay．ATM無卡分期．超取付款</div>
                        </div>
                        <div class="detail-box-right">
                            <div class="more-btn">
                                詳細內容<span class="icon-down"><i class="fas fa-angle-down"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="choose-way">
                        <div class="choose-way__single">
                            <div class="choose-way__single-icon icon-down"><i class="fas fa-truck"></i></div>
                            <div class="choose-way__single-text">宅配</div>
                        </div>
                        <div class="choose-way__single">
                            <div class="choose-way__single-icon icon-down"><i class="fas fa-archive"></i></div>
                            <div class="choose-way__single-text">宅配</div>
                        </div>
                        <div class="choose-way__single">
                            <div class="choose-way__single-icon icon-down"><i class="fas fa-store"></i></div>
                            <div class="choose-way__single-text">宅配</div>
                        </div>
                    </div>
                    <div class="">
                        <div class="btn">放入購物車</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main__bottom">
            <div class="main__bottom-title">
                <span class="icon"><img src="/themes/Okmaster/img/lifego/star.svg"></span>其他人也看了
            </div>
            <div class="main__bottom-container">
                <div class="main__bottom-container-left">
                    <a class="img" href="#"></a>
                    <a class="img" href="#"></a>
                    <a class="img" href="#"></a>
                    <a class="img" href="#"></a>
                    <a class="img" href="#"></a>
                    <a class="img" href="#"></a>
                </div>
                <div class="main__bottom-container-right">
                    <div class="info-box">
                        <div class="info-box__tab">
                            <div class="info-box__tab-left">商品資訊</div>
                            <div class="info-box__tab-right">留言問答</div>
                        </div>
                        <div class="info-box-content">
                            <div class="info-box-content-question">
                                <div class="info-box-content-question-left -name">
                                    <div class="info-box-content-question-name">小凱</div>
                                    <div class="info-box-content-question-date">2020/5/7</div>
                                </div>
                                <div class="info-box-content-question-right">
                                    請問殺菌效果好嗎
                                </div>
                            </div>
                            <div class="info-box-content-answer">
                                <div class="info-box-content-answer-left">
                                    您好~經實證證實滅菌率可達99.9%喔
                                </div>
                                <div class="info-box-content-answer-right -name">
                                    <div class="info-box-content-answer-name">樂購客服</div>
                                    <div class="info-box-content-answer-date">2020/5/8</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include file="$tpl_dir./LifeGo_footer.tpl"}
{$js}