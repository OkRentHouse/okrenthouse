//todo 加入我的最愛

$(document).ready(function(){

}).on('click', '.favorite', function(){
    let on = $(this).hasClass('on');
    add_favorite($(this).data('type'), $(this).data('id'), on);
}).on('click', '.give_star', function(){
    give_star($(this).data('title'), $(this).data('id'), $(this).data('star'));
});

function add_favorite(type, id, on) {
    let $this = $('.favorite[data-type="' + type + '"][data-id="' + id + '"]');
    if(id != undefined && id > 0){
        if(on == true){
            $this.children('img').attr('src', THEME_URL + '/img/icon/heart.svg');
            $this.removeClass('on').addClass('off');
        }else{
            $this.children('img').attr('src', THEME_URL + '/img/icon/heart_on.svg');
            $this.removeClass('off').addClass('on');
        }

//施工中 favorite
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'Favorite',
                'table_name' : type,
                'table_id' : id
            },
            dataType: 'json',
            success: function (data) {
                if(data["error"] !=""){
                    alert(data["error"]);
                }else{
                    if(data["active"]=='1'){
                        alert("已加入我的最愛");
                    }else{
                        alert("取消加入我的最愛");
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }
}

function give_star_img_htm(id, star, rounding=false){
    var img = '';
    for(i=1;i<=5;i++){
        var clas = '';
        if(i == star){
            clas = ' class="active"';
        };
        if(i <= star){				//一顆
            img += '<img src="'+THEME_URL+'/img/popularity2.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
        }else if(star - i >= 0.5){	//半顆
            if(rounding){	//顯示整顆星
                img += '<img src="'+THEME_URL+'/img/popularity2.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
            }else{	//顯示半顆星
                img += '<img src="'+THEME_URL+'/img/popularity2_half.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
            }
        }else{
            img += '<img src="'+THEME_URL+'/img/popularity2_gray.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
        };
    };
    return img;
};
//todo 給星星
function give_star(title, id, star){
    let img = give_star_img_htm(id, star);
    Swal.fire({
        title: title,
        html: '<div class="my_give_div">我給<div class="my_give" data-id="'+id+'">'+img+'</div></div>',
        focusConfirm: false,
        // showCloseButton: true,
        confirmButtonText:
            '確認',
    }).then((result) => {
        if (result.value) {
            let star = $('.my_give[data-id="'+id+'"] img.active').data('star');
            // $('.div_star[data-id="'+id+'"]').html(give_star_img_htm(id, star, true));
            // $('.div_popularity[data-id="'+id+'"]').html(give_star_img_htm(id, star, true));
            $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': false,
                    'action': 'Popularity',
                    'id' : id,
                    'star' : star
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if(data["error"] !=""){
                        alert(data["error"]);
                    }else{
                        // console.log(data);
                        $('.div_star[data-id="'+id+'"]').html(give_star_img_htm(id,data["return"]["mystar"], true));
                        $('.div_popularity[data-id="'+id+'"]').html(give_star_img_htm(id,data["return"]["popularity"], true));
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // console.log("test");
                }
            });
            //todo ajax
        };
    });
};