var arr_city = [];



class SearchHouse {

    constructor($this) {

        this.tag = true;

        this.submit = false;

        this.div = $($this);

        this.arr_name = [];

        var t = this;

        $('input[name$="[]"]', this.div).each(function() {

            t.get_check_name();

        });



        //跳頁

        $('a', '.ajax_pagination').click(function() {

            var p = $(this).data('p');

            var href = t.change_url();



            var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');

            if (!this.submit) {

                history.pushState({}, 'title', url('path') + href + '&p=' + p);

                $('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + url('path').substr(1) + href.substr(1) + '&p=' + p);

                $body.stop(true, false).animate({

                    scrollTop: 0

                }, 'slow', function() {

                    location.reload(); //重新整理

                    return false;

                });

                return false;

            } else {

                return true;

            }

        });



        this.div.submit(function() {

            //使用Submit

            if (t.submit) {

                t.arr_name.forEach(function(name) {

                    var value = [];

                    $('input[name="' + name + '[]"]:checked', t.div).each(function() {

                        value.push($(this).val());

                    });

                    $('input[name="' + name + '[]"]:checked', t.div).next('label').addClass('active');

                    if ($('input[name="' + name + '_s"]', t.div).length == 0) {

                        $(t.div).append('<input type="hidden" name="' + name + '_s" value="">');

                    }

                    $('input[name="' + name + '[]"]', t.div).remove();

                    $('input[name="' + name + '_s"]', t.div).val(value.join());

                });

                return true;

            } else {

                location.reload();

                return false;

            }

        });

        t.change_url();

        $(document).on('click', $this + ' .close_tag', function() {

            var $samp = $(this).parents('samp');

            var id = $samp.data('id');

            t.close_tag(id);

        }).on('click', $this + ' .close_all', function() {

            $('input[type="text"],input[type="number"],input[type="hidden"]', t.div).val(null);

            $('input[type="checkbox"], input[type="radio"]', t.div).prop('checked', false);

            $('input[type="radio"][value=""]', t.div).prop('checked', true);

            $('.county_city', t.div).html('全部');

            t.change_url();

        }).on($this + ' input', function() {

            t.change_url();

        }).on('change', $this + ' input[type="checkbox"]', function() {

            var $this = $(this);

            t.on_all($this)

        }).on('click', $this + ' input[type="radio"]', function() {

            var $this = $(this);

            var $search_div = $this.parents('.search_div');

            $('input[type="checkbox"]', $search_div).prop('checked', false);

            // }).on('click', 'a[href="/HouseMap"]', function () {

            // 	var href = t.change_url();

            // 	history.pushState({}, 'title', '/HouseMap'+href);

            // 	$('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + 'HouseMap'+href.substr(1));

            // 	location.reload();	//重新整理

            // 	return false;

            // }).on('click', 'a[href="/list"]', function () {

            // 	var href = t.change_url();

            // 	history.pushState({}, 'title', '/list'+href);

            // 	$('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + 'list'+href.substr(1));

            // 	location.reload();	//重新整理

            // 	return false;

        });

    }



    close_tag(id) {

        var t = this;

        switch (id) {

            case 'search':

                $('input#search', t.div).val(null);

                break;

            default:

                if (id.match('county') != null) {

                    $('.county_city', t.div).html('全部');

                    $('.city_div', t.div).html('');

                    $('samp[data-id^="city"]').remove();

                };

                var $input = $('input#' + id, t.div);

                $input.prop('checked', false);

                var $search_div = $input.parents('.search_div');

                if ($('input[type="checkbox"]', $search_div).length) {

                    $('input[type="radio"][name!="county"]', $search_div).prop('checked', false);

                }

                t.on_all($input);

                break;

        }

        t.change_url();

        $('samp#' + id).remove();

    }



    get_check_name() {

        var t = this;

        $('input[name$="[]"]', this.div).each(function() {

            var name = $(this).prop('name').replace('[]', '');

            if ($.inArray(name, t.arr_name) < 0) {

                t.arr_name.push(name);

            }

        });

    }



    on_all($this) {

        var $search_div = $this.parents('.search_div');

        if ($('input[type="checkbox"]:checked', $search_div).length) {

            $('input[type="radio"][name!="county"]', $search_div).prop('checked', false);

        } else {

            $('input[type="radio"][name!="county"]', $search_div).prop('checked', true);

        }

    }



    change_url() {

        var href = '?';

        var t = this;



        var html = '';

        t.get_check_name();



        if ($('#search', t.div).length && $('#search', t.div).val()) {

            href += '&search' + '=' + $('#search', t.div).val();

        }



        $('input[type="radio"]:checked', t.div).each(function() {

            var name = $(this).prop('name');

            var val = $(this).val();

            if (name != '' && name != undefined && name != 'undefined' && val != '' && val != '0') {

                href += '&' + name + '=' + val;

                if (t.tag) {

                    var id = $(this).attr('id');

                    var text = $(this).next('label').html();

                    html += '<samp data-id="' + id + '"><em>' + text + '</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';

                }

            }

        });



        // $('input[type!="checkbox"][type!="radio"]', t.div).each(function () {

        // 	var name = $(this).prop('name');

        // 	var val = $(this).val();

        // 	if (name != '' && name != undefined && name != 'undefined' && val != '' && val != '0') {

        // 		href += '&' + name + '=' + $(this).val();

        // 	}

        // });



        t.arr_name.forEach(function(name) {

            var value = [];

            $('input[name="' + name + '[]"]:checked', t.div).each(function() {

                value.push($(this).val());

                if (t.tag) {

                    var id = $(this).attr('id');

                    var text = $(this).next('label').html();

                    html += '<samp data-id="' + id + '"><em>' + text + '</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';

                }

            });

            if (value.length > 0) {

                href += '&' + name + '_s' + '=' + value;

            }

        });



        if ($('#search', t.div).length && $('#search', t.div).val()) {

            href += '&search' + '=' + $('#search', t.div).val();

            if (t.tag) {

                html += '<samp data-id="search"><em>搜尋：' + $('#search', t.div).val() + '</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';

                $('.room', t.div).html();

                $('.room_unit', t.div).html();

            }

        }



        $('.tag_div', t.div).html(html);

        if (!t.submit) {

            history.pushState({}, 'title', url('path') + href)

            $('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + url('path').substr(1) + href.substr(1));

        };

        return href;

    };



    changeInput(name) {

        $(document).on('change', '#search_box_big input[name$="[]"]');

    }

}



var st = new SearchHouse('#search_box_big');









function show_city_div() {

    var html = '';

    var county = $('input[name="county"]:checked', '#search_box_big').val();

    $.each(arr_city[county], function(index, item) {

        html += '<span class="city"><input type="checkbox" name="city[]" id="city_' + index + '" value="' + item + '"><label for="city_' + index + '">' + item + '</label></span>';

    });

    $('.city_div', '#search_box_big').html(html);

    $('.city_div', '#search_box_big').show();

    $('.county_div', '#search_box_big').hide();

}



function city_div() {

    var html = $('#search_box_big input[name="county"]:checked').val();

    $('#search_box_big input[name^="city"]:checked').each(function(index) {

        if (index > 0) {

            html += '、';

        }

        html += $(this).val();

    });

    $('.county_city', '#search_box_big').html(html + '<i class="fas fa-chevron-down"></i>');

}



function close_search_box_div(event) {

    if (!$('.search, .county_div, .county_div *, .city_div, .city_div *', '#search_box_big').is(event.target)) {

        $('.county_div', '#search_box_big').hide();

        $('.city_div', '#search_box_big').hide();

    }

}

function process_sel() {

    var pL = 0;

    if ($(".county_city").text() != "所有縣市" && $(".county_div").css("display") == "none") {

        //console.log($(".search_div").eq(1).text())

        $(".search_div").eq(1).show(300);

        pL = add_line(pL);

    }



    if ($("input[name='id_main_house_class[]']:checked").val() != undefined)

    {

        $(".search_div").eq(2).show(300);

        pL = add_line(pL);

    }



    if ($("input[name='id_types[]']:checked").val() != undefined)

    {

        $(".search_div").eq(3).show(300);

        pL = add_line(pL);

    }



    if ($("input[name='id_type[]']:checked").val() != undefined)

    {

        $(".search_div").show(300);

        pL = add_line(pL);

    }





    if ($("input[name='id_other[]']:checked").val() != undefined)

    {

        $(".search_div").show(300);

        pL = add_line(pL);

    }



}



function add_line(pL) {

    $("[name='process_bar_fas']").eq(pL).removeClass("fas_h");
    $("[name='process_bar_fas']").eq(pL).addClass("fas");

    pL++;

    $("[name='process_bar_t']").eq(pL).addClass("process_bar_t");

    return pL;

}

var search_txt_top; //$(window).scroll 判斷卷軸 是否超過搜尋列

$(document).ready(function() {

    process_sel(); //如果有被點選的話則要做該動作

    st = new SearchHouse('#search_box_big');

    if ($('article.Index').length) {

        st.tag = false;

        st.submit = true;

        $('samp', '#search_box_big').remove();

    }

}).on('click', '#search_forms', function() {

    setTimeout(function() {

        process_sel();

    }, 200);

}).on('click', '#search_box_big input[name="county"]', function() {

    show_city_div();

    city_div();

}).on('click', '#search_box_big input[name^="city"]', function() {

    city_div();

}).on('click', '#search_box_big .county_city', function() {

    $('.county_div', '#search_box_big').show();

    $('.city_div', '#search_box_big').hide();

}).on('click', '.search_div input[name="id_main_house_class[]"]', function() {

    setTimeout(function() {

        process_sel();

    }, 200);

}).on('click', '.search_div input[name="id_other[]"]', function() {

    setTimeout(function() {

        process_sel();

    }, 200);

}).on('click', 'body', function(event) {

    close_search_box_div(event);

});