$( document ).ready(function() {
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 6,
        slidesToScroll: 6
    });
    $(".draggable").removeClass("draggable")
});