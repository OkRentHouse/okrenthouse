
{include file="$tpl_dir./page_head.tpl"}
<div class="cms{if $display_header} cms_header{/if}{if $display_footer} cms_foote{/if}">
	{if $html}{$html}{else}{$html_0}{if $introduce_tpl}{include file=$introduce_tpl}{/if}{$html_1}{/if}

{if !empty($id_cms)}
	{$model_html}
{/if}
</div>
{if !empty($cms_css)}
	<style>
		{$cms_css}
	</style>
{/if}
<script>
{if !empty($id_cms)}
	{$model_js}
{/if}
{if !empty($cms_js)}
		{$cms_js}
{/if}
</script>
{include file="$tpl_dir./page_footer.tpl"}
