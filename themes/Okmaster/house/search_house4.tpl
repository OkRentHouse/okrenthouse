<form action="https://okrent.house/list" method="get" id="search_box_big">
    <div class="search_list_wrap">

        <div class="search_div">
{*            <strong>區域</strong>*}
            <div class="city_select">
                <spam class="county_city search search_1">
                    區域
                </spam>
            </div>
            <div class="county_div" style="display: none;">
                {foreach from=$select_county key=i item=county}<spam class="county"><input type="radio" name="county" id="county_{$i}" value="{$county.value}" {if $county.value == $smarty.get.county}checked{/if}><label for="county_{$i}">{$county.title}</label></spam>{/foreach}
            </div>
                <div class="city_div">{foreach from=$arr_city[$smarty.get.county] key=i item=city}<spam class="city"><input type="checkbox" name="city[]" id="city_{$i}" value="{$city}" {if in_array($city, $smarty.get.city)}checked{/if}><label for="city_{$i}">{$city}</label></spam>{/foreach}</div>
        </div>

        <div class="search_div">
{*            <strong>類別</strong>*}
            <div class="class_select" id="types_all_name">
{*                類別*}
                電梯大樓
            </div>
            <div class="category none">
             <input type="radio" id="id_types_all" value=""{if count($smarty.get.id_types) == 0} checked{/if}><label for="id_types_all" id="id_types">{*<img src="/img/all.png">*}全部</label>{foreach from=$select_types key=i item=types}<input type="checkbox" name="id_types[]" id="id_types_{$types.id_rent_house_types}" value="{$types.id_rent_house_types}" {if in_array($types.id_rent_house_types, $smarty.get.id_types)}checked{/if}><label for="id_types_{$types.id_rent_house_types}" id="types_name_{$types.id_rent_house_types}">{*<img src="{$types.img}">*}{$types.title}</label>{/foreach}
            </div>
        </div>

        <div class="search_div">
{*            <strong>型態</strong>*}

            <div class="type_select" id="type_all_name">
{*                型態*}
                整層住家
            </div>
            <div class="patterns none">
                <input type="radio" id="type_all" value=""{if count($smarty.get.id_type) == 0} checked{/if}><label for="type_all" id="type_name">{*<img src="/img/all.png">*}全部</label>{foreach from=$select_type key=i item=type}<input type="checkbox" name="id_type[]" id="id_type_{$type.id_rent_house_type}" value="{$type.id_rent_house_type}" {if in_array($type.id_rent_house_type, $smarty.get.id_type)}checked{/if}><label for="id_type_{$type.id_rent_house_type}" id="type_name_{$type.id_rent_house_type}">{*<img src="{$type.img}">*}{$type.title}</label>{/foreach}
            </div>
        </div>


        <div class="search_input"><input type="text" class="field w50p2" name="search" id="search"
                                         value="" placeholder="請輸入 社區 街道 名稱等 關鍵字或物件編號">
        </div>

{*        <button class="close_all search_btn" data-toggle="collapse" aria-expanded="false">*}
{*            全部清除*}
{*        </button>*}

        <button class="search_icon" type="submit"><img src="{$THEME_URL}/img/icon/pic_m.svg"></button>
        <button class="search_icon" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82"><img src="{$THEME_URL}/img/icon/map_m.svg"></a></button>
    </div>
</form>
<script>
    arr_city = {$arr_city|json_encode:1};
    {$add_js}
</script>