<form action="{if $page == 'index'}/list{/if}" method="get" id="search_box_big">

	<button class="collapsed search_btn" data-toggle="collapse" href="#search_forms" aria-expanded="false">

		條件搜尋<img src="{$THEME_URL}/img/icon/pic_m.svg">

	</button>

	<div class="tag_div"></div>

{*	<button class="close_all search_btn" data-toggle="collapse" aria-expanded="false">*}

{*		全部清除*}

{*	</button>*}

	<hr>

	<div class="search_forms_wrap">

		<div id="search_forms" class="searcounty_sch_content collapse" aria-expanded="false">

			<div class="search_div"><strong>區域</strong>

				<div><spam class="county_city search">{if empty($smarty.get.county)}所有縣市{else}{$smarty.get.county}{/if}</spam></div>{*<spam class="txt">(可複選)</spam>*}

				<div class="search_input"><input type="text" class="field w50p2" name="search" id="search" value="{$smarty.get.search}" placeholder="請輸入 社區 街道 名稱等 關鍵字或物件編號"><button type="submit"><img src="{$THEME_URL}/img/icon/pic_m.svg"></button></div>

				<div class="county_div">

                    {foreach from=$select_county key=i item=county}<spam class="county"><input type="radio" name="county" id="county_{$i}" value="{$county.value}" {if $county.value == $smarty.get.county}checked{/if}><label for="county_{$i}">{$county.title}</label></spam>{/foreach}

				</div>

				<div class="city_div">{foreach from=$arr_city[$smarty.get.county] key=i item=city}<spam class="city"><input type="checkbox" name="city[]" id="city_{$i}" value="{$city}" {if in_array($city, $smarty.get.city)}checked{/if}><label for="city_{$i}">{$city}</label></spam>{/foreach}</div>

			</div>

{*			<div class="search_div"><strong>用途</strong><spam class="txt">(可複選)</spam>*}

{*				<div>*}

{*					<input type="radio" value=""{if empty($smarty.get.use)} checked{/if}><label for="use_all">全部</label>{foreach from=$select_use key=i item=use}<input type="checkbox" name="use[]" id="use_{$use.id_use}" value="{$use.id_use}"><label for="use_{$use.id_use}">{$use.use}</label>{/foreach}*}

{*				</div>*}

{*			</div>*}

			<div class="search_div"><strong>型態</strong>{*<spam class="txt">(可複選)</spam>*}

				<div>

					<input type="radio" id="id_types_all" value=""{if count($smarty.get.id_types) == 0} checked{/if}>{*<label for="id_types_all"><img src="/img/all.png">全部</label>*}{foreach from=$select_types key=i item=types}<input type="checkbox" name="id_types[]" id="id_types_{$types.id_rent_house_types}" value="{$types.id_rent_house_types}" {if in_array($types.id_rent_house_types, $smarty.get.id_types)}checked{/if}><label for="id_types_{$types.id_rent_house_types}">{$types.title}</label>{if ($i%4 == 3)}<br>{/if}{/foreach}

				</div>

			</div>

			<div class="search_div"><strong>類別</strong>{*<spam class="txt">(可複選)</spam>*}

				<div>

					<input type="radio" id="type_all" value=""{if count($smarty.get.id_type) == 0} checked{/if}>{*<label for="type_all"><img src="/img/all.png">全部</label>*}
					{foreach from=$select_type key=i item=type}<input type="checkbox" name="id_type[]" id="id_type_{$type.id_rent_house_type}" value="{$type.id_rent_house_type}" {if in_array($type.id_rent_house_type, $smarty.get.id_type)}checked{/if}><label for="id_type_{$type.id_rent_house_type}">{$type.title}</label>
{*						{if $type.id_rent_house_type==1}*}
{*							<div class="pattern">*}
{*								<strong>格局</strong>*}
{*								<div class="search_table">*}
{*									<div class="w150"><div class="room"></div>*}
{*										<unit class="room_unit">房數</unit>*}
{*										<input type="hidden" name="min_room" id="min_room" value="{$smarty.get.min_room|string_format:"%d"}"><input type="hidden" name="max_room" id="max_room" value="{$smarty.get.max_room|string_format:"%d"}"></div>*}
{*									<div><div id="room_range"></div></div>*}
{*								</div>*}
{*							</div>*}
{*						{/if}*}
						{if ($i%4 == 3)}<br>{/if}
				{/foreach}

				</div>

			</div>


{*			<div class="search_div"><strong>設備堤供</strong>*}{*<spam class="txt">(可複選)</spam>*}

{*				<div>*}

{*					<input type="radio" id="id_device_class_all" value=""{if count($smarty.get.id_device_class) == 0} checked{/if}><label for="id_device_class_all"><img src="/img/all.png">全部</label>{foreach from=$select_device_category_class key=i item=device_class}<input type="checkbox" name="id_device_class[]" id="id_device_class_{$device_class.id_device_category_class}" value="{$device_class.id_device_category_class}" {if in_array($device_class.id_device_category_class, $smarty.get.id_device_class)}checked{/if}><label for="id_device_class_{$device_class.id_device_category_class}"><img src="{$device_class.img}"> {$device_class.title}</label>{/foreach}*}

{*				</div>*}

{*			</div>*}


{*			<div class="search_div"><strong>設備堤供</strong>*}{*<spam class="txt">(可複選)</spam>*}

{*				<div>*}

{*					<input type="radio" id="id_device_all" value=""{if count($smarty.get.id_device) == 0} checked{/if}><label for="id_device_all"><img src="/img/all.png">全部</label>{foreach from=$select_device_category key=i item=device}<input type="checkbox" name="id_device[]" id="id_device_{$device.id_device_category}" value="{$device.id_device_category}" {if in_array($device.id_device_category, $smarty.get.id_device)}checked{/if}><label for="id_device_{$device.id_device_category}"><img src="{$device.img}"> {$device.title}</label>{/foreach}*}

{*				</div>*}

{*			</div>*}

			<div class="search_div"><strong>搜尋條件</strong>{*<spam class="txt">(可複選)</spam>*}
				<div>
					<input type="radio" id="id_other_all" value=""{if count($smarty.get.id_other) == 0} checked{/if}>{*<label for="id_other_all"><img src="/img/all.png">全部</label>*}{foreach from=$select_other_conditions key=i item=other}<input type="checkbox" name="id_other[]" id="id_other_{$other.id_other_conditions}" value="{$other.id_other_conditions}" {if in_array($other.id_other_conditions, $smarty.get.id_other)}checked{/if}><label for="id_other_{$other.id_other_conditions}"><img src="{$other.img}">{$other.title}</label>{/foreach}
				</div>
			</div>

			<div class="search_div rang"><strong>房數需求</strong>
				<div class="search_table">
					<div class="w290" id="house_w290"><div class="room"></div><unit class="room_unit">房</unit><input type="hidden" name="min_room" id="min_room" value="{$smarty.get.min_room|string_format:"%d"}"><input type="hidden" name="max_room" id="max_room" value="{$smarty.get.max_room|string_format:"%d"}"></div>
					<div><div id="room_range"></div></div>
				</div>
			</div>

			<div class="search_div rang"><strong>室內</strong>
				<div class="search_table">
					<div class="w290"><input type="number" name="min_ping" id="min_ping" value="{$smarty.get.min_ping|string_format:"%d"}" step="{$ping_step}" min="{$min_ping}" max="{$max_ping}"><i> ～ </i><input type="number" name="max_ping" id="max_ping" value="{$smarty.get.max_ping|string_format:"%d"}" step="{$ping_step}" min="{$min_ping}" max="{$max_ping}"><unit>坪</unit></div>
					<div><div id="ping_range"></div></div>
				</div>
			</div>

			<div class="search_div rang"><strong>預算範圍</strong>
				<div class="search_table">
					<div class="w290"><input type="number" name="min_price" id="min_price" value="{$smarty.get.min_price|string_format:"%d"}" step="{$price_step}" min="{$min_price}" max="{$max_price}"><i> ～ </i><input type="number" name="max_price" id="max_price" value="{$smarty.get.max_price|string_format:"%d"}"step="{$price_step}" min="{$min_price}"  max="{$max_price}"><unit>元</unit></div>
					<div><div id="price_range"></div></div>
				</div>
			</div>

	</div>

	</div>

</form>

<script>

	arr_city = {$arr_city|json_encode:1};

    {if $min_price > 0}

	min_price = {$min_price};

    {else}

	min_price = 0;

    {/if}

    {if $max_price > 0}

	max_price = {$max_price};

    {else}

	max_price = 100000000;

    {/if}

    {if $price_step > 0}

	price_step = {$price_step};

    {/if}



    {if $min_ping > 0}

	min_ping = {$min_ping};

    {else}

	min_ping = 0;

    {/if}

    {if $max_ping > 0}

	max_ping = {$max_ping};

    {else}

	max_ping = 1000;

    {/if}

    {if $ping_step > 0}

	ping_step = {$ping_step};

    {/if}

</script>
