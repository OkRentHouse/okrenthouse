<form action="/HouseMap" method="get" id="search_box">
	<div class="select_group">
		<select name="county">
			<option value="">縣市</option>
            {foreach $select_county as $v => $arr}
                {if isset( $smarty.get.county)}
					<option value="{$arr.value}"{if $smarty.get.county == $arr.value} selected{/if}>{$arr.title}</option>
                {else}
					<option value="{$arr.value}"{if $default_county == $arr.value} selected{/if}>{$arr.title}</option>
                {/if}
            {/foreach}
		</select><select name="city">
			<option value="">行政區</option>
            {foreach $select_city as $v => $arr}
                {if isset( $smarty.get.city)}
					<option value="{$arr.value}" data-parent="{$arr.parent}" data-parent_name="{$arr.parent_name}" style="display: none;"{if $smarty.get.city == $arr.value} selected{/if}>{$arr.title}</option>
                {else}
					<option value="{$arr.value}" data-parent="{$arr.parent}" data-parent_name="{$arr.parent_name}" style="display: none;"{if $default_city == $arr.value} selected{/if}>{$arr.title}</option>
                {/if}
            {/foreach}
		</select><select name="type">
			<option value="">型態</option>
            {foreach $select_type as $i => $arr}
                {if isset( $smarty.get.type)}
					<option value="{$arr.id_rent_house_type}"{if $smarty.get.type == $arr.id_rent_house_type} selected{/if}>{$arr.title}</option>
                {else}
					<option value="{$arr.id_rent_house_type}"{if $default_type == $arr.id_rent_house_type} selected{/if}>{$arr.title}</option>
                {/if}
            {/foreach}
		</select><select name="types">
			<option value="">類別</option>
            {foreach $select_types as $i => $arr}
                {if isset($smarty.get.types)}
					<option value="{$arr.id_rent_house_types}"{if $smarty.get.types == $arr.id_rent_house_types} selected{/if}>{$arr.title}</option>
                {else}
					<option value="{$arr.id_rent_house_types}"{if $default_types == $arr.id_rent_house_types} selected{/if}>{$arr.title}</option>
                {/if}
            {/foreach}
		</select><select name="">
			<option value="">房數</option>
			<option value="1">一房</option>
			<option value="2">二房</option>
			<option value="3">三房</option>
			<option value="4">四房</option>
			<option value="5">五房</option>
		</select>
	</div>
	<div id="search_text">
		<input type="text" class="field" name="search_house" placeholder="請輸入 社區 街道 名稱等 關鍵字或物件編號">
	</div>
	<div id="search_btn">
		<button type="submit" class="btn"><img src="{$THEME_URL}/img/icon/pic_m.svg"></button>
	</div>
</form>