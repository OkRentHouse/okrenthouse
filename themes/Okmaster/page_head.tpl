<div class="head_group">
  {*$Breadcrumb*}
  {if $page}
  {$page_name=$page}
  {/if}
  {if $cms_url}
  {$page_name=$cms_url}
  {/if}

  <div class="head">
  <div class="logo" onclick="window.open('https://www.okmaster.life','_blank ')"></div>
  <div class="hyperlink">
    <label class="wall h-menu {if $page_name=='Sterilization' or $page_name=='Housecheck' or $page_name=='Cleansolution'}now_here{/if}">
      <a href="#" class="dropdown-toggle">超立淨</a>
        <ul class="d-menu" data-role="dropdown">
            <li><a href="/Sterilization">清淨對策</a></li>
            {* <li><a href="/Housecheck">健康宅檢測</a></li>
            <li><a href="/Cleansolution">清淨方案</a></li> *}
        </ul>
    </label>
    {* <label class="wall h-menu"><a href="https://okpt.life/" class="dropdown-toggle">好幫手</a></label>
    <label class="wall h-menu"><a href="https://shar.help/" class="dropdown-toggle">共享圈</a></label>
    <label class="wall h-menu {if $page_name=='smart_lock'}now_here{/if}"><a href="/smart_lock" class="dropdown-toggle">租樂智能鎖</a></label>
    <label class="wall h-menu"><a href="https://www.lifegroup.house/LifeGo" class="dropdown-toggle">生活樂購</a></label>
    <label class="wall h-menu"><a href="https://www.epro.house/" class="dropdown-toggle" href="_blank">裝修達人</a></label>
    <label class="wall h-menu {if $page_name=='store'}now_here{/if}"><a href="/Store" class="dropdown-toggle">生活好康</a></label>
    <label class="wall h-menu {if $page_name=='okmaster_ExpertAdvisor'}now_here{/if}"><a href="/okmaster_ExpertAdvisor" class="dropdown-toggle">生活家族</a></label> *}
    {* <label class="wall h-menu"><a href='javascript:alert("網頁架構中")' class="dropdown-toggle">好幫手</a></label>
    <label class="wall h-menu"><a href='javascript:alert("網頁架構中")' class="dropdown-toggle">共享圈</a></label>
    <label class="wall h-menu {if $page_name=='smart_lock'}now_here{/if}"><a href='javascript:alert("網頁架構中")' class="dropdown-toggle">租樂智能鎖</a></label>
    <label class="wall h-menu"><a href='javascript:alert("網頁架構中")' class="dropdown-toggle">生活樂購</a></label>
    <label class="wall h-menu"><a href='javascript:alert("網頁架構中")' class="dropdown-toggle" href="_blank">裝修達人</a></label>
    <label class="wall h-menu {if $page_name=='store'}now_here{/if}"><a href='javascript:alert("網頁架構中")' class="dropdown-toggle">生活好康</a></label>
    <label class="wall h-menu {if $page_name=='okmaster_ExpertAdvisor'}now_here{/if}"><a href='javascript:alert("網頁架構中")' class="dropdown-toggle">生活家族</a></label>
 *}
    <label class="h-menu {if $page_name=='Aboutokmaster'}now_here{/if}"><a href="/Aboutokmaster" class="dropdown-toggle">關於我們</a></label>
  </div>
  <div class="logon"><a href='javascript:alert("網頁架構中")'>登入</a> | <a href='javascript:alert("網頁架構中")'>註冊</a></div>
  <div class="share"><i class="fas fa-share-alt"></i></div>
  </div>
  <div class="hr_d">
    <div class="hr_light"></div>
    <img class="hr" src="/themes/Okmaster/img/page/hr.png">
  </div>
  <!-- <img class="hr" src="/themes/Okmaster/img/page/hr.png"> -->
</div>
<div class="head_group_zoon">
</div>
{$Breadcrumb}
<style>

:root{
    --sbox:#c3d700;
    --a_color:#56a2ad;
}

.head_group {
position: fixed;
  top: 0px;
  z-index: 9;
  background: #ffffffd6;
  margin-top: 0px;
  width: 100%;
  left: 0px;
}
.head .hyperlink label {
    background: unset;
}
.head_group_zoon {
    height: 150px;
}
.head_group .hr {
  width: 100%;
}

.hr_d {
    position: relative;
}

.hr_light {
    position: absolute;
    height: 6px;
    width: 100%;
    top: 11px;
}

a.dropdown-toggle:hover {
    color: var(--a_color) !important;
    font-size: 15pt;
    transform: scale(2) !important;
    transition: color 0.5s,font-size 0.5s,text-shadow 0.3s linear;
    text-shadow: var(--sbox) 0.1em 0.1em 0.2em, var(--sbox) -0.1em -0.1em 0.2em, var(--sbox) -0.1em 0.1em 0.2em, var(--sbox) 0.1em -0.1em 0.2em;
}


a.dropdown-toggle::before {
    display: none !important;
}

/* .now_here a {
    background: #eee;
    font-size: 18pt;
    color: rgb(42 130 142) !important;
} */

label.wall.h-menu.now_here:before {
    background: rgb(230 245 195);
    width: -webkit-fill-available;
    content: " ";
    height: 440px;
    position: absolute;
    left: 0px;
    bottom: -15px;
    z-index: 0;
    /* margin-right: -10px; */
}

</style>
<script>
$(document).ready(function () {
  //hr_light_flash();
  wallhmenu(0);
  $(".h-menu").find("a").css("color","#fff");

  if(window.outerWidth < 1024){
    $(".head .logo").attr("onclick","");
    $(".head .logo").click(function (){
        if($(".head_group").hasClass("clocked")){

                $(".head_group,.head .hyperlink").removeClass("clocked");

        }else{
            $(".head_group,.head .hyperlink").hide();
            $(".head_group,.head .hyperlink").addClass("clocked",function(){
                $(".head_group,.head .hyperlink").show(200);
            });
        }
    })
  }



 } )

function wallhmenu(i){
  var hmenu=$(".h-menu").eq(i).css("font-size");
  var hmenu_cl=$(".h-menu").eq(i).css("color");
  hmenu_cl="#3BB3C3";
  $(".h-menu").eq(i).find("a").css("color","#fff")
  $(".h-menu").eq(i).animate( {

  "font-size":24

},100,function() { $(this).find("a").animate( { "color":"#c3d700" },50,function() { $(this).css( "filter", "blur(5px)" );$(this).animate( { "color":"#5cb8c5" },50,function() { $(this).css( "filter", "blur(0px)" );$(this).css("color",hmenu_cl); } ) } );
$(this).animate( { "font-size":hmenu },300,function( ) {  } );if ( $(".h-menu").length > i+1 ) { wallhmenu(i+1) } else { hr_light_flash() }   } )


}

 function hr_light_flash(){

   bg=0;
   $(".hr_light").animate( {
       ba: '+=1'
     }, {
     duration:2000,
   step: function( now, fx ) {
   bg = bg + 1;
       // console.log(bg);
       $(this).css(
         // "background","linear-gradient(135deg, #00000000 "+(0+bg)+"%,#ffffff "+(14+bg)+"%,#ffffff "+(21+bg)+"%,#00000000 "+(33+bg)+"%,#00000000 "+(44+bg)+"%,#ffffff "+(51+bg)+"%,#00000000 "+(61+bg)+"%)"
         "background","linear-gradient(135deg, #00000000 "+(-61+bg)+"%,#ffffff "+(-47+bg)+"%,#ffffff "+(-40+bg)+"%,#00000000 "+(-28+bg)+"%,#00000000 "+(-17+bg)+"%,#ffffff "+(-10+bg)+"%,#00000000 "+(0+bg)+"%)"
       );
     }
     } );
     clearTimeout(hr_light_flash);
     setTimeout(hr_light_flash,3000);
 }
</script>
