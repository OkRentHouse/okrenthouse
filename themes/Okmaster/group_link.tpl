<section class="slider_center">
    {if !empty($group_link_data)}
      {include file="../../tpl/group_link.tpl"}
    {/if}
    {foreach $group_link_data as $k=>$v}
    {break}
    <div>
        <a href="{$v.url}"><img alt="{$v.txt}" src="{$v.img}"></a>
    </div>
    {/foreach}
</section>
<script>
    $(".{$slick_data.class}").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        autoplay : true,
        slidesToShow: {$slick_data.slidesToShow},
        slidesToScroll: {$slick_data.slidesToScroll},
    });
</script>
