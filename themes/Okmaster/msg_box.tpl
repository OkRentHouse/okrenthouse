{if isset($_msg) && $_msg != '' && $_error == ''}
<div class="alert alert-success {$this->show_type}" role="alert"><strong>{$strong}</strong>{$_msg}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
{/if}
{if isset($_error) && $_error != ''}
<div class="alert alert-danger {$this->show_type}" role="alert"><strong>{$strong}</strong>{$_error}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
{/if}