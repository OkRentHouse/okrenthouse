// $(document).ready(function(){
// });
$(document).ready(function () {
    $("#put_data").click(function () {
        var type = $("#put_data").data("type");
        var name = $("input[name='name']").val();
        var sex = $("input[name='sex']:checked").val();
        // var time = $("input[name='time']:checked").val();
        var tel = $("input[name='tel']").val();
        var message = $("textarea[name='message']").val();
        var agree = $("input[name='agree']:checked").val();


        if (agree != '1') {
            alert("請勾選同意書");
            return false;
        }

        if (name.length > '20') {
            alert("姓名輸入過長");
            return false;
        } else if (name == "" || name == 'undefined' || name == null) {
            alert("必須輸入姓名");
            return false;
        }

        if (sex != '0' && sex != '1') {
            alert("請選取性別");
            return false;
        }

        if (tel == '') {
            alert("請輸入手機號碼");
            return false;
        }

        // if (time != '0' && time != '1' && time != "2" && time != "3") {
        //     alert("請選取聯絡時段");
        //     return false;
        // }

        if (message != '' && message.length > '100') {
            alert("留言不能超過100個字");
            return false;
        }
        $("#form_data").submit();

    });

    // $('#tel_submit').click(function () {		//按鈕
    //     var phone = $("input[name='tel']").val();
    //     if (phone == '') {
    //         alert("請輸入手機號碼");
    //         return false;
    //     }
    //     $.ajax({
    //         url: document.location.pathname,
    //         method: 'POST',
    //         data: {
    //             'ajax': false,
    //             'action': 'MessageVerificationSMS',
    //             'phone': phone,
    //         },
    //         dataType: 'json',
    //         success: function(data) {
    //             if (data["error"] != "") {
    //                 alert(data["return"]);
    //             } else {
    //                 alert(data["return"]);
    //             }
    //         },

    //         error: function(xhr, ajaxOptions, thrownError) {
    //         }
    //     });

    // });

    $('#tel_code_submit').click(function () { //按鈕

        var phone = $("input[name='user']").val();
        if (phone == '') {
            alert("請輸入手機號碼");
            return false;
        }
        console.log($("input[name='user']").val());
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'InternationalTel',
                'tel_code': phone,
            },
            dataType: 'json',
            success: function (data) {
                if (data["error"] != "") {
                    alert(data["return"]);
                } else {
                    alert(data["return"]);
                }
            },

            error: function (xhr, ajaxOptions, thrownError) {}
        });

    });

});