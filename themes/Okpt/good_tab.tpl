<div class="item_wrap">
    <div class="title">生活好康</div>
    <div class="item_list">
        <a {if stristr($ip,'/Store') } class="active" {/if} href="/Store">特約商店</a>
        <i></i>
        <a {if $ip=='/GoodMember'} class="active" {/if} href="/GoodMember">好康會員</a>
        <i></i>
        <a {if $ip=='/GoodStore'} class="active" {/if} href="/GoodStore">好康店家</a>
    </div>
</div>