<div data-role="header" class="orange_page">
	{if $role != 'dialog'}
	{if !empty($back_url)}<a href="{$back_url}"{if $back_url == '#'} data-rel="back"{/if} class="ui-btn ui-alt-icon ui-icon-carat-l ui-btn-icon-left ui-shadow ui-corner-all ui-nodisc-icon ui-btn-icon-notext" data-transition="slide" data-direction="reverse" data-icon="back"></a>{else}<div class="open_btn glyphicon glyphicon-menu-hamburger"></div>{/if}
	{/if}
	<h1 class="ui-title">{$page_header_toolbar_title}</h1>
	{if isset($right_menu) && !empty($right_menu)}<div class="right_manu">{$right_menu}</div>{/if}
</div>