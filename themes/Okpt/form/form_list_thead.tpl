{strip}<thead>
	<tr>
      {foreach from=$fields.list key=i item=thead}
      	<th class="{$thead['class']}{if $thead['hidden']} hidden{/if}">
            	{$thead['title']}{$thead['search']}
		</th>
	{/foreach}
	{if $has_actions && $display_edit_div}
		<th class="text-center edit_div">{l s='編輯'}</th>
      {/if}
      </tr>
	{if count($fields['list_filter']) > 0}
      <tr class="search">
      {foreach from=$fields.list key=i item=thead}
      	<th class="{$thead['class']}{if $thead['hidden']} hidden{/if}">
			{if in_array($i, $fields['list_filter']) && $thead.filter}
				{if $thead['filter'] === 'date_range'}
					<input type="date" id="{$list_id}Filter_{$i}[]" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[0]}" placeholder="{l s="最小 (不填為不限制"}"/>{l s="至"}<input type="date" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[1]}" placeholder="{l s="最大 (不填為不限制"}"/>
				{elseif $thead['filter'] === 'time_range'}
					<input type="time" id="{$list_id}Filter_{$i}[]" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[0]}" placeholder="{l s="最小 (不填為不限制"}"/>{l s="至"}<input type="time" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[1]}" placeholder="{l s="最大 (不填為不限制"}"/>
				{elseif $thead['filter'] === 'datetime_range'}
					<input type="datetime-local" id="{$list_id}Filter_{$i}[]" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[0]}" placeholder="{l s="最小 (不填為不限制"}"/>{l s="至"}<input type="datetime-local" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[1]}" placeholder="{l s="最大 (不填為不限制"}"/>
				{elseif $thead['filter'] === 'range'}
					<input type="text" id="{$list_id}Filter_{$i}[]" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[0]}" placeholder="{l s="最小 (不填為不限制"}"/>{l s="至"}<input type="text" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}[]" class="form-control" value="{$thead.string_format[1]}" placeholder="{l s="最大 (不填為不限制"}"/>
				{elseif $thead['filter'] === 'time'}
					<input type="time" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}" class="form-control" value="{$thead.string_format}" placeholder="{l s="全部"}"/>
				{elseif $thead['filter'] === 'datetime'}
					<input type="datetime-local" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}" class="form-control" value="{$thead.string_format}" placeholder="{l s="全部"}"/>
				{elseif $thead['filter'] === 'date'}
					<input type="date" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}" class="form-control" value="{$thead.string_format}" placeholder="{l s="全部"}"/>
				{elseif (isset($thead.key) || isset($thead.values) || $thead['title'] == 'active') && ($thead['filter'] !== 'text')}
					<select id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}{if isset($thead.multiple) && !empty($thead.multiple)}[]{/if}" size="{if isset($thead.size) && !empty($thead.size)}{$thead.size}{/if}" class="form-control{if isset($thead.multiple) && !empty($thead.multiple)} selectpicker{/if}"{if isset($thead.multiple) && !empty($thead.multiple)} multiple="multiple" data-actions-box="true" data-select-all-text="{l s="全選"}" data-deselect-all-text="{l s="取消"}" data-none-selected-text="{l s="全選"}" data-actions-box="true" data-selected-text-format="count > 1" {/if}{if isset($thead.data)}{foreach from=$thead.data key=k item=thead_data} data-{$k}="{$thead_data}"{/foreach}{/if}>
						{if empty($thead.multiple)}<option value=""{if $thead.string_format[0] == "" && count($thead.string_format) == 1} selected="selected"{/if}>{l s="全部"}</option>{/if}
						{if isset($thead.key)}
							{foreach from=$related_var.$i key=fi item=fv}
								<option value="{$fi}"
								{if isset($thead.multiple) && !empty($thead.multiple)}
									{foreach from=$thead.string_format item=s_f}
										{if $s_f|string_format:"%s" === $fi|string_format:"%s"} selected="selected"{/if}
									{/foreach}
								{else}
									{if $thead.string_format|string_format:"%s" === $fi|string_format:"%s"} selected="selected"{/if}
								{/if}
								{if isset($fv.p_n)} data-parent="{$fv.p_v}" data-parent_name="{$fv.p_n}"{/if}>
									{if isset($fv.text) && !empty($fv.text)}{$fv.text}{else}{$fv.v}{/if}
								</option>
							{/foreach}
						{else}
							{foreach from=$thead.values key=fi item=fv}
								<option value="{$fv.value}"
								{if isset($thead.multiple) && !empty($thead.multiple)}
									{foreach from=$thead.string_format item=s_f}
										{if $s_f|string_format:"%s" === $fv.value|string_format:"%s"} selected="selected"{/if}
									{/foreach}
								{else}
									{if $thead.string_format|string_format:"%s" === $fv.value|string_format:"%s"} selected="selected"{/if}
								{/if}>
									{$fv.title}
								</option>
							{/foreach}
						{/if}
					</select>
				{else}
					<input type="text" id="{$list_id}Filter_{$i}" name="{$list_id}Filter_{$i}" class="form-control" value="{$thead.string_format}" placeholder="{l s="全部"}"/>
				{/if}
			{else}
				-
			{/if}
		</th>
	{/foreach}
	{if $has_actions && $display_edit_div}
		<th class="text-center edit_div"><button type="submit" class="btn btn-default">{l s='搜尋'}</button></th>
      {/if}
	</tr>
	{/if}
</thead>{/strip}