<ol class="breadcrumb">
	{foreach from=$breadcrumbs key=i item=$breadcrumbs_val}<li class="breadcrumb-item{if count($breadcrumbs) == $i+1} active{/if}"><a{if !empty($breadcrumbs_val['href'])} href="{$breadcrumbs_val['href']}"{/if}>{$breadcrumbs_val['title']}</a></li>{/foreach}
</ol>