var arr_city = [];

class SearchHouse {
    constructor($this) {
        this.tag = true;
        this.submit = false;
        this.div = $($this);
        this.arr_name = [];
        var t = this;
        $('input[name$="[]"]', this.div).each(function () {
            t.get_check_name();
        });


        this.div.submit(function () {
            //使用Submit
            if(t.submit){
                t.arr_name.forEach(function (name) {
                    var value = [];
                    $('input[name="' + name + '[]"]:checked', t.div).each(function () {
                        value.push($(this).val());
                    });
                    $('input[name="' + name + '[]"]:checked', t.div).next('label').addClass('active');
                    if ($('input[name="' + name + '_s"]', t.div).length == 0) {
                        $(t.div).append('<input type="hidden" name="' + name + '_s" value="">');
                    }
                    $('input[name="' + name + '[]"]', t.div).remove();
                    $('input[name="' + name + '_s"]', t.div).val(value.join());
                });
                return true;
            }else{
                location.reload();
                return false;
            }
        });
        t.change_url();
        $(document).on('click', $this + ' .close_tag', function () {
            var $samp = $(this).parents('samp');
            var id = $samp.data('id');
            t.close_tag(id);
        }).on('click', $this + ' .close_all', function () {
            $('input[type="text"],input[type="number"],input[type="hidden"]', t.div).val(null);
            $('input[type="radio"]', t.div).prop('checked', false);
            $('input[type="radio"][value=""]', t.div).prop('checked', true);

            $('.county_city', t.div).html('全部');
            t.change_url();
        }).on($this+' input', function () {
            t.change_url();
        }).on('change', $this + ' input[type="radio"]', function () {
            var $this = $(this);
            t.on_all($this)
        }).on('click', $this + ' input[type="radio"]', function () {
            var $this = $(this);
            var $search_div = $this.parents('.search_div');
            $('input[type="radio"]', $search_div).prop('checked', false);

        });
    }

    close_tag(id){
        var t = this;
        switch (id) {
            case 'search':
                $('input#search', t.div).val(null);
                break;
            default:
                if(id.match('county') != null){
                    $('.county_city', t.div).html('全部');
                    $('.city_div', t.div).html('');
                    $('samp[data-id^="city"]').remove();
                };
                var $input = $('input#'+id, t.div);
                $input.prop('checked', false);
                var $search_div = $input.parents('.search_div');
                if ($('input[type="radio"]', $search_div).length) {
                    $('input[type="radio"][name!="county"]', $search_div).prop('checked', false);
                }
                t.on_all($input);
                break;
        }
        t.change_url();
        $('samp#'+id).remove();
    }

    get_check_name() {
        var t = this;
        $('input[name$="[]"]', this.div).each(function () {
            var name = $(this).prop('name').replace('[]', '');
            if ($.inArray(name, t.arr_name) < 0) {
                t.arr_name.push(name);
            }
        });
    }

    on_all($this){
        var $search_div = $this.parents('.search_div');
        if ($('input[type="radio"]:checked', $search_div).length) {
            $('input[type="radio"][name!="county"]', $search_div).prop('checked', false);
        }else{
            $('input[type="radio"][name!="county"]', $search_div).prop('checked', true);
        }
    }

    change_url() {
        var href = '?';
        var t = this;

        var html = '';
        t.get_check_name();

        if($('#search', t.div).length && $('#search', t.div).val()){
            href += '&search' + '=' + $('#search', t.div).val();
        }

        $('input[type="radio"]:checked', t.div).each(function () {
            var name = $(this).prop('name');
            var val = $(this).val();
            if (name != '' && name != undefined && name != 'undefined' && val != '' && val != '0') {
                href += '&' + name + '=' + val;
                if(t.tag){
                    var id = $(this).attr('id');
                    var text = $(this).next('label').html();
                    html += '<samp data-id="'+id+'"><em>'+text+'</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
                }
            }
        });

        t.arr_name.forEach(function (name) {
            var value = [];
            $('input[name="' + name + '[]"]:checked', t.div).each(function () {
                value.push($(this).val());
                if(t.tag){
                    var id = $(this).attr('id');
                    var text = $(this).next('label').html();
                    html += '<samp data-id="'+id+'"><em>'+text+'</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
                }
            });
            if (value.length > 0) {
                href += '&' + name + '_s' + '=' + value;
            }
        });

        if($('#search', t.div).length && $('#search', t.div).val()){
            href += '&search' + '=' + $('#search', t.div).val();
            if (t.tag) {
                html += '<samp data-id="search"><em>搜尋：' + $('#search', t.div).val() + '</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
                $('.room', t.div).html();
                $('.room_unit', t.div).html();
            }
        }

        $('.tag_div', t.div).html(html);
        if(!t.submit){
            history.pushState({}, 'title', url('path') + href)
            $('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + url('path').substr(1) + href.substr(1));
        };
        return href;
    };

    changeInput(name) {
        $(document).on('change', '#search_box_big input[name$="[]"]');
    }
}

var st = new SearchHouse('#search_box_big');



function show_city_div() {
    var html = '';
    var county = $('input[name="county"]:checked', '#search_box_big').val();
    $.each(arr_city[county], function (index, item) {
        html += '<span class="city"><input type="radio" name="city" id="city_' + index + '" value="' + item + '"><label for="city_' + index + '">' + item + '</label></span>';
    });
    $('.city_div', '#search_box_big').html(html);
    $('.city_div', '#search_box_big').show();
    $('.county_div', '#search_box_big').hide();
}

function city_div() {
    var html = $('#search_box_big input[name="county"]:checked').val();
    $('#search_box_big input[name^="city"]:checked').each(function () {
        html += $(this).val();
    });
    $('.county_city', '#search_box_big').html(html);
    $('#address_1').val(html);

}

function close_search_box_div(event) {
    if (!$('.search, .county_div, .county_div *, .city_div, .city_div *', '#search_box_big').is(event.target)) {
        $('.county_div', '#search_box_big').hide();
        $('.city_div', '#search_box_big').hide();
    }
}

$(document).ready(function () {

    //ajax
    $('#submitAdd').click(function () {





      var pay = "";
      var pay_arr = document.getElementsByName("paying[]");
        for(i=0;i<pay_arr.length;i++){
            if(document.getElementsByName("paying[]")[i].checked){
                if(pay==""){
                    pay = '"'+document.getElementsByName("paying[]")[i].value+'"';
                }else{
                    pay = pay+',"'+document.getElementsByName("paying[]")[i].value+'"';
                }
            }
        }
        pay = '['+pay+']';

        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': true,
                'action': 'Register',
                'title' : $("#title").val(),
                'open_time_w' : $("#open_time_w").val(),
                'close_time' : $("close_time").val(),
                'id_store_type' : $("id_store_type").val(),
                'chain' : $("chain").val(),
                'county' : $("input[name='county']:checked").val(),
                'area' : $("input[name='city']:checked").val(),
                'address_1' : $("#address_1").val(),
                'tel1' : $("#tel1").val(),
                'tel2' : $("#tel2").val(),
                'pay' : pay,
                'service' : $("#service").val(),
                'discount' : $("#discount").val(),
                'contact_person' : $("#contact_person").val(),
                'contact_person_sex' : $("input[name='contact_person_sex']:checked").val(),
                'phone' : $("#phone").val(),
                'legal_agent' : $("#legal_agent").val(),
                'legal_agent_sex' : $("input[name='legal_agent_sex']:checked").val(),
                'company_tel' : $("#company_tel").val(),
                'address' : $("#address").val(),
                'email' : $("#email").val(),
                'line_id' : $("#line_id").val(),
                'firm' : $("#firm").val(),
                'agree' : 　$("#agree").val()
            },
            dataType: 'json',
            success: function (data) {



            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });


    });









    st = new SearchHouse('#search_box_big');
    if($('article.Index').length){
        st.tag = false;
        st.submit = true;
        $('samp', '#search_box_big').remove();
    }
}).on('click', '#search_box_big input[name="county"]', function () {
    show_city_div();
    city_div();

}).on('click', '#search_box_big input[name^="city"]', function () {
    city_div();
    $('.city_div', '#search_box_big').hide();
}).on('click', '#search_box_big .county_city', function () {
    $('.county_div', '#search_box_big').show();
    $('.city_div', '#search_box_big').hide();
}).on('click', 'body', function (event) {
    close_search_box_div(event);
});