<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link rel="icon" href="/themes/Epro/img/index/favicon.png" type="image/x-icon" />
  {if !empty($GOOGLE_SITE_VERIFICATION)}<meta name="google-site-verification" content="{$GOOGLE_SITE_VERIFICATION}" />{/if}
  <title>{$meta_title}</title>
  {if $meta_description != ''}<meta name="description" content="{$meta_description}">{/if}
  {if $meta_keywords != ''}<meta name="keywords" content="{$meta_keywords}">{/if}
  <link rel="stylesheet" href="themes/Epro/assets/web/assets/mobirise-icons2/mobirise2.css">
  <link rel="stylesheet" href="themes/Epro/assets/tether/tether.min.css">
  <link rel="stylesheet" href="themes/Epro/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="themes/Epro/assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="themes/Epro/assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="themes/Epro/assets/dropdown/css/style.css">
  <link rel="stylesheet" href="themes/Epro/assets/socicon/css/styles.css">
  <link rel="stylesheet" href="themes/Epro/assets/theme/css/style.css">
  <link rel="preload" as="style" href="themes/Epro/assets/mobirise/css/mbr-additional.css">
  <link rel="stylesheet" href="themes/Epro/assets/mobirise/css/mbr-additional.css" type="text/css">
  {literal}
  <style type="text/css">

.search__container {
    #padding-top: 80px;
    #width: 10vh;
    text-align: right;
    width: 50%;
    border-width:9px;
    border-style:solid;
    border:1px #000;
}

.search__title {
        font-size: 22px;
        font-weight: 900;
        text-align: center;
        color: #ff8b88;
}

.search__input {
        #width: 11rem;
        width: 5rem;
        padding: 2px 16px;
        background-color: transparent;
        transition: transform 250ms ease-in-out;
        font-size: 14pt;
        line-height: 18px;
        color: #575756;
        background-color: transparent;
      background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' style='fill:LightSeaGreen; stroke:none' viewBox='0 0 24 24'%3E%3Cpath d='M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z'/%3E%3Cpath d='M0 0h24v24H0z' fill='none'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-size: 18px 18px;
        background-position: 95% center;
        border-radius: 30px;
        border: 1px solid #bebebe;
        transition: all 250ms ease-in-out;
        backface-visibility: hidden;
        transform-style: preserve-3d;
    }

.search__input::placeholder {
            color: rgba(87, 87, 86, 0.8);
            text-transform: uppercase;
            letter-spacing: 1.5px;
        }

.search__input:hover,
        .search__input:focus {
            padding: 12px 0;
            outline: 0;
            border: 1px solid transparent;
            border-bottom: 1px solid #575756;
            border-radius: 0;
            background-position: 100% center;
        }
      .cid-s48OLK6784 .navbar-dropdown1{padding:.1rem .5rem;}
      .cid-s48OLK6784 .navbar-brand{max-width: 500px;width:90%;margin:0;padding:0;}
      .navbar-collapse{-webkit-flex-grow:0;}
      .navbar-logo{margin:0;width: 50%;}
      .cid-srUft5QXg9 .item-wrapper{border-radius:0;}
      .cid-s48OLK6784{min-height:45px;;}
      .cid-srUft5QXg9{padding-top: 0;}
      .nav-link .mbr-iconfont{color: #f6ab00;}
      .text-yellow{color: #f6ab00;}
      .cid-s48OLK6784 .navbar .navbar-short {height: 5vh !important;}
      .cid-srTmquGlWr .mbr-gallery .item-wrapper {cursor: default;}
      .cid-srTmquGlWr {background-color:#f4f4f4;}
      .cid-srUvsrbOaC {padding-top:0;margin:0;padding:0;}
      .col-12 .menu-img{width:60vw;display:block; margin:auto;vertical-align : middle;}
      .container-fluid .menu-img{width:60vw;display:block; margin:auto;vertical-align : middle;}
      .container-fluid .menu-img(mini-width: 500px){width:100%;}
      @media (max-width: 768px){
          .col-md-6{flex:none;max-width:100%;}
          .navbar-logo{#flex:none;#max-width:100%;}
          .cid-s48OLK6784 .navbar .navbar-logo img {height: 2.6rem !important;}
      }
      a.nav-link{color: #f6ab00;}
      .hamburger span{color:#f6ab00;}
  </style>
  {/literal}
</head>
<body>