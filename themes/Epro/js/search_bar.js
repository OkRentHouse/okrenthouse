function cell_window_appendurl(v){
  var attr=$(".cell_window a");
  var attr_v=url_query="";
  $(attr).each( function(i,item){
    attr_v=$(item).attr("href");
    url_query=attr_v.substring(attr_v.indexOf("sid")+4,attr_v.length);
    if(url_query.indexOf("&")>-1){
        url_query=url_query.substr(0,url_query.indexOf("&"));
    }
    if($(item).attr("href").indexOf("city") > -1){
      $(item).attr("href","/"+v+"?sid="+url_query+"&city="+$(".city").val());
    }else{
      $(item).attr("href","/"+v+"?sid="+url_query);
    }

  });
}

function cell_window_append_cityurl(v){
  $(".city").val(v);
  var attr=$(".cell_window a");
  var attr_v=url_query="";
  $(attr).each( function(i,item){
    attr_v=$(item).attr("href");
    url_query=attr_v.substring(attr_v.indexOf("city")+5,attr_v.length);
    if(url_query.indexOf("&")>-1){
        url_query=url_query.substr(0,url_query.indexOf("&"));
    }
    if($(item).attr("href").indexOf("?")>-1){
      $(item).attr("href",$(item).attr("href")+"&city="+v);
    }else{
      $(item).attr("href",$(item).attr("href")+"?city="+v);
    }


  });
}


$(document).ready(function() {

  var mune_o_margin = $(".mune").css("margin");

  top_banner_height = parseInt($(".top_banner").css("height"));
  var top_banner_height_c = parseInt($(".top_banner").css("height"));


  $(".search_windows").css("display", "block");

  cell_window_resize("15");

  search_windows_h = $(".search_windows").css("height");

  $(".search_windows").css("display", "none");

  $(".top_banner_2 table").css("height",top_banner_height_c);
  $(".top_banner_2").hide();


  search_windows_h = $(".search_windows").css("height");

  $(".search_bnt").click(

        function() {

            if ($(".search_windows").css("display") == "none") {
                $(".search_windows").css("display", "block");
                $(".search_windows").addClass("search_windows_hode");
                $(".search_windows").animate({ height: search_windows_h }, 300, function() { cell_window_resize("29"); });
                $(".search_bnt").text("︽");
                $(".search_bnt").html('<i class="fas fa-angle-up"></i>');
                p = $(".search_windows");
                position = p.position();
                if ($(window).scrollTop() < parseInt($(".top_banner").css("height"))) { $(window).scrollTop(position.top) };
                $(".search_bar").css({position: "fixed"});
            } else {
                $(".search_windows").hide(300);
                $(".search_bnt").text("︾");
                $(".search_bnt").html('<i class="fas fa-angle-down"></i>');
                //$(".search_bar").css({position: "unset"});
            };
            $(".search_windows").css("height", 0);
            $(".search_windows .cell_window").find("span").css({ "font-size": "", "font-weight": "", "background": "" }).find("a").css({ "-webkit-writing-mode": "vertical-lr" });
            cell_window_resize("40");

        }

    )

    o_width = 0;

    cell_window_resize("94");
    var resizeId;

    function cell_window_resize(line) {
        //console.log("line : " + line);
        o_width = $(window).width() / 11;
        o_width = Math.round(o_width);

        $(".search_windows .cell_window").css("width", "");
        $(".search_windows .cell_window").css("width", o_width);
        o_width = $(".search_windows .cell_window").css("width");

        $(".search_windows .cell_window").css("height",
            $(".search_windows .cell_window").css("width"));

        //console.log("window : " + $(window).height());
        //console.log("top_banner : " + $(".top_banner").css("height"));
        //console.log("search_bar : " + $(".search_bar").css("height"));

        //$(".top_banner").css("height", $(window).height() - parseInt($(".search_bar").css("height")));
        // $(".top_banner img").css("height", $(".top_banner").css("height", ));



    }

    $(".search_windows .cell_window").mousedown(function() {


        span_font_Size_max = "30pt";
        span_font_Size_sec = "20pt";
        span_font_Size_min = "13pt";

        var max_multiple = 2;
        var min_multiple_2 = 1;
        var min_multiple = (($(window).width() - ((parseInt(o_width) * min_multiple_2) * 2) - (parseInt(o_width) * max_multiple))) / 8;

        ////console.log("parseInt(o_width) -> " + parseInt(o_width));

        ////console.log("min_multiple -> " + min_multiple);
        /*
        $(this).parent().find("div").css("width", min_multiple);
        $(this).parent().find("div").css("height", parseInt(o_width) * max_multiple);
        */

        $(this).addClass("over_window");
        $(".over_window").animate({ width: parseInt(o_width) * max_multiple, height: parseInt(o_width) * max_multiple, opacity: 1}, 200, function() {});
        $(".over_window span").animate({ fontSize: span_font_Size_max }, 200, function() {
            $(this).css({ "font-weight": "bold", "background": "rgb(255 255 255 / 61%)" });
            $(this).parent().parent().find("a").css({ "-webkit-writing-mode": "vertical-lr" });
            $(this).find("a").css({ "-webkit-writing-mode": "unset" });

            //$(this).find("a").css("-webkit-writing-mode","vertical-lr")
        });


        if ($(this).next().prop("tagName") == undefined) {
            $(this).prev().prev().addClass("over_window");
        } else {
            $(this).next().addClass("over_window");
        }



        if ($(this).prev().prop("tagName") == undefined) {
            $(this).next().next().addClass("over_window");
        } else {
            $(this).prev().addClass("over_window");
        }

        $(this).parent().find("div:not(.over_window)").animate({ width: min_multiple, height: parseInt(o_width) * max_multiple }, 100, function() {});
        $(this).parent().find("div:not(.over_window)").find("span").animate({ fontSize: span_font_Size_min }, 200, function() { $(this).css({ "font-weight": "", "background": "" }) });
        $(this).next().find("span").animate({ fontSize: span_font_Size_sec }, 200, function() { $(this).css({ "font-weight": "", "background": "" }) });
        $(this).prev().find("span").animate({ fontSize: span_font_Size_sec }, 200, function() { $(this).css({ "font-weight": "", "background": "" }) });

        if ($(this).next().prop("tagName") == undefined) {
            $(this).prev().prev().animate({ width: parseInt(o_width) * min_multiple_2, height: parseInt(o_width) * max_multiple }, 100, function() { sec_span($(this)) });
        } else {
            $(this).next().animate({ width: parseInt(o_width) * min_multiple_2, height: parseInt(o_width) * max_multiple }, 100, function() { sec_span($(this)) });
        }


        if ($(this).prev().prop("tagName") == undefined) {
            $(this).next().next().animate({ width: parseInt(o_width) * min_multiple_2, height: parseInt(o_width) * max_multiple }, 100, function() { sec_span($(this)) });
        } else {
            $(this).prev().animate({ width: parseInt(o_width) * min_multiple_2, height: parseInt(o_width) * max_multiple }, 100, function() { sec_span($(this)) });
        }

        $(this).parent().nextAll().find("div").animate({ width: o_width, height: o_width, opacity: 0.8 }, 100, function() {});
        $(this).parent().prevAll().find("div").animate({ width: o_width, height: o_width, opacity: 0.8 }, 100, function() {});


        $(this).parent().nextAll().find("span").animate({ fontSize: span_font_Size_min }, 200, function() {
            $(this).css({ "font-weight": "", "background": "" });
            $(this).find("a").css({ "-webkit-writing-mode": "vertical-lr" })
        });
        $(this).parent().prevAll().find("span").animate({ fontSize: span_font_Size_min }, 200, function() {
            $(this).css({ "font-weight": "", "background": "" });
            $(this).find("a").css({ "-webkit-writing-mode": "vertical-lr" })
        });


    });

    $(".search_windows .cell_window").mouseout(function() {

        o_width = $(window).width() / 11;

        o_width = Math.round(o_width);

        $(this).removeClass("over_window");



        if ($(this).next().prop("tagName") == undefined) {

            $(this).prev().prev().removeClass("over_window");

        } else {

            $(this).next().removeClass("over_window");

        }



        if ($(this).prev().prop("tagName") == undefined) {

            $(this).next().next().removeClass("over_window");

        } else {
            $(this).prev().removeClass("over_window");

        }

        //$(this).parent().find("div").stop(true,true);

        //$(this).parent().find("div").animate({width:o_width},100,function(){  });

    });

    function sec_span(e) {
        //$(e).css({"font-size":"20pt"})
    }



})
