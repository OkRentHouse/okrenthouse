$( document ).ready(function() {
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 8,
        slidesToScroll: 8
    });
    $(".draggable").removeClass("draggable")
});
