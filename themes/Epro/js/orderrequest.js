
var get_other=false;
$(document).ready(function(){

    $(".forms_div").hide();
    $(".form_1").show();

    $(".GPS").click(function(){
      google_map()
    })



    $(".city").change(function(){
      $(".full_address").val($(".city").val())
    })

    $(".city").change(function(){
      t=$(this).find("option:selected").text();
      $(".full_address").val(t)
    })

    $(".town").change(function(){
      t=$(this).find("option:selected").text();
      $(".full_address").val($(".full_address").val()+t)
    })

    //head_portal_BP();
    $(window).scroll(function(){
      //head_portal_BP();
    })

    $(".draggable").removeClass("draggable");

    var $sticky = $('.search_bar');
     $sticky.hcSticky({
         stickTo: '.container-fluid.main',
         stickyClass : 'content_class'

     });

     $(".parent_item").on("change",function(){
       $(".child_item").find("option").remove();
       child_item=epro_item[$(this).val()];
       if (typeof child_item !== 'undefined' && child_item.length > 0) {
         child_item.forEach(function(item, i){
           $(".child_item").append("<option value='"+item+"'>"+item+"</option>");
           get_other=false;
         });
      }else{
        $(".child_item").append("<option value='0'>沒有此項目內容</option>");
      }



     })

     $(".child_item").on("change",function(){
       console.log($(this).val());
       $(".child_item_other").addClass("none");
       if( $(this).val().indexOf("其它") > -1 || $(this).val().indexOf("其他") > -1){
         $(".child_item_other.none").toggleClass("none");
       }
      })

})


function get_file_img(id){
  let file = document.getElementById(id).files[0];
  if(file.type !='image/png' && file.type !='image/jpeg' ){
      alert("請上傳圖片檔");
      return false;
  }
  var fileReader = new FileReader();
  fileReader.onload = function(event){//讀取完後執行的動作
      document.getElementById(id+'_0').src = event.target.result;
  };
  $("#"+id+'_0').show();
  // $("."+id).hide();
  console.log("."+id);
  $("."+id).css({display: "none"});
  $("."+id).css({background:"unset"});
  fileReader.readAsDataURL(file);
}



function google_map(){

  $(".google_map").attr("src","https://www.google.com/maps/embed/v1/search?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&language=zh-TW&q="+$(".full_address").val())

}

function show_next(e){
  //cl=$(e).parents("[class*='form']").attr("class");
  cl=$(e).parents("[class*='form']").attr("class").indexOf("form_");
  cl=$(e).parents("[class*='form']").attr("class").substring(cl+5,cl+6);
  $(".form_"+ parseInt(cl) ).hide(100,function(){
    $(".form_"+ (parseInt(cl)+1) ).show(200);
  });
}

function show_prev(e){
  //cl=$(e).parents("[class*='form']").attr("class");
  cl=$(e).parents("[class*='form']").attr("class").indexOf("form_");
  cl=$(e).parents("[class*='form']").attr("class").substring(cl+5,cl+6);
  $(".form_"+ parseInt(cl) ).hide(100,function(){
    $(".form_"+ (parseInt(cl)-1) ).show(200);
  });
}

function head_portal_BP(){
  if($(window).scrollTop() > 0){
    if($(window).scrollTop() < 75){
      $(".head_portal").css({"background-position-y": (75-$(window).scrollTop())})
      $(".display_content").css({"background":"#2a1f19"});
    }else{
      $(".head_portal").css({"background-position-y": 0})
      $(".display_content").css({"background":"#fff"});
    }
  };
}
