$(document).ready(function(){
  var top_banner_2_svg = $(".top_banner_2 svg").css("width");
  var top_banner_2_svg_h = parseInt($(".top_banner_2 svg").css("height"))*0.25;
  $(".top_banner_2 svg").first().load(function(){
     top_banner_2_svg = $(".top_banner_2 svg").css("width");
     console.log("top_banner_2_svg:"+top_banner_2_svg);
     top_banner_2_svg_h = parseInt($(".top_banner_2 svg").css("height"))*0.25;
     console.log("top_banner_2_svg_h:"+top_banner_2_svg_h);
     $(".top_banner_2 svg").addClass("small");
  })
  //var top_banner_2_svg = $(".top_banner_2 svg").css("width");


  //$(".top_banner_2 svg").addClass("small");
  $(".top_banner_2 span").css({opacity:0});
  $("#skills,.hb").click(
      function() {
        if($(".top_banner_2").css("display") == "block"){

            $(this).find("svg").stop(true, false);
            top_banner_2_svg_hide($(".top_banner_2").find("svg"),0,$(".top_banner_2").find("svg").length);


          $(".top_banner_2").removeClass("open");
        }else{

          $(".top_banner_2").show(200,function(){
            $(this).find("svg").stop(true, false);
            top_banner_2_svg_show($(this).find("svg"),0,$(this).find("svg").length);
          });



        }

    }
  )

  function top_banner_2_svg_show(e,i,last){

    $(e).eq(i).animate({width: parseInt(top_banner_2_svg),opacity: 1},{duration: 300,specialEasing:{width:"easeOutBounce"},complete:function(){
      console.log(i+":"+parseInt($(".top_banner_2 svg").css("width")));
      if(i==0){top_banner_2_svg_h = parseInt($(".top_banner_2 svg").css("width"))*0.25;};

        if((i+1)<last){top_banner_2_svg_show(e,i+1,last)}
        $(this).parents("li").find("span").animate({opacity: 1,color: "#716d65"},300,function(){})
        $(this).parents("li").css({opacity: 1});

        $(this).animate({height: (parseInt(top_banner_2_svg_h))+50,color: "#ffa500"},{duration: 50,specialEasing:{color:"linear",width:"easeOutBounce"},complete:function(){
          $(this).css({filter: "blur(5px)"});
          $(this).animate({height: (parseInt(top_banner_2_svg_h)),color: "#716d65"},{duration: 500,specialEasing:{color:"linear",width:"easeOutBounce"},complete:function(){
            $(this).parents("li").css({opacity: 0.6});
            $(this).css({filter: "blur(0px)"});
            }})
          }})
    }})
  };

  function top_banner_2_svg_hide(e,i,last){
    console.log("top_banner_2_svg_hide:"+i);
    $(e).stop(true, false);
    $(e).eq(i).animate({width: "5px",opacity: 0},100,function(){
      $(this).parents("li").find("span").animate({opacity: 0},50,function(){
        if((i+1)<last){top_banner_2_svg_hide(e,i+1,last)};
        if((i+1)>=last){$(".top_banner_2").hide(100,function(){});}
       })
    })
  };
  var o_color=$(this).find("svg").css("color");
  $(".row.top_fn ul li").mouseover(function(){
    o_color=$(this).find("svg").css("color");
    $(this).find("svg").css({color:"#fff"});
    $(this).find("span").css({color:"#fff"});
  })

  $(".row.top_fn ul li").mouseout(function(){
    $(this).find("svg").css({color:o_color});
    $(this).find("span").css({color:o_color});
  })

  $(".top_banner img").load(
    function() {
      $(".top_banner_2 table").css("height",$(".top_banner").height());
      if($(window).height() < $(".search_bar").height()+$(".top_banner").height()){
        $(".search_bar").addClass("fix");
      }
    }
  )

  $(".top_banner_2 table,.top_banner_2").css("height",$(window).height());


})
