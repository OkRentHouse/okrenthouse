$(document).ready(function(){
// //給city預設值

    $(".draggable").removeClass("draggable");

    var $sticky = $('.search_bar');
     $sticky.hcSticky({
         stickTo: '.container-fluid.main',
         stickyClass : 'content_class'

     });

     $(".tabmune div").on("click",function(){
       $(this).parent().find("div").removeClass("active");
       $(".tabs").css({display:"none"});
       $("."+$(this).attr("class")+".tabs").css({display:"block"});
       $(this).addClass("active");
     })

     $(".tabs").css({display:"none"});
     $(".tab_1.tabs").css({display:"block"});

});


function get_file_img(id){//這是抓單個id的
  let file = document.getElementById(id).files[0];

  if(file.type !='image/png' && file.type !='image/jpeg' ){
      alert("請上傳圖片檔");
      return false;
  }
  var fileReader = new FileReader();
  fileReader.onload = function(event){//讀取完後執行的動作
      document.getElementById(id+'_0').src = event.target.result;
  };
  $("#"+id+'_0').show();
  // $("."+id).hide();
  $("."+id).css({display: "none"});
  $("."+id).css({background:"unset"});
  fileReader.readAsDataURL(file);
}

function get_ilfe_img_on_name(e){//為了符合name而做的
   let data_name = $(e).data("name");
    $(e).parent().find("input[name='"+data_name+"']").click();
}

function get_file_img_arr(e,name){//抓被限制的多陣列
    let id = $(e).attr("id");//取得id
    let file = document.getElementById(id).files[0];
    if(file.type !='image/png' && file.type !='image/jpeg' ){
        alert("請上傳圖片檔");
        return false;
    }

    var fileReader = new FileReader();
    fileReader.onload = function(event){//讀取完後執行的動作
        document.getElementById(id+'_0').src = event.target.result;
    };
    $("#"+id+'_0').show();
    // $("."+id).hide();
    $("."+id).css({display: "none"});
    $("."+id).css({background:"unset"});
    fileReader.readAsDataURL(file);
}



function head_portal_BP(){
  if($(window).scrollTop() > 0){
    if($(window).scrollTop() < 75){
      $(".head_portal").css({"background-position-y": (75-$(window).scrollTop())})
      $(".display_content").css({"background":"#2a1f19"});
    }else{
      $(".head_portal").css({"background-position-y": 0})
    }
  };
}

function sumbit_data() {
    $("#sumbit_data").submit();
}


function get_id_epro_windows_item(e) {
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'Epro_windows_item',
            'id_epro_windows' : $(e).val(),
        },
        dataType: 'json',
        success: function (data) {
            if(data["error"] !=""){
                alert(data["error"]);
            }else{
                let html_data = '';
                data["return"].forEach(function(item, index, arr) {
                    html_data +='<option value="'+item['id_epro_windows_item']+'" data-name="'+item['title']+'"><span>'+item['title']+'</span></option>';
                });
                $(e).parent().find("select[name='id_epro_windows_item[]']").empty();
                $(e).parent().find("select[name='id_epro_windows_item[]']").html(html_data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function get_city(e){
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'City',
            'id_county' : $(e).val(),
        },
        dataType: 'json',
        success: function (data) {
            if(data["error"] !=""){
                alert(data["error"]);
            }else{
                let html_data = '';
                data["return"].forEach(function(item, index, arr) {
                    html_data +='<option value="'+item['id_city']+'" data-name="'+item['city_name']+'"><span>'+item['city_name']+'</span></option>';
                });
                $(e).parent().find(".city").empty();
                $(e).parent().find(".city").html(html_data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function server_address_add(e) {
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'server_address',
        },
        dataType: 'json',
        success: function (data) {
            if(data["error"] !=""){
                alert(data["error"]);
            }else{
                let html_data = '';
                let county = '';
                let city = '';
                data["return"]["county"].forEach(function(item, index, arr) {
                    let checked='';
                    if(item['checked']=='checked'){
                        checked='selected="selected"';
                    }
                    county +='<option value="'+item['id_county']+'" data-name="'+item['county_name']+'" '+checked+'>'+item['county_name']+'</option>';
                });
                data["return"]["city"].forEach(function(item, index, arr) {
                    city +='<option value="'+item['id_city']+'" data-name="'+item['city_name']+'">'+item['city_name']+'</option>';
                });

                $(e).parent().parent().append('<li class="add">\n' +
                    '<input type="hidden" name="id_epro_server_address[]">'+
                    '              <select class="county" name="id_county[]" onchange="get_city(this);">\n' +
                    county+
                    '              </select>\n' +
                    '              <select  name="id_city[]" class="city">\n' +
                    city+
                    '              </select>\n' +
                    '          <svg onclick="server_address_delete(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="minus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-minus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 242c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12H108c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h232z" class=""></path></svg>\n' +
                    '        </li>');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function server_address_delete(e) {
    let id_epro_server_item = $(e).parent().find("input[name='id_epro_server_address[]']").val();//取得id
    console.log(id_epro_server_item);
    $(e).parent().remove();
}

function server_item_add(e) {
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'Epro_windows',
        },
        dataType: 'json',
        success: function (data) {
            if(data["error"] !=""){
                alert(data["error"]);
            }else{
                let epro_windows = '';
                let epro_windows_item ='';
                console.log(data);
                data["return"]["epro_windows"].forEach(function(item, index, arr) {
                    epro_windows +='<option value="'+item['id_epro_windows']+'" data-name="'+item['title']+'"><span>'+item['title']+'</span></option>';
                });
                data["return"]["epro_windows_item"].forEach(function(item, index, arr) {
                    epro_windows_item +='<option value="'+item['id_epro_windows_item']+'" data-name="'+item['title']+'"><span>'+item['title']+'</span></option>';
                });

                $(e).parent().parent().parent().append(
                    '      <ul class="skill">\n' +
                    '        <input type="hidden" name="id_epro_server_item[]">\n' +
                    '        <select name=\'id_epro_windows[]\' onchange="get_id_epro_windows_item(this);">\n' +
                        epro_windows   +
                    '        </select>\n' +
                    '        <select name=\'id_epro_windows_item[]\'>\n' +
                    epro_windows_item   +
                    '        </select>\n' +
                    '        <div class="skill_input flex">\n' +
                    '          <input type="number" name="price[]" placeholder="價格">\n' +
                    '          <input type="text" name="unit[]" placeholder="單位" maxlength="32">\n' +
                    '          <svg onclick="server_item_delete(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="minus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-minus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 242c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12H108c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h232z" class=""></path></svg>\n' +
                    '        </div>\n' +
                    '      </ul>');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function server_item_delete(e){//未做ajax刪除部分
    let id_epro_server_item = $(e).parent().parent().find("input[name='id_epro_server_item[]']").val();//取得id
    $(e).parent().parent().remove();
}


function lincess_add(e) {
    let length = $(".lincess_img").length;
    $(e).parent().parent().parent().append('<li class="content lincess lincess_img">\n' +
        '      <svg aria-hidden="true" onclick="lincess_delete(this);" focusable="false" data-prefix="far" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x" style="position: absolute;z-index: 99;right: -34px;top: -13px;height: 25px;transform: rotate(45deg);"><path fill="currentColor" d="M384 240v32c0 6.6-5.4 12-12 12h-88v88c0 6.6-5.4 12-12 12h-32c-6.6 0-12-5.4-12-12v-88h-88c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h88v-88c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v88h88c6.6 0 12 5.4 12 12zm120 16c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-48 0c0-110.5-89.5-200-200-200S56 145.5 56 256s89.5 200 200 200 200-89.5 200-200z" class=""></path></svg>\n' +
        '      <input type="text" name="license_remarks[]" style="width:300px;margin: 1em 0;border-radius: 10px;" placeholder="證照名稱">\n' +
        '      <div class="upload" onclick="get_ilfe_img_on_name(this)" data-name="license[]" data-value="0" class="get_file"><div class="license_'+length+'"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>\n' +
        '        <img class="view" name="license_img[]" id="license_'+length+'_0" src="" style="display: none">\n' +
        '      </div>\n' +
        '      <input type="file" style="display:none" name="license[]" id="license_'+length+'" onchange="get_file_img_arr(this,\'license\')"  >\n' +
        '    </li>');
}

function lincess_delete(e){
    $(e).parent().remove();
}

