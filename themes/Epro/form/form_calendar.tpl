<div id='loading'>loading...</div>
<div id="calendar"></div>
<div id="calendar_list"></div>
<script type="text/javascript">
	function src_auto(){
		return 'auto';
	};
	var is_new = false;
	var fullCalendar_header_right = 'month,agendaWeek,agendaDay,listMonth';
	{if $smarty.get.is_new}is_new = {$smarty.get.is_new};{/if}
	if(is_new != false){
		fullCalendar_header_right = '';
	};
	$(document).on('click', '.fc-button-group button', function(){
		all_a();
		$('#calendar_list').html('');
		if($('.fc-today', '.fc-month-view').index() > -1){
			var $td = $('.fc-today', '.fc-month-view');
			var _date = $td.data('date');
			$('td.fc-day').removeClass('action');
			$td.addClass('action');
			show_link_today(_date);
		};
	});
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: fullCalendar_header_right
		},
		locale: '{configuration name="schedule_settings_locale"}',
		height: 'auto',
		contentHeight: src_auto,
		navLinks: false, 			// can click day/week names to navigate views
		businessHours: true, 		//圖險營業時間
		editable: false,
		timeFormat: 'HH:mm',
		dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
		businessHours: {
		    dow:{configuration name="schedule_settings_business_hours_date"},
		    start:'{configuration name="schedule_settings_business_hours_start"}',
		    end:'{configuration name="schedule_settings_business_hours_end"}'
		},
		events: {
			url: document.location.pathname+'?ajax=true&action=calendar&is_new='+is_new,
			error: function() {
				$.growl.error({
					title: '',
					message: '{l s="載入失敗!"}'
				});
			}
		},
		loading: function(bool) {
			$('#loading').toggle(bool);
			all_a();
			$('#calendar_list').html('');
			if($('.fc-today', '.fc-month-view').index() > -1){
				var $td = $('.fc-today', '.fc-month-view');
				var _date = $td.data('date');
				$('td.fc-day').removeClass('action');
				$td.addClass('action');
				show_link_today(_date);
			};
		},
		dayClick:function( event, jsEvent, view ) {
			var $td = $(this);
			var _date = $td.data('date');
			$('td.fc-day').removeClass('action');
			$td.addClass('action');
			show_link_today(_date);
		}
	});
	function all_a(){
		$('td.fc-day-top').each(function(){
			var _date = $(this).data('date');
			$('a[href$="#'+_date+'"]').eq(0).addClass('show');
		});
	}
	function show_link_today(_date){
		$('#calendar_list').html('');
		if($('.fc-month-view').index() > -1){
			var htm = '';
			$('a[href$="#'+_date+'"]').each(function(){
				var href = $(this).attr('href');
				var color = $(this).css('background-color');
				var h = $('.fc-title', this).html();
				if(h != undefined){
					var si = h.indexOf(' ');
					htm += '<a href="'+href+'"><samp class="time">'+h.substring(0, si+1)+'</samp><samp class="title">'+h.substring(si)+'</samp></a>';
				};
			});
			$('#calendar_list').html(htm);
		}
	};
</script>