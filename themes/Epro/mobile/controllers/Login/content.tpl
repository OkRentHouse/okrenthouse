<form action="/login" method="post" data-transition="none" data-ajax="false">
	<h3 class="text-center">{l s="用戶登入"}</h3>
	<div><input placeholder="{l s="帳號"}" type="email" maxlength="20" name="email" value="{$smarty.post.email}" required></div>
	<div><input placeholder="{l s="密碼"}" type="password" maxlength="20" name="password" required></div>
	<div>{l s="自動登入"} <select name="auto_loging" id="auto_loging" data-role="flipswitch" data-mini="true"><option value="">{l s="Off"}</option><option value="1" selected>{l s="On"}</option></select></div>
	<div><button type="submit" name="login">{l s="用戶登入"}</button></div>
	<div><a href="/register" class="register" data-role="button" data-transition="slide">{l s="新用戶註冊"}</a></div>
	{hook h='displayLoging'}
	{*<div>{l s="社群帳號登入"}</div>*}
	{*<hr>*}
	{*<div><button>{l s="Google"}</button></div>*}
	{*<div><button>{l s="Facebook"}</button></div>*}
	<input type="hidden" name="login" value="1">
	<input type="hidden" name="post_code" value="{$post_code}">
</form>