<div class="list">
	{if count($arr_house_message)}
		{foreach from=$arr_house_message key=key item=msg}
			<div><samp class="orange_title title">{$msg.title}</samp><samp class="time small_gray">{$msg.time}</samp></samp>{if $show_house_code}<samp class="house_code">{$msg.house_code}</samp>{/if}<samp class="message">{$msg.message}</div>
		{/foreach}
	{else}
		<div class="text-center">{l s="您尚未有最新訊息"}</div>
	{/if}
</div>
<div class="text-center">{l s="訊息記錄只顯示1個月內紀錄"}</div>