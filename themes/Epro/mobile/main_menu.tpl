<div class="menu_shadow"></div>
{*<div class="open_btn glyphicon glyphicon-menu-hamburger"></div>*}
<div class="side_menu">
	<div class="close_btn glyphicon glyphicon-remove"></div>
	<div class="menu_top{if !empty($house_code)} two{/if}">{if !empty($web)}<h3>{$web}</h3>{/if}<div class="user">
			{if !empty($house_code)}<div class="house_code">戶別｜{$house_code}</div>{/if}
			<div class="name">{if !empty($house_code)}住戶｜{/if}{$name}</div>
		</div>
	</div>
	<div class="main_menu">
		<div class="menu_level">
			<div><a href="/coupon" data-transition="slide"{* data-ajax="false"*}><div><img src="{$THEME_URL}/mobile/img/news.svg"><div class="txt">{l s="好康特報"}</div></div></a></div>
			<div><a href="/news" data-transition="slide"{* data-ajax="false"*}><div><img src="{$THEME_URL}/mobile/img/news.svg"><div class="txt">{l s="公告欄"}</div></div></a></div>
			<div><a href="javascript:void(0);"><div><img src="{$THEME_URL}/mobile/img/mail.svg"><div class="txt">{l s="我的信箱"}</div><i class="transition glyphicon glyphicon-chevron-right"></i></div></a>
				<div class="menu_level subs orange_page" style="display: none;">
					<div><a href="/registered" data-transition="slide"{* data-ajax="false"*}><div>{l s="掛號信件"}</div></a></div>
					<div><a href="/package" data-transition="slide"{* data-ajax="false"*}><div>{l s="我的包裹"}</div></a></div>
					<div><a href="/posting" data-transition="slide"{* data-ajax="false"*}><div>{l s="寄放物品"}</div></a></div>
					<div><a href="/message" data-transition="slide"{* data-ajax="false"*}><div>{l s="我的訊息"}</div></a></div>
				</div></div>
			<div><a href="/public" data-transition="slide"{* data-ajax="false"*}><div><img src="{$THEME_URL}/mobile/img/public.svg"><div class="txt">{l s="公設預約"}</div></div></a></div>
			<div><a href="/NaturalGas" data-transition="slide"{* data-ajax="false"*}><div><img src="{$THEME_URL}/mobile/img/gas.svg"><div class="txt">{l s="瓦斯抄表"}</div></div></a></div>
			<div><a href="/vote" data-transition="slide"{* data-ajax="false"*}><div><img src="{$THEME_URL}/mobile/img/vote.svg"><div class="txt">{l s="社區投票"}</div></div></a></div>
			<div><a href="/payment" data-transition="slide"{* data-ajax="false"*}><div><img src="{$THEME_URL}/mobile/img/payment.svg"><div class="txt">{l s="我要繳費"}</div></div></a></div>
			<div><a href="javascript:void(0);" data-transition="slide"{* data-ajax="false"*}><div><img src="{$THEME_URL}/mobile/img/payment.svg"><div class="txt">*{l s="房屋修繕"}</div><i class="transition glyphicon glyphicon-chevron-right"></i></div></a>
				<div class="menu_level subs orange_page" style="display: none;">
					<div><a href="/Service" data-transition="slide"{* data-ajax="false"*}><div>{l s="房屋修繕"}</div></a></div>
					<div><a href="/Search" data-transition="slide"{* data-ajax="false"*}><div>{l s="排程計畫"}</div></a></div>
				</div></div>
			<div><a href="/more" data-transition="slide"><div><img src="{$THEME_URL}/mobile/img/more.svg"><div class="txt">{l s="更多"}</div></div></a></div>
		</div>
	</div>
</div>