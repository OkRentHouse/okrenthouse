</article>
{if $group_link=='1'}
      {include file="group_link.tpl"}
{/if}
      {if $display_footer}
            <footer>
                  <div class="footer epro_footer">
                        <table>
                              <tbody>
                                    <tr>
                                          <td>
                                                {if !empty($footer_txt1)}<div class="footer_txt1">{$footer_txt1}</div>{/if}
                                                {if !empty($footer_txt2)}<div class="footer_txt2">{$footer_txt2}</div>{/if}
                                                {if !empty($footer_txt3)}<div class="footer_txt3">{$footer_txt3}</div>{/if}
                                          </td>
                                    </tr>
                              </tbody>
                        </table>
                  </div>
            </footer>
            {*include file="../../../../modules/DesigningFooter/designingfooter.tpl"*}
      {/if}
      {foreach from=$css_footer_files key=key item=css_uri}
            <link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
      {/foreach}
      {foreach from=$js_footer_files key=key item=js_uri}
            <script src="{$js_uri}"></script>
      {/foreach}
      </div>
</body>
{if $foot_js}
      {$foot_js}
{/if}
{if $group_link=='1'}
      {*$group_link_js*}
      {$group_link_main_js}
{/if}
</html>
