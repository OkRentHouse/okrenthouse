

<div class="bottom_fix_div">
</div>

{include file="$tpl_dir./search_bar.tpl"}

<div class="container-fluid">
        <header style="max-width: 960px;">

        </header>


        <div id="tm-bg"></div>
        {include file="../../page_banner.tpl"}
        <div class="details">
			<div class="details__bg details__bg--down" style="opacity: 0; transform: none;">
				<button class="details__close" style="opacity: 0; transform: translateY(100%);"><i class="fas fa-2x fa-times icon--cross tm-fa-close"></i></button>
				<div class="details__description" style="opacity: 0;"></div>
			</div>
			</div></div>
    </div>

    <div class="container-fluid main" id="main">

        <div class="breadcrumbs">
            <p>{$breadcrumbs_html}{if $sidname} &gt; {$sidname}{/if}</p><br>
        </div>




        <div class="row">
            <div class="col-xs-12 col-sm-2">
                {include file="../../page_left.tpl"}
            </div>
            <div class="col-xs-12 col-sm-10">


                <div>


                    <div class="desk">
                        <h3 class="center m030"><img src="/themes/Epro/img/index/tool-1.png" alt=""> 最新 <img src="/themes/Epro/img/index/tool-2.png" alt=""></h3>
                        <div class="avi">
                            <div class="card-deck">
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_1.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_2.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_3.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile">  <img src="/themes/Epro/img/index/people_4.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="m00600">
                        <div class="row justify-content-center align-items-center m15">
                            <!-- *** 分頁page-1 *** -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            &lt;</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">&gt;</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="desk">
                        <h3 class="center m30"><img src="/themes/Epro/img/index/tool-1.png" alt=""> 熱門 <img src="/themes/Epro/img/index/tool-2.png" alt=""></h3>
                        <div class="avi">
                            <div class="card-deck">
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_5.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile">  <img src="/themes/Epro/img/index/people_6.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile">   <img src="/themes/Epro/img/index/people_4.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_3.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="m00600">
                        <div class="row justify-content-center align-items-center m15">
                            <!-- *** 分頁page-1 *** -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            &lt;</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">&gt;</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>






                    <div class="desk">
                        <h3 class="center m30"><img src="/themes/Epro/img/index/tool-1.png" alt=""> 優惠 <img src="/themes/Epro/img/index/tool-2.png" alt=""></h3>
                        <div class="avi">
                            <div class="card-deck">
                                <div class="card">
                                    <a href="/Masterprofile">  <img src="/themes/Epro/img/index/people_2.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_1.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_3.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_5.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="">
                        <div class="row justify-content-center align-items-center m15">
                            <!-- *** 分頁page-1 *** -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            &lt;</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">&gt;</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <!-- === END CONTENT === -->
                </div>
            </div>
        </div>
    </div>
