<div class="col-sm-12 tab_1 tabs">

  <ul>
    {$skill=array("裝潢設計","衛浴","廚具","水電","冷氣空調","電器","熱水器","油漆粉刷","壁紙","管線","通管","燈飾照明","木作","泥作","鐵作","地板","石材及美化","玻璃","磁磚","防水抓漏","門窗門鎖","智能鎖","安控監視","消防保全","園藝植栽","環境清潔","沙發.床墊.窗簾.地毯除螨清洗","消毒滅菌","除蟲除味","窗簾地毯","淨水設備","升降設備","機電","弱電","節能","綠建材","智慧居家","房屋健檢","驗屋檢測","無障礙空間")}
    {$skill_child=array("空間設計規劃","翻修整建","隔局重整","頂樓增建","局部改造","套房改建","輕鋼架工程","輕隔間工程","設計繪圖")}
    <li class="title required hsa_select">叫修類別:
    <select class="parent_item org_bor">
      {for $i=0;$i<sizeof($skill);$i++}
      <option value="{$skill[$i]}">{$skill[$i]}</option>
      {/for}
    </select>
    項目 <select class="child_item org_bor">
      {for $i=0;$i<sizeof($skill_child);$i++}
      <option>{$skill_child[$i]}</option>
      {/for}
    </select>

<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sort-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-sort-down fa-w-10 fa-3x"><path fill="currentColor" d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z" class=""></path></svg>
<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sort-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-sort-down fa-w-10 fa-3x"><path fill="currentColor" d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z" class=""></path></svg>


    <div class="child_item_other none" style="margin-left: 19em;">其他(請說明)：<input class="child_item_other" type="text" style="width: 10em;border-radius: 10px;height: 30px;"></div>
  </li>
    <li class="content"></li>
    <li class="title required">需求了解:</li>
    <li class="content alt">
      <ul style=" position: relative;">
        {$alt_1=array("壁面","天花板","踢腳板","壁癌","裂縫","拆除壁紙")}
        {$alt_2=array("虹牌","青葉","得利","立邦","明星","其它")}
        <li class="title ">範圍</li>
        <li class="content org_bor">
          {for $i=0;$i<sizeof($alt_1);$i++}
            <input type="checkbox"><label for="vehicle_{$i}">{$alt_1[$i]}</label>
          {/for}
        </li>
        <!-- 塗料廠牌及顏色(註:未指定者,依專業職人評估選用) -->
        <li class="title ">指定</li>
        <li class="content org_bor">
          {for $i=0;$i<sizeof($alt_2);$i++}
            <input type="checkbox"><label for="vehicle_{$i}">{$alt_2[$i]}</label>
          {/for}
          <input type="text" class="other_input" placeholder="色號">
          <!-- <label for="notify">指定塗料廠牌及顏色(註:未指定者,依專業職人評估選用)</label> -->
        </li>
        <li class="title hidden">色號 | <input type="text"></li>
      </ul>
    </li>
    <li class="title required">問題描述 <label class="mark"> 請詳盡描述問題狀況，便於加速故障排除處理</label></li>
    <li class="content"><textarea class="org_bor" placeholder="限100字内"></textarea></li>
    <li class="title">
      <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="account_img[]" data-value="0" class="get_file" style=" width: 40px;"> 附件上傳 <label class="mark">最多可放5張照片，每張大小不超過2MB。( 格式為pdf、jpg、png、txt ) </label>:
    </li>

    <li class="content upload_div">{for $i=1;$i<6;$i++}<div class="upload_{$i} org_bor" data-name="myphoto_{$i}[]" data-value="0" class="get_file">
      <img class="view" id="frame_v_{$i}_0" src="" style="display: none">
      <div class="frame frame_v_{$i}" onclick="$('#frame_v_{$i}').click()">

        <svg class="svg-inline--fa fa-cloud-upload-alt fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cloud-upload-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" data-fa-i2svg=""><path fill="currentColor" d="M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4zM393.4 288H328v112c0 8.8-7.2 16-16 16h-48c-8.8 0-16-7.2-16-16V288h-65.4c-14.3 0-21.4-17.2-11.3-27.3l105.4-105.4c6.2-6.2 16.4-6.2 22.6 0l105.4 105.4c10.1 10.1 2.9 27.3-11.3 27.3z"></path></svg><!-- <i class="fas fa-cloud-upload-alt"></i> Font Awesome fontawesome.com --><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>
      <input type="file" style="display:none" id="frame_v_{$i}" name="myphoto_{$i}[]" onchange="get_file_img('frame_v_{$i}')" required="required">
    </div>{/for}</li>

    <li class="title">
      <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="account_img[]" data-value="0" class="get_file" style=" width: 40px;"> 影片上傳 <label class="mark">最多可放1個附件，每個大小不超過2MB。 </label>:
    </li>
    <li class="content upload_div_1">{for $i=0;$i<5;$i++}<div {if $i>0}style="border: 0px !important;"{/if} class="upload_{$i} org_bor" data-name="myphoto_{$i}[]" data-value="0" class="get_file">
      
      <img class="view" id="frame_v1_{$i}_0" src="" style="display: none">
      <div class="frame frame_v1_{$i}" onclick="$('#frame_v1_{$i}').click()" {if $i>0}style="display: none;"{/if} >

        <svg class="svg-inline--fa fa-cloud-upload-alt fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cloud-upload-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" data-fa-i2svg=""><path fill="currentColor" d="M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4zM393.4 288H328v112c0 8.8-7.2 16-16 16h-48c-8.8 0-16-7.2-16-16V288h-65.4c-14.3 0-21.4-17.2-11.3-27.3l105.4-105.4c6.2-6.2 16.4-6.2 22.6 0l105.4 105.4c10.1 10.1 2.9 27.3-11.3 27.3z"></path></svg><!-- <i class="fas fa-cloud-upload-alt"></i> Font Awesome fontawesome.com --><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>
      <input type="file" style="display:none" id="frame_v1_{$i}" name="myphoto_{$i}[]" onchange="get_file_img('frame_v1_{$i}')" required="required">
    </div>{/for}</li>

    <li class="title required hsa_select">報修地址:
      <div class="address hidden" style="    display: inline;    position: absolute;    width: 70%;">
<select class="city">
          <option disabled="" selected="">桃園市</option>
      <option data-content="台北市" class="county_0" value="county_0">台北市</option><option data-content="基隆市" class="county_1" value="county_1">基隆市</option><option data-content="新北市" class="county_2" value="county_2">新北市</option><option data-content="連江縣" class="county_3" value="county_3">連江縣</option><option data-content="宜蘭縣" class="county_4" value="county_4">宜蘭縣</option><option data-content="新竹市" class="county_5" value="county_5">新竹市</option><option data-content="新竹縣" class="county_6" value="county_6">新竹縣</option><option data-content="桃園市" class="county_7" value="county_7">桃園市</option><option data-content="苗栗縣" class="county_8" value="county_8">苗栗縣</option><option data-content="台中市" class="county_9" value="county_9">台中市</option><option data-content="彰化縣" class="county_10" value="county_10">彰化縣</option><option data-content="南投縣" class="county_11" value="county_11">南投縣</option><option data-content="嘉義市" class="county_12" value="county_12">嘉義市</option><option data-content="嘉義縣" class="county_13" value="county_13">嘉義縣</option><option data-content="雲林縣" class="county_14" value="county_14">雲林縣</option><option data-content="台南市" class="county_15" value="county_15">台南市</option><option data-content="高雄市" class="county_16" value="county_16">高雄市</option><option data-content="澎湖縣" class="county_17" value="county_17">澎湖縣</option><option data-content="金門縣" class="county_18" value="county_18">金門縣</option><option data-content="屏東縣" class="county_19" value="county_19">屏東縣</option><option data-content="台東縣" class="county_20" value="county_20">台東縣</option><option data-content="花蓮縣" class="county_21" value="county_21">花蓮縣</option>
  </select>市(縣)
  <select class="town">
        <option>桃園區</option>
        <option>中壢區</option>
        <option>中壢區</option>
        <option>大溪區</option>
        <option>大溪區</option>
        <option>楊梅區</option>
        <option>楊梅區</option>
        <option>蘆竹區</option>
        <option>蘆竹區</option>
        <option>大園區</option>
        <option>大園區</option>
        <option>龜山區</option>
        <option>龜山區</option>
        <option>八德區</option>
        <option>八德區</option>
        <option>龍潭區</option>
        <option>龍潭區</option>
        <option>平鎮區</option>
        <option>平鎮區</option>
        <option>新屋區</option>
        <option>新屋區</option>
        <option>觀音區</option>
        <option>觀音區</option>
        <option>復興區</option>
        <option>復興區</option>
</select>鄉鎭市區<br><input type="text" placeholder="完整地址" onchange="" class="full_address"><button type="button" class="GPS">定位</button></div>

<div class="address" style="    display: inline;width: 70%;">
<input type="text" class="address_input border_bottom">市(縣)<input type="text" class="address_input border_bottom">鄉鎭市區<input type="text" class="address_input border_bottom">路(街)<input type="text" class="address_input border_bottom">段
</div>
</li>

{$contact_timing=array("上午","中午","下午","晚間","全天","不便接聽可留言或簡訊")}
<li class="title required required">聯絡時段:{for $i=0;$i<sizeof($contact_timing);$i++}<input type="checkbox"><label>{$contact_timing[$i]}</label>{/for}</li>

<li class="title required has_txt">預算範圍:<input type="number"> ~ <input type="number"></li>
<li class="content"></li>
<li class="title required has_txt">完成期限:<input type="date"></li>
<li class="content"></li>
<li class="title required has_txt">達人指定:<input type="radio" name="mastercode"><label>是<input type="text" placeholder="請填入達人代碼" class="only_underbor"> </label><input type="radio" name="mastercode"><label>不指定</label></li>
<li class="content"></li>

    <li class="content has_txt"></li>
    <li class="add3rem title hidden">goog1e地圖定位</li>
    <li class="content hidden">
      <iframe class="google_map" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&language=zh-TW&q=taiwan" frameborder="0" style="display: none;border:0;width: 100%;height: 400px;" allowfullscreen="">
             </iframe>
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.7462891413193!2d121.30011081527928!3d25.00873558398417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34681fac202cdfad%3A0x1b7ec140531245a3!2zMzMw5qGD5ZyS5biC5qGD5ZyS5Y2A5Lit5q2j6Lev!5e0!3m2!1szh-TW!2stw!4v1609758573559!5m2!1szh-TW!2stw" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></li>
    {if !$smarty.session.nickname}
    <li class="title hidden">您是「裝修達人」epro的會員嗎?</li>
    <li class="content buttons hidden"><button type="button" class="ismamber">是</button><button type="button" class="ismamber">否</button></li>
    {/if}

    <li class="content buttons"><button type="button" class="Previous" onclick="show_prev(this)">取消</button><button type="button" class="next" onclick="show_next(this)">送出</button></li>
  </ul>
</div>
