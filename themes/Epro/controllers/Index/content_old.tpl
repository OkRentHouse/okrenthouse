<div class="bottom_fix_div">
</div>
{include file="$tpl_dir./search_bar.tpl"}
<div class="mune" style="display: none;">
    <div class="row">
        <div class="cell-3 mr">
            <ul>
                <li class="list"><a href="/Service">找達人</a></li>
                <li class=""><a href="/Masterview">接案子</a></li>
                <ul>
        </div>


        <!--<div class="cell-2 logo"></div>-->

        <div class="cell-6 bar_m">
            <div class="search_bar_m">
              <div class="selects">
              <select class="city" onchange="cell_window_append_cityurl(this.value)">
                <option disabled selected>桃園市</option>
              </select>
              <select class="itembox" onchange="cell_window_appendurl(this.value)">
                <option disabled selected>請選擇</option>
                <option value="Service" {if $page=='Service'}selected{/if}>PRO專業技術型</option>
                <option value="Vip" {if $page=='Vip'}selected{/if}>VIP量身訂製型</option>
              </select>
              </div>
                <input type="text" class="search_txtt" placeholder="可輸入，如：冷氣不冷 | 馬桶不通 | 壁癌粉刷 ... ">
                <button value="search" class="search_bnt"><i class="fas fa-angle-down"></i></button>
            </div>

        </div>

        <div class="cell-3 md">
            <ul>
                {if $smarty.session.nickname}
                  <li class="list"><a href="/Profile">{$smarty.session.nickname}</a></li>
                  <li class=""><a href="/Index?logout=1">登出</a></li>
                  {else}
                  <li class="list"><a href="/Signup">註冊</a></li>
                  <li class=""><a href="/Login">登入</a></li>
                {/if}
                <li class=""><svg id="skills" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 384.97 384.97" style="enable-background:new 0 0 384.97 384.97;" xml:space="preserve">
                        <rect id="backgroundrect" width="100%" height="100%" x="0" y="0" fill="none" stroke="none" />

                        <g class="currentLayer" style="">
                            <title>Layer 1</title>

                            <g id="svg_5" class="">

                            </g>

                            <g id="svg_6" class="">

                            </g>

                            <g id="svg_7" class="">

                            </g>

                            <g id="svg_8" class="">

                            </g>

                            <g id="svg_9" class="">

                            </g>

                            <g id="svg_10" class="">

                            </g>

                            <g id="svg_11">

                            </g>
                            <g id="svg_12">

                            </g>
                            <g id="svg_13">

                            </g>
                            <g id="svg_14">

                            </g>
                            <g id="svg_15">

                            </g>
                            <g id="svg_16">

                            </g>
                            <g id="svg_17">

                            </g>
                            <g id="svg_18">

                            </g>
                            <g id="svg_19">

                            </g>
                            <g id="svg_20">

                            </g>
                            <g id="svg_21">

                            </g>
                            <g id="svg_22">

                            </g>
                            <g id="svg_23">

                            </g>
                            <g id="svg_24">

                            </g>
                            <g id="svg_25">

                            </g>
                            <g class="selected">
                                <path
                                    d="M12.03,119.69693943548202 h360.909 c6.641,0 12.03,-5.39 12.03,-12.03 c0,-6.641 -5.39,-12.03 -12.03,-12.03 H12.03 c-6.641,0 -12.03,5.39 -12.03,12.03 C0,114.30693943548202 5.39,119.69693943548202 12.03,119.69693943548202 z"
                                    id="svg_2" class="" fill-opacity="1" stroke="#000000" stroke-opacity="1" />
                                <path
                                    d="M372.939,179.84893943548204 H12.03 c-6.641,0 -12.03,5.39 -12.03,12.03 s5.39,12.03 12.03,12.03 h360.909 c6.641,0 12.03,-5.39 12.03,-12.03 S379.58,179.84893943548204 372.939,179.84893943548204 z"
                                    id="svg_3" class="" fill-opacity="1" stroke="#000000" stroke-opacity="1" />
                                <path
                                    d="M367.51212920129797,264.060939435482 H18.36595369256031 c-9.636832629084587,0 -17.456873441934583,5.39 -17.456873441934583,12.03 c0,6.641 7.821491924524307,12.03 17.456873441934583,12.03 h349.14617550873754 c9.636832629084587,0 17.456873441934583,-5.39 17.456873441934583,-12.03 C384.97045375490677,269.449939435482 377.1489618303824,264.060939435482 367.51212920129797,264.060939435482 z"
                                    id="svg_4" class="" fill-opacity="1" stroke="#000000" stroke-opacity="1" />
                            </g>
                        </g>
                    </svg>
                </li>
                <ul>
        </div>
    </div>
</div>

{if count($arr_index_banner)}
<div class="top_banner" uk-slideshow="animation: push;autoplay: true;ratio: {$index_ratio}">
    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
        <ul class="uk-slideshow-items">
            {foreach $arr_index_banner as $v => $banner}
            <li>
                <a href="{$banner.href}"><img src="{$banner.img}"></a>
            </li>
            {/foreach}
        </ul>
        <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
        <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
    </div>
    <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
</div>
<!-- <div class="top_banner_2">
  <div>
    <table><tr><td colspan="5">
      <div class="fn_logo">
        <img src="/themes/Epro/img/index/fn_logo.png"><br>
        <span>專業可靠 · 安心便利</span>
      </div>
    </td></tr>
      <tr>
      <td><div><a href="\Service"><img src="/themes/Epro/img/index/icon-1.png"><br><span>裝修服務</span></a></div></td>
      <td><div><a href="\Master"><img src="/themes/Epro/img/index/icon-2.png"><br><span>達人專區</span></a></div></td>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-3.png"><br><span>最新訊息</span></a></div></td>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-4.png"><br><span>客服中心</span></a></div></td>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-5.png"><br><span>關於我們</span></a></div></td>
    </tr></table>
  </div>
</div> -->


<div class="row top_fn top_banner_2" style="height: 1087px; display: block; opacity: 1;position: fixed;">
  <div class="fff"></div>
  <ul class="logo">
    <li><a href="/"></a></li>
  </ul>
  <ul>
    <li><a href="/Service"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="tools" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-tools fa-w-16 fa-7x"><path fill="currentColor" d="M502.6 389.5L378.2 265c-15.6-15.6-36.1-23.4-56.6-23.4-15.4 0-30.8 4.4-44.1 13.3L192 169.4V96L64 0 0 64l96 128h73.4l85.5 85.5c-20.6 31.1-17.2 73.3 10.2 100.7l124.5 124.5c6.2 6.2 14.4 9.4 22.6 9.4 8.2 0 16.4-3.1 22.6-9.4l67.9-67.9c12.4-12.6 12.4-32.8-.1-45.3zM160 158.1v1.9h-48L42.3 67 67 42.3l93 69.7v46.1zM412.1 480L287.7 355.5c-9.1-9.1-14.1-21.1-14.1-33.9 0-12.8 5-24.9 14.1-33.9 9.1-9.1 21.1-14.1 33.9-14.1 12.8 0 24.9 5 33.9 14.1L480 412.1 412.1 480zM64 432c0 8.8 7.2 16 16 16s16-7.2 16-16-7.2-16-16-16-16 7.2-16 16zM276.8 66.9C299.5 44.2 329.4 32 360.6 32c6.9 0 13.8.6 20.7 1.8L312 103.2l13.8 83 83.1 13.8 69.3-69.3c6.7 38.2-5.3 76.8-33.1 104.5-8.9 8.9-19.1 16-30 21.5l23.6 23.6c10.4-6.2 20.2-13.6 29-22.5 37.8-37.8 52.7-91.4 39.7-143.3-2.3-9.5-9.7-17-19.1-19.6-9.5-2.6-19.7 0-26.7 7l-63.9 63.9-44.2-7.4-7.4-44.2L410 50.3c6.9-6.9 9.6-17.1 7-26.5-2.6-9.5-10.2-16.8-19.7-19.2C345.6-8.3 292 6.5 254.1 44.3c-12.9 12.9-22.9 27.9-30.1 44v67.8l22.1 22.1c-9.6-40.4 1.6-82.2 30.7-111.3zM107 467.1c-16.6 16.6-45.6 16.6-62.2 0-17.1-17.1-17.1-45.1 0-62.2l146.1-146.1-22.6-22.6L22.2 382.3c-29.6 29.6-29.6 77.8 0 107.5C36.5 504.1 55.6 512 75.9 512c20.3 0 39.4-7.9 53.7-22.3L231.4 388c-6.7-9.2-11.8-19.3-15.5-29.8L107 467.1z" class=""></path></svg>
      <span>裝修服務</span></a>
    </li>
    <li><a href="/Master"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="user-hard-hat" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-user-hard-hat fa-w-14 fa-7x"><path fill="currentColor" d="M313.6 352c-28.72 0-42.45 16-89.6 16s-60.88-16-89.56-16A134.4 134.4 0 0 0 0 486.4 25.6 25.6 0 0 0 25.6 512h396.8a25.6 25.6 0 0 0 25.6-25.6A134.4 134.4 0 0 0 313.6 352zM32.2 480a102.54 102.54 0 0 1 102.24-96c19.82 0 38.92 16 89.56 16 51 0 69.6-16 89.6-16a102.53 102.53 0 0 1 102.2 96zM88 160h12.66A124.32 124.32 0 0 0 96 192a128 128 0 0 0 256 0 124.32 124.32 0 0 0-4.66-32H360a8 8 0 0 0 8-8v-16a8 8 0 0 0-8-8h-8c0-51.52-35-94.46-82.33-107.52A31.89 31.89 0 0 0 240 0h-32a31.89 31.89 0 0 0-29.67 20.48C131 33.54 96 76.48 96 128h-8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8zM272 54.91A79.94 79.94 0 0 1 320 128h-48zM208 32h32v96h-32zm-80 96a79.94 79.94 0 0 1 48-73.09V128zm6 32h180a92 92 0 0 1 6 32 96 96 0 0 1-192 0 92 92 0 0 1 6-32z" class=""></path></svg>
      <span>達人專區</span></a>
    </li>
    <li><a href="####"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="volume-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-volume-up fa-w-18 fa-7x"><path fill="currentColor" d="M342.91 193.57c-7.81-3.8-17.5-.48-21.34 7.5-3.81 7.97-.44 17.53 7.53 21.34C343.22 229.2 352 242.06 352 256s-8.78 26.8-22.9 33.58c-7.97 3.81-11.34 13.38-7.53 21.34 3.86 8.05 13.54 11.29 21.34 7.5C368.25 306.28 384 282.36 384 256s-15.75-50.29-41.09-62.43zM231.81 64c-5.91 0-11.92 2.18-16.78 7.05L126.06 160H24c-13.26 0-24 10.74-24 24v144c0 13.25 10.74 24 24 24h102.06l88.97 88.95c4.87 4.87 10.88 7.05 16.78 7.05 12.33 0 24.19-9.52 24.19-24.02V88.02C256 73.51 244.13 64 231.81 64zM224 404.67L139.31 320H32V192h107.31L224 107.33v297.34zM421.51 1.83c-7.89-4.08-17.53-1.12-21.66 6.7-4.13 7.81-1.13 17.5 6.7 21.61 84.76 44.55 137.4 131.1 137.4 225.85s-52.64 181.3-137.4 225.85c-7.82 4.11-10.83 13.8-6.7 21.61 4.1 7.75 13.68 10.84 21.66 6.7C516.78 460.06 576 362.67 576 255.99c0-106.67-59.22-204.06-154.49-254.16zM480 255.99c0-66.12-34.02-126.62-88.81-157.87-7.69-4.38-17.59-1.78-22.04 5.89-4.45 7.66-1.77 17.44 5.96 21.86 44.77 25.55 72.61 75.4 72.61 130.12s-27.84 104.58-72.61 130.12c-7.72 4.42-10.4 14.2-5.96 21.86 4.3 7.38 14.06 10.44 22.04 5.89C445.98 382.62 480 322.12 480 255.99z" class=""></path></svg>
    <span>最新訊息</span></a>
  </li>
    <li><a href="####"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="user-headset" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-user-headset fa-w-14 fa-7x"><path fill="currentColor" d="M320 352h-4.7c-12.16 0-24 2.9-35.5 6.8a173.76 173.76 0 0 1-111.64 0c-11.48-3.9-23.29-6.78-35.42-6.78H128A128 128 0 0 0 0 480a32 32 0 0 0 32 32h384a32 32 0 0 0 32-32 128 128 0 0 0-128-128zM32 480a96.1 96.1 0 0 1 96-96h4.74c6.92 0 14.92 1.62 25.16 5.09a205.75 205.75 0 0 0 132.16 0c10.31-3.49 18.33-5.11 25.24-5.11h4.7a96.1 96.1 0 0 1 96 96zm16-256a16 16 0 0 0 16-16v-16c0-88.22 71.78-160 160-160s160 71.78 160 160v16a80.09 80.09 0 0 1-80 80h-32a32 32 0 0 0-32-32h-32a32 32 0 0 0 0 64h96a112.14 112.14 0 0 0 112-112v-16C416 86.12 329.88 0 224 0S32 86.12 32 192v16a16 16 0 0 0 16 16zM224 96a95.57 95.57 0 0 1 71.23 159.76c0 .09.13.15.18.24H304a47.89 47.89 0 0 0 40.55-22.58C349 220.36 352 206.58 352 192a128 128 0 0 0-256 0c0 40.42 19.1 76 48.35 99.47-.06-1.17-.35-2.28-.35-3.47a63.25 63.25 0 0 1 8.93-32A95.58 95.58 0 0 1 224 96z" class=""></path></svg>
    <span>客服中心</span></a>
  </li>
    <li><a href="####"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="comment-alt-dots" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt-dots fa-w-16 fa-7x"><path fill="currentColor" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 7.1 5.8 12 12 12 2.4 0 4.9-.7 7.1-2.4L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64zm32 352c0 17.6-14.4 32-32 32H293.3l-8.5 6.4L192 460v-76H64c-17.6 0-32-14.4-32-32V64c0-17.6 14.4-32 32-32h384c17.6 0 32 14.4 32 32v288zM128 184c-13.3 0-24 10.7-24 24s10.7 24 24 24 24-10.7 24-24-10.7-24-24-24zm128 0c-13.3 0-24 10.7-24 24s10.7 24 24 24 24-10.7 24-24-10.7-24-24-24zm128 0c-13.3 0-24 10.7-24 24s10.7 24 24 24 24-10.7 24-24-10.7-24-24-24z" class=""></path></svg>
    <span>關於我們</span></a>
  </li>
  </ul>
</div>

{/if} {*

<div class="top_banner">*} {* <img src="/themes/Epro/img/index/index_01.png" />*} {* 晚一點改輪播*} {*
</div>*} {include file="../../main_menu.tpl"} {*main_menu使用*}

<!-- <div class="search_bar">
    <div class="row">
        <div class="cell-3 search">
            <div></div>
        </div>
        <div class="cell-6 search">
            <div class="search_bar_m">
              <div class="selects">
              <select class="city" onchange="cell_window_append_cityurl(this.value)">
                <option disabled selected>桃園市</option>
              </select>
              <select class="itembox" onchange="cell_window_appendurl(this.value)">
                <option disabled selected>請選擇</option>
                <option value="Service">PRO專業技術型</option>
                <option value="Vip">VIP量身訂製型</option>
              </select>
              </div>
                <input type="text" class="search_txtt" placeholder="可輸入，如：冷氣不冷 | 馬桶不通 | 壁癌粉刷 ... ">
                <button value="search" class="search_bnt"><i class="fas fa-angle-down"></i></button>
            </div>
        </div>
        <div class="cell-3 search">
            <div></div>
        </div>
    </div>
</div> -->

<div class="search_windows">
    {foreach $windows_data as $key => $value} {if $key%10==0}
    <div class="row" {if ($key/10)%2==1}style="margin-left: 4%;" {/if}>
        {/if}
        <div class="cell_window lazyload" style="background: url({$value.img});">
            <span><a href="?sid={$value.id_epro_windows}" alt="{$value.keyword}">{$value.title}</a></span>
        </div>
        {if $key%10==9 || $windows_count==$key}
    </div>
    {/if} {/foreach}
</div>

{*
<div class="search_windows">*} {*
    <div class="row">*} {*
        <div class="cell_window" style="background: url(&quot;img/5_04.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="裝潢設計:室內裝修" style="-webkit-writing-mode: vertical-lr;">裝潢設計</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_114.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="衛浴:浴室廁所" style="-webkit-writing-mode: vertical-lr;">衛浴</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_06.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="廚具:流理台" style="-webkit-writing-mode: vertical-lr;">廚具</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_08.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="水電:瓦斯" style="-webkit-writing-mode: vertical-lr;">水電</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_10.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="冷氣空調:大金日立" style="-webkit-writing-mode: vertical-lr;">冷氣空調</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_80.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="電器:電鍋熱水瓶" style="-webkit-writing-mode: vertical-lr;">電器</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_82.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="熱水器:瓦斯天然瓦斯" style="-webkit-writing-mode: vertical-lr;">熱水器</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_84.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="油漆粉刷:立幫立邦乳膠漆" style="-webkit-writing-mode: vertical-lr;">油漆粉刷</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_86.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="壁紙" style="-webkit-writing-mode: vertical-lr;">壁紙</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_64.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="管線" style="-webkit-writing-mode: vertical-lr;">管線</a></span></div>*} {* </div>*} {*
    <div class="row" style="margin-left: 64px;">*} {*
        <div class="cell_window" style="background: url(&quot;img/5_66.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="通管" style="-webkit-writing-mode: vertical-lr;">通管</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_68.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="燈飾照明" style="-webkit-writing-mode: vertical-lr;">燈飾照明</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_19.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="木作" style="-webkit-writing-mode: vertical-lr;">木作</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_21.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="泥作" style="-webkit-writing-mode: vertical-lr;">泥作</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_23.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="鐵作" style="-webkit-writing-mode: vertical-lr;">鐵作</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_25.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="地板" style="-webkit-writing-mode: vertical-lr;">地板</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_94.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="石材及美化" style="-webkit-writing-mode: vertical-lr;">石材及美化</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_96.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="玻璃" style="-webkit-writing-mode: vertical-lr;">玻璃</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_98.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="磁磚" style="-webkit-writing-mode: vertical-lr;">磁磚</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_100.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="防水抓漏" style="-webkit-writing-mode: vertical-lr;">防水抓漏</a></span></div>*} {* </div>
    *} {*
    <div class="row">*} {*
        <div class="cell_window" style="background: url(&quot;img/5_102.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="門窗" style="-webkit-writing-mode: vertical-lr;">門窗</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_70.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="門鎖" style="-webkit-writing-mode: vertical-lr;">門鎖</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_72.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="智能鎖" style="-webkit-writing-mode: vertical-lr;">智能鎖</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_78.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="安控監視" style="-webkit-writing-mode: vertical-lr;">安控監視</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_33.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="消防保全" style="-webkit-writing-mode: vertical-lr;">消防保全</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_35.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="環境清潔" style="-webkit-writing-mode: vertical-lr;">環境清潔</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_37.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="沙發.床墊.窗簾.地毯除螨清洗"
                    style="-webkit-writing-mode: vertical-lr;">沙發.床墊.窗簾.地毯除螨清洗</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_117.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="消毒滅菌" style="-webkit-writing-mode: vertical-lr;">消毒滅菌</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_119.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="除蟲除味" style="-webkit-writing-mode: vertical-lr;">除蟲除味</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_41.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="窗簾地毯" style="-webkit-writing-mode: vertical-lr;">窗簾地毯</a></span></div>*} {* </div>
    *} {*
    <div class="row" style="margin-left: 64px;">*} {*
        <div class="cell_window" style="background: url(&quot;img/5_48.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="淨水設備" style="-webkit-writing-mode: vertical-lr;">淨水設備</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_50.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="升降設備" style="-webkit-writing-mode: vertical-lr;">升降設備</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_52.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="機電" style="-webkit-writing-mode: vertical-lr;">機電</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_54.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="弱電:網路" style="-webkit-writing-mode: vertical-lr;">弱電</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_56.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="節能" style="-webkit-writing-mode: vertical-lr;">節能</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_127.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="綠建材" style="-webkit-writing-mode: vertical-lr;">綠建材</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_129.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="智慧居家 " style="-webkit-writing-mode: vertical-lr;">智慧居家 </a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_131.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="房屋健檢" style="-webkit-writing-mode: vertical-lr;">房屋健檢</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_133.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="驗屋檢測" style="-webkit-writing-mode: vertical-lr;">驗屋檢測</a></span></div>*} {*
        <div class="cell_window" style="background: url(&quot;img/5_135.png&quot;); height: 129px; width: 129px;">
            <span><a href="####" alt="無障礙空間" style="-webkit-writing-mode: vertical-lr;">無障礙空間</a></span></div>*} {*
    </div>
    <div class="row">*} {* </div>*} {*
</div>*}

<div class="epro_slogan">
    <h2><span>體驗便利的裝修服務</span></h2>
</div>

<div class="epro_introduction">
    <div class="row">
        <div class="cell-5 c1">
            <div class="rows r1">
                <img src="/themes/Epro/img/index/index_04.png">
            </div>
            <div class="rows r2">
                <h5><span class="master_txt">依據您的需求推薦適任達人或由您指定的達人</span></h5><br><span class="sec_txt">提供電話諮詢|到府檢測|報價</span>
            </div>
            <div class="rows r3">
                <img src="/themes/Epro/img/index/index_10.png">
            </div>
        </div>
        <div class="cell-2 mid c2">
            <div></div>
        </div>
        <div class="cell-5 c3">
            <div class="rows l1">
                <div>
                    <h5><span class="master_txt">搜尋您需要的服務</span></h5><br><span class="sec_txt">例:冷氣不冷,即點選【冷氣空調】,建立服務需求單。</span>
                </div>
            </div>
            <div class="rows l2">
                <img src="/themes/Epro/img/index/index_13.png">
            </div>
            <div class="rows l3">
                <h5><span class="master_txt">建立派工預約單</span></h5><br><span class="sec_txt">裝修達人於預約時間前往施工,完工後進行本服務達人評價,<br>即可獲得紅利點數。</span>
            </div>
        </div>
    </div>
</div>
<div class="epro_slogan_temp">

</div>

<div class="epro_slogan">
    <h2><span>專業貼心，是您可靠的裝修專家</span></h2>
</div>

<div class="epro_family">
    <div class="cell-12"><img src="/themes/Epro/img/index/index_17.png" style="opacity: 0;"></div>
</div>

{if $talent_data}
<div class="epro_know_how">
    <div class="row">
        <div class="cell-12">
            <h3>{$epro_cms_main[0].title}</h3>
        </div>
    </div>
    {foreach $talent_data as $key => $value} {if $key%2==0}
    <div class="row">
        {/if}
        <div class="cell-6">
            <div class="body">
                <div class="row img" style="background-size: cover;background:{if $value.img}url({$value.img});{else}url(https://epro.house/img/default.png);{/if}">
                    <div class="dL_name">{$value.appeal}</div>
                </div>
                <div class="row txt">
                    <div class="ctxt_1">
                        <h5><span>{$value.title}</span></h5>
                    </div>
                    <div class="ctxt_2">
                        <h4><span>{$value.page_title}</span>
                        </h4>
                    </div>
                    <div class="ctxt_3"><span>{$value.description}</span></div>
                </div>
            </div>
        </div>
        {if $key%2==1}
    </div>
    {/if} {/foreach}
</div>
{/if} {if $member_data}
<div class="epro_share">
    <div class="row">
        <div class="cell-12">
            <h3>{$epro_cms_main[1].title}</h3>
        </div>
    </div>
    {foreach $member_data as $key => $value} {if $key%2==0}
    <div class="row">
        {/if}
        <div class="cell-6">
            <div class="row">
                <div class="cell-5">
                    <div class="body">
                        <div class="row img" style="background-size: cover;background:{if $value.img}url({$value.img});{else}url(https://epro.house/img/default.png);{/if}">
                            <div class="dL_name">{$value.appeal}</div>
                        </div>
                    </div>
                </div>
                <div class="cell-6">
                    <div class="ctxt_1">
                        <h5><span>{$value.title}</span></h5>
                    </div>
                    <div class="ctxt_2">
                        <h4><span>{$value.page_title}</span></h4>
                    </div>
                    <div class="ctxt_3"><span>{$value.description}</span></div>
                </div>
            </div>
        </div>
        {if $key%2==1}
    </div>
    {/if} {/foreach}
</div>
{/if} {if $new_data}
<div class="epro_news">
    <div class="row">

        <div class="cell-2 offset-3"></div>
        <div class="cell-2">
            <h5>{$epro_cms_main[2].title}</h5>
        </div>
        <div class="cell-2 offset-3">查看更多消息 &gt; </div>

    </div>
</div>
<div class="epro_news_bar">
    <div class="row">
        {foreach $new_data as $key => $value}
        <div class="cell-3">
            <div class="body">
                <div class="row r1">
                    <div class="cell-12">
                        <h5><span>{$value.title}</span></h5>
                    </div>
                </div>
                <div class="row r2">
                    <div class="cell-12">
                        <div class="row img" style="background-size: cover;background:{if $value.img}url({$value.img});{else}url(https://epro.house/img/default.png);{/if}">
                        </div>
                    </div>
                </div>
                <div class="row r3">
                    <div class="cell-12">
                        <div class="row r31">
                            <div class="cell-12"><span>{$value.description}</span></div>
                        </div>
                        <div class="row r32">
                            <div class="cell-12"><span>{$value.appeal}</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/foreach}
    </div>
</div>
{/if}

{$js}
