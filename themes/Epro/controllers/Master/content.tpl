

<div class="bottom_fix_div">
</div>

{include file="$tpl_dir./search_bar.tpl"}
{$cube=array("達人履歷","裝修任務","接案攻略","職業工會","保險方案")}
{$cubeSVG=array("<svg aria-hidden='true' focusable='false' data-prefix='fal' data-icon='clipboard-list-check' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 384 512' class='svg-inline--fa fa-clipboard-list-check fa-w-12 fa-7x'><path fill='currentColor' d='M336 64h-88.6c.4-2.6.6-5.3.6-8 0-30.9-25.1-56-56-56s-56 25.1-56 56c0 2.7.2 5.4.6 8H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM192 32c13.3 0 24 10.7 24 24s-10.7 24-24 24-24-10.7-24-24 10.7-24 24-24zm160 432c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V112c0-8.8 7.2-16 16-16h48v20c0 6.6 5.4 12 12 12h168c6.6 0 12-5.4 12-12V96h48c8.8 0 16 7.2 16 16v352zM112 328c-13.3 0-24 10.7-24 24s10.7 24 24 24 24-10.7 24-24-10.7-24-24-24zm168 8H168c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h112c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8zm-153.8-65.6l64.2-63.6c2.1-2.1 2.1-5.5 0-7.6l-12.6-12.7c-2.1-2.1-5.5-2.1-7.6 0l-47.6 47.2-20.6-20.9c-2.1-2.1-5.5-2.1-7.6 0l-12.7 12.6c-2.1 2.1-2.1 5.5 0 7.6l37.1 37.4c1.9 2.1 5.3 2.1 7.4 0zM280 240h-77.6l-32.3 32H280c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8z' class=''></path></svg>",
"<svg aria-hidden='true' focusable='false' data-prefix='fal' data-icon='bullseye-arrow' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 496 512' class='svg-inline--fa fa-bullseye-arrow fa-w-16 fa-7x'><path fill='currentColor' d='M305.05 98.74l23.96 53.62-92.33 92.33c-6.25 6.25-6.25 16.38 0 22.62 3.12 3.12 7.22 4.69 11.31 4.69s8.19-1.56 11.31-4.69l92.33-92.33 53.62 23.96a20.547 20.547 0 0 0 21.04-4.96l63.67-63.67c10.8-10.8 6.46-29.2-8.04-34.04l-55.66-18.55-18.55-55.65C404.73 13.08 396.54 8 388.16 8c-5.14 0-10.36 1.92-14.47 6.03L310.02 77.7a20.582 20.582 0 0 0-4.97 21.04zm78.18-48.99l12.7 38.09 5.06 15.18 15.18 5.06 38.09 12.7-44.93 44.93-49.09-21.93-21.93-49.09 44.92-44.94zm101.2 131.35l-26.18 26.18c3.63 15.69 5.75 31.95 5.75 48.72 0 119.1-96.9 216-216 216S32 375.1 32 256 128.9 40 248 40c16.78 0 33.04 2.11 48.72 5.75l26.18-26.18A247.848 247.848 0 0 0 248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248c0-26.11-4.09-51.26-11.57-74.9zM274.7 108.85a53.056 53.056 0 0 1-2.68-14.44C264.12 93.25 256.23 92 248 92c-90.65 0-164 73.36-164 164 0 90.65 73.36 164 164 164 90.65 0 164-73.36 164-164 0-8.35-1.25-16.35-2.45-24.36-4.89-.21-9.76-.79-14.41-2.34-2.12-.71 1.02.62-20.69-9.07C377.7 231.68 380 243.52 380 256c0 72.79-59.21 132-132 132s-132-59.21-132-132 59.21-132 132-132c12.48 0 24.32 2.3 35.77 5.55-9.69-21.7-8.36-18.56-9.07-20.7zM248 176c-44.11 0-80 35.89-80 80s35.89 80 80 80 80-35.89 80-80c0-3.77-.61-7.38-1.11-11.01l-44.95 44.95-.01-.01c-8.7 8.69-20.7 14.07-33.93 14.07-26.47 0-48-21.53-48-48 0-13.23 5.38-25.23 14.07-33.93l-.01-.01 44.95-44.95c-3.63-.5-7.24-1.11-11.01-1.11z' class=''></path></svg>",
"<svg aria-hidden='true' focusable='false' data-prefix='fal' data-icon='lightbulb-on' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 640 512' class='svg-inline--fa fa-lightbulb-on fa-w-20 fa-7x'><path fill='currentColor' d='M320,64A112.14,112.14,0,0,0,208,176a16,16,0,0,0,32,0,80.09,80.09,0,0,1,80-80,16,16,0,0,0,0-32Zm0-64C217.06,0,143.88,83.55,144,176.23a175,175,0,0,0,43.56,115.55C213.22,321,237.84,368.69,240,384l.06,75.19a15.88,15.88,0,0,0,2.69,8.83l24.5,36.84A16,16,0,0,0,280.56,512h78.85a16,16,0,0,0,13.34-7.14L397.25,468a16.17,16.17,0,0,0,2.69-8.83L400,384c2.25-15.72,27-63.19,52.44-92.22A175.9,175.9,0,0,0,320,0Zm47.94,454.31L350.84,480H289.12l-17.06-25.69,0-6.31h95.91ZM368,416H272l-.06-32H368Zm60.41-145.31c-14,15.95-36.32,48.09-50.57,81.29H262.22c-14.28-33.21-36.6-65.34-50.6-81.29A143.47,143.47,0,0,1,176.06,176C175.88,99,236.44,32,320,32c79.41,0,144,64.59,144,144A143.69,143.69,0,0,1,428.38,270.69ZM96,176a16,16,0,0,0-16-16H16a16,16,0,0,0,0,32H80A16,16,0,0,0,96,176ZM528,64a16.17,16.17,0,0,0,7.16-1.69l64-32A16,16,0,0,0,584.84,1.69l-64,32A16,16,0,0,0,528,64Zm96,96H560a16,16,0,0,0,0,32h64a16,16,0,0,0,0-32ZM119.16,33.69l-64-32A16,16,0,0,0,40.84,30.31l64,32A16.17,16.17,0,0,0,112,64a16,16,0,0,0,7.16-30.31Zm480,288-64-32a16,16,0,0,0-14.32,28.63l64,32a16,16,0,0,0,14.32-28.63ZM112,288a16.17,16.17,0,0,0-7.16,1.69l-64,32a16,16,0,0,0,14.32,28.63l64-32A16,16,0,0,0,112,288Z' class=''></path></svg>",
"<svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='user-hard-hat' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512' class='svg-inline--fa fa-user-hard-hat fa-w-14 fa-7x'><path fill='currentColor' d='M224 272a80.13 80.13 0 0 1-78.38-64h-48c8 63.06 61.17 112 126.39 112s118.44-48.94 126.39-112h-48a80.13 80.13 0 0 1-78.4 64zm89.6 80c-28.72 0-42.45 16-89.6 16s-60.88-16-89.56-16A134.4 134.4 0 0 0 0 486.4 25.6 25.6 0 0 0 25.6 512h396.8a25.6 25.6 0 0 0 25.6-25.6A134.4 134.4 0 0 0 313.6 352zM50.94 464a86.58 86.58 0 0 1 83.5-64c14.44 0 38.28 16 89.56 16 51.47 0 75.1-16 89.6-16a86.55 86.55 0 0 1 83.46 64zM88 176h272a8 8 0 0 0 8-8v-32a8 8 0 0 0-8-8h-8c0-46.41-28.53-85.54-68.79-102.42L256 80V16a16 16 0 0 0-16-16h-32a16 16 0 0 0-16 16v64l-27.21-54.42C124.53 42.46 96 81.59 96 128h-8a8 8 0 0 0-8 8v32a8 8 0 0 0 8 8z' class=''></path></svg>",
"<svg aria-hidden='true' focusable='false' data-prefix='fal' data-icon='umbrella-beach' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 640 512' class='svg-inline--fa fa-umbrella-beach fa-w-20 fa-7x'><path fill='currentColor' d='M443.48 18.08C409.77 5.81 375.31 0 341.41 0c-90.47 0-176.84 41.45-233.44 112.33-6.7 8.39-2.67 21.04 7.42 24.71l236.15 85.95L257.99 480H8c-4.42 0-8 3.58-8 8v16c0 4.42 3.58 8 8 8h560c4.42 0 8-3.58 8-8v-16c0-4.42-3.58-8-8-8H292.03l89.56-246.07 236.75 86.17c1.83.67 3.7.98 5.53.98 8.27 0 15.82-6.35 16.04-15.14 3.03-124.66-72.77-242.85-196.43-287.86zm-295.31 96.84C198.11 62.64 268.77 32 341.42 32c7.81 0 15.6.35 23.36 1.04-36.87 23.16-73.76 66.62-103.06 123.21l-113.55-41.33zm315.21 114.73l-171.12-62.28C332.69 90.93 384.89 46.1 420.4 46.09c4.35 0 8.32.68 12.13 2.06 19.56 7.12 33.97 35.16 38.56 75 3.66 31.83.53 68.45-7.71 106.5zm30.8 11.21c13.83-61.57 13.67-118.28.7-159.64 65.33 46.08 107.58 119.45 112.61 200.89l-113.31-41.25z' class=''></path></svg>")}

<div class="container-fluid">
        <header style="max-width: 960px;">

        </header>


        <div id="tm-bg"></div>

        <div id="tm-wrap-master">
          <div class="container tm-site-header-container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 col-md-col-xl-6 mb-md-0 mb-sm-4 mb-4 tm-site-header-col">
                    <div class="tm-site-header">
                      <div class="txt">
                        <h1 class="mb-4 main-title">{$page_title}</h1>
                        <p class="second-title">想拓展客源嗎?快上裝修達人<br>-不用出門也有生意上門</p>
                      </div>
                        <img src="/themes/Epro/img/master/underline.png" class="img-fluid mb-4">

                    </div>
                </div>

            </div>
        </div>
            <div class="tm-main-content">

                <!-- 次頁主內容 -->
                <article>

                  <div class="Cubes">
                    <div class="noCube"></div>
                    {$i=2}
                    {if $smarty.session.nickname}
                    {$i=0}
                    <div class="Cube">{$cubeSVG[$i]}{$cube[$i]}{$i=$i+1}</div>
                    <div class="Cube">{$cubeSVG[$i]}{$cube[$i]}{$i=$i+1}</div>
                    {/if}
                    <div class="Cube">{$cubeSVG[$i]}{$cube[$i]}{$i=$i+1}</div>
                    <div class="Cube">{$cubeSVG[$i]}{$cube[$i]}{$i=$i+1}</div>
                    <div class="Cube">{$cubeSVG[$i]}{$cube[$i]}{$i=$i+1}</div>
                  </div>
                  {$mune_1=array("加入達人","服務履歷","評價紀錄","大集大利")}
                  {$mune_2=array("任務清單","報價管理","接案紀錄","訊息留言")}
                  {$mune_3=array("接案技巧","廣宣行銷","職訓進修","專業認證")}
                  {$mune_4=array("勞健保","布告欄","同業交流","互助會","標案資訊")}
                  {$mune_5=array("業務責任險","汽|機車險")}
                  <div class="Cubes">
                    <div class="noCube"></div>
                    {if $smarty.session.nickname}
                    <div class="mune"><ul class="list">{for $i=0;$i<sizeof($mune_1);$i++}<li class="p"><a href="/Profile">{$mune_1[$i]}</a></li>{/for}</ul></div>
                    <div class="mune"><ul class="list">{for $i=0;$i<sizeof($mune_2);$i++}<li class="p">{$mune_2[$i]}</li>{/for}</ul></div>
                    {/if}
                    <div class="mune"><ul class="list">{for $i=0;$i<sizeof($mune_3);$i++}<li class="p">{$mune_3[$i]}</li>{/for}</ul></div>
                    <div class="mune"><ul class="list">{for $i=0;$i<sizeof($mune_4);$i++}<li class="p">{$mune_4[$i]}</li>{/for}</ul></div>
                    <div class="mune"><ul class="list">{for $i=0;$i<sizeof($mune_5);$i++}<li class="p">{$mune_5[$i]}</li>{/for}</ul></div>
                  </div>

                </article>

            </div>
            <!-- .tm-main-content -->

                </div>


        </div>
    </div>

    <div class="container-fluid main" id="main">

        <div class="breadcrumbs">
            <p>{$breadcrumbs_html}{if $sidname} &gt; {$sidname}{/if}</p><br>
        </div>




        <div class="row">

            <div class="col-xs-12 col-sm-12">


                <div>

                  {for $i=0;$i<4;$i++}
                    <div class="desk">

                        <img src="/themes/Epro/img/master/banner_{$i+1}.jpg">

                    </div>
                    {/for}



                    <!-- === END CONTENT === -->
                </div>
            </div>
        </div>
    </div>
