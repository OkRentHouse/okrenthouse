<div class="col-sm-12 tab_1 tabs">
  <ul>
    <li class="title">上傳工作室圖片:</li>
    <li class="content inline"><div class="upload" onclick="$('#frame').click()" data-name="work_photo" data-value="0" class="get_file"><div class="frame"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>
      <img class="view" id="frame_0" src="" style="display: none">
    </div>
      <input type="file" style="display:none" id="frame" name="work_photo" onchange="get_file_img('frame')" required="required">
    <!-- <input type="file" id="file_2" name="account_img[]" onchange="get_file_img('file_2')" required="required" style="display:none"> -->

    <ul>
        <li class="title">上傳大頭照:</li>
        <li class="content"><div class="upload" onclick="$('#frame_1').click()" data-name="my_photo" data-value="0" class="get_file"><div class="frame_1"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>
          <img class="view" id="frame_1_0" src="" style="display: none">
            </div>
          <input type="file" style="display:none" id="frame_1" name="my_photo" onchange="get_file_img('frame_1')" required="required">
        </li>
      </ul>
  </li>
    <!-- <li class="title">姓名:</li>
    <li class="content name"><input type="text" name="name" value="" maxlength="20">
      <ul>
        <li class="title">暱稱:</li>
        <li class="content"><input type="text" name="nickname" value="" maxlength="20"></li>
      </ul>
    </li> -->
    <li class="title">LINE / Facebook:</li>
    <li class="content">
      <ul>
        <li class="share_item">
        <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-facebook fa-w-16 fa-2x"><path fill="currentColor" d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z" class="">
        </path></svg><input type="text" name="fb_share" maxlength="128" value="{$data_base.fb_share}" placeholder="請輸入網址">
        </li><li class="share_item">
        <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="line" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-line fa-w-14 fa-2x"><path fill="currentColor" d="M272.1 204.2v71.1c0 1.8-1.4 3.2-3.2 3.2h-11.4c-1.1 0-2.1-.6-2.6-1.3l-32.6-44v42.2c0 1.8-1.4 3.2-3.2 3.2h-11.4c-1.8 0-3.2-1.4-3.2-3.2v-71.1c0-1.8 1.4-3.2 3.2-3.2H219c1 0 2.1.5 2.6 1.4l32.6 44v-42.2c0-1.8 1.4-3.2 3.2-3.2h11.4c1.8-.1 3.3 1.4 3.3 3.1zm-82-3.2h-11.4c-1.8 0-3.2 1.4-3.2 3.2v71.1c0 1.8 1.4 3.2 3.2 3.2h11.4c1.8 0 3.2-1.4 3.2-3.2v-71.1c0-1.7-1.4-3.2-3.2-3.2zm-27.5 59.6h-31.1v-56.4c0-1.8-1.4-3.2-3.2-3.2h-11.4c-1.8 0-3.2 1.4-3.2 3.2v71.1c0 .9.3 1.6.9 2.2.6.5 1.3.9 2.2.9h45.7c1.8 0 3.2-1.4 3.2-3.2v-11.4c0-1.7-1.4-3.2-3.1-3.2zM332.1 201h-45.7c-1.7 0-3.2 1.4-3.2 3.2v71.1c0 1.7 1.4 3.2 3.2 3.2h45.7c1.8 0 3.2-1.4 3.2-3.2v-11.4c0-1.8-1.4-3.2-3.2-3.2H301v-12h31.1c1.8 0 3.2-1.4 3.2-3.2V234c0-1.8-1.4-3.2-3.2-3.2H301v-12h31.1c1.8 0 3.2-1.4 3.2-3.2v-11.4c-.1-1.7-1.5-3.2-3.2-3.2zM448 113.7V399c-.1 44.8-36.8 81.1-81.7 81H81c-44.8-.1-81.1-36.9-81-81.7V113c.1-44.8 36.9-81.1 81.7-81H367c44.8.1 81.1 36.8 81 81.7zm-61.6 122.6c0-73-73.2-132.4-163.1-132.4-89.9 0-163.1 59.4-163.1 132.4 0 65.4 58 120.2 136.4 130.6 19.1 4.1 16.9 11.1 12.6 36.8-.7 4.1-3.3 16.1 14.1 8.8 17.4-7.3 93.9-55.3 128.2-94.7 23.6-26 34.9-52.3 34.9-81.5z" class=""></path></svg>
        <input type="text" name="line_share" value="{$data_base.line_share}" maxlength="128"  placeholder="請輸入網址">
        </li>
      </ul>
    </li>
    <li class="title">選擇項目:</li>
    <li class="content flex">
        {if !empty($data_base.epro_server_item)}
            {foreach $data_base.epro_server_item as $d_b_i_k => $d_b_i_v}
            <ul class="skill">
                <input type="hidden" name="id_epro_server_item[]" value="{$d_b_i_v.id_epro_server_item}">
                <select name='id_epro_windows[]' onchange="get_id_epro_windows_item(this);">
                    {foreach $epro_windows as  $e_w_k => $e_w_v}
                        <option value="{$e_w_v.id_epro_windows}" {if $d_b_i_v.id_epro_windows==$e_w_v.id_epro_windows}selected="selected"{/if}>{$e_w_v.title}</option>
                    {/foreach}
                </select>
                <select name='id_epro_windows_item[]'>
                    {foreach $epro_windows_item as $e_w_i_k => $e_w_i_v}
                        <option value="{$e_w_i_v.id_epro_windows_item}" {if $d_b_i_v.id_epro_windows_item==$e_w_i_v.id_epro_windows_item}selected="selected"{/if}  {if $e_w_i_v.id_epro_windows != $d_b_i_v.id_epro_windows}style="display:none;"{/if}>{$e_w_i_v.title}</option>
                    {/foreach}
                </select>
                <div class="skill_input flex">
                    <input type="number" name="price[]" value="{$d_b_i_v.price}" placeholder="價格">
                    <input type="text" name="unit[]" value="{$d_b_i_v.unit}" placeholder="單位" maxlength="32">
                    {if $d_b_i_k==0}
                        <svg onclick="server_item_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
                    {else}
                        <svg onclick="server_item_delete(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="minus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-minus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 242c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12H108c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h232z" class=""></path></svg>
                    {/if}
                </div>
            </ul>
          {/foreach}

        {else}
            <ul class="skill">
                <input type="hidden" name="id_epro_server_item[]">
                <select name='id_epro_windows[]' onchange="get_id_epro_windows_item(this);">
                    {foreach $epro_windows as  $e_w_k => $e_w_v}
                        <option value="{$e_w_v.id_epro_windows}">{$e_w_v.title}</option>
                    {/foreach}
                </select>
                <select name='id_epro_windows_item[]'>
                    {foreach $epro_windows_item as $e_w_i_k => $e_w_i_v}
                        <option value="{$e_w_i_v.id_epro_windows_item}" {if $e_w_i_v.id_epro_windows !='1'}style="display:none;"{/if}>{$e_w_i_v.title}</option>
                    {/foreach}
                </select>
                <div class="skill_input flex">
                    <input type="number" name="price[]" placeholder="價格">
                    <input type="text" name="unit[]" placeholder="單位" maxlength="32">
                    <svg onclick="server_item_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
                </div>
            </ul>
        {/if}
    </li>
    <li class="title">服務區域:</li>
    <li class="content zone">
      <ul  class="selectedzone">
          {if !empty($data_base.epro_server_address)}
              {foreach $data_base.epro_server_address as $d_b_a_k => $d_b_a_v}
                  <li class="add">
                      <input type="hidden" name="id_epro_server_address[]" value="{$d_b_a_v.id_epro_server_address}">
                      <select class="county" name="id_county[]" onchange="get_city(this);">
                          {foreach $county as $c_k => $c_v}
                              <option value="{$c_v.id_county}" data-name="{$c_v.county_name}" {if $d_b_a_v.id_county==$c_v.id_county}selected="selected"{/if}>{$c_v.county_name}</option>
                          {/foreach}
                      </select>
                      <select  name="id_city[]" class="city">
                          {foreach $city as $c_k => $c_v}
                              <option value="{$c_v.id_city}" data-name="{$c_v.city_name}"  {if $d_b_a_v.id_city==$c_v.id_city}selected="selected"{/if}  {if $d_b_a_v.id_county !=$c_v.id_county}style="display: none;" {/if} >{$c_v.city_name}</option>
                          {/foreach}
                      </select>
                      {if $d_b_a_k==0}
                          <svg onclick="server_address_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
                      {else}
                          <svg onclick="server_address_delete(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="minus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-minus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 242c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12H108c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h232z" class=""></path></svg>
                      {/if}
                  </li>
              {/foreach}
          {else}
              <li class="add">
                  <input type="hidden" name="id_epro_server_address[]">
                  <select class="county" name="id_county[]" onchange="get_city(this);">
                      {foreach $preset_county as $p_c_k => $p_c_v}
                          <option value="{$p_c_v.id_county}" data-name="{$p_c_v.county_name}" {if $p_c_v.checked=='checked'}selected="selected"{/if}>{$p_c_v.county_name}</option>
                      {/foreach}
                  </select>
                  <select  name="id_city[]" class="city">
                      {foreach $preset_city as $p_c_k => $p_c_v}
                          <option value="{$p_c_v.id_city}" data-name="{$p_c_v.city_name}">{$p_c_v.city_name}</option>
                      {/foreach}
                  </select>
                  <svg onclick="server_address_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
              </li>
          {/if}
      </ul>
  </li>
</ul>
</div>
