<div class="col-sm-12 tab_2 tabs">
  <ul>
    <li class="title">服務特色:</li>
    <li class="content">
      <textarea name="server_features" style="width:100%;height:10em">{$data_base.server_features}</textarea>
      </li>
    <li class="title">服務費用:</li>
    <li class="content flex service_pay">巡檢車馬費<input type="number"  name="honorarium" value="{$data_base.honorarium}">/次</li>
      <ul class="detail">
        <li class="time_date">服務時間：
          <select name="server_cycle">
            <option value="">請選擇服務時段</option>
            <option value="0" {if $data_base.server_cycle==0}selected="selected"{/if}>每月</option>
            <option value="1" {if $data_base.server_cycle==1}selected="selected"{/if}>每周</option>
            <option value="2" {if $data_base.server_cycle==2}selected="selected"{/if}>每日</option>
            <option value="3" {if $data_base.server_cycle==3}selected="selected"{/if}>假日</option>
          </select>
          <input type="time" name="server_time_s" value="{$data_base.server_time_s}">
          <input type="date" name="server_date_s" value="{$data_base.server_date_s}"> ~ <input type="time" name="server_time_e" value="{$data_base.server_time_e}">
          <input type="date" name="server_date_e" value="{$data_base.server_date_e}"></li>
        <li class="contact">聯絡資訊：
          <input type="text" name="phone_1" placeholder="請輸入手機1" value="{$data_base.phone_1}" maxlength="20">
          <input type="text" name="phone_2" placeholder="請輸入手機2" value="{$data_base.phone_2}" maxlength="20">
          <input type="text" name="line_id" placeholder="請輸入Line ID" value="{$data_base.line_id}" maxlength="64">
          <input type="text" name="local_calls" placeholder="請輸入市内電話" value="{$data_base.local_calls}" maxlength="20">
          <input type="text" name="address" placeholder="請輸入地址" value="{$data_base.address}" maxlength="128">
        </li>
      </ul>
    <li class="title">專業證照:<div class="lincess" style="display: inline;vertical-align: middle;">
      <svg onclick="lincess_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
    </div></li>
    <li class="content lincess lincess_img">
      <svg aria-hidden="true" onclick="lincess_delete(this);" focusable="false" data-prefix="far" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x" style="position: absolute;z-index: 99;right: -34px;top: -13px;height: 25px;transform: rotate(45deg);"><path fill="currentColor" d="M384 240v32c0 6.6-5.4 12-12 12h-88v88c0 6.6-5.4 12-12 12h-32c-6.6 0-12-5.4-12-12v-88h-88c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h88v-88c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v88h88c6.6 0 12 5.4 12 12zm120 16c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-48 0c0-110.5-89.5-200-200-200S56 145.5 56 256s89.5 200 200 200 200-89.5 200-200z" class=""></path></svg>
      <input type="text" name="license_remarks[]" style="width:300px;margin: 1em 0;border-radius: 10px;" placeholder="證照名稱">
      <div class="upload" onclick="get_ilfe_img_on_name(this)" data-name="license[]" data-value="0" class="get_file"><div class="license_0"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>
        <img class="view" name="license_img[]" id="license_0_0" src="" style="display: none">
      </div>
      <input type="file" style="display:none" name="license[]" id="license_0" onchange="get_file_img_arr(this,'license')"  >
    </li>
</ul>
</div>
