<div class="col-sm-12 tab_3 tabs">
  <ul>
    <li class="title">評價紀錄:</li>
    <li class="content flex">
      <div class="assess">
        <div class="view">
          <ul class="row">
            {$ttnum=4.8}
            {$talk=61}
            <li class="ttnum">{$ttnum}</li>
            <li class="avgstat">{for $i=1;$i<=$ttnum;$i++}<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-9x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg>{/for}</li>
            <li class="talk">{$talk}個認證評價</li>
          </ul>
        </div>
        <div class="list">
          <ul class="row">
            {$list_row=array("98","2","0","0","0")}
            {for $i=0;$i<=sizeof($list_row);$i++}
            <li class="lv"><span class="num">{5-$i}<span><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-9x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><label class="line"><label style="width: {$list_row[$i]*2}px;" class="line_go"></label></label><span class="percen">{$list_row[$i]}<span></li>
            {/for}
          </ul>
        </div>
      </div>
    </li>

</ul>
</div>
