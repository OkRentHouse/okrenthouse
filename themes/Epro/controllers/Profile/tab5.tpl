<div class="col-sm-12 tab_5 tabs">
  <ul>
    <li class="title">達人密技:</li>
    <li class="content flex more">
      {$img=array("profile-07.jpg","profile-08.jpg","profile-09.jpg","profile-10.jpg")}
      {for $i=0;$i<4;$i++}

      <div class="more">
        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="minus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-minus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M140 284c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h232c6.6 0 12 5.4 12 12v32c0 6.6-5.4 12-12 12H140zm364-28c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-48 0c0-110.5-89.5-200-200-200S56 145.5 56 256s89.5 200 200 200 200-89.5 200-200z" class=""></path></svg>
        <img src="/themes/Epro/img/master/{$img[$i]}">
        <h4>水電安裝/修繕</h4>
        <span>
        快速獲得修繕、清潔報價給你最快速、最專業的叫修體驗</span>
      </div>
      {/for}

    </li>


    <ul class="detail">

      <li class="time_date">上傳照片：<div class="upload" onclick="$('#myphoto').click()"><div class="frame"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div></div><input type="file" style="display:none" id="myphoto"></li>
      <li class="contact">主標題：<input type="text"></li>
      <li class="contact">說明文字：<textarea></textarea></li>
    </ul>


</ul>
</div>
