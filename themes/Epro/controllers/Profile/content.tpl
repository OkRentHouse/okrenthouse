

<div class="bottom_fix_div">
</div>

{include file="$tpl_dir./search_bar.tpl"}

    <div class="container-fluid main" id="main">

        <div class="row profile">

          <div class="col-sm-12 head">

            <img class="photo" src="/themes/Epro/img/profile/head.jpg">
            <span>
              達人 {$smarty.session.nickname} 歡迎您回來，請更新您的最新資料，以獲得最佳的接案媒合。
            </span>

          </div>
          <div class="breadcrumbs">
              <p>{$breadcrumbs_html}{if $sidname} &gt; {$sidname}{/if}</p><br>
          </div>
          <div class="col-sm-12 content">
            <div class="col-sm-12 tabmune">
              <div class="tab_1 active">基本資料</div>
              <div class="tab_2">服務介紹</div>
              <div class="tab_3 ">評價紀錄</div>
{*              <div class="tab_4">施作案例</div>*}
{*              <div class="tab_5">達人密技</div>*}
            </div>
            <form action="/Profile"  enctype="multipart/form-data" method="post" id="sumbit_data">
              <input type="hidden" id="sumbit" name="sumbit" value="1">
              {include file="tab1.tpl"}
              {include file="tab2.tpl"}
            </form>
            {include file="tab3.tpl"}
{*            {include file="tab4.tpl"}*}
{*            {include file="tab5.tpl"}*}

            <div class="col-sm-12 tab_0 save">
              <button type="submit" name="sumbit" value="1" onclick="sumbit_data();">儲存</button>
            </div>

          </div>

        </div>
