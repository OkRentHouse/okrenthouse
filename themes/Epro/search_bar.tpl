{include file="hb.tpl"}
<div class="search_bar">
    <div class="row">
        <div class="cell-4 search mr">
            <div>

              <ul class="l_txt">

                <li class="list"><a href="/Service">找達人</a></li>
                <li class=""><a href="/Masterview">接案子</a></li>
                  <li class="logo"><a href="\" class="logo_a"><img class="logo_white" src="/themes/Epro/img/index/fn_logo_1224.png"></a></li>
                </ul>
            </div>
        </div>
        <div class="cell-5 search">
            <div class="search_bar_m">
              <div class="selects">
              <select id="search_county" name="search_county" onchange="cell_window_append_cityurl(this.value)">
                {foreach $search_county as $s_c_k =>$s_c_v}
                    <option data-content="{$s_c_v.county_name}" class="county_{$s_c_v.id_county}" {if $s_c_v.checked=='checked'}selected="selected"{/if} value="{$s_c_v.id_county}">{$s_c_v.county_name}</option>
                {/foreach}
              </select>
              <select class="itembox" id="search_epro_windows" name="search_epro_windows"  onchange="cell_window_appendurl(this.value)">
                  <option disabled="">請選擇</option>
                  {foreach $search_epro_windows as $s_e_w_k =>  $s_e_w_v}
                    <option value="{$s_e_w_v.id_epro_windows}">{$s_e_w_v.title}</option>
                  {/foreach}
              </select>
              </div>
                <input type="text" class="search_txtt" id="search_key_word" name="search_key_word" placeholder="可輸入，如：冷氣不冷 | 馬桶不通 | 壁癌粉刷 ... ">
                <button value="search" class="search_bnt"><i class="fas fa-angle-down"></i></button>
            </div>
        </div>
        <div class="cell-3 search md">
            <div>

              <ul class="r_txt">
                {if $smarty.session.nickname}
                  <li class="list"><a href="/Profile">{$smarty.session.nickname}</a></li>
                  <li class=""><a href="/Index?logout=1">登出</a></li>
                  {else}
                  <li class="list"><a href="/Signup">註冊</a></li>
                  <li class=""><a href="/Login">登入</a></li>
                {/if}

                  <li class="hb">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" width="117px" height="117px" viewBox="0 0 117 117" enable-background="new 0 0 117 117" xml:space="preserve">
                    <g>
                    <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,21.992h97.498c2.363,0,4.296,1.933,4.296,4.509l0,0c0,2.578-1.934,4.512-4.296,4.512H9.75c-2.363,0-4.295-1.934-4.295-4.512   l0,0C5.455,23.925,7.387,21.992,9.75,21.992z"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,85.773h97.498c2.363,0,4.296,2.147,4.296,4.511l0,0c0,2.576-1.934,4.725-4.296,4.725H9.75c-2.363,0-4.295-2.149-4.295-4.725   l0,0C5.455,87.921,7.387,85.773,9.75,85.773z"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" stroke-width="10" stroke-miterlimit="10" d="   M9.75,53.774h97.498c2.363,0,4.296,2.15,4.296,4.726l0,0c0,2.362-1.934,4.51-4.296,4.51H9.75c-2.363,0-4.295-2.147-4.295-4.51l0,0   C5.455,55.924,7.387,53.774,9.75,53.774z"/>
                    </g>
                    <g>
                    <defs>
                    <path id="SVGID_51_" d="M-1154.24,25.184h558.504c11.808,0,21.456,9.648,21.456,21.528l0,0c0,11.808-9.648,21.456-21.456,21.456    h-558.504c-11.88,0-21.528-9.648-21.528-21.456l0,0C-1175.768,34.832-1166.12,25.184-1154.24,25.184z"/>
                    </defs>
                    <clipPath id="SVGID_2_">
                    <use xlink:href="#SVGID_51_" overflow="visible"/>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_81_" x="-1212.92" y="3445.472" width="174.384" height="27"/>
                    </defs>
                    <clipPath id="SVGID_4_">
                    <use xlink:href="#SVGID_81_" overflow="visible"/>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_105_" x="-941.336" y="3445.472" width="173.952" height="27"/>
                    </defs>
                    <clipPath id="SVGID_6_">
                    <use xlink:href="#SVGID_105_" overflow="visible"/>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_129_" x="-669.248" y="3445.472" width="174.024" height="27"/>
                    </defs>
                    <clipPath id="SVGID_8_">
                    <use xlink:href="#SVGID_129_" overflow="visible"/>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_155_" x="-397.232" y="3445.472" width="173.952" height="27"/>
                    </defs>
                    <clipPath id="SVGID_10_">
                    <use xlink:href="#SVGID_155_" overflow="visible"/>
                    </clipPath>
                    </g>
                    <g>
                    <defs>
                    <rect id="SVGID_183_" x="-1484.936" y="3445.472" width="174.024" height="27"/>
                    </defs>
                    <clipPath id="SVGID_12_">
                    <use xlink:href="#SVGID_183_" overflow="visible"/>
                    </clipPath>
                    </g>
                    <image display="none" overflow="visible" width="117" height="117" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHUAAAB1CAYAAABwBK68AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ bWFnZVJlYWR5ccllPAAAAchJREFUeNrs3eFNwkAYBuDqBIzACGygTKBsABOoIzgBbEA3gA2ECcQJ 7AiM4F2sptJr/Kd85HmSpkn597253rXh61UVAAAAAAAAAAAAAAB/7qp0cV3Xo3SaKM/ZaxbzefNr qCnQZTo9qlcYq3Q8p3CPXxeuBRpezmtdHKkp0HE6vatRWNM0WnenI/VeXUK7K91+R+oS2qQ4pxJ7 JVwK9aAuob31Qk2T7FawYeXHmXro9rvoDmPCBDrtPqeWXj6M2pXwWL1ijNBuoAAAAAAAAAAAAABc pKFO8txsowvu/B3azorhUNs/cm/ScatecYJNxyKF+90yc9p2IdB48l11sJM8//iqRmHl0VqfjlRz aGw3Q7df4hqXQtU5FX/B1At1py6h7YceaTbm1pB2aZE0HZpTcyf5Vo1iBZqOWfGR5mTEeqMUw7b7 0gEAAAAAAAAAAACAi1XaQWqcTg+VPckjaKrPraubwVDbP3G/VPZSjWbW/UxAqZNcoPGs2087/Aw1 XbRpX1w50HlppJpDY9NJfqGjtRdqoy6h7Uuh5tWTTwTEVfdCbXfLfVKbkFbdZ9XSy4e8Cl5aCYdw bF8+rJQCAAAAAADgv30IMAA8L2iFTX/P8QAAAABJRU5ErkJggg==">
                    </image>
                    </svg>
                  </li>
                </ul>

            </div>

        </div>
    </div>
</div>
<div class="search_windows">
    {foreach $windows_data as $key => $value} {if $key%10==0}
    <div class="row" {if ($key/10)%2==1}style="margin-left: 4%;" {/if}>
        {/if}
        <div class="cell_window lazyload" data-src="{$value.img}" style="background: url({$value.img});">
            <span><a href="?sid={$value.id_epro_windows}" alt="{$value.keyword}">{$value.title}</a></span>
        </div>
        {if $key%10==9 || $windows_count==$key}
    </div>
    {/if} {/foreach}
</div>
