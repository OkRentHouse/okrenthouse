<div class="store store_list">
	{foreach $arr_store as $v => $store}
		<div class="item" data-id="{$store.id_store}">
			<div class="img"><a href="/Store?id={$store.id_store}"><img src="{if $store.img}{$store.img}{else}{$THEME_URL}/img/default.jpg{/if}"></a><div class="favorite {if $store.favorite}on{/if}" data-id="{$store.id_store}" data-type="store">{if $store.favorite}<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}</div></div>
			<div class="main_title"><div class="title">{$store.title}</div><div class="career">{$store.store_type}</div><div class="popularity div_popularity give_star" data-title="{$store.title}" data-id="{$store.id_store}">{$store.popularity_img}</div></div>
			{if count($store.service)>0}<div class="hot"><div class="subtitle"><i><img src="{$THEME_URL}/img/hot.svg"></i>HOT</div><div>{foreach $store.service as $i => $service}<samp>{$service}</samp>{/foreach}</div></div>{/if}
			{if count($store.discount)>0}<div class="offer"><div class="subtitle">OFFER</div>{foreach $store.discount as $i => $discount}<a href="/Store?id={$store.id_store}">{$discount}</a>{/foreach}</div>{/if}
		</div>
	{foreachelse}
		<div class="item text-center">
			查不到相關店家
		</div>
	{/foreach}
</div>
<div class="store_list_loading text-center">{include file="$tpl_dir./mobile/lds.tpl"}</div>