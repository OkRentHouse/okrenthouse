<div class="item_wrap_box store_box">
	<div class="item_wrap">
		<div class="item">
			<div class="item_new">
				<img src="{$THEME_URL}/img/new.png">
			</div>
			<div class="item_more"><a href="/SearchStore">more...</a></div>
		</div>
        {foreach $arr_store as $v => $store}
		<div class="item">
			<a class="img_frame" href="/Store?id={$store.id_store}">
				<img src="{if $store.img}{$store.img}{else}{$THEME_URL}/img/default.jpg{/if}">
				<div class="info">{$store.title}</div>
			</a>
		</div>
		{/foreach}
		<div class="clear"></div>
	</div>
</div>