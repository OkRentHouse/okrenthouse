<form id="search_form" name="search_form" method="get" action="{$action}">
	<div class="search_select">
		<span><a href="#" data-transition="slide">桃園市桃園區</a></span>
		<span><a href="#" data-transition="slide">租屋業</a></span>
	</div>
	<div id="search_box">
		<div id="search_text">
			<input name="field" type="text" class="field" data-role="none" placeholder="請輸入關鍵字(店家名稱等)"
				   size="40"/>
		</div>
		<div class="search_btn">
			<img src="{$THEME_URL}/img/icon/pic_m.svg" class="sunmit list icon">
		</div>
		<div class="search_btn">
			<img src="{$THEME_URL}/img/icon/map_m.svg" class="sunmit map icon">
		</div>
	</div>
</form>