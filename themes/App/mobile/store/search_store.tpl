<form action="{$action}" method="get" class="search_content store_search">
	<div class="search_wrap none_buoy">
		<select class="button county active" data-role="none" data-v="{$smarty.get.county}" name="county">
			<option value="">{l s="全部"}</option>
            {foreach $arr_county as $v => $arr}
                {if isset($smarty.get.county)}
					<option value="{$arr.value}"{if $smarty.get.county == $arr.value} selected{/if}>{$arr.title}</option>
                {else}
					<option value="{$arr.value}"{if $default_county == $arr.value} selected{/if}>{$arr.title}</option>
                {/if}
            {/foreach}
		</select><select class="button area" data-role="none" data-v="{$smarty.get.parent}" name="area">
			<option value="">{l s="全部"}</option>
            {foreach $arr_area as $v => $arr}
                {if isset( $smarty.get.area)}
					<option value="{$arr.value}" data-parent="{$arr.parent}" data-parent_name="{$arr.parent_name}" style="display: none;"{if $smarty.get.area == $arr.value} selected{/if}>{$arr.title}</option>
                {else}
					<option value="{$arr.value}" data-parent="{$arr.parent}" data-parent_name="{$arr.parent_name}" style="display: none;"{if $default_area == $arr.value} selected{/if}>{$arr.title}</option>
                {/if}
            {/foreach}
		</select><select class="button career" data-role="none" data-v="{$smarty.get.career}" name="career">
			<option value="">{l s="全部"}</option>
            {foreach $arr_career as $v => $arr}
                {if isset( $smarty.get.career)}
					<option value="{$arr.value}"{if $smarty.get.career == $arr.value} selected{/if}>{$arr.title}</option>
                {else}
					<option value="{$arr.value}"{if $default_career == $arr.value} selected{/if}>{$arr.title}</option>
                {/if}
            {/foreach}
		</select>
		<buoy></buoy>
	</div>
	<div id="search_box">
		<div id="search_text">
			<input name="title" id="title" type="text" class="field" value="{$smarty.get.title}" data-role="none" placeholder="請輸入關鍵字(店家名稱等)"
				   size="40"/>
		</div>
		<div class="search_btn">
			<button type="button" class="button pic_btn" data-role="none"><img src="{$THEME_URL}/img/icon/pic_m.svg"></button>
		</div>
		<div class="">
			<button type="button" class="button map_btn" data-role="none"><img src="{$THEME_URL}/img/icon/map_m.svg"></button>
		</div>
	</div>
</form>