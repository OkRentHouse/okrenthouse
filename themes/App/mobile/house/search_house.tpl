
<form id="search_form" name="search_form" method="get" action="/list">
	<input type="hidden" name="active" value="1">
	<input type="hidden" name="house_choose" value="{$smarty.get.house_choose}">
	<input type="hidden" name="id_main_house_class" value="{$smarty.get.id_main_house_class}">
	<input type="hidden" name="id_types" value="{$smarty.get.id_types}">
	<input type="hidden" name="id_type" value="{$smarty.get.id_type}">
	<input type="hidden" name="county" value="{$smarty.get.county}">
	<input type="hidden" name="city" value="{$smarty.get.city}">
	<div style="display: flex">
		<div class="search_select">
			<span><a href="#" data-transition="slide" class="ui-link ui-link-house_choose-txt">租屋</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-county-txt">區域</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-main_house_class-txt">用途</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-types-txt">型態</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-type-txt">類別</a></span>
		</div>

		<div id="search_box">
			<div id="search_btn" class="search_btn">
				<img src="{$THEME_URL}/img/icon/pic_m.svg" class="sunmit icon">
			</div>
		</div>
	</div>
</form>
{$js}