<form id="search_form" name="search_form" method="get" action="/list">
    <input type="hidden" id="active" name="active" value="{$smarty.get.active}">
    <input type="hidden" name="house_choose" value="{$smarty.get.house_choose}">
    <input type="hidden" name="id_main_house_class" value="{$smarty.get.id_main_house_class}">
    <input type="hidden" name="id_types" value="{$smarty.get.id_types}">
    <input type="hidden" name="id_type" value="{$smarty.get.id_type}">
    <input type="hidden" name="county" value="{$smarty.get.county}">
    <input type="hidden" name="city" value="{$smarty.get.city}">
    <input type="hidden" name="max_price" value="{$smarty.get.max_price}">
    <input type="hidden" name="min_price" value="{$smarty.get.min_price}">
    <input type="hidden" name="max_room" value="{$smarty.get.max_room}">
    <input type="hidden" name="min_room" value="{$smarty.get.min_room}">
    <input type="hidden" name="id_other" value="{$smarty.get.id_other}">
    <input type="hidden" name="special" value="{$smarty.get.special}">

    <div class="contect_0">
        <div class="conditions">
            <ul class="head">
                <li>
                    租售
                </li>
            </ul>

            <div class="b_div">
              <div class="b2_div">
                <label id="label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                    {foreach $house_choose as $key => $value}
                    <li name="house_choose_a[]" id="house_choose_{$key}" value="{$value.value}" onclick="click_li(this);">{$value.title}</li>
                    {/foreach}
                </ul><div class="close_window">OK</div><div class="close_window_x">OK</div>
            </div></div>

            {$rck=0}
            <!--<div data-role="fieldcontain">
                <fieldset data-role="controlgroup">
                {foreach $house_choose as $key => $value}
                        <input type="radio" name="house_choose_a[]" id="radio-choice-{$rck}" value="{$value.value}" {if $key==0}checked="checked"{/if} />
                        <label for="radio-choice-{$rck}">{$value.title}</label>
                        {$rck=$rck+1}
                {/foreach}
                </fieldset>
            </div>-->
        </div>
    </div>

    <div class="contect_1">
        <div class="conditions">
            <ul class="head" onclick="get_county();">
                <li>區域</li>
            </ul>
            <div class="b_div">

                <ul class="buttons_c">
                    <li>
                        <a id="citys_txt" href="#" class="dropdown-toggle"></a>
                        <ul id="citys" class="d-menu" data-role="dropdown">
                        </ul>
                    </li>
                    <li>
                    </li>
                </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="contect_1_1" style="display:none">
        <div class="conditions">

            <ul class="head">
            </ul>
            <div class="select_box_bk"></div>
            <div class="select_box">
            <div class="b_div"></div><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
        </div>
    </div>

    <div class="contect_1_2" style="display:none">
        <div class="conditions">

            <ul class="head">
            </ul>
            <div class="select_box_bk"></div>
            <div class="select_box">
            <div class="b_div"></div><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
        </div>
    </div>

    <div class="contect_2">
        <div class="conditions">
            <ul class="head" onclick="check_label_types(this);">
                <li>
                    用途
                </li>
            </ul>
            <div class="b_div">
              <div class="b2_div">
                <label id="label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                    {foreach $id_main_house_class as $key => $value}
                    <li name="id_main_house_class_a[]" id="id_main_house_class_{$key}" value="{$value.id_main_house_class}" onclick="click_li(this);">{$value.title}</li>
                    {/foreach}
                </ul>
                <div class="close_window">OK</div><div class="close_window_x">OK</div>
            </div></div>


            <!--<div data-role="fieldcontain">
                <fieldset data-role="controlgroup">
                {foreach $id_main_house_class as $key => $value}
                        <input type="radio" name="id_main_house_class_a[]" id="radio-choice-{$rck}" value="{$value.id_main_house_class}" {if $key==0}checked="checked"{/if} />
                        <label for="radio-choice-{$rck}">{$value.title}</label>
                        {$rck=$rck+1}
                {/foreach}
                </fieldset>
            </div>-->
        </div>
    </div>

    <div class="contect_3">
        <div class="conditions">
            <ul class="head" onclick="check_label_types(this);">
                <li>
                    型態<br><!--<i class="type fas fa-caret-down"></i>-->
                </li>
            </ul>
            <div class="b_div b_div_id_types">
                <label id="types_label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                {foreach $select_types as $key=>$value} {if $key%4==0} {if $key!=0}<!--</ul>-->{/if}
                    <!--<ul class="buttons">-->{/if}
                    <li name="id_types_a[]" id="id_types_{$key}" value="{$value.id_rent_house_types}" onclick="click_li(this);">{$value.title}</li>
                    {if count($select_types)==$key}<!--</ul>-->{/if} {/foreach}
                </ul><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
            </div>
        </div>
    </div>

    <div class="contect_4">
        <div class="conditions">
            <ul class="head" onclick="check_label_type(this);">
                <li>
                    類別<br><!--<i class="type fas fa-caret-down"></i>-->
                </li>
            </ul>
            <div class="b_div b_div_id_type">
                <label id="type_label" onclick="">請選擇</label>
                <div class="select_box_bk"></div>
                <div class="select_box"><ul class="buttons">
                {foreach $select_type as $key=>$value} {if $key%4==0} {if $key!=0}<!--</ul>-->{/if}
                    <!--<ul class="buttons">-->{/if}
                    <li name="id_type_a[]" id="id_type_{$key}" value="{$value.id_rent_house_type}" onclick="click_li(this);">{str_replace(" ","",$value.title)}</li>
                    {if count($select_type)==$key}<!--</ul>-->{/if} {/foreach}
                </ul><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
            </div>
        </div>
    </div>

    <div class="contect_5">
        <div class="conditions">
            <ul class="head">
                <li>房數</li>
            </ul>
            <div class="b_div">
              <div class="b_div_editer"> <input type="text" id="room_min_editer"> <span>房 ~ </span> <input type="text" id="room_max_editer"> <span>房</span> </div>
                <input id="room" data-hint-position-min="top" data-hint-position-max="top" data-role="doubleslider" data-min="{$min_room}" data-max="{$max_room}" data-hint-always="false" data-hint="true" data-value-max="1" class="ultra-thin cycle-marker" data-on-change-value="slider_get(this);slider_transfor_room(this)">
                <div class="Sroom_txt">
                    <div style="width: 5%;padding-right: 14px;">Open</div>
                    {for $i=1;$i<=5;$i++}
                    <div>{$i}</div>
                    {/for }
                    <div style="padding-right: 0px;width: 60px;">5+</div>
                </div>
            </div>
        </div>
    </div>

    <div class="contect_5_5">
        <div class="conditions">
            <ul class="head">
                <li>坪數</li>
            </ul>
            <div class="b_div">
              <div class="b_div_editer"> <input type="text" id="ping_min_editer"> <span>坪 ~ </span> <input type="text" id="ping_max_editer"> <span>坪</span> </div>
                <input id="ping" data-hint-position-min="top" data-hint-position-max="top" data-role="doubleslider" data-min="{$min_ping}" data-max="{$max_ping}" data-hint-always="false" data-hint="true" data-value-max="1" class="ultra-thin cycle-marker" data-on-change-value="slider_get(this);slider_transfor_ping(this)">
                <div class="Sroom_txt">
                    <div style="width: 5%;padding-right: 14px;">10</div>
                    {for $i=2;$i<6;$i++}
                    <div>{$i}0</div>
                    {/for }
                    <!-- <div style="padding-right: 0px;width: 60px;">50+</div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="contect_6">
        <div class="conditions">
            <ul class="head">
                <li>預算</li>
            </ul>
            <div class="b_div">
                <div class="b_div_editer"> <input type="text" id="min_editer"> <span>元 ~ </span> <input type="text" id="max_editer"> <span>元</span> </div>
                <input id="price" data-role="doubleslider" data-accuracy="1000" data-min="{$min_price}" data-max="{$max_price}" data-value-max="10000"
                    class="ultra-thin cycle-marker" data-on-change-value="slider_get(this);slider_transfor(this)" data-hint="true">
                <div class="Scale_txt">
                    <div style="width: 2em;">1萬</div>
                    <!-- <div>1萬</div> -->
                    <div>2萬</div>
                    <div>3萬</div>
                    <div>4萬</div>
                    <div>5萬</div>
                    <div style="width: 27px;margin-right: -10px;">+</div>
                </div>
                <div class="Scale">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="contect_7">
        <div class="conditions under">
            <ul class="head">
                <li>
                    條件<br><!--<i class="type fas fa-caret-down"></i>-->
                </li>
            </ul>
            <div class="b_div">
              <label style="display:none">請選擇</label>
              <div class="select_box_bk"></div>
              <div class="select_box"><ul class="buttons">
                {foreach $select_other_conditions as $key=>$value} {if $key%3==0} {if $key!=0}<!--</ul>-->{/if}
                <!-- <ul class="buttons fixshow"> -->
                  {/if}
                    <li name="id_other_a[]" id="id_other_{$key}" value="{$value.id_other_conditions}" onclick="click_li(this);">{$value.title}</li>
                    {if count($select_other_conditions)==$key}<!--</ul>-->{/if} {/foreach}
                    </ul><div class="close_window">OK</div><div class="close_window_x">OK</div></div>
            </div>
        </div>
    </div>



    <div class="contect_8" style="
"><button type="button" class="search_btn">　搜尋</button></div>


        {$tt_i=19}
        {$f_style=''}

        <style>{for $i=1;$i<=$tt_i;$i++}
        {if round($tt_i/2)==$i}
.morex svg:nth-child({$i}) {
    font-size: 22pt;
}
        {/if}
        {if round($tt_i/2)+1==$i || round($tt_i/2)-1==$i}
.morex svg:nth-child({$i}) {
    font-size: 22pt;
}
        {/if}
        {if round($tt_i/2)+2==$i || round($tt_i/2)-2==$i}
.morex svg:nth-child({$i}) {
    font-size: 20pt;
}
        {/if}
        {if round($tt_i/2)+2==$i || round($tt_i/2)-2==$i}
.morex svg:nth-child({$i}) {
    font-size: 16pt;
    {if round($tt_i/2)+2==$i}
    margin-right: 20px;
    {/if}
    {if round($tt_i/2)-2==$i}
    margin-left: 20px;
    {/if}
}
        {/if}
        {if round($tt_i/2)+6<$i || round($tt_i/2)-6>$i}
.morex svg:nth-child({$i}) {
    font-size: 8pt;
}
        {/if}
        {/for }
.morex svg {
    font-size: 8pt;
}
</style>


    <div class="more">
      <div class="content">
      <!--<i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i>-->
      <!--<span>更多篩選條件</span>-->
      <!--<i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i><i class="fas fa-ellipsis-v fa-xs"></i>-->
      </div>
      <div id="search_box">
          <div id="search_text">
              <input name="search" id="search" type="text" class="field" data-role="none" placeholder="請輸入關鍵字" size="40" value="{$smarty.get.search}">
          </div>
          <div id="search_btn" class="search_btn">
              <img src="/themes/App/mobile/img/icon/pic_m.svg" class="sunmit icon">
          </div>
      </div>
      <div>

        {$f_style=''}

        {for $i=1;$i<=$tt_i;$i++}
        {if round($tt_i/2)+2<$i || round($tt_i/2)-2>$i}
        <!--<i class="fas fa-ellipsis-v fa-xs"></i>-->
        {else}
        {if round($tt_i/2)==$i}
        <!--<i class="fas fa-long-arrow-alt-down"></i>-->
        {else}
        <!--<i class="fas fa-long-arrow-alt-down"></i>-->{/if}{/if}{/for }
    </div></div>
</form>
<script>
    {$js}
</script>
