﻿<div class="menu_shadow"></div>
{*<div class="open_btn glyphicon glyphicon-menu-hamburger"></div>*}
<div class="side_menu">
	<div class="close_btn glyphicon glyphicon-remove"></div>
	<div class="title">
		<div class="logo text-center">
			<a href="/" data-role="none"><img class="" src="{$THEME_URL}/img/icon/logo.svg"></a>
		</div>
	</div>
	<div class="main_menu">
		<div class="nav_body">
			<div class="login_div text-center">
				<a class="login ui-link" href="login">會員登入</a>
			</div>
			<ul class="">
				<li class="list">
					<a class="ui-link underline" href="#">會員中心</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/star-03.svg"><a class="ui-link" href="#">我的</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/flow-02.svg"><a class="ui-link" href="#">好康VIP</a>
				</li>
				<li class="list">
					<img class="" ><a class="ui-link underline" href="#">租事順利</a>
				</li>
				<li class="list">
					<img class="" ><a class="ui-link underline" href="#">房地專區</a>
				</li>
				<li class="list">
					<i class="fas fa-search"></i><a class="ui-link" href="/list">吉屋快搜</a>
				</li>
				<li class="list">
					<i class="far fa-thumbs-up"></i><a class="ui-link" href="/list?switch=1" >精選推薦</a>
				</li>
{*				<li class="list">*}
{*					<i class="fas fa-ad"></i><a class="ui-link" href="#">刊登廣告</a>*}
{*				</li>*}
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/Inquire.svg"><a class="ui-link" href="#">房東專區</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/house.svg"><a class="ui-link" href="#">租客專區</a>
				</li>

				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/mail-04.svg"><a class="ui-link" href="#">信用認證</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/heart_d.svg"><a class="ui-link" href="#">安心租履約</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/credit.svg"><a class="ui-link" href="#">生活達人</a>
				</li>
				<li class="list">
					<img class="" ><a class="ui-link underline" href="/Inlife">生活分享</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/gift.svg"><a class="ui-link" href="/coupon">生活好康</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/share1.svg"><a class="ui-link" href="/Inlife">活動分享</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/share2.svg"><a class="ui-link" href="/WealthSharing">創富分享</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/fix.svg"><a class="ui-link" href="#">裝修達人</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/work.svg"><a class="ui-link" href="#">好幫手</a>
				</li>
				<li class="list">
					<img class="" src="{$THEME_URL}/img/icon/shar.svg"><a class="ui-link" href="#">共享圈</a>
				</li>
				<li class="list">
					<img class="" ><a class="ui-link underline" href="#">客服中心</a>
				</li>
			</ul>

			<div class="close">
				<div class="close_btn glyphicon glyphicon-remove"></div>
			</div>
		</div>
	</div>
</div>
