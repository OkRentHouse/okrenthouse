function alert(msg){
	Swal.fire(msg);
};
function open_side_menu(){
	$('.menu_shadow').stop(true, false).fadeIn('fast');
	$('.side_menu').animate({left:'0px'}, 'fast');
};
function close_side_menu(){
	$('.menu_shadow').stop(true, false).fadeOut('fast');
	$('.side_menu').animate({left:'-320vw'}, 'fast',function(){
		// $('.subs', $side_menu).stop(true, false).slideUp('fast');
		// $('.side_menu').scrollTop(0);
	});
};
//恢復開關
function flipswitch($this, type){
	if(type){
		$this.find('option[value="on"]').prop('selected', true);
		$this.parent('.ui-flipswitch').addClass('ui-flipswitch-active');
	}else{
		$this.find('option[value="off"]').prop('selected', true);
		$this.parent('.ui-flipswitch').removeClass('ui-flipswitch-active');
	};
};
function set_personally($type){		//本人領取
	$.ajax({
		url : '/information',
		method : 'POST',
		data : {
			'ajax'      : true,
			'action'    : 'SetPersonally',
			'type' : $type,
		},
		dataType : 'json',
		success: function(msg){
			if(!msg){
				$.growl.error({
					title: '',
					message: '資料錯誤'
				});
			};
		},
		beforeSend:function(){

		},
		complete:function(){

		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '資料錯誤'
			});
		}
	});
};
function setFCM($type){			//APP推播通知
	$.ajax({
		url : '/information',
		method : 'POST',
		data : {
			'ajax'      : true,
			'action'    : 'SetFCM',
			'type' : $type,
		},
		dataType : 'json',
		success: function(msg){
			if(!msg){
				$.growl.error({
					title: '',
					message: '資料錯誤'
				});
				flipswitch($('#information #fms'), !$type);
			};
		},
		beforeSend:function(){

		},
		complete:function(){

		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '資料錯誤'
			});
			flipswitch($('#information #fms'), !$type);
		}
	});
};
function setEmailOn($type){		//Email通知
	$.ajax({
		url : '/information',
		method : 'POST',
		data : {
			'ajax'      : true,
			'action'    : 'SetEmail',
			'type' : $type,
		},
		dataType : 'json',
		success: function(msg){
			if(!msg){
				$.growl.error({
					title: '',
					message: '資料錯誤'
				});
				flipswitch($('#information #email_notification'), !$type);
			};
		},
		beforeSend:function(){

		},
		complete:function(){

		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '資料錯誤'
			});
			flipswitch($('#information #email_notification'), !$type);
		}
	});
};
function personally(){
	let $personally = $('#personally', '#information');
	if($personally.val() == 'off'){
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-primary',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false,
		});
		swalWithBootstrapButtons.fire({
			title: '不限本人領取?',
			html: '您將取消"限本人裡取"設定<br>取消後先前信件、包裹領件紀錄同住戶將檢示的到!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: '確定',
			cancelButtonText: '取消',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				swalWithBootstrapButtons.fire(
					'不限本人領取',
					'同住戶將可代領您的信件、包裹;<br>檢視您的先前信件、包裹領件紀錄',
					'success'
				);
				set_personally(false);
			} else{
				flipswitch($personally, true);
				set_personally(true);
			};
		});
	}else{
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-primary',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false,
		});
		swalWithBootstrapButtons.fire({
			title: '僅限本人領取?',
			html: '您將設定"限本人裡取"<br>設定後先前信件、包裹領件紀錄同住戶將無法檢示!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: '確定',
			cancelButtonText: '取消',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				swalWithBootstrapButtons.fire(
					'僅限本人領取',
					'同住戶無法代領您的信件、包裹;<br>無法檢視您的先前信件、包裹領件紀錄',
					'success'
				);
				set_personally(true);
			} else{
				flipswitch($personally, false);
				set_personally(false);
			};
		});
	};
};
function ServiceMessage(){
	$.ajax({
		url : document.location.pathname,
		method : 'POST',
		data : {
			'ajax'		: true,
			'action'		: 'Message',
			'id_service'	: $('#id_service').val(),
			'id_message'	: $('#id_message').val(),
			'message'		: $('#message').val()
		},
		dataType : 'JSON',
		success: function(msg){
			if(msg.error == ''){
				$('#message').val('');
				$('#id_message').val(msg.id_message);
				msg.message.forEach(function(v){
					var class_txt = '';
					if(v.id_admin > 0){
						class_txt = 'admin_msg';
					}else{
						class_txt = 'member_msg text-right';
					};
					var htm = `<div class="` + class_txt + `">
						<div class="time">` + v.time + `</div>
						<div class="message_div">` + v.message + `</div>
					</div>`;
					$('.message_list2').append(htm).scrollTop(99999999999);
				});
			}else{
				$.growl.error({
					title: '',
					message: msg.error
				})
			};
		},
		error:function(xhr, ajaxOptions, thrownError){
			$.growl.error({
				title: '',
				message: '留言失敗!'
			})
		}
	});
};
//螢幕變亮
if(typeof(Android) != 'undefined'){				//Android
	function screen(on){
		try{
			Android.screen(on);
		}catch (e){

		};
	};
}else if(typeof(window.webkit) != 'undefined'){	//IOS
	function screen(on){
		try{
			window.webkit.messageHandlers.screen.postMessage(on);
		}catch (e){

		};
	};
}else{
	function screen(on){
		return false;
	};
};
function logout(){
	if(typeof(Android) != 'undefined'){
		try{
			Android.set_msg_num(0, re_time);
			Android.logout();
		}catch (e){

		};
	};
	if(typeof(window.webkit) != 'undefined'){
		try{
			window.webkit.messageHandlers.logout.postMessage();
		}catch (e){

		};
	};
};
function set_msg_num(msg_num, msg_time){
	if(typeof(Android) != 'undefined' && typeof(msg_num) != 'undefined' && typeof(msg_time) != 'undefined'){
		if(msg_time > 0){
			Android.set_msg_num(msg_num, msg_time);
		};
	};
	if(typeof(window.webkit) != 'undefined' && typeof(msg_num) != 'undefined' && typeof(msg_time) != 'undefined'){
		if(msg_time > 0){
			window.webkit.messageHandlers.set_msg_num.postMessage({'msg_num':msg_num, 'msg_time':msg_time});
		};
	};
};
function login(reg_id, type = 'Android'){
	$.ajax({
		url : '/',
		method : 'POST',
		data : {
			'ajax'	: true,
			'action': 'FCM',
			'reg_id': reg_id,
			'type'	: type
		},
		dataType : 'json',
		success: function(msg){
			set_msg_num(parseInt(msg.msg_num), parseInt(msg.msg_time));
		},
		beforeSend:function(){
		},
		complete:function(){
		},
		error:function(xhr, ajaxOptions, thrownError){
			// alert('error');
		}
	});
	return true;
};
function welcome_close(){
};
// //todo 加入我的最愛
// function add_favorite(type, id, on) {
// 	let $this = $('.favorite[data-type="' + type + '"][data-id="' + id + '"]');
// 	if(id != undefined && id > 0){
// 		if(on == true){
// 			$this.children('img').attr('src', THEME_URL + '/img/icon/heart.svg');
// 			$this.removeClass('on').addClass('off');
// 		}else{
// 			$this.children('img').attr('src', THEME_URL + '/img/icon/heart_on.svg');
// 			$this.removeClass('off').addClass('on');
// 		}
// 	}
// };
//todo 加入我的最愛
function add_favorite(type, id, on) {
	let $this = $('.favorite[data-type="' + type + '"][data-id="' + id + '"]');
	if(id != undefined && id > 0){
		if(on == true){
			$this.children('img').attr('src', THEME_URL + '/img/icon/heart.svg');
			$this.removeClass('on').addClass('off');
		}else{
			$this.children('img').attr('src', THEME_URL + '/img/icon/heart_on.svg');
			$this.removeClass('off').addClass('on');
		}

//施工中 favorite
		$.ajax({
			url: document.location.pathname,
			method: 'POST',
			data: {
				'ajax': false,
				'action': 'Favorite',
				'table_name' : type,
				'table_id' : id
			},
			dataType: 'json',
			success: function (data) {
				if(data["error"] !=""){
					alert(data["error"]);
				}else{
					if(data["active"]=='1'){
						alert("已加入我的最愛");
					}else{
						alert("取消加入我的最愛");
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			}
		});

	}
};
function give_star_img_htm(id, star, rounding=false){
	var img = '';
	for(i=1;i<=5;i++){
		var clas = '';
		if(i == star){
			clas = ' class="active"';
		};
		if(i <= star){				//一顆
			img += '<img src="'+THEME_URL+'/img/popularity2.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
		}else if(star - i >= 0.5){	//半顆
			if(rounding){	//顯示整顆星
				img += '<img src="'+THEME_URL+'/img/popularity2.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
			}else{	//顯示半顆星
				img += '<img src="'+THEME_URL+'/img/popularity2_half.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
			}
		}else{
			img += '<img src="'+THEME_URL+'/img/popularity2_gray.svg" data-id="'+id+'" data-star="'+i+'"'+clas+'>';
		};
	};
	return img;
};
//todo 給星星
function give_star(title, id, star){
	let img = give_star_img_htm(id, star);
	Swal.fire({
		title: title,
		html: '<div class="my_give_div">我給<div class="my_give" data-id="'+id+'">'+img+'</div></div>',
		focusConfirm: false,
		// showCloseButton: true,
		confirmButtonText:
			'確認',
	}).then((result) => {
		if (result.value) {
			let star = $('.my_give[data-id="'+id+'"] img.active').data('star');
			// $('.div_star[data-id="'+id+'"]').html(give_star_img_htm(id, star, true));
			// $('.div_popularity[data-id="'+id+'"]').html(give_star_img_htm(id, star, true));
			$.ajax({
				url: document.location.pathname,
				method: 'POST',
				data: {
					'ajax': false,
					'action': 'Popularity',
					'id' : id,
					'star' : star
				},
				dataType: 'json',
				success: function (data) {
					console.log(data);
					if(data["error"] !=""){
						alert(data["error"]);
					}else{
						// console.log(data);
						$('.div_star[data-id="'+id+'"]').html(give_star_img_htm(id,data["return"]["mystar"], true));
						$('.div_popularity[data-id="'+id+'"]').html(give_star_img_htm(id,data["return"]["popularity"], true));
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log("test");
				}
			});
			//todo ajax
		};
	});
};
function app_version(version){
	// alert(version);
};
var st;
function countdown(mini_second, fun, t, url){
	if(t == 0){
		clearTimeout(st);
	};
	$('.cd_m', '.countdown').css('transform', 'rotate(' + t / mini_second * 360 + 'deg)');
	if(t >= (mini_second/2)){
		$('.cd_t', '.countdown').hide();
		$('.cd_c', '.countdown').show();
	};
	if(t >= mini_second){
		eval(fun);
		$('.cd_t', '.countdown').show();
		$('.cd_c', '.countdown').hide();
		t = 0;
	};
	t += 0.5;
	if(location.pathname == url){
		st = setTimeout('countdown(' + mini_second + ', \'' + fun + '\', ' + t + ', \'' + url + '\')', 500);
	}else{
		clearTimeout(st);
	};
};

if(typeof(st) == "undefined"){
	st = null;
};

//todo
function show_menu_buoy(type){
	$('.menu_buoy').each(function () {
		let $this = $(this);
		if($('a.active', $this) == undefined){
			$('a', $this).eq(0).addClass('active');
		}
		let $active = $('a.active', $this);
		if($active.position('div') != undefined){
			let $this_left = $this.offset().left;
			let $byoy = $('buoy', $this);
			let left = $active.position('div').left;
			let width = $active.outerWidth();
			let all_width = $('div', $this).outerWidth();
			var ls = 0;
			var rs = 0;
			if($byoy.offset().left > left-$this_left){		//左
				ls = 200;
				rs = 300;
			}else{			//右
				ls = 300;
				rs = 200;
			};
			if(type){
				$('buoy', $this).css({'left':left, 'right':all_width-width-left});
			}else{
				$('buoy', $this).animate({'left':left}, { duration: ls, queue: false });
				$('buoy', $this).animate({'right':all_width-width-left}, { duration: rs, queue: false });
			}
		}
	});
};
function show_search_buoy(type){
	$('.search_content').each(function () {
		let $this = $(this);
		if($('select.active', $this) == undefined){
			$('select', $this).eq(0).addClass('active');
		}
		let $active = $('select.active', $this);
		if($active.position('div') != undefined){
			let $this_left = $this.offset().left;
			let $byoy = $('buoy', $this);
			let left = $active.position('div').left;
			let width = $active.outerWidth();
			let all_width = $('div', $this).outerWidth();
			var ls = 0;
			var rs = 0;
			if($byoy.offset().left > left-$this_left){		//左
				ls = 200;
				rs = 300;
			}else{			//右
				ls = 300;
				rs = 200;
			};
			if(type){
				$('buoy', $this).css({'left':left, 'right':all_width-width-left});
			}else{
				$('buoy', $this).animate({'left':left}, { duration: ls, queue: false });
				$('buoy', $this).animate({'right':all_width-width-left}, { duration: rs, queue: false });
			}
		}
	});
};
function menu_buoy(){
	$(document).on('click', '.menu_buoy a', function(){
		$(this).prevAll('a').removeClass('active');
		$(this).addClass('active');
		show_menu_buoy();
	});
	$('.menu_buoy').ready(function(){
		show_menu_buoy();
	});
};
function search_buoy(){
	$(document).on('click', '.search_content select', function(){
		$(this).prevAll('select').removeClass('active');
		$(this).addClass('active');
		show_search_buoy();
	});
	$('.search_content').ready(function(){
		show_search_buoy();
	});
};
//幻燈片
function slide_photo_slider(){
	$('.slide_photo ul li').on('beforeitemshow', function(){
		$slide_photo = $(this).parents('.slide_photo');
		let li = $('ul li', $slide_photo);
		li.each(function (i) {
			if($(this).hasClass('uk-active') && !$(this).hasClass('uk-transition-active')){
				$('.uk-slideshow-nav a', $slide_photo).removeClass('active');
				$('.uk-slideshow-nav a', $slide_photo).eq(i).addClass('active');
			};
		});
	});
};
$.mobile.defaultPageTransition = 'none';

function free_show(){
$(".free").animate({top:-10},50,function(){
        $(".free").animate({top:-5},50,function(){
            $(".free").animate({top:-15},50,function(){
                $(".free").animate({top:-5},50,function(){
                    $(".free").animate({top:-13,opacity:0},150,function(){
                        $(".free").animate({opacity:1,fontSize:"14pt",letterSpacing: 5},150,function(){
                            $(".free").animate({opacity:0,fontSize:"8pt",letterSpacing: 0},150,function(){
                                $(".free").animate({opacity:1,fontSize:"12pt",letterSpacing: 2},150,function(){
                                    $(".free").animate({opacity:1,fontSize:"8pt",letterSpacing: 0},150,function(){})
      })
      })
      })
      })
      })
      })
      })
      })
		}

$(document).ready(function(){

	setInterval(function () {
    free_show()
  }, 2000);

	let $side_menu = $('.side_menu');
	let $menu_shadow = $('.menu_shadow');
	$('.close_btn', $side_menu).click(function(){
		close_side_menu();
	});
	$('.menu_shadow').click(function(){
		close_side_menu();
	});
	$('a[href!="javascript:void(0);"]', $side_menu).click(function(){
		$('.action', $side_menu).removeClass('action');
		$(this).addClass('action');
		close_side_menu();
	});
	$('a[href="javascript:void(0);"]', $side_menu).click(function(){
		let $this = $(this);
		let $div = $this.parent('div');
		let $subs = $this.next('.subs');
		$('a', $div.siblings('div')).each(function(){
			$(this).removeClass('active');
			$(this).next('.subs').stop(true, false).slideUp('fast');
		});
		if($this.hasClass('active')){
			$this.removeClass('active');
			$subs.stop(true, false).slideUp('fast');
		}else{
			$this.addClass('active');
			$subs.stop(true, false).slideDown('fast');
		};
	});
	change_option();
	auto_address();
}).on('click', '.open_btn', function(){
	open_side_menu();
}).on('change','#information #personally', function(){	//Email通知
	personally();
}).on('change','#information #fms', function(){			//APP推播ON
	setFCM(($(this).val() == 'on'));
}).on('change','#information #email_notification', function(){
	setEmailOn(($(this).val() == 'on'));
}).on('change', '#screen', function () {					//亮度調整
	screen($(this).val() == 'on');
}).on('click', 'input[type="text"],textarea', function () {
	var target = this;
	setTimeout(function(){
		target.scrollIntoViewIfNeeded();
	},400);
}).on('click', 'a[href="?logout"]', function(){
	logout();
}).on('click', '#del_member', function(){
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-primary',
			// cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false,
	});
	swalWithBootstrapButtons.fire({
		title: '解除開通',
		html: '<div class="orange_title ">'+$(this).data('web')+'</div>'+$(this).data('dex')+'<br><br>'+'您確定要解除您的帳戶?',
		type: 'warning',
		showCancelButton: false,
		confirmButtonText: '確定',
		// cancelButtonText: '取消',
		reverseButtons: true
	}).then((result) => {
		if (result.value) {
			location.href = '/information?del_member&id='+$(this).data('id_houses');
		};
	});
}).on('pageshow', function() {	//頁面顯示再出現
	change_option();
	auto_address();
	menu_buoy();
	search_buoy();
	slide_photo_slider();
	UIkit.update(element = document.body, type = 'update');
}).on('click', '.favorite', function(){
	let on = $(this).hasClass('on');
	add_favorite($(this).data('type'), $(this).data('id'), on);
}).on('click', '.give_star', function(){
	give_star($(this).data('title'), $(this).data('id'), $(this).data('star'));
}).on('click', '.slide_area nav a', function () {
	let $nav = $(this).parent('nav');
	$('a', $nav).removeClass('active');
	$(this).addClass('active');
}).on('click', '.sunmit.list', function () {
	$(this).parents('form').submit();
}).on('click', '.sunmit.map', function () {
	$(this).parents('form').submit();
}).on('click', '.my_give img', function(){
	let id = $(this).data('id');
	$('.my_give[data-id="'+id+'"]').html(give_star_img_htm(id, $(this).data('star')));
});
$.mobile.loadPage(function(){
	// alert('test');
})
