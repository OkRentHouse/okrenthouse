$(document).ready(function () {
	$(".camera_icon").click(function(){
		$(this).parents(".form_item").find("input[type='file']").click();
	});

	$(".submit").eq(0).click(
		function(){
			$(this).parents('.applyform').hide(100);$('.form2').show(200);
		}
	);

	$(".button").click(function(){
		let ckeck = $("#start_form").val();
		if(ckeck !='start'){//確認是否有登入
			alert("您尚未登入或註冊 請先完成該手續!");
		}else{
			$(".applyform").show(100);$(".applyformbk").show(100);$(".applyform.form2").hide()
		}
  });

	$(".applyform").click(function(){
		// console.log("nodeName:"+event.target.nodeName);console.log("class:"+$(event.target).attr('class'));
		if( event.target.nodeName != "INPUT" && event.target.nodeName != "SELECT" && $(event.target).attr('class') != "submit" && $(event.target).attr('class') != "camera_icon" && $(event.target).attr('class').indexOf("photo_upload") < 0 ){
			console.log($(event.target).attr('class'));
			$(".applyform").hide(200);$(".applyformbk").hide(200);$(".applyform.form2").hide()
		}
  });

	$(".applyformbk").click(function(){
		if( event.target.nodeName != "INPUT" && event.target.nodeName != "SELECT" && $(event.target).attr('class') != "submit" && $(event.target).attr('class') != "camera_icon" && $(event.target).attr('class').indexOf("photo_upload") < 0 ){
			console.log($(event.target).attr('class').indexOf("photo_upload"));
			$(".applyform").hide(200);$(".applyformbk").hide(200);$(".applyform.form2").hide()
		}
  });

	$(".post_ID_txt").parent().css("border","0px");
	$(".post_ID_txt").attr("readonly",true);

	$(".massang_from").change(function(){
	$(".massang_from_inp").css("display","none");
	$("#"+$(this).find(":selected").attr("class")).show(300)
	})

	$("#id_county").change(function() {
		let id_county = $("#id_county").val();
		$.ajax({
			url: document.location.pathname,
			method: 'POST',
			data: {
				'ajax': false,
				'action': 'City',
				'id_county' : id_county,
			},
			dataType: 'json',
			success: function (data) {
				if(data["error"] !=""){
					alert(data["error"]);
				}else{
					let html_data = '<option disabled selected><span>鄉鎮區</span></option>';
					data["return"].forEach(function(item, index, arr) {
						html_data +='<option value="'+item['id_city']+'"><span>'+item['city_name']+'</span></option>';
					});
					$("#id_city").empty();
					$("#id_city").html(html_data);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
			}
		});
	});
});

function get_file_img(id){
	let file = document.getElementById(id).files[0];
	console.log(file.type);
	console.log(file);
	if(file.type !='image/png' && file.type !='image/jpeg' ){
		alert("請上傳圖片檔");
		return false;
	}
	var fileReader = new FileReader();
	fileReader.onload = function(event){//讀取完後執行的動作
		document.getElementById(id+'_0').src = event.target.result;
	};
	$("#"+id+'_0').show();
	$("."+id).hide(function(){$(this).parent().find(".statement").show()});
	fileReader.readAsDataURL(file);
}

function isExistDate(dateStr) {
	var dateObj = dateStr.split('-'); // yyyy-mm-dd
	//列出12個月，每月最大日期限制
	var limitInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	var theYear = parseInt(dateObj[0]);
	var theMonth = parseInt(dateObj[1]);
	var theDay = parseInt(dateObj[2]);
	var isLeap = new Date(theYear, 1, 29).getDate() === 29; // 是否為閏年?
	if (isLeap) {
		// 若為閏年，最大日期限制改為 29
		limitInMonth[1] = 29;
	}
	// 比對該日是否超過每個月份最大日期限制
	return theDay <= limitInMonth[theMonth - 1];
}

function submit_data(){
	let is_sumbit ='1';
	let name = $("input[name='name']").val();
	let nickname = $("input[name='nickname']").val();
	let birthday = $("#birthday_y").val()+'-'+$("#birthday_m").val()+'-'+$("#birthday_d").val();
	let phone = $("input[name='phone']").val();
	let line_id = $("input[name='line_id']").val();
	let email = $("input[name='email']").val();
	let postal = '';
	let id_county = $("#id_county").val();
	let id_city = $("#id_city").val();
	let address = $("input[name='address']").val();
	let message_source = $("#message_source").val();
	let identity = $("input[name='identity']").val();
	let bank = $("#bank").val();
	let branch = $("input[name='branch']").val();
	let bank_code =  $("input[name='bank_code']").val();
	let bank_account ='';
	$(".post_ID_n").each(function(index) {
		postal = postal+$(this).val();
	});

	$(".bank_ID_n").each(function(index) {
		bank_account = bank_account+$(this).val();
	});


	if(!$("input[name='agree']").is(":checked")){
		alert("請先同意會員同意條款");
		return;
	}

	if(name=='' || name==undefined || name.length>20){
		alert("姓名不符合格式");
		return;
	}
	if(nickname=='' || nickname==undefined || nickname.length>20){
		alert("暱稱不符合格式");
		return;
	}
	if(!isExistDate(birthday)){
		alert("生日不符合格式");
		return;
	}
	if(phone=='' || phone==undefined || phone.length>20){
		alert("手機號碼不符合格式");
		return;
	}
	if(line_id=='' || line_id==undefined || line_id.length>32){
		alert("LINE ID不符合格式");
		return;
	}
	if(email=='' || email==undefined || email.length>128){
		alert("email不符合格式");
		return;
	}
	if(postal=='' || postal==undefined || postal.length!=5){
		alert("郵遞區號請填寫");
		return;
	}
	if(id_county=='' || id_county==undefined){
		alert("縣市請選擇");
		return;
	}
	if(id_city=='' || id_city==undefined){
		alert("鄉鎮區請選擇");
		return;
	}
	if(address=='' || address==undefined || address.length>128){
		alert("地址未填寫完整或是過長");
		return;
	}
	if(identity=='' || identity==undefined  || identity.length>12 || identity.length<10){
		alert("身分證格式錯誤");
		return;
	}
	if(bank=='' || bank==undefined){
		alert("請選擇或輸入銀行");
		return;
	}
	if(branch=='' || branch==undefined){
		alert("請輸入分行");
		return;
	}
	if(bank_code=='' || bank_code==undefined || bank_code.length>4){
		alert("銀行代碼不符合格式");
		return;
	}
	if(bank_account=='' || bank_account==undefined || bank_account.length!=14){
		alert("存款帳號不符合格式");
		return;
	}

	$("input[name='id_img[]']").each(function(index) {
		if($(this).val()=='' || $(this).val()=='undefined'){
			is_sumbit=0;
			alert("請上傳圖片檔");
			return;
		}
		if(this.files[0].type !='image/png' && this.files[0].type !='image/jpeg'){
			is_sumbit=0;
			alert("請上傳圖片檔");
			return;
		}
	});

	$("input[name='account_img[]']").each(function(index) {
		if($(this).val()=='' || $(this).val()=='undefined'){
			is_sumbit=0;
			alert("請上傳圖片檔");
			return;
		}
		if(this.files[0].type !='image/png' && this.files[0].type !='image/jpeg'){
			is_sumbit=0;
			alert("請上傳圖片檔");
			return;
		}
	});

	if(is_sumbit==1){
		$("#sumbit_data").submit();
	}
}
