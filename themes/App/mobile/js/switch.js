$(document).ready(function(){
  Totel_height=0;
  for(i=0;i<=$(".zone").length;i++){
      Totel_height+=parseInt($(".zone").css("height"));
  }
  $("#search_form").animate({ height: Totel_height + 100 + "px" }, 1000, function() {});
});
//因部分與search_house.js不相容因此拉出來做
var citys_height = 0;
var citys_tottl_height = 0;
var citys_height_adj = -150;
var citys_font_size_0 = "48px";
var citys_font_size_1 = "30px";
var citys_font_size_2 = "20px";
var citys_font_size_3 = "15px";
var get_data = false;

function list_each(use_id, value) {
    if (value == '' || value == "undefined") {
        value == "";
    }
    var height_benchmark = '1';
    $.each($("[id*='" + use_id + "_']"), function(index, c) {
            $(c).css("font-size", citys_font_size_3);
            if (c.getAttribute('value') == value) {
                height_benchmark = index;
                $(c).css({ "font-size": citys_font_size_0, "background": "rgb(0 0 0 / 72%)", "padding": "0px" });
                //兄弟結點上下1.2層顏色不同
                $(c).next().css({ "font-size": citys_font_size_1, "background": "rgb(0 0 0 / 24%)", "padding": "0px" });
                $(c).prev().css({ "font-size": citys_font_size_1, "background": "rgb(0 0 0 / 24%)", "padding": "0px" });
                $(c).next().next().css({ "font-size": citys_font_size_2, "background": "rgb(0 0 0 / 13%)", "padding": "0px" });
                $(c).prev().prev().css({ "font-size": citys_font_size_2, "background": "rgb(0 0 0 / 13%)", "padding": "0px" });
            }
            if (value == '') {
                if (index == 0) {
                    $(c).css({ "font-size": citys_font_size_0, "background": "rgb(0 0 0 / 72%)", "padding": "0px" })
                }
                if (index == 1) {
                    $(c).css({ "font-size": citys_font_size_1, "background": "rgb(0 0 0 / 24%)", "padding": "0px" })
                }
                if (index == 2) {
                    $(c).css({ "font-size": citys_font_size_2, "background": "rgb(0 0 0 / 13%)", "padding": "0px" })
                }
            }
        }
    );

    citys_height = $("." + use_id + "s").eq(1).css("height");
    citys_height = citys_height.slice(0, -2) * 1;
    citys_tottl_height = $(".ui-link-" + use_id).css("height");
    citys_tottl_height = (citys_tottl_height.slice(0, -2) * 1) - (citys_height / 2) + citys_height_adj;

    //捲軸基礎

    if (height_benchmark != '1') {
        $(".ui-link-" + use_id).scrollTop(citys_height * height_benchmark + 2);
    }
    $("." + use_id + "s_0").css("height", ((citys_tottl_height * 1 / 2) + citys_height) + "px");
    $("." + use_id + "s_last").css("height", ((citys_tottl_height * 1 / 2) + citys_height * 2) + "px");
}

function create_list(e_name, data, value) {
    //$(".ui-link-city").html()
    var citys = data;
    if ($("[id*='" + e_name + "_']").length > 0) {
        $(".ui-link-" + e_name + "-selter").empty();
    }

    $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's_0" id="' + e_name + '" href="">&nbsp</a>');
    if (e_name == 'house_choose') {
        $.each(data, function(n, value_value) {
            $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's" id="' + e_name + '_' + value_value["value"] + '" value="' + value_value["value"] + '" onclick="put_data(this)">' + value_value["title"] + '</a>');
        });
    } else if (e_name == 'main_house_class') {
        $.each(data, function(n, value_value) {
            $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's" id="' + e_name + '_' + value_value["id_rent_house_types"] + '" value="' + value_value["id_main_house_class"] + '" onclick="put_data(this)">' + value_value["title"] + '</a>');
        });
    } else if (e_name == 'types') {
        $.each(data, function(n, value) {
            $.each(value, function(n, value_value) {
                $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's" id="' + e_name + '_' + value_value["id_rent_house_types"] + '" value="' + value_value["id_rent_house_types"] + '" onclick="put_data(this)">' + value_value["title"] + '</a>');
            });
        });
    } else if (e_name == 'type') {
        $.each(data, function(n, value) {
            $.each(value, function(n, value_value) {
                $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's" id="' + e_name + '_' + value_value["id_rent_house_type"] + '" value="' + value_value["id_rent_house_type"] + '" onclick="put_data(this)">' + value_value["title"] + '</a>');
            });
        });
    } else if (e_name == 'county') {
        $.each(data, function(n, value_value) {
            $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's" id="' + e_name + '_' + value_value["value"] + '" value="' + value_value["value"] + '" onclick="put_data(this)">' + value_value["title"] + '</a>');
        });
    } else if (e_name == 'city') {
        var county = $("input[name='county']").val();
        $.each(data, function(n, value_value) {
            if (county == value_value["parent"]) {
                $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's" id="' + e_name + '_' + value_value["value"] + '" value="' + value_value["value"] + '" onclick="put_data(this)">' + value_value["title"] + '</a>');
            }
        });
    }
    // data.forEach((c,index) => $(".ui-link-"+e_name+"-selter").append('<a class="'+e_name+'s" id="'+e_name+'_'+index+'" value="'+index+'" onclick="put_data(this)">'+c+'</a>'));
    $(".ui-link-" + e_name + "-selter").append('<a class="' + e_name + 's_last" id="' + e_name + '" href="">&nbsp</a>');
    if (value == '' || value == "undefined") {
        value == '0';
    }
    list_each(e_name, value);
    $("[id='" + e_name + "']").click(
        function() {
            $(".ui-link-" + e_name + "").css("display", "none");
        }
    );
    $("[id*='" + e_name + "_']").click(
        function() {
            $("." + e_name + "s").css("opa" + e_name + "", "0");
            $(".ui-link-" + e_name + "-txt").text($(this).text());
            $(".ui-link-" + e_name).css("display", "block"); //ui-link-zipcode
            $(".ui-link-" + e_name + "_b").css("display", "block");
            $(".ui-link-" + e_name + "").css("display", "none");
        }
    );

    $(".ui-link-" + e_name).css("background", "#424242e8");
    $(".ui-link-" + e_name + "_b").css("background", "");
    $(".ui-link-" + e_name + "_b").css("width", "");
    $(".ui-link-" + e_name + "_b").css("height", "");
    $(".ui-link-" + e_name + "").css("display", "none");
    $(".ui-link-type").css("display", "none");
}; //$.get end
function citys(c, index) {
    var position = $(c).position();
    //$(c).removeClass();
    //$(c).addClass("citys");
    if (position.top >= citys_tottl_height / 2 && position.top <= (citys_tottl_height / 2) + citys_height * 1) { $(c).css({ "font-size": citys_font_size_0, "background": "rgb(0 0 0 / 72%)", "padding": "0px" }); } else if (position.top > (citys_tottl_height / 2) + citys_height * 1 && position.top < citys_tottl_height / 2 + citys_height * 2) { $(c).css({ "font-size": citys_font_size_1, "background": "rgb(0 0 0 / 24%)", "padding": "0px" }) } else if (position.top < citys_tottl_height / 2 && position.top > citys_tottl_height / 2 - citys_height * 1) { $(c).css({ "font-size": citys_font_size_1, "background": "rgb(0 0 0 / 24%)", "padding": "0px" }) } else if (position.top > (citys_tottl_height / 2) + citys_height * 1 && position.top < citys_tottl_height / 2 + citys_height * 3) { $(c).css({ "font-size": citys_font_size_2, "background": "rgb(0 0 0 / 13%)", "padding": "0px" }) } else if (position.top < citys_tottl_height / 2 && position.top > citys_tottl_height / 2 - citys_height * 2) { $(c).css({ "font-size": citys_font_size_2, "background": "rgb(0 0 0 / 13%)", "padding": "0px" }) } else { $(c).css({ "font-size": citys_font_size_3, "background": "", "padding": "0px" }) }
}

function put_data(data) {
    var data_class = data.getAttribute("class");
    var data_value = data.getAttribute("value");
    //丟入input
    if (data_class == 'typess') {
        $("input[name='id_types']").val(data_value);
        get_url();
    } else if (data_class == 'countys') {
        $("input[name='county']").val(data_value);
        city_data();
        get_url();
    } else if (data_class == 'citys') {
        $("input[name='city']").val(data_value);
        var check_data = $(".search_select").find("a")[1];
        $(check_data).html($("input[name='county']").val() + ':' + $("input[name='city']").val());
        get_url();
    } else if (data_class == 'house_chooses') {
        $("input[name='house_choose']").val(data_value);
        get_url();
    }
}

function get_url(){//處理改變超連結值
    console.log("1234567");
    var url_data = "/list?active=1";
    var data_name =['house_choose','id_types','county','city'];
    for(i=0;i<data_name.length;i++){
        if($("input[name='"+data_name[i]+"']").val() !='' && $("input[name='"+data_name[i]+"']").val()!=undefined){
            url_data=url_data+'&'+data_name[i]+'='+$("input[name='"+data_name[i]+"']").val();
        }
    }
    $.each($("a[name='change_name']"), function(index, c) {
        var data_a = $(c).data("a");
        $(c).attr("href",url_data+data_a);
    });
}

function change_img(o) {
  var class_data = $(o).data("class");
  $(".img_w").hide();
    $(".img_w_"+class_data).show();
}