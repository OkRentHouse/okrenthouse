$(document).ready(function() {
  m4q.global(); // $ - now is a m4q constructor
    m4q.noConflict(); // $ - now is a jquery constructor
    $(".search_btn").click( //轉頁
        function() {
            $("#active").val("1");
            $("#search_form").submit();
        });

    $("#min_editer").blur(function() { refresh_price($(this).val(), $("#max_editer").val()); });
    $("#max_editer").blur(function() { refresh_price($("#min_editer").val(), $(this).val()); });
    $(".Scale_txt div").click(function() { refresh_price($("#min_editer").val(), parseInt($(this).text()) * 10000); });
    $(".Scale_txt div:last").click(function() { refresh_price($("#min_editer").val(), 51000); });

    $(".Sroom_txt div").click(function() { refresh_room($("[name='min_room']").val(), parseInt($(this).text()) * 1); });
    $(".Sroom_txt div:last").click(function() { refresh_room($("[name='min_room']").val(), 6); });

    $(".contect_3 ul.buttons,.contect_4 ul.buttons,.contect_7 ul.buttons").show();
    $(".contect_3 ul.buttons,.contect_4 ul.buttons,.contect_7 ul.buttons").css({display: "flex"});

    var p = $( ".contect_5" );
    var position = p.offset();


    var search_form_h = parseInt($("#search_form").css("height")) + parseInt($("#search_box").css("height")) * 4.5 + "px";
    //$("#search_form").animate({ height: position.top+"px" }, 1000, function() {});

    $(".contect_3 ul.buttons,.contect_4 ul.buttons").hide();

    $(".more").click(function() {

        $(this).css({ opacity: 0.6 });

        if (search_form_h == $("#search_form").css("height")) {
            $("#search_form").animate({ height: position.top+"px" }, 1000, function() {
                $(this).find(".more span").text("更多篩選條件");
                //$(".fa-long-arrow-alt-up").attr("class", "fa-long-arrow-alt-down");
                $(".more").css({ opacity: 1 });
            })
        } else {
            $("#search_form").animate({ height: search_form_h }, 30, function() {
                $(this).find(".more span").text("隱藏篩選條件");
                $(this).find(".more span").hide(); //2020.10.16
                $("div#List").scrollTop(1000);
                //$(".fa-long-arrow-alt-down").attr("class", "fa-long-arrow-alt-up");
                $(".more").css({ opacity: 1 });
            })
        }


    });
    var never_chose3=$(".contect_3 .b_div label").text();
    var never_chose4=$(".contect_4 .b_div label").text();
    var never_chose7=$(".contect_7").find("label").text();

    //先給預設值
    function get_selt_defult(){
      // $(".contect_3 .b_div label").text("電梯大樓").append("<i class='fas fa-plus-square'></i>");
      // $(".contect_4 .b_div label").text("整層住家").append("<i class='fas fa-plus-square'></i>");
      $(".contect_3 .b_div label").html("請選擇");
      $(".contect_4 .b_div label").html("請選擇");
      $(".conditions .b_div label,.contect_2 .b_div label,.contect_3 .b_div label,.contect_4 .b_div label").hide();


      $(".contect_7 .b_div label").append("<i class='fas fa-plus-square'></i>");

      $.each($(".contect_3 .b_div .buttons li"),function(){
        if($(this).text()==$(this).parent().parent().find("label").text()){$(this).addClass("buttons_checkes")}
      });

      $.each($(".contect_4 .b_div .buttons li"),function(){
        if($(this).text()==$(this).parent().parent().find("label").text()){$(this).addClass("buttons_checkes")}
      });

    }

    get_selt_defult();
    never_chose = "";
    $(".contect_0 .conditions .buttons li,.contect_2 .conditions .buttons li,.contect_3 .conditions .buttons li,.contect_4 .conditions .buttons li,.contect_7 .conditions .buttons li").click(function(){

      // if( $(this).parent().parent().parent().parent().prop("class")=="contect_3"){
      if( $(this).parents(".contect_3").length == 1){
        never_chose = never_chose3;
        console.log(never_chose);
      }else if( $(this).parents(".contect_7").length > 0){
        never_chose = never_chose7;
        console.log(never_chose);
      }
      else{
        never_chose = never_chose4;
        console.log(never_chose);
      }

      var lab = $(this).parents(".conditions").find("label");
      if($(this).parents(".contect_7").length > 0){
        lab = $(this).parents(".contect_7").find("label");
      }
      var txt = $(lab).text();
      //console.log("txt"+txt);
      //console.log("this txt"+$(this).text());
      //console.log( txt.indexOf($(this).text()) );



      if(txt.indexOf($(this).text())>=0){
        txt = txt.replace(","+$(this).text(),"");
        txt = txt.replace($(this).text(),"");
        //console.log( "replace" );
      }else{
        txt = txt + "," + $(this).text();
        //console.log( "+" );
      }

      txt = txt.replace("請選擇,","");


      //$(lab).text(txt);
      //檢檢查按鈕是不是被 checked
      if($(this).hasClass("buttons_checkes")){
        //console.log("lab txt"+$(lab).text());

      if( $(lab).text().length <= 1 ){
        $(lab).text(never_chose);
        $(lab).hide();
      }else{
        //txt = txt.replace(never_chose+",","");
        $(lab).text(txt)
        $(lab).show();
      }}

    });

    $(".contect_7 ul.head,.contect_7 .b_div label").click(function(){
        if($(this).parent().find(".buttons").css("display")=="flex"){
            $(this).parent().find(".buttons").hide(500);
            $(this).parent().parent().find("svg:not(.fa-plus-square)").attr("data-icon","caret-down")
        }else{
            $(this).parent().find(".buttons").show(200,function(){
                $(this).parent().find(".buttons").css("display","flex");
            })
            $(this).parent().parent().find("svg:not(.fa-plus-square)").attr("data-icon","caret-up")
        }
    })

    $(".ui-field-contain").css("height", parseInt($(this).find(".ui-radio").eq(0).css("height")) * 1.2);
    $(".ui-field-contain").scrollTop(parseInt($(this).find(".ui-radio").eq(0).css("height")) / 2);
    $(".ui-field-contain").scroll(function() {

        var p = $(this);
        var position = p.position();
        for (i = 0; i < $(this).find(".ui-radio").length; i++) {
            p = $(this).find(".ui-radio").eq(i);
            position = p.position();
            //console.log($(this).scrollTop() + "(" + i + ")>" + position.top);

            if ($(this).scrollTop() > position.top) {
                $(this).find(":radio").attr("checked", false);
                $(this).find("label").removeClass("ui-radio-on");
                $(this).find(":radio").eq(i).attr("checked", true);
                $(this).find("label").eq(i).addClass("ui-radio-on");
                //console.log(i + ":checked");
            }
        }
    })

    $("body").append("<div class='v_no''>version:20.4.001</div>");


    $(document).on("click",".close_window,.select_box_bk",function(){
      close_window_select_box_bk($(this))}
    );
    //$(document).on("click","#types_label,#type_label,.contect_7 label,.buttons_c",function(){types_label_c($(this))});
    $(document).on("click",".contect_0 .conditions .head,.contect_2 .conditions .head,.contect_3 .conditions .head,.contect_4 .conditions .head",function(){types_label_c($(this))});


    //label#types_label



});

var contect = "";

function types_label_c(e){

  var finds = ".select_box,.select_box ul";
  contect = ".contect_1_1";
  if( $(e).prop("class") == "buttons_checkes" || $(e).prop("class") == "buttons_c" || ($(e).prop("class")?$(e).prop("class"):$(e).prop("id")?$(e).prop("id"):$(e).prop("tagName"))==undefined){
    if($(e).prop("class") == "buttons_checkes" && $(".buttons_checkes").parents(".contect_1_1").length > 0){
      contect = ".contect_1_2";
    }
    finds = ".select_box,.select_box ul,"+contect;
    e=$(contect).find(".head");
    $(contect).show(200,function(){
      $(this).css({"height": "0px"});$(this).find(".conditions").css({"border-bottom": "0px"});

    });
  }

  // $(e).parent().find(finds).show(200,
  //   function() {
  //     $(e).parent().parent().find(".select_box_bk").show();
  //     $(e).parent().parent().find(".select_box_bk").animate({"opacity": 1},100, function() {})
  //   }
  // )
  var e_height = $(e).parent().find(finds).height();
  $(e).parent().find(finds).show(0,function(){e_height = $(e).parent().find(finds).height()==0?$(window).height()*0.4:$(e).parent().find(finds).height()+50; $(e).parent().find(".select_box").css({"height" : "0px"}) });
  $('.select_box_bk').stop(true, false);
  if($(e).parents(".contect_1").length > 0 ){
    $(".contect_1_1").find(".select_box_bk").show();
    $(".contect_1_1").find(".select_box_bk").animate({"opacity": 1},100, function() {})
  }else{
    $(e).parents(".conditions").find(".select_box_bk").show();
    $(e).parents(".conditions").find(".select_box_bk").animate({"opacity": 1},100, function() {})
  }



  m4q.animate( $(e).parent().find(finds) , {height: e_height}, 300, "easeOutCirc", function(){
        $(e).parent().find(finds).css({"height" : ""});

    });


}

function close_window_select_box_bk(e){

  class_name=$(e).attr("class");
  console.log( ($(e).prop("class")?$(e).prop("class"):$(e).prop("id")?$(e).prop("id"):$(e).prop("tagName"))+":"+(($(e).prop("class")?$(e).prop("class"):$(e).prop("id")?$(e).prop("id"):$(e).prop("tagName"))==undefined) );
  contect = ".contect_1_2";
  if($(e).prop("class") == "buttons_checkes"){
    if($(".buttons_checkes").parents(".contect_1_2").length == 0){
      contect = ".contect_1_1";
    }
    e=$(contect).find(".head");
  }

  if($(e).parents(".select_box").length > 0){
    e=$(e).parents(".select_box");console.log("parents")
  }else{
    e=$(e).parents("[class*='contect_']").find(".select_box");console.log("find:"+$(e).parents("[class*='contect_']").prop("class"))
  }
  $(e).hide(200,function(){
    e=$(e).parents("[class*='contect_']").find(".select_box_bk");
    $(e).stop(true, false);
    $(e).animate({"opacity": 0},50, function() {
      //if( class_name.indexOf("close_window") < 0 ){$(e).hide()}
      $(e).hide();
      $(e).parents("[class*='contect_']").find("svg:not(.fa-plus-square)").attr("data-icon","caret-down")
      })
  })
}

function check_label_type(o){
    if($("#type_label").parent().find(".buttons").css("display")=="flex"){
        $("#type_label").parent().find(".buttons").hide(500);
        $("#type_label").parent().parent().find("svg:not(.fa-plus-square)").attr("data-icon","caret-down")
    }else{
        $("#type_label").parent().find(".buttons").show(200,function(){
            $("#type_label").parent().find(".buttons").css("display","flex");
        })
        $("#type_label").parent().parent().find("svg:not(.fa-plus-square)").attr("data-icon","caret-up")
    }
}

function check_label_types(o){
    console.log("check_label_types-"+$(o).prop("tagName"))
    if($("#types_label").parent().find(".buttons").css("display")=="flex"){
        $("#types_label").parent().find(".buttons").hide(500);
        $("#types_label").parent().parent().find("svg:not(.fa-plus-square)").attr("data-icon","caret-down")
    }else{
        $("#types_label").parent().find(".buttons").show(200,function(){
            $("#types_label").parent().find(".buttons").css("display","flex");
        })
        $("#types_label").parent().parent().find("svg:not(.fa-plus-square)").attr("data-icon","caret-up")
    }
}

function refresh_price(min, max) {
    $('#price').data('doubleslider').val(min, max);
}

function refresh_room(min, max) {
    $('#room').data('doubleslider').val(min, max);
    //$(".contect_5 .hint.hint-max.top-side").text("5")
    if (max > 5) {
        $(".contect_5 .hint.hint-max.top-side").text("5房以上");
    }
}

function click_li(data) {

    var data_name = data.getAttribute("name");
    var data_value = data.getAttribute("value");
    var input_from = data_name.slice(0, -4);
    var data_class = data.getAttribute("class");
    if (data_class == null) {
        $(data).addClass("buttons_checkes");
    } else if (data_class.indexOf("buttons_checkes") == -1) {
        $(data).addClass("buttons_checkes");
    } else {
        $(data).removeClass("buttons_checkes");
    }

    var data_val = '';
    var data_html = '';
    $($("li[name='" + data_name + "']")).each(function(index) {
        if ($("li[name='" + data_name + "']")[index].getAttribute('class') == null) {} else if ($("li[name='" + data_name + "']")[index].getAttribute('class').search("buttons_checkes") != -1) {
            if (data_val == '') {
                data_val = $("li[name='" + data_name + "']")[index].getAttribute("value");
                data_html =  $("li[name='" + data_name + "']")[index].innerHTML;
            } else {
                data_val += ',' + $("li[name='" + data_name + "']")[index].getAttribute("value");
                data_html += ',' + $("li[name='" + data_name + "']")[index].innerHTML;
            }
        }
    });

    if(input_from=='id_types' || input_from=='id_type'){
        if(data_html=="" || data_html=="undefined"){//未選擇
            if(input_from=='id_types') $("#types_label").html("請選擇");
            if(input_from=='id_type') $("#type_label").html("請選擇");
        }else if(data_html!="" && data_html!="undefined"){//未選擇
            if(input_from=='id_types') $("#types_label").html(data_html);
            if(input_from=='id_type') $("#type_label").html(data_html);
        }
    }

    $("input[name='" + input_from + "']").val(data_val);
    if (input_from == 'city') { //city特有
        var data_id = data.getAttribute("id");
        $("#citys_txt").html($("#" + data_id).data("parent") + ':' + data_val);
    }

    if(data_name == 'id_main_house_class_a[]'){
        call_reset_main_data();
    }
}

function slider_get(data) {
    var data_id = data.getAttribute("id");
    $arr_val = $("#" + data_id).val().split(',');
    $("input[name='min_" + data_id + "']").val($arr_val[0].trim());
    $("input[name='max_" + data_id + "']").val($arr_val[1].trim());
}

function slider_transfor_room(data) {
    v = $(data).val().split(',');
    $("#room_min_editer").val(v[0]);
    if (parseInt(v[0]) == 0) { $("#room_min_editer").val(' Open ');$("#room_min_editer").parents(".b_div_editer").find("span").eq(0).text(" ~ ") }
    if (parseInt(v[1]) > 5) { v[1] = ' 5 ';$("#room_max_editer").parents(".b_div_editer").find("span").eq(1).text("房以上") }
    else{ $("#room_max_editer").parents(".b_div_editer").find("span").eq(1).text("房") };
    $("#room_max_editer").val(v[1]);
}

function slider_transfor_ping(data) {
    v = $(data).val().split(',');
    $("#ping_min_editer").val(v[0]);
    $("#ping_max_editer").val(v[1]);
}

function slider_transfor(data) {
    v = $(data).val().split(',');
    if ($.trim(ReplaceAll(v[0], ',', '')).length > 3) {
        v[0] = v[0].slice(0, v[0].length - 3) + ',' + v[0].substr(-3);
    }

    $("#min_editer").val(v[0]);
    if ($.trim(ReplaceAll(v[1], ',', '')).length > 3) {
        v[1] = v[1].slice(0, v[1].length - 3) + ',' + v[1].substr(-3);
    }

    if (parseInt(ReplaceAll(v[1], ',', '')) > 50000) { v[1] = ' 50,000 以上' };
    $("#max_editer").val(v[1]);
}
