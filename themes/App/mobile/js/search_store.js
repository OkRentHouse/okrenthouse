function store_search_click(type, $this) { 
	var $store_search = $this.parents('.store_search');
	if (type == 'map') {
		$store_search.attr('action', 'StoreMap');
	} else {
		$store_search.attr('action', 'SearchStore');
	}
	$store_search.submit();
}
function search_store_list(data){
	$.each(data, function(){
		var id_store = this.id_store;
		var html = '<div class="item" data-id="'+this.id_store+'">' +
			'<div class="img"><a href="/Store?id='+this.id_store+'"><img src="'+this.img+'"></a><div class="favorite '+this.favorite+'" data-id="'+this.id_store+'" data-type="store">';

		if(this.favorite == 'on'){
			html += '<img src="'+THEME_URL+'/img/icon/heart_on.svg">';
		}else{
			html += '<img src="'+THEME_URL+'/img/icon/heart.svg">';
		}
		html += '</div>' +
			'</div>' +
			'<div class="main_title">' +
			'<div class="title">'+this.title+'</div>' +
			'<div class="career">'+this.store_type+'</div>' +
			'<div class="popularity div_popularity give_star" data-title="'+this.title+'" data-id="'+this.id_store+'">'+this.popularity_img+'</div>' +
			'</div>';
		if(this.service.length > 0){
			html += '<div class="hot"><div class="subtitle"><i><img src="'+THEME_URL+'/img/hot.svg"></i>HOT</div><div>';
			$.each(this.service, function(){
				html += '<samp>'+this+'</samp>';
			});
			html += '</div></div>';
		}

		if(this.discount.length > 0){
			html += '<div class="offer"><div class="subtitle">OFFER</div>';
			$.each(this.discount, function(){
				html += '<a href="/Store?id='+id_store+'">'+this+'</a>';
			});
			html += '</div>';
		};
		html += '</div>';
		$('.store_list', '#search_store').append(html);
	});
};
let m = 10;
var p = 2;
var max_p = 9999999999;
var OnShowListAjax = false;
$(document).on('click', '.store_search .pic_btn', function () {
	store_search_click('list', $(this));
}).on('click', '.store_search .map_btn', function () {
	store_search_click('map', $(this));
}).on('pageshow', function () {	//頁面顯示再出現
	$('#search_store').on('scroll', function () {	//捲動載入
		var all_h = $('#SearchStore').height();
		var win_h = $(window).height();
		var scroll_top = $('#search_store').scrollTop();
		if(!OnShowListAjax
			&& scroll_top > (all_h - (2 * win_h))
			&& p < max_p){
			$.ajax({
				url: '/SearchStore',
				method: 'POST',
				data: {
					'ajax': true,
					'action': 'ShowList',
					'county': $('.county', '.store_search').data('v'),
					'area': $('.area', '.store_search').data('v'),
					'career': $('.career', '.store_search').data('v'),
					'min': '',
					'p': p,
					'm': m,
				},
				dataType: 'json',
				success: function (data) {
					if (data) {
						p = data.p;
						max_p = data.max;
						search_store_list(data.data)
						if(p == max_p){
							$('.store_list', '#search_store').append('<div class="text-center">已經是最底了!</div>');
						};
					};
				},
				beforeSend: function () {
					OnShowListAjax = true;
					$('.store_list_loading').show();
				},
				complete: function () {
					OnShowListAjax = false;
					$('.store_list_loading').hide();
				},
				error: function (xhr, ajaxOptions, thrownError) {

				},
			});
		};
	});
});