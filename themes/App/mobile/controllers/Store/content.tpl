<div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: {$store.ratio}">
	<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
		<ul class="uk-slideshow-items">
            {foreach $store.img as $v => $img}
				<li><img src="{$img}">{$v}</li>
			{foreachelse}
				<li><img src="{$THEME_URL}/img/default.jpg"></li>
            {/foreach}
		</ul>
		<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
			  uk-slideshow-item="previous"></samp>
		<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
			  uk-slideshow-item="next"></samp>
	</div>
	<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
	<div class="favorite {if $store.favorite}on{/if}" data-id="{$store.id_store}" data-type="store">{if $store.favorite}<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}</div>
{*	<div class="favorite {$store.favorite}" data-id="{$store.id_store}" data-type="store"><img src="{$THEME_URL}/img/icon/heart.svg"></div>*}
</div>
<div class="store_content">
	<div class="title">{$store.title}</div>
	<div class="career">{$store.store_type}</div>
    {foreach $store.address1 as $i => $address}
	<div class="add"><label>{if $i == 0}<i><img src="{$THEME_URL}/img/map_tag.svg"></i>ADD{else}　{/if}</label>
		<div>{$address}</div>
	</div>
    {/foreach}
    {foreach $store.tel as $i => $tel}
		<div class="tel"><label>{if $i == 0}<i><img src="{$THEME_URL}/img/phone.svg"></i>TEL{else}　{/if}</label>
			<div>{$tel}</div>
		</div>
	{/foreach}
    {foreach $store.time as $i => $time}
	<div class="time"><label>{if $i == 0}<i><img src="{$THEME_URL}/img/time.svg"></i>TIME{else}　{/if}</label>
{*		<div><div class="time_1">{$time[0]}</div><div class="time_2">{$time[1]}</div></div>*}
		<div><div>{$time}</div></div>
	</div>
    {/foreach}
	<div class="hot"><label><i><img src="{$THEME_URL}/img/hot.svg"></i>HOT</label>
		<div>{foreach $store.service as $i => $service}<samp>{$service}</samp><samp>{$service}</samp>{/foreach}</div>
	</div>
    {foreach $store.discount as $i => $discount}
	<div class="offer"><label>{if $i == 0}OFFER{else}　{/if}</label>
		<div><a>{$discount}</a></div>
	</div>
    {/foreach}
	<div class="popularity"><label>人氣星等</label><div class="div_popularity" data-id="{$store.id_store}">{$store.popularity}</div></div>
	{if !empty($smarty.session.id_member)}
	<div class="star"><label><i><img src="{$THEME_URL}/img/star.svg"></i>給星星</label><div class="div_star give_star" data-title="{$store.title}" data-id="{$store.id_store}" data-star="{$store.star}">{$store.star_img}</div></div>
	{/if}
</div>