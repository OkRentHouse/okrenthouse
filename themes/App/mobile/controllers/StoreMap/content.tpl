{include file="$tpl_dir./mobile/coupon/min_menu.tpl"}
<div class="document">
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">找好康</div>
	</div>
    {$search_store}
	<div id="map"></div>
	<script type="text/javascript">
		var map;
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
				center: {
					lat: {$google_map.lat},
					lng: {$google_map.lng},
				},
				zoom: {$google_map.zoom},
				minZoom:{$google_map.minZoom},
				maxZoom:{$google_map.maxZoom},
				streetViewControl:false,	//顯示街景
				fullscreenControl:true,		//全螢幕地圖
				zoomControl:false,			//放大縮小地圖
				mapTypeControl:false		//地圖與衛星類型
			});
		}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key={$google_key}&callback=initMap&libraries=geometry&sensor=false">
	</script>
</div>