{include file="$tpl_dir./mobile/menu_buoy.tpl"}
{include file="$tpl_dir./mobile/breadcrumbs.tpl"}
<div class="data_wrap">
    <div class="result"></div>
    <div class="commodity slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1" >
        <div class="photo_row">
            <a href="#" class="ui-link">
                <img src="{if $img[0]}{$img[0]}{else}https://www.lifegroup.house/uploads/comm_soon.jpg{/if}" class="img" alt="" />
                <collect data-id="{$arr_house.id_rent_house}" data-type="rent_house" class="favorite{if $arr_house.favorite} on{/if}">{if $arr_house.favorite}
                        <img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}
                </collect>
            </a>

            <div>
                <data class="rows_content">
                    <div class="rows_content_1">
                        <h3 class="title big_2"><a href="RentHouse.php?id_rent_house={$arr_house.id_rent_house}" tabindex="-1" class="ui-link">{$arr_house.rent_house_title}</a></h3>
                        <div class="title_2"><img src="https://www.okrent.house/themes/Rent/img/icon/icons8-address-50.png" />{$arr_house.rent_address}</div>
                        <div class="title_4">
                            <ul>
                                {if ($arr_house.t3=='' || $arr_house.t3=='0') && ($arr_house.t4=='' || $arr_house.t4=='0')}<li>開放空間</li>{/if}
                                    {if $arr_house.t3=='' || $arr_house.t3=='0'}{else}<li>{$arr_house.t3}房</li>{/if}
                                    {if $arr_house.t4=='' || $arr_house.t4=='0'}{else}<li>{$arr_house.t4}廳</li>{/if}
                                    {if $arr_house.t5=='' || $arr_house.t5=='0'}{else}<li>{$arr_house.t5}衛</li>{/if}
                                    {if $arr_house.t6=='' || $arr_house.t6=='0'}{else}<li>{$arr_house.t6}室</li>{/if}
                                    {if $arr_house.kitchen=='' || $arr_house.kitchen=='0'}{else}<li>{$arr_house.kitchen}廚</li>{/if}
                                    {if $arr_house.t7=='' || $arr_house.t7=='0'}{else}<li>{$arr_house.t7}前陽台</li>{/if}
                                    {if $arr_house.t8=='' || $arr_house.t8=='0'}{else}<li>{$arr_house.t8}後陽台</li>{/if}
                                    {if $arr_house.t9=='' || $arr_house.t9=='0'}{else}<li>{$arr_house.t9}房間陽台</li>{/if}
                                <li>{$arr_house.ping}坪</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rows_content_2">
                        <h3 class="title big"><a href="RentHouse.php?id_rent_house=15543" tabindex="-1" class="ui-link">{$arr_house.case_name}</a></h3>

                        <price style="position: absolute; top: 44px; right: 0px; padding-right: 5px;">
                            10000
                            <unit>元</unit>
                        </price>
                    </div>
                </data>
            </div>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F">
            <ul class="Fr">
                <li>租金含</li>
            </ul>
            <ul class="La">
                <li>
                    {if $arr_house.cleaning_fee_type==1}<span>管理費</span>{/if}
                    {if $arr_house.park_fee_type==1}<span>停車費</span>{/if}
                    {if $arr_house.pay_tv==2}<span>有線電視</span>{/if}
                    {if $arr_house.water_fee_type==1}<span>水費</span>{/if}
                    {if $arr_house.electricity_fee_type==1}<span>電費</span>{/if}
                    {if $arr_house.gas_fee_type==1}<span>瓦斯</span>{/if}
                </li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>房屋資訊</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <li>案號</li>
                <li>樓層</li>
                <li>汽車位</li>
                <li>座向</li>
                <li>坪數</li>
                <li> &nbsp; </li>
　
            </ul>
            <ul class="La">
                <li><span>{$arr_house.rent_house_code}</span></li>
                <li><span>{if $arr_house.whole_building=='1'}整棟建築{else}樓層 {$arr_house.rental_floor} F/ {$arr_house.floor} F{/if}</span></li>
                <li><span>{if $arr_house.motorcycle_have==1}有{else}無{/if}
                        {foreach $arr_house.cars.cars_park_position as $k => $v}
                            {if $arr_house.cars.cars_rent.$k!=''}{if $arr_house.cars.cars_rent_type.$k=='含管理費'}({$arr_house.cars.cars_rent_type.$k}){else}(每{$arr_house.cars.cars_rent_type.$k}{$arr_house.cars.cars_rent.$k}元){/if}{/if}
                        {/foreach}</span></li>
                <li><span>{$arr_house.house_door_seat}</span></li>
                <li><span>權狀 {if !empty($arr_house.ping) && $arr_house.ping!='0.00'}{$arr_house.ping}坪{else}暫不提供{/if}</span></li>
                <li class="pos"><span>主建 {if !empty($arr_house.main_construction_ping) && $arr_house.main_construction_ping!='0.00'}{$arr_house.main_construction_ping}坪{else}暫不提供{/if}</span>
                    <span>附屬 {if !empty($arr_house.accessory_ping) && $arr_house.accessory_ping!='0.00'}{$arr_house.accessory_ping}坪{else}暫不提供{/if}</span>
                    <span>公設 {if !empty($arr_house.axiom_ping) && $arr_house.axiom_ping!='0.00'}{$arr_house.axiom_ping}坪{else}暫不提供{/if}</span></li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr">
                <li>屋齡</li>
                <li>車位</li>
                <li>隔間</li>
                <li>機車位</li>
                <li>位置</li>
                <li> </li>
                <li> </li>
            </ul>
            <ul class="La">
                <li><span>{if $arr_house.house_old}{$arr_house.house_old}年{else}暫不提供{/if}</span></li>
                <li><span>{if !empty($arr_house.car_space_ping) && $arr_house.car_space_ping!='0.00'}{$arr_house.car_space_ping}坪{else}暫不提供{/if}</span></li>
                <li><span>{if $arr_house.genre}{$arr_house.genre}{else}未隔間{/if}</span></li>
                <li><span>{if $arr_house.motorcycle_have==1}有{else}無{/if}</span></li>
                <li><img src="/themes/App/mobile/img/RentHouse/go_map.png"></li>
                <li> </li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>社區公設</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                {if $arr_house.public_utilities["3"]}
                {$count_arr=ceil(count($arr_house.public_utilities["3"])/4)-1}
                    {for $foo=0 to $count_arr}
                        {if $foo==0}<li>管理</li>{else}<li> &nbsp; </li>{/if}
                    {/for}
                 {else}
                    <li>管理</li>
                {/if}

                <li>管理費</li>
                {if $arr_house.public_utilities["1"]}
                {$count_arr=ceil(count($arr_house.public_utilities["1"])/4)-1}
                    {for $foo=0 to $count_arr}
                        {if $foo==0}<li>休閒公設</li>{else}<li> &nbsp; </li>{/if}
                    {/for}
                {else}
                    <li>休閒公設</li>
                {/if}
            </ul>
            <ul class="La">
                {if !empty($arr_house.public_utilities["3"])}
                    {$count_arr=count($arr_house.public_utilities["3"])-1}
                    {foreach $arr_house.public_utilities["3"] as $k => $v}
                        {if $k%4==0}<li class="pos">{/if}
                            <span>{$v}</span>
                        {if $k%4==3 || $k==$count_arr}</li><li> &nbsp; </li>{/if}
                    {/foreach}
                {else}
                    <li><span>無</span></li>
                {/if}
                <li><span>{if s1==1}含管理費{else}{$arr_house.cleaning_fee}/{$arr_house.cleaning_fee_cycle}{if $arr_house.cleaning_fee_unit}({$arr_house.cleaning_fee_unit}){/if}{/if}</span></li>
                {if !empty($arr_house.public_utilities["1"])}
                    {$count_arr=count($arr_house.public_utilities["1"])-1}
                    {foreach $arr_house.public_utilities["1"] as $k => $v}
                        {if $k%4==0}<li class="pos">{/if}
                        <span>{$v}</span>
                        {if $k%4==3 || $k==$count_arr}</li><li> &nbsp; </li>{/if}
                    {/foreach}
                {else}
                    <li><span>無</span></li>
                {/if}
            </ul>
{*            <ul class="La">*}
{*                *}
{*            </ul>*}
{*            *}


{*            <ul class="Fr">*}
{*                <li>管理</li>*}
{*                <li>管理費</li>*}
{*                <li>休閒公設</li>*}
{*                <li></li>*}
{*            </ul>*}
{*            <ul class="La">*}
{*                <li class="pos"><span>物業保全</span><span>監視系統</span><span>門禁刷卡</span></li>*}
{*                <li> &nbsp; </li>*}
{*                <li><span>1966/月</span></li>*}
{*                <li class="pos"><span>電梯</span><span>公用陽台</span><span>接待大廳</span><span>游泳池</span></li>*}
{*            </ul>*}



        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>設備提供</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                {foreach $arr_house.device_category_data as $k => $v}
                    {$count_arr=ceil(count($v.device_category_data)/4)-1}
                        {for $foo=0 to $count_arr}
                            {if $foo==0}<li>{$v.title}</li>{else}<li> &nbsp; </li>{/if}
                        {/for}
                {/foreach}
                <li></li>
            </ul>
            <ul class="La">
                {foreach $arr_house.device_category_data as $k => $v}
                    {$count_arr=ceil(count($v.device_category_data))-1}
                    {foreach $v.device_category_data as $k_ch => $v_ch}
                        {if $k_ch%4==0}<li class="pos">{/if}
                        <span>{$v_ch.title}</span>
                        {if $k_ch%4==3 || $k_ch==$count_arr}</li><li> &nbsp; </li>{/if}
                    {/foreach}
                {/foreach}
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>出租條件</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <li>押金</li>
                <li>性别要求</li>
                <li>最短租期</li>
                <li>飼養寵物</li>
                <li>身分限制</li>
            </ul>
            <ul class="La">
                <li><span>{if !empty($arr_house.deposit)}{$arr_house.deposit}個月押金{else}無押金{/if}</span></li>
                <li><span>{if empty($arr_house.other_conditions["26"]) && empty($arr_house.other_conditions["27"])}
                     不限{else}{$arr_house.other_conditions["26"]} {$arr_house.other_conditions["27"]}{/if}</span></li>

                <li><span>{if !empty($arr_house.m_s) && !empty($arr_house.m_e)}{$arr_house.m_s}個月~{$arr_house.m_e}個月
                         {elseif !empty($arr_house.m_s)}{$arr_house.m_s}個月
                         {elseif !empty($arr_house.m_e)}{$arr_house.m_e}個月
                         {else}不可{/if}</span></li>
                <li><span>{if $arr_house.other_conditions["5"]}可{else}不可{/if}</span></li>
                <li class="pos"><span>限家庭/夫妻 </span><span> 拒合租/幼童</span></li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr">
                <li>入住時間</li>
                <li>與房東同住</li>
                <li>租約公證</li>
                <li>神龕拜拜</li>
            </ul>
            <ul class="La">
                <li><span>{if !empty($arr_house.housing_time_s) && !empty($arr_house.housing_time_e)}{$arr_house.housing_time_s}~{$arr_house.housing_time_e}
                         {elseif !empty($arr_house.housing_time_s) && empty($arr_house.housing_time_e)}{$arr_house.housing_time_s}之後
                         {elseif empty($arr_house.housing_time_s) && !empty($arr_house.housing_time_e)}{$arr_house.housing_time_e}之前
                         {else}隨時{/if}</span></li>
                <li><span>{if $arr_house.other_conditions["25"]}是{else}否{/if}</span></li>
                <li><span>{if $arr_house.other_conditions["24"]}是{else}否{/if}</span></li>
                <li><span>{if $arr_house.other_conditions["8"]}可設置{else}不可{/if}</span></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>生活機能</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_L">
            <ul class="Fr">
                <li>學區</li>
                <li>購物</li>
                <li>交通</li>
                <li>醫院</li>
            </ul>
            <ul class="La">
                <li class="pos">{if !empty($arr_house.e_school) && !empty($arr_house.j_school)}<span>{$arr_house.e_school}</span><span>{$arr_house.j_school}</span>
                        {elseif !empty($arr_house.e_school)}<span>{$arr_house.j_school}</span>
                        {elseif empty($arr_house.e_school)}<span>{$arr_house.j_school}</span>
                        {else}<span>無</span>{/if}</li>
                <li> &nbsp; </li>
                <li class="pos">
                    {if !empty($arr_house.supermarket) || !empty($arr_house.shopping_center) || !empty($arr_house.market) || !empty($arr_house.night_market)}
                        {if $arr_house.supermarket}<span>超商</span>{/if}
                        {if $arr_house.shopping_center}<span>購物中心/百貨公司</span>{/if}
                        {if $arr_house.market}<span>傳統市場</span>{/if}
                        {if $arr_house.night_market}<span>夜市</span>{/if}
                    {else}未提供{/if}</li>
                <li> &nbsp; </li>
                <li class="pos">
                    {if !empty($arr_house.bus) || !empty($arr_house.passenger_transport) || !empty($arr_house.train) || !empty($arr_house.mrt) || !empty($arr_house.high_speed_rail) || !empty($arr_house.interchange)}
                        {if $arr_house.bus}<span>公車</span>{/if}
                        {if $arr_house.passenger_transport}<span>客運</span>{/if}
                        {if $arr_house.night_market}<span>夜市</span>{/if}
                        {if $arr_house.mrt}<span>捷運</span>{/if}
                        {if $arr_house.high_speed_rail}<span>高鐵</span>{/if}
                        {if $arr_house.interchange}<span>交流道</span>{/if}
                    {else}未提供{/if}</li>
                <li> &nbsp; </li>
                <li class="pos">{if $arr_house.hospital}<span>{$arr_house.hospital}</span>{/if}</li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F contect_0_title">
            <ul class="Fr">
                <li>特色介紹</li>
            </ul>
            <ul class="La">
                <li></li>
            </ul>
        </div>
    </div>

    <div class="contect_0">
        <div class="contect_0_F">
            <ul class="Fr">
                <li></li>
            </ul>
            <ul class="La">
                {$arr_house.features}　
            </ul>
        </div>
    </div>
    <hr>
    <div class="contect_map_txt">
        <div class="contect_0_L" style="">
            <ul class="Fr" style="">
                <li>聯絡我</li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr" style="">
                <li>留言</li>
            </ul>
        </div>
        <div class="contect_0_L" style="">
            <ul class="Fr" style="">
                <li>預約賞屋</li>
            </ul>
        </div>
        <div class="contect_0_R">
            <ul class="Fr" style="">
                <li>呼叫顧問</li>
            </ul>
        </div>
    </div>
</div>
