{include file="$tpl_dir./mobile/coupon/min_menu.tpl"}
<div class="document">
    {if count($arr_content_benner)}
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true;ratio: {$content_ratio}">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
                    {foreach $arr_content_benner as $v => $banner}
						<li><a href="{$banner.href}"><img src="{$banner.img}"></a></li>
                    {/foreach}
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
    {/if}
</div>
<div class="title_banner s">
	<div class="separate"></div>
	<div class="slide_title">給好康</div>
	<div class="slide_title2">店家募集</div>
</div>{if !empty($give_1_img)}
<img src="{$give_1_img}">{/if}
<div class="text_area">
	<p>好康店家  免費廣告曝光</p>
	<p>結合好康行銷活動 吸引客群</p>
	<p> 廣為告知 增加曝光</p>
	<p>結市效應 提升業績</p>
{*	<p> 想要讓更多朋友認識你們嗎？！</p>*}
{*	<p> 歡迎具誠信及健康、安全原則之優質店家申請加入，</p>*}
{*	<p> 提供生活集團ＶＩＰ會員享有各類商品或服務，</p>*}
{*	<p> 生活集團免費幫商家推薦宣傳，</p>*}
{*	<p> 所有優惠活動由商家自行規範!</p>*}
</div>
<div class="join">
	<a class="join ui-link" href="https://app.life-house.com.tw/add_store">立即加入</a>
</div>
<br>
<br>
{*<div class="recognition_wrap">*}
{*	<div class="label">*}
{*		<img src="{$THEME_URL}/img/coupon/recognition.png">*}
{*	</div>*}
{*	<div class="">*}
{*		<p> 審核通過之店家</p>*}
{*		<p> 會獲得『生活好康』特約商店</p>*}
{*		<p> 識別標章靜電貼乙枚</p>*}
{*		<p> 以便會員消費識別</p>*}
{*	</div>*}
{*</div>*}
{if !empty($give_2_img)}<img src="{$give_2_img}">{/if}