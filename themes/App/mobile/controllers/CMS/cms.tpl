<div class="cms cms_{$cms_url}">{$html}</div>
{if !empty($cms_css)}
	<style>
		{$cms_css}
	</style>
{/if}
{if !empty($cms_js)}
	<script>
        {$cms_js}
	</script>
{/if}