{include file="$tpl_dir./mobile/coupon/min_menu.tpl"}
<div class="document">
    {if count($arr_content_banner)}
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true;ratio: {$content_ratio}">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
                    {foreach $arr_content_banner as $v => $banner}
						<li><a href="{$banner.href}"><img src="{$banner.img}"></a></li>
                    {/foreach}
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
    {/if}
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">特約商店</div>
	</div>
    {include file="$tpl_dir./mobile/store/search_store.tpl"}
	<div class="item_wrap_box store_box">
		<div class="banner">
			<a class="ui-link" href="/SearchStore">{if !empty($coupon_img)}<img class="" src="{$coupon_img}">{/if}</a>
		</div>
		<a class="ui-link new_store_wrap" href="/SearchStore/?order=news"></a>
	</div>
    {*    {include file="$tpl_dir./mobile/store/wrap_item_list.tpl"}*}
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">好康會員</div>
{*		<div class="slide_title2">會員專區</div>*}
	</div>
	<div class="want">
		<a href="/WantCoupon">{if !empty($want_img)}<img src="{$want_img}">{/if}</a>
        {if count($arr_want)}
			<div class="text">
                {foreach $arr_want as $v => $want}
					<div>
						<a href="{$want.href}">{$want.text}</a>
					</div>
                {/foreach}
			</div>
        {/if}
	</div>
	<div class="title_banner s">
		<div class="separate"></div>
		<div class="slide_title">好康店家</div>
{*		<div class="slide_title2">店家專區</div>*}
	</div>
	<div class="give">
		<a href="/GiveCoupon">{if !empty($give_img)}<img src="{$give_img}">{/if}
            {if !empty($give_text)}
				<div class="text text-center">
					{$give_text}
				</div>
            {/if}
		</a>
	</div>
</div>