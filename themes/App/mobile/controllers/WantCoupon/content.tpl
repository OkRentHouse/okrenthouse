{include file="$tpl_dir./mobile/coupon/min_menu.tpl"}
<div class="document">
    {if count($arr_content_benner)}
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true;ratio: {$content_ratio}">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
                    {foreach $arr_content_benner as $v => $banner}
						<li><a href="{$banner.href}"><img src="{$banner.img}"></a></li>
                    {/foreach}
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
    {/if}
</div>
<div class="title_banner s">
	<div class="separate"></div>
	<div class="slide_title">要好康</div>
{*	<div class="slide_title2">會員說明 | 生活樂購</div>*}
</div>
<div class="want1">
	<a class="ui-link" href="#">
        {if count($to_1_img)}<img src="{$to_1_img}">{/if}
		<a class="want_btn ui-link" href="#">我要好康</a>
	</a>
</div>
<div class="title_banner s">
	<div class="separate"></div>
</div>
<div class="want2">
	<a class="ui-link" href="#">
        {if count($to_2_img)}<img src="{$to_2_img}">{/if}
		<a class="link ui-link" href="#">每天都有新掀貨 快來逛逛</a>
	</a>
</div>