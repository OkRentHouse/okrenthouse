<!--租or售-->
<div class="ui-link-house_choose_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-house_choose">
        <div class="ui-link-house_choose-selter">

        </div>
    </div>
</div>

<!--用途-->
<div class="ui-link-main_house_class_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-main_house_class">
        <div class="ui-link-main_house_class-selter">

        </div>
    </div>
</div>

<!--型態-->
<div class="ui-link-types_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-types">
        <div class="ui-link-types-selter">

        </div>
    </div>
</div>

<!--類別-->
<div class="ui-link-type_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-type">
        <div class="ui-link-type-selter">

        </div>
    </div>
</div>

<!--城市-->
<div class="ui-link-county_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-county">
        <div class="ui-link-county-selter">

        </div>
    </div>
</div>

<!--鄉鎮-->
<div class="ui-link-city_b" style="background: #15191f;    width: 100%;    height: 100%; display:none">
    <div class="ui-link-city">
        <div class="ui-link-city-selter">

        </div>
    </div>
</div>

{if count($arr_index_banner)}
<div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: {$index_ratio}">
    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
        <ul class="uk-slideshow-items">
            {foreach $arr_index_banner as $v => $banner}
            <li>
                <a href="{$banner.href}"><img src="{$banner.img}"></a>
            </li>
            {/foreach}
        </ul>
        <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
        <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
    </div>
    <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>

</div>
{/if} {include file="$tpl_dir./mobile/menu_buoy.tpl"}
<div class="document">
    <!--<div class="title_banner">
		<div class="slide_title no_border_r">吉屋快搜</div>
	</div>-->

    <div class="slide_area">
        {*
        <div class="title_banner">*} {*
            <div class="slide_title border_r">News好康</div>
            <nav><a href="#" class="active">生活好康</a><a href="#">活動訊息</a><a href="#">創富分享</a></nav>*} {*
        </div>*}

        <div class="slide_photo" uk-slideshow="animation: push;ratio: {$list_ratio}" id="slide_photo_1" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">房地專區</div>
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="https://app.life-house.com.tw/list" class="" {* uk-slideshow-item="0" *}>吉屋快搜</a><a href="https://app.life-house.com.tw/list?switch=1" {* uk-slideshow-item="1" *}>推薦精選</a><a href="https://app.life-house.com.tw/list" {* uk-slideshow-item="2"
                            *}  style="position: relative;" >刊登廣告<span class="free" style="">免費</span></a></nav>
                </div>
            </div>
            {if count($arr_list_banner)}
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    {foreach $arr_list_banner as $v => $banner}
                    <li>
                        <a href="{$banner.href}"><img src="{$banner.img}"></a>
                    </li>
                    {/foreach}
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-nexttitle_banner uk-slideshow-item="next"></samp>
            </div>
            {/if} {* 取消下方點點*}
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
            {include file="$tpl_dir./mobile/house/search_house.tpl"}
        </div>

        <div class="slide_photo" uk-slideshow="animation: push;ratio: {$r_ratio}" id="slide_photo_2" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">租管服務</div>
                <!-- <div class="slide_title border_r">樂租管理</div> -->
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="#" uk-slideshow-item="0" class="active">管理查詢</a><a href="/handled" uk-slideshow-item="1">代租代管</a><a href="#" uk-slideshow-item="2">租事順利</a></nav>
                </div>
            </div>
            {if count($arr_rent_banner)}
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    {foreach $arr_rent_banner as $v => $banner}
                    <li>
                        <a href="{$banner.href}"><img src="{$banner.img}"></a>
                    </li>
                    {/foreach}
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
            </div>
            {/if} {* 取消下方點點*}
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>

        <div class="slide_photo" uk-slideshow="animation: push;ratio: {$n_ratio}" id="slide_photo_3" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">News好康</div>
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="https://app.life-house.com.tw/coupon" class="" {* uk-slideshow-item="0" *}>生活好康</a><a href="https://app.life-house.com.tw/InLife" {* uk-slideshow-item="1" *}>活動分享</a><a href="https://app.life-house.com.tw/LifeGo"
                            {*uk-slideshow-item="2"*} >生活樂購</a>
                    </nav>
                </div>
            </div>
            {if count($arr_coupon_banner)}
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    {foreach $arr_coupon_banner as $v => $banner}
                    <li>
                        <a href="{$banner.href}"><img src="{$banner.img}"></a>
                    </li>
                    {/foreach}
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-nexttitle_banner uk-slideshow-item="next"></samp>
            </div>
            {/if} {* 取消下方點點*}
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>

        <div class="slide_photo" uk-slideshow="animation: push;ratio: {$h_ratio}" id="slide_photo_4" tabindex="-1">
            <div class="title_banner">
                <div class="slide_title border_r">生活家族</div>
                <div>
                    <nav class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"><a href="https://app.life-house.com.tw/ExpertAdvisor"  class="active">達人顧問</a><a href="https://app.life-house.com.tw/Patner" {* uk-slideshow-item="1" *} >共好夥伴</a><a href="https://app.life-house.com.tw/JoinUs" >加盟體系</a></nav>
                </div>
            </div>
            {if count($arr_life_banner)}
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                <ul class="uk-slideshow-items">
                    {foreach $arr_life_banner as $v => $banner}
                    <li>
                        <a href="{$banner.href}"><img src="{$banner.img}"></a>
                    </li>
                    {/foreach}
                </ul>
                <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
                <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
            </div>
            {/if} {* 取消下方點點*}
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>
    </div>
</div>
