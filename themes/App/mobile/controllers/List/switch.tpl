<!--租or售-->
<div class="ui-link-house_choose_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-house_choose">
        <div class="ui-link-house_choose-selter">

        </div>
    </div>
</div>

<!--型態-->
<div class="ui-link-types_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-types">
        <div class="ui-link-types-selter">

        </div>
    </div>
</div>

<!--城市-->
<div class="ui-link-county_b" style="background: #15191f;    width: 100%;    height: 100%;">
    <div class="ui-link-county">
        <div class="ui-link-county-selter">

        </div>
    </div>
</div>

<!--鄉鎮-->
<div class="ui-link-city_b" style="background: #15191f;    width: 100%;    height: 100%; display:none">
    <div class="ui-link-city">
        <div class="ui-link-city-selter">

        </div>
    </div>
</div>

<table class="menu_buoy_main">

  <tr>
  <td class="c_1"><span class="txt">精選推薦</span></td>
  </tr>
  <tr>
  <td class="c_2"><nav class="menu_buoy">
    <div>
            {foreach $menu_buoy_switch as $k=>$v}
                <a href="{$v.a}" class="{if $v.active}active{/if} ui-link" {if $v.onclick}onclick="{$v.onclick}"{/if} {if $v.data_a}data-a="{$v.data_a}"{/if} {if $v.data_class}data-class="{$v.data_class}"{/if}>{$v.title}</a>
            {/foreach}

            <buoy style="left: 0px; right: 318px;"></buoy>
    </div>
  </nav>
  </td></tr>
</table>

<!-- 搜尋ber -->
	<input type="hidden" name="active" value="1">
	<input type="hidden" name="house_choose" value="">
	<input type="hidden" name="id_types" value="">
	<input type="hidden" name="county" value="">
	<input type="hidden" name="city" value="">
	<div style="display: flex;width: 70%;margin: auto;">
		<div class="search_select">
			<span><a href="#" data-transition="slide" class="ui-link ui-link-house_choose-txt">租屋</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-county-txt">區域</a></span>
			<span><a href="#" data-transition="slide" class="ui-link ui-link-types-txt">型態</a></span>
		</div>
{*		<div id="search_box">*}
{*			<div id="search_btn" class="search_btn">*}
{*				<img src="/themes/App/mobile/img/icon/pic_m.svg" class="sunmit icon">*}
{*			</div>*}
{*		</div>*}
	</div>
  <div class="notify" style="width: 100%;text-align: center;">
    <!--<label>選擇 <span>租屋</span> <span>區域</span> 或 <span>型態</span> 篩選後點圖片</label>-->
  </div>
{foreach $img_data as $k =>$v}
    <div class="zone zone_1 {$v.class}" {if $v.hidden==true} style="display: none"{/if}>
        <div class="item">
            <div class="top_bar">{$v.title}</div>
            <div class="photo">
                <!--<div class="search"><span class="notify">點我看更多</span> <i class="fas fa-angle-double-right"></i>  <i class="fas fa-search"></i> </div>-->
                <a href="{$v.a}" name="{$v.name}" {if $v.data_a}data-a="{$v.data_a}"{/if}>
                    <img src="{$v.img}">
                </a>
            </div>
        </div>
    </div>
{/foreach}
{$js}
<!-- 搜尋ber END-->
