{if $smarty.get.switch!=1}
    {include file="$tpl_dir./mobile/menu_buoy.tpl"}
{/if}
{if $smarty.get.active==1}
<div class="breadcrumb">{$breadcrumb}</div>
<div class="document" style="padding: 0 15px;">
    {foreach from=$arr_house key=i item=house}
    <div class="commodity slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1"
        style="width: 100%;margin: 0px 0px 5px 0px;overflow: hidden;height: 200px;border-radius: 5px;">
        <div class="photo_row" style="border-radius: 0;">
            <a href="/RentHouse?id_rent_house={$house.id_rent_house}" class="ui-link" target="_top">
                <img src="{if $house.img}{$house.img}{else}https://www.lifegroup.house/uploads/comm_soon.jpg{/if}"
                    class="img" alt="" />
                <collect data-id="{$house.id_rent_house}" data-type="rent_house"
                    class="favorite {if $house.favorite}on{/if}">
                    {if $house.favorite}
                    <img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">
                    {/if}
                </collect>
            </a>
            <div>
                <data class="rows_content">
                    <div style="position: absolute; padding-left: 5px;">
                        <h3 class="title big_2" style="text-align: left;"><a
                                href="/RentHouse?id_rent_house={$house.id_rent_house}" tabindex="-1"
                                class="ui-link">{$house.case_name}</a></h3>
                        <div class="title_2" style="display: flex;"><a
                                href="https://www.google.com.tw/maps/place/{$house.rent_address}" target="_blank">
                                <!--<img
                                    src="/themes/Rent/img/icon/icons8-address-50.png" />--><i
                                    class="fas fa-map-marked-alt"></i>&nbsp{$house.rent_address}</a>
                        </div>
                        <div class="title_4">
                            <h3 class="title big"><a href="RentHouse.php?id_rent_house=15543" tabindex="-1"
                                    class="ui-link">{$house.rent_house_title}</a></h3>
                        </div>
                    </div>
                    <div style="position: absolute; padding-right: 5px; width: 50%; text-align: end; right: 0px;">
                        <ul style="display: inline-flex; list-style-type: none;">
                            <li style="margin-right: 15px; margin-left: -30px;">{if $house.t3=='' ||
                                $house.t3=='0'}0房{else}{$house.t3}房{/if}{if $house.t4=='' ||
                                $house.t4=='0'}0廳{else}{$house.t4}廳{/if}{if $house.t5=='' ||
                                $house.t5=='0'}0衛{else}{$house.t5}衛{/if}{if $house.kitchen=='' ||
                                $house.kitchen=='0'}0廚{else}{$house.kitchen}廚{/if}</li>
                            <li>{$house.ping}坪</li>
                        </ul>
                        <price>{$house.rent_cash}
                            <unit>元</unit>
                        </price>
                    </div>
                </data>
            </div>
        </div>
    </div>
    {/foreach}
</div>
{elseif $smarty.get.switch==1}
    {include file="switch.tpl"}
{else}
{include file="$tpl_dir./mobile/house/search_list.tpl"}
{/if}
