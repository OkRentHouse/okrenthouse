<div class="home_sign_in">
    <div class="form_wrap">
        <form action="/login" method="post" enctype="application/x-www-form-urlencoded">
            <div class="floating">
                <input id="user" name="user" data-role="none" class="floating__input"  type="text"
                       placeholder="手機號碼">
                       <img src="/themes/App/mobile/img/login/id.png">
                <!-- <label for="user" class="floating__label" data-content="手機號碼"></label> -->
            </div>
            <div class="floating">
                <input id="password" name="password" data-role="none" class="floating__input" type="password"
                       placeholder="密碼">
                       <img src="/themes/App/mobile/img/login/psw.png">
                <!-- <label for="password" class="floating__label" data-content="密碼"></label> -->
            </div>

            <div class="content">
                <div class="flex">
                    <div class="item_l">
                        <input id="auto_login" name="auto_login" data-role="none" type="checkbox">
                        <label class="auto_sign_in" for="auto_login">自動登入</label>
                    </div>
                    <div class="item_r">
                        <a class="forget" href="forgot">忘記密碼?</a>
                    </div>
                </div>
                <br>
                <button class="btn_sign_in" type="submit" id="is_login" name="is_login">登 入</button>


            </div>
        </form>

    </div>
</div>
