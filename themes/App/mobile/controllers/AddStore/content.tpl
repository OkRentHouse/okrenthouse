{*{include file="$tpl_dir./mobile/coupon/min_menu.tpl"}*}
<div class="document">
    <div class="title_banner s">
        <div class="separate"></div>
        <div class="slide_title">好康店家</div>
        <div class="slide_title2">店家申請</div>
    </div>

    <!-- 廠商註冊框架 start -->
    <div class="store_registered_wrap">
        <div class="form-group flex">
            <label class="control-label">店名 | 品牌 名稱</label>
            <div class="full"><input class="form-control" type="text" placeholder="" name="title"></div>
        </div>
        <div class="form-group flex">
            <label class="control-label">營業時間 </label>
            <div class="full"><input class="form-control" id="open_time_w" name="open_time_w" type="text" placeholder=""></div>
        </div>
        <div class="form-group flex">
            <label class="control-label">休息日</label>
            <div class="full"><input class="form-control" id="close_time" name="close_time"  type="text" placeholder=""></div>
        </div>
        <div class="form-group flex">
            <label class="control-label">店家類別</label>
            <select class="form-control" name="id_store_type">
                <option>請選擇</option>
                {foreach  $row as $key => $value}
                    <option value="{$row[$key].id_store_type}">{$row[$key].store_type}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group flex">
            <label class="control-label">連鎖類別</label>
            <select class="form-control" name="chain">
                <option>請選擇</option>
                <option value="0">無</option>
                <option value="1">全國連鎖品牌</option>
                <option value="2">區域連鎖品牌</option>
            </select>
        </div>
        <div class=" form-group flex">
            <label class="control-label">店家地址</label>
            <div class="full"><input class="form-control" type="text" name="address1" placeholder="桃園市桃園區"></div>
        </div>
{*        <div class="form-group flex">*}
{*            <label class="control-label"></label>*}
{*            <div class="full"><input class="form-control" type="text" placeholder=""></div>*}
{*        </div>*}
        <div class="form-group flex">
            <label class="control-label">店家電話</label>
            <div class="full"><input class="form-control" type="text" name="tel1" placeholder=""></div>
        </div>

        <div class="form-group flex">
            <label class="control-label">付款方式</label>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="paying_1" name="paying[]" value="1">
                <label class="form-check-label">現金</label>
            </div>
            <div class="form-check flex">
                <input class="form-check-input" type="checkbox" id="paying_3" name="paying[]" value="3">
                <label class="form-check-label">刷卡</label>
            </div>
        </div>
        <div class="form-group flex">
            <label class="control-label"></label>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="paying_5" name="paying[]" value="5">
                <label class="form-check-label">會員制</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="paying_2" name="paying[]" value="2">
                <label class="form-check-label">線上支付</label>
            </div>
        </div>
        <div class="form-group flex">
            <label class="control-label">產品.服務</label>
            <div class="full"><input class="form-control" type="text" name="service" placeholder=""></div>
        </div>
        <div class="form-group flex">
            <label class="control-label">好康優惠</label>
            <div class="full"><input class="form-control" type="text" name="discount" placeholder=""></div>
        </div>

        <div class="segmentation"></div>
        <div class="form-group flex">
            <label class="control-label">聯&nbsp;&nbsp;絡&nbsp;&nbsp;人</label>
            <div class="full"><input class="form-control" type="text" name="contact_person" placeholder=""></div>
        </div>

        <div class="form-group flex">
            <label class="control-label"></label>
            <div class="full"><div class="radio">
                    <input type="radio" id="contact_person_0" name="contact_person_sex" value="0" checked="">小 姐
                </div>
                <div class="radio">
                    <input type="radio" id="contact_person_1" name="contact_person_sex" value="1">先 生
                </div></div>
        </div>

        <div class="form-group flex">
            <label class="control-label">聯絡手機</label>
            <div class="full">
                <input class="form-control" type="text" name="phone" placeholder="">
            </div>
        </div>



{*        <div class="form-group flex">*}
{*            <label class="control-label"></label>*}
{*            <div class="full"><input class="form-control" type="text" placeholder=""></div>*}
{*        </div>*}
        <div class="segmentation"></div>
        <div class="form-group flex grid3">
            <label class="control-label">負&nbsp;&nbsp;責&nbsp;&nbsp;人</label>
            <div class="full"><input class="form-control" type="text" name="legal_agent" placeholder=""></div>
        </div>

        <div class="form-group flex grid3">
            <label class="control-label"></label>
            <div class="full">
                <div class="radio">
                    <input type="radio" name="legal_agent_sex" id="legal_agent_sex_0" value="0" checked="">小 姐
                </div>
                <div class="radio">
                    <input type="radio" name="legal_agent_sex" id="legal_agent_sex_1" value="1">先 生
                </div>
            </div>
        </div>

        <div class="form-group flex">
            <label class="control-label">公司電話</label>
            <div class="full"><input class="form-control" id="company_tel" name="company_tel" type="text" name="address" placeholder="桃園市桃園區"></div>
        </div>

        <div class="form-group flex">
            <label class="control-label">登記地址</label>
            <div class="full"><input class="form-control" type="text" name="address" placeholder="桃園市桃園區"></div>
        </div>

        <div class="form-group flex">
            <label class="control-label">Line ID</label>
            <div class="full"><input class="form-control" type="text" name="line_id" placeholder=""></div>
        </div>
        <div class="form-group flex">
            <label class="control-label">e-mail</label>
            <div class="full"><input class="form-control" type="text" name="email" placeholder=""></div>
        </div>
        <div class="form-group flex">
            <label class="control-label">商號登記</label>
            <div class="full"><input class="form-control" type="text" name="firm" placeholder=""></div>
        </div>
        <div class="form-group flex">
            <label class="control-label">統一編號</label>
            <div class="full"><input class="form-control" type="text" name="uniform" placeholder=""></div>
        </div>
        <div class="agree">
            <input type="checkbox" value="1" id="agree">
            <label for="agree">
                我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款
            </label>
        </div>

{*        <div class="form-group flex">*}
{*            <label class="control-label">驗&nbsp;&nbsp;證&nbsp;&nbsp;碼</label>*}
{*            <div class="full"><input class="form-control" type="text" placeholder=""></div>*}
{*        </div>*}

        <div class="send text-center">
            <button type="button" class="btn">
                送 出
            </button>
        </div>


    </div>
    <!-- 會員註冊框架 end -->

</div>
























