<nav class="menu_buoy">
    <div>
        {foreach $menu_buoy as $key => $value}
            {if $key!=0}<b></b>{/if}
            <a {if !empty($value.onclick)} href="javascript:void(0)" onclick="{$value.onclick}"
                 {else}
                     href="{$value.url}"
                 {/if}
               {if in_array($buoy_url,$value.active)}class="active"{/if} target="_self" tabindex="-1">
                {$value.title}
            </a>
        {/foreach}
        <buoy></buoy>
    </div>
</nav>