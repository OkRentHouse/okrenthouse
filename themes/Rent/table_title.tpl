<style>
@media (max-width: 991px) {
{assign var=i value=1}
{foreach from=$fields.list key=key item=thead}
	.list .table tbody tr td:nth-of-type({$i++}):before {
		content: "{$thead['title']}";
	}
{/foreach}
}
</style>