</article>

{*{include file="goto_header.tpl"}*}

{if $group_link=='1'}

{include file="group_link.tpl"}

{/if}

{include file="../../modules/FloatShare/float_share.tpl"}

{if $display_footer}<footer>

    {* <nav>*}

        {* {foreach from=$footer_link key=i item=link}<a
            href="{if !empty($link.link)}{$link.link}{else}javascript:void(0);{/if}">{$link.txt}</a>{/foreach}*}

        {* </nav>*}

    <div class="footer">

        <table>

            <tr>

                <td>

                    {if !empty($footer_txt1)}<div class="footer_txt1">{$footer_txt1}</div>{/if}

                    {if !empty($footer_txt2)}<div class="footer_txt2">{$footer_txt2}</div>{/if}

                    {if !empty($footer_txt3)}<div class="footer_txt3">{$footer_txt3}</div>{/if}

                    {if !empty($footer_txt0428)}<div class="footer_txt0428">{$footer_txt0428}</div>{/if}

                    <div colspan="0" rowspan="0" style="vertical-align: middle;color: #fff;position: absolute;right: 23%;top: 28%;">
                        <a href="/statement" style="color: #fff;font-size: 8pt;">相關聲明</a>
                    </div>

                </td>

                {if !empty($qrcod_img)}

                <td class="qr_code_td">

                    <a href="{if !empty($footer_qrcode)}{$footer_qrcode}{else}{$qrcod_img}{/if}" target="_blank"
                        class="qrcode"><img src="{$qrcod_img}"></a>

                </td>{/if}

            </tr>

        </table>

    </div>

    {include file="../../../../modules/DesigningFooter/designingfooter.tpl"}

</footer>{/if}

{foreach from=$css_footer_files key=key item=css_uri}

<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css" />

{/foreach}

{foreach from=$js_footer_files key=key item=js_uri}

<script src="{$js_uri}"></script>

{/foreach}

</div>

</body>

</html>
