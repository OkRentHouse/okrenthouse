<section class="slider_center">
    <div>
        <a href="https://www.lifegroup.house/"><img alt="生活集團" src="/themes/Rent/img/index/logo-b-01.png"></a>
    </div>
    <div>
        <a href="https://www.okdeal.house/"><img alt="生活房屋" src="/themes/Rent/img/index/logo-b-02.png"></a>
    </div>
    <div>
        <a href="https://www.okmaster.life/ "><img alt="生活好科技" src="/themes/Rent/img/index/logo-b-12.png "></a>
    </div>
    <div>
        <a href="/Store?"><img alt="生活好康" src="/themes/Rent/img/index/logo-b-04.png "></a>
    </div>
    <div>
        <a href=""><img src="/themes/Rent/img/index/logo-b-05.png "></a>
    </div>
    <div>
        <a href="https://www.lifegroup.house/LifeGo"><img alt="生活樂購" src="/themes/Rent/img/index/logo-b-06.png "></a>
    </div>
    <div>
        <a href=" https://www.okpt.life"><img alt="好幫手" src="/themes/Rent/img/index/logo-b-07.png "></a>
    </div>
    <div>
        <a href=" https://www.okrent.tw"><img alt="共享圈" src="/themes/Rent/img/index/logo-b-08.png "></a>
    </div>
    <div>
        <a href="https://www.epro.house  "> <img alt="裝修達人" src="/themes/Rent/img/index/logo-b-09.png "></a>
    </div>
</section>

<script>
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 9,
        slidesToScroll: 9
    });
</script>
