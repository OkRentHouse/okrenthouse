//==================圖片詳細頁函數=====================
//鼠標經過預覽圖片函數
function preview(img) {
	$("#preview .jqzoom img").attr("src", $(img).attr("src"));
	$("#preview .jqzoom img").attr("jqimg", $(img).attr("bimg"));
	if($("#preview .jqzoom img").height() > $("#preview .jqzoom").height()){
		MT=($("#preview .jqzoom img").height() - $("#preview .jqzoom").height())/2;
		$("#preview .jqzoom img").css("margin-top",MT*-1)
	}else{
		$("#preview .jqzoom img").css("margin-top","")
	}

}

//圖片放大鏡效果
// $(function () {
// 	$(".jqzoom").jqueryzoom({
// 		xzoom: 380,
// 		yzoom: 410
// 	});
// });

//圖片預覽小圖移動效果,頁面加載時觸發
$(function () {
	var tempLength = 0; //臨時變量,當前移動的長度
	var viewNum = 4; //設置每次顯示圖片的個數量
	var moveNum = 1; //每次移動的數量
	var moveTime = 300; //移動速度,毫秒
	var scrollDiv = $(".spec-scroll .items ul"); //進行移動動畫的容器
	var scrollItems = $(".spec-scroll .items ul li"); //移動容器裡的集合
	var moveLength = scrollItems.eq(0).height() * moveNum; //計算每次移動的長度
	var countLength = 0;//(scrollItems.length - viewNum) * scrollItems.eq(0).height(); //計算總長度,總個數*單個長度
	var countLength_viewNum = 0;
	for(var i=0;i<scrollItems.length;i++){
		if(i<=viewNum){
			countLength_viewNum += scrollItems.eq(i).height();
		}
		countLength += scrollItems.eq(i).height();
	}; //計算總長度,總個數*單個長度

	countLength = countLength - scrollDiv.parent().height();

	//下一張
	$(".spec-scroll .next").bind("click", function () {

		moveLength = scrollItems.eq($("#jqimg_s").val()).height() * moveNum;
		if (tempLength < countLength) {
			if ((countLength - tempLength) > moveLength) {
				scrollDiv.animate({
					top: "-=" + moveLength + "px"
				}, moveTime);
				tempLength += moveLength;
				$("#jqimg_s").val(parseInt($("#jqimg_s").val())+1);
			} else {
				scrollDiv.animate({
					top: "-=" + (countLength - tempLength) + "px"
				}, moveTime);
				tempLength += (countLength - tempLength);
			}
		}
	});
	//上一張
	$(".spec-scroll .prev").bind("click", function () {
		moveLength = scrollItems.eq(parseInt($("#jqimg_s").val())-1).height() * moveNum;
		if (tempLength > 0) {
			if (tempLength > moveLength) {
				scrollDiv.animate({
					top: "+=" + moveLength + "px"
				}, moveTime);
				tempLength -= moveLength;
				$("#jqimg_s").val(parseInt($("#jqimg_s").val())-1);
			} else {
				scrollDiv.animate({
					top: "+=" + tempLength + "px"
				}, moveTime);
				tempLength = 0;
				$("#jqimg_s").val(0);
			}
		}
	});
});
//==================圖片詳細頁函數=====================



$(document).ready(function(){
  new_fun_1013();
  $(".house_search .spec-scroll .prev").text("∧");
  $(".house_search .spec-scroll .next").text("∨");
});

$(window).resize(function(){
	new_fun_1013();
});

function new_fun_1013(){
	$(".house_search .spec-scroll .items img").css("width",$(".house_search .spec-scroll .items").width()+"px");
	$(".house_search .spec-scroll .items ul li").css("width",$(".house_search .spec-scroll .items").width()+"px");
}
