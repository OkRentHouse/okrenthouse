$(document).ready(function(){



    $("#id_county").change(function() {
        let id_county = $("#id_county").val();
        get_city(id_county);
    });

    $("#bank").change(function(){
        let code = $("#bank option:selected").data("code");
        $("input[name='bank_code']").val(code);
    });

    $(".get_file").click(function(){
        let name = $(this).data("name");
        let value = $(this).data("value");
        function_img(name,value);
    });

    function function_img(name,index){
        let name_data = document.getElementsByName(name)[index];//取得元素
        $(name_data).click();
    }

    $("#sumbit_data").validate({
        rules: {
            name :{
                required :true,
                max:20,
            }
        },
        messages: {
            required: "这是必填字段",
        },
    })

    $(".get_file").click(function(){
      $(this).parent().find("input[type='file']").click();
    })

});

function start_form(){
  $(".join_life_wrap.step1").show();
  $(".join_life_wrap.step1").css({opacity:0});
  $(".join_life_wrap.step1").animate({opacity:1},100,function(){

  })
  // $(".join_life_wrap.step1").hide(100,function(){$(".join_life_wrap.step2").show(100);});
}

function sub_next(){

  $(".join_life_wrap.step1").animate({opacity:0},100,function(){
    $(".join_life_wrap.step2").show();$(".join_life_wrap.step2").css({opacity:0});$(".join_life_wrap.step1").hide();
    $(".join_life_wrap.step2").animate({opacity:1},500,function(){
      //window.scrollTo(0,0)

    })
  })
  // $(".join_life_wrap.step1").hide(100,function(){$(".join_life_wrap.step2").show(100);});
}

function sub_per(){

  $(".join_life_wrap.step2").animate({opacity:0},100,function(){
    $(".join_life_wrap.step1").show();$(".join_life_wrap.step1").css({opacity:0});$(".join_life_wrap.step2").hide();
    $(".join_life_wrap.step1").animate({opacity:1},500,function(){

    })
  })
  // $(".join_life_wrap.step1").hide(100,function(){$(".join_life_wrap.step2").show(100);});
}

function get_city(id_county){
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'City',
            'id_county' : id_county,
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if(data["error"] !=""){
                alert(data["error"]);
            }else{
                let html_data = '';
                data["return"].forEach(function(item, index, arr) {
                    html_data +='<option value="'+item['id_city']+'"><span>'+item['city_name']+'</span></option>';
                });
                $("#id_city").empty();
                $("#id_city").html(html_data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function submit_data(){
    let sumbit_result =  $("#sumbit_data").valid();
    if(!sumbit_result){
        return;
    }


    // $("#sumbit_data").submit();
}
