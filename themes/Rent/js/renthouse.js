    function push_message(id,type){
        //取出各值
        var name = $("#modal-container"+type+" input[name='name']").val();
        var sex = $("#modal-container"+type+" input[name='sex_"+type+"']:checked").val();
        var phone = $("#modal-container"+type+" input[name='phone']").val();
        var tel = $("#modal-container"+type+" input[name='tel']").val();
        var email = $("#modal-container"+type+" input[name='email']").val();
        var contact_time = $("#modal-container"+type+" input[name='contact_time_"+type+"']:checked").val();
        var year = $("#modal-container"+type+" input[name='year']").val();
        var mom = $("#modal-container"+type+" input[name='mom']").val();
        var day = $("#modal-container"+type+" input[name='day']").val();
        var appointment_period = $("#modal-container"+type+" input[name='appointment_period_"+type+"']:checked").val();
        var message = $("#modal-container"+type+" textarea[name='message']").val();
        var agree = $("#modal-container"+type+" input[name='agree']:checked").val();
        if(type=='2'){
            var appointment_time = year+'-'+mom+'-'+day;//yyyy-mm-dd
        }else{
            var appointment_time ='';
        }

        // console.log(name+" "+sex+" "+phone+" "+tel+" "+email+" "+" "+contact_time+" "+year+" "+mom+" "+day+" "+appointment_period+" "+message+" "+agree);
        console.log(appointment_period);
        if(agree !='1'){
            alert("請勾選同意書");
            return false;
        }
        if(name.length >'64') {
            alert("姓名輸入過長");
            return false;
        }else if(name=="" || name=='undefined'  || name==null){
            alert("必須輸入姓名");
            return false;
        }
        if(sex !='0' && sex !='1' && sex !="2" ){
            alert("請選取性別");
            return false;
        }
        if(phone ==''){
            alert("請輸入手機號碼");
            return false;
        }
        if(contact_time !='0' && contact_time !='1' && contact_time !="2" && contact_time !="3"){
            alert("請選取聯絡時段");
            return false;
        }
        if(type=='2' && (appointment_period !='0' && appointment_period !='1' && appointment_period !="2" && appointment_period !="3")){
            alert("請選取賞屋時段");
            return false;
        }
        if(message !='' && message.length>'100'){
            alert("留言不能超過100個字");
            return false;
        }

        //施工中~
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'RentHouseMessage',
                'id' : id,
                'type' : type,
                'name' : name,
                'sex' : sex,
                'phone' : phone,
                'tel' : tel,
                'email' : email,
                'contact_time' : contact_time,
                'appointment_period' : appointment_period,
                'message' : message,
                'agree' : agree,
                'appointment_time' : appointment_time
            },
            dataType: 'json',
            success: function (data) {
                if(data["error"] !=""){
                    alert(data["error"]);
                }else{
                    if(data["return"]=="1"){
                        alert("以留言成功!");
                    }else if(data["return"]=="2"){
                        alert("發送預約訊息成功!");
                    }else if(data["return"]=="3"){
                        alert("以發送訊息給樂租顧問 我們會盡快回應您!");
                    }else{
                        alert(data["return"]);
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    }

    function MessageVerification(type){
        var phone = $("#modal-container" + type + " input[name='phone']").val();
        if (phone == '') {
            alert("請輸入手機號碼");
            return false;
        }
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'MessageVerificationSMS',
                'phone': phone,
            },
            dataType: 'json',
            success: function(data) {
                if (data["error"] != "") {
                    alert(data["return"]);
                } else {
                    alert(data["return"]);
                }
            },

            error: function(xhr, ajaxOptions, thrownError) {
            }
        });
    }