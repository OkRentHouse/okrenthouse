$(document).ready(function(){
    $(window).scroll(function(){
        last=$("body").height()-$(window).height();
        if($(window).scrollTop()>=last && ($("#count").val() > $("#count_now").val())){
            //拉到最下方執行ajax
            $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': false,
                    'action': 'Addhtml',
                    'count' : $("#count").val(),
                    'count_now' : $("#count_now").val()
                },
                dataType: 'json',
                success: function (data) {
                    if(data["error"] !=""){
                        alert(data["error"]);
                    }else{
                        var count_now  =  Number($("#count_now").val());//轉型為數字
                        var compartment = Number($("#compartment").val()); //間格
                        var count_now = count_now+compartment;//
                        if(count_now>=$("#count").val()){//超過要等於
                            $("#count_now").val($("#count").val());
                        }else{
                            $("#count_now").val(count_now);
                        }
                    }
                    $("#add_wrap").append(data["return"]["addhtml"]);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                }
            });
        }
    });
});