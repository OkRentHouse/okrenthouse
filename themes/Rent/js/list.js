function push_message(type) {
  //取消動作
  if (type == '0') {
    $("div#modal-container1").hide();
    $(".modal-backdrop.fade.in").remove();
    $(".modal-open").removeAttr("style");
    $(".modal-open").removeClass("modal-open");
    return false;
  }
    //取出各值

    var id = $("#modal-container" + type + " input[name='id_rent_house']").val();
    var name = $("#modal-container" + type + " input[name='name']").val();
    var verification = $("#modal-container" + type + " input[name='verification']").val();
    var sex = $("#modal-container" + type + " input[name='sex_" + type + "']:checked").val();
    var phone = $("#modal-container" + type + " input[name='phone']").val();
    var tel = $("#modal-container" + type + " input[name='tel']").val();
    var email = $("#modal-container" + type + " input[name='email']").val();
    var contact_time = $("#modal-container" + type + " input[name='contact_time_" + type + "']:checked").val();
    var year = $("#modal-container" + type + " input[name='year']").val();
    var mom = $("#modal-container" + type + " input[name='mom']").val();
    var day = $("#modal-container" + type + " input[name='day']").val();
    var appointment_period = $("#modal-container" + type + " input[name='appointment_period_" + type + "']:checked").val();
    var message = $("#modal-container" + type + " textarea[name='message']").val();
    var agree = $("#modal-container" + type + " input[name='agree']:checked").val();

    if (type == '2') {
        var appointment_time = year + '-' + mom + '-' + day; //yyyy-mm-dd
    } else {
        var appointment_time = '';
    }
    // console.log(name+" "+sex+" "+phone+" "+tel+" "+email+" "+" "+contact_time+" "+year+" "+mom+" "+day+" "+appointment_period+" "+message+" "+agree);
    console.log(tel);
    if (id == '') {
        alert("請選擇服務");
        return false;
    }


    if (email != '' && email.indexOf('@') < 0 ) {
      alert("電子信箱格式錯誤，必須有 @ ");
      return false;
    }else if (email != '' && email.indexOf('.com') < 0 ) {
      alert("電子信箱格式錯誤，必須有 .com ");
      return false;
    }


    if (tel != '' && tel.slice(0,1) != '0' ) {
        alert("請輸入電話縣市區碼。台北02，桃園03，新竹03 ... ");
        return false;
    }else if ( tel != '' && tel.indexOf('-') < 0 ){
      if( tel.indexOf('036') >= 0 || tel.indexOf('049') >= 0 || tel.indexOf('089') >= 0 ){
        $("input[name='tel']").val(tel.slice(0,3)+"-"+tel.slice(3));
      }else{
        $("input[name='tel']").val(tel.slice(0,2)+"-"+tel.slice(2));
      }
    }

    if (agree != '1') {
        alert("請勾選同意書");
        return false;
    }

    if (name.length > '64') {
        alert("姓名輸入過長");
        return false;

    } else if (name == "" || name == 'undefined' || name == null) {
        alert("必須輸入姓名");
        return false;
    }

    if (sex != '0' && sex != '1' && sex != "2") {
        alert("請選取性別");
        return false;
    }

    if (phone == '') {
        alert("請輸入手機號碼");
        return false;
    }
    if (contact_time != '0' && contact_time != '1' && contact_time != "2" && contact_time != "3") {
        alert("請選取聯絡時段");
        return false;
    }

    if (type == '2' && (appointment_period != '0' && appointment_period != '1' && appointment_period != "2" && appointment_period != "3")) {
        alert("請選取賞屋時段");
        return false;
    }

    if (message != '' && message.length > '100') {
        alert("留言不能超過100個字");
        return false;
    }

    //施工中~
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'RentHouseMessage',
            'id': id,
            'type': type,
            'name': name,
            'sex': sex,
            'phone': phone,
            'tel': tel,
            'email': email,
            'contact_time': contact_time,
            'appointment_period': appointment_period,
            'message': message,
            'agree': agree,
            'appointment_time': appointment_time,
            'verification' : verification,
        },
        dataType: 'json',
        success: function(data) {
            if (data["error"] != "") {
                if(data["error"]=="請登入帳號或是驗證手機"){
                    $("#modal-container" + type + " input[name='verification']").parent().show();
                }
                alert(data["error"]);
            } else {
                if (data["return"] == "1") {
                    alert("以留言成功!");
                } else if (data["return"] == "2") {
                    alert("發送預約訊息成功!");
                } else if (data["return"] == "3") {
                    alert("以發送訊息給樂租顧問 我們會盡快回應您!");
                } else {
                    alert(data["return"]);

                }
            }
        },

        error: function(xhr, ajaxOptions, thrownError) {

        }
    });

}

function MessageVerification(type){
    var phone = $("#modal-container" + type + " input[name='phone']").val();
    if (phone == '') {
        alert("請輸入手機號碼");
        return false;
    }
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'MessageVerificationSMS',
            'phone': phone,
        },
        dataType: 'json',
        success: function(data) {
            if (data["error"] != "") {
                alert(data["return"]);
            } else {
                alert(data["return"]);
            }
        },

        error: function(xhr, ajaxOptions, thrownError) {

        }
    });
}



//109.08.26 - 修改整體字體大小使用下面 function，可使用方向鍵"上"，"下"調整字體大小
/*
function fs_re(e) {
    var f = $(e).css("font-size");
    var fsTxt = f.slice(0, -2) * 1;
    var item_tag=$(e).attr("class")?$(e).attr("class"):$(e).prop('tagName');
    console.log(item_tag +"-"+ (fsTxt + 1) + f.slice(-2));
    $(e).css("font-size", fsTxt + 1 + f.slice(-2));
};

function fs_re_low(e) {
    var f = $(e).css("font-size");

    var fsTxt = f.slice(0, -2) * 1;
    console.log((fsTxt - 1) + f.slice(-2));
    $(e).css("font-size", fsTxt - 1 + f.slice(-2));
};

document.onkeydown = function(e) {
    switch (e.which) {

        case 38:
            // up
console.log("+1");
fs_re($(".commodity h3.title.big > a"));
fs_re($(".list .commodity .object .title"));
fs_re($(".list .commodity .object .address"));
fs_re($(".list .commodity .object>*"));
fs_re($(".List price"));
fs_re($(".List price unit"));
fs_re($(".List price .cost"));
fs_re($(".List .data span"));
fs_re($(".List .data>*"));
fs_re($(".List .data .number"));
fs_re($(".List .data .btn_wrap a"));

break;


case 40:
    // down
    console.log("-1");
fs_re_low($(".commodity h3.title.big > a"));
fs_re_low($(".list .commodity .object .title"));
fs_re_low($(".list .commodity .object .address"));
fs_re_low($(".list .commodity .object>*"));
fs_re_low($(".List price"));
fs_re_low($(".List price unit"));
fs_re_low($(".List price .cost"));
fs_re_low($(".List .data span"));
fs_re_low($(".List .data>*"));
fs_re_low($(".List .data .number"));
fs_re_low($(".List .data .btn_wrap a"));

break;

default:
return;
}
e.preventDefault();
};
*/


$(document).ready(function() {

    /* 109.08.26 全部調降2級字 */
    $(".list .commodity .object .address").css("font-size", "14px");
    $(".list .commodity .object>*").css("font-size", "14px");
    $(".List price").css("font-size", "26px");
    $(".List price unit").css("font-size", "14px");
    $(".List price .cost").css("font-size", "14px");
    $(".List .data span").css("font-size", "20px");
    $(".List .data>*").css("font-size", "16px");
    $(".List .data .number").css("font-size", "18px");
    $(".List .data .btn_wrap a").css("font-size", "16px");
    $(".list .commodity .object .title").css("font-size", "20px");
    $(".commodity h3.title.big>a").css("font-size", "24px");

    /* 110.01.22 以下調升 */
    $(".title.structure").css("font-size", "15px");
    $(".List price .cost").css("font-size", "15px");
    $(".List .data .number").css("font-size", "19px");
    $(".List .data .btn_wrap a").css("font-size", "17px");
    $("tag p").css("font-size", "15px");
    $(".List price unit").css("font-size", "15px");

    $(".search_forms_wrap_III").css({"opacity": 0});

});

window.onload=function(){
  //$(".search_forms_wrap_III").hide();
  $(".search_forms_wrap_III").slideUp(50);

}

function selectCondition(){

  console.log($(".search_forms_wrap_III").css("display"));
  if ($(".search_forms_wrap_III").css("display") == "none")
    {
      //$(".search_forms_wrap_III").show();
      $(".search_forms_wrap_III").css({"opacity": 1});
      $(".container.rage3 .slider .marker").css({"margin-top":0})
      $(".search_forms_wrap_III").slideDown(500);
    }
  else
    {
      //$(".search_forms_wrap_III").hide();
        $(".search_forms_wrap_III").slideUp(100);
  }

}
