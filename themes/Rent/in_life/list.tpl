<ul class="list_wrap">
	{foreach $in_life as $i => $item}<li class="list"><a href="/{if empty($item.url)}in_life?id={$item.id_in_life}{else}{$item.url}{/if}"><div class="img">
				{if !empty($item.img)}<img src="{$item.img}">{else}<div class="no_img"><img src="{$THEME_URL}/img/icon/logo.svg"></div>{/if}
			</div><div class="content">
				<h3 class="caption1">{$item.title}</h3>
				<div class="caption2">{$item.title2}</div>
				{if !empty($item.exp)}<div class="text">{$item.exp}</div>{/if}
			</div>{if !empty($item.date_t) || !empty($item.weekday_t) || !empty($item.time_t) || !empty($item.address) || !empty($item.add_name)}<ul class="info">
			{if !empty($item.date_t) || !empty($item.weekday_t) || !empty($item.time_t)}<li class="time"><span>時間</span>{if !empty($item.date_t)}<div>{$item.date_t}</div>{/if}{if !empty($item.weekday_t)}<div class="weekday">{$item.weekday_t}</div>{/if}{if !empty($item.time_t)}<div class="time">{$item.time_t}</div>{/if}</li>{/if}
			{if !empty($item.address) || !empty($item.add_name)}<li class="postion"><span>地點</span>{if !empty($item.address)}<div>{$item.address}</div>{/if}{if !empty($item.add_name)}<div>{$item.add_name}</div>{/if}</li>{/if}
			</ul>{/if}</a></li>{foreachelse}<li class="no_data"><div class="title">
			<h3 class="caption1">{l s="找不到相關資料"}</h3>
		</div></li>{/foreach}
</ul>{include file='../pagination.tpl'}