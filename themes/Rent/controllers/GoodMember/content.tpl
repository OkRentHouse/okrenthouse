{$breadcrumbs_htm}
{include file="$tpl_dir./good_tab.tpl"}
{if $smarty.get.id_announcement}
    <div class="margin_box">
        <div class="title">
            <h1>{$announcement_arr.title}</h1>
            <div class="good_member">
               <div>發佈時間 {$announcement_arr.create_time}</div>
            </div>
        </div>
        <div class="good_member_warp">
            {$announcement_arr.html_content}
        </div>
    </div>
{else}
    <div class="goodies_member_wrap">
        <div class="banner">
            <img class="" src="/themes/Rent/img/goodmember/banner.jpg">
        </div>

        <div class="text-center">
            <h2 class="caption"><span><img src="/themes/Rent/img/goodmember/star.svg" alt="" class=""></span>我要享好康</h2>
        </div>

        <div class="step_main_wrap">
            <div class="step_img step_one">
                <img src="/themes/Rent/img/goodmember/download.svg" alt="">
            </div>

            <div class="step_wrap">
                <div class="list_frame">
                    <p class="">到
                        <a target="_blank"
                           href="https://play.google.com/store/apps/details?id=com.ilife_house.life_house&hl=zh_TW"
                           class=""><img src="/themes/Rent/img/goodmember/google_play.png" alt="google_play" class=""></a>
                        下載生活樂租APP或到生活集團體系網站線上完成會員註冊。
                    </p>
                    <ul class="flex">
                        <li class=""><img src="/themes/Rent/img/goodmember/01.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/02.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/03.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/04.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/05.svg" alt="" class=""></li>
                        <i></i>
                        <li class=""><img src="/themes/Rent/img/goodmember/06.svg" alt="" class=""></li>
                    </ul>

                    <div class="text-center">
                        <a href="" class="btn">立馬註冊</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="step_main_wrap">
            <div class="step_img step_two">
                <img src="/themes/Rent/img/goodmember/mobile.svg" alt="">
            </div>

            <div class="step_wrap">
                <div class="list_frame">
                    <p class="">
                        以有效手機號碼綁定VIP卡，接收認證簡訊，立馬就享好康。
                    </p>
                </div>
            </div>
        </div>

        <div class="step_main_wrap">
            <div class="step_img step_three">
                <img src="/themes/Rent/img/goodmember/gift.svg" alt="">
            </div>

            <div class="step_wrap">
                <div class="list_frame">
                    <p class="">
                        成為會員享有會員專屬生日精美禮品及紅利積點換好康。
                    </p>
                </div>
            </div>
        </div>

        <div style="margin-bottom: 130px;"></div>

        <div class="text-center">
            <img src="/themes/Rent/img/goodmember/click.jpg" alt="" class="text-center">
        </div>

        <div style="margin-bottom: 50px;"></div>

        <div class="text-center">
            <h2 class="caption"><span><img src="/themes/Rent/img/goodmember/star.svg" alt="" class=""></span>NEWS</h2>
        </div>

        {if !empty($row_all)}
            <input type="hidden" id="count" value="{$row_sql.count_log}">
            <input type="hidden" id="count_now" value="{$compartment}">
            <input type="hidden" id="compartment" value="{$compartment}">
            <ul class="list_wrap" id="add_wrap">
                {foreach from=$row_all key=k item=v}
                    <li class="list">
                        <div class="image_wrap">
                            <img src="{$v.img}">
                        </div>
                        <div class="text_wrap">
                            <div class="caption_wrap">
                                <div class="caption">
                                    {$v.title}
                                </div>
                                <div class="date">{$v.create_time}</div>
                            </div>
                            {$v.content}<span><a href="/GoodMember?id_announcement={$v.id_announcement}">more</a></span>
                        </div>
                    </li>
                {/foreach}
            </ul>
        {/if}
    </div>
{/if}


