{$breadcrumbs_htm}
{include file='../../in_life/in_life_type.tpl'}
    <div class="join_life_wrap" style="margin: 0 auto;">
        <div class="content_block_wrap" style="width: 90%;">
            <div class="image_wrap">
                <img src="/themes/Rent/img/inlifeindex/img_1.jpg" alt="" class="">
            </div>
            <div class="text_wrap">
                <h1 class="caption">
                    <span class="title_h">活動分享</span>
                </h1>
                <p class="text"><span class="title_h_sec">精彩豐富 與您分享</span></p>
                {$carousel}
            </div>
        </div>
        <div class="content_block_wrap " style="width: 90%;">
            <div class="text_wrap">
                <h1 class="caption"><span class="title_h">創富分享</span></h1>
                <p class="text"><span class="title_h_sec">透過分享 創造被動永續收入</span></p>
            </div>
            <div class="image_wrap">
                <img src="/themes/Rent/img/inlifeindex/img_2.jpg" alt="" class="">
            </div>
        </div>
        <div class="content_block_wrap" style="width: 90%;">
            <div class="image_wrap">
                <img src="/themes/Rent/img/inlifeindex/img_3.jpg" alt="" class="">
            </div>

            <div class="text_wrap">
                <img src="/themes/Rent/img/inlifeindex/offer.svg" alt="" class="">
            </div>
        </div>
        <div class="content_block_wrap" style="width: 90%;">
            <div class="text_wrap">				<span class="title_h_sec">
                <p class="text">特別的... 新奇的... 有趣的... 實用的...</p>
                <p class="text">盡在 </p>				</span>
                <img src="/themes/Rent/img/inlifeindex/let_go.svg" alt="" class="">
            </div>
            <div class="image_wrap">
                <img src="/themes/Rent/img/inlifeindex/img_4.jpg" alt="" class="">
            </div>
        </div>
    </div>
{$carousel_js}
