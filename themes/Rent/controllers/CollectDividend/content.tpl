 {$breadcrumbs_htm}
        <div class="collect_dividend_wrap">
            <div class="banner">
                <img class="" src="https://okrent.house/img/step_img/collect_dividendfile_banner.jpg">
            </div>
            <br>
            <br>
            <hr>
            <div class="explanation_wrap">
                <h2 class="title text-center">活動說明</h2>
                <div class="content">
                    <p class="">註冊為生活集團暨旗下相關企業之會員後，於上述企業機構內消費或參與活動，
                        皆有機會獲得紅利點數；每位會員累積之點數，可依網站公告兌換各項物品或服務。
                    </p>
                </div>
            </div>

            <div class="store_wrap">

                <section class="regular slider">
                    <div class="list">
                        <a href="" class="">
                            <div class="tag">我要兌換</div>
                        </a>
                        <img src="https://okrent.house/img/step_img/collect_dividendfile_01.jpg" alt="" class="">
                        <div class="content">
                            <div class="caption">DASHIANG保溫杯</div>
                            <div class="point">1,000<span> 兌點</span></div>
                            <div class="quantity">剩餘數量 <span>20</span> 個</div>
                        </div>
                    </div>

                    <div class="list">
                        <a href="" class="">
                            <div class="tag">我要兌換</div>
                        </a>
                        <img src="https://okrent.house/img/step_img/collect_dividendfile_02.jpg" alt="" class="">
                        <div class="content">
                            <div class="caption">健康御守滅菌王漱口錠</div>
                            <div class="point">2,000<span> 兌點</span></div>
                            <div class="quantity">剩餘數量 <span>5</span> 組</div>
                        </div>
                    </div>

                    <div class="list">
                        <a href="" class="">
                            <div class="tag">我要兌換</div>
                        </a>
                        <img src="https://okrent.house/img/step_img/collect_dividendfile_03.jpg" alt="" class="">

                        <div class="content">
                            <div class="caption">室內空氣健康指數
                                檢測服務</div>
                            <div class="point">5,000<span> 兌點</span></div>
                            <div class="quantity">剩餘數量 <span>30</span> 次</div>
                        </div>
                    </div>

                    <div class="list">
                        <a href="" class="">
                            <div class="tag">我要兌換</div>
                        </a>
                        <img src="https://okrent.house/img/step_img/collect_dividendfile_04.jpg" alt="" class="">
                        <div class="content">
                            <div class="caption">健康御守-
                                滅菌王隨身寶禮盒組</div>
                            <div class="point">3,000<span> 兌點</span></div>
                            <div class="quantity">剩餘數量 <span>20</span> 組</div>
                        </div>
                    </div>

                    <div class="list">
                        <a href="" class="">
                            <div class="tag">我要兌換</div>
                        </a>
                        <img src="https://okrent.house/img/step_img/collect_dividendfile_01.jpg" alt="" class="">

                        <div class="content">
                            <div class="caption">DASHIANG保溫杯</div>
                            <div class="point">1,000<span> 兌點</span></div>
                            <div class="quantity">剩餘數量 <span>20</span> 個</div>
                        </div>
                    </div>

                    <div class="list">
                        <a href="" class="">
                            <div class="tag">我要兌換</div>
                        </a>
                        <img src="https://okrent.house/img/step_img/collect_dividendfile_02.jpg" alt="" class="">
                        <div class="content">
                            <div class="caption">健康御守滅菌王漱口錠</div>
                            <div class="point">2,000<span> 兌點</span></div>
                            <div class="quantity">剩餘數量 <span>5</span> 組</div>
                        </div>
                    </div>

                </section>

            </div>

            <div class="text-center">
                <a href="" class="btn">紅利點數查詢</a>
            </div>

            <div class="info_wrap">
                <div class="left">
                    <h2 class="title">紅利點數取得規定</h2>
                    <div class="content">
                        <div class="caption">紅利點數取得規定</div>
                        <p class="">
                            註冊成為本公司或生活集團旗下企業的會員後，只要參加本公司網站不定期所推出的活動，即可獲得紅利點數；若會員在生活集團旗下關係企業所提供的商品或服務為消費時，亦可獲得紅利點數，當次消費金額以每100(新台幣)元累計紅利一點，未滿100元的金額不計入點數，也無法將該不滿100元之金額，累計至下次消費金額換算紅利點數。紅利點數取得之詳細規定，依各活動規定為準。
                        </p>
                        <p class="">
                            所累積的紅利點數到達兌換標準時，會員可依本公司訂定的紅利點數活動與兌換標準，對本網站內相關物品或服務進行兌換。若紅利點數不足以兌換商品或服務時，會員應以現金支付差額部分。
                        </p>
                    </div>
                    <div class="content">
                        <div class="caption">紅利點數的使用限制</div>
                        <p class="">紅利點數不得作為買賣、贈與、轉讓之標的。一經兌換或使用後，即不得要求再回復為紅利點數。</p>
                        <p class="">紅利點數僅適用於本網站規劃之範圍，於兌換使用之前，並不構成會員的資產。</p>
                        <p class="">會員不得要求本公司將紅利點數折算現金，也不得以不屬於本公司網站內的物品或服務，而要求本公司為兌換。</p>
                    </div>
                    <div class="content">
                        <div class="caption">紅利點數有效期限</div>
                        <p class="">會員之紅利點數累計，從會員於網站首次消費或參與活動時起計算，且所累積點數，終身有效，沒有使用時限。
                        </p>
                    </div>
                    <div class="content">
                        <div class="caption">修改及終止</div>
                        <p class="">本公司保留修正、暫停、終止本活動及本辦法（但不限於參加資格、積點計算）之權利。
                        </p>
                    </div>
                    <div class="content">
                        <div class="caption">注意事項</div>
                        <p class="">
                            本網站內所提供之物品或服務，係由各廠商直接提供予本公司會員，本公司與廠商之間並無任何合夥、經銷、代理或保證關係，如會員與廠商就所提供之商品或服務有發生爭議時，應由各廠商負責，概與本公司無涉。
                            會員於兌換紅利時，即表示同意提供物品或服務的廠商，得以在兌換紅利目的之必要範圍內，蒐集、處理、網路傳輸及利用會員的個人資料。
                        </p>
                        <p class="">
                            會員所累積之紅利點數兌換之行為，依財政部函釋規定，不會成為會員的所得。因此，本公司無法為會員扣繳任何稅款，在會計年度終了時也不會發給會員任何形式的扣繳憑單。</p>
                    </div>

                </div>

                <div class="right">
                    <div class="other_collect_dividend">
                        {foreach $row_all as $k => $v}
                            <a href="https://www.okrent.house/in_life?id={$v.id_in_life}">
                                <img src="{$v.img}">
                            </a>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <script>
            {$js}
        </script>




