{$breadcrumbs_htm}
<div class="house_content">
	<div class="main_caption"><h1>中悅一品<i></i>裝潢雅緻屋況優<i></i>桃園市桃園區中正路</h1></div>
	<div class="content">
		<div id="content" class="data_wrap">
			<div class="img_preview">
				<div id="preview" class="spec-preview"><span class="jqzoom"><img src="/img/house/1/img_01.jpg">
				</div>
				<div class="spec-scroll">
					<a class="prev">&lt;</a> <a class="next">&gt;</a>
					<div class="items">
						<ul>
							<li><img bimg="/img/house/1/img_01.jpg" src="/img/house/1/img_01.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_02.jpg" src="/img/house/1/img_02.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_03.jpg" src="/img/house/1/img_03.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_04.jpg" src="/img/house/1/img_04.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_05.jpg" src="/img/house/1/img_05.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_06.jpg" src="/img/house/1/img_06.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_01.jpg" src="/img/house/1/img_01.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_02.jpg" src="/img/house/1/img_02.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_03.jpg" src="/img/house/1/img_03.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_04.jpg" src="/img/house/1/img_04.jpg"
									 onmousemove="preview(this);">
							</li>
							<li><img bimg="/img/house/1/img_05.jpg" src="/img/house/1/img_05.jpg"
									 onmousemove="preview(this);">
							</li>

						</ul>
					</div>
				</div>
			</div>
			<!-- 基本資料 -->
			<section class="base_data">
				<h3 class="caption">基本資料</h3>
				<ul class="list_wrap">

					<li class="list">
						<div class="title letter_s05">屋齡</div>
						<div class="info">5.2年</div>
					</li>


					<li class="list">
						<div class="title">產權登記</div>
						<div class="info">有</div>
					</li>
					<li class="list">
						<div class="title">隔間材質</div>
						<div class="info">水泥</div>
					</li>
					<li class="list">
						<div class="title">座 向</div>
						<div class="info">座東南</div>
					</li>
					<li class="list">
						<div class="title">邊 間</div>
						<div class="info">是</div>
					</li>
					<li class="list">
						<div class="title">採 光</div>
						<div class="info">二面</div>
					</li>
					<li class="list">
						<div class="title">層 戶</div>
						<div class="info">2戶 / 層</div>
					</li>
					<li class="list full">
						<div class="title">汽車位</div>
						<div class="info">
							<div>平面(內含於租金內)</div>
							<div>機械(另計每月2500元)</div>
						</div>
					</li>
					<li class="list full">
						<div class="title">管理費</div>
						<div class="info">5000元/月</div>
					</li>
					<li class="list full">
						<div class="title">安全管理</div>
						<div class="info">物業保全公司‧門禁刷卡‧監視系統</div>
					</li>
				</ul>
			</section>
			<!-- 設備提供 -->
			<section class="base_data equipment_provided">
				<h3 class="caption">設備提供</h3>
				<ul class="list_wrap">
					<li class="list full">
						<div class="title">傢俱設備</div>
						<div class="info">
							<ol>
								<li class="item" title="床組"><i class="device_icon icon_01"></i>床組</li>
								<li class="item" title="衣櫃"><i class="device_icon icon_02"></i>衣櫃</li>
								<li class="item" title="梳妝台"><i class="device_icon icon_03"></i>梳妝台</li>
								<li class="item" title="書桌椅"><i class="device_icon icon_04"></i>書桌椅</li>
								<li class="item" title="餐桌椅"><i class="device_icon icon_05"></i>餐桌椅</li>
								<li class="item" title="沙發"><i class="device_icon icon_06"></i>沙發</li>
								<li class="item" title="浴缸"><i class="device_icon icon_07"></i>浴缸</li>
							</ol>
						</div>
					</li>
					<li class="list full">
						<div class="title">電器設備</div>
						<div class="info">
							<ol>
								<li class="item" title="冷氣"><i class="device_icon icon_08"></i>冷氣</li>
								<li class="item" title="電視"><i class="device_icon icon_09"></i>電視</li>
								<li class="item" title="冰箱"><i class="device_icon icon_10"></i>冰箱</li>
								<li class="item" title="洗衣機"><i class="device_icon icon_11"></i>洗衣機</li>
								<li class="item" title="烘衣機"><i class="device_icon icon_12"></i>烘衣機</li>
								<li class="item" title="熱水器"><i class="device_icon icon_13"></i>熱水器</li>
							</ol>
						</div>
					</li>
					<li class="list full">
						<div class="title">炊煮設備</div>
						<div class="info">
							<ol>
								<li class="item" title="瓦斯爐"><i class="device_icon icon_14"></i>瓦斯爐</li>
								<li class="item" title="萬用爐"><i class="device_icon icon_15"></i>萬用爐</li>
								<li class="item" title="流理臺"><i class="device_icon icon_16"></i>流理臺</li>
							</ol>
						</div>
					</li>
					</li>
					<li class="list full">
						<div class="title">電器設備</div>
						<div class="info">
							<ol>
								<li class="item" title="網路"><i class="device_icon icon_17"></i>網路</li>
								<li class="item" title="有線電視"><i class="device_icon icon_18"></i>有線電視</li>
								<li class="item" title="衛星電視"><i class="device_icon icon_19"></i>衛星電視</li>
							</ol>
						</div>
					</li>
				</ul>

			</section>
			<!-- 出租條件 -->
			<section class="base_data rental_conditions">
				<h3 class="caption">出租條件</h3>
				<ul class="list_wrap">
					<li class="list">
						<div class="title">最短租期</div>
						<div class="info">12個月</div>
					</li>
					<li class="list clear">
						<div class="title">押金</div>
						<div class="info">2個月租金</div>
					</li>
					<li class="list">
						<div class="title">神龕設置</div>
						<div class="info">不可</div>
					</li>
					<li class="list">
						<div class="title">與房東同住</div>
						<div class="info">否</div>
					</li>
					<li class="list full">
						<div class="title">身份要求</div>
						<div class="info">上班族‧夫妻‧家庭 拒4～7歲幼童‧70歲以上長者</div>
					</li>
					<li class="list full">
						<div class="title">租約公證</div>
						<div class="info">是</div>
					</li>
					<li class="list full">
						<div class="title">入住時間</div>
						<div class="info">隨時</div>
					</li>
				</ul>

			</section>
			<!-- 生活機能 -->
			<section class="base_data life">
				<h3 class="caption">生活機能</h3>
				<ul class="list_wrap">
					<li class="list full">
						<div class="title">學 區</div>
						<div class="info">同安國小‧同德國中</div>
					</li>
					<li class="list full">
						<div class="title">公 園</div>
						<div class="info">中正公園‧莊敬公園</div>
					</li>
					<li class="list full">
						<div class="title">購 物</div>
						<div class="info">家樂福經國店‧頂好超市</div>
					</li>
					<li class="list full">
						<div class="title">醫 療</div>
						<div class="info">敏盛醫院</div>
					</li>
				</ul>
			</section>
			<!-- 交通 -->
			<section class="traffic">
				<h3 class="caption">交通</h3>
				<table class="table">
					<tbody>
					<tr>
						<td>類 別</td>
						<td>站 名</td>
						<td>路 線</td>
					</tr>
					<tr>
						<td><i class="transportation_icon icon_01"></i>公車</td>
						<td>同德十一街口</td>
						<td>免費市民公車</td>
					</tr>
					<tr class="">
						<td><i class="transportation_icon icon_02"></i>客運</td>
						<td>中正藝文特區</td>
						<td>桃客‧首都</td>
					</tr>
					<tr class="">
						<td><i class="transportation_icon icon_03"></i>火車</td>
						<td>桃園站</td>
						<td></td>
					</tr>
					<tr class="">
						<td><i class="transportation_icon icon_04"></i>捷運</td>
						<td>G10</td>
						<td>綠線</td>
					</tr>
					<tr class="">
						<td><i class="transportation_icon icon_05"></i>高鐵</td>
						<td>青埔站</td>
						<td></td>
					</tr>
					</tbody>
				</table>

			</section>
			<!-- 社區介紹 -->
			<section class="base_data community">
				<h3 class="caption">社區介紹</h3>
				<ul class="list_wrap">
					<li class="list">
						<div class="title">戶 數</div>
						<div class="info"></div>
					</li>
					<li class="list">
						<div class="title">建 商</div>
						<div class="info"></div>
					</li>
					<li class="list full">
						<div class="title">休閒公設</div>
						<div class="info">接待大廳‧室內溫水游泳池‧健身房</div>
					</li>
					<li class="list full">
						<div class="title title2">垃圾處理</div>
						<div class="info">垃圾車時段 週一至週五‧下午6：20於121巷口<br>社區垃圾處理室 全天開放</div>
					</li>
				</ul>
			</section>
			<!-- 特色說明 -->
			<section class="characteristic">
				<h3 class="caption">特色說明</h3>
				<div>
					<p>★ 全新典雅裝潢</p>
					<p>★ 獨立陽台、獨立洗衣機</p>
					<p> ■ □ 附傢俱、電，便利生活圈 ～ 吃和玩樂真方便</p>
					<p>★ 位便利生活圈，鄰購物中心及傳統市集商場、綠地公園、近多所優質學區。</p>
					<p>★ 生活機能便利：餐廳小吃、各式商店林立，近大型購物中心及醫療院所僅數分鐘。</p>
					<p>★ 交通便捷-近公車/客運/交流道/車站/捷運等四通八達。</p>
					<p>★ 住戶單純環境幽雅。門聽電梯皆是感應是門禁管制，生活隱私有保障。</p>
				</div>
			</section>
		</div>
		<!-- 右側資訊欄 - start-->
		<div id="element" class="float_data">
			<div class="detailed_data">
				<div class="price">
					<price>18,500<unit> 元/月</unit>
						<div class="cost">(含管理費)</div>
					</price>
				</div>
				<div class="case_number">
					<div class="title">案號</div>
					<div class="info">3A617</div>
				</div>

				<ul class="list_wrap base_data">
					<li class="list">
						<div class="title">型 態</div>
						<div class="info">電梯大樓</div>
					</li>
					<li class="list clear">
						<div class="title">類 別</div>
						<div class="info">整層住家</div>
					</li>
					<li class="list full">
						<div class="title">格 局</div>
						<div class="info">3房<i></i>2廳<i></i>2衛<i></i>1室<i></i>2陽台</div>
					</li>
					<li class="list full">
						<div class="title">樓 層</div>
						<div class="info">3F / 23F</div>
					</li>
					<li class="list">
						<div class="title">權 狀</div>
						<div class="info">38.68坪</div>
					</li>
					<li class="list">
						<div class="title">使 用</div>
						<div class="info">32.68坪</div>
					</li>
					<li class="list">
						<div class="title">更新日期</div>
						<div class="info">今日</div>
					</li>
					<li class="list">
						<div class="title">有效期限</div>
						<div class="info">2020-09-09</div>
					</li>
				</ul>
				<div class="user_info">
					<div class="user_img">
						<img src="img/house_search_icon_user.svg" alt=""/>
					</div>
					<div class="user_content">
						<p><span>郝旺旺</span> 先生 (收取服務費)</p>
						<p>生活樂租 藝文直營店</p>
						<div class="category">
							<p>仲介業<i></i>租管業</p>

						</div>
						<div class="company">生活資產管理股份有限公司</div>

						<div class="phone_info">
							<div class="number"><img src="img/house_search_phone_01.svg" alt=""/>0939-688089
							</div>
							<div class="number"><img src="img/house_search_phone_02.svg" alt=""/>03-358-1098
							</div>
						</div>

						<!-- 彈窗 - strat -->
						<div class="modal_btn text-center">
							<div class="btn_wrap">
								<a class="" href="#modal-container1" role="button" data-toggle="modal">
									留言
								</a>
								<a class="" href="#modal-container1" role="button" data-toggle="modal">
									預約賞屋
								</a>
							</div>


							<!-- 留言 -->
							<div class="modal fade" id="modal-container1" role="dialog"
								 aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="myModalLabel">
												留言回電 | 賞屋預約
											</h5>
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">×</span>
											</button>

											<hr>
										</div>
										<div class="modal-body">
											<div class="content_wrap">
												<div class="content">

													<div class="form">
														<div class="form_list">
															<div class="floating frist">
																<img class="icon" src="img/info_1.svg">
																<input id="input__username"
																	   class="floating__input" name="username"
																	   type="text" placeholder="Username"/>
																<label for="input__username"
																	   class="floating__label" data-content="姓 名">
																				<span class="hidden--visually">
																					姓 名</span></label>
															</div>
															<div class="radio_wrap">
																<div class="radio">
																	<input type="radio" name="optionsRadios"
																		   id="" value="option1" checked>小 姐
																</div>
																<div class="radio">
																	<input type="radio" name="optionsRadios"
																		   id="" value="option2">先 生
																</div>
															</div>
														</div>

														<div class="form_list">
															<div class="floating">
																<img class="icon" src="img/info_2.svg">
																<input class="floating__input" name="text"
																	   type="text" placeholder="Password"/>
																<label for="input__password"
																	   class="floating__label" data-content="手 機">
																	<span class="hidden--visually">手 機</span>
																</label>
															</div>
															<div class="floating no_required phone">
																<img class="icon" src="img/info_4.svg">
																<input class="floating__input" name="text"
																	   type="text" placeholder="Password"/>
																<label for="input__password"
																	   class="floating__label" data-content="市 話">
																	<span class="hidden--visually">市 話</span>
																</label>
															</div>
														</div>

														<div class="form_list">
															<div class="floating full">
																<img class="icon" src="img/info_3.svg">
																<input class="floating__input" name="text"
																	   type="text" placeholder="Password"/>
																<label for="input__password"
																	   class="floating__label"
																	   data-content="E-Mail">
																	<span class="hidden--visually">E-Mail</span>
																</label>
															</div>
														</div>

														<div class="contact_time_wrap">

															<div class="caption">方便聯絡時間 <i></i></div>


															<div class="radio">
																<input type="radio" name="optionsRadios" id=""
																	   value="option1" checked>上午

															</div>
															<div class="radio">
																<input type="radio" name="optionsRadios" id=""
																	   value="option2">下午

															</div>
															<div class="radio">
																<input type="radio" name="optionsRadios" id=""
																	   value="option3">晚上
															</div>
															<div class="radio">
																<input type="radio" name="optionsRadios" id=""
																	   value="option3">皆可
															</div>
														</div>


														<div class="m_b_20"></div>

														<div class="message">
															<div class="caption">留言 <span>(限100個字)</span>
															</div>
															<!-- <ul class="message_info">
																<li></li>
															</ul> -->
															<textarea class="form-control"
																	  aria-label="With textarea"></textarea>
														</div>


														<div class="order_time_wrap">
															<div class="caption">預約賞屋時間 <i></i>
															</div>
															<div class="radio_wrap">
																<!-- <input class="form-control" type="text"
																	placeholder="年">
																<input class="form-control" type="text"
																	placeholder="月">
																<input class="form-control" type="text"
																	placeholder="日"> -->
																<div class="border">(限一週內)</div>
																<div class="radio">
																	<input type="radio" name="optionsRadios"
																		   id="" value="option1" checked>上午
																</div>
																<div class="radio">
																	<input type="radio" name="optionsRadios"
																		   id="" value="option2">下午

																</div>
																<div class="radio">
																	<input type="radio" name="optionsRadios"
																		   id="" value="option3">晚上
																</div>
																<div class="radio">
																	<input type="radio" name="optionsRadios"
																		   id="" value="option3">皆可
																</div>
															</div>
														</div>


													</div>
												</div>
											</div>

										</div>
										<div class="agree">
											<label><input type="checkbox" value="">
												我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
										</div>

										<div class="modal-footer">
											<button type="button" class="btn" data-dismiss="modal">
												送 出
											</button>
										</div>
									</div>
								</div>

							</div>

						</div>
						<!-- 彈窗 - end -->
						<div class="qr_code">
							<img src="img/qr_code.jpg" alt=""/>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- 右側資訊欄 - end-->
		<div class="m_b_30"></div>
	</div>
	<!-- 底部滿版區塊 -->
	<div class="full_block">
		<hr>
		<!-- 地圖 -->
		<section class="map">
			<h3 class="caption">位置及生活機能地圖</h3>
			<iframe class="google_map"
					src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.5134670695797!2d121.29601925034929!3d25.01664308390189!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34681fb06736ab0f%3A0x9be0cfbe531a8a5a!2zMzMw5qGD5ZyS5biC5qGD5ZyS5Y2A5Lit5q2j6Lev!5e0!3m2!1szh-TW!2stw!4v1577324315566!5m2!1szh-TW!2stw"
					frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</section>

		<!-- 推薦 -->
		<h3 class="caption">其他人也看了下列週邊物件推薦</h3>

		<div class="commodity_list">
            {foreach from=$arr_house key=i item=house}{include file='../../house/house_item_list.tpl'}{foreachelse}{/foreach}
		</div>
	</div>
</div>