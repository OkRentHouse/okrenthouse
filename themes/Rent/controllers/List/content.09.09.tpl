{$breadcrumbs_htm}
<div class="big_box">
{*    {include file='../../share/share.tpl'}*}
{*	<h2><i class="pic"><img src="{$THEME_URL}/img/icon/pic.svg"></i>吉屋快搜</h2>*}
    {include file='../../house/search_house3.tpl'}
	<div class="list">
        {foreach from=$arr_house key=i item=house}
            {include file='../../house/house_list_item2.tpl'}
            {foreachelse}
        {/foreach}
	</div>
    {include file='../../pagination.tpl'}
</div>
<!-- 留言 -->
<div class="modal fade" id="modal-container1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <input type="hidden" name="id_rent_house">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal_title1" id="myModalLabel">
                    留言 | 回電
                </div>
                <div class="modal_title2">
                    <label for="">案 名</label>
                    <input type="text" name="case_name">
                    <label2 for="">案 號</label2>
                    <input type="text" name="rent_house_code">
                </div>
            </div>
            <div class="modal-body">
                <div class="content_wrap">

                    <div class="content">

                        <div class="form">
                            <div class="row list">
                                <div class="col-md-5 name_frame">
                                    <div class="floating">
                                        <i class="fas fa-user"></i>
                                        <input  data-role="none" class="floating_input name "
                                               name="name" type="text" placeholder="姓 名" value="" minlength="2"
                                               maxlength="20" required="">
                                    </div>
                                </div>

                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="radio_wrap">

                                        <div class="radio">
                                            <input  type="radio" name="sex_1" value="0" checked="checked">
                                            <label class="caption" for="miss">
                                                女 士
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input  type="radio" name="sex_1" value="1">
                                            <label class="caption" for="mister">
                                                先 生
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-mobile-alt"></i>
                                        <input  data-role="none" class="floating_input" name="phone"
                                               type="text" placeholder="手 機" value="" maxlength="20"
                                               required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-phone-volume"></i>
                                        <input  data-role="none" class="floating_input" name="tel"
                                               type="text" placeholder="市 話" value="" maxlength="20"
                                               required="">
                                    </div>
                                </div>
                            </div>

                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="far fa-envelope"></i>
                                        <input  data-role="none" class="floating_input " name="email"
                                               type="text" placeholder="電 郵" value="" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="contact_time_wrap">
                                        <div class="caption">方便聯絡時間</div>
                                        <div class="radio">
                                            <input checked="checked" name="contact_time_1" type="radio"
                                                   value="0">早上
                                        </div>
                                        <div class="radio"><input name="contact_time_1" type="radio"
                                                                  value="1">下午
                                        </div>
                                        <div class="radio"><input name="contact_time_1" type="radio"
                                                                  value="2">晚上
                                        </div>
                                        <div class="radio"><input name="contact_time_1" type="radio"
                                                                  value="3">其他
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="message">
                                <div class="caption">
                                    <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                    留言 ( 限100個字內 )</div>
                                <textarea class="form-control" aria-label="With textarea" id="message"
                                          name="message"></textarea>
                            </div>

                            <div class="agree">
                                <label><input type="checkbox" name="agree" value="1">
                                    我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" onclick="push_message('1');">
                    確定送出
                </button>
            </div>
        </div>
    </div>
</div>

<!-- 預約賞屋 -->
<div class="modal fade" id="modal-container2" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <input type="hidden" name="id_rent_house">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal_title1" id="myModalLabel">
                    留言 | 回電
                </div>
                <div class="modal_title2">
                    <label for="">案 名</label>
                    <input type="text" name="case_name">
                    <label2 for="">案 號</label2>
                    <input type="text" name="rent_house_code">
                </div>
            </div>
            <div class="modal-body">
                <div class="content_wrap">

                    <div class="content">

                        <div class="form">
                            <div class="row list">
                                <div class="col-md-5 name_frame">
                                    <div class="floating">
                                        <i class="fas fa-user"></i>
                                        <input  data-role="none" class="floating_input name "
                                               name="name" type="text" placeholder="姓 名" value="" minlength="2"
                                               maxlength="20" required="">
                                    </div>
                                </div>

                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="radio_wrap">

                                        <div class="radio">
                                            <input  type="radio" name="sex_2" value="0" checked="checked">
                                            <label class="caption" for="miss">
                                                女 士
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input  type="radio" name="sex_2" value="1">
                                            <label class="caption" for="mister">
                                                先 生
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-mobile-alt"></i>
                                        <input  data-role="none" class="floating_input" name="phone"
                                                type="text" placeholder="手 機" value="" maxlength="20"
                                                required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-phone-volume"></i>
                                        <input  data-role="none" class="floating_input" name="tel"
                                                type="text" placeholder="市 話" value="" maxlength="20"
                                                required="">
                                    </div>
                                </div>
                            </div>

                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="far fa-envelope"></i>
                                        <input  data-role="none" class="floating_input " name="email"
                                                type="text" placeholder="電 郵" value="" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="contact_time_wrap">
                                        <div class="caption">方便聯絡時間</div>
                                        <div class="radio">
                                            <input checked="checked" name="contact_time_2" type="radio"
                                                   value="0">早上
                                        </div>
                                        <div class="radio"><input name="contact_time_2" type="radio"
                                                                  value="1">下午
                                        </div>
                                        <div class="radio"><input name="contact_time_2" type="radio"
                                                                  value="2">晚上
                                        </div>
                                        <div class="radio"><input name="contact_time_2" type="radio"
                                                                  value="3">其他
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="order_time_wrap">
                                <div class="caption">預約賞屋時間 | <span>(限一週內)</span></div>
                                <div class="radio_wrap">
                                    <input class="form-control" name="year" type="text" placeholder="年">
                                    <input class="form-control" name="mom" type="text" placeholder="月">
                                    <input class="form-control" name="day" type="text" placeholder="日">
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="0"
                                               checked>上午
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="1">下午

                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="2">晚上
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="appointment_period_2" id="" value="3">皆可
                                    </div>
                                </div>
                            </div>
                            <div class="message">
                                <div class="caption">
                                    <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                    留言 ( 限100個字內 )</div>
                                <textarea class="form-control" aria-label="With textarea" id="message"
                                          name="message"></textarea>
                            </div>

                            <div class="agree">
                                <label><input type="checkbox" name="agree" value="1">
                                    我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" onclick="push_message('2');">
                    確定送出
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-container3" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <input type="hidden" name="id_rent_house">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal_title1" id="myModalLabel">
                    留言 | 回電
                </div>
                <div class="modal_title2">
                    <label for="">案 名</label>
                    <input type="text" name="case_name">
                    <label2 for="">案 號</label2>
                    <input type="text" name="rent_house_code">
                </div>
            </div>
            <div class="modal-body">
                <div class="content_wrap">
                    <div class="content">
                        <div class="form">
                            <div class="row list">
                                <div class="col-md-5 name_frame">
                                    <div class="floating">
                                        <i class="fas fa-user"></i>
                                        <input  data-role="none" class="floating_input name "
                                                name="name" type="text" placeholder="姓 名" value="" minlength="2"
                                                maxlength="20" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="radio_wrap">
                                        <div class="radio">
                                            <input  type="radio" name="sex_3" value="0" checked="checked">
                                            <label class="caption" for="miss">
                                                女 士
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input  type="radio" name="sex_3" value="1">
                                            <label class="caption" for="mister">
                                                先 生
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-mobile-alt"></i>
                                        <input  data-role="none" class="floating_input" name="phone"
                                                type="text" placeholder="手 機" value="" maxlength="20"
                                                required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="fas fa-phone-volume"></i>
                                        <input  data-role="none" class="floating_input" name="tel"
                                                type="text" placeholder="市 話" value="" maxlength="20"
                                                required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row list">
                                <div class="col-md-5">
                                    <div class="floating">
                                        <i class="far fa-envelope"></i>
                                        <input  data-role="none" class="floating_input " name="email"
                                                type="text" placeholder="電 郵" value="" required="">
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="contact_time_wrap">
                                        <div class="caption">方便聯絡時間</div>
                                        <div class="radio">
                                            <input checked="checked" name="contact_time_3" type="radio"
                                                   value="0">早上
                                        </div>
                                        <div class="radio"><input name="contact_time_3" type="radio"
                                                                  value="1">下午
                                        </div>
                                        <div class="radio"><input name="contact_time_3" type="radio"
                                                                  value="2">晚上
                                        </div>
                                        <div class="radio"><input name="contact_time_3" type="radio"
                                                                  value="3">其他
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="message">
                                <div class="caption">
                                    <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                    留言 ( 限100個字內 )</div>
                                <textarea class="form-control" aria-label="With textarea" id="message"
                                          name="message"></textarea>
                            </div>
                            <div class="agree">
                                <label><input type="checkbox" name="agree" value="1">
                                    我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" onclick="push_message('3');">
                    確定送出
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    {$js}
    </script>