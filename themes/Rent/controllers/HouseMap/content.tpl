{$breadcrumbs_htm}
<div class="big_box">
    {include file='../../house/search_house3.tpl'}
	<div id="map" class="house_map ajax_map" data-lat="{$google_map.lat}" data-lng="{$google_map.lng}" data-zoom="{$google_map.zoom}" data-minZoom="{$google_map.minZoom}" data-maxZoom="{$google_map.maxZoom}" data-url="{$google_map.url}"></div>
</div>
	<script>
		var map;

		function initMap() {
			var data_window = [];
			var myLatLng = {
					lat:{$google_map.lat},
					lng: {$google_map.lng}
					};

			var map = new google.maps.Map(document.getElementById('map'), {
				center: myLatLng,
				zoom : {$google_map.zoom},
			});
			var info_config = {$info_config};
			var marker_config = {$marker_config};
			var info_config_html = {$info_config_html};

			for (var x= 0; info_config.length >x ; x++) {
				var contentString ='';
				contentString = '<div class="map_marker">' +
						'<div class="photo_row">' +
						'<img src="'+info_config[x]["img"]+'" class="img" alt="'+info_config[x]["rent_house_title"]+'">' +
						 info_config[x]["favorite_html"] +
						'</div>' +
						'<div class="data">' +
						'<h3 class="title">'+info_config[x]["case_name"]+'</h3>' +
						'<div>'+info_config[x]["type_title"]+'<i></i>'+info_config[x]["ping"]+'坪<i></i>'+info_config[x]["floor"]+'</div>' +
						'<div class="address">'+info_config[x]["part_address"]+'</div>' +
						'<price>'+info_config[x]["rent_cash"]+'<unit>元/月</unit></price>' +
						'<br>' +
						 info_config[x]["tag"]+
						'<a class="phone" href="tel:'+info_config[x]["phone"]+'">' +
						'<img src="/themes/Rent/img/icon/house_search_phone_01.svg">'+info_config[x]["phone"] +
						'</a>' +
						'<div class="name">'+info_config[x]["name"]+'</div>' +
						'</div>' +
						'</div>';


				// if(info_config[x]["favorite"]){
				// 	contentString ='<h2><a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+info_config[x]["case_name"]+'</a></h2>'+
				// 	'<span>'+info_config[x]["rent_house_title"]+'</span></br>'+
				// 	'<div class="photo_row">'+
				// 			'<a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+
				// 				'<img src="'+info_config[x]["img"]+'" class="img" alt="'+info_config[x]["rent_house_title"]+'">'+
				// 			'</a>'+
				// 			'<collect data-id="'+info_config[x]["id_rent_house"]+'" data-type="rent_house" class="favorite on">'+
				// 				'<img src="/themes/Rent/img/icon/heart_on.svg">'+
				// 			'</collect>'+
				// 	'<div>';
				// }else{
				// 	contentString ='<h2><a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+info_config[x]["case_name"]+'</a></h2>'+
				// 			'<span>'+info_config[x]["rent_house_title"]+'</span></br>'+
				// 				'<div class="photo_row">'+
				// 					'<a href="/RentHouse?id_rent_house='+info_config[x]["id_rent_house"]+'">'+
				// 						'<img src="'+info_config[x]["img"]+'" class="img" alt="'+info_config[x]["rent_house_title"]+'">'+
				// 					'</a>'+
				// 					'<collect data-id="'+info_config[x]["id_rent_house"]+'" data-type="rent_house" class="favorite">'+
				// 						'<img src="/themes/Rent/img/icon/heart.svg">'+
				// 					'</collect>'+
				// 				'<div>';
				// }
				data_window.push(contentString);
			}

			marker_config.forEach(function (e,i) {

				var infowindow = new google.maps.InfoWindow({
					content: data_window[i]
					// content: info_config_html[i]
				});

				var marker = new google.maps.Marker(e);

				marker.addListener('click', function() {
					infowindow.open(map, marker);
				});

			})
		}




	</script>
{*	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c&callback=initMap">*}
{*	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c&callback=initMap" async="" defer=""></script>*}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&callback=initMap" async="" defer=""></script>
	</script>
