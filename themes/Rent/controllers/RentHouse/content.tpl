 <!-- 麵包屑 -->
 {$breadcrumbs_htm}
    <div class="house_search_view_postion">
        <div class="house_search_view">
          <!-- <svg style="cursor: pointer;" onclick="share('FB')" class="svg-inline--fa fa-share-alt fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="share-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M352 320c-22.608 0-43.387 7.819-59.79 20.895l-102.486-64.054a96.551 96.551 0 0 0 0-41.683l102.486-64.054C308.613 184.181 329.392 192 352 192c53.019 0 96-42.981 96-96S405.019 0 352 0s-96 42.981-96 96c0 7.158.79 14.13 2.276 20.841L155.79 180.895C139.387 167.819 118.608 160 96 160c-53.019 0-96 42.981-96 96s42.981 96 96 96c22.608 0 43.387-7.819 59.79-20.895l102.486 64.054A96.301 96.301 0 0 0 256 416c0 53.019 42.981 96 96 96s96-42.981 96-96-42.981-96-96-96z"></path></svg> -->


          <svg onmouseover="$('.qr_code').show()" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="qrcode" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-qrcode fa-w-14 fa-2x" style="
    width: 12pt;
    vertical-align: top;
    color: #aaa;
    margin-right: 10px;
">
            <path fill="currentColor" d="M0 224h192V32H0v192zM64 96h64v64H64V96zm192-64v192h192V32H256zm128 128h-64V96h64v64zM0 480h192V288H0v192zm64-128h64v64H64v-64zm352-64h32v128h-96v-32h-32v96h-64V288h96v32h64v-32zm0 160h32v32h-32v-32zm-64 0h32v32h-32v-32z" class=""></path></svg>
          <div class="qr_code" style="
    position: absolute;
    top: -30px;
    width: 100px;
    left: -110px;
    display: none;
">
              <img style="
    width: 100%;
" src="/themes/Rent/img/renthouse/qr_code.jpg" alt="" />
          </div>
            瀏覽數 <span class="view_number">{$browse["total"]}</span> ( <img src="/themes/Rent/img/renthouse/house_search_pc_01.svg"> {$browse["pc"]} | <img
                    src="/themes/Rent/img/renthouse/house_search_pc_02.svg"> {$browse["phone"]} )
        </div>
    </div>
    <div class="clear"></div>
 <!-- <div class="main_caption">
     <h3>{$arr_house.case_name}</h3>
 </div>
 <div class="main_caption">
     <h3>{$arr_house.rent_address}</h3>
 </div> -->
 <span class="top_title">

   <label>{$arr_house.case_name}</label><span class="sec_title"><label>{$arr_house.rent_house_title}</label></span><label style="display:none">{$arr_house.rent_address}</label></span>
 <div class="house_search">
     <div id="content" class="data_wrap">
         <div class="object_view">
{*             <div id="preview" class="spec-preview"> <span class="jqzoom"><img jqimg="/themes/Rent/img/renthouse/object/img_01.jpg"*}
{*                                                                               src="/themes/Rent/img/renthouse/object/img_01.jpg" /></span> </div>*}
             <div id="preview" class="spec-preview"> <span class="jqzoom"><img jqimg="{$img[0]}" src="{$img[0]}" style="width: 100%;height: 600px;" /></span> </div>
             <!--縮圖開始-->
             <input type="hidden" id="jqimg_s" value="2">
             <div class="spec-scroll"> <a class="prev">&lt;</a> <a class="next">&gt;</a>
                 <div class="items">
                     <ul>
                         {foreach from=$img key=k item=v}
                             <li><img alt="" bimg="{$v}" src="{$v}" onmousemove="preview(this);">
                             </li>
                         {/foreach}
                     </ul>
                 </div>
             </div>
             <!--縮圖結束-->
             <div class="clear"></div>
         </div>

         <section class="characteristic">
                {$arr_house.features}
         </section>
         <section class="devices base_data equipment_provided">
             <h3 class="caption"><span>設備提供</span></h3>
             {$list_html}
         </section>
         <!-- 出租條件 -->
         <section class="base_data rental_conditions">
             <h3 class="caption"><span>出租條件</span></h3>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">性別</div>
                     <div class="info">{if empty($arr_house.other_conditions["26"]) && empty($arr_house.other_conditions["27"])}
                     不限{else}{$arr_house.other_conditions["26"]} {$arr_house.other_conditions["27"]}{/if}</div>
                 </li>
                 <li class="list clear">
                     <div class="title">身份</div>
                     <div class="info">{if empty($arr_house.other_conditions["28"]) && empty($arr_house.other_conditions["29"]) && empty($arr_house.other_conditions["41"])}
                         不限{else}{$arr_house.other_conditions["28"]} {$arr_house.other_conditions["29"]} {$arr_house.other_conditions["41"]}{/if}</div>
{*                     <div class="info">上班族‧夫妻‧家庭</div>*}
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">短租</div>
                     <div class="info">{if !empty($arr_house.m_s) && !empty($arr_house.m_e)}{$arr_house.m_s}個月~{$arr_house.m_e}個月
                         {elseif !empty($arr_house.m_s)}{$arr_house.m_s}個月
                         {elseif !empty($arr_house.m_e)}{$arr_house.m_e}個月
                         {else}不可{/if}</div>
                 </li>
                 <li class="list clear">
                     <div class="title">押金</div>
                     <div class="info">{if !empty($arr_house.deposit)}{$arr_house.deposit}個月押金{else}無押金{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">神龕</div>
                     <div class="info">{if $arr_house.other_conditions["8"]}可設置{else}不可{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">公證</div>
                     <div class="info">{if $arr_house.other_conditions["24"]}是{else}否{/if}</div>
                 </li>
                 <li class="list clear">
                     <div class="title">寵物</div>
                     <div class="info">{if $arr_house.other_conditions["5"]}可{else}不可{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">與房東同住</div>
                     <div class="info">{if $arr_house.other_conditions["25"]}是{else}否{/if}</div>
                 </li>
                 <li class="list clear">
                     <div class="title">入住時間</div>
                     <div class="info">{if !empty($arr_house.housing_time_s) && !empty($arr_house.housing_time_e)}{$arr_house.housing_time_s}~{$arr_house.housing_time_e}
                         {elseif !empty($arr_house.housing_time_s) && empty($arr_house.housing_time_e)}{$arr_house.housing_time_s}之後
                         {elseif empty($arr_house.housing_time_s) && !empty($arr_house.housing_time_e)}{$arr_house.housing_time_e}之前
                         {else}隨時{/if}</div>
                 </li>
             </ul>

             {if empty($arr_house.my_house)}
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">年齡限制</div>
                     <div class="info">{if !empty($arr_house.age) && !empty($arr_house.year_old)}拒{$arr_house.age}歲以下幼童‧{$arr_house.age}以上長者
                         {elseif !empty($arr_house.age) && empty($arr_house.year_old)}拒{$arr_house.age}以上長者
                         {elseif empty($arr_house.age) && !empty($arr_house.year_old)}拒{$arr_house.age}歲以下幼童
                         {else}不拘年齡{/if}</div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">行業限制</div>
                     <div class="info">{if $arr_house["limit_industry"]}{$arr_house["limit_industry"]}{else}無{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">營業及工廠登記</div>
                     <div class="info">{if !empty($arr_house.other_conditions["10"]) && !empty($arr_house.other_conditions["11"])}皆可{elseif !empty($arr_house.other_conditions["10"])}可營業登記{elseif !empty($arr_house.other_conditions["11"])}可工廠登記{else}否{/if}</div>
                 </li>
             </ul>
             {/if}
         </section>

         <!-- 社區介紹 -->
         <section class="community">
             <h3 class="caption"><span>社區介紹</span></h3>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">戶 數</div>
                     <div class="info">{if $arr_house.all_num}總戶數:{$arr_house.all_num}戶 {/if}{if $arr_house.household_num}住家:{$arr_house.household_num}戶 {/if}{if $arr_house.storefront_num}店家:{$arr_house.storefront_num}戶 {/if}</div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">建 商</div>
                     <div class="info">{if $arr_house.builders}{$arr_house.builders}{else}無{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list full">
                     <div class="title">休閒公設</div>
                     <div class="info">{if !empty($arr_house.public_utilities["3"])}{$arr_house.public_utilities["3"]}{else}無{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list full">
                     <div class="title title2">垃圾處理</div>
                     <div class="info">{$arr_house.clean_time}</div>
{*                     <div class="info">垃圾車時段 週一至週五‧下午6：20於121巷口</div>*}
{*                     <div class="info">社區垃圾處理室 全天開放</div>*}
                 </li>
             </ul>
         </section>

         <!-- 生活機能 -->
         <section class="life">
             <h3 class="caption"><span>生活機能</span><div class="labels"><label>位置</label><label>學區</label><label>公園</label><label>購物</label><label>醫療</label><label>餐飲</label><label>交通</label></div></h3>

             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">學區</div>
                     <div class="info">{if !empty($arr_house.e_school) && !empty($arr_house.j_school)}{$arr_house.e_school}‧{$arr_house.j_school}
                         {elseif !empty($arr_house.e_school)}{$arr_house.j_school}
                         {elseif empty($arr_house.e_school)}{$arr_house.j_school}
                         {else}無{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list">
                     <div class="title">公園</div>
                     <div class="info">{if $arr_house.park}{$arr_house.park}{else}無{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">購物</div>
                     <div class="info">{if $arr_house.supermarket}{$arr_house.supermarket}{else}無{/if}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <li class="list clear">
                     <div class="title">醫療</div>
                     <div class="info">{if $arr_house.hospital}{$arr_house.hospital}{else}無{/if}</div>
                 </li>
             </ul>
             {if $arr_house.rent_address}
             <iframe class="google_map" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&q={$arr_house.rent_address}&language=zh-TW" frameborder="0" style="border:0;width: 100%;height: 600px;" allowfullscreen="">
             </iframe>
             {else}
             <div class="google_map"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="map-marker-alt-slash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-map-marker-alt-slash fa-w-20 fa-3x"><path fill="currentColor" d="M300.8 502.4c9.6 12.8 28.8 12.8 38.4 0 18.6-26.69 35.23-50.32 50.14-71.47L131.47 231.62c10.71 52.55 50.15 99.78 169.33 270.78zm333.02-44.3L462.41 325.62C502.09 265.52 512 238.3 512 192 512 86.4 425.6 0 320 0c-68.2 0-128.24 36.13-162.3 90.12L45.47 3.37C38.49-2.05 28.43-.8 23.01 6.18L3.37 31.45C-2.05 38.42-.8 48.47 6.18 53.9l588.35 454.73c6.98 5.43 17.03 4.17 22.46-2.81l19.64-25.27c5.42-6.97 4.17-17.02-2.81-22.45zm-263.7-203.81L247 159.13c12.34-27.96 40.01-47.13 73-47.13 44.8 0 80 35.2 80 80 0 25.57-11.71 47.74-29.88 62.29z" class=""></path></svg><br>Opss ! 本物件沒有提供詳細位置 ... <br>歡迎聯繫 <label><a href="tel:+886033581098">03-358-1098</a></label> 由專人為您服務<br>來電請告知物件編號：<label>{$arr_house.rent_house_code}</label></div>
             {/if}
         </section>

         <!-- 地圖 -->
         <section class="map">
             <h3 class="caption"><span>生活機能 / 地圖</h3>
{*             <iframe class="google_map"*}
{*                     src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.5134670695797!2d121.29601925034929!3d25.01664308390189!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34681fb06736ab0f%3A0x9be0cfbe531a8a5a!2zMzMw5qGD5ZyS5biC5qGD5ZyS5Y2A5Lit5q2j6Lev!5e0!3m2!1szh-TW!2stw!4v1577324315566!5m2!1szh-TW!2stw"*}
{*                     frameborder="0" style="border:0;" allowfullscreen=""></iframe>*}
             <iframe class="google_map" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAnn_L1YaAE-nonNtxkRs9FAP5yXXdPOBs&q={$arr_house.rent_address}&language=zh-TW" frameborder="0" style="border:0;" allowfullscreen="">
             </iframe>
             <div id="map"></div>
    <div id="coords"></div>
{*             <iframe class="google_map"*}
{*                     src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyDm7y4HwH2zngRpDBbeKyHec04cwLiQZ3c&location=25.0147685,121.3160043"*}
{*                     frameborder="0" style="border:0;" allowfullscreen=""></iframe>*}
         </section>

         <!-- 交通 -->
         <section class="traffic">
             <h3 class="caption"><span>大眾運輸</span></h3>
             <div class="stops">
               <div class="stop">
                 <ul>
                   <li class="type">公車</li>
                   <li class="list">
                     <div class="stop_name">同德十一街口</div>
                     <ul>
                       {$bus=preg_split("/[.]/",$arr_house.bus)}
                       {for $i=0;$i<sizeof($bus);$i++}
                       <li>{$bus[$i]}</li>
                       {/for}
                     </ul>
                   </li>
                 </ul>


                 <ul>
                   <li class="type">客運</li>
                   <li class="list">
                     <div class="stop_name">中正藝文特區</div>
                     <ul>
                       {$bus=preg_split("/[.]/",$arr_house.passenger_transport)}
                       {for $i=0;$i<sizeof($bus);$i++}
                       <li>{$bus[$i]}</li>
                       {/for}
                     </ul>
                   </li>
                 </ul>


                   <ul>
                     <li class="type">火車</li>
                     <li class="list">
                       <div class="stop_name">桃園站</div>
                       <ul>
                         {$bus=preg_split("/[.]/",$arr_house.train)}
                         {for $i=0;$i<sizeof($bus);$i++}
                         <li>{$bus[$i]}</li>
                         {/for}
                       </ul>
                     </li>
                   </ul>

                 <ul>
                   <li class="type">捷運</li>
                   <li class="list">
                     <div class="stop_name">A9站</div>
                     <ul>
                       {$bus=preg_split("/[.]/",$arr_house.mrt)}
                       {for $i=0;$i<sizeof($bus);$i++}
                       <li>{$bus[$i]}</li>
                       {/for}
                     </ul>
                   </li>
                 </ul>
                 <ul>
                   <li class="type">高鐵</li>
                   <li class="list">
                     <div class="stop_name">桃園站</div>
                     <ul>
                       {$bus=preg_split("/[.]/",$arr_house.high_speed_rail)}
                       {for $i=0;$i<sizeof($bus);$i++}
                       <li>{$bus[$i]}</li>
                       {/for}
                     </ul>
                   </li>
                 </ul>

               </div>
             </div>
             <table class="table table-bordered">
                 <thead>
                     <tr>
                         <th>類別</th>
                         <th>站名</th>
                         <th>路線</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td>公車</td>
                         <td>{$arr_house.bus}</td>
                         <td>{$arr_house.bus_station}</td>
                     </tr>
                     <tr class="table-active">
                         <td>客運</td>
                         <td>{$arr_house.passenger_transport}</td>
                         <td>{$arr_house.passenger_transport_station}</td>
                     </tr>
                     <tr class="table-success">
                         <td>火車</td>
                         <td>{$arr_house.train}</td>
                         <td></td>
                     </tr>
                     <tr class="table-warning">
                         <td>捷運</td>
                         <td>{$arr_house.mrt}</td>
                         <td>{$arr_house.mrt_station}</td>
                     </tr>
                     <tr class="table-danger">
                         <td>高鐵</td>
                         <td>{$arr_house.high_speed_rail}</td>
                         <td></td>
                     </tr>
                 </tbody>
             </table>
         </section>

         <!-- 基本資料 -->
         {$Break_count=3}
         {$i=$Break_count}
         {$Break_code='</ul><ul class="list_wrap">'}
         <section class="base_data">
             <h3 class="caption"><span>基本資料</span></h3>
             <ul class="list_wrap">
                  <li class="list">
                      <div class="title">型態</div>
                      <div class="info">{foreach from=$arr_house.sql_types key=t item=types}{if $t !='0'},{/if}{$types.title}
                          {/foreach}</div>
                  </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                  <li class="list">
                      <div class="title">類別</div>
                      <div class="info">{foreach from=$arr_house.sql_type key=t item=type}{if $t !='0'},{/if}{$type.title}
                          {/foreach}</div>
                  </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">屋齡</div>
                     <div class="info">{if $arr_house.house_old!=''}約{$arr_house.house_old}年{else}暫不提供{/if}</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">權狀坪數</div>
                     <div class="info">{$arr_house.ping}坪</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                    <div class="title">室內坪數</div>
                    <div class="info">0.00坪</div>
                </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">產權登記</div>
                     <!-- <div class="info">{if $arr_house.house_power}有{else}無{/if}</div> -->
                     <div class="info">有</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">隔間材質</div>
                     <div class="info">{if $arr_house.genre}{$arr_house.genre}{else}未隔間{/if}</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">邊間</div>
                     <div class="info">{if $arr_house.is_sideroom==1}{$arr_house.is_sideroom}有{else}無{/if}</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">採光</div>
                     <div class="info">{if $arr_house.lighting_num}{$arr_house.lighting_num}面{else}無採光{/if}</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">戶數</div>
                     <div class="info">{if $arr_house.households}{$arr_house.households}戶{else}未提供{/if}</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">座向</div>
{*                     <div class="info">座東南</div>*}
                     <div class="info">{$arr_house.house_door_seat}</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">汽車位</div>
                     <div class="info">{if $arr_house.cars_have==1}有{else}無{/if}{$arr_house.pfy}
{*                     {foreach $arr_house.cars.cars_park_position as $k => $v}*}
{*                            {if $arr_house.cars.cars_rent.$k!=''}車位停放類型:{$arr_house.cars.cars_type.$k}{if $arr_house.cars.cars_rent_type.$k=='含管理費'}({$arr_house.cars.cars_rent_type.$k}){else}(每{$arr_house.cars.cars_rent_type.$k}{$arr_house.cars.cars_rent.$k}元) &nbsp; {/if}{/if}*}
{*                     {/foreach}*}
                    </div>
{*                        {$arr_house.cars_arr}*}
{*                     <div class="info">平面x1(含於租金內)‧機械x1(另計每月2500元)</div>*}
{*                     <br>*}
{*                     <div class="info_2">平面x1(社區出租 每月3000元)</div>*}
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">管理費</div>
                     <div class="info">{if $house.s1==1}(包含在租金){else}{$arr_house.cleaning_fee}元/{$arr_house.cleaning_fee_cycle}{if $arr_house.cleaning_fee_unit}(單位:{$arr_house.cleaning_fee_unit}){else}{/if}{/if}</div>
                 </li>{$i=$i+1}{if $i%$Break_count==0}{$Break_code}{/if}
                 <li class="list">
                     <div class="title">警衛管理</div>
                     <div class="info">{if !empty($arr_house.public_utilities["3"])}{$arr_house.public_utilities["3"]}{else}無{/if}</div>
                 </li>
             </ul>
         </section>
         <div class="clear"></div>
         <!--<hr>-->
         <!-- 設備提供 -->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->

         <div class="clear"></div>
         <!--<hr>-->
         <!-- 特色說明 -->

         <div class="clear"></div>
     </div>
     <!-- 右側資訊欄 - start-->
     <div id="element" class="user_info_wrap">
         <div class="postion_for_qrcode">
             <!-- QRcode -->

             <div class="main_caption btn_a">
                 {$arr_house.rent_house_code}
             </div>
             <!-- <div class="main_caption">
                  <div class="title" style="display:none">主標</div>
                 <h3>{$arr_house.case_name}</h3>
             </div> -->
             <div class="main_caption">
                 <h3>{$arr_house.title}</h3>
             </div>
             <div class="caption"><span>{$arr_house.rent_cash}</span><span style="color: #000;font-size: xx-small;"> 元/月 </span><span style="color: #999;font-size: xx-small;">{if $arr_house.s1==1 or $arr_house.cars.cars_rent_type.0=='含管理費'}(含{if $arr_house.s1==1}管理費{/if}{if $arr_house.cars_have==1 && $arr_house.s1==1 && $arr_house.cars.cars_rent_type.0=='含管理費'}、{/if}{if $arr_house.cars_have==1 && $arr_house.cars.cars_rent_type.0=='含管理費'}車位{/if}){/if}</span></div>
             <!-- <div class="number_wrap">
                 <li class="number_list">
                     <div class="title">案號</div>
                     <div class="info">{$arr_house.rent_house_code}</div>
                 </li>
             </div> -->

             <ul class="list_wrap">
                 <li class="list_full">
                     <!-- <div class="title">地址</div> -->
                     <div class="info address">{$arr_house.rent_address}</div>
                 </li>
             </ul>

             <ul class="list_wrap">
                 <li class="list">
                     {* <div class="title">型態</div> *}
                     <div class="info">{foreach from=$arr_house.sql_types key=t item=types}{if $t !='0'},{/if}{$types.title}
                         {/foreach}</div>
                 </li>

                 <li class="list clear">
                     {* <div class="title">類別</div> *}
                     <div class="info">{foreach from=$arr_house.sql_type key=t item=type}{if $t !='0'},{/if}{$type.title}
                         {/foreach}</div>
                 </li>
             </ul>
             <ul class="list_wrap">
                 <!-- <li class="list_full"> -->
                     <!-- <div class="title">樓層</div> -->
                     <!-- <div class="info">{if $arr_house.whole_building=='1'}整棟建築{else}{$arr_house.rental_floor} / {$arr_house.floor} F{/if}</div> -->
                 <!-- </li> -->
                 <li class="list_full">
                     <!-- <div class="title">格局</div> -->
                     <div class="info" style="margin-left: -15px;">
                         {$ti=0}
                         {if ($arr_house.t3=='' || $arr_house.t3=='0') && ($arr_house.t4=='' || $arr_house.t4=='0')}{$ti=1}
                             開放空間
                         {/if}
                         {if $arr_house.t3=='' || $arr_house.t3=='0'}{else}&ensp;{$arr_house.t3}<span  class="unit">房</span>{/if}
                         {if $arr_house.t4=='' || $arr_house.t4=='0'}{else}&ensp;{$arr_house.t4}<span  class="unit">廳</span>{/if}
                         {if $arr_house.t5=='' || $arr_house.t5=='0'}{else}&ensp;{$arr_house.t5}<span  class="unit">衛</span>{/if}
                         {if $arr_house.t6=='' || $arr_house.t6=='0'}{else}&ensp;{$arr_house.t6}<span  class="unit">室</span>{/if}
                         {if $arr_house.kitchen=='' || $arr_house.kitchen=='0'}{else}&ensp;{$arr_house.kitchen}<span  class="unit">廚</span>{/if}
                         {if $arr_house.t7=='' || $arr_house.t7=='0'}{else}&ensp;{$arr_house.t7}<span  class="unit">前陽台</span>{/if}
                         {if $arr_house.t8=='' || $arr_house.t8=='0'}{else}&ensp;{$arr_house.t8}<span  class="unit">後陽台</span>{/if}
                         {if $arr_house.t9=='' || $arr_house.t9=='0'}{else}&ensp;{$arr_house.t9}<span  class="unit">房間陽台</span>{/if}
                     </div>
                 </li>
             </ul>
<style>

.info.address {
    font-size: 8pt !important;
    margin-top: -10px;
    font-weight: bold;
}

li.info_50w > div {
    width: 50%;
    margin-top: 10px;
}

li.info_50w {
    display: flex;
}

li.info_50w span.left_title {
  color: #999;
  font-weight: bold;
  text-align: end;
  display: inline-table;
  letter-spacing: 2px;
  font-size: 16px;
  padding-right: 5px;
}

li.info_50w span.right_title {
    font-weight: bold;
    font-size: 16px;
    vertical-align: middle;
}

li.info_50w > div span {
    width: 50%;
    display: -webkit-inline-box;
}

span.e_phone {
    color: var(--main_color1);
    vertical-align: bottom;
}

span.e_phone svg {
    width: 10px;
    fill: var(--main_color1);
    vertical-align: middle;
    margin: 0 10px;
}
span.e_phone .number {
    font-size: 15pt !important;
    letter-spacing: 1px !important;
}
.house_search .user_info_wrap .user_info .text span:nth-child(1) {
    display: inline-grid;
}
.house_search .user_info_wrap .user_info .text span label {
    line-height: 5pt;
    font-size: xx-small;
}
.category_wrap {
    background: yellow;
    border-radius: 0 20px;
}
.category p:nth-child(1) {
    text-align: center;
    font-size: 12pt !important;
}
.category p:nth-child(2) {
    font-size: 11pt !important;
    letter-spacing: -1px;
    font-weight: bold;
    color: var(--main_color1);
}
.category p:nth-child(2) img {
    width: 15px;
}
.house_search .user_info_wrap .box_wrap {
    width: 60%;
}

.house_search .user_info_wrap .box label {
    border-right: 0px solid #999;
    font-size: 10pt;
    padding: 0 5px;
    margin: 5px 0;
    position: relative;
}

.house_search .user_info_wrap .box label:last-child{
    border: 0px;
}

.house_search .user_info_wrap .box p:last-child{
    font-size: 11pt !important;
    margin-top: -4px;
    margin-bottom: 10px;
}

.category p, .box_wrap .box p {
    margin: 4px 0;
}

.category_wrap .category{
    margin-right: 10px;

}

.house_search .user_info_wrap .box {
    min-width: 100%;
}

.house_search .user_info_wrap .box label:first-child {
    padding-left: 0px;
}

.house_search .user_info_wrap .box label:last-child {
    padding-right: 0px;
}

.house_search .user_info_wrap .box label:after {
  border-right: 2px solid #bbb;
    content: " ";
    position: absolute;
    top: 0px;
    right: 0px;
    height: 7pt;
    top: 8px;
}

.house_search .user_info_wrap .box label:last-child:after {
    border-right: 0px solid #999;
    content: " ";

}

.house_search .user_info_wrap .box p {
  text-align: center;
}

.house_search .user_info_wrap .qr_code {
    top: -10px;
    right: -10px;
}

.category_wrap .category:after{
  border: 0px;
}

.modal_btn .btn_wrap {
    background: unset;
    border: 0px;
}

.modal_btn a:after {
    border: 0px;
}

.modal_btn a {
    background: #fff;
    margin: 0px 10px;
    border-radius: 0 10px;
    border: 2px solid #aaa;
    line-height: 2em;
}
</style>
             {$info=array("坪數","屋齡","樓層","當層戶數","採光/邊間","座向","汽車","","機車")}
             {$info_name=array("ping","house_old","floor","rental_floor","lighting_num","house_door_seat","car","","motorcycle")}

             {$info=array("坪數","屋齡","樓層","總樓層","當層戶數","座向","採光","邊間","汽車","","機車")}
             {$info_name=array("ping","house_old","floor","tt_floor","rental_floor","house_door_seat","lighting_num_1","lighting_num_2","car","","motorcycle")}

             {$_house["ping"]=$arr_house.ping}
             {$_house["house_old"]=$arr_house.house_old}
             {$_house["floor"]=$arr_house.rental_floor|cat:" / "|cat:$arr_house.floor|cat:"F"}
             {$_house["floor"]=$arr_house.rental_floor|cat:"F"}
             {$_house["tt_floor"]=$arr_house.floor|cat:"F"}
             {$_house["rental_floor"]=$arr_house.households|cat:"戶"}
             {$_house["lighting_num_1"]=$arr_house.lighting_num|cat:"面"}
             {$_house["lighting_num"]=$arr_house.lighting_num|cat:" / "}
             {if $arr_house.is_sideroom==0}
             {$_house["lighting_num"]=$_house["lighting_num"]|cat:"無"}
             {$_house["lighting_num_2"]="無"}
             {else}
             {$_house["lighting_num"]=$_house["lighting_num"]|cat:"有"}
             {$_house["lighting_num_2"]="有"}
             {/if}
             {$_house["house_door_seat"]=$arr_house.house_door_seat}
             {$_house["car"]="無"}

             {if $arr_house.cars_have==1}
             {if $arr_house.cars.cars_rent_type.0=='含管理費'}
              {$_house["car"]=$arr_house.cars.cars_type.0|cat:"，"|cat:$arr_house.cars.cars_rent_type.0}
             {else}
             {$_house["car"]=$arr_house.cars.cars_type.0|cat:"，另計"|cat:$arr_house.cars.cars_rent.0|cat:"/"|cat:$arr_house.cars.cars_rent_type.0}
             {/if}
             {/if}

             {$_house["motorcycle"]="無"}
             {if $arr_house.motorcycle_have==1}
               {if $arr_house.motorcycle.motorcycle_rent_type.0=='含管理費'}
                {$_house["motorcycle"]=$arr_house.motorcycle.motorcycle_type.0|cat:"，"|cat:$arr_house.motorcycle.motorcycle_rent_type.0}
               {else}
               {$_house["motorcycle"]=$arr_house.motorcycle.motorcycle_type.0|cat:"，另計"|cat:$arr_house.motorcycle.motorcycle_rent.0|cat:"/"|cat:$arr_house.motorcycle.motorcycle_rent_type.0}
               {/if}
             {/if}
             <ul class="info" style="    width: 100%;">
               {for $i=0;$i<sizeof($info);$i=$i+2}
                <li class="info_50w"><div><span class="left_title">{$info[$i]}</span><span class="right_title"{if !$info[$i+1]} style="position: absolute; margin-top: 3px;"{/if}>{$_house[$info_name[$i]]}</span></div>
                  {if $info[$i+1]}
                  <div><span class="left_title">{$info[$i+1]}</span><span class="right_title">{$_house[$info_name[$i+1]]}</span></div></li>
                  {/if}
               {/for}
             </ul>

             <!-- <ul class="list_wrap house_pattern">

             </ul> -->

             <!-- <ul class="list_wrap">
                 <li class="list_full">
                     <div class="title">權狀坪數</div>
                     <div class="info">{$arr_house.ping}坪</div>
                 </li>
                 <li class="list_full">
                     <div class="title">室內坪數</div>
                     <div class="info">{$arr_house.indoor_ping}坪</div>
                 </li>
             </ul> -->
             <ul class="list_wrap">
                 <!-- <li class="list_full">
                     <div class="title">更新時間</div>
                     <div class="info">{$arr_house.update_date}</div>
                 </li> -->
{*                 <li class="list_full">*}
{*                     <div class="title">有效期限</div>*}
{*                     <div class="info">2019-09-09</div>*}
{*                     <div class="info">{if $arr_house.contract_time_e}{$arr_house.contract_time_e}{else}無{/if}</div>*}
{*                 </li>*}
             </ul>
{$arr_house.user_html}
             <!-- 彈窗 - strat -->
             <div class="modal_btn text-center">
                 <div class="btn_wrap">
                     <a class="" href="#modal-container1" role="button" data-toggle="modal">
                         留言
                     </a>
                     <a class="" href="#modal-container2" role="button" data-toggle="modal">
                         預約賞屋
                     </a>
                    {if $arr_house.consultant}
                        <a class="" href="#modal-container3" role="button" data-toggle="modal">
                            呼叫顧問
                        </a>
                    {/if}
                 </div>
                 <div class="clear"></div>
             </div>
         </div>
     </div>
     <!-- 右側資訊欄 - end-->

     <div class="clear"></div>
     <div class="m_b_30"></div>
     <!-- 底部滿版區塊 -->
     <div class="full_block ">
         <div class="clear"></div>
         <!--<hr>-->



         <div class="house_search"><div id="content" class="data_wrap_bot">
         <h3 class="caption"><span>其它人也看了下列週邊物件推薦</span></h3></div></div>
         <section class="commodity_list">

         {foreach from=$commodity_data key=i item=v}{include file='../../house/house_item_list.tpl'}{foreachelse}{/foreach}
         </section>
     </div>
 </div>

 <!-- 彈窗 - start -->
 <!-- 留言 -->
 <div class="modal fade" id="modal-container1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>
                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="{$arr_house.case_name}">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="{$arr_house.rent_house_code}">
                 </div>
             </div>
             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name" name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_1" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>
                                         <div class="radio">
                                             <input  type="radio" name="sex_1" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel"  type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_1" type="radio" value="0">早上
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="1">下午
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio">
                                             <input name="contact_time_1" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>
                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <input  name="verification" type="text" value=""  placeholder="已登入則免驗證"><button type="button" onclick="MessageVerification('1')">進行驗證</button>
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message({$arr_house.id_rent_house},'1');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>
 <!-- 預約賞屋 -->

 <div class="modal fade" id="modal-container2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>

                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="{$arr_house.case_name}">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="{$arr_house.rent_house_code}">
                 </div>
             </div>

             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_2" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>
                                         <div class="radio">
                                             <input  type="radio" name="sex_2" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel" type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_2" type="radio" value="0">早上
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="1">下午
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio"><input name="contact_time_2" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="order_time_wrap">
                                 <div class="caption">預約賞屋時間 | <span>(限一週內)</span></div>
                                 <div class="radio_wrap">
                                     <input class="form-control" name="year" type="text" placeholder="年">
                                     <input class="form-control" name="mom" type="text" placeholder="月">
                                     <input class="form-control" name="day" type="text" placeholder="日">
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="0" checked>上午
                                     </div>

                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="1">下午
                                     </div>
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="2">晚上
                                     </div>
                                     <div class="radio">
                                         <input type="radio" name="appointment_period_2" id="" value="3">皆可
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>

                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message({$arr_house.id_rent_house},'2');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>

 <!-- 呼叫樂租顧問 -->

 <div class="modal fade" id="modal-container3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <div class="modal_title1" id="myModalLabel">
                     留言 | 回電
                 </div>
                 <div class="modal_title2">
                     <label for="">案 名</label>
                     <input type="text" name="case_name" value="{$arr_house.case_name}">
                     <label2 for="">案 號</label2>
                     <input type="text" name="rent_house_code" value="{$arr_house.rent_house_code}">
                 </div>
             </div>

             <div class="modal-body">
                 <div class="content_wrap">
                     <div class="content">
                         <div class="form">
                             <div class="row list">
                                 <div class="col-md-5 name_frame">
                                     <div class="floating">
                                         <i class="fas fa-user"></i>
                                         <input  data-role="none" class="floating_input name " name="name" type="text" placeholder="姓 名" value="" minlength="2" maxlength="20" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="radio_wrap">
                                         <div class="radio">
                                             <input  type="radio" name="sex_3" value="0" checked="checked">
                                             <label class="caption" for="miss">
                                                 女 士
                                             </label>
                                         </div>

                                         <div class="radio">
                                             <input  type="radio" name="sex_3" value="1">
                                             <label class="caption" for="mister">
                                                 先 生
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-mobile-alt"></i>
                                         <input  data-role="none" class="floating_input" name="phone" type="text" placeholder="手 機" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                                 <div class="col-md-1"></div>
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="fas fa-phone-volume"></i>
                                         <input  data-role="none" class="floating_input" name="tel"　type="text" placeholder="市 話" value="" maxlength="20" required="">
                                     </div>
                                 </div>
                             </div>

                             <div class="row list">
                                 <div class="col-md-5">
                                     <div class="floating">
                                         <i class="far fa-envelope"></i>
                                         <input  data-role="none" class="floating_input " name="email" type="text" placeholder="電 郵" value="" required="">
                                     </div>
                                 </div>

                                 <div class="col-md-1"></div>
                                 <div class="col-md-6">
                                     <div class="contact_time_wrap">
                                         <div class="caption">方便聯絡時間</div>
                                         <div class="radio">
                                             <input checked="checked" name="contact_time_3" type="radio" value="0">早上
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="1">下午
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="2">晚上
                                         </div>
                                         <div class="radio"><input name="contact_time_3" type="radio" value="3">其他
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="message">
                                 <div class="caption">
                                     <img class="" src="/themes/Rent/img/msg.svg" alt="">
                                     留言 ( 限100個字內 )</div>
                                 <textarea class="form-control" aria-label="With textarea" id="message" name="message"></textarea>
                             </div>
                             <div class="agree">
                                 <label><input type="checkbox" name="agree" value="1">
                                     我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal" onclick="push_message({$arr_house.id_rent_house},'3');">
                     確定送出
                 </button>
             </div>
         </div>
     </div>
 </div>
 <!-- 彈窗 - end -->

 <script>
     $(document).ready(function () {
         $('.nav .dropdown').hover(function () {
             $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(200);
         }, function () {
             $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(200);
         });
     });
     $($sticky).css("width","30%");
     stickywidth=$($sticky).css("width");

     var $sticky = $('#element');
     $sticky.hcSticky({
         stickTo: '#content',
         stickyClass : 'content_class'

     });
 </script>
