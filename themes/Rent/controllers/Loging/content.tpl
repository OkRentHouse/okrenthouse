<div class="c_spacing reg_con">
<div class="reg_con_1">
   <div class="reg_bg">
      <form action="{if !empty($smarty.get.page)}?page={$smarty.get.page}{/if}" method="post"  enctype="application/x-www-form-urlencoded">
            <div class="text-center"><img src="themes/construct/img/login_b_tit.png"/></div>
            <label for="email" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">帳 號</label>
            <div class="col-lg-12"><input type="email" id="email" class="form-control" name="email" maxlength="100" required value="{$smarty.post.email}" autocorrect="none" placeholder="{l s="請輸入e-mail"}"></div>
            <label for="password" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">密 碼</label>
            <div class="col-lg-12"><input type="password" id="password" class="form-control" name="password" maxlength="20" required placeholder="{l s="請輸入密碼"}"></div>
            <div class="col-lg-12 reg_bot">
                  <div class="text-right">
                        <input type="checkbox" class="panel-body form-gx" id="auto" name="auto_loging" value="1"><label for="auto">自動登入</label>
                  </div>
            </div>
            <div class="col-lg-12"><button type="submit" id="loging" name="AdminLogin" class="submit btn">登 入</button></div>
      </form>
   </div>
</div>
</div>