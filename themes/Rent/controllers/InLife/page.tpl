{if !empty($row_id.title)}
{$breadcrumbs_htm}
    <div class="activity_wrap">
        <div class="banner">
            <img class="" src="{if !empty($row_id.id_file.url)}{$row_id.id_file.url}{else}https://okrent.house/img/in_life_banner_default.jpg{/if}" width="100%">
        </div>
        <div class="title_wrap">
            {$row_id.content}
            <div class="info_wrap">
                <div class="left">
                    <div class="info_list">
                        <div class="caption">時間場次
                        </div>
                        <div class="text">
                            {$row_id.date_s}.{$row_id.time_s} ~ {if !empty($row_id[$row_id.date_e])}{$row_id.date_e}{else}{$row_id.date_s}{/if}.{$row_id.time_e}
                        </div>
                    </div>
                    <div class="info_list">
                        <div class="caption">活動費用
                        </div>
                        <div class="text">
                            {$row_id.cost}
                        </div>
                    </div>
                    <div class="info_list">
                        <div class="caption">主辦單位
                        </div>
                        <div class="text">
                            {$row_id.organizer}
                        </div>
                    </div>
                    <div class="info_list">
                        <div class="caption">協辦單位
                        </div>
                        <div class="text">
                            {$row_id.co_organizer}
                        </div>
                    </div>
                    <form action="" method="post"
                          enctype="application/x-www-form-urlencoded" class="form_wrap">
                        <div class="sign_up_wrap">
                        <div class="title">
                            活動報名
                        </div>
{*                        <div class="form-group">*}
{*                            <div class="floating frist">*}
{*                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/head_m.svg">*}
{*                                <input id="input__username" class="floating__input" name="name" id="name" type="text"*}
{*                                       placeholder="姓 名" />*}
{*                                <label for="input__username" class="floating__label" data-content="姓 名">*}
{*                                                <span class="hidden--visually">*}
{*                                                    姓 名</span></label>*}
{*                            </div>*}
{*                            <div class="radio_wrap">*}
{*                                <div class="radio">*}
{*                                    <input type="radio" name="gender"  value="0" checked>小 姐*}
{*                                </div>*}
{*                                <div class="radio">*}
{*                                    <input type="radio" name="gender"  value="1">先 生*}
{*                                </div>*}
{*                            </div>*}
{*                        </div>*}

                        <div class="form-group">
                            <div class="floating">
                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/phone_01.svg">
                                <input class="floating__input" type="text" name="phone" id="phone" placeholder="手 機" required />
                                <label for="input__text" class="floating__label" data-content="手 機">
                                </label>
                            </div>
{*                            <div class="floating no_required phone">*}
{*                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/phone.svg">*}
{*                                <input class="floating__input" type="text" name="mobile" id="mobile" placeholder="市 話" />*}
{*                                <label for="input__text" class="floating__label" data-content="市 話">*}
{*                                </label>*}
{*                            </div>*}
                        </div>
{*                        <div class="form-group">*}
{*                            <div class="floating full">*}
{*                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/mail.svg">*}
{*                                <input class="floating__input" type="text" name="mail" id="mail" placeholder="e-mail"  />*}
{*                                <label for="input__text" class="floating__label" data-content="e-mail">*}
{*                                </label>*}
{*                            </div>*}
{*                        </div>*}
                        <div class="form-group">
                            <div class="floating">
                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/sessions.svg">
                                <!-- <label class="control-label" for="">報名場次</label> -->
                                <select class="form-control search-form" name="id_in_life_activity" id="id_in_life_activity">
                                    {$option}
                                </select>
                            </div>
                            <div class="floating">
                                <img class="icon" src="https://www.okrent.house/themes/Rent/img/icon/people.svg">
                                <input class="floating__input" type="number" placeholder="人數" id="people" name="people" required />
                                <label for="input__text" class="floating__label" data-content="人數">
                                </label>
                            </div>
                        </div>
{*                        <div class="agree">*}
{*                            <label><input type="checkbox" name="agree" value="1">*}
{*                                我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</label>*}
{*                            <p>報名此活動會加入會員 預設帳號為手機號碼 密碼為簡訊驗證碼 請盡快登入修改密碼</p>*}
{*                        </div>*}
                        <div class="verification">
                            <div class="caption">驗證碼</div>
                            <img class="icon" src="http://lifegroup.house/verification">
                            <div class="floating ">
{*                                <input class="floating__input" type="text" name="verification" id="verification" placeholder="Password" />*}
                                <input type="text" id="verification" class="form-control" name="verification" maxlength="20" required autocomplete="off">
                                <label for="input__text" class="floating__label" data-content="">
                                </label>
                            </div>
                        </div>
                        <div class="btn_wrap">
                            <button class="btn" type="submit"  id="submit{$table}" name="submit{$table}" value="1">送 出</button>
{*                            <button type="button" class="btn" data-dismiss="modal">*}
{*                                送 出*}
{*                            </button>*}
                        </div>
                    </div>
                    </form>


                </div>
                <div class="right">
                    <div class="other_activity">
                        {foreach $row_all as $k => $v}
                            <a href="https://www.okrent.house/in_life?id={$v.id_in_life}">
                                <img src="{$v.img}">
                            </a>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>

{else}
    找不到相關資料
{/if}