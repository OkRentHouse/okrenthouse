<div class="c_spacing reg_con">
    <div class="reg_con_1">
        <div class="home_sign_in">
                        <form action="{if !empty($smarty.get.page)}?page={$smarty.get.page}{/if}" method="post"
                                          enctype="application/x-www-form-urlencoded" class="form_wrap">
                <div class="floating">
                    <i class="fas fa-mobile-alt"></i>
                    <input id="user" name="user" maxlength="100" required="" value="" type="text"
                           placeholder="手機號碼">
                </div>

                <div class="floating">
                    <i class="fas fa-unlock-alt"></i>
                    <input id="password" name="password" maxlength="20" required="" value="" type="password"
                           placeholder="密 碼">
                </div>
                <div class="content">
                    <div class="funtion_wrap flex">
                        <div class="item_l">
                            <input id="auto" type="checkbox" class="panel-body form-gx" name="auto_login"
                                   value="1">
                            <label class="auto_sign_in" for="auto">自動登入</label>
                        </div>
                        <div class="item_r">
                            <a class="forget" href="/forgot">忘記密碼?</a>
                        </div>
                    </div>
                    <br>
                    <div class="text-center">
                        <button class="btn_sign_in" type="submit" id="is_login" name="is_login" href="#">登
                            入</button>
                        <button class="btn_sign_in" ><a href="/Register">註冊</a></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>