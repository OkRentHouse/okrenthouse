{$breadcrumbs_htm}
{include file="$tpl_dir./good_tab.tpl"}
    <div class="special_store_wrap" style="margin: auto;">
        <div class="banner" style="width: 90%;margin: auto;text-align: center;">
            <img class="" src="/themes/Rent/img/store/banner_1210.png">
        </div>

        <div class="roof" style="width: 90%;text-align: center;">

          <div class="b_padding title_banner" style="width: 100%;margin: auto;">
              <form action="#" method="get" id="search_box_big">
                  <div class="search_list_wrap">
                      <div class="search_div">
                          {*            <strong>區域</strong>*}
                          <div class="city_select">
                              <spam class="county_city search">區域<i class="fas fa-chevron-down"></i></spam>
                          </div>
                          <div class="county_div" style="display: none;">
                              {foreach from=$select_county key=i item=county}<spam class="county"><input type="radio" name="county" id="county_{$i}" value="{$county.value}" {if $county.value == $smarty.get.county}checked{/if}><label for="county_{$i}">{$county.title}</label></spam>{/foreach}
                          </div>
                          <div class="city_div" style="display: none;">{foreach from=$arr_city[$smarty.get.county] key=i item=city}<spam class="city"><input type="checkbox" name="city[]" id="city_{$i}" value="{$city}" {if in_array($city, $smarty.get.city)}checked{/if}><label for="city_{$i}">{$city}</label></spam>{/foreach}</div>
                      </div>

                      <div class="search_div">
                          <div class="class_select" id="id_store_type_name">類別<i class="fas fa-chevron-down"></i></div>
                          <div class="category none">
                              <input type="radio" id="id_store_type_all" value=""{if count($smarty.get.id_store_type) == 0} checked{/if}><label for="id_store_type_all" id="id_store_type">全部</label>{foreach from=$select_type key=i item=types}<input type="checkbox" name="id_store_type[]" id="id_store_type_{$types.id_store_type}" value="{$types.id_store_type}" {if in_array($types.id_store_type,$smarty.get.id_store_type)}checked{/if}><label for="id_store_type_{$types.id_store_type}" id="store_type_name_{$types.id_store_type}">{$types.store_type}</label>{/foreach}
                          </div>
                      </div>


                      <div class="search_input"><input type="text" class="field" name="search" id="search" value="{$smarty.get.search}" placeholder="可輸入關鍵字">
                      </div>
  {*                    <button class="close_all search_btn" data-toggle="collapse" aria-expanded="false">*}
  {*                        清除*}
  {*                    </button>*}
                      {*<button class="search_icon" type="submit"><img src="{$THEME_URL}/img/icon/pic_m.svg"></button>*}

                      <button class="search_icon" type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">

                        <path style="fill: #666;" class="st0" d="M20.71,18.8l-6.01-6.01c-0.25-0.16-0.58-0.16-0.82,0.08l-0.17,0.17l-1.97-1.92c2.02-2.56,1.87-6.25-0.49-8.61  C9.93,1.27,8.29,0.62,6.64,0.62c-1.73,0-3.37,0.66-4.61,1.97c-2.55,2.55-2.55,6.66,0,9.21C3.27,13.04,5,13.7,6.64,13.7  c1.39,0,2.83-0.5,3.99-1.44l1.94,1.94l-0.16,0.16c-0.25,0.25-0.25,0.58-0.08,0.82l6.09,6.01c0.16,0.16,0.58,0.16,0.82-0.08l1.4-1.48  C20.96,19.37,20.96,19.04,20.71,18.8z M10.02,10.49c-0.9,0.99-2.14,1.4-3.37,1.4c-1.23,0-2.47-0.41-3.37-1.32  c-1.89-1.89-1.89-4.94,0-6.75c0.9-0.99,2.14-1.4,3.37-1.4c1.23,0,2.39,0.41,3.37,1.32C11.83,5.63,11.83,8.68,10.02,10.49z"/>
                        </svg>
                      </button>

                      {*<button class="search_icon" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82"><img src="{$THEME_URL}/img/icon/map_m.svg"></a></button>*}

                      <button class="search_icon" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="圖層_1" x="0px" y="0px" viewBox="0 0 25 22" style="enable-background:new 0 0 25 22;" xml:space="preserve">

                      <g>
                      	<polygon class="st0" points="0.5,1.8 8.5,5 8.5,13.08 0.5,9.88  "/>
                      	<polygon class="st0" points="0.5,9.8 8.5,13 8.5,21.07 0.5,17.87  "/>
                      	<polygon class="st0" points="16.5,1.85 8.5,5.03 8.5,13.03 16.5,9.85  "/>
                      	<polygon class="st0" points="16.5,9.85 8.5,13.02 8.5,21.02 16.5,17.85  "/>
                      	<polygon class="st0" points="16.5,1.85 24.5,5.03 24.5,13.03 16.5,9.85  "/>
                      	<polygon class="st0" points="16.5,9.85 24.5,13.02 24.5,21.02 16.5,17.85  "/>
                      	<path class="st0" d="M16.6,7.55c1.3,0,2.38,1.08,2.38,2.45c0,1.3-1.08,2.38-2.38,2.38c-1.37,0-2.45-1.08-2.45-2.38   C14.15,8.63,15.23,7.55,16.6,7.55z"/>
                      	<path class="st1" d="M16.6,8.56c0.79,0,1.37,0.65,1.37,1.44s-0.58,1.37-1.37,1.37c-0.79,0-1.44-0.58-1.44-1.37   S15.81,8.56,16.6,8.56z"/>
                      </g>
                      </svg>
                      </a></button>

                  </div>
              </form>
              <script>
                  arr_city = {$arr_city|json_encode:1};
                  {$js}
              </script>

          </div>

            <img src="/themes/Rent/img/store/roof.jpg" alt="" class="">
        </div>
        <div class="store_list_wrap" style="width: 90%;margin: auto;">
            <ul class="">
                {foreach $arr_store as $v => $store}
                    <li class="">
                        <div class="image_frame">
                            <div class="img"><a href="/StoreIntroduction?id={$store.id_store}"><img src="{if !empty($store.img)}{$store.img}{else}{$THEME_URL}/img/default.jpg{/if}"></a><div class="favorite {if $store.favorite}on{/if}" data-id="{$store.id_store}" data-type="store">{if $store.favorite}<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}</div></div>
                        </div>
                        <div class="content_wrap">
                            <div class="caption">{$store.title}<i></i><span class="">{$storestore_type}</span></div>
                            <div class="list">
                                <div class="hot">Hot</div>
                                <i></i>
                                <div class="hot_list">{foreach $store.service as $i => $service}<samp>{$service}</samp>{/foreach}</div>
                            </div>
                            <div class="list">
                                <div class="offer">Offer</div>
                                <i></i>
                                {foreach $store.discount as $i => $discount}<a href="/StoreIntroduction?id={$store.id_store}">{$discount}</a>{/foreach}
                            </div>
                        </div>
                    </li>
                {foreachelse}
                <li class="">
                    查不到相關店家
                </li>
                {/foreach}
            </ul>
        </div>
        {include file='../../pagination.tpl'}
    </div>
