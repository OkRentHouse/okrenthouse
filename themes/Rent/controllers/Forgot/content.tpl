<div class="bg">
	<div class="home_web_register">
		<form action="https://www.okrent.house/Register" method="post"
			  enctype="application/x-www-form-urlencoded" class="form_wrap">
			<div class="row list">
				<div class="col-md-3">
				</div>
				<div class="col-md-6">
					<div class="floating">
						<i class="fas fa-mobile-alt"></i>
						<input id="user" data-role="none" class="floating_input" name="user" type="text"
							   placeholder="手機號碼" value="{$smarty.post.user}" minlength="6" maxlength="20" required >
					</div>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			<div class="row list">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<div class="floating text-center" id="tel_submit">
						<button type="button" id="forgot_pwd" name="forgot_pwd" class="btn ">發送密碼</button>
					</div>
				</div>
				<div class="col-md-3">
					<div class="floating text-center">
						<button type="button" onclick="location.href='/login'" class="btn ">返回登入</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

