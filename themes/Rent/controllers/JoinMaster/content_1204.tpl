{$breadcrumbs_htm}
<div class="join_life_wrap" style="margin: 0 auto;">
    <!-- <div class="item_wrap">
	    <div class="title">加入生活</div>
	    <div class="item_list">
		    <a class="" href="/in_life">活動分享</a>
            <i></i>
            <a class="" href="/WealthSharing">創富分享</a>
            <i></i>
            <a class="" href="/Store">生活好康</a>
            <i></i>
            <a class="" href="/LifeBuy">生活樂購</a>
	    </div>
    </div> -->
    <form action="/JoinMaster"  enctype="multipart/form-data" method="post" id="sumbit_data" >
        <input type="hidden" name="put_sumbit" value="1">
        <div class="applyform">
            <div class="row_1">
                <div class="form_item">
                    <span class="must">*</span>
                    <span class="title">
                        <img src="/themes/Rent/img/Jointalent/head.svg">姓名
                    </span>
                    <input class="inp" type="text" name="name"  maxlength="20" value="{$smarty.session.name}" maxlength="20" placeholder="" required="required">
                </div>
                <div class="form_item">
                    <span class="title">暱稱</span>
                    <input class="inp" type="text" name="nickname" value="{$smarty.session.nickname}" maxlength="20" placeholder="" required="required">
                </div>
                <div class="form_item">
                    <span class="must">*</span>
                    <span class="title">生日</span>
                    <input class="inp" type="date" name="birthday" placeholder=" 　/　/　  " required="required">
                </div>
                <div class="form_item">
                    <span class="must">*</span>
                    <span class="title">身分證字號</span>
                    <input class="inp" type="text" name="identity" maxlength="10" placeholder="" required="required">
                </div>
            </div>
            <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">身分證正面(會自動添加<span style="color:red">僅供申請生活達人之用,不得移為他用</span>之浮水印)
            <br/>
            <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="1" class="get_file">身分證反面(會自動添加<span style="color:red">僅供申請生活達人之用,不得移為他用</span>之浮水印)
            <input type="file" name="id_img[]" required="required" style= "display: none;">
            <input type="file" name="id_img[]" required="required" style= "display: none;">
            <div class="row_2">
                <div class="form_item">
                    <div class="post_ID">
                        <input type="hidden" name="postal" maxlength="6">
                        <span class="must">*</span><span class="title">通訊地址</span>
                        <input class="post_ID_n" name="postal_1" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[1].focus()" onfocus="this.value=''">
                        <input class="post_ID_n" name="postal_2" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[2].focus()" onfocus="this.value=''">
                        <input class="post_ID_n" name="postal_3" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[3].focus()" onfocus="this.value=''">
                        <input class="nobor" value="-">
                        <input class="post_ID_n" name="postal_4" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[4].focus()" onfocus="this.value=''">
                        <input class="post_ID_n" name="postal_5" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[5].focus()" onfocus="this.value=''">
                        <input type="hidden" name="address" maxlength="255">
                        <select class="inp" name="id_county" id="id_county">
                            <option value=""><span>縣市</span></option>
                            {foreach $county as $k => $v}
                            <option value="{$v.id_county}"><span>{$v.county_name}</span></option>
                            {/foreach}
                        </select>
                        <select class="inp" name="id_city" id="id_city">
                            <option><span>鄉鎮區</span></option>
                        </select>
                        <input type="text" class="inp" name="road" placeholder="">
                        <input type="text" class="inp _1em" name="segment" placeholder="">段
                        <input type="text" class="inp _1em" name="lane" placeholder="">巷<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <input type="text" class="inp _1em" name="alley" placeholder="">弄
                      <!-- </div><div class="post_ID"><span class="must">&nbsp</span><span class="title">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span> -->
                        <input type="text" class="inp _1em" name="no" placeholder="">號之
                        <input type="text" class="inp _1em" name="no_her" placeholder="">
                        <input type="text" class="inp _1em" name="floor" placeholder="">樓之
                        <input type="text" class="inp _1em" name="floor_her" placeholder="">
                        <input type="text" class="inp _1em" name="address_room" placeholder="">室
                    </div>
                </div>
            </div>
            <div class="row_3">
                <div class="form_item"><span class="must">*</span>
                    <span class="title"><img src="/themes/Rent/img/Jointalent/telephone.svg">手機1</span>
                    <input class="inp" type="text" name="phone" value="{$smarty.session.user}" placeholder="0912345678" maxlength="20" required="required">
                </div>
                <div class="form_item">
                    <span class="title">
                        <img src="/themes/Rent/img/Jointalent/telephone.svg">手機2
                    </span>
                    <input class="inp" type="text" name="phone_2" maxlength="20" placeholder="0912345678">
                </div>
                <div class="form_item">
                    <span class="must">*</span>
                    <span class="title">
                        <img src="/themes/Rent/img/Jointalent/line.svg">LinelD
                    </span>
                    <input class="inp" type="text" name="line_id" maxlength="32" placeholder="" required="required">
                </div>
                <div class="form_item"><span class="title">
                        <img src="/themes/Rent/img/Jointalent/call.svg">市話</span>
                    <input class="inp" type="text" name="local_calls" maxlength="20" placeholder="02-12345678">
                </div>
            </div>
            <div class="row_4">
                <div class="form_item"><span class="must">*</span>
                    <span class="title">電郵</span>
                    <input class="inp" type="email" name="email" maxlength="128" placeholder="　　@　　　　　　　　"  required="required"></div>
            </div>
            <span class="must">*</span>
            <img src="/themes/Rent/img/Jointalent/p_bank.svg">獎金入款帳目正面<img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="account_img[]" data-value="0" class="get_file">上傳檔案 (會自動添加<span style="color:red">僅供申請生活達人之用,不得移為他用</span>之浮水印)
            <br/>
            <span class="must">*</span>
            <img src="/themes/Rent/img/Jointalent/p_bank.svg">獎金入款帳目反面<img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="account_img[]" data-value="1" class="get_file">上傳檔案 (會自動添加<span style="color:red">僅供申請生活達人之用,不得移為他用</span>之浮水印)
            <input type="file" name="account_img[]" required="required" style= "display: none;">
            <input type="file" name="account_img[]" required="required" style= "display: none;">
            <div class="row_5">
                <div class="form_item">
            <!-- <span class="title">獎金入款帳目</span>
            <p>&nbsp</p> -->
                    <table class="bank_tb">
                        <tr>
                            <td rowspan="2" class="left_title">帳號</td>
                            <td class="row_5_td">
                            <div class="row_5_1">
                                <span class="title">
                                    <select style="width: 10rem!important;" class="inp" name="bank" id="bank">
                                            <option>請選擇您帳號的銀行代碼</option>
                                        {foreach $bank as $k=>$v}
                                            <option value="{$v.text}" data-code="{$v.code}">{$v.text}</option>
                                        {/foreach}
                                    </select>銀行</span>
                                    <span class="title">
                                        <input class="inp" type="text" name="branch" placeholder="" required="required"><span class="title">分行
                                    </span>
                                    <span class="title">銀行代碼：
                                        <input class="inp" type="text" name="bank_code" placeholder="822" maxlength="4" required="required">
                                    </span>
                                    <span class="title"></span>
                                </div>
                                <span class="bank_txt_0">金融機構存款帳號(分行別、科目、編號、檢查號碼)</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="bank_ID">
                                    <table class="bank_ID_tb">
                                        <input type="hidden" name="bank_account" maxlength="32">
                                        <tr>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_1" type="text" maxlength="1" onkeyup="this.value=this.value.toUpperCase();document.getElementsByClassName('bank_ID_n')[1].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_2" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[2].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_3" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[3].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_4" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[4].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_5" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[5].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_6" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[6].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_7" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[7].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_8" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[8].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_9" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[9].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_10" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[10].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_11" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[11].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_12" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[12].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_13" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[13].focus()" onfocus="this.value=''">
                                            </td>
                                            <td>
                                                <input class="bank_ID_n" name="bank_account_14" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[14].focus()" onfocus="this.value=''">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <span class="bank_txt_1">※限用中國信託、國泰世華帳戶非上述2家銀行者須自行負擔每次轉帳之手續費15元</span>
            <!-- <select>
              <option disabled selected>銀行</option>
              <option >國泰世華</option>
              <option >中國信託</option>
            </select> -->
            <!-- <select>
              <option disabled selected>銀行代碼</option>
              <option >013</option>
              <option >004</option>
            </select> -->
            </div>
        </div>
        <div class="row_6">
            <div class="form_item">
                <span>
                    <img src="/themes/Rent/img/Jointalent/speaker.svg">訊息來源
                </span>
                <span>
                    <input type="radio" name="message_source" value="0" checked>友人分享
                </span>
                <span>
                    <input type="radio" name="message_source" value="1" >其他網站APP
                </span>
                <span>
                    <input type="radio" name="message_source" value="2" >人力銀行
                </span>
                <span>
                    <input type="radio" name="message_source" value="3" >舉薦顧問
                    <input class="inp" type="text" name="recommend_id_member" maxlength="20" placeholder="填入後將獲得大集大利紅利點數100點喔!">
                </span>
            </div>
        </div>
        <div class="row_7">
            <div class="form_item" style="display: flex;">
              <!-- <div class="SMS submit"> 發送驗證碼 </div><input class="inp" type="text" placeholder="請輸入手機驗證碼"> -->
            </div>
        </div>
        <div class="submit">
            <img src="\themes\App\mobile\img\expertAdvisor\button_empty_180t.svg" class="submit_img" onclick="submit_data()">
        </div>
        </div>
    </form>
</div>
