    <div class="join_life_wrap step2" style="margin: 0 auto;">
        <div id="step_02" class="step_02">Step1.加入申請
            <img src="/themes/Rent/img/joinmaster/banner_02_arrow_an.svg">
            <span style="color:#3f0d81">Step2.上傳資料</span>
        </div>
        <div class="applyform">
            <div class="row_134">
                <div class="row_1row_1_1">
                    <div class="row_1">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">身分證字號</span>
                            <input class="inp" type="text" name="identity" maxlength="10" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="row_1_1">
                        <div class="attachment">
                            <div class="row_1_1_1">
                                <input class="file_1_1_1" type="file" style="display:none">
                                <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">
                                <span class="get_file">上傳身分證正面</span>
                                <div class="view">僅供申請生活達人之用，不得移為他用</div>
                            </div>
                            <div class="row_1_1_2">
                                <input class="file_1_1_2" type="file" style="display:none">
                                <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">
                                <span class="get_file">上傳身分證反面</span>
                                <div class="view">僅供申請生活達人之用，不得移為他用</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row_3row_4" style="display:none">
                    <div class="row_3">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title"><img src="/themes/Rent/img/Jointalent/telephone.svg" class="svg">手機</span>
                            <input class="inp" type="text" name="phone" value="{$smarty.session.user}" placeholder="0912345678" maxlength="20" required="required">
                        </div>
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">
                                <img src="/themes/Rent/img/Jointalent/line.svg" class="svg">LinelD
                            </span>
                            <input class="inp" type="text" name="line_id" maxlength="32" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="row_4">
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">電郵</span>
                            <input class="inp" type="email" name="email" maxlength="128" placeholder="　　@　　　　　　　　"  required="required">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_2">
                <div class="form_item">
                    <div class="post_ID">
                        <input type="hidden" name="postal" maxlength="6">
                        <span class="must">*</span>
                        <span class="title">獎金撥人帳戶(限本人帳戶)</span>
                    </div>
                </div>
            </div>
            <div class="row_6 form2">
                <div class="form_item">
                    <div class="bank_tb">
                        <table class="bank_tb">
                            <tr>
                                <td class="row_5_td">
                                <div class="row_5_1">
                                    <span class="title">
                                        <select style="width: 10rem!important;" class="inp no_border" name="bank" id="bank">
                                            <option>中國信託</option>
                                            {foreach $bank as $k=>$v}
                                                <option value="{$v.text}" data-code="{$v.code}">{$v.text}</option>
                                            {/foreach}
                                        </select>銀行
                                    </span>
                                    <span class="title">
                                        <input class="inp" type="text" name="branch" placeholder="" required="required" style="border: 1px solid;"><span class="title">分行<a target="_blank" href="/JoinMaster?p=list"><i class="fas fa-external-link-alt"></i></a>
                                    </span>
                                    <span class="title">銀行代碼：
                                        <input class="inp" type="text" name="bank_code" placeholder="822" maxlength="4" required="required">
                                    </span>
                                    <span class="title"></span>
                                </div>
                                <br>
                                <span class="bank_txt_0">金融機構存款帳號(分行別、科目、編號、檢查號碼)</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="bank_ID">
                                        <table class="bank_ID_tb">
                                            <input type="hidden" name="bank_account" maxlength="32">
                                            <tr>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_1" type="text" maxlength="1" onkeyup="this.value=this.value.toUpperCase();document.getElementsByClassName('bank_ID_n')[1].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_2" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[2].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_3" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[3].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_4" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[4].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_5" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[5].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_6" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[6].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_7" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[7].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_8" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[8].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_9" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[9].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_10" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[10].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_11" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[11].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_12" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[12].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_13" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[13].focus()" onfocus="this.value=''">
                                                </td>
                                                <td>
                                                    <input class="bank_ID_n" name="bank_account_14" type="text" maxlength="1" onkeyup="document.getElementsByClassName('bank_ID_n')[14].focus()" onfocus="this.value=''">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row_6_1">
                <div class="attachment">
                    <div class="row_1_1_1">
                        <input class="file_1_1_2" type="file" style="display:none">
                        <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file">
                        <span class="get_file">上傳存摺正面</span>
                        <div class="view">僅供申請生活達人之用，不得移為他用</div>
                    </div>
                    <div class="row_1_1_2">
                        <input class="file_1_1_2" type="file" style="display:none">
                        <img src="/themes/Rent/img/Jointalent/attachment.svg" data-name="id_img[]" data-value="0" class="get_file"><span class="get_file">上傳存摺反面</span>
                        <div class="view">僅供申請生活達人之用，不得移為他用</div>
                    </div>
                </div>
            </div>
            <div class="row_6_1_1">
                <div class="form_item">
                    <div class="post_ID">
                        <input type="hidden" name="postal" maxlength="6">
                        <input type="checkbox" name="i_agree"><span class="title">我已閲讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</span>
                    </div>
                </div>
            </div>
            <div class="row_7">
                <div class="form_item" style="display: flex;">
                    <div class="submit" onclick="sub_per()">
                        上一步
                    </div>
                </div>
                <p>&nbsp</p>
                <div class="form_item" style="display: flex;">
                    <div class="submit" onclick="submit_data()" class="submit_img">
                      送出
                    </div>
                </div>
            </div>
        </div>
    </div>
