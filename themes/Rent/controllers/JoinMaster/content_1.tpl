{$breadcrumbs_htm}
<div class="join_life_wrap step1" style="margin: 0 auto;">
    <div class="item_wrap">
	    <div class="title">加入生活</div>
	    <div class="item_list">
		    <a class="" href="/in_life" style="font-weight: bold;">生活家族</a>
            <i></i>
            <a class="" href="/WealthSharing">加盟體系</a>
            <i></i>
            <a class="" href="/Store">菁英招募</a>
	    </div>
    </div>
    <form action="/JoinMaster"  enctype="multipart/form-data" method="post" id="sumbit_data" >
        <input type="hidden" name="put_sumbit" value="1">
        <img src="/themes/Rent/img/joinmaster/banner_01.svg" class="">
        <!-- <img src="/themes/Rent/img/joinmaster/step_01.svg" class=""> -->
        <div class="step_01"><span style="color:#3f0d81">Step1.加入申請</span> <img src="/themes/Rent/img/joinmaster/banner_01_arrow_an.svg"> Step2.上傳資料</div>
        <div class="applyform">
                  <div class="row_134">
                  <div class="row_1row_1_1">
                    <div class="row_1">
                      <div class="form_item">
                          <span class="must">*</span>
                          <span class="title">
                              <img src="/themes/Rent/img/Jointalent/head.svg" class="svg">姓名
                          </span>
                          <input class="inp no_border" type="text" name="name"  maxlength="20" value="{$smarty.session.name}" maxlength="20" placeholder="" required="required">
                      </div>
                      <div class="form_item">
                          <span class="title">暱稱</span>
                          <input class="inp no_border" type="text" name="nickname" value="{$smarty.session.nickname}" maxlength="20" placeholder="" required="required">
                      </div>

                    </div>
                    <div class="row_1_1">
                      <div class="form_item">
                          <span class="must">*</span>
                          <span class="title">生日</span>


                          <!-- <select class="inp no_border" name="birthday" required="required">{for $i=2020;$i>=1090;$i--}<option>{$i}</option>{/for}</select>年
                          <select class="inp no_border" name="birthday" required="required">{for $i=1;$i<=12;$i++}<option>{$i}</option>{/for}</select>月
                          <select class="inp no_border" name="birthday" required="required">{for $i=1;$i<=31;$i++}<option>{$i}</option>{/for}</select>日 -->
                          <input class="inp no_border" type="text" name="year" maxlength="128" placeholder="" required="required">年
                          <input class="inp no_border" type="text" name="month" maxlength="128" placeholder="" required="required">月
                          <input class="inp no_border" type="text" name="day" maxlength="128" placeholder="" required="required">日

                      </div>
                    </div>
                  </div><div class="row_3row_4"><div class="row_3">
                        <div class="form_item"><span class="must">*</span>
                            <span class="title"><img src="/themes/Rent/img/Jointalent/telephone.svg" class="svg">手機</span>
                            <input class="inp no_border" type="text" name="phone" value="" placeholder="0912345678" maxlength="20" required="required">
                        </div>
                        <div class="form_item">
                            <span class="must">*</span>
                            <span class="title">
                                <img src="/themes/Rent/img/Jointalent/line.svg" class="svg">Line lD
                            </span>
                            <input class="inp no_border" type="text" name="line_id" maxlength="32" placeholder="" required="required">
                        </div>

                    </div>
                    <div class="row_4">
                        <div class="form_item"><span class="must">*</span>
                            <span class="title">電郵</span>
                            <input class="inp no_border" type="email" name="email" maxlength="128" placeholder="　　@　　　　　　　　" required="required"></div>
                    </div>
                    </div></div><div class="row_2">
                        <div class="form_item">
                            <div class="post_ID">
                                <input type="hidden" name="postal" maxlength="6">
                                <span class="must">*</span><span class="title">通訊地址</span>
                                <input class="post_ID_n no_border" name="postal_1" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[1].focus()" onfocus="this.value=''">
                                <input class="post_ID_n no_border" name="postal_2" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[2].focus()" onfocus="this.value=''">
                                <input class="post_ID_n no_border" name="postal_3" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[3].focus()" onfocus="this.value=''">
                                <!-- <input class="nobor" value="-"> -->
                                <input class="post_ID_n no_border" name="postal_4" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[4].focus()" onfocus="this.value=''">
                                <input class="post_ID_n no_border" name="postal_5" type="number" maxlength="1" onkeyup="document.getElementsByClassName('post_ID_n')[5].focus()" onfocus="this.value=''">
                                <input type="hidden" name="address" maxlength="255">
                                <select class="inp no_border" name="id_county" id="id_county">
                                    <option value="">桃園市</option>
                                    {foreach $county as $k => $v}
                                        <option value="{$v.id_county}">{$v.county_name}</option>
                                    {/foreach}
                                </select>
                                <select class="inp no_border" name="id_city" id="id_city">
                                    <option>桃園區</option>
                                </select>
                                <input type="text" class="inp _10em no_border" name="address" placeholder="">
                                <!-- <input type="text" class="inp no_border" name="road" placeholder="">
                                <input type="text" class="inp _1em" name="segment" placeholder="">段
                                <input type="text" class="inp _1em" name="lane" placeholder="">巷<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" class="inp _1em" name="alley" placeholder="">弄
                                </div><div class="post_ID"><span class="must">&nbsp</span><span class="title">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
                                <input type="text" class="inp _1em" name="no" placeholder="">號之
                                <input type="text" class="inp _1em" name="no_her" placeholder="">
                                <input type="text" class="inp _1em" name="floor" placeholder="">樓之
                                <input type="text" class="inp _1em" name="floor_her" placeholder="">
                                <input type="text" class="inp _1em" name="address_room" placeholder="">室 -->
                            </div>
                        </div>
                    </div>



                <div class="row_6">
                    <div class="form_item">
                        <span>
                            <img src="/themes/Rent/img/Jointalent/speaker.svg" class="svg">訊息來源
                        </span>
                        <span>
                            <input type="radio" name="message_source" value="0" checked="">友人分享
                        </span>
                        <span>
                            <input type="radio" name="message_source" value="1">其他網站APP<input type="text" class="inp_bottom_line no_border" name="address_room" placeholder="">
                        </span>
                        <span>
                            <input type="radio" name="message_source" value="2">人力銀行<input type="text" class="inp_bottom_line no_border" name="address_room" placeholder="">
                        </span>
                        <span>
                            <input type="radio" name="message_source" value="3">其他<input type="text" class="inp_bottom_line no_border" name="address_room" placeholder="">
                        </span>
                    </div>
                </div>
                <div class="row_6_1" style="display:none">
                  <span class="must">*</span>
                  <span>
                      <!-- <input type="radio" name="message_source" value="3"> -->
                      舉薦顧問
                      <input class="inp no_border" type="text" name="recommend_id_member" maxlength="20" placeholder="請填入分享代碼">
                      ※填入本欄資訊您將獲得紅利點數200點的獎勵喔!
                  </span>
                </div>
                <div class="row_7">
                    <div class="form_item" style="display: flex;">
                      <!-- <div class="SMS submit"> 發送驗證碼 </div><input class="inp no_border" type="text" placeholder="請輸入手機驗證碼"> -->
                      <div class="submit" onclick="sub_next()">
                          下一步
                      </div>
                    </div>
                </div>
                <!-- <div class="submit">
                    <img src="\themes\App\mobile\img\expertAdvisor\button_empty_180t.svg" class="svg" class="submit_img" onclick="submit_data()">
                </div> -->
                </div>
    </form>
</div>
