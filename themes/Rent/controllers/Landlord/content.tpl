{$breadcrumbs_htm}
<div class="rent_service">
    {include file='../../rent_service/menu.tpl'}
	<div class="frame_wrap">
		<div class="title text-center">{Config::get('landlord_title')}</div>
		<div class="honeycomb">
			<ul class="honeycomb_list_wrap1"> 
                {assign var='j' value=1}
                {foreach from=$row key=i item=item}
                {assign var='k' value=0}
                {if in_array($i, [5, 11, 16, 21])}
                    {assign var='k' value=1}
                {/if}
                {if $j != ($j+$k)}
                {assign var='j' value=$j+1}
			</ul><ul class="honeycomb_list_wrap{$j}">
                {/if}
				<li class="honeycomb_list">
					<a class="hexagontent"{if !empty($item.content)} data-show="service_content{$i}" href="#service_content"{else} href="javascript:void()"{/if}>{$item.title}</a>
				</li>
                {/foreach}
			</ul>
		</div>
		<div class="service_content_wrap" id="service_content">
            {foreach from=$row key=i item=item}
				<div id="service_content{$i}" class="service_content collapse">{$item.content}</div>
            {/foreach}
		</div>
	</div>
</div>
{$Landlord_HTML}
{if !empty($Landlord_CSS)}
	<style>
		{$Landlord_CSS}
	</style>
{/if}
{if !empty($Landlord_JS)}
	<script type="text/javascript">
        {$Landlord_JS}
	</script>
{/if}
