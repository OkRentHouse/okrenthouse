{$breadcrumbs_htm}
<div class="rent_service">
    {include file='../../rent_service/menu.tpl'}
	<div class="frame_wrap">
		<div class="title text-center">{Config::get('tenant_title')}</div>
		<div class="honeycomb">
			<ul class="honeycomb_list_wrap1">
                {assign var='j' value=1}
                {foreach from=$row key=i item=item}
                {assign var='k' value=0}
                {if in_array($i, [5, 11, 16, 22])}
                    {assign var='k' value=1}
                {/if}
                {if $j != ($j+$k)}
                {assign var='j' value=$j+1}
			</ul><ul class="honeycomb_list_wrap{$j}">
                {/if}
				<li class="honeycomb_list">
					<a class="hexagontent"{if !empty($item.content)} data-show="service_content{$i}" href="#service_content"{else} href="javascript:void()"{/if}>{$item.title}</a>
				</li>
                {/foreach}
			</ul>
		</div>
		<div class="service_content_wrap" id="service_content">
            {foreach from=$row key=i item=item}
				<div id="service_content{$i}" class="service_content collapse">{$item.content}</div>
            {/foreach}
		</div>
	</div>
</div>
{$Tenant_HTML}
{if count($commodity_data)}
<div class="house_row">
  <div class="commodity_list commodity_data">
            {foreach from=$commodity_data key=i item=v}{include file='../../house/house_item_list.tpl'}{foreachelse}{/foreach}
  </div>
</div>
{/if}

<div class="banner2" style="margin: 0 auto;">
<ul class="list_wrap">
<li>&nbsp;</li>
</ul>
</div>
<div class="banner" style="margin: 0 auto;">
<h2 class="caption"><span class="title_h">管理查詢</span></h2>
<ul class="list_wrap">
<li class="list1"><a>管理查詢</a></li>
<li class="list2"><a>紅利點數</a></li>
<li class="list3"><a>租約</a></li>
<li class="list4"><a>屋況報修</a></li>
<li class="list5"><a>租期</a></li>
<li class="list6"><a>房屋點交</a></li>
<li class="list7"><a>費用查詢</a></li>
</ul>
</div>
<div class="banner4" style="margin: 0 auto;">
<ul class="list_wrap">
<li>&nbsp;</li>
</ul>
</div>

{if !empty($Tenant_CSS)}
<style>
    {$Tenant_CSS}
</style>
{/if}
{if !empty($Tenant_JS)}
<script type="text/javascript">
    {$Tenant_JS}
</script>
{/if}

<section class="slider_center footers">

    {include file="../../../../../../tpl/group_link.tpl"}

</section>


{$js}
