{$breadcrumbs_htm}
{include file="$tpl_dir./good_tab.tpl"}
{if $smarty.get.id_announcement}
<div class="margin_box">
    <div class="title">
        <h1>{$announcement_arr.title}</h1>
        <div class="good_store">
            <div>發佈時間 {$announcement_arr.create_time}</div>
        </div>
    </div>
    <div class="good_store_warp">
        {$announcement_arr.html_content}
    </div>
</div>
{else}
<div class="goodies_member_wrap">
    <div class="banner">
        <img class="" src="/themes/Rent/img/goodstore/banner.jpg">
    </div>

    <div class="text-center">
        <h2 class="caption"><span><img src="/themes/Rent/img/goodstore/star.svg" alt="" class=""></span>加入好康店家</h2>
    </div>

    <div class="step_main_wrap">
        <div class="step_img step_one">
            <img src="/themes/Rent/img/goodstore/download.svg" alt="">
        </div>

        <div class="step_wrap">
            <div class="list_frame">
                <p class="">到
                    <a target="_blank"
                       href="https://play.google.com/store/apps/details?id=com.ilife_house.life_house&hl=zh_TW"
                       class=""><img src="/themes/Rent/img/goodmember/google_play.png" alt="google_play" class=""></a>
                    下載生活樂租APP或到生活集團體系網站線上完成會員註冊。
                </p>
                <ul class="flex">
                    <li class=""><img src="/themes/Rent/img/goodstore/01.svg" alt="" class=""></li>
                    <i></i>
                    <li class=""><img src="/themes/Rent/img/goodstore/02.svg" alt="" class=""></li>
                    <i></i>
                    <li class=""><img src="/themes/Rent/img/goodstore/03.svg" alt="" class=""></li>
                    <i></i>
                    <li class=""><img src="/themes/Rent/img/goodstore/04.svg" alt="" class=""></li>
                    <i></i>
                    <li class=""><img src="/themes/Rent/img/goodstore/05.svg" alt="" class=""></li>
                    <i></i>
                    <li class=""><img src="/themes/Rent/img/goodstore/06.svg" alt="" class=""></li>
                </ul>

                <div class="text-center">
                    <a href="https://okmaster.life/StoreApplication" class="btn" target="_blank">立馬註冊</a>
                </div>
            </div>

        </div>
    </div>

    <div class="step_main_wrap">
        <div class="step_img step_two">
            <img src="/themes/Rent/img/goodstore/click.svg" alt="">
        </div>

        <div class="step_wrap">
            <div class="list_frame">
                <p class="">
                    登入後至會員中心> in生活 ><img src="/themes/Rent/img/goodstore/life_member.png" alt=""> 上傳好康店家相關訊息，<br>
                    立馬免費曝光。
                </p>
            </div>
        </div>
    </div>

    <div class="step_main_wrap">
        <div class="step_img step_three">
            <img src="/themes/Rent/img/goodstore/spk.svg" alt="">
        </div>

        <div class="step_wrap">
            <div class="list_frame">
                <p class="">
                    店家可自行更換調整好康優惠項目及店家訊息。
                </p>
            </div>
        </div>
    </div>


    <div style="margin-bottom: 130px;"></div>

    <div class="text-center">
        <img src="/themes/Rent/img/goodstore/img_1.jpg" alt="" class="text-center">
    </div>

    <div style="margin-bottom: 50px;"></div>

    <div class="text-center">
        <h2 class="caption"><span><img src="/themes/Rent/img/goodstore/star.svg" alt="" class=""></span>NEWS</h2>
    </div>

    {if !empty($row_all)}
        <input type="hidden" id="count" value="{$row_sql.count_log}">
        <input type="hidden" id="count_now" value="{$compartment}">
        <input type="hidden" id="compartment" value="{$compartment}">
        <ul class="list_wrap" id="add_wrap">
            {foreach from=$row_all key=k item=v}
                <li class="list">
                    <div class="image_wrap">
                        <img src="{$v.img}">
                    </div>
                    <div class="text_wrap">
                        <div class="caption_wrap">
                            <div class="caption">
                                {$v.title}
                            </div>
                            <div class="date">{$v.create_time}</div>
                        </div>
                        {$v.content}<span><a href="/GoodStore?id_announcement={$v.id_announcement}">more</a></span>
                    </div>
                </li>
            {/foreach}
        </ul>
    {/if}
</div>
{/if}

