{$breadcrumbs_htm}
    <div class="join_life_wrap" style="margin: 0 auto;">
      <div class="tm-container mx-auto">
              <section class="tm-section tm-section-1">
                  <div class="container">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="tm-bg-circle-white2 tm-flex-center-v">
                                  <header class="text-center fixed">
                                  </header>

                              </div>
                          </div>
                      </div>
                  </div>
              </section>
              <section id="tm-section-2" class="tm-section pt-2 pb-2">
                  <div class="container">
                      <div class="row">
                          <div class="col-xl-5 col-lg-5 tm-flex-center-v tm-section-left">
                              <div class="col-xl-12 col-lg-12 tm-flex-center-v tm-text-container tm-section-left m300">
                                  <h2 class="tm-color-secondary mb-4 m1000300">生活家族</h2>
                                  <p class="mb-4 m00150">隨時隨地透過分享<br>為自己開創多元的永續收入</p>
                                  <!-- <img src="img/img-01.png" alt="Image" class="rounded-circle tm-circle-img2"> -->
                              </div>
                          </div>
                          <div class="col-xl-7 col-lg-7 tm-circle-img-container tm-contact-right">
                              <div class="map-all">
                                  <div class="map"><a href="/ExpertAdvisor" target="_blank"><img src="/themes/Rent/img/joinlife/1201/m1.jpg" target="_blank" alt="生活達人" style="width:300px;height:auto;"></a> </div>
                                  <div class="map"><a href="https://epro.house/" target="_blank"><img src="/themes/Rent/img/joinlife/1201/m2.jpg" target="_blank" alt="裝修達人" style="width:300px;height:auto;"></a> </div>
                              </div>
                              <div class="map-all">
                                  <div class="map"><a href="https://shar.help/" target="_blank"><img src="/themes/Rent/img/joinlife/1201/m3.jpg" target="_blank" alt="共享圈" style="width:300px;height:auto;"></a> </div>
                                  <div class="map"><a href="https://okpt.life/" target="_blank"><img src="/themes/Rent/img/joinlife/1201/m4.jpg" target="_blank" alt="好幫手" style="width:300px;height:auto;"></a> </div>
                              </div>

                          </div>
                      </div>
                  </div>
              </section>
              <section id="tm-section-3" class="tm-section tm-section-3">
                  <div class="container">
                      <div class="row">
                          <div class="col-sm-12 tm-section-2-right">
                              <div class="tm-bg-circle-white tm-bg-circle-pad-2 tm-flex-center-v">
                                  <header class="fixed">
                                      <h2 class="tm-color-primary">菁英招募</h2>
                                  </header>
                                  <ul class="dashed">
                                      <li>
                                          <!-- 租管顧問師............ -->
                                          <a href="https://www.104.com.tw/job/6bwi3?jobsource=company_job" target="_blank"><img src="/themes/Rent/img/joinlife/1201/104-logo.png" target="_blank" alt="104人力銀行" style="width:120px;height:auto;"></a>
                                          &nbsp;&nbsp;
                                          <a href="https://www.1111.com.tw/job/79733394/" target="_blank"><img src="/themes/Rent/img/joinlife/1201/1111-logo.png" target="_blank" alt="1111人力銀行" style="width:120px;height:auto;"></a>
                                      </li>
                                  </ul>

                              </div>
                          </div>
                      </div>
                  </div>

              </section>
              <section id="tm-section-4" class="tm-section pt-2 pb-3">
                  <div class="container">
                      <div class="row">
                          <div class="col-xl-5 col-lg-5 tm-flex-center-v tm-section-left">
                              <div class="col-xl-12 col-lg-12 tm-flex-center-v tm-text-container tm-section-left">
                                  <h2 class="tm-color-secondary mb-4 m600300">加盟體系</h2>
                                  <p class="mb-4 m00">租賃 管理 領導品牌</p>
                                  <p class="mb-4 m00">品牌優勢與獨特服務</p>
                                  <p class="mb-4 m00">成就您掌握 ５０００ 億租賃錢潮</p>
                                  <p class="mb-4 m00">門店加盟 | 團隊加盟</p>
                                  <p class="mb-4 m00">開創被動 + 永續收入</p>
                              </div>
                          </div>
                          <div class="col-xl-7 col-lg-7 tm-circle-img-container tm-contact-right m300">
                              <img src="/themes/Rent/img/joinlife/1201/img-02.jpg" alt="Image" class="rounded-circle tm-circle-img">
                          </div>
                      </div>
                  </div>
              </section>
          </div>
    </div>
