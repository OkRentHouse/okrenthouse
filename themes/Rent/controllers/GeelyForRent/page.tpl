{$breadcrumbs_htm}
{include file='../../geely_for_rent/rent_type.tpl'}
<div class="margin_box">
	<h1>{$geely_rent[0].title}</h1>
	<div class="information">
		<div><div>發佈時間</div><div class="time">{$geely_rent[0].date}</div></div>
		<div><div>類别</div><div class="type">{$geely_rent[0].type}</div></div>
		<div><div>瀏覧次數</div><div class="views">{$geely_rent[0].views|number_format:0:'.':','}</div></div>
	</div>
	<div class="geely_rent">
        {$geely_rent[0].content}
        {if !empty($geely_rent[0].author)}
{*			<author><div>作者</div><div class="author">{$geely_rent[0].author}</div>{if !empty($geely_rent[0].identity)}{$geely_rent[0].identity}{/if}</author>*}
		{/if}
	</div>
    {if $geely_rent[0].author}
		<div class="information no_border_b">
			<div><div>作者</div><div class="author">{$geely_rent[0].author}</div>{if $geely_rent[0].identity} ({$geely_rent[0].identity}){/if}</div>
		</div>
    {/if}
</div>