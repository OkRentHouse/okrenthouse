<div class="bg">
    <div class="home_web_register">
        <form action="https://www.okrent.house/Register" method="post"
              enctype="application/x-www-form-urlencoded" class="form_wrap">
            <div class="row list">
                <div class="col-md-6 name_frame">
                    <div class="floating">
                        <i class="fas fa-user"></i>
                        <input id="name" data-role="none" class="floating_input name " name="name"
                               type="text" placeholder="姓名" value="{$smarty.post.name}" minlength="2" maxlength="20" required="">
                    </div>

                    <div class="radio_wrap">
                        <div class="radio">
                            <input type="radio" name="gender" value="0" checked="">
                            <img src="/themes/Rent/img/register/woman.svg" alt="小 姐" title="小 姐" class="">

                        </div>
                        <div class="radio">
                            <input type="radio" name="gender" value="1">
                            <img src="/themes/Rent/img/register/men.svg" alt="先 生" title="先 生" class="">

                        </div>
                    </div>

                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">

                    <div class="floating">
                        <i class="fas fa-heart"></i>
                        <input id="nickname" data-role="none" class="floating_input nickname "
                               name="nickname" type="text" placeholder="暱稱" value="{$smarty.post.nickname}" minlength="2"
                               maxlength="20" required="">
                    </div>


                </div>
            </div>

            <div class="row list">
                <div class="col-md-6">
                    <div class="floating">
                        <i class="fas fa-mobile-alt"></i>
                        <input id="user" data-role="none" class="floating_input" name="user" type="text"
                               placeholder="手機號碼" value="{$smarty.post.user}" minlength="6" maxlength="20" required >
                    </div>
                </div>
            </div>

            <div class="row list">
                <div class="col-md-6">
                    <div class="floating">
                        <i class="fas fa-unlock-alt"></i>
                        <input id="password" data-role="none" class="floating_input " name="password"
                               type="password" placeholder="密 碼 - 數字或英文最少6個字" value="{$smarty.post.password}" minlength="6"
                               maxlength="20" required="">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="floating">
                        <i class="fas fa-unlock-alt"></i>
                        <input id="new_password" data-role="none" class="floating_input "
                               name="check_password" type="password" placeholder="確認密碼" value="{$smarty.post.check_password}" minlength="6"
                               maxlength="20" required >
                    </div>
                </div>

            </div>

            <div class="row list">
                <div class="col-md-3">
                    <div class="floating text-center" id="tel_submit">
                        <button type="button" id="tel_code_submit" class="btn ">取得驗證碼</button>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="floating" id="captcha_hidden">
                        <input id="captcha" data-role="none" class="floating_input" name="captcha"
                               type="text" placeholder="驗證碼" maxlength="16" required="">
                    </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class=" content text-center">
                        <button class="btn_reg" type="submit"  id="submitAdd{$table}" name="submitAdd{$table}" value="1">註 冊</button>
                    </div>
                </div>
            </div>

            <div class="row agree">
                <div class="col-md-12 flex">
                    <div><input id="agree" name="agree" type="checkbox" value="1" checked="true"></div>
                    <label for="agree">
                        <a href="https://okrent.house/statement" target="_blank">
                            我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</a>
                    </label>
                </div>
            </div>

        </form>
    </div>
</div>




{*<div class="bg">*}
{*    <div class="home_web_register">*}
{*        <div class="title text-center">*}
{*            <h1>會員註冊</h1>*}
{*        </div>*}
{*        <form action="" method="post"*}
{*              enctype="application/x-www-form-urlencoded" class="form_wrap">*}
{*                        <div class="list">*}
{*                            <div class="floating">*}
{*                                <input id="name" data-role="none" class="floating_input name " name="name" type="text"*}
{*                                       placeholder="" value="{$smarty.post.name}" minlength="2" maxlength="20" required>*}
{*                                <label for="" class="floating__label" data-content="中文姓名"></label>*}
{*                            </div>*}
{*                            <div class="radio_wrap">*}
{*                                <div class="radio">*}
{*                                    <input type="radio" name="gender"  value="0" checked="">小 姐*}
{*                                </div>*}
{*                                <div class="radio">*}
{*                                    <input type="radio" name="gender" value="1">先 生*}
{*                                </div>*}
{*                            </div>*}
{*                            <div class="floating">*}
{*                                <input id="nickname" data-role="none" class="floating_input nickname " name="nickname" type="text"*}
{*                                       placeholder="" value="{$smarty.post.nickname}" minlength="2" maxlength="20" required>*}
{*                                <label for="" class="floating__label" data-content="暱稱"></label>*}
{*                            </div>*}
{*                        </div>*}
{*            <div class="list">*}
{*                <div class="floating">*}
{*                    <input id="user" data-role="none" class="floating_input" name="user" type="text"*}
{*                           placeholder="限用手機號碼" value="{$smarty.post.user}" minlength="6" maxlength="20" required>*}
{*                    <label for="" class="floating__label" data-content="手機號碼"></label>*}
{*                </div>*}
{*                <div class="floating" id="captcha_hidden">*}
{*                    <input id="captcha" data-role="none" class="floating_input" name="captcha" type="text"*}
{*                           placeholder="請填寫驗證碼" maxlength="16" required>*}
{*                    <label for="" class="floating__label" data-content="驗證碼"></label>*}
{*                </div>*}
{*                <div class="floating" id="tel_submit">*}
{*                    <button  type="button" id="tel_code_submit" class="btn btn-primary">點選取得驗證碼</button>*}
{*                </div>*}
{*            </div>*}
{*            <div class="list">*}
{*                <div class="floating">*}
{*                    <input id="password" data-role="none" class="floating_input " name="password" type="password"*}
{*                           placeholder="最小輸入6個數字或英文" value="{$smarty.post.password}" minlength="6" maxlength="20" required>*}
{*                    <label for="input__password" class="floating__label" data-content="密 碼"></label>*}
{*                </div>*}
{*                <div class="floating">*}
{*                    <input id="new_password" data-role="none" class="floating_input " name="check_password" type="password"*}
{*                           placeholder="最小輸入6個數字或英文" value="{$smarty.post.check_password}" minlength="6" maxlength="20" required>*}
{*                    <label for="input__password" class="floating__label" data-content="確認密碼"></label>*}
{*                </div>*}
{*            </div>*}
{*            <div class="agree flex">*}
{*                <div><input id="agree" name="agree" type="checkbox" value="1"></div>*}
{*                <label for="agree">*}
{*                    <a href="https://okrent.house/statement" target="_blank">*}
{*                        我已閱讀並同意生活樂租的【服務條款】與【隱私權聲明】及會員同意條款</a>*}
{*                </label>*}
{*            </div>*}
{*            <br>*}
{*            <div class="content">*}
{*                <button class="btn_reg" type="submit"  id="submitAdd{$table}" name="submitAdd{$table}" value="1">註 冊</button>*}
{*            </div>*}
{*        </form>*}
{*    </div>*}
{*</div>*}





{if $smarty.get.success=="ok"}
<script>
        $(document).ready(function (e){
            alert("您已註冊成功! 三秒後跳轉頁面到登入畫面!");
        setTimeout(wait_location,3000);
        });

        function wait_location() {
            window.location = 'https://www.okrent.house/login';
        }
</script>
{/if}