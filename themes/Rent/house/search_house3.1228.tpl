<form action="{if $page == 'index'}/list{/if}" method="get" id="search_box_big">
	<input type="hidden" name="active" value="1">
	<input type="hidden" id="p" name="p" value="{$smarty.get.p}">
	<div class="tag_div"></div>

	<div class="search_forms_wrap">
{*		line *}
		<div id="search_forms" class="searcounty_sch_content">
			<div class="div_rage">
				<div class="search_div search_row_div L">

						<strong class="bont">用途<label class="border">請選擇</label></strong>
						<div>
							<div class="items">
							<input type="radio" id="id_main_house_class_all" value=""{if count($smarty.get.id_main_house_class) == 0} checked{/if}>
							{foreach from=$main_house_class key=i item=v }
								<input type="checkbox" name="id_main_house_class[]" id="id_main_house_class_{$main_house_class.$i.id_main_house_class}" value="{$main_house_class.$i.id_main_house_class}"
									   {if in_array($main_house_class.$i.id_main_house_class, $smarty.get.id_main_house_class)}checked{/if}>
								<label for="id_main_house_class_{$main_house_class.$i.id_main_house_class}" id="main_house_class_name_{$main_house_class.$i.id_main_house_class}">{$main_house_class.$i.title}</label>
							{/foreach}
							</div>
						</div>

				</div>
				<div class="search_div search_row_div R">
					<div class="search_div">
					<strong class="county_city search dropdown-toggle bont">區域<label class="border">請選擇</label></strong>
					<div><spam class="county_city search" style="display:none">{if empty($smarty.get.county)}所有縣市{else}{$smarty.get.county}{/if}</spam></div>{*<spam class="txt">(可複選)</spam>*}

					<div class="county_div">
	                    {foreach from=$select_county key=i item=county}<spam class="county"><input type="radio" name="county" id="county_{$i}" value="{$county.value}" {if $county.value == $smarty.get.county}checked{/if}><label for="county_{$i}">{$county.title}</label></spam>{/foreach}
					</div>

					<div class="city_div">{foreach from=$arr_city[$smarty.get.county] key=i item=city}<spam class="city"><input type="checkbox" name="city[]" id="city_{$i}" value="{$city}" {if in_array($city, $smarty.get.city)}checked{/if}><label for="city_{$i}">{$city}</label></spam>{/foreach}</div>

				</div>
				</div>
			</div>

			<div class="div_rage sec_class" style="display: none">

			<div class="search_div search_row_div L">
			<strong class="bont">類別<label class="border">請選擇</label></strong>{*<spam class="txt">(可複選)</spam>*}
				<div class="type_class items">
					<input type="radio" id="type_all" value=""{if count($smarty.get.id_type) == 0} checked{/if}>{*<label for="type_all"><img src="/img/all.png">全部</label>*}
					{foreach from=$select_type key=i item=type}<input type="checkbox" name="id_type[]" id="id_type_{$type.id_rent_house_type}" value="{$type.id_rent_house_type}" {if in_array($type.id_rent_house_type, $smarty.get.id_type)}checked{/if}><label for="id_type_{$type.id_rent_house_type}">{$type.title}</label>
				{/foreach}
				</div>
			</div>

			<div class="search_div search_row_div R">
				<strong class="bont">型態<label class="border">請選擇</label></strong>{*<spam class="txt">(可複選)</spam>*}
					<div class="items">
						<input type="radio" id="id_types_all" value=""{if count($smarty.get.id_types) == 0} checked{/if}>{*<label for="id_types_all"><img src="/img/all.png">全部</label>*}{foreach from=$select_types key=i item=types}<input type="checkbox" name="id_types[]" id="id_types_{$types.id_rent_house_types}" value="{$types.id_rent_house_types}" {if in_array($types.id_rent_house_types, $smarty.get.id_types)}checked{/if}><label for="id_types_{$types.id_rent_house_types}">{$types.title}</label>{/foreach}
					</div>

			</div>


			</div>

			<div class="div_rage">

				<div class="search_div rang search_row_div"><strong>房數</strong>
					<div class="search_table">
						<div class="w290" id="house_w290"><div style="display:none" class="room"></div>
						<div class="reng_view">
					<input type="text" class="input_no_border" id="room_0" value="OPEN">
						<input type="number" class="input_no_border" name="min_room" id="min_room" value="{$smarty.get.min_room|string_format:"%d"}" min="0" max="6" style="display:none"> ~
						<input type="number" class="input_no_border" name="max_room" id="max_room" value="{$smarty.get.max_room|string_format:"%d"}" min="0" max="6"> <unit class="room_unit">房</unit> </div>
						</div><p> </p><p> </p>
						<div><div id="room_range"></div><div class="room_Scale_line">
							{for $i=0;$i<7;$i++}
							<div class="Scale"><!--|--></div>
							{/for}
						</div>
						<div class="room_Scale">
							{for $i=0;$i<7;$i++}
							<div class="number {if $i%2==0}m-5{/if}" ><span>{if $i == 0}OPEN{else}{if $i>5}{$i-1}+{else}{$i}{/if}{/if}</span></div>
							{/for}
						</div>
					</div>
					</div>
				</div>

			</div><div class="div_rage">

				<div class="search_div rang search_row_div"><strong>租金</strong>
					<div class="search_table">
						<div class="w290">
							<div class="reng_view">
							<input type="number" class="input_no_border" name="min_price" id="min_price" value="{$smarty.get.min_price|string_format:"%d"}" step="{$price_step}" min="{$min_price}" max="{$max_price}"><i> ～ </i><input type="number" class="input_no_border" name="max_price" id="max_price" value="{$smarty.get.max_price|string_format:"%d"}"step="{$price_step}" min="{$min_price}"  max="{$max_price}"><unit>元</unit></div>
							</div><p> </p><p> </p>
						<div><div id="price_range"></div><div class="price_Scale_line">
							{for $i=0;$i<6;$i++}
							<div class="Scale"><!--|--></div>
							{/for}
						</div>
						<div class="price_Scale">
							{for $i=1;$i<12;$i++}
							<div class="number m-5{if $i%2==0}{/if}" ><span>{if $i>10}{$max_price*(($i-1)/10)/10000}萬+{else}{$max_price*($i/10)/10000}萬{/if}</span></div>
							{/for}
						</div></div>
					</div>
				</div>

			</div><div class="div_rage">

				<div class="search_div rang search_row_div"><strong>坪數</strong>
					<div class="search_table">
						<div class="w290">
							<div class="reng_view">
							<input type="number" class="input_no_border" name="min_ping" id="min_ping" value="{$smarty.get.min_ping|string_format:"%d"}" step="{$ping_step}" min="{$min_ping}" max="{$max_ping}"><i> ～ </i><input type="number" class="input_no_border" name="max_ping" id="max_ping" value="{$smarty.get.max_ping|string_format:"%d"}" step="{$ping_step}" min="{$min_ping}" max="{$max_ping}"><unit>坪</unit></div>
							</div><p> </p><p> </p>
						<div><div id="ping_range"></div>
						<div class="ping_Scale_line">
							{for $i=0;$i<6;$i++}
							<div class="Scale"><!--|--></div>
							{/for}
						</div>
						<div class="ping_Scale">
							{for $i=1;$i<12;$i++}
							<div class="number m-5{if $i%2==0}{/if}" ><span>{if $i>10}{$max_ping*(($i-1)/50*5)}+{else}{$max_ping*($i/50*5)}{/if}</span></div>
							{/for}
						</div>
					</div>
					</div>
				</div>



			</div>

			<div class="div_rage">

				<div class="search_div xsearch_row_div"><strong class="bont">需求<label class="border">請選擇</label></strong>
					<div class="items">
						<input type="radio" id="id_other_all" value=""{if count($smarty.get.id_other) == 0} checked{/if}>{*<label for="id_other_all"><img src="/img/all.png">全部</label>*}{foreach from=$select_other_conditions key=i item=other}<input type="checkbox"  name="id_other[]" id="id_other_{$other.id_other_conditions}" value="{$other.id_other_conditions}" data-id_main_house_class="{$other.id_main_house_class}" {if in_array($other.id_other_conditions, $smarty.get.id_other)}checked{/if}><label for="id_other_{$other.id_other_conditions}" class="{if $other.remarks}other_remarks{/if}">{$other.title}{if $other.remarks}<br><span class="fv">({$other.remarks})</span>{/if}</label>{/foreach}
					</div>
				</div>

				<div class="L">



			</div>
				<div class="R">



				</div>
				</div>

				<div class="submit_b">

						<input type="text" class="field w50p2" name="search" id="search" value="" placeholder="請輸入街道名稱或社區名稱等關鍵字" style="border: 1px solid #727171; width: 400px !important;height: 30pt;vertical-align: revert;border-radius: 10px;">
						<button type="submit" class="submit">送出</button>


				</div>





			<label class="search_remid"> <i class="far fa-hand-point-left" style="margin-right: 10px;    font-size: 14pt;" aria-hidden="true"></i>點放大鏡開始搜尋喔<i class="fas fa-exclamation-circle" aria-hidden="true"></i></label>
			<div class="input_txt search_div search_txt" style="display:none">
				<!-- <div class="search_input"><input type="text" class="field w50p2" name="search" id="search" value="{$smarty.get.search}" placeholder="請輸入關鍵字"><button type="submit"><img src="{$THEME_URL}/img/icon/pic_m.svg"></button></div> -->
			</div>
		</div>
	</div>
</form>

<script>

	arr_city = {$arr_city|json_encode:1};

    {if $min_price > 0}
	min_price = {$min_price};
    {else}
	min_price = 0;
    {/if}

    {if $max_price > 0}
	max_price = {$max_price};
    {else}
	max_price = 50000;//100000000;
    {/if}

    {if $price_step > 0}
	price_step = {$price_step};
    {/if}

    {if $min_ping > 0}
	min_ping = {$min_ping};
    {else}
	min_ping = 0;
    {/if}

    {if $max_ping > 0}
	max_ping = {$max_ping};
    {else}
	max_ping = 1000;
    {/if}

    {if $ping_step > 0}
	ping_step = {$ping_step};
    {/if}
</script>
