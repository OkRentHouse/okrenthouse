{* create at 04/27 by ben *}
{* <form action="{if $page == 'index'}/list{/if}" method="get" id="search_box_big2"> *}
<script>
	function clean(){
		document.getElementById("search").value = '';
		document.getElementById("county").selectedIndex  = '';
		document.getElementById("id_main_house_class").selectedIndex  = '';
		document.getElementById("id_type").selectedIndex  = '';
		document.getElementById("id_types").selectedIndex  = '';
	}
	</script>
<form action="https://okrent.house/list" method="get" id="search_box_big2">
	<input type="hidden" name="active" value="1">
	<input type="hidden" id="p" name="p" value="{$smarty.get.p}">
	<div class="tag_div"></div>
	<div class="search_forms_wrap">

		<div id="search_forms" class="searcounty_sch_content">
			<div class="div_rage rage1">

				{* {$search_tt_class=array("county_city search dropdown-toggle bont","bont","類別","型態","需求條件","輸入關鍵字")} *}
				{$search_tt_class=array("county_city search dropdown-toggle bont","bont","類別","型態","輸入關鍵字")}
				{$search_tt=array("區域","用途","型態","類別","輸入關鍵字")}
				
				{foreach $search_tt as $st => $v}
				{if $st == array_key_last($search_tt)}
				<div class="search_tt_div"><input class="search_tt" type="text" id="search" name="search" placeholder="{$v}" value="{$smarty.get.search}"><button class="search_icon" type="submit"><img class="pic_m" src="{$THEME_URL}/img/icon/pic_m.svg"></button></div>
				{else}
					{if $v == "區域"}
					<select class="search_tt" id="county" name="county">
					<option value="">區域</option>
					{foreach from=$select_county key=i item=county}<option value="{$county.value}" {if $county.value == $smarty.get.county}selected{/if}>{$county.value}</option>{/foreach}
					</select>
					{else if $v == "用途"}		
					<select class="search_tt" id="id_main_house_class" name="id_main_house_class">
					<option value="">用途</option>
					{foreach from=$main_house_class key=i item=type}<option value="{$type.title}" {if $type.title == $smarty.get.id_main_house_class}selected{/if}>{$type.title}</option>{/foreach}
					</select>
					{else if $v == "型態"}		
					<select class="search_tt" id="id_types" name="id_types">
					<option value="">型態</option>
					{foreach from=$select_types key=i item=types}<option value="{$types.id_rent_house_types}" {if $types.id_rent_house_types == $smarty.get.id_types}selected{/if}>{$types.title}</option>{/foreach}
					</select>	
					{else if $v == "類別"}		
					<select class="search_tt" id="id_type" name="id_type[]">
					<option value="">類別</option>
					{foreach from=$select_type key=i item=type}<option value="{$type.id_rent_house_type}" {if in_array($type.id_rent_house_type, $smarty.get.id_type)}selected{/if}>{$type.title}</option>{/foreach}
					</select>		
					{else}
					<select class="search_tt"><option>{$v}</option></select>
					{/if}
					
				{/if}
				{/foreach}


			</div>
		</div>

	</div>

	

	<div class="search_forms_wrap_II">

		<div class="div_rage rage2">
			<img class="pic_m selectCondition" src="{$THEME_URL}/img/list/Group 310_l.svg" onclick="selectCondition()">
			<img class="pic_m mapview" src="{$THEME_URL}/img/list/Group 361.svg">
		</div>
		<img class="Union_6" onclick="clean()" src="{$THEME_URL}/img/list/Union 6.svg">


	</div>

	<div class="search_forms_wrap_III">
		{$search_tt=array("房數","衛浴數","屋齡","室內坪數","預算","居住人數","租期","起租日")}
		{$search_tt_unit=array("間","間","年","坪","元","人","年","日")}
		<div class="container rage3">
			{foreach $search_tt as $st => $v}
			{if ($st+1)%2==1 || $st==0}
			<div class="row">
			{/if}
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-2 searchform_title">{$v}</div>
						<div class="col-lg-9">
							{if $st == array_key_last($search_tt)}
							<img class="linear" src="{$THEME_URL}/img/list/linear color.svg"><input type="date" class="linear">
							{else}
							<input data-role="doubleslider" data-hint="true" data-hint-always="true" data-hint-mask-min="$1 {$search_tt_unit[$st]}" data-hint-mask-max="$1 {$search_tt_unit[$st]}">
							{/if}
						</div>
						<div class="col-lg-1"></div>
					</div>
				</div>
			{if ($st+1)%2==0}
			</div>
			{/if}
			{/foreach}
		</div>
		<div class="rage_hr rage4">
			<span class="searchform_title">設備</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>


		<div class="rage4 contect">
		{$rage4_c=array("床組","梳妝台組","衣櫃","沙發茶几組","餐桌椅","書桌椅","天然氣","冷氣","電視","冰箱","洗衣機","烘衣機","第四臺","網路","熱水器:",":電熱水器","廚房炊具:",":瓦斯爐")}
		{$rage4_c=array()}
		<div class="hidden">
		{foreach $select_device_category as $st => $v}
			{array_push($rage4_c,$v.title)}
		{/foreach}
		</div>
		{foreach $rage4_c as $st => $v}

			{if !preg_match("/:/i", $v)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
			</div>
			{else}
				{if mb_stripos($v, ':') == (mb_strlen($v)-1)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
				{else if mb_stripos($v, ':') == 0 }
				<select class="form-select" aria-label="Default select example">
				  <option selected>{str_replace(':','',$v)}</option>
				</select>
			</div>
				{/if}
			{/if}

		{/foreach}
		</div>

		<div class="rage_hr rage5">
			<span class="searchform_title">需求</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>

		{$rage5_c=array()}
		<div class="hidden">
		{foreach $select_other_conditions as $st => $v}
			{array_push($rage5_c,$v.title)}
		{/foreach}
		</div>

		<div class="rage5 contect">
			{foreach $rage5_c as $st => $v}

			{if !preg_match("/:/i", $v)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
			</div>
			{else}
				{if mb_stripos($v, ':') == (mb_strlen($v)-1)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
				{else if mb_stripos($v, ':') == 0 }
				<select class="form-select" aria-label="Default select example">
				  <option selected>{str_replace(':','',$v)}</option>
				</select>
			</div>
				{/if}
			{/if}

		{/foreach}
		</div>

		<div class="rage_hr rage6">
			<span class="searchform_title">機能</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>

		{$rage6_c=array("近商場賣場","近傳統市場","近交流道","近綠地公園","近捷運")}


		<div class="rage6 contect">
			{foreach $rage6_c as $st => $v}

			{if !preg_match("/:/i", $v)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
			</div>
			{else}
				{if mb_stripos($v, ':') == (mb_strlen($v)-1)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
				{else if mb_stripos($v, ':') == 0 }
				<select class="form-select" aria-label="Default select example">
				  <option selected>{str_replace(':','',$v)}</option>
				</select>
			</div>
				{/if}
			{/if}

		{/foreach}
		</div>

		<div class="rage_hr rage7">
			<span class="searchform_title">社區休閒公設</span> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z" class=""></path></svg>
		</div>

		{$rage7_c=array()}
		<div class="hidden">
		{foreach $select_public_utilities as $st => $v}
			{array_push($rage7_c,$v.title)}
		{/foreach}
		</div>

		<div class="rage7 contect">
			{foreach $rage7_c as $st => $v}

			{if !preg_match("/:/i", $v)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
			</div>
			{else}
				{if mb_stripos($v, ':') == (mb_strlen($v)-1)}
			<div class="itme">
				<input type="checkbox" data-role="checkbox" value="{str_replace(':','',$v)}" data-caption="{str_replace(':','',$v)}">
				{else if mb_stripos($v, ':') == 0 }
				<select class="form-select" aria-label="Default select example">
				  <option selected>{str_replace(':','',$v)}</option>
				</select>
			</div>
				{/if}
			{/if}

		{/foreach}
		</div>

	</div>


</form>

<script>

	arr_city = {$arr_city|json_encode:1};

    {if $min_price > 0}
	min_price = {$min_price};
    {else}
	min_price = 0;
    {/if}

    {if $max_price > 0}
	max_price = {$max_price};
    {else}
	max_price = 50000;//100000000;
    {/if}

    {if $price_step > 0}
	price_step = {$price_step};
    {/if}

    {if $min_ping > 0}
	min_ping = {$min_ping};
    {else}
	min_ping = 0;
    {/if}

    {if $max_ping > 0}
	max_ping = {$max_ping};
    {else}
	max_ping = 1000;
    {/if}

    {if $ping_step > 0}
	ping_step = {$ping_step};
    {/if}
</script>
