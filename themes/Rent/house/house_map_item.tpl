<div class="commodity">
	<div class="photo_row"><img src="{$house.img}" class="img" alt="{$house.title}">
		<collect data-id="{$house.id}" data-type="house" class="favorite {if $house.collect}on{/if}">{if $house.collect}
				<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}
		</collect>

	</div><data>
		<h3 class="title big">{$house.title}</h3>
		<h3 class="title">{$house.subtitle}</h3>
		<div>{implode('<i></i>', [$house.t1, $house.t2, $house.t3])}</div>
		<div>{implode('<i></i>', [$house.t4])}</div>
		<price>{$house.price|number_format:0:".":","}
			<unit>{$house.unit}</unit>
		</price>
		<tag>{foreach from=$house.tag key=i item=tag}
				<i><img src="{$tag.img}" alt="{$tag.txt}" title="{$tag.txt}"></i>
		{/foreach}</tag>
	</data>
</div>