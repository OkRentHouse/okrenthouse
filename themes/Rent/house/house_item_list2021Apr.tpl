<div class="commodity">
	<div class="photo_row">
		<a href="/RentHouse?id_rent_house={$v.id_rent_house}">
			<img src="{$v.img}" class="img" alt="">
			<collect data-id="{$v.id_rent_house}" data-type="rent_house" class="favorite {if $v.favorite}on{/if}">{if $v.favorite}
					<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}
		</a>
	</div><data>
		<h3 class="title big"><a href="/RentHouse?id_rent_house={$v.id_rent_house}">{$v.case_name}</a></h3>
		<h3 class="title big_2"><a href="/RentHouse?id_rent_house={$v.id_rent_house}">{$v.title}</a></h3>
		<div class="title_2"><img src="/themes/Rent/img/icon/icons8-address-50.png">{$v.rent_address}</div>
		<div class="title_4">
		{if ($v.room=='' || $v.room=='0') && ($v.hall=='' || $v.hall=='0')}開放空間
		{else}
			{if $v.room=='' || $v.room=='0'}{else}{$v.room}房{/if}
			{if $v.hall=='' || $v.hall=='0'}{else}{$v.hall}廳{/if}
			{if $v.muro=='' || $v.muro=='0'}{else}{$v.muro}室{/if}
			{if $v.kitchen=='' || $v.kitchen=='0'}{else}{$v.kitchen}廚{/if}
		{/if}
		{if $v.bathroom=='' || $v.bathroom=='0'}{else}{$v.bathroom}衛{/if}
			<!--<i></i>--> &nbsp &nbsp &nbsp <span class="title_4_pin">{$v.ping}坪</span></div>
		<price>${$v.rent_cash}
			<unit>/月</unit>
		</price>
	</data>
</div>
