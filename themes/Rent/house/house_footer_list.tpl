<style>
.commodity_footers.slick-initialized .slick-slide {
    border: 0px solid rgba(201,202,202,1);
}
{for $m_top=2;$m_top<=2;$m_top=$m_top+2}
.commodity_footers.slick-initialized .slick-slide:nth-child({$m_top}) {
    margin-top: 32px;
}
{/for}

.commodity_footers.slick-initialized .slick-slide.mar_top {
	margin-top: 32px;
}

.commodity_footers .slick-next {
    position: absolute;
    right: 1rem;
    top: -1rem;
}

.commodity_footers .slick-prev {
	position: absolute;
	right: 0px;
	top: -1rem;
	left: auto;
}

.commodity_footers button.slick-arrow.slick-next:before {
    content: url(/img/svg/b_s_next.svg);
}

.commodity_footers button.slick-arrow.slick-prev:before {
    content: url(/img/svg/b_s_prev.svg);
}
</style>
{$footers_link=array("","#","#","#","#","#","#","#","#","#")}
{$footers_title=array("","#","#","#","#","#","#","#","#","#")}
<div class="commodity_list commodity_footers">
	{for $footers=1;$footers<=9;$footers++}
		<div class="commodity{if $footers%2==0} mar_top{/if}">
			<div class="footers_row">
				<a href="{$footers_link[$footers]}">
					<img src="/img/footer{$footers}.svg" class="img" alt="{$footers_title[$footers]}">
				</a>
			</div>
		</div>
	{/for}
</div>

<script>
	$(".commodity_footers").slick({
	dots: false,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 3000,
	slidesToShow: 4,
	slidesToScroll: 4,
	});
</script>
