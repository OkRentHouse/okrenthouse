<div class="list t_container top">
    <input type="hidden" name="id_main_class" value="{$smarty.get.id_main_class}">
    <input type="hidden" name="county" value="{$smarty.get.county}">
    <input type="hidden" name="city" value="{$smarty.get.city}">
    <input type="hidden" name="id_product_class" value="{$smarty.get.id_product_class}">

    <div class="top_5">
        <a href="/index"><img src="/themes/Okrent/img/list/logo_w_f.png"></a>
    </div>
    <div class="top_6">
      <span class="search_item">
        <a href="#" class="dropdown-toggle">{if $smarty.get.id_main_class}{foreach $head_main_class as $key =>$value}{if $value.id_main_class==$smarty.get.id_main_class}{$value.title}{/if}{/foreach}{else}共享趣{/if}</a>
        <ul class="d-menu menu_1" data-role="dropdown">
            {foreach $head_main_class as $k => $v}
                <li><a href="#" data-name="id_main_class" data-value="{$v.id_main_class}" onclick="forwarding(this)">{$v.title}</a></li>
                {/foreach}
        </ul>
      </span>

      <span class="search_item"><a href="#" class="dropdown-toggle">{if !empty($smarty.get.county) || !empty($smarty.get.city)}{$smarty.get.county}{$smarty.get.city}{else}桃園市桃園區{/if}</a>
        <ul class="d-menu menu_1 citys" data-role="dropdown">
            <li><a href="#" onclick="county_city(this)"></a></li>
        </ul></span>

      <span class="search_item">
        <a href="#" class="dropdown-toggle">{if $smarty.get.id_product_class}{foreach $head_product_class as $key =>$value}{if $value.id_product_class==$smarty.get.id_product_class}{$value.title}{/if}{/foreach}{else}工具設備{/if}</a>
        <ul class="d-menu menu_2" data-role="dropdown">
            {foreach $head_product_class as $key =>$value}
                <li><a href="#"  onclick="forwarding(this)" data-name="id_product_class" data-value="{$value.id_product_class}">{$value.title}</a></li>
            {/foreach}
        </ul>
      </span>
      <span class="search_item input"><input type="text" name="keyword" value="{$smarty.get.keyword}" placeholder="可輸入關鍵字"></span>
      <i class="fas fa-search" data-name="no" data-value="no"  onclick="forwarding(this)"></i>
    </div>
    <div class="top_7">
        <a href="####">登入</a> | <a href="####">註冊</a>
    </div>
    <div class="top_8">
      <div class="top_9">

        <img class="share" src="/img/share-alt-solid-w.svg">
        <img class="hb" src="/themes/Okrent/img/list/hb.png">
      </div>

    </div>
    {include file="$tpl_dir./top_fn.tpl"}
</div>
