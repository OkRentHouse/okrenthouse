
<!-- <div class="top_more"><span class="more">挖寶趣 | 交換趣 | 環保捐贈 | 願望清單</span></div> -->

<div class="containerx"> 
  <div class="row item">
    <div class="photo">
      <div class="row">
        <div class="picture border"></div>
        <img class="heart" src="/themes/LifeHouse/img/lifego/heart.svg" alt="">
      </div>
      <div class="row minipictures">
        <div class="minipicture border"></div>
        <div class="minipicture border"></div>
        <div class="minipicture border"></div>
        <div class="minipicture border"></div>
      </div>
    </div>
    <div class="info">
      <span class="title">{$data['title']}</span><span style="display: flex;margin-left: -2rem;" class="heart"><div class="heart_in"></div><img src="/themes/LifeHouse/img/lifego/heart.svg" alt="">388</span>
      <span class="title_vice">
        {$data['subtitle']}
      </span>
      <span class="class_name">{$data.product_class.title}</span>
      <span class="price">$<span class="HighLight rent_price">800</span>元<div class="rent_day"><img src="\themes\Okrent\img\list\clock.svg"><span class="rent_time">5日</span>|<span class="rent_time">10日</span>|<span class="rent_time">15日</span>|<span class="rent_time">30日</span></div></span>
      <span class="price_o">${$data.deposit}元</span>
      <span class="payment">{foreach $data['pay'] as $k => $v}{if $k!=0}・{/if}{$v.name}{/foreach}</span>
      <span class="delivery">{foreach $data['delivery'] as $k => $v}{if $v.url}<img src="{$v.url}">{elseif $v.icon}<i class="{$v.icon}"></i>{else}{/if}{$v.name} {/foreach}</span>
      <div class="type_icon">
        <img src="\themes\Okrent\img\list\ch_icon.png">
        <img src="\themes\Okrent\img\list\calendar.png">
      </div>
      <span class="user_info"><div class="photo"><img src="\themes\Okrent\img\list\head.svg"></div>白白屋 5.8<img class="thumbsup" src="\themes\Okrent\img\list\thumbsup.svg"> {$data.county.county_name}{$data.city.city_name}</span>
    </div>
  </div>
  <div class="border item_introduce row">
  </div>
  <div class="row message_title">
    <span>留言板</span>
  </div>
  <div class="row arranged border">
    <div class="user">小凱</div><label class="line"></label><div class="command">有附睡袋嗎?</div>
  </div>
</div>
