{$css}
<div class="containerx">
  <div class="row">
    <p>&nbsp</p>
    <h3 style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-family: 微軟正黑體, cwTeXYen, sans-serif; color: #333333;"><span style="font-size: 18pt;"><strong>服務條款</strong></span></h3>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;">「共享圈」（以下稱本站）係依據本服務條款提供物品租借 (https://www.okrent.house/rent_lend_main) 平台服務（以下稱本服務）。當您使用本站時，表示你已閱讀、瞭解並同意接受本條款所有內容。本站有權隨時修改或變更本條款之內容，若你在條款內容修改或變更後仍繼續使用本站，視為你已瞭解並同意接受修改或變更後的條款。</span><span style="font-size: 14pt;"><span style="font-size: 18.6667px;">因此，請您隨時注意本站之所有最新訊息。</span></span></p>

<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><strong><span style="font-size: 18.6667px;">免責聲明</span></strong></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">一、本站是第三方物品租借搜尋平台，本站無法判斷會員刊登個人資料是否真實，也無法對於每個會員或任何使用本站訊息的網友之動機與履約能力，進行了解與審查。此外，會員與其相對人間之實際互動、交涉等行為，對於後續發生的糾紛與相關損害賠償，本站概不負責。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">二、本站根據會員、網友在本站上所搜尋的物品資訊、價格及結果，加以彙整便利使用者閱讀，僅為檢索資訊，不代表本站提供的任何意見。若會員查詢本站以外的外部網站，除了法律規定的範圍，本公司不對外部網站內容負責。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">三、本站對下列無法合理預見之狀況所產生之損害，概不負責：</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(一)戰爭、事變、天災地變等非常事態，不可抗力的狀況所生的損害。</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(二)會員、使用者故意或過失行為，所造成的損害。</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(三)因網路或是電信服務提供業者，所生之網路或通信障礙而生的損害。</span></p>

<p><strong><span style="font-size: 18.6667px;">會員服務說明</span></strong></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">會員申請和限制</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">一、你必須先完成註冊確認程序，成為本站會員。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">二、如有以下任一情形者，本站不予同意您加入會員並使用本站服務：</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(一)以他人的名字申請註冊。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(二)未滿七歲無行為能力者。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(三)曾違反會員條款被停權者。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(四)與已加入會員的註冊電話相同時。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(五)其他未符合本站之申請使用條件時。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">三、若以假名或他人的身份訊息加入會員者，需自行擔負所有法律的責任。本站有權暫停或終止該會員帳號，並拒絕該人使用本站服務。</span></p>

<p><strong><span style="font-size: 18.6667px;">資訊刊登</span></strong></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">一、本站僅提供一個第三方平台服務，廠商或個人可能透過本站內容或經由本站內容連結之其他網站提供商品買賣、服務或其他交易行為。會員若因此與該等廠商或個人進行交易，各該買賣或其他合約，均僅存在你與各該廠商或個人之間，與本站無涉。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">二、會員應對各該廠商或個人，就其商品、服務或其他交易標的物之品質、內容、運送、保證事項與瑕疵擔保責任等，應事先詳細說明或瞭解。若因而產生的爭議，應由各該廠商或個人自行尋求救濟或解決。本公司絕不介入會員與廠商或個人間的任何買賣、服務或其他各項交易行為，對於會員所取得之商品、服務或其他交易標的物，亦不負任何擔保責任。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">三、本站所有的會員刊登的資訊或任何廣告內容、文字與圖片中所包含之說明、展示圖片或其他資訊，均由廣告、產品與服務之會員或提供者所提供，與本公司無涉。本公司不對刊登內容的正確性負責。</span></p>

<p><strong><span style="font-size: 18.6667px;">會員禁止事項</span></strong></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">一、會員於使用本站時不得從事下列所述行為：</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(一)刊登或傳送內容，有(a)引誘或助長自殺、自傷行為或濫用藥物的表現、(b)暴力、性暗示的表現、(c)涉及人種、國籍、信仰、性別、社會地位、家世等有關歧視的表現、(d)其他使人感到不愉快的表現的行為。</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(二)偽裝成本站，故意散布不實資訊的行為。</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(三)傳送相同或類似廣告訊息給不特定多數會員的行為、隨機將其他會員加入為聊天對象的行為、及其他經本站判斷為垃圾或是騷擾訊息的行為。</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(四)以性行為或猥褻行為為目的之行為、以與異性認識或交往或婚姻介紹為目的之行為、以騷擾或毀謗中傷其他會員為目的之行為。</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(五)不當蒐集、公開或提供他人的個人資料、物品資料等的行為。</span></p>
<p style="padding-left: 80px;"><span style="font-size: 18.6667px;">(六)其他本站判斷為不適當的行為、其他與本站訂定的使用目的不同之行為。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">二、會員使用本站如有上開情事，本站有權對違規者停止其會員資格及相關服務。</span></p>

<p><strong><span style="font-size: 18.6667px;">管轄法院</span></strong></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">關於會員與本公司因本站所提供之服務所生之糾紛，雙方同意依照中華民國法律作為準據法，並以台灣台北地方法院為第一審管轄法院。</span></p>

<h3 style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-family: 微軟正黑體, cwTeXYen, sans-serif; color: #333333;"><strong>個資授權運用聲明</strong></h3>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">一、對於會員所登錄或留存之個人資料、發言等資訊，會員同意本站得於合理之範圍內，加以蒐集、處理、保存、傳輸及使用，以讓本站得據以提供相關資訊或服務、作成統計資料、進行網路行為之調查或研究，或為任何之合法使用。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">二、基於供服務或贈獎之目的，會員同意授權本站得為提提供必要之會員資料給合作廠商，本站承諾會在此目的範圍內作妥善使用。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">三、任何資料，經會員上傳、輸入或提供至本站時，視為您擔保相關資訊有合法權利，並允許本公司無條件使用、修改、重製、改作、散布、發行、公開發表、傳輸、播送該等資料，並得將前述權利轉授權本站相關連公司、或第二項目的之第三人，對此您絕無異議表示認同。你並應保證本公司作使用、修改、重製、改作、散布、發行、公開發表、傳輸、播送、轉授權該等資料，不致侵害任何第三人之智慧財產權，否則應對本公司負損害賠償責任（包括但不限於訴訟費用及律師費用等）。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">四、若會員不同意本站為前述各項範圍內之運用時，請通知本站刪除其會員帳號，同時放棄會員權利。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif; padding-left: 40px;"><span style="font-size: 18.6667px;">五、本站所取得之個人資料，在未徵求您同意前，不會任意將相關資料作超脫本條款目的以外之使用，或洩漏給第三者。但以下狀況除外：</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(一)基於法律規定。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(二)司法機關基於法定程序要求。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(三)當事人自行公開或其他已合法公開之個人資料。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(四)與公共利益有關。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(五)個人資料取自於一般可得之來源。</span></p>
<p style="padding-left: 40px;"><span style="font-size: 18.6667px;">　　(六)為避免本站其他使用者之權益受重大危害。</span></p>

<h3 style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-family: 微軟正黑體, cwTeXYen, sans-serif; color: #333333;"><span style="font-size: 18pt;"><strong>智慧財產權的保護</strong></span></h3>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;">本站所使用之軟體或程式、網站的所有內容，包括但不限於著作、圖片、檔案、資訊、資料、網站架構、網站畫面的安排、網頁設計， 均由本站或其他權利人依法擁有其智慧財產權，包括但不限於商標權、專利權、著作權、營業秘密與專有技術等。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;">任何人不得逕自使用、 修改、重製、公開播送、公開發表、公開傳輸、改作、散布、發行、進行還原工程、解編或反向組譯。</span></p>
<p style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-size: 18.1434px; font-family: 微軟正黑體, cwTeXYen, sans-serif;"><span style="font-size: 18.6667px;">若您欲引用或轉載前述軟體、程式或網站內容，依法應取得本站或其他權利人的事前書面同意。如有任何侵犯本站智慧財產權之行為，您應對本站負損害賠償責任（包括但不限於訴訟費用及律師費用等）。</span></p>


  </div>
</div>
