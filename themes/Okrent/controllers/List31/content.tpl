<!-- <div class="top_more"><span class="more">挖寶趣 | 交換趣 | 環保捐贈 | 願望清單</span></div> -->
<div class="containerx">
  <div class="row">
      {include file="$tpl_dir./page_left.tpl"}
      <div class="col-8 right_mune">
         {foreach $product as $k =>$v}
          <div class="rows_1 nobk row_{$k}">
              <div class="content">
                  <div class="news_row">
                      <ul class="anset">
                          <li class="set_1">
                              <div class="items">
                                  <div class="prodocu">
                                      <img class="transfer" src="/themes/LifeHouse/img/lifego/transfer.svg" alt="">
                                      <div class="photo">
                                          <div class="p_broder">
                                              <img src="{$v.img}">
{*                                              <span>物品照片 </span>*}
                                          </div>
                                      </div>
                                      <div class="infor">
                                          <ul>
                                              <li>
                                                  <ul class="row row_0 p_name">
                                                      <li class="l" style=""><span style="position: absolute;font-size: 17pt;">{$v.name}</span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style=""><span style=""></span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_1 p_name">
                                                      <li class="l" style="min-height: 7rem;"><span style="position: absolute;line-height: 2rem;">{$v.title}</span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style=""><span style=""></span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_2 p_name">
                                                      <li class="l" style=""><span style="">{$v.unit}</span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""><div class="heart_in"><img src="/themes/LifeHouse/img/lifego/heart.svg" alt=""></div></span></li>
                                                      <li class="c_r" style="margin-right: 70px"><span style="">{$v.like}</span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_3 p_name">
                                                      <li class="l" style=""><span style="">{$v.member_name}</span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""><i class="far fa-thumbs-up"></i></span></li>
                                                      <li class="c_r" style="margin-right: 70px"><span style="">{$v.great}</span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_4 p_name">
                                                      <li class="l" style=""><span style="">{$v.country}</span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style=""><span style=""></span></li>
                                                  </ul>
                                              </li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                          </li>
                          <li class="set_2">
                              <div class="items">
                                  <div class="prodocu">
                                      <div class="photo">
                                          <div class="p_broder">
                                              <img src="{$v.exchange_img}">
{*                                              <span>物品照片</span>*}
                                          </div>
                                      </div>
                                      <div class="infor">
                                          <ul>
                                              <li>
                                                  <ul class="row row_0 p_name">
                                                      <li class="l" style=""><span style="position: absolute;font-size: 17pt;">{$v.exchange_name}</span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style=""><span style=""></span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_1 p_name">
                                                      <li class="l" style="min-height: 7rem;"><span style="position: absolute;line-height: 2rem;">{$v.exchange_title}</span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style=""><span style=""></span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_2 p_name">
                                                      <li class="l" style=""><span style=""></span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style="margin-right: 70px"><span style=""></span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_3 p_name">
                                                      <li class="l" style=""><span style=""></span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style="margin-right: 70px"><span style=""></span></li>
                                                  </ul>
                                              </li>
                                              <li>
                                                  <ul class="row row_4 p_name">
                                                      <li class="l" style=""><span style=""></span></li>
                                                      <li class="c_l" style=""><span style=""></span></li>
                                                      <li class="r" style=""><span style=""></span></li>
                                                      <li class="c_r" style=""><span style=""></span></li>
                                                  </ul>
                                              </li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
          {/foreach}
      </div>
</div>

{$js}
