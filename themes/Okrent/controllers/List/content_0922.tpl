<div class="t_container top">
    <div class="top_1">
        <img src="/themes/Okrent/img/list/content_1090922_06.png">
    </div>
    <div class="top_2">
        類別&nbsp;&nbsp;<span>&nbsp;&nbsp;全&nbsp;&nbsp;部&nbsp;&nbsp;&nbsp;&nbsp;</span>
        品項&nbsp;&nbsp;<span>&nbsp;&nbsp;全&nbsp;&nbsp;部&nbsp;&nbsp;&nbsp;&nbsp;</span>
        品名&nbsp;&nbsp;<span>&nbsp;&nbsp;全&nbsp;&nbsp;部&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <i class="fas fa-search"></i>
    </div>
    <div class="top_3">
        <a href="####">登入</a> | <a href="####">註冊</a> | <a href="####">快速上架</a> | <a href="####">喜愛清單</a>
    </div>
    <div class="top_4"><img src="/themes/Okrent/img/list/content_1090922_05.png"></div>
</div>
<div class="top_more"><span class="more">挖寶趣 | 交換趣 | 環保捐贈 | 願望清單</span></div>

<div class="rows_1">
    <div class="content">
        <div class="title_txt">
            <img src="/themes/Okrent/img/index/1090918_10_01.png">
            <span>樂租物品</span>
        </div>

        <div class="news_row">
            <section class="slider regular">
                {foreach $caption_data1 as $key => $value}
                    <div class="list">
                        <div class="img_frame"><a href="LifeGoProduct?item_id={$value.item_id}"><img src="/themes/Okrent/img/index/1090918_12.png"></a></div>
                        <div class="content_frame">
                            <div style="" class="heart_div">
                                <div class="number">{$value.item_number}</div>
                                <div class="heart">
                                    <img src="/themes/LifeHouse/img/lifego/heart.svg" alt="" style="width: 20px;">
                                    <div class="quantity">{$value.favorite}</div>
                                </div>
                            </div>
                            <div class="caption"><a href="LifeGoProduct">{$value.main_title}</a></div>
                            <div class="caption2">{$value.title}</div>
                            <div class="heart_wrap">
                                <div class="price_original">原價 <span class="outer"><span class="inner">{$value.price_original}日/</span></span>元</div>
                                <div class="text-center price">
                                    <div class="text-center"><span>$ {$value.price}</span>/日</div>
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </section>
        </div>
    </div>
</div>

<div class="rows_2">
    <div class="content">
        <div class="title_txt">
            <img src="/themes/Okrent/img/index/1090918_10_01.png">
            <span>{$caption_title1}</span></div>
        <div class="news_row">
            <section class="slider regular">
                {foreach $caption_data1 as $key => $value}
                    <div class="list">
                        <div class="img_frame"><a href="LifeGoProduct?item_id={$value.item_id}"><img src="/themes/Okrent/img/index/1090918_13.png"></a></div>
                        <div class="content_frame">
                            <div style="" class="heart_div">
                                <div class="number">{$value.item_number}</div>
                                <div class="heart">
                                    <img src="/themes/LifeHouse/img/lifego/heart.svg" alt="" style="width: 20px;">
                                    <div class="quantity">{$value.favorite}</div>
                                </div>
                            </div>
                            <div class="caption"><a href="LifeGoProduct">{$value.main_title}</a></div>
                            <div class="caption2">{$value.title}</div>
                            <div class="heart_wrap">
                                <div class="price_original">原價 <span class="outer"><span class="inner">{$value.price_original}日/</span></span>元</div>
                                <div class="text-center price">
                                    <div class="text-center"><span>$ {$value.price}</span>/日</div>
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </section>
        </div>
    </div>
</div>
{$js}