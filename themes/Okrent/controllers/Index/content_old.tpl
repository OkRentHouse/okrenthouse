<div class="t_container top">
    <div class="row index">
        <div class="fn_bar">
          <div class="l">
            <a href="/Login">登入</a> | <a href="/Signup">註冊</a>
          </div><div class="R">
            <!-- <img class="share" src="/themes/Okrent/img/index/share.png"> -->

            <img class="share" src="/img/share-alt-solid-w.svg">
            <img class="hb" src="/themes/Okrent/img/index/hb.png">
            <!--<i class="fas fa-share-alt"></i>-->

          </div>
        </div>

        {$arr_index_banner=array("1091231_1_banner_1.jpg","1091222_1_banner_2.JPG","1091210_1_banner_3.jpg")}
        <div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: 1903:1070">
      		<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
      			<ul class="uk-slideshow-items">
                      {foreach $arr_index_banner as $v => $banner}
      					<li><a href="####"><img src="/themes/Okrent/img/index/{$banner}"></a></li>
                      {/foreach}
      			</ul>

      			<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
      				  uk-slideshow-item="previous"></samp>
      			<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
      				  uk-slideshow-item="next"></samp>
      		</div>
      		<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
      	</div>


    </div>
    {include file="$tpl_dir./top_fn.tpl"}
</div>
{$i=0}
{$array_imgs=array("Subtraction_1.png","Subtraction_2.png","Subtraction_1_v.png","Subtraction_2_w.png","Subtraction_1_x.png","Subtraction_2_y.png")}
{$array_imgs=array("home_1231_1.jpg","home_1231_2.jpg","home_1231_3.jpg","home_1231_4.jpg","home_1231_5.jpg","home_1231_6.jpg")}
{$array_icons=array("<img class='icons' src='/themes/Okrent/img/index/Group_14.png'>",
"",
"<div id='exchange_icon'><div id='Group_53'><svg class='Ellipse_46'><ellipse id='Ellipse_46' rx='7.645463466644287' ry='7.645463466644287' cx='7.645463466644287' cy='7.645463466644287'></ellipse></svg><svg class='Path_652' viewBox='-273.656 -1510.64 51.428 15.316'><path id='Path_652' d='M -222.2285461425781 -1496.489990234375 C -222.2285461425781 -1496.489990234375 -224.5710144042969 -1510.414306640625 -249.1690368652344 -1510.637939453125 C -273.7671813964844 -1510.862060546875 -273.6562805175781 -1495.324462890625 -273.6562805175781 -1495.324462890625'></path></svg></div><div id='Group_54'><svg class='Ellipse_46_bp'><ellipse id='Ellipse_46_bp' rx='7.645463466644287' ry='7.645463466644287' cx='7.645463466644287' cy='7.645463466644287'></ellipse></svg><svg class='Path_652_bq' viewBox='-273.656 -1510.64 51.428 15.316'><path id='Path_652_bq' d='M -273.6562805175781 -1509.474853515625 C -273.6562805175781 -1509.474853515625 -271.3138732910156 -1495.550537109375 -246.7157897949219 -1495.326904296875 C -222.1177062988281 -1495.102783203125 -222.2285461425781 -1510.640380859375 -222.2285461425781 -1510.640380859375'></path></svg></div></div>",
"<img class='icons' src='/themes/Okrent/img/index/Group_221.png'>",
"<img class='icons' src='/themes/Okrent/img/index/Group_72.png'>",
"<img class='icons_1' src='/themes/Okrent/img/index/Group_615.png'><img class='icons_2' src='/themes/Okrent/img/index/Group_615.png'>")}
{$array_txt=array("<span>租用</span><br> <div class='content'>何必買 租就好<br> <b>以租代買</b> 分享經濟效益最大化<br></div>",
"<div class='content' style='margin-bottom: -25px;'>出租閒置物品<br><span>輕鬆創造被動式收入</span><br> 還能多了淨空空間<br> </div>",
"<span>交換趣</span><br>  <div class='content'>享受舊愛變新歡的新奇樂趣^以物易物 多種效益</div>",
  "<span>物友圈</span>^<div class='content'>以物會友 拓展社交圈^志趣相投 豐富心視野</div>",
  "<span>公益捐贈</span><br> <div class='content'>扶助弱勢做公益^^將用不到的物資 發揮愛心^送給需要扶助的社福團體|勢族群</div>",
  "<span>物盡其用</span><br> <div class='content'>愛護地球做環保^^用不到的 放著佔地方又積灰塵 丟了可惜! ^何不給需要它的人?</div>")}

<div class="t_container mid">
    <!-- <div class="process_1">
        <img src="/themes/Okrent/img/index/1090918_03.png">
        <div class="process_2">
            <ul>
              <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                <div class="r0 txt l">{str_replace('^', '<br>', $array_txt[$i])}</div><div class="r0 r"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}">{$array_icons[$i]}</div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                  <div class="r1 l"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}">{$array_icons[$i]}</div><div class="r1 txt r">{str_replace('^', '<br>', $array_txt[$i])}
                        </div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                  <div class="r2 txt l">{str_replace('^', '<br>', $array_txt[$i])}</div><div class="r2 r"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}">{$array_icons[$i]}</div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=2','_blank ')">
                  <div class="r3 l"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}">{$array_icons[$i]}</div><div class="r3 txt r">{str_replace('^', '<br>', $array_txt[$i])}</div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=3','_blank ')">
                  <div class="r4 txt l">{str_replace('^', '<br>', $array_txt[$i])}</div><div class="r4 r"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}">{$array_icons[$i]}</div></li>{$i=$i+1}

                <li><div class="r5 l"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}">{$array_icons[$i]}</div><div class="r5 txt r">{str_replace('^', '<br>', $array_txt[$i])}</div></li>

                </ul>
        </div>
    </div> -->
{$i=0}
    <div class="process_1">
        <img src="/themes/Okrent/img/index/1090918_03.png">
        <div class="process_2">
            <ul>
              <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                <div class="r_full"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}"></div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                  <div class="r_full"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}"></div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=1','_blank ')">
                  <div class="r_full"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}"></div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=2','_blank ')">
                  <div class="r_full"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}"></div></li>{$i=$i+1}

                <li class="open_url" onclick="window.open('/list3?id_main_class=3','_blank ')">
                  <div class="r_full"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}"></div></li>{$i=$i+1}

                  <li class="open_url" onclick="window.open('/list3?id_main_class=3','_blank ')">
                    <div class="r_full"><img src="/themes/Okrent/img/index/{$array_imgs[$i]}"></div></li>

                </ul>
        </div>
    </div>

    <div class="process_3">
        <div class="title_txt">租用三部曲</div>
        <div class="content">
            <img src="/themes/Okrent/img/index/1091027-2.png">
            <div class="txt c_1"><label>挑選 | 預約</label><br>超強搜尋功能協助找到<br>最適合你的<br>預約使用期間</div>
            <div class="txt c_2"><label>簽約 | 點收</label><br>完成租賃單<br>面交點收租賃物<br>即能開始享用</div>
            <div class="txt c_3"><label>歸還 | 退押</label><br>享用後準時歸還<br>退還押金<br>讓大家都能共享</div>
        </div>
    </div>
</div>

<div class="t_container bot">
    <div class="rows_1">
        <div class="content">
            <div class="title_txt">
                <img src="/themes/Okrent/img/index/1090918_10_01.png">
                <span>{$caption_title0}</span>
            </div>

            <div class="news_row">
                <img src="/themes/Okrent/img/index/1090918_10_02.png">



                {$l_titel=array("<h5><b>露營帳篷</b></h5>","品牌<label></label>","物主<label></label>","租金<label></label>","租期<label></label>","可租日<label></label>")}
                {$l_c=array("","","白白屋","","1日租金|7日租金 15日租金|30日租金","2020-11-10")}
                {$r_titel=array("瀏覽次數<label></label>","規格型號<label></label>","<i class='far fa-thumbs-up'></i> 5.0","位置<label></label>","","")}
                {$r_c=array("38","","","桃園市","","")}
                {$l_c_s=array("","","","","position: absolute;margin-top: -15px;","position: absolute;margin-top: -15px;")}

                {$l_titel=array("<h4><b>露營帳篷</b></h4>","<b>品牌</b><label></label>","<b>租金</b><label></label>","","","")}
                {$l_c=array("","","","","","")}
                {$r_titel=array("","<b>規格型號</b><label></label>","<b>位置</b><label></label>","","","")}
                {$r_c=array("","","桃園市","","","")}
                {$l_c_s=array("","","","","","")}

            <section class="slider regular">
                {foreach $caption_data1 as $key => $value}
                    <div class="list">
                        <div class="img_frame">
                          <div class="heart">
                                    <a><i class="far fa-heart"></i></a>
                                </div>
                          <a href="List?item_id={$value.item_id}"><img src="/themes/Okrent/img/index/1090918_12.png"></a></div>
                        <div class="content_frame">

                          <div class="infor">

                            <ul>


                  <li class="sec_r"><div class="L">A123456</div><div class="R">桃園市桃園區</div></li>
                  <li><div class="title"><a href="/item" target="_blank">全功能切割機</a></div><div class="context"><a href="/item" target="_blank">用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!</a></div>
                                  <!-- <div class="more"><a href="####">...(詳全文)</a></div> -->
                              </li>

                            </ul>

                          </div>

                            <!--<div style="" class="heart_div">
                                <div class="number">{$value.item_number}</div>
                                <div class="heart">
                                    <img src="/themes/LifeHouse/img/lifego/heart.svg" alt="" style="width: 20px;">
                                    <div class="quantity">{$value.favorite}</div>
                                </div>
                            </div>
                            <div class="caption"><a href="List">{$value.main_title}</a></div>
                            <div class="caption2">{$value.title}</div>
                            <div class="heart_wrap">
                                <div class="price_original">原價 <span class="outer"><span class="inner">{$value.price_original}日/</span></span>元</div>
                                <div class="text-center price">
                                  <div class="text-center"><span>$ {$value.price}</span>/日</div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                {/foreach}
            </section>
            <span> <a href="####"><</a> <a href="####">1</a> &nbsp <a href="####">2</a> &nbsp <a href="####">3</a> <a href="####">></a> </span>
            </div>
        </div>
    </div>

    <div class="rows_2">
            <div class="content">
                <div class="title_txt">
                    <img src="/themes/Okrent/img/index/1090918_10_01.png">
                    <span>{$caption_title1}</span></div>
                <div class="news_row">
                    <img src="/themes/Okrent/img/index/1090918_10_02.png">
                    <section class="slider regular">
                        {foreach $caption_data1 as $key => $value}
                            <div class="list">
                                <div class="img_frame">
                                  <div class="heart">
                                            <a><i class="far fa-heart"></i></a>
                                        </div>
                                  <a href="List?item_id={$value.item_id}"><img src="/themes/Okrent/img/index/1090918_13.png"></a></div>
                                <div class="content_frame">

                                  <div class="infor">

                                    <ul>


                                      <li class="sec_r"><div class="L">A123456</div><div class="R">桃園市桃園區</div></li>
                                      <li><div class="title"><a href="/item" target="_blank">全功能切割機</a></div><div class="context"><a href="/item" target="_blank">用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!</a></div>

                                    </ul>

                                  </div>

                                    <!--<div style="" class="heart_div">
                                        <div class="number">{$value.item_number}</div>
                                        <div class="heart">
                                            <img src="/themes/LifeHouse/img/lifego/heart.svg" alt="" style="width: 20px;">
                                            <div class="quantity">{$value.favorite}</div>
                                        </div>
                                    </div>
                                    <div class="caption"><a href="List">{$value.main_title}</a></div>
                                    <div class="caption2">{$value.title}</div>
                                    <div class="heart_wrap">
                                      <div class="price_original">原價 <span class="outer"><span class="inner">{$value.price_original}日/</span></span>元</div>
                                      <div class="text-center price">
                                        <div class="text-center"><span>$ {$value.price}</span>/日</div>
                                      </div>
                                    </div>-->
                                </div>
                            </div>
                        {/foreach}
                    </section>
                    <span> <a href="####"><</a> <a href="####">1</a> &nbsp <a href="####">2</a> &nbsp <a href="####">3</a> <a href="####">></a> </span>
                </div>
            </div>
        </div>

    <div class="rows_3">
      <!-- <i class="fas fa-chevron-right"></i>
      <i class="fas fa-chevron-left"></i> -->
        <div class="content">
            <div class="title_txt">
                <span>使用分享</span>
            </div>
            <div class="news_row">
                <ul class="content_ul">
                  {$content_li_img=array("1090918_14.png","1090918_15.png")}
                  {$content_li=array("物盡其用做公益愛護地球做環保^","物盡其用做公益愛護地球做環保^")}
                  {$content_li_context=array("用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!何不贈送給需要扶助的社福團體","用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!何不贈送給需要扶助的社福團體")}
                  {for $i=0;$i<sizeof($content_li_img);$i++}
                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/{$content_li_img[$i]}"></li>
                            <li><div class="title">{str_replace('^', '<br>', $content_li[$i])}</div><div class="context">{str_replace('^', '<br>', $content_li_context[$i])}</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>
                    {/for}
                    <!-- <li class="content_li">
                        <ul class="content_2">
                            <li><img src="/themes/Okrent/img/index/1090918_15.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>

    <div class="rows_4">
        <div class="content">
            <div class="title_txt">
                <span>最新消息</span>
            </div>
            <div class="news_row">
                <ul class="content_ul">
                  {$content_li_img=array("1090918_16.png","1090918_17.png","1090918_18.png","1090918_19.png")}
                  {$content_li=array("物盡其用做公益愛護地球做環保^",
                  "物盡其用做公益愛護地球做環保^",
                  "物盡其用做公益愛護地球做環保^",
                  "物盡其用做公益愛護地球做環保^")}
                  {$content_li_context=array("用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!何不贈送給需要扶助的社福團體","用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!何不贈送給需要扶助的社福團體","用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!何不贈送給需要扶助的社福團體","用不到的放著估地方又積灰塵丟了可惜!送人又不好意思!何不贈送給需要扶助的社福團體")}
                  {for $i=0;$i<sizeof($content_li_img);$i++}
                  <li class="content_li">
                      <ul class="content_1">
                          <li><img src="/themes/Okrent/img/index/{$content_li_img[$i]}"></li>
                          <li><div class="title">{str_replace('^', '<br>', $content_li[$i])}</div><div class="context">{str_replace('^', '<br>', $content_li_context[$i])}</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                      </ul>
                  </li>
                  {/for}
                    <!-- <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_17.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>

                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_18.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li>

                    <li class="content_li">
                        <ul class="content_1">
                            <li><img src="/themes/Okrent/img/index/1090918_19.png"></li>
                            <li><div class="title">物盡其用做公益<br>愛護地球做環保</div><div class="context">用不到的放著估地方又積灰塵<br>丟了可惜!送人又不好意思!何<br>不贈送給需要扶助的社福團體</div><div class="more"><a href="####">...(詳全文)</a></div></li>
                        </ul>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</div>
    {$js}
