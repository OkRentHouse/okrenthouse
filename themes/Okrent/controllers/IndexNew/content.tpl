
  <style class="INLINE_PEN_STYLESHEET_ID">
    @import url('https://fonts.googleapis.com/css?family=Raleway:400,700,900');

/* Base styling */

body {
    font-family:Microsoft JhengHei;
}

.search__container {
        #padding-top: 80px;
    }

.search__title {
        font-size: 22px;
        font-weight: 900;
        text-align: center;
        color: #ff8b88;
    }

.search__input {
        width: 11rem;
        padding: 2px 16px;

        background-color: transparent;
        transition: transform 250ms ease-in-out;
        font-size: 14pt;
        line-height: 18px;
        
        color: #575756;
        background-color: transparent;
 
      background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'%3E%3Cpath d='M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z'/%3E%3Cpath d='M0 0h24v24H0z' fill='none'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-size: 18px 18px;
        background-position: 95% center;
        border-radius: 30px;
        border: 1px solid #84be2b;
        transition: all 250ms ease-in-out;
        backface-visibility: hidden;
        transform-style: preserve-3d;
    }

.search__input::placeholder {
            color: rgba(87, 87, 86, 0.8);
            text-transform: uppercase;
            letter-spacing: 1.5px;
        }

.search__input:hover,
        .search__input:focus {
            padding: 12px 0;
            outline: 0;
            border: 1px solid transparent;
            border-bottom: 1px solid #575756;
            border-radius: 0;
            background-position: 100% center;
        }

.credits__container {
        #margin-top: 24px;
    }

.credits__text {
        text-align: center;
        font-size: 13px;
        line-height: 18px;
    }

.credits__link {
        color: #ff8b88;
        text-decoration: none;
        transition: color 250ms ease-in;
    }

.credits__link:hover,
        .credits__link:focus {
            color: color(#ff8b88 blackness(+25%));
        }
</style>

  <section class="menu cid-s48OLK6784" once="menu" id="menu1-h">
    
    <nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg">
        <div class="container">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="#">
                        <img src="themes/Okrent/image/logo.png" alt="share">
                    </a>
                </span>
                <div class="search__container">
                    <input class="search__input" type="text" placeholder="搜尋">
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true"><li class="nav-item"><a class="nav-link link text-black display-4" href="#">註冊</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#">登入</a></li>
                    <li class="nav-item"><a class="nav-link link text-black display-4" href="#">快速刊登&nbsp; &nbsp;</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#"><span class="mbrib-smile-face mbr-iconfont mbr-iconfont-btn"></span></a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#"><span class="socicon socicon-sharethis mbr-iconfont mbr-iconfont-btn"></span>&nbsp; &nbsp;</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#">&nbsp;&nbsp;</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="#">&nbsp;&nbsp;</a></li></ul>
                
                
            </div>
        </div>
    </nav>

</section>

<section class="image3 cid-sr9vj9KxfE mbr-fullscreen" id="image3-q">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-12">
                <div class="image-wrapper">
                    <img src="themes/Okrent/image/index-0308_04.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
    
<section class="features3 cid-sr9PFoETcB" id="features3-r">
    <div class="container-fluid">
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_07.jpg">
                    </div>
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_09.jpg">
                    </div>
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_11.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_13.jpg">
                    </div>
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_15.jpg">
                    </div>
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_17.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="features23 cid-sr9tH0ZgZ8" id="features24-l">

    
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card-wrapper mb-4">
                    <div class="card-box align-center">   
        <div class="mbr-section-head">
                <div class="item-wrapper">
                    <div class="item-img" style="text-align: center;">
                        <img src="themes/Okrent/image/index-0308_20.jpg" style="max-width: 480px;display:inline;">
                    </div>
                </div>
        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="item first mbr-flex p-4">
                    <div class="icon-wrap w-100">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_24.jpg">
                    </div>
                </div>
                    </div>

                    
                </div>
                <!-- <span mbr-icon class="mbr-iconfont mobi-mbri-devices mobi-mbri"></span> -->
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="item mbr-flex p-4">
                    <div class="icon-wrap w-100">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_26.jpg">
                    </div>
                </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="item mbr-flex p-4">
                    <div class="icon-wrap w-100">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/index-0308_28.jpg">
                    </div>
                </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
    
    
<section class="image3 cid-sr9vj9KxfE mbr-fullscreen" id="image3-q">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-12">
                <div class="image-wrapper">
                    <img src="themes/Okrent/image/index-0308_33.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
    
    
<section class="gallery3 cid-sr9s8AExmN" id="gallery3-k">
    
    
    <div class="container-fluid">
        <div class="mbr-section-head">
                <div class="item-wrapper">
                    <div class="item-img" style="text-align: center;">
                        <img src="themes/Okrent/image/index-0308_36.jpg" style="max-width: 480px;display:inline;">
                    </div>
                </div>
        </div>
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item1.jpg">
                        <a href="#">
                    </a>
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-4">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item2.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item3.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div><div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item4.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            
        </div>
    </div>
    
</section>

<section class="gallery3 cid-sr9s8AExmN" id="gallery3-k">
    
    
    <div class="container-fluid">
        <div class="mbr-section-head">
                <div class="item-wrapper">
                    <div class="item-img" style="text-align: center;">
                        <img src="themes/Okrent/image/index-0308_53.jpg" style="max-width: 480px;display:inline;">
                    </div>
                </div>
        </div>
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item1.jpg">
                        <a href="#">
                    </a>
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-4">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item2.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item3.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div><div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item4.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            
        </div>
    </div>
    
</section>
    
<section class="gallery3 cid-sr9s8AExmN" id="gallery3-k">
    
    
    <div class="container-fluid">
        <div class="mbr-section-head">
                <div class="item-wrapper">
                    <div class="item-img" style="text-align: center;">
                        <img src="themes/Okrent/image/index-0308_56.jpg" style="max-width: 480px;display:inline;">
                    </div>
                </div>
        </div>
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item1.jpg">
                        <a href="#">
                    </a>
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-4">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item2.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item3.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div><div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="themes/Okrent/image/item4.jpg">
                    </div>
                    <!--
                    <div class="item-content">
                        <h5 class="item-title mbr-fonts-style display-7">
                            <strong>家用IH煙燻鍋</strong>
                        </h5>
                        
                        <p class="mbr-text mbr-fonts-style mt-3 display-7">在家輕鬆享受燻製樂趣,簡單快速</p>
                    </div>
                    -->
                </div>
            </div>
            
        </div>
    </div>
    
</section>
    
<section class="features14 cid-sr9tRBDgM7" id="features15-o">
        <div class="mbr-section-head">
                <div class="item-wrapper">
                    <div class="item-img" style="text-align: center;">
                        <img src="themes/Okrent/image/index-0308_58.jpg" style="max-width: 480px;display:inline;">
                    </div>
                </div>
        </div>
    <div class="container" style="margin-top: 1rem;">
        <div class="row justify-content-center">
            <div class="card col-12 col-md-6 col-lg-3">
                <div class="card-wrapper">
                    <span class=""></span>
                    <div class="card-box" style="font-size:0.6rem;">
                        <h4 class="card-title mbr-fonts-style mb-2" style="font-size:1rem;"><strong>⚉裝潢大仁哥:</strong></h4>
                        <h5 class="card-text mbr-fonts-style display-7">如何解決開放式廚房的油煙
排放問題？</h5>         
很多屋主嚮往歐美開放式廚房，但因為
飲食型態與烹調方式不盡相同，若完全
依照歐美國家使用...(詳細內容)
                    </div>
                </div>
            </div>
            <div class="card col-12 col-md-6 col-lg-3">
                <div class="card-wrapper">
                    <span class=""></span>
                    <div class="card-box" style="font-size:0.6rem;">
                        <h4 class="card-title mbr-fonts-style mb-2" style="font-size:1rem;"><strong>⚉裝潢大仁哥:</strong></h4>
                        <h5 class="card-text mbr-fonts-style display-7">如何解決開放式廚房的油煙
排放問題？</h5>         
很多屋主嚮往歐美開放式廚房，但因為
飲食型態與烹調方式不盡相同，若完全
依照歐美國家使用...(詳細內容)
                    </div>
                </div>
            </div>
            <div class="card col-12 col-md-6 col-lg-3">
                <div class="card-wrapper">
                    <span class=""></span>
                    <div class="card-box" style="font-size:0.6rem;">
                        <h4 class="card-title mbr-fonts-style mb-2" style="font-size:1rem;"><strong>⚉裝潢大仁哥:</strong></h4>
                        <h5 class="card-text mbr-fonts-style display-7">如何解決開放式廚房的油煙
排放問題？</h5>         
很多屋主嚮往歐美開放式廚房，但因為
飲食型態與烹調方式不盡相同，若完全
依照歐美國家使用...(詳細內容)
                    </div>

                </div>
            </div>
            <div class="card col-12 col-md-6 col-lg-3">
                <div class="card-wrapper">
                    <span class=""></span>
                    <div class="card-box" style="font-size:0.6rem;">
                        <h4 class="card-title mbr-fonts-style mb-2" style="font-size:1rem;"><strong>⚉裝潢大仁哥:</strong></h4>
                        <h5 class="card-text mbr-fonts-style display-7">如何解決開放式廚房的油煙
排放問題？</h5>         
很多屋主嚮往歐美開放式廚房，但因為
飲食型態與烹調方式不盡相同，若完全
依照歐美國家使用...(詳細內容)
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="footer3 cid-s48P1Icc8J" once="footers" id="footer3-i" style="background-color: #aaa;">

    

    

    <div class="container">
        <div class="media-container-row align-center mbr-white">
            <div class="row row-links">
                <ul class="foot-menu">
                    
                    
                    
                    
                    
                <li class="foot-menu-item mbr-fonts-style display-7"><a href="#/help/" class="text-success" style="font-size: 11pt;">圈友中心</a></li><li class="foot-menu-item mbr-fonts-style display-7"><a href="#" class="text-success" style="font-size: 11pt;">客服中心</a></li><li class="foot-menu-item mbr-fonts-style display-7"><a href="#/" class="text-success" style="font-size: 11pt;">訊息中心</a></li><li class="foot-menu-item mbr-fonts-style display-7"><a href="#/" class="text-success" style="font-size: 11pt;">關於我們</a></li><li class="foot-menu-item mbr-fonts-style display-7">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</li><li class="foot-menu-item mbr-fonts-style" style="font-size: 11pt;"><a href="#/" class="text-success">@若對平台使用上有任何建議與疑問</a></li></ul>
            </div>
            <div class="row social-row">
                <div class="social-list align-right pb-2">
                    
                    
                    
                    
                    
                    
                <div class="soc-item">

                    </div><div class="soc-item">

                    </div></div>
            </div>
            <div class="row row-copirayt">
                <p class="mbr-text mbr-fonts-style mt-3 display-4" style="color: #333333">
                        <img src="themes/Okrent/image/index-0308_103.jpg" style="max-width: 70px;display:inline;">
                                     生活好科技有限公司     Copyright © LIFE MASTER TECHNOLOGY COMPANY
                </p>
            </div>
        </div>
    </div>
</section><section style="background-color: #aaa; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Helvetica Neue', Arial, sans-serif; color:#aaa; font-size:12px; padding: 0; align-items: center; display: flex;"><a href="https://mobirise.site/y" style="flex: 1 1; height: 3rem; padding-left: 1rem;"></a>
    </section><script src="themes/Okrent/assets/web/assets/jquery/jquery.min.js"></script>  <script src="themes/Okrent/assets/popper/popper.min.js"></script>  <script src="themes/Okrent/assets/tether/tether.min.js"></script>  <script src="themes/Okrent/assets/bootstrap/js/bootstrap.min.js"></script>  <script src="themes/Okrent/assets/smoothscroll/smooth-scroll.js"></script>  <script src="themes/Okrent/assets/parallax/jarallax.min.js"></script>  <script src="themes/Okrent/assets/dropdown/js/nav-dropdown.js"></script>  <script src="themes/Okrent/assets/dropdown/js/navbar-dropdown.js"></script>  <script src="themes/Okrent/assets/touchswipe/jquery.touch-swipe.min.js"></script>  <script src="themes/Okrent/assets/theme/js/script.js"></script>
