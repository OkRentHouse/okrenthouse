<div class="commodity">
	<div class="photo_row"><a href="#"><img src="{$house.img}" class="img" alt="{$house.title}"></a>
		<collect data-id="{$house.id}" data-type="house" class="favorite {if $house.collect}on{/if}">{if $house.collect}
				<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}
		</collect>
	</div><div class="pattern"><img src="{$house.pattern}"></div><data class="data">
		<h3 class="title big"><a href="#">早安藝文</a></h3>
		<h3 class="title">大器方正格局 中正藝文特區</h3>
		<div>桃園市桃園區新埔八街</div>
		<div>大樓<i></i>獨立套房<i></i>5/16樓</div>
		<div>3房　1廳　2衛<i></i>38.52坪</div>
		<tag>
			<i><img src="{$THEME_URL}/img/icon/furniture.svg" alt="含傢俱" title="含傢俱"></i>
			<i><img src="{$THEME_URL}/img/icon/tv.svg" alt="含家電" title="含家電"></i>
			<i><img src="{$THEME_URL}/img/icon/parking.svg" alt="停車場" title="停車場"></i>
			<i><img src="{$THEME_URL}/img/icon/kitchenware.svg" alt="含廚具" title="含廚具"></i>
			<i><img src="{$THEME_URL}/img/icon/pet.svg" alt="可養寵物" title="可養寵物"></i>
		</tag>
		<price>16,800<unit>元</unit></price>
		<div class="management">含管理費</div>
	</data>
</div>