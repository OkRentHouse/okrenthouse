<form action="https://okrent.house/list" method="get" id="search_box_big">
    <div class="search_list_wrap">

        <div class="search_div">
{*            <strong>區域</strong>*}
            <div class="city_select">
                <spam class="county_city search search_1">
                    區域
                </spam>
            </div><img class="class_select_img class_select_img_2" src="themes/Rent/img/select_D.png" style="margin-top:3px">
            <div class="county_div" style="display: none;">
                {foreach from=$select_county key=i item=county}<spam class="county"><input type="radio" name="county" id="county_{$i}" value="{$county.value}" {if $county.value == $smarty.get.county}checked{/if}><label for="county_{$i}">{$county.title}</label></spam>{/foreach}
            </div>
                <div class="city_div">{foreach from=$arr_city[$smarty.get.county] key=i item=city}<spam class="city"><input type="checkbox" name="city[]" id="city_{$i}" value="{$city}" {if in_array($city, $smarty.get.city)}checked{/if}><label for="city_{$i}">{$city}</label></spam>{/foreach}</div>
        </div>

        <div class="search_div">
            <div class="type_select search_1" id="main_house_class_all_title">
                住家
            </div><img class="class_select_img class_select_img_2" src="themes/Rent/img/select_D.png" style="margin-top:3px">
            <div class="patterns none">
                <input type="radio" id="id_main_house_class_all" value=""{if count($smarty.get.id_main_house_class) == 0} checked{/if}>
                <label for="id_main_house_class_all" id="id_main_house_class_name">全部</label>
                {foreach from=$main_house_class key=i item=v }
                    <input type="checkbox" name="id_main_house_class[]" id="id_main_house_class_{$main_house_class.$i.id_main_house_class}" value="{$main_house_class.$i.id_main_house_class}"
                           {if in_array($main_house_class.$i.id_main_house_class, $smarty.get.id_main_house_class)}checked{/if}>
                            <label for="id_main_house_class_{$main_house_class.$i.id_main_house_class}" id="main_house_class_name_{$main_house_class.$i.id_main_house_class}">{$main_house_class.$i.title}</label>
                {/foreach}
            </div>
        </div>

        <div class="search_div">
            <div class="class_select" id="types_all_name">
                電梯大樓
            </div><img class="class_select_img class_select_img_2" src="themes/Rent/img/select_D.png" style="margin-top:3px">
            <div class="category none">
                <input type="radio" id="id_types_all" value=""{if count($smarty.get.id_types) == 0} checked{/if}>
                <label for="id_types_all" id="id_types">全部</label>
                {foreach from=$select_types key=i item=types}
                    <input type="checkbox" name="id_types[]" id="id_types_{$types.id_rent_house_types}" value="{$types.id_rent_house_types}"
                           {if in_array($types.id_rent_house_types, $smarty.get.id_types)}checked{/if}>
                            <label for="id_types_{$types.id_rent_house_types}" id="types_name_{$types.id_rent_house_types}">{$types.title}</label>
                {/foreach}
            </div>
        </div>

        <div class="search_div">
            <div class="houses_select" id="type_all_name">
                整層住家
            </div><img class="class_select_img class_select_img_2" src="themes/Rent/img/select_D.png" style="margin-top:3px">
            <div class="houses none">
                <input type="radio" id="type_all" value=""{if count($smarty.get.id_type) == 0} checked{/if}><label for="type_all" id="type_name">全部</label>{foreach from=$select_type key=i item=type}<input type="checkbox" name="id_type[]" id="id_type_{$type.id_rent_house_type}" value="{$type.id_rent_house_type}" {if in_array($type.id_rent_house_type, $smarty.get.id_type)}checked{/if}><label for="id_type_{$type.id_rent_house_type}" id="type_name_{$type.id_rent_house_type}">{$type.title}</label>{/foreach}
            </div>
        </div>


        <div class="search_input"><input type="text" class="field w50p2" name="search" id="search"
                                         value="" placeholder="請輸入 社區 街道 名稱等 關鍵字或物件編號">
        </div>

{*        <button class="close_all search_btn" data-toggle="collapse" aria-expanded="false">*}
{*            全部清除*}
{*        </button>*}

        <button class="search_icon" type="submit"><img src="{$THEME_URL}/img/icon/pic_m.svg"></button>
        <button class="search_icon search_icon_map" type="button"><a href="https://www.okrent.house/HouseMap?&county=%E6%A1%83%E5%9C%92%E5%B8%82"><img src="{$THEME_URL}/img/icon/map_m.svg"></a></button>
    </div>
</form>
<script>
    arr_city = {$arr_city|json_encode:1};
    {$add_js}
</script>