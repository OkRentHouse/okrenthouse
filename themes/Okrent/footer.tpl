{if $group_link=='1'}
      {if preg_match("/list/i",FILENAME)==0 && preg_match("/item/i",FILENAME)==0}
        {include file="group_link.tpl"}
      {/if}
{/if}
</article>
{if $display_footer}
<footer>
      <div class="footer_0"></div>
      {if !empty($footer_txt)}<div class="footer">{$footer_txt}</div>{/if}
      {if !empty($footer_txt1)}<div class="footer_1">{$footer_txt1}</div>{/if}
      {if !empty($footer_txt2)}<div class="footer_2">{$footer_txt2}</div>{/if}
      {if !empty($footer_txt3)}<div class="footer_3">{$footer_txt3}</div>{/if}
      {*include file="../../../../modules/DesigningFooter/designingfooter.tpl"*}
</footer>
{/if}
      {foreach from=$css_footer_files key=key item=css_uri}
            <link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
      {/foreach}
      {foreach from=$js_footer_files key=key item=js_uri}
            <script src="{$js_uri}"></script>
      {/foreach}
      </div>
      <input type="hidden" value="\themes\Okrent\footer.tpl">
</body>
</html>
