$(document).ready(function(e) {
    $("#signup").validate({
        rules: {
            user: {
                required: true,
                maxlength: 50,
                minlength: 6
            },
            password: {
                required: true,
                maxlength: 20,
                minlength: 6
            },
            check_password: {
                required: true,
                maxlength: 20,
                minlength: 6,
                equalTo: "#password"
            },
            name: {
                required: true,
                maxlength: 20,
                minlength: 2
            },
            gender:{
                required: true,
            }

        },
        messages :{
            user:{
                required:'請輸入電話號碼',
                maxlength: '長度不能超過50字母',
                minlength: '長度不能少於6字母'
            },
            password: {
                required: '請輸入密碼',
                maxlength: '長度不能超過20字母',
                minlength: '長度不能少於6字母'
            },
            check_password: {
                required: '請輸入密碼',
                maxlength: '長度不能超過20字母',
                minlength: '長度不能少於6字母',
                equalTo: '請與密碼輸入相同'
            },
            name: {
                required: '請輸入姓名',
                maxlength: '長度不能超過20字母',
                minlength: '長度不能少於過2字母',
            },
            gender:{
                required: '請選擇性別',
            }
        }
    })
});