function forwarding(i){
  //console.log($(e).text());
  // $(e).parents(".search_item").find("a.dropdown-toggle:not(.ch)").text($(e).text());
  var name = $(i).data("name");
  var value = $(i).data("value");
  if(name!='no' && name!='city'){//因為這兩個已經有轉換過了因此不需要轉
    $("input[name='"+name+"']").val(value);
  }
  var a_data = '';
  $(".t_container input").each(function(e,item) {
    var a_name = $(item).attr("name");
    var a_value =  $(item).val();
    if(a_name=='id_product_class_item' && ($("input[name='id_product_class_before']").val() !=$("input[name='id_product_class']").val() )){
        return;
    }
    a_data +='&'+a_name+'='+a_value;
  });
  location.href = "/list3?"+a_data;
}

function county_city(i){
  var name = $(i).data("name");
  var value = $(i).data("value");
  $("input[name='"+name+"']").val(value);//給所點選的city or county加值
  var county = $("input[name='county']").val();
  var city = $("input[name='city']").val();
  $(i).parents(".search_item").find("a.dropdown-toggle:not(.ch)").text(county+city);
  if(name=='city'){
    forwarding(i);//呼叫轉址
  }
}

var citys_a;
$(document).ready(function(){
  //citys_a=$(".d-menu.citys").find("li");
  citys_a=$(".city.options").find("li");

  $.get( "https://cors-anywhere.herokuapp.com/http://api.opencube.tw/twzipcode/get-citys", function( data ) {
    //var citys=$(".d-menu.citys").find("li");
    var citys=$(".city.options").find("li");
    var new_citys;
    //$(".d-menu.citys").find("li").remove();
    $(".city.options").find("li").remove();
    data["data"].forEach(function(item, i){
      $(citys).find("a").text(item);
      $(citys).find("a").addClass("dropdown-toggle");
      $(citys).find("a").addClass("ch");
      $(citys).find("a").attr("data-value", item);
      $(citys).find("a").attr("data-name", "county");

      new_citys=$(citys);
      // $(".d-menu.citys").append("<li data-content='"+item+"' class='county_"+i+"'>"+$(new_citys).html()+"</li>");
      // $(".d-menu.citys .county_"+i).append("<ul class='districts' data-role='dropdown'><li><img src='/themes/Okrent/img/list/6d0b072c3d3f.gif'></li></ul>");
      if($("input[name='county']").val()==item){
        $(".city.options").append("<li data-content='"+item+"' class='county_"+i+" active'>"+$(new_citys).html()+"</li>");
        //$(".left_mune li.zone > ul").css("display","block");
        console.log("new_citys"+item+" / i"+i);
        city_click($(".county_"+i),i);
      }else{
        $(".city.options").append("<li data-content='"+item+"' class='county_"+i+"'>"+$(new_citys).html()+"</li>");
      }
      $(".city.options .county_"+i).append("<ul class='districts' data-role='dropdown'><li><img src='/themes/Okrent/img/list/6d0b072c3d3f.gif'></li></ul>");
      //$(".city_"+i).on('click',function(){city_click($(this),i)});
      $(".county_"+i+" a").on('click',function(){city_click($(this).parents(".county_"+i),i)});
    });
  });
})

function city_click(c,i){
  //https://api.opencube.tw/twzipcode?city=c
  //console.log($(c).attr("data-content"));
  var new_ul;
  var new_a;
  //if (!$(".city_"+i+" ul li a").is(event.target)) {
    //console.log($(".city_"+i).find("ul").html());

    $.get( "https://cors-anywhere.herokuapp.com/https://api.opencube.tw/twzipcode?city="+$(c).attr("data-content"), function( data ) {
      $(".county_"+i).find("li").remove();
      //new_ul=$(".city_"+i).append("<ul class='districts' data-role='dropdown'></ul>");
      new_ul=c;
      data["data"].forEach(function(item, i){
          new_a=item["district"];
          //$(new_a).text(item["district"]);
          if($("input[name='city']").val() == item["district"]){
            $(new_ul).find("ul").append("<li data-content='"+item["district"]+"' class='city_"+i+" active'>"+$(citys_a).html()+"</li>");
            $(".city_"+i).parent().css("display","block");
          }else{
            $(new_ul).find("ul").append("<li data-content='"+item["district"]+"' class='city_"+i+"'>"+$(citys_a).html()+"</li>");
          }
          $(new_ul).find("ul").find(".city_"+i).find("a").removeClass("dropdown-toggle").text(new_a);
          $(new_ul).find("ul").find(".city_"+i).find("a").attr("data-value", item["district"]);
          $(new_ul).find("ul").find(".city_"+i).find("a").attr("data-name", "city");
      });
      });
    //}
}
