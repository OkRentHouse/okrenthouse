<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link rel="icon" href="/themes/Okrent/img/favicon.png" type="image/x-icon" />
  {if !empty($GOOGLE_SITE_VERIFICATION)}<meta name="google-site-verification" content="{$GOOGLE_SITE_VERIFICATION}" />{/if}
  <title>{$meta_title}</title>
  {if $meta_description != ''}<meta name="description" content="{$meta_description}">{/if}
  {if $meta_keywords != ''}<meta name="keywords" content="{$meta_keywords}">{/if}
	<link href="themes/Okrent/assets/web/assets/mobirise-icons2/mobirise2.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/web/assets/mobirise-icons-bold/mobirise-icons-bold.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/tether/tether.min.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/bootstrap/css/bootstrap.min.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/bootstrap/css/bootstrap-grid.min.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/bootstrap/css/bootstrap-reboot.min.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/dropdown/css/style.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/socicon/css/styles.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/theme/css/style.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
	<link href="themes/Okrent/assets/mobirise/css/mbr-additional.css?v=1.0.0.4" rel="stylesheet" type="text/css"/>
  {literal}
  <style class="INLINE_PEN_STYLESHEET_ID">
    @import url('https://fonts.googleapis.com/css?family=Raleway:400,700,900');

/* Base styling */

body {
    font-family:Microsoft JhengHei;
}

.search__container {
        #padding-top: 80px;
    }

.search__title {
        font-size: 22px;
        font-weight: 900;
        text-align: center;
        color: #ff8b88;
    }

.search__input {
        width: 11rem;
        padding: 2px 16px;

        background-color: transparent;
        transition: transform 250ms ease-in-out;
        font-size: 14pt;
        line-height: 18px;
        
        color: #575756;
        background-color: transparent;
 
      background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'%3E%3Cpath d='M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z'/%3E%3Cpath d='M0 0h24v24H0z' fill='none'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-size: 18px 18px;
        background-position: 95% center;
        border-radius: 30px;
        border: 1px solid #84be2b;
        transition: all 250ms ease-in-out;
        backface-visibility: hidden;
        transform-style: preserve-3d;
    }

.search__input::placeholder {
            color: rgba(87, 87, 86, 0.8);
            text-transform: uppercase;
            letter-spacing: 1.5px;
        }

.search__input:hover,
        .search__input:focus {
            padding: 12px 0;
            outline: 0;
            border: 1px solid transparent;
            border-bottom: 1px solid #575756;
            border-radius: 0;
            background-position: 100% center;
        }

.credits__container {
        #margin-top: 24px;
    }

.credits__text {
        text-align: center;
        font-size: 13px;
        line-height: 18px;
    }

.credits__link {
        color: #ff8b88;
        text-decoration: none;
        transition: color 250ms ease-in;
    }

.credits__link:hover,
        .credits__link:focus {
            color: color(#ff8b88 blackness(+25%));
        }
.hamburger span{color:#84be2b;}
.cid-s48P1Icc8J{background-color:#ececec;}
.item-img{background-color:#fff;}
.foot-menu-item.mbr-fonts-style.display-7{font-size:.9rem;}
.cid-s48OLK6784 .container{     justify-content: space-between;}
nav.navbar .container{flex-wrap: initial;}
.search__container.mobile{ display:none}
.search__container.pc{ display:block}
.search__input.mo{ display:block}
.container-fluid{padding:0 5rem;}
.search_bar{display:none;}

@media (max-width: 992px){
  .cid-s48OLK6784 .navbar .navbar-collapse{z-index: 1;
    width: calc(100vw + 12rem);
    position: absolute;
    left: -12rem;
    top: 60px;
    height: 225px;
    background: #fff;
}
@media (max-width: 768px){
        .mo_search{ width:24px; height:24px;opacity:0.3;}
         .cid-s48OLK6784 .container{ flex-direction: row-reverse;}
         .search__container.mobile{  display:block;}
         .search__input.mo{ display:none; width:100%}
         .search__container.pc{ display:none}
         .cid-s48OLK6784 .navbar .navbar-collapse{top: 55px;left: -3rem;width: calc(100vw + 3rem);}
         .container-fluid{padding:0 3rem;}
         .search_bar{display:block;}
}
@media (min-width: 992px){
        .container, .container-lg, .container-md, .container-sm{max-width: 90%;padding:0 5rem;}
}
</style>
  {/literal}
</head>
<body>