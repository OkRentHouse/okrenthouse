<div class="col-2 left_mune">
    {if $smarty.get.id_product_class}<span>{foreach $head_product_class as $key =>$value}{if $value.id_product_class==$smarty.get.id_product_class}{$value.title}{/if}{/foreach}</span>{/if}
    <ul class="options">
        {if !empty($smarty.get.id_product_class)}
            {foreach $head_product_class_item as $key => $value}
                <li><a href="/list2?id_product_class_item={$value.id_product_class_item}{foreach $smarty.get as $k => $v}&{$k}={$v}{/foreach}">{$value.title}</a></li>
            {/foreach}
        {/if}
    </ul>

    <li class="options Sort">排序
        <div>
            <input type="checkbox" name="sort[]" value="1" {if strstr(1,$smarty.get.sort)} checked {/if}>更新時間<br>
            <input type="checkbox" name="sort[]" value="2" {if strstr(2,$smarty.get.sort)} checked {/if}>金額(低到高)<br>
            <input type="checkbox" name="sort[]" value="3" {if strstr(3,$smarty.get.sort)} checked {/if}>待租/換中<br>
            <input type="checkbox" name="sort[]" value="4" {if strstr(4,$smarty.get.sort)} checked {/if}>評價數(高到低)<br>
            <input type="checkbox" name="sort[]" value="5" {if strstr(5,$smarty.get.sort)} checked {/if}>點閱數(高到低)<br>
        </div>
    </li>

    <ul>
        <li class="other_watching">其他人也看了
            <div>
                <ul class="content_ul">
                    {foreach $other_look_product as $k_o => $v_o}
                        <li class="content_li">
                            <ul class="content_1">
                                <li><img style="width: 215px;height: 215px;max-width: 100%" src="{$v_o.img}"></li>
                                <li class="down_area"><div class="title">{$v_o.name}</div><div class="context">{$v_o.title}</div>
                                </li>
                            </ul>
                        </li>
                    {/foreach}
                </ul>
            </div>
        </li>
    </ul>
</div>