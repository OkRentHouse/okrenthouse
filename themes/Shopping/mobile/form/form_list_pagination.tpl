{if $form_list_pagination_max > 1}
  <div class="text-center">
    <nav class="visible-lg-block">
      <ul class="pagination pagination-lg">
        <li{if $smarty.get.p <= 1} class="disabled"{/if}><a href="?p={max($smarty.get.p-1, 1)}&{$form_list_pagination_url}"><span aria-hidden="true">«</span><span class="sr-only">{l s='Previous'}</span></a></li>
        {for $pagination_p=$form_list_pagination_min to $form_list_pagination_max max=$form_list_pagination_num}
          <li{if ($smarty.get.p == $pagination_p) || (($pagination_p == 1) && ($smarty.get.p <= 1)) || (($pagination_p == $form_list_pagination_max) && ($smarty.get.p >= $form_list_pagination_max))} class="active"{/if}><a href="?p={$pagination_p}&{$form_list_pagination_url}">{$pagination_p}<span class="sr-only">(current)</span></a></li>
        {/for}
        <li{if ($smarty.get.p >= $form_list_pagination_max) || ($smarty.get.p == $form_list_pagination_max)} class="disabled"{/if}><a href="?p={min(max($smarty.get.p+1, 2), $form_list_pagination_max)}&{$form_list_pagination_url}"><span aria-hidden="true">»</span><span class="sr-only">{l s='Next'}</span></a></li>
      </ul>
    </nav>
    <nav class="visible-md-block">
      <ul class="pagination">
        <li{if $smarty.get.p <= 1} class="disabled"{/if}><a href="?p={max($smarty.get.p-1, 1)}&{$form_list_pagination_url}"><span aria-hidden="true">«</span><span class="sr-only">{l s='Previous'}</span></a></li>
        {for $pagination_p=$form_list_pagination_min to $form_list_pagination_max max=$form_list_pagination_num}
          <li{if ($smarty.get.p == $pagination_p) || (($pagination_p == 1) && ($smarty.get.p <= 1)) || (($pagination_p == $form_list_pagination_max) && ($smarty.get.p >= $form_list_pagination_max))} class="active"{/if}><a href="?p={$pagination_p}&{$form_list_pagination_url}">{$pagination_p}<span class="sr-only">(current)</span></a></li>
        {/for}
        <li{if ($smarty.get.p >= $form_list_pagination_max) || ($smarty.get.p == $form_list_pagination_max)} class="disabled"{/if}><a href="?p={min(max($smarty.get.p+1, 2), $form_list_pagination_max)}&{$form_list_pagination_url}"><span aria-hidden="true">»</span><span class="sr-only">{l s='Next'}</span></a></li>
      </ul>
    </nav>
    <nav class="visible-sm-block visible-xs-block">
      <ul class="pagination pagination-sm">
        <li{if $smarty.get.p <= 1} class="disabled"{/if}><a href="?p={max($smarty.get.p-1, 1)}&{$form_list_pagination_url}"><span aria-hidden="true">«</span><span class="sr-only">{l s='Previous'}</span></a></li>
        {for $pagination_p=$form_list_pagination_min to $form_list_pagination_max max=$form_list_pagination_num}
          <li{if ($smarty.get.p == $pagination_p) || (($pagination_p == 1) && ($smarty.get.p <= 1)) || (($pagination_p == $form_list_pagination_max) && ($smarty.get.p >= $form_list_pagination_max))} class="active"{/if}><a href="?p={$pagination_p}&{$form_list_pagination_url}">{$pagination_p}<span class="sr-only">(current)</span></a></li>
        {/for}
        <li{if ($smarty.get.p >= $form_list_pagination_max) || ($smarty.get.p == $form_list_pagination_max)} class="disabled"{/if}><a href="?p={min(max($smarty.get.p+1, 2), $form_list_pagination_max)}&{$form_list_pagination_url}"><span aria-hidden="true">»</span><span class="sr-only">{l s='Next'}</span></a></li>
      </ul>
    </nav>
  </div>
{/if}