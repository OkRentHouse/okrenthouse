{block name="defaultTab"}{strip}
	{if isset($fields.tabs)}
		<div class="count_tabs col-lg-2">
		<ul class="list-group">
			{foreach from=$fields.tabs key=link_id item=tab_title}
				<li id="tabs_{$link_id}" class="list-group-item">{$tab_title}</li>
			{/foreach}
		</ul>
		</div>{/if}
{/strip}{/block}{block name="defaultForm"}{if count($fields.form) > 0}
<form method="post"
	name="{if isset($fields.form.name)}{$fields.form.name}{elseif isset($fields.form.id)}{$fields.form.id}{else}{$table}{/if}"
	id="{if isset($fields.form.id)}{$fields.form.id}{elseif isset($fields.form.name)}{$fields.form.name}{else}{$table}{/if}"
	class="defaultForm form-horizontal{if $view} view{/if}{if isset($fields.tabs)} col-lg-10{/if}{if isset($fields.form.class)}{$fields.form.class}{/if}"
	novalidate>{/if}{foreach from=$fields.form key=form_tab_id item=form}{strip}
	<div class="tab-count tabs_{$form.tab}{if $form.tab_class} {$form.tab_class}{/if}"{if isset($fields.tabs)} style="display:none"{/if}
	     enctype="multipart/form-data">
		<div class="panel panel-default{if isset($form.class)} {$form.class}{/if}">
			{assign var=body_display value=false}
			{foreach from=$form key=key item=form_v}
			{if $key == 'html'}
			{$form_v}
			{elseif $key == 'tpl'}
			{include file=$form_v}
			{elseif $key == 'legend'}
			{block name="legend"}
			{assign var=body_display value=true}
			{if $form.heading_display}
				<div class="panel-heading">
					{if isset($form_v.image) && isset($field.title)}<img src="{$form_v.image}"
													     alt="{$form_v.title|escape:'html':'UTF-8'}" />{/if}
					{if isset($form_v.icon)}<i class="{$form_v.icon}"></i>{/if}
					{$form_v.title}
				</div>
			{/if}
			<div class="panel-body">
				{/block}
				{elseif $key == 'description' && $form_v}
				<div class="alert alert-info">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'warning' && $form_v}
				<div class="alert alert-warning">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'success' && $form_v}
				<div class="alert alert-success">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'error' && $form_v}
				<div class="alert alert-danger">{$form_v}
					<button type="button" class="close" data-dismiss="alert"><span
							  aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				</div>
				{elseif $key == 'input'}
				<div class="form-wrapper col-lg-12">
					{foreach $form_v as $input}
					{block name="input_row"}
					{if (isset($input.is_prefix) && $input.is_prefix) || (!isset($input.is_prefix) && !isset($input.is_suffix))}
					<div class=" form-group{if isset($input.form_group_class)} {$input.form_group_class}{/if}{if $input.type == 'hidden'} hide{/if}"{if isset($tabs) && isset($input.tab)} data-tab-id="{$input.tab}"{/if}>{/if}
						{if $input.type == 'hr'}
						<hr{if isset($input.id)} id="{$input.id}"{/if}{if isset($input.class)} class="{$input.class}"{/if}>
						{elseif $input.type == 'hidden'}
						<input type="hidden" name="{$input.name}" id="{$input.name}"
							 class="{if isset($input.class)}{$input.class}{/if}"
							 value="{if isset($input.string_format)}{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}{else}{$input.val|escape:'html':'UTF-8'}{/if}"/>
						{else}
						{block name="label"}
							{if isset($input.label)}
								<label class="control-label col-lg-{if isset($input.label_col) && $input.label_col > 0}{$input.label_col}{else}3{/if}{if isset($input.label_class) && $input.label_class} {$input.label_class}{/if}{if isset($input.required) && $input.required && $input.type != 'radio'} required{/if}"
									 for="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}">
									{if isset($input.hint)}
									<span class="label-tooltip" data-toggle="tooltip" data-html="true"
										title="{if is_array($input.hint)}
                                                {foreach $input.hint as $hint}
                                                      {if is_array($hint)}
                                                            {$hint.text|escape:'quotes'}
                                                      {else}
                                                            {$hint|escape:'quotes'}
                                                      {/if}
                                                {/foreach}
                                          {else}
                                                {$input.hint|escape:'quotes'}
                                          {/if}">
                                    {/if}
										{$input.label}
										{if isset($input.hint)}</span>{/if}
								</label>
							{/if}
						{/block}
						{block name="field"}
						{if (isset($input.is_prefix) && $input.is_prefix) || (!isset($input.is_prefix) && !isset($input.is_suffix))}
						<div class="col-lg-{if isset($input.col)}{$input.col|intval}{else}3{/if}{if isset($input.div_class)} {$input.div_class}{/if}{if !isset($input.label) && !$input.no_label} col-lg-offset-3{/if}">{/if}
							{if ((isset($input.prefix) || isset($input.suffix)) && (!isset($input.is_prefix) && !isset($input.is_suffix))) || $input.is_prefix}
							<div class="input-group fixed-width-lg">{/if}
								{if isset($input.prefix)}<span
									  class="input-group-addon">{$input.prefix}</span>{/if}
								{if $input.type == 'tpl'}
								{include file=$input.file}
								{elseif $input.type == 'text' || $input.type == 'number' || $input.type == 'color' || $input.type == 'email' || $input.type == 'tel' || $input.type == 'url' || $input.type == 'date' || $input.type == 'datetime' || $input.type == 'datetime-local' || $input.type == 'time'}
								<input type="{$input.type}"
									 id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
									 name="{$input.name}{if isset($input.multiple) && $input.multiple}[]{/if}"
									 class="form-control {if isset($input.class)}{$input.class}{/if}"
									 value="{if isset($input.string_format)}
											{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}
										{else}
											{$input.val|escape:'html':'UTF-8'}
										{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.min) && ($input.min || $input.min == 0)} min="{$input.min|intval}"{/if}
									  {if isset($input.max) && $input.max} max="{$input.max|intval}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
									  {if isset($input.required) && $input.required} required="required" {/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.autocorrect) && $input.autocorrect} autocorrect="{$input.autocorrect}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
								/>

								{elseif $input.type == 'select'}
								{if $input.is_prefix || $input.is_suffix}
								<div class="input-group-btn">{/if}
									<select name="{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}"
										  class="{if isset($input.class)}{$input.class|escape:'html':'utf-8'}{/if}{if $input.is_prefix || $input.is_suffix || $input.live_search} selectpicker{/if} form-control"
										  id="{if isset($input.id)}{$input.id|escape:'html':'utf-8'}{else}{$input.name|escape:'html':'utf-8'}{/if}"
										  {if isset($input.multiple) && $input.multiple} multiple="multiple"{/if}
										  {if isset($input.size)} size="{$input.size|escape:'html':'utf-8'}"{/if}
										  {if isset($input.onchange)} onchange="{$input.onchange|escape:'html':'utf-8'}"{/if}
										  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
										  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
										  {if isset($input.js) && $input.js} onchange="{$input.js}"{/if}
										  {if isset($input.required) && $input.required} required="required"{/if}
										  {if isset($input.live_search) && $input.live_search} data-live-search="true"{/if}
										  {if isset($input.data)}
										{foreach from=$input.data key=k item=$input_data}
											data-{$k}="{$input_data}"
										{/foreach}
										  {/if}>
										{if isset($input.options.default)}
											<option value="{$input.options.default.val|escape:'html':'utf-8'}"
												  {if isset($input.string_format)}
												{if isset($input.multiple)}
													{foreach $input.string_format as $input_string_format}
														{if $input_string_format == $input.options.default.val} selected="selected"{/if}
													{/foreach}
												{else}
													{if $input.string_format == $input.options.default.val} selected="selected"{/if}
												{/if}
												  {elseif isset($input.val)}
												{if isset($input.multiple)}
													{foreach $input.val as $input_val}
														{if $input_val == $input.options.default.val} selected="selected"{/if}
													{/foreach}
												{else}
													{if $input.val == $input.options.default.val} selected="selected"{/if}
												{/if}
												  {/if}>{$input.options.default.text|escape:'html':'utf-8'}</option>
										{/if}
										{if isset($input.options.val)}
											{foreach $input.options.val as $option}
												<option value="{$option.val|escape:'html':'utf-8'}"{if isset($option.parent)} data-parent="{$option.parent}"{/if}{if isset($option.parent_name)} data-parent_name="{$option.parent_name}"{/if}
													  {if isset($option.data)}
														  {foreach from=$option.data key=k item=option_data}
															  data-{$k}="{$option_data}"
														  {/foreach}
													  {/if}
													  {if isset($input.string_format)}
													{if isset($input.multiple)}
														{foreach $input.string_format as $input_string_format}
															{if $input_string_format == $option.val} selected="selected"{/if}
														{/foreach}
													{else}
														{if $input.string_format == $option.val} selected="selected"{/if}
													{/if}
													  {elseif isset($input.val)}
													{if isset($input.multiple)}
														{foreach $input.val as $input_val}
															{if $input_val == $option.val} selected="selected"{/if}
														{/foreach}
													{else}
														{if $input.val == $option.val} selected="selected"{/if}
													{/if}
													  {/if}>{$option.text|escape:'html':'utf-8'}</option>
											{/foreach}
										{/if}
									</select>
									{if $input.is_prefix || $input.is_suffix}</div>{/if}
								{elseif $input.type == 'change-password'}
								<div class="row">
									<div class="col-lg-12">
										<button type="button" id="{$input.name}-btn-change"
											  class="btn btn-default">
											<i class="icon-lock"></i>
											{l s='更改密碼'}
										</button>
										<div id="{$input.name}-change-container"
										     class="form-password-change well hide">
											<div class="form-group">
												<label for="old_passwd"
													 class="control-label col-lg-2 required">
													{l s='當前密碼'}
												</label>
												<div class="col-lg-10">
													<div class="input-group fixed-width-lg">
                                                                                    <span class="input-group-addon">
                                                                                          <i class="glyphicon glyphicon-lock"></i>
                                                                                    </span>
														<input type="password"
															 id="old_passwd"
															 name="old_passwd"
															 class="form-control" required
															 autocomplete="off"
															 placeholder="{l s='現在使用的密碼'}">
													</div>
												</div>
											</div>
											<hr/>
											<div class="form-group">
												<label for="{$input.name}"
													 class="required control-label col-lg-2">
                                                                              <span class="label-tooltip"
														data-toggle="tooltip"
														data-html="true" title=""
														data-original-title="{l s='密碼最少要8個字元'}">
                                                                                    {l s='新密碼'}
                                                                              </span>
												</label>
												<div class="col-lg-6">
													<div class="input-group fixed-width-lg">
                                                                                    <span class="input-group-addon">
                                                                                          <i class="glyphicon glyphicon-lock"></i>
                                                                                    </span>
														<input type="password"
															 id="{$input.name}"
															 name="{$input.name}"
															 class="form-control{if isset($input.class)} {$input.class}{/if}"
															 required autocomplete="off"
															 placeholder="{l s='新密碼'}"/>
													</div>
													<span id="{$input.name}-output"></span>
												</div>
											</div>
											<div class="form-group">
												<label for="{$input.name}2"
													 class="required control-label col-lg-2">
													{l s='確認密碼'}
												</label>
												<div class="col-lg-6">
													<div class="input-group fixed-width-lg">
                                                                                    <span class="input-group-addon">
                                                                                          <i class="glyphicon glyphicon-lock"></i>
                                                                                    </span>
														<input type="password"
															 id="{$input.name}2"
															 name="{$input.name}2"
															 class="form-control{if isset($input.class)} {$input.class}{/if}"
															 value="" autocomplete="off"
															 placeholder="{l s='確認密碼'}"/>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<button type="button"
														  id="{$input.name}-cancel-btn"
														  class="btn btn-default">
														<i class="icon-remove"></i>
														{l s='取消'}
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>{/strip}
								<script type="text/javascript">
									$(function () {
										var $oldPwd = $('#old_passwd');
										var $passwordField = $('#{$input.name}');
										var $output = $('#{$input.name}-output');
										var $generateBtn = $('#{$input.name}-generate-btn');
										var $generateField = $('#{$input.name}-generate-field');
										var $cancelBtn = $('#{$input.name}-cancel-btn');
										var $container = $('#{$input.name}-change-container');
										var $changeBtn = $('#{$input.name}-btn-change');
										var $confirmPwd = $('#{$input.name}2');
										$changeBtn.on('click', function () {
											$container.removeClass('hide');
											$changeBtn.addClass('hide');
										});
										$cancelBtn.on('click', function () {
											$container.find("input").val("");
											$container.addClass('hide');
											$changeBtn.removeClass('hide');
										});
										var email_type = true;
										if ($('input[name="email"]').attr('type') != 'email') email_type = false;
										$('#{if isset($form.id)}{$form.id}{elseif isset($form.name)}{$form.name}{else}{$table}{/if}').validate({
											rules: {
												"email": {
													email: email_type
												},
												"{$input.name}": {
													minlength: 8
												},
												"{$input.name}2": {
													password_same: true
												},
												"old_passwd": {},
											},
											// override jquery validate plugin defaults for bootstrap 3
											highlight: function (element) {
												$(element).closest('.form-group').addClass('has-error');
											},
											unhighlight: function (element) {
												$(element).closest('.form-group').removeClass('has-error');
											},
											errorElement: 'span',
											errorClass: 'help-block',
											errorPlacement: function (error, element) {
												if (element.parent('.input-group').length) {
													error.insertAfter(element.parent());
												} else {
													error.insertAfter(element);
												}
												;
											}
										});
									});
								</script>{strip}
								{elseif $input.type == 'password'}

								<input type="password"
									 id="{$input.name}"
									 name="{$input.name}"
									 class="form-control {if isset($input.class)}{$input.class}{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
									 autocomplete="off"/>

								{elseif $input.type == 'confirm-password'}

								<input type="password"
									 id="{$input.name}"
									 name="{$input.name}"
									 class="form-control {if isset($input.class)}{$input.class}{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
									 autocomplete="off"/>

								{elseif $input.type == 'switch'}
								{if $input.is_prefix || $input.is_suffix}
								<div class="input-group-btn">{/if}
									<div class="btn-group" data-toggle="buttons">
										{foreach $input.values as $value}
											<label class="btn btn-primary{if isset($input.string_format)}
												{foreach $input.string_format as $string_format_v}
													{if $string_format_v == $value.value} active{/if}
												{/foreach}
											{else}
												{foreach $input.val as $value_value_v}
													{if $value_value_v == $value.value} active{/if}
												{/foreach}
											{/if}{if isset($value.label_class)} {$value.label_class}{/if}">
												<input type="radio" autocomplete="off"
													 name="{$input.name}" id="{$value.id}"
													 value="{$value.value}"
													  {if isset($input.string_format)}
														  {foreach $input.string_format as $string_format_v}
															  {if $string_format_v == $value.value} checked="checked"{/if}
														  {/foreach}
													  {else}
														  {foreach $input.val as $value_value_v}
															  {if $value_value_v == $value.value} checked="checked"{/if}
														  {/foreach}
													  {/if}
													  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}/>{$value.label}
											</label>
										{/foreach}
									</div>
									{if $input.is_prefix || $input.is_suffix}</div>{/if}
								{elseif $input.type == 'checkbox'}
								<div class="btn-group" data-toggle="buttons">
									{foreach $input.values as $value}
										<label class="btn btn-primary{if isset($input.string_format)}
                                                            	{foreach $input.string_format as $string_format_v}
                                                            	{if $string_format_v == $value.value} active{/if}
                                                                  {/foreach}
                                                            {else}
											{foreach $input.val as $value_value_v}
                                                            	{if $value_value_v == $value.value} active{/if}
                                                                  {/foreach}
                                                            {/if}">
											<input type="checkbox" autocomplete="off"
												 name="{$input.name}[]" id="{$value.id}"
												 value="{$value.value}"
												  {if isset($input.string_format)}
													  {foreach $input.string_format as $string_format_v}
														  {if $string_format_v == $value.value} checked="checked"{/if}
													  {/foreach}
												  {else}
													  {foreach $input.val as $value_value_v}
														  {if $value_value_v == $value.value} checked="checked"{/if}
													  {/foreach}
												  {/if}
												  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}/>{$value.label}
										</label>
									{/foreach}
								</div>
								{elseif $input.type == 'textarea'}
								<textarea type="text"
									    id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
									    name="{$input.name}{if isset($input.multiple) && $input.multiple}[]{/if}"
									    class="form-control {if isset($input.class)}{$input.class}{/if}{if $input.type == 'tags'} tagify{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.rows)} rows="{$input.rows|intval}"{/if}
									  {if isset($input.cols)} cols="{$input.cols|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
									  {if isset($input.required) && $input.required} required="required" {/if}
									  {if isset($input.autocorrect) && $input.autocorrect} autocorrect="autocorrect" {/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.data)}
									{foreach from=$input.data key=k item=$input_data}
										data-{$k}="{$input_data}"
									{/foreach}
									  {/if}>
									{if isset($input.string_format)}{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}{else}{$input.val|escape:'html':'UTF-8'}{/if}</textarea>

								{elseif $input.type == 'file'}
								<input type="file"
									 id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
									 name="{$input.name|escape:'html':'utf-8'}{if isset($input.multiple) && $input.multiple}[]{/if}"
									 class="form-control {if isset($input.class)}{$input.class}{/if}"
									 value="{if isset($input.string_format)}{$input.val|string_format:$input.string_format|escape:'html':'UTF-8'}{else}{$input.val|escape:'html':'UTF-8'}{/if}"
									  {if isset($input.size)} size="{$input.size}"{/if}
									  {if isset($input.min) && ($input.min || $input.min == 0)} min="{$input.min|intval}"{/if}
									  {if isset($input.max) && $input.max} max="{$input.max|intval}"{/if}
									  {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
									  {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
									  {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
									  {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
									  {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
									  {if isset($input.required) && $input.required} required="required" {/if}
									  {if isset($input.multiple) && $input.multiple} multiple="multiple"{/if}
									  {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder}"{/if}
									  {if isset($input.autocorrect) && $input.autocorrect} autocorrect="{$input.autocorrect}"{/if}
									  {if isset($input.file.type)} data-allowed-file-extensions="{$input.file.type}"{/if}
									  {if isset($input.data)}
										  {foreach from=$input.data key=k item=$input_data}
											  data-{$k}="{$input_data}"
										  {/foreach}
									  {/if}
								/>
								<script type="text/javascript">
									{if isset($input.file.advanced) && $input.file.advanced}

									{else}
									var initialPreview_{$input.name} = [];
									var initialPreviewConfig_{$input.name} = [];
									var UpFileVal_{$input.name} = [];
									{if !empty($initialPreview_{$input.name})}initialPreview_{$input.name} = {$initialPreview_{$input.name}};{/if}
									{if !empty($initialPreviewConfig_{$input.name})}initialPreviewConfig_{$input.name} = {$initialPreviewConfig_{$input.name}};{/if}
									{if !empty($UpFileVal_{$input.name})}UpFileVal_{$input.name} = {$UpFileVal_{$input.name}};{/if}
									$('#{if isset($input.id)}{$input.id}{else}{$input.name}{/if}').fileinput({
										{if !isset($input.file.ajax)}
										uploadUrl: document.location.pathname,
										{else}
										{if $input.file.ajax}uploadUrl: document.location.pathname,{/if}
										{/if}
										uploadAsync: true,
										autoUpload: true,
										{if $input.file.size > 0}maxFileSize:{$input.file.size},{/if}
										{if !empty($input.file.class)}mainClass: '{$input.file.class}',{/if}
										language: '{if isset($input.file.language)}{$input.file.language}{else}zh-TW{/if}',
										minFileCount: {if isset($input.file.min)}{$input.file.min}{else}1{/if},
										maxFileCount: {if isset($input.file.max)}{$input.file.max}{else}'auto'{/if},
										{if !empty($input.file.class)}mainClass: '{$input.file.class}',{/if}
										{if !empty($input.file.min_width)}minImageWidth: '{$input.file.min_width}',{/if}
										{if !empty($input.file.min_hight)}minImageHeight: '{$input.file.min_hight}',{/if}
										{if !empty($input.file.max_width)}maxImageWidth: '{$input.file.max_width}',{/if}
										{if !empty($input.file.max_hight)}maxImageHeight: '{$input.file.max_hight}',{/if}
										initialPreview: initialPreview_{$input.name},
										initialPreviewAsData: true,
										initialPreviewFileType: 'image',
										initialPreviewConfig: initialPreviewConfig_{$input.name},
										{if isset($input.file.allowed)}allowedFileExtensions:{$input.file.allowed|@json_encode},{/if}
										uploadExtraData: {
											ajax: true,
											action: '{if isset($input.file.action)}{$input.file.action}{else}UpFile{/if}',
											UpFileVal: UpFileVal_{$input.name}
										}
									}).on('filesorted', function (event, data, previewId, index) {
										console.log('File sorted params', data);
									}).on('fileuploaded', function (event, data, previewId, index) {
										console.log('File uploaded params', data);
									});
									{/if}
								</script>
								{elseif $input.type == 'view'}
								{if $input.multiple && isset($input.options)}
									<textarea type="text"
										    id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
										    disabled style="width:100%;" rows="8"
										  {if isset($input.data)}
										{foreach from=$input.data key=k item=$input_data}
											data-{$k}="{$input_data}"
										{/foreach}
										  {/if}>{if !empty($input.val)}{$input.val}{/if}</textarea>
								{else}
									<samp class="form-control view {if isset($input.class)}{$input.class}{/if}"{if isset($input.rows) && $input.rows > 0} style="height:{$input.rows * 1.5}em;"{/if}
										id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
										  {if isset($input.data)}
										{foreach from=$input.data key=k item=$input_data}
											data-{$k}="{$input_data}"
										{/foreach}
										  {/if}>{if !empty($input.val)}{$input.val}{/if}</samp>
								{/if}
								{/if}
								{if isset($input.suffix)}<span
									  class="input-group-addon">{$input.suffix}</span>{/if}
								{if ((isset($input.prefix) || isset($input.suffix)) && (!isset($input.is_prefix) && !isset($input.is_suffix))) || $input.is_suffix}
							</div>{/if}
							{if isset($input.p) && $input.p}<p
								class="help-block{if isset($input.p_class) && ($input.p_class != "")} {$input.p_class}{/if}">{$input.p}</p>{/if}
							{if ((isset($input.is_suffix) && $input.is_suffix) || (!isset($input.is_prefix) && !isset($input.is_suffix))) || $input.is_suffix}
						</div>{/if}
						{/block}
						{/if}
						{if (isset($input.is_suffix) && $input.is_suffix) || (!isset($input.is_prefix) && !isset($input.is_suffix))}
					</div>{/if}
					{/block}
					{/foreach}
				</div>
				{/if}
				{/strip}{/foreach}
				{if $body_display}</div>{/if}
			{if $form.footer_display}
				{block name="footer"}{strip}
					<div class="panel-footer">
					{if isset($form.submit) && !empty($form.submit)}
						{foreach from=$form.submit item=sub}
							<button type="submit" value="1"
								  id="{if isset($sub.id)}{$sub.id}{else}{$table}_form_submit_btn{/if}"
								  name="{if isset($sub.name)}{$sub.name}{else}submit{$submit_display}{if !empty($form.table)}{$form.table}{else}{$form.tab}{/if}{/if}{if isset($sub.stay) && $sub.stay}AndStay{/if}"
								  class="{if isset($sub.class)}{$sub.class}{else}btn btn-default pull-right{/if}">
								<i class="{if isset($sub.icon)}{$sub.icon}{else}icon glyphicon glyphicon-floppy-disk{/if}"></i>{$sub.title}
							</button>
						{/foreach}
					{/if}

					{if isset($form.cancel) && $form.cancel}
						<a href="{$back_url|escape:'html':'UTF-8'}" class="btn btn-default"><i
								  class="{if isset($form.submit.icon)}{$form.submit.icon}{else}icon glyphicon glyphicon-remove{/if}"></i> {l s='取消'}
						</a>
					{/if}

					{if isset($form.reset)}
						<button
							  type="reset"
							  id="{if isset($form.reset.id)}{$form.reset.id}{else}{$table}_form_reset_btn{/if}"
							  class="{if isset($form.reset.class)}{$form.reset.class}{else}btn btn-default{/if}"
						>
							<i class="{if isset($form.submit.icon)}{$form.submit.icon}{else}icon glyphicon glyphicon-refresh{/if}"></i> {$form.reset.title}
						</button>
					{/if}

					{if isset($form.buttons)}
						{foreach from=$form.buttons key=k item=btn}
							{if isset($btn.href) && trim($btn.href) != ''}
								<a href="{$btn.href}" {if isset($btn.id)}id="{$btn.id}"{/if}
								   class="btn btn-default{if isset($btn.class)} {$btn.class}{/if}" {if isset($btn.js) && $btn.js} onclick="{$btn.js}"{/if}>{if isset($btn.icon)}
										<i class="{$btn.icon}"></i> {/if}{$btn.title}</a>
							{else}
								<button type="{if isset($btn.type)}{$btn.type}{else}button{/if}"
									  {if isset($btn.id)}id="{$btn.id}"{/if}
									  class="btn btn-default{if isset($btn.class)} {$btn.class}{/if}"
									  name="{if isset($btn.name)}{$btn.name}{else}submitOptions{$table}{/if}"{if isset($btn.js) && $btn.js} onclick="{$btn.js}"{/if}>{if isset($btn.icon)}
										<i class="{$btn.icon}"></i> {/if}{$btn.title}</button>
							{/if}
						{/foreach}{/if}</div>{/strip}{/block}{/if}</div>
	</div>{if !empty($submit_action)}<input type="hidden" name="{$submit_action}" value="1"/>{/if}
	{/foreach}
	{if count($fields.form) > 0}</form>{/if}
{/block}