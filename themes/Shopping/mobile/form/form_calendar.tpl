<div id="loading" class="col-lg-12">loading...</div>
<div id="calendar" class="col-lg-12"></div>
<div id="calendar_list"></div>

<script type="text/javascript">
	function src_auto(){
		return 'auto';
	};
	var is_new = false;
	var fullCalendar_header_right = '{if !empty($fields.calendar.header_right)}{$fields.calendar.header_right}{else}month,agendaWeek,agendaDay,listMonth{/if}';
	{if $smarty.get.is_new}is_new = {$smarty.get.is_new};{/if}
	if(is_new != false){
		fullCalendar_header_right = '';
	};
	$(document).on('click', '.fc-button-group button', function(){
		all_a();
		$('#calendar_list').html('');
		if($('.fc-today', '.fc-month-view').index() > -1){
			var $td = $('.fc-today', '.fc-month-view');
			var _date = $td.data('date');
			$('td.fc-day').removeClass('action');
			$td.addClass('action');
			show_link_today(_date);
		};
	}).on('click', '.fc-listMonth-button', function(){
		var fc_list_show = false;
		$('tr', '.fc-list-table').each(function(){
			if($(this).data('date') >= '{$smarty.now|date_format:"%Y-%m-%d"}') {
				fc_list_show = true;
			};
			if(fc_list_show){
				$(this).show();
			}else{
				$(this).hide();
			};
		});
	}).on('click', '.fc-bg td', function(){
		var $td = $(this);
		var _date = $td.data('date');
		$('td.fc-day').removeClass('action');
		$td.addClass('action');
		show_link_today(_date);
	});
	$('div[data-role="page"]').on('pageshow', function() {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: fullCalendar_header_right
			},
			locale: '{Configuration name="WEB_LANGIAGE"}',
			height: 'auto',
			// contentHeight: src_auto,
			navLinks: {if !is_null($fields.calendar.navLinks)}{$fields.calendar.navLinks}{else}false{/if}, 			//可以單擊日/週名稱來導航視圖
			editable: false,
			timeFormat: 'HH:mm',
			dayNamesShort: {if !empty($fields.calendar.dayNamesShort)}{$fields.calendar.dayNamesShort}{else}['日', '一', '二', '三', '四', '五', '六']{/if},
			businessHours: {		//圖險營業時間
				dow:{if !empty($fields.calendar.business_hours_date)}{$fields.calendar.business_hours_date}{else}[0, 1, 2, 3, 4, 5, 6]{/if},
				start:'{if !empty($fields.calendar.business_hours_start)}{$fields.calendar.business_hours_start}{else}00:00{/if}',
				end:'{if !empty($fields.calendar.business_hours_end)}{$fields.calendar.business_hours_end}{else} 24:00{/if}'
			},
			events: {
				url: document.location.pathname+'?ajax=true&action=calendar&is_new='+is_new,
				error: function() {
					$.growl.error({
						title: '',
						message: '{l s="載入失敗!"}'
					});
				}
			},
			loading: function(bool) {
				$('#loading').toggle(bool);
				all_a();
				$('#calendar_list').html('');
				if($('.fc-today', '.fc-month-view').index() > -1){
					var $td = $('.fc-today', '.fc-month-view');
					var _date = $td.data('date');
					$('td.fc-day').removeClass('action');
					$td.addClass('action');
				};
			}
		});
	});

	function all_a(){
		$('td.fc-day-top').each(function(){
			var _date = $(this).data('date');
			$('a[href$="#'+_date+'"]').eq(0).addClass('show');
		});
	};
	function show_link_today(_date){
		$('#calendar_list').html('');
		if($('.fc-month-view').index() > -1){
			var htm = '';
			$('a[href$="#'+_date+'"]').each(function(){
				var href = $(this).attr('href');
				var color = $(this).css('background-color');
				var h = $('.fc-title', this).html();
				if(h != undefined){
					var si = h.indexOf(' ');
					htm += '<a href="'+href+'"><samp class="time">'+h.substring(0, si+1)+'</samp><samp class="title">'+h.substring(si)+'</samp></a>';
				};
			});
			$('#calendar_list').html(htm);
			if($(window).width() < 991){
				scrollTop($('#calendar_list').offset().top);
			};
		};
	};
</script>