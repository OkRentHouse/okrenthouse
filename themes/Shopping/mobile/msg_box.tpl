{if isset($_msg) && $_msg != '' && $_error == ''}
	<script>
		Swal.fire({
			type: 'success',
			html: '{$_msg}'
		});
	</script>
{/if}
{if isset($_error) && $_error != ''}
	<script>
		Swal.fire({
			type: 'error',
			html: '{$_error}'
		});
	</script>
{/if}