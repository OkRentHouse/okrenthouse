<div id="footer" class="footer transition footer_box-shadow">
	<div>
		<a href="/FlashBarcode" data-transition="none"><img src="{$THEME_URL}/mobile/img/barcode_scanning.svg">{l s="條碼掃描"}</a>
	</div>
	<div>
		<a href="/points" data-transition="none"><img src="{$THEME_URL}/mobile/img/point_query.svg">{l s="點數查詢"}</a>
	</div>
	<div>
		<a href="/information" data-transition="none"><img src="{$THEME_URL}/mobile/img/personal_area.svg">{l s="個人專區"}</a>
	</div>
</div>