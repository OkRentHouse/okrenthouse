<div class="row">
  <div class="cell-1"></div>
  <div class="search_items cell-10">
    <svg class="search_item" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="filter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-filter fa-w-16 fa-3x"><path fill="currentColor" d="M487.976 0H24.028C2.71 0-8.047 25.866 7.058 40.971L192 225.941V432c0 7.831 3.821 15.17 10.237 19.662l80 55.98C298.02 518.69 320 507.493 320 487.98V225.941l184.947-184.97C520.021 25.896 509.338 0 487.976 0z" class=""></path></svg>
    <label class="search_item">條件搜尋</label>
    <select class="search_item"><option>類別</option></select>
    <select class="search_item"><option>最新</option></select>
    <label class="search_item">價格排序</label>
    <input class="search_item">
    <label class="search_item">~</label>
    <input class="search_item">
    <select class="search_item"><option>品牌</option></select>
    <select class="search_item"><option>商品現況</option></select>
    <select class="search_item"><option>評價</option></select>
    <select class="search_item"><option>付款方式</option></select>
    <select class="search_item"><option>出貨地點</option></select>
    <select class="search_item"><option>運送方式</option></select>
          </div>
  <div class="cell-1"></div>
</div>
