<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>{}</title>
	<link href="{$tpl_uri}/css/reset.css" rel="stylesheet" type="text/css">
	<link href="{$tpl_uri}/css/maintenance.css" rel="stylesheet">
	</head>
<body>
<header>
	<div class="nav-bar">
		<nav class="nav_left">
			<img class="logo" src="{$tpl_uri}/images/logo.png" title="" alt="">
		</nav>
	</div>
</header>
<article>
	<img src="{$tpl_uri}/images/maintenance/Planet.png" class="planet">
	<img src="{$tpl_uri}/images/maintenance/UFO.png" class="ufo">
	{if Configuration::get('MAINTENANCE_TEXT') != ''}<div class="text">{Configuration::get('MAINTENANCE_TEXT')}</div>{/if}
	{if Configuration::get('MAINTENANCE_TEXT') != '' && Configuration::get('MAINTENANCE_EMAIL') != ''}
	<div class="link">
		{if Configuration::get('MAINTENANCE_FB') != ''}<a href="{Configuration::get('MAINTENANCE_FB')}" class="fb"><img src="{$tpl_uri}/images/maintenance/UFO_fb.png" class="fb" title="facebook" alt="facebook"></a>{/if}
		{if Configuration::get('MAINTENANCE_EMAIL') != ''}<a href="mailto:{Configuration::get('MAINTENANCE_EMAIL')}" class="email"><img src="{$tpl_uri}/images/maintenance/UFO_email.png" class="email" title="email" alt="email"></a>{/if}
	</div>
	{/if}
</article>
</body>
</html>