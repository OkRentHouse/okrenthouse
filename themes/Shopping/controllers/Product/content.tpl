{$brack_title=array("<label>開賣啦</label>","<label>最精選</label>","<label>好新掀</label>","<label>海外代購</label>","<label>推薦</label>","<label>分享讚</label>")}
{$bt=0}
{$page_tab="<ul><li><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='chevron-left' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-chevron-left fa-w-10 fa-3x'><path fill='currentColor' d='M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z' class=''></path></svg></li><li class='action'>1</li><li>2</li><li>3</li><li><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='chevron-right' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-chevron-right fa-w-10 fa-3x'><path fill='currentColor' d='M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z' class=''></path></svg></li></ul>"}
	<div class="body">
		{include file='../../head_ad_bar.tpl'}

		<div class="row p_top">
			<div class="cell-1"></div>
		  <div class="cell-4 p_l">
				<label class="p_title">清淨錠 每盒10錠</label><br>
				<label class="p_sec_title">防疫第一選擇 消毒 殺菌 除臭</label>
			</div>
		  <div class="cell-6" style="border-bottom: 1px solid #000;">
				<div class="row p_r">

					<div class="cell-4 p_number">
						L1234567
					</div>
				  <div class="cell-8 s_stauts">
						<label>5.0 {for $i=0;$i<5;$i++}
							<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-2x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg>
							{/for}
						</label>
						<label><span class="p_number">熱銷</span>102</label>
						<label><span class="p_number">庫存</span>38</label>
						<label><span class="p_line">LINE</span></label>
					</div>

				</div>
			</div>
			<div class="cell-1"></div>
		</div>
		<div class="row">
		  <div class="cell-1"></div>
			<div class="cell-4">
				<img src="/themes/Shopping/img/p_main.png">

				<div class="commodity_list commodity_data commodity_product" style="display: inline-block;width: 100%;">
                {foreach from=$commodity_product_photo key=i item=v}{include file='themes/Shopping/commodity.tpl'}{foreachelse}{/foreach}
				</div>
				{$js}

			</div>
		  <div class="cell-6">
				{$item_infor=array("關鍵字","促銷","價錢","數量")}
				{$item_infor_h["關鍵字"]=array("<a class='keyword'>#消毒殺菌</a>","<a class='keyword'>#清潔</a>")}
				{$item_infor_h["促銷"]=array("<label class='discount'>滿5送1</label>","<label class='discount'>紅利點數5點</label>")}
				{$item_infor_h["價錢"]=array("<label class='sale'>$1,350</label>","<label class='p_number sale_del'>$2,100</label>")}
				{$item_infor_h["數量"]=array("<input type='number' class='type_number' data-validate='number'><svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='plus' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 384 512' class='svg-inline--fa fa-plus fa-w-12 fa-3x'><path fill='currentColor' d='M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z' class=''></path></svg><svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='minus' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 384 512' class='svg-inline--fa fa-minus fa-w-12 fa-3x'><path fill='currentColor' d='M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z' class=''></path></svg>")}
				{foreach $item_infor as $ii => $v}
					<div class="row">
						<div class="cell-2" style="    align-items: center;    display: flex;">{$v}</div>
						<div class="cell-10">{foreach $item_infor_h[$v] as $ii_ => $v_}{$v_}{/foreach}</div>
					</div>
				{/foreach}
				{$item_infor=array("<a class='shop_car_push'>放入購物車</a>","<a class='shop_car_buy'>直接購買</a>")}

					<div class="row shop_car">

						<div class="col-lg-offset-2">
							{foreach $item_infor as $ii => $v}{$v}{/foreach}
						</div>
					</div>

				<hr>

				{$item_infor=array("所在地區","付款","運費")}
				{$item_infor_h["所在地區"]=array("")}
				{foreach $form_list_county as $f_l_c => $v}
					{if $f_l_c == 0}
						{$item_infor_h["所在地區"][$f_l_c]='<select class="city_sel"><option>'|cat:$v.value|cat:'</option>'}
					{elseif $f_l_c == sizeof($form_list_county)-1}
						{$item_infor_h["所在地區"][$f_l_c]='<option>'|cat:$v.value|cat:'</option></select>'}
					{else}
						{$item_infor_h["所在地區"][$f_l_c]='<option>'|cat:$v.value|cat:'</option>'}
					{/if}
				{/foreach}
				{$item_infor_h["付款"]=array("ATM，","信用卡，","LINE Pay，","Google Pay，","Apple Pay，","貨到付款，","超商")}
				{$item_infor_h["運費"]=array("<img id='_bak' src='/themes/Shopping/img/_bak.png' srcset='/themes/Shopping/img/_bak.png 1x, /themes/Shopping/img/_bak@2x.png 2x'>宅配 單件 60元<p>","<img id='_bai' src='/themes/Shopping/img/_bai.png' srcset='/themes/Shopping/img/_bai.png 1x, /themes/Shopping/img/_bai@2x.png 2x'>郵寄 單件 60元<p>","<img id='_16_v' src='/themes/Shopping/img/_16_v.png' srcset='/themes/Shopping/img/_16_v.png 1x, /themes/Shopping/img/_16_v@2x.png 2x'>超商取貨 7-ELEVEN-$70元 全家-$70元 萊爾富-$65")}
				{foreach $item_infor as $ii => $v}
					<div class="row">
						<div class="cell-2" style="    align-items: center;    display: flex;">{$v}</div>
						<div class="cell-10">{foreach $item_infor_h[$v] as $ii_ => $v_}{$v_}{/foreach}</div>
					</div>
				{/foreach}
			</div>
			<div class="cell-1"></div>
		</div>

		<div class="row">
			<div class="cell-1"></div>
			<div class="cell-5"><div id="_49_">
				<img id="_7__8_w" src="/themes/Shopping/img/_7__8_w.png" srcset="/themes/Shopping/img/_7__8_w.png 1x, /themes/Shopping/img/_7__8_w@2x.png 2x">


				<img id="_7__9_x" src="/themes/Shopping/img/_7__9_x.png" srcset="/themes/Shopping/img/_7__9_x.png 1x, /themes/Shopping/img/_7__9_x@2x.png 2x">


				<svg class="_7__10_y" viewBox="0 0 600 2">
					<path id="_7__10_y" d="M 600 0 L 0 0">
					</path>
				</svg>
				<svg class="_7__13_z" viewBox="0 0 12 2">
					<path id="_7__13_z" d="M 0 0 L 12 0">
					</path>
				</svg>
				<div id="__a">
					<span> 商品資訊</span>
				</div>
				<div id="_bbk">
					<span>留言問答</span>
				</div>
			</div></div>
			<div class="cell-3 offset-2">

				<div class="hot_push_title">
					<label>人氣推薦</label>
				</div>
				{for $i=0;$i<3;$i++}
				<div class="hot_push_{$i}">
					<div class="hot_push_photo"><img src="/themes/Shopping/img/Clip_a.png"></div>
					<div class="hot_push_contect">
						<span>選手牌 蛙鏡及#36蛙鞋</span>
					</div>
				</div>
				{/for}
			</div>
			<div class="cell-1"></div>
		</div>


		<div class="row{if $get_listclass == $bt} active{else} hidden{/if}">
			<div class="cell-1"></div>
			<div class="cell-10 list_6">
				<div class="row">
					<div class="cell-12">{include file='tpl/footer_list.tpl'}</div>
			</div>
			<div class="cell-1"></div>
		</div>

	</div>
	</div>
