{strip}<div id="top_bar">
      {$breadcrumbs_htm}
      {if !isset($title) && isset($page_header_toolbar_title)}
            {assign var=title value=$page_header_toolbar_title}
      {/if}
      <h2 class="page-title">{$page_header_toolbar_title}</h2>
      <div id="top_bar_button">
            {if count($bulk_actions)>0}
            <select name="action" id="action">
            {foreach from=$bulk_actions key=i item=ba}
            <option value="{$i}">{$ba['text']}</option>
            {/foreach}
            </select>
            {/if}
            {if count($toolbar_btn)>0}
		{if isset($toolbar_btn['print'])}<a href="{$toolbar_btn['print']['href']}" class="view btn btn-default pull-right" title="{$toolbar_btn['print']['desc']}"><span class="glyphglyphicon-print"></span>{$toolbar_btn['print']['desc']}</a>{/if}
		{if isset($toolbar_btn['execl'])}<a href="{$toolbar_btn['execl']['href']}" class="view btn btn-default pull-right" title="{$toolbar_btn['execl']['desc']}"><span class="glyphglyphicon-export"></span>{$toolbar_btn['execl']['desc']}</a>{/if}
            {if isset($toolbar_btn['view'])}<a href="{$toolbar_btn['view']['href']}" class="view btn btn-default pull-right" title="{$toolbar_btn['view']['desc']}"><span class="glyphglyphicon-eye-open"></span>{$toolbar_btn['view']['desc']}</a>{/if}
            {if isset($toolbar_btn['new'])}<a href="{$toolbar_btn['new']['href']}" class="new btn btn-default pull-right" title="{$toolbar_btn['new']['desc']}"><span class="glyphglyphicon-plus"></span>{$toolbar_btn['new']['desc']}</a>{/if}
            {if isset($toolbar_btn['edit'])}<a href="{$toolbar_btn['edit']['href']}" class="edit btn btn-default pull-right" title="{$toolbar_btn['edit']['desc']}"><span class="glyphglyphicon-pencil"></span>{$toolbar_btn['edit']['desc']}</a>{/if}
            {if isset($toolbar_btn['del'])}<a href="{$toolbar_btn['del']['href']}" class="del btn btn-default pull-right" title="{$toolbar_btn['del']['desc']}"><span class="glyphglyphicon-trash"></span>{$toolbar_btn['del']['desc']}</a>{/if}
            {if isset($toolbar_btn['cancel'])}<a href="{$toolbar_btn['cancel']['href']}" class="cancel btn btn-default pull-right" title="{$toolbar_btn['cancel']['desc']}"><span class="glyphglyphicon-remove"></span>{$toolbar_btn['cancel']['desc']}</a>{/if}
            {if isset($toolbar_btn['back'])}<a href="{$toolbar_btn['back']['href']}" class="back btn btn-default pull-right" title="{$toolbar_btn['back']['desc']}"><span class="glyph glyphicon-share-alt"></span>{$toolbar_btn['back']['desc']}</a>{/if}
            {if isset($toolbar_btn['save'])}<a href="{$toolbar_btn['save']['href']}" class="href btn btn-default pull-right save" title="{$toolbar_btn['save']['desc']}"><span class="glyphglyphicon-floppy-disk"></span>{$toolbar_btn['save']['desc']}</a>{/if}
            {if isset($toolbar_btn['save_and_stay'])}<a href="{$toolbar_btn['save_and_stay']['href']}" class="save_and_stay btn btn-default pull-right" title="{$toolbar_btn['save_and_stay']['desc']}"><span class="glyphglyphicon-floppy-disk"></span>{$toolbar_btn['save_and_stay']['desc']}</a>{/if}

		{if isset($toolbar_btn['calendar']) || isset($toolbar_btn['list'])}<div class="btn-group btn-group" role="group" aria-label="Large button group">{/if}
		{if isset($toolbar_btn['calendar'])}<a href="{$toolbar_btn['calendar']['href']}" class="view btn btn-default pull-left" title="{$toolbar_btn['calendar']['desc']}"><span class="glyphglyphicon-calendar"></span>{$toolbar_btn['calendar']['desc']}</a>{/if}
		{if isset($toolbar_btn['list'])}<a href="{$toolbar_btn['list']['href']}" class="view btn btn-default pull-left" title="{$toolbar_btn['list']['desc']}"><span class="glyphglyphicon-list-alt"></span>{$toolbar_btn['list']['desc']}</a>{/if}
		{if isset($toolbar_btn['calendar']) || isset($toolbar_btn['list'])}</div>{/if}
            {/if}
      </div>
</div>
{if isset($toolbar_btn['save']) || isset($toolbar_btn['save_and_stay'])}
<script type="text/javascript">
$('.save').click(function(){
	$('#content>.content form').each(function(index, element) {
            $(this).submit();
      });
});
$('.save_and_stay').click(function(){
	$('#content>.content form').each(function(index, element) {
            $(this).submit();
      });
});
</script>
{/if}{/strip}