<!doctype html>
<html>
<head>
<head><link rel="icon" href="{$smarty.const.THEME_URL}/img/favicon.png" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	{if !empty($GOOGLE_SITE_VERIFICATION)}<meta name="google-site-verification" content="{$GOOGLE_SITE_VERIFICATION}" />{/if}
<title>{$meta_title}</title>
{if $meta_description != ''}<meta name="description" content="{$meta_description}">{/if}
{if $meta_keywords != ''}<meta name="keywords" content="{$meta_keywords}">{/if}
<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
<meta property="og:image" content="{$smarty.const.THEME_URL}/img/favicon.png" />
<meta property=og:url content="176.shopping">
{if $meta_description != ''}<meta property=og:title content="{$meta_description}">{/if}
<meta property="og:image:width" content="664" />
<meta property="og:image:height" content="523" />
<meta property="twitter:image" content="{$smarty.const.THEME_URL}/img/favicon.png" />
<meta property="line:image" content="{$smarty.const.THEME_URL}/img/favicon.png" />
{if !empty($t_color)}<meta name="theme-color" content="{$t_color}">{/if}
{if !empty($mn_color)}<meta name="msapplication-navbutton-color" content="{$mn_color}">{/if}
{if !empty($amwasb_style)}<meta name="apple-mobile-web-app-status-bar-style" content="{$amwasb_style}">{/if}
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
{include file="$tpl_dir./table_title.tpl"}
{foreach from=$css_files key=key item=css_uri}
	<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
{/foreach}
{if !isset($display_header_javascript) || $display_header_javascript}
<script type="text/javascript">
var THEME_URL = '{$smarty.const.THEME_URL}';
</script>
{foreach from=$js_files key=key item=js_uri}
	<script type="text/javascript" src="{$js_uri}"></script>
{/foreach}
{if count($_errors_name) > 0}
<script type="text/javascript">
$(document).ready(function(e) {
{foreach from=$_errors_name item=err_name}
	{if count($_errors_name_i.$err_name) > 0}
		{foreach from=$_errors_name_i.$err_name item=ei}
			$('[name="{$err_name}"]').eq({$ei}).parents('.form-group').addClass('has-error');
			$('[name="{$err_name}"]').eq({$ei}).parent('td').addClass('has-error');
			$('[name="{$err_name}"]').focus(function(){
				$('[name="{$err_name}"]').eq({$ei}).parents('.form-group').removeClass('has-error');
				$('[name="{$err_name}"]').eq({$ei}).parent('td').removeClass('has-error');
			});
		{/foreach}
	{else}
		$('[name="{$err_name}"]').parents('.form-group').addClass('has-error');
		$('[name="{$err_name}"]').parent('td').addClass('has-error');
		$('[name="{$err_name}"]').focus(function(){
			$('[name="{$err_name}"]').parents('.form-group').removeClass('has-error');
			$('[name="{$err_name}"]').parent('td').removeClass('has-error');
		});
	{/if}
{/foreach}
});
</script>
{/if}
{/if}
</head>
<body>
<div class="display_content">
	{if $display_header}
    <header class="minbar">
			<div class="top_link_bar">
				<div class="cell-1"></div>
				<div class="links icons cell-5">
					<!-- <img src="/themes/Okmaster/img/login/Line_logo.png">
					<img src="/themes/Okmaster/img/login/Google.png">
					<img src="/themes/Okmaster/img/login/fb.png"> -->
					<span class="right_border">關注我們</span><label class="borderline"></label><span class="right_border">下載</span><label class="borderline"></label><span>客服</span></div>
				<div class="cell-5 links r"><span class="right_border">買家</span><label class="borderline"></label><span class="right_border">賣家</span><label class="borderline"></label><span>註冊</span></div>
				<div class="cell-1"></div>
			</div>
			<div class="top_bar">
				<div class="icons_">
					{for $icons_=1;$icons_<=25;$icons_++}
					{if $icons_ != 5}
					<div><img src="/themes/Shopping/img/icons_{$icons_}.png"></div>
					{/if}
					{/for}
				</div>
				<div class="cell-1"></div>
				<div class="links logo cell-3 l">
					<span class="right_border"><img src="/themes/Shopping/img/menu_03.png" alt="分類" class="classify"></span>


					<span class="right_border logo"><img src="/themes/Shopping/img/menu_07.png" alt="logo" class="classify"></span>
				</div>

				<div class="cell-4 m search_key_w">
					<div class="search_input"><input type="text" placeholder="請輸入關鍵字"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16 fa-2x"><path fill="#fff" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z" class=""></path></svg></div>
					<span>超立淨</span><span class="action">外套</span><span>包包</span><span>長褲</span><span>防疫口罩</span><span>公背包</span><span>行動電源</span></div>
				<div class="links cell-3 r">
					<span class="right_border head_photo"><img src="/themes/Shopping/img/menu_05.png"></span>
					<span class="right_border"><img src="/themes/Shopping/img/menu_11.png" alt="購物中心"> </span>
					<span class="right_border"><img src="/themes/Shopping/img/menu_09.png" alt="互動訊息"></span>
					<span><img src="/themes/Shopping/img/menu_13.png" alt="幫助中心"></span></div>
					<div class="cell-1"></div>
			</div>
    </header>
	{/if}
    <article class="{$mamber_url}">
    {$msg_box}
