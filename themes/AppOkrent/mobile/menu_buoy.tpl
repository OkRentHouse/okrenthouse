<nav class="menu_buoy">
    <div>
        {foreach $menu_buoy as $key => $value}
            {if $key!=0}<b></b>{/if}
            <a href="{$value.url}" {if in_array($buoy_url,$value.active)}class="active"{/if} target="_self" >
                {$value.title}
            </a>
        {/foreach}
        <buoy></buoy>
    </div>
</nav>