<div data-role="header" class="color_page" data-id="index" data-position="fixed">
	{if $role != 'dialog'}
		{if !empty($back_url)}<a href="{$back_url}"{if $back_url == '#'} class="close_btn" data-rel="back"{/if} data-transition="slide" data-direction="reverse" data-role="none"><img class="" src="{$THEME_URL}/img/icon/return.svg"></a>{/if}
{*		{if $page !='index'}<a href="/index" data-transition="slide" data-direction="reverse" data-role="none"><img class="" src="{$THEME_URL}/img/icon/return.svg"></a>{/if}*}
		<div class="open_btn glyphicon glyphicon-menu-hamburger"></div>{/if}
	{if $page_header_toolbar_img != ''}
		<h2 class="ui-title"><a href="/" data-role="none"><img src="{$page_header_toolbar_img}"></a></h2>
	{else}
		<h1 class="ui-title">{$page_header_toolbar_title}</h1>
	{/if}
	{if isset($right_menu) && !empty($right_menu)}<div class="right_manu">{$right_menu}</div>{/if}
</div>