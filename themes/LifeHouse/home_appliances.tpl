{foreach from=$tpl_data key=k item=v}
{*   {$v.label}*}
    {if !empty($v.html)}
    {elseif $v.type=='hidden'}
        <input type="hidden" name="{$v.name}" id="{$v.id}" value="{if isset($row.$k)}{$row.$k} {elseif $main_index==$v.name}{$smarty.get.$main_index}{/if}">
    {elseif $v.type=='text' || $v.type=='number'}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <input class="form-control" type="{$v.type}" name="{$v.name}" id="{$v.id}" value="{if isset($row.$k)}{$row.$k}{/if}">
            </div>
        </div>
    {elseif $v.type=='textarea'}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <textarea type="text" name="{$v.name}" id="{$v.id}" class="form-control"
                    {if isset($v.rows)} rows="{$v.rows|intval}"{/if}
                    {if isset($v.cols)} cols="{$v.cols|intval}"{/if}>{$row.$k}</textarea>
            </div>
        </div>
    {elseif $v.type=='checkbox'}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <div class="btn-group" data-toggle="buttons">
                    {foreach from=$v.values key=ckb_k item=ckb_v}
                        <label class="btn btn-default {if $ckb_v.val==$row.$k}active{/if}" for="{$ckb_v.id}">
                            <input type="radio" autocomplete="off" name="{$v.name}" id="{$ckb_v.id}" value="{$ckb_v.val}" {if $ckb_v.val==$row.$k}checked="checked"{/if}>{$ckb_v.label}
                        </label>
                    {/foreach}
                </div>
            </div>
        </div>
    {elseif $v.tpl==1}
{*        {$v.name}*}
{*        {$v.name|cat:"_num"}*}
{*        {$row[$v.name|cat:"_num"]}*}
{*        {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}*}
{*            {$rv}*}
{*        {/foreach}*}
{*        {$row.lcd_num}*}

    <div class="form-group col-lg-{$v.form_col}">
        <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
        <div class="form-group col-lg-{$v.col}" id="">

        {if isset($row[$v.name|cat:"_num"])}
            {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                {if $rk==0}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                        <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                        <span class="input-group-addon" data-type="1" data-name="{$v.name}" onclick="add_data_h_a(this,1)" >+</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">狀態</span>
                        <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                    </div>
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                        <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                        <span class="input-group-addon" onclick="del_data_h_a(this,1)" >-</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">狀態</span>
                        <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                    </div>
                {/if}
            {/foreach}
        {else}
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="">
                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="">
                <span class="input-group-addon" data-type="1" data-name="{$v.name}" onclick="add_data_h_a(this,1)" >+</span>
            </div>
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">狀態</span>
                <input class="form-control" type="text" name="{$v.name}_status[]" value="">
            </div>
        {/if}
        </div>
    </div>
    {elseif $v.tpl==2}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row[$v.name|cat:"_num"])}
                    {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">椅(數量)</span><input class="form-control" type="text" name="{$v.name}_chair[]"  value="{$row[$v.name|cat:"_chair"][$rk]}">
                                <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                                <span class="input-group-addon" data-type="2" data-name="{$v.name}" onclick="add_data_h_a(this,1)" >+</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">椅(數量)</span><input class="form-control" type="text" name="{$v.name}_chair[]"  value="{$row[$v.name|cat:"_chair"][$rk]}">
                                <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                                <span class="input-group-addon" onclick="del_data_h_a(this,2)" >-</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                        <span class="input-group-addon">椅(數量)</span><input class="form-control" type="text" name="{$v.name}_chair[]"  value="">
                        <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="">
                        <span class="input-group-addon" data-type="2" data-name="{$v.name}" onclick="add_data_h_a(this,2)" >+</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">狀態</span>
                        <input class="form-control" type="text" name="{$v.name}_status[]" value="">
                    </div>
                {/if}
            </div>
        </div>
    {elseif $v.tpl==3}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row[$v.name|cat:"_num"])}
                    {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span><input class="form-control" type="text" name="{$v.name}_type[]"  value="{$row[$v.name|cat:"_type"][$rk]}">
                                <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                                <span class="input-group-addon" data-type="3" data-name="{$v.name}" onclick="add_data_h_a(this,3)" >+</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span><input class="form-control" type="text" name="{$v.name}_type[]"  value="{$row[$v.name|cat:"_type"][$rk]}">
                                <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                                <span class="input-group-addon" onclick="del_data_h_a(this,3)" >-</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                        <span class="input-group-addon">類型</span><input class="form-control" type="text" name="{$v.name}_type[]"  value="">
                        <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="">
                        <span class="input-group-addon" data-type="3" data-name="{$v.name}" onclick="add_data_h_a(this,3)" >+</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">狀態</span>
                        <input class="form-control" type="text" name="{$v.name}_status[]" value="">
                    </div>
                {/if}
            </div>
        </div>
    {elseif $v.tpl==4}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row[$v.name|cat:"_num"])}
                    {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                                <span class="input-group-addon" data-type="4" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}" onclick="add_data_h_a(this,4)" >+</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="{$row[$v.name|cat:"_brand"][$rk]}">
                                <span class="input-group-addon" onclick="del_data_h_a(this,4)" >-</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                        <span class="input-group-addon">類型</span>
                        <select class="form-control" name="{$v.name}_type[]">
                            {foreach from=$v.values  key=se_k item=se_v}
                                <option value="{$se_v.val}" id="{$se_v.val}">{$se_v.text}</option>
                            {/foreach}
                        </select>
                        <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="{$v.name}_brand[]" value="">
                        <span class="input-group-addon" data-type="4" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}" onclick="add_data_h_a(this,4)" >+</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">狀態</span>
                        <input class="form-control" type="text" name="{$v.name}_status[]" value="">
                    </div>
                {/if}
            </div>
        </div>
    {elseif $v.tpl==5}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row[$v.name|cat:"_num"])}
                    {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                                <span class="input-group-addon" data-type="5" data-name="{$v.name}" onclick="add_data_h_a(this,5)" >+</span>
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                                <span class="input-group-addon" onclick="del_data_h_a(this,5)" >-</span>
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                        <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="">
                        <span class="input-group-addon" data-type="5" data-name="{$v.name}" onclick="add_data_h_a(this,5)" >+</span>
                    </div>
                {/if}
            </div>
        </div>
    {elseif $v.tpl==6}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row[$v.name|cat:"_num"])}
                    {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                                <span class="input-group-addon" data-type="6" data-name="{$v.name}" onclick="add_data_h_a(this,6)">+</span>
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                                <span class="input-group-addon" onclick="del_data_h_a(this,6)">-</span>
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                        <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="">
                        <span class="input-group-addon" data-type="6" data-name="{$v.name}" onclick="add_data_h_a(this,6)" >+</span>
                    </div>
                {/if}
            </div>
        </div>
    {elseif $v.tpl==7}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row[$v.name|cat:"_num"])}
                    {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon" data-type="7" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}" onclick="add_data_h_a(this,7)" >+</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="{$row[$v.name|cat:"_position"][$rk]}">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon" onclick="del_data_h_a(this,7)" >-</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">狀態</span>
                                <input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">位置</span><input class="form-control" type="text" name="{$v.name}_position[]" value="">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                        <span class="input-group-addon">類型</span>
                        <select class="form-control" name="{$v.name}_type[]">
                            {foreach from=$v.values  key=se_k item=se_v}
                                <option value="{$se_v.val}" id="{$se_v.val}">{$se_v.text}</option>
                            {/foreach}
                        </select>
                        <span class="input-group-addon" data-type="7" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}" onclick="add_data_h_a(this,7)" >+</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">狀態</span>
                        <input class="form-control" type="text" name="{$v.name}_status[]" value="">
                    </div>
                {/if}
            </div>
        </div>
    {elseif $v.tpl==8}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row[$v.name|cat:"_num"])}
                    {foreach from=$row[$v.name|cat:"_num"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                                <span class="input-group-addon" data-type="8" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}" onclick="add_data_h_a(this,8)" >+</span>
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="{$row[$v.name|cat:"_num"][$rk]}">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="{$row[$v.name|cat:"_status"][$rk]}">
                                <span class="input-group-addon" onclick="del_data_h_a(this,8)" >-</span>
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">數量</span><input class="form-control" type="text" name="{$v.name}_num[]"  value="">
                        <span class="input-group-addon">類型</span>
                        <select class="form-control" name="{$v.name}_type[]">
                            {foreach from=$v.values  key=se_k item=se_v}
                                <option value="{$se_v.val}" id="{$se_v.val}">{$se_v.text}</option>
                            {/foreach}
                        </select>
                        <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="{$v.name}_status[]" value="">
                        <span class="input-group-addon" data-type="8" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}" onclick="add_data_h_a(this,8)" >+</span>
                    </div>
                {/if}
            </div>
        </div>
    {/if}
{/foreach}
{*tpl 1範本*}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">液晶螢幕</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">位置</span><input class="form-control" type="text" name="lcd_position[]" value="">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="lcd_num[]"  value="">*}
{*            <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="lcd_brand[]" value="">*}
{*            <span class="input-group-addon" data-type="1" data-name="lcd" onclick="add_data_h_a(this,1)" >+</span>*}
{*        </div>*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="lcd_status[]" value="">*}
{*        </div>*}
{*    </div>*}
{*</div>*}
{*tpl 2 範本*}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">梳妝台</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">位置</span><input class="form-control" type="text" name="dressing_table_position[]" value="">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="dressing_table_num[]"  value="">*}
{*            <span class="input-group-addon">椅(數量)</span><input class="form-control" type="text" name="dressing_table_chair[]"  value="">*}
{*            <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="dressing_table_brand[]" value="">*}
{*            <span class="input-group-addon" data-type="2" data-name="dressing_table" onclick="add_data_h_a(this,2)" >+</span>*}
{*        </div>*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="dressing_table_status[]" value="">*}
{*        </div>*}
{*    </div>*}
{*</div>*}
{*tpl 3 範本*}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">茶几</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">位置</span><input class="form-control" type="text" name="tea_table_position[]" value="">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="tea_table_num[]"  value="">*}
{*            <span class="input-group-addon">類型</span><input class="form-control" type="text" name="tea_table_type[]"  value="">*}
{*            <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="tea_table_brand[]" value="">*}
{*            <span class="input-group-addon" data-type="3" data-name="tea_table" onclick="add_data_h_a(this,3)" >+</span>*}
{*        </div>*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="tea_table_status[]" value="">*}
{*        </div>*}
{*    </div>*}
{*</div>*}
{*tpl 4 範本 selct_val 跟 select_text 是針對底板使用 *}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">沙發</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">位置</span><input class="form-control" type="text" name="sofa_position[]" value="">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="sofa_num[]"  value="">*}
{*            <span class="input-group-addon">類型</span>*}
{*            <select class="form-control" name="sofa_type[]">*}
{*                <option value="">選擇沙發材質</option>*}
{*                <option value="0">布</option>*}
{*                <option value="1">皮</option>*}
{*                <option value="2">實木</option>*}
{*                <option value="3">藤</option>*}
{*            </select>*}
{*            <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="sofa_brand[]" value="">*}
{*            <span class="input-group-addon" data-type="4" data-name="sofa" data-select_text="選擇沙發材質,布,皮,實木,藤" data-select_val=",0,1,2,3"  onclick="add_data_h_a(this,4)" >+</span>*}
{*        </div>*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="sofa_status[]" value="">*}
{*        </div>*}
{*    </div>*}
{*</div>*}
{*tpl 5 範本 *}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">烘碗機</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">位置</span><input class="form-control" type="text" name="dish_dryer_position[]" value="">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="dish_dryer_num[]"  value="">*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="dish_dryer_status[]" value="">*}
{*            <span class="input-group-addon" data-type="5" data-name="dish_dryer"  onclick="add_data_h_a(this,5)" >+</span>*}
{*        </div>*}
{*    </div>*}
{*</div>*}
{*tpl 6 範本 *}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">流理臺</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="fluid_table_num[]"  value="">*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="fluid_table_status[]" value="">*}
{*            <span class="input-group-addon" data-type="6" data-name="fluid_table"  onclick="add_data_h_a(this,6)" >+</span>*}
{*        </div>*}
{*    </div>*}
{*</div>*}
{*tpl 7 範本 selct_val 跟 select_text 是針對底板使用 *}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">曬衣架</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">位置</span><input class="form-control" type="text" name="clotheshorse_position[]" value="">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="clotheshorse_num[]"  value="">*}
{*            <span class="input-group-addon">類型</span>*}
{*            <select class="form-control" name="clotheshorse_type[]">*}
{*                <option value="">選擇曬衣架運作方式</option>*}
{*                <option value="0">電動</option>*}
{*                <option value="1">手動</option>*}
{*            </select>*}
{*            <span class="input-group-addon" data-type="7" data-name="clotheshorse" data-select_text="選擇曬衣架運作方式,電動,手動" data-select_val=",0,1"  onclick="add_data_h_a(this,7)" >+</span>*}
{*        </div>*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="clotheshorse_status[]" value="">*}
{*        </div>*}
{*    </div>*}
{*</div>*}
{*tpl 8 範本 selct_val 跟 select_text 是針對底板使用 *}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3" for="">窗簾</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">數量</span><input class="form-control" type="text" name="curtain_num[]"  value="">*}
{*            <span class="input-group-addon">類型</span>*}
{*            <select class="form-control" name="curtain_type[]">*}
{*                <option value="">選擇窗簾材質</option>*}
{*                <option value="0">布</option>*}
{*                <option value="1">紗</option>*}
{*            </select>*}
{*            <span class="input-group-addon">狀態</span>*}
{*            <input class="form-control" type="text" name="curtain_status[]" value="">*}
{*            <span class="input-group-addon" data-type="8" data-name="curtain" data-select_text="選擇窗簾材質,布,紗" data-select_val=",0,1"  onclick="add_data_h_a(this,8)" >+</span>*}
{*        </div>*}
{*    </div>*}
{*</div>*}




