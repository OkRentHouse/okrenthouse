<section class="slider_center">
  {include file="../../tpl/group_link.tpl"}
</section>

<script>
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 7,
        slidesToScroll: 7
    });
</script>
