{$role_data = ''}
{if $form.data=='database_data_role_agent'}
    {$role_data = $database_data_role_agent}
{elseif $form.name=='database_data_role_relation'}
    {$role_data = $database_data_role_relation}
{/if}
{if empty($role_data)}
    <div class="panel panel-default">
        <div class="panel-heading" {$form.name} onclick="collapse_data(this)">{$form.title}(點選收合)
        </div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">
                <input type="hidden" name="{$form.name}_id_member_role[]"  class="" value="">
                <input type="hidden" name="{$form.name}_id_member[]"  class="" value="">
{*                <input type="hidden" name="{$form.name}_id_member_landlord[]"  class="" value="">*}
                <input type="hidden" name="{$form.name}_type[]"  class="" value="{$form.type}">
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_name">{$form.title}</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_name[]"  value="" maxlength="20">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_user">手機</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_user[]" onKeyUp="value=value.replace(/[^\d]/g,'')"  value=""  maxlength="20">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_en_name">{$form.title}英文</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_en_name[]"  value="" maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_tel">備用手機</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_tel[]" onKeyUp="value=value.replace(/[^\d]/g,'')" value=""  maxlength="20">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_appellation">稱謂</label>
                    <div class="form-group col-lg-6">
                        <select class="form-control" name="{$form.name}_appellation[]" >
                            <option value="">請選擇稱謂</option>
                            <option value="0">先生</option>
                            <option value="1">小姐</option>
                            <option value="2">太太</option>
                            <option value="3">媽媽</option>
                            <option value="4">伯伯</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_id_source">來源</label>
                    <div class="form-group col-lg-6">
                        <select class="form-control" name="{$form.name}_id_source[]" >
                            <option value="">請選擇來源</option>
                            {foreach $database_source as $d_s_k => $d_s_v}
                                <option value="{$d_s_v.id_source}">{$d_s_v.source}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_birthday">出生年月日</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="date" name="{$form.name}_birthday[]"  value="">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_identity">ID</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_identity[]" value=""  maxlength="16">
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_line_id">Line ID</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_line_id[]" value=""  maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-12"  style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_email">email</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_email[]" value=""  maxlength="100">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_local_tel_1">市話1</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_local_tel_1[]"  onKeyUp="value=value.replace(/[^\d]/g,'')" value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_local_tel_2">市話2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_local_tel_2[]" onKeyUp="value=value.replace(/[^\d]/g,'')" value=""  maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_id_address_domicile">戶籍地址</label>
                    <input type="hidden" name="{$form.name}_id_address_domicile[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_domicile_postal[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_domicile_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                {foreach $database_county as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_domicile_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                {foreach $database_city as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   name="{$form.name}_id_address_domicile_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" name="{$form.name}_id_address_domicile_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   name="{$form.name}_id_address_domicile_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_domicile_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" name="{$form.name}_id_address_domicile_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" name="{$form.name}_id_address_domicile_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" name="{$form.name}_id_address_domicile_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_domicile_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_domicile_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_domicile_floor_her[]"  class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_domicile_address_room[]"  class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" name="{$form.name}_id_address_domicile_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_id_address_contact">連絡地址</label>
                    <input type="hidden" name="{$form.name}_id_address_contact[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_contact_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_contact_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                {foreach $database_county as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_contact_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                {foreach $database_city as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   name="{$form.name}_id_address_contact_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" name="{$form.name}_id_address_contact_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   name="{$form.name}_id_address_contact_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_contact_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" name="{$form.name}_id_address_contact_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" name="{$form.name}_id_address_contact_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" name="{$form.name}_id_address_contact_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_contact_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_contact_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_contact_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_contact_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" name="{$form.name}_id_address_contact_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_id_address_house">住家地址</label>
                    <input type="hidden" name="{$form.name}_id_address_house[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_house_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_house_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                {foreach $database_county as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_house_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                {foreach $database_city as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   name="{$form.name}_id_address_house_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" name="{$form.name}_id_address_house_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   name="{$form.name}_id_address_house_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_house_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" name="{$form.name}_id_address_house_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" name="{$form.name}_id_address_house_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" name="{$form.name}_id_address_house_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_house_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_house_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_house_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_house_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" name="{$form.name}_id_address_house_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_id_address_company">公司地址</label>
                    <input type="hidden" name="{$form.name}_id_address_company[]" class="" value="">
                    <div class="form-group col-lg-9">
                        <div class="col-lg-2 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_company_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
                            </div>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_company_id_county[]" class="form-control county">
                                <option value="">請選擇縣市</option>
                                {foreach $database_county as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding">
                            <select name="{$form.name}_id_address_company_id_city[]" class="form-control city">
                                <option value="">請選擇鄉政區</option>
                                {foreach $database_city as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding">
                            <div class="input-group fixed-width-lg">
                                <input type="text"   name="{$form.name}_id_address_company_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                                <input type="number" name="{$form.name}_id_address_company_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                                <input type="text"   name="{$form.name}_id_address_company_road[]" class="form-control " value="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="input-group fixed-width-lg">
                                <input type="text" name="{$form.name}_id_address_company_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                                <input type="text" name="{$form.name}_id_address_company_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                                <input type="text" name="{$form.name}_id_address_company_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                                <input type="text" name="{$form.name}_id_address_company_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_company_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_company_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                                <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_company_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                                <input type="text" name="{$form.name}_id_address_company_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-9">
                            <input type="text" name="{$form.name}_id_address_company_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_company">公司名/戶名</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_company[]"  value="" maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_job">公司職稱</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_job[]"  value=""  maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-12"  style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_type_role">與房東關係</label>
                    <div class="form-group col-lg-3">
                        <input class="form-control" type="text" name="{$form.name}_type_role[]"  value="" maxlength="64">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_bank_account">銀行帳號</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_bank_account[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_bank_code">銀行代碼</label>
                    <div class="form-group col-lg-3">
                        <input class="form-control" type="text" name="{$form.name}_bank_code[]"  value=""  maxlength="4">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_bank">銀行</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_bank[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_branch">分行</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_branch[]"  value=""  maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_bank_account_2">銀行帳號2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_bank_account_2[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_bank_code_2">銀行代碼2</label>
                    <div class="form-group col-lg-3">
                        <input class="form-control" type="text" name="{$form.name}_bank_code_2[]"  value=""  maxlength="4">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_bank_2">銀行2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_bank_2[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_branch_2">分行2</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="text" name="{$form.name}_branch_2[]"  value=""  maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_id_other_conditions">其他條件(複選)</label>
                    <div class="form-group col-lg-9">
                        <div class="btn-group" data-toggle="buttons">
                            {foreach $database_other_conditions as $d_o_c_k => $d_o_c_v}
                                <label class="btn btn-default checkbox" for="{$form.name}_id_other_conditions_{$d_o_c_v.id_other_conditions}"> <input type="checkbox" autocomplete="off" name="{$form.name}_id_other_conditions_0[]"  value="{$d_o_c_v.id_other_conditions}">{$d_o_c_v.title}</label>
                            {/foreach}
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-6" for="{$form.name}_reserve_price">底價</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="number" name="{$form.name}_reserve_price[]"  value="" maxlength="32">
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3" for="{$form.name}_buy_price">入手價</label>
                    <div class="form-group col-lg-6">
                        <input class="form-control" type="number" name="{$form.name}_buy_price[]"  value=""  maxlength="4">
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_ag">AG</label>
                    <div class="col-lg-3 no_padding">
                        <div class="input-group fixed-width-lg">
                            <input class="form-control" type="text" name="{$form.name}_ag_0[]"  value=""  maxlength="32">
                            <span class="input-group-addon" data-table="{$form.name}" data-name="{$form.name}_ag" data-count="0" onclick="add_field_role(this)">+</span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label class="control-label col-lg-3" for="{$form.name}_ag">下架記錄</label>
                    <div class="col-lg-8 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">下架時間</span>
                            <input class="form-control" type="date" name="{$form.name}_active_off_date_0[]"  value="">
                            <span class="input-group-addon">原因</span>
                            <input class="form-control" type="text" name="{$form.name}_active_off_reason_0[]"  value=""  maxlength="64">
                            <span class="input-group-addon">人員</span>
                            <input class="form-control" type="text" name="{$form.name}_active_off_admin_0[]" value="" maxlength="32">
                            <span class="input-group-addon" data-table="{$form.name}" data-name="active_off" data-count="0" onclick="add_field_role(this)" >+</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{else}
    {foreach $role_data as $k=>$v}
        <div class="panel panel-default {$form.name}">
            <div class="panel-heading">{$form.title}
            </div>
            <div class="panel-body">
                <div class="form_data">
                    <input type="hidden" name="{$form.name}_id_member_role[]"  class="" value="{$v.id_member_role}">
                    <input type="hidden" name="{$form.name}_id_member[]"  class="" value="{$v.id_member}">
                    {*                <input type="hidden" name="{$form.name}_id_member_landlord[]"  class="" value="">*}
                    {*                    上面這個遮掉是因為現在沒有要用多個 有要用多個要給 因為要給定跟者房東走*}
                    <input type="hidden" name="{$form.name}_type[]"  class="" value="{$form.type}">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_name">{$form.title}</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_name[]"  value="{$v.name}" maxlength="20">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_user">手機</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_user[]" onKeyUp="value=value.replace(/[^\d]/g,'')" value="{$v.user}"  maxlength="20">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_en_name">{$form.title}英文</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_en_name[]"  value="{$v.en_name}" maxlength="64">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_tel">備用手機</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_tel[]" onKeyUp="value=value.replace(/[^\d]/g,'')" value="{$v.tel}"  maxlength="20">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_appellation">稱謂</label>
                        <div class="form-group col-lg-6">
                            <select class="form-control" name="{$form.name}_appellation[]" >
                                <option value="">請選擇稱謂</option>
                                <option value="0" {if $v.appellation==0} selected {/if}>先生</option>
                                <option value="1" {if $v.appellation==1} selected {/if}>小姐</option>
                                <option value="2" {if $v.appellation==2} selected {/if}>太太</option>
                                <option value="3" {if $v.appellation==3} selected {/if}>媽媽</option>
                                <option value="4" {if $v.appellation==4} selected {/if}>伯伯</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_id_source">來源</label>
                        <div class="form-group col-lg-6">
                            <select class="form-control" name="{$form.name}_id_source[]" >
                                <option value="">請選擇來源</option>
                                {foreach $database_source as $d_s_k => $d_s_v}
                                    <option value="{$d_s_v.id_source}" {if $v.id_source==$d_s_v.id_source} selected {/if}>{$d_s_v.source}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_birthday">出生年月日</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="date" name="{$form.name}_birthday[]"  value="{$v.birthday}">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_identity">ID</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_identity[]" value="{$v.identity}"  maxlength="16">
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_line_id">Line ID</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_line_id[]" value="{$v.line_id}"  maxlength="64">
                        </div>
                    </div>
                    <div class="form-group col-lg-12"  style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_email">email</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_email[]" value="{$v.email}"  maxlength="100">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_local_tel_1">市話1</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_local_tel_1[]" onKeyUp="value=value.replace(/[^\d]/g,'')" value="{$v.local_tel_1}" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_local_tel_2">市話2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_local_tel_2[]" onKeyUp="value=value.replace(/[^\d]/g,'')" value="{$v.local_tel_2}"  maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_id_address_domicile">戶籍地址</label>
                        <input type="hidden" name="{$form.name}_id_address_domicile[]" class="" value="{$v.id_address_domicile}">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_domicile_postal[]" class="form-control " value="{$v.id_address_domicile_postal}" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_domicile_id_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    {foreach $database_county as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_county}" {if $v.id_address_domicile_id_county==$d_c_v.id_county}selected{/if}>{$d_c_v.county_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_domicile_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    {foreach $database_city as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_city}" {if $v.id_address_domicile_id_city==$d_c_v.id_city}selected{/if}>{$d_c_v.city_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   name="{$form.name}_id_address_domicile_village[]" class="form-control " value="{$v.id_address_domicile_village}" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" name="{$form.name}_id_address_domicile_neighbor[]" class="form-control " value="{$v.id_address_domicile_neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   name="{$form.name}_id_address_domicile_road[]" class="form-control " value="{$v.id_address_domicile_road}" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_domicile_segment[]" class="form-control " value="{$v.id_address_domicile_segment}" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" name="{$form.name}_id_address_domicile_lane[]" class="form-control " value="{$v.id_address_domicile_lane}" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" name="{$form.name}_id_address_domicile_alley[]" class="form-control " value="{$v.id_address_domicile_alley}" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" name="{$form.name}_id_address_domicile_no[]" class="form-control " value="{$v.id_address_domicile_no}" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_domicile_no_her[]" class="form-control " value="{$v.id_address_domicile_no_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_domicile_floor[]" class="form-control " value="{$v.id_address_domicile_floor}" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_domicile_floor_her[]"  class="form-control " value="{$v.id_address_domicile_floor_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_domicile_address_room[]"  class="form-control " value="{$v.id_address_domicile_address_room}" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" name="{$form.name}_id_address_domicile_address[]" class="form-control " value="{$v.id_address_domicile_address}" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_id_address_contact">連絡地址</label>
                        <input type="hidden" name="{$form.name}_id_address_contact[]" class="" value="{$v.id_address_contact}">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_contact_postal[]"  class="form-control " value="{$v.id_address_contact_postal}" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_contact_id_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    {foreach $database_county as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_county}" {if $v.id_address_contact_id_county==$d_c_v.id_county}selected{/if}>{$d_c_v.county_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_contact_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    {foreach $database_city as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_city}" {if $v.id_address_contact_id_city==$d_c_v.id_city}selected{/if}>{$d_c_v.city_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   name="{$form.name}_id_address_contact_village[]" class="form-control " value="{$v.id_address_contact_village}" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" name="{$form.name}_id_address_contact_neighbor[]" class="form-control " value="{$v.id_address_contact_neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   name="{$form.name}_id_address_contact_road[]" class="form-control " value="{$v.id_address_contact_road}" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_contact_segment[]" class="form-control " value="{$v.id_address_contact_segment}" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" name="{$form.name}_id_address_contact_lane[]" class="form-control " value="{$v.id_address_contact_lane}" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" name="{$form.name}_id_address_contact_alley[]" class="form-control " value="{$v.id_address_contact_alley}" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" name="{$form.name}_id_address_contact_no[]" class="form-control " value="{$v.id_address_contact_no}" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_contact_no_her[]" class="form-control " value="{$v.id_address_contact_no_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_contact_floor[]" class="form-control " value="{$v.id_address_contact_floor}" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_contact_floor_her[]" class="form-control " value="{$v.id_address_contact_floor_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_contact_address_room[]" class="form-control " value="{$v.id_address_contact_address_room}" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" name="{$form.name}_id_address_contact_address[]" class="form-control " value="{$v.id_address_contact_address}" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_id_address_house">住家地址</label>
                        <input type="hidden" name="{$form.name}_id_address_house[]" class="" value="{$v.id_address_house}">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_house_postal[]"  class="form-control " value="{$v.id_address_house_postal}" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_house_id_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    {foreach $database_county as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_county}" {if $d_c_v.id_county==$v.id_address_house_id_county}selected{/if}>{$d_c_v.county_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_house_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    {foreach $database_city as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_city}" {if $d_c_v.id_city==$v.id_address_house_id_city}selected{/if}>{$d_c_v.city_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   name="{$form.name}_id_address_house_village[]" class="form-control " value="{$v.id_address_house_village}" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" name="{$form.name}_id_address_house_neighbor[]" class="form-control " value="{$v.id_address_house_neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   name="{$form.name}_id_address_house_road[]" class="form-control " value="{$v.id_address_house_road}" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_house_segment[]" class="form-control " value="{$v.id_address_house_segment}" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" name="{$form.name}_id_address_house_lane[]" class="form-control " value="{$v.id_address_house_lane}" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" name="{$form.name}_id_address_house_alley[]" class="form-control " value="{$v.id_address_house_alley}" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" name="{$form.name}_id_address_house_no[]" class="form-control " value="{$v.id_address_house_no}" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_house_no_her[]" class="form-control " value="{$v.id_address_house_no_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_house_floor[]" class="form-control " value="{$v.id_address_house_floor}" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_house_floor_her[]" class="form-control " value="{$v.id_address_house_floor_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_house_address_room[]" class="form-control " value="{$v.id_address_house_address_room}" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" name="{$form.name}_id_address_house_address[]" class="form-control " value="{$v.id_address_house_address}" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_id_address_company">公司地址</label>
                        <input type="hidden" name="{$form.name}_id_address_company[]" class="" value="{$v.id_address_company}">
                        <div class="form-group col-lg-9">
                            <div class="col-lg-2 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_company_postal[]"  class="form-control " value="{$v.id_address_company_postal}" maxlength="11"><span class="input-group-addon">郵遞號</span>
                                </div>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_company_id_county[]" class="form-control county">
                                    <option value="">請選擇縣市</option>
                                    {foreach $database_county as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_county}"  {if $v.id_address_company_id_county==$d_c_v.id_county}selected {/if}>{$d_c_v.county_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-2 no_padding">
                                <select name="{$form.name}_id_address_company_id_city[]" class="form-control city">
                                    <option value="">請選擇鄉政區</option>
                                    {foreach $database_city as $d_c_k => $d_c_v}
                                        <option value="{$d_c_v.id_city}"  {if $v.id_address_company_id_city==$d_c_v.id_city}selected {/if}>{$d_c_v.city_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="col-lg-5 no_padding">
                                <div class="input-group fixed-width-lg">
                                    <input type="text"   name="{$form.name}_id_address_company_village[]" class="form-control " value="{$v.id_address_company_village}" maxlength="20"><span class="input-group-addon">里</span>
                                    <input type="number" name="{$form.name}_id_address_company_neighbor[]" class="form-control " value="{$v.id_address_company_neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                                    <input type="text"   name="{$form.name}_id_address_company_road[]" class="form-control " value="{$v.id_address_company_road}" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group fixed-width-lg">
                                    <input type="text" name="{$form.name}_id_address_company_segment[]" class="form-control " value="{$v.id_address_company_segment}" maxlength="10"><span class="input-group-addon">段</span>
                                    <input type="text" name="{$form.name}_id_address_company_lane[]" class="form-control " value="{$v.id_address_company_lane}" maxlength="10"><span class="input-group-addon">巷</span>
                                    <input type="text" name="{$form.name}_id_address_company_alley[]" class="form-control " value="{$v.id_address_company_alley}" maxlength="10"><span class="input-group-addon">弄</span>
                                    <input type="text" name="{$form.name}_id_address_company_no[]" class="form-control " value="{$v.id_address_company_no}" maxlength="10"><span class="input-group-addon">號</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_company_no_her[]" class="form-control " value="{$v.id_address_company_no_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_company_floor[]" class="form-control " value="{$v.id_address_company_floor}" maxlength="10"><span class="input-group-addon">樓</span>
                                    <span class="input-group-addon">之</span><input type="text" name="{$form.name}_id_address_company_floor_her[]" class="form-control " value="{$v.id_address_company_floor_her}" maxlength="10"><span class="input-group-addon"></span>
                                    <input type="text" name="{$form.name}_id_address_company_address_room[]" class="form-control " value="{$v.id_address_company_address_room}" maxlength="8"><span class="input-group-addon">室</span>
                                </div>
                            </div>
                            <div class="form-group col-lg-9">
                                <input type="text" name="{$form.name}_id_address_company_address[]" class="form-control " value="{$v.id_address_company_address}" maxlength="255" placeholder="完整地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_company">公司名/戶名</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_company[]"  value="{$v.company}" maxlength="64">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_job">公司職稱</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_job[]"  value="{$v.job}"  maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-12"  style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_type_role">與房東關係</label>
                        <div class="form-group col-lg-3">
                            <input class="form-control" type="text" name="{$form.name}_type_role[]"  value="{$v.type_role}" maxlength="64">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_bank_account">銀行帳號</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_bank_account[]"  value="{$v.bank_account}" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_bank_code">銀行代碼</label>
                        <div class="form-group col-lg-3">
                            <input class="form-control" type="text" name="{$form.name}_bank_code[]"  value="{$v.bank_code}"  maxlength="4">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_bank">銀行</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_bank[]"  value="{$v.bank}" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_branch">分行</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_branch[]"  value="{$v.branch}"  maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_bank_account_2">銀行帳號2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_bank_account_2[]"  value="{$v.bank_account_2}" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_bank_code_2">銀行代碼2</label>
                        <div class="form-group col-lg-3">
                            <input class="form-control" type="text" name="{$form.name}_bank_code_2[]"  value="{$v.landlord_bank_code_2}"  maxlength="4">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_bank_2">銀行2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_bank_2[]"  value="{$v.bank_2}" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_branch_2">分行2</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="text" name="{$form.name}_branch_2[]"  value="{$v.branch_2}"  maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_id_other_conditions">其他條件(複選)</label>
                        <div class="form-group col-lg-9">
                            <div class="btn-group" data-toggle="buttons">
                                {foreach $database_other_conditions as $d_o_c_k => $d_o_c_v}
                                    <label class="btn btn-default checkbox {if in_array($d_o_c_v.id_other_conditions,$v.id_other_conditions)}active{/if}" for="{$form.name}_id_other_conditions_{$d_o_c_v.id_other_conditions}">
                                        <input type="checkbox" autocomplete="off" name="{$form.name}_id_other_conditions_0[]"  value="{$d_o_c_v.id_other_conditions}" {if in_array($d_o_c_v.id_other_conditions,$v.id_other_conditions)}checked="checked"{/if}>{$d_o_c_v.title}</label>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-6" for="{$form.name}_reserve_price">底價</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="number" name="{$form.name}_reserve_price[]"  value="{$v.reserve_price}" maxlength="32">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="{$form.name}_buy_price">入手價</label>
                        <div class="form-group col-lg-6">
                            <input class="form-control" type="number" name="{$form.name}_buy_price[]"  value="{$v.buy_price}"  maxlength="4">
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_ag">AG</label>
                        <div class="col-lg-3 no_padding">
                            {if empty($v.ag)}
                            <div class="input-group fixed-width-lg">
                                <input class="form-control" type="text" name="{$form.name}_ag_0[]"  value=""  maxlength="32">
                                <span class="input-group-addon" data-table="{$form.name}" data-name="{$form.name}_ag" data-count="0" onclick="add_field_role(this)">+</span>
                            </div>
                            {else}
                                {foreach $v.ag as $ag_k => $ag_v}
                                    <div class="input-group fixed-width-lg">
                                        <input class="form-control" type="text" name="{$form.name}_ag_0[]"  value="{$ag_v}"  maxlength="32">
                                        {if $ag_k==0}
                                            <span class="input-group-addon" data-table="{$form.name}" data-name="{$form.name}_ag" data-count="0" onclick="add_field_role(this)">+</span>
                                        {else}
                                            <span class="input-group-addon" data-table="{$form.name}" data-name="{$form.name}_ag" data-count="0" onclick="delete_field_role(this)">-</span>
                                        {/if}
                                    </div>
                                {/foreach}
                            {/if}
                        </div>
                    </div>
                    <div class="form-group col-lg-12" style="margin: auto -23px;">
                        <label class="control-label col-lg-3" for="{$form.name}_ag">下架記錄</label>
                        <div class="col-lg-8 no_padding">
                            {if empty($v.active_off_date)}
                                <div class="input-group fixed-width-lg">
                                    <span class="input-group-addon">下架時間</span>
                                    <input class="form-control" type="date" name="{$form.name}_active_off_date_0[]"  value="">
                                    <span class="input-group-addon">原因</span>
                                    <input class="form-control" type="text" name="{$form.name}_active_off_reason_0[]"  value=""  maxlength="64">
                                    <span class="input-group-addon">人員</span>
                                    <input class="form-control" type="text" name="{$form.name}_active_off_admin_0[]" value="" maxlength="32">
                                    <span class="input-group-addon" data-table="{$form.name}" data-name="active_off" data-count="0" onclick="add_field_role(this)" >+</span>
                                </div>
                            {else}
                                {foreach $v.active_off_date as $active_off_date_k => $active_off_date_v}
                                    <div class="input-group fixed-width-lg">
                                        <span class="input-group-addon">下架時間</span>
                                        <input class="form-control" type="date" name="{$form.name}_active_off_date_0[]"  value="{$active_off_date_v}">
                                        <span class="input-group-addon">原因</span>
                                        <input class="form-control" type="text" name="{$form.name}_active_off_reason_0[]"  value="{$v.active_off_reason.$active_off_date_k}"  maxlength="64">
                                        <span class="input-group-addon">人員</span>
                                        <input class="form-control" type="text" name="{$form.name}_active_off_admin_0[]" value="{$v.active_off_admin.$active_off_date_k}" maxlength="32">
                                        {if $active_off_date_k==0}
                                            <span class="input-group-addon" data-table="{$form.name}" data-name="active_off" data-count="{$active_off_date_k}" onclick="add_field_role(this)" >+</span>
                                        {else}
                                            <span class="input-group-addon" data-table="{$form.name}" data-name="active_off" data-count="{$active_off_date_k}" onclick="delete_field_role(this)" >-</span>
                                        {/if}
                                    </div>
                                {/foreach}
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/foreach}
{/if}
