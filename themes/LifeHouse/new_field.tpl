<div class="panel panel-default">
        <div class="panel-heading" {$form.name} onclick="collapse_data(this)">{$form.title}(點選收合)
        </div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">

<script>
function clean(){
  document.getElementById("userName").value = '';
  document.getElementById("black_reason").value = '';
  document.getElementById("black_add").checked = false;
  document.getElementById("leasetitle").value = '';
  document.getElementById("birthday").value = '';
  document.getElementById("userID").value = '';
  document.getElementById("mobile1").value = '';
  document.getElementById("mobile2").value = '';
  document.getElementById("lineID").value = '';
  document.getElementById("tel_h").value = '';
  document.getElementById("tel_o").value = '';
  document.getElementById("residenceAddress").value = '';
  document.getElementById("contractAddress").value = '';
  document.getElementById("homeAddress").value = '';
  document.getElementById("businessAddress").value = '';
  document.getElementById("email").value = '';
  document.getElementById("bussinessNumber").value = '';
  document.getElementById("worktitle").value = '';
  document.getElementById("leaseCondition").value = '';
  document.getElementById("bankAccount1").value = '';
  document.getElementById("bankAccount2").value = '';
  document.getElementById("relation").value = '';
  document.getElementById("payRecord").value = '';
  document.getElementById("leaseRangeStart").value = '';
  document.getElementById("leaseRangeEnd").value = '';
  document.getElementById("rentMoney").value = '';
  document.getElementById("rent_other").value = '';
  document.getElementById("rent_other1").value = '';
  document.getElementById("rentPayDate").value = '';
  document.getElementById("lookOther").value = '';
  document.getElementById("bonds").value = '';
  document.getElementById("bondsRecord").value = '';
  document.getElementById("bondsPer").value = '';
  document.getElementById("leasefull").value = '';
  document.getElementById("ag123").value = '';
  document.getElementById("agencyPlan").value = '';
  document.getElementById("managePlan").value = '';
  document.getElementById("rentAndSale").value = '';
  document.getElementById("checkin").value = '';
  document.getElementById("checkinPeople").value = '';
  document.getElementById("breaklease").value = '';
  document.getElementById("rentAssoc").value = '';
  document.getElementById("addRent").value = '';
  document.getElementById("addCredit").value = '';
  document.getElementById("addCare").value = '';
  document.getElementById("buyPrice").value = '';
  document.getElementById("taoAssoc").value = '';
  document.getElementById("twAssoc").value = '';
  document.getElementById("lease_number").value = '';
  document.getElementById("userName_relation").value = '';
  document.getElementById("leasetitle_relation").value = '';
  document.getElementById("relation_relation").value = '';
  document.getElementById("birthday_relation").value = '';
  document.getElementById("userID_relation").value = '';
  document.getElementById("mobile1_relation").value = '';
  document.getElementById("mobile2_relation").value = '';
  document.getElementById("lineID_relation").value = '';
  document.getElementById("tel_h_relation").value = '';
  document.getElementById("tel_o_relation").value = '';
  document.getElementById("email_relation").value = '';
  
  
}

function clean2(){
  document.getElementById("lease_number").value = '';
  document.getElementById("userName_relation").value = '';
  document.getElementById("leasetitle_relation").value = '';
  document.getElementById("relation_relation").value = '';
  document.getElementById("birthday_relation").value = '';
  document.getElementById("userID_relation").value = '';
  document.getElementById("mobile1_relation").value = '';
  document.getElementById("mobile2_relation").value = '';
  document.getElementById("lineID_relation").value = '';
  document.getElementById("tel_h_relation").value = '';
  document.getElementById("tel_o_relation").value = '';
  document.getElementById("email_relation").value = '';
  document.getElementById("residenceAddress_relation").value = '';
  document.getElementById("homeAddress_relation").value = '';
  document.getElementById("businessAddress_relation").value = '';
  document.getElementById("bussinessNumber_relation").value = '';
  document.getElementById("worktitle_relation").value = '';
}
</script>
<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">房東</td>
    <td colspan="7">
    <input type="text" id="read_landlord_name" name="read_landlord_name" value="{$read_landlord_name}" size="10" readonly>
    <input type="hidden" id="read_landlord_id_member" name="read_landlord_id_member" value="{$read_landlord_id_member}" size="10" readonly>
    </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租約:</td>
    <td><input type="text" id="lease" name="lease" value="{$lease_house_code}" size="10" readonly></td>
    <td class="main_t_2 input-group-addon" style="color:red">順序:<br/>目前合約編號:{$leaseSequence}<input type="hidden" id="update_lease_id" name="update_lease_id" value="{$leaseSequence}"></td>
    <td><input type="text" id="leaseSequence" name="leaseSequence" value="" size="10"></td>
    <td class="main_t_2 input-group-addon">來源:</td>
    <td><select class="form-control" name="source" >
                            <option value="">請選擇來源</option>
                            {foreach $database_source as $d_s_k => $d_s_v}
                                <option value="{$d_s_v.source}">{$d_s_v.source}</option>
                            {/foreach}
                        </select></td>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text" id="userName" name="userName" value="{$userName}" size="10"></td>
  </tr>
  <tr>
  <td class="main_t_2 input-group-addon" style="color:red">是否黑名單:</td>
  <td><input type="checkbox" id="black_add" name="black_add" value="1" {if $black_ck =="1"} checked {/if} style="margin:5px;"></td>
  <td class="main_t_2 input-group-addon">原因:</td>
  <td><input type="text" id="black_reason" name="black_reason" value="{$black_reason}" style="width:100%;"></td>
  <td class="main_t_2 input-group-addon">紀錄時間:</td>
  <td><input type="date" id="recodeTime" name="recodeTime" value="{$black_recodeTime}"></td>
  <td class="main_t_2 input-group-addon">紀錄人:</td>
  <td><input type="text" id="black_cp" name="black_cp" value="{$smarty.session.name}" style="width:100%;" readonly></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text" id="leasetitle" name="leasetitle" value="{$leasetitle}" size="10"></td>
    <td class="main_t_2 input-group-addon">生日:</td>
    <td><input type="date" id="birthday" name="birthday" value="{$birthday}" size="10"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text" id="userID" name="userID" value="{$userID}" size="10"></td>
    <td class="main_t_2 input-group-addon">手機1:</td>
    <td><input type="text" id="mobile1" name="mobile1" value="{$mobile1}" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text" id="mobile2" name="mobile2" value="{$mobile2}" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text" id="lineID" name="lineID" value="{$lineID}" size="10"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" id="tel_h" name="tel_h" value="{$tel_h}" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text" id="tel_o" name="tel_o" value="{$tel_o}" onKeyUp="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text" id="residenceAddress" name="residenceAddress" value="{$residenceAddress}" size="50"></td>
    <td class="main_t_2 input-group-addon">聯絡地址:</td>
    <td colspan="3"><input type="text" id="contractAddress" name="contractAddress" value="{$contractAddress}" size="50"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text" id="homeAddress" name="homeAddress" value="{$homeAddress}" size="50"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" id="businessAddress" name="businessAddress" value="{$businessAddress}" size="50"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" id="email" name="email" value="{$email}" size="10"></td>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text" id="bussinessNumber" name="bussinessNumber" value="{$bussinessNumber}" size="10"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text" id="worktitle" name="worktitle" value="{$worktitle}" size="10"></td>
    <td class="main_t_2 input-group-addon">租約條件:</td>
    <td><input type="text" id="leaseCondition" name="leaseCondition" value="{$leaseCondition}" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入帳帳戶1:</td>
    <td><input type="text" id="bankAccount1" name="bankAccount1" value="{$bankAccount1}" size="10"></td>
    <td class="main_t_2 input-group-addon">入帳帳戶2:</td>
    <td><input type="text" id="bankAccount2" name="bankAccount2" value="{$bankAccount2}" size="10"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text" id="relation" name="relation" value="{$relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">繳租記錄:</td>
    <td><input type="text" id="payRecord" name="payRecord" value="{$payRecord}" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租期:</td>
    <td>起<input type="date" id="leaseRangeStart" name="leaseRangeStart" value="{$leaseRangeStart}" size="10"><br/>結<input type="date" id="leaseRangeEnd" name="leaseRangeEnd" value="{$leaseRangeEnd}" size="10"></td>
    <td class="main_t_2 input-group-addon">租金:</td>
    <td><input type="text" id="rentMoney" name="rentMoney" value="{$rentMoney}" onKeyUp="value=value.replace(/[^\d]/g,'')" size="10"></td>
    <td class="main_t_2 input-group-addon">其他：</td>
    <td><input type="text" id="rent_other" name="rent_other" value="{$rent_other}" size="10"><br/>
    {if $rent_other1 == "0"}
      <input type="checkbox" id="rent_other1" name="rent_other1" value="0" checked/>
      車位
      {else}
  <input type="radio" id="rent_other1" name="rent_other1" value="0" />
      車位
      {/if}
      {if $rent_other1 == "1"}
      <input type="radio" id="rent_other1" name="rent_other1" value="1" checked/>
      管理費
      {else}
  <input type="radio" id="rent_other1" name="rent_other1" value="1" />
      管理費
      {/if}
      {if $rent_other1 == "2"}
      <input type="radio" id="rent_other1" name="rent_other1" value="2" checked/>
      兩者
      {else}
  <input type="radio" id="rent_other1" name="rent_other1" value="2" />
      兩者
      {/if}
      {if $rent_other1 == "3"}
      <input type="radio" id="rent_other1" name="rent_other1" value="3" checked/>
      取消
      {else}
  <input type="radio" id="rent_other1" name="rent_other1" value="3" />
      取消
      {/if}
    </td>
    <td class="main_t_2 input-group-addon">繳租日:</td>
    <td><input type="text" id="rentPayDate" name="rentPayDate" value="{$rentPayDate}" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">帶看配合:</td>
    <td><input type="text" id="lookOther" name="lookOther" value="{$lookOther}" size="10"></td>
    <td class="main_t_2 input-group-addon">押金金額:</td>
    <td><input type="text" id="bonds" name="bonds" value="{$bonds}" onKeyUp="value=value.replace(/[^\d]/g,'')" size="10"></td>
    <td class="main_t_2 input-group-addon">押金繳交記錄:</td>
    <td><input type="text" id="bondsRecord" name="bondsRecord" value="{$bondsRecord}" size="10"></td>
    <td class="main_t_2 input-group-addon">押金擔保:</td>
    <td><input type="text" id="bondsPer" name="bondsPer" value="{$bondsPer}" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">期滿退租:</td>
    <td><input type="text" id="leasefull" name="leasefull" value="{$leasefull}" size="10"></td>
    <td class="main_t_2 input-group-addon">AG:</td>
    <td><input type="text" id="ag123" name="ag123" value="{$ag}" size="10"></td>
    <td class="main_t_2 input-group-addon">仲介方案:</td>
    <td> {*
      <select id="agencyPlan" name="YourLocation">
        
    　
        <option value="仲介方案A">仲介方案A</option>
        
    　
        <option value="仲介方案B">仲介方案B</option>
      </select>
      *}
      {if $agencyPlan == "A"}
      <input type="radio" id="agencyPlan" name="agencyPlan" value="甲" checked/>
      甲
      {else}
  <input type="radio" id="agencyPlan" name="agencyPlan" value="甲" />
      甲
      {/if}
      {if $agencyPlan == "B"}
      <input type="radio" id="agencyPlan" name="agencyPlan" value="乙" checked/>
      乙
      {else}
  <input type="radio" id="agencyPlan" name="agencyPlan" value="乙" />
      乙
      {/if} </td>
    <td class="main_t_2 input-group-addon">代管方案:</td>
    <td> {*
      <select id="managePlan" name="YourLocation">
        
    　

        <option value="代管方案A">代管方案A</option>
        
    　
        <option value="代管方案B">代管方案B</option>
        
    　
        <option value="代管方案C">代管方案C</option>
      </select>
      *}
      {if $managePlan == "A"}
      <input type="radio" id="managePlan" name="managePlan" value="A" checked/>
      A
      {else}
  <input type="radio" id="managePlan" name="managePlan" value="A" />
      A
      {/if}
      {if $managePlan == "B"}
      <input type="radio" id="managePlan" name="managePlan" value="B" checked/>
      B
      {else}
  <input type="radio" id="managePlan" name="managePlan" value="B" />
      B
      {/if}
      {if $managePlan == "C"}
      <input type="radio" id="managePlan" name="managePlan" value="C" checked/>
      C
      {else}
  <input type="radio" id="managePlan" name="managePlan" value="C" />
      C
      {/if} </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租售同步:</td>
    <td><input type="text" id="rentAndSale" name="rentAndSale" value="{$rentAndSale}" size="10"></td>
    <td class="main_t_2 input-group-addon">點 in 日期:</td>
    <td><input type="date" id="checkin" name="checkin" value="{$checkin}" size="10"></td>
    <td class="main_t_2 input-group-addon">點交人員:</td>
    <td><input type="text" id="checkinPeople" name="checkinPeople" value="{$checkinPeople}" size="10"></td>
    <td class="main_t_2 input-group-addon">中途解約:</td>
    <td><input type="text" id="breaklease" name="breaklease" value="{$breaklease}" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租管工會:</td>
    <td><input type="text" id="rentAssoc" name="rentAssoc" value="{$rentAssoc}" size="10"></td>
    <td class="main_t_2 input-group-addon">加值 - 租霸補助:</td>
    <td><input type="text" id="addRent" name="addRent" value="{$addRent}" size="10"></td>
    <td class="main_t_2 input-group-addon">加值 - 信用認證:</td>
    <td><input type="text" id="addCredit" name="addCredit" value="{$addCredit}" size="10"></td>
    <td class="main_t_2 input-group-addon">加值 - 安心履約:</td>
    <td><input type="text" id="addCare" name="addCare" value="{$addCare}" size="10"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入手價:</td>
    <td><input type="text" id="buyPrice3" name="buyPrice2" value="{$buyPrice}" onKeyUp="value=value.replace(/[^\d]/g,'')" size="10"></td>
    <td class="main_t_2 input-group-addon">桃協會:</td>
    <td><input type="text" id="taoAssoc3" name="taoAssoc2" value="{$taoAssoc}" size="10"></td>
    <td class="main_t_2 input-group-addon">台協會:</td>
    <td><input type="text" id="twAssoc3" name="twAssoc2" value="{$twAssoc}" size="10"></td>
    <td class="main_t_2 input-group-addon" style="color:red">契約編號</td>
    <td><input style="color:red" type="text" id="lease_number" name="lease_number" value="{$lease_number}" size="10"/></td>
  </tr>
  <tr>
    <td colspan="6">

      <div class="form-group col-lg-12" style="margin: auto -23px;">
                    <label for="member_landlord_id_other_conditions">其他條件(複選)</label>
                    <div>
                        <div class="btn-group" data-toggle="buttons">
                        {* {foreach $database_data as $k=>$v} *}
                            {foreach $database_other_conditions as $d_o_c_k => $d_o_c_v }
                                <label class="btn btn-default checkbox" for="member_landlord_id_other_conditions_{$d_o_c_v.id_other_conditions}"> <input type="checkbox" autocomplete="off" name="member_landlord_id_other_conditions[]"  value="{$d_o_c_v.title}">{$d_o_c_v.title}</label>
                            {/foreach}
                        {* {/foreach} *}
                        {* {foreach $database_data as $k=>$v}
                            {foreach $database_other_conditions as $d_o_c_k => $d_o_c_v}
                                    <label class="btn btn-default checkbox {if in_array($d_o_c_v.id_other_conditions,$v.id_other_conditions)}active{/if}" for="member_landlord_id_other_conditions_{$d_o_c_v.id_other_conditions}">
                                        <input type="checkbox" autocomplete="off" name="member_landlord_id_other_conditions[]"  value="{$d_o_c_v.id_other_conditions}" {if in_array($d_o_c_v.id_other_conditions,$v.id_other_conditions)}checked="checked"{/if}>{$d_o_c_v.title}</label>

                                {/foreach}
                        {/foreach} *}
                        </div>
                    </div>
                </div>
    </td>
    <td class="main_t_2 input-group-addon" style="color:red">更新人員</td>
    <td><input style="color:red" type="text" id="update_people2" name="update_people" value="{$smarty.session.name}" size="10" disabled="disabled"/></td>
  </tr>

</table>

<br/>
<br/>
<br/>

<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">新擔保人合約</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text" id="userName_relation" name="userName_relation" value="{$userName_relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text" id="leasetitle_relation" name="leasetitle_relation" value="{$leasetitle_relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text" id="relation_relation" name="relation_relation" value="{$relation_relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">生日:</td>
    <td><input type="date" id="birthday_relation" name="birthday_relation" value="{$birthday_relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text" id="userID_relation" name="userID_relation" value="{$userID_relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">手機1:</td>
    <td><input type="text" id="mobile1_relation" name="mobile1_relation" value="{$mobile1_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text" id="mobile2_relation" name="mobile2_relation" value="{$mobile2_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text" id="lineID_relation" name="lineID_relation" value="{$lineID_relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" id="tel_h_relation" name="tel_h_relation" value="{$tel_h_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text" id="tel_o_relation" name="tel_o_relation" value="{$tel_o_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10"></td>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" id="email_relation" name="email_relation" value="{$email_relation}" size="10"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text" id="residenceAddress_relation" name="residenceAddress_relation" value="{$residenceAddress_relation}" size="50"></td>
    <td ></td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text" id="homeAddress_relation" name="homeAddress_relation" value="{$homeAddress_relation}" size="50"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" id="businessAddress_relation" name="businessAddress_relation" value="{$businessAddress_relation}" size="50"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text" id="bussinessNumber_relation" name="bussinessNumber_relation" value="{$bussinessNumber_relation}" size="10"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text" id="worktitle_relation" name="worktitle_relation" value="{$worktitle_relation}" size="10"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td>&nbsp;</td>
    <td style="color:red"><input type="button" onclick="clean();" value="清空租客所有欄位"></td>
    <td style="color:red"><input type="button" onclick="clean2();" value="清空履保人所有欄位"></td>
    <td class="main_t_2 input-group-addon" style="color:red">是否更新：</td>
    <td><input type="radio" id="updd" name="updd" value="0" checked />
      更新
      <input type="radio" id="updd" name="updd" value="1" />
      新增 </td>
  </tr>
</table>
</div>
</div>
</div>
