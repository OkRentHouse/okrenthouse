{foreach from=$member_additional_row key=key item=v}

  {$new_row=json_decode($v.id_member_additional)}
  {if sizeof($new_row)>1}
    {$new_row[0] = null}
    {$new_member_additional_row[$new_row[1]]=$v.additional_COUNT}
    {$count_new_member_additional_row=$count_new_member_additional_row+$v.additional_COUNT}
    {else}
    {$new_member_additional_row[$new_row[0]]=$v.additional_COUNT}
    {$count_new_member_additional_row=$count_new_member_additional_row+$v.additional_COUNT}
  {/if}
{/foreach}
{$new_member_additional_row[1]=$count_new_member_additional_row}
<style>
  .id_member_additional_table td {
      border: 1px solid #CCC;
      text-align: center;
  }
  .id_member_additional_table th {
    position: relative;
    white-space: nowrap;
    background-color: #2e6ea5;
    color: #fff;
  }
</style>
<div class="panel-default">
  <div class="panel-heading">
        會員統計
      </div>
		</div>
<table style="width: 100%;" class="id_member_additional_table">

    <tr>
      {foreach from=$fields.list.id_member_additional.values key=key item=v}
        <th class="text-center">{$v.title}<div class="th_title" style="top: 0px;">{$v.title}</div></th>
      {/foreach}
    </tr>

  <tr>
    {foreach from=$fields.list.id_member_additional.values key=key item=v}
      <td class="">{$new_member_additional_row[$v.value]}</td>
    {/foreach}
  </tr>
</table>
