<style>
.customer tr td{ padding:10px 15px; border:#ccc 1px solid;}
.commodity {
    color: #333;
}
tbody>tr:hover, tbody>tr.active, .table>tbody>tr.active>td {
    background-color:transparent;
}
</style>
<table width="100%" border="1" class="customer">
  <tr>
    <td width="2%">戶外媒體</td>
    <td width="98%">
    {assign var="p1" value={$new_ad_OutdoorType}}  
    {assign var="p2" value=", "|explode:$p1}
    {$ppp1=array("布條")}
    {foreach $ppp1 as $n => $x}
    <input type="checkbox" id="AD_OutdoorType[]" name="AD_OutdoorType[]" value="布條" style="margin:5px;" {if $x|in_array:$p2}checked="checked"{/if}>布條&nbsp;&nbsp;&nbsp;
    {/foreach}
    位置:<input type="text" id="AD_Location" name="AD_Location" value="{$new_ad_location}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    刊登期間:<input type="date" id="AD_Date1" name="AD_Date1" value="{$new_ad_date1}" style="margin:5px;">&nbsp;&nbsp;&nbsp;
    費用:<input type="text" id="AD_Fee" name="AD_Fee" value="{$new_ad_fee}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    負擔:<input type="text" id="AD_Load" name="AD_Load" value="{$new_ad_load}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    廠商:<input type="text" id="AD_Business" name="AD_Business" value="{$new_ad_business}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    撤回日期:<input type="date" id="AD_Date2" name="AD_Date2" value="{$new_ad_date2}"><br/>
    {assign var="p1" value={$new_ad_OutdoorType}}  
    {assign var="p2" value=", "|explode:$p1}
    {$ppp2=array("帆布","字牌")}
    {foreach $ppp2 as $n => $x}
    <input type="checkbox" id="AD_OutdoorType[]" name="AD_OutdoorType[]" value="{$x}" style="margin:5px;" {if $x|in_array:$p2}checked="checked"{/if}>{$x}<br/>
    {/foreach}
    {assign var="p1" value={$new_ad_OutdoorType}}  
    {assign var="p2" value=", "|explode:$p1}
    {$ppp2=array("公佈欄")}
    {foreach $ppp2 as $n => $x}
    <input type="checkbox" id="AD_OutdoorType[]" name="AD_OutdoorType[]" value="公佈欄" style="margin:5px;" {if $x|in_array:$p2}checked="checked"{/if}>公佈欄&nbsp;&nbsp;&nbsp;
    {/foreach}
    期間:<input type="date" id="AD_BBS_Date" name="AD_BBS_Date" value="{$new_ad_bbs_date}" style="margin:5px;">&nbsp;&nbsp;&nbsp;
    位置:<input type="text" id="AD_BBS_Location" name="AD_BBS_Location" value="{$new_ad_bbs_location}" style="width:150px;margin:5px;">
    </td>
  </tr>
  <tr>
    <td>網路</td>
    <td>
    {assign var="p3" value={$new_ad_NetworkType}}  
    {assign var="p4" value=", "|explode:$p3}
    {$ppp1=array("319")}
    {foreach $ppp1 as $n => $x}
    <input type="checkbox" id="AD_NetworkType[]" name="AD_NetworkType[]" value="319" style="margin:5px;"  {if $x|in_array:$p4}checked="checked"{/if}>319&nbsp;&nbsp;&nbsp;
    {/foreach}
    位置:<input type="text" id="AD_N_Location" name="AD_N_Location" value="{$new_ad_N_Location}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    刊登期間:<input type="date" id="AD_N_Date1" name="AD_N_Date1" value="{$new_ad_N_Date1}">&nbsp;~&nbsp;<input type="date" id="AD_N_Date2" name="AD_N_Date2" value="{$new_ad_N_Date2}">
    費用:<input type="text" id="AD_N_Fee" name="AD_N_Fee" value="{$new_ad_N_Fee}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    負擔:<input type="text" id="AD_N_Load" name="AD_N_Load" value="{$new_ad_N_Load}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    帳號:<input type="text" id="AD_N_Account" name="AD_N_Account" value="{$new_ad_N_Account}" style="width:80px;margin:5px;">&nbsp;&nbsp;&nbsp;
    承辦:
    <select id="AD_N_AG" name="AD_N_AG" style="margin:5px;">
      <<option value="請選擇業務">請選擇業務</option>
      {foreach $newag as $n => $x}
        <option value="{$x.en_name}"
        {if $new_ad_N_AG == $x.en_name}
        selected
        {/if}>{$x.en_name}</option>
      {/foreach}
    </select>
    &nbsp;&nbsp;&nbsp;
    {if $new_ad_N_CP ==""}
    紀錄:<input type="text" id="AD_N_CP" name="AD_N_CP" value="{$smarty.session.name}" style="width:80px;margin:5px;" readonly><br/>
    {else}
    紀錄:<input type="text" id="AD_N_CP" name="AD_N_CP" value="{$new_ad_N_CP}" style="width:80px;margin:5px;" readonly><br/>
    {/if}
    {assign var="p3" value={$new_ad_NetworkType}}  
    {assign var="p4" value=", "|explode:$p3}
    {$ppp1=array("591","樂屋","好房","房搜","拼生活","台灣房屋幸福租","免費租","林媽媽","網路地產")}
    {foreach $ppp1 as $n => $x}
    <input type="checkbox" id="AD_NetworkType[]" name="AD_NetworkType[]" value="{$x}" style="margin:5px;" {if $x|in_array:$p4}checked="checked"{/if}>{$x}&nbsp;&nbsp;&nbsp;
    {/foreach}
    </td>
  </tr>
  <tr style="color:#0056b3;" align="right">
    <td colspan="2">
    <input type="radio" id="ad_ss" name="ad_ss" value="0" style="margin:5px;" checked>不更新
    <input type="radio" id="ad_ss" name="ad_ss" value="1" style="margin:5px;">更新
    <input type="radio" id="ad_ss" name="ad_ss" value="2" style="margin:5px;">新增
    </td>
  </tr>
</table>
