<div class="panel panel-default">
        <div class="panel-heading" {$form.name} onclick="collapse_data(this)">{$form.title}(點選收合)
        </div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">
            {foreach from=$landlord_history key=k item=v}
            <table width="100%" border="1">
                <tr>
                    <td class="main_t_2 input-group-addon">房東名字:</td>
                    <td><input type="text"  value="{$v.member_landlord_name}" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">手機:</td>
                    <td><input type="text"  value="{$v.member_landlord_user}" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">備用手機:</td>
                    <td><input type="text"  value="{$v.member_landlord_tel}" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">房東英文:</td>
                    <td><input type="text"  value="{$v.member_landlord_en_name}" size="10" disabled="disabled"></td>
                    <td class="main_t_2 input-group-addon">稱謂:</td>
                    <td>
                    {if $v.member_landlord_appellation == "0"}
                    <input type="text"  value="先生" size="10" disabled="disabled">
                    {else}
                    <input type="text"  value="小姐" size="10" disabled="disabled">
                    {/if}
                    </td>
                </tr>
                <tr>
                <td class="main_t_2 input-group-addon" style="color:red">是否黑名單:</td>
                <td><input type="checkbox"  value="0" size="10" {if $v.blacktype =="1"} checked {/if} onclick="return false;"></td>
                <td class="main_t_2 input-group-addon">原因:</td>
                <td><input type="text" id="black_reason" name="black_reason" value="{$v.blackreason}" style="width:100%;"></td>
                <td class="main_t_2 input-group-addon">紀錄時間:</td>
                <td><input type="date" id="recodeTime" name="recodeTime" value="{$v.recodeTime}"></td>
                <td class="main_t_2 input-group-addon">紀錄人:</td>
                <td><input type="text" id="black_cp" name="black_cp" value="{$v.create_people}" style="width:100%;" readonly></td>
                </tr>
                <tr>
                  <td class="main_t_2 input-group-addon">來源:</td>
                  <td><input type="text"  value="{$v.member_landlord_id_source}" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">生日:</td>
                  <td><input type="text"  value="{$v.member_landlord_birthday}" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">ID:</td>
                  <td><input type="text"  value="{$v.member_landlord_identity}" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">Line ID:</td>
                  <td><input type="text"  value="{$v.member_landlord_line_id}" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">Email:</td>
                  <td><input type="text"  value="{$v.member_landlord_email}" size="10" disabled="disabled"></td>
                </tr>
                <tr>
                  <td class="main_t_2 input-group-addon">房東備註</td>
                  <td><input type="text"  value="{$v.member_landlord_landlord_details}" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">市話1</td>
                  <td><input type="text"  value="{$v.member_landlord_local_tel_1}" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">市話2:</td>
                  <td><input type="text"  value="{$v.member_landlord_local_tel_2}" size="10" disabled="disabled"></td>
                  <td class="main_t_2 input-group-addon">更新日期:</td>
                  <td><input type="text"  value="{$v.member_landlord_active_off_date_0}" size="10" disabled="disabled"></td>
                  <td><span class="main_t_2 input-group-addon">原因:</span></td>
                    <td><input type="text"  value="{$v.member_landlord_active_off_reason_0}" size="10" disabled="disabled"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><span class="main_t_2 input-group-addon">更新者:</span></td>
                    <td><input type="text"  value="{$v.member_landlord_active_off_admin_0}" size="10" disabled="disabled"></td>
                </tr>
                </table>
            <hr>
            {/foreach}
            </div>
</div>
</div>