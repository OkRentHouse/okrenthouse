<div class="panel panel-default">
        <div class="panel-heading" {$form.name} onclick="collapse_data(this)">{$form.title}(點選收合)
        </div>
        <div class="panel-body" style="display: none;">
            <div class="form_data">
{foreach from=$catch_history key=k item=v}
<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">房東</td>
    <td colspan="7">
    <input type="text" id="read_landlord_name" name="read_landlord_name" value="{$v.landlord_name}" size="10" readonly>
    <input type="hidden" id="read_landlord_id_member" name="read_landlord_id_member" value="{$read_landlord_id_member}" size="10" readonly>
    </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租約:</td>
    <td><input type="text" value="{$v.lease}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon" style="color:red">目前合約編號:</td>
    <td><input type="text" value="{$v.leaseSequence}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">來源:</td>
    <td><input type="text" value="{$v.source}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text" value="{$v.userName}" size="10" disabled="disabled"></td>
  </tr>
    <tr>
  <td class="main_t_2 input-group-addon" style="color:red">是否黑名單:</td>
  <td><input type="checkbox" id="black_add" name="black_add" value="1" {if $v.blacktype =="1"} checked {/if} onclick="return false;"></td>
  <td class="main_t_2 input-group-addon">原因:</td>
  <td><input type="text" id="black_reason" name="black_reason" value="{$v.blackreason}" style="width:100%;"></td>
  <td class="main_t_2 input-group-addon">紀錄時間:</td>
  <td><input type="date" id="recodeTime" name="recodeTime" value="{$v.recodeTime}"></td>
  <td class="main_t_2 input-group-addon">紀錄人:</td>
  <td><input type="text" id="black_cp" name="black_cp" value="{$v.create_people}" style="width:100%;" readonly></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text"  value="{$v.leasetitle}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">生日:</td>
    <td><input type="date" value="{$v.birthday}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text" value="{$v.userID}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">手機1:</td>
    <td><input type="text" value="{$v.mobile1}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text"  value="{$v.mobile2}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text"  value="{$v.lineID}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" value="{$v.tel_h}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text" value="{$v.tel_o}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text"  value="{$v.residenceAddress}" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">聯絡地址:</td>
    <td colspan="3"><input type="text" value="{$v.contractAddress}" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text" value="{$v.homeAddress}" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" value="{$v.businessAddress}" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" value="{$v.email}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text" value="{$v.bussinessNumber}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text"  value="{$v.worktitle}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">租約條件:</td>
    <td><input type="text" value="{$v.leaseCondition}" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入帳帳戶1:</td>
    <td><input type="text"  value="{$v.bankAccount1}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">入帳帳戶2:</td>
    <td><input type="text"  value="{$v.bankAccount2}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text"  value="{$v.relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">繳租記錄:</td>
    <td><input type="text"  value="{$v.payRecord}" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租期:</td>
    <td>起<input type="date" value="{$v.leaseRangeStart}" size="10" disabled="disabled"><br/>結<input type="date" id="leaseRangeEnd" name="leaseRangeEnd" value="{$v.leaseRangeEnd}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">租金:</td>
    <td><input type="text" value="{$v.rentMoney}" onkeyup="value=value.replace(/[^\d]/g,'')" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">其他：</td>
    <td><input type="text" value="{$v.rent_other}" size="10" disabled="disabled"><br/>
    {if $v.rent_other1 == "0"}
      車位
      {/if}
      {if $v.rent_other1 == "1"}
      管理費
      {/if}
      {if $v.rent_other1 == "2"}
      兩者
      {/if}
    </td>
    <td class="main_t_2 input-group-addon">繳租日:</td>
    <td><input type="test"  value="{$v.rentPayDate}" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">帶看配合:</td>
    <td><input type="text"  value="{$v.lookOther}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">押金金額:</td>
    <td><input type="text" value="{$v.bonds}" onkeyup="value=value.replace(/[^\d]/g,'')" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">押金繳交記錄:</td>
    <td><input type="text"  value="{$v.bondsRecord}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">押金擔保:</td>
    <td><input type="text" value="{$v.bondsPer}" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">期滿退租:</td>
    <td><input type="text"  value="{$v.leasefull}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">AG:</td>
    <td><input type="text"  value="" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">仲介方案:</td>
    <td> {*
      <select id="agencyPlan" name="YourLocation">
        
    　
        <option value="仲介方案A">仲介方案A</option>
        
    　
        <option value="仲介方案B">仲介方案B</option>
      </select>
      *}
      {if $v.agencyPlan == "A"}
      
      甲
      {/if}
      {if $v.agencyPlan == "B"}
      乙
      {/if} </td>
    <td class="main_t_2 input-group-addon">代管方案:</td>
    <td>
      {if $v.managePlan == "A"}      
      A
      {/if}
      {if $v.managePlan == "B"}
      B
      {/if}
      {if $v.managePlan == "C"}
      C
      {/if} </td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租售同步:</td>
    <td><input type="text"  value="{$v.rentAndSale}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">點 in 日期:</td>
    <td><input type="date" value="{$v.checkin}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">點交人員:</td>
    <td><input type="text"  value="{$v.checkinPeople}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">中途解約:</td>
    <td><input type="text" value="{$v.breaklease}" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">租管工會:</td> 
    <td><input type="text"  value="{$v.rentAssoc}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">加值 - 租霸補助:</td>
    <td><input type="text"  value="{$v.addRent}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">加值 - 信用認證:</td>
    <td><input type="text"  value="{$v.addCredit}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">加值 - 安心履約:</td>
    <td><input type="text"  value="{$v.addCare}" size="10" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">入手價:</td>
    <td><input type="text"  value="{$v.buyPrice}" onkeyup="value=value.replace(/[^\d]/g,'')" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">桃協會:</td>
    <td><input type="text"  value="{$v.taoAssoc}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">台協會:</td>
    <td><input type="text"  value="{$v.twAssoc}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon" style="color:red">契約編號</td>
    <td><input style="color:red" type="text" id="lease_number" name="lease_number" value="{$v.lease_number}" size="10" disabled="disabled"/></td>
  </tr>

  <tr>
    <td class="main_t_2 input-group-addon">租屋條件:</td>
    <td colspan="5"><input type="text" id="member_landlord_id_other_conditions" name="member_landlord_id_other_conditions" value="{$v.member_landlord_id_other_conditions}" size="120" disabled="disabled"/></td>
    <td class="main_t_2 input-group-addon" style="color:red">更新人員</td>
    <td><input style="color:red" type="text" id="update_people2" name="update_people" value="{$v.updatePeople}" size="10" disabled="disabled"/></td>
  </tr>

</table>
<br/>
<table width="100%" border="1">
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">新擔保人合約</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">姓名:</td>
    <td><input type="text"  value="{$v.userName_relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">稱謂:</td>
    <td><input type="text"  value="{$v.leasetitle_relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">關係:</td>
    <td><input type="text"  value="{$v.relation_relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon" style="color:red">生日:</td>
    <td><input type="date"  value="{$v.birthday_relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">身分證:</td>
    <td><input type="text"  value="{$v.userID_relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon" style="color:red">手機1:</td>
    <td><input type="text"  value="{$v.mobile1_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">手機2:</td>
    <td><input type="text"  value="{$v.mobile2_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">Line ID:</td>
    <td><input type="text"  value="{$v.lineID_relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話H:</td>
    <td><input type="text" value="{$v.tel_h_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">市話O:</td>
    <td><input type="text"  value="{$v.tel_o_relation}" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="10" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">E-mail:</td>
    <td><input type="email" value="{$v.email_relation}" size="10" disabled="disabled"></td>
    </tr>
  <tr>
    <td class="main_t_2 input-group-addon">戶籍地址:</td>
    <td colspan="3"><input type="text"  value="{$v.residenceAddress_relation}" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">聯絡地址:</td>
    <td colspan="3"><input type="text" value="{$v.homeAddress_relation}" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">住家地址:</td>
    <td colspan="3"><input type="text"  value="{$v.homeAddress_relation}" size="50" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">公司地址:</td>
    <td colspan="3"><input type="text" value="{$v.businessAddress_relation}" size="50" disabled="disabled"></td>
  </tr>
  <tr>
    <td class="main_t_2 input-group-addon">公司寶號:</td>
    <td><input type="text"  value="{$v.bussinessNumber_relation}" size="10" disabled="disabled"></td>
    <td class="main_t_2 input-group-addon">工作|職稱:</td>
    <td><input type="text"  value="{$v.worktitle_relation}" size="10" disabled="disabled"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
</table>
<br/><br/>
<hr>
{/foreach}
</div>
</div>
</div>
