{foreach from=$tpl_data_c_i_s key=k item=v}
    {*   {$v.label}*}
    {if !empty($v.html)}
    {elseif $v.type=='hidden'}
        <input type="hidden" name="{$v.name}" id="{$v.id}" value="{if isset($row_c_i_s.$k)}{$row_c_i_s.$k} {elseif $main_index==$v.name}{$smarty.get.$main_index}{/if}">
    {elseif $v.type=='text' || $v.type=='number' || $v.type=='date'}
        <div class="form-group col-lg-{$v.form_col}" {if $v.form_id} id="{$v.form_id}"{/if}>
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <input class="form-control" type="{$v.type}" name="{$v.name}" id="{$v.id}" value="{if isset($row_c_i_s.$k)}{$row_c_i_s.$k}{/if}" {if $v.maxlength} maxlength="{$v.maxlength}" {/if}>
            </div>
        </div>
    {elseif $v.type=='textarea'}
        <div class="form-group col-lg-{$v.form_col}" {if $v.form_id}id="{$v.form_id}"{/if}>
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <textarea type="text" name="{$v.name}" id="{$v.id}" class="form-control"
                    {if isset($v.rows)} rows="{$v.rows|intval}"{/if}
                        {if isset($v.cols)} cols="{$v.cols|intval}"{/if} {if $v.maxlength} maxlength="{$v.maxlength}" {/if}>{$row_c_i_s.$k}</textarea>
            </div>
        </div>
    {elseif $v.type=='checkbox'}
        <div class="form-group col-lg-{$v.form_col}" {if $v.form_id}id="{$v.form_id}"{/if}>
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <div class="btn-group" data-toggle="buttons">
                    {foreach from=$v.values key=ckb_k item=ckb_v}
                        <label class="btn btn-default {if $ckb_v.val==$row_c_i_s.$k}active{/if}" for="{$ckb_v.id}">
                            <input type="radio" autocomplete="off" name="{$v.name}" id="{$ckb_v.id}" value="{$ckb_v.val}" {if $ckb_v.val==$row_c_i_s.$k}checked="checked"{/if}>{$ckb_v.label}
                        </label>
                    {/foreach}
                </div>
            </div>
        </div>
    {elseif $v.type=='select'}
        <div class="form-group col-lg-{$v.form_col}" {if $v.form_id} id="{$v.form_id}"{/if}>
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <select class="form-control" name="{$v.name}">
                    {foreach from=$v.values  key=se_k item=se_v}
                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row_c_i_s[$v.name][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    {elseif $v.type=='tpl'}
        <div class="form-group col-lg-{$v.form_col}" {if $v.form_id}id="{$v.form_id}"{/if}>
            <label class="control-label col-lg-{$v.label_col}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                {if isset($row_c_i_s[$v.name|cat:"_detail"])}
                    {foreach from=$row_c_i_s[$v.name|cat:"_detail"] key=rk item=rv}
                        {if $rk==0}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row_c_i_s[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon">金額</span><input class="form-control" type="number" name="{$v.name}_price[]" value="{$row_c_i_s[$v.name|cat:"_price"][$rk]}" oninput="price_total(this.value)">
                                <span class="input-group-addon">日期</span><input class="form-control" type="date" name="{$v.name}_date[]"  value="{$row_c_i_s[$v.name|cat:"_date"][$rk]}">
                                <span class="input-group-addon" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}" onclick="add_data_c_i_s(this)" >+</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">明細</span>
                                <input class="form-control" type="text" name="{$v.name}_detail[]" value="{$row_c_i_s[$v.name|cat:"_detail"][$rk]}">
                            </div>
                        {else}
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">類型</span>
                                <select class="form-control" name="{$v.name}_type[]">
                                    {foreach from=$v.values  key=se_k item=se_v}
                                        <option value="{$se_v.val}" id="{$se_v.val}" {if $row_c_i_s[$v.name|cat:"_type"][$rk]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                                    {/foreach}
                                </select>
                                <span class="input-group-addon">金額</span><input class="form-control" type="number" name="{$v.name}_price[]" value="{$row_c_i_s[$v.name|cat:"_price"][$rk]}" oninput="price_total(this.value)">
                                <span class="input-group-addon">日期</span><input class="form-control" type="date" name="{$v.name}_date[]"  value="{$row_c_i_s[$v.name|cat:"_date"][$rk]}">
                                <span class="input-group-addon" onclick="del_data_c_i_s(this)" >-</span>
                            </div>
                            <div class="input-group fixed-width-lg">
                                <span class="input-group-addon">明細</span>
                                <input class="form-control" type="text" name="{$v.name}_detail[]" value="{$row_c_i_s[$v.name|cat:"_detail"][$rk]}">
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">類型</span>
                        <select class="form-control" name="{$v.name}_type[]">
                            {foreach from=$v.values  key=se_k item=se_v}
                                <option value="{$se_v.val}" id="{$se_v.val}">{$se_v.text}</option>
                            {/foreach}
                        </select>
                        <span class="input-group-addon">金額</span><input class="form-control" type="number" name="{$v.name}_price[]" value="" oninput="price_total(this.value)">
                        <span class="input-group-addon">日期</span><input class="form-control" type="date" name="{$v.name}_date[]"  value="">
                        <span class="input-group-addon" data-name="{$v.name}" data-select_text="{$v.select.text}" data-select_val="{$v.select.val}"  onclick="add_data_c_i_s(this)" >+</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">明細</span>
                        <input class="form-control" type="text" name="{$v.name}_detail[]" value="">
                    </div>
                {/if}
            </div>
        </div>
    {/if}
{/foreach}