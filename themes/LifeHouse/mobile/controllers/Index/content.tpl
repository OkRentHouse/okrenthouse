<div class="banner_area" uk-slideshow="animation: push;autoplay: true;ratio: 40:19">
	<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
		<ul class="uk-slideshow-items">
			<li><img src="{$THEME_URL}/mobile/img/index/banner01.png"</li>
			<li><img src="{$THEME_URL}/mobile/img/index/banner02.png"</li>
		</ul>
		<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
			  uk-slideshow-item="previous"></samp>
		<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
			  uk-slideshow-item="next"></samp>
	</div>
	<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
</div>
<nav class="menu_buoy">
	<div>
	<a href="#" class="active">
		吉屋快搜
	</a><b></b><a href="#">
		NEWS好康
	</a><b></b><a href="#">
		樂租客服
	</a><b></b><a href="#">
		刊登廣告
	</a><b></b><a href="#">
		生活家族
	</a>
		<buoy></buoy>
	</div>
</nav>
<div class="document">
    {*	<div class="house_my_search">*}
    {*		<div class="house_wrapper">*}
    {*			<div class="house_area">*}
    {*				<div class="house_photo">*}
    {*					<div class="house_type">租</div>*}
    {*					<img src="{$THEME_URL}/mobile/img/text/b.jpg">*}
    {*					<div class="house_casename">大有商圈電梯大樓時尚套房</div>*}
    {*				</div>*}
    {*				<div class="house_text">*}
    {*					<div class="house_location">台北市/光路復</div>*}
    {*					<div class="house_room">3房2廳2衛/32.4坪</div>*}
    {*					<div class="house_price"><samp class="price">6,800</samp> 元/月</div>*}
    {*				</div>*}
    {*			</div>*}
    {*			<div class="house_area">*}
    {*				<div class="house_photo">*}
    {*					<div class="house_type">售</div>*}
    {*					<img src="{$THEME_URL}/mobile/img/text/a.jpg">*}
    {*					<div class="house_casename">中正商圈電梯大樓雙拼住宅</div>*}
    {*				</div>*}
    {*				<div class="house_text">*}
    {*					<div class="house_location">桃園縣/藝文特區/路名</div>*}
    {*					<div class="house_room">2房1廳1衛/31.4坪</div>*}
    {*					<div class="house_price"><samp class="price">680</samp> 萬</div>*}
    {*				</div>*}
    {*			</div>*}
    {*		</div>*}
    {*	</div>*}
	<div class="slide_title no_border_r">吉屋快搜</div>
	<form id="search_form" name="search_form" method="post">
		<div class="search_select">
			<select data-role="none">
				<option value="1">租屋</option>
				<option value="2">買屋</option>
			</select>
			<select data-role="none">
                {foreach $arr_county as $v => $arr}
					<option value="{$v}">{$arr.text}</option>
                {/foreach}
			</select>
			<select data-role="none">
				<option>區別</option>
                {foreach $arr_area as $v => $text}
					<option value="{$v}">{$text}</option>
                {/foreach}
			</select>
            {*			<select data-role="none">*}
            {*				<option>用途</option>*}
            {*			</select>*}
			<select data-role="none" class="no_border_r">
                {foreach $arr_class as $v => $text}
					<option value="{$v}">{$text}</option>
                {/foreach}
			</select>
            {*			<select data-role="none">*}
            {*				<option>房數</option>*}
            {*			</select>*}
			<img src="{$THEME_URL}/mobile/img/icon/pic_m.svg" class="icon">
		</div>
		<div id="search_box">
			<div id="search_text">
				<input name="field" type="text" class="field" data-role="none" placeholder="請輸入關鍵字(社區、街道、捷運名稱等)"
					   size="40"/>
			</div>
			<div id="search_btn">
				<button type="submit" id="search_button" class="btn" data-role="none">查詢</button>
			</div>
		</div>
	</form>
	<div class="slide_area">
		<div class="slide_title">News好康</div>
		<nav><a href="#">生活好康</a><a href="#">活動分享</a><a href="#">創富分享</a></nav>
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
					<li><img src="{$THEME_URL}/mobile/img/index/news01.png"</li>
					<li><img src="{$THEME_URL}/mobile/img/index/news02.jpg"</li>
					<li><img src="{$THEME_URL}/mobile/img/index/news03.png"</li>
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
		<div class="slide_title">生活家族</div>
		<nav><a href="#">達人顧問</a><a href="#">樂租夥伴</a><a href="#">加盟生活</a></nav>
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
					<li><img src="{$THEME_URL}/mobile/img/index/family01.png"</li>
					<li><img src="{$THEME_URL}/mobile/img/index/family02.png"</li>
					<li><img src="{$THEME_URL}/mobile/img/index/family03.png"</li>
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
		<div class="slide_title">樂租服務</div>
		<nav><a href="#">管理查詢</a><a href="#">宅急修</a><a href="#">租賃信用</a></nav>
		<div class="slide_photo" uk-slideshow="animation: push;autoplay: true">
			<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
				<ul class="uk-slideshow-items">
					<li><img src="{$THEME_URL}/mobile/img/index/service01.jpg"</li>
					<li><img src="{$THEME_URL}/mobile/img/index/service02.png"</li>
					<li><img src="{$THEME_URL}/mobile/img/index/service03.png"</li>
				</ul>
				<samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
					  uk-slideshow-item="previous"></samp>
				<samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
					  uk-slideshow-item="next"></samp>
			</div>
			<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
	</div>
</div>