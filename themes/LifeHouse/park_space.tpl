        {if !empty($park_space[$input.name])}
            {if !empty($park_space[$input.name][$input.name|cat:"_park_position"])}
                {foreach from=$park_space[$input.name][$input.name|cat:"_park_position"] key=rk item=rv}
                <div class="input-group fixed-width-lg">
                    {if $rk==0}
                    <span class="input-group-addon">位置</span>
                    <select class="form-control" name="{$input.name}_park_position[]">
                        <option value="0" {if $park_space[$input.name][$input.name|cat:"_park_position"][$rk]=="0"}selected="selected"{/if}>社區內</option>
                        <option value="1" {if $park_space[$input.name][$input.name|cat:"_park_position"][$rk]=="1"}selected="selected"{/if}>社區外</option>
                    </select>
                    <span class="input-group-addon">類型</span>
                    <select class="form-control" name="{$input.name}_type[]">
                        <option value="0" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="0"}selected="selected"{/if}>坡道平面</option>
                        <option value="1" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="1"}selected="selected"{/if}>坡道機械</option>
                        <option value="2" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="2"}selected="selected"{/if}>升降平面</option>
                        <option value="3" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="3"}selected="selected"{/if}>升降機械</option>
                        <option value="4" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="4"}selected="selected"{/if}>一樓平面</option>
                    </select>
                    <span class="input-group-addon">編號</span><input class="form-control" type="text" name="{$input.name}_serial[]" value="{$park_space[$input.name][$input.name|cat:"_serial"][$rk]}" maxlength="20">
                    <span class="input-group-addon" data-name="{$input.name}" onclick="add_data_park_space(this)" >+</span>
                </div>
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">租金</span><input class="form-control" type="number" name="{$input.name}_rent[]" value="{$park_space[$input.name][$input.name|cat:"_rent"][$rk]}">
                    <span class="input-group-addon">繳費方式</span>
                    <select class="form-control" name="{$input.name}_rent_type[]">
                        <option value="0" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="0"}selected="selected"{/if}>月</option>
                        <option value="1" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="1"}selected="selected"{/if}>季</option>
                        <option value="2" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="2"}selected="selected"{/if}>半年</option>
                        <option value="3" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="3"}selected="selected"{/if}>年</option>
                        <option value="4" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]==""}selected="selected"{/if}>年</option>
                       <option value="4" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="4"}selected="selected"{/if}>包含管理費</option>
                    </select>
                    <span class="input-group-addon">備註</span>
                    <input class="form-control" type="text" name="{$input.name}_remarks[]" value="{$park_space[$input.name][$input.name|cat:"_remarks"][$rk]}" maxlength="64">
                </div>
                {else}
            <span class="input-group-addon">位置</span>
            <select class="form-control" name="{$input.name}_park_position[]">
                <option value="0" {if $park_space[$input.name][$input.name|cat:"_park_position"][$rk]=="0"}selected="selected"{/if}>社區內</option>
                <option value="1" {if $park_space[$input.name][$input.name|cat:"_park_position"][$rk]=="1"}selected="selected"{/if}>社區外</option>
            </select>
            <span class="input-group-addon">類型</span>
            <select class="form-control" name="{$input.name}_type[]">
                <option value="0" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="0"}selected="selected"{/if}>坡道平面</option>
                <option value="1" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="1"}selected="selected"{/if}>坡道機械</option>
                <option value="2" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="2"}selected="selected"{/if}>升降平面</option>
                <option value="3" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="3"}selected="selected"{/if}>升降機械</option>
                <option value="4" {if $park_space[$input.name][$input.name|cat:"_type"][$rk]=="4"}selected="selected"{/if}>一樓平面</option>
            </select>
            <span class="input-group-addon">編號</span><input class="form-control" type="text" name="{$input.name}_serial[]" value="{$park_space[$input.name][$input.name|cat:"_serial"][$rk]}" maxlength="20">
            <span class="input-group-addon" data-name="{$input.name}" onclick="delete_data_park_space(this)" >-</span>
        </div>
        <div class="input-group fixed-width-lg">
            <span class="input-group-addon">租金</span><input class="form-control" type="number" name="{$input.name}_rent[]" value="{$park_space[$input.name][$input.name|cat:"_rent"][$rk]}">
            <span class="input-group-addon">繳費方式</span>
            <select class="form-control" name="{$input.name}_rent_type[]">
                <option value="0" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="0"}selected="selected"{/if}>月</option>
                <option value="1" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="1"}selected="selected"{/if}>季</option>
                <option value="2" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="2"}selected="selected"{/if}>半年</option>
                <option value="3" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="3"}selected="selected"{/if}>年</option>
               <option value="4" {if $park_space[$input.name][$input.name|cat:"_rent_type"][$rk]=="4"}selected="selected"{/if}>包含管理費</option>
            </select>
            <span class="input-group-addon">備註</span>
            <input class="form-control" type="text" name="{$input.name}_remarks[]" value="{$park_space[$input.name][$input.name|cat:"_remarks"][$rk]}" maxlength="64">
        </div>
        {/if}
        {/foreach}
    {else}
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">位置</span>
                    <select class="form-control" name="{$input.name}_park_position[]">
                        <option value="0">社區內</option>
                        <option value="1">社區外</option>
                    </select>
                    <span class="input-group-addon">類型</span>
                    <select class="form-control" name="{$input.name}_type[]">
                        <option value="0">坡道平面</option>
                        <option value="1">坡道機械</option>
                        <option value="2">升降平面</option>
                        <option value="3">升降機械</option>
                        <option value="4">一樓平面</option>
                    </select>
                    <span class="input-group-addon">編號</span><input class="form-control" type="text" name="{$input.name}_serial[]" value="" maxlength="20">
                    <span class="input-group-addon" data-name="{$input.name}" onclick="add_data_park_space(this)" >+</span>
                </div>
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">租金</span><input class="form-control" type="number" name="{$input.name}_rent[]" value="">
                    <span class="input-group-addon">繳費方式</span>
                    <select class="form-control" name="{$input.name}_rent_type[]">
                        <option value="0">月</option>
                        <option value="1">季</option>
                        <option value="2">半年</option>
                        <option value="3">年</option>
                       <option value="4">包含管理費</option>
                    </select>
                    <span class="input-group-addon">備註</span>
                    <input class="form-control" type="text" name="{$input.name}_remarks[]" value="" maxlength="64">
                </div>
    {/if}
        {else}
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">位置</span>
                <select class="form-control" name="{$input.name}_park_position[]">
                    <option value="0">社區內</option>
                    <option value="1">社區外</option>
                </select>
                <span class="input-group-addon">類型</span>
                <select class="form-control" name="{$input.name}_type[]">
                    <option value="0">坡道平面</option>
                    <option value="1">坡道機械</option>
                    <option value="2">升降平面</option>
                    <option value="3">升降機械</option>
                    <option value="4">一樓平面</option>
                </select>
                <span class="input-group-addon">編號</span><input class="form-control" type="text" name="{$input.name}_serial[]" value="" maxlength="20">
                <span class="input-group-addon" data-name="{$input.name}" onclick="add_data_park_space(this)" >+</span>
            </div>
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">租金</span><input class="form-control" type="number" name="{$input.name}_rent[]" value="">
                <span class="input-group-addon">繳費方式</span>
                <select class="form-control" name="{$input.name}_rent_type[]">
                    <option value="0">月</option>
                    <option value="1">季</option>
                    <option value="2">半年</option>
                    <option value="3">年</option>
                   <option value="4">包含管理費</option>
                </select>
                <span class="input-group-addon">備註</span>
                <input class="form-control" type="text" name="{$input.name}_remarks[]" value="" maxlength="64">
            </div>
        {/if}