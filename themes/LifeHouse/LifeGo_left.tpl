<div class="t_col_2 left search in_txt">

  <div class="search">
    <input type="text" placeholder="清淨神器">
    <div class="zoomIMG"><img src="/themes/LifeHouse/img/lifego/search-solid.svg"></div>
  </div>

    <ul>
        {if empty($smarty.get.id_product_class)}
            {$len=count($product_class_arr)-1}
            {foreach $product_class_arr as $k => $v}
                <li><a href="/LifeGo?id_product_class={$v.id_product_class}"><img src="{$v.img}"><span>{$v.title}</span></a>{if $len!=$k}<div class="border"></div>{/if}</li>
                {if $len==$k}<li class="last"> </li>{/if}
            {/foreach}

        {else}

{*           {print_r($product_class_arr)}*}
            {if !empty($smarty.get.id_product_class) && !empty($product_class_item_arr[$smarty.get.id_product_class])}
                {foreach $product_class_arr as $k => $v}
                    {if $v.id_product_class== $smarty.get.id_product_class}
                    <li><a href="/LifeGo?id_product_class={$v.id_product_class}"><img src="{$v.img}"><span>{$v.title}</span></a><div class="border"></div></li>
                    {/if}
                {/foreach}
                {$len=count($product_class_item_arr)-1}
                {foreach $product_class_item_arr as $k => $v}
                    <li><a href="/LifeGo?LifeGoProduct={$v.id_product_class_item}"><img src=" "><span>{$v.item_title}</span></a><div class="border_Lv2"></div></li>
                {/foreach}
            {/if}
        {/if}

        {if $add_left_bottom}
            {$add_left_bottom}
        {else}
            {if $left_bottom}
            <li class="actions sep_line"></li>
                {foreach $left_bottom as $k => $v}
                    <li class="actions">
                        <a href="{$v.url}"><img src="{$v.img}"></a>
                        <div class="bottom_txt" alt="{$v.title}">{$v.title}</div>
                    </li>
                {/foreach}
            {/if}
        {/if}
    </ul>
  </div>
