<input type="hidden" name="{$input.name}">
<div class="col-lg-2 no_padding">
    <div class="input-group fixed-width-lg">
        <input type="text" name="postal[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>
    </div>
</div>

    <div class="col-lg-2 no_padding">
        <select name="id_county[]" class="form-control county">
            <option value="">請選擇縣市</option>
            {foreach $database_county as $d_c_k => $d_c_v}
                <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>
            {/foreach}
        </select>
    </div>
    <div class="col-lg-2 no_padding">
        <select name="id_city[]" class="form-control city">
            <option value="">請選擇鄉政區</option>
            {foreach $database_city as $d_c_k => $d_c_v}
                <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>
            {/foreach}
        </select>
    </div>
    <div class="col-lg-5 no_padding">
        <div class="input-group fixed-width-lg">
            <input type="text" name="village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
            <input type="number" name="neighbor" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
            <input type="text" name="road" class="form-control " value="" maxlength="20">
        </div>
    </div>
    <div class="col-lg-12 no_padding">
        <div class="input-group fixed-width-lg">
            <input type="text" name="segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
            <input type="text" name="lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
            <input type="text" name="alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
            <input type="text" name="no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
            <span class="input-group-addon">之</span><input type="text" name="no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
            <input type="text" name="floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
            <span class="input-group-addon">之</span><input type="text" name="floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
            <input type="text" name="address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
        </div>
    </div>
    <div class="col-lg-12 no_padding">
        <input type="text" name="address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
    </div>