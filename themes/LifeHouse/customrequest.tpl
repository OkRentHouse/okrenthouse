<style>
.customer tr td{ padding:10px 15px; border:#ccc 1px solid;}
.commodity {
    color: #333;
}
tbody>tr:hover, tbody>tr.active, .table>tbody>tr.active>td {
    background-color:transparent;
}
</style>
<script>
var nDefCity = "<?=$rowMember->city?>";
var nDefArea = "<?=$rowMember->area?>";


$(function() {
	$("#Birthday_Y").change(function() {
		setBirthday_Day($(this).val(),$("#Birthday_M").val());
	});
	$("#Birthday_M").change(function() {
		setBirthday_Day($("#Birthday_M").val(),$(this).val());
	});
	setBirthday_Day($("#Birthday_Y").val(),$("#Birthday_M").val());
});


function setBirthday_Day(nY, nM) {
	var dDate = new Date(nY, nM, 0);
	nCurDay = $("#Birthday_D").val();
	$("#Birthday_D option").remove();
	for (i=1; i<= dDate.getDate(); i++) $("#Birthday_D").append("<option></option>").children(":last").val(i).text(i).attr("selected", (nCurDay==i));
}


function setCityArea() {
	$("#Area option").remove();
	$("#Area").append("<option value=''>請選擇</option>").append($("#jAllArea option[c="+$("#City").val()+"]").clone());
	if ($("#City").val() == nDefCity && nDefCity !== "") $("#Area option[value='"+nDefArea+"']").attr("selected",true);
}


function setZip() {
	$("#ZIP").val($("#Area option:selected").attr("z"));
}


$(function() {
	//複製新Area並隱藏
	$("#Area").after('<select name="jAllArea" id="jAllArea"></select>');
	$("#jAllArea").hide().append($("#Area option").clone());
	
	$("#City").change(function() {
		setCityArea($(this).val());
		setZip();
	});
	$("#Area").change(function() {
		setZip();
	});
	
	setCityArea();
});


</script>
{if !empty($CreateDate)}
{* {foreach from=$CR key=k item=v} *}
<table width="100%" border="1" class="customer">
  <tr>
    <td>登入:<span style="color:red;margin:5px;">{$CreatePeople}</span></td>
    <td align="center"><span style="color:#0056b3;margin:5px;"><h2><b>客戶需求單</b></h2></span></td>
    <td>來源:
    {$input=array("緣故","達人","公佈欄","掃街陌開","保全","網站:591","網站:樂屋","電話開發","拜訪","追蹤","社區拜訪","介紹","名單","房東")}
    <select id="Source" name="Source" style="margin:5px;">
      {foreach $input as $n => $x}
        <option value="{$x}"
        {if $Source == $x}
        selected
        {/if}>{$x}</option>
      {/foreach}
    </select>
    </td>
    <td>派單:
    <select id="newag" name="newag" style="margin:5px;">
      <option value="請選擇業務">請選擇業務</option>
      {foreach $newag as $n => $x}
        <option value="{$x.email}"
        {if $ag == $x.email}
        selected
        {/if}>{$x.en_name}</option>
      {/foreach}
    </select>
    </td> 
  </tr>
  <tr>
    <td>姓名:<input type="text" style="width:100px;margin:5px;" id="CustomerName" name="CustomerName" value="{$CustomerName}">&nbsp;&nbsp;&nbsp;
    <br/>稱謂:
    {$input2=array("先生","小姐")}
    <select id="CustomerTitle" name="CustomerTitle" style="margin:5px;">
    {foreach $input2 as $n => $x}
        <option value="{$x}"
        {if $CustomerTitle == $x}
        selected
        {/if}>{$x}</option>
      {/foreach}
    </select></td>
    <td colspan="2">
    手機1:<input type="text" style="width:100px;margin:5px;"  id="CustomerPhone" name="CustomerPhone" value="{$CustomerPhone}" maxlength="10" size="10" onKeyUp="value=value.replace(/[^\d]/g,'')"> &nbsp;&nbsp;&nbsp;
    手機2:<input type="text" style="width:100px;margin:5px;"  id="CustomerPhone1" name="CustomerPhone1" value="{$CustomerPhone}" maxlength="10" size="10" onKeyUp="value=value.replace(/[^\d]/g,'')"> &nbsp;&nbsp;&nbsp;
    市話:<input type="text" id="CustomerTel" name="CustomerTel" style="width:100px;margin:5px;" value="{$CustomerTel}" maxlength="10" size="10" onKeyUp="value=value.replace(/[^\d]/g,'')"></td>
    <td>
    職業:<input type="text" style="width:100px;margin:5px;" id="CustomerWork" name="CustomerWork" value="{$CustomerWork}"><br/>
    居住人數:大x<input type="text" id="LivePeople1" name="LivePeople1" style="width:50px;margin:5px;" value="{$LivePeople1}" size="5">小x<input type="text" id="LivePeople2" name="LivePeople2" value="{$LivePeople2}" size="5" style="width:50px;margin:5px;"><br/>
    關係:
    {assign var="c1" value={$CustomerRelation}}  
    {assign var="c2" value=", "|explode:$c1}
    {$fff1=array("家庭","朋友","同事")}
    {foreach $fff1 as $n => $x}
        <input type="checkbox" id="CustomerRelation[]" name="CustomerRelation[]" value="{$x}" style="margin:5px;" {if $x|in_array:$c2}checked="checked"{/if}>{$x}
    {/foreach}
    </td>
  </tr>
  <tr>
    <td colspan="2">區域:{$HouseArea}<br/>{$HouseCity}</td>
    <td colspan="2">
    格局:
    {$roo2=array("","2房","3房","4房","5房")}
    <select id="Room2" name="Room2" style="margin:5px;">
    {foreach $roo2 as $n => $x}
        <option value="{$x}"
        {if $Room2 == $x}
        selected
        {/if}>{$x}</option>
      {/foreach}
    </select>
    &nbsp; ~ &nbsp;
    {$roo2_1=array("","2房","3房","4房","5房")}
    <select id="Room2_1" name="Room2_1" style="margin:5px;">
    {foreach $roo2_1 as $n => $x}
        <option value="{$x}"
        {if $Room2_1 == $x}
        selected
        {/if}>{$x}</option>
      {/foreach}
    </select>
    &nbsp; / &nbsp;
    {$roo3=array("","1廳","2廳","3廳","4廳")}
    <select id="Room3" name="Room3" style="margin:5px;">
    	{foreach $roo3 as $n => $x}
        <option value="{$x}" 
        {if $Room3 == $x}
        selected
        {/if}>{$x}</option>
      {/foreach}
    </select>
    &nbsp; / &nbsp;
    {$roo4=array("","1衛","2衛","3衛","4衛","5衛")}
    <select id="Room4" name="Room4" style="margin:5px;">
    	{foreach $roo4 as $n => $x}
        <option value="{$x}"
        {if $Room4 == $x}
        selected
        {/if}>{$x}</option>
      {/foreach}
    </select><br/>
    套房:
    {assign var="room1" value={$Room}}  
    {assign var="room2" value=", "|explode:$room1}
    {$rrr1=array("分租","OPEN","隔間")}
    {foreach $rrr1 as $n => $x}
        <input type="checkbox" id="Room[]" name="Room[]" value="{$x}" style="margin:5px;" {if $x|in_array:$room2}checked="checked"{/if}>{$x}
    {/foreach}
    </td>
    </tr>
  <tr>
    <td>類別預算</td>
    <td colspan="3">
      月-月租:
      <input type="text" id="HouseRent" name="HouseRent" value="{$HouseRent}" style="width:100px;margin:5px;"/><br/> 
      <input type="checkbox" id="Investment" name="Investment" value="投資" style="margin:5px;" {if $Investment =="投資"}checked{/if}>投資 &nbsp;&nbsp;&nbsp; 買-總價:
      <input type="text" id="BuyPrice" name="BuyPrice" value="{$BuyPrice}" style="width:100px;margin:5px;"/>&nbsp;&nbsp;&nbsp; 自備:<input type="text" id="DownPayment" name="DownPayment" value="{$DownPayment}" size="15" style="width:100px;margin:5px;"/>&nbsp;&nbsp;&nbsp;貸款:<input type="text" id="Loan" name="Loan" value="{$Loan}" size="15" style="width:100px;margin:5px;"/>&nbsp;&nbsp;&nbsp;
    </td>
  </tr>
  <tr>
    <td>類別格局</td>
    <td colspan="3">
    <br/>
    類別:
    {assign var="p1" value={$Pattern}}  
    {assign var="p2" value=", "|explode:$p1}
    {$ppp1=array("華廈","公寓","透天","別墅","店面","辦公室","廠房","住辦","土地")}
    {foreach $ppp1 as $n => $x}
        <input type="checkbox" id="Pattern[]" name="Pattern[]" value="{$x}" style="margin:5px;" {if $x|in_array:$p2}checked="checked"{/if}>{$x}
    {/foreach}
    </td>
  </tr>
  <tr>
    <td>需求</td>
    <td colspan="3">室內坪數:<input type="text" id="InsidePi" name="InsidePi" value="{$InsidePi}" style="width:50px;margin:5px;">&nbsp;&nbsp;&nbsp;屋齡(<input type="text" id="HouseOld" name="HouseOld" value="{$HouseOld}" size="5" style="width:50px;margin:5px;">
      年↓)&nbsp;&nbsp;&nbsp;
      可炊:
      {assign var="h1" value={$HouseCook}}  
      {assign var="h2" value=", "|explode:$h1}
      {$hhh1=array("瓦斯","電磁")}
      {foreach $hhh1 as $n => $x}
        <input type="checkbox" id="HouseCook[]" name="HouseCook[]" value="{$x}" style="margin:5px;" {if $x|in_array:$h2}checked="checked"{/if}>{$x}
      {/foreach}&nbsp;&nbsp;&nbsp;
      陽台:
      {assign var="b1" value={$HouseBalcony}}  
      {assign var="b2" value=", "|explode:$b1}
      {$bbb1=array("前陽台","後陽台")}
      {foreach $bbb1 as $n => $x}
        <input type="checkbox" id="HouseBalcony[]" name="HouseBalcony[]" value="{$x}" style="margin:5px;" {if $x|in_array:$b2}checked="checked"{/if}>{$x}
      {/foreach}<br/>
      <input type="checkbox" id="Decorate" name="Decorate" value="美裝潢" style="margin:5px;" {if $Decorate =="美裝潢"}checked{/if}>美裝潢&nbsp;&nbsp;&nbsp;
    其他:
    {assign var="o1" value={$Other}}  
    {assign var="o2" value=", "|explode:$o1}
    {$ooo1=array("遷籍","報稅","輔助","(營/廠)登記")}
    {foreach $ooo1 as $n => $x}
        <input type="checkbox" id="Other[]" name="Other[]" value="{$x}" style="margin:5px;" {if $x|in_array:$o2}checked="checked"{/if}>{$x}
    {/foreach}&nbsp;&nbsp;&nbsp;
    車位:平面 X <input type="text" id="CarPlace" name="CarPlace" value="{$CarPlace}" size="5" style="width:50px;margin:5px;">
    個&nbsp;&nbsp;&nbsp;
    可寵:
    {assign var="pet1" value={$Pet}}  
    {assign var="pet2" value=", "|explode:$pet1}
    {$ppet1=array("貓","狗")}
    {foreach $ppet1 as $n => $x}
        <input type="checkbox" id="Pet[]" name="Pet[]" value="{$x}" style="margin:5px;" {if $x|in_array:$pet2}checked="checked"{/if}>{$x}
    {/foreach}<br/>
    近學區:
    <input type="text" id="School" name="School" value="{$School}" style="width:100px;margin:5px;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="ShortRent" name="ShortRent" value="短租" style="margin:5px;" {if $Decorate =="短租"}checked{/if}>短租&nbsp;&nbsp;&nbsp;
    休閒設施:
    {assign var="le1" value={$Lestime}}  
    {assign var="le2" value=", "|explode:$le1}
    {$let1=array("游泳池","健身房","視聽中心","KTV","兒童遊戲區","SPA","球室","閱覽室","交誼廳","咖啡吧","中庭花園","會議室","接待大廳","公用陽台","游泳池")}
    {foreach $let1 as $n => $x}
        <input type="checkbox" id="Lestime[]" name="Lestime[]" value="{$x}" style="margin:5px;" {if $x|in_array:$le2}checked="checked"{/if}>{$x}
    {/foreach}
    &nbsp;&nbsp;&nbsp;
    交通:
    {assign var="traffic1" value={$Traffic}}  
    {assign var="traffic2" value=", "|explode:$traffic1}
    {$ttr1=array("站牌","交流道","車站")}
    {foreach $ttr1 as $n => $x}
        <input type="checkbox" id="Traffic[]" name="Traffic[]" value="{$x}" style="margin:5px;" {if $x|in_array:$traffic2}checked="checked"{/if}>{$x}
    {/foreach}<br/>
    <input type="checkbox" id="Security" name="Security" value="0" style="margin:5px;" {if $Security =="0"}checked{/if}>保全管理&nbsp;&nbsp;&nbsp;申請:包租代管計畫<input type="text" id="ApplyRent" name="ApplyRent" value="{$ApplyRent}" style="width:200px;margin:5px;">
    </td>
  </tr>
  <tr>
    <td>座層</td>
    <td>喜：座<input type="text" id="HouseLike" name="HouseLike" value="{$HouseLike}" style="width:100px;margin:5px;"></td>
    <td>忌：座<input type="text" id="HouseDislike" name="HouseDislike" value="{$HouseDislike}" style="width:100px;margin:5px;"></td>
    <td>樓層、數字<input type="text" id="HouseFloor" name="HouseFloor" value="{$HouseFloor}" style="width:100px;margin:5px;"></td>
  </tr>
  <tr>
    <td>期限</td>
    <td><input type="date" id="Period" name="Period" value="{$Period}" style="width:200px;margin:5px;"></td>
    <td colspan="2">帶看時間
    <input type="datetime-local" id="Looking" name="Looking" value="{$Looking}" style="width:250px;margin:5px;"></td>
  </tr>
  <tr>
    <td>家具家電需求</td>
    <td colspan="3"><input type="checkbox" id="All_Options" name="All_Options" value="0" style="margin:5px;" {if $All_Options =="0"}checked{/if}>
      全配&nbsp;&nbsp;&nbsp;床組:(單人 x 
      <input type="text" id="BedSingle" name="BedSingle" value="{$BedSingle}" size="5" style="width:50px;margin:5px;">
      ，雙人 x <input type="text" id="BedDouble" name="BedDouble" value="{$BedDouble}" size="5" style="width:50px;margin:5px;">)&nbsp;&nbsp;&nbsp;書桌椅x<input type="text" id="DeskTable" name="DeskTable" value="{$DeskTable}" size="5" style="width:50px;margin:5px;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="DressingTable" name="DressingTable" value="0" style="margin:5px;" {if $DressingTable =="0"}checked{/if}>梳妝台&nbsp;&nbsp;&nbsp;衣櫥x<input type="text" id="Wardrobe" name="Wardrobe" value="{$Wardrobe}" size="5" style="width:50px;margin:5px;"><input type="checkbox" id="Sofa" name="Sofa" value="0" style="margin:5px;" {if $Sofa =="0"}checked{/if}>沙發<br/>
    <input type="checkbox" id="DinnerTable" name="DinnerTable" value="0" style="margin:5px;" {if $DinnerTable =="0"}checked{/if}>餐桌椅組&nbsp;&nbsp;&nbsp;<input type="checkbox" id="TV" name="TV" value="0" style="margin:5px;" {if $TV =="0"}checked{/if}>電視&nbsp;&nbsp;&nbsp;冷氣x<input type="text" id="Aircondition" name="Aircondition" value="{$Aircondition}" size="5" style="width:50px;margin:5px;">&nbsp;&nbsp;&nbsp;
    洗衣機:
    {assign var="w1" value={$WashMachine}}  
    {assign var="w2" value=", "|explode:$w1}
    {$www1=array("洗","脫","烘")}
    {foreach $www1 as $n => $x}
        <input type="checkbox" id="WashMachine[]" name="WashMachine[]" value="{$x}" style="margin:5px;" {if $x|in_array:$w2}checked="checked"{/if}>{$x}
    {/foreach}&nbsp;&nbsp;&nbsp;<input type="checkbox" id="Fridge" name="Fridge" value="0" style="margin:5px;" {if $Fridge =="0"}checked{/if}>冰箱
    <input type="hidden" id="ss" name="ss" value="1">
    </td>
  </tr>
</table>
<br/>
<HR>
<table width="100%" border="1" class="customer">
  <tr>
    <td colspan="4" style="font-size: 26px;color:#0056b3;margin: 0px auto 5px;height: 1pt;" align="center">帶看服務預約單</td>
  </tr>
  <tr>
    <td>帶看日期</td>
    <td>服務專員</td>
    <td>社區名稱</td>
    <td>備註</td>
  </tr>
  <tr>
    <td><input type="datetime-local" id="slook" name="slook" value="{$LookDate2}" style="width:250px;margin:5px;"></td>
    <td><select id="newag" name="newag" style="margin:5px;">
      <option value="請選擇業務">請選擇業務</option>
      {foreach $newag as $n => $x}
        <option value="{$x.email}"
        {if $AG2 == $x.email}
        selected
        {/if}>{$x.en_name}</option>
      {/foreach}
    </select></td>
    <td><input type="text" id="sarea" name="sarea" value="{$Sarea2}" style="width:150px;margin:5px;"></td>
    <td><input type="text" id="note" name="note" value="{$Note2}" style="width:250px;margin:5px;"></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>是否更新</td>
    <td><input type="radio" id="upda" name="upda" value="0" checked>不更新<input type="radio" id="upda" name="upda" value="1">更新<input type="radio" id="upda" name="upda" value="2">新增&nbsp;&nbsp;&nbsp;
    </td>
  </tr>
</table>
<br/>
<HR>
{* <h1>搜尋條件:租金 <= {$HouseRent} , 格局 {$Room2} ~ {$Room2_1}, 坪數 <= {$InsidePi} *}
{foreach from=$search key=k item=v}
<table width="100%" border="1">
  <tr>
    <td width="30%" ><a href="https://okrent.house/RentHouse?id_rent_house={$v.id_rent_house}" target="_blank"><img src="../{$v.file_url|replace:'%2F':'/'}{$v.filename}" width="301" height="218"></a></td>
    <td class="title big" style="font-size:14px;height: 1pt;">{$v.rent_house_code}&nbsp;&nbsp;&nbsp;{$v.part_address}<br/><br/>
    <span style="font-size: 26px;color:#0056b3;margin: 0px auto 5px;height: 1pt;"><a href="https://okrent.house/RentHouse?id_rent_house={$v.id_rent_house}" target="_blank">{$v.case_name}</a></span><br/><br/>
    <span style="font-size: 20px;color:#0056b3;margin: 0px auto 5px;height: 1pt;">{$v.title}</span><br/><br/>
    <span style="font-size: 15px;height: 1pt;">{$v.room}房&nbsp;&nbsp;&nbsp;{$v.hall}廳&nbsp;&nbsp;&nbsp;{$v.bathroom}衛&nbsp;&nbsp;&nbsp;{$v.ping}坪</span></td>
  </tr>
  {* <tr>
    <td style="font-size: 26px;color:#0056b3;margin: 0px auto 5px;height: 1pt;">{$v.case_name}</td>
  </tr>
  <tr>
    <td style="font-size: 20px;color:#0056b3;margin: 0px auto 5px;height: 1pt;">{$v.title}</td>
  </tr>
  <tr>
    <td style="font-size: 15px;height: 1pt;border-bottom: 1px solid;">{$v.room}房&nbsp;&nbsp;&nbsp;{$v.hall}廳&nbsp;&nbsp;&nbsp;{$v.bathroom}衛&nbsp;&nbsp;&nbsp;{$v.ping}坪</td>
  </tr> *}
</table>
<h1>
{/foreach}
{else}
<table width="100%" border="1" class="customer">
  <tr>
    <td>登入:<span style="color:red;margin:5px;">{$smarty.session.name}</span></td>
    <td align="center"><span style="color:#0056b3;margin:5px;"><h2><b>客戶需求單</b></h2></span></td>
    <td>來源:
    {$input=array("緣故","達人","公佈欄","掃街陌開","保全","網站:591","網站:樂屋","電話開發","拜訪","追蹤","社區拜訪","介紹","名單","房東")}
    <select id="Source" name="Source" style="margin:5px;">
      {foreach $input as $n => $x}
        <option value="{$x}">{$x}</option>
      {/foreach}
    </select>
    </td>
    <td>派單:
    <select id="newag" name="newag" style="margin:5px;">
      <option value="請選擇業務">請選擇業務</option>
      {foreach $newag as $n => $x}
        <option value="{$x.email}">{$x.en_name}</option>
      {/foreach}
    </select>
    </td> 
  </tr>
  <tr>
    <td>姓名:<input type="text" style="width:100px;margin:5px;" id="CustomerName" name="CustomerName" value="{$CR.CustomerName}">
    &nbsp;&nbsp;&nbsp;
    稱謂:
    {$input2=array("先生","小姐")}
    <select id="CustomerTitle" name="CustomerTitle" style="margin:5px;">
    {foreach $input2 as $n => $x}
        <option value="{$x}"
        {if $CustomerTitle == $x}
        selected
        {/if}>{$x}</option>
      {/foreach}</td>
    <td colspan="2">
    手機1:<input type="text" style="width:100px;margin:5px;"  id="CustomerPhone" name="CustomerPhone" value="" maxlength="10" size="10" onKeyUp="value=value.replace(/[^\d]/g,'')"> &nbsp;&nbsp;&nbsp;
    手機2:<input type="text" style="width:100px;margin:5px;"  id="CustomerPhone1" name="CustomerPhone1" value="{$CustomerPhone}" maxlength="10" size="10" onKeyUp="value=value.replace(/[^\d]/g,'')"> &nbsp;&nbsp;&nbsp;
    市話:<input type="text" id="CustomerTel" name="CustomerTel" style="width:100px;margin:5px;" value="" maxlength="10" size="10" onKeyUp="value=value.replace(/[^\d]/g,'')"></td>
    <td>
    職業:<input type="text" style="width:100px;margin:5px;" id="CustomerWork" name="CustomerWork" value="{$CustomerWork}"><br/>
    居住人數:大x<input type="text" id="LivePeople1" name="LivePeople1" style="width:50px;margin:5px;" value="" size="5" onKeyUp="value=value.replace(/[^\d]/g,'')">小x<input type="text" id="LivePeople2" name="LivePeople2" value="" size="5" style="width:50px;margin:5px;" onKeyUp="value=value.replace(/[^\d]/g,'')"><br/>
    關係:<input type="checkbox" id="CustomerRelation[]" name="CustomerRelation[]" value="家庭" style="margin:5px;">家庭<input type="checkbox" id="CustomerRelation[]" name="CustomerRelation[]" value="朋友" style="margin:5px;">朋友<input type="checkbox" id="CustomerRelation[]" name="CustomerRelation[]" value="同事" style="margin:5px;">同事
    </td>
  </tr>
  <tr>
    <td colspan="2">區域:<select name="HouseArea" id="City">
<option value="">請選擇</option>
<option value="台北市">台北市</option>
<option value="基隆市">基隆市</option>
<option value="新北市">新北市</option>
<option value="宜蘭縣">宜蘭縣</option>
<option value="新竹市">新竹市</option>
<option value="新竹縣">新竹縣</option>
<option value="桃園市">桃園市</option>
<option value="苗栗縣">苗栗縣</option>
<option value="台中市">台中市</option>
<option value="台中縣">台中縣</option>
<option value="彰化縣">彰化縣</option>
<option value="南投縣">南投縣</option>
<option value="嘉義市">嘉義市</option>
<option value="嘉義縣">嘉義縣</option>
<option value="雲林縣">雲林縣</option>
<option value="台南市">台南市</option>
<option value="台南縣">台南縣</option>
<option value="高雄市">高雄市</option>
<option value="澎湖縣">澎湖縣</option>
<option value="屏東縣">屏東縣</option>
<option value="台東縣">台東縣</option>
<option value="花蓮縣">花蓮縣</option>
<option value="金門縣">金門縣</option>
<option value="連江縣">連江縣</option>
</select>
縣市
<select name="HouseCity[]" id="Area" multiple="multiple" style="width:100px;height:100px;">
<option value="中正區" c="台北市" z="100">中正區</option>
<option value="大同區" c="台北市" z="103">大同區</option>
<option value="中山區" c="台北市" z="104">中山區</option>
<option value="松山區" c="台北市" z="105">松山區</option>
<option value="大安區" c="台北市" z="106">大安區</option>
<option value="萬華區" c="台北市" z="108">萬華區</option>
<option value="信義區" c="台北市" z="110">信義區</option>
<option value="士林區" c="台北市" z="111">士林區</option>
<option value="北投區" c="台北市" z="112">北投區</option>
<option value="內湖區" c="台北市" z="114">內湖區</option>
<option value="南港區" c="台北市" z="115">南港區</option>
<option value="文山區" c="台北市" z="116">文山區</option>
<option value="仁愛區" c="基隆市" z="200">仁愛區</option>
<option value="信義區" c="基隆市" z="201">信義區</option>
<option value="中正區" c="基隆市" z="202">中正區</option>
<option value="中山區" c="基隆市" z="203">中山區</option>
<option value="安樂區" c="基隆市" z="204">安樂區</option>
<option value="暖暖區" c="基隆市" z="205">暖暖區</option>
<option value="七堵區" c="基隆市" z="206">七堵區</option>
<option value="萬里鄉" c="新北市" z="207">萬里鄉</option>
<option value="金山鄉" c="新北市" z="208">金山鄉</option>
<option value="板橋市" c="新北市" z="220">板橋市</option>
<option value="汐止市" c="新北市" z="221">汐止市</option>
<option value="深坑鄉" c="新北市" z="222">深坑鄉</option>
<option value="石碇鄉" c="新北市" z="223">石碇鄉</option>
<option value="瑞芳鎮" c="新北市" z="224">瑞芳鎮</option>
<option value="平溪鄉" c="新北市" z="226">平溪鄉</option>
<option value="雙溪鄉" c="新北市" z="227">雙溪鄉</option>
<option value="貢寮鄉" c="新北市" z="228">貢寮鄉</option>
<option value="新店市" c="新北市" z="231">新店市</option>
<option value="坪林鄉" c="新北市" z="232">坪林鄉</option>
<option value="烏來鄉" c="新北市" z="233">烏來鄉</option>
<option value="永和市" c="新北市" z="234">永和市</option>
<option value="中和市" c="新北市" z="235">中和市</option>
<option value="土城市" c="新北市" z="236">土城市</option>
<option value="三峽鎮" c="新北市" z="237">三峽鎮</option>
<option value="樹林市" c="新北市" z="238">樹林市</option>
<option value="鶯歌鎮" c="新北市" z="239">鶯歌鎮</option>
<option value="三重市" c="新北市" z="241">三重市</option>
<option value="新莊市" c="新北市" z="242">新莊市</option>
<option value="泰山鄉" c="新北市" z="243">泰山鄉</option>
<option value="林口鄉" c="新北市" z="244">林口鄉</option>
<option value="蘆洲市" c="新北市" z="247">蘆洲市</option>
<option value="五股鄉" c="新北市" z="248">五股鄉</option>
<option value="八里鄉" c="新北市" z="249">八里鄉</option>
<option value="淡水鎮" c="新北市" z="251">淡水鎮</option>
<option value="三芝鄉" c="新北市" z="252">三芝鄉</option>
<option value="石門鄉" c="新北市" z="253">石門鄉</option>
<option value="宜蘭市" c="宜蘭縣" z="260">宜蘭市</option>
<option value="頭城鎮" c="宜蘭縣" z="261">頭城鎮</option>
<option value="礁溪鄉" c="宜蘭縣" z="262">礁溪鄉</option>
<option value="壯圍鄉" c="宜蘭縣" z="263">壯圍鄉</option>
<option value="員山鄉" c="宜蘭縣" z="264">員山鄉</option>
<option value="羅東鎮" c="宜蘭縣" z="265">羅東鎮</option>
<option value="三星鄉" c="宜蘭縣" z="266">三星鄉</option>
<option value="大同鄉" c="宜蘭縣" z="267">大同鄉</option>
<option value="五結鄉" c="宜蘭縣" z="268">五結鄉</option>
<option value="冬山鄉" c="宜蘭縣" z="269">冬山鄉</option>
<option value="蘇澳鎮" c="宜蘭縣" z="270">蘇澳鎮</option>
<option value="南澳鄉" c="宜蘭縣" z="272">南澳鄉</option>
<option value="全區" c="新竹市" z="300">全區</option>
<option value="竹北市" c="新竹縣" z="302">竹北市</option>
<option value="湖口鄉" c="新竹縣" z="303">湖口鄉</option>
<option value="新豐鄉" c="新竹縣" z="304">新豐鄉</option>
<option value="新埔鄉" c="新竹縣" z="305">新埔鄉</option>
<option value="關西鎮" c="新竹縣" z="306">關西鎮</option>
<option value="芎林鄉" c="新竹縣" z="307">芎林鄉</option>
<option value="寶山鄉" c="新竹縣" z="308">寶山鄉</option>
<option value="竹東鎮" c="新竹縣" z="310">竹東鎮</option>
<option value="五峰鄉" c="新竹縣" z="311">五峰鄉</option>
<option value="橫山鄉" c="新竹縣" z="312">橫山鄉</option>
<option value="尖石鄉" c="新竹縣" z="313">尖石鄉</option>
<option value="北埔鄉" c="新竹縣" z="314">北埔鄉</option>
<option value="峨嵋鄉" c="新竹縣" z="315">峨嵋鄉</option>
<option value="中壢區" c="桃園市" z="320">中壢區</option>
<option value="平鎮區" c="桃園市" z="324">平鎮區</option>
<option value="龍潭區" c="桃園市" z="325">龍潭區</option>
<option value="楊梅區" c="桃園市" z="326">楊梅區</option>
<option value="新屋區" c="桃園市" z="327">新屋區</option>
<option value="觀音區" c="桃園市" z="328">觀音區</option>
<option value="桃園市" c="桃園市" z="330">桃園市</option>
<option value="龜山區" c="桃園市" z="333">龜山區</option>
<option value="八德區" c="桃園市" z="334">八德區</option>
<option value="大溪區" c="桃園市" z="335">大溪區</option>
<option value="復興區" c="桃園市" z="336">復興區</option>
<option value="大園區" c="桃園市" z="337">大園區</option>
<option value="蘆竹區" c="桃園市" z="338">蘆竹區</option>
<option value="竹南鎮" c="苗栗縣" z="350">竹南鎮</option>
<option value="頭份鎮" c="苗栗縣" z="351">頭份鎮</option>
<option value="三灣鄉" c="苗栗縣" z="352">三灣鄉</option>
<option value="南庄鄉" c="苗栗縣" z="353">南庄鄉</option>
<option value="獅潭鄉" c="苗栗縣" z="354">獅潭鄉</option>
<option value="後龍鎮" c="苗栗縣" z="356">後龍鎮</option>
<option value="通霄鎮" c="苗栗縣" z="357">通霄鎮</option>
<option value="苑裡鎮" c="苗栗縣" z="358">苑裡鎮</option>
<option value="苗栗市" c="苗栗縣" z="360">苗栗市</option>
<option value="造橋鄉" c="苗栗縣" z="361">造橋鄉</option>
<option value="頭屋鄉" c="苗栗縣" z="362">頭屋鄉</option>
<option value="公館鄉" c="苗栗縣" z="363">公館鄉</option>
<option value="大湖鄉" c="苗栗縣" z="364">大湖鄉</option>
<option value="泰安鄉" c="苗栗縣" z="365">泰安鄉</option>
<option value="鉰鑼鄉" c="苗栗縣" z="366">鉰鑼鄉</option>
<option value="三義鄉" c="苗栗縣" z="367">三義鄉</option>
<option value="西湖鄉" c="苗栗縣" z="368">西湖鄉</option>
<option value="卓蘭鄉" c="苗栗縣" z="369">卓蘭鄉</option>
<option value="中區" c="台中市" z="400">中區</option>
<option value="東區" c="台中市" z="401">東區</option>
<option value="南區" c="台中市" z="402">南區</option>
<option value="西區" c="台中市" z="403">西區</option>
<option value="北區" c="台中市" z="404">北區</option>
<option value="北屯區" c="台中市" z="406">北屯區</option>
<option value="西屯區" c="台中市" z="407">西屯區</option>
<option value="南屯區" c="台中市" z="408">南屯區</option>
<option value="太平市" c="台中縣" z="411">太平市</option>
<option value="大里市" c="台中縣" z="412">大里市</option>
<option value="霧峰鄉" c="台中縣" z="413">霧峰鄉</option>
<option value="烏日鄉" c="台中縣" z="414">烏日鄉</option>
<option value="豐原市" c="台中縣" z="420">豐原市</option>
<option value="后里鄉" c="台中縣" z="421">后里鄉</option>
<option value="石岡鄉" c="台中縣" z="422">石岡鄉</option>
<option value="東勢鎮" c="台中縣" z="423">東勢鎮</option>
<option value="和平鄉" c="台中縣" z="424">和平鄉</option>
<option value="新社鄉" c="9台中縣" z="426">新社鄉</option>
<option value="潭子鄉" c="台中縣" z="427">潭子鄉</option>
<option value="大雅鄉" c="台中縣" z="428">大雅鄉</option>
<option value="神岡鄉" c="台中縣" z="429">神岡鄉</option>
<option value="大肚鄉" c="台中縣" z="432">大肚鄉</option>
<option value="沙鹿鎮" c="台中縣" z="433">沙鹿鎮</option>
<option value="龍井鄉" c="台中縣" z="434">龍井鄉</option>
<option value="梧棲鎮" c="台中縣" z="435">梧棲鎮</option>
<option value="清水鎮" c="台中縣" z="436">清水鎮</option>
<option value="大甲鎮" c="台中縣" z="437">大甲鎮</option>
<option value="外圃鄉" c="台中縣" z="438">外圃鄉</option>
<option value="大安鄉" c="台中縣" z="439">大安鄉</option>
<option value="彰化市" c="彰化縣" z="500">彰化市</option>
<option value="芬園鄉" c="彰化縣" z="502">芬園鄉</option>
<option value="花壇鄉" c="彰化縣" z="503">花壇鄉</option>
<option value="秀水鄉" c="彰化縣" z="504">秀水鄉</option>
<option value="鹿港鎮" c="彰化縣" z="505">鹿港鎮</option>
<option value="福興鄉" c="彰化縣" z="506">福興鄉</option>
<option value="線西鄉" c="彰化縣" z="507">線西鄉</option>
<option value="和美鎮" c="彰化縣" z="508">和美鎮</option>
<option value="伸港鄉" c="彰化縣" z="509">伸港鄉</option>
<option value="員林鎮" c="彰化縣" z="510">員林鎮</option>
<option value="社頭鄉" c="彰化縣" z="511">社頭鄉</option>
<option value="永靖鄉" c="彰化縣" z="5112">永靖鄉</option>
<option value="埔心鄉" c="彰化縣" z="513">埔心鄉</option>
<option value="溪湖鎮" c="彰化縣" z="514">溪湖鎮</option>
<option value="大村鄉" c="彰化縣" z="515">大村鄉</option>
<option value="埔鹽鄉" c="彰化縣" z="516">埔鹽鄉</option>
<option value="田中鎮" c="彰化縣" z="520">田中鎮</option>
<option value="北斗鎮" c="彰化縣" z="521">北斗鎮</option>
<option value="田尾鄉" c="彰化縣" z="522">田尾鄉</option>
<option value="埤頭鄉" c="彰化縣" z="523">埤頭鄉</option>
<option value="溪州鄉" c="彰化縣" z="524">溪州鄉</option>
<option value="竹塘鄉" c="彰化縣" z="525">竹塘鄉</option>
<option value="二林鎮" c="彰化縣" z="526">二林鎮</option>
<option value="大城鄉" c="彰化縣" z="527">大城鄉</option>
<option value="芳苑鄉" c="彰化縣" z="528">芳苑鄉</option>
<option value="二水鄉" c="彰化縣" z="530">二水鄉</option>
<option value="南投市" c="南投縣" z="540">南投市</option>
<option value="中寮鄉" c="南投縣" z="541">中寮鄉</option>
<option value="草屯鎮" c="南投縣" z="542">草屯鎮</option>
<option value="國姓鄉" c="南投縣" z="544">國姓鄉</option>
<option value="埔里鎮" c="南投縣" z="545">埔里鎮</option>
<option value="仁愛鄉" c="南投縣" z="546">仁愛鄉</option>
<option value="名間鄉" c="南投縣" z="551">名間鄉</option>
<option value="集集鄉" c="南投縣" z="552">集集鄉</option>
<option value="水里鄉" c="南投縣" z="553">水里鄉</option>
<option value="魚池鄉" c="南投縣" z="555">魚池鄉</option>
<option value="信義鄉" c="南投縣" z="556">信義鄉</option>
<option value="竹山鎮" c="南投縣" z="557">竹山鎮</option>
<option value="鹿谷鄉" c="南投縣" z="558">鹿谷鄉</option>
<option value="全區" c="嘉義市" z="600">全區</option>
<option value="番路鄉" c="嘉義縣" z="602">番路鄉</option>
<option value="梅山鄉" c="嘉義縣" z="603">梅山鄉</option>
<option value="竹崎鄉" c="嘉義縣" z="604">竹崎鄉</option>
<option value="阿里山" c="嘉義縣" z="605">阿里山</option>
<option value="中埔鄉" c="嘉義縣" z="606">中埔鄉</option>
<option value="大埔鄉" c="嘉義縣" z="607">大埔鄉</option>
<option value="水上鄉" c="嘉義縣" z="608">水上鄉</option>
<option value="鹿草鄉" c="嘉義縣" z="611">鹿草鄉</option>
<option value="太保市" c="嘉義縣" z="612">太保市</option>
<option value="朴子市" c="嘉義縣" z="613">朴子市</option>
<option value="東石鄉" c="嘉義縣" z="614">東石鄉</option>
<option value="六腳鄉" c="嘉義縣" z="615">六腳鄉</option>
<option value="新港鄉" c="嘉義縣" z="616">新港鄉</option>
<option value="民雄鄉" c="嘉義縣" z="621">民雄鄉</option>
<option value="大林鎮" c="嘉義縣" z="622">大林鎮</option>
<option value="漢口鄉" c="嘉義縣" z="623">漢口鄉</option>
<option value="義竹鄉" c="嘉義縣" z="624">義竹鄉</option>
<option value="布袋鎮" c="嘉義縣" z="625">布袋鎮</option>
<option value="斗南市" c="雲林縣" z="630">斗南市</option>
<option value="大埤鄉" c="雲林縣" z="631">大埤鄉</option>
<option value="虎尾鎮" c="雲林縣" z="632">虎尾鎮</option>
<option value="土庫鎮" c="雲林縣" z="633">土庫鎮</option>
<option value="褒忠鄉" c="雲林縣" z="634">褒忠鄉</option>
<option value="東勢鄉" c="雲林縣" z="635">東勢鄉</option>
<option value="台西鄉" c="雲林縣" z="636">台西鄉</option>
<option value="崙背鄉" c="雲林縣" z="637">崙背鄉</option>
<option value="麥寮鄉" c="雲林縣" z="638">麥寮鄉</option>
<option value="斗六市" c="雲林縣" z="640">斗六市</option>
<option value="林內鄉" c="雲林縣" z="643">林內鄉</option>
<option value="古坑鄉" c="雲林縣" z="646">古坑鄉</option>
<option value="莿桐鄉" c="雲林縣" z="647">莿桐鄉</option>
<option value="西螺鎮" c="雲林縣" z="648">西螺鎮</option>
<option value="二崙鄉" c="雲林縣" z="649">二崙鄉</option>
<option value="北港鎮" c="雲林縣" z="651">北港鎮</option>
<option value="水林鄉" c="雲林縣" z="652">水林鄉</option>
<option value="口湖鄉" c="雲林縣" z="653">口湖鄉</option>
<option value="四湖鄉" c="雲林縣" z="654">四湖鄉</option>
<option value="元長鄉" c="雲林縣" z="655">元長鄉</option>
<option value="中區" c="台南市" z="700">中區</option>
<option value="東區" c="台南市" z="701">東區</option>
<option value="南區" c="台南市" z="702">南區</option>
<option value="西區" c="台南市" z="703">西區</option>
<option value="北區" c="台南市" z="704">北區</option>
<option value="安平區" c="台南市" z="708">安平區</option>
<option value="安南區" c="台南市" z="709">安南區</option>
<option value="永康市" c="台南縣" z="710">永康市</option>
<option value="歸仁鄉" c="台南縣" z="711">歸仁鄉</option>
<option value="新化鎮" c="台南縣" z="712">新化鎮</option>
<option value="左鎮鄉" c="台南縣" z="713">左鎮鄉</option>
<option value="玉井鄉" c="台南縣" z="714">玉井鄉</option>
<option value="楠西鄉" c="台南縣" z="715">楠西鄉</option>
<option value="南化鄉" c="台南縣" z="716">南化鄉</option>
<option value="仁德鄉" c="台南縣" z="717">仁德鄉</option>
<option value="關廟鄉" c="台南縣" z="718">關廟鄉</option>
<option value="龍崎鄉" c="台南縣" z="719">龍崎鄉</option>
<option value="官田鄉" c="台南縣" z="720">官田鄉</option>
<option value="麻豆鎮" c="台南縣" z="721">麻豆鎮</option>
<option value="佳里鎮" c="台南縣" z="722">佳里鎮</option>
<option value="西港鄉" c="台南縣" z="723">西港鄉</option>
<option value="七股鄉" c="台南縣" z="724">七股鄉</option>
<option value="將軍鄉" c="台南縣" z="725">將軍鄉</option>
<option value="學甲鎮" c="台南縣" z="726">學甲鎮</option>
<option value="北門鄉" c="台南縣" z="727">北門鄉</option>
<option value="新營市" c="台南縣" z="730">新營市</option>
<option value="後壁鄉" c="台南縣" z="731">後壁鄉</option>
<option value="白河鎮" c="台南縣" z="732">白河鎮</option>
<option value="東山鄉" c="台南縣" z="733">東山鄉</option>
<option value="六甲鄉" c="台南縣" z="734">六甲鄉</option>
<option value="下營鄉" c="台南縣" z="735">下營鄉</option>
<option value="柳營鄉" c="台南縣" z="736">柳營鄉</option>
<option value="鹽水鎮" c="台南縣" z="737">鹽水鎮</option>
<option value="善化鎮" c="台南縣" z="741">善化鎮</option>
<option value="大內鄉" c="台南縣" z="742">大內鄉</option>
<option value="山上鄉" c="台南縣" z="743">山上鄉</option>
<option value="新市鄉" c="台南縣" z="744">新市鄉</option>
<option value="安定鄉" c="台南縣" z="745">安定鄉</option>
<option value="新興區" c="高雄市" z="800">新興區</option>
<option value="前金區" c="高雄市" z="801">前金區</option>
<option value="苓雅區" c="高雄市" z="802">苓雅區</option>
<option value="鹽埕區" c="高雄市" z="803">鹽埕區</option>
<option value="鼓山區" c="高雄市" z="804">鼓山區</option>
<option value="旗津區" c="高雄市" z="805">旗津區</option>
<option value="前鎮區" c="高雄市" z="806">前鎮區</option>
<option value="三民區" c="高雄市" z="807">三民區</option>
<option value="楠梓區" c="高雄市" z="811">楠梓區</option>
<option value="小港區" c="高雄市" z="812">小港區</option>
<option value="左營區" c="高雄市" z="813">左營區</option>
<option value="仁武區" c="高雄市" z="814">仁武區</option>
<option value="大社區" c="高雄市" z="815">大社區</option>
<option value="岡山區" c="高雄市" z="820">岡山區</option>
<option value="路竹區" c="高雄市" z="821">路竹區</option>
<option value="阿蓮區" c="高雄市" z="822">阿蓮區</option>
<option value="田寮區" c="高雄市" z="823">田寮區</option>
<option value="燕巢區" c="高雄市" z="824">燕巢區</option>
<option value="橋頭區" c="高雄市" z="825">橋頭區</option>
<option value="梓官區" c="高雄市" z="826">梓官區</option>
<option value="彌陀區" c="高雄市" z="827">彌陀區</option>
<option value="永安區" c="高雄市" z="828">永安區</option>
<option value="湖內區" c="高雄市" z="829">湖內區</option>
<option value="鳳山區" c="高雄市" z="830">鳳山區</option>
<option value="大寮區" c="高雄市" z="831">大寮區</option>
<option value="林園區" c="高雄市" z="832">林園區</option>
<option value="鳥松區" c="高雄市" z="833">鳥松區</option>
<option value="大樹區" c="高雄市" z="840">大樹區</option>
<option value="旗山區" c="高雄市" z="842">旗山區</option>
<option value="美濃區" c="高雄市" z="843">美濃區</option>
<option value="六龜區" c="高雄市" z="844">六龜區</option>
<option value="內門區" c="高雄市" z="845">內門區</option>
<option value="杉林區" c="高雄市" z="846">杉林區</option>
<option value="甲仙區" c="高雄市" z="847">甲仙區</option>
<option value="桃源區" c="高雄市" z="848">桃源區</option>
<option value="三民區" c="高雄市" z="849">三民區</option>
<option value="茂林區" c="高雄市" z="851">茂林區</option>
<option value="茄萣區" c="高雄市" z="852">茄萣區</option>
<option value="馬公市" c="澎湖縣" z="880">馬公市</option>
<option value="西嶼鄉" c="澎湖縣" z="881">西嶼鄉</option>
<option value="望安鄉" c="澎湖縣" z="882">望安鄉</option>
<option value="七美鄉" c="澎湖縣" z="883">七美鄉</option>
<option value="白沙鄉" c="澎湖縣" z="884">白沙鄉</option>
<option value="湖西鄉" c="澎湖縣" z="885">湖西鄉</option>
<option value="屏東市" c="屏東縣" z="900">屏東市</option>
<option value="三地門" c="屏東縣" z="901">三地門</option>
<option value="霧台鄉" c="屏東縣" z="902">霧台鄉</option>
<option value="瑪家鄉" c="屏東縣" z="903">瑪家鄉</option>
<option value="九如鄉" c="屏東縣" z="904">九如鄉</option>
<option value="里港鄉" c="屏東縣" z="905">里港鄉</option>
<option value="高樹鄉" c="屏東縣" z="906">高樹鄉</option>
<option value="鹽埔鄉" c="屏東縣" z="907">鹽埔鄉</option>
<option value="長治鄉" c="屏東縣" z="908">長治鄉</option>
<option value="麟洛鄉" c="屏東縣" z="909">麟洛鄉</option>
<option value="竹田鄉" c="屏東縣" z="911">竹田鄉</option>
<option value="內埔鄉" c="屏東縣" z="912">內埔鄉</option>
<option value="萬丹鄉" c="屏東縣" z="913">萬丹鄉</option>
<option value="潮州鎮" c="屏東縣" z="920">潮州鎮</option>
<option value="泰武鄉" c="屏東縣" z="921">泰武鄉</option>
<option value="來義鄉" c="屏東縣" z="922">來義鄉</option>
<option value="萬巒鄉" c="屏東縣" z="923">萬巒鄉</option>
<option value="嵌頂鄉" c="屏東縣" z="924">嵌頂鄉</option>
<option value="新埤鄉" c="屏東縣" z="925">新埤鄉</option>
<option value="南州鄉" c="屏東縣" z="926">南州鄉</option>
<option value="林邊鄉" c="屏東縣" z="927">林邊鄉</option>
<option value="東港鎮" c="屏東縣" z="928">東港鎮</option>
<option value="琉球鄉" c="屏東縣" z="929">琉球鄉</option>
<option value="佳冬鄉" c="屏東縣" z="931">佳冬鄉</option>
<option value="新園鄉" c="屏東縣" z="932">新園鄉</option>
<option value="枋寮鄉" c="屏東縣" z="940">枋寮鄉</option>
<option value="枋山鄉" c="屏東縣" z="941">枋山鄉</option>
<option value="春日鄉" c="屏東縣" z="942">春日鄉</option>
<option value="獅子鄉" c="屏東縣" z="943">獅子鄉</option>
<option value="車城鄉" c="屏東縣" z="944">車城鄉</option>
<option value="牡丹鄉" c="屏東縣" z="945">牡丹鄉</option>
<option value="恆春鎮" c="屏東縣" z="946">恆春鎮</option>
<option value="滿州鄉" c="屏東縣" z="947">滿州鄉</option>
<option value="台東市" c="台東縣" z="950">台東市</option>
<option value="台東市" c="台東縣" z="951">台東市</option>
<option value="蘭嶼鄉" c="台東縣" z="952">蘭嶼鄉</option>
<option value="延平鄉" c="台東縣" z="953">延平鄉</option>
<option value="卑南鄉" c="台東縣" z="954">卑南鄉</option>
<option value="鹿野鄉" c="台東縣" z="955">鹿野鄉</option>
<option value="關山鎮" c="台東縣" z="956">關山鎮</option>
<option value="海端鄉" c="台東縣" z="957">海端鄉</option>
<option value="池上鄉" c="台東縣" z="958">池上鄉</option>
<option value="東河鄉" c="台東縣" z="959">東河鄉</option>
<option value="成氐燡" c="台東縣" z="961">成氐燡</option>
<option value="長濱鄉" c="台東縣" z="962">長濱鄉</option>
<option value="太麻里" c="台東縣" z="963">太麻里</option>
<option value="金峰鄉" c="台東縣" z="964">金峰鄉</option>
<option value="大武鄉" c="台東縣" z="965">大武鄉</option>
<option value="達仁鄉" c="台東縣" z="966">達仁鄉</option>
<option value="花蓮市" c="花蓮縣" z="970">花蓮市</option>
<option value="新城鄉" c="花蓮縣" z="971">新城鄉</option>
<option value="秀林鄉" c="花蓮縣" z="972">秀林鄉</option>
<option value="吉安鄉" c="花蓮縣" z="973">吉安鄉</option>
<option value="壽豐鄉" c="花蓮縣" z="974">壽豐鄉</option>
<option value="鳳林鎮" c="花蓮縣" z="975">鳳林鎮</option>
<option value="光復鄉" c="花蓮縣" z="976">光復鄉</option>
<option value="豐濱鄉" c="花蓮縣" z="977">豐濱鄉</option>
<option value="瑞穗鄉" c="花蓮縣" z="978">瑞穗鄉</option>
<option value="萬榮鄉" c="花蓮縣" z="979">萬榮鄉</option>
<option value="玉里鎮" c="花蓮縣" z="981">玉里鎮</option>
<option value="卓溪鄉" c="花蓮縣" z="982">卓溪鄉</option>
<option value="富里鄉" c="花蓮縣" z="983">富里鄉</option>
<option value="金沙鎮" c="金門縣" z="890">金沙鎮</option>
<option value="金湖鎮" c="金門縣" z="891">金湖鎮</option>
<option value="金寧鄉" c="金門縣" z="892">金寧鄉</option>
<option value="金城鎮" c="金門縣" z="893">金城鎮</option>
<option value="烈嶼鄉" c="金門縣" z="894">烈嶼鄉</option>
<option value="烏坵鄉" c="金門縣" z="896">烏坵鄉</option>
<option value="南竿鄉" c="連江縣" z="209">南竿鄉</option>
<option value="北竿鄉" c="連江縣" z="210">北竿鄉</option>
<option value="莒光鄉" c="連江縣" z="211">莒光鄉</option>
<option value="東引" c="連江縣" z="212">東引</option>
</select></td>
    <td colspan="2">格局:
    {$roo2=array("","2房","3房","4房","5房")}
    <select id="Room2" name="Room2" style="margin:5px;" required>
    	{foreach $roo2 as $n => $x}
        <option value="{$x}">{$x}</option>
      {/foreach}
    </select>
    &nbsp; ~ &nbsp;
    {$roo2_1=array("","2房","3房","4房","5房")}
    <select id="Room2_1" name="Room2_1" style="margin:5px;" required>
    	{foreach $roo2_1 as $n => $x}
        <option value="{$x}">{$x}</option>
      {/foreach}
    </select>
    &nbsp; / &nbsp;
    {$roo3=array("","1廳","2廳","3廳","4廳")}
    <select id="Room3" name="Room3" style="margin:5px;">
    	{foreach $roo3 as $n => $x}
        <option value="{$x}">{$x}</option>
      {/foreach}
    </select>
    &nbsp; / &nbsp;
    {$roo4=array("","1衛","2衛","3衛","4衛","5衛")}
    <select id="Room4" name="Room4" style="margin:5px;">
    	{foreach $roo4 as $n => $x}
        <option value="{$x}">{$x}</option>
      {/foreach}
    </select><br/>
    套房:<input type="checkbox" id="Room[]" name="Room[]" value="分租" style="margin:5px;">分租<input type="checkbox" id="Room[]" name="Room[]" value="OPEN" style="margin:5px;">OPEN<input type="checkbox" id="Room[]" name="Room[]" value="隔間" style="margin:5px;">隔間
</td>
  </tr>
  <tr>
    <td>類別預算</td>
    <td colspan="3">
      月-月租:
      <input type="text" id="HouseRent" name="HouseRent" value="" size="15" style="width:150px;margin:5px;"/><br/> 
      <input type="checkbox" id="Investment" name="Investment" value="投資" style="margin:5px;">投資 &nbsp;&nbsp;&nbsp; 買-總價:
      <input type="text" id="BuyPrice" name="BuyPrice" value="" size="15" style="width:100px;margin:5px;"/>&nbsp;&nbsp;&nbsp; 自備:<input type="text" id="DownPayment" name="DownPayment" value="" size="15" style="width:100px;margin:5px;"/>&nbsp;&nbsp;&nbsp;貸款:<input type="text" id="Loan" name="Loan" value="" size="15" style="width:100px;margin:5px;"/>&nbsp;&nbsp;&nbsp;
    </td>
  </tr>
  <tr>
    <td>類別格局</td>
    <td colspan="3">
    類別:<input type="checkbox" id="Pattern[]" name="Pattern[]" value="華廈" style="margin:5px;">華廈<input type="checkbox" id="Pattern[]" name="Pattern[]" value="公寓" style="margin:5px;">公寓<input type="checkbox" id="Pattern[]" name="Pattern[]" value="透天" style="margin:5px;">透天<input type="checkbox" id="Pattern[]" name="Pattern[]" value="別墅" style="margin:5px;">別墅<input type="checkbox" id="Pattern[]" name="Pattern[]" value="店面" style="margin:5px;">店面<input type="checkbox" id="Pattern[]" name="Pattern[]" value="辦公室" style="margin:5px;">辦公室<input type="checkbox" id="Pattern[]" name="Pattern[]" value="廠房" style="margin:5px;">廠房<input type="checkbox" id="Pattern[]" name="Pattern[]" value="住辦" style="margin:5px;">住辦<input type="checkbox" id="Pattern[]" name="Pattern[]" value="土地" style="margin:5px;">土地
    </td>
  </tr>
  <tr>
    <td>需求</td>
    <td colspan="3">室內坪數:<input type="text" id="InsidePi" name="InsidePi" value="" style="width:50px;margin:5px;" required>&nbsp;&nbsp;&nbsp;屋齡(<input type="text" id="HouseOld" name="HouseOld" value="" size="5" style="width:50px;margin:5px;">
      年↓)&nbsp;&nbsp;&nbsp;可炊:<input type="checkbox" id="HouseCook" name="HouseCook" value="瓦斯" style="margin:5px;">瓦斯<input type="checkbox" id="HouseCook" name="HouseCook" value="電磁" style="margin:5px;">電磁&nbsp;&nbsp;&nbsp;陽台:<input type="checkbox" id="HouseBalcony[]" name="HouseBalcony[]" value="前陽台" style="margin:5px;">前陽台<input type="checkbox" id="HouseCook[]" name="HouseCook[]" value="後陽台" style="margin:5px;">後陽台<br/>
    <input type="checkbox" id="Decorate" name="Decorate" value="美裝潢" style="margin:5px;">美裝潢&nbsp;&nbsp;&nbsp;其他:<input type="checkbox" id="Other[]" name="Other[]" value="遷籍" style="margin:5px;">遷籍<input type="checkbox" id="Other[]" name="Other[]" value="報稅" style="margin:5px;">報稅<input type="checkbox" id="Other[]" name="Other[]" value="輔助" style="margin:5px;">補助<input type="checkbox" id="Other[]" name="Other[]" value="(營/廠)登記" style="margin:5px;">(營/廠)登記&nbsp;&nbsp;&nbsp;車位:平面 X <input type="text" id="CarPlace" name="CarPlace" value="" size="5" style="width:50px;margin:5px;">
    個&nbsp;&nbsp;&nbsp;可寵:<input type="checkbox" id="Pet[]" name="Pet[]" value="貓" style="margin:5px;">貓<input type="checkbox" id="Pet[]" name="Pet[]" value="狗" style="margin:5px;">狗<br/>
    近學區:
    <input type="text" id="School" name="School" value="" style="width:100px;margin:5px;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="ShortRent" name="ShortRent" value="短租" style="margin:5px;">短租&nbsp;&nbsp;&nbsp;
    休閒設施:
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="游泳池" size="20" style="margin:5px;">游泳池
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="健身房" size="20" style="margin:5px;">健身房
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="視聽中心" size="20" style="margin:5px;">視聽中心
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="KTV" size="20" style="margin:5px;">KTV
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="兒童遊戲區" size="20" style="margin:5px;">兒童遊戲區
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="SPA" size="20" style="margin:5px;">SPA
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="球室" size="20" style="margin:5px;">球室
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="閱覽室" size="20" style="margin:5px;">閱覽室
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="交誼廳" size="20" style="margin:5px;">交誼廳
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="咖啡吧" size="20" style="margin:5px;">咖啡吧
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="中庭花園" size="20" style="margin:5px;">中庭花園
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="會議室" size="20" style="margin:5px;">會議室
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="接待大廳" size="20" style="margin:5px;">接待大廳
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="公用陽台" size="20" style="margin:5px;">公用陽台
    <input type="checkbox" id="Lestime[]" name="Lestime[]" value="游泳池" size="20" style="margin:5px;">游泳池
    交通:<input type="checkbox" id="Traffic[]" name="Traffic[]" value="站牌" style="margin:5px;">站牌<input type="checkbox" id="Traffic[]" name="Traffic[]" value="交流道" style="margin:5px;">交流道<input type="checkbox" id="Traffic[]" name="Traffic[]" value="車站" style="margin:5px;">車站<br/>
    <input type="checkbox" id="Security" name="Security" value="0" style="margin:5px;">保全管理&nbsp;&nbsp;&nbsp;申請:包租代管計畫<input type="text" id="ApplyRent" name="ApplyRent" value="" style="width:200px;margin:5px;">
    </td>
  </tr>
  <tr>
    <td>座層</td>
    <td>喜：座<input type="text" id="HouseLike" name="HouseLike" value="" style="width:100px;margin:5px;"></td>
    <td>忌：座<input type="text" id="HouseDislike" name="HouseDislike" value="" style="width:100px;margin:5px;"></td>
    <td>樓層、數字<input type="text" id="HouseFloor" name="HouseFloor" value="" style="width:100px;margin:5px;"></td>
  </tr>
  <tr>
    <td>期限</td>
    <td><input type="date" id="Period" name="Period" value="" style="width:200px;margin:5px;"></td>
    <td colspan="2">帶看時間
    <input type="datetime-local" id="Looking" name="Looking" value="" style="width:250px;margin:5px;"></td>
  </tr>
  <tr>
    <td>家具家電需求</td>
    <td colspan="3"><input type="checkbox" id="All_Options" name="All_Options" value="0" style="margin:5px;">
      全配&nbsp;&nbsp;&nbsp;床組:(單人 x 
      <input type="text" id="BedSingle" name="BedSingle" value="" size="5" style="width:50px;margin:5px;">
      ，雙人 x <input type="text" id="BedDouble" name="BedDouble" value="" size="5" style="width:50px;margin:5px;">)&nbsp;&nbsp;&nbsp;書桌椅x<input type="text" id="DeskTable" name="DeskTable" value="" size="5" style="width:50px;margin:5px;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="DressingTable" name="DressingTable" value="0" style="margin:5px;">梳妝台&nbsp;&nbsp;&nbsp;衣櫥x<input type="text" id="Wardrobe" name="Wardrobe" value="" size="5" style="width:50px;margin:5px;"><input type="checkbox" id="Sofa" name="Sofa" value="0" style="margin:5px;">沙發<br/>
    <input type="checkbox" id="DinnerTable" name="DinnerTable" value="0" style="margin:5px;">餐桌椅組&nbsp;&nbsp;&nbsp;<input type="checkbox" id="TV" name="TV" value="0" style="margin:5px;">電視&nbsp;&nbsp;&nbsp;冷氣x<input type="text" id="Aircondition" name="Aircondition" value="" size="5" style="width:50px;margin:5px;">&nbsp;&nbsp;&nbsp;洗衣機:<input type="checkbox" id="WashMachine[]" name="WashMachine[]" value="洗" style="margin:5px;">洗<input type="checkbox" id="WashMachine[]" name="WashMachine[]" value="脫" style="margin:5px;">脫<input type="checkbox" id="WashMachine[]" name="WashMachine[]" value="烘" style="margin:5px;">烘&nbsp;&nbsp;&nbsp;<input type="checkbox" id="Fridge" name="Fridge" value="0" style="margin:5px;">冰箱
    <input type="hidden" id="ss" name="ss" value="1">
    </td>
  </tr>
</table>
{/if}
