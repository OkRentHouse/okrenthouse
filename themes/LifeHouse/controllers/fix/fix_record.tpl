<nav class="nav navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            {if !$fix_record_row eq ''}
                {foreach from=$fix_record_row key=k item=v}
                    {if $k==0}
                        <li class="active"><a data-toggle="tab" href="#fix_record_0" aria-expanded="true" class="fix_record_number">檢修記錄</a></li>
                    {else}
                        <li class=""><a data-toggle="tab" href="#fix_record_{$k}"  class="fix_record_number">檢修記錄</a></li>
                    {/if}
                {/foreach}
            {else}
                <li class="active"><a data-toggle="tab" href="#fix_record_0" class="fix_record_number">檢修記錄</a></li>
            {/if}
        </ul>
        <button type="button" class="btn btn-primary" style="position: relative; display: block; padding: 10px 15px;" onclick="fix_record_add(this)">新增檢修紀錄</button>
    </div>
</nav>

<div class="tab-content" id="nav_body">
{if !$fix_record_row eq ''}
    {foreach from=$fix_record_row key=k item=v}
        <div class="tab-pane fade {if $k==0}in active{/if}" id="fix_record_{$k}">
            <input class="form-control hidden" type="text" name="id_fix_record[]" id="" value="{$v.id_fix_record}" >
            <div class="form-group col-lg-6">
                <label class="control-label col-lg-6" for="">發派日期</label>
                <div class="form-group col-lg-6" id="">
                    <input class="form-control" type="date" name="dispatch_date[]" id="" value="{$v.dispatch_date}" >
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label class="control-label col-lg-3" for="">發派業務</label>
                <div class="form-group col-lg-6" id="">
                    <input class="form-control" type="text" name="dispatch_ag[]" id="" value="{$v.dispatch_ag}" maxlength="20">
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="control-label col-lg-3 " for="">發派廠商</label>
                <div class="col-lg-6" id="">
                    <input class="form-control" type="text" name="dispatch_vendor[]" id="" value="{$v.dispatch_vendor}" maxlength="32">
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="control-label col-lg-3 " for="">廠商回報</label>
                <div class="col-lg-6" id="">
                    <input class="form-control" type="text" name="vendor_return[]" id="" value="{$v.vendor_return}" maxlength="64">
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="control-label col-lg-3 " for="">回報屋主</label>
                <div class="col-lg-6" id="">
                    <input class="form-control" type="text" name="return_landlord[]" id="" value="{$v.return_landlord}" maxlength="64">
                </div>
            </div>

            <div class="form-group col-lg-12">
                <label class="control-label col-lg-3 " for="">付款</label>
                <div class="form-group col-lg-9" id="">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">報價</span><input class="form-control" type="number" name="quoted_price[]" value="{$v.quoted_price}">
                        <span class="input-group-addon">議價</span><input class="form-control" type="number" name="negotiated_price[]" value="{$v.negotiated_price}">
                        <span class="input-group-addon">淨賺</span><input class="form-control" type="number" name="net_price[]" value="{$v.net_price}">
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">付款方式</span>
                        <select class="form-control" name="pay[]">
                            <option value="" >選擇付款方式</option>
                            <option value="0" {if ($v.pay==0 && $v.pay !='')} selected="selected" {/if}>租客負擔</option>
                            <option value="1" {if $v.pay==1} selected="selected" {/if}>租客租金抵扣</option>
                            <option value="2" {if $v.pay==2} selected="selected" {/if}>房東轉匯至專戶</option>
                            <option value="3" {if $v.pay==3} selected="selected" {/if}>自代收租金抵扣</option>
                        </select>
                        <span class="input-group-addon">付款額外資料</span>
                        <input class="form-control" type="text" name="pay_additional_data[]" value="{$v.pay_additional_data}" maxlength="64">
                    </div>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="control-label col-lg-6" for="">報銷日期</label>
                <div class="form-group col-lg-6" id="">
                    <input class="form-control" type="date" name="reimbursement_date[]" id="" value="{$v.reimbursement_date}" >
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label class="control-label col-lg-3" for="">報銷業務</label>
                <div class="form-group col-lg-6" id="">
                    <input class="form-control" type="text" name="reimbursement_ag[]" id="" value="{$v.reimbursement_ag}" maxlength="20">
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="control-label col-lg-3 " for="">報銷紀錄</label>
                <div class="col-lg-6" id="">
                    <input class="form-control" type="text" name="reimbursement_record[]" id="" value="{$v.reimbursement_record}" maxlength="64">
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label class="control-label col-lg-6" for="">建立時間</label>
                <div class="form-group col-lg-6" id="">
                    <input class="form-control" type="text" name="create_time[]" id="" value="{$v.create_time}" disabled="disabled" >
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label class="control-label col-lg-3" for="">建立人</label>
                <div class="form-group col-lg-6" id="">
                    <input class="form-control" type="text" name="id_admin[]" id="" value="{$v.id_admin}" disabled="disabled">
                </div>
            </div>
        </div>
    {/foreach}
{else}
    <div class="tab-pane fade in active" id="fix_record_0">
        <input class="form-control hidden" type="text" name="id_fix_record[]" id="" value="" >
        <div class="form-group col-lg-6">
            <label class="control-label col-lg-6" for="">發派日期</label>
            <div class="form-group col-lg-6" id="">
                <input class="form-control" type="date" name="dispatch_date[]" id="" value="" >
            </div>
        </div>
        <div class="form-group col-lg-6">
            <label class="control-label col-lg-3" for="">發派業務</label>
            <div class="form-group col-lg-6" id="">
                <input class="form-control" type="text" name="dispatch_ag[]" id="" value="" maxlength="20">
            </div>
        </div>
        <div class="form-group col-lg-12">
            <label class="control-label col-lg-3 " for="">發派廠商</label>
            <div class="col-lg-6" id="">
                <input class="form-control" type="text" name="dispatch_vendor[]" id="" value="" maxlength="32">
            </div>
        </div>
        <div class="form-group col-lg-12">
            <label class="control-label col-lg-3 " for="">廠商回報</label>
            <div class="col-lg-6" id="">
                <input class="form-control" type="text" name="vendor_return[]" id="" value="" maxlength="64">
            </div>
        </div>
        <div class="form-group col-lg-12">
            <label class="control-label col-lg-3 " for="">回報屋主</label>
            <div class="col-lg-6" id="">
                <input class="form-control" type="text" name="return_landlord[]" id="" value="" maxlength="64">
            </div>
        </div>

        <div class="form-group col-lg-12">
            <label class="control-label col-lg-3 " for="">付款</label>
            <div class="form-group col-lg-9" id="">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">報價</span><input class="form-control" type="number" name="quoted_price[]" value="">
                    <span class="input-group-addon">議價</span><input class="form-control" type="number" name="negotiated_price[]" value="">
                    <span class="input-group-addon">淨賺</span><input class="form-control" type="number" name="net_price[]" value="">
                </div>
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">付款方式</span>
                    <select class="form-control" name="pay[]">
                        <option value="">選擇付款方式</option>
                        <option value="0">租客負擔</option>
                        <option value="1">租客租金抵扣</option>
                        <option value="2">房東轉匯至專戶</option>
                        <option value="3">自代收租金抵扣</option>
                    </select>
                    <span class="input-group-addon">付款額外資料</span>
                    <input class="form-control" type="text" name="pay_additional_data[]" value="" maxlength="64">
                </div>
            </div>
        </div>

        <div class="form-group col-lg-6">
            <label class="control-label col-lg-6" for="">報銷日期</label>
            <div class="form-group col-lg-6" id="">
                <input class="form-control" type="date" name="reimbursement_date[]" id="" value="" >
            </div>
        </div>
        <div class="form-group col-lg-6">
            <label class="control-label col-lg-3" for="">報銷業務</label>
            <div class="form-group col-lg-6" id="">
                <input class="form-control" type="text" name="reimbursement_ag[]" id="" value="" maxlength="20">
            </div>
        </div>
        <div class="form-group col-lg-12">
            <label class="control-label col-lg-3 " for="">報銷紀錄</label>
            <div class="col-lg-6" id="">
                <input class="form-control" type="text" name="reimbursement_record[]" id="" value="" maxlength="64">
            </div>
        </div>
    </div>
{/if}
</div>

{*<input class="form-control hidden" type="text" name="id_fix_record[]" id="" value="" >*}
{*<div class="form-group col-lg-6">*}
{*    <label class="control-label col-lg-6" for="">發派日期</label>*}
{*    <div class="form-group col-lg-6" id="">*}
{*        <input class="form-control" type="date" name="dispatch_date[]" id="" value="" >*}
{*    </div>*}
{*</div>*}
{*<div class="form-group col-lg-6">*}
{*    <label class="control-label col-lg-3" for="">發派業務</label>*}
{*    <div class="form-group col-lg-7" id="">*}
{*        <input class="form-control" type="text" name="dispatch_ag[]" id="" value="" maxlength="20">*}
{*    </div>*}
{*</div>*}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3 " for="">發派廠商</label>*}
{*    <div class="col-lg-6" id="">*}
{*        <input class="form-control" type="text" name="dispatch_vendor[]" id="" value="" maxlength="32">*}
{*    </div>*}
{*</div>*}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3 " for="">廠商回報</label>*}
{*    <div class="col-lg-6" id="">*}
{*        <input class="form-control" type="text" name="vendor_return[]" id="" value="" maxlength="64">*}
{*    </div>*}
{*</div>*}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3 " for="">回報屋主</label>*}
{*    <div class="col-lg-6" id="">*}
{*        <input class="form-control" type="text" name="return_landlord[]" id="" value="" maxlength="64">*}
{*    </div>*}
{*</div>*}

{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3 " for="">付款</label>*}
{*    <div class="form-group col-lg-9" id="">*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">報價</span><input class="form-control" type="number" name="quoted_price[]" value="">*}
{*            <span class="input-group-addon">議價</span><input class="form-control" type="number" name="negotiated_price[]" value="">*}
{*            <span class="input-group-addon">淨賺</span><input class="form-control" type="number" name="net_price[]" value="">*}
{*        </div>*}
{*        <div class="input-group fixed-width-lg">*}
{*            <span class="input-group-addon">付款方式</span>*}
{*            <select class="form-control" name="pay[]">*}
{*                <option value="">選擇付款方式</option>*}
{*                <option value="0">租客負擔</option>*}
{*                <option value="1">租客租金抵扣</option>*}
{*                <option value="2">房東轉匯至專戶</option>*}
{*                <option value="3">自代收租金抵扣</option>*}
{*            </select>*}
{*            <span class="input-group-addon">付款額外資料</span>*}
{*            <input class="form-control" type="text" name="pay_additional_data[]" value="" maxlength="64">*}
{*        </div>*}
{*    </div>*}
{*</div>*}

{*<div class="form-group col-lg-6">*}
{*    <label class="control-label col-lg-6" for="">報銷日期</label>*}
{*    <div class="form-group col-lg-6" id="">*}
{*        <input class="form-control" type="date" name="reimbursement_date[]" id="" value="" >*}
{*    </div>*}
{*</div>*}
{*<div class="form-group col-lg-6">*}
{*    <label class="control-label col-lg-3" for="">報銷業務</label>*}
{*    <div class="form-group col-lg-7" id="">*}
{*        <input class="form-control" type="text" name="reimbursement_ag[]" id="" value="" maxlength="20">*}
{*    </div>*}
{*</div>*}
{*<div class="form-group col-lg-12">*}
{*    <label class="control-label col-lg-3 " for="">發派廠商</label>*}
{*    <div class="col-lg-6" id="">*}
{*        <input class="form-control" type="text" name="reimbursement_record[]" id="" value="" maxlength="64">*}
{*    </div>*}
{*</div>*}
