{foreach from=$tpl_fix key=k item=v}
    {if !empty($v.html)}

    {elseif $v.type=='hidden'}
        <input type="hidden" name="{$v.name}" id="{$v.id}" value="{if isset($fix_row.$k)}{$fix_row.$k} {elseif $main_index==$v.name}{$smarty.get.$main_index}{/if}">
    {elseif $v.type=='text' || $v.type=='number' || $v.type=='date'}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col} {if isset($v.required)} required{/if}" for="">{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <input class="form-control" type="{$v.type}" name="{$v.name}" id="{$v.id}" value="{if isset($fix_row.$k)}{$fix_row.$k}{/if}"
                       {if isset($v.maxlength)}maxlength="{$v.maxlength}" {/if}
                        {if isset($v.required)} required="required" {/if}
                        {if isset($v.disabled)} disabled="disabled" {/if}>
            </div>
        </div>
    {elseif $v.type=='select'}
        <div class="form-group col-lg-{$v.form_col}">
            <label class="control-label col-lg-{$v.label_col} {if isset($v.required)} required{/if}" for="" >{$v.label}</label>
            <div class="form-group col-lg-{$v.col}" id="">
                <select class="form-control {if isset($v.required) } required{/if}>{$v.label}" name="{$v.name}">
                    {foreach from=$v.values  key=se_k item=se_v}
                        <option value="{$se_v.val}" id="{$se_v.val}" {if $fix_row[$v.name]==$se_v.val}selected="selected"{/if}>{$se_v.text}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    {elseif $v.type=='return'}
<div class="form-group col-lg-{$v.form_col}">
    <label class="control-label col-lg-{$v.label_col} {if isset($v.required)} required{/if}" for="" >{$v.label}</label>
    <div class="form-group col-lg-{$v.col}" id="">
        {if isset($fix_row[$v.name|cat:"_ag"])}
            {foreach from=$fix_row[$v.name|cat:"_ag"] key=rk item=rv}
                {if $rk==0}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">回報業務</span><input class="form-control" type="text" name="{$v.name}_ag[]" value="{$fix_row[$v.name|cat:"_ag"][$rk]}" maxlength="20">
                        <span class="input-group-addon">回報時間</span><input class="form-control" type="date" name="{$v.name}_date[]"  value="{$fix_row[$v.name|cat:"_date"][$rk]}">
                        <span class="input-group-addon" data-name="{$v.name}" onclick="add_data(this)" >+</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">回報紀錄</span>
                        <input class="form-control" type="text" name="{$v.name}_record[]" value="{$fix_row[$v.name|cat:"_record"][$rk]}" maxlength="64">
                    </div>
                {else}
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">回報業務</span><input class="form-control" type="text" name="{$v.name}_ag[]" value="{$fix_row[$v.name|cat:"_ag"][$rk]}" maxlength="20">
                        <span class="input-group-addon">回報時間</span><input class="form-control" type="date" name="{$v.name}_date[]"  value="{$fix_row[$v.name|cat:"_date"][$rk]}">
                        <span class="input-group-addon" onclick="del_data(this)" >-</span>
                    </div>
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">回報紀錄</span>
                        <input class="form-control" type="text" name="{$v.name}_record[]" value="{$fix_row[$v.name|cat:"_record"][$rk]}" maxlength="64">
                    </div>
                {/if}
            {/foreach}
        {else}
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">回報業務</span><input class="form-control" type="text" name="{$v.name}_ag[]" value="" maxlength="20">
                <span class="input-group-addon">回報時間</span><input class="form-control" type="date" name="{$v.name}_date[]"  value="">
                <span class="input-group-addon" data-name="{$v.name}" onclick="add_data(this)" >+</span>
            </div>
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">回報紀錄</span>
                <input class="form-control" type="text" name="{$v.name}_record[]" value="" maxlength="64">
            </div>
        {/if}
    </div>
</div>
    {/if}
{/foreach}







