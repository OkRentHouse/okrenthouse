{*<div class="entrance_wrap_bk">*}
{*	<img src="">*}
{*</div>*}
<main class="entrance_wrap"> 
	<img id="welcome_icon_00" src="themes/LifeHouse/img/welcom/welcome_honeycomb.svg">

	<a class="top_1 left_3" href="{$nested_group["0"]["url"]}">
		<img id="welcome_icon_1" src="{$nested_group["0"]["img_0"]["url"]}" alt="LOGO">
	</a>
	<a class="top_2 left_2" href="{$nested_group["1"]["url"]}">
		<img id="welcome_icon_2" src="{$nested_group["1"]["img_0"]["url"]}" alt="生活樂租 LIFE OK RENT">
	</a>
	<a class="top_2 left_4" href="{$nested_group["2"]["url"]}">
		<img id="welcome_icon_3" src="{$nested_group["2"]["img_0"]["url"]}" alt="生活房屋 LIFE HOUSE">
	</a>
	<a class="top_3 left_1" href="{$nested_group["3"]["url"]}">
		<img id="welcome_icon_4" src="{$nested_group["3"]["img_0"]["url"]}" alt="生活好康">
	</a>
	<a class="zoom top_3 left_3" href="{$nested_group["4"]["url"]}">
		<img id="welcome_icon_5" src="{$nested_group["4"]["img_0"]["url"]}" alt="生活集團 LIFE GROUP">
	</a>
	<a class="zoom top_3 left_5" href="{$nested_group["5"]["url"]}">
		<img id="welcome_icon_6" src="{$nested_group["5"]["img_0"]["url"]}" alt="租吧借">
	</a>
	<a class="top_4 left_4" href="{$nested_group["6"]["url"]}">
		<img id="welcome_icon_7" src="{$nested_group["6"]["img_0"]["url"]}" alt="好幫手">
	</a>
	<a class="top_4 left_2" href="{$nested_group["7"]["url"]}">
		<img id="welcome_icon_8" src="{$nested_group["7"]["img_0"]["url"]}" alt="生活樂租APP">
	</a>
	<a class="top_5 left_1" href="{$nested_group["8"]["url"]}">
		<img id="welcome_icon_9" src="{$nested_group["8"]["img_0"]["url"]}" alt="生活樂購">
	</a>
	<a class="zoom top_5 left_3" href="{$nested_group["9"]["url"]}">
		<img id="welcome_icon_10" src="{$nested_group["9"]["img_0"]["url"]}" alt="生活資產">
	</a>
	<a class="zoom top_5 left_5" href="{$nested_group["10"]["url"]}">
		<img id="welcome_icon_11" src="{$nested_group["10"]["img_0"]["url"]}" alt="健康御守">
	</a>
	<a class="top_6 left_2" href="{$nested_group["11"]["url"]}" >
		<img id="welcome_icon_12" src="{$nested_group["11"]["img_0"]["url"]}" alt="裝修達人">
	</a>
	<a class="top_6 left_4" href="{$nested_group["12"]["url"]}">
		<img id="welcome_icon_13" src="{$nested_group["12"]["img_0"]["url"]}" alt="生活好科技">
	</a>
	<a class="top_7 left_3" href="{$nested_group["13"]["url"]}">
		<img id="welcome_icon_14" src="{$nested_group["13"]["img_0"]["url"]}" alt="生活居家裝潢">
	</a>
</main>

<div class="banner_wrap">
	<div class="house_search">
		<!--縮圖開始-->
		<div class="spec-scroll">
			<div>
				<table class="items">
					<tr>
						<td>
							<strong><span onmousemove="preview_str(this);">生活達人</span></strong>
							<img alt="" bimg="themes/LifeHouse/img/welcom/welcome_banner_01.jpg" src="themes/LifeHouse/img/welcom/welcome_banner_01.jpg" onmousemove="preview(this);">
						</td>
						<td>
							<strong><span onmousemove="preview_str(this);">生活樂購</span></strong>
							<img alt="" bimg="themes/LifeHouse/img/welcom/welcome_banner_02.jpg" src="themes/LifeHouse/img/welcom/welcome_banner_02.jpg" onmousemove="preview(this);">
						</td>
						<td>
							<strong><span onmousemove="preview_str(this);">加盟生活樂租</span></strong>
							<img alt="" bimg="themes/LifeHouse/img/welcom/welcome_banner_03.jpg" src="themes/LifeHouse/img/welcom/welcome_banner_03.jpg" onmousemove="preview(this);">
						</td>
						<td>
							<strong><span onmousemove="preview_str(this);">生活好康</span></strong>
							<img alt="" bimg="themes/LifeHouse/img/welcom/welcome_banner_04.jpg" src="themes/LifeHouse/img/welcom/welcome_banner_04.jpg" onmousemove="preview(this);">
						</td>
						<td>
							<strong><span onmousemove="preview_str(this);">裝修達人</span></strong>
							<img alt="" bimg="themes/LifeHouse/img/welcom/welcome_banner_05.jpg" src="themes/LifeHouse/img/welcom/welcome_banner_05.jpg" onmousemove="preview(this);">
						</td>
						<td>
							<strong><span onmousemove="preview_str(this);">好幫手</span></strong>
							<img alt="" bimg="themes/LifeHouse/img/welcom/welcome_banner_06_0106.jpg" src="themes/LifeHouse/img/welcom/welcome_banner_06_0106.jpg" onmousemove="preview(this);">
						</td>
						<td>
							<strong class="no_border"><span onmousemove="preview_str(this);">租·換共享圈</span></strong>
							<img alt="" bimg="themes/LifeHouse/img/welcom/welcome_banner_07.031921.jpg" src="themes/LifeHouse/img/welcom/welcome_banner_07.031921.jpg" onmousemove="preview(this);">
						</td>
					</tr>
				</table>
			<div>
		</div>
		<!--縮圖結束-->
		<div id="content" class="data_wrap">
			<div class="object_view">
				<div id="preview" class="spec-preview"> <span class="jqzoom"><img
								jqimg="https://okrent.house/img/rent_house/1/4/8/8/3/l5efbf67889562.jpg"
								src="themes/LifeHouse/img/welcom/welcome_banner_01.jpg"
								style="" /></span> </div>
				<div class="clear"></div>
			</div>
		</div></div></div>
<div id="bottom_partner" ">
	<div class="company_wrap" >
		<ul class="list_wrap">
			<li class="list">
				<a href="https://www.facebook.com/桃園市租任管理服務業從業人員職業工會-600320820503706" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_01.jpg" alt="桃園市租任管理服務業從業人員職業工會">
				</a>
			</li>
			<li class="list">
				<a href="https://www.cathay-ins.com.tw/insurance" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_02.jpg" alt="國泰產險">
				</a>
			</li>
			<li class="list">
				<a href="http://www.tycrehda.org.tw/" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_03.jpg" alt="桃園市樂安居住宅發展促進協會">
				</a>
			</li>
			<li class="list">
				<a href="http://www.pcss.com.tw/" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_04.jpg" alt="保成務業保全">
				</a>
			</li>
			<li class="list">
				<a href="https://www.hoan.com.tw/" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_05.jpg" alt="和安保險">
				</a>
			</li>
			<li class="list">
				<a href="http://www.da-i.com.tw/" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_06.jpg" alt="大愛搬家">
				</a>
			</li>
			<li class="list">
				<a href="https://www.hoan.com.tw/" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_07.jpg" alt="僑馥建經">
				</a>
			</li>
			<li class="list">
				<a href="http://www.tbm.com.tw/" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_08.jpg" alt="桃園市公寓大廈管理維護商業公會">
				</a>
			</li>
			<li class="list">
				<a href="http://lawyeratticus.com/" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_09.jpg" alt="鄭三川律師">
				</a>
			</li>
			<li class="list">
				<a href="#" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_10.jpg" alt="生活好康">
				</a>
			</li>
			<li class="list">
				<a href="#" target="_blank">
					<img src="themes/LifeHouse/img/welcom/welcome_company_11.jpg" alt="台灣樂安居">
				</a>
			</li>
		</ul>
	</div>
</div>
{$js_slick}
{$js}
