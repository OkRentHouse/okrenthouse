<input type="hidden" name="id_admin" value="">
<label for="email">公司帳號</label>
<div class="form-group col-lg-12" >
    <div class="col-lg-10 no_padding">
        <div class="input-group fixed-width-lg">
            <span class="input-group-addon">公司帳號:</span>
            <input class="form-control" type="text" name="email" value="{$database_data.email}" maxlength="100" disabled="disabled">
        </div>
    </div>
</div>
    <label>狀態</label>
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">來源:</span>
                <select name="id_admin_source" id="id_admin_source" class="form-control">
                    <option value="">請選擇來源</option>
                    {foreach $database_source as $d_s_k => $d_s_v}
                        <option value="{$d_s_v.id_admin_source}" {if $d_s_v.id_admin_source==$database_data.id_admin_source}selected="selected"{/if}>{$d_s_v.name}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">單位:</span>
                <select name="id_company" id="id_company" class="form-control">
                    <option value="">請選擇單位</option>
                    {foreach $database_company as $d_c_k => $d_c_v}
                        <option value="{$d_c_v.id_company}" {if $d_c_v.id_company==$database_data.id_company}selected="selected"{/if}>{$d_c_v.name}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">單位:</span>
                <select name="id_department" id="id_department" class="form-control">
                    <option value="">請選擇部門</option>
                    {foreach $database_department as $d_d_k => $d_d_v}
                        <option value="{$d_d_v.id_department}" {if $d_d_v.id_department==$database_data.id_department}selected="selected"{/if}>{$d_d_v.name}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">職稱:</span>
                <input class="form-control" type="text"  name="job" id="job" value="{$database_data.job}" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">到職日期:</span>
                <input class="form-control" type="date"  name="on_duty_date" id="on_duty_date" value="{$database_data.on_duty_date}">
            </div>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">離職日期:</span>
                <input class="form-control" type="date"  name="resign_date" id="resign_date" value="{$database_data.resign_date}">
            </div>
        </div>
        <div class="col-lg-7 no_padding" style="padding-left: 60px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">離職原因:</span>
                <input class="form-control" type="text"  name="resign_reason" id="resign_reason" maxlength="128" value="{$database_data.resign_reason}">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">離職確認:</span>
                {if $database_data.active == "1"}
                <input class="form-control" type="checkbox"  name="active" id="active" value="1" >
                {else}
                <input class="form-control" type="checkbox"  name="active" id="active" value="0" checked>
                {/if}
            </div>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">停留日期:</span>
                <input class="form-control" type="date"  name="stay_date" id="stay_date" value="{$database_data.stay_date}">
            </div>
        </div>
        <div class="col-lg-7 no_padding" style="padding-left: 60px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">停留原因:</span>
                <input class="form-control" type="text"  name="stay_reason" id="stay_reason" value="{$database_data.stay_reason}" maxlength="128">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">復職日期:</span>
                <input class="form-control" type="date"  name="reinstatement_date" id="reinstatement_date" value="{$database_data.reinstatement_date}">
            </div>
        </div>
    </div>
    {if $page!='PersonnelUnit' && $page!='LifeAssets'}
    <label>資訊帳戶相關處理 <span class="mark text-primary"> (本設定畫面不適用) </span></label><p>
    {else}
    <label>資訊帳戶相關處理，<span class="mark text-primary"> (人員完成離職手續後、請人事通知資訊管理員刪除帳戶程序) </span></label>
    <div class="form-group col-lg-12">
        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <label style="vertical-align: sub;" class="navbar-collapse">到職：</label>
            </div>
        </div>
        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">後台帳號:</span>
                <input class="form-control" type="text" value="{$database_data.email}" placeholder="同公司帳號" readonly>
            </div>
        </div>
        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">email account:</span>
                <input class="form-control" type="text" value="{$database_data.email}" placeholder="同公司帳號" readonly>
            </div>
        </div>
        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">建立日期:</span>
                <input class="form-control" type="date"  name="create_date" id="create_date" value="{if $database_data.create_date}{$database_data.create_date}{else}{date('Y-m-d')}{/if}" readonly>
            </div>
        </div>

    </div>

    <div class="form-group col-lg-12">
        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <label style="vertical-align: sub;" class="navbar-collapse">離職：</label>
            </div>
        </div>
        {if sizeof($database_adminuser)==0}{$RLY='readonly'}{/if}
        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
              {if sizeof($database_adminuser)==0}
                <span class="input-group-addon label-danger">處理人：僅帳戶管理員可輸入</span>
                {$RLY='readonly'}
                {else}
                <span class="input-group-addon">處理人</span>
              {/if}

                <!-- <input class="form-control" type="text"  name="disable_by" id="disable_by" value="{$database_data.disable_by}" > -->

                  {foreach $database_adminuser as $d_d_k => $d_d_v}
                      <label class="form-control input-group-addon">{if $database_data.disable_by}最後處理:{$database_data.disable_by}{else}{$d_d_v.first_name_en}{/if}</label>
                      <input class="form-control" type="text"  name="disable_by" id="disable_by" value="{$d_d_v.first_name_en}" style="opacity: 0;padding: 0px;height: 0px;" {$RLY}>
                  {/foreach}

            </div>
        </div>

        <div class="col-lg-4 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">處理狀況 / 備註</span>
                <input class="form-control" type="text"  name="disable_note" id="disable_note" value="{$database_data.disable_note}" placeholder="移除後臺帳號 / email / 其他帳號" {$RLY}>
            </div>
        </div>

        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">刪除日期:</span>
                <input class="form-control" type="date"  name="disable_date" id="disable_date" value="{$database_data.disable_date}" {$RLY}>
            </div>
        </div>


    </div>
    {if sizeof($database_adminuser)!=0}
    <div class="form-group col-lg-12">
      <div class="col-lg-1 no_padding"></div>
      <div class="col-lg-4 no_padding"></div>
      <div class="col-lg-4 no_padding">快捷:<a onclick="$('#disable_note').val($('#disable_note').val() + this.innerText)" href="javascript:void(0)">刪除後台帳戶,</a>
        <a onclick="$('#disable_note').val($('#disable_note').val() + this.innerText)" href="javascript:void(0)">刪除mail帳戶,</a>
      <a onclick="$('#disable_note').val($('#disable_note').val() + this.innerText)" href="javascript:void(0)">無後台帳戶勿須處理</a></div>
      <div class="col-lg-1 no_padding"></div>
    </div>
    {/if}
    <p> </p>
    {/if}
{if $personnel_special==1}
{*  全部都要補不存在的欄位 防止錯誤 除了id,role,update_time,create_time *}
    {*  上面這邊是處理之後版型的因為有些東西不能給人顯示  *}
    <label for="labor">勞保</label>
    <div class="form-group col-lg-12">
        {if !empty($database_data.personnel_insurance.labor)}
        {foreach $database_data.personnel_insurance.labor as $p_l_k => $p_l_v}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="{$p_l_v.id_personnel_insurance}">
            <input type="hidden" name="id_personnel_insurance_type[]" value="0">
            <input type="hidden" name="id_personnel_insurance_e_date[]" >
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        {foreach $database_company as $d_c_k => $d_c_v}
                            <option value="{$d_c_v.id_company}" {if $p_l_v.id_company==$d_c_v.id_company}selected="selected"{/if}>{$d_c_v.name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
             <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="{$p_l_v.s_date}">
                </div>
            </div>
            <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0" {if $p_l_v.type==0}selected="selected"{/if}>公司轉帳</option>
                        <option value="1" {if $p_l_v.type==1}selected="selected"{/if}>憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" value="{$p_l_v.handler}" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="{$p_l_v.update_time}" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="{$p_l_v.price}">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]" value="{$p_l_v.insurance_price}">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">公司負擔:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]"  value="{$p_l_v.company_price}">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">自負額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]" value="{$p_l_v.personal_price}">
                </div>
            </div>
        </div>
        {/foreach}
        {/if}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" >
            <input type="hidden" name="id_personnel_insurance_type[]" value="0">
            <input type="hidden" name="id_personnel_insurance_e_date[]" >
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        {foreach $database_company as $d_c_k => $d_c_v}
                            <option value="{$d_c_v.id_company}" >{$d_c_v.name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" >
                </div>
            </div>
            <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0">公司轉帳</option>
                        <option value="1">憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]"  disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">公司負擔:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">自負額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="0"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
    <label for="health">健保</label>
    <div class="form-group col-lg-12">
        {if !empty($database_data.personnel_insurance.health)}
            {foreach $database_data.personnel_insurance.health as $p_h_k => $p_h_v}
                <div class="form-group col-lg-12">
                    <input type="hidden" name="id_personnel_insurance[]" value="{$p_h_v.id_personnel_insurance}">
                    <input type="hidden" name="id_personnel_insurance_type[]" value="1">
                    <input type="hidden" name="id_personnel_insurance_e_date[]">
                    <input type="hidden" name="id_personnel_deduction_type[]">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保單位:</span>
                            <select class="form-control" name="id_personnel_insurance_id_company[]">
                                <option value="">請選擇單位</option>
                                {foreach $database_company as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_company}" {if $p_h_v.id_company==$d_c_v.id_company}selected="selected"{/if}>{$d_c_v.name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保日期:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="{$p_h_v.s_date}">
                        </div>
                    </div>
                    <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">扣款方式:</span>
                            <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                                <option value="">請選擇單位</option>
                                <option value="0" {if $p_h_v.deduction_type==0}selected="selected"{/if}>公司轉帳</option>
                                <option value="1" {if $p_h_v.deduction_type==1}selected="selected"{/if}>憑單繳費</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">承辦人:</span>
                            <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" value="{$p_h_v.handler}" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">修改時間:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="{$p_h_v.update_time}"  disabled="disabled">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">眷屬:</span>
                            <input class="form-control" type="text"  name="id_personnel_insurance_dependents[]" value="{$p_h_v.dependents}" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">投保金額:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="{$p_h_v.price}">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">保費:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]"  value="{$p_h_v.insurance_price}">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">公司負擔:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]" value="{$p_h_v.company_price}">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">自負額:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]"  value="{$p_h_v.personal_price}">
                        </div>
                    </div>
                </div>
            {/foreach}
        {/if}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="">
            <input type="hidden" name="id_personnel_insurance_type[]" value="1">
            <input type="hidden" name="id_personnel_insurance_e_date[]">
            <input type="hidden" name="id_personnel_insurance_deduction_type[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        {foreach $database_company as $d_c_k => $d_c_v}
                            <option value="{$d_c_v.id_company}">{$d_c_v.name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">
                </div>
            </div>
            <div class="col-lg-3 no_padding"  style="padding-left: 62px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0">公司轉帳</option>
                        <option value="1">憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">眷屬:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_dependents[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">公司負擔:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">自負額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="1"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
    <label for="accident">意外險</label>
    <div class="form-group col-lg-12">
        {if !empty($database_data.personnel_insurance.accident)}
            {foreach $database_data.personnel_insurance.accident as $p_a_k => $p_a_v}
                <div class="form-group col-lg-12">
                    <input type="hidden" name="id_personnel_insurance[]" value="{$p_a_v.id_personnel_insurance}">
                    <input type="hidden" name="id_personnel_insurance_type[]" value="2">
                    <input type="hidden" name="id_personnel_insurance_company_price[]">
                    <input type="hidden" name="id_personnel_insurance_personal_price[]">
                    <input type="hidden" name="id_personnel_insurance_dependents[]">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保單位:</span>
                            <select class="form-control" name="id_personnel_insurance_id_company[]">
                                <option value="">請選擇單位</option>
                                {foreach $database_company as $d_c_k => $d_c_v}
                                    <option value="{$d_c_v.id_company}" {if $p_a_v.id_company==$d_c_v.id_company}selected="selected"{/if}>{$d_c_v.name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">加保日期:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="{$p_a_v.s_date}">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding" style="padding-left: 60px">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">退保日期:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]" value="{$p_a_v.e_date}">
                        </div>
                    </div>
                    <div class="col-lg-3 no_padding" style="padding-left: 122px">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">承辦人:</span>
                            <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" value="{$p_a_v.handler}" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">修改時間:</span>
                            <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="{$p_a_v.update_time}"  disabled="disabled">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">投保金額:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="{$p_a_v.price}">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">保費:</span>
                            <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]" value="{$p_a_v.insurance_price}">
                        </div>
                    </div>
                </div>
            {/foreach}
        {/if}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="">
            <input type="hidden" name="id_personnel_insurance_type[]" value="2">
            <input type="hidden" name="id_personnel_insurance_company_price[]">
            <input type="hidden" name="id_personnel_insurance_personal_price[]">
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        {foreach $database_company as $d_c_k => $d_c_v}
                            <option value="{$d_c_v.id_company}">{$d_c_v.name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="padding-left: 60px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">退保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]">
                </div>
            </div>
            <div class="col-lg-3 no_padding" style="padding-left: 122px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="2"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
    <label for="personnel_ensure">人事保證險</label>
    <div class="form-group col-lg-12">
        {if !empty($database_data.personnel_insurance.personnel_ensure)}
        {foreach $database_data.personnel_insurance.personnel_ensure as $p_p_e_k => $p_p_e_v}
            <div class="form-group col-lg-12">
                <input type="hidden" name="id_personnel_insurance[]" value="{$p_p_e_v.id_personnel_insurance}">
                <input type="hidden" name="id_personnel_insurance_type[]" value="3">
                <input type="hidden" name="id_personnel_insurance_company_price[]">
                <input type="hidden" name="id_personnel_insurance_personal_price[]">
                <input type="hidden" name="id_personnel_insurance_dependents[]">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">加保單位:</span>
                        <select class="form-control" name="id_personnel_insurance_id_company[]">
                            <option value="">請選擇單位</option>
                            {foreach $database_company as $d_c_k => $d_c_v}
                                <option value="{$d_c_v.id_company}" {if $p_p_e_v.id_company==$d_c_v.id_company}selected="selected"{/if}>{$d_c_v.name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">加保日期:</span>
                        <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" value="{$p_p_e_v.s_date}">
                    </div>
                </div>
                <div class="col-lg-2 no_padding" style="padding-left: 60px">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">退保日期:</span>
                        <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]" value="{$p_p_e_v.e_date}">
                    </div>
                </div>
                <div class="col-lg-3 no_padding" style="padding-left: 122px">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">承辦人:</span>
                        <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32" value="{$p_p_e_v.handler}">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">修改時間:</span>
                        <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" value="{$p_p_e_v.update_time}" disabled="disabled">
                    </div>
                </div>
                <div class="col-lg-3 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">扣款方式:</span>
                        <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                            <option value="">請選擇單位</option>
                            <option value="0" {if $p_p_e_v.type==0}selected="selected"{/if}>公司轉帳</option>
                            <option value="1" {if $p_p_e_v.type==1}selected="selected"{/if}>憑單繳費</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">投保金額:</span>
                        <input class="form-control" type="number"  name="id_personnel_insurance_price[]" value="{$p_p_e_v.price}">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">保費:</span>
                        <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]" value="{$p_p_e_v.insurance_price}">
                    </div>
                </div>
            </div>
        {/foreach}
        {/if}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_personnel_insurance[]" value="">
            <input type="hidden" name="id_personnel_insurance_type[]" value="3">
            <input type="hidden" name="id_personnel_insurance_company_price[]">
            <input type="hidden" name="id_personnel_insurance_personal_price[]">
            <input type="hidden" name="id_personnel_insurance_dependents[]">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保單位:</span>
                    <select class="form-control" name="id_personnel_insurance_id_company[]">
                        <option value="">請選擇單位</option>
                        {foreach $database_company as $d_c_k => $d_c_v}
                            <option value="{$d_c_v.id_company}">{$d_c_v.name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">加保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="padding-left: 60px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">退保日期:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]">
                </div>
            </div>
            <div class="col-lg-3 no_padding" style="padding-left: 122px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">承辦人:</span>
                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">修改時間:</span>
                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">扣款方式:</span>
                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">
                        <option value="">請選擇單位</option>
                        <option value="0">公司轉帳</option>
                        <option value="1">憑單繳費</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">投保金額:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">保費:</span>
                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">
                </div>
            </div>
            <div class="col-lg-1 no_padding">
                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"  data-type="3"  onclick="add_personnel_data(this)" >+</button>
            </div>
        </div>
    </div>
{/if}
<label>基本資料</label>
<div class="form-group col-lg-12">
    <div class="form-group col-lg-12">
        {if $personnel_special==1}
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">人力編號:</span>
                    <input class="form-control" type="text"  name="admin_number" id="admin_number" value="{$database_data.admin_number}" maxlength="32">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">分享代碼:</span>
                    <input class="form-control" type="text"  name="share_number" id="share_number" value="{$database_data.share_number}" maxlength="32">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">雲總機:</span>
                    <input class="form-control" type="text"  name="switchboard" id="switchboard" value="{$database_data.switchboard}" maxlength="32">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">出勤代碼:</span>
                    <input class="form-control" type="text"  name="attendance_number" id="attendance_number" value="{$database_data.attendance_number}" maxlength="32">
                </div>
            </div>
        {else}
{*           *}
            <input class="form-control" type="hidden"  name="admin_number" id="admin_number" value="{$database_data.admin_number}" maxlength="32">
            <input class="form-control" type="hidden"  name="share_number" id="share_number" value="{$database_data.share_number}" maxlength="32">
            <input class="form-control" type="hidden"  name="switchboard" id="switchboard" value="{$database_data.switchboard}" maxlength="32">
            <input class="form-control" type="hidden"  name="attendance_number" id="attendance_number" value="{$database_data.attendance_number}" maxlength="32">

            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">人力編號:</span>
                    <input class="form-control" type="text"  name="admin_number" id="admin_number" value="{$database_data.admin_number}" maxlength="32" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">分享代碼:</span>
                    <input class="form-control" type="text"  name="share_number" id="share_number" value="{$database_data.share_number}" maxlength="32" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">雲總機:</span>
                    <input class="form-control" type="text"  name="switchboard" id="switchboard" value="{$database_data.switchboard}" maxlength="32" disabled="disabled">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">出勤代碼:</span>
                    <input class="form-control" type="text"  name="attendance_number" id="attendance_number" value="{$database_data.attendance_number}" maxlength="32" disabled="disabled">
                </div>
            </div>
        {/if}


        <div class="col-lg-1 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">姓:</span>
                <input class="form-control" type="text"  name="last_name" id="last_name" value="{$database_data.last_name}" maxlength="10" required="required" >
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">名:</span>
                <input class="form-control" type="text"  name="first_name" id="first_name" value="{$database_data.first_name}" maxlength="10" required="required">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">英文姓名:</span>
                <input class="form-control" type="text"  name="en_name" id="en_name" value="{$database_data.en_name}" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">性別:</span>
                <select class="form-control" name="gender" id="gender">
                    <option value="">請選擇</option>
                    <option value="0" {if $database_data.gender==0}selected="selected"{/if}>男</option>
                    <option value="1" {if $database_data.gender==1}selected="selected"{/if}>女</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">身分證號碼:</span>
                <input class="form-control" type="text"  name="identity" id="identity" value="{$database_data.identity}" maxlength="16">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">生日:</span>
                <input class="form-control" type="date"  name="birthday" id="birthday" value="{$database_data.birthday}">
            </div>
        </div>

        <div class="col-lg-4 no_padding" style="left: 37px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">Line id:</span>
                <input class="form-control" type="text"  name="line_id" id="line_id" value="{$database_data.line_id}" maxlength="128">
            </div>
        </div>
        <div class="col-lg-5 no_padding" style="left: 35px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">email:</span>
                <input class="form-control" type="text"  name="private_email" id="private_email"  value="{$database_data.private_email}" maxlength="128">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">健康狀況:</span>
                <select class="form-control" name="health" id="health">
                    <option value="">請選擇狀況</option>
                    <option value="0" {if $database_data.health==0}selected="selected"{/if}>良好</option>
                    <option value="1" {if $database_data.health==1}selected="selected"{/if}>有疾病</option>
                    <option value="2" {if $database_data.health==2}selected="selected"{/if}>領有身心障礙手冊</option>
                </select>
            </div>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">疾病:</span>
                <input class="form-control" type="text"  name="disease" id="disease" value="{$database_data.disease}" maxlength="128">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">宗教:</span>
                <select class="form-control" name="religion" id="religion">
                    <option value="">請選擇信仰</option>
                    <option value="0" {if $database_data.religion==0}selected="selected"{/if}>道教</option>
                    <option value="1" {if $database_data.religion==1}selected="selected"{/if}>佛教</option>
                    <option value="2" {if $database_data.religion==2}selected="selected"{/if}>天主教</option>
                    <option value="3" {if $database_data.religion==3}selected="selected"{/if}>基督教(新教)</option>
                    <option value="4" {if $database_data.religion==4}selected="selected"{/if}>回教</option>
                    <option value="5" {if $database_data.religion==5}selected="selected"{/if}>印度教</option>
                    <option value="6" {if $database_data.religion==6}selected="selected"{/if}>摩門教</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">戶籍電話:</span>
                <input class="form-control" type="text"  name="domicile_tel" id="domicile_tel" value="{$database_data.domicile_tel}" maxlength="32">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">連絡電話:</span>
                <input class="form-control" type="text"  name="contact_tel" id="contact_tel" value="{$database_data.contact_tel}" maxlength="32">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">手機:</span>
                <input class="form-control" type="text"  name="phone" id="contact_tel" value="{$database_data.contact_tel}" maxlength="32">
            </div>
        </div>
    </div>
</div>

<div class="form-group col-lg-12" >
    <label>連絡地址</label>
    <input type="hidden" name="id_address_domicile" class="" value="{$database_data.id_address_domicile}">
    <div class="form-group col-lg-12">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_address_domicile_postal"  class="form-control " value="{$database_data.address_domicile.postal}" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_domicile_id_county" class="form-control county">
                <option value="">請選擇縣市</option>
                {foreach $database_county as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_county}" {if $d_c_v.id_county==$database_data.address_domicile.id_county}selected="selected"{/if}>{$d_c_v.county_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_domicile_id_city" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                {foreach $database_city as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_city}"  {if $d_c_v.id_city==$database_data.address_domicile.id_city}selected="selected"{/if}>{$d_c_v.city_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_address_domicile_village" class="form-control " value="{$database_data.address_domicile.village}" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_address_domicile_neighbor" class="form-control " value="{$database_data.address_domicile.neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_address_domicile_road" class="form-control " value="{$database_data.address_domicile.road}" maxlength="20"><span class="input-group-addon">填全名</span>
            </div>
        </div>
        <div class="form-group col-lg-12">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_address_domicile_segment" class="form-control " value="{$database_data.address_domicile.segment}" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_address_domicile_lane" class="form-control " value="{$database_data.address_domicile.lane}" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_address_domicile_alley" class="form-control " value="{$database_data.address_domicile.alley}" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_address_domicile_no" class="form-control " value="{$database_data.address_domicile["no"]}" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_domicile_no_her" class="form-control " value="{$database_data.address_domicile.no_her}" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_domicile_floor" class="form-control " value="{$database_data.address_domicile.floor}" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_domicile_floor_her" class="form-control " value="{$database_data.address_domicile.floor_her}" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_domicile_address_room" class="form-control " value="{$database_data.address_domicile.address_room}" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9">
            <input type="text" name="id_address_domicile_address" class="form-control " value="{$database_data.address_domicile.address}" maxlength="255" placeholder="完整地址">
        </div>
    </div>
</div>
<div class="form-group col-lg-12" >
    <label>居住地址</label>
    <div class="form-check form-check-inline">
        <input class="form-check-input same_address" type="checkbox" id="same_address" name="same_address" value="1" {if $database_data.same_address}checked{/if} >
        <label class="form-check-label" >同上地址</label>
    </div>
    <input type="hidden" name="id_address_contact" class="" value="{$database_data.id_address_contact}">
    <div class="form-group col-lg-12 same_address">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg" >
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_address_contact_postal"  class="form-control " value="{$database_data.address_contact.postal}" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_contact_id_county" class="form-control county">
                <option value="">請選擇縣市</option>
                {foreach $database_county as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_county}" {if $d_c_v.id_county==$database_data.address_contact.id_county}selected="selected"{/if}>{$d_c_v.county_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_address_contact_id_city" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                {foreach $database_city as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_city}" {if $d_c_v.id_city==$database_data.address_contact.id_city}selected="selected"{/if}>{$d_c_v.city_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_address_contact_village" class="form-control " value="{$database_data.address_contact.village}" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_address_contact_neighbor" class="form-control " value="{$database_data.address_contact.neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_address_contact_road" class="form-control " value="{$database_data.address_contact.road}" maxlength="20"><span class="input-group-addon">填全名</span>
            </div>
        </div>
        <div class="form-group col-lg-12">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_address_contact_segment" class="form-control " value="{$database_data.address_contact.segment}" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_address_contact_lane" class="form-control " value="{$database_data.address_contact.lane}" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_address_contact_alley" class="form-control " value="{$database_data.address_contact.alley}" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_address_contact_no" class="form-control " value="{$database_data.address_contact['no']}" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_contact_no_her" class="form-control " value="{$database_data.address_contact.no_her}" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_contact_floor" class="form-control " value="{$database_data.address_contact.floor}" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_address_contact_floor_her" class="form-control " value="{$database_data.address_contact.floor_her}" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_address_contact_address_room" class="form-control " value="{$database_data.address_contact.address_room}" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9">
            <input type="text" name="id_address_contact_address" class="form-control " value="{$database_data.address_contact.address}" maxlength="255" placeholder="完整地址">
        </div>
    </div>
</div>

<div class="form-group col-lg-12" >
    <label>家庭成員</label>
    {if !empty($database_data.admin_family)}
        {foreach $database_data.admin_family as $d_d_a_f_k => $d_d_a_f_v}
            {if $d_d_a_f_v.urgent_type == 0}
                <div class="form-group col-lg-12">
                <input type="hidden" name="id_admin_family[]" class="" value="{$d_d_a_f_v.id_admin_family}">
                <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="0">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="{$d_d_a_f_v.relation}" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="{$d_d_a_f_v.name}" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="{$d_d_a_f_v.tel}" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="{$d_d_a_f_v.phone}" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="{$d_d_a_f_v.job}" maxlength="32">
                    </div>
                </div>
                <input type="hidden" name="id_admin_family_id_address[]" value="{$d_d_a_f_v.address.id_address}">
                <div class="col-lg-2 no_padding" style="display:none;">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="{$d_d_a_f_v.address.postal}" maxlength="11">
                    </div>
                </div>
                <div class="col-lg-2 no_padding" style="display:none;">
                    <select name="id_admin_family_id_county[]" class="form-control county">
                        <option value="">請選擇縣市</option>
                        {foreach $database_county as $d_c_k => $d_c_v}
                            <option value="{$d_c_v.id_county}" {if $d_c_v.id_county==$d_d_a_f_v.address.id_county}selected="selected"{/if}>{$d_c_v.county_name}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-lg-2 no_padding" style="display:none;">
                    <select name="id_admin_family_id_city[]" class="form-control city">
                        <option value="">請選擇鄉鎮區</option>
                        {foreach $database_city as $d_c_k => $d_c_v}
                            <option value="{$d_c_v.id_city}" {if $d_c_v.id_city==$d_d_a_f_v.address.id_city}selected="selected"{/if}>{$d_c_v.city_name}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-lg-5 no_padding" style="display:none;">
                    <div class="input-group fixed-width-lg">
                        <input type="text"   name="id_admin_family_village[]" class="form-control " value="{$d_d_a_f_v.address.village}" maxlength="20"><span class="input-group-addon">里</span>
                        <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="{$d_d_a_f_v.address.neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                        <input type="text"   name="id_admin_family_road[]" class="form-control " value="{$d_d_a_f_v.address.road}" maxlength="20"><span class="input-group-addon">填全名</span>
                    </div>
                </div>
                <div class="form-group col-lg-12" style="display:none;">
                    <div class="input-group fixed-width-lg">
                        <input type="text" name="id_admin_family_segment[]" class="form-control " value="{$d_d_a_f_v.address.segment}" maxlength="10"><span class="input-group-addon">段</span>
                        <input type="text" name="id_admin_family_lane[]" class="form-control " value="{$d_d_a_f_v.address.lane}" maxlength="10"><span class="input-group-addon">巷</span>
                        <input type="text" name="id_admin_family_alley[]" class="form-control " value="{$d_d_a_f_v.address.alley}" maxlength="10"><span class="input-group-addon">弄</span>
                        <input type="text" name="id_admin_family_no[]" class="form-control " value="{$d_d_a_f_v.address['no']}" maxlength="10"><span class="input-group-addon">號</span>
                        <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="{$d_d_a_f_v.address.no_her}" maxlength="10"><span class="input-group-addon"></span>
                        <input type="text" name="id_admin_family_floor[]" class="form-control " value="{$d_d_a_f_v.address.floor}" maxlength="10"><span class="input-group-addon">樓</span>
                        <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="{$d_d_a_f_v.address.floor_her}" maxlength="10"><span class="input-group-addon"></span>
                        <input type="text" name="id_admin_family_address_room[]" class="form-control " value="{$d_d_a_f_v.address.address_room}" maxlength="8"><span class="input-group-addon">室</span>
                    </div>
                </div>
                <div class="form-group col-lg-9" style="display:none;">
                    <input type="text" name="id_admin_family_address[]" class="form-control " value="{$d_d_a_f_v.address.address}" maxlength="255" placeholder="完整地址">
                </div>
            </div>
            {/if}
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_admin_family[]" class="" value="">
        <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="0">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>

        <input type="hidden" name="id_admin_family_id_address[]" value="" style="display:none;">
        <div class="col-lg-2 no_padding" style="display:none;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="display:none;">
            <select name="id_admin_family_id_county[]" class="form-control county">
                <option value="">請選擇縣市</option>
                {foreach $database_county as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-2 no_padding" style="display:none;">
            <select name="id_admin_family_id_city[]" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                {foreach $database_city as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-5 no_padding" style="display:none;">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_admin_family_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_admin_family_road[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">填全名</span>
            </div>
        </div>
        <div class="form-group col-lg-12" style="display:none;">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_admin_family_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_admin_family_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_admin_family_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_admin_family_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9" style="display:none;">
            <input type="text" name="id_admin_family_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="admin_family"  data-type="0"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<div class="form-group col-lg-12" >
    <label>緊急聯絡人</label>
    {if !empty($database_data.admin_family)}
        {foreach $database_data.admin_family as $d_d_a_f_k => $d_d_a_f_v}
            {if $d_d_a_f_v.urgent_type == 1}
                <div class="form-group col-lg-12">
                    <input type="hidden" name="id_admin_family[]" class="" value="{$d_d_a_f_v.id_admin_family}">
                    <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="1">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="{$d_d_a_f_v.relation}" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="{$d_d_a_f_v.name}" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="{$d_d_a_f_v.tel}" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="{$d_d_a_f_v.phone}" maxlength="32">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="{$d_d_a_f_v.job}" maxlength="32">
                        </div>
                    </div>
                    <input type="hidden" name="id_admin_family_id_address[]" value="{$d_d_a_f_v.address.id_address}">
                    <div class="col-lg-2 no_padding">
                        <div class="input-group fixed-width-lg">
                            <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="{$d_d_a_f_v.address.postal}" maxlength="11">
                        </div>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <select name="id_admin_family_id_county[]" class="form-control county">
                            <option value="">請選擇縣市</option>
                            {foreach $database_county as $d_c_k => $d_c_v}
                                <option value="{$d_c_v.id_county}" {if $d_c_v.id_county==$d_d_a_f_v.address.id_county}selected="selected"{/if}>{$d_c_v.county_name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-lg-2 no_padding">
                        <select name="id_admin_family_id_city[]" class="form-control city">
                            <option value="">請選擇鄉鎮區</option>
                            {foreach $database_city as $d_c_k => $d_c_v}
                                <option value="{$d_c_v.id_city}" {if $d_c_v.id_city==$d_d_a_f_v.address.id_city}selected="selected"{/if}>{$d_c_v.city_name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-lg-5 no_padding">
                        <div class="input-group fixed-width-lg">
                            <input type="text"   name="id_admin_family_village[]" class="form-control " value="{$d_d_a_f_v.address.village}" maxlength="20"><span class="input-group-addon">里</span>
                            <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="{$d_d_a_f_v.address.neighbor}" maxlength="11"><span class="input-group-addon">鄰</span>
                            <input type="text"   name="id_admin_family_road[]" class="form-control " value="{$d_d_a_f_v.address.road}" maxlength="20"><span class="input-group-addon">填全名</span>
                        </div>
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="input-group fixed-width-lg">
                            <input type="text" name="id_admin_family_segment[]" class="form-control " value="{$d_d_a_f_v.address.segment}" maxlength="10"><span class="input-group-addon">段</span>
                            <input type="text" name="id_admin_family_lane[]" class="form-control " value="{$d_d_a_f_v.address.lane}" maxlength="10"><span class="input-group-addon">巷</span>
                            <input type="text" name="id_admin_family_alley[]" class="form-control " value="{$d_d_a_f_v.address.alley}" maxlength="10"><span class="input-group-addon">弄</span>
                            <input type="text" name="id_admin_family_no[]" class="form-control " value="{$d_d_a_f_v.address['no']}" maxlength="10"><span class="input-group-addon">號</span>
                            <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="{$d_d_a_f_v.address.no_her}" maxlength="10"><span class="input-group-addon"></span>
                            <input type="text" name="id_admin_family_floor[]" class="form-control " value="{$d_d_a_f_v.address.floor}" maxlength="10"><span class="input-group-addon">樓</span>
                            <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="{$d_d_a_f_v.address.floor_her}" maxlength="10"><span class="input-group-addon"></span>
                            <input type="text" name="id_admin_family_address_room[]" class="form-control " value="{$d_d_a_f_v.address.address_room}" maxlength="8"><span class="input-group-addon">室</span>
                        </div>
                    </div>
                    <div class="form-group col-lg-9">
                        <input type="text" name="id_admin_family_address[]" class="form-control " value="{$d_d_a_f_v.address.address}" maxlength="255" placeholder="完整地址">
                    </div>
                </div>
            {/if}
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_admin_family[]" class="" value="">
        <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="1">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="" maxlength="32">
            </div>
        </div>

        <input type="hidden" name="id_admin_family_id_address[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="" maxlength="11">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_admin_family_id_county[]" class="form-control county">
                <option value="">請選擇縣市</option>
                {foreach $database_county as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-2 no_padding">
            <select name="id_admin_family_id_city[]" class="form-control city">
                <option value="">請選擇鄉鎮區</option>
                {foreach $database_city as $d_c_k => $d_c_v}
                    <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="col-lg-5 no_padding">
            <div class="input-group fixed-width-lg">
                <input type="text"   name="id_admin_family_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>
                <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>
                <input type="text"   name="id_admin_family_road[]" class="form-control " value="" maxlength="20">
            </div>
        </div>
        <div class="form-group col-lg-12">
            <div class="input-group fixed-width-lg">
                <input type="text" name="id_admin_family_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>
                <input type="text" name="id_admin_family_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>
                <input type="text" name="id_admin_family_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>
                <input type="text" name="id_admin_family_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>
                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>
                <input type="text" name="id_admin_family_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>
            </div>
        </div>
        <div class="form-group col-lg-9">
            <input type="text" name="id_admin_family_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="admin_family"  data-type="1"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>

<label>學歷</label>
<div class="form-group col-lg-12">
    {if !empty($database_data.education)}
        {foreach $database_data.education as $d_e_k=>$d_e_v}
            <div class="form-group col-lg-12">
                <input type="hidden" name="id_education[]" value="{$d_e_v.id_education}">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">學歷:</span>
                        <select class="form-control" name="id_education_education_level[]">
                            <option value="">請選擇學歷</option>
                            <option value="0" {if $d_e_v.education_level==0}selected="selected"{/if}>高中以下</option>
                            <option value="1" {if $d_e_v.education_level==1}selected="selected"{/if}>二技</option>
                            <option value="2" {if $d_e_v.education_level==2}selected="selected"{/if}>四技</option>
                            <option value="3" {if $d_e_v.education_level==3}selected="selected"{/if}>大學</option>
                            <option value="4" {if $d_e_v.education_level==4}selected="selected"{/if}>碩士</option>
                            <option value="5" {if $d_e_v.education_level==5}selected="selected"{/if}>博士</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">學校:</span>
                        <input class="form-control" type="text"  name="id_education_school[]" value="{$d_e_v.school}"   maxlength="64">
                    </div>
                </div>
                <div class="col-lg-3 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">科系:</span>
                        <input class="form-control" type="text"  name="id_education_department[]" value="{$d_e_v.department}"  maxlength="64">
                    </div>
                </div>　
                <div class="col-lg-2 no_padding" style="left: -15px;">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">畢/肄業:</span>
                        <select class="form-control" name="id_education_graduation[]">
                            <option value="">是否畢業</option>
                            <option value="0" {if $d_e_v.graduation==0}selected="selected"{/if}>畢業</option>
                            <option value="1" {if $d_e_v.graduation==1}selected="selected"{/if}>肄業</option>
                        </select>
                    </div>
                </div>
            </div>
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_education[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">學歷:</span>
                <select class="form-control" name="id_education_education_level[]">
                    <option value="">請選擇學歷</option>
                    <option value="0">高中以下</option>
                    <option value="1">二技</option>
                    <option value="2">四技</option>
                    <option value="3">大學</option>
                    <option value="4">碩士</option>
                    <option value="5">博士</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">學校:</span>
                <input class="form-control" type="text"  name="id_education_school[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">科系:</span>
                <input class="form-control" type="text"  name="id_education_department[]"   maxlength="64">
            </div>
        </div>　
        <div class="col-lg-2 no_padding" style="left: -15px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">畢/肄業:</span>
                <select class="form-control" name="id_education_graduation[]">
                    <option value="">是否畢業</option>
                    <option value="0">畢業</option>
                    <option value="1">肄業</option>
                </select>
            </div>
        </div>
        <div class="col-lg-1 no_padding" style="left: -15px;">
            <button type="button" class="btn btn-secondary " data-table="education" data-type="" onclick="add_personnel_data(this)">+</button>
        </div>
    </div>
</div>

<label>語言</label>
<div class="form-group col-lg-12">
    {if !empty($database_data.language)}
        {foreach $database_data.language as $d_l_k =>$d_l_v}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_language[]" value="{$d_l_v.id_language}">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">語言:</span>
                    <select class="form-control" name="id_language_name[]">
                        <option value="">選擇語言</option>
                        <option value="1" {if $d_l_v.name==1}selected="selected"{/if}>中文</option>
                        <option value="2" {if $d_l_v.name==2}selected="selected"{/if}>台語</option>
                        <option value="3" {if $d_l_v.name==3}selected="selected"{/if}>英文</option>
                        <option value="4" {if $d_l_v.name==4}selected="selected"{/if}>日文</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">聽:</span>
                    <select class="form-control" name="id_language_listen[]">
                        <option value="">選擇程度</option>
                        <option value="0" {if $d_l_v.listen==0}selected="selected"{/if}>不懂</option>
                        <option value="1" {if $d_l_v.listen==1}selected="selected"{/if}>略懂</option>
                        <option value="2" {if $d_l_v.listen==2}selected="selected"{/if}>中等</option>
                        <option value="3" {if $d_l_v.listen==3}selected="selected"{/if}>精通</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">說:</span>
                    <select class="form-control" name="id_language_speak[]">
                        <option value="">選擇程度</option>
                        <option value="0" {if $d_l_v.speak==0}selected="selected"{/if}>不懂</option>
                        <option value="1" {if $d_l_v.speak==1}selected="selected"{/if}>略懂</option>
                        <option value="2" {if $d_l_v.speak==2}selected="selected"{/if}>中等</option>
                        <option value="3" {if $d_l_v.speak==3}selected="selected"{/if}>精通</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">讀:</span>
                    <select class="form-control" name="id_language_read[]">
                        <option value="">選擇程度</option>
                        <option value="0" {if $d_l_v.read==0}selected="selected"{/if}>不懂</option>
                        <option value="1" {if $d_l_v.read==1}selected="selected"{/if}>略懂</option>
                        <option value="2" {if $d_l_v.read==2}selected="selected"{/if}>中等</option>
                        <option value="3" {if $d_l_v.read==3}selected="selected"{/if}>精通</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">寫:</span>
                    <select class="form-control" name="id_language_write[]">
                        <option value="">選擇程度</option>
                        <option value="0" {if $d_l_v.write==0}selected="selected"{/if}>不懂</option>
                        <option value="1" {if $d_l_v.write==1}selected="selected"{/if}>略懂</option>
                        <option value="2" {if $d_l_v.write==2}selected="selected"{/if}>中等</option>
                        <option value="3" {if $d_l_v.write==3}selected="selected"{/if}>精通</option>
                    </select>
                </div>
            </div>
        </div>
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_language[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">語言:</span>
                <select class="form-control" name="id_language_name[]">
                    <option value="">選擇語言</option>
                    <option value="1">中文</option>
                    <option value="2">台語</option>
                    <option value="3">英文</option>
                    <option value="4">日文</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">聽:</span>
                <select class="form-control" name="id_language_listen[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">說:</span>
                <select class="form-control" name="id_language_speak[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">讀:</span>
                <select class="form-control" name="id_language_read[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">寫:</span>
                <select class="form-control" name="id_language_write[]">
                    <option value="">選擇程度</option>
                    <option value="0">不懂</option>
                    <option value="1">略懂</option>
                    <option value="2">中等</option>
                    <option value="3">精通</option>
                </select>
            </div>
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="language"  data-type=""  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>經歷</label>
<div class="form-group col-lg-12">
    {if !empty($database_data.experience)}
        {foreach $database_data.experience as $d_e_k => $d_e_v}
            <div class="form-group col-lg-12">
                <input type="hidden" name="id_experience[]" value="{$d_e_v.id_experience}">
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">公司:</span>
                        <input class="form-control" type="text"  name="id_experience_company[]"  value="{$d_e_v.company}" maxlength="64">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">工作:</span>
                        <input class="form-control" type="text"  name="id_experience_job[]"  value="{$d_e_v.job}" maxlength="32">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">待遇:</span>
                        <input class="form-control" type="number"  name="id_experience_salary[]" value="{$d_e_v.salary}">
                    </div>
                </div>
                <div class="col-lg-2 no_padding">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">開始時間:</span>
                        <input class="form-control" type="date"  name="id_experience_s_date[]" value="{$d_e_v.s_date}">
                    </div>
                </div>
                <div class="col-lg-2 no_padding" style="left: 65px;">
                    <div class="input-group fixed-width-lg">
                        <span class="input-group-addon">結束時間:</span>
                        <input class="form-control" type="date"  name="id_experience_e_date[]"  value="{$d_e_v.e_date}">
                    </div>
                </div>
            </div>
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_experience[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">公司:</span>
                <input class="form-control" type="text"  name="id_experience_company[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">工作:</span>
                <input class="form-control" type="text"  name="id_experience_job[]"   maxlength="32">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">待遇:</span>
                <input class="form-control" type="number"  name="id_experience_salary[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">開始時間:</span>
                <input class="form-control" type="date"  name="id_experience_s_date[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">結束時間:</span>
                <input class="form-control" type="date"  name="id_experience_e_date[]">
            </div>
        </div>
        <div class="col-lg-1 no_padding" style="left: 133px;">
            <button type="button" class="btn btn-secondary " data-table="experience"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>專業證照</label>
<div class="form-group col-lg-12">
    {if !empty($database_data.license)}
        {foreach $database_data.license as $d_l_k => $d_l_v}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_license[]" value="{$d_l_v.id_license}">
            <div class="col-lg-3 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">證照名稱:</span>
                    <input class="form-control" type="text"  name="id_license_name[]" value="{$d_l_v.name}"  maxlength="64">
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">開始時間:</span>
                    <input class="form-control" type="date"  name="id_license_s_date[]" value="{$d_l_v.s_date}">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px;">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">結束時間:</span>
                    <input class="form-control" type="date"  name="id_license_e_date[]" value="{$d_l_v.e_date}">
                </div>
            </div>
            <div class="col-lg-3 no_padding" style="left: 130px;">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">備註:</span>
                    <input class="form-control" type="text"  name="id_license_remarks[]"  value="{$d_l_v.remarks}"   maxlength="128">
                </div>
            </div>
        </div>
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_license[]" value="">
        <div class="col-lg-3 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">證照名稱:</span>
                <input class="form-control" type="text"  name="id_license_name[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">開始時間:</span>
                <input class="form-control" type="date"  name="id_license_s_date[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">結束時間:</span>
                <input class="form-control" type="date"  name="id_license_e_date[]">
            </div>
        </div>
        <div class="col-lg-3 no_padding" style="left: 130px;">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">備註:</span>
                <input class="form-control" type="text"  name="id_license_remarks[]"   maxlength="128">
            </div>
        </div>
        <div class="col-lg-1 no_padding"  style="left: 128px;">
            <button type="button" class="btn btn-secondary " data-table="license"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>人民團體</label>
<div class="form-group col-lg-12">
    {if !empty($database_data.people_group)}
        {foreach $database_data.people_group as $d_p_k=>$d_p_v}
        <div class="form-group col-lg-12">
            <input type="hidden" name="id_people_group[]" value="{$d_p_v.id_people_group}">
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">團體:</span>
                    <select name="id_people_group_id_company[]"  class="form-control">
                        <option value="">請選擇團體</option>
                        {foreach $database_company as $d_c_k => $d_c_v}
                            {if $d_c_v.type==1}
                                <option value="{$d_c_v.id_company}" {if $d_p_v.id_company==$d_c_v.id_company}selected="selected"{/if}>{$d_c_v.name}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-lg-2 no_padding">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">入會日期:</span>
                    <input class="form-control" type="date"  name="id_people_group_s_date[]" value="{$d_p_v.s_date}">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">會員編號:</span>
                    <input class="form-control" type="text"  name="id_people_group_number[]"  value="{$d_p_v.number}"  maxlength="64">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">會費:</span>
                    <input class="form-control" type="number"  name="id_people_group_due[]"  value="{$d_p_v.due}">
                </div>
            </div>
            <div class="col-lg-2 no_padding" style="left: 65px">
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">年度:</span>
                    <input class="form-control" type="text"  name="id_people_group_year[]"  value="{$d_p_v.year}" maxlength="8">
                </div>
            </div>
        </div>
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <input type="hidden" name="id_people_group[]" value="">
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">團體:</span>
                <select name="id_people_group_id_company[]"  class="form-control">
                    <option value="">請選擇團體</option>
                    {foreach $database_company as $d_c_k => $d_c_v}
                        {if $d_c_v.type==1}
                            <option value="{$d_c_v.id_company}">{$d_c_v.name}</option>
                        {/if}
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="col-lg-2 no_padding">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">入會日期:</span>
                <input class="form-control" type="date"  name="id_people_group_s_date[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">會員編號:</span>
                <input class="form-control" type="text"  name="id_people_group_number[]"   maxlength="64">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">會費:</span>
                <input class="form-control" type="number"  name="id_people_group_due[]">
            </div>
        </div>
        <div class="col-lg-2 no_padding" style="left: 65px">
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">年度:</span>
                <input class="form-control" type="text"  name="id_people_group_year[]" maxlength="8">
            </div>
        </div>
        <div class="col-lg-1 no_padding" style="left: 65px">
            <button type="button" class="btn btn-secondary " data-table="people_group"  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>專長/技能</label>
<div class="form-group col-lg-12">
    {if !empty($database_data.id_skill)}
        {foreach $database_data.id_skill as $d_d_s_k => $d_d_s_v}
        <div class="form-group col-lg-12">
            <div class="col-lg-12 no_padding" >
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">專長/技能:</span>
                    <span class="input-group-addon">類別:</span>
                    <select name="id_skill_class[]" class="form-control skill_class" >
                        {foreach $datebase_skill_class as $d_s_c_k =>$d_s_c_v}
                            <option data-child="id_skill_classification[]" data-child_value="{$d_s_c_v.id_skill_class}" value="{$d_s_c_v.id_skill_class}" {if $d_d_s_v.s_c_i_id_skill_class==$d_s_c_v.id_skill_class}selected="selected"{/if}>{$d_s_c_v.title}</option>
                        {/foreach}
                    </select>
                    <span class="input-group-addon">分類:</span>
                    <select name="id_skill_classification[]" class="form-control skill_classification">
                        {foreach $datebase_skill_classification as $d_s_c_i_k =>$d_s_c_i_v}
                            <option value="{$d_s_c_i_v.id_skill_classification}" {if $d_d_s_v.id_skill_classification==$d_s_c_i_v.id_skill_classification}selected="selected"{/if}>{$d_s_c_i_v.title}</option>
                        {/foreach}
                    </select>
                    <span class="input-group-addon">技能:</span>
                    <select name="id_skill[]"  class="form-control skill">
                        {foreach $datebase_skill as $d_s_k =>$d_s_v}
                            <option value="{$d_s_v.id_skill}"  {if $d_d_s_v.id_skill==$d_s_v.id_skill}selected="selected"{/if}>{$d_s_v.title}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <div class="col-lg-12 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">專長/技能:</span>
                <span class="input-group-addon">類別:</span>
                <select name="id_skill_class[]" class="form-control skill_class">
                    <option >請選擇技能類別</option>
                    {foreach $datebase_skill_class as $d_s_c_k =>$d_s_c_v}
                        <option data-child="id_skill_classification[]" data-child_value="{$d_s_c_v.id_skill_class}"  value="{$d_s_c_v.id_skill_class}">{$d_s_c_v.title}</option>
                    {/foreach}
                </select>
                <span class="input-group-addon">分類:</span>
                <select name="id_skill_classification[]"  class="form-control skill_classification">
                    <option >請選擇技能分類</option>
                    {foreach $datebase_skill_classification as $d_s_c_i_k =>$d_s_c_i_v}
                        <option  value="{$d_s_c_i_v.id_skill_classification}">{$d_s_c_i_v.title}</option>
                    {/foreach}
                </select>
                <span class="input-group-addon">技能:</span>
                <select name="id_skill_classification[]"  class="form-control skill">
                    <option >請選擇技能</option>
                    {foreach $datebase_skill as $d_s_k =>$d_s_v}
                        <option  value="{$d_s_v.id_skill}">{$d_s_v.title}</option>
                    {/foreach}
                </select>
                <span class="input-group-addon" data-table="skill" data-type=""  onclick="add_personnel_data(this)" >+</span>
            </div>
        </div>
    </div>
</div>


<label>興趣</label>
<div class="form-group col-lg-12">
    <div class="form-group col-lg-12">
        <div class="col-lg-12 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">興趣:</span>
                <input class="form-control" type="text"  name="interest" value="{$database_data.interest}" maxlength="255">
            </div>
        </div>
    </div>
</div>

<label>專業訓練</label>
<div class="form-group col-lg-12">
    {if !empty($database_data.training_class)}
        {foreach $database_data.training_class as $d_t_k => $d_t_v}
        <div class="form-group col-lg-12">
            <div class="col-lg-4 no_padding" >
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">課程名稱:</span>
                    <input class="form-control" type="text" name="training_class[]" value="{$d_t_v}" maxlength="64">
                </div>
            </div>
            <div class="col-lg-4 no_padding" >
                <div class="input-group fixed-width-lg">
                    <span class="input-group-addon">技能:</span>
                    <input class="form-control" type="text" name="training_skill[]" value="{$database_data.training_skill[$d_t_k]}" maxlength="64">
                </div>
            </div>
        </div>
        {/foreach}
    {/if}
    <div class="form-group col-lg-12">
        <div class="col-lg-4 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">課程名稱:</span>
                <input class="form-control" type="text" name="training_class[]" maxlength="64">
            </div>
        </div>
        <div class="col-lg-4 no_padding" >
            <div class="input-group fixed-width-lg">
                <span class="input-group-addon">技能:</span>
                <input class="form-control" type="text" name="training_skill[]" maxlength="64">
            </div>
        </div>
        <div class="col-lg-1 no_padding">
            <button type="button" class="btn btn-secondary " data-table="training"  data-type=""  onclick="add_personnel_data(this)" >+</button>
        </div>
    </div>
</div>
<label>生涯觀點</label>
<br>
<label>一.在您過去的工經驗中獲得甚麼?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_work" id="career_work">{$database_data.career_work}</textarea>
</div>
<label>二.個人對於未來的生涯規劃?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_future" id="career_future">{$database_data.career_future}</textarea>
</div>
<label>三.形容一下您所期待的工作環境及工作性質?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_expect" id="career_expect">{$database_data.career_expect}</textarea>
</div>
<label>四.如果我們有緣成為工作夥伴,您認為和工作團隊間彼此的共創空間是甚麼?</label>
<div class="form-group col-lg-12">
    <textarea class="form-control" rows="3" name="career_partner" id="career_partner">{$database_data.career_partner}</textarea>
</div>
