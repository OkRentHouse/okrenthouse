<div class="cms{if $display_header} cms_header{/if}{if $display_footer} cms_foote{/if}">{$html}</div>
{if !empty($cms_css)}
	<style>
		{$cms_css}
	</style>
{/if}
{if !empty($cms_js)}
	<script>
        {$cms_js}
	</script>
{/if}