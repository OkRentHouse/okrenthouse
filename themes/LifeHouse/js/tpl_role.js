
function add_field_role(e) {//姬本上幾個關係人
    let data_name = $(e).data('name');
    let count  = $(e).data('count');
    let table = $(e).data('table');

    if(data_name == 'member_role_agent_ag' || data_name == 'member_role_relation_ag'){
        let name = $(e).prev().attr('name');
        $(e).parent().parent().append('<div class="input-group fixed-width-lg">\n' +
            '                        <input class="form-control" type="text" name="'+table+'_ag_'+count+'[]"  value=""  maxlength="32">\n' +
            '                        <span class="input-group-addon" data-name="'+table+'_ag" data-count="'+count+'" onclick="delete_field_role(this)">-</span>\n' +
            '                    </div>');
    }else if(data_name == 'active_off' || data_name == 'active_off'){
        $(e).parent().parent().append('<div class="input-group fixed-width-lg">\n' +
            '                        <span class="input-group-addon">下架時間</span>\n' +
            '                            <input class="form-control" type="date" name="'+table+'_active_off_date_'+count+'[]"  value="">\n' +
            '                        <span class="input-group-addon">原因</span>\n' +
            '                            <input class="form-control" type="text" name="'+table+'_active_off_reason_'+count+'[]"  value=""  maxlength="64">\n' +
            '                        <span class="input-group-addon">人員</span>\n' +
            '                            <input class="form-control" type="text" name="'+table+'_active_off_admin_'+count+'[]" value="" maxlength="32">\n' +
            '                        <span class="input-group-addon" data-table="'+table+'" data-name="active_off" data-count="'+count+'" onclick="delete_field_role(this)" >-</span>\n' +
            '                    </div>');
    }
}

function delete_field_role(e){
    let data_name = $(e).data('name');
    if(data_name == 'member_role_agent_ag' || data_name == 'member_role_relation_ag') {
        $(e).prev().remove();
        $(e).remove();
    }else if(data_name == 'active_off'){
        $(e).parent().remove();
    }
}

function collapse_data(e){
    $(e).next().toggle();
}