$(document).ready(function () {
    ajax_work();
    $("#lease_period").css("width", "120px");
    Business_office(); //確認商辦
    //Choose_God(); //可設立神
    //CarParking(); //車位
    id_rent_house_types_1_2(); //透天別墅 才有庭院
    device_category();
    swimming_pool_type();
    power_electric();
    //map auto work


    // if(($("#latitude").val()==undefined || $("#latitude").val()=='')  ||  ($("#longitude").val()==undefined || $("#longitude").val()=='') ){
    //     lat_log();
    // }

    $(".part_address_add").change(function () { //會影響部分地址
        lat_log();
    });


    $("#public_utilities_1").change(function () {
        swimming_pool_type();
    });
    $("#device_category_33").change(function () {
        power_electric();
        console.log("點到了");
    });


    $("#id_main_house_class").change(function () {
        var id_main_house_class = $("#id_main_house_class").val();
        $("input[name='id_device_category[]']").parent().parent().parent().parent().hide();
        if (id_main_house_class == '1') {
            $(".device_category_1").parent().parent().show();
            $(".device_category_2").parent().parent().show();
        } else if (id_main_house_class == '2') {
            $(".device_category_3").parent().parent().show();
        } else if (id_main_house_class == '3') {} else {
            $(".device_category_1").parent().parent().show();
            $(".device_category_2").parent().parent().show();
            $(".device_category_3").parent().parent().show();
        }
        $(".device_category_4").parent().parent().show();
    });

    if (location.search.search("addrent_house") != '-1') { //代表有搜索到 預設新增checked
        // $("input[name='genre']").each(function(i) {//物件類型預設水泥
        //     if($(this).val()=='5'){
        //         $(this).attr("checked", true);
        //         $(this).parent().addClass("active");
        //     }
        // });


        $("input[name='id_device_category[]']").each(function (i) { //設備類別預設有床組
            if ($(this).val() == '1' || $(this).val() == '7' || $(this).val() == '9' || $(this).val() == '10') {
                $(this).attr("checked", true);
                $(this).parent().addClass("active");
            }
        });
        $("input[name='id_other_conditions[]']").each(function (i) { //其他條件預設有
            if ($(this).val() == '21' || $(this).val() == '22' || $(this).val() == '37' || $(this).val() == '38') {
                $(this).attr("checked", true);
                $(this).parent().addClass("active");
            }
        });
    }
    //剩餘數字 start
    title_p();
    case_name();
    //剩餘數字 end

    $("#title").keyup(function () {
        title_p();
    });

    $("#case_name").keyup(function () {
        case_name();
    });

    function title_p() {
        var title = $("#title").val().length;
        if (Number(title) <= 14 || title == undefined) {
            var last_number = 14 - Number(title);
            $("#title_remind").html("您剩餘" + last_number + "字數");
        } else {
            var last_number = Number(title) - 14;
            $("#title_remind").html("您已超過" + last_number + "字數");
        }
    }

    function case_name() {
        var case_name = $("#case_name").val().length;
        if (Number(case_name) <= 14 || case_name == undefined) {
            var last_number = 14 - Number(case_name);
            $("#case_name_remind").html("您剩餘" + last_number + "字數");
        } else {
            var last_number = Number(title) - 14;
            $("#case_name_remind").html("您已超過" + last_number + "字數");
        }
    }

    //ajax
    var deposit = $("#deposit").val();
    if (deposit == '' || deposit == null) {
        $("#deposit").val("2"); //給預設2
    }

    var id_rent_house_community = $("#id_rent_house_community").val();
    if (id_rent_house_community != '') { //代表有被選擇
        ajax_work('first'); //有選擇則套入該規則
    }
    /*
    $("#rent_house_code").on("click", function() {
        ajax_rent_code();
    });

    function ajax_rent_code(){
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': true,
                'action': 'RentCode',
                'rent_house_code' : $("#rent_house_code").val(),
                'id_rent_house' : $("#id_rent_house").val()
            },
            dataType: 'json',
            success: function (data) {
                if(data["error"] !=""){
                    alert(data["error"]);
                }else{
                    if(data["return"]==0){
                        $("button[name='submitEditrent_house']").attr("disabled",false);
                        $("button[name='submitEditrent_houseAndStay']").attr("disabled",false);
                        alert("請輸入非重複的編號");
                    }else{
                        $("button[name='submitEditrent_house']").attr("disabled",true);
                        $("button[name='submitEditrent_houseAndStay']").attr("disabled",true);
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }
*/

    function ajax_work(str = '') { //ajax 自動帶入社區相關資料
        var id_rent_house_community = '';
        if (str == 'first') { //只有第一次
            id_rent_house_community = $("#id_rent_house_community").val();
        }

        var community_arr = ['id_rent_house_community', 'rent_house_community_code', 'community', 'all_num', 'id_exterior_wall', 'household_num', 'storefront_num', 'property_management', 'community_phone', 'community_fax', 'builders', 'redraw_area',
            'clean_time', 'time_s', 'time_e', 'id_public_utilities', 'e_school', 'j_school', 'park', 'market', 'night_market', 'supermarket', 'shopping_center', 'hospital', 'bus', 'bus_station', 'passenger_transport', 'passenger_transport_station',
            'train', 'mrt', 'mrt_station', 'high_speed_rail', 'interchange'
        ];
        console.log(community_arr);
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'Community',
                'id_rent_house_community': id_rent_house_community,
                'community': $("#community").val(),
                'id_rent_house': $("#id_rent_house").val()
            },
            dataType: 'json',
            success: function (data) {
                if (data["error"] != "") {
                    alert(data["error"]);
                } else {
                    for (i = 0; i < community_arr.length; i++) {
                        if (community_arr[i] == 'id_exterior_wall') { //他是用select
                            $("#id_exterior_wall").val(data["return"][community_arr[i]]);
                        } else if (community_arr[i] != 'id_disgust_facility' && community_arr[i] != 'id_public_utilities') {
                            $("input[name='" + community_arr[i] + "']").val(data["return"][community_arr[i]]);
                        } else if (community_arr[i] == 'id_disgust_facility') { //針對 id_disgust_facility 因為它是多選
                            var id_disgust_facility_arr = data["return"]["id_disgust_facility"];
                            $("input[name='id_disgust_facility[]']").parent().removeClass('active');
                            $("input[name='id_disgust_facility[]']").prop('checked', false);
                            for (j = 0; j < document.getElementsByName('id_disgust_facility[]').length; j++) {
                                for (k = 0; k < id_disgust_facility_arr.length; k++) {
                                    // console.log(id_disgust_facility_arr[k]);
                                    // console.log(document.getElementsByName('id_disgust_facility[]')[j].value);
                                    if (document.getElementsByName('id_disgust_facility[]')[j].value == id_disgust_facility_arr[k]) {
                                        document.getElementsByName('id_disgust_facility[]')[j].checked = true;
                                        document.getElementsByName('id_disgust_facility[]')[j].parentNode.className += ' active';
                                    }
                                }
                            }
                        } else if (community_arr[i] == 'id_public_utilities') {
                            var id_public_utilities_arr = data["return"]["id_public_utilities"];
                            $("input[name='id_public_utilities[]']").parent().removeClass('active');
                            $("input[name='id_public_utilities[]']").prop('checked', false);
                            for (j = 0; j < document.getElementsByName('id_public_utilities[]').length; j++) {
                                for (k = 0; k < id_public_utilities_arr.length; k++) {
                                    // console.log(id_public_utilities_arr[k]);
                                    // console.log(document.getElementsByName('id_public_utilities[]')[j].value);
                                    if (document.getElementsByName('id_public_utilities[]')[j].value == id_public_utilities_arr[k]) {
                                        document.getElementsByName('id_public_utilities[]')[j].checked = true;
                                        document.getElementsByName('id_public_utilities[]')[j].parentNode.className += ' active';
                                    }
                                }
                            }
                        }
                    }
                    // console.log(community_arr);
                    // $(".community_data").attr('disabled',true);
                    // $("input[name='id_disgust_facility[]']").parent().attr('disabled',true);
                    // $("input[name='id_disgust_facility[]']").attr('disabled',true);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {}
        });
    }

    // $("#community").change(function() {
    //     var community = $("#community").val(); //點選後
    //     //這邊要把那些跟社區相關的全部設定成鎖定
    //     if (community == '') { //代表有被選擇
    //     } else { //等於空的
    //         ajax_work();
    //     }
    // });

    function furniture_check() {
        $("#other_conditions_21").attr("checked", true);
        $("#other_conditions_21").parent().addClass("active");
    }

    function home_appliance_check() {
        $("#other_conditions_22").attr("checked", true);
        $("#other_conditions_22").parent().addClass("active");
    }

    function add_checkbox($id) {
        $("#other_conditions_" + $id).attr("checked", true);
        $("#other_conditions_" + $id).parent().addClass("active");
    }

    function remove_checkbox($id) {
        $("#other_conditions_" + $id).attr("checked", false);
        $("#other_conditions_" + $id).parent().removeClass("active");
    }

    //傢俱checked start
    $("input[name='id_device_category[]']").change(function () {
        var checked_1 = '0'; //用於判斷是否有checked 以便後面使用　有三個我就給三個參數
        var checked_2 = '0';
        var checked_3 = '0';
        for (i = 0; i < $("input[name='id_device_category[]']").length; i++) {
            if ($("input[name='id_device_category[]']")[i].checked) { //是否被選取
                //class_id=1 是家具 2是電器 3是炊煮
                var class_id = $("input[name='id_device_category[]']")[i].dataset.class_id;
                if (class_id == "1") { //傢俱
                    checked_1++;
                    furniture_check();
                } else if (class_id == "2") { //家電
                    checked_2++;
                    home_appliance_check();
                } else if (class_id == "3") { //炊煮
                    checked_3++;
                    add_checkbox(4);
                }
            }
        }
        //這邊是做如果都沒點選的話則會自動拔掉下面其他條件
        if (checked_1 == "0") {
            remove_checkbox(21);
        } else if (checked_2 == "0") {
            remove_checkbox(22);
        } else if (checked_3 == "0") {
            remove_checkbox(4);
        }
    });

    $(".furniture").change(function () {
        var checked = '0';
        for (i = 0; i < $(".furniture").length; i++) {
            if (document.getElementsByClassName("furniture")[i].value != null && document.getElementsByClassName("furniture")[i].value != '0' &&
                document.getElementsByClassName("furniture")[i].value != '') {
                furniture_check();
                checked = checked + 1;
            }
        }

        if (checked == '0') {
            $("#other_conditions_21").attr("checked", false);
            $("#other_conditions_21").parent().removeClass("active");
        }
    });

    //傢俱checked end

    //家電checked start

    $(".home_appliance").change(function () {
        var checked = '0';
        for (i = 0; i < $(".furniture").length; i++) {
            if (document.getElementsByClassName("home_appliance")[i].value != null && document.getElementsByClassName("home_appliance")[i].value != '0' &&
                document.getElementsByClassName("home_appliance")[i].value != '') {
                home_appliance_check();
                checked = checked + 1;
            }
        }
        if (checked == '0') {
            $("#other_conditions_22").attr("checked", false);
            $("#other_conditions_22").parent().removeClass("active");
        }
    });

    //家電checked end

    $("#ag").change(function () {
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'AG',
                'ag': $("#ag").val()
            },
            dataType: 'json',
            success: function (data) {
                if (data["error"] != "") {
                    alert(data["error"]);
                } else {
                    $("#develop_ag").val(data["return"]["name"]);
                    $("#ag_email").val(data["return"]["email"]);
                    $("#ag_phone").val(data["return"]["phone"]);

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {}
        });
    });

    $("#rent_house_type_20").change(function () {
        Business_office();
    });

    // $("#other_conditions_8").change(function() {
    //     Choose_God();
    // });
    // $("#other_conditions_1").change(function() {
    //     CarParking();
    // });
    // $("#other_conditions_12").change(function() {
    //     Move_household();
    // });
    $("#id_rent_house_types").change(function () {
        id_rent_house_types_1_2();
    });

    $("#complete_address").change(function () {
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        show_add($(this).val(), $(this), arf);
    });
    $("#member_landlord_id_address_domicile_address").change(function () {
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_domicile_" + arf[index]));
        show_add($(this).val(), $(this), arf);
    });
    $("#member_landlord_id_address_house_address").change(function () {
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_house_" + arf[index]));
        show_add($(this).val(), $(this), arf);
    });
    $("#member_landlord_id_address_company_address").change(function () {
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_company_" + arf[index]));
        show_add($(this).val(), $(this), arf);
    });
    $("#member_landlord_id_address_contact_address").change(function () {
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_contact_" + arf[index]));
        show_add($(this).val(), $(this), arf);
    });
    $("[name='member_role_agent_id_address_domicile_address[]']").change(function () {
        var main_name = "member_role_agent_id_address_domicile_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });
    $("[name='member_role_agent_id_address_contact_address[]']").change(function () {
        var main_name = "member_role_agent_id_address_contact_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });
    $("[name='member_role_agent_id_address_house_address[]']").change(function () {
        var main_name = "member_role_agent_id_address_house_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });
    $("[name='member_role_agent_id_address_company_address[]']").change(function () {
        var main_name = "member_role_agent_id_address_company_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });
    $("[name='member_role_relation_id_address_domicile_address[]']").change(function () {
        var main_name = "member_role_relation_id_address_domicile_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });
    $("[name='member_role_relation_id_address_contact_address[]']").change(function () {
        var main_name = "member_role_relation_id_address_contact_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });
    $("[name='member_role_relation_id_address_house_address[]']").change(function () {
        var main_name = "member_role_relation_id_address_house_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });
    $("[name='member_role_relation_id_address_company_address[]']").change(function () {
        var main_name = "member_role_relation_id_address_company_";
        $(this).prop("id", main_name + "address");
        console.log($(this).prop("id"));
        var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
        const arfCBC = (element, index) => {
            arf[index] = main_name + arf[index];
            $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
            console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
            show_add($(this).val(), $(this), arf);
        }
        arf.forEach(arfCBC);
        //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
    });


    $("input[name='member_landlord_user[]'],input[name='member_landlord_name[]']").change(function () {
        let member_landlord_name = $("input[name='member_landlord_name[]']").val();
        let member_landlord_user = $("input[name='member_landlord_user[]']").val();
        // member_landlord
        if (member_landlord_name != '' && member_landlord_name != undefined && member_landlord_user != '' && member_landlord_user != undefined) {
            member_landlord(member_landlord_name, member_landlord_user);
        }
    });

    $("input[name='member_role_agent_user[]'],input[name='member_role_agent_name[]']").change(function () {
        let member_role_agent_name = $("input[name='member_role_agent_name[]']").val();
        let member_role_agent_user = $("input[name='member_role_agent_user[]']").val();
        // member_landlord
        if (member_role_agent_name != '' && member_role_agent_name != undefined && member_role_agent_user != '' && member_role_agent_user != undefined) {
            member_role(member_role_agent_name, member_role_agent_user, 'member_role_agent');
        }
    });

    $("input[name='member_role_relation_user[]'],input[name='member_role_relation_name[]']").change(function () {
        let member_role_relation_name = $("input[name='member_role_relation_name[]']").val();
        let member_role_relation_user = $("input[name='member_role_relation_user[]']").val();
        // member_landlord
        if (member_role_relation_name != '' && member_role_relation_name != undefined && member_role_relation_user != '' && member_role_relation_user != undefined) {
            member_role(member_role_relation_name, member_role_relation_user, 'member_role_relation');
        }
    });
});




/* 1217 */
/* 自動 地址 插入程式，提高User輸入地址快速放入對應欄位 */

$("#complete_address").change(function () {
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    show_add($(this).val(), $(this), arf);
});
$("#member_landlord_id_address_domicile_address").change(function () {
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_domicile_" + arf[index]));
    show_add($(this).val(), $(this), arf);
});
$("#member_landlord_id_address_house_address").change(function () {
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_house_" + arf[index]));
    show_add($(this).val(), $(this), arf);
});
$("#member_landlord_id_address_company_address").change(function () {
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_company_" + arf[index]));
    show_add($(this).val(), $(this), arf);
});
$("#member_landlord_id_address_contact_address").change(function () {
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    arf.forEach((element, index) => (arf[index] = "member_landlord_id_address_contact_" + arf[index]));
    show_add($(this).val(), $(this), arf);
});
$("[name='member_role_agent_id_address_domicile_address[]']").change(function () {
    var main_name = "member_role_agent_id_address_domicile_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});
$("[name='member_role_agent_id_address_contact_address[]']").change(function () {
    var main_name = "member_role_agent_id_address_contact_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});
$("[name='member_role_agent_id_address_house_address[]']").change(function () {
    var main_name = "member_role_agent_id_address_house_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});
$("[name='member_role_agent_id_address_company_address[]']").change(function () {
    var main_name = "member_role_agent_id_address_company_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});
$("[name='member_role_relation_id_address_domicile_address[]']").change(function () {
    var main_name = "member_role_relation_id_address_domicile_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});
$("[name='member_role_relation_id_address_contact_address[]']").change(function () {
    var main_name = "member_role_relation_id_address_contact_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});
$("[name='member_role_relation_id_address_house_address[]']").change(function () {
    var main_name = "member_role_relation_id_address_house_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});
$("[name='member_role_relation_id_address_company_address[]']").change(function () {
    var main_name = "member_role_relation_id_address_company_";
    $(this).prop("id", main_name + "address");
    console.log($(this).prop("id"));
    var arf = ["id_county", "id_county", "id_city", "id_city", "id_city", "id_city", "road", "segment", "road", "village", "village", "neighbor", "lane", "alley", "no", "floor", "floor", "floor", "address_room", "address_room", "address_room"];
    const arfCBC = (element, index) => {
        arf[index] = main_name + arf[index];
        $("[name='" + arf[index] + "[]']").prop("id", arf[index]);
        console.log(arf[index] + "[] : " + $("#" + arf[index]).prop("id"));
        show_add($(this).val(), $(this), arf);
    }
    arf.forEach(arfCBC);
    //arf.forEach( (element,index) => ( arf[index]=main_name+arf[index] ));
});



var address = [];

function show_add(value, e, arf) {
    var city = dist = road = street = village = Alley = lanes = number = floor = room = value;
    var number_dash = floor_dash = room_dash = "";
    var newadd = [];
    address = new Array;

    value = value.replaceAll("-", "之");
    value = value.toLowerCase();
    var findto = ["縣", "市", "鎮", "鄉", "區", "市", "路", "段", "街", "村", "里", "鄰", "巷", "弄", "號", "樓", "f", "floor", "室", "r", "room"];
    //var findto_f=["id_county","id_county","id_city","id_city","id_city","id_city","road","segment","road","village","village","neighbor","lane","alley","no","floor","floor","floor","address_room","address_room","address_room"];
    var findto_f = arf;
    $("#" + $(e).prop("id")).val(value);
    for (var fi = 0; fi < findto.length; fi++) {
        address.push(finditappend(value, findto[fi], findto_f[fi])); //console.log(finditappend(value,findto[fi],findto_f[fi]));
        value = value.replace(address[fi], "");
        if (address[fi] != "" && $("#" + findto_f[fi])) {
            if ($("#" + findto_f[fi]).prop("tagName") == "SELECT") {
                $("#" + findto_f[fi]).find("option").each(function (inx, va) {
                    if ($(va).text() == address[fi]) {
                        $(va).prop("selected", "true")
                    };
                })
            } else {
                if (address[fi].indexOf("之") < 0) {
                    $("#" + findto_f[fi]).val(address[fi])
                }
            }
        };

    }

}

function finditappend(v, f, _f) {
    if (v.indexOf(f, 0) > 0) {
        if (v.indexOf("之", 0) == 0 || v.indexOf("-", 0) == 0) {
            return v.substring(1, v.indexOf(f, 1)) + f;
        } else if (v.indexOf("之", 0) > 0) {
            //console.log("之處理："+v.indexOf("之",0) +"~"+ v.indexOf(f,0));
            //if(v.indexOf("之",0) > v.indexOf("-",0)){dash = "之"}else{dash = "-"}; //判斷是 之 還是 -
            if (v.indexOf("之", 0) < v.indexOf(f, 0)) { //如果 之 小於 目標物 , ex 300之3(v)
                $("#" + _f).val(v.substring(0, v.indexOf("之", 0)) + f);
                $("#" + _f + "_her").val(v.substring(v.indexOf("之", 0) + 1, v.indexOf(f, v.indexOf("之", 0))));
                return v.substring(0, v.indexOf("之", 0)) + "之" + v.substring(v.indexOf("之", 0) + 1, v.indexOf(f, v.indexOf("之", 0))) + f;

            } else if (v.indexOf("之", 0) == v.indexOf(f, 0) + 1) { //如果 之 大於 目標物 & 緊接在後 , ex 3(v)之8
                $("#" + _f).val(v.substring(0, v.indexOf("之", 0) - 1) + f);
                $("#" + _f + "_her").val(parseInt(v.substring(v.indexOf("之", 0) + 1, v.indexOf("之", 0) + 3)));
                return v.substring(0, v.indexOf("之", 0) - 1) + f + "之" + parseInt(v.substring(v.indexOf("之", 0) + 1, v.indexOf("之", 0) + 3));

            }
        }
        return v.substring(0, v.indexOf(f, 0)) + f;
    } else {
        return ""
    }
}

/* 1217 END */

function Business_office() {
    if ($("#rent_house_type_20").parent().hasClass("active")) { //商辦是否備選取
        $("#width").parent().parent().parent().show();
        $("#height").parent().parent().parent().show();
        $("#road_width").parent().parent().parent().show();
    } else {
        $("#width").parent().parent().parent().hide();
        $("#height").parent().parent().parent().hide();
        $("#road_width").parent().parent().parent().hide();
    }
}


// function Choose_God(){
//     if($("#other_conditions_8").parent().hasClass("active")){//商辦是否備選取
//         $("#choose_god1").show();
//         $("#god_1").parent().parent().parent().parent().show();
//         //$("#choose_god2").parent().parent().parent().show();
//         $("#choose_god12").parent().parent().parent().parent().show();
//     }else{
//         $("#choose_god1").hide();
//         $("#god_2").parent().parent().parent().parent().hide();
//         //$("#choose_god2").parent().parent().parent().hide();
//         //$("#choose_god12").parent().parent().parent().hide();
//         $("#choose_god12").parent().parent().parent().parent().hide();
//     }
// }


// function CarParking(){
//     if($("#other_conditions_1").parent().hasClass("active")){//車位是否備選取
//         $("#car_1").parent().parent().parent().parent().show();
//         $("#choose_car_park").parent().parent().parent().parent().show();
//     }else{
//         $("#car_2").parent().parent().parent().parent().hide();
//         $("#choose_car_park").parent().parent().parent().parent().hide();
//     }
// }




// function Move_household(){
//     if($("#other_conditions_12").parent().hasClass("active")){//商辦是否備選取
//         $("#choose_god1").show();
//         $("#god_1").parent().parent().parent().parent().show();
//         //$("#choose_god2").parent().parent().parent().show();
//         $("#choose_god12").parent().parent().parent().parent().show();
//     }else{
//         $("#choose_god1").hide();
//         $("#god_2").parent().parent().parent().parent().hide();
//         //$("#choose_god2").parent().parent().parent().hide();
//         //$("#choose_god12").parent().parent().parent().hide();
//         $("#choose_god12").parent().parent().parent().parent().hide();
//     }
// }

function id_rent_house_types_1_2() {
    if ($("#id_rent_house_types").val() == 1 || $("#id_rent_house_types").val() == 2) { //透天或別墅
        $("#patio").parent().parent().show();
        $("#patio_remarks").parent().parent().show();
        $(".patio_name").parent().show();
        $("input[name='elevator']").parent().parent().parent().parent().show();
    } else {
        $("#patio").parent().parent().hide();
        $("#patio_remarks").parent().parent().hide();
        $(".patio_name").parent().hide();
        $("input[name='elevator']").parent().parent().parent().parent().hide();
    }
}

function device_category() {
    console.log($("#id_rent_house").val());
    if ($("#id_rent_house").val() != '' && $("#id_rent_house").val() != undefined) {
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'DeviceCategory',
                'id_rent_house': $("#id_rent_house").val(),
            },
            dataType: 'json',
            success: function (data) {
                if (data["error"] != "") {
                    alert(data["error"]);
                } else {
                    $(data["return"]["id_device_category"]).each(function (index_head, value_head) {
                        $("input[name='id_device_category[]']").each(function (index, value) { //測測看
                            if ($(value).attr("value") == value_head) {
                                $(value).attr("checked", 'checked');
                                $(value).parent().addClass('active');
                            }
                        });
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {}
        });
    }
}

function swimming_pool_type() {
    if ($("#public_utilities_1").attr("checked") || $("#public_utilities_1").parent().hasClass("active")) {
        $("input[name='swimming_pool_type']").parent().parent().parent().parent().show();
    } else {
        $("input[name='swimming_pool_type']").parent().parent().parent().parent().hide();
        $("input[name='swimming_pool_type']").attr("checked", false);
    }
}

function power_electric() {
    if ($("#device_category_33").attr("checked") || $("#device_category_33").parent().hasClass("active")) {
        $("input[name='power_electric']").parent().parent().parent().show();
    } else {
        $("input[name='power_electric']").parent().parent().parent().hide();
        $("input[name='power_electric']").attr("checked", false);
    }
}

function member_landlord(member_landlord_name, member_landlord_user) {
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': true,
            'action': 'Memberlandlord',
            'name': member_landlord_name,
            'user': member_landlord_user,
        },
        dataType: 'json',
        success: function (data) {
            if (data["error"] != "") {
                alert(data["error"]);
            } else {
                console.log(data);
                // data.forEach(function(item,index,array){
                //     console.log(item);
                //     console.log(index);
                //     console.log(array);
                // })

                $.each(data['return'], function (index, val) {
                    if (index == 'id_address_domicile_arr' || index == 'id_address_contact_arr' || index == 'id_address_house_arr' || index == 'id_address_company_arr') {
                        $.each(val, function (val_index, val_v) {
                            let address = index.substring(0, index.length - 4);
                            if (val_index == 'id_address') {
                                //上面已經放入了
                            } else if (val_index == 'id_city' || val_index == 'id_county') {
                                $("select[name='member_landlord_" + address + '_' + val_index + "[]']").val(val_v);
                            } else {
                                $("input[name='member_landlord_" + address + '_' + val_index + "[]']").val(val_v);
                            }
                        });
                    } else if (index == 'id_source' || index == 'appellation') {
                        $("select[name='member_landlord_" + index + "[]']").val(val);
                    } else {
                        $("input[name='member_landlord_" + index + "[]']").val(val);
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {}
    });
}

function member_role(name, user, role) {
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': true,
            'action': role,
            'name': name,
            'user': user,
        },
        dataType: 'json',
        success: function (data) {
            if (data["error"] != "") {
                alert(data["error"]);
            } else {
                $.each(data['return'], function (index, val) {
                    if (index == 'id_address_domicile_arr' || index == 'id_address_contact_arr' || index == 'id_address_house_arr' || index == 'id_address_company_arr') {
                        $.each(val, function (val_index, val_v) {
                            let address = index.substring(0, index.length - 4);
                            if (val_index == 'id_address') {
                                //上面已經放入了
                            } else if (val_index == 'id_city' || val_index == 'id_county') {
                                $("select[name='" + role + "_" + address + '_' + val_index + "[]']").val(val_v);
                            } else {
                                $("input[name='" + role + "_" + address + '_' + val_index + "[]']").val(val_v);
                            }
                        });
                    } else if (index == 'id_source' || index == 'appellation') {
                        $("select[name='" + role + "_" + index + "[]']").val(val);
                    } else {
                        $("input[name='" + role + "_" + index + "[]']").val(val);
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {}
    });
}

function lat_log() {
    let neighbor = '';
    let segment = '';
    let lane = '';
    if ($("#neighbor").val() != undefined && $("#neighbor").val() != '') {
        neighbor = $("#neighbor").val() + '鄰';
    }
    if ($("#segment").val() != undefined && $("#segment").val() != '') {
        segment = $("#neighbor").val() + '段';
    }
    if ($("#lane").val() != undefined && $("#lane").val() != '') {
        lane = $("#neighbor").val() + '巷';
    }
    let part_address = $("#id_county").find("option:selected").text() + $("#id_city").find("option:selected").text() + $("#village").val() + neighbor + $("#road").val() + segment + lane;
    $("#part_address").val(part_address);
}