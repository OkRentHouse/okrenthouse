$(document).ready(function(){
    $(".county").change(function(){
            let city_html = '';
            let data_this = $(this).parent().parent().find(".city");
            $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': false,
                    'action': 'City',
                    'id_county': $(this).val()
                },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if (data["error"] != "") {
                        alert(data["error"]);
                    } else {
                        data_this.empty();//去除現有的
                        data['return'].forEach(function(val,index){
                            city_html +='<option value="'+val['id+city']+'">'+val['city_name']+'</option>';
                        });
                        data_this.html(city_html);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
    });
});