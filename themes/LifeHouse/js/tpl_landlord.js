$(document).ready(function(){
    $(".county").change(function(){
        let city_html = '';
        let data_this = $(this).parent().parent().find(".city");
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'City',
                'id_county': $(this).val()
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data["error"] != "") {
                    alert(data["error"]);
                } else {
                    data_this.empty();//去除現有的
                    data['return'].forEach(function(val,index){
                        city_html +='<option value="'+val['id_city']+'">'+val['city_name']+'</option>';
                    });
                    data_this.html(city_html);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });
    });
});
// function add_landlord(e) {
//     let member_landlord_leng = $(".member_landlord").length+1;
//     let county = '';
//     let city = '';
//     let source = '';
//     let rent_mange_program = '';
//     let intermediary_program = '';
//     let other_conditions = '';
//     let rent_union_type = '';
//     let pay_type = '';
//
//     $.ajax({
//         url: document.location.pathname,
//         method: 'POST',
//         data: {
//             'ajax': true,
//             'action': 'add_landlord',
//         },
//         dataType: 'json',
//         success: function(data) {
//             if (data["error"] != "") {
//                 alert(data["error"]);
//             } else {
//                 let data = '<div class="panel panel-default member_landlord">\n' +
//                     '    <div class="panel-heading"><i class="icon-cogs"></i>過往房東<i class="btn btn-primary" onclick="add_landlord(this);">+</i></div>\n' +
//                     '    <div class="panel-body">\n' +
//                     '        <div class="form_data">\n' +
//                     '                <input type="hidden" name="id_member_landlord[]"  class="" value="">\n' +
//                     '                <input type="hidden" name="member_landlord_id_member[]"  class="" value="">\n' +
//                     '\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_name">房東</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_name[]"  value="" maxlength="20">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_user">手機</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_user[]"  value=""  maxlength="20">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_en_name">房東英文</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_en_name[]"  value="" maxlength="64">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_tel">備用手機</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_tel[]"  value=""  maxlength="20">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_appellation">稱謂</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <select class="form-control" name="member_landlord_appellation[]" >\n' +
//                     '                        <option value="">請選擇稱謂</option>\n' +
//                     '                        <option value="0">先生</option>\n' +
//                     '                        <option value="1">小姐</option>\n' +
//                     '                        <option value="2">太太</option>\n' +
//                     '                        <option value="3">媽媽</option>\n' +
//                     '                        <option value="4">伯伯</option>\n' +
//                     '                    </select>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_id_source">來源</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <select class="form-control" name="member_landlord_id_source[]" >\n' +
//                     '                        <option value="">請選擇來源</option>\n' +
//                     '                        {foreach $database_source as $d_s_k => $d_s_v}\n' +
//                     '                            <option value="{$d_s_v.id_source}">{$d_s_v.source}</option>\n' +
//                     '                        {/foreach}\n' +
//                     '                    </select>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_birthday">出生年月日</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="date" name="member_landlord_birthday[]"  value="">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_identity">ID</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_identity[]" value=""  maxlength="16">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_line_id">Line ID</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_line_id[]" value=""  maxlength="64">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12"  style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_email">email</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_email[]" value=""  maxlength="100">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_local_tel_1">市話1</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_local_tel_1[]"  value="" maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_local_tel_2">市話2</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_local_tel_2[]" value=""  maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_id_address_domicile">戶籍地址</label>\n' +
//                     '                <input type="hidden" name="member_landlord_id_address_domicile[]" class="" value="">\n' +
//                     '                <div class="form-group col-lg-9">\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_domicile_postal[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_domicile_id_county[]" class="form-control county">\n' +
//                     '                            <option value="">請選擇縣市</option>\n' +
//                     '                            {foreach $database_county as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_domicile_id_city[]" class="form-control city">\n' +
//                     '                            <option value="">請選擇鄉政區</option>\n' +
//                     '                            {foreach $database_city as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-5 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_domicile_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>\n' +
//                     '                            <input type="number" name="member_landlord_id_address_domicile_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_domicile_road[]" class="form-control " value="" maxlength="20">\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-12">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_domicile_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_domicile_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_domicile_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_domicile_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_domicile_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_domicile_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_domicile_floor_her[]"  class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_domicile_address_room[]"  class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-9">\n' +
//                     '                        <input type="text" name="member_landlord_id_address_domicile_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_id_address_contact">連絡地址</label>\n' +
//                     '                <input type="hidden" name="member_landlord_id_address_contact[]" class="" value="">\n' +
//                     '                <div class="form-group col-lg-9">\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_contact_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_contact_id_county[]" class="form-control county">\n' +
//                     '                            <option value="">請選擇縣市</option>\n' +
//                     '                            {foreach $database_county as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_contact_id_city[]" class="form-control city">\n' +
//                     '                            <option value="">請選擇鄉政區</option>\n' +
//                     '                            {foreach $database_city as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-5 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_contact_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>\n' +
//                     '                            <input type="number" name="member_landlord_id_address_contact_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_contact_road[]" class="form-control " value="" maxlength="20">\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-12">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_contact_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_contact_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_contact_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_contact_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_contact_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_contact_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_contact_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_contact_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-9">\n' +
//                     '                        <input type="text" name="member_landlord_id_address_contact_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_id_address_house">住家地址</label>\n' +
//                     '                <input type="hidden" name="member_landlord_id_address_house[]" class="" value="">\n' +
//                     '                <div class="form-group col-lg-9">\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_house_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_house_id_county[]" class="form-control county">\n' +
//                     '                            <option value="">請選擇縣市</option>\n' +
//                     '                            {foreach $database_county as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_house_id_city[]" class="form-control city">\n' +
//                     '                            <option value="">請選擇鄉政區</option>\n' +
//                     '                            {foreach $database_city as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-5 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_house_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>\n' +
//                     '                            <input type="number" name="member_landlord_id_address_house_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_house_road[]" class="form-control " value="" maxlength="20">\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-12">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_house_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_house_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_house_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_house_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_house_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_house_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_house_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_house_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-9">\n' +
//                     '                        <input type="text" name="member_landlord_id_address_house_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_id_address_company">公司地址</label>\n' +
//                     '                <input type="hidden" name="member_landlord_id_address_company[]" class="" value="">\n' +
//                     '                <div class="form-group col-lg-9">\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_company_postal[]"  class="form-control " value="" maxlength="11"><span class="input-group-addon">郵遞號</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_company_id_county[]" class="form-control county">\n' +
//                     '                            <option value="">請選擇縣市</option>\n' +
//                     '                            {foreach $database_county as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_county}">{$d_c_v.county_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-2 no_padding">\n' +
//                     '                        <select name="member_landlord_id_address_company_id_city[]" class="form-control city">\n' +
//                     '                            <option value="">請選擇鄉政區</option>\n' +
//                     '                            {foreach $database_city as $d_c_k => $d_c_v}\n' +
//                     '                                <option value="{$d_c_v.id_city}">{$d_c_v.city_name}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="col-lg-5 no_padding">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_company_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>\n' +
//                     '                            <input type="number" name="member_landlord_id_address_company_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>\n' +
//                     '                            <input type="text"   name="member_landlord_id_address_company_road[]" class="form-control " value="" maxlength="20">\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-12">\n' +
//                     '                        <div class="input-group fixed-width-lg">\n' +
//                     '                            <input type="text" name="member_landlord_id_address_company_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_company_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_company_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_company_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_company_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_company_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>\n' +
//                     '                            <span class="input-group-addon">之</span><input type="text" name="member_landlord_id_address_company_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
//                     '                            <input type="text" name="member_landlord_id_address_company_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>\n' +
//                     '                        </div>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="form-group col-lg-9">\n' +
//                     '                        <input type="text" name="member_landlord_id_address_company_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_company">公司名</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_company[]"  value="" maxlength="64">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_job">公司職稱</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_job[]"  value=""  maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_bank_account">銀行帳號</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_bank_account[]"  value="" maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_bank_code">銀行代碼</label>\n' +
//                     '                <div class="form-group col-lg-3">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_bank_code[]"  value=""  maxlength="4">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_bank">銀行</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_bank[]"  value="" maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_branch">分行</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_branch[]"  value=""  maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_bank_account_2">銀行帳號2</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_bank_account_2[]"  value="" maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_bank_code_2">銀行代碼2</label>\n' +
//                     '                <div class="form-group col-lg-3">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_bank_code_2[]"  value=""  maxlength="4">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_bank_2">銀行2</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_bank_2[]"  value="" maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_branch_2">分行2</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="text" name="member_landlord_branch_2[]"  value=""  maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_id_other_conditions">其他條件(複選)</label>\n' +
//                     '                <div class="form-group col-lg-9">\n' +
//                     '                    <div class="btn-group" data-toggle="buttons">\n' +
//                     '                    {foreach $database_other_conditions as $d_o_c_k => $d_o_c_v}\n' +
//                     '                        <label class="btn btn-default checkbox" for="member_landlord_id_other_conditions_{$d_o_c_v.id_other_conditions}"> <input type="checkbox" autocomplete="off" name="member_landlord_id_other_conditions_0[]"  value="{$d_o_c_v.id_other_conditions}">{$d_o_c_v.title}</label>\n' +
//                     '                    {/foreach}\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-6" for="member_landlord_reserve_price">底價</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="number" name="member_landlord_reserve_price[]"  value="" maxlength="32">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-6">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_buy_price">入手價</label>\n' +
//                     '                <div class="form-group col-lg-6">\n' +
//                     '                    <input class="form-control" type="number" name="member_landlord_buy_price[]"  value=""  maxlength="4">\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_ag">AG</label>\n' +
//                     '                <div class="col-lg-3 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_ag_0[]"  value=""  maxlength="32">\n' +
//                     '                        <span class="input-group-addon" data-name="member_landlord_ag_$x[]" onclick="add_field_landlord(this)">+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="member_landlord_ag">下架記錄</label>\n' +
//                     '                <div class="col-lg-8 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <span class="input-group-addon">下架時間</span>\n' +
//                     '                            <input class="form-control" type="date" name="member_landlord_active_off_0[]"  value="">\n' +
//                     '                        <span class="input-group-addon">原因</span>\n' +
//                     '                            <input class="form-control" type="text" name="member_landlord_active_off_reason_0[]"  value=""  maxlength="64">\n' +
//                     '                        <span class="input-group-addon">人員</span>\n' +
//                     '                            <input class="form-control" type="text" name="member_landlord_active_off_admin_0[]" value="" maxlength="32">\n' +
//                     '                        <span class="input-group-addon" data-name="active_off" onclick="add_field_landlord(this)" >+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="intermediary_program">仲介方案</label>\n' +
//                     '                <div class="col-lg-7 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <span class="input-group-addon">仲介方案</span>\n' +
//                     '                        <select class="form-control" name="member_landlord_id_intermediary_program_0[]">\n' +
//                     '                            <option value="">請選擇仲介方案</option>\n' +
//                     '                            {foreach $database_intermediary_program as $d_i_p_k => $d_i_p_v}\n' +
//                     '                                <option value="{$d_i_p_v.id_rent_mange_program}">{$d_i_p_v.title}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                        <span class="input-group-addon">仲介PV</span>\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_intermediary_program_pv_0[]"  value=""  maxlength="32">\n' +
//                     '                        <span class="input-group-addon" data-name="intermediary_program" onclick="add_field_landlord(this)" >+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="rent_mange_program">帶租代管</label>\n' +
//                     '                <div class="col-lg-7 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <span class="input-group-addon">帶租代管</span>\n' +
//                     '                        <select class="form-control" name="member_landlord_id_rent_mange_program_0[]">\n' +
//                     '                            <option value="">請選擇帶租代管方案</option>\n' +
//                     '                            {foreach $database_rent_mange_program as $d_r_m_p_k => $d_r_m_p_v}\n' +
//                     '                                <option value="{$d_r_m_p_v.id_rent_mange_program}">{$d_r_m_p_v.title}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                        <span class="input-group-addon">帶租代管PV</span>\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_rent_mange_program_pv_0[]"  value=""  maxlength="32">\n' +
//                     '                        <span class="input-group-addon" data-name="rent_mange_program" onclick="add_field_landlord(this)" >+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="deposit_trust">押金信託</label>\n' +
//                     '                <div class="col-lg-3 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_deposit_trust_0[]"  value=""  maxlength="64">\n' +
//                     '                        <span class="input-group-addon" data-name="member_landlord_deposit_trust_$x[]" onclick="add_field_landlord(this)">+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="guarantee">租霸補助</label>\n' +
//                     '                <div class="col-lg-3 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_guarantee_0[]"  value=""  maxlength="32">\n' +
//                     '                        <span class="input-group-addon" data-name="member_landlord_guarantee_$x[]" onclick="add_field_landlord(this)">+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="credit_certification">信用認證</label>\n' +
//                     '                <div class="col-lg-3 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_credit_certification_0[]"  value=""  maxlength="32">\n' +
//                     '                        <span class="input-group-addon" data-name="member_landlord_credit_certification_$x[]" onclick="add_field_landlord(this)">+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="safe_performance">安全履約</label>\n' +
//                     '                <div class="col-lg-3 no_padding">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_safe_performance_0[]"  value=""  maxlength="32">\n' +
//                     '                        <span class="input-group-addon" data-name="member_landlord_safe_performance_$x[]" onclick="add_field_landlord(this)">+</span>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '\n' +
//                     '            <div class="form-group col-lg-12" style="margin: auto -23px;">\n' +
//                     '                <label class="control-label col-lg-3" for="\tid_rent_union_pay">組管公會</label>\n' +
//                     '                <div class="col-lg-8 no_padding">\n' +
//                     '                    <input class="form-control" type="hidden" name="member_landlord_id_rent_union_pay_0[]"  value="">\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <span class="input-group-addon">會期</span>\n' +
//                     '                        <input class="form-control" type="date" name="member_landlord_member_period_s_0[]"  value=""  >\n' +
//                     '                        <span class="input-group-addon">~</span>\n' +
//                     '                        <input class="form-control" type="date" name="member_landlord_member_period_e_0[]" value="" >\n' +
//                     '                        <span class="input-group-addon" data-name="member_landlord_rent_union_pay_$x[]" onclick="add_field_landlord(this)">+</span>\n' +
//                     '                    </div>\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <select class="form-control" name="member_landlord_id_rent_union_type_0[]">\n' +
//                     '                            <option value="">請選擇租賃公會</option>\n' +
//                     '                            {foreach $database_rent_union_type as $d_r_u_n_k => $d_r_u_n_v}\n' +
//                     '                                <option value="{$d_r_u_n_v.id_rent_union_type}">{$d_r_u_n_v.title}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                        <span class="input-group-addon">會員編號</span>\n' +
//                     '                        <input class="form-control" type="text" name="member_landlord_member_number_0[]" value="" >\n' +
//                     '                    </div>\n' +
//                     '                    <div class="input-group fixed-width-lg">\n' +
//                     '                        <span class="input-group-addon">會費</span>\n' +
//                     '                        <input class="form-control" type="number" name="member_landlord_member_price_0[]" value="" >\n' +
//                     '                        <span class="input-group-addon">繳費</span>\n' +
//                     '                        <select class="form-control" name="member_landlord_id_pay_type_0[]">\n' +
//                     '                            <option value="">請選擇繳費方式</option>\n' +
//                     '                            {foreach $database_pay_type as $d_p_t_k => $d_p_t_v}\n' +
//                     '                                <option value="{$d_p_t_v.id_pay_type}">{$d_p_t_v.title}</option>\n' +
//                     '                            {/foreach}\n' +
//                     '                        </select>\n' +
//                     '                    </div>\n' +
//                     '                </div>\n' +
//                     '            </div>\n' +
//                     '        </div>\n' +
//                     '    </div>\n' +
//                     '</div>';
//                 console.log(data);
//                 data['return']['database_county'].forEach((item,index)=>{
//                     console.log(item);//value
//                     console.log(index);//key
//                 })
//             }
//         },
//         error: function(xhr, ajaxOptions, thrownError) {}
//     });
//     console.log("get_landlord");
// }

// function delete_landlord(e) {
//     $(e).remove();
// }
function add_field_landlord(e) {//姬本上幾個房東是抓
    let data_name = $(e).data('name');
    let count  = $(e).data('count');
    if(data_name == 'member_landlord_ag'){
        let name = $(e).prev().attr('name');
        $(e).parent().parent().append('<div class="input-group fixed-width-lg">\n' +
            '                        <input class="form-control" type="text" name="member_landlord_ag_'+count+'[]"  value=""  maxlength="32">\n' +
            '                        <span class="input-group-addon" data-name="member_landlord_ag" data-count="'+count+'" onclick="delete_field_landlord(this)">-</span>\n' +
            '                    </div>');
    }else if(data_name == 'active_off'){
        $(e).parent().parent().append('<div class="input-group fixed-width-lg">\n' +
            '                        <span class="input-group-addon">下架時間</span>\n' +
            '                            <input class="form-control" type="date" name="member_landlord_active_off_date_'+count+'[]"  value="">\n' +
            '                        <span class="input-group-addon">原因</span>\n' +
            '                            <input class="form-control" type="text" name="member_landlord_active_off_reason_'+count+'[]"  value=""  maxlength="64">\n' +
            '                        <span class="input-group-addon">人員</span>\n' +
            '                            <input class="form-control" type="text" name="member_landlord_active_off_admin_'+count+'[]" value="" maxlength="32">\n' +
            '                        <span class="input-group-addon" data-name="active_off" data-count="'+count+'" onclick="delete_field_landlord(this)" >-</span>\n' +
            '                    </div>');
    }
}

function delete_field_landlord(e){
    let data_name = $(e).data('name');
    if(data_name == 'member_landlord_ag') {
        $(e).prev().remove();
        $(e).remove();
    }else if(data_name == 'active_off'){
        $(e).parent().remove();
    }
}

