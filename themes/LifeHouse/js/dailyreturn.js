$(document).ready(function () {
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'Add',
        },
        dataType: 'json',
        success: function (data) {
            if(data["error"] !=""){
                alert(data["error"]);
            }else{
                if($("input[name='ag']").val()=='' || $("input[name='ag']").val()==null){
                    $("input[name='ag']").val(data["return"]["ag"]);
                }
                if(data["return"]['id_group']=='9'){//業務
                    $("input[name='ag']").attr("disabled","true");
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
});

