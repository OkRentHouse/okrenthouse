function del_data_h_a(o,type){//刪除下一個(狀態維修) 跟指定的目前的
    if(type=='1' || type=='2' || type=='3' || type=='4' || type=='7'){//因為只有這些版型才會到第二排
        $(o).parent().next().remove();
    }
    $(o).parent().remove();
}
　
function add_data_h_a(o){
    var name = $(o).data("name");
    var type = $(o).data("type");
    if(type=='1'){
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"><span class="input-group-addon">位置</span><input class="form-control" type="text" name="'+name+'_position[]" value=""> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="'+name+'_brand[]" value=""> <span class="input-group-addon" onclick="del_data_h_a(this,1)" >-</span></div>' +
            '<div class="input-group fixed-width-lg"><span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" ></div>');

    }else if(type=='2'){
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"><span class="input-group-addon">位置</span><input class="form-control" type="text" name="'+name+'_position[]" value=""> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">椅(數量)</span><input class="form-control" type="text" name="'+name+'_chair[]"  value=""> <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="'+name+'_brand[]" value=""> <span class="input-group-addon" onclick="del_data_h_a(this,2)" >-</span></div>' +
            '<div class="input-group fixed-width-lg"><span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" ></div>');
    }else if(type=='3'){
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"><span class="input-group-addon">位置</span><input class="form-control" type="text" name="'+name+'_position[]" value=""> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">類型</span><input class="form-control" type="text" name="'+name+'_type[]"  value=""> <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="'+name+'_brand[]" value=""> <span class="input-group-addon" onclick="del_data_h_a(this,3)" >-</span></div>' +
            '<div class="input-group fixed-width-lg"><span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" ></div>');
    }else if(type=='4'){
        var select_text = $(o).data("select_text").split(',');
        var select_val =$(o).data("select_val").split(',');
        var option = '';
        for(i=0;i<select_text.length;i++){
            option += '<option value="'+select_val[i]+'">'+select_text[i]+'</option>';
        }
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"><span class="input-group-addon">位置</span><input class="form-control" type="text" name="'+name+'_position[]" value=""> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">類型</span><select class="form-control" name="'+name+'_type[]">'+option+'</select> <span class="input-group-addon">品牌/規格</span><input class="form-control" type="text" name="'+name+'_brand[]" value=""> <span class="input-group-addon" onclick="del_data_h_a(this,4)" >-</span></div>' +
            '<div class="input-group fixed-width-lg"><span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" ></div>');
    }else if(type=='5'){
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"><span class="input-group-addon">位置</span><input class="form-control" type="text" name="'+name+'_position[]" value=""> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" > <span class="input-group-addon" onclick="del_data_h_a(this,5)" >-</span></div>');
    }else if(type=='6'){
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" > <span class="input-group-addon" onclick="del_data(this,6)" >-</span></div>');
    }else if(type=='7'){
        var select_text = $(o).data("select_text").split(',');
        var select_val =$(o).data("select_val").split(',');
        var option = '';
        for(i=0;i<select_text.length;i++){
            option += '<option value="'+select_val[i]+'">'+select_text[i]+'</option>';
        }
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"><span class="input-group-addon">位置</span><input class="form-control" type="text" name="'+name+'_position[]" value=""> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">類型</span><select class="form-control" name="'+name+'_type[]">'+option+'</select> <span class="input-group-addon" onclick="del_data_h_a(this,7)" >-</span></div>' +
            '<div class="input-group fixed-width-lg"><span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" ></div>');
    }else if(type=='8'){
        var select_text = $(o).data("select_text").split(',');
        var select_val =$(o).data("select_val").split(',');
        var option = '';
        for(i=0;i<select_text.length;i++){
            option += '<option value="'+select_val[i]+'">'+select_text[i]+'</option>';
        }
        $(o).parent().parent().append('<div class="input-group fixed-width-lg"> <span class="input-group-addon">數量</span><input class="form-control" type="text" name="'+name+'_num[]"  value=""> <span class="input-group-addon">類型</span><select class="form-control" name="'+name+'_type[]">'+option+'</select> <span class="input-group-addon">狀態</span><input class="form-control" type="text" name="'+name+'_status[]" > <span class="input-group-addon" onclick="del_data_h_a(this,8)" >-</span></div>');
    }
}
