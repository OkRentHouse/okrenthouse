$(document).ready(function(){
    $(".new").hide();//因為這邊不能新增因此關閉
    $(".county").change(function(){
        let city_html = '';
        let data_this = $(this).parent().parent().find(".city");
        $.ajax({
            url: document.location.pathname,
            method: 'POST',
            data: {
                'ajax': false,
                'action': 'City',
                'id_county': $(this).val()
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data["error"] != "") {
                    alert(data["error"]);
                } else {
                    data_this.empty();//去除現有的
                    data['return'].forEach(function(val,index){
                        city_html +='<option value="'+val['id_city']+'">'+val['city_name']+'</option>';
                    });
                    data_this.html(city_html);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });
    });

    // $(".skill_class").each(function(index,element){
    //     let child = $(element).data("child");
    //     let child_value = $(element).data("child_value");
    //     console.log(element);
    //     console.log(child);
    //     console.log(child_value);
    // });

    // console.log($(".skill"));
    //這個做一開始進來的隱藏
    // $(".skill").each(function(index,element){
    //     console.log('index=>'+ index);
    //     console.log(element);
    //     console.log($(element).val());
    //
    //
    //     // $(document.getElementsByClassName("skill_class")[index]).each(function(s_c_index,s_c_element){
    //     //     console.log('s_c_index=>'+s_c_index);
    //     //     console.log(s_c_element);
    //     //     console.log($(s_c_element).val());
    //     // });
    //
    //
    //     // let data_class = document.getElementsByClassName("skill_class")[index];
    //     // console.log(data_class)
    //     // console.log($(data_class).val());
    //
    //
    //
    // });
    same_address_type();
    $("#same_address").click(function(){
        same_address_type();
    });
});

function same_address_type() {
    if($("#same_address").prop('checked')){
        $("input[name='id_address_contact']").val("");//清空準備處理後面的事情
        $(".same_address input").prop("disabled",true);
        $(".same_address select").prop("disabled",true);
    }else{
        $(".same_address input").prop("disabled",false);
        $(".same_address select").prop("disabled",false);
    }
}



function skill_toggle(){

}

function add_personnel_data(e) {
    let table = $(e).data("table");
    let type = $(e).data("type");
    switch (table) {
        case 'personnel_insurance':
            let company ='<option>請選擇單位</option>';
            $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': true,
                    'action': 'company',
                },
                dataType: 'json',
                success: function(data) {
                    if (data["error"] != "") {
                        alert(data["error"]);
                    } else {
                        // console.log(data);
                        data['return'].forEach(function(value,index){
                            company　+='<option value="'+value["id_company"]+'">'+value["name"]+'</option>';
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });

            switch (type) {
                case 0:
                    $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                        '        <input type="hidden" name="id_personnel_insurance[]" >\n' +
                        '        <input type="hidden" name="id_personnel_insurance_type[]" value="0">\n' +
                        '        <input type="hidden" name="id_personnel_insurance_e_date[]" >\n' +
                        '        <input type="hidden" name="id_personnel_insurance_dependents[]">\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">加保單位:</span>\n' +
                        '                <select class="form-control" name="id_personnel_insurance_id_company[]">\n' +
                        '                    <option value="">請選擇單位</option>\n' +
                        company +
                        '                </select>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">加保日期:</span>\n' +
                        '                <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]" >\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-3 no_padding"  style="padding-left: 62px">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">扣款方式:</span>\n' +
                        '                <select class="form-control" name="id_personnel_insurance_deduction_type[]">\n' +
                        '                    <option value="">請選擇單位</option>\n' +
                        '                    <option value="0">公司轉帳</option>\n' +
                        '                    <option value="1">憑單繳費</option>\n' +
                        '                </select>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">承辦人:</span>\n' +
                        '                <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">修改時間:</span>\n' +
                        '                <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]"  disabled="disabled">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">投保金額:</span>\n' +
                        '                <input class="form-control" type="number"  name="id_personnel_insurance_price[]">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">保費:</span>\n' +
                        '                <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">公司負擔:</span>\n' +
                        '                <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-2 no_padding">\n' +
                        '            <div class="input-group fixed-width-lg">\n' +
                        '                <span class="input-group-addon">自負額:</span>\n' +
                        '                <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-lg-1 no_padding">\n' +
                        '            <button type="button" class="btn btn-secondary " data-table="personnel_insurance"   onclick="delete_personnel_data(this)" >-</button>\n' +
                        '        </div>\n' +
                        '    </div>');

                    break;
                case 1:
                    $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                        '            <input type="hidden" name="id_personnel_insurance[]" value="">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_type[]" value="1">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_e_date[]">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_deduction_type[]">\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">加保單位:</span>\n' +
                        '                    <select class="form-control" name="id_personnel_insurance_id_company[]">\n' +
                        company +
                        '                    </select>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">加保日期:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-3 no_padding"  style="padding-left: 62px">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">扣款方式:</span>\n' +
                        '                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">\n' +
                        '                        <option value="">請選擇單位</option>\n' +
                        '                        <option value="0">公司轉帳</option>\n' +
                        '                        <option value="1">憑單繳費</option>\n' +
                        '                    </select>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">承辦人:</span>\n' +
                        '                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">修改時間:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">眷屬:</span>\n' +
                        '                    <input class="form-control" type="text"  name="id_personnel_insurance_dependents[]" maxlength="32">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">投保金額:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">保費:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">公司負擔:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_company_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">自負額:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_personal_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-1 no_padding">\n' +
                        '                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"    onclick="delete_personnel_data(this)" >-</button>\n' +
                        '            </div>\n' +
                        '        </div>');
                    break;
                case 2:
                    $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                        '            <input type="hidden" name="id_personnel_insurance[]" value="">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_type[]" value="2">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_company_price[]">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_personal_price[]">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_dependents[]">\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">加保單位:</span>\n' +
                        '                    <select class="form-control" name="id_personnel_insurance_id_company[]">\n' +
                        company+
                        '                    </select>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">加保日期:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding" style="padding-left: 60px">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">退保日期:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-3 no_padding" style="padding-left: 122px">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">承辦人:</span>\n' +
                        '                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">修改時間:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">投保金額:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">保費:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-1 no_padding">\n' +
                        '                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"   onclick="delete_personnel_data(this)" >-</button>\n' +
                        '            </div>\n' +
                        '        </div>');
                    break;
                case 3:
                    $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                        '            <input type="hidden" name="id_personnel_insurance[]" value="">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_type[]" value="3">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_company_price[]">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_personal_price[]">\n' +
                        '            <input type="hidden" name="id_personnel_insurance_dependents[]">\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">加保單位:</span>\n' +
                        '                    <select class="form-control" name="id_personnel_insurance_id_company[]">\n' +
                            company+
                        '                    </select>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">加保日期:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_s_date[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding" style="padding-left: 60px">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">退保日期:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_e_date[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-3 no_padding" style="padding-left: 122px">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">承辦人:</span>\n' +
                        '                    <input class="form-control" type="text"  name="id_personnel_insurance_handler[]" maxlength="32">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">修改時間:</span>\n' +
                        '                    <input class="form-control" type="date"  name="id_personnel_insurance_update_time[]" disabled="disabled">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-3 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">扣款方式:</span>\n' +
                        '                    <select class="form-control" name="id_personnel_insurance_deduction_type[]">\n' +
                        '                        <option value="">請選擇單位</option>\n' +
                        '                        <option value="0">公司轉帳</option>\n' +
                        '                        <option value="1">憑單繳費</option>\n' +
                        '                    </select>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">投保金額:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-2 no_padding">\n' +
                        '                <div class="input-group fixed-width-lg">\n' +
                        '                    <span class="input-group-addon">保費:</span>\n' +
                        '                    <input class="form-control" type="number"  name="id_personnel_insurance_insurance_price[]">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="col-lg-1 no_padding">\n' +
                        '                <button type="button" class="btn btn-secondary " data-table="personnel_insurance"   onclick="delete_personnel_data(this)" >-</button>\n' +
                        '            </div>\n' +
                        '        </div>');
                    break;
                default:
                    break;
            }
            break;
        case 'admin_family':
            $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': true,
                    'async' : false,
                    'action': 'database_county_city',
                },
                dataType: 'json',
                success: function(data) {
                    if (data["error"] != "") {
                        alert(data["error"]);
                    } else {
                        let county ='<option>請選擇縣市</option>';
                        let city ='<option>請選擇鄉鎮區</option>';
                        let loop_skill =["county","city"];
                        loop_skill.forEach(function(value,index){
                            data["return"][value].forEach(function(o_value,o_index){
                                console.log(o_value);
                                console.log(o_index);
                                if(value=='county') county +='<option value="'+o_value["id_"+value]+'">'+o_value[value+"_name"]+'</option>';
                                if(value=='city') city +='<option value="'+o_value["id_"+value]+'">'+o_value[value+"_name"]+'</option>';
                            });
                        });
                        let address_hidden = '';
                        if(type==0) address_hidden = 'style="display: none;"';


                        $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                            '        <input type="hidden" name="id_admin_family[]" class="" value="">\n' +
                            '        <input type="hidden" name="id_admin_family_urgent_type[]" class="" value="'+type+'">\n' +
                            '        <div class="col-lg-2 no_padding">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">關係</span><input type="text" name="id_admin_family_relation[]"  class="form-control " value="" maxlength="32">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">姓名</span><input type="text" name="id_admin_family_name[]"  class="form-control " value="" maxlength="32">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">電話</span><input type="text" name="id_admin_family_tel[]"  class="form-control " value="" maxlength="32">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">手機</span><input type="text" name="id_admin_family_phone[]"  class="form-control " value="" maxlength="32">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">職業</span><input type="text" name="id_admin_family_job[]"  class="form-control " value="" maxlength="32">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '\n' +
                            '        <input type="hidden" name="id_admin_family_id_address[]" value="">\n' +
                            '        <div class="col-lg-2 no_padding"  '+address_hidden+'>\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">郵遞號</span><input type="text" name="id_admin_family_postal[]"  class="form-control " value="" maxlength="11">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding" '+address_hidden+'>\n' +
                            '            <select name="id_admin_family_id_county[]" class="form-control county" onchange="county_onclick(this)">\n' +
                            county+
                            '            </select>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding" '+address_hidden+'>\n' +
                            '            <select name="id_admin_family_id_city[]" class="form-control city">\n' +
                            city+
                            '            </select>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-5 no_padding" '+address_hidden+'>\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <input type="text"   name="id_admin_family_village[]" class="form-control " value="" maxlength="20"><span class="input-group-addon">里</span>\n' +
                            '                <input type="number" name="id_admin_family_neighbor[]" class="form-control " value="" maxlength="11"><span class="input-group-addon">鄰</span>\n' +
                            '                <input type="text"   name="id_admin_family_road[]" class="form-control " value="" maxlength="20">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="form-group col-lg-12" '+address_hidden+'>\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <input type="text" name="id_admin_family_segment[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">段</span>\n' +
                            '                <input type="text" name="id_admin_family_lane[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">巷</span>\n' +
                            '                <input type="text" name="id_admin_family_alley[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">弄</span>\n' +
                            '                <input type="text" name="id_admin_family_no[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">號</span>\n' +
                            '                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_no_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
                            '                <input type="text" name="id_admin_family_floor[]" class="form-control " value="" maxlength="10"><span class="input-group-addon">樓</span>\n' +
                            '                <span class="input-group-addon">之</span><input type="text" name="id_admin_family_floor_her[]" class="form-control " value="" maxlength="10"><span class="input-group-addon"></span>\n' +
                            '                <input type="text" name="id_admin_family_address_room[]" class="form-control " value="" maxlength="8"><span class="input-group-addon">室</span>\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="form-group col-lg-9" '+address_hidden+'>\n' +
                            '            <input type="text" name="id_admin_family_address[]" class="form-control " value="" maxlength="255" placeholder="完整地址">\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-1 no_padding">\n' +
                            '            <button type="button" class="btn btn-secondary " data-table="admin_family"  onclick="delete_personnel_data(this)" >-</button>\n' +
                            '        </div>\n' +
                            '    </div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
            break;
        case 'language':
            $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                '        <input type="hidden" name="id_language[]" value="">\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">語言:</span>\n' +
                '                <select class="form-control" name="id_language_name[]">\n' +
                '                    <option value="">選擇語言</option>\n' +
                '                    <option value="1">中文</option>\n' +
                '                    <option value="2">台語</option>\n' +
                '                    <option value="3">英文</option>\n' +
                '                    <option value="4">日文</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">聽:</span>\n' +
                '                <select class="form-control" name="id_language_listen[]">\n' +
                '                    <option value="">選擇程度</option>\n' +
                '                    <option value="0">不懂</option>\n' +
                '                    <option value="1">略懂</option>\n' +
                '                    <option value="2">中等</option>\n' +
                '                    <option value="3">精通</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">說:</span>\n' +
                '                <select class="form-control" name="id_language_speak[]">\n' +
                '                    <option value="">選擇程度</option>\n' +
                '                    <option value="0">不懂</option>\n' +
                '                    <option value="1">略懂</option>\n' +
                '                    <option value="2">中等</option>\n' +
                '                    <option value="3">精通</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">讀:</span>\n' +
                '                <select class="form-control" name="id_language_read[]">\n' +
                '                    <option value="">選擇程度</option>\n' +
                '                    <option value="0">不懂</option>\n' +
                '                    <option value="1">略懂</option>\n' +
                '                    <option value="2">中等</option>\n' +
                '                    <option value="3">精通</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">寫:</span>\n' +
                '                <select class="form-control" name="id_language_write[]">\n' +
                '                    <option value="">選擇程度</option>\n' +
                '                    <option value="0">不懂</option>\n' +
                '                    <option value="1">略懂</option>\n' +
                '                    <option value="2">中等</option>\n' +
                '                    <option value="3">精通</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-1 no_padding">\n' +
                '            <button type="button" class="btn btn-secondary " data-table="language"  data-type=""  onclick="delete_personnel_data(this)" >-</button>\n' +
                '        </div>\n' +
                '    </div>');
            break;
        case 'training':
            $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                '        <div class="col-lg-4 no_padding" >\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">課程名稱:</span>\n' +
                '                <input class="form-control" type="text" name="training_class[]" maxlength="64">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-4 no_padding" >\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">技能:</span>\n' +
                '                <input class="form-control" type="text" name="training_skill[]" maxlength="64">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-1 no_padding">\n' +
                '            <button type="button" class="btn btn-secondary " data-table="training"  data-type=""  onclick="delete_personnel_data(this)" >-</button>\n' +
                '        </div>\n' +
                '    </div>');
            break;
        case 'skill':
            $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': true,
                    'action': 'skill',
                },
                dataType: 'json',
                success: function(data) {
                    if (data["error"] != "") {
                        alert(data["error"]);
                    } else {
                        let skill_class ='<option>請選擇技能類別</option>';
                        let skill_classification ='<option>請選擇技能分類</option>';
                        let skill ='<option>請選擇技能</option>';
                        let loop_skill =["skill_class","skill_classification","skill"];

                        loop_skill.forEach(function(value,index){
                            data["return"][value].forEach(function(o_value,o_index){
                                if(value=='skill_class') skill_class +='<option value="'+o_value["id_"+value]+'">'+o_value["title"]+'</option>';
                                if(value=='skill_classification') skill_classification +='<option value="'+o_value["id_"+value]+'">'+o_value["title"]+'</option>';
                                if(value=='skill') skill +='<option value="'+o_value["id_"+value]+'">'+o_value["title"]+'</option>';
                            });
                        });
                        $(e).parent().parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                            '        <div class="col-lg-12 no_padding" >\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">專長/技能:</span>\n' +
                            '                <span class="input-group-addon">類別:</span>\n' +
                            '                <select name="id_skill_class[]" class="form-control skill_class">\n' +
                                skill_class +
                            '                </select>\n' +
                            '                <span class="input-group-addon">分類:</span>\n' +
                            '                <select name="id_skill_classification[]" data-parent="" class="form-control skill_classification">\n' +
                            skill_classification+
                            '                </select>\n' +
                            '                <span class="input-group-addon">技能:</span>\n' +
                            '                <select name="id_skill_classification[]" data-parent="" class="form-control skill">\n' +
                            skill+
                            '                </select>\n' +
                            '                <span class="input-group-addon" data-table="skill" data-type=""  onclick="delete_personnel_data(this)" >-</span>\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '    </div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });

            break;
        case 'education':
            $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                '        <input type="hidden" name="id_education[]" value="">\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">學歷:</span>\n' +
                '                <select class="form-control" name="id_education_education_level[]">\n' +
                '                    <option value="">請選擇學歷</option>\n' +
                '                    <option value="0">高中以下</option>\n' +
                '                    <option value="1">二技</option>\n' +
                '                    <option value="2">四技</option>\n' +
                '                    <option value="3">大學</option>\n' +
                '                    <option value="4">碩士</option>\n' +
                '                    <option value="5">博士</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-3 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">學校:</span>\n' +
                '                <input class="form-control" type="text"  name="id_education_school[]"   maxlength="64">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-3 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">科系:</span>\n' +
                '                <input class="form-control" type="text"  name="id_education_department[]"   maxlength="64">\n' +
                '            </div>\n' +
                '        </div>　\n' +
                '        <div class="col-lg-2 no_padding" style="left: -15px;">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">畢/肄業:</span>\n' +
                '                <select class="form-control" name="id_education_graduation[]">\n' +
                '                    <option value="">是否畢業</option>\n' +
                '                    <option value="0">畢業</option>\n' +
                '                    <option value="1">肄業</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-1 no_padding" style="left: -15px;">\n' +
                '            <button type="button" class="btn btn-secondary " data-table="education"  onclick="delete_personnel_data(this)">-</button>\n' +
                '        </div>\n' +
                '    </div>');
        break;
        case 'experience':
            $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                '        <input type="hidden" name="id_experience[]" value="">\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">公司:</span>\n' +
                '                <input class="form-control" type="text"  name="id_experience_company[]"   maxlength="64">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">工作:</span>\n' +
                '                <input class="form-control" type="text"  name="id_experience_job[]"   maxlength="32">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">待遇:</span>\n' +
                '                <input class="form-control" type="number"  name="id_experience_salary[]">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">開始時間:</span>\n' +
                '                <input class="form-control" type="date"  name="id_experience_s_date[]">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding" style="left: 65px;">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">結束時間:</span>\n' +
                '                <input class="form-control" type="date"  name="id_experience_e_date[]">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-1 no_padding" style="left: 133px;">\n' +
                '            <button type="button" class="btn btn-secondary " data-table="experience"  onclick="delete_personnel_data(this)" >-</button>\n' +
                '        </div>\n' +
                '    </div>');
            break;
        case 'license':
            $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                '        <input type="hidden" name="id_license[]" value="">\n' +
                '        <div class="col-lg-2 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">證照名稱:</span>\n' +
                '                <input class="form-control" type="text"  name="id_license_name[]"   maxlength="64">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-3 no_padding">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">開始時間:</span>\n' +
                '                <input class="form-control" type="date"  name="id_license_s_date[]">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-2 no_padding" style="left: 65px;">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">結束時間:</span>\n' +
                '                <input class="form-control" type="date"  name="id_license_e_date[]">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-3 no_padding" style="left: 130px;">\n' +
                '            <div class="input-group fixed-width-lg">\n' +
                '                <span class="input-group-addon">備註:</span>\n' +
                '                <input class="form-control" type="text"  name="id_license_remarks[]"   maxlength="128">\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="col-lg-1 no_padding"  style="left: 128px;">\n' +
                '            <button type="button" class="btn btn-secondary " data-table="license"  onclick="delete_personnel_data(this)" >-</button>\n' +
                '        </div>\n' +
                '    </div>');
            break;
        case 'people_group':
            $.ajax({
                url: document.location.pathname,
                method: 'POST',
                data: {
                    'ajax': true,
                    'action': 'company',
                    'type' : '1',
                },
                dataType: 'json',
                success: function(data) {
                    if (data["error"] != "") {
                        alert(data["error"]);
                    } else {
                        let company ='<option>請選擇單位</option>';
                        // console.log(data);
                        data['return'].forEach(function(value,index){
                            company　+='<option value="'+value["id_company"]+'">'+value["name"]+'</option>';
                        });

                        $(e).parent().parent().parent().append('<div class="form-group col-lg-12">\n' +
                            '        <input type="hidden" name="id_people_group[]" value="">\n' +
                            '        <div class="col-lg-2 no_padding">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">團體:</span>\n' +
                            '                <select name="id_people_group_id_company[]"  class="form-control">\n' +
                            company+
                            '                </select>\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">入會日期:</span>\n' +
                            '                <input class="form-control" type="date"  name="id_people_group_s_date[]">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding" style="left: 65px">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">會員編號:</span>\n' +
                            '                <input class="form-control" type="text"  name="id_people_group_number[]"   maxlength="64">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding" style="left: 65px">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">會費:</span>\n' +
                            '                <input class="form-control" type="number"  name="id_people_group_due[]">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-2 no_padding" style="left: 65px">\n' +
                            '            <div class="input-group fixed-width-lg">\n' +
                            '                <span class="input-group-addon">年度:</span>\n' +
                            '                <input class="form-control" type="text"  name="id_people_group_year[]" maxlength="8">\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="col-lg-1 no_padding" style="left: 65px">\n' +
                            '            <button type="button" class="btn btn-secondary " data-table="people_group"  onclick="delete_personnel_data(this)" >-</button>\n' +
                            '        </div>\n' +
                            '    </div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
            break;
        default:
            break;
    }
}

function delete_personnel_data(e) {
    let table = $(e).data("table");
    switch (table) {
        case 'language':
        case 'training':
        case 'personnel_insurance':
        case 'admin_family':
        case 'education':
        case 'experience':
        case 'license':
        case 'people_group':
            $(e).parent().parent().remove();
            break;
        case  'skill':
            $(e).parent().parent().parent().remove();
            break;

        default:
            break;
    }
}

function county_onclick(e) {
    let city_html ='';
    let data_this = $(e).parent().parent().find(".city");
    console.log(data_this);
    console.log($(e).val());
    $.ajax({
        url: document.location.pathname,
        method: 'POST',
        data: {
            'ajax': false,
            'action': 'City',
            'id_county': $(e).val()
        },
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if (data["error"] != "") {
                alert(data["error"]);
            } else {
                data_this.empty();//去除現有的
                data['return'].forEach(function(val,index){
                    city_html +='<option value="'+val['id_city']+'">'+val['city_name']+'</option>';
                });
                data_this.html(city_html);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {}
    });
}