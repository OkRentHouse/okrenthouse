$(function() {
    $('#contract_date').datepicker({
        format: "yyyy-mm",
        autoclose: true,
        // startDate: "today",
        // clearBtn: true,
        // calendarWeeks: true,
        // todayHighlight: true,
        minViewMode: 1,
        // maxViewMode: 4,
        language: 'zh-TW'
    });
});

function price_total(input) {
    var total_price = '0';
    for(i=0;i<document.getElementsByName("item_price[]").length;i++){
        console.log(document.getElementsByName("item_price[]")[i].value);
        total_price = parseInt(total_price)+parseInt(document.getElementsByName("item_price[]")[i].value);
    }
    $('#funds_total').val(total_price);

}

function del_data_c_i_s(o){//刪除下一個(狀態維修) 跟指定的目前的
    $(o).parent().next().remove();
    $(o).parent().remove();
}

function add_data_c_i_s(o){
    var name = $(o).data("name");
    var select_text = $(o).data("select_text").split(',');
    var select_val =$(o).data("select_val").split(',');
    var option = '';
    for(i=0;i<select_text.length;i++){
        option += '<option value="'+select_val[i]+'">'+select_text[i]+'</option>';
    }

    $(o).parent().parent().append('<div class="input-group fixed-width-lg">\n' +
        '                                <span class="input-group-addon">類型</span>\n' +
        '                                <select class="form-control" name="'+name+'_type[]">\n' + option+
        '                                </select>\n' +
        '                                <span class="input-group-addon">金額</span><input class="form-control" type="number" name="'+name+'_price[]" value="" oninput="price_total(this.value)">\n' +
        '                                <span class="input-group-addon">日期</span><input class="form-control" type="date" name="'+name+'_date[]"  value="">\n' +
        '                                <span class="input-group-addon" onclick="del_data_c_i_s(this)" >-</span>\n' +
        '                            </div>\n' +
        '                            <div class="input-group fixed-width-lg">\n' +
        '                                <span class="input-group-addon">明細</span>\n' +
        '                                <input class="form-control" type="text" name="'+name+'_detail[]" value="">\n' +
        '                            </div>'
    );
}
