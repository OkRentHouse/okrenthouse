$(document).ready(function(){
    $("i.fas.fa-minus").click(function(){$(this).parent().find(":text").val( function(){ if( parseInt($(this).val()) > 0  ){return parseInt($(this).val())-1}else{return 0}} )});
    $("i.fas.fa-plus").click(function(){$(this).parent().find(":text").val( function(){return parseInt($(this).val())+1} )});

    $(".main__top-left-nav-img").mouseover(function(){
        var background=$(this).css("background");
        $(".main__top-left-img-inner").css("background",background);
    });
});
