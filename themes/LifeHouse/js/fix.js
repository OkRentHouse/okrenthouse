function del_data(o){//刪除下一個(狀態維修) 跟指定的目前的
        $(o).parent().next().remove();
        $(o).parent().remove();
}
function add_data(o){
    var name = $(o).data("name");
    var data_length = $('input[name="'+name+'_ag[]"]').length;
        if(data_length>='5'){//因為目前預設最多新增至5個因此超過的話需要擴大回報次數
            alert("目前回報新增不能超過5個如欲超過則需諮詢後台工程師!");
        }else{
            $(o).parent().parent().append(
                '<div class="input-group fixed-width-lg"><span class="input-group-addon">回報業務</span><input class="form-control" type="text" name="'+name+'_ag[]" value="" maxlength="20">'+
                '<span class="input-group-addon">回報時間</span><input class="form-control" type="date" name="'+name+'_date[]"  value="">'+
                '<span class="input-group-addon" onclick="del_data(this)" >-</span></div>'+
                '<div class="input-group fixed-width-lg"><span class="input-group-addon">回報紀錄</span><input class="form-control" type="text" name="'+name+'_record[]" value="" maxlength="64"></div>');

        }
}

function fix_record_add(o){
    //這段是做點選新增後會多的資料
    $(o).prev().children("li").removeClass("active");
    var fix_record_length = $(".fix_record_number").length;

    $(o).prev().children("li:last-child").after('<li class="active"><a data-toggle="tab" href="#fix_record_'+fix_record_length+'"  class="fix_record_number">檢修記錄</a></li>');
    $("#nav_body").children().removeClass("in active");

    $("#nav_body").append('<div class="tab-pane fade in active" id="fix_record_'+fix_record_length+'">\n' +
        '        <input class="form-control hidden" type="text" name="id_fix_record[]" id="" value="" >\n' +
        '        <div class="form-group col-lg-6">\n' +
        '            <label class="control-label col-lg-6" for="">發派日期</label>\n' +
        '            <div class="form-group col-lg-6" id="">\n' +
        '                <input class="form-control" type="date" name="dispatch_date[]" id="" value="" >\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="form-group col-lg-6">\n' +
        '            <label class="control-label col-lg-3" for="">發派業務</label>\n' +
        '            <div class="form-group col-lg-7" id="">\n' +
        '                <input class="form-control" type="text" name="dispatch_ag[]" id="" value="" maxlength="20">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="form-group col-lg-12">\n' +
        '            <label class="control-label col-lg-3 " for="">發派廠商</label>\n' +
        '            <div class="col-lg-6" id="">\n' +
        '                <input class="form-control" type="text" name="dispatch_vendor[]" id="" value="" maxlength="32">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="form-group col-lg-12">\n' +
        '            <label class="control-label col-lg-3 " for="">廠商回報</label>\n' +
        '            <div class="col-lg-6" id="">\n' +
        '                <input class="form-control" type="text" name="vendor_return[]" id="" value="" maxlength="64">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="form-group col-lg-12">\n' +
        '            <label class="control-label col-lg-3 " for="">回報屋主</label>\n' +
        '            <div class="col-lg-6" id="">\n' +
        '                <input class="form-control" type="text" name="return_landlord[]" id="" value="" maxlength="64">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '\n' +
        '        <div class="form-group col-lg-12">\n' +
        '            <label class="control-label col-lg-3 " for="">付款</label>\n' +
        '            <div class="form-group col-lg-9" id="">\n' +
        '                <div class="input-group fixed-width-lg">\n' +
        '                    <span class="input-group-addon">報價</span><input class="form-control" type="number" name="quoted_price[]" value="">\n' +
        '                    <span class="input-group-addon">議價</span><input class="form-control" type="number" name="negotiated_price[]" value="">\n' +
        '                    <span class="input-group-addon">淨賺</span><input class="form-control" type="number" name="net_price[]" value="">\n' +
        '                </div>\n' +
        '                <div class="input-group fixed-width-lg">\n' +
        '                    <span class="input-group-addon">付款方式</span>\n' +
        '                    <select class="form-control" name="pay[]">\n' +
        '                        <option value="">選擇付款方式</option>\n' +
        '                        <option value="0">租客負擔</option>\n' +
        '                        <option value="1">租客租金抵扣</option>\n' +
        '                        <option value="2">房東轉匯至專戶</option>\n' +
        '                        <option value="3">自代收租金抵扣</option>\n' +
        '                    </select>\n' +
        '                    <span class="input-group-addon">付款額外資料</span>\n' +
        '                    <input class="form-control" type="text" name="pay_additional_data[]" value="" maxlength="64">\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '\n' +
        '        <div class="form-group col-lg-6">\n' +
        '            <label class="control-label col-lg-6" for="">報銷日期</label>\n' +
        '            <div class="form-group col-lg-6" id="">\n' +
        '                <input class="form-control" type="date" name="reimbursement_date[]" id="" value="" >\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="form-group col-lg-6">\n' +
        '            <label class="control-label col-lg-3" for="">報銷業務</label>\n' +
        '            <div class="form-group col-lg-7" id="">\n' +
        '                <input class="form-control" type="text" name="reimbursement_ag[]" id="" value="" maxlength="20">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="form-group col-lg-12">\n' +
        '            <label class="control-label col-lg-3 " for="">發派廠商</label>\n' +
        '            <div class="col-lg-6" id="">\n' +
        '                <input class="form-control" type="text" name="reimbursement_record[]" id="" value="" maxlength="64">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>');


    // $(o).parent().prev().after('<li><a data-toggle="tab" href="#fix_record_'+fix_record_length+'"  class="fix_record_number">檢修記錄</a></li>');
    // console.log($(o).prev().children("li:last-child"));
    // console.log($(o).prev().find("li"));
    // console.log($(o).prev().find("li").length);
    // console.log($(o).parent().prev());
    // console.log($(o).parent().prev().addClass("active"));
    // $(o).parent().removeClass("active");
    // $(o).parent().prev().addClass("active");
    // $(o).parent().removeClass("active");
}