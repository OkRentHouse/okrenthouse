$(document).ready(function() {
    $("section.slider.regular.slick-initialized.slick-slider.slick-dotted").css("height","320px");

    $(".top div ul li").mouseover(function(){
      $(".top div ul li span").stop(true, false);
      $(".top div ul li span").animate({opacity: 1},100,function() {
      // Animation complete.
    })
    });

    $(".top div ul li").mouseout(function(){
      $(".top div ul li span").stop(true, false);
      $(".top div ul li span").animate({opacity: 0},300,function() {
      // Animation complete.
    })
    });

})

$(window).resize(function() {
    $("section.slider.regular.slick-initialized.slick-slider.slick-dotted").css("height","320px");
})
