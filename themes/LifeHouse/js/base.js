﻿//==================圖片詳細頁函數=====================
//鼠標經過預覽圖片函數
function preview(img) {
	$("#preview .jqzoom img").attr("src", $(img).attr("src"));
	$("#preview .jqzoom img").attr("jqimg", $(img).attr("bimg"));
}

function preview_str(img_str) {
	var img=$(img_str).parent().next();
	$("#preview .jqzoom img").attr("src", $(img).attr("src"));
	$("#preview .jqzoom img").attr("jqimg", $(img).attr("bimg"));
	$(".items").find("span").css("color","gray");
	$(img_str).css("color","#53268a");
}

//圖片放大鏡效果
// $(function () {
// 	$(".jqzoom").jqueryzoom({
// 		xzoom: 380,
// 		yzoom: 410
// 	});
// });

//圖片預覽小圖移動效果,頁面加載時觸發
$(function () {
	var tempLength = 0; //臨時變量,當前移動的長度
	var viewNum = 5; //設置每次顯示圖片的個數量
	var moveNum = 1; //每次移動的數量
	var moveTime = 300; //移動速度,毫秒
	var scrollDiv = $(".spec-scroll .items ul"); //進行移動動畫的容器
	var scrollItems = $(".spec-scroll .items ul li"); //移動容器裡的集合
	var moveLength = scrollItems.eq(0).width() * moveNum; //計算每次移動的長度
	var countLength = (scrollItems.length - viewNum) * scrollItems.eq(0).width(); //計算總長度,總個數*單個長度

	//下一張
	$(".spec-scroll .next").bind("click", function () {
		if (tempLength < countLength) {
			if ((countLength - tempLength) > moveLength) {
				scrollDiv.animate({
					left: "-=" + moveLength + "px"
				}, moveTime);
				tempLength += moveLength;
			} else {
				scrollDiv.animate({
					left: "-=" + (countLength - tempLength) + "px"
				}, moveTime);
				tempLength += (countLength - tempLength);
			}
		}
	});
	//上一張
	$(".spec-scroll .prev").bind("click", function () {
		if (tempLength > 0) {
			if (tempLength > moveLength) {
				scrollDiv.animate({
					left: "+=" + moveLength + "px"
				}, moveTime);
				tempLength -= moveLength;
			} else {
				scrollDiv.animate({
					left: "+=" + tempLength + "px"
				}, moveTime);
				tempLength = 0;
			}
		}
	});
});
//==================圖片詳細頁函數=====================