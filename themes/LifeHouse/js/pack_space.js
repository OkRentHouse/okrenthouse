$(document).ready(function () {

});

function add_data_park_space(o){
    var name = $(o).data("name");
    $(o).parent().parent().append('<div class="input-group fixed-width-lg">\n' +
        '                <span class="input-group-addon">位置</span>\n' +
        '                <select class="form-control" name="'+name+'_park_position[]">\n' +
        '                    <option value="0">社區內</option>\n' +
        '                    <option value="1">社區外</option>\n' +
        '                </select>\n' +
        '                <span class="input-group-addon">類型</span>\n' +
        '                <select class="form-control" name="'+name+'_type[]">\n' +
        '                    <option value="0">坡道平面</option>\n' +
        '                    <option value="1">坡道機械</option>\n' +
        '                    <option value="2">升降平面</option>\n' +
        '                    <option value="3">升降機械</option>\n' +
        '                    <option value="4">一樓平面</option>\n' +
        '                </select>\n' +
        '                <span class="input-group-addon">編號</span><input class="form-control" type="text" name="'+name+'_serial[]" value="" maxlength="20">\n' +
        '                <span class="input-group-addon"  onclick="delete_data_park_space(this)" >-</span>\n' +
        '            </div>\n' +
        '            <div class="input-group fixed-width-lg">\n' +
        '                <span class="input-group-addon">租金</span><input class="form-control" type="number" name="'+name+'_rent[]" value="">\n' +
        '                <span class="input-group-addon">繳費方式</span>\n' +
        '                <select class="form-control" name="'+name+'_rent_type[]">\n' +
        '                    <option value="0">月</option>\n' +
        '                    <option value="1">季</option>\n' +
        '                    <option value="2">半年</option>\n' +
        '                    <option value="3">年</option>\n' +
        // '                    <option value="4">包含管理費</option>\n' +
        '                </select>\n' +
        '                <span class="input-group-addon">備註</span>\n' +
        '                <input class="form-control" type="text" name="'+name+'_remarks[]" value="" maxlength="64">\n' +
        '            </div>');
}

function delete_data_park_space(o){//刪除該項
    $(o).parent().next().remove();
    $(o).parent().remove();
}