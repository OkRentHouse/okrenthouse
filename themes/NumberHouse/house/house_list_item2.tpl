<div class="commodity">
	<div class="photo_row"><a href="/RentHouse?id_rent_house={$house.id_rent_house}"><img src="{$house.img}" class="img" alt="{$house.title}"></a>
		<collect data-id="{$house.id_rent_house}" data-type="rent_house" class="favorite {if $house.favorite}on{/if}">{if $house.favorite}
				<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}
		</collect>
	</div>
	<div class="object">
		<div class="title_wrap">
			<div class="number">{$house.rent_house_code}</div>
			<div class="address">{$house.rent_address}</div>
		</div>
		<h3 class="title big"><a href="/RentHouse?id_rent_house={$house.id_rent_house}">{$house.case_name}</a></h3>
		<div class="title"><a href="/RentHouse?id_rent_house={$house.id_rent_house}">{$house.rent_house_title}</a></div>
		<div class="title structure">
			{$ti=0}
			{if ($house.t3=='' || $house.t3=='0') && ($house.t4=='' || $house.t4=='0')}{$ti=1}
				開放空間　
			{/if}
			{if $house.t3=='' || $house.t3=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.t3}</span>房{/if}
			{if $house.t4=='' || $house.t4=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.t4}</span>廳{/if}
			{if $house.t5=='' || $house.t5=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.t5}</span>衛{/if}
			{if $house.t6=='' || $house.t6=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.t6}</span>室{/if}
			{if $house.kitchen=='' || $house.kitchen=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.kitchen}</span>廚{/if}
			{if $house.t7=='' || $house.t7=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.t7}</span>前陽台{/if}
			{if $house.t8=='' || $house.t8=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.t8}</span>後陽台{/if}
			{if $house.t9=='' || $house.t9=='0'}{else}{if $ti==1}　{else}{$ti=1}{/if}<span class="get_me_power">{$house.t9}</span>房間陽台{/if}
		</div>
		<div class="balcony">
			{*			{if $house.t7=='' || $house.t7=='0'}{else}{$house.t7}前陽台<i></i>{/if}*}
			{*			{if $house.t8=='' || $house.t8=='0'}{else}{$house.t8}後陽台<i></i>{/if}*}
			{*			{if $house.t9=='' || $house.t9=='0'}{else}{$house.t9}房間陽台{/if}*}
		</div>
		<div class="title structure">{$house.ping} 坪&nbsp;&nbsp;&nbsp;&nbsp;{foreach from=$house.sql_types key=t item=types}{if $t !='0'},{/if}{$types.title}
			{/foreach}{if $house.id_rent_house_type}&nbsp;&nbsp;&nbsp;&nbsp;{foreach from=$house.sql_type key=t item=type}{if $t !='0'},{/if}{$type.title}
			{/foreach}{/if}&nbsp;&nbsp;&nbsp;&nbsp;{if $house.whole_building=='1'}整棟建築{else}{$house.rental_floor} ／ {$house.floor} F{/if}</div>
		{$house.tag}
	</div>
{*	<div class="object">*}
{*		<div class="title_wrap">*}
{*			<div class="number">{$house.rent_house_code}</div>*}
{*			<div class="address">{$house.rent_address}</div>*}
{*		</div>*}
{*		<h3 class="title big"><a href="/RentHouse?id_rent_house={$house.id_rent_house}">{$house.case_name}</a></h3>*}
{*		<div class="title"><a href="/RentHouse?id_rent_house={$house.id_rent_house}">{$house.rent_house_title}</a></div>*}
{*		<div>{if ($house.t3=='' || $house.t3=='0') && ($house.t4=='' || $house.t4=='0') &&*}
{*			($house.t5=='' || $house.t5=='0') && ($house.t6=='' || $house.t6=='0')}開放空間*}
{*				{if $house.t4=='' || $house.t4=='0'}{else}<i></i>{$house.t4}廳{/if}*}
{*				{if $house.t5=='' || $house.t5=='0'}{else}<i></i>{$house.t5}衛{/if}*}
{*				{if $house.t6=='' || $house.t6=='0'}{else}<i></i>{$house.t6}室<{/if}*}
{*			{else}*}
{*				{if $house.t3=='' || $house.t3=='0'}0房<i></i>{else}{$house.t3}房<i></i>{/if}*}
{*				{if $house.t4=='' || $house.t4=='0'}0廳<i></i>{else}{$house.t4}廳<i></i>{/if}*}
{*				{if $house.t5=='' || $house.t5=='0'}0衛<i></i>{else}{$house.t5}衛<i></i>{/if}*}
{*				{if $house.t6=='' || $house.t6=='0'}0室{else}{$house.t6}室{/if}*}
{*			{/if}*}
{*		</div>*}
{*		<div class="balcony">*}
{*			{if $house.t7=='' || $house.t7=='0'}0前陽台<i></i>{else}{$house.t7}前陽台<i></i>{/if}*}
{*			{if $house.t8=='' || $house.t8=='0'}0後陽台<i></i>{else}{$house.t8}後陽台<i></i>{/if}*}
{*			{if $house.t9=='' || $house.t9=='0'}0房間陽台{else}{$house.t9}房間陽台{/if}*}
{*		</div>*}
{*		<div>{$house.ping}坪<i></i>{foreach from=$house.sql_types key=t item=types}{if $t !='0'},{/if}{$types.title}*}
{*		{/foreach}{if $house.id_rent_house_type}<i></i>{foreach from=$house.sql_type key=t item=type}{if $t !='0'},{/if}{$type.title}*}
{*			{/foreach}{/if}<i></i>{if $house.whole_building=='1'}整棟建築{else}{$house.rental_floor} / {$house.floor} F{/if}</div>*}
{*		{$house.tag}*}
{*	</div>*}
{*	<div class="data">*}
{*		<div class="title">*}
{*			<span>{$house.memeber_name}</span> {$house.member_gender}(收取服務費)*}
{*		</div>*}
{*		<p class="">生活樂租 {$house.w_title}</p>*}
{*		<p class="">仲介業 <i></i> 租管業</p>*}
{*		<p class="">生活資產管理股份有限公司</p>*}
{*		<div class="number"><img src="{$THEME_URL}/img/icon/house_search_phone_01.svg" alt="">{$house.member_phone}</div>*}
{*		<div class="number"><img src="{$THEME_URL}/img/icon/house_search_phone_02.svg" alt="">{$house.tel}</div>*}
{*		<div class="btn_wrap">*}
{*			<a class="" href="#"> 留 言 </a>*}
{*			<i></i>*}
{*			<a class="" href="#"> 預約賞屋 </a>*}
{*		</div>*}
{*	</div>*}
	{$house.user_html}
</div>
{*<div class="commodity">*}
{*	<div class="photo_row"><a href="#"><img src="{$house.img}" class="img" alt="{$house.title}"></a>*}
{*		<collect data-id="{$house.id}" data-type="house" class="favorite {if $house.collect}on{/if}">{if $house.collect}*}
{*				<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}*}
{*		</collect>*}
{*	</div>*}
{*	<div class="object">*}
{*		<div class="title_wrap">*}
{*			<div class="number">{$house.rent_house_code}</div>*}
{*			<h3 class="title big"><a href="#">{$house.title}</a></h3>*}
{*		</div>*}
{*		<div class="title"><a href="#">{$house.case_name}</a></div>*}
{*		<div class="price">*}
{*			<price>18,500<unit>元/月</unit>*}
{*                {if $house.s1}<div class="cost">{$house.s1}</div>{/if}*}
{*			</price>*}
{*		</div>*}
{*		<div class="address">{$house.address}</div>*}
{*		<div>{implode('<i></i>', [$house.t4, $house.t5, $house.t6, $house.t7])}{$house.t4}<i></i>1陽台</div>*}
{*		<div>12.26坪<i></i>大樓<i></i>獨立套房<i></i>7 / 18 F</div>*}
{*		<tag>*}
{*			<i>*}
{*				<img src="{$THEME_URL}/img/icon/furniture.svg" alt="附傢俱" title="附傢俱">*}
{*				<p>附傢俱</p>*}
{*			</i>*}
{*			<i>*}
{*				<img src=" {$THEME_URL}/img/icon/tv.svg" alt="附家電" title="附家電">*}
{*				<p>附家電</p>*}
{*			</i>*}
{*			<i>*}
{*				<img src="{$THEME_URL}/img/icon/parking.svg" alt="停車位" title="停車位">*}
{*				<p>停車位</p>*}
{*			</i>*}
{*			<i>*}
{*				<img src="{$THEME_URL}/img/icon/kitchenware.svg" alt="可炊煮" title="可炊煮">*}
{*				<p>可炊煮</p>*}
{*			</i>*}
{*			<i>*}
{*				<img src="{$THEME_URL}/img/icon/pet.svg" alt="可養寵物" title="可養寵物">*}
{*				<p>可養寵物</p>*}
{*			</i>*}
{*			<div class="clear"></div>*}
{*		</tag>*}
{*	</div>*}
{*	<div class="data">*}
{*		<div class="title">*}
{*			<span>郝旺旺</span> 先生 (收取服務費)*}
{*		</div>*}
{*		<p class="">生活樂租 藝文直營店</p>*}
{*		<p class="">仲介業 <i></i> 租管業</p>*}
{*		<p class="">生活資產管理股份有限公司</p>*}
{*		<div class="number"><img src="{$THEME_URL}/img/icon/house_search_phone_01.svg" alt="">0939-688089</div>*}
{*		<div class="number"><img src="{$THEME_URL}/img/icon/house_search_phone_02.svg" alt="">03-358-1098</div>*}
{*		<div class="btn_wrap">*}
{*			<a class="" href="#"> 留 言 </a>*}
{*			<i></i>*}
{*			<a class="" href="#"> 預約賞屋 </a>*}
{*		</div>*}
{*	</div>*}
{*</div>*}
