{if $group_link==1}
  <section class="slider_center">
    {include file="../../tpl/group_link.tpl"}
  </section>
{/if}

{if $display_footer}

{$logo="<img style='width: 3em;margin: 0 5px;color:#f0047f;' src='/themes/Rent/img/1786/logo.png'>聯絡我們"}
{$logo="聯絡我們"}
{$share_icons="<div class='linkto share_infor_box'><div class='l'><a class='icon_block FB'><img src='/themes/Rent/img/1786/fb.svg' style-top='margin:1em;'></a></div><div class='l'><a class='icon_block LINE'><img src='/themes/Rent/img/1786/line.svg' style='margin-top:1em;'></a></div></div>"}
{$webmap_t=array("租售好幫手","友好連結","相關聲明","關於我們",$logo)}

{$webmap_c["關於我們"]=array("品牌理念",
"歷史軌跡",
"媒體公關",
"合作提案")}

{$webmap_c["租售好幫手"]=array("房貸專區",
"稅費試算##https://www.etax.nat.gov.tw/etwmain/front/ETW118W/VIEW/23",
"市價行情",
"實價登錄##http://210.65.131.75/")}


{$webmap_c["友好連結"]=array("裝修達人##https://www.epro.house/",
"好幫手##https://okpt.life/",
"共享圈##https://www.okrent.tw/",
"生活樂購##https://www.lifegroup.house/LifeGo")}

{$webmap_c["相關聲明"]=array("免責聲明",
"隱私權聲明",
"服務條款")}

{$logo="<br>"}

{$webmap_t=array("買賣好幫手","租屋好幫手","關於我們",$logo)}

{$webmap_c["買賣好幫手"]=array("買賣照過來",
"房貸專區",
"稅費試算",
"市價行情",
"實價登錄")}

{$webmap_c["租屋好幫手"]=array("租屋照過來",
"租金補貼",
"社會住宅",
"包租代管")}

{$webmap_c["關於我們"]=array("合作提案",
"服務條款",
"隱私權聲明",
"免責聲明")}

<!--{$mail_icon="<svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='envelope' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' class='svg-inline--fa fa-envelope fa-w-16 fa-3x' style='font-size: 1em;margin: 0 5px;color:#f0047f;'><path fill='currentColor' d='M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z' class=''></path></svg>"}-->
{$mail_icon="<img class='bnfy-enter' width='37'  src='/themes/Rent/img/1786/images/210315/1786_index_210315_40.jpg' style='margin-top:2em 0'>　"}
{$clock_icon="<svg aria-hidden='true' focusable='false' data-prefix='far' data-icon='clock' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' class='svg-inline--fa fa-clock fa-w-16 fa-3x' style='font-size: 1em;margin: 0 5px;color:#f0047f;'><path fill='currentColor' d='M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z' class=''></path></svg>"}


{$webmap_c[$logo]=array(
"若對平台使用上有任何建議與疑問請洽：",
$mail_icon|cat:"mail | 1786@lifegroup.house##mailto:1786@lifegroup.house",
$share_icons)}





<div class="webmap">
  {for $i=0;$i<sizeof($webmap_t);$i++}
  <ul>
    <li>{$webmap_t[$i]}
      <ul>
        {for $ix=0;$ix<sizeof($webmap_c[$webmap_t[$i]]);$ix++}
        {$webmap_c_0=$webmap_c[$webmap_t[$i]][$ix]}
        {$webmap_c_0f="#"}
        {if stripos($webmap_c_0,'##')}
          {$webmap_c_0s=explode("##",$webmap_c[$webmap_t[$i]][$ix])}
          {$webmap_c_0f=$webmap_c_0s[1]}
          {$webmap_c_0=$webmap_c_0s[0]}
        {/if}
          <li><a href="{$webmap_c_0f}">{$webmap_c_0}</a>
        {/for}
      </ul>
    </li>
  </ul>
  {/for}
</div>
    <footer>
        <div class="footer A1786_footer">
            <table>
                <tbody>
                <tr>
                    <td>
                        <div class="footer_txt2"><a href="/Aboutokmaster" class="hidden"><img src="/themes/Okmaster/img/index/footer_logo.png">生活好科技有限公司 </a><span class="spacing"></span> Copyright ©2021 by Life Master Technology Co., Ltd All Rights reserved <span class="spacing"></span> <!--<a style='margin-left: 20%;' href='https://www.okmaster.life/Aboutokmaster'>合作提案</a> |--> <a href="/State">相關聲明</a></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </footer>
    {/if}
{foreach from=$css_footer_files key=key item=css_uri}
<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css" />
{/foreach}
{foreach from=$js_footer_files key=key item=js_uri}
<script src="{$js_uri}"></script>
{/foreach}
</div>
</body>

<script>
    $(".slider_center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 9
    });
</script>

</html>
