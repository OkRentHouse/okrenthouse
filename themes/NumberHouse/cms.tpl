<div class="cms{if $display_header} cms_header{/if}{if $display_footer} cms_foote{/if}">{$html}
{if !empty($id_cms)}
	{$model_html}
{/if}
</div>
{if !empty($cms_css)}
	<style>
		{$cms_css}
	</style>
{/if}
<script>
{if !empty($id_cms)}
	{$model_js}
{/if}
{if !empty($cms_js)}
		{$cms_js}
{/if}
</script>