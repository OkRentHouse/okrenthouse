<!doctype html>
<html>
<head>
<link rel="shortcut icon" href="/themes/Rent/img/1786/logo.png" type="image/vnd.microsoft.icon" />
<link rel="icon" href="/themes/Rent/img/1786/logo.png" type="image/vnd.microsoft.icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content=" user-scalable=yes">
	{if !empty($GOOGLE_SITE_VERIFICATION)}<meta name="google-site-verification" content="{$GOOGLE_SITE_VERIFICATION}" />{/if}
<title>{$meta_title}</title>
{if $meta_description != ''}<meta name="description" content="{$meta_description}">{/if}
{if $meta_keywords != ''}<meta name="keywords" content="{$meta_keywords}">{/if}
{if $meta_img != ''}<meta property="og:image" content="{$meta_img[0]}">{/if}
<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
{if !empty($t_color)}<meta name="theme-color" content="{$t_color}">{/if}
{if !empty($mn_color)}<meta name="msapplication-navbutton-color" content="{$mn_color}">{/if}
{if !empty($amwasb_style)}<meta name="apple-mobile-web-app-status-bar-style" content="{$amwasb_style}">{/if}
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
{include file="$tpl_dir./table_title.tpl"}
{foreach from=$css_files key=key item=css_uri}
	<link href="{$css_uri|escape:'html':'UTF-8'}" rel="stylesheet" type="text/css"/>
{/foreach}
<style type="text/css">
	{Configuration::get('Rent_css_code')}
</style>
{if !isset($display_header_javascript) || $display_header_javascript}
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-M3BZDSGV51"></script>
<script type="text/javascript">
{*var THEME_URL = '{$smarty.const.THEME_URL}';*}
</script>
{foreach from=$js_files key=key item=js_uri}
	<script type="text/javascript" src="{$js_uri}"></script>
{/foreach}
{if count($_errors_name) > 0}
<script type="text/javascript">
$(document).ready(function(e) {
{foreach from=$_errors_name item=err_name}
	{if count($_errors_name_i.$err_name) > 0}
		{foreach from=$_errors_name_i.$err_name item=ei}
			$('[name="{$err_name}"]').eq({$ei}).parents('.form-group').addClass('has-error');
			$('[name="{$err_name}"]').eq({$ei}).parent('td').addClass('has-error');
			$('[name="{$err_name}"]').focus(function(){
				$('[name="{$err_name}"]').eq({$ei}).parents('.form-group').removeClass('has-error');
				$('[name="{$err_name}"]').eq({$ei}).parent('td').removeClass('has-error');
			});
		{/foreach}
	{else}
		$('[name="{$err_name}"]').parents('.form-group').addClass('has-error');
		$('[name="{$err_name}"]').parent('td').addClass('has-error');
		$('[name="{$err_name}"]').focus(function(){
			$('[name="{$err_name}"]').parents('.form-group').removeClass('has-error');
			$('[name="{$err_name}"]').parent('td').removeClass('has-error');
		});
	{/if}
{/foreach}
});
</script>
{/if}
{/if}
</head>
<body {if empty($smarty.session.id_member)}onContextMenu="return false"{/if}>
<div class="display_content">
	{if $display_header}
		<header class="minbar">
            {include file="$tpl_dir./main_menu.tpl"}
		</header>
    {/if}
    <article class="{$mamber_url}">
    {$msg_box}
