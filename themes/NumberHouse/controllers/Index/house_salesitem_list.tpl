<div class="commodity">
	<div class="photo_row" style="background:#00ffcc;opacity: .9;">
		<a href="/RentHouse?id_rent_house={$v.id_rent_house}">
			<img src="{$v.img}" class="img" alt="">
			<collect data-id="{$v.id_rent_house}" data-type="rent_house" class="favorite {if $v.favorite}on{/if}">{if $v.favorite}
					<img src="{$THEME_URL}/img/icon/heart_on.svg">{else}<img src="{$THEME_URL}/img/icon/heart.svg">{/if}
		</a>

		<div style="opacity: .8;background:#989495;width:100%;text-align:center">
		<price style="position:relative;color:#fff">{$v.rent_cash}
			<unit>元</unit>
		</price>
		</div>
	</div><data>
		<h3 class="title big"><a href="/RentHouse?id_rent_house={$v.id_rent_house}" style="color:#000">{$v.case_name}</a></h3>
		<h3 class="title big_2"><a href="/RentHouse?id_rent_house={$v.id_rent_house}" style="color:#000">{$v.title}</a></h3>
		<div class="title_2"><img src="/themes/Rent/img/icon/icons8-address-50.png">{$v.rent_address}</div>
		<div class="title_4" style="color:#989495">
		{if ($v.room=='' || $v.room=='0') && ($v.hall=='' || $v.hall=='0')}開放空間
		{else}
			{if $v.room=='' || $v.room=='0'}{else}{$v.room}房{/if}
			{if $v.hall=='' || $v.hall=='0'}{else}{$v.hall}廳{/if}
			{if $v.muro=='' || $v.muro=='0'}{else}{$v.muro}室{/if}
			{if $v.kitchen=='' || $v.kitchen=='0'}{else}{$v.kitchen}廚{/if}
		{/if}
		{if $v.bathroom=='' || $v.bathroom=='0'}{else}{$v.bathroom}衛{/if}
			<!--<label class="rent_cash" style="color:#F1209F">{$v.rent_cash}</label>--><!--<i></i>--> &nbsp &nbsp &nbsp <span class="title_4_pin" style="color:#F1209F">{$v.rent_cash}萬</span></div>
	</data>
</div>
