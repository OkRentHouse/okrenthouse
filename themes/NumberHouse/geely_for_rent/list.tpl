<div class="rent_list">
    {foreach $geely_rent as $i => $item}<li><a href="/geely_for_rent?id={$item.id_geely_for_rent}"><div class="img">{if !empty($item.img)}<img src="{$item.img}">{else}<div class="no_img"><img src="{$THEME_URL}/img/icon/logo.svg"></div>{/if}</div><div class="title">{$item.title}</div><div class="time text-center"><div class="month">{substr($item.date, 5, 5)}</div><div class="year">{substr($item.date, 0, 4)}</div></div></a></li>{foreachelse}
		<li class="no_data"><div class="title">{l s="找不到相關資料"}</div></li>
    {/foreach}
</div>{include file='../pagination.tpl'} 