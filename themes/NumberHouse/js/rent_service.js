$(document).ready(function () {
	a_top(50);
	$('.rent_service a[href="#service_content"]').click(function () {
		var id = $(this).data('show');
		$('#service_content>div').stop('true', 'false').slideUp();
		if ($('#' + id).is(':hidden')) {
			$('#' + id).stop('true', 'false').slideDown();
		}
	});
});