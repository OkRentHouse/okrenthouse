var arr_city = [];
// var min_price = 0;
// var max_price = 0;//100000000;
// var min_price_v = 0;
// var max_price_v = 0;//100000000;
// var price_step = 0;
// var min_ping = 0;
// var max_ping = 0;
// var min_ping_v = 0;
// var max_ping_v = 0;
var ping_step = 1; //有效果
var min_room = 0; //有效果
var max_room = 6; //有效果

class SearchHouse {
	constructor($this) {
		this.tag = true;
		this.submit = false;
		this.div = $($this);
		this.arr_name = [];
		var t = this;
		$('input[name$="[]"]', this.div).each(function () {
			t.get_check_name();
		});

		//跳頁
		$('a', '.ajax_pagination').click(function () {
			var p = $(this).data('p');
			var href = t.change_url();

			var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
			if(!this.submit){
				history.pushState({}, 'title', url('path') + href + '&p=' + p);
				$('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + url('path').substr(1) + href.substr(1) + '&p=' + p);
				$body.stop(true,false).animate({
					scrollTop: 0
				}, 0, function() {
					location.reload();	//重新整理
					return false;
				});
				return false;
			}else{
				return true;
			}
		});

		this.div.submit(function () {
			//使用Submit
			if(t.submit){
				t.arr_name.forEach(function (name) {
					var value = [];
					$('input[name="' + name + '[]"]:checked', t.div).each(function () {
						value.push($(this).val());
					});
					$('input[name="' + name + '[]"]:checked', t.div).next('label').addClass('active');
					if ($('input[name="' + name + '_s"]', t.div).length == 0) {
						$(t.div).append('<input type="hidden" name="' + name + '_s" value="">');
					}
					$('input[name="' + name + '[]"]', t.div).remove();
					$('input[name="' + name + '_s"]', t.div).val(value.join());
				});
				return true;
			}else{
				location.reload();
				return false;
			}
		});
		t.change_url();
		//$(document).on('click', $this + ' .close_tag', function () { for 2021.01.22 tag_div move to breadcrumbs_htm after use
		$(document).on('click',' .close_tag', function () {
			var $samp = $(this).parents('samp');
			var id = $samp.data('id');			
			t.close_tag(id);
		}).on('click', $this + ' .close_all', function () {
			$('input[type="text"],input[type="number"],input[type="hidden"]', t.div).val(null);
			$('input[type="checkbox"], input[type="radio"]', t.div).prop('checked', false);
			$('input[type="radio"][value=""]', t.div).prop('checked', true);
			price_range(0, 0);
			ping_range(0, 0);
			room_range(0, 0);
			$('.county_city', t.div).html('全部');
			t.change_url();
		}).on($this+' input', function () {
			t.change_url();
		}).on('change', $this + ' input[type="checkbox"]', function () {
			var $this = $(this);
			t.on_all($this)
		}).on('click', $this + ' input[type="radio"]', function () {
			var $this = $(this);
			var $search_div = $this.parents('.search_div');
			$('input[type="checkbox"]', $search_div).prop('checked', false);
		// }).on('click', 'a[href="/HouseMap"]', function () {
		// 	var href = t.change_url();
		// 	history.pushState({}, 'title', '/HouseMap'+href);
		// 	$('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + 'HouseMap'+href.substr(1));
		// 	location.reload();	//重新整理
		// 	return false;
		// }).on('click', 'a[href="/list"]', function () {
		// 	var href = t.change_url();
		// 	history.pushState({}, 'title', '/list'+href);
		// 	$('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + 'list'+href.substr(1));
		// 	location.reload();	//重新整理
		// 	return false;
		});
	}

	close_tag(id){
		var t = this;
		switch (id) {
			case 'price':
				$('input#min_price', t.div).val(null);
				$('input#max_price', t.div).val(null);
				price_range(0, 0);
				break;
			case 'ping':
				$('input#min_ping', t.div).val(null);
				$('input#max_ping', t.div).val(null);
				ping_range(0, 0);
				break;
			case 'room':
				$('input#min_room', t.div).val(null);
				$('input#max_room', t.div).val(null);
				room_range(0, 0);
				break;
			case 'search':
				$('input#search', t.div).val(null);
				break;
			default:
				if(id.match('county') != null){
					$('.county_city', t.div).html('全部');
					$('.city_div', t.div).html('');
					$('samp[data-id^="city"]').remove();
				};
				var $input = $('input#'+id, t.div);
				$input.prop('checked', false);
				var $search_div = $input.parents('.search_div');
				if ($('input[type="checkbox"]', $search_div).length) {
					$('input[type="radio"][name!="county"]', $search_div).prop('checked', false);
				}
				t.on_all($input);
				break;
		}
		t.change_url();
		$('samp#'+id).remove();
	}

	get_check_name() {
		var t = this;
		$('input[name$="[]"]', this.div).each(function () {
			var name = $(this).prop('name').replace('[]', '');
			if ($.inArray(name, t.arr_name) < 0) {
				t.arr_name.push(name);
			}
		});
	}

	on_all($this){
		var $search_div = $this.parents('.search_div');
		if ($('input[type="checkbox"]:checked', $search_div).length) {
			$('input[type="radio"][name!="county"]', $search_div).prop('checked', false);
		}else{
			$('input[type="radio"][name!="county"]', $search_div).prop('checked', true);
		}
	}

	change_url() {
		var href = '?';
		var t = this;

		var p = $("#p").val();
		href = href+'&p='+p;

		var html = '';
		t.get_check_name();

		if($('#search', t.div).length && $('#search', t.div).val()){
			href += '&search' + '=' + $('#search', t.div).val();
		}

		$('input[type="radio"]:checked', t.div).each(function () {
			var name = $(this).prop('name');
			var val = $(this).val();
			if (name != '' && name != undefined && name != 'undefined' && val != '' && val != '0') {
				href += '&' + name + '=' + val;
				if(t.tag){
					var id = $(this).attr('id');
					var text = $(this).next('label').html();
					html += '<samp data-id="'+id+'"><em>'+text+'</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
				}
			}
		});

		// $('input[type!="checkbox"][type!="radio"]', t.div).each(function () {
		// 	var name = $(this).prop('name');
		// 	var val = $(this).val();
		// 	if (name != '' && name != undefined && name != 'undefined' && val != '' && val != '0') {
		// 		href += '&' + name + '=' + $(this).val();
		// 	}
		// });

		t.arr_name.forEach(function (name) {
			var value = [];
			$('input[name="' + name + '[]"]:checked', t.div).each(function () {
				value.push($(this).val());
				if(t.tag){
					var id = $(this).attr('id');
					var text = $(this).next('label').html();
					html += '<samp data-id="'+id+'"><em>'+text+'</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
				}
			});
			if (value.length > 0) {
				href += '&' + name + '_s' + '=' + value;
			}
		});

		if($('#min_price', t.div).length || $('#max_price', t.div).length){
			var min_price = $('#min_price', t.div).val();
			var max_price = $('#max_price', t.div).val();
			if(min_price > 0 || max_price > 0){
				href += '&price' + '=' + min_price+','+max_price;
				if(t.tag) {
					html += '<samp data-id="price"><em>' + min_price + ' ~ ' + max_price + '元</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
				}
			}
		}

		if($('#min_ping', t.div).length || $('#max_ping', t.div).length){
			var min_ping = $('#min_ping', t.div).val();
			var max_ping = $('#max_ping', t.div).val();
			if(min_ping > 0 || max_ping > 0){
				href += '&ping' + '=' + min_ping+','+max_ping;
				if(t.tag) {
					html += '<samp data-id="ping"><em>' + min_ping + ' ~ ' + max_ping + '坪</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
				}
			}
		}

		if($('#min_room', t.div).length || $('#max_room', t.div).length){
			var min_room = $('#min_room', t.div).val();
			var max_room = $('#max_room', t.div).val();
			if(min_room > 0){
				$("#room_0").hide();$("#min_room").show()}
				else{
					$("#room_0").show();$("#min_room").hide()}
			if(min_room > 0 || max_room > 0) {
				href += '&room' + '=' + $('#min_room', t.div).val() + ',' + $('#max_room', t.div).val();
				if (t.tag) {
					html += '<samp data-id="room"><em>' + $('.room', t.div).html() + ' ' + $('.room_unit', t.div).html() + '</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
					$('.room', t.div).html();
					$('.room_unit', t.div).html();
				}
			}
		}

		if($('#search', t.div).length && $('#search', t.div).val()){
			href += '&search' + '=' + $('#search', t.div).val();
			if (t.tag) {
				html += '<samp data-id="search"><em>搜尋：' + $('#search', t.div).val() + '</em><i class="close_tag glyphicon glyphicon-remove"></i></samp>';
				$('.room', t.div).html();
				$('.room_unit', t.div).html();
			}
		}

		//User 點進階搜尋時
		if(html == ""){
			//$(".process_bar").css("display","none");
		}

		//User 點 放大鏡 進入 進階搜尋時
		if(html != ""){
			$(".search_txt").addClass("input_txt_h");
			$(".search_txt").removeClass("input_txt");
			$(".process_bar").css("display","none");
			$("article .breadcrumb		").css("padding-bottom", "1em");
		}

		var find_List_d = false;
		for(var i=0;i<$(".List_d a").length;i++){
				if($(".List_d a").eq(i).text() == "條件搜尋"){
					find_List_d = true;

				}
		}
		if(find_List_d){
			console.log("find_List_d:"+find_List_d);
			$('.tag_div_0122').html(html);
			$('.tag_div', t.div).html(html);
		}

		if(!t.submit){
			var p = $("#p").val();
			history.pushState({}, 'title', url('path') + href)
			$('img.qrCode_img', '.qr_code').attr('src', '/modules/FloatShare/FloatShare.php?qe_code=' + url('path').substr(1) + href.substr(1)+ '&p=' + p);
		};

		return href;
	};

	changeInput(name) {
		$(document).on('change', '#search_box_big input[name$="[]"]');
	}
}

var st = new SearchHouse('#search_box_big');

var price_range = (min, max) => {
	$('#price_range').slider({
		range: true,
		min: min_price,
		max: max_price,
		values: [min, max],
		step: price_step,
		slide: function (event, ui) {
			$('#min_price').val(ui.values[0]);
			$('#max_price').val(ui.values[1]);
		},
		stop: function (event, ui) {
			st.change_url();
		},
	});
};
var ping_range = (min, max) => {
	$('#ping_range').slider({
		range: true,
		min: min_ping,
		max: max_ping,
		values: [min, max],
		step: ping_step,
		slide: function (event, ui) {
			$('#min_ping').val(ui.values[0]);
			$('#max_ping').val(ui.values[1]);
		},
		stop: function (event, ui) {
			st.change_url();
		},
	});
};
var room_range = (min, max) => {
	$('#room_range').slider({
		range: true,
		min: min_room,
		max: max_room,
		values: [min, max],
		step: 1,
		slide: function (event, ui) {
			$('#min_room').val(ui.values[0]);
			$('#max_room').val(ui.values[1]);
			span_txt(ui.values[0], ui.values[1]);
		},
		stop: function (event, ui) {
			st.change_url();
		},
	});
	span_txt(min, max);
};

function span_txt(min, max) {
	$("input#max_room").css({opacity:1});
	var unit = '房';
	var room_txt = min + ' ~ ' + max;
	if (min == 0 && max == 0) {
		// room_txt = '所有格局';
		room_txt='';
		unit = '房';
	} else if (min > 0 && max == 0) {
		room_txt = min;
		unit = '房以上';
	} else if (min == 0 && max > 0) {
		room_txt = '0 ~ ' + max;
		// unit = '房以下';
		unit = '房';
	} else if (min == max) {
		room_txt = max;
		unit = '房';
		if (max == max_room) {
			unit = '房以上';
		}
	} else if (min > 0 && max > 0) {
		room_txt = min + ' ~ ' + max;
		if (max == max_room) {
			unit = '房以上';
		}
	}
	if (max == max_room) {
		unit = '5+房以上';
		$("input#max_room").css({opacity:0});
	}
	$('.room', '#search_box_big').html(room_txt);
	$('.room_unit', '#search_box_big').html(unit);
}

function show_city_div() {
	var html = '';
	var county = $('input[name="county"]:checked', '#search_box_big').val();
	$.each(arr_city[county], function (index, item) {
		html += '<span class="city"><input type="checkbox" name="city[]" id="city_' + index + '" value="' + item + '"><label for="city_' + index + '">' + item + '</label></span>';
	});
	$('.city_div', '#search_box_big').html(html);
	$('.city_div', '#search_box_big').show();
	$('.county_div', '#search_box_big').hide();
}

function city_div() {
	var html = $('#search_box_big input[name="county"]:checked').val();
	$('#search_box_big input[name^="city"]:checked').each(function (index) {
		if (index > 0) {
			html += '、';
		}
		html += $(this).val();
	});
	//$('.county_city', '#search_box_big').html(html);
	$('.county_city', '#search_box_big').html("區域："+html);
}

function close_search_box_div(event) {
	if (!$('.search, .county_div, .county_div *, .city_div, .city_div *', '#search_box_big').is(event.target)) {
		$('.county_div', '#search_box_big').hide();
		$('.city_div', '#search_box_big').hide();
	}
}
function process_sel(){
	var pL=0;
	//if($(".county_city").text() != "所有縣市" && $(".county_div").css("display") == "none"){
	if($(".county_city").text() != "區域：所有縣市" && $(".county_div").css("display") == "none"){
		//console.log($(".search_div").eq(1).text())
		$(".search_div").eq(1).show(300);
		pL = add_line(pL);
	}

	if($("input[name='id_main_house_class[]']:checked").val() != undefined)
	{
		$(".search_div").eq(2).show(300);
		pL = add_line(pL);
	}

	if($("input[name='id_types[]']:checked").val() != undefined)
	{
		$(".search_div").eq(3).show(300);
		pL = add_line(pL);
	}

	if($("input[name='id_type[]']:checked").val() != undefined )
	{
		$(".search_div").show(300);
		pL = add_line(pL);
	}

	if( $("#min_room").val() > 0 || $("#max_room").val() > 0)
	{
		$(".search_div").show(300);
		pL = add_line(pL);
	}

	if( $("#min_ping").val() > 0 || $("#max_ping").val() > 0)
	{
		$(".search_div").show(300);
		pL = add_line(pL);
	}

	if($("input[name='id_other[]']:checked").val() != undefined)
	{
		$(".search_div").show(300);
		pL = add_line(pL);
	}

	if( $("#min_price").val() > 0 || $("#max_price").val() > 0)
	{
		$(".search_div").show(300);
		pL = add_line(pL);
	}
}

function add_line(pL){
	/*	$("[name='process_bar_line']").eq(pL).removeClass("process_bar_line_h");
	$("[name='process_bar_line']").eq(pL).addClass("process_bar_line");
	$("[name='arrow']").eq(pL).css("border-top","1px solid #512779");
	$("[name='arrow']").eq(pL).css("border-left","1px solid #512779");	*/	$("[name='process_bar_fas']").eq(pL).removeClass("fas_h");	$("[name='process_bar_fas']").eq(pL).addClass("fas");
	pL++;
	$("[name='process_bar_t']").eq(pL).addClass("process_bar_t");
	return pL;
}		$(window).scroll(	function () {								p = $(".search_forms_wrap");			 		var position2 = p.offset();
		//console.log( (search_txt_top - $(window).scrollTop()) + " < " + parseInt($(".main_menu").css("height")) + " ? " + (search_txt_top - $(window).scrollTop() < parseInt($(".main_menu").css("height"))) );
		//console.log( (position2.top + parseInt($(".search_forms_wrap").css("height"))) + " > " + $(window).scrollTop() + " ? " + ((position2.top + parseInt($(".search_forms_wrap").css("height"))) > $(window).scrollTop()) );
		if( search_txt_top - $(window).scrollTop() < parseInt($(".main_menu").css("height")) && (position2.top + parseInt($(".search_forms_wrap").css("height"))) > $(window).scrollTop() ){
		$(".search_txt").addClass("input_txt_h");
		$(".search_txt").removeClass("input_txt");
		$("label.search_remid").addClass("search_remid_show");
		}else{

		//$(".search_txt").addClass("input_txt");
		//$(".search_txt").removeClass("input_txt_h");
		//$("label.search_remid").removeClass("search_remid_show");
		};
		});
		var search_txt_top; //$(window).scroll 判斷卷軸 是否超過搜尋列



$(document).ready(function () {
	var p = $(".search_txt");
	var position = p.offset();
	search_txt_top = position.top;
	min_price_v = $('#min_price').val();
	max_price_v = $('#max_price').val();
	min_ping_v = $('#min_ping').val();
	max_ping_v = $('#max_ping').val();
	min_room_v = $('#min_room').val();
	max_room_v = $('#max_room').val();
	price_range(min_price_v, max_price_v);
	ping_range(min_ping_v, max_ping_v);
	room_range(min_room_v, max_room_v);
	span_txt(min_room_v, max_room_v);

	$(".bont").click(function(){
      var item=$(this).parent().find(".items");
      if($(item).css("display")=="none"){$(item).show(100)}else{$(item).hide(10)};
			$(this).find("label").remove();
    })

    $(".items label").click(function(){

       var bont = $(this).parents(".search_div").find(".bont").text();

       if(bont.indexOf($(this).text())>=0){
         bont = bont.replace(","+$(this).text(),"");
         bont = bont.replace("："+$(this).text(),"");
        bont = bont.replace($(this).text(),"");
        //console.log( "replace" );
        }else{
          if(bont.indexOf("：")<0){
            bont = bont + "：" + $(this).text();
          }else{
            bont = bont + "," + $(this).text();
          }

          //console.log( "+" );
        }

       $(this).parents(".search_div").find(".bont").text(bont);

    })



	process_sel();//如果有被點選的話則要做該動作

	$('#min_price').on('change', function () {
		price_range($(this).val(), $('#max_price').val());
	});
	$('#max_price').on('change', function () {
		price_range($('#min_price').val(), $(this).val());
	});
	$('#min_ping').on('change', function () {
		ping_range($(this).val(), $('#max_ping').val());
	});
	$('#max_ping').on('change', function () {
		ping_range($('#min_ping').val(), $(this).val());
	});
	$('#min_room').on('change', function () {
		span_txt($(this).val(), $('#max_room').val());
		room_range($(this).val(), $('#max_room').val());
	});
	$('#max_room').on('change', function () {
		span_txt($('#min_room').val(), $(this).val());
		room_range($('#min_room').val(), $(this).val());
	});
	st = new SearchHouse('#search_box_big');
	if($('article.Index').length){
		st.tag = false;
		st.submit = true;
		$('samp', '#search_box_big').remove();
	}
}).on('click', '#search_forms', function () {
		setTimeout(function(){
			process_sel();
		},200);
}).on('click', '#search_box_big input[name="county"]', function () {
	show_city_div();
	city_div();
}).on('click', '#search_box_big input[name^="city"]', function () {
	city_div();
}).on('click', '#search_box_big .county_city', function () {
	$('.county_div', '#search_box_big').show();
	$('.city_div', '#search_box_big').hide();
}).on('click', '.search_div input[name="id_main_house_class[]"]', function () {
	setTimeout(function(){
		process_sel();
	},200);
}).on('click', '#room_range', function () {
	setTimeout(function(){
		process_sel();
	},200);
}).on('click', '#ping_range', function () {
	setTimeout(function(){
		process_sel();
	},200);
}).on('click', '#price_range', function () {
	setTimeout(function(){
		process_sel();
	},200);
}).on('click', '.search_div input[name="id_other[]"]', function () {
	setTimeout(function(){
		process_sel();
	},200);
}).on('click', 'body', function (event) {
	close_search_box_div(event);
	close_items_box_div(event);
}).on('click', 'span.city', function (event) {
  console.log("1");
  $(".sec_class").show(200,function(){
    $(".sec_class").css({display:"flex"})
  });

});

function close_items_box_div(event) {
	if (!$('.bont,.items *').is(event.target)) {
		$('.items').hide();
	}
}
