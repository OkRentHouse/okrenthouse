$(document).ready(function () {
	$('.nav .dropdown').hover(function () {
		$(this).find('.dropdown-menu').delay(50).fadeIn(200);
	}, function () {
		$(this).find('.dropdown-menu').delay(50).fadeOut(200);
	});
	$('#element').hcSticky({
		stickTo: '#content'
	});
});