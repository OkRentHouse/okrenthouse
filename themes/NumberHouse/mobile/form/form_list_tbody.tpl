<div>{strip}
{if count($fields.list_val) > 0}
{foreach from=$fields.list_val key=fi item=list}
	<a id="tr_{$fi}"{if isset($list.tr_class)} class="{$list.tr_class} pointer"{/if} href="{$form_action|escape:'html':'UTF-8'}{if in_array('list', $actions)}&amp;list{$table}{elseif in_array('view', $actions) && in_array('view', $post_processing)}&amp;view{$table}{elseif in_array('edit', $actions)}&amp;edit{$table}{elseif in_array('view', $actions)}&amp;view{$table}{/if}&amp;{$fields.index}={$list[$fields.index]}" data-transition="slide">
      {foreach from=$fields.list key=i item=thead}
		{$i = str_replace('!', '_', $i)}
      	<div class="inline-block {$thead.class}{if $thead.hidden} hidden{/if}">
		      {if isset($thead.index2)}{$list.index2=$list.$i}{/if}
            {if isset($thead['type'])}
			{if $thead['type'] == 'icon'}
				<i class="{$list.icon}"></i>
			{else}
				<input type="{$thead.type}" name="{$i}[]" value="{$list.index}">
			{/if}
            {else}
            	{if isset($thead.values)}
				{$in_arr=false}
				{if isset($thead.in_table) || $thead.in_table}
					{$list.$i}
				{else}
					{foreach $thead.values key=thv_i item=val_v}
						{if $list.$i == $val_v.value}<samp class="{$val_v.class}">{$val_v.title}</samp>{$in_arr=true}{break}{/if}
					{/foreach}
					{if $in_arr == false}<samp></samp>{/if}
				{/if}
			{else}
				{if isset($thead.number_format)}{$list.$i|number_format:$thead.number_format:"0":","}{elseif empty($list.$i) || $list.$i == '0000-00-00' || $list.$i == '0000-00-00 00:00:00'}<samp></samp>{else}{$list.$i}{/if}
			{/if}
            {/if}
		</div>
	{/foreach}
	</a>
{/foreach}
{else}
<div><div class="text-center no_information" colspan="{if $has_actions && $display_edit_div}{count($fields.list)+1}{else}{count($fields.list)}{/if}">{$no_information}</div><div>
{/if}{/strip}
</div>