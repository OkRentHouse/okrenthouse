{$form_list_pagination}
{$form_list_field_list}{if count($fields.list) > 0}<form action="" method="get" class="list" accept-charset="utf-8">{/if}
	{if $fields.list_body_display}
      <table class="table">{strip}
            {*{$form_list_thead}*}
            {$form_list_tbody}
            {$form_list_tfoot}
      {/strip}</table>
	{/if}
      {if $fields.list_footer_display}<div class="panel-footer">{$fields.list_footer}</div>{/if}
{$form_list_pagination}
<input type="hidden" name="p" value="{$smarty.get.p}" />
<input type="hidden" name="m" value="{$smarty.get.m}" />
{if count($fields.list) > 0}</form>{/if}