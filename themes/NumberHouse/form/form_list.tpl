<form action="" method="get" class="list" accept-charset="utf-8">
<div class="panel panel-default">
	{if $fields.list_heading_display}<div class="panel-heading">{$fields.title} <samp class="badge label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s="搜尋筆數"}">{$table_num}</samp>{if isset($fields.show_list_num) && !empty($fields.show_list_num)}<div class="pull-right input-group w180 pieces"><span class="input-group-addon">{l s="顯示"}</span><input type="number" min="0" id="{$list_id}Filter_m" name="{$list_id}Filter_m" class="form-control input-sm" value="{$fields.list_num}" title="{l s="顯示幾筆資料"}" placeholder="{l s="50"}"><span class="input-group-addon">{l s="筆資料"}</div>{/if}</div>{/if}
      {if $fields.list_body_display}
	<div class="panel-body">
      <table class="table table-bordered">{strip}
            {$form_list_thead}
            {$form_list_tbody}
            {$form_list_tfoot}
      {/strip}</table>
	</div>
	{/if}
      {if $fields.list_footer_display}<div class="panel-footer">{$fields.list_footer}</div>{/if}
</div>
{$form_list_pagination}
<input type="hidden" name="p" value="{$smarty.get.p}" />
<input type="hidden" name="m" value="{$smarty.get.m}" />
</form>