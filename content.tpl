      <div data-role="content" id="Coupon" class="ui-content" role="main">
            {include file="$tpl_dir./mobile/coupon/min_menu.tpl"}
            <div class="activity_list_wrap">
                <div class="list">
                    <div class="label">
                        <div class="caption1">大吉大利 拿好康</div>
                        <div class="caption2">快來兌換你的專屬好康</div>
                    </div>
                    <a class="ui-link" href="/SearchStore">{if !empty($point_img)}<img class="" src="{$point_img}">{/if}</a>
                </div>
                <div class="list">
                    <div class="label">
                        <div class="caption1">租約健檢 免費諮詢</div>
                        <div class="caption2">小心條款陷阱 以免權益受損</div>
                    </div>
                   <a class="ui-link" href="/SearchStore">{if !empty($lease_img)}<img class="" src="{$lease_img}">{/if}</a>
                </div>
                <div class="list">
                    <div class="label">
                        <div class="caption1">樂租 好好客</div>
                        <div class="caption2">立馬推薦送好禮</div>
                    </div>
                    <a class="ui-link" href="/SearchStore">{if !empty($rent_img)}<img class="" src="{$rent_img}">{/if}</a>
                </div>
                <div class="list">
                    <div class="label">
                        <div class="caption1">樂租房東ing</div>
                        <div class="caption2">讓您租事順利</div>
                    </div>
                    <a class="ui-link" href="/SearchStore">{if !empty($happy_img)}<img class="" src="{$happy_img}">{/if}</a>
                </div>
                <div class="list">
                    <div class="label">
                        <div class="caption1">租屋不踩雷</div>
                        <div class="caption2">趨吉避凶 惡房東退散</div>
                    </div>
                    <a class="ui-link" href="/SearchStore">{if !empty($zele_img)}<img class="" src="{$zele_img}">{/if}</a>
                </div>
            </div>
        </div>
