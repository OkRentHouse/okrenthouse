<?php
global $start_time;
$start_time = microtime(true);
define('DS', DIRECTORY_SEPARATOR);				//
include_once('LilyHouse.php');
LilyHouse::getContext()->cache = true;	//Config::get(WEB_CACHE);
LilyHouse::getContext()->debug = false;
LilyHouse::getContext()->type = 'module';
//todo 判別網站
LilyHouse::getContext()->arr_web = [
    'okdeal.house' => [
        'web_dir' => 'web_house',
    ],
	'okrent.house' => [
		'web_dir' => 'web_rent',
	],
	'app.life-house.com.tw' => [
		'web_dir' => 'web_app',
	],
	'app.lifegroup.house' => [
		'web_dir' => 'web_app',
	],
    'app.okrent.tw' => [
        'web_dir' => 'web_app_okrent',
    ],
	'lifegroup.house' => [
		'web_dir' => 'web_life_house',
	],
    'okmaster.life' => [
        'web_dir' => 'web_okmaster',
    ],
    'epro.house' => [
        'web_dir' => 'web_epro',
    ],
    'okrent.tw' => [
        'web_dir' => 'web_okrent',
    ],
    'okpt.life' => [
        'web_dir' => 'web_okpt'
    ],
	'104.199.245.59' => [
		'web_dir' => 'web_rent',
	],
    'shar.help' => [
        'web_dir' => 'web_okrent',
    ],
    'app.shar.help' => [
        'web_dir' => 'web_app_okrent',
    ],
    'www.shar.help' => [
        'web_dir' => 'web_okrent',
    ],
    '1786.house' => [
        'web_dir' => 'web_number_house',
    ],
    'www.1786.house' => [
        'web_dir' => 'web_number_house',
    ],
];

LilyHouse::getContext()->run();