var initialPreview = [
       '/file/a.jpg',
        '/file/b.jpg',
        'http://kartik-v.github.io/bootstrap-fileinput-samples/samples/pdf-sample.pdf',
        "http://kartik-v.github.io/bootstrap-fileinput-samples/samples/small.mp4",
    ];
var initialPreviewConfig = [
        {caption: "a.jpg", size: 762980, url: "/Service?&viewservice&id_service=1", key: 8},
        {caption: "b.jpg", size: 823782, url: "/Service?&viewservice&id_service=1", key: 9}, 
        {type: "pdf", size: 8000, caption: "PDF-Sample.pdf", url: "/file-upload-batch/2", key: 10}, 
        {type: "video", size: 375000, filetype: "video/mp4", caption: "KrajeeSample.mp4", url: "/file-upload-batch/2", key: 14},  
    ]
    var UpFileVal = [];
$("#input-pa").fileinput({
    uploadUrl: document.location.pathname,
    uploadAsync: false,
    autoUpload : true,
    language : "zh-TW",
    minFileCount: 1,	//最小上傳數量
    maxFileCount: 'auto',	//最大上傳數量
    overwriteInitial: false,
    initialPreview: initialPreview,
    initialPreviewAsData: true, // defaults markup
    initialPreviewFileType: 'image', // 默認type
    initialPreviewConfig: initialPreviewConfig,
    uploadExtraData: {
	  ajax: true,
	  action : 'UpFile',
        val: UpFileVal
    }
}).on('filesorted', function(e, params) {
    console.log('File sorted params', params);
}).on('fileuploaded', function(e, params) {
    console.log('File uploaded params', params);
});