MyAdvModel_v = 1.0;
function paddingLeft(str,lenght){
	str=''+str;
	if(str.length<lenght){
		j=lenght-str.length;
		str='0'+str;
	};
	return str
};
function array_key_exists (key, search) {
	// http://jsphp.co/jsphp/fn/view/array_key_exists
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Felix Geisendoerfer (http://www.debuggable.com/felix)
	// *     example 1: array_key_exists('kevin', {'kevin': 'van Zonneveld'});
	// *     returns 1: true
	// input sanitation
	if (!search || (search.constructor !== Array && search.constructor !== Object)) {
		return false;
	}
	return key in search;
}
function big5(str) {
	return str.replace(/[^\u0000-\u00FF]/g,function($0){return escape($0).replace(/(%u)(\w{4})/g,"&#x$2;")});
}
function decodeEntities(s){ 
	var str, temp=document.createElement('p'); 
	temp.innerHTML= s; 
	str= temp.textContent || temp.innerText; 
	temp=null; 
	return str;
};
Date.prototype.yyyymmdd = function() {
	var mm = this.getMonth() + 1; // getMonth() is zero-based
	var dd = this.getDate();

	return [this.getFullYear(),'-',
		(mm>9 ? '' : '0') + mm,'-',
		(dd>9 ? '' : '0') + dd
	].join('');
};
Date.prototype.hhmmss = function () {
	var hours   = this.getHours();
	var minutes = this.getMinutes();
	var seconds = this.getSeconds();

	if (hours   < 10) {hours   = "0"+hours;}
	if (minutes < 10) {minutes = "0"+minutes;}
	if (seconds < 10) {seconds = "0"+seconds;}
	return hours+':'+minutes+':'+seconds;
}
Date.prototype.hhmm = function () {
	var hours   = this.getHours();
	var minutes = this.getMinutes();

	if (hours   < 10) {hours   = "0"+hours;}
	if (minutes < 10) {minutes = "0"+minutes;}
	return hours+':'+minutes;
}
function getDate(){
	var date = new Date();
	return date.yyyymmdd();
};
function getTime(str){
	var date = new Date();
	if(isNaN(str) || str == 'hhmmss') {
		return date.hhmmss();
	}else if(str == 'hhmm'){
		return date.hhmm();
	};
};
function diffDateTime(datetime1, datetime2){
	var d1 = new Date(datetime1);
	var d2 = new Date(datetime2);
	return parseInt(d2 - d1);
};
function diffTime(time1, time2){
	var _startTime = time1.split(':');
	var _endTime = time2.split(':');
	var startDate = new Date(0, 0, 0, _startTime[0], _startTime[1], 0);
	var endDate = new Date(0, 0, 0, _endTime[0], _endTime[1], 0);
	var diff = endDate.getTime() - startDate.getTime();
	return diff;
};
function formatNumber(str, glue) {
	// 如果傳入必需為數字型參數，不然就噴 isNaN 回去
	if(isNaN(str)) {
		return NaN;
	}
	// 決定三個位數的分隔符號
	var glue = (typeof glue == 'string') ? glue: ',';
	var digits = str.toString().split('.'); // 先分左邊跟小數點
	var integerDigits = digits[0].split(''); // 獎整數的部分切割成陣列
	var threeDigits = []; // 用來存放3個位數的陣列
	// 當數字足夠，從後面取出三個位數，轉成字串塞回 threeDigits
	while (integerDigits.length > 3) {
		threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(''));
	}
	threeDigits.unshift(integerDigits.join(''));
	digits[0] = threeDigits.join(glue);

	return digits.join('.');
};
//改變選單父-子
function change_option(){
	//改變選單父-子
	var arr_parent_name = new Array();
	var uniques = new Array();
	$('option[data-parent_name]').each(function(index, element){
		arr_parent_name.push($(this).data('parent_name'));
	});
	//刪除重複
	arr_parent_name.forEach(function(value) {
		if (uniques.indexOf(value) == -1) {
			uniques.push(value);
		};
	});
	arr_parent_name = uniques;
	uniques = [];
	if(arr_parent_name.length){
		arr_parent_name.forEach(function(value) {
			if($('select[name="'+value+'"]').length > 0){
				$('select[name="'+value+'"]').each(function(index, element) {
					$('option[data-parent_name="'+value+'"]').hide();
					$('option[data-parent_name="'+value+'"][value=""]').show();
					var val_v = $(this).val();
					if(Array.isArray(val_v)){
						if(val_v.indexOf('all') == -1 && val_v.indexOf('') == -1){
							val_v.forEach(function(v) {
								$('option[data-parent="'+v+'"]').show();
								$('option[data-parent*="_'+v+'_"]').show();
								$('option[data-parent="'+v+'"][value=""]').show();
							});
						}else{
							$('option[data-parent_name="'+value+'"]').show();
						};
					}else{
						$('option[data-parent="'+val_v+'"]').show();
						$('option[data-parent*="_'+val_v+'_"]').show();
						$('option[data-parent="'+val_v+'"][value=""]').show();
						if(val_v == '' || val_v == 'all'){
							$('option[data-parent_name="'+value+'"]').show();
						};
					};
					$('option[data-parent_name="'+value+'"][style="display: none;"]').removeAttr('selected');
				});
				$('select[name="'+value+'"]').change(function(){
					var $p_option = $('option[data-parent_name="'+value+'"]');
					var $p_select = $p_option.parents('select');
					var $p_dropdown = $p_select.prev('div.dropdown-menu');
					var $button = $p_dropdown.prev('button.btn');
					var y = false;
					if($p_dropdown.length > 0){	//是否有使用 bootstrap-select.js
						y = true;
					};
					$('option[data-parent_name="'+value+'"][value=""]').show().attr('selected', true);
					$('option[data-parent_name="'+value+'"][value="all"]').attr('selected', true);
					$p_option.siblings('option[value=""]').attr('selected', true);
					$p_option.siblings('option[value="all"]').attr('selected', true);
					var val_v = $(this).val();
					//todo
					if(Array.isArray(val_v)){
						if(val_v.indexOf('all') == -1 && val_v.indexOf('') == -1){
							$p_option.hide();
							if(y){
								$('li a', $p_dropdown).hide();
							};
							val_v.forEach(function(v) {
								var $o1 = $('option[data-parent="'+v+'"]');
								var $o2 = $('option[data-parent*="_'+v+'_"]');
								$o1.show();
								$o2.show();
								if(y){
									$p_dropdown.each(function(){
										var $o_l = $(this).next('select');
										$('option[data-parent="'+v+'"]', $o_l).each(function(i){
											$('li[data-original-index="'+$o1.eq(i).index()+'"] a', $p_dropdown).show();
										});
										$('option[data-parent*="_'+v+'_"]').each(function(i){
											$('li[data-original-index="'+$o1.eq(i).index()+'"] a', $p_dropdown).show();
										});
									});
								};
							});
						}else{
							$p_option.show();
							$('option[data-parent_name="'+value+'"][value=""]').show();
						};
					}else{
						if((val_v != 'all') && (val_v != '')){
							var $o1 = $('option[data-parent="'+val_v+'"]');
							var $o2 = $('option[data-parent*="_'+val_v+'_"]');
							$p_option.hide();
							$('option[data-parent_name="'+value+'"][value=""]').show();
							$o1.show();
							$o2.show();
							if(y){
								$('li a', $p_dropdown).hide();
								$p_dropdown.each(function(){
									var $o_l = $(this).next('select');
									$('option[data-parent="'+v+'"]', $o_l).each(function(i){
										$('li[data-original-index="'+$o1.eq(i).index()+'"] a', $p_dropdown).show();
									});
									$('option[data-parent*="_'+v+'_"]').each(function(i){
										$('li[data-original-index="'+$o1.eq(i).index()+'"] a', $p_dropdown).show();
									});
								});
							};
						}else{
							$p_option.show();
							$('option[data-parent_name="'+value+'"][value=""]').show();
						};
					};
					if(y) {
						$('li a[style="display: none;"]', $p_dropdown).each(function () {
							$(this).attr('aria-selected', 'false');
							$(this).parent('li').removeClass('selected');
						});
						var $p_dropdown_a = $('li a[style="display: block;"][aria-selected="true"]', $p_dropdown);
						var l = $p_dropdown_a.length;
						if (l > 0) {
							var txt = '已經選取'+l+'個項目';
							if(l == 1){	//todo
								txt = $('span.text', $p_dropdown_a).html();
							};
							$button.attr('title', txt).removeClass('bs-placeholder');
							$('span.filter-option', $button).html(txt);
						}else{
							$button.attr('title', '全選').addClass('bs-placeholder');
							$('span.filter-option', $button).html('全選');
						};
					};
					$('option[data-parent_name="'+value+'"][style="display: none;"]').attr('selected', false);
					$('option[data-parent="'+val_v+'"][value=""]').attr('selected', true);
				});
			}else{
				$('input[name="'+value+'"]').each(function(index, element) {
					$('option[data-parent_name="'+value+'"]').hide();
					$('option[data-parent_name="'+value+'"][value=""]').show();
					$('option[data-parent="'+$(this).val()+'"]').show();
					$('option[data-parent*="_'+$(this).val()+'_"]').show();
				});
			};
		});;
	};
};
function all_checkbox(_all_che_class,_che_class_t){
	var _all_che_class = $(_all_che_class);
	var _che_class = $(_che_class_t);
	_all_che_class.click(function(){
		if(_all_che_class.prop('checked')){
			_che_class.prop('checked',true);
			_che_class.parent('label').addClass('label_checkbox');
		}else{
			_che_class.prop('checked',false);
			_che_class.parent('label').removeClass('label_checkbox');
		};
	});
	_che_class.click(function(){
		if($(this).prop('checked')){
			$(this).parent('label').addClass('label_checkbox');
		}else{
			$(this).parent('label').removeClass('label_checkbox');
		};
		if(_che_class.length == $(_che_class_t+':checked').length){
			_all_che_class.prop('checked',true);
		}else{
			_all_che_class.prop('checked',false);
		};
	});

	if(_che_class.length == $(_che_class_t+'[checked]').length){
		_all_che_class.prop('checked',true);
	};
};
function add_top_dim($dom){
	if($("#top").length==0){
		var _html='<div id="top"><img src="images/top.png" alt="top"></div>';
		$($dom).append(_html);   
	};
	var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
	var $window=$(window);
	var $document=$(document);
	var _top=$('#top');
	_top.click(function(){
		$body.stop(true,false).animate({
			scrollTop:0
		},'slow');
	});
	top_xy();
	$document.scroll(function(){
		top_xy();
	});
	$window.resize(function(e){
		top_xy();
	});
};
function top_xy(){
	var $window=$(window);
	var $document=$(document);
	$window_height=$window.height();
	$document_top=$document.scrollTop();
	if($document_top>($window_height/4)){
		$opacity=1;
	}else{
		$opacity=0;
	};
	$("#top").stop(true,false).animate({
		top: $document_top + ($window_height/4*3),
		opacity:$opacity
	},'slow');
};

function a_top(){
	var _top = arguments[0];
	if(_top == undefined)_top = 0;
	$('a[href^="#"]').click(function(){
		var t = $(this).attr('href');
		scrollTop($(t).offset().top - _top);
		 var state = { href: $(this).attr('href') };
		history.pushState(state, document.title, state.href);
		return false;
	});
	$('a[href="#top"]').click(function(){
		scrollTop(0);
		return false;
	});
};
/*滑鼠滾輪捲動*/
function MouseWheel (e) {
	var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
	if(!$body.is(":animated")){
		e = e || window.event;
		var r = (e.wheelDelta <= 0 || e.detail > 0) ? 0 : 1;
		var max_i = $('a[href^="#"]').length;
		var tt = 0;
		if(r == 1){
			mous_i--;
			tt = 1;
		}else{
			mous_i++;
			tt = -1;
		};
		if(mous_i >= max_i){
			mous_i = max_i-1;
		}else if(mous_i < 0){
			mous_i = 0;
		};
		
		if($('a[href^="#"]').eq(mous_i).attr('href') == '#')mous_i += tt;
		var _href = $('a[href^="#"]').eq(mous_i).attr('href');
		var state = { href:_href };
		history.pushState(state, document.title, state.href);
		scrollTop($(_href).offset().top);
	};	
};
function scrollTop_to_roller(){
	var _hash = location.hash;
	if(_hash == '#' || _hash == ''){
		mous_i =  $('a[href="'+_hash+'"]').index('a[href^="#"]')
	};
	if('onmousewheel' in window) {
		window.onmousewheel = MouseWheel;
	} else if ('onmousewheel' in document) {
		document.onmousewheel = MouseWheel;
	} else if ('addEventListener' in window) {
		window.addEventListener("mousewheel", MouseWheel, false);
		window.addEventListener("DOMMouseScroll", MouseWheel, false);
	};
};
function now_datetime(){
	var dateObj=new Date();
	var _getMonth = dateObj.getMonth();
	var _getDate = dateObj.getDate();
	var _getHours = dateObj.getHours();
	var _getMinutes = dateObj.getMinutes();
	var _getSeconds = dateObj.getSeconds();
	if(_getMonth < 10){
		_getMonth = '0' + _getMonth;
	};
	if(_getDate < 10){
		_getDate = '0' + _getDate;
	};
	if(_getHours < 10){
		_getHours = '0' + _getHours;
	};
	if(_getMinutes < 10){
		_getMinutes = '0' + _getMinutes;
	};
	if(_getSeconds < 10){
		_getSeconds = '0' + _getSeconds;
	};
	return dateObj.getFullYear()+'-'+_getMonth+'-'+_getDate+' '+_getHours+':'+_getMinutes+':'+_getSeconds;
};

function scrollLeft(){
	$left = arguments[0];
	$type = arguments[1];
	if($left == undefined) $left = 0;
	if($type == undefined) $type = 'slow';
	var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
	$body.stop(true,false).animate({
		scrollLeft: $left
	}, $type);
};
function scrollTop(){
	$top = arguments[0];
	$type = arguments[1];
	if($top == undefined) $top = 0;
	if($type == undefined) $type = 'slow';
	var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
	$body.stop(true,false).animate({
		scrollTop: $top
	}, $type);
};
function ReplaceAll(strOrg,strFind,strReplace){   
    return strOrg.replace(new RegExp(strFind,"g"),strReplace);   
}
;(function($){
	$.fn.showalt=function(){
		attr = arguments[0];
		alt_x = arguments[1];	//X 座標
		alt_y = arguments[2];	//Y 座標
		alt_w = arguments[3];	//寬度 (px)
		alt_h = arguments[4];	//長度 (px)
		if(alt_x == 'undefined') alt_x = 0;
		if(alt_y == 'undefined') alt_y = 0;
		if($("#b_alt").length==0){
			var _html='<div id="b_alt"><div id="alt"></div></div>';
			$('body').append(_html);   
		};
		var _b_alt=$('#b_alt');
		var _alt=$('#alt');
		return this.each(function(){
			$this_attr = $(this).attr(attr);
			if($this_attr != undefined && $this_attr != ''){
				$(this).on('mouseover',function(e){
					alt_xy(e);
					_b_alt.stop(true,false).show();
				}).on('mousemove',function(e){
					alt_xy(e);
					_b_alt.html($(this).attr(attr));
				}).on('mouseout',function(e){
					_b_alt.stop(true,false).hide();
				});
				function alt_xy(e){
					_b_alt.css({'top':(e.pageY+alt_y)+'px','left':(e.pageX+alt_x)+'px'});
					if(alt_w != 'undefined') _b_alt.css({'width':alt_w + 'px'});
					if(alt_h != 'undefined') _b_alt.css({'height':alt_h + 'px'});
				};
			};
		});
		$(window).resize(function(e){
			 alt_xy(e);
		});
	};
	function f_full_forum($this,top_p,left_p,size){
		$this.css('position','fixed').css('width','auto').css('height','auto');
		var t_width		= parseInt($this.outerWidth(true));
		var t_height	= parseInt($this.outerHeight(true));
		var w_width		= parseInt($(window).outerWidth(true));
		var w_height	= parseInt($(window).outerHeight(true));
		var w_margin	= w_width - t_width;
		var h_margin	= w_height - t_height;
		var n_width1	= w_width;	//寬基準
		var n_height1	= t_height * (n_width1 / t_width);
		var n_height2	= w_height;	//高基準
		var n_width2	= t_width * (n_height2 / t_height);
		if((n_height1 - w_height) >= 0 && (n_width2 - w_width) >= 0){
			if((n_height1 - w_height) < (n_width2 - w_width)){
				n_height	= n_height1;
				n_width	= n_width1;
			}else{
				n_height	= n_height2;
				n_width	= n_width2;
			};
		}else if((n_height1 - w_height) >= 0){
			n_height	= n_height1;
			n_width	= n_width1;
		}else{
			n_height	= n_height2;
			n_width	= n_width2;
		};
		n_height = n_height * size;
		n_width = n_width * size;
		var top = (w_height - n_height) * top_p;
		var left = (w_width - n_width) * left_p;
		$this.css('position','absolute').css('width',n_width+'px').css('height',n_height+'px').css('top',top+'px').css('left',left+'px');
	};
	$.fn.full_forum=function(){
		var $this = $(this);
		var top_p	= arguments[0];		//對齊top方式(%)
		var left_p	= arguments[1];		//對齊left方式(%)
		var size	= arguments[2];		//圖片放大比率(%)
		if(top_p == undefined) top_p = 50;	//top位置百分比
		if(left_p == undefined) left_p = 50;	//left位置百分比
		if(top_p > 0) top_p = (top_p / 100);
			else top_p = 0;
		if(left_p > 0) left_p = (left_p / 100);
			else left_p = 0;
		if(size > 0) size = (size / 100);
			else size = 1;
		$(window).resize(function(e){
			 $this.each(function(){
				f_full_forum($(this),top_p,left_p,size);
			});
		});
		return this.each(function(){
			var t_width		= parseInt($this.outerWidth(true));
			var t_height	= parseInt($this.outerHeight(true));
			var w_width		= parseInt($(window).outerWidth(true));
			var w_height	= parseInt($(window).outerHeight(true));
			var w_margin	= w_width - t_width;
			var h_margin	= w_height - t_height;
			var n_width1	= w_width;	//寬基準
			var n_height1	= t_height * (n_width1 / t_width);
			var n_height2	= w_height;	//高基準
			var n_width2	= t_width * (n_height2 / t_height);
			if((n_height1 - w_height) >= 0 && (n_width2 - w_width) >= 0){
				if((n_height1 - w_height) < (n_width2 - w_width)){
					n_height	= n_height1;
					n_width	= n_width1;
				}else{
					n_height	= n_height2;
					n_width	= n_width2;
				};
			}else if((n_height1 - w_height) >= 0){
				n_height	= n_height1;
				n_width	= n_width1;
			}else{
				n_height	= n_height2;
				n_width	= n_width2;
			};
			n_height = n_height * size;
			n_width = n_width * size;
			var top = (w_height - n_height) * top_p;
			var left = (w_width - n_width) * left_p;
			$this.css('position','absolute').css('width',n_width+'px').css('height',n_height+'px').css('top',top+'px').css('left',left+'px');
		});
	};
	$.fn.to_top_menu=function(dom_y){
		if(dom_y == '') dom_y=0;
		return this.each(function(){
			var $this = $(this);
			if($this!=undefined){
				var $window = $(window);
				var $document = $(document);
				$dom_top = $this.offset().top;
				if($dom_left = 'undefined')$dom_left = 0;
				$document.scroll(function(){
					move_top_menu();
				});
				$window.resize(function(){
					move_top_menu();
				});
				function move_top_menu(){
					if( $document.scrollTop() >= $dom_top)
						$this.addClass('top_menu');
					else
						$this.removeClass('top_menu');
				};
			};
		});
	};
})(jQuery);
function set_date($y,$m,$d,year_start){
    var now = new Date();
    for(var i = now.getFullYear(); i >= year_start; i--){
        $($y).
        append($("<option></option>").
        attr("value",i).
        text(i));
    }
    for(var i = 1; i <= 12; i++){
        $($m).
        append($("<option></option>").
        attr("value",i).
        text(i));
    }
    $($y).change(onChang_date);
    $($m).change(onChang_date);
    function onChang_date(){
        if($($y).val() != -1 && $($m).val() != -1){
            var date_temp = new Date($($y).val(), $($m).val(), 0);
            $($d).children('option').each(function(){
                if($(this).val() != -1 && $(this).val() > date_temp.getDate()) $(this).remove();
            });
            for(var i = 1; i <= date_temp.getDate(); i++){
                if(! $($d).children("option[value='" + i + "']").length){
                    $($d).
                    append($("<option></option>").
                    attr("value",i).
                    text(i));
                }
            }
        } else {
            $($d).children('option:selected').removeAttr("selected");
        }      
    }
}
var topmenu_ctime = '';
function topmenu($topmenu,_s){
	var $content_li =$('.content li',$topmenu);
	var _max_num = $content_li.length;
	var _html = '';
	for(i=0;i<_max_num;i++){
		_html +='<li></li>';
	};
	$('.menu',$topmenu).html(_html);
	var $menu_li = $('.menu li',$topmenu);
	$menu_li.click(function(){
		var eq = $(this).index();
		rum_menu_list(eq,_max_num,_s);
	});
	rum_menu_list(0,_max_num,_s);
};
function rum_menu_list(_num,_max_num,_s,$topmenu){
	on_click_munu_li(_num,$topmenu);
	_num++;
	if(_num == _max_num)_num = 0;
	if(topmenu_ctime != "undefined")clearTimeout(topmenu_ctime);
	topmenu_ctime = setTimeout('rum_menu_list('+_num+','+_max_num+','+_s+','+$topmenu+')',_s);
};
function on_click_munu_li(eq,$topmenu){
	var $menu_li = $('.menu li',$topmenu);
	var $content_li = $('.content li',$topmenu);
	$menu_li.eq(eq).siblings().removeClass('on');
	$menu_li.eq(eq).addClass('on');
	$content_li.eq(eq).siblings().stop(false,true).fadeOut();
	$content_li.eq(eq).stop(false,true).fadeIn();
};
function audio_status($id){
	var player = document.getElementById($id),
	map =  ['error','src','currentSrc','networkState','readyState','preload','buffered','played','seekable','seeking','currentTime','startTime','duration','paused','defaultPlaybackRate','playbackRate','ended','autoplay','loop','controls','volume','muted'];
	var arr_str = Array();
	for(var i=0, j=map.length; i<j; i++) {  
		arr_str[map[i]] = player[map[i]];  
	};
	return arr_str;
};
function isMove(v){
	var arr_move_type = Array('video/flv','video/x-flv','video/mp4','video/quicktime','video/mpeg','video/avi','video/msvideo','video/x-msvideo','video/x-ms-asf','video/x-ms-wmv')
	if(arr_move_type.lastIndexOf(v) >= 0)return true;
	return;
};
function words_deal(){ 
	var curLength=$("#his_txt_textarea").val().length; 
	if(curLength>40){ 
		var num=$("#his_txt_textarea").val().substr(0,40); 
		$("#his_txt_textarea").val(num); 
		alert('超過字數上限'); 
	}else{ 
		$("#textCount").text(40-$("#his_txt_textarea").val().length); 
	};
};
/*
$(document).ajaxStart(function(){
	_ajaxStart=true;
	top_msg('資料處理中...',true);
}).ajaxStop(function(){
	_ajaxStart=false;
	top_msg('',false);
});
*/
/*
$(document).on('submit','form',function(e){
	$('input[type="submit"]','form').prop('disabled',true);
});
*/
$(document).ready(function(e) {
     $('.all_checkbox').each(function(index, element) {
		all_checkbox($(this),$(this).attr('all_checkbox'));
     });
});
function run_fixed_top($vn, $ft){
	window_sTop = 0;
	if($vn.length > 0){
		var top = $vn.offset().top;
		var height = $vn.height();
		$(window).resize(function(){
			// $vn.css({'padding-top':'unset'});
			top = $vn.offset().top;
			fixed_top($vn, $ft, top);
		}).scroll(function(){
			// $vn.css({'padding-top':'unset'});
			top = $vn.offset().top;
			fixed_top($vn, $ft, top);
		});
	};
};
function formatFloat(num, pos){
	var size = Math.pow(10, pos);
	return Math.round(num * size) / size;
};
function fixed_top($vn, $ft, top){
	var vn_bottom = 10;
	var $w = $(window);
	var sTop = $w.scrollTop();		//捲軸高度
	var w_height = $w.height();         //瀏覽器高度
	var f_height = $ft.height();         //footer高度
	var height = $vn.height();          //物建高度
	var up_t = parseInt($vn.css('padding-top'));

	if(sTop > window_sTop){	//下
		var diff = (sTop + w_height) - (top + height + up_t + vn_bottom);
		if(diff > 0){
			up_t += diff;
		};
	}else if(sTop < window_sTop){		//上
		if(up_t > (sTop - top)){
			up_t = sTop - top;
		};
	};
	up_t = Math.min(up_t, $('body').height() - f_height - top - height - vn_bottom - 10);		//最底
	if(height > w_height){
		$vn.css('padding-top', up_t+'px');
	}else{
		$vn.css('padding-top', up_t+'px');
	};
	window_sTop = sTop;
};

//複製區塊文字
function CopyTextToClipboard(id){
	var TextRange = document.createRange();
	TextRange.selectNode(document.getElementById(id));
	sel = window.getSelection();
	sel.removeAllRanges();
	sel.addRange(TextRange);
	document.execCommand('copy');
	sel.removeRange(TextRange);
};
function auto_address() {
		$('input.auto_address[data-county]').each(function () {
			var county = $(this).data('county');
			var city = $(this).data('city');
			var $this = $(this);
			var $county = $('#'+county);
			var $city = $('#'+city);
			$county.change(function(){
				var str = '';
				if($county.val() != ''){
					str += $('option:selected', $county).html();
				}
				if($city.val() != ''){
					str += $('option:selected', $city).html();
				}
				$this.val(str);
			});
			$city.change(function(){
				var str = '';
				if($county.val() != ''){
					str += $('option:selected', $county).html();
				}
				if($city.val() != ''){
					str += $('option:selected', $city).html();
				}
				$this.val(str);
			});
		});
};
function address_select(input, county, city){
	var $input = $('#'+input);
	var $county = $('#'+county);
	var $city = $('#'+city);
	$county.change(function(){
		var str = '';
		if($county.val() != ''){
			str += $('option:selected', $county).html();
		}
		if($city.val() != ''){
			str += $('option:selected', $city).html();
		}
		$input.val(str);
	});
	$city.change(function(){
		var str = '';
		if($county.val() != ''){
			str += $('option:selected', $county).html();
		}
		if($city.val() != ''){
			str += $('option:selected', $city).html();
		}
		$input.val(str);
	});
};