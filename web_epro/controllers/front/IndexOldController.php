<?php
namespace web_epro;
use \EproController;
use \FrontController;
use \Db;
use \File;
class IndexController extends EproController
{
    public $page = 'index';


//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('裝修達人│要裝潢 欠修理 找裝修達人就對了');
		$this->page_header_toolbar_title = $this->l('首頁');
    $this->meta_description          = '全方位居家裝修服務~設計裝潢、維修保養免費到府估價|價格透明|責任施工|後續保固|24hr|24小時|專業貼心，是專業可靠的裝修專家。提供裝潢設計|衛浴|廚具|水電|冷氣空調|電器|熱水器|油漆粉刷|壁紙|管線|通管|燈飾照明|木作|泥作|鐵作|地板|石材及美化|玻璃|磁磚|防水抓漏|門窗門鎖|智能鎖|安控監視|消防保全|園藝植栽|環境清潔|沙發.床墊.窗簾.地毯除螨清洗|消毒滅菌|除蟲除味|窗簾地毯|淨水設備|升降設備|機電|弱電|節能|綠建材|智慧居家|房屋健檢|驗屋檢測|無障礙空間等裝修服務。生活居家裝修企業社';
    $this->meta_keywords             = '裝修 裝潢 修繕 水電 清潔 智能鎖 健康宅檢測 電器修繕';
		$this->className                 = 'IndexController';
		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
		parent::__construct();
	}

	public function initProcess()
	{

        $sql ="SELECT * FROM ilifehou_epro.`epro_cms_main` WHERE active='1'";
        $epro_cms_main = Db::rowSQL($sql);


        $sql = "SELECT * FROM ilifehou_epro.`epro_cms` WHERE id_epro_cms_main=1 AND active=1 ORDER  BY position ASC ,id_epro_cms ASC Limit 4";
        $talent_data = Db::rowSQL($sql);
        //        處理圖片
        foreach($talent_data as $i => $v){
            $sql = "SELECT * FROM ilifehou_epro.`epro_cms_file` WHERE file_type=0 AND id_epro_cms=".$v['id_epro_cms'].' ORDER BY id_epro_cms_file ASC LIMIT 1';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $talent_data[$i]['img'] = $row['url'];
        }

        $sql = "SELECT * FROM ilifehou_epro.`epro_cms` WHERE id_epro_cms_main=2 AND active=1 ORDER  BY position ASC ,id_epro_cms ASC Limit 4";
        $member_data = Db::rowSQL($sql);
        //        處理圖片
        foreach($member_data as $i => $v){
            $sql = "SELECT * FROM ilifehou_epro.`epro_cms_file` WHERE file_type=0 AND id_epro_cms=".$v['id_epro_cms'].' ORDER BY id_epro_cms_file ASC LIMIT 1';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $member_data[$i]['img'] = $row['url'];
        }

        $sql = "SELECT * FROM ilifehou_epro.`epro_cms` WHERE id_epro_cms_main=3 AND active=1 ORDER  BY position ASC ,id_epro_cms ASC Limit 4";
        $new_data = Db::rowSQL($sql);
        //        處理圖片
        foreach($new_data as $i => $v){
            $sql = "SELECT * FROM ilifehou_epro.`epro_cms_file` WHERE file_type=0 AND id_epro_cms=".$v['id_epro_cms'].' ORDER BY id_epro_cms_file ASC LIMIT 1';
            $sql_url =  Db::rowSQL($sql, true);
            $row = File::get($sql_url['id_file']);
            $new_data[$i]['img'] = $row['url'];
        }

        $index_ratio      = '1932:978';
        $arr_index_banner =[
            [
                'img'=>'/themes/Epro/img/index/epro_banner_1_1210.jpg',
                'href'=>'',
            ],
            [
                'img'=>'/themes/Epro/img/index/epro_banner_1.jpg',
                'href'=>'',
            ],
            [
                'img'=>'/themes/Epro/img/index/epro_banner_3.jpg',
                'href'=>'',
            ],
        ];

		$this->context->smarty->assign([
            'epro_cms_main' => $epro_cms_main,
            'member_data'   => $member_data,
            'talent_data'   => $talent_data,
            'new_data'      => $new_data,
            'group_link'    => 1,
            'index_ratio'      => $index_ratio,
            'arr_index_banner' =>$arr_index_banner,
		]);
		parent::initProcess();
	}

	public function setMedia()
	{
		parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS('/js/slick.min.js');
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');
        $this->addCSS('/css/metro-all.css');
		$this->addCSS('/themes/Epro/css/rwd-index.css');
        $this->addJS('/js/metro.min.js');
        $this->addJS(THEME_URL . '/js/group_link.js');
        $this->addJS(THEME_URL . '/js/search_bar.js');
        $this->addJS(THEME_URL . '/js/hb.js');
        $this->addCSS(THEME_URL . '/css/search_bar.css');
        $this->addJS('/js/lazysizes.min.js');
        $this->addCSS(THEME_URL . '/css/page_all.css');
	}
}
