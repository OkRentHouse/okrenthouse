<?php

namespace web_epro;

use \FrontController;
use \EproController;
use \Db;
use\LineApi;
use \Tools;
use \Member;

//use \File;

class Signup3Controller extends EproController
{
    //public $page = 'Signup2';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;
//      public $fb_app_id = '1319773841697579';
//    public $fb_api_version = 'v9.0';
    public $table='member';

    public function __construct()
    {
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'Signup3Controller';
        $this->no_FormTable              = true;
//        $this->display_footer            = false;
        parent::__construct();
        $this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;

    }



	public function initProcess()
	{
        if(!empty($_GET['code']) && !empty($_GET['state'])){


        }



		$this->context->smarty->assign([
            'fb_app_id'=>$this->fb_app_id,
            'fb_api_version'=>$this->fb_api_version,
            'table'=>$this->table,
		]);

		// parent::initProcess();

	}

    public function postProcess()
    {
        if (Tools::isSubmit('submitAdd' . $this->table)) {
//            $user    = Tools::getValue('user');
            $tel_code = (substr(Tools::getValue('user'),0,1)=='0')?substr(Tools::getValue('user'),1):Tools::getValue('user');
            $user = Tools::getValue('international_code').$tel_code;//包含國際碼的手機號碼
            $password = Tools::getValue('password');
            $name = Tools::getValue('name');
            $nickname = Tools::getValue('nickname');
            $gender = Tools::getValue('gender');

//            print_r($_POST);

           Member::getContext()->validateRules($_POST);//丟去驗證

            if (count($this->_errors))
                return;


            $tt = false;
            while (!$tt) {        //產生不重複會員條碼
                $barcode = random16number(16);
                $sql     = sprintf('SELECT `id_member` FROM `member` WHERE `barcode` = %s LIMIT 0, 1',
                    GetSQL($barcode, 'text'));
                $row     = Db::rowSQL($sql, true);
                if (empty($row['id_member'])) {
                    $tt = true;
                }
            }

            $_POST['barcode'] = $barcode;
            $this->fields['form'][0]['input']['barcode'] = [
                'name' => 'barcode',
                'type' => 'text',
            ];

            $sql = 'SELECT count(user) as num,user FROM `'.$this->table.'` WHERE  `user`= '.GetSQL($user, 'text').'AND active=1 ';
            //有user 代表有註冊過
            $row = Db::rowSQL($sql,true);

            if($row['num']<'1'){
                //測試帳號暫時先不啟用 等api串接完成方可註冊完畢後啟用
                $sql = 'INSERT INTO `'.$this->table.'` (`user`, `password`,`name`,`nickname`,`gender`,`barcode`,`active`,`id_member_additional`)
                    VALUES('.GetSQL($user,"text").', '.GetSQL($password,"text").',
                    '.GetSQL($name,"text").','.GetSQL($nickname,"text").',
                    '.GetSQL($gender,"int").','.GetSQL($barcode,"int").',
                    1,'.GetSQL('["1"]',"text").')';
                Db::rowSQL($sql);
                $_GET["success"] = "ok";
//                header('Location: https://www.okrent.house/login');//轉址
//                exit;
            }else{
                $this->_errors[] = $this->l('此手機已註冊過!');
            }
        }
    }




//外部登入R
//    public function displayAjax()
//    {
//        $action = Tools::getValue('action');
//
//        switch($action){
//            case 'line_registered':
//                $line_get_authorize = [
//                    'response_type'=>'code',
//                    'client_id'=>'1655314652',
//                    'redirect_uri'=>'https://www.okrent.tw/Signup',
//                ];
//                $line_get_authorize['state'] = 'test';//自己產生 之後要處理
//                $line_get_authorize['nonce']= 'signup';//加在後面
//                echo json_encode(LineApi::Authorization($line_get_authorize));
//                break;
//            default :
//                echo json_encode("no");
//                break;
//        }
//        exit;
//    }




	public function setMedia(){
        $this->removeJS(THEME_URL .'/css/page_left.css');
		parent::setMedia();
        $this->addCSS(THEME_URL . '/css/signup.css');
	}
}
