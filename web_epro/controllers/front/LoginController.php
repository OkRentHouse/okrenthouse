<?php

namespace web_epro;

use \FrontController;
use \EproController;
use \Db;
use \Tools;
use \Member;

class LoginController extends EproController{
    public $page = 'Login';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;
    public function __construct(){

        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'LoginController';
        $this->no_FormTable              = true;
        $this->display_footer            = false;
//        $this->display_footer            = false;
        parent::__construct();
        $this->display_header            = false;//首頁不用共用表頭
        $this->fields['form'] = [
            'member' => [
                'input' => [
                    'user'     => [
                        'name'        => 'user',
                        'type'        => 'text',
                        'label'       => $this->l('帳號'),
                        'placeholder' => $this->l('帳號'),
                        'maxlength'   => '100',
                        'required'    => true,
                    ],
                    'password' => [
                        'type'        => 'new-password',
                        'label'       => $this->l('密碼'),
                        'placeholder' => $this->l('密碼'),
                        'required'    => true,
                        'minlength'   => '6',
                        'maxlength'   => '20',
                        'name'        => 'password',
                    ],
                ],
            ],
        ];
    }


    public function postProcess(){

        $tel_code = (substr(Tools::getValue('user'),0,1)=='0')?substr(Tools::getValue('user'),1):Tools::getValue('user');
        $user = Tools::getValue('international_code').$tel_code;//包含國際碼的手機號碼
        $password = Tools::getValue('password');
        $is_login    = Tools::isSubmit('is_login');
        $auto_loging = Tools::getValue('auto_login');

        if (!empty($is_login)) {
            $this->validateRules();
            if (count($this->_errors) == 0) {
                if (!Member::login($user, $password, $auto_loging)) {
                    $this->_errors[] = $this->l('帳號密碼錯誤!');
                } else {
                    $this->login();
                    exit;
                }
            }
        }
    }

    //已登入
    public function login()
    {
        $page = Tools::getValue('page');
        if (empty($page)) {
            Tools::redirectLink('//' . WEB_DNS);
        } else {
            Tools::redirectLink('//' . WEB_DNS . base64_decode(Tools::getValue('page')));
        }
        exit;
    }

	public function initProcess(){

		$this->context->smarty->assign([

		]);

		// parent::initProcess();
	}



	public function setMedia(){

		parent::setMedia();
	}
}
