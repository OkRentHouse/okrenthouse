<?php

namespace web_epro;

use \FrontController;
use \EproController;
use \Db;
use\LineApi;
use \Tools;
use \Member;

use \File;
use \Epro;

class Orderrequest2Controller extends EproController
{
    //public $page = 'Orderrequest2';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
    public $definition;
    public $_errors_name;
//      public $fb_app_id = '1319773841697579';
//    public $fb_api_version = 'v9.0';
    public $table='member';

    public function __construct()
    {
/**
        $this->page_header_toolbar_title = $this->l('首頁');
        $this->className                 = 'Orderrequest2Controller';
        $this->no_FormTable              = true;
//        $this->display_footer            = false;
        parent::__construct();
        $this->display_header            = false;//首頁不用共用表頭
        $this->display_footer            = false;
**/

		$this->meta_title                = $this->l('裝修達人│要裝潢 欠修理 找裝修達人就對了');
		$this->page_header_toolbar_title = $this->l('首頁');
    $this->meta_description          = '全方位居家裝修服務~設計裝潢、維修保養免費到府估價|價格透明|責任施工|後續保固|24hr|24小時|專業貼心，是專業可靠的裝修專家。提供裝潢設計|衛浴|廚具|水電|冷氣空調|電器|熱水器|油漆粉刷|壁紙|管線|通管|燈飾照明|木作|泥作|鐵作|地板|石材及美化|玻璃|磁磚|防水抓漏|門窗門鎖|智能鎖|安控監視|消防保全|園藝植栽|環境清潔|沙發.床墊.窗簾.地毯除螨清洗|消毒滅菌|除蟲除味|窗簾地毯|淨水設備|升降設備|機電|弱電|節能|綠建材|智慧居家|房屋健檢|驗屋檢測|無障礙空間等裝修服務。生活居家裝修企業社';
    $this->meta_keywords             = '裝修 裝潢 修繕 水電 清潔 智能鎖 健康宅檢測 電器修繕';
		$this->className                 = 'OrderController';
		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
		parent::__construct();
    }



	public function initProcess()
	{

        //$obj = new OrderController();
        //$page=$obj->page;
        $page_title = '叫修派工';
        $breadcrumbs_html = "首頁 &gt; 裝修服務 &gt; 服務選單 &gt; ".$page_title;

        $Case_ratio      = '1932:978';
        $arr_Case_banner =[
            [
                'img'=>'/themes/Epro/img/Case/epro_banner_1.jpg',
                'href'=>'',
            ],
            [
                'img'=>'/themes/Epro/img/Case/epro_banner_2.jpg',
                'href'=>'',
            ],
            [
                'img'=>'/themes/Epro/img/Case/epro_banner_3.jpg',
                'href'=>'',
            ],
        ];

		$this->context->smarty->assign([
            'group_link'    => 1,
            'Case_ratio'      => $Case_ratio,
            'arr_Case_banner'=>$arr_Case_banner,
            'group_link_js'=>$group_link_js,
            'breadcrumbs_html' => $breadcrumbs_html,
            'page_title' => $page_title,
            'page' => $page,
            'table'=>$this->table,
		]);

		parent::initProcess();


        /**
        if(!empty($_GET['code']) && !empty($_GET['state'])){
        }
		$this->context->smarty->assign([
            'fb_app_id'=>$this->fb_app_id,
            'fb_api_version'=>$this->fb_api_version,
            'table'=>$this->table,
		]);
        **/

		// parent::initProcess();
	}

    public function postProcess()
    {
        if (Tools::isSubmit('submitAdd' . $this->table)) {
//            $user    = Tools::getValue('user');
            $tel_code = (substr(Tools::getValue('user'),0,1)=='0')?substr(Tools::getValue('user'),1):Tools::getValue('user');
            $user = Tools::getValue('international_code').$tel_code;//包含國際碼的手機號碼
            $password = Tools::getValue('password');
            $name = Tools::getValue('name');
            $nickname = Tools::getValue('nickname');
            $gender = Tools::getValue('gender');

//            print_r($_POST);
//echo"ok";exit();
           //Member::getContext()->validateRules($_POST);//丟去驗證
           /**
            if (count($this->_errors))
                return;
**/
                
            $tt = false;
            while (!$tt) {        //產生不重複會員條碼
                $barcode = random16number(16);
                $sql     = sprintf('SELECT `id_member` FROM `member` WHERE `barcode` = %s LIMIT 0, 1',
                    GetSQL($barcode, 'text'));
                $row     = Db::rowSQL($sql, true);
                if (empty($row['id_member'])) {
                    $tt = true;
                }
            }
            
            $_POST['barcode'] = $barcode;
            $this->fields['form'][0]['input']['barcode'] = [
                'name' => 'barcode',
                'type' => 'text',
            ];
            
            $sql = 'SELECT count(user) as num,user FROM `'.$this->table.'` WHERE  `user`= '.GetSQL($user, 'text').'AND active=1 ';
            //有user 代表有註冊過
            $row = Db::rowSQL($sql,true);
//echo"ok";exit();
//echo $row['num'];exit();


            //if($row['num']<'1'){
            if($row['num']<'9'){
                //測試帳號暫時先不啟用 等api串接完成方可註冊完畢後啟用
                
                $sql = 'INSERT INTO `'.$this->table.'` (`user`, `password`,`name`,`nickname`,`gender`,`barcode`,`active`,`id_member_additional`)
                    VALUES('.GetSQL($user,"text").', '.GetSQL($password,"text").',
                    '.GetSQL($name,"text").','.GetSQL($nickname,"text").',
                    '.GetSQL($gender,"int").','.GetSQL($barcode,"int").',
                    1,'.GetSQL('["1"]',"text").')';
                Db::rowSQL($sql);

                //$this->_errors[] = $this->l('新增完成!'.$user);
                //$this->_errors[] = $this->l($sql);
                $this->_errors[] = "";
                $this->_errors[] = $this->l($user.'新增完成!');
                //echo $sql;exit();
/**
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO MyGuests (firstname, lastname, email)
VALUES ('John', 'Doe', 'john@example.com')";

if ($conn->query($sql) === TRUE) {
  echo "New record created successfully";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
**/

                $_GET["success"] = "ok";
//                header('Location: https://www.okrent.house/login');//轉址
//                exit;
            }else{
                $this->_errors[] = $this->l('此手機已註冊過!');
            }
        }
    }




//外部登入R
//    public function displayAjax()
//    {
//        $action = Tools::getValue('action');
//
//        switch($action){
//            case 'line_registered':
//                $line_get_authorize = [
//                    'response_type'=>'code',
//                    'client_id'=>'1655314652',
//                    'redirect_uri'=>'https://www.okrent.tw/Signup',
//                ];
//                $line_get_authorize['state'] = 'test';//自己產生 之後要處理
//                $line_get_authorize['nonce']= 'signup';//加在後面
//                echo json_encode(LineApi::Authorization($line_get_authorize));
//                break;
//            default :
//                echo json_encode("no");
//                break;
//        }
//        exit;
//    }




	public function setMedia(){
		parent::setMedia();
        $this->addCSS('/css/slick-theme.css');
        $this->addCSS('/css/slick.css');
        $this->addCSS('/css/metro-all.css');
        $this->addCSS(THEME_URL . '/css/orderrequest.css');
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addCSS(THEME_URL . '/css/rwd-Case.css');
        $this->addCSS(THEME_URL . '/css/search_bar.css');
        $this->addCSS(THEME_URL . '/css/page_left.css');
        $this->addCSS(THEME_URL . '/css/page_all.css');
        $this->addJS(THEME_URL .'/js/orderrequest.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');
        $this->addJS('/js/slick.min.js');
        $this->addJS('/js/metro.min.js');
        $this->addJS(THEME_URL .'/js/hb.js');
        $this->addJS(THEME_URL .'/js/search_bar.js');
        $this->addJS('/js/hc-sticky.js');
        $this->addJS(THEME_URL .'/js/_var_items.js');
	}
}
