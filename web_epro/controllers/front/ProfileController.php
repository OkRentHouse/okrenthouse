<?php
namespace web_epro;
use \EproController;
use \FrontController;
use \Db;
use \File;
use \Epro;
use  \County;
use \Tools;
class ProfileController extends EproController
{
    public $page = 'Profile';
//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('裝修達人│要裝潢 欠修理 找裝修達人就對了');
		$this->page_header_toolbar_title = $this->l('首頁');
        $this->meta_description          = '全方位居家裝修服務~設計裝潢、維修保養免費到府估價|價格透明|責任施工|後續保固|24hr|24小時|專業貼心，是專業可靠的裝修專家。提供裝潢設計|衛浴|廚具|水電|冷氣空調|電器|熱水器|油漆粉刷|壁紙|管線|通管|燈飾照明|木作|泥作|鐵作|地板|石材及美化|玻璃|磁磚|防水抓漏|門窗門鎖|智能鎖|安控監視|消防保全|園藝植栽|環境清潔|沙發.床墊.窗簾.地毯除螨清洗|消毒滅菌|除蟲除味|窗簾地毯|淨水設備|升降設備|機電|弱電|節能|綠建材|智慧居家|房屋健檢|驗屋檢測|無障礙空間等裝修服務。生活居家裝修企業社';
        $this->meta_keywords             = '裝修 裝潢 修繕 水電 清潔 智能鎖 健康宅檢測 電器修繕';
		$this->className                 = 'ProfileController';
		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
		parent::__construct();
	}

	public function initProcess()
	{
        $page_title = '達人履歷';
        $breadcrumbs_html = "首頁 &gt; 裝修服務 &gt; 服務選單 &gt; ".$page_title;

//        print_r(County::getContext()->database_city(7));

		$this->context->smarty->assign([
            'data_base'=>Epro::get_data($_SESSION["id_member"]),
            'group_link'    => 1,
            'breadcrumbs_html' => $breadcrumbs_html,
            'page_title' => $page_title,
            'page' => $this->page,
            'county'=>County::getContext()->database_county(),
            'city'=>County::getContext()->database_city(),
            'preset_county'=>County::getContext()->database_county(7),//桃園的預設
            'preset_city'=>County::getContext()->database_city(7),//桃園的預設值
            'epro_windows'=>Epro::get_epro_windows(),
            'epro_windows_item'=>Epro::get_epro_windows_item(),//給預設值
		]);
		parent::initProcess();
	}

    public function postProcess(){
        if($_POST['sumbit']==1){//代表被執行
            $validation = Epro::validation($_POST,$_FILES);
            if(!empty($validation['error'])){
                $this->_errors = $validation['error'];//錯誤訊息
            }else{
                Epro::post_put_data($_SESSION['id_member'],$_POST,$_FILES);
                //處理完之後要刪除
                unset($_POST);
                unset($_FILES);
            }
        }
    }

    public function displayAjaxCity()
    {
        $action = Tools::getValue('action');    //action = 'City';
        $id_county = Tools::getValue('id_county');    //選定國家
        switch($action){
            case 'City':
                if($id_county==null){
                    echo json_encode(array("error"=>"","return"=>"請重新選擇縣市"));
                    break;
                }
                echo json_encode(array("error"=>"","return"=>County::getContext()->database_city($id_county)));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;//保險ajax能夠完整結束
    }

    public function displayAjax()
    {
        $action = Tools::getValue('action');
        switch($action){
            case 'Epro_windows':
                echo json_encode(array("error"=>"","return"=>
                    array('epro_windows'=>Epro::getContext()->get_epro_windows(),
                        'epro_windows_item'=>Epro::getContext()->get_epro_windows_item(1))
                ));
                break;
            case 'Epro_windows_item':
                $id_epro_windows = Tools::getValue('id_epro_windows');
                if(!empty($id_epro_windows)){
                    echo json_encode(array("error"=>"","return"=>Epro::get_epro_windows_item($id_epro_windows)));
                }else{
                    echo json_encode(array("error"=>"主項目未選擇","return"=>""));
                }
                break;
            case 'server_address':
                echo json_encode(array("error"=>"","return"=>
                    array('county'=>County::getContext()->database_county(7),
                          'city'=>County::getContext()->database_city(7))
                ));
                break;
            default :
                echo json_encode(array("error"=>"Illegal path sign","return"=>""));
                break;
        }
        exit;//保險ajax能夠完整結束
    }

	public function setMedia()
	{
		parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');


        $this->addCSS('/css/metro-all.css');

        $this->addCSS('/themes/Epro/css/rwd-Service.css');
        $this->addCSS(THEME_URL . '/css/search_bar.css');
        $this->addCSS(THEME_URL . '/css/page_banner.css');
        $this->addCSS(THEME_URL . '/css/page_left.css');
        $this->addJS('/js/metro.min.js');
        $this->addJS(THEME_URL . '/js/hb.js');
        $this->addJS(THEME_URL . '/js/search_bar.js');
        $this->addJS('/js/hc-sticky.js');
        $this->addCSS(THEME_URL . '/css/page_all.css');
//        $this->addJS(THEME_URL . '/js/_var_items.js');
	}
}
