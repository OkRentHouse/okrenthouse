<?php
namespace web_epro;
use \EproController;
use \FrontController;
use \Db;
use \File;
class MasterviewController extends EproController
{
    public $page = 'Masterview';



//	public $tpl_folder;    //樣版資料夾
    public $tpl_folder;
	public $definition;
	public $_errors_name;

	public function __construct()
	{
		$this->meta_title                = $this->l('裝修達人│要裝潢 欠修理 找裝修達人就對了');
		$this->page_header_toolbar_title = $this->l('首頁');
    $this->meta_description          = '全方位居家裝修服務~設計裝潢、維修保養免費到府估價|價格透明|責任施工|後續保固|24hr|24小時|專業貼心，是專業可靠的裝修專家。提供裝潢設計|衛浴|廚具|水電|冷氣空調|電器|熱水器|油漆粉刷|壁紙|管線|通管|燈飾照明|木作|泥作|鐵作|地板|石材及美化|玻璃|磁磚|防水抓漏|門窗門鎖|智能鎖|安控監視|消防保全|園藝植栽|環境清潔|沙發.床墊.窗簾.地毯除螨清洗|消毒滅菌|除蟲除味|窗簾地毯|淨水設備|升降設備|機電|弱電|節能|綠建材|智慧居家|房屋健檢|驗屋檢測|無障礙空間等裝修服務。生活居家裝修企業社';
    $this->meta_keywords             = '裝修 裝潢 修繕 水電 清潔 智能鎖 健康宅檢測 電器修繕';
		$this->className                 = 'MasterviewController';
		$this->no_FormTable              = true;
		$this->display_header            = false;//首頁不用共用表頭
		parent::__construct();
	}

	public function initProcess()
	{

        $obj = new MasterviewController();
        $page=$obj->page;
        $page_title = '找達人';
        $breadcrumbs_html = "首頁 &gt; 裝修服務 &gt; 服務選單 &gt; ".$page_title;


        $Service_ratio      = '1932:978';
        $arr_Service_banner =[
            [
                'img'=>'/themes/Epro/img/Service/epro_banner_1.jpg',
                'href'=>'',
            ],
            [
                'img'=>'/themes/Epro/img/Service/epro_banner_2.jpg',
                'href'=>'',
            ],
            [
                'img'=>'/themes/Epro/img/Service/epro_banner_3.jpg',
                'href'=>'',
            ],
        ];



		$this->context->smarty->assign([
            'group_link'    => 1,
            'Service_ratio'      => $Service_ratio,
            'arr_Service_banner'=>$arr_Service_banner,
            'group_link_js'=>$group_link_js,
            'breadcrumbs_html' => $breadcrumbs_html,
            'page_title' => $page_title,
            'page' => $page,
		]);
		parent::initProcess();
	}

	public function setMedia()
	{
		parent::setMedia();
        $this->addCSS('/' . MEDIA_URL . '/uikit-3.2.0/css/uikit.min.css');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit.min.js');
        $this->addJS('/' . MEDIA_URL . '/uikit-3.2.0/js/uikit-icons.min.js');


        $this->addCSS('/css/metro-all.css');

    		$this->addCSS('/themes/Epro/css/rwd-Service.css');
        $this->addCSS(THEME_URL . '/css/search_bar.css');
        $this->addCSS(THEME_URL . '/css/page_banner.css');
        $this->addCSS(THEME_URL . '/css/page_left.css');
        $this->addJS('/js/metro.min.js');
        $this->addJS(THEME_URL . '/js/hb.js');
        $this->addJS(THEME_URL . '/js/search_bar.js');
        $this->addJS('/js/hc-sticky.js');
        $this->addCSS(THEME_URL . '/css/page_all.css');
	}
}
