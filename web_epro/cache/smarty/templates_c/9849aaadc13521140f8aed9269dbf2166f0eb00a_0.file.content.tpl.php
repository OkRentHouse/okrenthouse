<?php
/* Smarty version 3.1.28, created on 2021-01-04 20:11:01
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Masterview/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5ff305d52b24e8_04149289',
  'file_dependency' => 
  array (
    '9849aaadc13521140f8aed9269dbf2166f0eb00a' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Masterview/content.tpl',
      1 => 1609761154,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../page_left.tpl' => 1,
  ),
),false)) {
function content_5ff305d52b24e8_04149289 ($_smarty_tpl) {
?>


<div class="bottom_fix_div">
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./search_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<div class="container-fluid">
        <header style="max-width: 960px;">

        </header>


        <div id="tm-bg"></div>


        <div id="tm-wrap-case">
            <div class="tm-main-content">

                <!-- 次頁主內容 -->
                <article>





                </article>

            </div>
            <!-- .tm-main-content -->

                <div class="details">
        			<div class="details__bg details__bg--down" style="opacity: 0; transform: none;">
        				<button class="details__close" style="opacity: 0; transform: translateY(100%);"><svg class="svg-inline--fa fa-times fa-w-11 fa-2x icon--cross tm-fa-close" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" data-fa-i2svg=""><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg><!-- <i class="fas fa-2x fa-times icon--cross tm-fa-close"></i> Font Awesome fontawesome.com --></button>
        				<div class="details__description" style="opacity: 0;"></div>
        			</div>
        			</div></div>


        <div class="details">
			<div class="details__bg details__bg--down" style="opacity: 0; transform: none;">
				<button class="details__close" style="opacity: 0; transform: translateY(100%);"><i class="fas fa-2x fa-times icon--cross tm-fa-close"></i></button>
				<div class="details__description" style="opacity: 0;"></div>
			</div>
			</div></div>
    </div>

    <div class="container-fluid main" id="main">

        <div class="breadcrumbs">
            <p><?php echo $_smarty_tpl->tpl_vars['breadcrumbs_html']->value;
if ($_smarty_tpl->tpl_vars['sidname']->value) {?> &gt; <?php echo $_smarty_tpl->tpl_vars['sidname']->value;
}?></p><br>
        </div>




        <div class="row">
            <div class="col-xs-12 col-sm-2">
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../page_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <div class="col-xs-12 col-sm-10">

              <?php $_smarty_tpl->tpl_vars['class_title'] = new Smarty_Variable(array("NEW","HOT","<svg aria-hidden='true' focusable='false' data-prefix='fal' data-icon='thumbs-up' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' class='svg-inline--fa fa-thumbs-up fa-w-16 fa-7x'><path fill='currentColor' d='M496.656 285.683C506.583 272.809 512 256 512 235.468c-.001-37.674-32.073-72.571-72.727-72.571h-70.15c8.72-17.368 20.695-38.911 20.695-69.817C389.819 34.672 366.518 0 306.91 0c-29.995 0-41.126 37.918-46.829 67.228-3.407 17.511-6.626 34.052-16.525 43.951C219.986 134.75 184 192 162.382 203.625c-2.189.922-4.986 1.648-8.032 2.223C148.577 197.484 138.931 192 128 192H32c-17.673 0-32 14.327-32 32v256c0 17.673 14.327 32 32 32h96c17.673 0 32-14.327 32-32v-8.74c32.495 0 100.687 40.747 177.455 40.726 5.505.003 37.65.03 41.013 0 59.282.014 92.255-35.887 90.335-89.793 15.127-17.727 22.539-43.337 18.225-67.105 12.456-19.526 15.126-47.07 9.628-69.405zM32 480V224h96v256H32zm424.017-203.648C472 288 472 336 450.41 347.017c13.522 22.76 1.352 53.216-15.015 61.996 8.293 52.54-18.961 70.606-57.212 70.974-3.312.03-37.247 0-40.727 0-72.929 0-134.742-40.727-177.455-40.727V235.625c37.708 0 72.305-67.939 106.183-101.818 30.545-30.545 20.363-81.454 40.727-101.817 50.909 0 50.909 35.517 50.909 61.091 0 42.189-30.545 61.09-30.545 101.817h111.999c22.73 0 40.627 20.364 40.727 40.727.099 20.363-8.001 36.375-23.984 40.727zM104 432c0 13.255-10.745 24-24 24s-24-10.745-24-24 10.745-24 24-24 24 10.745 24 24z' class=''></path></svg>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'class_title', 0);?>
                <div>
                  <h3 class="center m030">&nbsp</h3>
                  <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 3) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 3; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                    <div class="desk">
                      <h3 class="center m030"><img src="/themes/Epro/img/index/tool-1.png" alt=""> <?php echo $_smarty_tpl->tpl_vars['class_title']->value[$_smarty_tpl->tpl_vars['i']->value];?>
 <img src="/themes/Epro/img/index/tool-2.png" alt=""></h3>
                        <div class="avi">

                            <div class="card-deck">
                              <?php
$_smarty_tpl->tpl_vars['avi'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['avi']->value = 0;
if ($_smarty_tpl->tpl_vars['avi']->value < 4) {
for ($_foo=true;$_smarty_tpl->tpl_vars['avi']->value < 4; $_smarty_tpl->tpl_vars['avi']->value++) {
?>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_1.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">房屋退租及入住清潔服務</h5>
                                        <p class="card-text">預算<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">地點</p>
                                        <p class="card-text"><i class="fas fa-eye"></i> 9999 人關注</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                                <?php }
}
?>

                            </div>

                        </div>

                    </div>
                    <div class="m00600">
                        <div class="row justify-content-center align-items-center m15">
                            <!-- *** 分頁page-1 *** -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            &lt;</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">&gt;</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <?php }
}
?>




                    <!-- === END CONTENT === -->
                </div>
            </div>
        </div>
    </div>
<?php }
}
