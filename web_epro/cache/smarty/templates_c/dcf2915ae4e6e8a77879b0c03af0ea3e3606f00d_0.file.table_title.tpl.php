<?php
/* Smarty version 3.1.28, created on 2021-03-23 18:01:35
  from "/opt/lampp/htdocs/life-house.com.tw/themes/default/table_title.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6059bc7fcf9588_53343164',
  'file_dependency' => 
  array (
    'dcf2915ae4e6e8a77879b0c03af0ea3e3606f00d' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/default/table_title.tpl',
      1 => 1607678483,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6059bc7fcf9588_53343164 ($_smarty_tpl) {
?>
<style>
@media (max-width: 991px) {
<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
$_from = $_smarty_tpl->tpl_vars['fields']->value['list'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_thead_0_saved_item = isset($_smarty_tpl->tpl_vars['thead']) ? $_smarty_tpl->tpl_vars['thead'] : false;
$__foreach_thead_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['thead'] = new Smarty_Variable();
$__foreach_thead_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_thead_0_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['thead']->value) {
$__foreach_thead_0_saved_local_item = $_smarty_tpl->tpl_vars['thead'];
?>
	.list .table tbody tr td:nth-of-type(<?php echo $_smarty_tpl->tpl_vars['i']->value++;?>
):before {
		content: "<?php echo $_smarty_tpl->tpl_vars['thead']->value['title'];?>
";
	}
<?php
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_0_saved_local_item;
}
}
if ($__foreach_thead_0_saved_item) {
$_smarty_tpl->tpl_vars['thead'] = $__foreach_thead_0_saved_item;
}
if ($__foreach_thead_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_thead_0_saved_key;
}
?>
}
</style><?php }
}
