<?php
/* Smarty version 3.1.28, created on 2020-12-10 01:00:20
  from "/home/ilifehou/life-house.com.tw/themes/Epro/controllers/Service/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd102a4302c45_16698212',
  'file_dependency' => 
  array (
    '3e54f8e0eafc93ca2952b3387b87f06d4a552087' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Epro/controllers/Service/content.tpl',
      1 => 1607533216,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd102a4302c45_16698212 ($_smarty_tpl) {
?>
<div class="bottom_fix_div">
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./search_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<?php if (count($_smarty_tpl->tpl_vars['arr_index_banner']->value)) {?>
<div class="top_banner" uk-slideshow="animation: push;autoplay: true;ratio: <?php echo $_smarty_tpl->tpl_vars['index_ratio']->value;?>
">
    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
        <ul class="uk-slideshow-items">
            <?php
$_from = $_smarty_tpl->tpl_vars['arr_index_banner']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_banner_0_saved_item = isset($_smarty_tpl->tpl_vars['banner']) ? $_smarty_tpl->tpl_vars['banner'] : false;
$__foreach_banner_0_saved_key = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable();
$__foreach_banner_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_banner_0_total) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value => $_smarty_tpl->tpl_vars['banner']->value) {
$__foreach_banner_0_saved_local_item = $_smarty_tpl->tpl_vars['banner'];
?>
            <li>
                <a href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['href'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['banner']->value['img'];?>
"></a>
            </li>
            <?php
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_local_item;
}
}
if ($__foreach_banner_0_saved_item) {
$_smarty_tpl->tpl_vars['banner'] = $__foreach_banner_0_saved_item;
}
if ($__foreach_banner_0_saved_key) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_banner_0_saved_key;
}
?>
        </ul>
        <samp class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></samp>
        <samp class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></samp>
    </div>
    <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
</div>
<div class="top_banner_2">
  <div>
    <table><tr><td colspan="5">
      <div class="fn_logo">
        <img src="/themes/Epro/img/index/fn_logo.png"><br>
        <span>專業可靠 · 安心便利</span>
      </div>
    </td></tr>
      <tr>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-1.png"><br><span>裝修服務</span></a></div></td>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-2.png"><br><span>達人專區</span></a></div></td>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-3.png"><br><span>最新訊息</span></a></div></td>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-4.png"><br><span>客服中心</span></a></div></td>
      <td><div><a href="####"><img src="/themes/Epro/img/index/icon-5.png"><br><span>關於我們</span></a></div></td>
    </tr></table>
  </div>
</div>
<?php }?>


<div class="head_portal tm-main-content grid">
  <div class="container tm-site-header-container">
                      <div class="row">
                          <div class="col-sm-12 col-md-6 col-lg-6 col-md-col-xl-6 mb-md-0 mb-sm-4 mb-4 tm-site-header-col">
                              <div class="tm-site-header">
                                  <h1 class="mb-4 main-title">裝修服務</h1>
                                  <img src="/themes/Epro/img/index/underline.png" class="img-fluid mb-4">
                                  <p class="second-title">體驗便利的裝修，服務專業貼心，<br>是您可靠的裝修專家</p>
                              </div>
                          </div>
                          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                              <div class="content">
                                  <div class="grid">
                                      <div class="grid__item" id="home-link">
                                          <div class="product">
                                              <div class="tm-nav-link">
                                                  <i class="fas fa-screwdriver fa-3x tm-nav-icon"></i>
                                                  <span class="tm-nav-text">PRO 服務<br><small>適合需要<br>專業技術類型的服務</small></span>

                                                  <div class="product__bg"></div>
                                              </div>
                                              <div class="product__description">
                                                  <div class="p-sm-4 p-2">
                                                      <div class="row mb-3">
                                                          <div class="col-12">
                                                              <h2 class="tm-page-title">PRO級服務</h2>
                                                              <div class="line-1"></div>
                                                          </div>
                                                      </div>
                                                      <div class="row tm-reverse-sm">
                                                          <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                                              <h4 class="m150">適合只需要專業技術類型的服務</h4>
                                                              <h5>專業職人的最佳謀合平台</h5>
                                                              <p class="mb-4 m150">◎ 開發客戶 . 免成交手續費 . 進案通知自動配對 . 免費註冊 . 案源多 . 謀合快 . 行銷曝光 . 同業交流 . 接案攻略 . 職業工會 . 各類保障方案</p>
                                                          </div>
                                                          <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-lg-0 mb-sm-4 mb-4">
                                                              <img src="/themes/Epro/img/index/repair-1.jpg" class="img-fluid">
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="grid__item" id="team-link">
                                          <div class="product">
                                              <div class="tm-nav-link">
                                                  <i class="fas fa-wrench fa-3x tm-nav-icon"></i>

                                                  <span class="tm-nav-text">VIP 服務<br><small>需要量身訂製的<br>專業技術服務</small></span>

                                                  <div class="product__bg"></div>
                                              </div>

                                              <div class="product__description">
                                                  <div class="p-sm-4 p-2">
                                                      <div class="row mb-3">
                                                          <div class="col-12">
                                                              <h2 class="tm-page-title">VIP級服務</h2>
                                                              <div class="line-1"></div>
                                                          </div>
                                                      </div>
                                                      <div class="row tm-reverse-sm">
                                                          <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                                              <h4 class="m150">需要量身訂製的專業技術服務，達人將提供最佳解決方案</h4>
                                                              <h5>專業職人的最佳謀合平台</h5>
                                                              <p class="mb-4 m150">◎ 開發客戶 . 免成交手續費 . 進案通知自動配對 . 免費註冊 . 案源多 . 謀合快 . 行銷曝光 . 同業交流 . 接案攻略 . 職業工會 . 各類保障方案</p>
                                                          </div>
                                                          <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-lg-0 mb-sm-4 mb-4">
                                                              <img src="/themes/Epro/img/index/repair-2.jpg" class="img-fluid">
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="grid__item">
                                          <div class="product">
                                              <div class="tm-nav-link">
                                                  <i class="fas fa-heart fa-3x tm-nav-icon"></i>
                                                  <span class="tm-nav-text m30">達人密技</span>
                                                  <div class="product__bg"></div>
                                              </div>
                                              <div class="product__description">
                                                  <div class="row mb-3">
                                                      <div class="col-12">
                                                          <h2 class="tm-page-title">達人密技</h2>
                                                          <div class="line-1"></div>
                                                      </div>
                                                  </div>
                                                  <div class="row">
                                                      <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                          <img src="/themes/Epro/img/index/product_2.jpg" class="img-fluid">
                                                          <p class="graycolor">達人 小林</p>
                                                          <h4 class="ycolor">粉刷五顏六色 怎麼挑?</h4>
                                                          <p>室內粉刷色系玲瑯滿目該怎麼挑出既美觀又安全的顏色呢? 讓裝修達人-小白分享他的專業建議……</p>
                                                      </div>
                                                      <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                          <h4 class="ycolor">粉刷五顏六色 怎麼挑?</h4>
                                                          <p>室內粉刷色系玲瑯滿目該怎麼挑出既美觀又安全的顏色呢? 讓裝修達人-小白分享他的專業建議……</p>
                                                          <img src="/themes/Epro/img/index/product_1.jpg" class="img-fluid">
                                                          <p class="graycolor">達人 小白</p>
                                                      </div>
                                                  </div>
                                                  <p><a href="#" class="btn tm-btn-gray">更多密技</a></p>

                                              </div>
                                          </div>
                                      </div>

                                      <div class="grid__item">
                                          <div class="product">
                                              <div class="tm-nav-link">
                                                  <i class="fas fa-hand-holding-heart fa-3x tm-nav-icon"></i>
                                                  <span class="tm-nav-text m30">體驗分享</span>
                                                  <div class="product__bg"></div>
                                              </div>
                                              <div class="product__description">
                                                  <div class="p-sm-4 p-2">
                                                      <div class="row mb-3">
                                                          <div class="col-12">
                                                              <h2 class="tm-page-title">體驗分享</h2>
                                                              <div class="line-1"></div>
                                                          </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                          <div class="col-12">
                                                              <p>不論你是新品上市，或是你想借由現有顧客創造影響力，透過我們的平台，絕對讓你事半功倍！</p>
                                                          </div>
                                                      </div>
                                                      <div class="row">
                                                          <div class="col-12">
                                                              <div class="p-sm-4 p-2 tm-img-container">
                                                                  <div class="tm-img-slider" id="tmImgSlider">
                                                                      <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_1.jpg" alt="Image 1" class="img-fluid tm-slider-img"></a>
                                                                      <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_2.jpg" alt="Image 2" class="img-fluid tm-slider-img"></a>
                                                                      <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_3.jpg" alt="Image 3" class="img-fluid tm-slider-img"></a>
                                                                      <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_4.jpg" alt="Image 4" class="img-fluid tm-slider-img"></a>
                                                                      <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_5.jpg" alt="Image 5" class="img-fluid tm-slider-img"></a>
                                                                      <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_6.jpg" alt="Image 6" class="img-fluid tm-slider-img"></a>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
<div class="main row">
  <div class="cell-3">
    <div class="row"></div>
  </div>
  <div class="cell-10"></div>
</div>
</div>


<div class="search_windows search_windows_hode">
    <?php
$_from = $_smarty_tpl->tpl_vars['windows_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_1_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_1_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?> <?php if ($_smarty_tpl->tpl_vars['key']->value%10 == 0) {?>
    <div class="row" <?php if (($_smarty_tpl->tpl_vars['key']->value/10)%2 == 1) {?>style="margin-left: 4%;" <?php }?>>
        <?php }?>
        <div class="cell_window" style="background: url(<?php echo $_smarty_tpl->tpl_vars['value']->value['img'];?>
);">
            <span><a href="<?php echo $_smarty_tpl->tpl_vars['value']->value['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['value']->value['keywords'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</a></span>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['key']->value%10 == 9 || $_smarty_tpl->tpl_vars['windows_count']->value == $_smarty_tpl->tpl_vars['key']->value) {?>
    </div>
    <?php }?> <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_local_item;
}
}
if ($__foreach_value_1_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_item;
}
if ($__foreach_value_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_1_saved_key;
}
?>
</div>

                                                  
<?php }
}
