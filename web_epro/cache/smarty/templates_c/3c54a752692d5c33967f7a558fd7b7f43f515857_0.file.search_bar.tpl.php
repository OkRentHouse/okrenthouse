<?php
/* Smarty version 3.1.28, created on 2020-12-09 19:03:05
  from "/home/ilifehou/life-house.com.tw/themes/Epro/search_bar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd0aee98b26a9_15869159',
  'file_dependency' => 
  array (
    '3c54a752692d5c33967f7a558fd7b7f43f515857' => 
    array (
      0 => '/home/ilifehou/life-house.com.tw/themes/Epro/search_bar.tpl',
      1 => 1607511783,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd0aee98b26a9_15869159 ($_smarty_tpl) {
?>
<div class="search_bar">
    <div class="row">
        <div class="cell-4 search mr">
            <div>
              <a href="####" class="logo_a"><img class="logo_white" src="/themes/Epro/img/index/logo-white.png"></a>
              <ul class="l_txt">

                  <li class="list"><a href="####">找達人</a></li>
                  <li class=""><a href="####">接案子</a></li>
                </ul>
            </div>
        </div>
        <div class="cell-4 search">
            <div class="search_bar_m">
                <input type="text" class="search_txtt" placeholder="可輸入，如：冷氣不冷 | 馬桶不通 | 壁癌粉刷 ... ">
                <button value="search" class="search_bnt">︾</button>
            </div>
        </div>
        <div class="cell-4 search md">
            <div>

              <ul class="r_txt">
                  <li class="list"><a href="####">註冊</a></li>
                  <li class=""><a href="####">登入</a></li>
                  <li class=""><svg id="skills" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 384.97 384.97" style="enable-background:new 0 0 384.97 384.97;" xml:space="preserve">
                          <rect id="backgroundrect" width="100%" height="100%" x="0" y="0" fill="none" stroke="none" />

                          <g class="currentLayer" style="">
                              <title>Layer 1</title>

                              <g id="svg_5" class="">

                              </g>

                              <g id="svg_6" class="">

                              </g>

                              <g id="svg_7" class="">

                              </g>

                              <g id="svg_8" class="">

                              </g>

                              <g id="svg_9" class="">

                              </g>

                              <g id="svg_10" class="">

                              </g>

                              <g id="svg_11">

                              </g>
                              <g id="svg_12">

                              </g>
                              <g id="svg_13">

                              </g>
                              <g id="svg_14">

                              </g>
                              <g id="svg_15">

                              </g>
                              <g id="svg_16">

                              </g>
                              <g id="svg_17">

                              </g>
                              <g id="svg_18">

                              </g>
                              <g id="svg_19">

                              </g>
                              <g id="svg_20">

                              </g>
                              <g id="svg_21">

                              </g>
                              <g id="svg_22">

                              </g>
                              <g id="svg_23">

                              </g>
                              <g id="svg_24">

                              </g>
                              <g id="svg_25">

                              </g>
                              <g class="selected">
                                  <path
                                      d="M12.03,119.69693943548202 h360.909 c6.641,0 12.03,-5.39 12.03,-12.03 c0,-6.641 -5.39,-12.03 -12.03,-12.03 H12.03 c-6.641,0 -12.03,5.39 -12.03,12.03 C0,114.30693943548202 5.39,119.69693943548202 12.03,119.69693943548202 z"
                                      id="svg_2" class="" fill-opacity="1" stroke="#000000" stroke-opacity="1" />
                                  <path
                                      d="M372.939,179.84893943548204 H12.03 c-6.641,0 -12.03,5.39 -12.03,12.03 s5.39,12.03 12.03,12.03 h360.909 c6.641,0 12.03,-5.39 12.03,-12.03 S379.58,179.84893943548204 372.939,179.84893943548204 z"
                                      id="svg_3" class="" fill-opacity="1" stroke="#000000" stroke-opacity="1" />
                                  <path
                                      d="M367.51212920129797,264.060939435482 H18.36595369256031 c-9.636832629084587,0 -17.456873441934583,5.39 -17.456873441934583,12.03 c0,6.641 7.821491924524307,12.03 17.456873441934583,12.03 h349.14617550873754 c9.636832629084587,0 17.456873441934583,-5.39 17.456873441934583,-12.03 C384.97045375490677,269.449939435482 377.1489618303824,264.060939435482 367.51212920129797,264.060939435482 z"
                                      id="svg_4" class="" fill-opacity="1" stroke="#000000" stroke-opacity="1" />
                              </g>
                          </g>
                      </svg>
                  </li>
                </ul>

            </div>
        </div>
    </div>
</div>
<?php }
}
