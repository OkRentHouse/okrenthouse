<?php
/* Smarty version 3.1.28, created on 2020-12-12 22:06:29
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/msg_box.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fd4ce65625bb3_43315278',
  'file_dependency' => 
  array (
    '7eb35819a2773ccfd4c37c380e9d7e4e1b538208' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/msg_box.tpl',
      1 => 1607678484,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fd4ce65625bb3_43315278 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['_msg']->value) && $_smarty_tpl->tpl_vars['_msg']->value != '' && $_smarty_tpl->tpl_vars['_error']->value == '') {?>
<div class="alert alert-success <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_msg']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
if (isset($_smarty_tpl->tpl_vars['_error']->value) && $_smarty_tpl->tpl_vars['_error']->value != '') {?>
<div class="alert alert-danger <?php echo $_smarty_tpl->tpl_vars['this']->value->show_type;?>
" role="alert"><strong><?php echo $_smarty_tpl->tpl_vars['strong']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['_error']->value;?>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button></div>
<?php }
}
}
