<?php
/* Smarty version 3.1.28, created on 2021-01-04 19:02:08
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/page_banner.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5ff2f5b0eeeff3_64819543',
  'file_dependency' => 
  array (
    'e7d795e2c07b9ae0cf40c62f4888977e7a1aeab0' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/page_banner.tpl',
      1 => 1609758118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ff2f5b0eeeff3_64819543 ($_smarty_tpl) {
?>
<div id="tm-wrap">
    <div class="tm-main-content">
        <div class="container tm-site-header-container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 col-md-col-xl-6 mb-md-0 mb-sm-4 mb-4 tm-site-header-col">
                    <div class="tm-site-header">
                        <h1 class="mb-4 main-title"><?php echo $_smarty_tpl->tpl_vars['page_title']->value;?>
</h1>
                        <img src="/themes/Epro/img/index/underline.png" class="img-fluid mb-4">
                        <p class="second-title">體驗便利的裝修，服務專業貼心，<br>是您可靠的裝修專家</p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="content">
                        <div class="grid">
                            <div class="grid__item" id="home-link" onclick="window.open('/Service','_self')">
                                <div class="product">
                                    <div class="tm-nav-link">
                                        <i class="fas fa-tools fa-3x tm-nav-icon"></i>
                                        <span class="tm-nav-text m30">服務選單</span>
                                        <div class="product__bg"></div>
                                    </div>
                                    <div class="product__description">
                                        <div class="p-sm-4 p-2">
                                            <div class="row mb-3">
                                                <div class="col-12">
                                                    <h2 class="tm-page-title">PRO級服務</h2>
                                                    <div class="line-1"></div>
                                                </div>
                                            </div>
                                            <div class="row tm-reverse-sm">
                                                <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                                    <h4 class="m150">適合只需要專業技術類型的服務</h4>
                                                    <h5>專業職人的最佳謀合平台</h5>
                                                    <!-- <p class="mb-4 m150">◎ 開發客戶 . 免成交手續費 . 進案通知自動配對 . 免費註冊 . 案源多 . 謀合快 . 行銷曝光 . 同業交流 . 接案攻略 . 職業工會 . 各類保障方案</p> -->
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-lg-0 mb-sm-4 mb-4">
                                                    <img src="/themes/Epro/img/index/repair-1.jpg" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid__item" id="team-link" onclick="window.open('/Orderrequest','_self')">
                                <div class="product">
                                    <div class="tm-nav-link">
                                        <i class="fas fa-toolbox fa-3x tm-nav-icon"></i>
                                        <span class="tm-nav-text m30">叫修派工</span>
                                        <div class="product__bg"></div>
                                    </div>

                                    <div class="product__description">
                                        <div class="p-sm-4 p-2">
                                            <div class="row mb-3">
                                                <div class="col-12">
                                                    <h2 class="tm-page-title">VIP級服務</h2>
                                                    <div class="line-1"></div>
                                                </div>
                                            </div>
                                            <div class="row tm-reverse-sm">
                                                <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                                    <h4 class="m150">需要量身訂製的專業技術服務，達人將提供最佳解決方案</h4>
                                                    <h5>專業職人的最佳謀合平台</h5>
                                                    <!-- <p class="mb-4 m150">◎ 開發客戶 . 免成交手續費 . 進案通知自動配對 . 免費註冊 . 案源多 . 謀合快 . 行銷曝光 . 同業交流 . 接案攻略 . 職業工會 . 各類保障方案</p> -->
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-lg-0 mb-sm-4 mb-4">
                                                    <img src="/themes/Epro/img/index/repair-2.jpg" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid__item" onclick="window.open('/Master','_self')">
                                <div class="product">
                                    <div class="tm-nav-link">
                                        <i class="fas fa-heart fa-3x tm-nav-icon"></i>
                                        <span class="tm-nav-text m30">找達人</span>
                                        <div class="product__bg"></div>
                                    </div>
                                    <div class="product__description">
                                        <div class="row mb-3">
                                            <div class="col-12">
                                                <h2 class="tm-page-title">找達人</h2>
                                                <div class="line-1"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                                <!-- <p class="graycolor">達人 小林</p> -->
                                                <h4 class="ycolor">裝修眉角這麼多 該注意些什麼? <br>就讓達人來分享吧!</h4><br>
                                                <!-- <p>室內粉刷色系玲瑯滿目該怎麼挑出既美觀又安全的顏色呢? 讓裝修達人-小白分享他的專業建議……</p> -->
                                                <img src="/themes/Epro/img/index/product_1.jpg" class="img-fluid">
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <br><br><br><br><br><br>
                                                <h4 class="ycolor">粉刷五顏六色 怎麼挑?</h4>
                                                <p>室內粉刷色系玲瑯滿目該怎麼挑出既美觀又安全的顏色呢? 讓裝修達人-小白分享他的專業建議……</p>
                                                <!-- <img src="/themes/Epro/img/index/product_1.jpg" class="img-fluid"> -->
                                                <!-- <p class="graycolor">達人 小白</p> -->
                                            </div>
                                        </div><br>
                                        <p><a href="#" class="btn tm-btn-gray">更多密技</a></p>

                                    </div>
                                </div>
                            </div>

                            <div class="grid__item" onclick="window.open('/Service','_self')">
                                <div class="product">
                                    <div class="tm-nav-link">
                                        <i class="fas fa-hand-holding-heart fa-3x tm-nav-icon"></i>
                                        <span class="tm-nav-text m30">體驗分享</span>
                                        <div class="product__bg"></div>
                                    </div>
                                    <div class="product__description">
                                        <div class="p-sm-4 p-2">
                                            <div class="row mb-3">
                                                <div class="col-12">
                                                    <h2 class="tm-page-title">體驗分享</h2>
                                                    <div class="line-1"></div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-12">
                                                    <p>不論你是新品上市，或是你想借由現有顧客創造影響力，透過我們的平台，絕對讓你事半功倍！</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="p-sm-4 p-2 tm-img-container">
                                                        <div class="tm-img-slider" id="tmImgSlider">
                                                            <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_1.jpg" alt="Image 1" class="img-fluid tm-slider-img"></a>
                                                            <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_2.jpg" alt="Image 2" class="img-fluid tm-slider-img"></a>
                                                            <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_3.jpg" alt="Image 3" class="img-fluid tm-slider-img"></a>
                                                            <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_4.jpg" alt="Image 4" class="img-fluid tm-slider-img"></a>
                                                            <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_5.jpg" alt="Image 5" class="img-fluid tm-slider-img"></a>
                                                            <a href="#" class="tm-slider-img-link"><img src="/themes/Epro/img/index/people_6.jpg" alt="Image 6" class="img-fluid tm-slider-img"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 次頁主內容 -->
        <article>


        </article>

    </div>
    <!-- .tm-main-content -->
<?php }
}
