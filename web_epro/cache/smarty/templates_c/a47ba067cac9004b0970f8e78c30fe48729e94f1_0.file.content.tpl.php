<?php
/* Smarty version 3.1.28, created on 2021-01-05 18:33:00
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Orderrequest/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5ff4405c6029c9_65319454',
  'file_dependency' => 
  array (
    'a47ba067cac9004b0970f8e78c30fe48729e94f1' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Orderrequest/content.tpl',
      1 => 1609842778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tab1.tpl' => 1,
    'file:tab2.tpl' => 1,
  ),
),false)) {
function content_5ff4405c6029c9_65319454 ($_smarty_tpl) {
?>


<div class="bottom_fix_div">
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./search_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<div class="container-fluid">
        <header style="max-width: 960px;">

        </header>


        <div id="tm-bg"></div>


        <div id="tm-wrap-case">
            <div class="tm-main-content">

                <!-- 次頁主內容 -->
                <article>





                </article>

            </div>
            <!-- .tm-main-content -->

                <div class="details">
        			<div class="details__bg details__bg--down" style="opacity: 0; transform: none;">
        				<button class="details__close" style="opacity: 0; transform: translateY(100%);"><svg class="svg-inline--fa fa-times fa-w-11 fa-2x icon--cross tm-fa-close" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" data-fa-i2svg=""><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg><!-- <i class="fas fa-2x fa-times icon--cross tm-fa-close"></i> Font Awesome fontawesome.com --></button>
        				<div class="details__description" style="opacity: 0;"></div>
        			</div>
        			</div></div>


        <div class="details">
			<div class="details__bg details__bg--down" style="opacity: 0; transform: none;">
				<button class="details__close" style="opacity: 0; transform: translateY(100%);"><i class="fas fa-2x fa-times icon--cross tm-fa-close"></i></button>
				<div class="details__description" style="opacity: 0;"></div>
			</div>
			</div></div>
    </div>

    <div class="container-fluid main" id="main">

        <div class="breadcrumbs">
            <p><?php echo $_smarty_tpl->tpl_vars['breadcrumbs_html']->value;
if ($_smarty_tpl->tpl_vars['sidname']->value) {?> &gt; <?php echo $_smarty_tpl->tpl_vars['sidname']->value;
}?></p><br>
        </div>




        <div class="row">
          <div class="col-sm-1 "></div>
          <div class="col-sm-10 ">
                <div class="col-xs-12 col-sm-12 form_1 forms_div">

                  <img src="/themes/Epro/img/case/orderrequest_img_1.jpg">

                  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tab1.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                </div><div class="col-xs-12 col-sm-12 form_2 forms_div">

                  <img src="/themes/Epro/img/case/orderrequest_img_2.jpg">

                  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tab2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                </div>
          </div>
          <div class="col-sm-1 "></div>
        </div>
    </div>
<?php }
}
