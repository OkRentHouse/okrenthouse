<?php
/* Smarty version 3.1.28, created on 2020-12-30 19:28:11
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Profile/tab4.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fec644b75ea20_69228769',
  'file_dependency' => 
  array (
    'd40dc84cc667cd12c04915a35b962c9b45347c90' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Profile/tab4.tpl',
      1 => 1609327689,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fec644b75ea20_69228769 ($_smarty_tpl) {
?>
<div class="col-sm-12 tab_4 tabs">
  <ul>
    <li class="title">施做案例:</li>
    <li class="content flex more">
      <?php $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable(array("profile-07.jpg","profile-08.jpg","profile-09.jpg","profile-10.jpg"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'img', 0);?>
      <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < 4) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < 4; $_smarty_tpl->tpl_vars['i']->value++) {
?>

      <div class="more">
        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="minus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-minus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M140 284c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h232c6.6 0 12 5.4 12 12v32c0 6.6-5.4 12-12 12H140zm364-28c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-48 0c0-110.5-89.5-200-200-200S56 145.5 56 256s89.5 200 200 200 200-89.5 200-200z" class=""></path></svg>
        <img src="/themes/Epro/img/master/<?php echo $_smarty_tpl->tpl_vars['img']->value[$_smarty_tpl->tpl_vars['i']->value];?>
">
        <h4>水電安裝/修繕</h4>
        <span>
        快速獲得修繕、清潔報價給你最快速、最專業的叫修體驗</span>
      </div>
      <?php }
}
?>


    </li>


    <ul class="detail">

      <li class="time_date">上傳照片：<div class="upload" onclick="$('#myphoto').click()"><div class="frame"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div></div><input type="file" style="display:none" id="myphoto"></li>
      <li class="contact">主標題：<input type="text"></li>
      <li class="contact">說明文字：<textarea></textarea></li>
    </ul>


</ul>
</div>
<?php }
}
