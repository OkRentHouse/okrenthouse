<?php
/* Smarty version 3.1.28, created on 2020-12-30 08:37:07
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Service/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5febcbb327efd0_07074631',
  'file_dependency' => 
  array (
    '3b1a47863e449360a50c021d6413f8701bc645cf' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Service/content.tpl',
      1 => 1609244017,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../page_banner.tpl' => 1,
    'file:../../page_left.tpl' => 1,
  ),
),false)) {
function content_5febcbb327efd0_07074631 ($_smarty_tpl) {
?>


<div class="bottom_fix_div">
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./search_bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<div class="container-fluid">
        <header style="max-width: 960px;">

        </header>


        <div id="tm-bg"></div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../page_banner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <div class="details">
			<div class="details__bg details__bg--down" style="opacity: 0; transform: none;">
				<button class="details__close" style="opacity: 0; transform: translateY(100%);"><i class="fas fa-2x fa-times icon--cross tm-fa-close"></i></button>
				<div class="details__description" style="opacity: 0;"></div>
			</div>
			</div></div>
    </div>

    <div class="container-fluid main" id="main">

        <div class="breadcrumbs">
            <p><?php echo $_smarty_tpl->tpl_vars['breadcrumbs_html']->value;
if ($_smarty_tpl->tpl_vars['sidname']->value) {?> &gt; <?php echo $_smarty_tpl->tpl_vars['sidname']->value;
}?></p><br>
        </div>




        <div class="row">
            <div class="col-xs-12 col-sm-2">
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../../page_left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <div class="col-xs-12 col-sm-10">


                <div>


                    <div class="desk">
                        <h3 class="center m030"><img src="/themes/Epro/img/index/tool-1.png" alt=""> 最新 <img src="/themes/Epro/img/index/tool-2.png" alt=""></h3>
                        <div class="avi">
                            <div class="card-deck">
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_1.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_2.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_3.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile">  <img src="/themes/Epro/img/index/people_4.jpg" alt=""></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                    <!-- <div class="center m15">
                                        <button type="button" class="btn btn-success btn-md ">我要詢問</button>
                                    </div> -->
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="m00600">
                        <div class="row justify-content-center align-items-center m15">
                            <!-- *** 分頁page-1 *** -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            &lt;</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">&gt;</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="desk">
                        <h3 class="center m30"><img src="/themes/Epro/img/index/tool-1.png" alt=""> 熱門 <img src="/themes/Epro/img/index/tool-2.png" alt=""></h3>
                        <div class="avi">
                            <div class="card-deck">
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_5.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile">  <img src="/themes/Epro/img/index/people_6.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile">   <img src="/themes/Epro/img/index/people_4.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_3.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="m00600">
                        <div class="row justify-content-center align-items-center m15">
                            <!-- *** 分頁page-1 *** -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            &lt;</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">&gt;</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>






                    <div class="desk">
                        <h3 class="center m30"><img src="/themes/Epro/img/index/tool-1.png" alt=""> 優惠 <img src="/themes/Epro/img/index/tool-2.png" alt=""></h3>
                        <div class="avi">
                            <div class="card-deck">
                                <div class="card">
                                    <a href="/Masterprofile">  <img src="/themes/Epro/img/index/people_2.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_1.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_3.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                                <div class="card">
                                    <a href="/Masterprofile"> <img src="/themes/Epro/img/index/people_5.jpg" class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <h5 class="card-title">開關插座維修</h5>
                                        <p class="card-text">報價<snap class="right-1">$500<small>/坪</small></snap></p>
                                        <p class="card-text">區域</p>
                                        <!-- <p class="card-text"><i class="fas fa-eye"></i> 1225 關注</p> -->
                                        <!-- <p class="card-text" style="text-align: right;"><i class="far fa-thumbs-up"></i> 225</p> -->
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="">
                        <div class="row justify-content-center align-items-center m15">
                            <!-- *** 分頁page-1 *** -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            &lt;</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">&gt;</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <!-- === END CONTENT === -->
                </div>
            </div>
        </div>
    </div>
<?php }
}
