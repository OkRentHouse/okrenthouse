<?php
/* Smarty version 3.1.28, created on 2020-12-30 19:41:25
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/page_left.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fec67653f1908_56068045',
  'file_dependency' => 
  array (
    '3659d0cc0353420c461ea290d1909e303fe8bd16' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/page_left.tpl',
      1 => 1609328426,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fec67653f1908_56068045 ($_smarty_tpl) {
?>

                <div class="aside">
                    <h3 class="center m00150"><svg class="svg-inline--fa fa-tools fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tools" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M501.1 395.7L384 278.6c-23.1-23.1-57.6-27.6-85.4-13.9L192 158.1V96L64 0 0 64l96 128h62.1l106.6 106.6c-13.6 27.8-9.2 62.3 13.9 85.4l117.1 117.1c14.6 14.6 38.2 14.6 52.7 0l52.7-52.7c14.5-14.6 14.5-38.2 0-52.7zM331.7 225c28.3 0 54.9 11 74.9 31l19.4 19.4c15.8-6.9 30.8-16.5 43.8-29.5 37.1-37.1 49.7-89.3 37.9-136.7-2.2-9-13.5-12.1-20.1-5.5l-74.4 74.4-67.9-11.3L334 98.9l74.4-74.4c6.6-6.6 3.4-17.9-5.7-20.2-47.4-11.7-99.6.9-136.6 37.9-28.5 28.5-41.9 66.1-41.2 103.6l82.1 82.1c8.1-1.9 16.5-2.9 24.7-2.9zm-103.9 82l-56.7-56.7L18.7 402.8c-25 25-25 65.5 0 90.5s65.5 25 90.5 0l123.6-123.6c-7.6-19.9-9.9-41.6-5-62.7zM64 472c-13.2 0-24-10.8-24-24 0-13.3 10.7-24 24-24s24 10.7 24 24c0 13.2-10.7 24-24 24z"></path></svg><!-- <i class="fas fa-tools"></i> Font Awesome fontawesome.com --> 搜尋條件</h3>
                    <!-- <div class="box1">
                        <h4>類型</h4>
                        <div class="btn-wc"><button type="button" class="btn btn-success btn-wc">PRO 型</button> </div>
                        <div class="btn-wc"><button type="button" class="btn btn-success btn-wc">VIP 型</button> </div>

                    </div> -->
                    <!-- 類別 -->
                    <div class="box2">
                        <div class="nav-list">
                            <ul class="menu m00">

                                <li class="itembox drop-down">
                                    <a class="item" href="#">類別 <svg class="svg-inline--fa fa-angle-down fa-w-10" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg=""><path fill="currentColor" d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"></path></svg><!-- <i class="fas fa-angle-down "></i> Font Awesome fontawesome.com --></a>
                                    <div class="submenu">
                                        <a class="submenu-item" href="/Service<?php if ($_smarty_tpl->tpl_vars['sid']->value) {?>?sid=<?php echo $_smarty_tpl->tpl_vars['sid']->value;
}?>#main">專業服務</a>
                                        <a class="submenu-item" href="/Vip<?php if ($_smarty_tpl->tpl_vars['sid']->value) {?>?sid=<?php echo $_smarty_tpl->tpl_vars['sid']->value;
}?>#main">量身訂做</a>
                                    </div>
                                </li>
                            </ul>

                        </div>

                    </div>
                    <!-- 項目 -->
                    <!-- <div class="box2">
                        <div class="nav-list">
                            <ul class="menu m00">
                                </li>
                                <li class="itembox drop-down">
                                    <a class="item" href="#">項目 <i class="fas fa-angle-down "></i></a>
                                    <div class="submenu">
                                        <a class="submenu-item">裝潢設計</a>
                                        <a class="submenu-item">衛浴</a>
                                        <a class="submenu-item">廚具</a>
                                        <a class="submenu-item">水電</a>
                                        <a class="submenu-item">冷氣空調</a>
                                        <a class="submenu-item">電器</a>
                                        <a class="submenu-item">熱水器</a>
                                        <a class="submenu-item">油漆粉刷</a>
                                        <a class="submenu-item">壁紙</a>
                                        <a class="submenu-item">管線|通管</a>
                                        <a class="submenu-item">燈飾照明</a>
                                        <a class="submenu-item">木作</a>
                                        <a class="submenu-item">泥作</a>
                                        <a class="submenu-item">鐵作</a>
                                        <a class="submenu-item">地板</a>
                                        <a class="submenu-item">石材及美化</a>
                                        <a class="submenu-item">玻璃|磁磚</a>
                                        <a class="submenu-item">防水抓漏</a>
                                        <a class="submenu-item">門窗|鎖具</a>
                                        <a class="submenu-item">安控監視</a>
                                        <a class="submenu-item">消防保全</a>
                                        <a class="submenu-item">園藝植栽</a>
                                        <a class="submenu-item">清潔消毒</a>
                                        <a class="submenu-item">窗簾地毯</a>
                                        <a class="submenu-item">淨水設備</a>
                                        <a class="submenu-item">升降設備</a>
                                        <a class="submenu-item">機電|弱電</a>
                                        <a class="submenu-item">節能|綠建材</a>
                                        <a class="submenu-item">智慧居家</a>
                                        <a class="submenu-item">房屋檢測</a>
                                        <a class="submenu-item">無障礙空間</a>
                                    </div>
                                </li>
                            </ul>

                        </div>

                    </div> -->
                    <!-- <div class="box2">
                        <h4>類型</h4>
                        <div class="list">
                            <div class="form-check">
                                <ul>
                                    <li><input class="form-check-input position-static" type="checkbox" checked="checked" id="blankCheckbox" value="option1" aria-label="..."> PRO 專業型</li>

                                    <li><input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="..."> VIP 量身訂做型</li>
                                </ul>
                            </div>
                        </div>

                    </div> -->
                    <div class="box2">
                        <h4>排序</h4>
                        <div class="list">
                            <div class="form-check">
                                <ul>
                                    <li><input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="..."> 更新時間</li>

                                    <li><input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="..."> 價格 (低-&gt;高)</li>
                                    <li><input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="..."> 點閱數</li>
                                    <li><input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="..."> 評價數</li>
                                    <!-- <li><input class="form-check-input position-static" type="checkbox" checked="checked" id="blankCheckbox" value="option1" aria-label="..."> PRO 服務</li>
                                    <li><input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="..."> VIP 服務</li> -->
                                </ul>
                            </div>
                        </div>

                    </div>
                    <!-- <div class="box2">
                        <h4>區域</h4>
                        <div class="list">
                            <form>
                                <div class="row">
                                  <div class="col">
                                    <input type="text" class="form-control" placeholder="縣市">
                                  </div>
                                  <div class="col">
                                    <input type="text" class="form-control" placeholder="行政">
                                  </div>
                                </div>
                              </form>
                        </div>

                    </div> -->
                    <div class="box2">
                        <!-- <h4>區域 <i class="fas fa-angle-down "></i></h4> -->
                        <div class="list">
                            <form>
                                <select class="city" style="width: 42%;">
                                    <option>選擇縣市</option>
                                    <option>基隆市</option>
                                    <option>台北市</option>
                                    <option>新北市</option>
                                    <option selected="selected">桃園市</option>
                                    <option>新竹市</option>
                                    <option>苗栗縣</option>
                                </select>&nbsp; &nbsp;
                                <select class="district" style="width: 42%;">
                                    <option selected="selected">桃園區</option>
                                    <option></option>
                                    <option></option>
                                    <option></option>
                                    <option></option>
                                    <option></option>
                                    <option></option>
                                </select>

                            </form>
                        </div>

                    </div>
                    <div class="box2">
                        <h4>價格</h4>
                        <div class="list">
                            <form>
                                <div class="price">
                                    <input type="text" class="form-control" placeholder="">
                                    <span>～</span>
                                    <input type="text" class="form-control" placeholder="">
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="box3">
                        <div class="card">
                            <!-- 也可以不設定固定寬度，把style="width: 18rem;"拿掉 -->
                            <h5 class="card-title">健康宅清淨對策</h5>
                            <img src="/themes/Epro/img/index/product_3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">檢測+滅菌+除味</p>
                                <a href="#" class="btn btn-primary">7折限時優惠</a>
                            </div>
                        </div>
                    </div>
                    <div class="box3">
                        <div class="card">
                            <!-- 也可以不設定固定寬度，把style="width: 18rem;"拿掉 -->
                            <h5 class="card-title">健康宅清淨對策</h5>
                            <img src="/themes/Epro/img/index/product_3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">檢測+滅菌+除味</p>
                                <a href="#" class="btn btn-primary">7折限時優惠</a>
                            </div>
                        </div>
                    </div>
                    <div class="box3">
                        <div class="card">
                            <!-- 也可以不設定固定寬度，把style="width: 18rem;"拿掉 -->
                            <h5 class="card-title">健康宅清淨對策</h5>
                            <img src="/themes/Epro/img/index/product_3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">檢測+滅菌+除味</p>
                                <a href="#" class="btn btn-primary">7折限時優惠</a>
                            </div>
                        </div>
                    </div>
                    <div class="box3">
                        <div class="card">
                            <!-- 也可以不設定固定寬度，把style="width: 18rem;"拿掉 -->
                            <h5 class="card-title">健康宅清淨對策</h5>
                            <img src="/themes/Epro/img/index/product_3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">檢測+滅菌+除味</p>
                                <a href="#" class="btn btn-primary">7折限時優惠</a>
                            </div>
                        </div>
                    </div>
                    <div class="box3">
                        <div class="card">
                            <!-- 也可以不設定固定寬度，把style="width: 18rem;"拿掉 -->
                            <h5 class="card-title">健康宅清淨對策</h5>
                            <img src="/themes/Epro/img/index/product_3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">檢測+滅菌+除味</p>
                                <a href="#" class="btn btn-primary">7折限時優惠</a>
                            </div>
                        </div>
                    </div>

                </div>
<?php }
}
