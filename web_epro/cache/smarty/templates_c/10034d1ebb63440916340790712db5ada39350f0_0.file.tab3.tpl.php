<?php
/* Smarty version 3.1.28, created on 2020-12-30 19:24:03
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Profile/tab3.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5fec63531f5eb2_27864351',
  'file_dependency' => 
  array (
    '10034d1ebb63440916340790712db5ada39350f0' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Profile/tab3.tpl',
      1 => 1609327441,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fec63531f5eb2_27864351 ($_smarty_tpl) {
?>
<div class="col-sm-12 tab_3 tabs">
  <ul>
    <li class="title">評價紀錄:</li>
    <li class="content flex">
      <div class="assess">
        <div class="view">
          <ul class="row">
            <?php $_smarty_tpl->tpl_vars['ttnum'] = new Smarty_Variable(4.8, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ttnum', 0);?>
            <?php $_smarty_tpl->tpl_vars['talk'] = new Smarty_Variable(61, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'talk', 0);?>
            <li class="ttnum"><?php echo $_smarty_tpl->tpl_vars['ttnum']->value;?>
</li>
            <li class="avgstat"><?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['ttnum']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['ttnum']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-9x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><?php }
}
?>
</li>
            <li class="talk"><?php echo $_smarty_tpl->tpl_vars['talk']->value;?>
個認證評價</li>
          </ul>
        </div>
        <div class="list">
          <ul class="row">
            <?php $_smarty_tpl->tpl_vars['list_row'] = new Smarty_Variable(array("98","2","0","0","0"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'list_row', 0);?>
            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value <= sizeof($_smarty_tpl->tpl_vars['list_row']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= sizeof($_smarty_tpl->tpl_vars['list_row']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
            <li class="lv"><span class="num"><?php echo 5-$_smarty_tpl->tpl_vars['i']->value;?>
<span><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18 fa-9x"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class=""></path></svg><label class="line"><label style="width: <?php echo $_smarty_tpl->tpl_vars['list_row']->value[$_smarty_tpl->tpl_vars['i']->value]*2;?>
px;" class="line_go"></label></label><span class="percen"><?php echo $_smarty_tpl->tpl_vars['list_row']->value[$_smarty_tpl->tpl_vars['i']->value];?>
<span></li>
            <?php }
}
?>

          </ul>
        </div>
      </div>
    </li>

</ul>
</div>
<?php }
}
