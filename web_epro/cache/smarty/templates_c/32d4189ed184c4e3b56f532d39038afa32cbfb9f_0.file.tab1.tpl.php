<?php
/* Smarty version 3.1.28, created on 2021-01-29 17:08:13
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Profile/tab1.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_6013d07d1c2f88_92567647',
  'file_dependency' => 
  array (
    '32d4189ed184c4e3b56f532d39038afa32cbfb9f' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Profile/tab1.tpl',
      1 => 1611911278,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6013d07d1c2f88_92567647 ($_smarty_tpl) {
?>
<div class="col-sm-12 tab_1 tabs">
  <ul>
    <li class="title">上傳工作室圖片:</li>
    <li class="content inline"><div class="upload" onclick="$('#frame').click()" data-name="work_photo" data-value="0" class="get_file"><div class="frame"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>
      <img class="view" id="frame_0" src="" style="display: none">
    </div>
      <input type="file" style="display:none" id="frame" name="work_photo" onchange="get_file_img('frame')" required="required">
    <!-- <input type="file" id="file_2" name="account_img[]" onchange="get_file_img('file_2')" required="required" style="display:none"> -->

    <ul>
        <li class="title">上傳大頭照:</li>
        <li class="content"><div class="upload" onclick="$('#frame_1').click()" data-name="my_photo" data-value="0" class="get_file"><div class="frame_1"><i class="fas fa-cloud-upload-alt"></i><br>拖曳檔案到此處<br>或<br><button type="button">加入圖片</button></div>
          <img class="view" id="frame_1_0" src="" style="display: none">
            </div>
          <input type="file" style="display:none" id="frame_1" name="my_photo" onchange="get_file_img('frame_1')" required="required">
        </li>
      </ul>
  </li>
    <!-- <li class="title">姓名:</li>
    <li class="content name"><input type="text" name="name" value="" maxlength="20">
      <ul>
        <li class="title">暱稱:</li>
        <li class="content"><input type="text" name="nickname" value="" maxlength="20"></li>
      </ul>
    </li> -->
    <li class="title">LINE / Facebook:</li>
    <li class="content">
      <ul>
        <li class="share_item">
        <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-facebook fa-w-16 fa-2x"><path fill="currentColor" d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z" class="">
        </path></svg><input type="text" name="fb_share" maxlength="128" value="<?php echo $_smarty_tpl->tpl_vars['data_base']->value['fb_share'];?>
" placeholder="請輸入網址">
        </li><li class="share_item">
        <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="line" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-line fa-w-14 fa-2x"><path fill="currentColor" d="M272.1 204.2v71.1c0 1.8-1.4 3.2-3.2 3.2h-11.4c-1.1 0-2.1-.6-2.6-1.3l-32.6-44v42.2c0 1.8-1.4 3.2-3.2 3.2h-11.4c-1.8 0-3.2-1.4-3.2-3.2v-71.1c0-1.8 1.4-3.2 3.2-3.2H219c1 0 2.1.5 2.6 1.4l32.6 44v-42.2c0-1.8 1.4-3.2 3.2-3.2h11.4c1.8-.1 3.3 1.4 3.3 3.1zm-82-3.2h-11.4c-1.8 0-3.2 1.4-3.2 3.2v71.1c0 1.8 1.4 3.2 3.2 3.2h11.4c1.8 0 3.2-1.4 3.2-3.2v-71.1c0-1.7-1.4-3.2-3.2-3.2zm-27.5 59.6h-31.1v-56.4c0-1.8-1.4-3.2-3.2-3.2h-11.4c-1.8 0-3.2 1.4-3.2 3.2v71.1c0 .9.3 1.6.9 2.2.6.5 1.3.9 2.2.9h45.7c1.8 0 3.2-1.4 3.2-3.2v-11.4c0-1.7-1.4-3.2-3.1-3.2zM332.1 201h-45.7c-1.7 0-3.2 1.4-3.2 3.2v71.1c0 1.7 1.4 3.2 3.2 3.2h45.7c1.8 0 3.2-1.4 3.2-3.2v-11.4c0-1.8-1.4-3.2-3.2-3.2H301v-12h31.1c1.8 0 3.2-1.4 3.2-3.2V234c0-1.8-1.4-3.2-3.2-3.2H301v-12h31.1c1.8 0 3.2-1.4 3.2-3.2v-11.4c-.1-1.7-1.5-3.2-3.2-3.2zM448 113.7V399c-.1 44.8-36.8 81.1-81.7 81H81c-44.8-.1-81.1-36.9-81-81.7V113c.1-44.8 36.9-81.1 81.7-81H367c44.8.1 81.1 36.8 81 81.7zm-61.6 122.6c0-73-73.2-132.4-163.1-132.4-89.9 0-163.1 59.4-163.1 132.4 0 65.4 58 120.2 136.4 130.6 19.1 4.1 16.9 11.1 12.6 36.8-.7 4.1-3.3 16.1 14.1 8.8 17.4-7.3 93.9-55.3 128.2-94.7 23.6-26 34.9-52.3 34.9-81.5z" class=""></path></svg>
        <input type="text" name="line_share" value="<?php echo $_smarty_tpl->tpl_vars['data_base']->value['line_share'];?>
" maxlength="128"  placeholder="請輸入網址">
        </li>
      </ul>
    </li>
    <li class="title">選擇項目:</li>
    <li class="content flex">
        <?php if (!empty($_smarty_tpl->tpl_vars['data_base']->value['epro_server_item'])) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['data_base']->value['epro_server_item'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_b_i_v_0_saved_item = isset($_smarty_tpl->tpl_vars['d_b_i_v']) ? $_smarty_tpl->tpl_vars['d_b_i_v'] : false;
$__foreach_d_b_i_v_0_saved_key = isset($_smarty_tpl->tpl_vars['d_b_i_k']) ? $_smarty_tpl->tpl_vars['d_b_i_k'] : false;
$_smarty_tpl->tpl_vars['d_b_i_v'] = new Smarty_Variable();
$__foreach_d_b_i_v_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_b_i_v_0_total) {
$_smarty_tpl->tpl_vars['d_b_i_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_b_i_k']->value => $_smarty_tpl->tpl_vars['d_b_i_v']->value) {
$__foreach_d_b_i_v_0_saved_local_item = $_smarty_tpl->tpl_vars['d_b_i_v'];
?>
            <ul class="skill">
                <input type="hidden" name="id_epro_server_item[]" value="<?php echo $_smarty_tpl->tpl_vars['d_b_i_v']->value['id_epro_server_item'];?>
">
                <select name='id_epro_windows[]' onchange="get_id_epro_windows_item(this);">
                    <?php
$_from = $_smarty_tpl->tpl_vars['epro_windows']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_e_w_v_1_saved_item = isset($_smarty_tpl->tpl_vars['e_w_v']) ? $_smarty_tpl->tpl_vars['e_w_v'] : false;
$__foreach_e_w_v_1_saved_key = isset($_smarty_tpl->tpl_vars['e_w_k']) ? $_smarty_tpl->tpl_vars['e_w_k'] : false;
$_smarty_tpl->tpl_vars['e_w_v'] = new Smarty_Variable();
$__foreach_e_w_v_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_e_w_v_1_total) {
$_smarty_tpl->tpl_vars['e_w_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['e_w_k']->value => $_smarty_tpl->tpl_vars['e_w_v']->value) {
$__foreach_e_w_v_1_saved_local_item = $_smarty_tpl->tpl_vars['e_w_v'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['e_w_v']->value['id_epro_windows'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_b_i_v']->value['id_epro_windows'] == $_smarty_tpl->tpl_vars['e_w_v']->value['id_epro_windows']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['e_w_v']->value['title'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['e_w_v'] = $__foreach_e_w_v_1_saved_local_item;
}
}
if ($__foreach_e_w_v_1_saved_item) {
$_smarty_tpl->tpl_vars['e_w_v'] = $__foreach_e_w_v_1_saved_item;
}
if ($__foreach_e_w_v_1_saved_key) {
$_smarty_tpl->tpl_vars['e_w_k'] = $__foreach_e_w_v_1_saved_key;
}
?>
                </select>
                <select name='id_epro_windows_item[]'>
                    <?php
$_from = $_smarty_tpl->tpl_vars['epro_windows_item']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_e_w_i_v_2_saved_item = isset($_smarty_tpl->tpl_vars['e_w_i_v']) ? $_smarty_tpl->tpl_vars['e_w_i_v'] : false;
$__foreach_e_w_i_v_2_saved_key = isset($_smarty_tpl->tpl_vars['e_w_i_k']) ? $_smarty_tpl->tpl_vars['e_w_i_k'] : false;
$_smarty_tpl->tpl_vars['e_w_i_v'] = new Smarty_Variable();
$__foreach_e_w_i_v_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_e_w_i_v_2_total) {
$_smarty_tpl->tpl_vars['e_w_i_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['e_w_i_k']->value => $_smarty_tpl->tpl_vars['e_w_i_v']->value) {
$__foreach_e_w_i_v_2_saved_local_item = $_smarty_tpl->tpl_vars['e_w_i_v'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['e_w_i_v']->value['id_epro_windows_item'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_b_i_v']->value['id_epro_windows_item'] == $_smarty_tpl->tpl_vars['e_w_i_v']->value['id_epro_windows_item']) {?>selected="selected"<?php }?>  <?php if ($_smarty_tpl->tpl_vars['e_w_i_v']->value['id_epro_windows'] != $_smarty_tpl->tpl_vars['d_b_i_v']->value['id_epro_windows']) {?>style="display:none;"<?php }?>><?php echo $_smarty_tpl->tpl_vars['e_w_i_v']->value['title'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['e_w_i_v'] = $__foreach_e_w_i_v_2_saved_local_item;
}
}
if ($__foreach_e_w_i_v_2_saved_item) {
$_smarty_tpl->tpl_vars['e_w_i_v'] = $__foreach_e_w_i_v_2_saved_item;
}
if ($__foreach_e_w_i_v_2_saved_key) {
$_smarty_tpl->tpl_vars['e_w_i_k'] = $__foreach_e_w_i_v_2_saved_key;
}
?>
                </select>
                <div class="skill_input flex">
                    <input type="number" name="price[]" value="<?php echo $_smarty_tpl->tpl_vars['d_b_i_v']->value['price'];?>
" placeholder="價格">
                    <input type="text" name="unit[]" value="<?php echo $_smarty_tpl->tpl_vars['d_b_i_v']->value['unit'];?>
" placeholder="單位" maxlength="32">
                    <?php if ($_smarty_tpl->tpl_vars['d_b_i_k']->value == 0) {?>
                        <svg onclick="server_item_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
                    <?php } else { ?>
                        <svg onclick="server_item_delete(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="minus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-minus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 242c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12H108c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h232z" class=""></path></svg>
                    <?php }?>
                </div>
            </ul>
          <?php
$_smarty_tpl->tpl_vars['d_b_i_v'] = $__foreach_d_b_i_v_0_saved_local_item;
}
}
if ($__foreach_d_b_i_v_0_saved_item) {
$_smarty_tpl->tpl_vars['d_b_i_v'] = $__foreach_d_b_i_v_0_saved_item;
}
if ($__foreach_d_b_i_v_0_saved_key) {
$_smarty_tpl->tpl_vars['d_b_i_k'] = $__foreach_d_b_i_v_0_saved_key;
}
?>

        <?php } else { ?>
            <ul class="skill">
                <input type="hidden" name="id_epro_server_item[]">
                <select name='id_epro_windows[]' onchange="get_id_epro_windows_item(this);">
                    <?php
$_from = $_smarty_tpl->tpl_vars['epro_windows']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_e_w_v_3_saved_item = isset($_smarty_tpl->tpl_vars['e_w_v']) ? $_smarty_tpl->tpl_vars['e_w_v'] : false;
$__foreach_e_w_v_3_saved_key = isset($_smarty_tpl->tpl_vars['e_w_k']) ? $_smarty_tpl->tpl_vars['e_w_k'] : false;
$_smarty_tpl->tpl_vars['e_w_v'] = new Smarty_Variable();
$__foreach_e_w_v_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_e_w_v_3_total) {
$_smarty_tpl->tpl_vars['e_w_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['e_w_k']->value => $_smarty_tpl->tpl_vars['e_w_v']->value) {
$__foreach_e_w_v_3_saved_local_item = $_smarty_tpl->tpl_vars['e_w_v'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['e_w_v']->value['id_epro_windows'];?>
"><?php echo $_smarty_tpl->tpl_vars['e_w_v']->value['title'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['e_w_v'] = $__foreach_e_w_v_3_saved_local_item;
}
}
if ($__foreach_e_w_v_3_saved_item) {
$_smarty_tpl->tpl_vars['e_w_v'] = $__foreach_e_w_v_3_saved_item;
}
if ($__foreach_e_w_v_3_saved_key) {
$_smarty_tpl->tpl_vars['e_w_k'] = $__foreach_e_w_v_3_saved_key;
}
?>
                </select>
                <select name='id_epro_windows_item[]'>
                    <?php
$_from = $_smarty_tpl->tpl_vars['epro_windows_item']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_e_w_i_v_4_saved_item = isset($_smarty_tpl->tpl_vars['e_w_i_v']) ? $_smarty_tpl->tpl_vars['e_w_i_v'] : false;
$__foreach_e_w_i_v_4_saved_key = isset($_smarty_tpl->tpl_vars['e_w_i_k']) ? $_smarty_tpl->tpl_vars['e_w_i_k'] : false;
$_smarty_tpl->tpl_vars['e_w_i_v'] = new Smarty_Variable();
$__foreach_e_w_i_v_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_e_w_i_v_4_total) {
$_smarty_tpl->tpl_vars['e_w_i_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['e_w_i_k']->value => $_smarty_tpl->tpl_vars['e_w_i_v']->value) {
$__foreach_e_w_i_v_4_saved_local_item = $_smarty_tpl->tpl_vars['e_w_i_v'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['e_w_i_v']->value['id_epro_windows_item'];?>
" <?php if ($_smarty_tpl->tpl_vars['e_w_i_v']->value['id_epro_windows'] != '1') {?>style="display:none;"<?php }?>><?php echo $_smarty_tpl->tpl_vars['e_w_i_v']->value['title'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['e_w_i_v'] = $__foreach_e_w_i_v_4_saved_local_item;
}
}
if ($__foreach_e_w_i_v_4_saved_item) {
$_smarty_tpl->tpl_vars['e_w_i_v'] = $__foreach_e_w_i_v_4_saved_item;
}
if ($__foreach_e_w_i_v_4_saved_key) {
$_smarty_tpl->tpl_vars['e_w_i_k'] = $__foreach_e_w_i_v_4_saved_key;
}
?>
                </select>
                <div class="skill_input flex">
                    <input type="number" name="price[]" placeholder="價格">
                    <input type="text" name="unit[]" placeholder="單位" maxlength="32">
                    <svg onclick="server_item_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
                </div>
            </ul>
        <?php }?>
    </li>
    <li class="title">服務區域:</li>
    <li class="content zone">
      <ul  class="selectedzone">
          <?php if (!empty($_smarty_tpl->tpl_vars['data_base']->value['epro_server_address'])) {?>
              <?php
$_from = $_smarty_tpl->tpl_vars['data_base']->value['epro_server_address'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_b_a_v_5_saved_item = isset($_smarty_tpl->tpl_vars['d_b_a_v']) ? $_smarty_tpl->tpl_vars['d_b_a_v'] : false;
$__foreach_d_b_a_v_5_saved_key = isset($_smarty_tpl->tpl_vars['d_b_a_k']) ? $_smarty_tpl->tpl_vars['d_b_a_k'] : false;
$_smarty_tpl->tpl_vars['d_b_a_v'] = new Smarty_Variable();
$__foreach_d_b_a_v_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_d_b_a_v_5_total) {
$_smarty_tpl->tpl_vars['d_b_a_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['d_b_a_k']->value => $_smarty_tpl->tpl_vars['d_b_a_v']->value) {
$__foreach_d_b_a_v_5_saved_local_item = $_smarty_tpl->tpl_vars['d_b_a_v'];
?>
                  <li class="add">
                      <input type="hidden" name="id_epro_server_address[]" value="<?php echo $_smarty_tpl->tpl_vars['d_b_a_v']->value['id_epro_server_address'];?>
">
                      <select class="county" name="id_county[]" onchange="get_city(this);">
                          <?php
$_from = $_smarty_tpl->tpl_vars['county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_c_v_6_saved_item = isset($_smarty_tpl->tpl_vars['c_v']) ? $_smarty_tpl->tpl_vars['c_v'] : false;
$__foreach_c_v_6_saved_key = isset($_smarty_tpl->tpl_vars['c_k']) ? $_smarty_tpl->tpl_vars['c_k'] : false;
$_smarty_tpl->tpl_vars['c_v'] = new Smarty_Variable();
$__foreach_c_v_6_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_c_v_6_total) {
$_smarty_tpl->tpl_vars['c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['c_k']->value => $_smarty_tpl->tpl_vars['c_v']->value) {
$__foreach_c_v_6_saved_local_item = $_smarty_tpl->tpl_vars['c_v'];
?>
                              <option value="<?php echo $_smarty_tpl->tpl_vars['c_v']->value['id_county'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['c_v']->value['county_name'];?>
" <?php if ($_smarty_tpl->tpl_vars['d_b_a_v']->value['id_county'] == $_smarty_tpl->tpl_vars['c_v']->value['id_county']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['c_v']->value['county_name'];?>
</option>
                          <?php
$_smarty_tpl->tpl_vars['c_v'] = $__foreach_c_v_6_saved_local_item;
}
}
if ($__foreach_c_v_6_saved_item) {
$_smarty_tpl->tpl_vars['c_v'] = $__foreach_c_v_6_saved_item;
}
if ($__foreach_c_v_6_saved_key) {
$_smarty_tpl->tpl_vars['c_k'] = $__foreach_c_v_6_saved_key;
}
?>
                      </select>
                      <select  name="id_city[]" class="city">
                          <?php
$_from = $_smarty_tpl->tpl_vars['city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_c_v_7_saved_item = isset($_smarty_tpl->tpl_vars['c_v']) ? $_smarty_tpl->tpl_vars['c_v'] : false;
$__foreach_c_v_7_saved_key = isset($_smarty_tpl->tpl_vars['c_k']) ? $_smarty_tpl->tpl_vars['c_k'] : false;
$_smarty_tpl->tpl_vars['c_v'] = new Smarty_Variable();
$__foreach_c_v_7_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_c_v_7_total) {
$_smarty_tpl->tpl_vars['c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['c_k']->value => $_smarty_tpl->tpl_vars['c_v']->value) {
$__foreach_c_v_7_saved_local_item = $_smarty_tpl->tpl_vars['c_v'];
?>
                              <option value="<?php echo $_smarty_tpl->tpl_vars['c_v']->value['id_city'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['c_v']->value['city_name'];?>
"  <?php if ($_smarty_tpl->tpl_vars['d_b_a_v']->value['id_city'] == $_smarty_tpl->tpl_vars['c_v']->value['id_city']) {?>selected="selected"<?php }?>  <?php if ($_smarty_tpl->tpl_vars['d_b_a_v']->value['id_county'] != $_smarty_tpl->tpl_vars['c_v']->value['id_county']) {?>style="display: none;" <?php }?> ><?php echo $_smarty_tpl->tpl_vars['c_v']->value['city_name'];?>
</option>
                          <?php
$_smarty_tpl->tpl_vars['c_v'] = $__foreach_c_v_7_saved_local_item;
}
}
if ($__foreach_c_v_7_saved_item) {
$_smarty_tpl->tpl_vars['c_v'] = $__foreach_c_v_7_saved_item;
}
if ($__foreach_c_v_7_saved_key) {
$_smarty_tpl->tpl_vars['c_k'] = $__foreach_c_v_7_saved_key;
}
?>
                      </select>
                      <?php if ($_smarty_tpl->tpl_vars['d_b_a_k']->value == 0) {?>
                          <svg onclick="server_address_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
                      <?php } else { ?>
                          <svg onclick="server_address_delete(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="minus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-minus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 242c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12H108c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h232z" class=""></path></svg>
                      <?php }?>
                  </li>
              <?php
$_smarty_tpl->tpl_vars['d_b_a_v'] = $__foreach_d_b_a_v_5_saved_local_item;
}
}
if ($__foreach_d_b_a_v_5_saved_item) {
$_smarty_tpl->tpl_vars['d_b_a_v'] = $__foreach_d_b_a_v_5_saved_item;
}
if ($__foreach_d_b_a_v_5_saved_key) {
$_smarty_tpl->tpl_vars['d_b_a_k'] = $__foreach_d_b_a_v_5_saved_key;
}
?>
          <?php } else { ?>
              <li class="add">
                  <input type="hidden" name="id_epro_server_address[]">
                  <select class="county" name="id_county[]" onchange="get_city(this);">
                      <?php
$_from = $_smarty_tpl->tpl_vars['preset_county']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_p_c_v_8_saved_item = isset($_smarty_tpl->tpl_vars['p_c_v']) ? $_smarty_tpl->tpl_vars['p_c_v'] : false;
$__foreach_p_c_v_8_saved_key = isset($_smarty_tpl->tpl_vars['p_c_k']) ? $_smarty_tpl->tpl_vars['p_c_k'] : false;
$_smarty_tpl->tpl_vars['p_c_v'] = new Smarty_Variable();
$__foreach_p_c_v_8_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_p_c_v_8_total) {
$_smarty_tpl->tpl_vars['p_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['p_c_k']->value => $_smarty_tpl->tpl_vars['p_c_v']->value) {
$__foreach_p_c_v_8_saved_local_item = $_smarty_tpl->tpl_vars['p_c_v'];
?>
                          <option value="<?php echo $_smarty_tpl->tpl_vars['p_c_v']->value['id_county'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['p_c_v']->value['county_name'];?>
" <?php if ($_smarty_tpl->tpl_vars['p_c_v']->value['checked'] == 'checked') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['p_c_v']->value['county_name'];?>
</option>
                      <?php
$_smarty_tpl->tpl_vars['p_c_v'] = $__foreach_p_c_v_8_saved_local_item;
}
}
if ($__foreach_p_c_v_8_saved_item) {
$_smarty_tpl->tpl_vars['p_c_v'] = $__foreach_p_c_v_8_saved_item;
}
if ($__foreach_p_c_v_8_saved_key) {
$_smarty_tpl->tpl_vars['p_c_k'] = $__foreach_p_c_v_8_saved_key;
}
?>
                  </select>
                  <select  name="id_city[]" class="city">
                      <?php
$_from = $_smarty_tpl->tpl_vars['preset_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_p_c_v_9_saved_item = isset($_smarty_tpl->tpl_vars['p_c_v']) ? $_smarty_tpl->tpl_vars['p_c_v'] : false;
$__foreach_p_c_v_9_saved_key = isset($_smarty_tpl->tpl_vars['p_c_k']) ? $_smarty_tpl->tpl_vars['p_c_k'] : false;
$_smarty_tpl->tpl_vars['p_c_v'] = new Smarty_Variable();
$__foreach_p_c_v_9_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_p_c_v_9_total) {
$_smarty_tpl->tpl_vars['p_c_k'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['p_c_k']->value => $_smarty_tpl->tpl_vars['p_c_v']->value) {
$__foreach_p_c_v_9_saved_local_item = $_smarty_tpl->tpl_vars['p_c_v'];
?>
                          <option value="<?php echo $_smarty_tpl->tpl_vars['p_c_v']->value['id_city'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['p_c_v']->value['city_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['p_c_v']->value['city_name'];?>
</option>
                      <?php
$_smarty_tpl->tpl_vars['p_c_v'] = $__foreach_p_c_v_9_saved_local_item;
}
}
if ($__foreach_p_c_v_9_saved_item) {
$_smarty_tpl->tpl_vars['p_c_v'] = $__foreach_p_c_v_9_saved_item;
}
if ($__foreach_p_c_v_9_saved_key) {
$_smarty_tpl->tpl_vars['p_c_k'] = $__foreach_p_c_v_9_saved_key;
}
?>
                  </select>
                  <svg onclick="server_address_add(this);" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-plus-square fa-w-14 fa-3x"><path fill="currentColor" d="M400 64c8.8 0 16 7.2 16 16v352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h352m0-32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-60 206h-98v-98c0-6.6-5.4-12-12-12h-12c-6.6 0-12 5.4-12 12v98h-98c-6.6 0-12 5.4-12 12v12c0 6.6 5.4 12 12 12h98v98c0 6.6 5.4 12 12 12h12c6.6 0 12-5.4 12-12v-98h98c6.6 0 12-5.4 12-12v-12c0-6.6-5.4-12-12-12z" class=""></path></svg>
              </li>
          <?php }?>
      </ul>
  </li>
</ul>
</div>
<?php }
}
