<?php
/* Smarty version 3.1.28, created on 2021-03-31 21:15:34
  from "/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Index/content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_606475f63252e1_53338719',
  'file_dependency' => 
  array (
    'dc5b514c432c9ba6c2c82b7f9a0e29b936c55081' => 
    array (
      0 => '/opt/lampp/htdocs/life-house.com.tw/themes/Epro/controllers/Index/content.tpl',
      1 => 1617196524,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606475f63252e1_53338719 ($_smarty_tpl) {
?>
  <section class="menu cid-s48OLK6784" once="menu" id="menu1-w">
    <nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg">
        <div class="container">
            <div class="navbar-brand" style="margin:0;padding:0;">
                <span class="navbar-logo">
                    <div class="logo_title">
                    <a href="#">
                        <img src="/themes/Epro/assets/images/logo_title.jpg">
                    </a>
                    </div>
                    <a href="#">
                        <img src="/themes/Epro/assets/images/logo.jpg">
                    </a>
                </span>
                <div class="search__container">
                    <span class="navbar-logo">
                    <div class="search_img">
                        <img src="/themes/Epro/assets/images/Search-location.jpg" style="max-height:2rem;width:auto;">
                    </div>
                    <input class="search__input" type="text" placeholder="搜尋">
                    </span>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true"><li class="nav-item"><a class="nav-link link display-4" href="#">註冊</a></li><li class="nav-item"><a class="nav-link link display-4" href="#">登入</a></li>
                    <li class="nav-item">
                        <a class="nav-link link text-black display-4" href="#"></a></li>
                        <li class="nav-item"><a class="nav-link link text-black display-4" href="#"><span class="socicon socicon-sharethis mbr-iconfont mbr-iconfont-btn"></span></a></li>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>


<section class="gallery1 cid-srUft5QXg9" id="gallery1-1a">
    
    
    <div>
        
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-9-618x464.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-10-618x464.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-11-618x464.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            
        </div>
    </div>
</section>

<section class="gallery6 mbr-gallery cid-srUgmoNdvO" id="gallery6-1b">
    

    

    <div class="container-fluid">
        <div class="mbr-section-head">
            <div class="col-12">
                <img src="/themes/Epro/assets/images/menu_1.jpg" class="menu-img">
            </div>
        </div>
        <div class="row mbr-gallery mt-4" style="max-width:60rem;margin:0 auto;">
            <div class="col-12 col-md-6 col-lg-4 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJ2kRTl-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/service1.jpg" alt="" data-slide-to="0" data-target="#lb-ss4DJ2kRTl">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-6 col-lg-4 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJ2kRTl-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/service2.jpg" alt="" data-slide-to="1" data-target="#lb-ss4DJ2kRTl">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-6 col-lg-4 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJ2kRTl-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/service3.jpg" alt="" data-slide-to="2" data-target="#lb-ss4DJ2kRTl">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            
        </div>

        <div class="modal mbr-slider" tabindex="-1" role="dialog" aria-hidden="true" id="ss4DJ2kRTl-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="carousel slide" id="lb-ss4DJ2kRTl" data-interval="5000">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-18-518x240.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-19-518x240.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-20-518x240.jpg" alt="">
                                </div>
                                
                            </div>
                            <ol class="carousel-indicators">
                                <li data-slide-to="0" class="active" data-target="#lb-ss4DJ2kRTl"></li>
                                <li data-slide-to="1" data-target="#lb-ss4DJ2kRTl"></li>
                                <li data-slide-to="2" data-target="#lb-ss4DJ2kRTl"></li>
                                
                            </ol>
                            <a role="button" href="" class="close" data-dismiss="modal" aria-label="Close">
                            </a>
                            <a class="carousel-control-prev carousel-control" role="button" data-slide="prev" href="#lb-ss4DJ2kRTl">
                                <span class="mobi-mbri mobi-mbri-arrow-prev" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next carousel-control" role="button" data-slide="next" href="#lb-ss4DJ2kRTl">
                                <span class="mobi-mbri mobi-mbri-arrow-next" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="header2 cid-srUj4mbJsE mbr-fullscreen" id="header2-1d" style="min-height:10vh;">
    <div class="container-fluid">
        <div class="row justify-content-end">
            <div class="col-12 col-lg-6" style="max-width:40%;flex:0 0 60%;height:10rem;">
            </div>
        </div>
    </div>
</section>

<section class="content5 cid-srUljWQ7xX" id="content5-1f">
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-10">
                
                
                <p class="mbr-text mbr-fonts-style display-7">---</p>
            </div>
        </div>
    </div>
</section>

<section class="header2 cid-srUj5s7ovv mbr-fullscreen" id="header2-1e" style="min-height:10vh;">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-4" style="max-width:50%;flex:0 0 50%;height:10rem;">
            </div>
        </div>
    </div>
</section>

<section class="gallery5 mbr-gallery cid-srUmQUf7Jo" id="gallery5-1g">
    

    

    <div class="container">
        <div class="mbr-section-head">
            <div class="col-12">
                <img src="/themes/Epro/assets/images/menu_2.jpg" class="menu-img">
            </div>
        </div>
        <div class="row mbr-gallery mt-4">
            <div class="col-12 col-md-6 col-lg-4 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJ6K5ru-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/-32-551x128.jpg" alt="" data-slide-to="0" data-target="#lb-ss4DJ6K5ru">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-6 col-lg-4 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJ6K5ru-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/-34-553x128.jpg" alt="" data-slide-to="1" data-target="#lb-ss4DJ6K5ru">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-6 col-lg-4 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJ6K5ru-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/-36-552x128.jpg" alt="" data-slide-to="2" data-target="#lb-ss4DJ6K5ru">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            
        </div>

        <div class="modal mbr-slider" tabindex="-1" role="dialog" aria-hidden="true" id="ss4DJ6K5ru-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="carousel slide" id="lb-ss4DJ6K5ru" data-interval="6000">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-32-551x128.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-34-553x128.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-36-552x128.jpg" alt="">
                                </div>
                                
                            </div>
                            <ol class="carousel-indicators">
                                <li data-slide-to="0" class="active" data-target="#lb-ss4DJ6K5ru"></li>
                                <li data-slide-to="1" data-target="#lb-ss4DJ6K5ru"></li>
                                <li data-slide-to="2" data-target="#lb-ss4DJ6K5ru"></li>
                                
                            </ol>
                            <a role="button" href="" class="close" data-dismiss="modal" aria-label="Close">
                            </a>
                            <a class="carousel-control-prev carousel-control" role="button" data-slide="prev" href="#lb-ss4DJ6K5ru">
                                <span class="mobi-mbri mobi-mbri-arrow-prev" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next carousel-control" role="button" data-slide="next" href="#lb-ss4DJ6K5ru">
                                <span class="mobi-mbri mobi-mbri-arrow-next" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="features3 cid-srUnJYn8Lr" id="features3-1h">
    
    
    <div class="container">
        <div class="mbr-section-head">
            <div class="col-12">
                <img src="/themes/Epro/assets/images/menu_3.jpg" class="menu-img">
            </div>
        </div>
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-45-518x466.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-47-518x466.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-49-517x466.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!--
<section class="team1 cid-srUup6ADP3" id="team1-1i" style="background:#FFF;">
    

    
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12" style="margin-bottom:2em;">
                <img src="/themes/Epro/assets/images/menu_4.jpg" class="menu-img">
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card-wrap">
                    <div class="image-wrap">
                        <img src="/themes/Epro/assets/images/-56-518x270.jpg" alt="">
                    </div>
                    <div class="content-wrap">
                        
                        
                        
                        
                        
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="card-wrap">
                    <div class="image-wrap">
                        <img src="/themes/Epro/assets/images/-57-518x270.jpg" alt="">
                    </div>
                    <div class="content-wrap">
                        
                        
                        
                        
                        
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="card-wrap">
                    <div class="image-wrap">
                        <img src="/themes/Epro/assets/images/-58-517x270.jpg" alt="">
                    </div>
                    <div class="content-wrap">
                        
                        
                        
                        
                        
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</section>
-->
<section class="features3 cid-srUnJYn8Lr" id="features3-1h">
    
    
    <div class="container">
        <div class="mbr-section-head">
            <div class="col-12">
                <img src="/themes/Epro/assets/images/menu_4.jpg" class="menu-img">
            </div>
        </div>
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-56-518x270.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-57-518x270.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-4">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-58-517x270.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>
<section class="gallery3 cid-srUvpS8kUh" id="gallery3-1j">
    
    
    <div class="container-fluid">
        <div class="mbr-section-head">
                <img src="/themes/Epro/assets/images/menu_5.jpg" class="menu-img">
        </div>
        <div class="row mt-4">
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="menu">
                        <img src="/themes/Epro/assets/images/-65-392x225.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-67-391x225.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-69-392x225.jpg" alt="">
                    </div>
                    
                    
                </div>
            </div>
            <div class="item features-image сol-12 col-md-6 col-lg-3">
                <div class="item-wrapper">
                    <div class="item-img">
                        <img src="/themes/Epro/assets/images/-71-391x225.jpg" alt="" title="">
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gallery5 mbr-gallery cid-srUvsrbOaC" id="gallery5-1k">
    
    <div class="container-fluid">
        
        <div class="row mbr-gallery">
            <div class="col-12 col-md-6 col-lg-3 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJcj5S9-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/-76-392x224.jpg" alt="" data-slide-to="0" data-target="#lb-ss4DJcj5S9">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-6 col-lg-3 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJcj5S9-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/-77-391x224.jpg" alt="" data-slide-to="1" data-target="#lb-ss4DJcj5S9">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-6 col-lg-3 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJcj5S9-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/-78-392x224.jpg" alt="" data-slide-to="2" data-target="#lb-ss4DJcj5S9">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-12 col-md-6 col-lg-3 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#ss4DJcj5S9-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/-79-391x224.jpg" alt="" data-slide-to="3" data-target="#lb-ss4DJcj5S9">
                    <div class="icon-wrapper">
                        <span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="modal mbr-slider" tabindex="-1" role="dialog" aria-hidden="true" id="ss4DJcj5S9-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="carousel slide" id="lb-ss4DJcj5S9" data-interval="5000">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-76-392x224.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-77-391x224.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-78-392x224.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/themes/Epro/assets/images/-79-391x224.jpg" alt="">
                                </div>
                            </div>
                            <ol class="carousel-indicators">
                                <li data-slide-to="0" class="active" data-target="#lb-ss4DJcj5S9"></li>
                                <li data-slide-to="1" data-target="#lb-ss4DJcj5S9"></li>
                                <li data-slide-to="2" data-target="#lb-ss4DJcj5S9"></li>
                                <li data-slide-to="3" data-target="#lb-ss4DJcj5S9"></li>
                            </ol>
                            <a role="button" href="" class="close" data-dismiss="modal" aria-label="Close">
                            </a>
                            <a class="carousel-control-prev carousel-control" role="button" data-slide="prev" href="#lb-ss4DJcj5S9">
                                <span class="mobi-mbri mobi-mbri-arrow-prev" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next carousel-control" role="button" data-slide="next" href="#lb-ss4DJcj5S9">
                                <span class="mobi-mbri mobi-mbri-arrow-next" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="gallery5 mbr-gallery cid-srTmquGlWr" id="gallery5-15">
    <div class="container-fluid">
        <div class="mbr-section-head">
            <h3 class="mbr-section-title mbr-fonts-style align-center m-0 display-2"></h3>
            <h4 class="mbr-section-subtitle mbr-fonts-style align-center mb-0 mt-2 display-5"></h4>
        </div>
        <div class="row mbr-gallery mt-4">
            <div class="col-12 col-md-6 col-lg-2 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#srTBgTXvjq-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/footer0.jpg" alt="" data-slide-to="0" data-target="#lb-srTBgTXvjq">
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-2 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#srTBgTXvjq-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/footer1.jpg" alt="" data-slide-to="1" data-target="#lb-srTBgTXvjq">
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-2 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#srTBgTXvjq-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/footer2.jpg" alt="" data-slide-to="2" data-target="#lb-srTBgTXvjq">
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-2 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#srTBgTXvjq-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/footer3.jpg" alt="" data-slide-to="3" data-target="#lb-srTBgTXvjq">
                </div>
            </div>
            
            <div class="col-12 col-md-6 col-lg-2 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#srTBgTXvjq-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/footer4.jpg" alt="" data-slide-to="3" data-target="#lb-srTBgTXvjq">
                </div>
            </div>
            
            <div class="col-12 col-md-6 col-lg-2 item gallery-image">
                <div class="item-wrapper" data-toggle="modal" data-target="#srTBgTXvjq-modal">
                    <img class="w-100" src="/themes/Epro/assets/images/footer5.jpg" alt="" data-slide-to="3" data-target="#lb-srTBgTXvjq">
                </div>
            </div>
        </div>
    </div>
</section>
    
        <?php echo '<script'; ?>
 src="/themes/Epro/assets/web/assets/jquery/jquery.min.js"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 src="/themes/Epro/assets/popper/popper.min.js"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 src="/themes/Epro/assets/tether/tether.min.js"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 src="/themes/Epro/assets/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 src="/themes/Epro/assets/smoothscroll/smooth-scroll.js"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 src="/themes/Epro/assets/dropdown/js/nav-dropdown.js"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 src="/themes/Epro/assets/dropdown/js/navbar-dropdown.js"><?php echo '</script'; ?>
>  <?php }
}
